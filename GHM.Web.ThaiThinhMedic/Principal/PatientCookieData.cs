﻿namespace GHM.Web.ThaiThinhMedic.Principal
{
    public class PatientCookieData
    {
        public string PatientId { get; set; }
        public string PatientName { get; set; }
    }
}