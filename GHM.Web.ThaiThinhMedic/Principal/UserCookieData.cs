﻿namespace GHM.Web.ThaiThinhMedic.Principal
{
    public class UserCookieData
    {
        public string Id { get; set; }
        public string UserName { get; set; }
    }
}
