﻿namespace GHM.Web.ThaiThinhMedic.Models
{
    public class PatientWaiting
    {
        public string MaBenhNhan { get; set; }
        public int TrangThai { get; set; }
        public string SoPhong { get; set; }
    }
}