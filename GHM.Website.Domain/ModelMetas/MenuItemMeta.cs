﻿using System;

namespace GHM.Website.Domain.ModelMetas
{
    public class MenuItemMeta
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int MenuId { get; set; } // Mã nhóm menu

        public long? ObjectId { get; set; } // Mã đối tượng menu

        public byte ObjectType { get; set; } // Loại đối tượng menu: 0: Custom 1: Nhóm 2: Bài viết

        public string ObjectName { get; set; } // Tên đối tượng sử dụng

        public string Url { get; set; } // Đường dẫn khi click vào link

        public bool IsActive { get; set; }

        public bool IsDelete { get; set; } // Trạng thái đã xóa hay chưa

        public int? ParentId { get; set; } // Mã menu cha

        public string IdPath { get; set; } // IdPath

        public DateTime CreateTime { get; set; }

        public string CreatorId { get; set; }

        public string CreatorFullName { get; set; }

        public byte Order { get; set; }
    }
}
