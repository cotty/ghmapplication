﻿using System;

namespace GHM.Website.Domain.ModelMetas
{
    public class FaqMeta
    {
        public int Id { get; set; }
     
        public string Question { get; set; }

        public string Answer { get; set; }

        public string CreatorId { get; set; }

        public string CreatorName { get; set; }

        public DateTime CreateTime { get; set; }

        public bool IsDelete { get; set; }

        public bool IsActive { get; set; }
    }
}
