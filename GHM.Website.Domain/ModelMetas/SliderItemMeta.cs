﻿namespace GHM.Website.Domain.ModelMetas
{
    public class SliderItemMeta
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public string ImageUrl { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public string SliderId { get; set; }

        public bool IsDelete { get; set; }
    }
}
