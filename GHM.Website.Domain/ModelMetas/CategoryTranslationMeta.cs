﻿namespace GHM.Website.Domain.ModelMetas
{
    public class CategoryTranslationMeta
    {
        public int CategoryId { get; set; }
        public string LanguageId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SeoLink { get; set; }
    }
}
