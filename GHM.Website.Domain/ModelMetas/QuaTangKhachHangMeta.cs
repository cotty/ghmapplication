﻿using System;
using System.Collections.Generic;

namespace GHM.Website.Domain.ModelMetas
{
    public class QuaTangKhachHangMeta
    {
        public long? IDQuaTang { get; set; }

        public DateTime Ngay { get; set; }

        public string MaQuaTang { get; set; }

        public string MaKhachHang { get; set; }

        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string MaNguoiPhat { get; set; }

        public bool? Dung { get; set; }

        public List<string> NguoiThamGiaCung { get; set; }
    }
}
