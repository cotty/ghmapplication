﻿using System;
using System.ComponentModel;

namespace GHM.Website.Domain.ModelMetas
{
    public class DatHenMeta
    {
        public long Id { get; set; }

        public string FullName { get; set; }

        public DateTime Birthday { get; set; }

        public string PhoneNumber { get; set; }

        public string Address { get; set; }

        [DisplayName(@"Email")]
        public string Email { get; set; }

        public string DoctorId { get; set; }

        public string DoctorName { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public string Hours { get; set; }

        public string ServiceId { get; set; }

        public string ServiceName { get; set; }

        public string Note { get; set; }

        public string Shift { get; set; }

        public string PromotionCode { get; set; }

        public decimal? PromotionServicePrice { get; set; }

        public decimal PromotionDiscountPrice { get; set; }

        public byte? PromotionDiscountPercent { get; set; }

        public decimal? PromotionTotalPayable { get; set; }
    }
}
