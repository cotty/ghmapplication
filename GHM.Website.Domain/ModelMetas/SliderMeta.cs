﻿using System;
using GHM.Website.Domain.Constants;

namespace GHM.Website.Domain.ModelMetas
{
    public class SliderMeta
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime CreateTime { get; set; }

        public string CreatorId { get; set; }

        public string CreatorFullName { get; set; }

        public bool IsActive { get; set; }

        public bool IsDelete { get; set; }

        public SliderType Type { get; set; }
    }
}
