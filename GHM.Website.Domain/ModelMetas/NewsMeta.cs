﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GHM.Website.Domain.ModelMetas
{
    public class NewsMeta
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string UnsignName { get; set; }

        public string Content { get; set; }

        public DateTime CreateTime { get; set; }

        public string CreatorId { get; set; }

        public string CreatorFullName { get; set; }

        public string CreatorImage { get; set; }

        public int ViewCount { get; set; }

        public int CommentCount { get; set; }

        public int LikeCount { get; set; }

        public string Source { get; set; }

        public bool IsActive { get; set; }

        public bool IsDelete { get; set; }

        public string Tags { get; set; }

        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public string Attachments { get; set; }

        public string Image { get; set; }

        public int PageId { get; set; }

        public bool IsHot { get; set; }

        public bool IsHomePage { get; set; }
    }
}
