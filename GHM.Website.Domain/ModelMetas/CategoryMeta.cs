﻿using System;
using System.Collections.Generic;

namespace GHM.Website.Domain.ModelMetas
{
    public class CategoryMeta
    {
        public string TenantId { get; set; }

        public string CreatorId { get; set; }

        public string CreatorFullName { get; set; }

        public bool IsActive { get; set; }

        public bool IsDelete { get; set; }

        public int? ParentId { get; set; }

        public string IdPath { get; set; }

        public string Image { get; set; }

        public List<CategoryTranslationMeta> CategoryTranslations { get; set; }
    }
}
