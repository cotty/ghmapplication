﻿namespace GHM.Website.Domain.Constants
{
    public enum ImageType
    {
        Jpg,
        Png
    }
}
