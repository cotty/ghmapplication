﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Website.Domain.Constants
{
    public enum VideoType
    {
        YouTube,
        Vimeo,
        Custom
    }
}
