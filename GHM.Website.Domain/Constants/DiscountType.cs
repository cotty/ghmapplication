﻿namespace GHM.Website.Domain.Constants
{
    public enum DiscountType
    {
        Percent = 1,
        Money = 2
    }
}

