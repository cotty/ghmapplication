﻿namespace GHM.Website.Domain.Constants
{
    public enum SystemLogType
    {
        Debug = 1,
        Information = 2,
        Warning = 3,
        Error = 4,
        Fatal = 5
    }
}
