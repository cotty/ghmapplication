﻿namespace GHM.Website.Domain.Constants
{
    public enum SliderType
    {
        HomeSlider = 1,
        HomePopup = 2
    }
}
