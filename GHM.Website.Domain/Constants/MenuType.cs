﻿namespace GHM.Website.Domain.Constants
{
    public enum MenuType
    {
        Custom,
        Category,
        News
    }
}
