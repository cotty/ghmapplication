﻿namespace GHM.Website.Domain.Constants
{
    public enum ReferenceType
    {
        /// <summary>
        /// Tự nhập.
        /// </summary>
        Custom,
        /// <summary>
        /// Nhóm chuyên mục.
        /// </summary>
        Category,
        /// <summary>
        /// Tin tức.
        /// </summary>
        News
    }
}
