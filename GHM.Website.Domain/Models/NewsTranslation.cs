﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Website.Domain.Models
{
    public class NewsTranslation
    {
        public string NewsId { get; set; }
        public string LanguageId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string UnsignName { get; set; }
        public string SeoLink { get; set; }
        public string Content { get; set; }
    }
}
