﻿namespace GHM.Website.Domain.Models
{
    public class FaqTranslation
    {
        public string FaqId { get; set; }
        public string LanguageId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
