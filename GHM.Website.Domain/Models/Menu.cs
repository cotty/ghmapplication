﻿using GHM.Infrastructure.Models;
using GHM.Website.Domain.Constants;

namespace GHM.Website.Domain.Models
{
    public class Menu : EntityBase<int>
    {
        /// <summary>
        /// Id của khách hàng.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Trạng thái kích hoạt của trang. Trường hợp không kích hoạt sẽ không được hiển thị ngoài menu của người dùng.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Đường dẫn Id của trang.
        /// </summary>
        public string IdPath { get; set; }

        /// <summary>
        /// Icon của trang.
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// Thứ tự hiển thị của trang.
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Mã trang cha.
        /// </summary>
        public int? ParentId { get; set; }

        /// <summary>
        /// Số lượng trang con.
        /// </summary>
        public int ChildCount { get; set; }

        public int? ReferenceId { get; set; }

        public ReferenceType ReferenceType { get; set; }

        public bool IsDelete { get; set; }

        public string CreatorId { get; set; }

        public string CreatorFullName { get; set; }

        public Menu()
        {
            IdPath = "";
            ReferenceId = null;
            ReferenceType = ReferenceType.Custom;
        }

        public Menu(string icon, bool isActive, int order, int? referenceId, ReferenceType referenceType, int? parentId,
            string idPath)
        {
            Icon = icon?.Trim();
            IsActive = isActive;
            Order = order;
            ReferenceId = referenceId;
            ReferenceType = referenceType;
            ParentId = parentId;
            IdPath = idPath;
        }
    }
}
