﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Website.Domain.Models
{
    public class MenuTranslation
    {
        public string MenuId { get; set; }

        public string LanguageId { get; set; }
        /// <summary>
        /// Tên trang.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Tên viết không dấu hỗ trợ tìm kiếm tương đối.
        /// </summary>
        public string UnsignName { get; set; }

        /// <summary>
        /// Đường dẫn trang.
        /// </summary>
        public string Url { get; set; }
    }
}
