﻿using GHM.Infrastructure.Models;

namespace GHM.Website.Domain.Models
{
    public class Category : EntityBase<int>
    {
        public string TenantId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public string IdPath { get; set; }
        public int? ParentId { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }

        public Category() { }

        public Category(bool isActive, int? parentId)
        {

            IdPath = "-1";
            IsActive = isActive;
            ParentId = parentId;
        }
    }
}
