﻿using System.Threading.Tasks;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Website.Domain.ModelMetas;
using GHM.Website.Domain.ViewModels;

namespace GHM.Website.Domain.IServices
{
    public interface ICourseService
    {
        Task<ActionResultResponse> Insert(CourseMeta courseMeta);

        Task<ActionResultResponse> Update(string id, CourseMeta courseMeta);

        Task<ActionResultResponse> Delete(string id);

        Task<SearchResult<CourseSearchViewModel>> Search(string keyword, bool? isActive, int page, int pageSize);
    }
}
