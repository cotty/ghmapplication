﻿using System.Threading.Tasks;
using GHM.Infrastructure.Models;
using GHM.Website.Domain.ModelMetas;
using GHM.Website.Domain.Models;

namespace GHM.Website.Domain.IServices
{
    public interface ICategoryTranslationService
    {
        Task<ActionResultResponse> Save(CategoryTranslationMeta categoryTranslationMeta);

        Task<ActionResultResponse> Insert(CategoryTranslationMeta categoryTranslationMeta);

        Task<ActionResultResponse> Update(CategoryTranslationMeta categoryTranslationMeta);
    }
}
