﻿using System.Threading.Tasks;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Website.Domain.ModelMetas;
using GHM.Website.Domain.ViewModels;

namespace GHM.Website.Domain.IServices
{
    public interface ICategoryService
    {
        Task<ActionResultResponse> Insert(CategoryMeta categoryMeta);

        Task<ActionResultResponse> Update(int id, CategoryMeta categoryMeta);

        Task<ActionResultResponse> Delete(int id);

        Task<SearchResult<CategorySearhViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive, int page, int pageSize);
    }
}
