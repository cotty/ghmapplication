﻿using GHM.Website.Domain.ModelMetas;
using GHM.Website.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GHM.Website.Domain.IRepository
{
    public interface INewsRepository
    {
        ICategoryRepository CategoryRepository { get; set; }

        Task<int> Insert(NewsMeta newsMeta);

        Task<int> Update(NewsMeta newsMeta);

        Task<int> Delete(long id);

        Task<News> GetInfo(long id, bool? isActive = null);

        Task<News> GetInfo(string seoLink);

        Task<bool> CheckExistsBySeoLink(long id, string seoLink);

        Task<int> UpdateActiveStatus(long id, bool isActive);

        Task<int> UpdateIsHotStatus(long id, bool isHot);

        Task<int> UpdateIsHomePage(long id, bool isHomePage);

        Task<List<News>> GetTopHotNews(int top, long? id = null);

        Task<List<News>> GetTopNewNews(int top);

        Task<List<News>> GetAllNewsByCategoryId(int categoryId);

        Task<News> PreviousNews(long id);

        Task<News> NextNews(long id);

        Task<int> UpdateSeoLink(long id, string seolink);

        Task<int> UpdateViewCount(long id);

        Task<List<News>> SearchByCategoryId(long categoryId, int page, int pageSize, out long totalRows);

        Task<List<News>> GetTopNewsByCategoryIdForHomePage(int top, int categoryId);

        Task<int> GetMaxPriorityValue();

        Task<List<T>> GetListSpecialist<T>(Expression<Func<News, T>> projector);

        Task<List<T>> Search<T>(Expression<Func<News, T>> projector, string keyword, int pageId, int? categoryId, bool? isActive, bool? isHot, bool? isHomePage, int page, int pageSize, out long totalRows);

        Task<List<T>> SearchByTags<T>(Expression<Func<News, T>> projector, int pageId, string tag, int page, int pageSize, out long totalRows);

        Task<List<T>> SearchForSelect<T>(Expression<Func<News, T>> projector, string keyword, int? categoryId, int pageId, int page, int pageSize, out long totalRows);
    }
}
