﻿using GHM.Website.Domain.ModelMetas;
using GHM.Website.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Website.Domain.IRepository
{
    public interface IFaqRepository
    {
        Task<int> Insert(FaqMeta faqMeta);

        Task<int> Update(FaqMeta faqMeta);

        Task<int> Delete(int id);

        Task<Faq> GetInfo(int id);

        Task<bool> CheckExistsByQuestion(int id, string question);

        Task<int> UpdateIsActive(int id, bool isActive);

        Task<List<Faq>> GetAllActive();

        Task<List<Faq>> Search(bool? isActive, int page, int pageSize, out int totalRows);
    }
}
