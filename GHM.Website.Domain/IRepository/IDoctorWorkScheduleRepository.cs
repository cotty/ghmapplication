﻿using System;
using System.Threading.Tasks;

namespace GHM.Website.Domain.IRepository
{
    public interface IDoctorWorkScheduleRepository
    {
        Task<bool> CheckIsHasWorkSchedule(string doctorId, DateTime date, string shift);
    }
}
