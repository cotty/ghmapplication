﻿using GHM.Website.Domain.ModelMetas;
using GHM.Website.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GHM.Website.Domain.ViewModels;

namespace GHM.Website.Domain.IRepository
{
    public interface ICategoryRepository
    {
        Task<int> Insert(Category category);

        Task<int> Update(Category category);

        Task<int> UpdateCategoryIdPath(int id, string idPath);

        Task<int> UpdateChildrenIdPath(string oldParentIdPath, string newParentIdPath);

        Task<int> Delete(int id);

        Task<int> ForceDelete(int id);

        Task<Category> GetInfo(int id);

        Task<List<CategorySearhViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive, int page, int pageSize, out int totalRows);
    }
}
