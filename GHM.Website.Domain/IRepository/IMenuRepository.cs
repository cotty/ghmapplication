﻿using GHM.Website.Domain.ModelMetas;
using GHM.Website.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Website.Domain.IRepository
{
    public interface IMenuRepository
    {
        INewsRepository NewsRepository { get; set; }

        Task<int> Insert(MenuMeta menuMeta);

        Task<int> Update(MenuMeta menuMeta);

        Task<int> Delete(int id);

        Task<bool> CheckUseInMenu(long objectId, byte type);

        Task<List<Menu>> GetListActiveMenu();
    }
}
