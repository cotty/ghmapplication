﻿using GHM.Website.Domain.ModelMetas;
using GHM.Website.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Website.Domain.ViewModels;

namespace GHM.Website.Domain.IRepository
{
    public interface ISliderRepository
    {
        Task<int> Insert(SliderMeta slider, List<SliderItemMeta> listSliderItem);

        Task<int> Update(SliderMeta slider);

        Task<int> Delete(string id);

        Task<Slider> GetInfo(string id);

        Task<bool> CheckNameExists(string id, string name);

        Task<int> UpdateActiveStatus(string id, bool isActive);

        Task<int> InsertItems(List<SliderItemMeta> sliderItem);

        Task<string> InsertItem(SliderItemMeta sliderItem);

        Task<int> UpdateItem(SliderItemMeta sliderItem);

        Task<int> UpdateItemActiveStatus(string id, bool isActive);

        Task<int> DeleteItem(string id);

        Task<SliderItem> GetItemInfo(string id);

        Task<List<SliderSearchViewModel>> Search(string keyword, byte? type, bool? isActive, int page, int pageSize, out int totalRows);

        Task<List<SliderSearchViewModel>> GetSliderInfoByType(byte type);
    }
}
