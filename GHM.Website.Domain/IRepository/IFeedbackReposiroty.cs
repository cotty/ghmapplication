﻿using GHM.Website.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Website.Domain.IRepository
{
    public interface IFeedbackReposiroty
    {
        Task<int> Insert(Feedback feedback);

        Task<List<Feedback>> Search(string keyword, DateTime? fromDate, DateTime? toDate, int page, int pageSize, out long totalRows);
    }
}
