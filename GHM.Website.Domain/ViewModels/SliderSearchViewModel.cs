﻿using System;
using GHM.Website.Domain.Constants;

namespace GHM.Website.Domain.ViewModels
{
    public class SliderSearchViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
        public DateTime CreateTime { get; set; }
        public SliderType Type { get; set; }
        public string ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public bool ItemIsActive { get; set; }
        public string Url { get; set; }
        public string ImageUrl { get; set; }
    }
}
