﻿using System;

namespace GHM.Website.Domain.ViewModels
{
    public class CategorySearhViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreateTime { get; set; }
        public bool IsActive { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
    }
}
