﻿using GHM.Infrastructure.SqlServer;
using GHM.Website.Domain.IRepository;
using GHM.Website.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Website.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace GHM.Website.Infrastructure.Repository
{
    public class CategoryRepository : RepositoryBase, ICategoryRepository
    {
        private readonly IRepository<Category> _categoryRepository;

        public CategoryRepository(IDbContext context, IRepository<Category> categoryRepository) : base(context)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task<int> Insert(Category category)
        {
            _categoryRepository.Create(category);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(Category category)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateCategoryIdPath(int id, string idPath)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateChildrenIdPath(string oldParentIdPath, string newParentIdPath)
        {
            var childrens = await _categoryRepository.GetsAsync(false, x => x.IdPath.StartsWith($"{oldParentIdPath}."));
            if (!childrens.Any())
                return -1;

            foreach (var children in childrens)
            {
                var oldIdPath = children.IdPath;
                children.IdPath = $"{newParentIdPath}.{children.Id}";
                await UpdateChildrenIdPath(oldIdPath, $"{children.IdPath}");
            }
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(int id)
        {
            var info = await GetInfo(id);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(int id)
        {
            var info = await GetInfo(id);
            if (info == null)
                return -1;

            _categoryRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<Category> GetInfo(int id)
        {
            return await _categoryRepository.GetAsync(false, x => x.Id == id && !x.IsDelete);
        }

        public Task<List<CategorySearhViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive, int page, int pageSize,
            out int totalRows)
        {
            Expression<Func<Category, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId;
            Expression<Func<CategoryTranslation, bool>> specTranslation = x => x.LanguageId == languageId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            if (isActive.HasValue)
                spec = spec.And(x => x.IsActive == isActive);

            var query = Context.Set<Category>().Where(spec)
                .Join(Context.Set<CategoryTranslation>().Where(specTranslation), c => c.Id, ct => ct.CategoryId,
                    (c, ct) => new CategorySearhViewModel
                    {
                        Id = c.Id,
                        Name = ct.Name,
                        Description = ct.Description,
                        CreateTime = c.CreateTime,
                        IsActive = c.IsActive,
                        CreatorId = c.CreatorId,
                        CreatorFullName = c.CreatorFullName
                    });
            totalRows = query.Count();
            return query.OrderByDescending(x => x.CreateTime)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }
    }
}
