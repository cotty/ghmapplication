﻿using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using GHM.Website.Domain.IRepository;
using GHM.Website.Domain.ModelMetas;
using GHM.Website.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GHM.Website.Infrastructure.Repository
{
    public class FaqRepository : RepositoryBase, IFaqRepository
    {
        private readonly IRepository<Faq> _faqRepository;
        public FaqRepository(IDbContext context) : base(context)
        {
            _faqRepository = Context.GetRepository<Faq>();
        }

        public async Task<int> Insert(FaqMeta faqMeta)
        {
            var isQuestionExists = await CheckExistsByQuestion(faqMeta.Id, faqMeta.Question);

            if (isQuestionExists)
                return -1;

            var faq = new Faq
            {
//                Question = faqMeta.Question,
//                Answer = faqMeta.Answer,
                CreatorId = faqMeta.CreatorId,
                CreatorName = faqMeta.CreatorName,
                IsActive = faqMeta.IsActive,
            };

            _faqRepository.Create(faq);
            await Context.SaveChangesAsync();
            return 1;
        }

        public async Task<int> Update(FaqMeta faqMeta)
        {
            var info = await GetInfo(faqMeta.Id);

            if (info == null)
                return -1;

            var isQuestionExists = await CheckExistsByQuestion(faqMeta.Id, faqMeta.Question);

            if (isQuestionExists)
                return -2;

//            info.Question = faqMeta.Question;
//            info.Answer = faqMeta.Answer;
            info.IsActive = faqMeta.IsActive;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(int id)
        {
            var info = await GetInfo(id);

            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<Faq> GetInfo(int id)
        {
//            return await _faqRepository.GetAsync(false, x => x.Id == id && !x.IsDelete);
            return null;
        }

        public async Task<bool> CheckExistsByQuestion(int id, string question)
        {
//            return await _faqRepository.ExistAsync(x => x.Id != id && x.Question.Equals(question) && !x.IsDelete);
            return false;
        }

        public async Task<int> UpdateIsActive(int id, bool isActive)
        {
            var info = await GetInfo(id);

            if (info == null)
                return -1;

            info.IsActive = isActive;
            return await Context.SaveChangesAsync();
        }

        public async Task<List<Faq>> GetAllActive()
        {
            return await _faqRepository.GetsAsync(true, x => !x.IsDelete && x.IsActive);
        }

        public Task<List<Faq>> Search(bool? isActive, int page, int pageSize, out int totalRows)
        {
            Expression<Func<Faq, bool>> spec = x => !x.IsDelete;


            if (isActive.HasValue)
            {
                spec = spec.And(x => x.IsActive == isActive.Value);
            }

            var sort = Context.Filters.Sort<Faq, string>(a => a.Id, true);
            var paging = Context.Filters.Page<Faq>(page, pageSize);

            totalRows = _faqRepository.Count(spec);
            return _faqRepository.GetsAsync(false, spec, sort, paging);
        }
    }
}
