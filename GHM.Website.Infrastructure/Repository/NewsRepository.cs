﻿using GHM.Infrastructure.SqlServer;
using GHM.Website.Domain.IRepository;
using GHM.Website.Domain.ModelMetas;
using GHM.Website.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GHM.Website.Infrastructure.Repository
{
    public class NewsRepository : RepositoryBase, INewsRepository
    {
        private readonly IRepository<News> _newsRepository;
        private readonly IMenuRepository _menuRepository;

        public NewsRepository(IDbContext context, IMenuRepository menuRepository) : base(context)
        {
            _newsRepository = Context.GetRepository<News>();
            menuRepository.NewsRepository = this;
            _menuRepository = menuRepository;
        }

        public ICategoryRepository CategoryRepository { get; set; }
        public async Task<int> Insert(NewsMeta newsMeta)
        {
            throw new NotImplementedException();
        }

        public async Task<int> Update(NewsMeta newsMeta)
        {
            throw new NotImplementedException();
        }

        public async Task<int> Delete(long id)
        {
            throw new NotImplementedException();
        }

        public async Task<News> GetInfo(long id, bool? isActive = null)
        {
            throw new NotImplementedException();
        }

        public async Task<News> GetInfo(string seoLink)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> CheckExistsBySeoLink(long id, string seoLink)
        {
            throw new NotImplementedException();
        }

        public async Task<int> UpdateActiveStatus(long id, bool isActive)
        {
            throw new NotImplementedException();
        }

        public async Task<int> UpdateIsHotStatus(long id, bool isHot)
        {
            throw new NotImplementedException();
        }

        public async Task<int> UpdateIsHomePage(long id, bool isHomePage)
        {
            throw new NotImplementedException();
        }

        public async Task<List<News>> GetTopHotNews(int top, long? id = null)
        {
            throw new NotImplementedException();
        }

        public async Task<List<News>> GetTopNewNews(int top)
        {
            throw new NotImplementedException();
        }

        public async Task<List<News>> GetAllNewsByCategoryId(int categoryId)
        {
            throw new NotImplementedException();
        }

        public async Task<News> PreviousNews(long id)
        {
            throw new NotImplementedException();
        }

        public async Task<News> NextNews(long id)
        {
            throw new NotImplementedException();
        }

        public async Task<int> UpdateSeoLink(long id, string seolink)
        {
            throw new NotImplementedException();
        }

        public async Task<int> UpdateViewCount(long id)
        {
            throw new NotImplementedException();
        }

        public Task<List<News>> SearchByCategoryId(long categoryId, int page, int pageSize, out long totalRows)
        {
            totalRows = 1;
            throw new NotImplementedException();
        }

        public async Task<List<News>> GetTopNewsByCategoryIdForHomePage(int top, int categoryId)
        {
            throw new NotImplementedException();
        }

        public async Task<int> GetMaxPriorityValue()
        {
            throw new NotImplementedException();
        }

        public async Task<List<T>> GetListSpecialist<T>(Expression<Func<News, T>> projector)
        {
            throw new NotImplementedException();
        }

        public Task<List<T>> Search<T>(Expression<Func<News, T>> projector, string keyword, int pageId, int? categoryId, bool? isActive, bool? isHot,
            bool? isHomePage, int page, int pageSize, out long totalRows)
        {
            throw new NotImplementedException();
        }

        public Task<List<T>> SearchByTags<T>(Expression<Func<News, T>> projector, int pageId, string tag, int page, int pageSize, out long totalRows)
        {
            throw new NotImplementedException();
        }

        public Task<List<T>> SearchForSelect<T>(Expression<Func<News, T>> projector, string keyword, int? categoryId, int pageId, int page, int pageSize,
            out long totalRows)
        {
            throw new NotImplementedException();
        }
    }
}
