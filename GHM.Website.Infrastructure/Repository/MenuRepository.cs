﻿using GHM.Infrastructure.SqlServer;
using GHM.Website.Domain.IRepository;
using GHM.Website.Domain.ModelMetas;
using GHM.Website.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Website.Infrastructure.Repository
{
    public class MenuRepository : RepositoryBase, IMenuRepository
    {
        private readonly IRepository<Menu> _menuRepository;

        public INewsRepository NewsRepository { get; set; }

        public MenuRepository(IDbContext context) : base(context)
        {
            _menuRepository = Context.GetRepository<Menu>();
        }

        public async Task<int> Insert(MenuMeta menuMeta)
        {
            return 1;
        }

        public Task<int> Update(MenuMeta menuMeta)
        {
            throw new global::System.NotImplementedException();
        }

        public Task<int> Delete(int id)
        {
            throw new global::System.NotImplementedException();
        }

        public async Task<bool> CheckUseInMenu(long objectId, byte type)
        {
            //            return await _menuItemRepository.ExistAsync(x => x.ObjectId == objectId && x.ObjectType == type);
            return false;
        }

        public async Task<List<Menu>> GetListActiveMenu()
        {
            return await _menuRepository.GetsAsync(false);
        }
    }
}
