﻿using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using GHM.Website.Domain.IRepository;
using GHM.Website.Domain.ModelMetas;
using GHM.Website.Domain.Models;
using GHM.Website.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.Extensions;
using GHM.Website.Domain.Constants;

namespace GHM.Website.Infrastructure.Repository
{
    public class SliderRepository : RepositoryBase, ISliderRepository
    {
        private readonly IRepository<Slider> _sliderRepository;
        private readonly IRepository<SliderItem> _sliderItemRepository;

        public SliderRepository(IDbContext context) : base(context)
        {
            _sliderRepository = Context.GetRepository<Slider>();
            _sliderItemRepository = Context.GetRepository<SliderItem>();
        }

        public async Task<int> Insert(SliderMeta slider, List<SliderItemMeta> listSliderItem)
        {
            var isNameExists = await CheckNameExists(slider.Id, slider.Name);
            if (isNameExists)
                return -1;

            using (var transaction = Context.Database.BeginTransaction())
            {
                var sliderObject = new Slider
                {
                    Name = slider.Name,
                    UnsignName = slider.Name.StripVietnameseChars(),
                    Description = slider.Description,
                    CreatorId = slider.CreatorId,
                    CreatorFullName = slider.CreatorFullName,
                    IsActive = slider.IsActive,
                    Type = slider.Type
                };
                _sliderRepository.Create(sliderObject);
                var result = await Context.SaveChangesAsync();

                if (result > 0)
                {
                    listSliderItem.ForEach(x => x.SliderId = sliderObject.Id);
                    await InsertItems(listSliderItem);
                }

                transaction.Commit();
                return result;
            }
        }

        public async Task<int> Update(SliderMeta slider)
        {
            var info = await GetInfo(slider.Id);
            if (info == null)
                return -2;

            var isNameExists = await CheckNameExists(slider.Id, slider.Name);

            if (isNameExists)
                return -1;

            info.Name = slider.Name;
            info.UnsignName = slider.Name.StripVietnameseChars();
            info.Description = slider.Description;
            info.IsActive = slider.IsActive;
            info.Type = slider.Type;

            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string id)
        {
            var info = await GetInfo(id);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<Slider> GetInfo(string id)
        {
            return await _sliderRepository.GetAsync(false, x => x.Id == id && !x.IsDelete);
        }

        public async Task<bool> CheckNameExists(string id, string name)
        {
            return await _sliderRepository.ExistAsync(x => x.Id != id && x.Name.ToLower().Equals(name.ToLower()));
        }

        public async Task<int> UpdateActiveStatus(string id, bool isActive)
        {
            var info = await GetInfo(id);
            if (info == null)
                return -1;

            info.IsActive = isActive;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> InsertItems(List<SliderItemMeta> listSliderItem)
        {
            List<SliderItem> items = listSliderItem.Select(x => new SliderItem
            {
                Description = x.Description,
                ImageUrl = x.ImageUrl,
                IsActive = x.IsActive,
                Name = x.Name,
                SliderId = x.SliderId,
                Url = x.Url
            }).ToList();
            _sliderItemRepository.Creates(items);
            return await Context.SaveChangesAsync();
        }

        public async Task<string> InsertItem(SliderItemMeta sliderItem)
        {
            var item = new SliderItem
            {
                Description = sliderItem.Description,
                ImageUrl = sliderItem.ImageUrl,
                IsActive = sliderItem.IsActive,
                Name = sliderItem.Name,
                SliderId = sliderItem.SliderId,
                Url = sliderItem.Url
            };
            _sliderItemRepository.Create(item);
            await Context.SaveChangesAsync();
            return item.Id;
        }

        public async Task<int> UpdateItem(SliderItemMeta sliderItem)
        {
            var itemInfo = await GetItemInfo(sliderItem.Id);
            if (itemInfo == null)
                return -1;

            itemInfo.Description = sliderItem.Description;
            itemInfo.ImageUrl = sliderItem.ImageUrl;
            itemInfo.IsActive = sliderItem.IsActive;
            itemInfo.Name = sliderItem.Name;
            itemInfo.Url = sliderItem.Url;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateItemActiveStatus(string id, bool isActive)
        {
            var itemInfo = await GetItemInfo(id);
            if (itemInfo == null)
                return -1;

            itemInfo.IsActive = isActive;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeleteItem(string id)
        {
            var itemInfo = await GetItemInfo(id);
            if (itemInfo == null)
                return -1;

            itemInfo.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<SliderItem> GetItemInfo(string id)
        {
            return await _sliderItemRepository.GetAsync(false, x => x.Id == id);
        }

        public Task<List<SliderSearchViewModel>> Search(string keyword, SliderType? type, bool? isActive, int page, int pageSize, out int totalRows)
        {
            Expression<Func<Slider, bool>> spec = x => !x.IsDelete;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripVietnameseChars();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (type.HasValue)
            {
                spec = spec.And(x => x.Type == type);
            }

            if (isActive.HasValue)
            {
                spec = spec.And(x => x.IsActive == isActive);
            }

            var querySlider = Context.Set<Slider>().Where(spec);
            var querySliderItem = Context.Set<SliderItem>().Where(x => !x.IsDelete);

            totalRows = querySlider.Count();

            return querySlider
                .OrderByDescending(x => x.CreateTime)
                .Skip((page - 1) * pageSize).Take(pageSize)
                .Join(querySliderItem, s => s.Id, si => si.SliderId, (s, si) => new SliderSearchViewModel
                {
                    Id = s.Id,
                    Name = s.Name,
                    Description = s.Description,
                    IsActive = s.IsActive,
                    CreatorId = s.CreatorId,
                    CreatorFullName = s.CreatorFullName,
                    CreateTime = s.CreateTime,
                    Type = s.Type,
                    ItemId = si.Id,
                    ItemName = si.Name,
                    ItemDescription = si.Description,
                    ItemIsActive = si.IsActive,
                    Url = si.Url,
                    ImageUrl = si.ImageUrl
                }).ToListAsync();
        }

        public async Task<List<SliderSearchViewModel>> GetSliderInfoByType(SliderType type)
        {
            return await Context.Set<Slider>().Where(x => !x.IsDelete && x.IsActive && x.Type == type)
                .Join(Context.Set<SliderItem>().Where(x => !x.IsDelete), s => s.Id, si => si.SliderId, (s, si) => new SliderSearchViewModel
                {
                    Id = s.Id,
                    Name = s.Name,
                    Description = s.Description,
                    IsActive = s.IsActive,
                    CreatorId = s.CreatorId,
                    CreatorFullName = s.CreatorFullName,
                    CreateTime = s.CreateTime,
                    Type = s.Type,
                    ItemId = si.Id,
                    ItemName = si.Name,
                    ItemDescription = si.Description,
                    ItemIsActive = si.IsActive,
                    Url = si.Url,
                    ImageUrl = si.ImageUrl
                }).ToListAsync();
        }

        Task<List<SliderSearchViewModel>> ISliderRepository.Search(string keyword, byte? type, bool? isActive, int page, int pageSize, out int totalRows)
        {
            throw new NotImplementedException();
        }

        Task<List<SliderSearchViewModel>> ISliderRepository.GetSliderInfoByType(byte type)
        {
            throw new NotImplementedException();
        }
    }
}
