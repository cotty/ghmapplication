﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using GHM.Website.Domain.IRepository;
using GHM.Website.Domain.IServices;
using GHM.Website.Domain.ModelMetas;
using GHM.Website.Domain.Models;
using GHM.Website.Domain.Resources;
using GHM.Website.Domain.ViewModels;

namespace GHM.Website.Infrastructure.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly ICategoryTranslationRepository _categoryTranslationRepository;
        private readonly ICategoryTranslationService _categoryTranslationService;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmWebsiteResource> _resourceService;

        public CategoryService(ICategoryRepository categoryRepository, ICategoryTranslationRepository categoryTranslationRepository, IResourceService<SharedResource> sharedResourceService, IResourceService<GhmWebsiteResource> resourceService, ICategoryTranslationService categoryTranslationService)
        {
            _categoryRepository = categoryRepository;
            _categoryTranslationRepository = categoryTranslationRepository;
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
            _categoryTranslationService = categoryTranslationService;
        }

        public async Task<ActionResultResponse> Insert(CategoryMeta categoryMeta)
        {
            if (!categoryMeta.CategoryTranslations.Any())
                return new ActionResultResponse(-1, _sharedResourceService.GetString("Please select at least language."));

            var categoryId = Guid.NewGuid().ToString();
            var category = new Category
            {
                TenantId = categoryMeta.TenantId,
                ConcurrencyStamp = categoryId,
                CreatorId = categoryMeta.CreatorId,
                CreatorFullName = categoryMeta.CreatorFullName,
                IdPath = categoryId,
                IsActive = categoryMeta.IsActive,
            };
            if (categoryMeta.ParentId.HasValue)
            {
                var parentInfo = await _categoryRepository.GetInfo(categoryMeta.ParentId.Value);
                if (parentInfo == null)
                    return new ActionResultResponse(-2, _resourceService.GetString("Parent category does not exists."));

                category.ParentId = parentInfo.Id;
                category.IdPath = $"{parentInfo.IdPath}.{categoryId}";
            }

            var result = await _categoryRepository.Insert(category);
            if (result <= 0)
                return new ActionResultResponse(-3, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            #region Update category idPath.
            category.IdPath = category.IdPath.Replace("-1", category.Id.ToString());
            await _categoryRepository.UpdateCategoryIdPath(category.Id, category.IdPath);
            #endregion

            var categoryTranslations = new List<CategoryTranslation>();
            foreach (var categoryTranslation in categoryMeta.CategoryTranslations)
            {
                categoryTranslations.Add(new CategoryTranslation
                {
                    CategoryId = category.Id,
                    LanguageId = categoryTranslation.LanguageId,
                    Name = categoryTranslation.Name.Trim(),
                    Description = categoryTranslation.Description?.Trim(),
                    UnsignName = categoryTranslation.Name.Trim().StripVietnameseChars().ToUpper(),
                    IsDelete = false,
                    SeoLink = categoryTranslation.Name.Trim().ToUrlString().ToLower()
                });
            }

            var resultInsertCategoryTranslation = await _categoryTranslationRepository.Inserts(categoryTranslations);
            if (resultInsertCategoryTranslation <= 0)
            {
                await RollbackInsert(category.Id);
                return new ActionResultResponse(-4, _resourceService.GetString("Can not insert category. Please contact with administrator."));
            }

            return new ActionResultResponse(1, _resourceService.GetString("Add new category successful."));
        }

        public async Task<ActionResultResponse> Update(int id, CategoryMeta categoryMeta)
        {
            if (!categoryMeta.CategoryTranslations.Any())
                return new ActionResultResponse(-1, _sharedResourceService.GetString("Please select at least language."));

            var info = await _categoryRepository.GetInfo(id);
            if (info == null)
                return new ActionResultResponse(-2, _resourceService.GetString("Category does not exists."));

            var oldIdPath = info.IdPath;
            info.IsActive = categoryMeta.IsActive;
            if (info.ParentId != categoryMeta.ParentId)
            {
                if (!categoryMeta.ParentId.HasValue)
                {
                    info.ParentId = null;
                    info.IdPath = info.Id.ToString();
                }
                else
                {
                    var parentInfo = await _categoryRepository.GetInfo(categoryMeta.ParentId.Value);
                    if (parentInfo == null)
                        return new ActionResultResponse(-3, _resourceService.GetString("Parent category does not exists."));

                    info.ParentId = parentInfo.Id;
                    info.IdPath = $"{parentInfo.IdPath}.{info.Id}";
                }
            }

            var result = await _categoryRepository.Update(info);
            if (result <= 0)
                return new ActionResultResponse(-4, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            #region Update category translation.
            var categoryTranslations = new List<CategoryTranslation>();
            foreach (var categoryTranslation in categoryMeta.CategoryTranslations)
            {
                await _categoryTranslationService.Save(categoryTranslation);
            }
            #endregion

            #region Update child id path.

            if (info.IdPath != oldIdPath)
            {
                await _categoryRepository.UpdateChildrenIdPath(oldIdPath, info.IdPath);
            }
            #endregion
            var resultInsertCategoryTranslation = await _categoryTranslationRepository.Inserts(categoryTranslations);
            return resultInsertCategoryTranslation <= 0 ? new ActionResultResponse(-4, _resourceService.GetString("Can not update category. Please contact with administrator."))
                : new ActionResultResponse(1, _resourceService.GetString("Update categogry successful."));
        }

        public async Task<ActionResultResponse> Delete(int id)
        {
            var result = await _categoryRepository.Delete(id);
            if (result <= 0)
                return new ActionResultResponse(result, result == -1 ? _resourceService.GetString("Category doest not exists.")
                    : _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            return new ActionResultResponse(result, _resourceService.GetString("Delete category successful."));
        }

        public async Task<SearchResult<CategorySearhViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive, int page, int pageSize)
        {
            return new SearchResult<CategorySearhViewModel>
            {
                Items = await _categoryRepository.Search(tenantId, languageId, keyword, isActive, page, pageSize, out var totalRows),
                TotalRows = totalRows
            };
        }

        private async Task RollbackInsert(int categoryId)
        {
            await _categoryRepository.ForceDelete(categoryId);
            await _categoryTranslationRepository.ForceDeleteByCategoryId(categoryId);
        }
    }
}
