﻿using System.Threading.Tasks;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Website.Domain.IServices;
using GHM.Website.Domain.ModelMetas;
using GHM.Website.Domain.ViewModels;

namespace GHM.Website.Infrastructure.Services
{
    public class CourseService : ICourseService
    {
        public Task<ActionResultResponse> Insert(CourseMeta courseMeta)
        {
            throw new System.NotImplementedException();
        }

        public Task<ActionResultResponse> Update(string id, CourseMeta courseMeta)
        {
            throw new System.NotImplementedException();
        }

        public Task<ActionResultResponse> Delete(string id)
        {
            throw new System.NotImplementedException();
        }

        public Task<SearchResult<CourseSearchViewModel>> Search(string keyword, bool? isActive, int page, int pageSize)
        {
            throw new System.NotImplementedException();
        }
    }
}
