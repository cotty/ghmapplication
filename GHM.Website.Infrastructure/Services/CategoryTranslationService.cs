﻿using System.Threading.Tasks;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Website.Domain.IRepository;
using GHM.Website.Domain.IServices;
using GHM.Website.Domain.ModelMetas;
using GHM.Website.Domain.Models;
using GHM.Website.Domain.Resources;

namespace GHM.Website.Infrastructure.Services
{
    public class CategoryTranslationService : ICategoryTranslationService
    {
        private readonly ICategoryTranslationRepository _categoryTranslationRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmWebsiteResource> _resourceService;

        public CategoryTranslationService(ICategoryTranslationRepository categoryTranslationRepository, IResourceService<SharedResource> sharedResourceService, IResourceService<GhmWebsiteResource> resourceService)
        {
            _categoryTranslationRepository = categoryTranslationRepository;
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
        }

        public async Task<ActionResultResponse> Save(CategoryTranslationMeta categoryTranslationMeta)
        {
            var info = await _categoryTranslationRepository.GetInfo(categoryTranslationMeta.CategoryId,
                categoryTranslationMeta.LanguageId);
            if (info == null)
                return await Insert(categoryTranslationMeta);

            return await Update(categoryTranslationMeta);
        }

        public async Task<ActionResultResponse> Insert(CategoryTranslationMeta categoryTranslationMeta)
        {
            // Check name exists.
            var isExists = await _categoryTranslationRepository.CheckExists(categoryTranslationMeta.CategoryId,
                categoryTranslationMeta.LanguageId, categoryTranslationMeta.Name);
            if (isExists)
                return new ActionResultResponse(-1, _resourceService.GetString("Category: \"{0}\" already exists.", categoryTranslationMeta.Name));

            var result = await _categoryTranslationRepository.Insert(new CategoryTranslation(categoryTranslationMeta.CategoryId, categoryTranslationMeta.LanguageId,
                categoryTranslationMeta.Name, categoryTranslationMeta.Description, categoryTranslationMeta.SeoLink));
            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                : _resourceService.GetString("Add new category successful."));
        }

        public async Task<ActionResultResponse> Update(CategoryTranslationMeta categoryTranslationMeta)
        {
            // Check name exists.
            var isExists = await _categoryTranslationRepository.CheckExists(categoryTranslationMeta.CategoryId,
                categoryTranslationMeta.LanguageId, categoryTranslationMeta.Name);
            if (isExists)
                return new ActionResultResponse(-1, _resourceService.GetString("Category: \"{0}\" already exists.", categoryTranslationMeta.Name));

            var info = await _categoryTranslationRepository.GetInfo(categoryTranslationMeta.CategoryId,
                categoryTranslationMeta.LanguageId);
            if (info == null)
                return new ActionResultResponse(-2,
                    _resourceService.GetString("Category does not exists."));

            info.Name = categoryTranslationMeta.Name.Trim();
            info.UnsignName = info.Name.StripVietnameseChars().ToUpper();
            info.Description = categoryTranslationMeta.Description?.Trim();
            info.SeoLink = info.Name.ToUrlString();
            var resultUpdate = await _categoryTranslationRepository.Update(info);
            return new ActionResultResponse(resultUpdate, resultUpdate <= 0
                ? _sharedResourceService.GetString("Something went wrong. Please contact with administator.")
                : _resourceService.GetString("Update category successful."));
        }
    }
}
