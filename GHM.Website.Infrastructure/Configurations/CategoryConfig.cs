using GHM.Website.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Website.Infrastructure.Configurations
{
    public class CategoryConfig : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.Property(x => x.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Property(x => x.TenantId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.CreateTime).IsRequired();
            builder.Property(x => x.CreatorId).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.CreatorFullName).IsRequired().HasMaxLength(50);
            builder.Property(x => x.IsActive).IsRequired();
            builder.Property(x => x.IsDelete).IsRequired().HasDefaultValue(false);
            builder.Property(x => x.ParentId).IsRequired(false);
            builder.Property(x => x.IdPath).IsRequired().IsUnicode(false);
            builder.ToTable("Categories").HasKey(x => x.Id);
        }
    }

}
