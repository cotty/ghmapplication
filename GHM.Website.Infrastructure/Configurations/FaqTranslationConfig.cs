﻿using System;
using GHM.Website.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Website.Infrastructure.Configurations
{
    public class FaqTranslationConfig : IEntityTypeConfiguration<FaqTranslation>
    {
        public void Configure(EntityTypeBuilder<FaqTranslation> builder)
        {
            builder.Property(x => x.FaqId).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.LanguageId).IsRequired().IsUnicode(false).HasMaxLength(10);
            builder.Property(x => x.Question).IsRequired().HasMaxLength(4000);
            builder.Property(x => x.Answer).IsRequired().HasMaxLength(4000);
            builder.ToTable("FaqTranslations").HasKey(x => new { x.FaqId, x.LanguageId });
        }
    }
}
