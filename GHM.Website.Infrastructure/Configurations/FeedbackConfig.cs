using GHM.Website.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Website.Infrastructure.Configurations
{
    public class FeedbackConfig : IEntityTypeConfiguration<Feedback>
    {
        public void Configure(EntityTypeBuilder<Feedback> builder)
        {
            builder.Property(x => x.Id).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.TenantId).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.CustomerId).IsRequired(false).IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.FullName).IsRequired().HasMaxLength(50);
            builder.Property(x => x.PhoneNumber).IsRequired().IsUnicode(false).HasMaxLength(20);
            builder.Property(x => x.Email).IsRequired(false).HasMaxLength(500);
            builder.Property(x => x.UnsignName).IsRequired().IsUnicode(false).HasMaxLength(500);
            builder.Property(x => x.Content).IsRequired().HasMaxLength(4000);
            builder.Property(x => x.CreateTime).IsRequired();
            builder.ToTable("Feedbacks").HasKey(x => x.Id);
        }
    }

}
