﻿using System;
using System.Collections.Generic;
using System.Text;
using GHM.Website.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Website.Infrastructure.Configurations
{
    public class NewsTranslationConfig : IEntityTypeConfiguration<NewsTranslation>
    {
        public void Configure(EntityTypeBuilder<NewsTranslation> builder)
        {
            builder.Property(x => x.NewsId).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.LanguageId).IsRequired().IsUnicode(false).HasMaxLength(10);
            builder.Property(x => x.Title).IsRequired().HasMaxLength(256);
            builder.Property(x => x.Content).IsRequired();
            builder.Property(x => x.Description).IsRequired().HasMaxLength(500);
            builder.Property(x => x.SeoLink).IsRequired().HasMaxLength(500).IsUnicode(false);
            builder.Property(x => x.UnsignName).IsRequired().HasMaxLength(256);
            builder.ToTable("NewsTranslations").HasKey(x => new { x.NewsId, x.LanguageId });
        }
    }
}
