using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using GHM.Website.Domain.Models;

namespace GHM.Website.Infrastructure.Configurations
{
    public class SliderConfig : IEntityTypeConfiguration<Slider>
    {
        public void Configure(EntityTypeBuilder<Slider> builder)
        {
            builder.HasAlternateKey(x => x.Id);

            builder.Property(x => x.Id).IsRequired().UseSqlServerIdentityColumn();
            builder.Property(x => x.Name).IsRequired().HasMaxLength(250);
            builder.Property(x => x.UnsignName).IsUnicode(false).IsRequired().HasMaxLength(250);
            builder.Property(x => x.Description).IsRequired(false).HasMaxLength(500);
            builder.Property(x => x.CreateTime).IsRequired();
            builder.Property(x => x.CreatorId).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.CreatorFullName).IsRequired().HasMaxLength(50);
            builder.Property(x => x.IsActive).IsRequired();
            builder.Property(x => x.IsDelete).IsRequired();
            builder.Property(x => x.Type).IsRequired();
            builder.ToTable("Sliders");
        }
    }
}
