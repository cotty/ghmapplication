using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using GHM.Website.Domain.Models;

namespace GHM.Website.Infrastructure.Configurations
{
    public class NewsConfig : IEntityTypeConfiguration<News>
    {
        public void Configure(EntityTypeBuilder<News> builder)
        {
            builder.HasAlternateKey(x => x.Id);
            builder.Property(x => x.Id).HasColumnName("Id").IsRequired().HasColumnType("bigint").UseSqlServerIdentityColumn();           
            builder.Property(x => x.CreateTime).HasColumnName("CreateTime").IsRequired().HasColumnType("datetime");
            builder.Property(x => x.CreatorId).HasColumnName("CreatorId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.CreatorFullName).HasColumnName("CreatorFullName").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
            builder.Property(x => x.CreatorImage).HasColumnName("CreatorImage").IsRequired(false).IsUnicode(false).HasColumnType("varchar").HasMaxLength(500);
            builder.Property(x => x.ViewCount).HasColumnName("ViewCount").IsRequired().HasColumnType("int");
            builder.Property(x => x.CommentCount).HasColumnName("CommentCount").IsRequired().HasColumnType("int");
            builder.Property(x => x.LikeCount).HasColumnName("LikeCount").IsRequired().HasColumnType("int");
            builder.Property(x => x.Source).HasColumnName("Source").IsRequired(false).IsUnicode(false).HasColumnType("varchar").HasMaxLength(250);
            builder.Property(x => x.IsActive).HasColumnName("IsActive").IsRequired().HasColumnType("bit");
            builder.Property(x => x.IsDelete).HasColumnName("IsDelete").IsRequired().HasColumnType("bit");
            builder.Property(x => x.Attachments).HasColumnName("Attachments").IsRequired(false).HasColumnType("nvarchar");
            builder.Property(x => x.Image).HasColumnName("Image").IsRequired(false).IsUnicode(false).HasColumnType("varchar").HasMaxLength(500);
            builder.Property(x => x.IsHot).HasColumnName("IsHot").IsRequired().HasColumnType("bit");
            builder.Property(x => x.IsHomePage).HasColumnName("IsHomePage").IsRequired().HasColumnType("bit");
            builder.Property(x => x.Priority).HasColumnName("Priority").IsRequired(false).HasColumnType("int");
            builder.ToTable("News");
        }
    }
}
