using GHM.Website.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Website.Infrastructure.Configurations
{
    public class MenuTranslationConfig : IEntityTypeConfiguration<MenuTranslation>
    {
        public void Configure(EntityTypeBuilder<MenuTranslation> builder)
        {
            builder.Property(x => x.MenuId).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.LanguageId).IsRequired().IsUnicode(false).HasMaxLength(10);
            builder.Property(x => x.Name).IsRequired().HasMaxLength(256);
            builder.Property(x => x.UnsignName).IsRequired().HasMaxLength(256);
            builder.Property(x => x.Url).IsRequired().HasMaxLength(500);
            builder.ToTable("MenuTranslations").HasKey(x => new { x.MenuId, x.LanguageId });
        }
    }

}
