using GHM.Website.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Website.Infrastructure.Configurations
{
    public class MenuConfig : IEntityTypeConfiguration<Menu>
    {
        public void Configure(EntityTypeBuilder<Menu> builder)
        {
            builder.Property(x => x.Id).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.TenantId).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.CreateTime).IsRequired();
            builder.Property(x => x.CreatorId).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.CreatorFullName).IsRequired().HasMaxLength(50);
            builder.ToTable("Menus").HasKey(x => x.Id);
        }
    }

}
