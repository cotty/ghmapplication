using GHM.Website.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Website.Infrastructure.Configurations
{
    public class CategoryTranslationConfig : IEntityTypeConfiguration<CategoryTranslation>
    {
        public void Configure(EntityTypeBuilder<CategoryTranslation> builder)
        {
            builder.Property(x => x.CategoryId).IsRequired();
            builder.Property(x => x.LanguageId).IsRequired().HasMaxLength(10).IsUnicode(false);
            builder.Property(x => x.Name).IsRequired().HasMaxLength(256);
            builder.Property(x => x.UnsignName).IsRequired().IsUnicode(false).HasMaxLength(256);
            builder.Property(x => x.Description).IsRequired(false).HasMaxLength(500);
            builder.Property(x => x.SeoLink).IsRequired().IsUnicode(false).HasMaxLength(500);
            builder.ToTable("CategorieTranslations").HasKey(x => new { x.CategoryId, x.LanguageId });
        }
    }

}
