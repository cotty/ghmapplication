using GHM.Website.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Website.Infrastructure.Configurations
{
    public class FaqConfig : IEntityTypeConfiguration<Faq>
    {
        public void Configure(EntityTypeBuilder<Faq> builder)
        {
            builder.Property(x => x.Id).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.TenantId).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.CreatorId).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.CreatorName).IsRequired().HasMaxLength(50);
            builder.Property(x => x.CreateTime).IsRequired();
            builder.Property(x => x.IsDelete).IsRequired().HasDefaultValue(false);
            builder.Property(x => x.IsActive).IsRequired();
            builder.ToTable("Faqs").HasKey(x => x.Id);
        }
    }

}
