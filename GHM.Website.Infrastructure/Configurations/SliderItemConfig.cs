using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using GHM.Website.Domain.Models;

namespace GHM.Website.Infrastructure.Configurations
{
    public class SliderItemConfig: IEntityTypeConfiguration<SliderItem>
    {
        public void Configure(EntityTypeBuilder<SliderItem> builder)
        {
            builder.Property(x => x.Id).HasColumnName("Id").IsRequired().HasColumnType("bigint").UseSqlServerIdentityColumn();
            builder.Property(x => x.Name).HasColumnName("Name").IsRequired().HasColumnType("nvarchar").HasMaxLength(500);
            builder.Property(x => x.Url).HasColumnName("Url").IsRequired(false).IsUnicode(false).HasColumnType("varchar").HasMaxLength(500);
            builder.Property(x => x.ImageUrl).HasColumnName("ImageUrl").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(500);
            builder.Property(x => x.Description).HasColumnName("Description").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(500);
            builder.Property(x => x.IsActive).HasColumnName("IsActive").IsRequired().HasColumnType("bit");
            builder.Property(x => x.SliderId).HasColumnName("SliderId").IsRequired().HasColumnType("int");
            builder.Property(x => x.IsDelete).HasColumnName("IsDelete").IsRequired().HasColumnType("bit");

            builder.ToTable("SliderItem");
        }
    }

}
