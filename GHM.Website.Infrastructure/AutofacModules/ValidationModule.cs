﻿using Autofac;
using FluentValidation;
using GHM.Website.Domain.ModelMetas;
using GHM.Website.Infrastructure.Validations;

namespace GHM.Website.Infrastructure.AutofacModules
{
    public class ValidationModule : Autofac.Module
    {
        public ValidationModule()
        {

        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CategoryMetaValidator>()
                .As<IValidator<CategoryMeta>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<FaqMetaValidator>()
                .As<IValidator<FaqMeta>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<MenuItemMetaValidator>()
                .As<IValidator<MenuItemMeta>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<MenuMetaValidator>()
                .As<IValidator<MenuMeta>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<NewsMetaValidator>()
                .As<IValidator<NewsMeta>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<SlideItemMetaValidator>()
                .As<IValidator<SliderItemMeta>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<SliderMetaValidator>()
                .As<IValidator<SliderMeta>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<DatHenMetaValidator>()
                .As<IValidator<DatHenMeta>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<QuaTangKhachHangMetaValidator>()
                .As<IValidator<QuaTangKhachHangMeta>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<SlideItemMetaValidator>()
                .As<IValidator<SliderItemMeta>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<SliderMetaValidator>()
                .As<IValidator<SliderMeta>>()
                .InstancePerLifetimeScope();
        }
    }
}
