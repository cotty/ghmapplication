﻿using Autofac;
using GHM.Infrastructure.SqlServer;
using GHM.Website.Domain.IRepository;
using GHM.Website.Infrastructure.Repository;

namespace GHM.Website.Infrastructure.AutofacModules
{
    public class ApplicationModule : Module
    {
        public string ConnectionString { get; }
        public ApplicationModule(string connectionString)
        {
            ConnectionString = connectionString;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<WebsiteDbContext>()
                .As<IDbContext>()
                .InstancePerLifetimeScope();

            builder.RegisterType<CategoryRepository>()
                .As<ICategoryRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<FaqRepository>()
                .As<IFaqRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<FeedbackRepository>()
                .As<IFeedbackReposiroty>()
                .InstancePerLifetimeScope();

            builder.RegisterType<MenuRepository>()
                .As<IMenuRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<NewsRepository>()
                .As<INewsRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<SliderRepository>()
                .As<ISliderRepository>()
                .InstancePerLifetimeScope();
        }
    }
}
