﻿using System;
using System.Threading;
using System.Threading.Tasks;
using GHM.Infrastructure.SqlServer;
using GHM.Website.Domain.Models;
using GHM.Website.Infrastructure.Configurations;
using Microsoft.EntityFrameworkCore;

namespace GHM.Website.Infrastructure
{
    public class WebsiteDbContext : DbContextBase
    {
        //private readonly Lazy<EntityQueryFilterProvider> _filterProviderInitializer = new Lazy<EntityQueryFilterProvider>();

        public WebsiteDbContext(DbContextOptions<WebsiteDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Configurations:
            builder.ApplyConfiguration(new CategoryConfig());
            builder.ApplyConfiguration(new CategoryTranslationConfig());
            builder.ApplyConfiguration(new FaqConfig());
            builder.ApplyConfiguration(new FaqTranslationConfig());
            builder.ApplyConfiguration(new FeedbackConfig());
            builder.ApplyConfiguration(new MenuConfig());
            builder.ApplyConfiguration(new MenuTranslationConfig());
            builder.ApplyConfiguration(new NewsConfig());
            builder.ApplyConfiguration(new NewsTranslationConfig());
            builder.ApplyConfiguration(new SliderConfig());
            builder.ApplyConfiguration(new SliderItemConfig());
        }

        //public Task<int> SaveChangesAsync()
        //{
        //    return SaveChangesAsync(new CancellationToken());
        //}

        //public IRepository<T> GetRepository<T>() where T : class
        //{
        //    return new EfRepository<T>(this);
        //}

        //public QueryFilterProvider Filters => _filterProviderInitializer.Value;
    }
}
