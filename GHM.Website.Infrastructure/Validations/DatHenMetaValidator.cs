﻿using FluentValidation;
using GHM.Website.Domain.ModelMetas;

namespace GHM.Website.Infrastructure.Validations
{
    public class DatHenMetaValidator : AbstractValidator<DatHenMeta>
    {
        public DatHenMetaValidator()
        {
            RuleFor(x => x.FullName).NotEmpty().WithMessage("Tên bệnh nhân không được để trống.");
            RuleFor(x => x.Birthday).NotEmpty().WithMessage("Ngày tháng năm sinh không được để trống");
            RuleFor(x => x.PhoneNumber).NotEmpty().WithMessage("Số điện thoại không được để trống");
            RuleFor(x => x.Address).NotEmpty().WithMessage("Địa chỉ không được để trống");
            RuleFor(x => x.StartTime).NotEmpty().WithMessage("Ngày đặt hẹn không được để trống");
            RuleFor(x => x.Hours).NotEmpty().WithMessage("Giờ đặt hẹn không được để trống");
            RuleFor(x => x.ServiceId).NotEmpty().WithMessage("Dịch vụ không được để trống");
        }
    }
}
