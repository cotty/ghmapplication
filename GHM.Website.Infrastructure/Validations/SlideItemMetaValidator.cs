﻿using FluentValidation;
using GHM.Website.Domain.ModelMetas;

namespace GHM.Website.Infrastructure.Validations
{
    public class SlideItemMetaValidator : AbstractValidator<SliderItemMeta>
    {
        public SlideItemMetaValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Tên slider item không được để trống");
            RuleFor(x => x.Name).MaximumLength(500).WithMessage("Tên slider item không quá 500 ký tự");
            RuleFor(x => x.Url).MaximumLength(500).WithMessage("Đường dẫn khống quá 500 ký tự");
            RuleFor(x => x.ImageUrl).NotEmpty().WithMessage("Đường dẫn ảnh không được để trống");
            RuleFor(x => x.ImageUrl).MaximumLength(500).WithMessage("Đường dẫn ảnh không quá 500 ký tự");
            RuleFor(x => x.Description).MaximumLength(500).WithMessage("Mô tả không được để trống");
            RuleFor(x => x.IsActive).NotEmpty().WithMessage("Kích hoạt không được để trống");
        }
    }
}
