﻿using FluentValidation;
using GHM.Website.Domain.ModelMetas;

namespace GHM.Website.Infrastructure.Validations
{
    public class SliderMetaValidator : AbstractValidator<SliderMeta>
    {
        public SliderMetaValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Tên slide không được để trống");
            RuleFor(x => x.Name).MaximumLength(250).WithMessage("Tên slide không quá 250 ký tự");
            RuleFor(x => x.Description).MaximumLength(500).WithMessage("Mô tả không quá 500 ký tự");
            RuleFor(x => x.Type).NotEmpty().WithMessage("Loại slide không được để trống");
        }
    }
}
