﻿using FluentValidation;
using GHM.Website.Domain.ModelMetas;

namespace GHM.Website.Infrastructure.Validations
{
    public class QuaTangKhachHangMetaValidator : AbstractValidator<QuaTangKhachHangMeta>
    {
        public QuaTangKhachHangMetaValidator()
        {
            RuleFor(x => x.MaQuaTang).NotEmpty().WithMessage("Mã quà tặng không được để trống.");
        }
    }
}
