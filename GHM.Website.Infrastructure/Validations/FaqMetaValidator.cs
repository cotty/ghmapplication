﻿using FluentValidation;
using GHM.Website.Domain.ModelMetas;

namespace GHM.Website.Infrastructure.Validations
{
    public class FaqMetaValidator : AbstractValidator<FaqMeta>
    {
        public FaqMetaValidator()
        {
            RuleFor(x => x.Question).NotEmpty().WithMessage("Câu hỏi không được để trống.");
            RuleFor(x => x.Answer).NotEmpty().WithMessage("Câu trả lời không được để trống");
        }
    }
}
