﻿using FluentValidation;
using GHM.Website.Domain.ModelMetas;

namespace GHM.Website.Infrastructure.Validations
{
    public class NewsMetaValidator : AbstractValidator<NewsMeta>
    {
        public NewsMetaValidator()
        {
            RuleFor(x => x.Title).NotEmpty().WithMessage("Tiêu đề không được để trống");
            RuleFor(x => x.Title).NotEmpty().MaximumLength(500).WithMessage("Tiêu đề không được quá 500 ký tự");
            RuleFor(x => x.Description).NotEmpty().WithMessage("Mô tả không được để trống");
            RuleFor(x => x.Description).NotEmpty().MaximumLength(500).WithMessage("Mô tả không được quá 1000 ký tự");
            RuleFor(x => x.Content).NotEmpty().WithMessage("Nội dung không được để trống");
            RuleFor(x => x.CategoryId).NotEmpty().WithMessage("Chuyên mục không được để trống");
        }
    }
}
