﻿using FluentValidation;
using GHM.Website.Domain.ModelMetas;

namespace GHM.Website.Infrastructure.Validations
{
    public class MenuItemMetaValidator : AbstractValidator<MenuItemMeta>
    {
        public MenuItemMetaValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Tên menu không được để trống.");
            RuleFor(x => x.Name).MaximumLength(500).WithMessage("Tên menu không được quá 500 ký tự");
        }
    }
}
