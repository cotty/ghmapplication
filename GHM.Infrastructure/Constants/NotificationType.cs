﻿namespace GHM.Infrastructure.Constants
{
    public enum NotificationType
    {
        Warning,
        Info,
        Danger,
        User
    }
}
