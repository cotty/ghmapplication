﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Infrastructure.Constants
{
    public enum ScheduleType
    {
        Once,
        DailyDay,
        DailyMonth,
        DailyYear,
        None
    }
}
