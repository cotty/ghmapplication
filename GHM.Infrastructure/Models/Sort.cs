﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Infrastructure.Models
{
    public class Sort
    {
        public string Key { get; set; }
        public bool Ascending { get; set; }
    }
}
