﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Infrastructure.Models
{
     public class LoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool RemberLogin { get; set; }
        public string ReturnUrl { get; set; }
    }
}
