﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Infrastructure.Models
{
    public class Pagination
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}
