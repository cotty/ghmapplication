﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Infrastructure.Models
{
    public class Filtering
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
