﻿namespace GHM.Infrastructure.Models
{
    public class ApiUrlSettings
    {
        public string CoreApiUrl { get; set; }
        public string HrApiUrl { get; set; }
        public string TaskApiUrl { get; set; }
        public string TimekeepingApiUrl { get; set; }
        public string Authority { get; set; }
        public string CheckPermissionUrl { get; set; }
        public string PatientApiUrl { get; set; }
        public string NotificationApiUrl { get; set; }
        public string CustomerApiUrl { get; set; }
        public string FileApiUrl { get; set; }
    }
}
