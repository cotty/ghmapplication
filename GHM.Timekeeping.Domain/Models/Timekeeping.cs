﻿using GHM.Infrastructure.MongoDb;

namespace GHM.Timekeeping.Domain.Models
{
    public class Timekeeping : Entity<string>
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public int TotalWorkingDays { get; set; }
        public int TotalSunday { get; set; }
        public int TotalHoliday { get; set; }
        public int TotalOvertimes { get; set; }
        public byte Month { get; set; }
        public byte Quartar { get; set; }
        public int Year { get; set; }
        public ShiftReport[] Shift { get; set; }
        public DayOffGroupReport[] DayOffGroup { get; set; }
    }

    public class ShiftReport
    {
        public string ShiftId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public decimal Value { get; set; }
    }

    public class DayOffGroupReport
    {
        public string DayOffGroupId { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
    }
}

