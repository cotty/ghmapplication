﻿using System.Collections.Generic;
using GHM.Infrastructure.MongoDb;

namespace GHM.Timekeeping.Domain.Models
{
    public class ReportByMonth : Entity<string>
    {
        public int EnrollNumber { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string UnsignName { get; set; }
        public int OfficeId { get; set; }
        public string OfficeIdPath { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public List<ReportShiftAggregate> ReportShiftAggregates { get; set; }
        public decimal TotalSundays { get; set; }
        public decimal TotalNormalDays { get; set; }
        public decimal TotalHolidays { get; set; }
        public int TotalOvertime { get; set; }
        public decimal TotalAnnualLeave { get; set; }
        public decimal TotalUnpaidLeave { get; set; }
        public decimal TotalCompensatory { get; set; }
        public decimal TotalInsuranceLeave { get; set; }
        public decimal TotalEntitlement { get; set; }
        public decimal TotalHolidaysLeave { get; set; }
        public decimal TotalInvalidWorkingDays { get; set; }
        public decimal TotalDaysValidMeal { get; set; }
        public decimal TotalInLateMin { get; set; }
        public decimal TotalOutLateMin { get; set; }
        public decimal TotalInSoonMin { get; set; }
        public decimal TotalOutSoonMin { get; set; }
        public decimal DaysInMonth { get; set; }
        public decimal TotalDayOff { get; set; }
        public decimal TotalInvalidWorkSchedule { get; set; }
        public decimal TotalInLatencyMin { get; set; }
        public decimal TotalOutLatencyMin { get; set; }

        /// <summary>
        /// Trường hợp đã được xác nhận. Sẽ khóa không cho phép sửa bất cứ thông tin gì
        /// </summary>
        public bool IsApprove { get; set; }

        public string ApproveUserId { get; set; }
        public string ApproveFullName { get; set; }
        public string ApproveTime { get; set; }

        public ReportByMonth()
        {
            IsApprove = false;
        }
    }
}

