﻿using System;
using System.Collections.Generic;
using GHM.Infrastructure.MongoDb;
using GHM.Infrastructure.MongoDb.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GHM.Timekeeping.Domain.Models
{
    public class InLateOutEarly : Entity<string>
    {
        public int EnrollNumber { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string OfficeIdPath { get; set; }
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public string Avatar { get; set; }
        public string DeclineReason { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RegisterDate { get; set; }

        //public string ShiftId { get; set; }
        //public string ShiftReportName { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string ManagerUserId { get; set; }
        public string ManagerFullName { get; set; }
        public bool IsDelete { get; set; }
        public List<InLateOutEarlyShift> Shifts { get; set; }
        public InLateOutEarly()
        {
            CreateTime = DateTime.Now;
            IsDelete = false;
        }

        /// <summary>
        /// Trạng thái xác nhận đơn.
        /// </summary>
        public bool IsConfirmed { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime ConfirmDateTime { get; set; }
    }

    public class InLateOutEarlyShift
    {
        /// <summary>
        /// True: Đi muộn 
        /// False: Về sớm
        /// </summary>
        public bool IsInLate { get; set; }
        public string ShiftId { get; set; }
        public string ShiftCode { get; set; }
        public string ShiftReportName { get; set; }
        public TimeObject StartTime { get; set; }
        public TimeObject EndTime { get; set; }
        public int TotalMin { get; set; }                

        /// <summary>
        /// Trạng thái duyệt hoặc không duyệt
        /// </summary>
        public bool? IsApprove { get; set; }

        public string Reason { get; set; }
        public string DeclineReason { get; set; }
    }
}
