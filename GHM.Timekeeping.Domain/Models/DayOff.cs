﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Domain.Constant;
using MongoDB.Bson.Serialization.Attributes;

namespace GHM.Timekeeping.Domain.Models
{    
    public class DayOff : Entity<string>
    {
        public int EnrollNumber { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public int OfficeId { get; set; }
        public string OfficeIdPath { get; set; }
        public string OfficeName { get; set; }
        public int? TitleId { get; set; }
        public string TitleName { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
        public string UnsignName { get; set; }
        public DayOffStatus Status { get; set; }

        [DisplayName(@"Nghỉ từ ngày")]
        [Required(ErrorMessage = "Vui lòng chọn nghỉ từ ngày.")]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime FromDate { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? ToDate { get; set; }

        public decimal TotalDays { get; set; }
        public decimal? TotalApprovedDays { get; set; }
        public List<DayOffDate> Dates { get; set; }
        public string Reason { get; set; }

//        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
//        public DateTime RegisterDate { get; set; }

        public string ManagerUserId { get; set; }
        public string ManagerFullName { get; set; }
        public string ApproverUserId { get; set; }
        public string ApproverFullName { get; set; }
        public string ManagerNote { get; set; }
        public string ApproverNote { get; set; }
        public string ManagerDeclineReason { get; set; }
        public string ApproverDeclineReason { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? ManagerApproveTime { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? ApproverApproveTime { get; set; }

        [BsonConstructor]
        public DayOff()
        {
            CreatorId = UserId;
            CreatorFullName = FullName;
        }
    }

    public class DayOffDate
    {
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime Date { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }

        /// <summary>
        /// Giá trị ngày để kiểm tra xem ngày hôm đấy có phải là ngày nghỉ tuần hay không.
        /// 1: Chủ nhật
        /// 2: Thứ 2
        /// 4: Thứ 3
        /// 8: Thứ 4
        /// 16: Thứ 5
        /// 32: Thứ 6
        /// 64: Thứ 7
        /// </summary>
        public WorkingDayValue Value { get; set; }

        /// <summary>
        /// Hình thức nghỉ phép:
        /// 0: Nghỉ phép
        /// 1: Nghỉ không lương
        /// 2: Nghỉ bù
        /// 3: Nghỉ bảo hiểm
        /// 4: Nghỉ chế độ
        /// 5: Nghỉ tuần
        /// 6: Nghỉ lễ
        /// </summary>
        public DayOffMethod? Method { get; set; }
        public bool? IsManagerApprove { get; set; }
        public bool? IsApproverApprove { get; set; }
        public string ManagerNote { get; set; }
        public string ApproverNote { get; set; }
        public string ManagerDeclineReason { get; set; }
        public string ApproverDeclineReason { get; set; }
        public string ShiftId { get; set; }
        public string ShiftCode { get; set; }
        public string ShiftReportName { get; set; }
        public int ShiftWorkingDaysValue { get; set; }
        public decimal ShiftWorkUnit { get; set; }
    }
}
