﻿using System;
using GHM.Infrastructure.MongoDb;
using GHM.Infrastructure.MongoDb.Models;
using GHM.Timekeeping.Domain.Constant;
using MongoDB.Bson.Serialization.Attributes;

namespace GHM.Timekeeping.Domain.Models
{
    public class ReportByShift : Entity<string>
    {
        public int EnrollNumber { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public int OfficeId { get; set; }
        public string OfficeIdPath { get; set; }
        public string UnsignName { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }
        public int Quarter { get; set; }
        public int Year { get; set; }
        public string ShiftId { get; set; }
        public string ShiftCode { get; set; }
        public string ShiftReportName { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? In { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? Out { get; set; }
        public TimeObject InTime { get; set; }
        public TimeObject OutTime { get; set; }
        public int TotalWorkingMin { get; set; }
        public int TotalOvertimeMin { get; set; }
        public int InLateMin { get; set; }
        public int InSoonMin { get; set; }
        public int OutLateMin { get; set; }
        public int OutSoonMin { get; set; }
        public int? InLatencyMin { get; set; }
        public int? OutLatencyMin { get; set; }
        public string InLatencyReason { get; set; }
        public string OutLatencyReason { get; set; }

        public decimal WorkUnit { get; set; }
        public bool IsSunday { get; set; }
        public bool IsHoliday { get; set; }
        public bool IsValid { get; set; }
        /// <summary>
        /// Có lịch làm việc hợp lệ không.
        /// </summary>
        public bool IsValidWorkSchedule { get; set; }
        public bool IsOvertime { get; set; }

        /// <summary>
        /// Trạng thái làm việc.
        /// null: đi làm bình thường.       
        /// 0: Nghỉ phép
        /// 1: Nghỉ không lương
        /// 2: Nghỉ bù
        /// 3: Nghỉ bảo hiểm
        /// 4: Nghỉ chế độ
        /// 5: Nghỉ Tuần
        /// 6: Nghỉ lễ
        /// 7: Nghỉ không phép
        /// </summary>
        public DayOffMethod? Method { get; set; }        

        /// <summary>
        /// Loại hình thêm
        /// 0: Automatic from service and time machine
        /// 1: Manual insert. So when reaggregate will not update this record.
        /// Ex: When somebody forgot checkin or checkout, user that has permission can insert manually.
        /// </summary>
        public ReportByShiftType? Type { get; set; }

        public ReportByShift()
        {
            IsHoliday = false;
            IsSunday = false;
        }
    }
}
