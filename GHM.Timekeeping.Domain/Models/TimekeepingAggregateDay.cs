﻿using System;
using GHM.Infrastructure.MongoDb;
using MongoDB.Bson.Serialization.Attributes;

namespace GHM.Timekeeping.Domain.Models
{
    public class TimekeepingAggregateDay : Entity<string>
    {
        public int EnrollNumber { get; set; }
        public string UserId { get; set; }
        public int OfficeId { get; set; }
        public int TitleId { get; set; }
        public string OfficeIdPath { get; set; }
        public byte Day { get; set; }
        public byte Month { get; set; }
        public byte Quartar { get; set; }
        public int Year { get; set; }
        public bool? IsSunday { get; set; }
        public bool? IsHoliday { get; set; }
        public int TotalMinutes { get; set; }
        public int TotalLateMinutes { get; set; }
        public int TotalSoonMinutes { get; set; }
        public byte Status { get; set; }        
        public bool IsValid { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime Time { get; set; }

        /// <summary>
        /// Đơn vị tính dựa vào ngày chấm công. Nếu ngày đi làm 1 ca (0.5 công) thì sẽ là 0.5 2 ca sẽ là 1 công
        /// </summary>
        public decimal WorkUnit { get; set; }

        /// <summary>
        /// Thời gian tính công làm thêm giờ. Nếu ca làm là
        /// </summary>
        public int OvertimeMinutes { get; set; }
    }
}
