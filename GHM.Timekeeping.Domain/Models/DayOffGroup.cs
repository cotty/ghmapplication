﻿using GHM.Infrastructure.MongoDb;

namespace GHM.Timekeeping.Domain.Models
{
    public class TimekeepingDayOffGroup : Entity<string>
    {
        public string Name { get; set; }
        /// <summary>
        /// Nghỉ phép. Sẽ dựa vào số ngày phép còn lại của nhân viên để đưa ra cảnh báo khi nhân viên chọn hình thức nghỉ
        /// là nhóm này
        /// </summary>
        public bool IsAnnualLeave { get; set; }

        /// <summary>
        /// Nghỉ bù. Sẽ hiển thị ngày nghỉ và ngày làm bù cho nhân viên chọn khi nhân viên chọn nhóm này.
        /// </summary>
        public bool IsChangeSchedule { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    }
}
