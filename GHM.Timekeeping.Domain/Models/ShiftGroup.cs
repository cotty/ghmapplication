﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using GHM.Infrastructure.MongoDb;

namespace GHM.Timekeeping.Domain.Models
{
    public class ShiftGroup : Entity<string>
    {
        [DisplayName(@"Tên ca làm việc")]
        [Required(ErrorMessage = "Vui lòng nhập tên ca làm việc.")]
        [MaxLength(250, ErrorMessage = "Tên ca làm việc không được phép vượt quá 250 ký tự.")]
        public string Name { get; set; }

        [DisplayName(@"Mô tả")]
        [MaxLength(500, ErrorMessage = "Mô tả không được phép vượt quá 500 ký tự.")]
        public string Description { get; set; }

        public bool IsActive { get; set; }

        public bool IsDelete { get; set; }

        public List<Shift> Shifts { get; set; }
    }
}
