﻿using System.Collections.Generic;
using GHM.Infrastructure.MongoDb;
using MongoDB.Bson;

namespace GHM.Timekeeping.Domain.Models
{
    public class ReportByDay : Entity<string>
    {
        public int EnrollNumber { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }        
        public int OfficeId { get; set; }        
        public string OfficeIdPath { get; set; }                
        public byte Day { get; set; }
        public byte Month { get; set; }
        public byte Quarter { get; set; }
        public int Year { get; set; }
        public bool? IsSunday { get; set; }
        public bool? IsHoliday { get; set; }
        public decimal WorkUnit { get; set; }
        public int TotalWorkingMin { get; set; }
        public int TotalInLateMin { get; set; }
        public int TotalInSoonMin { get; set; }
        public int TotalOutLateMin { get; set; }
        public int TotalOutSoonMin { get; set; }
        public List<ReportShiftAggregate> ReportShiftAggregate { get; set; }
    }
}
