﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Domain.Constant;
using MongoDB.Bson.Serialization.Attributes;

namespace GHM.Timekeeping.Domain.Models
{
    public class ForgotCheckIn : Entity<string>

    {
        public string UserId { get; set; }
        public int EnrollNumber { get; set; }
        public string FullName { get; set; }
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string OfficeIdPath { get; set; }
        public string Image { get; set; }
        public string ShiftId { get; set; }
        public string ShiftReportName { get; set; }
        public string UnsignName { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Trạng thái
        /// 0: Chờ QLTT duyệt
        /// 1: QLTT duyệt
        /// 2: QLTT không duyệt
        /// </summary>
        public ApproveStatus Status { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime ApproveTime { get; set; }
        public string ApproverUserId { get; set; }
        public string ApproverFullName { get; set; }

        [DisplayName(@"Ghi chú")]
        [MaxLength(500, ErrorMessage = "Ghi chú không được phép vượt quá 500 ký tự.")]
        public string Note { get; set; }

        public int Day { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }

        public int Quarter { get; set; }

        public string ManagerUserId { get; set; }
        public string ManagerFullName { get; set; }

        public bool IsCheckIn { get; set; }

        [DisplayName(@"Lý do không duyệt")]
        [MaxLength(500, ErrorMessage = "Vui lòng nhập lý do không duyệt.")]
        public string DeclineReason { get; set; }

        public string CreatorUserId { get; set; }
        public string CreatorFullName { get; set; }
    }
}
