﻿using System;
using System.Collections.Generic;
using GHM.Infrastructure.MongoDb.Models;
using GHM.Timekeeping.Domain.Constant;
using MongoDB.Bson.Serialization.Attributes;

namespace GHM.Timekeeping.Domain.Models
{
    public class ShiftReference
    {
        public string Id { get; set; }        
        public string ReportName { get; set; }
        public TimeObject StartTime { get; set; }
        public TimeObject EndTime { get; set; }
        public byte? InLatency { get; set; }
        public byte? OutLatency { get; set; }
        public decimal WorkUnit { get; set; }
        public MeaningTime MeaningTime { get; set; }
        public string Code { get; set; }
        public bool IsOvertime { get; set; }

        /// <summary>
        /// Giá trị sẽ là tổng lũy thừa của 2
        /// 1: Chủ nhật
        /// 2: Thứ 2
        /// 4: Thứ 3
        /// 8: Thứ 4
        /// 16: Thứ 5
        /// 32: Thứ 6
        /// 64: Thứ 7
        /// </summary>
        public WorkingDayValue WorkingDaysValue { get; set; }

        [BsonIgnoreIfNull]
        public string ReferenceId { get; set; }
    }
}
