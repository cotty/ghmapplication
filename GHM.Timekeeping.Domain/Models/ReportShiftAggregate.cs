﻿using System;

namespace GHM.Timekeeping.Domain.Models
{
    public class ReportShiftAggregate
    {
        public string ShiftId { get; set; }        
        public string ShiftCode { get; set; }
        public decimal TotalDays { get; set; }
        public string ReportName { get; set; }
    }
}
