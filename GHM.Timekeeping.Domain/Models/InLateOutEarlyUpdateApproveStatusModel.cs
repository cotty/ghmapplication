﻿using System.Collections.Generic;

namespace GHM.Timekeeping.Domain.Models
{
    public class InLateOutEarlyUpdateApproveStatusModel
    {
        public string Id { get; set; }
        public List<InLateOutEarlyUpdateApproveStatusShiftModel> Shifts { get; set; }
    }

    public class InLateOutEarlyUpdateApproveStatusShiftModel
    {
        public string ShiftId { get; set; }
        public bool IsInLate { get; set; }
        public string DeclineReason { get; set; }
        public bool IsApprove { get; set; }
    }
}
