﻿using System;
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Domain.Constant;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GHM.Timekeeping.Domain.Models
{
    public class CheckInCheckOutHistory : Entity<string>
    {
        /// <summary>
        /// Mã chấm công của người dùng
        /// </summary>
        public int EnrollNumber { get; set; }
        /// <summary>
        /// Thời gian chấm vân tay
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime CheckInTime { get; set; }

        /// <summary>
        ///  Mã máy chấm công chấm vân tay
        /// </summary>
        public string MachineId { get; set; }

        public int Year { get; set; }

        public int Month { get; set; }

        public int Day { get; set; }

        public int Hour { get; set; }

        public int Minute { get; set; }

        public int Second { get; set; }

        /// <summary>
        /// Mã ca làm việc
        /// </summary>
        //public ShiftReference Shift { get; set; }
        public string ShiftId { get; set; }

        [BsonIgnoreIfNull]
        public string ShiftCode { get; set; }

        /// <summary>
        /// Có phải là ngày chủ nhật không
        /// </summary>
        public bool IsSunday { get; set; }

        /// <summary>
        /// Có phải là ngày lễ không
        /// </summary>
        public bool IsHoliday { get; set; }

        /// <summary>
        /// True - là checkin False - là checkout
        /// </summary>
        public bool IsCheckIn { get; set; }

        // Số phút đến sớm
        public int InSoonMin { get; set; }

        // Số phút đến muộn
        public int InLateMin { get; set; }

        // Số phút về sớm
        public int OutSoonMin { get; set; }

        // Số phút về muộn
        public int OutLateMin { get; set; }

        public int InLatencyMin { get; set; }

        public int OutLatencyMin { get; set; }

        /// <summary>
        /// Phương thức chấm công:
        /// 0: Vân tay.
        /// 4: Thẻ.        
        /// </summary>
        public CheckInCheckOutMethod Method { get; set; }

        public decimal WorkUnit { get; set; }

        public bool IsOvertime { get; set; }

        public bool IsValidWorkSchedule { get; set; }

        public CheckInCheckOutHistory()
        {
            InLateMin = 0;
            InSoonMin = 0;
            OutLateMin = 0;
            OutSoonMin = 0;
            InLatencyMin = 0;
            OutLatencyMin = 0;
        }
    }
}
