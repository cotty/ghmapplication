﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using GHM.Infrastructure.MongoDb;
using GHM.Infrastructure.MongoDb.Models;
using MongoDB.Bson.Serialization.Attributes;

namespace GHM.Timekeeping.Domain.Models
{
    public class Shift : Entity<string>
    {
        [DisplayName(@"Tên ca làm việc")]
        [Required(ErrorMessage = "Vui lòng nhập ca làm việc.")]
        [MaxLength(250, ErrorMessage = "Tên ca làm việc không được phép vượt quá 250 ký tự.")]
        public string Name { get; set; }

        [DisplayName(@"Tên trong báo cáo")]
        [MaxLength(250, ErrorMessage = "Tên trong báo cáo không được phép vượt quá 250 ký tự.")]
        public string ReportName { get; set; }

        [DisplayName(@"Giờ bắt đầu ca")]
        [Required(ErrorMessage = "Vui lòng nhập giờ bắt đầu ca.")]
        public TimeObject StartTime { get; set; }

        [DisplayName(@"Giờ bắt Kết thúc ca")]
        [Required(ErrorMessage = "Vui lòng nhập giờ kết thúc ca.")]
        public TimeObject EndTime { get; set; }

        public byte? InLatency { get; set; }

        public byte? OutLatency { get; set; }

        [DisplayName(@"Tính công")]
        [Required(ErrorMessage = "Tính công không được để trống.")]
        public decimal WorkUnit { get; set; }

        [DisplayName(@"Thời gian hiểu ca")]
        [Required(ErrorMessage = "Vui lòng nhập thời gian hiểu ca.")]
        public MeaningTime MeaningTime { get; set; }

        [DisplayName(@"Ký hiệu ca")]
        [Required(ErrorMessage = "Vui lòng nhập ký hiệu ca.")]
        [MaxLength(20, ErrorMessage = "Ký hiệu ca không được phép vượt quá 20 ký tự.")]
        public string Code { get; set; }

        public bool IsOvertime { get; set; }

        public bool IsDelete { get; set; }

        /// <summary>
        /// Mã ca gối trước ca này.
        /// </summary>
        [BsonIgnoreIfNull]
        public string ReferenceId { get; set; }
    }

    /// <summary>
    /// Thời gian hiểu ca
    /// </summary>
    public class MeaningTime
    {
        /// <summary>
        ///  Thời gian bắt đầu hiểu ca vào
        /// </summary>
        public TimeObject StartTimeIn { get; set; }

        /// <summary>
        ///  Thời gian bắt đầu hiểu ca ra
        /// </summary>
        public TimeObject EndTimeIn { get; set; }

        /// <summary>
        /// Thời gian bắt đầu hiểu ca ra
        /// </summary>
        public TimeObject StartTimeOut { get; set; }

        /// <summary>
        /// Thời gian kết thúc hiểu ca ra
        /// </summary>
        public TimeObject EndTimeOut { get; set; }
    }
}
