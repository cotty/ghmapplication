﻿using GHM.Infrastructure.MongoDb;

namespace GHM.Timekeeping.Domain.Models
{
    public class Config : Entity<string>
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string Group { get; set; }
    }
}
