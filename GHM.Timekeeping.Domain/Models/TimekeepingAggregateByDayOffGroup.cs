﻿namespace GHM.Timekeeping.Domain.Models
{
    public class TimekeepingAggregateByDayOffGroup
    {
        public string DayOffGroupId { get; set; }
        public string DayOffGroupName { get; set; }
        public int TotalDay { get; set; }
    }
}
