﻿using System.Collections.Generic;

namespace GHM.Timekeeping.Domain.Models
{
    public class ShiftGroupReference
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public List<ShiftReference> Shifts { get; set; }
    }
}
