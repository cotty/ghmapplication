﻿namespace GHM.Timekeeping.Domain.Models
{
    public class TimekeepingAggregateMonth
    {
        public int EnrollNumber { get; set; }
        public string UserId { get; set; }
        public int TotalWorkday { get; set; }
        public int TotalSunday { get; set; }
        public int TotalHoliday { get; set; }
        public int TotalLateMinutes { get; set; }
        public int TotalSoonMinutes { get; set; }
        public int TotalOvertimeMinutes { get; set; }
    }
}
