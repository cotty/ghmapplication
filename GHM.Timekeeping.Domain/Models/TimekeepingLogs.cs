﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace GHM.Timekeeping.Domain.Models
{
    public class TimekeepingLogs
    {
        public string UserId { get; set; }
        public string FullName { get; set; }

        /// <summary>
        /// Giá trị update
        /// </summary>
        public int Disposition { get; set; }

        /// <summary>
        /// Dạng dữ liệu update
        /// 0: Value 1: System (Dạng system sau này phục vụ cho đa ngôn ngữ) 2: Ngày tháng 3: Người dùng 4: Json
        /// </summary>
        public byte DataType { get; set; }

        /// <summary>
        /// Hình thức
        /// 0: Truy cập 1: Thêm mới 2: Sửa 3: Xoá 4: Export 5: In ấn 6: Duyệt
        /// </summary>
        public int Action { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime ActionTime { get; set; }

        public string FromValue { get; set; }
        public string ToValue { get; set; }
    }
}
