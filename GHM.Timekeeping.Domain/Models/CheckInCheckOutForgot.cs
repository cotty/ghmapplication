﻿using System;
using System.Collections.Generic;

namespace GHM.Timekeeping.Domain.Models
{
    public class CheckInCheckOutForgot
    {
        /// <summary>
        /// Mã chấm công của người dùng.
        /// </summary>
        public int EnrollNumber { get; set; }

        /// <summary>
        /// Mã người dùng.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Ngày quyên chấm công.
        /// </summary>
        public DateTime DateForgot { get; set; }

        /// <summary>
        /// Mã người tạo ra bản ghi này.
        /// </summary>
        public string CreatorId { get; set; }

        /// <summary>
        /// Tên người tạo ra bản ghi này.
        /// </summary>
        public string CreatorFullName { get; set; }

        /// <summary>
        /// Danh sách ca quên chấm công.
        /// </summary>
        public List<ShiftForgot> ShiftForgots { get; set; }
    }

    public class ShiftForgot
    {
        /// <summary>
        /// Mã ca quên chấm công
        /// </summary>
        public string ShiftId { get; set; }

        /// <summary>
        /// Loại quên: 0: Checkin 1: Checkout 2: Both
        /// </summary>
        public byte Type { get; set; }
    }
}
