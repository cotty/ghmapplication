﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Timekeeping.Domain.Models
{
    public class TimekeepingGeneralConfig
    {
        public string MaxInOutMin { get; set; }
        public string MaxInOutTimes { get; set; }
    }
}
