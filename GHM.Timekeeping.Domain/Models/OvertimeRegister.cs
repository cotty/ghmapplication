﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GHM.Infrastructure.MongoDb;
using GHM.Infrastructure.MongoDb.Models;
using GHM.Timekeeping.Domain.Constant;
using MongoDB.Bson.Serialization.Attributes;

namespace GHM.Timekeeping.Domain.Models
{
    public class OvertimeRegister : Entity<string>
    {
        public string UserId { get; set; }
        public int EnrollNumber { get; set; }
        public string FullName { get; set; }
        public string TitleId { get; set; }
        public string TitleName { get; set; }
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string OfficeIdPath { get; set; }
        public string Image { get; set; }
        public string ShiftId { get; set; }
        public string ShiftReportName { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RegisterDate { get; set; }

        [DisplayName(@"Thời gian bắt đầu")]
        [Required(ErrorMessage = "Vui lòng chọn thời gian bắt đầu.")]
        public TimeObject From { get; set; }

        [DisplayName(@"Thời gian kê thúc")]
        [Required(ErrorMessage = "Vui lòng chọn thời gian kết thúc.")]
        public TimeObject To { get; set; }

        public int TotalMinutes { get; set; }        

        /// <summary>
        /// Trạng thái
        /// 0: Chờ QLTT duyệt
        /// 1: QLTT duyệt
        /// 2: QLTT không duyệt
        /// </summary>
        public ApproveStatus Status { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime ApproveTime { get; set; }
        public string ApproverId { get; set; }
        public string ApproverFullName { get; set; }

        [DisplayName(@"Ghi chú")]
        [MaxLength(500, ErrorMessage = "Ghi chú không được phép vượt quá 500 ký tự.")]
        public string Note { get; set; }

        public int Day { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }

        public int Quarter { get; set; }

        public string ManagerUserId { get; set; }

        [DisplayName(@"Hình thức")]
        [Required(ErrorMessage = "Vui lòng chọn hình thức")]
        public OvertimeRegister Type { get; set; }

        [DisplayName(@"Lý do không duyệt")]
        [MaxLength(500, ErrorMessage = "Lý do không duyệt không được phép vượt quá 500 ký tự.")]
        public string DeclineReason { get; set; }

        public bool IsDelete { get; set; }

        public OvertimeRegister()
        {
            IsDelete = false;
        }
    }
}
