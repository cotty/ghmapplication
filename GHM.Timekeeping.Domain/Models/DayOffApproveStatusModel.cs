﻿using System;
using System.Collections.Generic;
using GHM.Timekeeping.Domain.Constant;

namespace GHM.Timekeeping.Domain.Models
{
    public class DayOffApproveStatusModel
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string CurrentUserId { get; set; }
        public int EnrollNumber { get; set; }
        public string FullName { get; set; }
        public int OfficeId { get; set; }
        public string OfficeIdPath { get; set; }
        public string UnsignName { get; set; }
        public string ManagerNote { get; set; }
        public string ApproverNote { get; set; }
        public string ManagerDeclineReason { get; set; }
        public string ApproverDeclineReason { get; set; }
        public DayOffStatus Status { get; set; }
        public DayOffApproveType Type { get; set; }
        public List<DayOffDate> Dates { get; set; }

        public DayOffApproveStatusModel(string id, string userId, string currentUserId, int enrollNumber, string fullName, int officeId, string officeIdPath,
            string unsignName, string managerNote, string approverNote, string managerDeclineReason, string approverDeclineReason, DayOffStatus status,
            DayOffApproveType type, List<DayOffDate> dates)
        {
            Id = id;
            UserId = userId;
            CurrentUserId = currentUserId;
            EnrollNumber = enrollNumber;
            FullName = fullName;
            OfficeId = officeId;
            OfficeIdPath = officeIdPath;
            UnsignName = unsignName;
            ManagerNote = managerNote;
            ApproverNote = approverNote;
            ManagerDeclineReason = managerDeclineReason;
            ApproverDeclineReason = approverDeclineReason;
            Status = status;
            Type = type;
            Dates = dates;
        }
    }
}
