﻿using GHM.Infrastructure.MongoDb;

namespace GHM.Timekeeping.Domain.Models
{
    public class Holidays : Entity<string>
    {
        public string Name { get; set; }
        public DayObject FromDay { get; set; }
        public DayObject ToDay { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public bool IsRangerDate { get; set; }
        public int Year { get; set; }
    }

    public class DayObject
    {
        public int? Day { get; set; }
        public int? Month { get; set; }
    }
}
