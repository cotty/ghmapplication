﻿using System;
using GHM.Infrastructure.MongoDb.Models;
using GHM.Timekeeping.Domain.Constant;

namespace GHM.Timekeeping.Domain.Models
{
    public class ShiftAggregate
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public DateTime In { get; set; }
        public DateTime? Out { get; set; }
        public TimeObject InTime { get; set; }
        public TimeObject OutTime { get; set; }
        public int LateMinutes { get; set; }
        public int SoonMinutes { get; set; }

        /// <summary>
        /// Trạng thái làm việc.
        /// null: đi làm bình thường.  
        /// </summary>
        public DayOffMethod? Status { get; set; }
    }
}
