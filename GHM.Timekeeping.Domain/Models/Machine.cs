﻿using GHM.Infrastructure.MongoDb;

namespace GHM.Timekeeping.Domain.Models
{
    public class Machine : Entity<string>
    {
        public int No { get; set; }
        public string Name { get; set; }
        public string Ip { get; set; }
        public int Port { get; set; }
        public bool IsActive { get; set; }
        public string SerialNumber { get; set; }        
    }
}
