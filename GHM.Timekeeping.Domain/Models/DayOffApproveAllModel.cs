﻿namespace GHM.Timekeeping.Domain.Models
{
    public class DayOffApproveAllModel
    {
        public string Id { get; set; }
        public bool IsApprove { get; set; }
        public string Note { get; set; }
        public string Reason { get; set; }
        public int? UserHolidayRemaining { get; set; }
    }
}
