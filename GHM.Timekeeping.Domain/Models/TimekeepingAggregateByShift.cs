﻿using System.Collections.Generic;

namespace GHM.Timekeeping.Domain.Models
{
    public class TimekeepingAggregateByShift
    {
        public int EnrollNumber { get; set; }
        public string UserId { get; set; }
        public int OfficeId { get; set; }
        public int TitleId { get; set; }
        public string OfficeIdPath { get; set; }
        public List<ShiftGroupAggregate> ShiftGroupAggregates { get; set; }
    }

    public class ShiftGroupAggregate
    {
        public string ShiftId { get; set; }
        public int Total { get; set; }
    }
}
