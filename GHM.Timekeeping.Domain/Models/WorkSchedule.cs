﻿using System;
using System.Collections.Generic;
using GHM.Infrastructure.MongoDb;
using MongoDB.Bson.Serialization.Attributes;

namespace GHM.Timekeeping.Domain.Models
{
    public class WorkSchedule : Entity<string>
    {
        public int? EnrollNumber { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string UnsignName { get; set; }
        public string Image { get; set; }
        public int? OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string OfficeIdPath { get; set; }
        public int? TitleId { get; set; }
        public string TitleName { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime FromDate { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? ToDate { get; set; }

        public string ShiftGroupId { get; set; }
        public string ShiftGroupName { get; set; }
        public List<ShiftReference> Shifts { get; set; }

        public WorkSchedule()
        {
            FromDate = DateTime.Now;
        }
    }
}