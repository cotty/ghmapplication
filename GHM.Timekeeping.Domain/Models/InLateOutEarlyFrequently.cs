﻿using System;
using GHM.Infrastructure.MongoDb;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GHM.Timekeeping.Domain.Models
{
    public class InLateOutEarlyFrequently : Entity<string>
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string OfficeIdPath { get; set; }
        public InOutFrequentlyDetail[] InOutFrequentlyDetails { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? FromDate { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? ToDate { get; set; }

        public string Reason { get; set; }
        public string Note { get; set; }
        public bool IsDelete { get; set; }
        public bool IsActive { get; set; }
        public string UnsignName { get; set; }
    }

    public class InOutFrequentlyDetail : Entity<string>
    {
        /// <summary>
        /// Mã ngày trong tuần.
        /// 0: CN
        /// 1: Thứ 2
        /// 2: Thứ 3
        /// 3: Thứ 4
        /// 4: Thứ 5
        /// 5: Thứ 6
        /// 6: Thứ 7
        /// </summary>
        public int DayOfWeek { get; set; }
        public string ShiftId { get; set; }
        public string ShiftReportName { get; set; }
        public bool IsInLate { get; set; }
        public int TotalMinutes { get; set; }
        public bool IsDelete { get; set; }

        public InOutFrequentlyDetail()
        {
            Id = ObjectId.GenerateNewId().ToString();
        }

    }
}