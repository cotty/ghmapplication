﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using GHM.Infrastructure.MongoDb;
using GHM.Infrastructure.MongoDb.Models;
using GHM.Timekeeping.Domain.Models;
using MongoDB.Bson;

namespace GHM.Timekeeping.Domain.ModelMetas
{
    public class TimekeepingShiftMeta
    {
        public class TimekeepingShift : Entity<string>
        {
            [DisplayName(@"Tên ca làm việc")]
            [Required(ErrorMessage = "Tên ca làm việc không được để trống.")]
            [MaxLength(250, ErrorMessage = "Tên ca làm việc không được phép vượt quá 250 ký tự.")]
            public string Name { get; set; }

            [DisplayName(@"Giờ bắt đầu ca")]
            [Required(ErrorMessage = "Vui lòng nhập giờ bắt đầu ca.")]
            public TimeObject StartTime { get; set; }

            [DisplayName(@"Giờ kết thúc ca")]
            [Required(ErrorMessage = "Vui lòng nhập giờ kết thúc ca.")]
            public TimeObject EndTime { get; set; }

            public byte? InLatency { get; set; }
            public byte? OutLatency { get; set; }

            [DisplayName(@"Ký hiệu ca")]
            [Required(ErrorMessage = "Vui lòng nhập ký hiệu ca.")]
            [MaxLength(20, ErrorMessage = "Ký hiệu ca không được phép vượt quá 20 ký tự.")]           
            public decimal WorkUnit { get; set; }

            [DisplayName(@"Thời gian hiểu ca")]
            [Required(ErrorMessage = "Vui lòng nhập thời gian hiểu ca.")]
            public MeaningTime MeaningTime { get; set; }

            [DisplayName(@"Tính công")]
            [Required(ErrorMessage = "Tính công không được để trống.")]
            public string Code { get; set; }

            public bool IsOvertime { get; set; }

            public bool IsDelete { get; set; }

            public bool IsAllowNoStartTime { get; set; }
        }
    }
}
