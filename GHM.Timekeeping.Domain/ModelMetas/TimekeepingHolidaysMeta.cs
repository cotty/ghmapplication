﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Domain.ModelMetas
{
    public class TimekeepingHolidaysMeta
    {
        public string Id { get; set; }

        [DisplayName(@"Họ và tên")]
        [Required(ErrorMessage = "Họ và tên không được để trống.")]
        [MaxLength(250, ErrorMessage = "Họ và tên không được phép vượt quát 250 ký tự.")]
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public DayObject FromDay { get; set; }

        public DayObject ToDay { get; set; }

        public int Year { get; set; }
    }
}
