﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Domain.ModelMetas
{
    public class TimekeepingInLateOutEarlyFrequentlyMeta
    {
        public string Id { get; set; }

        [DisplayName(@"Người dùng")]
        [Required()]
        public string UserId { get; set; }

        [DisplayName(@"Lý do xin nghỉ")]
        [Required(ErrorMessage = "Lý do xin nghỉ không được để trống.")]
        [MaxLength(500, ErrorMessage = "Lý do xin nghỉ không được phép vượt quá 500 ký tự")]
        public string Reason { get; set; }

        [DisplayName(@"Ghi chú")]
        [MaxLength(500, ErrorMessage = "Ghi chú không được phép vượt quá 500 ký tự.")]
        public string Note { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public bool IsActive { get; set; }

        public InOutFrequentlyDetail[] InOutFrequentlyDetails { get; set; }
    }
}
