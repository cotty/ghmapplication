﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Domain.IRepository
{
    public interface ICheckInCheckOutAggregateRepository
    {
        Task<string> Insert(ReportByShift aggregate);

        Task<bool> CheckExists(int day, int month, int year);
    }
}
