﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Domain.IRepository
{
    public interface IShiftRepository
    {        
        Task<long> Insert(Shift shift);

        Task<long> Update(Shift shift);

        Task<long> Delete(string id);

        Task<List<Shift>> SearchAll();

        Task<Shift> GetInfo(string id);
    }
}
