﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using GHM.Timekeeping.Domain.ModelMetas;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Domain.IRepository
{
    public interface IConfigShiftRepository
    {
        #region Shift
        Task<long> Insert(Shift shift);

        Task<long> Update(Shift shift);

        Task<long> Delete(string id);

        Task<List<Shift>> SearchAll();
        #endregion


        #region shift group
        Task<ShiftGroup> InsertGroup(ShiftGroup shiftGroup);

        Task<long> UpdateGroup(ShiftGroup shiftGroup);

        Task<long> UpdateGroupActive(string id, bool isActive);

        Task<long> DeleteGroup(string id);

        Task<ShiftGroup> GetGroupInfo(string id);

        Task<List<ShiftGroup>> SearchAllGroup(bool? isActive = null);
        #endregion
    }
}
