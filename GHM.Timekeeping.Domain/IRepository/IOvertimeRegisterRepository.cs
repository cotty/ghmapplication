﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GHM.Timekeeping.Domain.Constant;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Domain.IRepository
{
    public interface IOvertimeRegisterRepository
    {
        Task<long> Insert(OvertimeRegister overtimeRegister);

        Task<long> Update(OvertimeRegister overtimeRegister);

        Task<long> Delete(string id);

        Task<long> Approve(string id, string userId, bool isApprove, string note);

        Task<OvertimeRegister> GetInfo(string id);

        Task<OvertimeRegister> GetInfo(string userId, string shiftId, int day, int month, int year);

        Task<bool> CheckExists(string shiftId, string userId, int day, int month, int year);

        Task<List<OvertimeRegister>> Search(string currentUserId, string userId, int month, int year, byte type, ApproveStatus? status, int page, int pageSize,
            out long totalRows);
    }
}
