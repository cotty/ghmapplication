﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Domain.IRepository
{
    public interface IReportByMonthRepository
    {
        IReportByShiftRepository ReportByShiftRepository { get; set; }

        Task<long> Save(ReportByMonth reportByMonth);

        Task Aggregate(int enrollNumber, int month, int year);

        Task<ReportByMonth> GetInfo(int enrollNumber, int month, int year);

        Task<bool> CheckExists(int enrollNumber, int month, int year);

        Task<List<ReportByMonth>> Search(string keyword, int officeId, int month, int year);

        Task<List<ReportByMonth>> SearchMyResult(string userId, byte? month, int year);
    }
}
