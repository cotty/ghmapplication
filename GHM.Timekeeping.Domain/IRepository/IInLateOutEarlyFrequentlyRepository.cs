﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Timekeeping.Domain.ModelMetas;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Domain.IRepository
{
    public interface IInLateOutEarlyFrequentlyRepository
    {
        Task<bool> CheckExists(string id, string userId, DateTime? fromDate, DateTime? toDate);

        Task<long> Insert(TimekeepingInLateOutEarlyFrequentlyMeta inLateOutEarlyFrequentlyMeta);

        Task<long> Update(TimekeepingInLateOutEarlyFrequentlyMeta inLateOutEarlyFrequentlyMeta);

        Task<long> InsertDetail(string id, InOutFrequentlyDetail inOutFrequentlyDetail);

        Task<long> UpdateDetail(string id, InOutFrequentlyDetail inOutFrequentlyDetail);

        Task<long> Delete(string id);

        Task<long> DeleteDetail(string id, string detailId);

        Task<long> UpdateActive(string id, bool isActive);

        Task<InLateOutEarlyFrequently> GetInfo(string id);

        Task<InLateOutEarlyFrequently> GetInfoByUserId(string userId, DateTime dayCheck);

        Task<List<InLateOutEarlyFrequently>> Search(string keyword, bool? isActive, DateTime? fromDate, DateTime? toDate,
            int page, int pageSize, out long totalRows);
    }
}
