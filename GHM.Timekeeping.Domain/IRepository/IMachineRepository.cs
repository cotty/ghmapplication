﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Domain.IRepository
{
    public interface IMachineRepository
    {
        Task<long> Insert(Machine machine);

        Task<long> Update(Machine machine);

        Task<int> InsertUserToMachine(int enrollNumber, string fullName, string cardNumber = "", string password = "");

        Task<long> Delete(string id);

        Task<Machine> GetInfo(string id);

        Task<List<Machine>> GetAllMachine(bool? isActive = null);
    }
}
