﻿using System.Threading.Tasks;
using MongoDB.Bson;

namespace GHM.Timekeeping.Domain.IRepository
{
    using Models;

    public interface IConfigRepository
    {
        Task<ObjectId> Insert(Timekeeping timekeeping);

        Task<int> Update(Timekeeping timekeeping);

        Task<int> Delete(ObjectId id);
    }
}
