﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Domain.IRepository
{
    public interface ICheckInCheckOutHistoryRepository
    {
        Task<long> Save(CheckInCheckOutHistory history);

        Task<long> Insert(CheckInCheckOutHistory history);

        Task<long> Update(CheckInCheckOutHistory history);

        Task<bool> CheckExists(int enrollNumber, int year, int month, int day, int hour, int minute, int second, string shiftId);

        Task<bool> CheckIsHasCheckIn(int enrollNumber, string shiftId, int day, int month, int year);

        Task<bool> CheckExistsByDayAndShift(DateTime day, string shiftId);

        Task<CheckInCheckOutHistory> GetInfo(string id);

        Task<long> AddNewEndShiftReference(string referenceId, int enrollNumber, string machineId, int day, int month, int year);

        Task<List<CheckInCheckOutHistory>> GetsForAggregate(int enrollNumber, string shiftId, int day, int month, int year);

        Task<List<CheckInCheckOutHistory>> GetCheckInCheckOutHistory(int enrollNumber, int day, int month, int year);        

        Task<decimal> CountHolidays(int day, int month, int year, int enrollNumber);        
    }
}
