using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Domain.IRepository
{
    public interface IShiftGroupRepository
    {
        Task<ShiftGroup> Insert(ShiftGroup shiftGroup);
        Task<long> Update(ShiftGroup shiftGroup);
        Task<long> UpdateActive(string id, bool isActive);
        Task<long> Delete(string id);
        Task<ShiftGroup> GetInfo(string id);
        Task<long> UpdateShift(Shift shift);
        Task<List<ShiftGroup>> SearchAll(bool? isActive = null);
    }
}