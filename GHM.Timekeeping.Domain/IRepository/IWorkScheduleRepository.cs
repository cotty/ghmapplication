﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Domain.IRepository
{
    public interface IWorkScheduleRepository
    {
        IShiftRepository ShiftRepository { get; set; }
        IShiftGroupRepository ShiftGroupRepository { get; set; }

        Task<List<WorkSchedule>> Search(string keyword, int officeId, int page, int pageSize, out long totalRows);

        Task<long> Insert(WorkSchedule workSchedule);

        Task<long> Save(WorkSchedule workSchedule);

        Task<long> SaveMany(List<WorkSchedule> workSchedule);

        Task<WorkSchedule> GetByEnrollNumber(int enrollNumber);

        Task<List<WorkSchedule>> GetAllByEnrollNumber(int enrollNumber);

        Task<WorkSchedule> GetByEnrollNumberAndCheckInTime(int enrollNumber, DateTime checkInTime);

        Task<WorkSchedule> GetByUserId(string userId);

        Task<WorkSchedule> GetCurrentWorkScheduleByUserId(string userId);

        Task<long> UpdateShift(Shift shift);

        Task<WorkSchedule> GetInfo(string id);

        Task<bool> CheckIsHasWorkSchedule(int enrollNumber, string shiftId, DateTime checkInTime);

        Task<bool> CheckIsWeekLeave(int enrollNumber, string shiftId, DateTime dayCheck);

        Task<bool> CheckShiftExists(string shiftId);

        Task<bool> CheckShiftGroupExists(string shiftGroupId);

        Task<WorkSchedule> GetMyWorkSchedule(string userId);

        List<ShiftReference> GetMyWorkScheduleShift(string userId);

        Task<long> UpdateOfficeInfo(int officeId, string officeName);
    }
}
