﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Timekeeping.Domain.Constant;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Domain.IRepository
{
    public interface IDayOffRepository
    {
        Task<List<DayOff>> Search(string currentUserId, string keyword, DateTime? fromDate, DateTime? toDate, DayOffStatus? status,
            byte type, int page, int pageSize, out long totalRows);

        Task<int> Insert(DayOff register);

        Task<long> Update(DayOff register);

        Task<long> Delete(string id);

        Task<long> Approve(DayOffApproveStatusModel dayOffApprove);

        Task<long> ApproveAll(DayOffApproveAllModel dayOffApprove, string currentUserId);

        Task<long> Cancel(string id);

        Task<DayOff> GetInfo(string id);

        Task<List<DayOff>> GetListDayOffRegister(int enrollNumber, DateTime day);

        Task<long> InsertApprovedDayOffIntoReportByShift(int enrollNumber, DateTime day);

        //Task<bool> CheckExists(int enrollNumber, string shiftId, DateTime fromDate, DateTime? toDate);
    }
}
