﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Domain.IRepository
{
    public interface IInLateOutEarlyRepository
    {
        Task<List<InLateOutEarly>> Search(string userId, string currentUserId, byte type, int month, int year, bool? isConfirm, int page, int pageSize, out long totalRows);

        Task<long> Insert(InLateOutEarly inLateOutEarly, string currentUserId);

        Task<long> Update(InLateOutEarly inLateOutEarly, string currentUserId);

        Task<long> UpdateConfirmStatus(string id);

        Task<long> Approve(InLateOutEarlyUpdateApproveStatusModel inLateOutEarly);

        Task<long> Delete(string id);

        Task<InLateOutEarly> GetInfo(string id);

        Task<InLateOutEarly> GetInfo(int enrollNumber, int day, int month, int year, string shiftId);

        Task<bool> CheckIsAllowAddMore(string id, string userId, int month, int year);

        Task<bool> CheckIsValidMin(int min);

        Task<bool> CheckExists(string id, string userId, bool isInLate, int day, int month, int year, string shiftId);

        int GetTotalInLatencyMin(int enrollNumber, int month, int year);

        int GetTotalOutLatencyMin(int enrollNumber, int month, int year);

        Task<int> GetTotalApprovedInLateOutEarlyTimes(string userId, int month, int year);
    }
}
