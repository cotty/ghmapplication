﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Domain.IRepository
{
    public interface ITimekeepingLogsRepository
    {
        Task<long> Insert();

        Task<List<TimekeepingLogs>> Search(DateTime? fromDate, DateTime? toDate, int page, int pageSize,
            out long totalRows);
    }
}
