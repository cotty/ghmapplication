﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Timekeeping.Domain.Constant;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Domain.IRepository
{
    public interface IReportByShiftRepository
    {

        IDayOffRepository DayOffRepository { get; set; }

        Task<long> Save(ReportByShift reportByShift);

        Task<long> Insert(ReportByShift reportByShift);

        Task<long> InsertFromDayOffRegister(List<ReportByShift> reportByShifts);

        Task<long> DeleteShift(int day, int month, int year, int enrollNumber, string shiftId);

        Task<List<ReportByShift>> GetListTimesheet(string keyword, int officeId, int month, int year);

        Task<List<ReportByShift>> GetListMyTimeSheet(string userId, int? month, int year);

        Task<List<ReportByShift>> GetListReportByShiftForAggregate(int month, int year);

        Task<List<ReportByShift>> GetListReportByShiftForAggregateByUser(int enrollNumber, int month, int year);

        Task<List<ReportByShift>> GetListTimesheetResult(string keyword, int officeId, byte month, int year);

        Task<List<ReportByShift>> GetListReportByShiftByUser(int enrollNumber, byte month, int year);

        Task<bool> CheckExists(int day, int month, int year, int enrollNumber, string shiftId);

        Task<bool> CheckIsHasCheckInOrCheckOut(int day, int month, int year, int enrollNumber, string shiftId, bool isCheckIn);

        Task<long> UpdateLatency(int enrollNumber, string shiftId, int day, int month, int year, int inLatencyMin,
            int outLatencyMin, string inLatencyReason, string outLatencyReason);

        Task<long> UpdateOvertimeRegister(int enrollNumber, string shiftId, int day, int month, int year, int totalMinutes);

        Task<long> UpdateCheckInCheckOutTime(ForgotCheckIn forgotCheckIn);

        Task<long> MarkAsValid(int day, int month, int year, int enrollNumber, string shiftId, bool isCheckIn);

        Task<long> ChangeMethod(int day, int month, int year, int enrollNumber, string shiftId, DayOffMethod method);
    }
}
