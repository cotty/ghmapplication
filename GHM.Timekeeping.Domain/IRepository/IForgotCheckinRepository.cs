﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Timekeeping.Domain.Constant;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Domain.IRepository
{
    public interface IForgotCheckInRepository
    {
        Task<long> Insert(ForgotCheckIn forgotCheckin);

        Task<long> Update(ForgotCheckIn forgotCheckin);

        Task<long> Delete(string id);

        Task<long> Approve(string id, bool isApprove, string note);

        Task<ForgotCheckIn> GetInfo(string id);

        Task<ForgotCheckIn> GetInfo(string userId, string shiftId, int day, int month, int year);

        Task<bool> CheckExists(string shiftId, string userId, int day, int month, int year);

        Task<List<ForgotCheckIn>> Search(string currentUserId, string userId, int month, int year, byte type, ApproveStatus? status, int page, int pageSize,
            out long totalRows);
    }
}
