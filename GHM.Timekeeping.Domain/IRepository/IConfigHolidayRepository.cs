﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Timekeeping.Domain.ModelMetas;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Domain.IRepository
{
    public interface IConfigHolidayRepository
    {
        Task<Holidays> Insert(TimekeepingHolidaysMeta timekeepingHoliday);

        Task<long> Update(TimekeepingHolidaysMeta timekeepingHoliday);

        Task<long> Delete(string id);

        Task<List<Holidays>> SearchAll(int year, bool? isActive = null);

        Task<List<T>> SearchAll<T>(Expression<Func<Holidays, T>> projector);

        Task<long> UpdateActiveStatus(string id, bool isActive);

        Task<bool> CheckExists(int day, int month, int year);

        Task<List<Holidays>> SearchByMonthAndYear(int month, int year);
    }
}
