﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Timekeeping.Domain.Constant
{
    public enum OvertimeRegisterType
    {
        /// <summary>
        /// Đánh máy.
        /// </summary>
        Typing,
        /// <summary>
        /// Làm thủ thuật.
        /// </summary>
        DoTrick,
        /// <summary>
        /// Tăng cường.
        /// </summary>
        Reinforcement,
        /// <summary>
        /// Trực trưa
        /// </summary>
        NoonShift
    }
}
