﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Timekeeping.Domain.Constant
{
    public enum WorkingDayValue
    {
        /// <summary>
        /// Chủ nhật.
        /// </summary>
        Sunday = 1,
        /// <summary>
        /// Thứ 2.
        /// </summary>
        Monday = 2,
        /// <summary>
        /// Thứ 3.
        /// </summary>
        Tuesday = 4,
        /// <summary>
        /// Thứ 4.
        /// </summary>
        Wednesday = 8,
        /// <summary>
        /// Thứ 5.
        /// </summary>
        Thursday = 16,
        /// <summary>
        /// Thứ 6.
        /// </summary>
        Friday = 32,
        /// <summary>
        /// Thứ 7.
        /// </summary>
        Saturday = 64
    }
}
