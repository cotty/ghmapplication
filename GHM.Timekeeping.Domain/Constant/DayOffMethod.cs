﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Timekeeping.Domain.Constant
{
    public enum DayOffMethod
    {
        /// <summary>
        /// Nghỉ phép hàng năm.
        /// </summary>
        AnnualLeave,
        /// <summary>
        /// Nghỉ không lương.
        /// </summary>
        UnpaidLeave,
        /// <summary>
        /// Nghỉ bù.
        /// </summary>
        Compensatory,
        /// <summary>
        /// Nghỉ bảo hiểm.
        /// </summary>
        InsuranceLeave,
        /// <summary>
        /// Nghỉ chế độ.
        /// </summary>
        Entitlement,
        /// <summary>
        /// Nghỉ tuần.
        /// </summary>
        WeekLeave,
        /// <summary>
        /// Nghỉ lễ.
        /// </summary>
        HolidayLeave,
        /// <summary>
        /// Nghỉ không phép.
        /// </summary>
        UnAuthorizedLeave 
    }
}
