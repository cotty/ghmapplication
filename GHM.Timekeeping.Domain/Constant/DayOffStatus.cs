﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Timekeeping.Domain.Constant
{
    public enum DayOffStatus
    {
        /// <summary>
        /// Chời QLTT phê duyệt.
        /// </summary>
        WaitingManagerApprove,
        /// <summary>
        /// QLTT duyệt.
        /// </summary>
        ManagerApprove,
        /// <summary>
        /// QLTT Duyệt chờ QLPD duyệt.
        /// </summary>
        ManagerApproveWaitingApproverApprove,
        /// <summary>
        /// QLTT không duyệt.
        /// </summary>
        ManagerDecline,
        /// <summary>
        /// QLPD duyệt.
        /// </summary>
        ApproverApprove,
        /// <summary>
        /// QLPD không duyệt.
        /// </summary>
        ApproverDecline,
        /// <summary>
        /// Hủy.
        /// </summary>
        Cancel
    }
}
