﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Timekeeping.Domain.Constant
{
    public enum CheckInCheckOutMethod
    {
        Finger = 1,
        Card = 4,
        Auto = 5
    }
}
