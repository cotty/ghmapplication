﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Timekeeping.Domain.Constant
{
    public enum ReportByShiftType
    {
        /// <summary>
        /// Tự động.
        /// </summary>
        Auto,
        /// <summary>
        /// Thủ công.
        /// </summary>
        Manual
    }
}
