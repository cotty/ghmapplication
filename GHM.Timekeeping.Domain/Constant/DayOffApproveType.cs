﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Timekeeping.Domain.Constant
{
    public enum DayOffApproveType
    {
        /// <summary>
        /// Nhân viên.
        /// </summary>
        User,
        /// <summary>
        /// QLTT.
        /// </summary>
        Manager,
        /// <summary>
        /// QLPD.
        /// </summary>
        Approver
    }
}
