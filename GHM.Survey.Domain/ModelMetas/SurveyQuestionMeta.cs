﻿using System.Collections.Generic;

namespace GHM.Survey.Domain.ModelMetas
{
    public class SurveyQuestionMeta
    {
        public string Id { get; set; }
        public decimal? Point { get; set; }
        public List<SurveyQuestionAnswerMeta> Answers { get; set; }
    }

    public class SurveyQuestionAnswerMeta
    {
        public string Id { get; set; }
        public string ToQuestionVersionId { get; set; }
    }
}
