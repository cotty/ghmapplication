﻿using System.Collections.Generic;

namespace GHM.Survey.Domain.ModelMetas
{
  public  class SurveyQuestionsMeta
    {
        public List<string> Questions { get; set; }
        public List<SurveyQuestionGroupMeta> QuestionGroups { get; set; }
    }
}
