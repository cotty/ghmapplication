﻿using System.Collections.Generic;
using GHM.Survey.Domain.Constants;

namespace GHM.Survey.Domain.ModelMetas
{
    public class ChangeListQuestionStatusMeta
    {
        public List<string> QuestionVersionIds { get; set; }

        public QuestionStatus QuestionStatus { get; set; }

        public string Reason { get; set; }
    }
}
