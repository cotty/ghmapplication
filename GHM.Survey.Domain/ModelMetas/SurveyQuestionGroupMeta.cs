﻿namespace GHM.Survey.Domain.ModelMetas
{
    public class SurveyQuestionGroupMeta
    {
        public int Id { get; set; }
        public int TotalQuestions { get; set; }
    }
}
