﻿using System.Collections.Generic;
namespace GHM.Survey.Domain.ModelMetas
{
  public  class QuestionGroupMeta
    {
        public int Order { get; set; }
        public int? ParentId { get; set; }
        public bool IsActive { get; set; }
        public string ConcurrencyStamp { get; set; }
        //public int TotalQuestion { get; set; }
        public List<QuestionGroupTranslationMeta> QuestionGroupTranslations { get; set; }
    }
}
