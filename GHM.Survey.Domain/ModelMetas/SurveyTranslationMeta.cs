﻿namespace GHM.Survey.Domain.ModelMetas
{
  public  class SurveyTranslationMeta
    {
        public string LanguageId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

    }
}
