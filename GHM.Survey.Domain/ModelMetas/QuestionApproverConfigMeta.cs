﻿namespace GHM.Survey.Domain.ModelMetas
{
    /// <summary>
    /// Model meta được gửi lên từ truy vấn của client.
    /// </summary>

    public class QuestionApproverConfigMeta
    {
        /// <summary>
        /// UserId
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// PositionId
        /// </summary>
        public string PositionId { get; set; }

        /// <summary>
        /// OfficeId
        /// </summary>
        public int OfficeId { get; set; }

        /// <summary>
        /// FullName
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Avatar
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// OfficeName
        /// </summary>
        public string OfficeName { get; set; }

        /// <summary>
        /// PositionName
        /// </summary>
        public string PositionName { get; set; }

    }

}
