﻿namespace GHM.Survey.Domain.ModelMetas
{
    public class UserAnswerMeta
    {
        public string SurveyId { get; set; }

        public string QuestionVersionId { get; set; }

        // Apply for single choice, multi choice, rating.
        public string AnswerId { get; set; }

        // Apply for self response and essay.
        public string SurveyUserAnswerId { get; set; }

        public string Value { get; set; }
    }
}
