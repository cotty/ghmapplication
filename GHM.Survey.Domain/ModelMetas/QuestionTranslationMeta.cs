﻿namespace GHM.Survey.Domain.ModelMetas
{
    /// <summary>
    /// Model meta được gửi lên từ truy vấn của client.
    /// </summary>

    public class QuestionTranslationMeta
    {
        /// <summary>
        /// LanguageId
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// QuestionVersionId
        /// </summary>
        public string QuestionVersionId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Content
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Explain
        /// </summary>
        public string Explain { get; set; }

    }
}
