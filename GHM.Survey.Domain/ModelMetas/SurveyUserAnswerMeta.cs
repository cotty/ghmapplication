﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Survey.Domain.ModelMetas
{
    /// <summary>
    /// Model meta được gửi lên từ truy vấn của client.
    /// </summary>

    public class SurveyUserAnswerMeta
    {

        /// <summary>
        /// SurveyId
        /// </summary>
        public string SurveyId { get; set; }

        /// <summary>
        /// QuestionVersionId
        /// </summary>
        public string QuestionVersionId { get; set; }

        /// <summary>
        /// AnswerId
        /// </summary>
        public string AnswerId { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }

    }

}
