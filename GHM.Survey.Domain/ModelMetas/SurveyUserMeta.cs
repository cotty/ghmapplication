﻿namespace GHM.Survey.Domain.ModelMetas
{
    public class SurveyUserMeta
    {
        public string UserId { get; set; }
    }
}
