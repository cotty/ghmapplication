﻿using GHM.Survey.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Survey.Domain.ModelMetas
{
    /// <summary>
    /// Model meta được gửi lên từ truy vấn của client.
    /// </summary>

    public class QuestionMeta
    {
        /// <summary>
        /// VersionId
        /// </summary>
        public string VersionId { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// QuestionGroupId
        /// </summary>
        public int QuestionGroupId { get; set; }

        /// <summary>
        /// IsActive
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// QuestionType
        /// </summary>
        public QuestionType Type { get; set; }

        /// <summary>
        /// Point
        /// </summary>
        public int? Point { get; set; }

        /// <summary>
        /// ConcurrencyStamp
        /// </summary>
        public string ConcurrencyStamp { get; set; }

        /// <summary>
        /// TotalAnswer
        /// </summary>
        public byte? TotalAnswer { get; set; }

        public List<QuestionTranslationMeta> Translations { get; set; }

        public List<AnswerMeta> Answers { get; set; }

    }

}
