﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Survey.Domain.ModelMetas
{
    /// <summary>
    /// Model meta được gửi lên từ truy vấn của client.
    /// </summary>

    public class AnswerMeta
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// QuestionId
        /// </summary>
        public string QuestionVersionId { get; set; }

        /// <summary>
        /// IsCorrect
        /// </summary>
        public bool IsCorrect { get; set; }

        /// <summary>
        /// Order
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// ConcurrencyStamp
        /// </summary>
        public string ConcurrencyStamp { get; set; }

        public List<AnswerTranslationMeta> Translations { get; set; }
    }

}
