﻿using System;
using System.Collections.Generic;
using GHM.Infrastructure.ModelBinders;
using GHM.Survey.Domain.Constants;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Survey.Domain.ModelMetas
{
    public class SurveyMeta
    {
        public int? SurveyGroupId { get; set; }
        public bool IsActive { get; set; }
        public bool IsPreRendering { get; set; }
        public bool IsRequire { get; set; }
        public int TotalQuestion { get; set; }
        public int LimitedTimes { get; set; }
        public int? LimitedTime { get; set; }
        public string ConcurrencyStamp { get; set; }
        public SurveyType Type { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<SurveyTranslationMeta> SurveyTranslations { get; set; }
        public List<string> Users { get; set; }
        //public List<string> Questions { get; set; }
        //public List<SurveyQuestionGroupMeta> QuestionGroups { get; set; }
    }
}
