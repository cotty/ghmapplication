﻿using System.Collections.Generic;
namespace GHM.Survey.Domain.ModelMetas
{
  public  class SurveyGroupMeta
    {
        public int Order { get; set; }
        public int? ParentId { get; set; }
        public bool IsActive { get; set; }
        public string ConcurrencyStamp { get; set; }
        public List<SurveyGroupTranslationMeta> SurveyGroupTranslations { get; set; }
    }
}
