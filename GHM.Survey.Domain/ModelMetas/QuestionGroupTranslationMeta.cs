﻿namespace GHM.Survey.Domain.ModelMetas
{
 public   class QuestionGroupTranslationMeta
    {
        public int QuestionGroupId { get; set; }
        public string LanguageId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
