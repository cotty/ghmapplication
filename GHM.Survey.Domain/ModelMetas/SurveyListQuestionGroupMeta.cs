﻿using System.Collections.Generic;

namespace GHM.Survey.Domain.ModelMetas
{
    public class SurveyListQuestionGroupMeta
    {
        public List<SurveyQuestionMeta> Questions { get; set; }
        public List<SurveyQuestionGroupMeta> QuestionGroups { get; set; }
    }
}
