﻿namespace GHM.Survey.Domain.Models
{
    public class SurveyUsersReport
    {
        public string SurveyId { get; set; }
        public string SurveyUserId { get; set; }
        public string SurveyUserAnswerTimesId { get; set; }
        public string UserId { get; set; }
        public string Avatar { get; set; }
        public string FullName { get; set; }
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string PositionId { get; set; }
        public string PositionName { get; set; }
        public int? TotalCorrectAnswers { get; set; }
        public decimal? TotalCorrectScores { get; set; }
        public decimal? TotalScores { get; set; }
        public int? TotalQuestions { get; set; }
        public int? TotalSeconds { get; set; }
        public int? TotalTimes { get; set; }
        public int? TotalUserAnswers { get; set; }
        public bool? IsViewResult { get; set; }
    }
}
