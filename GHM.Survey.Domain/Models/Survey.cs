﻿using System;
using System.Collections.Generic;
using GHM.Survey.Domain.Constants;

namespace GHM.Survey.Domain.Models
{
    public class Survey
    {
        public string Id { get; set; }

        /// <summary>
        /// Mã nhóm khảo sát.
        /// </summary>
        public int? SurveyGroupId { get; set; }

        /// <summary>
        /// Có public khảo sát ra bên ngoài.
        /// </summary>
        public bool IsPublishOutside { get; set; }

        /// <summary>
        /// Trạng thái dùng.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Tạo đề trước khi thi.
        /// </summary>
        public bool IsPreRendering { get; set; }

        /// <summary>
        /// Khảo sát bắt buộc.
        /// </summary>
        public bool IsRequire { get; set; }

        /// <summary>
        /// Tổng số câu hỏi.
        /// </summary>
        public int TotalQuestion { get; set; }


        /// <summary>
        /// Số người tham gia.
        /// </summary>
        public int TotalUser { get; set; }

        /// <summary>
        /// Trạng thái xóa.
        /// </summary>
        public bool IsDelete { get; set; }

        /// <summary>
        /// Mã khách hàng.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// số lần làm bài.
        /// </summary>
        public int LimitedTimes { get; set; }

        /// <summary>
        /// Thời gian làm bài.
        /// </summary>
        public int? LimitedTime { get; set; }

        /// <summary>
        /// Trạng thái.
        /// </summary>
        public SurveyStatus Status { get; set; }

        /// <summary>
        /// Mã người duyệt.
        /// </summary>
        public string ApproverUserId { get; set; }

        /// <summary>
        /// Tên người duyệt.
        /// </summary>
        public string ApproverFullName { get; set; }

        /// <summary>
        /// Thời gian gửi.
        /// </summary>
        public DateTime? SentTime { get; set; }

        /// <summary>
        /// Thời gian duyệt.
        /// </summary>
        public DateTime? ApprovedTime { get; set; }

        /// <summary>
        /// link public ra bên ngoài.
        /// </summary>
        public string SeoLink { get; set; }

        /// <summary>
        /// update.
        /// </summary>
        public string ConcurrencyStamp { get; set; }

        /// <summary>
        /// thời gian update gần nhất.
        /// </summary>
        public DateTime? LastUpdate { get; set; }

        /// <summary>
        /// mã người dùng update.
        /// </summary>
        public string LastUpdateUserId { get; set; }

        /// <summary>
        /// Tên người dùng update.
        /// </summary>
        public string LastUpdateFullName { get; set; }

        /// <summary>
        /// Mã người tạo.
        /// </summary>
        public string CreatorId { get; set; }

        /// <summary>
        /// Tên người tạo.
        /// </summary>
        public string CreatorFullName { get; set; }

        /// <summary>
        /// Thời gian tạo.
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// Thời gian bắt đầu.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Thời gian kết thúc.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// yêu cầu đăng nhập.
        /// </summary>
        public bool IsRequireLogin { get; set; }

        /// <summary>
        /// Loại khảo sát:
        // 0: Dạng đề trắc nghiệm thông thường.
        // 1: Dạng đề khảo sát có điều hướng.
        /// </summary>
        public SurveyType Type { get; set; }

        public Survey()
        {
            IsPublishOutside = false;
            Status = SurveyStatus.Approved;
            ApproverUserId = "admin";
            ApproverFullName = "admin";
            ApprovedTime = DateTime.Now;
            SentTime = DateTime.Now;
            SeoLink = string.Empty;
            IsDelete = false;
            IsActive = true;
            Type = SurveyType.Normal;
            CreateTime = DateTime.Now;
            IsRequireLogin = true;
            TotalUser = 0;
            IsPreRendering = false;
            LimitedTimes = 1;
        }

        public List<SurveyTranslation> SurveyTranslations { get; set; }
    }
}
