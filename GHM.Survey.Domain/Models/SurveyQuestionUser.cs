﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Survey.Domain.Models
{
    /// <summary>
    /// Model mapping với bảng trong database.
    /// </summary>

    public class SurveyQuestionUser
    {
        /// <summary>
        /// SurveyId
        /// </summary>
        public string SurveyId { get; set; }

        /// <summary>
        /// QuestionVersionId
        /// </summary>
        public string QuestionVersionId { get; set; }

        /// <summary>
        /// SurveyUserId
        /// </summary>
        public string SurveyUserId { get; set; }

        public string SurveyUserAnswerTimesId { get; set; }

        public bool? IsCorrect { get; set; }

        public SurveyQuestionUser()
        {
            SurveyId = "";
            QuestionVersionId = "";
            SurveyUserId = "";
        }
    }

}
