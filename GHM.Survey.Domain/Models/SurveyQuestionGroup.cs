﻿namespace GHM.Survey.Domain.Models
{
  public  class SurveyQuestionGroup
    {
        /// <summary>
        /// Mã khảo sát.
        /// </summary>
        public string SurveyId { get; set; }

        /// <summary>
        /// Mã nhóm câu hỏi.
        /// </summary>
        public int QuestionGroupId { get; set; }

        /// <summary>
        /// Tổng số câu hỏi.
        /// </summary>
        public int TotalQuestion { get; set; }

  
    }
}
