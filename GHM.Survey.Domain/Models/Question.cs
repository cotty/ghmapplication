﻿using GHM.Survey.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Survey.Domain.ModelMetas
{
    /// <summary>
    /// Model mapping với bảng trong database.
    /// </summary>

    public class Question 
    {
        /// <summary>
        /// VersionId
        /// </summary>
        public string VersionId { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// QuestionGroupId
        /// </summary>
        public int QuestionGroupId { get; set; }

        /// <summary>
        /// TenantId
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// ApproverUserId
        /// </summary>
        public string ApproverUserId { get; set; }

        /// <summary>
        /// CreatorId
        /// </summary>
        public string CreatorId { get; set; }

        /// <summary>
        /// IsActive
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// IsDelete
        /// </summary>
        public bool IsDelete { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public QuestionStatus Status { get; set; }

        /// <summary>
        /// ApprovedTime
        /// </summary>
        public DateTime? ApprovedTime { get; set; }

        /// <summary>
        /// ApproverFullName
        /// </summary>
        public string ApproverFullName { get; set; }

        /// <summary>
        /// QuestionType
        /// </summary>
        public QuestionType Type { get; set; }

        /// <summary>
        /// Point
        /// </summary>
        public int Point { get; set; }

        /// <summary>
        /// CreateTime
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// SentTime
        /// </summary>
        public DateTime? SentTime { get; set; }

        /// <summary>
        /// CreatorFullName
        /// </summary>
        public string CreatorFullName { get; set; }

        /// <summary>
        /// ConcurrencyStamp
        /// </summary>
        public string ConcurrencyStamp { get; set; }

        /// <summary>
        /// TotalAnswer
        /// </summary>
        public byte TotalAnswer { get; set; }

        /// <summary>
        /// ToDate
        /// </summary>
        public DateTime? ToDate { get; set; }

        public string DeclineReason { get; set; }

        public Question()
        {
            ConcurrencyStamp = Guid.NewGuid().ToString();
            //VersionId = "";
            //Id = "";
            //TenantId = "";
            //CreatorId = "";
            IsActive = false;
            IsDelete = false;
            CreateTime = DateTime.Now;
            CreatorFullName = "";
            ConcurrencyStamp = "";
            TotalAnswer = 0;
            Point = 0;
            DeclineReason = "";
        }
    }

}
