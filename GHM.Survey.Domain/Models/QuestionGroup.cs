﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Survey.Domain.Models
{
  public  class QuestionGroup
    {

        public int Id { get; set; }

        /// <summary>
        /// Mã cha
        /// </summary>
        public int? ParentId { get; set; }

        /// <summary>
        /// Thứ tự sắp xếp.
        /// </summary>
        public int Order { get; set; }
        
        /// <summary>
        /// Thứ tự sắp xếp theo cây thư mục.
        /// </summary>
        public string OrderPath { get; set; }

        /// <summary>
        /// Trạng thái sử dụng.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Trạng thái xóa
        /// </summary>
        public bool IsDelete { get; set; }

        /// <summary>
        /// Mã khách hàng.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Số lượng câu hỏi.
        /// </summary>
        public int TotalQuestion { get; set; }

        /// <summary>
        /// IdPath 
        /// </summary>
        public string IdPath { get; set; }

        /// <summary>
        /// Phục vụ update
        /// </summary>
        public string ConcurrencyStamp { get; set; }


        /// <summary>
        /// thời gian tạo 
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// Số lượng  con.
        /// </summary>
        public int ChildCount { get; set; }

        /// <summary>
        /// Mã người tạo.
        /// </summary>
        public string CreatorId { get; set; }

        /// <summary>
        /// Tên người tạo.
        /// </summary>
        public string CreatorFullName { get; set; }

        /// <summary>
        /// thời gian update.
        /// </summary>
        public DateTime? LastUpdate { get; set; }

        /// <summary>
        /// Mã người update.
        /// </summary>
        public string LastUpdateUserId { get; set; }

        /// <summary>
        /// Tên người update.
        /// </summary>
        public string LastUpdateFullName { get; set; }

        public QuestionGroup()
        {
            Order = 0;
            IsDelete = false;
            IsActive = true;
            ChildCount = 0;
            CreateTime = DateTime.Now;
            ConcurrencyStamp = Guid.NewGuid().ToString();
        }

        public List<QuestionGroupTranslation> QuestionGroupTranslations { get; set; }


    }
}
