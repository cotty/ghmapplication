﻿namespace GHM.Survey.Domain.Models
{
   public class SurveyQuestion
    {
        /// <summary>
        /// Mã khảo sát.
        /// </summary>
        public string SurveyId { get; set; }

        /// <summary>
        /// Mã câu hỏi.
        /// </summary>
        public string QuestionVersionId { get; set; }

        /// <summary>
        /// Điểm số.
        /// </summary>
        public decimal? Point { get; set; }

        /// <summary>
        /// Thứ tự câu hỏi.
        /// </summary>
        public int Order { get; set; }
    }
}
