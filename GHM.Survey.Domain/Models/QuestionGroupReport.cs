﻿namespace GHM.Survey.Domain.Models
{
  public  class QuestionGroupReport
    {
        public int QuestionGroupId { get; set; }
        public string QuestionGroupName { get; set; }
        public int TotalQuestions { get; set; }
        public int TotalAnswers { get; set; }
        public int TotalCorrectAnswers { get; set; }
        public double CorrectPercent { get; set; }
    }
}
