﻿namespace GHM.Survey.Domain.Models
{
    public class SurveyTranslation
    {
        /// <summary>
        /// Mã khảo sát.
        /// </summary>
        public string SurveyId { get; set; }

        /// <summary>
        /// Mã ngôn ngữ.
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// Tên khảo sát.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Mô tả.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Tên không dấu.
        /// </summary>
        public string UnsignName { get; set; }

        /// <summary>
        ///  Tên nhóm khảo sát.
        /// </summary>
        /// 
        public string SurveyGroupName { get; set; }

        /// <summary>
        ///  Đối tượng quan hệ tham chiếu phục vụ foreign key.
        /// </summary>
        public Survey Survey { get; set; }
    }
}
