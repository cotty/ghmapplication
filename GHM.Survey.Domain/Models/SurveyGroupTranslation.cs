﻿namespace GHM.Survey.Domain.Models
{
  public  class SurveyGroupTranslation
    {
        /// <summary>
        /// Mã khách hàng.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Mã nhóm khảo sát.
        /// </summary>
        public int SurveyGroupId { get; set; }

        /// <summary>
        /// Mã ngôn ngữ.
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// Tên ngôn ngữ.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Mô tả 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Tên không dấu để tìm kiếm
        /// </summary>
        public string UnsignName { get; set; }

        /// <summary>
        /// Tên nhóm câu hỏi cha
        /// </summary>
        public string ParentName { get; set; }

        /// <summary>
        ///  Đối tượng quan hệ tham chiếu phục vụ foreign key.
        /// </summary>
        public SurveyGroup SurveyGroup { get; set; }
    }
}
