﻿using GHM.Survey.Domain.Constants;
using System;

namespace GHM.Survey.Domain.Models
{
    /// <summary>
    /// Model mapping với bảng trong database.
    /// </summary>

    public class SurveyUserAnswerTime
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// SurveyId
        /// </summary>
        public string SurveyId { get; set; }

        /// <summary>
        /// SurveyUserId
        /// </summary>
        public string SurveyUserId { get; set; }

        /// <summary>
        /// StartTime
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// EndTime
        /// </summary>
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// Tổng số câu người dùng trả lời.
        /// </summary>
        public int TotalUserAnswers { get; set; }

        /// <summary>
        /// TotalCorrectAnswers
        /// </summary>
        public int TotalCorrectAnswers { get; set; }

        /// <summary>
        /// TotalCorrectScores
        /// </summary>
        public decimal TotalCorrectScores { get; set; }

        /// <summary>
        /// TotalScores
        /// </summary>
        public decimal TotalScores { get; set; }

        /// <summary>
        /// TotalSeconds
        /// </summary>
        public int? TotalSeconds { get; set; }

        /// <summary>
        /// Tổng số câu hỏi.
        /// </summary>
        public int TotalQuestions { get; set; }

        public bool? IsViewResult { get; set; }

        public SurveyUserAnswerTime()
        {
            Id = "";
            SurveyId = "";
            SurveyUserId = "";
            StartTime = DateTime.Now;
            IsViewResult = false;
        }
    }

}
