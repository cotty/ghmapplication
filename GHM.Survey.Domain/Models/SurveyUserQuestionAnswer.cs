﻿using GHM.Survey.Domain.Constants;

namespace GHM.Survey.Domain.Models
{
    /// <summary>
    /// Table not mapped to database. Just result of join query for get user answer.
    /// </summary>
    public class SurveyUserQuestionAnswer
    {
        public string TimeId { get; set; }
        public string QuestionVersionId { get; set; }
        public QuestionType QuestionType { get; set; }
        public string QuestionContent { get; set; }
        public int QuestionOrder { get; set; }
        public byte TotalAnswer { get; set; }
        public string QuestionName { get; set; }
        public string AnswerId { get; set; }
        public string AnswerName { get; set; }
        public string AnswerValue { get; set; }
        public int? AnswerOrder { get; set; }
        public string SurveyUserAnswerId { get; set; }
        public bool IsSelected { get; set; }
        public string AnswerIdUserSelected { get; set; }
        public string ToQuestionVersionId { get; set; }
        public int? Order { get; set; }
    }
}
