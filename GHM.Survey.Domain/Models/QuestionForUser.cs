﻿using System;
using System.Collections.Generic;
using System.Text;
using GHM.Survey.Domain.Constants;

namespace GHM.Survey.Domain.Models
{
    /// <summary>
    /// Model mapping với bảng trong database.
    /// </summary>

    public class QuestionForUser
    {
        /// <summary>
        /// VersionId
        /// </summary>
        public string VersionId { get; set; }

        /// <summary>
        /// QuestionType
        /// </summary>
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// LanguageId
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Content
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Explain
        /// </summary>
        public string Explain { get; set; }

        /// <summary>
        /// TotalAnswer
        /// </summary>
        public int? TotalAnswer { get; set; }

        public int Point { get; set; }
        /// <summary>
        /// AnswerId
        /// </summary>
        public string AnswerId { get; set; }
        /// <summary>
        /// Answer_Name
        /// </summary>
        public string AnswerName { get; set; }
        /// <summary>
        /// Answer_Explain
        /// </summary>        
        public string AnswerExplain { get; set; }
        public bool AnswerIsSelect { get; set; }
        public int? AnswerOrder { get; set; }
        public string AnswerValue { get; set; }
        public string SurveyUserAnswerId { get; set; }
    }
}
