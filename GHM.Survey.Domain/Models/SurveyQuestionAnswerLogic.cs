﻿namespace GHM.Survey.Domain.Models
{
    public class SurveyQuestionAnswerLogic
    {
        public string SurveyId { get; set; }
        public string QuestionVersionId { get; set; }
        public string AnswerId { get; set; }
        public string ToQuestionVersionId { get; set; }
    }
}
