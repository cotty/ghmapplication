﻿using System;

namespace GHM.Survey.Domain.Models
{
    public class SurveyReport
    {
        public string SurveyId { get; set; }
        public string SurveyName { get; set; }
        public int TotalUsers { get; set; }
        public int TotalParticipants { get; set; }
        public int TotalFinished { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
