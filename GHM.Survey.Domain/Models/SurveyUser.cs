﻿namespace GHM.Survey.Domain.Models
{
  public  class SurveyUser
    {
        /// <summary>
        /// Mã người dùng khảo sát.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Mã khảo sát.
        /// </summary>
        public string SurveyId { get; set; }

        /// <summary>
        /// Mã người dùng.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// ảnh đại diện.
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// Họ và tên.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Mã phòng ban.
        /// </summary>
        public int OfficeId { get; set; }

        /// <summary>
        /// Tên phòng ban.
        /// </summary>
        public string OfficeName { get; set; }

        /// <summary>
        /// Mã chức danh.
        /// </summary>
        public string PositionId { get; set; }

        /// <summary>
        /// Tên chức danh.
        /// </summary>
        public string PositionName { get; set; }
    }
}
