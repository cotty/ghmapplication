﻿namespace GHM.Survey.Domain.Models
{
    /// <summary>
    /// Model mapping với bảng trong database.
    /// </summary>

    public class QuestionApproverConfig
    {
        /// <summary>
        /// Mã khách hàng.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Mã người phê duyệt.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Mã chức vụ.
        /// </summary>
        public string PositionId { get; set; }

        /// <summary>
        /// Mã phòng ban.
        /// </summary>
        public int OfficeId { get; set; }

        /// <summary>
        /// Họ tên người phê duyệt.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// UnsignName
        /// </summary>
        public string UnsignName { get; set; }

        /// <summary>
        /// Ảnh đại diện người phê duyệt.
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// Tên phòng ban.
        /// </summary>
        public string OfficeName { get; set; }

        /// <summary>
        /// Tên chức vụ.
        /// </summary>
        public string PositionName { get; set; }

    }


}
