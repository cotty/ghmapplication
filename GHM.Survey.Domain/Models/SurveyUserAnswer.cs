﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Survey.Domain.Models
{
    /// <summary>
    /// Model mapping với bảng trong database.
    /// </summary>

    public class SurveyUserAnswer
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// SurveyId
        /// </summary>
        public string SurveyId { get; set; }

        /// <summary>
        /// SurveyUserAnswerTimesId
        /// </summary>
        public string SurveyUserAnswerTimesId { get; set; }

        /// <summary>
        /// SurveyUserId
        /// </summary>
        public string SurveyUserId { get; set; }

        /// <summary>
        /// Mã nhóm câu hỏi khảo sát.
        /// </summary>
        public int QuestionGroupId { get; set; }

        /// <summary>
        /// QuestionVersionId
        /// </summary>
        public string QuestionVersionId { get; set; }

        /// <summary>
        /// AnswerId
        /// </summary>
        public string AnswerId { get; set; }

        /// <summary>
        /// AnswerTime
        /// </summary>
        public DateTime AnswerTime { get; set; }

        /// <summary>
        /// IsCorrect
        /// </summary>
        public bool IsCorrect { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }

        public SurveyUserAnswer()
        {
            Id = Guid.NewGuid().ToString();
            SurveyId = "";
            SurveyUserAnswerTimesId = "";
            SurveyUserId = "";
            QuestionVersionId = "";
            AnswerTime = DateTime.Now;
            IsCorrect = false;
        }
    }

}
