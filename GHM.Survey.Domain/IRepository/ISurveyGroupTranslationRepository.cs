﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;

namespace GHM.Survey.Domain.IRepository
{
    public interface ISurveyGroupTranslationRepository
    {
        Task<string> GetSurveyGroupName(string tenantId, int surveyGroupId, string languageId);

        Task<int> Insert(SurveyGroupTranslation surveyGroupTranslation);

        Task<int> Update(SurveyGroupTranslation surveyGroupTranslation);

        Task<int> Inserts(List<SurveyGroupTranslation> surveyGroupTranslations);

        Task<int> ForceDeleteBySurveyGroupId(string tenantId, int surveyGroupId);

        Task<bool> CheckExistsByName(int surveyGroupId, string tenantId, string languageId, string name);

        Task<SurveyGroupTranslation> GetInfo(string tenantId, int surveyGroupId, string languageId, bool isReadonly = false);

        Task<List<SurveyGroupTranslationViewModel>> GetsBySurveyGroupId(string tenantId, int surveyGroupId);
    }
}

