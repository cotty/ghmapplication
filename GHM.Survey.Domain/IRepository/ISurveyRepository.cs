﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
namespace GHM.Survey.Domain.IRepository
{
    public interface ISurveyRepository
    {
        Task<List<SurveyViewModel>> Search(string tenantId, string languageId, string keyword, int? surveyGroupId,
            DateTime? startDate, DateTime? endDate, bool? isActive, int page, int pageSize, out int totalRows);

        Task<bool> CheckSurveyExistsByTenantId(string tenantId, string surveyId);

        Task<int> Insert(Models.Survey survey);

        Task<int> Update(Models.Survey survey);

        Task<int> Delete(string surveyId);

        Task<int> ForceDelete(string surveyId);

        Task<int> UpdateTotalUser(string surveyId, int totalUser);

        Task<int> UpdateTotalQuestion(string surveyId, int totalQuestion);

        Task<Models.Survey> GetInfo(string id, bool isReadonly = false);

        Task<Models.Survey> GetInfo(string tenantId, string id, bool isReadonly = false);

        Task<bool> CheckExistsBySurveyGroupId(int surveyGroupId);

        Task<List<SurveyQuestionViewModel>> GetListSurveyQuestions(string surveyId, string languageId);

        Task<List<SurveyQuestionGroupViewModel>> GetListSurveyQuestionGroups(string surveyId, string languageId);

        Task<List<SurveyUserQuestionAnswer>> GetSurveyUserQuestionAnswer(string tenantId, string languageId, string surveyId,
            string surveyUserId, string timesId, bool isPrerendering);

        Task<List<SurveyUserQuestionAnswer>> GetSurveyUserQuestionLogicAnswer(string languageId, string surveyId,
            string surveyUserId, string timesId);

        List<SurveyReport> SearchReport(string tenantId, string languageId, string keyword, int? surveyGroupId, DateTime? startDate,
            DateTime? endDate, int page, int pageSize, out int totalRows);

        Task<List<SurveyUserQuestionAnswerDetailViewModel>> GetSurveyUserQuestionAnswerDetail(string tenantId, string languageId,
            string surveyId, string surveyUserId, string timesId, bool isPrerendering, bool isManager);

        Task<List<SurveyUserQuestionAnswerDetailViewModel>> GetSurveyUserQuestionLogicAnswerDetail(string tenantId, string languageId,
           string surveyId, string surveyUserId, string timesId);

        Task<List<SurveyInfoViewModel>> UserGetTopSurvey(string tenantId, string languageId, string userId, int top);
    }
}
