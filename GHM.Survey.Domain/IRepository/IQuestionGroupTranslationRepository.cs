﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;

namespace GHM.Survey.Domain.IRepository
{
    public interface IQuestionGroupTranslationRepository
    {
        Task<string> GetQuestionGroupName(int questonGroupId, string languageId);

        Task<int> Insert(QuestionGroupTranslation questionGroupTranslation);

        Task<int> Update(QuestionGroupTranslation questionGroupTranslation);

        Task<int> Inserts(List<QuestionGroupTranslation> questionGroupTranslations);

        Task<int> ForceDeleteByQuestionGroupId(int questionGroupId);

        Task<bool> CheckExistsByName(int questionGroupId, string tenantId, string languageId, string name);

        Task<QuestionGroupTranslation> GetInfo(int questionGroupId, string languageId, bool isReadonly = false);

        Task<List<QuestionGroupTranslationViewModel>> GetsByQuestionGroupId(int questionGroupId);
    }
}

