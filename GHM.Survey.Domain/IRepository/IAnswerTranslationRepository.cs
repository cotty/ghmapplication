﻿using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Survey.Domain.IRepository
{
    public interface IAnswerTranslationRepository
    {

        Task<int> Insert(AnswerTranslation answertranslation);

        Task<int> Inserts(List<AnswerTranslation> answertranslations);

        Task<int> Update(AnswerTranslation answertranslation);

        Task<int> ForceDeleteByAnswerId(string answerId);

        //Task<int> Delete(string answertranslationId);

        //Task<int> ForceDelete(string answertranslationId);

        //Task<AnswerTranslation> GetInfo(string answertranslationId, string tenantId, string languageId, bool isReadOnly = false);

        //Task<bool> CheckExistsByAnswerTranslationId(string answertranslationId, string tenantId, bool isReadOnly = false);



    }
}
