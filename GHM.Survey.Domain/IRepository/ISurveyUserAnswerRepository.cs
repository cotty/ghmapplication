﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GHM.Survey.Domain.Models;

namespace GHM.Survey.Domain.IRepository
{
    public interface ISurveyUserAnswerRepository
    {        
        Task<bool> CheckExists(string surveyId, string questionVersionId, string surveyUserId, string surveryUserAnswerTimeId);

        Task<bool> CheckExists(string surveyId, string questionVersionId, string surveyUserId, string answerId, string surveryUserAnswerTimeId);

        Task<int> Insert(SurveyUserAnswer surveyuseranswer);

        Task<int> Update(SurveyUserAnswer surveyuseranswer);

        Task<int> ForceDelete(string surveyUserAnswersId);

        Task<int> Delete(string surveyId, string questionVersionId, string surveyUserId, string answerId, string surveryUserAnswerTimeId);

        Task<int> Delete(string surveyId, string questionVersionId, string surveyUserId, string surveryUserAnswerTimeId);

        Task<int> ForceDeletes(string surveyUserId, string surveyUserAnswerTimesId);

        Task<SurveyUserAnswer> GetInfo(string surveyUserAnswersId, bool isReadOnly = false);

        Task<SurveyUserAnswer> GetInfo(string surveyId, string questionVersionId, string surveyUserId, string surveryUserAnswerTimeId);

        Task<List<SurveyUserAnswer>> GetInfos(string surveyUserAnswerTimesId, string surveyUserId,
            bool isReadOnly = false);

        Task<int> GetTotalAnswer(string surveyId, string questionVersionId, string surveyUserId, string surveryUserAnswerTimeId, bool? isCorrect);

        /// <summary>
        /// Danh sách câu hỏi và các phương án trả lời cho người tham gia tạo đề khi thi
        /// </summary>
        /// <param name="tenantId"></param>
        /// <param name="surveyId"></param>
        /// <param name="surveyUserId"></param>
        /// <param name="languageId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        Task<List<QuestionForUser>> GetQuestionForUserInSurvey(string tenantId, string surveyId, string surveyUserId,
                string languageId, int page, int pageSize, out int totalRows);
       
    }
}
