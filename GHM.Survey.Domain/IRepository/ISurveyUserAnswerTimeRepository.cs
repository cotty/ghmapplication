﻿using GHM.Survey.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GHM.Survey.Domain.ViewModels;

namespace GHM.Survey.Domain.IRepository
{
    public interface ISurveyUserAnswerTimeRepository
    {
        Task<int> Insert(SurveyUserAnswerTime surveyUserAnswerTime);

        Task<bool> CheckExistsBySurveyId(string surveyId);

        Task<bool> CheckIsActive(string surveyId, string surveyUserId);

        Task<int> ForceDelete(string surveyUserAnswerTimesId);

        Task<SurveyUserAnswerTime> GetInfo(string surveyUserAnswertimesId, bool isReadOnly = false);

        Task<SurveyUserAnswerTime> GetCurrentActiveSurveyTimes(string surveyId, string surveyUserId, bool isReadOnly = false);

        Task<List<string>> GetUserIdsBySurveyId(string surveyId);

        //Task<int> GetSurveyTime(string surveyUserAnswerTimesId);

        Task<bool> CheckExistsBySurveyIdAndSurveyUserId(string surveyId, string surveyUserId);

        Task<SurveyUserAnswerTime> GetInfoBySurveyIdAndSurveyUserId(string surveyId, string surveyUserId, bool isReadOnly = false);

        Task<int> SurveyUserAnswerTimesIsPreRendering(string surveyId, string surveyUsersId);

        Task<int> FinishDoExam(string surveyId, string surveyUserId);

        Task<int> GetTotalSurveyUserAnswerTimes(string surveyId, string surveyUserId);

        Task<List<SurveyUserAnswerTime>> GetBySurveyIdAndUserId(string surveyId, string surveyUserId);

        Task<List<SurveyUserAnswerTime>> GetByListSurveyUserTimesIds(List<string> surveyUserAnswerTimesIds);
        
        Task<int> UpdateTotalAnswer(string surveyId, string surveyUserId, string surveyUserAnswerTimesId);

        Task<List<SurveyUserAnswerTimeViewModel>> GetListSurveyUserAnswerTimes(string surveyId, string surveyUserId);

        Task<List<ReportSurveyUserAnswerTimesViewModel>> SearchReportBySurveyIdAndSurveyUserId(string surveyId, string surveyUserId);

        Task<int> Update(SurveyUserAnswerTime surveyUserAnswerTime);
    }
}
