﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
namespace GHM.Survey.Domain.IRepository
{
    public interface ISurveyGroupRepository
    {
        Task<bool> CheckExists(int id);

        Task<int> Insert(SurveyGroup surveyGroup);

        Task<int> Update(SurveyGroup surveyGroup);

        Task<int> UpdateSurveyGroupIdPath(int id, string idPath);

        Task<int> UpdateChildCount(int id, int childCount);

        Task<int> UpdateChildrenIdPath(string oldIdPath, string newIdPath);

        Task<int> Delete(int surveyGroupId);

        Task<int> ForceDelete(int surveyGroupId);

        Task<SurveyGroup> GetInfo(int surveyGroupId, bool isReadOnly = false);

        Task<SurveyGroup> GetInfo(string surveyGroupIdPath, bool isReadOnly = false);

        Task<int> GetChildCount(int id);

        Task<List<SurveyGroupSearchViewModel>> GetAllActivatedSurveyGroup(string tenantId, string languageId);

        Task<List<SurveyGroupSearchViewModel>> GetActivedSurveyGroup(string tenantId, string languageId);

        Task<List<SurveyGroupForSelectViewModel>> GetAllSurveyGroupForSelect(string tenantId, string languageId, string keyword, int page, int pageSize, out int totalRows);

        Task<List<SurveyGroupSearchViewModel>> SearchSurveyGroup(string tenantId, string languageId, string keyword, bool? isActive, int page, int pageSize, out int totalRows);

        Task<List<SurveyGroupSearchViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive, int page, int pageSize, out int totalRows);

    }
}
