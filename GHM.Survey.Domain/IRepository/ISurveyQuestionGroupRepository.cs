﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
namespace GHM.Survey.Domain.IRepository
{
    public interface ISurveyQuestionGroupRepository
    {
        Task<int> Insert(SurveyQuestionGroup surveyQuestionGroup);
        Task<int> Update(SurveyQuestionGroup surveyQuestionGroup);
        Task<int> Delete(string surveyId, int questionGroupId);
        Task<int> DeleteBysurveyId(string surveyId);
        Task<int> Inserts(List<SurveyQuestionGroup> surveyQuestionGroups);
        Task<int> Deletes(List<SurveyQuestionGroup> surveyQuestionGroups);
        Task<List<SurveyQuestionGroupViewModel>> GetInfo(string surveyId, int questionGroupId, bool isReadOnly = false);
        Task<List<SurveyQuestionGroupViewModel>> GetsBySurveyId(string surveyId, string languageId);
        Task<bool> CheckExistsBysurveyIdQuestionGroupId(string surveyId, int questionGroupId);
    }
}
