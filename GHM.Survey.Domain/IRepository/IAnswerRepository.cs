﻿using GHM.Survey.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Survey.Domain.ViewModels;

namespace GHM.Survey.Domain.IRepository
{
    public interface IAnswerRepository
    {
        Task<int> Insert(Answer answer);

        Task<int> Insert(List<Answer> answers);

        Task<int> Update(Answer answer);

        Task<int> Delete(string answerId);

        Task<int> ForceDeleteByQuestionVersionId(string answerId);

        Task<Answer> GetInfo(string answerId);

        Task<AnswerDetailByLanguageViewModel> GetInfo(string questionId, string answerId, string languageId);

        Task<List<Answer>> GetsByQuestionVersionId(string questionVersionId);

        Task<List<AnswerTranslation>> GetsAnswerTranslationByAnswerId(string answerId);

        Task<int> TotalAnswer(string questionVersionId, bool? isCorrect);

        Task<List<AnswerSuggestionViewModel>> GetsByQuestionVersionIds(List<string> questionVersionIds, string languageId);
    }

}
