﻿using GHM.Survey.Domain.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Survey.Domain.IRepository
{
    public interface ISurveyQuestionUserRepository
    {
        /// <summary>
        /// Random câu hỏi khi người dùng đăng nhập tham gia vài kiểm tra
        /// </summary>
        /// <param name="surveyId">Mã đợt khảo sát</param>
        /// <param name="surveyUserId">Mã người tham gia</param>
        /// <returns></returns>
        Task<int> RanDomQuestion(string surveyId, string userId, string surveyUserAnswerTimesId);

        /// <summary>
        /// Kiểm tra tồn tại
        /// </summary>
        /// <param name="questionVersionId"></param>
        /// <returns></returns>
        Task<bool> CheckByQuestionVersionId(string questionVersionId);

        Task<bool> CheckBySurveyUserIdAndsurveyId(string surveyUserId, string surveyId);

        Task<List<SurveyQuestionUser>> GetInfos(string surveyUserId, string surveyId, bool isReadOnly = false);

        Task<int> ForceDelete(string surveyUserId, string surveyId);

        Task<SurveyQuestionUser> GetInfo(string surveyUserId, string surveyId, string surveyUserAnswerTimesId, string questionVersionId, bool isReadOnly = false);

        Task<int> UpdateIsCorrect(string surveyUserId, string surveyId, string surveyUserAnswerTimesId, string questionVersionId, bool isCorrect);
    }
}
