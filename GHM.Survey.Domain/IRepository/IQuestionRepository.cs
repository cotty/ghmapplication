﻿using GHM.Survey.Domain.Constants;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Survey.Domain.IRepository
{
    public interface IQuestionRepository
    {
        Task<List<QuestionViewModel>> Search(string tenantId, string languageId, string keyword,
               QuestionType? questionType, int? questionGroupId, string creatorId, string currentUserId, QuestionStatus? questionStatus,
               int page, int pageSize, out int totalRows);

        Task<List<QuestionViewModel>> SearchForApprove(string tenantId, string languageId, string keyword,
            QuestionType? questionType, int? questionGroupId, string creatorId, int page, int pageSize, out int totalRows);

        Task<int> Insert(Question question);

        Task<int> Update(Question question);

        Task<int> Delete(string questionVersionId);

        Task<int> UpdateDeclineReason(string questionVersionId, string declineReason);

        Task<int> ForceDelete(string tenantId, string versionId, string questionId);

        Task<Question> GetInfo(string tenantId, string questionId, string versionId, bool isReadOnly = false);

        Task<Question> GetInfo(string versionId, bool isReadOnly = false);

        Task<Question> GetInfo(string tenantId, string questionId);

        Task<List<Question>> GetsInfo(string tenantId, List<string> questionIds);

        Task<Question> GetInfoRecord(string versionId);

        Task<bool> CheckExistsByQuestionId(string questionId, string tenantId, bool isReadOnly = false);

        Task<bool> CheckExistsByQuestionName(string name, string languageId);

        Task<bool> CheckExistsByQuestionGroupId(int questionGroupId);

        Task<List<Question>> GetAllQuestionByGroupQuestionId(int questionGroupId);

        Task<int> GetCountQuestionByGroupQuestionId(int questionGroupId);


        /// <summary>
        /// Danh sách câu hỏi theo người duyệt
        /// </summary>
        /// <param name="approverUserId"></param>
        /// <returns></returns>
       // Task<List<Question>> GetAllQuestionByApproverUserId(string tenantId, string approverUserId);

        /// <summary>
        /// Danh sách câu hỏi theo người duyệt
        /// </summary>
        /// <param name="approverUserId"></param>
        /// <returns></returns>
        Task<List<QuestionViewModel>> GetAllQuestionByApproverUserId(string tenantId, string approverUserId, string languageId);

        Task<List<QuestionSuggestionViewModel>> SearchForSuggestion(string tenantId, string languageId, string keyword, int? questionGroupId,
            QuestionType? type, SurveyType? surveyType, int page, int pageSize, out int totalRows);

        Task<int> ChangeQuestionStatus(string questionVersionId, QuestionStatus questionStatus);
    }

}
