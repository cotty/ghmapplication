﻿using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Survey.Domain.IRepository
{
    public interface IQuestionTranslationRepository
    {

        Task<int> Insert(QuestionTranslation questiontranslation);

        Task<int> Inserts(List<QuestionTranslation> questiontranslations);

        Task<int> Update(QuestionTranslation questiontranslation);

        Task<QuestionTranslation> GetInfo(string tenantId, string languageId, string questionVersionId, bool isReadOnly = false);

        Task<List<QuestionTranslation>> GetsByQuestionVersionId(string versionId);

        Task<int> UpdateQuestionGroupName(string tenantId, string languageId, int questionGroupId, string questionGroupName);

        Task<int> ForceDeleteByQuestionVersionId(string questionVersionId);

    }

}
