﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
namespace GHM.Survey.Domain.IRepository
{
    public interface ISurveyQuestionRepository
    {
        Task<int> Insert(SurveyQuestion surveyQuestion);
        Task<int> Delete(string surveyId, string questionVersionId);
        Task<int> DeleteBysurveyId(string surveyId);
        Task<int> Inserts(List<SurveyQuestion> surveyQuestions);
        Task<int> Deletes(List<SurveyQuestion> surveyQuestions);
        Task<bool> CheckExistsBySurveyIdQuestionVersionId(string surveyId, string questionVersionId);
        Task<int> GetTotalQuestion(string surveyId);
        Task<List<SurveyQuestion>> GetListSurveyQuestions(string surveyId, bool isReadOnly = false);
        List<SurveyQuestionSearchViewModel> GetSurveyQuestions(string surveyId, string languageId);
    }
}
