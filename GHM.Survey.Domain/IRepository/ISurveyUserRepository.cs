﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;

namespace GHM.Survey.Domain.IRepository
{
    public interface ISurveyUserRepository
    {
        Task<int> Insert(SurveyUser surveyUser);

        Task<int> Update(SurveyUser surveyUser);

        Task<int> Delete(string surveyId, string userId);

        Task<int> Deletes(string surveyId, List<string> userIds);

        Task<int> DeletebysurveyId(string surveyId);

        Task<int> Inserts(List<SurveyUser> surveyUsers);

        Task<int> Deletes(List<SurveyUser> surveyUsers);

        Task<SurveyUser> GetInfo(string id, bool isReadOnly = false);

        Task<SurveyUser> GetInfo(string surveyId, string userId, bool isReadOnly = false);

        Task<List<SurveyUser>> GetListSurveyUser(string surveyId, bool isReadOnly = false);

        Task<bool> CheckExistsBySurveyIdUserId(string surveyId, string userId);

        Task<int> CountSurveyUser(string surveyId);

        Task<SurveyUser> GetInfoByUserId(string surveyId, string userId, bool isReadOnly = false);

        List<SurveyUsersReport> SearchSurveyUsersReport(string surveyId, string keyword, int page, int pageSize, out int totalRows);
        Task<List<SurveyUserReportViewModel>> GetListSurveyByUserId(string tenantId, string languageId, string userId, string keyWord, int? surveyGroupId,
            DateTime? startDate, DateTime? endDate, int page, int pageSize, out int totalRows);
    }
}
