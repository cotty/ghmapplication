﻿using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Survey.Domain.IRepository
{
    public interface IQuestionApproverConfigRepository
    {
        Task<List<QuestionApproverConfigSearchViewModel>> Search(string tenantId, string keyword, int page, int pageSize,
            out int totalRows);

        Task<int> Insert(QuestionApproverConfig questionapproverconfig);

        Task<int> Update(QuestionApproverConfig questionapproverconfig);

        Task<int> ForceDelete(string tenantId, string userId);

        Task<QuestionApproverConfig> GetInfo(string tenantId, string userId);

        Task<bool> CheckExistsUserId(string tenantId, string userId);

        Task<IReadOnlyCollection<QuestionApproverConfig>> GetAllApprover(string teanantId);

    }

}
