﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
namespace GHM.Survey.Domain.IRepository
{
    public interface ISurveyTranslationRepository
    {
        Task<int> Insert(SurveyTranslation surveyTranslation);

        Task<int> Update(SurveyTranslation surveyTranslation);

        Task<int> Inserts(List<SurveyTranslation> surveyTranslations);

        Task<int> Delete(string surveyId);

        Task<SurveyTranslation> GetInfo(string surveyId, string languageId, bool isReadOnly = false);        

        Task<string> GetSurveyName(string surveyId, string languageId);

        Task<List<SurveyTranslationViewModel>> GetsBySurveyId(string surveyId);

        Task<bool> CheckExists(string surveyId, string languageId, string name);

        Task<int> UpdateSurveyGroupName(string tenantId, string languageId, int surveyGroupId, string surveyGroupName);
    }
}
