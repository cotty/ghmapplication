﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
namespace GHM.Survey.Domain.IRepository
{
    public interface IQuestionGroupRepository
    {
        Task<bool> CheckExists(int id);

        Task<int> Insert(QuestionGroup questionGroup);

        Task<int> Update(QuestionGroup questionGroup);

        Task<int> UpdateQuestionGroupIdPath(int id, string idPath);

        Task<int> UpdateChildCount(int id, int childCount);

        Task<int> UpdateChildrenIdPath(string oldIdPath, string newIdPath);

        Task<int> UpdateTotalQuestion(int id, int totalQuestion);

        Task<int> Delete(int questionGroupId);

        Task<int> ForceDelete(int questionGroupId);

        Task<QuestionGroup> GetInfo(int questionGroupId, bool isReadOnly = false);

        Task<QuestionGroup> GetInfo(string questionGroupIdPath, bool isReadOnly = false);

        Task<int> GetChildCount(int id);

        Task<List<QuestionGroupSearchViewModel>> GetAllActivatedQuestionGroup(string tenantId, string languageId);

        Task<List<QuestionGroupSearchViewModel>> GetActivedQuestionGroup(string tenantId, string languageId);

        Task<List<QuestionGroupForSelectViewModel>> GetAllQuestionGroupForSelect(string tenantId, string languageId, string keyword, int page, int pageSize, out int totalRows);

        Task<List<QuestionGroupSearchViewModel>> SearchQuestionGroup(string tenantId, string languageId, string keyword, bool? isActive, int page, int pageSize, out int totalRows);

        Task<List<QuestionGroupSearchViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive, int page, int pageSize, out int totalRows);

        Task<List<QuestionGroupReport>> SearchReport(string surveyId, string surveyUserAnswerTimesId, string languageId);
    }
}
