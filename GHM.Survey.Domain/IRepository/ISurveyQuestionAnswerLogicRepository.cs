﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;

namespace GHM.Survey.Domain.IRepository
{
    public interface ISurveyQuestionAnswerLogicRepository
    {
        Task<List<SurveyQuestionAnswerLogicViewModel>> GetsBySurveyId(string surveyId, string languageId);

        Task<int> Inserts(List<SurveyQuestionAnswerLogic> listSurveyQuestionAnswerLogics);

        Task<int> DeleteBySurveyId(string surveyId);
    }
}
