﻿using System;

namespace GHM.Survey.Domain.ViewModels
{
    public class ReportSurveyUserAnswerTimesViewModel
    {
        public string SurveyUserId { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public string OfficeName { get; set; }
        public string PositionName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int TotalCorrectAnswers { get; set; }
        public decimal? TotalCorrectScores { get; set; }
        public decimal? TotalScores { get; set; }
        public int? TotalSecond { get; set; }
        public int TotalQuestions { get; set; }
        public bool IsViewResult { get; set; }
    }
}
