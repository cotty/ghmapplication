﻿using System.Collections.Generic;

namespace GHM.Survey.Domain.ViewModels
{
    public class SurveyUsersReportDetailViewModel
    {
        public string SurveyUserId { get; set; }
        public string UserId { get; set; }
        public string SurveyId { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string PositionId { get; set; }
        public string PositionName { get; set; }

        public List<SurveyUserAnswerTimeViewModel> SurveyUserAnswerTimes { get; set; }
    }
}
