﻿using System.Collections.Generic;
namespace GHM.Survey.Domain.ViewModels
{
 public   class SurveyGroupDetailViewModel
    {
        public int Order { get; set; }
        public int? ParentId { get; set; }
        public bool IsActive { get; set; }
        public int ChildCount { get; set; }
        public string ConcurrencyStamp { get; set; }
        public List<SurveyGroupTranslationViewModel> SurveyGroupTranslations { get; set; }
    }
}
