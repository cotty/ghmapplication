﻿using GHM.Survey.Domain.Constants;
using System.Collections.Generic;

namespace GHM.Survey.Domain.ViewModels
{
    /// <summary>
    /// 
    /// </summary>

    public class QuestionDetailViewModel
    {
        /// <summary>
        /// VersionId
        /// </summary>
        public string VersionId { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// QuestionGroupId
        /// </summary>
        public int QuestionGroupId { get; set; }

        /// <summary>
        /// IsActive
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public QuestionStatus Status { get; set; }

        /// <summary>
        /// QuestionType
        /// </summary>
        public QuestionType Type { get; set; }

        /// <summary>
        /// Point
        /// </summary>
        public int Point { get; set; }

        /// <summary>
        /// ConcurrencyStamp
        /// </summary>
        public string ConcurrencyStamp { get; set; }

        public bool IsApprover { get; set; }

        public string CreatorId { get; set; }

        /// <summary>
        /// TotalAnswer
        /// </summary>
        public byte TotalAnswer { get; set; }

        public string DeclineReason { get; set; }

        public List<QuestionTranslationViewModel> Translations { get; set; }

        public List<AnswerViewModel> Answers { get; set; }
    }

}
