﻿using System;
using System.Collections.Generic;
using GHM.Survey.Domain.Models;

namespace GHM.Survey.Domain.ViewModels
{
    public class ExamOverviewViewModel
    {
        public string SurveyId { get; set; }
        public string SurveyUserId { get; set; }
        public string SurveyName { get; set; }
        public int TotalQuestion { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? LimitedTimes { get; set; }
        public int? LimitedTime { get; set; }
        public bool IsRequired { get; set; }
        public string SurveyDescription { get; set; }
        public List<SurveyUserAnswerTime> SurveyUserAnswerTimes { get; set; }
    }
}
