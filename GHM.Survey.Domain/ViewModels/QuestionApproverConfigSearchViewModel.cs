﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Survey.Domain.ViewModels
{
    public class QuestionApproverConfigSearchViewModel
    {
        /// <summary>
        /// UserId
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// PositionId
        /// </summary>
        public string PositionId { get; set; }

        /// <summary>
        /// OfficeId
        /// </summary>
        public int? OfficeId { get; set; }

        /// <summary>
        /// FullName
        /// </summary>
        public string FullName { get; set; }

        public string UnsignName { get; set; }

        /// <summary>
        /// Avatar
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// OfficeName
        /// </summary>
        public string OfficeName { get; set; }

        /// <summary>
        /// PositionName
        /// </summary>
        public string PositionName { get; set; }

    }

}
