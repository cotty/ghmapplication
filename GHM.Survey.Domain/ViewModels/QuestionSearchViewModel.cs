﻿using GHM.Survey.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Survey.Domain.ViewModels
{
    public class QuestionSearchViewModel
    {
        /// <summary>
        /// VersionId
        /// </summary>
        public string VersionId { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// QuestionGroupId
        /// </summary>
        public int QuestionGroupId { get; set; }

         /// <summary>
        /// IsActive
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// IsDelete
        /// </summary>
        public bool IsDelete { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public QuestionStatus Status { get; set; }

        /// <summary>
        /// ApprovedTime
        /// </summary>
        public DateTime? ApprovedTime { get; set; }

        /// <summary>
        /// ApproverFullName
        /// </summary>
        public string ApproverFullName { get; set; }

        /// <summary>
        /// QuestionType
        /// </summary>
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// Point
        /// </summary>
        public int Point { get; set; }

        /// <summary>
        /// CreateTime
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// SentTime
        /// </summary>
        public DateTime? SentTime { get; set; }

        /// <summary>
        /// CreatorFullName
        /// </summary>
        public string CreatorFullName { get; set; }
               
        /// <summary>
        /// TotalAnswer
        /// </summary>
        public byte TotalAnswer { get; set; }

        /// <summary>
        /// ToDate
        /// </summary>
        public DateTime? ToDate { get; set; }

    }

}
