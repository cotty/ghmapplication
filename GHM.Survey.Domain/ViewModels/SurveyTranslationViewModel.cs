﻿namespace GHM.Survey.Domain.ViewModels
{
 public   class SurveyTranslationViewModel
    {
        public string LanguageId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string UnsignName { get; set; }
        public string SurveyGroupName { get; set; }
    }
}
