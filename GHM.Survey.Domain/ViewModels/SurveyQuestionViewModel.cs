﻿
namespace GHM.Survey.Domain.ViewModels
{
  public  class SurveyQuestionViewModel
    {
        public string QuestionVersionId { get; set; }
        public string QuestionName { get; set; }
    }
}
