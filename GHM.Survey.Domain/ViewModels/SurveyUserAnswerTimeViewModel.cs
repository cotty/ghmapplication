﻿using System;

namespace GHM.Survey.Domain.ViewModels
{
    public class SurveyUserAnswerTimeViewModel
    {       
        public string Id { get; set; }            
        public DateTime StartTime { get; set; }        
        public DateTime? EndTime { get; set; }      
        public int TotalUserAnswers { get; set; }        
        public int TotalCorrectAnswers { get; set; }        
        public decimal TotalCorrectScores { get; set; }        
        public decimal TotalScores { get; set; }        
        public int? TotalSeconds { get; set; }        
        public int TotalQuestions { get; set; }
        public bool? IsViewResult { get; set; }
    }
}
