﻿using System;
using System.Collections.Generic;
using System.Text;
using GHM.Survey.Domain.Constants;

namespace GHM.Survey.Domain.ViewModels
{
    public class SurveyInfoViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public SurveyType Type { get; set; }
        public List<SurveyQuestionSearchViewModel> Questions { get; set; }
    }
}
