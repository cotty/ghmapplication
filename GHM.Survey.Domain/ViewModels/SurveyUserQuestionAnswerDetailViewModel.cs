﻿using GHM.Survey.Domain.Constants;

namespace GHM.Survey.Domain.ViewModels
{
    public class SurveyUserQuestionAnswerDetailViewModel
    {
        public string TimeId { get; set; }
        public string QuestionVersionId { get; set; }
        public QuestionType QuestionType { get; set; }
        public string QuestionContent { get; set; }
        public byte TotalAnswer { get; set; }
        public string QuestionName { get; set; }
        public string AnswerId { get; set; }
        public string AnswerName { get; set; }
        public string AnswerValue { get; set; }
        public int? AnswerOrder { get; set; }
        public bool? AnswerIsCorrect { get; set; }
        public string SurveyUserAnswerId { get; set; }
        public bool IsSelected { get; set; }
        public bool? IsCorrect { get; set; }
        public string AnswerIdUserSelected { get; set; }
        public int QuestionOrder { get; set; }
        public string ToQuestionVersionId { get; set; }
    }
}
