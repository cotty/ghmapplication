﻿using System.Collections.Generic;

namespace GHM.Survey.Domain.ViewModels
{
 public   class SurveyGroupTreeViewModel
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Text { get; set; }
        public string Icon { get; set; }
        public string IdPath { get; set; }
        public dynamic Data { get; set; }
        public State State { get; set; }
        public List<SurveyGroupTreeViewModel> Children { get; set; }
        public int? ChildCount { get; set; }

        public SurveyGroupTreeViewModel()
        {
            Icon = string.Empty;

            State = new State
            {
                Opened = false,
                Selected = false,
                Disabled = false
            };
        }
    }


}


