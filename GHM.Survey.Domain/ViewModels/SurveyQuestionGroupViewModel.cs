﻿namespace GHM.Survey.Domain.ViewModels
{
    public class SurveyQuestionGroupViewModel
    {
        public string SurveyId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int TotalQuestions { get; set; }
    }
}
