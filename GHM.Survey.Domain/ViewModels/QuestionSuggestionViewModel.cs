﻿using System.Collections.Generic;
using GHM.Infrastructure.Models;
using GHM.Survey.Domain.Constants;

namespace GHM.Survey.Domain.ViewModels
{
    public class QuestionSuggestionViewModel : Suggestion<string>
    {
        public QuestionType Type { get; set; }
        public byte TotalAnswer { get; set; }
        public List<AnswerSuggestionViewModel> Answers { get; set; }
    }
}
