﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Survey.Domain.ViewModels
{
    public class QuestionTranslationViewModel
    {
        /// <summary>
        /// TenantId
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// LanguageId
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// QuestionVersionId
        /// </summary>
        public string QuestionVersionId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Content
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Explain
        /// </summary>
        public string Explain { get; set; }

        /// <summary>
        /// UnsignName
        /// </summary>
        public string UnsignName { get; set; }

        /// <summary>
        /// QuestionGroupName
        /// </summary>
        public string QuestionGroupName { get; set; }

    }

}
