﻿using System;
using GHM.Survey.Domain.Constants;

namespace GHM.Survey.Domain.ViewModels
{
  public  class SurveyViewModel
    {
        public string Id { get; set; }
        public int? SurveyGroupId { get; set; }
        public bool IsActive { get; set; }
        public bool IsRequire { get; set; }
        public int TotalQuestion { get; set; }
        public int TotalUser { get; set; }
        public int LimitedTimes { get; set; }
        public int? LimitedTime { get; set; }
        public SurveyStatus Status { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public SurveyType Type { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SurveyGroupName { get; set; }
        public bool IsPreRendering { get; set; }
        public DateTime CreateTime { get; set; }        
    }
}
