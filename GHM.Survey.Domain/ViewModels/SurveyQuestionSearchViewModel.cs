﻿using System.Collections.Generic;
using GHM.Survey.Domain.Constants;

namespace GHM.Survey.Domain.ViewModels
{
    public class SurveyQuestionSearchViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public QuestionType Type { get; set; }
        public decimal? Point { get; set; }
        public byte TotalAnswer { get; set; }
        public int Order { get; set; }
        public List<SurveyQuestionAnswerSearchViewModel> Answers { get; set; }
    }

    public class SurveyQuestionAnswerSearchViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ToQuestionVersionId { get; set; }
        public string ToQuestionName { get; set; }
    }
}
