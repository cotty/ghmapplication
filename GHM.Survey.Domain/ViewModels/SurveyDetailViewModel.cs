﻿using System;
using System.Collections.Generic;
using GHM.Survey.Domain.Constants;

namespace GHM.Survey.Domain.ViewModels
{
    public class SurveyDetailViewModel
    {

        public string Id { get; set; }
        public int? SurveyGroupId { get; set; }
        public bool IsActive { get; set; }
        public bool IsRequire { get; set; }
        public int TotalQuestion { get; set; }
        public int TotalUser { get; set; }
        public int LimitedTimes { get; set; }
        public int? LimitedTime { get; set; }
        public SurveyStatus Status { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public SurveyType Type { get; set; }
        public string ConcurrencyStamp { get; set; }
        public bool IsPreRendering { get; set; }
        public List<SurveyTranslationViewModel> SurveyTranslations { get; set; }
        public List<SurveyQuestionSearchViewModel> Questions { get; set; }
        public List<SurveyQuestionGroupViewModel> QuestionGroups { get; set; }
        public List<SurveyUserViewModel> Users { get; set; }

    }
}
