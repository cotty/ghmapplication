﻿namespace GHM.Survey.Domain.ViewModels
{
    public class AnswerSuggestionViewModel
    {
        public string Id { get; set; }
        public string QuestionVersionId { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
    }
}
