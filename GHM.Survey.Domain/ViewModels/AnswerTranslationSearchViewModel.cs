﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Survey.Domain.ViewModels
{
    public class AnswerTranslationSearchViewModel
    {
        /// <summary>
        /// AnswerId
        /// </summary>
        public string AnswerId { get; set; }

        /// <summary>
        /// LanguageId
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Explain
        /// </summary>
        public string Explain { get; set; }

    }

}
