﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Survey.Domain.ViewModels
{
    public class SurveyQuestionInfoViewModel
    {
        public List<SurveyQuestionSearchViewModel> Questions { get; set; }
        public List<SurveyQuestionGroupViewModel> QuestionGroups { get; set; }
    }
}
