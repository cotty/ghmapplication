﻿
namespace GHM.Survey.Domain.ViewModels
{
  public  class QuestionGroupForSelectViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TotalQuestions { get; set; }
    }
}
