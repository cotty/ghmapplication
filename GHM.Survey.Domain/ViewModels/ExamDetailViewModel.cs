﻿using GHM.Survey.Domain.Constants;
using System;
using System.Collections.Generic;

namespace GHM.Survey.Domain.ViewModels
{
    public class ExamDetailViewModel
    {
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public string SurveyName { get; set; }
        public SurveyType SurveyType { get; set; }
        public int? TotalSeconds { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string OfficeName { get; set; }
        public string PositionName { get; set; }
        public int TotalCorrectAnswers { get; set; }
        public bool? IsViewResult { get; set; }
        public List<SurveyUserQuestionAnswerDetailViewModel> SurveyUserQuestionAnswerDetails { get; set; }
    }
}
