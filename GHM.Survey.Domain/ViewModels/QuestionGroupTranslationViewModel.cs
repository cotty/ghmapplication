﻿
namespace GHM.Survey.Domain.ViewModels
{
 public   class QuestionGroupTranslationViewModel
    {
        public string LanguageId { get; set; }

        public string Name { get; set; }

        public string ParentName { get; set; }

        public string Description { get; set; }

    }
}
