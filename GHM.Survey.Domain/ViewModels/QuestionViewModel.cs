﻿using GHM.Survey.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Survey.Domain.ViewModels
{
    public class QuestionViewModel
    {
        public string VersionId { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string GroupName { get; set; }
        public string CreatorId { get; set; }
        public string FullName { get; set; }
        public QuestionType Type { get; set; }
        public QuestionStatus Status { get; set; }
        public DateTime CreateTime { get; set; }
        public bool IsActive { get; set; }
    }
}
