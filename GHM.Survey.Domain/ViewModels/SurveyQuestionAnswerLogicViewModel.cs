﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Survey.Domain.ViewModels
{
    public class SurveyQuestionAnswerLogicViewModel
    {
        public string AnswerId { get; set; }
        public string ToQuestionVersionId { get; set; }
        public string ToQuestionName { get; set; }
    }
}
