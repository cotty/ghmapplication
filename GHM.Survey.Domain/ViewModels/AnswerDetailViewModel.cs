﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Survey.Domain.ViewModels
{
    /// <summary>
    /// 
    /// </summary>

    public class AnswerDetailViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// QuestionId
        /// </summary>
        public string QuestionVersionId { get; set; }

        /// <summary>
        /// CreatorId
        /// </summary>
        public string CreatorId { get; set; }

        /// <summary>
        /// LastUpdateUserId
        /// </summary>
        public string LastUpdateUserId { get; set; }

        /// <summary>
        /// IsCorrect
        /// </summary>
        public bool IsCorrect { get; set; }

        /// <summary>
        /// IsDelete
        /// </summary>
        public bool IsDelete { get; set; }

        /// <summary>
        /// Order
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// CreatorFullName
        /// </summary>
        public string CreatorFullName { get; set; }

        /// <summary>
        /// CreateTime
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// LastUpdate
        /// </summary>
        public DateTime? LastUpdate { get; set; }

        /// <summary>
        /// ConcurrencyStamp
        /// </summary>
        public string ConcurrencyStamp { get; set; }

        /// <summary>
        /// LastUpdateFullName
        /// </summary>
        public string LastUpdateFullName { get; set; }

        public List<AnswerTranslationViewModel> AnswerTranslations { get; set; }
    }

}
