﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Survey.Domain.ViewModels
{
    /// <summary>
    /// 
    /// </summary>

    public class AnswerDetailByLanguageViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

}
