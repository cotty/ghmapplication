﻿
namespace GHM.Survey.Domain.ViewModels
{
  public  class SurveyGroupForSelectViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
