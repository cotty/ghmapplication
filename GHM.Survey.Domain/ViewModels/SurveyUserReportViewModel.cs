﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Survey.Domain.ViewModels
{
    public class SurveyUserReportViewModel
    {
        public string SurveyId { get; set; }
        public string SurveyUserId { get; set; }
        public string SurveyName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? LimitedTime { get; set; }
        public double? TotalTimeExam { get; set; }
        public DateTime? StartTimeExam { get; set; }
        public DateTime? EndTimeExam { get; set; }
        public bool? IsViewResult { get; set; }
        public string SurveyUserTimeId { get; set; }
    }
}
