﻿using GHM.Survey.Domain.Models;
using System;
using System.Collections.Generic;
using GHM.Survey.Domain.Constants;

namespace GHM.Survey.Domain.ViewModels
{
    public class DoExamViewModel
    {
        public string SurveyId { get; set; }
        public string SurveyName { get; set; }
        public string TimesId { get; set; }
        public int? TotalSeconds { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string SurveyUserId { get; set; }
        public string SurveyUserAnswerTimeId { get; set; }
        public string OfficeName { get; set; }
        public string PositionName { get; set; }
        public SurveyType SurveyType { get; set; }
        public List<SurveyUserQuestionAnswer> SurveyUserQuestionAnswers { get; set; }

        public DoExamViewModel()
        {
        }
    }
}
