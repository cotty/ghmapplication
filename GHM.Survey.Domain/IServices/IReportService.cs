﻿using System;
using System.Threading.Tasks;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;

namespace GHM.Survey.Domain.IServices
{
    public interface IReportService
    {
        SearchResult<SurveyReport> SearchReport(string tenantId, string languageId, string keyword, int? surveyGroupId, DateTime? startDate,
            DateTime? endDate, int page, int pageSize);

        Task<SearchResult<SurveyUsersReport>> SearchUsersReport(string tenantId, string surveyId, string keyword, int page,
            int pageSize);

        Task<ActionResultResponse<SurveyUsersReportDetailViewModel>> SearchUsersReportDetail(string tenantId, string surveyId, 
            string surveyUserId);

        Task<SearchResult<ReportSurveyUserAnswerTimesViewModel>> SearchReportSurveyUserAnswerTimes(string tenantId, string surveyId,
            string surveyUserId);

        Task<SearchResult<QuestionGroupReport>> GetQuestionGroupReport(string tenantId, string languageId, string surveyId,
            string surveyUserAnswerTimeId);

        Task<SearchResult<SurveyUserReportViewModel>> GetSurveyByUserId(string tenantId, string languageId, string userId, string keyWord, int? surveyGroupId,
             DateTime? startDate, DateTime? endDate, int page, int pageSize);
    }
}
