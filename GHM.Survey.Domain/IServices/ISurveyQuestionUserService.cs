﻿using GHM.Infrastructure.Models;
using GHM.Survey.Domain.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Survey.Domain.IServices
{
    public interface ISurveyQuestionUserService
    {

        /// <summary>
        /// Sinh danh sách câu hỏi khảo sát
        /// </summary>
        /// <param name="surveyId"></param>
        /// <param name="surveyUserId"></param>
        /// <returns></returns>
        Task<int> RanDomQuestionAsync(string surveyId, string surveyUserId, string surveyUserAnswerTimesId);
        
      

    }
}
