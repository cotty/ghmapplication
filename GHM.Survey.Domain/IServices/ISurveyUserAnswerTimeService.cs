﻿using GHM.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Survey.Domain.IServices
{
    public interface ISurveyUserAnswerTimeService
    {        
        Task<ActionResultResponse> Delete(string surveyUserAnswerTimesId);

        Task<int> GetSurveyTime(string surveyUserAnswerTimesId);

        Task<bool> CheckExistsBySurveyIdAndSurveyUserId(string surveyId, string surveyUserId);

        Task<int> FinishDoExam(string surveyId, string surveyUserId, string surveyUserAnswerTimeId);

        Task<ActionResultResponse> UpdateIsViewResult(string tenantId, string surveyId, string surveyUserId, string surveyUserAnswerTimeId, bool isViewResult);

        Task<ActionResultResponse> UpdateAllIsViewResult(List<string> surveyUserAnswerTimesIds, bool isViewResult);
    }
}
