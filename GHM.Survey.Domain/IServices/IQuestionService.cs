﻿using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Survey.Domain.Constants;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Survey.Domain.IServices
{
    public interface IQuestionService
    {
        Task<SearchResult<QuestionViewModel>> Search(string tenantId, string languageId, string keyword,
                QuestionType? questionType, int? questionGroupId, string creatorId, string currentUserId, QuestionStatus? questionStatus,
                int page, int pageSize);

        Task<SearchResult<QuestionViewModel>> SearchQuestionForApprove(string tenantId, string languageId, string userId, string keyword,
            QuestionType? questionType, int? questionGroupId, string creatorId, int page, int pageSize);

        Task<ActionResultResponse> Insert(string tenantId, string questionId, bool isSend, string creatorId,
            string creatorFullName, string creatorAvatar, QuestionMeta questionMeta);

        Task<ActionResultResponse> Update(string tenantId, string versionId, string questionId, bool isSend,
            string creatorId, string creatorFullName, string creatorAvatar, QuestionMeta questionMeta);

        Task<ActionResultResponse> Delete(string userid, string questionVersionId);

        Task<ActionResultResponse> DeleteListQuestion(List<string> questionVersionIds, bool isReadOnly = false);

        Task<ActionResultResponse<QuestionDetailViewModel>> GetDetail(string tenantId, string userId, string versionId);

        Task<SearchResult<QuestionViewModel>> GetAllQuestionByApproverUserId(string tenantId, string approverUserId, string languageId);

        Task<SearchResult<QuestionSuggestionViewModel>> SearchForSuggestion(string tenantId, string languageId, string keyword, int? surveyGroupId,
            QuestionType? type, SurveyType? surveyType, int page, int pageSize);

        Task<ActionResultResponse> ChangeQuestionStatus(string tenantId, string questionVersionId,
            QuestionStatus questionStatus, string reason, string userId, string fullName, string avatar);

        Task<List<ActionResultResponse>> ChangeListQuestionStatus(string tenantId, List<string> questionVersionIds,
            QuestionStatus questionStatus, string reason, string userId, string fullName, string avatar);

        Task<ActionResultResponse> UpdateDeclineReason(string questionVersionId, string declineReason);

    }

}
