﻿using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.ViewModels;
using System.Threading.Tasks;

namespace GHM.Survey.Domain.IServices
{
    public interface IQuestionApproverConfigService
    {
        Task<SearchResult<QuestionApproverConfigSearchViewModel>> SearchAsync(string tenantId, string keyword, int page, int pageSize);

        Task<ActionResultResponse> Insert(string tenantId, string userId);

        Task<ActionResultResponse> Delete(string tenantId, string userId);

    }
}
