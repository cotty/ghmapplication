﻿using System.Threading.Tasks;
using GHM.Infrastructure.Models;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.ViewModels;

namespace GHM.Survey.Domain.IServices
{
    public interface IExamService
    {
        Task<ActionResultResponse<ExamOverviewViewModel>> GetOverview(string tenantId, string languageId, string surveyId, string userId);

        Task<ActionResultResponse<DoExamViewModel>> Start(string tenantId, string languageId, string surveyId, string userId);

        Task<ActionResultResponse<string>> SaveAnswer(string userId, UserAnswerMeta userAnswerMeta);

        Task<ActionResultResponse> Finish(string tenantId, string languageId, string surveyId, string surveyUserId);

        Task<ActionResultResponse<ExamDetailViewModel>> GetExamDetail(string tenantId, string languageId, string surveyId, 
            string surveyUserId, string surveyUserTimeId, bool isManager);
    }
}
