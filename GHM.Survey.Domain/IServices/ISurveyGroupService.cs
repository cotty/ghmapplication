﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;


namespace GHM.Survey.Domain.IServices
{
    public  interface ISurveyGroupService
    {
        Task<List<SurveyGroupSearchViewModel>> GetsAll(string tenantId, string languageId);

        Task<SearchResult<SurveyGroupSearchViewModel>> Search(string tenantId, string languageId, string keyword,
            bool? isActive, int page, int pageSize);

        Task<SearchResult<SurveyGroupSearchViewModel>> SearchTree(string tenantId, string languageId, string keyword, bool? isActive,
            int page, int pageSize);

        Task<List<TreeData>> GetFullSurveyGroupTree(string tenantId, string languageId);

        Task<ActionResultResponse<int>> Insert(string tenantId, string creatorId, string creatorFullName, SurveyGroupMeta surveyGroupMeta);

        Task<ActionResultResponse> Update(string tenantId, string lastUpdateUserId, string lastUpdateFullName, int surveyGroupId, SurveyGroupMeta surveyGroupMeta);

        Task<ActionResultResponse> Delete(string tenantId, int id);

        Task<ActionResultResponse<SurveyGroupDetailViewModel>> GetDetail(string tenantId, string languageId, int id);

        Task<List<SurveyGroupForSelectViewModel>> GetSurveyGroupForSelect(string tenantId, string languageId, string keyword, int page, int pageSize);
    }
}
