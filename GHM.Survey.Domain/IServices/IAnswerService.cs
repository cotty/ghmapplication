﻿using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Survey.Domain.IServices
{
    public interface IAnswerService
    {
        Task<ActionResultResponse> Insert( AnswerMeta answerMeta);

        Task<ActionResultResponse> Update(string answerId, AnswerMeta answerMeta);

        Task<ActionResultResponse> Delete(string answerId);

        Task<ActionResultResponse<AnswerDetailViewModel>> GetDetail(string answerId);

    }

}
