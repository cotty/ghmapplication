﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;


namespace GHM.Survey.Domain.IServices
{
    public  interface IQuestionGroupService
    {
        Task<List<QuestionGroupSearchViewModel>> GetsAll(string tenantId, string languageId);

        Task<SearchResult<QuestionGroupSearchViewModel>> Search(string tenantId, string languageId, string keyword,
            bool? isActive, int page, int pageSize);

        Task<SearchResult<QuestionGroupSearchViewModel>> SearchTree(string tenantId, string languageId, string keyword, bool? isActive,
            int page, int pageSize);

        Task<List<TreeData>> GetFullQuestionGroupTree(string tenantId, string languageId);

        Task<ActionResultResponse<int>> Insert(string tenantId, string creatorId, string creatorFullName, QuestionGroupMeta questionGroupMeta);

        Task<ActionResultResponse> Update(string tenantId, string lastUpdateUserId, string lastUpdateFullName, int questionGroupId, 
            QuestionGroupMeta questionGroupMeta);

        //Task<ActionResultResponse> UpdateTotalQuestion(string tenantId, int questionGroupId, int totalQuestion);

        Task<ActionResultResponse> Delete(string tenantId, int id);

        Task<ActionResultResponse<QuestionGroupDetailViewModel>> GetDetail(string tenantId, string languageId, int id);

        Task<SearchResult<QuestionGroupForSelectViewModel>> GetQuestionGroupForSelect(string tenantId, string languageId, string keyword, 
            int page, int pageSize);
    }
}
