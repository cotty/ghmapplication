﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;

namespace GHM.Survey.Domain.IServices
{
    public interface ISurveyService
    {
        Task<SearchResult<SurveyViewModel>> Search(string tenantId, string languageId, string keyword, int? surveyGroupId,
            DateTime? startDate, DateTime? endDate, bool? isActive, int page, int pageSize);

        Task<ActionResultResponse<string>> Insert(string tenantId, string creatorId, string creatorFullName, string creatorAvatar, SurveyMeta surveyMeta);

        Task<ActionResultResponse<string>> Update(string tenantId, string lastUpdateUserId, string lastUpdateFullName, string lastUpdateAvatar,
            string surveyId, SurveyMeta surveyMeta);

        Task<ActionResultResponse> Delete(string tenantId, string surveyId);

        Task<ActionResultResponse<SurveyDetailViewModel>> GetDetail(string tenantId, string languageId, string surveyId);

        Task<ActionResultResponse> SaveQuestions(string tenantId, string surveyId, string currentUserId, string currentUserFullName,
            string currentUserAvatar, SurveyListQuestionGroupMeta surveyQusetionGroupMeta);

        Task<ActionResultResponse<SurveyQuestionInfoViewModel>> GetSurveyQuestions(string tenantId, string languageId,
            string surveyId);

        //Task<ActionResultResponse<List<SurveyInfoViewModel>>> UserGetTopRequiredSurvey(string tenantId, string languageId, string userId, int top);

        Task<ActionResultResponse<SurveyInfoViewModel>> UserGetSurveyInfo(string tenantId, string languageId, string userId, string surveyId);
    }
}
