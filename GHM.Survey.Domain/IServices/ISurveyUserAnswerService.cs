﻿using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Models;
using System.Threading.Tasks;

namespace GHM.Survey.Domain.IServices
{
    public interface ISurveyUserAnswerService
    {
        Task<ActionResultResponse<string>> Insert(string userId, SurveyUserAnswerMeta surveyUserAnswerMeta);

        Task<ActionResultResponse> Update(string surveyUserAnswerId, string userId, SurveyUserAnswerMeta surveyUserAnswerMeta);

        Task<ActionResultResponse> Delete(string surveyUserAnswerId);

        /// <summary>
        /// Danh sách câu hỏi và các phương án trả lời cho người tham gia
        /// </summary>
        /// <param name="tenantId"></param>
        /// <param name="surveyId"></param>
        /// <param name="surveyUserId"></param>
        /// <param name="languageId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<SearchResult<QuestionForUser>> DetailSurveyQuestionUser(string tenantId, string userId, string surveyId, string surveyUserId, 
            string languageId, int page, int pageSize);       
    }
}