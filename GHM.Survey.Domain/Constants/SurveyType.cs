﻿namespace GHM.Survey.Domain.Constants
{
    public enum SurveyType
    {
        /// <summary>
        /// khảo sát thông thương
        /// </summary>
        Normal,
        /// <summary>
        /// khảo sát điều hướng
        /// </summary>
        Logic
    }
}
