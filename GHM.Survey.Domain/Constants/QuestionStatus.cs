﻿namespace GHM.Survey.Domain.Constants
{
    public enum QuestionStatus
    {
        Draft, // 0 - Draft (Nháp).
        Pending, // 1- Pending (Đang gửi).
        Approved, // 2 - Approved (Đã duyệt). 
        Decline // 3 - Decline (Không duyệt).
    }
}
