﻿namespace GHM.Survey.Domain.Constants
{
    public enum QuestionType
    {
        /// <summary>
        /// Một đáp án.
        /// </summary>
        SingleChoice,
        /// <summary>
        /// Nhiều đáp án.
        /// </summary>
        MultiChoice,
        /// <summary>
        /// Đánh giá.
        /// </summary>
        Rating,
        /// <summary>
        /// Tự luận.
        /// </summary>
        Essay,
        /// <summary>
        /// Tự trả lời. Cho phép người dùng tự đưa ra các phương án trả lời.
        /// </summary>
        SelfResponded,
        /// <summary>
        /// Câu hỏi dạng nút bấm. (Sử dụng trong khảo sát điều hướng.
        /// </summary>
        Logic  
    }
}
