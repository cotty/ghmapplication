﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Survey.Domain.Constants
{
    public enum SurveyStatus
    {
        Drafts,
        Approved
    }
}
