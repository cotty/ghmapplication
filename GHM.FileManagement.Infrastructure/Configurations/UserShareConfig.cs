﻿using GHM.FileManagement.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.FileManagement.Infrastructure.Configurations
{
    public class UserShareConfig : IEntityTypeConfiguration<UserShare>
    {
        public void Configure(EntityTypeBuilder<UserShare> builder)
        {
            builder.Property(x => x.Id).HasColumnName("Id").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.UserShareId).HasColumnName("UserShareId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.UserShareFullName).HasColumnName("UserShareFullName").IsRequired().HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.ReciverAvatar).HasColumnName("ReciverAvatar").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(500);
            builder.Property(x => x.ReciverPositionName).HasColumnName("ReciverPositionName").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.ReciverTitleName).HasColumnName("ReciverTitleName").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.ReciverOfficeName).HasColumnName("ReciverOfficeName").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.ReciverOfficeId).HasColumnName("ReciverOfficeId").IsRequired(false).HasColumnType("int");
            builder.Property(x => x.ReciverTitleId).HasColumnName("ReciverTitleId").IsRequired(false).IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.ReciverPositionId).HasColumnName("ReciverPositionId").IsRequired(false).IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.Type).HasColumnName("Type").IsRequired().HasColumnType("int");
            builder.Property(x => x.FolderId).HasColumnName("FolderId").IsRequired(false).HasColumnType("int");
            builder.Property(x => x.FileId).HasColumnName("FileId").IsRequired(false).IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.Role).HasColumnName("Role").IsRequired(false).HasColumnType("int");
            builder.Property(x => x.IsShare).HasColumnName("IsShare").IsRequired(false).HasColumnType("bit");
            builder.Property(x => x.FromDate).HasColumnName("FromDate").IsRequired(false).HasColumnType("datetime");
            builder.Property(x => x.ToDate).HasColumnName("ToDate").IsRequired(false).HasColumnType("datetime");

            builder.ToTable("UserShares").HasKey(x => x.Id);
        }
    }
}
