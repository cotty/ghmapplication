﻿using GHM.FileManagement.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.FileManagement.Infrastructure.Configurations
{
    public class FileConfig : IEntityTypeConfiguration<File>
    {
        public void Configure(EntityTypeBuilder<File> builder)
        {
            builder.Property(x => x.Id).HasMaxLength(50).IsRequired().IsUnicode(false);
            builder.Property(x => x.TenantId).HasMaxLength(50).IsRequired().IsUnicode(false);
            builder.Property(x => x.FolderId).HasMaxLength(50).IsRequired(false).IsUnicode(false);
            builder.Property(x => x.Name).HasMaxLength(256).IsRequired();
            builder.Property(x => x.Type).HasMaxLength(50).IsRequired().IsUnicode(false);
            builder.Property(x => x.Url).HasMaxLength(500).IsRequired().IsUnicode(false);
            builder.Property(x => x.CreatorId).HasMaxLength(50).IsRequired().IsUnicode(false);
            builder.Property(x => x.CreatorFullName).HasMaxLength(50).IsRequired();
            builder.Property(x => x.CreatorAvatar).HasMaxLength(500).IsRequired(false).IsUnicode(false);
            builder.Property(x => x.Extension).HasMaxLength(10).IsRequired(false).IsUnicode(false);
            builder.Property(x => x.IsTrash).HasColumnName("IsTrash").IsRequired(false).HasColumnType("bit");
            builder.Property(x => x.ShareLevel).HasColumnName("ShareLevel").IsRequired(false).HasColumnType("int");

            builder.ToTable("Files").HasKey(x => x.Id);
        }
    }
}
