﻿using GHM.FileManagement.Domain.IServices;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.FileManagement.Domain.ModelMetas;
using GHM.FileManagement.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.FileManagement.Domain.IRepositories;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.IServices;
using GHM.FileManagement.Domain.Resources;
using GHM.FileManagement.Domain.Models;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Extensions;
using GHM.FileManagement.Domain.Constants;
using Microsoft.Extensions.Configuration;
using GHM.Infrastructure.Services;
using System.Linq;

namespace GHM.FileManagement.Infrastructure.Services
{
    public class FolderService : IFolderService
    {
        private readonly IFolderRepository _folderRepository;
        private readonly IFileRepository _fileRepository;
        private readonly IUserShareRepository _userShareRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmFileManagementResource> _fileManagermentResourceResource;
        private readonly IConfiguration _configuration;

        public FolderService(IFolderRepository folderRepository, IFileRepository fileRepository,
        IResourceService<SharedResource> sharedResourceService, IUserShareRepository userShareRepository,
        IConfiguration configuration,
            IResourceService<GhmFileManagementResource> fileManagerMentResourceResource)
        {
            _folderRepository = folderRepository;
            _fileRepository = fileRepository;
            _userShareRepository = userShareRepository;
            _sharedResourceService = sharedResourceService;
            _configuration = configuration;
            _fileManagermentResourceResource = fileManagerMentResourceResource;
        }

        public async Task<FolderSearchViewModel> Search(string tenantId, string userId, string keyword, int page, int pageSize)
        {
            var folders = await _folderRepository.Search(tenantId, userId, keyword, page, pageSize, out var totalFolderRows);
            var files = await _fileRepository.Search(tenantId, userId, keyword, null, page, pageSize, out var totalFileRows);

            return new FolderSearchViewModel
            {
                Folders = folders,
                Files = files,
                TotalFiles = totalFileRows,
                TotalFolders = totalFolderRows
            };
        }

        public Task<SearchResult<FolderViewModel>> SearchTree(string tenantId, string keyword, int page, int pageSize)
        {
            throw new NotImplementedException();
        }

        public async Task<ActionResultResponse<Folder>> Insert(string tenantId, string creatorId, string creatorFullName, string creatorAvatar,
            FolderMeta folderMeta)
        {
            var folder = new Folder
            {
                IdPath = "-1",
                TenantId = tenantId,
                ShareLevel = folderMeta.ShareLevel,
                CreatorId = creatorId,
                CreatorFullName = creatorFullName,
                CreatorAvatar = creatorAvatar,
                Name = folderMeta.Name.Trim(),
                UnsignName = folderMeta.Name.Trim().StripVietnameseChars().ToUpper()
            };

            var isExistsByName = await _folderRepository.CheckName(tenantId, folderMeta.ParentId, creatorId, folder.UnsignName);
            if (isExistsByName)
                return new ActionResultResponse<Folder>(-2,
                    _fileManagermentResourceResource.GetString("This folder does exists. Please try again."));

            if (folderMeta.ParentId.HasValue)
            {
                var parentInfo = await _folderRepository.GetInfo(tenantId, creatorId, folderMeta.ParentId.Value);
                if (parentInfo == null)
                    return new ActionResultResponse<Folder>(-2,
                        _fileManagermentResourceResource.GetString("Parent folder does not exists. Please try again."));

                folder.ParentId = parentInfo.Id;
                folder.IdPath = $"{parentInfo.IdPath}.-1";
            }

            var result = await _folderRepository.Insert(folder);
            if (result <= 0)
                return new ActionResultResponse<Folder>(result, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            folder.IdPath = folder.IdPath.Replace("-1", folder.Id.ToString());
            await _folderRepository.UpdateFolderIdPath(folder.Id, folder.IdPath);

            return new ActionResultResponse<Folder>(result, _fileManagermentResourceResource.GetString("Add new folder successful."),
                string.Empty, folder);
        }

        public async Task<ActionResultResponse> Update(string tenantId,
            string lastUpdateUserId, string lastUpdateFullName, int folderId, FolderMeta folderMeta)
        {
            var folderInfo = await _folderRepository.GetInfo(tenantId, lastUpdateUserId, folderId);
            if (folderInfo == null)
                return new ActionResultResponse(-2, _fileManagermentResourceResource.GetString("Folder info does not exists. Please try again."));

            if (folderInfo.TenantId != tenantId)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (folderInfo.ConcurrencyStamp != folderMeta.ConcurrencyStamp)
                return new ActionResultResponse(-4,
                    _sharedResourceService.GetString("The folder already updated by other people. You can not update this folder."));

            if (folderMeta.ParentId.HasValue && folderInfo.Id == folderMeta.ParentId.Value)
                return new ActionResultResponse(-5, _fileManagermentResourceResource.GetString("Folder and parent folder can not be the same.")); ;

            var oldParentId = folderInfo.ParentId;
            var oldIdPath = folderInfo.IdPath;

            folderInfo.Name = folderMeta.Name;
            //folderInfo.Order = folderMeta.Order;
            folderInfo.ConcurrencyStamp = Guid.NewGuid().ToString();
            folderInfo.LastUpdate = DateTime.Now;
            folderInfo.LastUpdateUserId = lastUpdateUserId;
            folderInfo.LastUpdateFullName = lastUpdateFullName;
            folderInfo.UnsignName = folderMeta.Name.Trim().StripVietnameseChars().ToUpper();

            if (await _folderRepository.CheckName(tenantId, folderMeta.ParentId, folderInfo.CreatorId, folderInfo.UnsignName))
                return new ActionResultResponse<Folder>(-2,
                    _fileManagermentResourceResource.GetString("This folder does exists. Please try again."));

            if (folderInfo.ParentId.HasValue && !folderMeta.ParentId.HasValue)
            {
                folderInfo.ParentId = null;
                folderInfo.IdPath = folderInfo.Id.ToString();
                //folderInfo.OrderPath = folderInfo.Order.ToString();
            }
            else if (folderMeta.ParentId.HasValue && folderMeta.ParentId != folderInfo.ParentId)
            {
                var parentInfo = await _folderRepository.GetInfo(tenantId, lastUpdateUserId, folderMeta.ParentId.Value);
                if (parentInfo == null)
                    return new ActionResultResponse(-6, _fileManagermentResourceResource.GetString("Parent folder does not exists. Please try again.")); ;

                parentInfo.IdPath = $"{parentInfo.IdPath}.{parentInfo.Id}";
                parentInfo.ParentId = parentInfo.Id;
                //parentInfo.OrderPath = $"{parentInfo.OrderPath}.{parentInfo.Order}";
            }

            await _folderRepository.Update(folderInfo);

            // Update children IdPath and RootInfo
            if (folderInfo.IdPath != oldIdPath)
            {
                await _folderRepository.UpdateChildrenIdPath(oldIdPath, folderInfo.IdPath);
            }

            // Update parent survey group child count.
            if (folderInfo.ParentId.HasValue && oldParentId.HasValue && folderInfo.ParentId.Value != oldParentId.Value)
            {
                // Update old parent survey group child count.
                var oldChildCount = await _folderRepository.GetChildCount(oldParentId.Value);
                await _folderRepository.UpdateChildCount(tenantId, lastUpdateUserId, oldParentId.Value, oldChildCount);

                // Update new parent survey group child count.
                var newParentId = folderInfo.ParentId.Value;
                var newChildCount = await _folderRepository.GetChildCount(newParentId);
                await _folderRepository.UpdateChildCount(tenantId, lastUpdateUserId, newParentId, newChildCount);
            }

            //update lai folder child by Id
            var childCountid = await _folderRepository.GetChildCount(folderInfo.Id);
            await _folderRepository.UpdateChildCount(tenantId, lastUpdateUserId, folderInfo.Id, childCountid);

            //// Update survey group name translation in survey translation.
            //if (surveyGroupMeta.SurveyGroupTranslations.Any())
            //    await UpdateSurveyTranslations();

            return new ActionResultResponse(1,
                _fileManagermentResourceResource.GetString("Update folder name successful."));
        }

        public async Task<ActionResultResponse> Delete(string tenantId, string userId, int folderId)
        {
            var folderInfo = await _folderRepository.GetInfo(tenantId, userId, folderId);
            if (folderInfo == null)
                return new ActionResultResponse(-1, _fileManagermentResourceResource.GetString("Folder does not exists. Please try again."));

            if (folderInfo.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            //// Check is has child.
            //var folderChildCount = await _folderRepository.GetChildCount(folderInfo.Id);
            //if (folderChildCount > 0)
            //    return new ActionResultResponse(-3,
            //        _fileManagermentResourceResource.GetString("This folder has children folder. You can not delete this folder."));

            //// Check is has File
            //var existsFileInFolder = await _fileRepository.CheckExistsByFolderId(folderId);
            //if (existsFileInFolder)
            //    return new ActionResultResponse(-4,
            //        _fileManagermentResourceResource.GetString("This folder has children file. You can not delete this folder."));

            var listFolderChildren = await _folderRepository.GetAllFolderChildren(tenantId, userId, folderInfo.IdPath);
            var listFolderChildrenId = listFolderChildren.Select(x => x.Id).ToList();
            var listFileByFolderId = await _fileRepository.GetFileByListFolder(tenantId, userId, listFolderChildrenId);

            var result = await _folderRepository.Delete(tenantId, userId, folderInfo.Id);
            if (result > 0)
            {
                if (folderInfo.ParentId.HasValue)
                {
                    //Update parent folder child count.
                    var childCount = await _folderRepository.GetChildCount(folderInfo.ParentId.Value);
                    await _folderRepository.UpdateChildCount(tenantId, userId, folderInfo.ParentId.Value, childCount);
                }

                if (listFileByFolderId != null && listFileByFolderId.Any())
                    await _fileRepository.Deletes(listFileByFolderId);

                if (listFolderChildren != null && listFolderChildren.Any())
                    await _folderRepository.Deletes(listFolderChildren);
            }

            return new ActionResultResponse(result, result > 0
                ? _fileManagermentResourceResource.GetString("Delete folder successful.")
                : _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
        }

        public async Task<ActionResultResponse<Folder>> GetDetail(string tenantId, string userId, int folderId)
        {
            var folderInfo = await _folderRepository.GetInfo(tenantId, userId, folderId, true);
            return new ActionResultResponse<Folder>
            {
                Data = folderInfo,
            };
        }

        public async Task<List<FolderViewModel>> GetFolderByParentId(string tenantId, string userId, int? parentId)
        {
            return await _folderRepository.GetFolderByParentId(tenantId, userId, parentId);
        }

        public async Task<List<FolderViewModel>> GetFolderShareByParentId(string tenantId, string userId, int? parentId)
        {
            var result = await _folderRepository.GetFolderShareByParentId(tenantId, userId, parentId);

            return result;
        }

        public async Task<List<FolderViewModel>> GetAll(string tenantId, string userId)
        {
            return await _folderRepository.GetAll(tenantId, userId);
        }

        public async Task<ActionResultResponse> UpdateName(string tenantId, string lastUpdateUserId, string lastUpdateFullName, int folderId,
            string concurrencyStamp, string name)
        {
            if (string.IsNullOrEmpty(name))
                return new ActionResultResponse(-4,
                   _sharedResourceService.GetString("The folder name is empty."));

            var folderInfo = await _folderRepository.GetInfo(tenantId, lastUpdateUserId, folderId);
            if (folderInfo == null)
                return new ActionResultResponse(-2, _fileManagermentResourceResource.GetString("Folder info does not exists. Please try again."));

            if (folderInfo.TenantId != tenantId)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (folderInfo.ConcurrencyStamp != concurrencyStamp)
                return new ActionResultResponse(-4,
                    _sharedResourceService.GetString("The folder already updated by other people. You can not update this folder."));

            folderInfo.Name = name;
            folderInfo.ConcurrencyStamp = Guid.NewGuid().ToString();
            folderInfo.LastUpdate = DateTime.Now;
            folderInfo.LastUpdateUserId = lastUpdateUserId;
            folderInfo.LastUpdateFullName = lastUpdateFullName;
            folderInfo.UnsignName = name.Trim().StripVietnameseChars().ToUpper();

            await _folderRepository.Update(folderInfo);

            return new ActionResultResponse(1,
                _fileManagermentResourceResource.GetString("Update folder name successful."));
        }

        public async Task<List<Breadcrumb>> GetBreadcrumbs(string tenantId, string userId, int? folderId)
        {
            if (!folderId.HasValue)
                return null;

            var folderInfo = await _folderRepository.GetInfo(tenantId, userId, folderId.Value, true);
            if (folderInfo == null)
                return null;

            return await _folderRepository.GetBreadcrumbByIdPath(tenantId, userId, folderInfo.IdPath);
        }

        public async Task<ActionResultResponse> ShareFolderOrFile(string tenantId, string userId, string fullName,
            FolderShareMeta folderShareMeta)
        {
            if ((folderShareMeta.ShareLevel == ShareLevelConst.NoShareWithPerson || folderShareMeta.ShareLevel == ShareLevelConst.ShareWithPerson)
                && (folderShareMeta.UserShares == null || !folderShareMeta.UserShares.Any()))
                return new ActionResultResponse(-1, folderShareMeta.ShareLevel == ShareLevelConst.NoShareWithPerson ? "Vui lòng chọn nhân viên không được chia sẻ"
                                : "Vui lòng chọn nhân viên được chia sẻ");

            if (folderShareMeta.FromDate.HasValue && folderShareMeta.ToDate.HasValue &&
                DateTime.Compare(folderShareMeta.ToDate.Value, folderShareMeta.FromDate.Value) < 0)
                return new ActionResultResponse(-2, "Ngày bắt đâu không đước lớn hơn ngày kết thúc.");

            var listStringUserShares = new List<string>();
            var apiUrls = _configuration.GetApiUrl();
            if (apiUrls == null)
                return new ActionResultResponse<string>(-5,
                    _sharedResourceService.GetString(
                        "Missing some configuration. Please contact with administrator."));

            foreach (var user in folderShareMeta.UserShares)
            {
                if (user.UserId != userId)
                {
                    listStringUserShares.Add(user.UserId);
                }
            }

            var resultUsers = await new HttpClientService()
                .PostAsync<List<ShortUserInfoViewModel>>($"{apiUrls.HrApiUrl}/users/{tenantId}/short-user-info",
                    listStringUserShares);

            if ((resultUsers == null || !resultUsers.Any())
                && folderShareMeta.UserShares != null && folderShareMeta.UserShares.Any())
                return new ActionResultResponse<string>(-6,
                    _sharedResourceService.GetString("Có gì đó không đúng. Vui lòng liên hệ với quản trị viên"));

            if (folderShareMeta.Type == ShareType.File)
            {
                var fileInfo = await _fileRepository.GetInfo(tenantId, folderShareMeta.FileId);
                if (fileInfo == null)
                    return new ActionResultResponse(-1, _fileManagermentResourceResource.GetString("Tập tin không tồn tại"));

                if (fileInfo != null && fileInfo.FolderId.HasValue)
                {
                    var folderInfo = await _folderRepository.GetInfo(tenantId, userId, fileInfo.FolderId.Value, true);
                    if (folderInfo == null)
                        return new ActionResultResponse(-1, "Thu muc khong ton tai");

                }
                await ShareFile(fileInfo, null);
            }

            #region shareFolder
            if (folderShareMeta.Type == ShareType.Folder)
            {
                if (!folderShareMeta.FolderId.HasValue)
                    return new ActionResultResponse(-2, _fileManagermentResourceResource.GetString("Vui lòng chọn thư mục chia sẻ"));

                var folderInfo = await _folderRepository.GetInfo(tenantId, userId, folderShareMeta.FolderId.Value);
                if (folderInfo == null)
                    return new ActionResultResponse(-3, _fileManagermentResourceResource.GetString("Thư mục chia sẻ không tồn tại"));

                await ShareFolder(folderInfo, null);

                var listFolderChildren = await _folderRepository.GetAllFolderChildren(tenantId, userId, folderInfo.IdPath);

                if (listFolderChildren != null && listFolderChildren.Any())
                {
                    var listFolderChildrenId = listFolderChildren.Select(x => x.Id);
                    listFolderChildrenId = listFolderChildrenId.Append(folderInfo.Id);
                    var listFileByFolderId = await _fileRepository.GetFileByListFolder(tenantId, userId, listFolderChildrenId.ToList());

                    foreach (var folder in listFolderChildren)
                    {
                        await ShareFolder(folder, folder.ParentId);
                    }

                    if (listFileByFolderId != null && listFileByFolderId.Any())
                    {
                        foreach (var file in listFileByFolderId)
                        {
                            await ShareFile(file, file.FolderId);
                        }
                    }
                }

                //var listFolderParent = await _folderRepository.GetAllFolderParent(tenantId, userId, folderInfo.IdPath);
                //if(listFolderChildren != null && listFolderChildren.Any())
                //{
                //    foreach (var folder in listFolderParent)
                //    {
                //        await ShareFolder(folder);
                //    }
                //}
            }

            return new ActionResultResponse(1, folderShareMeta.Type == ShareType.File ? "Chia sẻ file thành công"
                : "Chia sẻ thư mục thành công");

            #endregion
            async Task<ActionResultResponse> ShareFolder(Folder folder, int? parentId)
            {
                var oldShareLevel = folder.ShareLevel;
                folder.ShareLevel = folderShareMeta.ShareLevel;
                var result = await _folderRepository.Update(folder);

                await DeleteListUserShare(string.Empty, folder.Id, ShareType.Folder);
                if (folderShareMeta.ShareLevel == ShareLevelConst.NoShareWithPerson
                    || folderShareMeta.ShareLevel == ShareLevelConst.ShareWithPerson)
                {
                    await InsertListUserShare(string.Empty, folder.Id, parentId, ShareType.Folder);
                }

                return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                    : _fileManagermentResourceResource.GetString("Cập nhật chia sẻ thành công"));
            }

            async Task<ActionResultResponse> ShareFile(File fileInfo, int? folderId)
            {
                var oldShareLevel = fileInfo.ShareLevel;
                fileInfo.ShareLevel = folderShareMeta.ShareLevel;
                var result = await _fileRepository.Update(fileInfo);

                await DeleteListUserShare(fileInfo.Id, folderId, ShareType.File);
                if (folderShareMeta.ShareLevel == ShareLevelConst.NoShareWithPerson
                    || folderShareMeta.ShareLevel == ShareLevelConst.ShareWithPerson)
                {
                    await InsertListUserShare(fileInfo.Id, folderId, null, ShareType.File);
                }

                return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                    : _fileManagermentResourceResource.GetString("Cập nhật chia sẻ thành công"));
            }

            async Task<ActionResultResponse> InsertListUserShare(string fileId, int? folderId,
                int? folderParentId, ShareType type)
            {
                var listUserShare = new List<UserShare>();

                foreach (var userShare in resultUsers)
                {
                    var userShareInsert = new UserShare
                    {
                        TenantId = tenantId,
                        UserShareId = userId,
                        UserShareFullName = fullName,
                        ReciverFullName = userShare.FullName,
                        ReciverUserId = userShare.UserId,
                        ReciverUserName = userShare.UserName,
                        ReciverAvatar = userShare.Avatar,
                        ReciverPositionName = userShare.PositionName,
                        ReciverTitleName = userShare.TitleName,
                        ReciverOfficeName = userShare.OfficeName,
                        ReciverOfficeId = userShare.OfficeId,
                        ReciverTitleId = userShare.TitleId,
                        ReciverPositionId = userShare.PositionId,
                        Type = type,
                        FolderId = folderId,
                        FileId = fileId,
                        IsShare = folderShareMeta.ShareLevel == ShareLevelConst.NoShareWithPerson ? false : true,
                        FromDate = folderShareMeta.FromDate,
                        ToDate = folderShareMeta.ToDate,
                        FolderParentId = folderParentId,
                    };

                    listUserShare.Add(userShareInsert);
                }

                var result = await _userShareRepository.Inserts(listUserShare);
                if (result <= 0)
                    return new ActionResultResponse(-4, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

                return new ActionResultResponse(1, _fileManagermentResourceResource.GetString("Cập nhật chia sẻ thành công"));
            }

            async Task DeleteListUserShare(string fileId, int? folderId, ShareType type)
            {
                await _userShareRepository.DeleteListUserShare(tenantId, fileId, folderId, type);
            }
        }

        public async Task<ActionResultResponse<FolderShareViewModel>> GetFolderOrFileWidthUserShare(string tenantId,
            string userId, ShareType shareType, string fileId, int? folderId)
        {
            var listUserShare = await _userShareRepository.GetListUserShareViewModel(tenantId, fileId, folderId, shareType);

            if (shareType == ShareType.File)
            {
                var fileInfo = await _fileRepository.GetInfo(tenantId, fileId, true);
                return new ActionResultResponse<FolderShareViewModel>
                {
                    Data = new FolderShareViewModel
                    {
                        FileId = fileInfo.Id,
                        ShareLevel = fileInfo.ShareLevel,
                        Type = ShareType.File,
                        FolderId = fileInfo.FolderId,
                        UserShares = listUserShare,
                        FromDate = listUserShare.FirstOrDefault()?.FromDate,
                        ToDate = listUserShare.FirstOrDefault()?.ToDate,
                    }
                };
            }

            var folderInfo = await _folderRepository.GetInfo(tenantId, userId, folderId.Value, true);
            return new ActionResultResponse<FolderShareViewModel>
            {
                Data = new FolderShareViewModel
                {
                    FileId = fileId,
                    ShareLevel = folderInfo.ShareLevel,
                    Type = ShareType.Folder,
                    FolderId = folderInfo.Id,
                    UserShares = listUserShare,
                    FromDate = listUserShare.FirstOrDefault()?.FromDate,
                    ToDate = listUserShare.FirstOrDefault()?.ToDate,
                }
            };
        }
    }
}
