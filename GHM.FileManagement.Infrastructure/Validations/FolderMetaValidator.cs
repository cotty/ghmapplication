﻿using FluentValidation;
using GHM.FileManagement.Domain.ModelMetas;
using GHM.FileManagement.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Helpers.Validations;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using System;

namespace GHM.FileManagement.Infrastructure.Validations
{
    public class FolderMetaValidator : AbstractValidator<FolderMeta>
    {
        public FolderMetaValidator(IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmFileManagementResource> resourceService)
        {
            RuleFor(x => x.Name)
                .NotNullAndEmpty(resourceService.GetString("Tên thư mục không được để trống"))
                .MaximumLength(256).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength, resourceService.GetString("Tên thư mục"), 256));            
        }
    }
}
