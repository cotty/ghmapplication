﻿using FluentValidation;
using GHM.FileManagement.Domain.ModelMetas;
using GHM.FileManagement.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Helpers.Validations;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;

namespace GHM.FileManagement.Infrastructure.Validations
{
    public class FolderShareMetaValidator : AbstractValidator<FolderShareMeta>
    {
        public FolderShareMetaValidator(IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmFileManagementResource> resourceService)
        {
            RuleFor(x => x.Type)
                .NotNull().WithMessage(sharedResourceService.GetString(ValidatorMessage.PleaseSelect, resourceService.GetString("Loại chia sẻ")))
                .IsInEnum().WithMessage(sharedResourceService.GetString(ValidatorMessage.InValid, resourceService.GetString("Loại chia sẻ")));

            RuleFor(x => x.ShareLevel)
               .NotNull().WithMessage(sharedResourceService.GetString(ValidatorMessage.PleaseSelect, resourceService.GetString("Mức độ chia sẻ")))
               .IsInEnum().WithMessage(sharedResourceService.GetString(ValidatorMessage.InValid, resourceService.GetString("Mức độ chia sẻ")));

            RuleFor(x => x.FolderId)
                .MustBeNumber(sharedResourceService.GetString(ValidatorMessage.MustBeNumber, resourceService.GetString("Mã thư mục")));

            RuleFor(x => x.FileId)
                .MaximumLength(50).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength, resourceService.GetString("Mã file"), 50));

            RuleFor(x => x.FromDate)
                .MustBeValidDate(sharedResourceService.GetString(ValidatorMessage.InValid, resourceService.GetString("Từ ngày")));

            RuleFor(x => x.ToDate)
               .MustBeValidDate(sharedResourceService.GetString(ValidatorMessage.InValid, resourceService.GetString("Đến ngày")));
        }
    }
}
