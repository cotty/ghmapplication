﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.FileManagement.Domain.Constants;
using GHM.FileManagement.Domain.IRepositories;
using GHM.FileManagement.Domain.Models;
using GHM.FileManagement.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;
using File = GHM.FileManagement.Domain.Models.File;

namespace GHM.FileManagement.Infrastructure.Repositories
{
    public class FileRepository : RepositoryBase, IFileRepository
    {
        private readonly IRepository<File> _fileRepository;

        public FileRepository(IDbContext context) : base(context)
        {
            _fileRepository = Context.GetRepository<File>();
        }

        public async Task<File> GetInfo(string tenantId, string fileId, bool isReadOnly = false)
        {
            return await _fileRepository.GetAsync(isReadOnly, x => x.Id == fileId);
        }

        public async Task<int> Delete(string tenantId, string userId, string fileId)
        {
            var fileInfo = await GetInfo(tenantId, fileId);
            if (fileInfo == null)
                return -1;

            fileInfo.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(string tenantId, string userId, string fileId)
        {
            var fileInfo = await GetInfo(tenantId, fileId);
            if (fileInfo == null)
                return -1;

            _fileRepository.Delete(fileInfo);
            return await Context.SaveChangesAsync();
        }

        public async Task<List<FileViewModel>> SearchAnyFile(string tenantId, string userId, string keyword)
        {
            Expression<Func<File, bool>> spec = f => !f.IsDelete && f.TenantId == tenantId && f.CreatorId == userId;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            return await Context.Set<File>().Where(spec).Select(x => new FileViewModel
            {
                Id = x.Id,
                Name = x.Name,
                Extension = x.Extension,
                Type = x.Type,
                Url = x.Url,
                CreateTime = x.CreateTime,
                ConcurrencyStamp = x.ConcurrencyStamp,
                CreatorId = x.CreatorId,
                CreatorAvatar = x.CreatorAvatar,
                CreatorFullName = x.CreatorFullName,
                FolderId = x.FolderId,
                LastUpdate = x.LastUpdate,
                Size = x.Size,
                ShareLevel = x.ShareLevel
            }).ToListAsync();
        }

        public async Task<int> Insert(List<File> files)
        {
            _fileRepository.Creates(files);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(File file)
        {
            return await Context.SaveChangesAsync();
        }

        public Task<List<FileViewModel>> Search(string tenantId, string userId, string keyword, int? folderId, int page,
            int pageSize, out int totalRows)
        {
            Expression<Func<File, bool>> spec = t => !t.IsDelete && t.TenantId == tenantId && (!t.IsTrash.HasValue || !t.IsTrash.Value)
                                            && t.CreatorId == userId;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (folderId.HasValue)
            {
                spec = spec.And(x => x.FolderId == folderId);
            }

            var query = Context.Set<File>().Where(spec)
                .Select(x => new FileViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Extension = x.Extension,
                    Type = x.Type,
                    Url = x.Url,
                    CreatorId = x.CreatorId,
                    CreatorFullName = x.CreatorFullName,
                    CreatorAvatar = x.CreatorAvatar,
                    CreateTime = x.CreateTime,
                    ConcurrencyStamp = x.ConcurrencyStamp,
                    FolderId = x.FolderId,
                    LastUpdate = x.LastUpdate,
                    Size = x.Size,
                    ShareLevel = x.ShareLevel
                }).AsNoTracking();

            totalRows = _fileRepository.Count(spec);
            return query
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<bool> CheckExistsByFolderId(int? folderId)
        {
            return await _fileRepository.ExistAsync(x => x.FolderId == folderId && !x.IsDelete);
        }

        public async Task<List<FileViewModel>> GetAll(string tenantId, string userId, int? folderId)
        {
            Expression<Func<File, bool>> spec = f => !f.IsDelete && f.TenantId == tenantId && f.CreatorId == userId
                && f.FolderId == folderId && (!f.IsTrash.HasValue || !f.IsTrash.Value);

            var query = await Context.Set<File>().Where(spec).Select(x => new FileViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Extension = x.Extension,
                Type = x.Type,
                Url = x.Url,
                CreatorId = x.CreatorId,
                CreatorFullName = x.CreatorFullName,
                CreatorAvatar = x.CreatorAvatar,
                CreateTime = x.CreateTime,
                ConcurrencyStamp = x.ConcurrencyStamp,
                FolderId = x.FolderId,
                LastUpdate = x.LastUpdate,
                Size = x.Size,
                ShareLevel = x.ShareLevel
            }).AsNoTracking().ToListAsync();

            return query;
        }

        public async Task<List<File>> GetFileByListFolder(string tenantId, string userId, List<int> folderIds)
        {
            return await _fileRepository.GetsAsync(false, x => x.TenantId == tenantId && !x.IsDelete
            && x.FolderId.HasValue && folderIds.Contains(x.FolderId.Value));
        }

        public async Task<File> GetInfoByUrl(string tenantId, string id, string url, bool isReadOnly)
        {
            return await _fileRepository.GetAsync(isReadOnly, x => x.TenantId == tenantId && x.Url == url);
        }

        public async Task<int> Deletes(List<File> files)
        {
            _fileRepository.Deletes(files);
            return await Context.SaveChangesAsync();
        }

        public async Task<List<FileViewModel>> GetFileShare(string tenantId, string userId, int? folderId)
        {
            Expression<Func<File, bool>> spec = f => !f.IsDelete && f.TenantId == tenantId
                     && (!f.IsTrash.HasValue || !f.IsTrash.Value) && f.CreatorId != userId && f.FolderId == folderId;

            //var testQuery = from file in Context.Set<File>().Where(spec)
            //                join userSahre in Context.Set<UserShare>().Where(x => x.TenantId == tenantId && x.Type == ShareType.File
            //                && (!x.ToDate.HasValue || x.ToDate.Value >= DateTime.Now)
            //                && (!x.FromDate.HasValue || x.FromDate.Value <= DateTime.Now)
            //                && ((x.ReciverUserId == userId && (x.IsShare == true))
            //                        || ((x.IsShare == false) && x.ReciverUserId != userId)))
            //                on file.Id equals userSahre.FileId
            //                select file;

            //var resulttest =await testQuery.ToListAsync();

            var result = from file in Context.Set<File>().Where(spec)
                         join userSahre in Context.Set<UserShare>().Where(x => x.TenantId == tenantId && x.Type == ShareType.File
                         && (!x.ToDate.HasValue || x.ToDate.Value >= DateTime.Now)
                         && (!x.FromDate.HasValue || x.FromDate.Value <= DateTime.Now)
                         && ((x.ReciverUserId == userId && x.IsShare == true)
                                 || (x.IsShare == false && x.ReciverUserId != userId)))
                         on file.Id equals userSahre.FileId into g
                         from userShare in g.DefaultIfEmpty()
                         where (file.ShareLevel == ShareLevelConst.Public
                              || (file.ShareLevel == ShareLevelConst.ShareWithPerson && userShare.ReciverUserId == userId && (userShare.IsShare == true))
                              || (file.ShareLevel == ShareLevelConst.NoShareWithPerson && userShare.ReciverUserId != userId && (userShare.IsShare == false)))
                         select new FileViewModel
                         {
                             Id = file.Id,
                             Name = file.Name,
                             Extension = file.Extension,
                             Type = file.Type,
                             Url = file.Url,
                             CreatorId = file.CreatorId,
                             CreatorFullName = file.CreatorFullName,
                             CreatorAvatar = file.CreatorAvatar,
                             CreateTime = file.CreateTime,
                             ConcurrencyStamp = file.ConcurrencyStamp,
                             FolderId = file.FolderId,
                             LastUpdate = file.LastUpdate,
                             Size = file.Size,
                             ShareLevel = file.ShareLevel
                         };

            return await result.AsNoTracking().ToListAsync();
        }
    }
}
