﻿using GHM.FileManagement.Domain.Constants;
using GHM.FileManagement.Domain.IRepositories;
using GHM.FileManagement.Domain.Models;
using GHM.FileManagement.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GHM.FileManagement.Infrastructure.Repositories
{
    public class FolderRepository : RepositoryBase, IFolderRepository
    {
        private readonly IRepository<Folder> _folderRepository;

        public FolderRepository(IDbContext context) : base(context)
        {
            _folderRepository = Context.GetRepository<Folder>();
        }

        public async Task<bool> CheckExists(string tenantId, int? folderId)
        {
            return await _folderRepository.ExistAsync(x => x.TenantId == tenantId && x.Id == folderId && !x.IsDelete);
        }

        public async Task<bool> CheckName(string tenantId, int? parentId, string userId, string unsignName)
        {
            return await _folderRepository.ExistAsync(x => x.TenantId == tenantId && x.UnsignName.Equals(unsignName)
            && !x.IsDelete && x.CreatorId == userId && x.ParentId == parentId);
        }

        public async Task<int> Insert(Folder folder)
        {
            _folderRepository.Create(folder);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(Folder folder)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateFolderIdPath(int? folderId, string idPath)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateChildrenIdPath(string oldIdPath, string newIdPath)
        {
            List<Folder> childrenSurveyGroup = await _folderRepository.GetsAsync(false, x => !x.IsDelete && x.IdPath.StartsWith(oldIdPath + "."));
            if (childrenSurveyGroup == null || !childrenSurveyGroup.Any())
            {
                return -1;
            }

            foreach (Folder surveyGroup in childrenSurveyGroup)
            {
                surveyGroup.IdPath = $"{newIdPath}.{surveyGroup.Id}";
            }
            return await Context.SaveChangesAsync();
        }

        public async Task<Folder> GetInfo(string tenantId, string userId, int folderId, bool isReadOnly = false)
        {
            return await _folderRepository.GetAsync(isReadOnly, x => !x.IsDelete && x.Id == folderId && x.TenantId == tenantId);
        }

        public async Task<Folder> GetInfo(string tenantId, string userId, string folderIdPath, bool isReadOnly = false)
        {
            return await _folderRepository.GetAsync(isReadOnly, x => x.IdPath == folderIdPath && !x.IsDelete
                && x.TenantId == tenantId && x.CreatorId == userId);
        }

        public async Task<int> Delete(string tenantId, string userId, int folderId)
        {
            Folder info = await GetInfo(tenantId, userId, folderId);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(string tenantId, string userId, int folderId)
        {
            Folder info = await GetInfo(tenantId, userId, folderId);
            if (info == null)
                return -1;

            int folderChildCount = await GetChildCount(folderId);
            if (folderChildCount > 0)
                return -2;

            _folderRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> GetChildCount(int? folderId)
        {
            return await _folderRepository.CountAsync(x => x.ParentId == folderId && !x.IsDelete);
        }

        public async Task<List<FolderViewModel>> GetFolderByParentId(string tenantId, string userId, int? parentId)
        {
            return await Context.Set<Folder>().Where(x => x.TenantId == tenantId && x.CreatorId == userId
                    && !x.IsDelete && x.ParentId == parentId)
                .Select(x => new FolderViewModel
                {
                    Id = x.Id,
                    IdPath = x.IdPath,
                    Name = x.Name,
                    ParentId = x.ParentId,
                    CreatorId = x.CreatorId,
                    CreatorFullName = x.CreatorFullName,
                    CreatorAvatar = x.CreatorAvatar,
                    CreateTime = x.CreateTime,
                    ChildCount = x.ChildCount,
                    ConcurrencyStamp = x.ConcurrencyStamp,
                    ShareLevel = x.ShareLevel
                }).AsNoTracking().ToListAsync();
        }

        public async Task<List<FolderViewModel>> GetAll(string tenantId, string userId)
        {
            return await Context.Set<Folder>().Where(x => x.TenantId == tenantId && x.CreatorId == userId && !x.IsDelete)
                .Select(x => new FolderViewModel
                {
                    Id = x.Id,
                    IdPath = x.IdPath,
                    Name = x.Name,
                    ParentId = x.ParentId,
                    ChildCount = x.ChildCount,
                    ShareLevel = x.ShareLevel
                }).AsNoTracking().ToListAsync();
        }

        public Task<List<FolderViewModel>> Search(string tenantId, string userId, string keyword, int page, int pageSize, out int totalRows)
        {
            Expression<Func<Folder, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId && x.CreatorId == userId;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            IQueryable<FolderViewModel> query = Context.Set<Folder>().Where(spec)
                .Select(x => new FolderViewModel
                {
                    Id = x.Id,
                    IdPath = x.IdPath,
                    Name = x.Name,
                    ParentId = x.ParentId,
                    CreatorId = x.CreatorId,
                    CreatorAvatar = x.CreatorAvatar,
                    CreatorFullName = x.CreatorFullName,
                    ChildCount = x.ChildCount,
                    CreateTime = x.CreateTime,
                    ShareLevel = x.ShareLevel
                }).AsNoTracking();

            totalRows = query.Count();
            return query
                .OrderBy(x => x.IdPath).Skip((page - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<int> UpdateChildCount(string tenantId, string userId, int folderId, int childCount)
        {
            Folder info = await GetInfo(tenantId, userId, folderId);
            if (info == null)
                return -1;

            info.ChildCount = childCount;
            return await Context.SaveChangesAsync();
        }

        public Task<List<Breadcrumb>> GetBreadcrumbByIdPath(string tenantId, string userId, string idPath)
        {
            Expression<Func<Folder, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId && x.CreatorId == userId
                                                       && (idPath.StartsWith(x.IdPath + ".") || x.IdPath == idPath);

            var query = Context.Set<Folder>().Where(spec)
                .Select(x => new
                {
                    x.Id,
                    x.IdPath,
                    x.Name,
                }).AsNoTracking().OrderBy(x => x.IdPath);

            return query.Select(x => new Breadcrumb()
            {
                Id = x.Id,
                Name = x.Name,
            }).ToListAsync();
        }

        public async Task<List<Folder>> GetAllFolderChildren(string tenantId, string userId, string idPath, bool isReadOnly = false)
        {
            Expression<Func<Folder, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId && x.CreatorId == userId
                                                       && (x.IdPath.StartsWith(idPath + "."));

            return await _folderRepository.GetsAsync(false, spec);
        }

        public async Task<List<Folder>> GetAllFolderParent(string tenantId, string userId, string idPath, bool isReadOnly = false)
        {
            Expression<Func<Folder, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId && x.CreatorId == userId
                                                       && idPath.StartsWith(x.IdPath + ".");

            return await _folderRepository.GetsAsync(false, spec);
        }

        public async Task<int> Deletes(List<Folder> folders)
        {
            _folderRepository.Deletes(folders);
            return await Context.SaveChangesAsync();
        }

        public async Task<List<FolderViewModel>> GetFolderShareByParentId(string tenantId, string userId, int? parentId)
        {
            var result = from folder in Context.Set<Folder>().Where(x=> x.TenantId == tenantId && !x.IsDelete
                               && x.CreatorId != userId && x.ParentId == parentId)
                         join userSahre in Context.Set<UserShare>().Where(x => x.TenantId == tenantId
                               && x.Type == ShareType.Folder
                               //&& x.FolderParentId == parentId
                               && (!x.ToDate.HasValue || x.ToDate.Value >= DateTime.Now)
                               && (!x.FromDate.HasValue || x.FromDate.Value <= DateTime.Now)
                               && ((x.ReciverUserId == userId && x.IsShare.Value)
                                 || (!x.IsShare.Value
                                && x.ReciverUserId != userId)))
                         on folder.Id equals userSahre.FolderId into g
                         from userShare in g.DefaultIfEmpty()
                         where folder.ShareLevel == ShareLevelConst.Public
                              || (folder.ShareLevel == ShareLevelConst.ShareWithPerson && userShare.ReciverUserId == userId
                                 && (userShare.IsShare == true))
                              || (folder.ShareLevel == ShareLevelConst.NoShareWithPerson
                              && (userShare.IsShare == false)
                              && userShare.ReciverUserId != userId)

                         select new FolderViewModel
                         {
                             Id = folder.Id,
                             IdPath = folder.IdPath,
                             Name = folder.Name,
                             ParentId = folder.ParentId,
                             CreatorId = folder.CreatorId,
                             CreatorFullName = folder.CreatorFullName,
                             CreatorAvatar = folder.CreatorAvatar,
                             CreateTime = folder.CreateTime,
                             ChildCount = folder.ChildCount,
                             ConcurrencyStamp = folder.ConcurrencyStamp,
                             ShareLevel = folder.ShareLevel
                         };

            return await result.AsNoTracking().ToListAsync();
        }
    }
}
