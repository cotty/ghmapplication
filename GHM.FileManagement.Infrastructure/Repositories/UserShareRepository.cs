﻿using GHM.FileManagement.Domain.Constants;
using GHM.FileManagement.Domain.IRepositories;
using GHM.FileManagement.Domain.Models;
using GHM.FileManagement.Domain.ViewModels;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GHM.FileManagement.Infrastructure.Repositories
{
    public class UserShareRepository : RepositoryBase, IUserShareRepository
    {
        private readonly IRepository<UserShare> _userShareRepository;
        public UserShareRepository(IDbContext context) : base(context)
        {
            _userShareRepository = Context.GetRepository<UserShare>();
        }

        public async Task<bool> CheckExists(string tenantId, string userId, string fileId, int? folderId, ShareType type)
        {
            return await _userShareRepository.ExistAsync(x => x.TenantId == tenantId && x.ReciverUserId == userId
            && (string.IsNullOrEmpty(fileId) || x.FileId == fileId) && x.Type == type && (!folderId.HasValue || x.FolderId == folderId));
        }

        public async Task<int> Delete(string tenantId, string userId, string fileId, int? folderId, ShareType type)
        {
            var userShareInfo = await GetInfo(tenantId, userId, fileId, folderId, type);
            if (userShareInfo == null)
                return -1;

            _userShareRepository.Delete(userShareInfo);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeleteListUserShare(string tenantId, string fileId, int? folderId, ShareType type)
        {
            var listUserShare = await GetListUserShare(tenantId, fileId, folderId, type, false);
            if (listUserShare == null && !listUserShare.Any())
                return -1;

            _userShareRepository.Deletes(listUserShare);
            return await Context.SaveChangesAsync();
        }

        public async Task<UserShare> GetInfo(string tenantId, string userId, string fileId, int? folderId, ShareType type,
            bool isReadOnly = false)
        {
            return await _userShareRepository.GetAsync(isReadOnly, x => x.TenantId == tenantId 
            && (string.IsNullOrEmpty(fileId) || x.FileId == fileId)
            && (!folderId.HasValue || x.FolderId == folderId) && x.Type == type && x.ReciverUserId == userId);
        }

        public Task<List<UserShareViewModel>> GetListUserShareViewModel(string tenantId, string fileId,
            int? folderId, ShareType type)
        {
            Expression<Func<UserShare, bool>> spec = x => x.TenantId == tenantId 
            && (string.IsNullOrEmpty(fileId) || x.FileId == fileId) && (!folderId.HasValue || x.FolderId == folderId)
            && x.Type == type;
            var result = from userShare in Context.Set<UserShare>().Where(spec).AsNoTracking()
                         select new UserShareViewModel
                         {
                             UserId = userShare.ReciverUserId,
                             FullName = userShare.ReciverFullName,
                             PositionName = userShare.ReciverPositionName,
                             OfficeName = userShare.ReciverOfficeName,
                             TitleName = userShare.ReciverTitleName,
                             Avatar = userShare.ReciverAvatar,
                             FromDate = userShare.FromDate,
                             ToDate = userShare.ToDate,
                         };

            return result.ToListAsync();
        }

        public async Task<int> Insert(UserShare userShare)
        {
            _userShareRepository.Create(userShare);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<UserShare> userShares)
        {
            if (userShares == null || !userShares.Any())
                return -1;

            _userShareRepository.Creates(userShares);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(UserShare userShare)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<List<UserShare>> GetListUserShare(string tenantId, string fileId,
           int? folderId, ShareType type, bool isReadOnly = false)
        {
            return await _userShareRepository.GetsAsync(false, x => x.TenantId == tenantId
            && ((x.FileId == fileId && type == ShareType.File) || (x.FolderId == folderId && type == ShareType.Folder)));
        }
    }
}
