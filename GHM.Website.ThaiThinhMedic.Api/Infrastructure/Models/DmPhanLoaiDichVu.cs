﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GHM.Website.ThaiThinhMedic.Api.Infrastructure.Models
{
    public class DmPhanLoaiDichVu
    {
        public string MaPhanLoaiDichVu { get; set; }
        public string TenPhanLoaiDichVu { get; set; }
        public string MaLoaiDichVu { get; set; }
        public string GhiChu { get; set; }
        public int? Stt { get; set; }
    }
}
