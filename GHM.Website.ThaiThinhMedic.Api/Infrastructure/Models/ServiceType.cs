﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GHM.Website.ThaiThinhMedic.Api.Infrastructure.Models
{
    /// <summary>
    /// Danh mục Loại dịch vụ
    /// </summary>
    public class ServiceType
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
