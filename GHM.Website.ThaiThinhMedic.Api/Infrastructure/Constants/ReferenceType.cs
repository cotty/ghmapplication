﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GHM.Website.ThaiThinhMedic.Api.Infrastructure.Constants
{
    public enum ReferenceType
    {
        /// <summary>
        /// Tự nhập.
        /// </summary>
        Custom,
        /// <summary>
        /// Nhóm chuyên mục.
        /// </summary>
        Category,
        /// <summary>
        /// Tin tức.
        /// </summary>
        News
    }
}
