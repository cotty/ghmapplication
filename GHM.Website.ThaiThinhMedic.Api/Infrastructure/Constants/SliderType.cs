﻿namespace GHM.Website.ThaiThinhMedic.Api.Infrastructure.Constants
{
    public enum SliderType
    {
        /// <summary>
        /// Slider trang chủ.
        /// </summary>
        HomePageSlider,
        /// <summary>
        /// Popup trang chủ.
        /// </summary>
        HomePopup
    }
}
