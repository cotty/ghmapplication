﻿namespace GHM.Website.ThaiThinhMedic.Api.Infrastructure.Constants
{
    public enum DiscountType
    {
        Percent = 1,
        Money = 2
    }
}
