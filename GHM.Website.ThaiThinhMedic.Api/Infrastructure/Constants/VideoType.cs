﻿namespace GHM.Website.ThaiThinhMedic.Api.Infrastructure.Constants
{
    public enum VideoType
    {    
        YouTube,
        Vimeo,
        Custom
    }
}
