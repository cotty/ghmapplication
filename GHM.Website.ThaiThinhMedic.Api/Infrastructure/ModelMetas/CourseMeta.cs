﻿namespace GHM.Website.ThaiThinhMedic.Api.Infrastructure.ModelMetas
{
    public class CourseMeta
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public bool IsActive { get; set; }
    }
}
