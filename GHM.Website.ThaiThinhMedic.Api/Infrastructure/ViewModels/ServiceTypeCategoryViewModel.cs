﻿namespace GHM.Website.ThaiThinhMedic.Api.Infrastructure.ViewModels
{
    public class ServiceTypeCategoryViewModel
    {
        public string TypeId { get; set; }
        public string TypeName { get; set; }
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
