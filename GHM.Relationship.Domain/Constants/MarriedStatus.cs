﻿namespace GHM.Relationship.Domain.Constants
{
    /// <summary>
    /// Tình trạng hôn nhân:
    /// 0: Khác
   /// 1: Độc thân.
    /// 2: Đã kết hôn.
    /// 3: Ly hôn.

    /// </summary>
    public enum MarriedStatus
    {
        undefined,
        Single,
        Married,
        Divorce
    }
}
