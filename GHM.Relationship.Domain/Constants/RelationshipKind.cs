﻿
namespace GHM.Relationship.Domain.Constants
{
    /// <summary>
    /// Loại quan hệ ngang hàng , và cấp trên
    /// </summary>
    public enum RelationshipKind
    {
        /// <summary>
        ///Ngang hàng.
        /// </summary>
        Samelevel,
        /// <summary>
        ///cấp trên.
        /// </summary>
        Superior
     
    }
}
