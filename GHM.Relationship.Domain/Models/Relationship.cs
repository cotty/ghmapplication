﻿
using System;
using GHM.Relationship.Domain.Constants;

namespace GHM.Relationship.Domain.Models
{
    public class Relationship
    {
        /// <summary>
        /// Mã khách hàng sử dụng.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Mã khách hàng đang được thêm quan hệ ; VD là A
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Mã khách hàng Quan hệ : A là chồng B
        /// </summary>
        public string RelationalUserId { get; set; }

        /// <summary>
        /// Mã loại quan hệ
        /// </summary>
        public string RelationshipTypeId { get; set; }

        /// <summary>
        /// Tên người dùng quan hệ
        /// </summary>
        public string UserFullName { get; set; }

        /// <summary>
        /// Ngày sinh người dùng.
        /// </summary>
        public DateTime UserBirthday { get; set; }

        /// <summary>
        /// Giới tính người dùng.
        /// </summary>
        public Gender UserGender { get; set; }

        /// <summary>
        /// Tên người quan hệ.
        /// </summary>
        public string RelationalUserFullName { get; set; }

        /// <summary>
        /// Ngày sinh người quan hệ.
        /// </summary>
        public DateTime RelationalUserBirthday { get; set; }

        /// <summary>
        /// Giới tính người quan hệ.
        /// </summary>
        public Gender RelationalUserGender { get; set; }

        /// <summary>
        /// Tên không dấu để tìm kiếm
        /// </summary>
        public string UnsignName { get; set; }

        /// <summary>
        /// Mô tả quan hệ.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Trạng thái xóa.
        /// </summary>
        public bool IsDelete { get; set; }

        /// <summary>
        /// Trạng thái kích hoạt.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Mã kiêm tra khi update.
        /// </summary>
        public string ConcurrencyStamp { get; set; }

        /// <summary>
        /// Lần chỉnh sửa cuối cùng.
        /// </summary>
        public DateTime?  LastUpdate { get; set; }
    }
}
