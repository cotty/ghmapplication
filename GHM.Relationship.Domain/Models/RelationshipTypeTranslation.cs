﻿namespace GHM.Relationship.Domain.Models
{
    public class RelationshipTypeTranslation
    {
        /// <summary>
        /// Mã khách hàng.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Mã loại quan hệ.
        /// </summary>
        public string RelationshipTypeId { get; set; }

        /// <summary>
        /// Mã ngôn ngữ.
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// Tên loại quan hệ theo ngôn ngữ.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Mô tả loại quan hệ theo ngôn ngữ.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Tên không dấu phục vụ tìm kiếm.
        /// </summary>
        public string UnsignName { get; set; }

        /// <summary>
        ///  Đối tượng quan hệ tham chiếu phục vụ foreign key.
        /// </summary>
        public RelationshipType RelationshipType { get; set; }
    }
}
