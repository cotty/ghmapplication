﻿using System.Collections.Generic;
using GHM.Infrastructure.Models;
using GHM.Relationship.Domain.Constants;

namespace GHM.Relationship.Domain.Models
{
    public class RelationshipType : EntityBase<string>
    {
        /// <summary>
        /// Mã khách hàng.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Loại quan hệ.
        /// </summary>
        public RelationshipKind Kind { get; set; }

        /// <summary>
        /// Trạng thái xóa.
        /// </summary>
        public bool IsDelete { get; set; }

        /// <summary>
        /// Trạng thái kích hoạt.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Đối tượng quan hệ.
        /// </summary>
        public List<RelationshipTypeTranslation> RelationshipTypeTranslations { get; set; }
    }
}
