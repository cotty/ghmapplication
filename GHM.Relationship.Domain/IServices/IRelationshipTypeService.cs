﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Relationship.Domain.ModelMetas;
using GHM.Relationship.Domain.ViewModels;
using GHM.Infrastructure.Models;

namespace GHM.Relationship.Domain.IServices
{
    public interface IRelationshipTypeService
    {
        Task<ActionResultResponse> Insert(string tenantId, RelationshipTypeMeta typeRelationshipMeta);

        Task<ActionResultResponse> Update(string tenantId, string id, RelationshipTypeMeta typeRelationshipMeta);

        Task<ActionResultResponse> Delete(string tenantId, string id);

        //Task<ActionResultResponse> ForceDeleteDelete(string tenantId, string id);

        Task<List<RelationshipTypeSearchViewModel>> GetsAll(string tenantId, string languageId, bool? isActive);

        Task<ActionResultResponse<RelationshipTypeViewModel>> GetDetail(string tenantId, string id);

        Task<List<Suggestion<string>>> GetsForSuggestions(string tenantId, string languageId);
    }
}
