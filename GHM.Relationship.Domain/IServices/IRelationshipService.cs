﻿using System.Threading.Tasks;
using GHM.Relationship.Domain.ModelMetas;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;

namespace GHM.Relationship.Domain.IServices
{
  public  interface IRelationshipService
    {
        Task<ActionResultResponse> Insert(string tenantId, RelationshipMeta relationshipMetas);

        Task<ActionResultResponse> Update(string tenantId, string userId, string relationalUserId, RelationshipMeta relationshipMetas);

        Task<ActionResultResponse> Delete(string tenantId, string userId, string relationalUserId);

        Task<ActionResultResponse> ForceDelete(string tenantId, string userId, string relationalUserId);

        Task<SearchResult<Models.Relationship>> Search(string tenantId, string keyword, bool? isActive,int page, int pageSize);

        Task<ActionResultResponse<Domain.Models.Relationship>> GetDetail(string tenantId, string userId, string relationalUserId);
    }
}
