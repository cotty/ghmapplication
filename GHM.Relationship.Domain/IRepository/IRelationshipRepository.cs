﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Relationship.Domain.IRepository
{
 public   interface IRelationshipRepository
    {
        Task<Models.Relationship> GetInfo(string userId, string relationalUserId, bool isReadOnly = false);

        Task<int> Insert(Models.Relationship relationship);

        Task<int> Update(Models.Relationship relationship);

        Task<int> Delete(string userId, string relationalUserId);

        Task<int> ForceDelete(string userId, string relationalUserId);

        Task<bool> CheckExists(string userId, string relationalUserId);

        Task<List<Models.Relationship>> Search(string tenantId, string keyword,
            bool? isActive, int page, int pageSize, out int totalRows);
    }
}
