﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Relationship.Domain.Models;

namespace GHM.Relationship.Domain.IRepository
{
    public interface IRelationshipTypeTranslationRepository
    {
        Task<bool> CheckExists(string tenantId, string id, string languageId, string name);

        Task<int> Insert(RelationshipTypeTranslation relationshipTypeTranslation);

        Task<int> Inserts(List<RelationshipTypeTranslation> relationshipTypeTranslations);

        Task<int> Update(RelationshipTypeTranslation relationshipTypeTranslation);

        Task<int> ForceDeleteByRelationshipTypeId(string relationshipTypeId);

        Task<RelationshipTypeTranslation> GetInfo(string tenantId, string relationshipTypeId, string languageId, bool isReadOnly = false);

        Task<List<RelationshipTypeTranslation>> GetsByRelationshipTypeId(string relationshipTypeId);
    }
}
