﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.Models;
using GHM.Relationship.Domain.Models;
using GHM.Relationship.Domain.ViewModels;


namespace GHM.Relationship.Domain.IRepository
{
    public interface IRelationshipTypeRepository
    {
        Task<RelationshipType> GetInfo(string id, bool isReadOnly = false);

        Task<int> Insert(RelationshipType typeRelationship);

        Task<int> Update(RelationshipType typeRelationship);

        Task<int> Delete(string id);

        Task<int> ForceDelete(string id);

        Task<bool> CheckExists(string id, string tenantId);

        Task<List<RelationshipTypeSearchViewModel>> GetsAll(string tenantId, string languageId, bool? isActive);

        Task<List<T>> GetAllTypeRelationship<T>(Expression<Func<RelationshipType, T>> projector, string tenantId);

        Task<List<Suggestion<string>>> GetsForSuggestion(string tenantId, string languageId);
    }
}
