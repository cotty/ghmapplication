﻿
using System.Collections.Generic;
using GHM.Relationship.Domain.Constants;

namespace GHM.Relationship.Domain.ModelMetas
{
    public class RelationshipTypeMeta
    {
        public RelationshipKind Kind { get; set; }

        public bool IsActive { get; set; }

        public List<RelationshipTypeTranslationMeta> RelationshipTypeTranslations { get; set; }
    }
}
