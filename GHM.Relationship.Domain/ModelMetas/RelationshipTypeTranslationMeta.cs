﻿namespace GHM.Relationship.Domain.ModelMetas
{
    public class RelationshipTypeTranslationMeta
    {
        /// <summary>
        /// Mã ngôn ngữ.
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// Tên loại quan hệ theo ngôn ngữ.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Mô tả loại quan hệ theo ngôn ngữ.
        /// </summary>
        public string Description { get; set; }
    }
}
