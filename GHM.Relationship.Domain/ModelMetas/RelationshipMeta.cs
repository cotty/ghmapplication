﻿namespace GHM.Relationship.Domain.ModelMetas
{
  public  class RelationshipMeta
    {
        public string UserId { get; set; }

        public string RelationalUserId { get; set; }

        public string RelationshipTypeId { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public string ConcurrencyStamp { get; set; }
    }
}
