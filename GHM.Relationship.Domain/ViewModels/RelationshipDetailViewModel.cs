﻿using System;
using GHM.Relationship.Domain.Constants;

namespace GHM.Relationship.Domain.ViewModels
{
   public class RelationshipDetailViewModel
    {
        /// <summary>
        /// Mã khách hàng đang được thêm quan hệ ; VD là A
        /// </summary>
        public string CustomerCode { get; set; }
        /// <summary>
        /// Mã khách hàng Quan hệ : A là chồng B
        /// </summary>
        public string CustomerRelationshipCode { get; set; }
        /// <summary>
        ///Loại quan hệ
        /// </summary>
        public string IdTypeRelationship { get; set; }

        public string CustomerName { get; set; }

        public DateTime CustomerBirthday { get; set; }

        public Gender CustomerGender { get; set; }

        public string CustomerRelationshipName { get; set; }

        public DateTime CustomerRelationshipBirthday { get; set; }

        public Gender CustomerRelationshipGender { get; set; }

        public MarriedStatus MarriedStatus { get; set; }

        /// <summary>
        /// Tên không dấu để tìm kiếm
        /// </summary>
        public string UnsignName { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }
    }
}
