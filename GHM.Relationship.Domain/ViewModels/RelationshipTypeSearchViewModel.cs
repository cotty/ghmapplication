﻿using System;
using GHM.Relationship.Domain.Constants;

namespace GHM.Relationship.Domain.ViewModels
{
    public class RelationshipTypeSearchViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public RelationshipKind Kind { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
