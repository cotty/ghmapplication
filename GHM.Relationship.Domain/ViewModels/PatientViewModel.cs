﻿

using System;
using GHM.Relationship.Domain.Constants;

namespace GHM.Relationship.Domain.ViewModels
{
   public class PatientViewModel
    {
        public string PatientCode { get; set; }
        public string Name { get; set; }
        public DateTime Birthday { get; set; }
        public Gender Gender { get; set; }
    }
}
