﻿
using System.Collections.Generic;
using GHM.Relationship.Domain.Constants;
using GHM.Relationship.Domain.Models;

namespace GHM.Relationship.Domain.ViewModels
{
    public class RelationshipTypeViewModel
    {
        public string Id { get; set; }
        public RelationshipKind Kind { get; set; }
        public bool IsActive { get; set; }
        public List<RelationshipTypeTranslation> RelationshipTypeTranslations { get; set; }
    }
}
