﻿using GHM.FileManagement.Domain.Constants;
using GHM.FileManagement.Domain.Models;
using GHM.FileManagement.Domain.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.FileManagement.Domain.IRepositories
{
    public interface IUserShareRepository
    {
        Task<int> Inserts(List<UserShare> userShares);

        Task<int> Insert(UserShare userShare);

        Task<int> Update(UserShare userShare);

        Task<int> Delete(string tenantId, string userId, string fileId, int? folderId, ShareType type );

        Task<int> DeleteListUserShare(string tenantId, string fileId, int? folderId, ShareType type);

        Task<UserShare> GetInfo(string tenantId, string userId, string fileId, int? folderId, ShareType type, bool isReadOnly = false);

        Task<bool> CheckExists(string tenantId, string userId, string fileId, int? folderId, ShareType type);

        Task<List<UserShareViewModel>> GetListUserShareViewModel(string tenantId, string fileId, int? folderId, ShareType type);

        Task<List<UserShare>> GetListUserShare(string tenantId, string fileId, int? folderId, ShareType type, bool isReadOnly = false);
    }
}
