﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.FileManagement.Domain.Models;
using GHM.FileManagement.Domain.ViewModels;

namespace GHM.FileManagement.Domain.IRepositories
{
    public interface IFileRepository
    {
        Task<List<FileViewModel>> Search(string tenantId, string userId, string keyword, int? folderId, int page,
            int pageSize, out int totalRows);

        Task<int> Insert(List<File> files);

        Task<int> Deletes(List<File> files);

        Task<int> Update(File file);

        Task<int> Delete(string tenantId, string userId, string fileId);

        Task<int> ForceDelete(string tenantId, string userId, string fileId);

        Task<File> GetInfo(string tenantId, string fileId, bool isReadOnly = false);

        Task<bool> CheckExistsByFolderId(int? folderId);

        Task<List<FileViewModel>> GetAll(string tenantId, string userId, int? folderId);

        Task<List<FileViewModel>> GetFileShare(string tenantId, string userId, int? folderId);

        Task<List<File>> GetFileByListFolder(string tenantId, string userId, List<int> folderIds);

        Task<File> GetInfoByUrl(string tenantId, string id, string url, bool isReadOnly);
    }
}
