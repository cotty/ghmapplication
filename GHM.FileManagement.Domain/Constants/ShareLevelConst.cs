﻿namespace GHM.FileManagement.Domain.Constants
{
    public enum ShareLevelConst
    {
        Private,
        Public,
        ShareWithPerson,
        NoShareWithPerson
    }

    public enum ShareType
    {
        Folder,
        File
    }
}
