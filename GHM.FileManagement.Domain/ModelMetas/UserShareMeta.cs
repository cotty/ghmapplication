﻿using GHM.FileManagement.Domain.Constants;
using System;
using System.Collections.Generic;

namespace GHM.FileManagement.Domain.ModelMetas
{
    public class FolderShareMeta
    {
        public ShareType Type { get; set; } // 0 Thư mục, 1 File
        public ShareLevelConst ShareLevel { get; set; }
        public int? FolderId { get; set; }
        public string FileId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public List<UserShareMeta> UserShares { get; set; }
    }

    public class UserShareMeta
    {
        public string UserId { get; set; }
    }
}
