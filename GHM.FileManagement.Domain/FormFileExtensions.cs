﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace GHM.FileManagement.Domain
{
    public static class FormFileExtensions
    {
        public static string GetFilename(this IFormFile file)
        {
            var fileName = file.FileName.Split(".");
            if (fileName.Length > 1)
                return fileName[0];
            else
                return file.FileName;
        }

        public static async Task<MemoryStream> GetFileStream(this IFormFile file)
        {
            MemoryStream filestream = new MemoryStream();
            await file.CopyToAsync(filestream);
            return filestream;
        }

        public static async Task<byte[]> GetFileArray(this IFormFile file)
        {
            MemoryStream filestream = new MemoryStream();
            await file.CopyToAsync(filestream);
            return filestream.ToArray();
        }

        public static long GetFileSize(this IFormFile file)
        {
            var sizeFile = file.Length;
            return sizeFile;
        }

        public static string GetExtensionFile(this IFormFile file)
        {
            var exten = System.IO.Path.GetExtension(file.FileName).Split(".");
            if (exten.Length > 1)
                return exten[1];
            else
                return string.Empty;
        }

        public static string GetNameNotExtensionFile(this IFormFile file)
        {
            var exten = System.IO.Path.GetFileNameWithoutExtension(file.FileName);
            if (exten.Length > 1)
                return exten;
            else
                return string.Empty;
        }

        public static string GetTypeFile(this IFormFile file)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(file.FileName).ToLowerInvariant();
            if (!types.ContainsKey(ext))
                return string.Empty;
            return types[ext];
        }

        private static Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"},
                {".swf", "graphics/vector"}
            };
        }
    }
}
