﻿using GHM.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.FileManagement.Domain.ModelMetas;
using GHM.FileManagement.Domain.Models;
using GHM.FileManagement.Domain.ViewModels;
using GHM.Infrastructure.ViewModels;
using Microsoft.AspNetCore.Http;

namespace GHM.FileManagement.Domain.IServices
{
    public interface IFileService
    {
        Task<ActionResultResponse<List<FileViewModel>>> UploadFiles(string tenantId, string creatorId, string creatorFullName, string creatorAvatar,
            int? folderId, bool? isTrash, bool? isPublic, IFormFileCollection formFileCollection);

        Task<ActionResultResponse> Update(string tenantId, string userId, string fileId, FileMeta fileMeta);

        Task<ActionResultResponse> Delete(string tenantId, string userId, string fileId);

        Task<ActionResultResponse<File>> GetInfo(string tenantId, string fileId, bool isReadOnly = false);

        Task<List<FileViewModel>> GetsAll(string tenantId, string userId, int? folderId);

        Task<List<FileViewModel>> GetFildeShare(string tenantId, string userId, int? folderId);

        Task<File> GetInfoByUrl(string tenantId, string id, string url, bool isReadOnly = false);
    }
}
