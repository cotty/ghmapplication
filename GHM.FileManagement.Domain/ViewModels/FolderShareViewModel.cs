﻿using GHM.FileManagement.Domain.Constants;
using System;
using System.Collections.Generic;

namespace GHM.FileManagement.Domain.ViewModels
{
    public class FolderShareViewModel
    {
        public int? FolderId { get; set; }
        public string FileId { get; set; }
        public ShareLevelConst? ShareLevel { get; set; }
        public ShareType Type { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public List<UserShareViewModel> UserShares { get; set; }
    }

    public class UserShareViewModel
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string OfficeName { get; set; }
        public string PositionName { get; set; }
        public string TitleName { get; set; }
        public string Avatar { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
