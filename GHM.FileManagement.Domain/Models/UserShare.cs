﻿using GHM.FileManagement.Domain.Constants;
using System;

namespace GHM.FileManagement.Domain.Models
{
    public class UserShare
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public string UserShareId { get; set; } // Id người chia sẻ
        public string UserShareFullName { get; set; }
        public string ReciverFullName { get; set; }
        public string ReciverUserId { get; set; }
        public string ReciverUserName { get; set; }
        public string ReciverAvatar { get; set; }
        public string ReciverPositionName { get; set; }
        public string ReciverTitleName { get; set; }
        public string ReciverOfficeName { get; set; }
        public int? ReciverOfficeId { get; set; }
        public string ReciverTitleId { get; set; }
        public string ReciverPositionId { get; set; }
        public ShareType Type { get; set; } // 1:  File, 0: Folder
        public int? FolderId { get; set; }
        public string FileId { get; set; }
        public int? Role { get; set; } // Quyền
        public bool? IsShare { get; set; } // 0: Những người không được chia sẻ, 1: Những người được chía sẻ
        public DateTime? FromDate { get; set; } // Chia sẻ từ ngày
        public DateTime? ToDate { get; set; } // Chia sẻ đến ngày
        public int? FolderParentId { get; set; }

        public UserShare()
        {
            IsShare = true;
            Id = Guid.NewGuid().ToString();
        }
    }
}
