﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using FluentValidation.AspNetCore;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Infrastructure.AutofacModules;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace GHM.Timekeeping.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddCors();
            services.AddMvcCore()
                .AddAuthorization().AddJsonFormatters().AddFluentValidation();
            services.AddMediatR();

            var authority = Configuration.GetSection("Authority");
            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = !string.IsNullOrEmpty(authority?.Value) ? authority.Value : "http://localhost:5000";
                    options.RequireHttpsMetadata = false;
                    options.ApiName = "GHM_Timekeeping_Api";
                });

            // Get Api Url Settings.
            services.Configure<ApiUrlSettings>(Configuration.GetSection("ApiUrlSettings"));
            services.Configure<ApiServiceInfo>(Configuration.GetSection("ApiServiceInfo"));
            services.AddOptions();

            // Config Autofac.
            var container = new ContainerBuilder();
            container.Populate(services);

            container.RegisterModule(new ApplicationModule(Configuration.GetConnectionString("TimekeepingConnectionString")));
            //container.RegisterModule(new ValidationModule());
            var autofacServiceProvider = new AutofacServiceProvider(container.Build());
            return autofacServiceProvider;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            #region Allow Origins
            var allowOrigins = Configuration.GetSection("AllowOrigins")
                .GetChildren().Select(x => x.Value).ToArray();
            app.UseCors(builder =>
            {
                builder.WithOrigins(allowOrigins);
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
                builder.AllowCredentials();
            });
            #endregion
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
