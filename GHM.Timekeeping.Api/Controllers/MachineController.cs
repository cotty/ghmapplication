﻿using System.Net;
using System.Threading.Tasks;
using GHM.Infrastructure.Models;
using GHM.Timekeeping.Domain.IRepository;
using GHM.Timekeeping.Domain.Models;
using GHM.Timekeeping.Infrastructure.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Timekeeping.Api.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    public class MachineController : GhmControllerBase
    {
        private readonly IMachineRepository _machineRepository;
        public MachineController(IMachineRepository machineRepository)
        {
            _machineRepository = machineRepository;
        }

        [Route("insert-machine"), AcceptVerbs("POST")]
        //[CheckPermission(new[] { Permission.Insert, Permission.Update, Permission.View }, PageId.TimekeepingConfig)]
        public async Task<IActionResult> InsertMachine(Machine machine)
        {
            var isConnected = new TimeMachineHelper().Connect(machine.Ip, machine.Port);
            if (!isConnected)
                return BadRequest(new ActionResultResponse(-4, "Không thể kết nối đến máy chấm công bạn muốn thêm. Vui lòng kiểm tra lại địa chỉ IP hoặc cổng kết nối."));

            var result = await _machineRepository.Insert(machine);
            if (result < 0)
                return BadRequest(new ActionResultResponse(result, result == -1 ? "IP đã tồn tại. Vui lòng kiểm tra lại."
                    : result == -2 ? "Số máy đã tồn tại. Vui lòng kiểm tra lại."
                        : result == -3 ? "Số Seri đã tồn tại với 2 máy khác. Vui lòng lấy lại số seri" : "Có gì đó hoạt động chưa đúng. Vui lòng liên hệ với Quản trị viên."));

            return Ok(result);
        }

        [Route("update-machine"), AcceptVerbs("POST")]
        //[CheckPermission(new[] { Permission.Insert, Permission.Update, Permission.View }, PageId.TimekeepingConfig)]
        public async Task<IActionResult> UpdateMachine(Machine machine)
        {
            var isConnected = new TimeMachineHelper().Connect(machine.Ip, machine.Port);
            if (!isConnected)
                return BadRequest(new ActionResultResponse(-4, "Không thể kết nối đến máy chấm công bạn muốn thêm. Vui lòng kiểm tra lại địa chỉ IP hoặc cổng kết nối."));

            var result = await _machineRepository.Update(machine);
            if (result < 0)
                return BadRequest(new ActionResultResponse(result, result == -1 ? "IP đã tồn tại. Vui lòng kiểm tra lại."
                    : result == -2 ? "Số máy đã tồn tại. Vui lòng kiểm tra lại."
                        : result == -3 ? "Số Seri đã tồn tại với 2 máy khác. Vui lòng lấy lại số seri" : "Có gì đó hoạt động chưa đúng. Vui lòng liên hệ với Quản trị viên."));

            return Ok(result);
        }

        [Route("delete-machine"), AcceptVerbs("DELETE")]
        //[CheckPermission(new[] { Permission.Insert, Permission.Update, Permission.View }, PageId.TimekeepingConfig)]
        public async Task<IActionResult> DeleteMachine(string machineId)
        {
            return Ok(await _machineRepository.Delete(machineId));
        }

        [Route("search-machine"), AcceptVerbs("GET")]
        //[CheckPermission(new[] { Permission.Insert, Permission.Update, Permission.View }, PageId.TimekeepingConfig)]
        public async Task<IActionResult> GetAllMachine()
        {
            return Ok(await _machineRepository.GetAllMachine());
        }

        [Route("get-serial-number"), AcceptVerbs("GET")]
        //[CheckPermission(new[] { Permission.Insert, Permission.Update, Permission.View }, PageId.TimekeepingConfig)]
        public IActionResult GetSerialNumber(string ip, int port)
        {
            return Ok(new TimeMachineHelper().GetSerialNumber(ip, port));
        }
    }
}
