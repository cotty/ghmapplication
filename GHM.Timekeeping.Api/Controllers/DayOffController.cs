﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Services;
using GHM.Infrastructure.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace GHM.Timekeeping.Api.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    public class DayOffController : GhmControllerBase
    {
        private readonly IOptions<ApiUrlSettings> _apiUrlSettings;
        private readonly IOptions<ApiServiceInfo> _apiServiceInfo;
        private readonly IMediator _mediator;
        private readonly IHttpClientService _httpClientService;

        public DayOffController(IOptions<ApiUrlSettings> apiUrlSettings, IOptions<ApiServiceInfo> apiServiceInfo, IMediator mediator, IHttpClientService httpClientService)
        {
            _apiUrlSettings = apiUrlSettings;
            _apiServiceInfo = apiServiceInfo;
            _mediator = mediator;
            _httpClientService = httpClientService;
        }

        //[Route("approve"), AcceptVerbs("POST")]
        //[CheckPermission(new[] { Permission.Approve }, PageId.TimekeepingDayOffApprove)]
        //public async Task<IActionResult> Approve(DayOffApproveCommand dayOffApprove)
        //{
        //    //var httpClient = new HttpClientService<UserInfoViewModel>(_apiUrlSettings.Value.Authority, _apiServiceInfo.Value.ClientId, _apiServiceInfo.Value.ClientSecret,
        //        //_apiServiceInfo.Value.Scopes);
        //    var userInfo = await _httpClientService.GetAsync<UserInfoViewModel>($"{_apiUrlSettings.Value.HrApiUrl}users/{dayOffApprove.UserId}");
        //    if (userInfo == null)
        //    {
        //        return BadRequest(new ActionResultResponse(-1,
        //            "Không tìm thấy thông tin nhân sự. Vui lòng kiểm tra lại hoặc liên hệ với Quản trị viên."));
        //    }

        //    dayOffApprove.UserId = userInfo.UserId;
        //    dayOffApprove.HolidayRemaining = userInfo.HolidayRemaining;

        //    var result = await _mediator.Send(dayOffApprove);
        //    if (result <= 0)
        //    {
        //        return BadRequest(new ActionResultResponse(result, result == -1 ? "Số ngày nghỉ phép được duyệt lớn hơn số phép người dùng hiện có. Vui lòng kiếm tra lại."
        //            : result == -2 ? "Vui lòng nhập lý do không duyệt."
        //                : "Có gì đó hoạt động chưa đúng. Vui lòng liên hệ với Quản trị viên."));
        //    }

        //    return Ok(new ActionResultResponse(result, "Duyệt đăng ký nghỉ thành công."));
        //    //var userInfo = HttpClientHelper<UserInfoViewModel>.GetExternalResult(_apiUrlSettings.Value.Authoriy,
        //    //    _apiServiceInfo.Value.ClientId, _apiServiceInfo.Value.ClientSecret,
        //    //    "GHM_Hr_Api", $"{_apiUrlSettings.Value.HrApiUrl}get-user-info");

        //    //if (dayOffApprove.Type == 1)
        //    //{
        //    //    var unApproveCountWithOutReason = dayOffApprove.Dates.Count(x => x.IsManagerApprove.HasValue
        //    //        && x.IsManagerApprove.Value == false && string.IsNullOrEmpty(x.ManagerDeclineReason)
        //    //        && x.Method.HasValue && x.Method.Value != (byte)DayOffMethod.WeekLeave && x.Method.Value != (byte)DayOffMethod.UnAuthorizedLeave
        //    //        && x.Method.Value == (byte)DayOffMethod.HolidayLeave);
        //    //    if (unApproveCountWithOutReason > 0 && string.IsNullOrEmpty(dayOffApprove.ManagerDeclineReason))
        //    //        return new ErrorActionResult(HttpStatusCode.BadRequest, new ActionResultResponse(-10, "Vui lòng nhập lý do không duyệt."));
        //    //}

        //    //if (dayOffApprove.Type == 2)
        //    //{
        //    //    var unApproveCountWithOutReason = dayOffApprove.Dates.Count(x => x.IsApproverApprove.HasValue && x.IsApproverApprove.Value == false
        //    //        && string.IsNullOrEmpty(x.ApproverDeclineReason)
        //    //        && x.Method.HasValue && x.Method.Value != (byte)DayOffMethod.WeekLeave && x.Method.Value != (byte)DayOffMethod.UnAuthorizedLeave
        //    //        && x.Method.Value == (byte)DayOffMethod.HolidayLeave);
        //    //    if (unApproveCountWithOutReason > 0 && string.IsNullOrEmpty(dayOffApprove.ApproverDeclineReason))
        //    //        return new ErrorActionResult(HttpStatusCode.BadRequest, new ActionResultResponse(-10, "Vui lòng nhập lý do không duyệt."));
        //    //}

        //    //var info = await _dayOffRepository.GetInfo(dayOffApprove.Id);
        //    //if (info == null)
        //    //    return new ErrorActionResult(HttpStatusCode.BadRequest, new ActionResultResponse(-1, "Thông tin đăng ký nghỉ không tồn tại. Vui lòng kiểm tra lại."));

        //    //// Kiểm tra người duyệt có phải QLTT hoặc QLPD không.
        //    //var isManager = await _userRepository.CheckIsManagerOrApprover(info.UserId, CurrentUser.Id);
        //    //if (!isManager)
        //    //    return new ErrorActionResult(HttpStatusCode.Forbidden, new ActionResultResponse(-3, "Bạn không có quyền thực hiện chức năng này."));

        //    //byte status = 100; // 100 Mean not valid
        //    //decimal totalAnnualLeave = 0;

        //    //// Kiểm tra số ngày phép.
        //    //var listAnnualLeave = info.Dates
        //    //    .Where(x => x.Method.HasValue && x.Method.Value == (byte)DayOffMethod.AnnualLeave)
        //    //    .ToList();
        //    //var useHolidayRemain = _userRepository.GetTotalHolidayRemain(info.UserId);
        //    //totalAnnualLeave = listAnnualLeave.Any()
        //    //    ? dayOffApprove.Type == 1 ? listAnnualLeave.Where(x => x.IsManagerApprove.HasValue && x.IsManagerApprove.Value).Sum(x => x.ShiftWorkUnit)
        //    //    : dayOffApprove.Type == 2 ? listAnnualLeave.Where(x => x.IsApproverApprove.HasValue && x.IsApproverApprove.Value).Sum(x => x.ShiftWorkUnit) : 0 : 0;

        //    //if (totalAnnualLeave > 0)
        //    //{
        //    //    if (!useHolidayRemain.HasValue)
        //    //        return new ErrorActionResult(HttpStatusCode.BadRequest,
        //    //            new ActionResultResponse(-7,
        //    //                "Bạn chưa được cấu hình ngày phép. Vui lòng liên hệ với bộ phận Hành chính - Nhân sự để được trợ giúp."));

        //    //    if (useHolidayRemain.Value < totalAnnualLeave)
        //    //        return new ErrorActionResult(HttpStatusCode.BadRequest,
        //    //            new ActionResultResponse(-6,
        //    //                "Số ngày phép của nhân viên đăng ký không đủ. Bạn không thể duyệt đơn nghỉ phép này."));
        //    //}

        //    //var validDays = dayOffApprove.Dates.Where(x =>
        //    //    x.Method.HasValue && x.Method != (byte)DayOffMethod.HolidayLeave &&
        //    //    x.Method != (byte)DayOffMethod.UnAuthorizedLeave && x.Method != (byte)DayOffMethod.WeekLeave);
        //    //var totalApprovedDays = dayOffApprove.Type == 1
        //    //    ? validDays.Where(x => x.IsManagerApprove.HasValue && x.IsManagerApprove.Value && x.Method != (byte)DayOffMethod.WeekLeave && x.Method != (byte)DayOffMethod.UnAuthorizedLeave
        //    //                           && x.Method != (byte)DayOffMethod.HolidayLeave).Sum(x => x.ShiftWorkUnit)
        //    //    : validDays.Where(x => x.IsApproverApprove.HasValue && x.IsApproverApprove.Value).Sum(x => x.ShiftWorkUnit);
        //    //if (dayOffApprove.Type == 1)
        //    //{
        //    //    if (totalApprovedDays >= 5)
        //    //    {
        //    //        var dayOffInfo = await _dayOffRepository.GetInfo(dayOffApprove.Id);
        //    //        status = !string.IsNullOrEmpty(dayOffInfo.ApproverUserId)
        //    //            ? (byte)DayOffStatus.ManagerApproveWaitingApproverApprove
        //    //            : (byte)DayOffStatus.ManagerApprove;
        //    //    }
        //    //    else
        //    //    {
        //    //        status = (byte)DayOffStatus.ManagerApprove;
        //    //    }
        //    //}
        //    //if (dayOffApprove.Type == 2)
        //    //{
        //    //    status = (byte)DayOffStatus.ApproverApprove;
        //    //}
        //    //var result = await _dayOffRepository.Approve(dayOffApprove, status);
        //    //if (result <= 0)
        //    //{
        //    //    return new ErrorActionResult(HttpStatusCode.BadRequest, new ActionResultResponse(result,
        //    //         result == -1 ? "Thông tin đăng ký không tồn tại. Vui lòng kiểm tra lại."
        //    //        : result == -2 ? "Bạn không có quyền thực hiện chức năng này."
        //    //        : Resources.Message_Something_Went_Wrong));
        //    //}

        //    //var url = $"/{Helpers.GetAppPrefix()}/timekeeping/day-off";
        //    //var registerUrl = $"{url}?id={info.Id}&type=0";
        //    //var managerUrl = $"{url}?id={info.Id}&type=1";
        //    //var approverUrl = $"{url}?id={info.Id}&type=2";

        //    //var updatedDayOffInfo = await _dayOffRepository.GetInfo(info.Id);
        //    //if (updatedDayOffInfo == null)
        //    //    return new ErrorActionResult(HttpStatusCode.BadRequest, new ActionResultResponse(-4, "Có gì đó hoạt động chưa đúng. Vui lòng liên hệ với quản trị viên."));

        //    //var isApprove = updatedDayOffInfo.Status == (byte)DayOffStatus.ManagerApprove ||
        //    //                updatedDayOffInfo.Status == (byte)DayOffStatus.ApproverApprove || updatedDayOffInfo.Status == (byte)DayOffStatus.ManagerApproveWaitingApproverApprove;

        //    //// Trường hợp QLPD duyệt, không duyệt gửi thông báo về cho cả QLTT và Nhân viên đăng ký
        //    //if (updatedDayOffInfo.Status == (byte)DayOffStatus.ApproverApprove || updatedDayOffInfo.Status == (byte)DayOffStatus.ApproverDecline)
        //    //{
        //    //    // Gửi thông báo về cho QLTT
        //    //    await NotifyHelper.CreateAndSendNotifyToClient($"<b>{CurrentUser.FullName}</b> đã {(isApprove ? "duyệt" : "không duyệt")} cho đơn đăng ký nghỉ của nhân viên <b>{updatedDayOffInfo.FullName}</b>"
        //    //        , $"<b>{CurrentUser.FullName}</b> đã <b>{(isApprove ? "duyệt" : "không duyệt")}</b> cho đơn đăng ký xin nghỉ của <b>{updatedDayOffInfo.FullName}</b>. Click vào đây để xem chi tiết", CurrentUser.FullName,
        //    //        CurrentUser.Image, updatedDayOffInfo.ManagerUserId, NotificationType.Info, managerUrl);

        //    //    // Gửi thông báo về cho nhân viên đăng ký
        //    //    await NotifyHelper.CreateAndSendNotifyToClient($"<b>{CurrentUser.FullName}</b> đã {(isApprove ? "duyệt" : "không duyệt")} cho đơn đăng ký xin nghỉ của bạn", $"<b>{CurrentUser.FullName}</b> đã <b>{(isApprove ? "duyệt" : "không duyệt")}</b> cho đơn đăng ký xin nghỉ của Bạn. Click vào đây để xem chi tiết", CurrentUser.FullName,
        //    //        CurrentUser.Image, updatedDayOffInfo.UserId, NotificationType.Info, registerUrl);
        //    //}

        //    //// Trường hợp QLTT duyệt, không duyệt gửi thông báo về cho nhân viên đăng ký.
        //    //if (updatedDayOffInfo.Status == (byte)DayOffStatus.ManagerApprove ||
        //    //    updatedDayOffInfo.Status == (byte)DayOffStatus.ManagerDecline)
        //    //{
        //    //    // Gửi thông báo về cho nhân viên đăng ký
        //    //    await NotifyHelper.CreateAndSendNotifyToClient($"<b>{CurrentUser.FullName} đã {(isApprove ? "duyệt" : "không duyệt")} cho đơn đăng ký xin nghỉ của bạn",
        //    //        $"<b>{CurrentUser.FullName} đã {(isApprove ? "duyệt" : "không duyệt")} cho đơn đăng ký xin nghỉ của bạn. Vui lòng click vào đây để xem chi tiết.", CurrentUser.FullName,
        //    //        CurrentUser.Image, updatedDayOffInfo.UserId, NotificationType.Info, registerUrl);
        //    //}

        //    //// Trường hợp QLTT duyệt chờ QLPD duyệt gửi thông báo về cho nhân viên đăng ký và QLPD
        //    //if (updatedDayOffInfo.Status == (byte)DayOffStatus.ManagerApproveWaitingApproverApprove)
        //    //{
        //    //    // Gửi thông báo về cho QLPD
        //    //    await NotifyHelper.CreateAndSendNotifyToClient($"<b>{CurrentUser.FullName}</b> duyệt cho đơn đăng ký xin nghỉ của <b>{updatedDayOffInfo.FullName}</b> chờ bạn duyệt.",
        //    //        $"<b>{CurrentUser.FullName}</b> đã <b>duyệt</b> cho đơn đăng ký xin nghỉ của <b>{updatedDayOffInfo.FullName}</b> và chờ Bạn duyệt. Click vào đây để xem chi tiết.", CurrentUser.FullName,
        //    //        CurrentUser.Image, updatedDayOffInfo.ApproverUserId, NotificationType.Info, approverUrl);

        //    //    // Gửi thông báo về cho nhân viên đăng ký
        //    //    await NotifyHelper.CreateAndSendNotifyToClient($"<b>{CurrentUser.FullName}</b> duyệt cho đơn đăng ký xin nghỉ của <b>{updatedDayOffInfo.FullName}</b> chờ <b>{updatedDayOffInfo.ApproverFullName}</b> duyệt.",
        //    //        $"<b>{CurrentUser.FullName}</b> đã duyệt cho đơn đăng ký xin nghỉ của Bạn và chờ <b>{updatedDayOffInfo.FullName}</b> duyệt. Click vào đây để xem chi tiêt", CurrentUser.FullName,
        //    //        CurrentUser.Image, updatedDayOffInfo.UserId, NotificationType.Info, registerUrl);
        //    //}

        //    ////// Trường hợp QLTT hoặc QLPD duyệt. Cập nhật lại tổng số ngày phép còn lại.
        //    ////if (updatedDayOffInfo.Status != (byte)DayOffStatus.ManagerApprove && updatedDayOffInfo.Status !=
        //    ////    (byte)DayOffStatus.ApproverApprove)
        //    ////    return Ok(result);

        //    //// Cập nhật lại số ngày phép 
        //    //if (totalAnnualLeave > 0)
        //    //{
        //    //    await _userRepository.UpdateUserHoliday(info.UserId, totalAnnualLeave);
        //    //}

        //    //return Ok(new ActionResultResponse(result, "Duyệt ngày nghỉ thành công.", "Chúc mừng", new
        //    //{
        //    //    status,
        //    //    totalApprovedDays
        //    //}));
        //}
    }
}
