﻿using System.Threading.Tasks;
using GHM.Infrastructure.Models;
using GHM.Timekeeping.Domain.IRepository;
using GHM.Timekeeping.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Timekeeping.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/v1/workSchedules")]
    public class WorkScheduleController : GhmControllerBase
    {
        private readonly IWorkScheduleRepository _workScheduleRepository;
        public WorkScheduleController(IWorkScheduleRepository workScheduleRepository)
        {
            _workScheduleRepository = workScheduleRepository;
        }

        [AcceptVerbs("POST")]
        //[CheckPermission(new[] { Permission.Update, Permission.Insert }, PageId.TimekeepingWorkingSchedule)]
        public async Task<IActionResult> SaveWorkSchedule(WorkSchedule workSchedule)
        {
            var result = await _workScheduleRepository.Save(workSchedule);
            if (result < 0)
            {
                return BadRequest(new ActionResultResponse(result,
                    result == -1 ? "Thông tin lịch làm việc chưa tồn tại. Vui lòng liên hệ với quản trị viên."
                        : result == -2 ? "Thông tin nhóm ca làm việc không tồn tại. Vui lòng kiểm tra lại"
                            : result == -3 ? "Thông tin ca làm việc không tồn tại. Vui lòng kiểm tra lại"
                                : "Có gì đó hoạt động chưa đúng. Vui lòng liên hệ với Quản trị viên."));
            }
            return Ok(new ActionResultResponse(result, "Cập nhật lịch làm việc thành công."));
        }
    }
}
