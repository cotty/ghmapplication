﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using GHM.Events;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Notifications.Api.Hubs;
using GHM.Notifications.Api.Infrastructure.IRepository;
using GHM.Notifications.Api.Infrastructure.IServices;
using GHM.Notifications.Api.Infrastructure.ModelMeta;
using GHM.Notifications.Api.Infrastructure.Models;
using GHM.Notifications.Api.Resources;
using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace GHM.Notifications.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/schedule-job")]
    public class ScheduleJobController : GhmControllerBase
    {
        private readonly IScheduleJobService _scheduleJobService;
        private readonly IHubContext<NotificationHub> _notificationHub;
        private readonly IUserConnectionRepository _userConnectionRepository;
        private readonly INotificationService _notificationService;
        private readonly IResourceService<GhmNotificationResource> _resourceService;

        public ScheduleJobController(IScheduleJobService scheduleJobService,
               IUserConnectionRepository userConnectionRepository,
               IResourceService<GhmNotificationResource> resourceService,
            IHubContext<NotificationHub> notificationHub,
            INotificationService notificationService)
        {
            _notificationService = notificationService;
            _notificationHub = notificationHub;
            _scheduleJobService = scheduleJobService;
            _userConnectionRepository = userConnectionRepository;
            _resourceService = resourceService;
        }

        [Route("insert"), AcceptVerbs("POST")]
        public async Task<IActionResult> AddScheduleJob([FromBody]ScheduleJobMeta scheduleJobMeta)
        {
            var result = await _scheduleJobService.Insert(scheduleJobMeta);
            // send notification to Account online and update Notification
            var minutesNotification = (scheduleJobMeta.Date - DateTime.Now).TotalMinutes +
                scheduleJobMeta.Hour * 60 + scheduleJobMeta.Minutes;
            if (scheduleJobMeta.ScheduleType == ScheduleType.None) {
                RecurringJob.RemoveIfExists(scheduleJobMeta.JobTypeId);
            } else
            {
                RecurringJob.AddOrUpdate<ScheduleJobController>(scheduleJobMeta.JobTypeId, x => x.SendNotification(scheduleJobMeta.Notifications), Cron.Daily, null);
            }
           
             
            return Ok(result);
        }

        [Route("{jobType}/{jobId}/checkExist"), AcceptVerbs("GET")]
        public async Task<IActionResult> CheckExist(JobType jobType, string jobId)
        {
            var result = await _scheduleJobService.CheckExist(CurrentUser.TenantId, jobType, jobId);

            return Ok(result);
        }

        [Route("update"), AcceptVerbs("POST")]
        public async Task<IActionResult> Update([FromBody]ScheduleJobMeta scheduleJobMeta)
        {
            var result = await _scheduleJobService.Update(scheduleJobMeta);

            if (result.Code < 0)
                return BadRequest(result);

            if (scheduleJobMeta.ScheduleType == ScheduleType.None)
            {
                RecurringJob.RemoveIfExists(scheduleJobMeta.JobTypeId);
            }
            else
            {
                RecurringJob.AddOrUpdate<ScheduleJobController>(scheduleJobMeta.JobTypeId, x => x.SendNotification(scheduleJobMeta.Notifications), Cron.Daily, null);
            }

            return Ok(result);
        }

        [Route("detail/{jobTypeId}/{jobType}"), AcceptVerbs("GET")]
        public async Task<IActionResult> GetDetail(string jobTypeId, JobType jobType)
        {
            var result = await _scheduleJobService.GetDetailByJobTypeId(jobType, CurrentUser.TenantId, jobTypeId);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        private async Task SendNotification(List<NotificationModel> listNotification)
        {
            foreach (var notify in listNotification)
            {
                var notification = new Notification
                {
                    TenantId = notify.TenantId,
                    Id = notify.Id.ToString(),
                    Title = notify.Title,
                    Content = notify.Content,
                    Image = notify.Image,
                    ReceiverId = notify.ReceiverId,
                    SenderId = notify.SenderId,
                    SenderFullName = notify.SenderFullName,
                    SenderAvatar = notify.SenderAvatar,
                    Type = (NotificationType)notify.Type,
                    Url = notify.Url,
                    IsSend = false

                };

                var result = await _notificationService.Insert(notification);
                if (result.Code > 0)
                {
                    var userConnections = await _userConnectionRepository.GetAllUserConnections(notification.ReceiverId);
                    if (userConnections.Any())
                    {
                        // Parse notification title to current language.
                        notification.Title = ExtractMessageContent(notification.Title);
                        notification.Content = ExtractMessageContent(notification.Content);

                        foreach (var userConnection in userConnections)
                        {
                            await _notificationHub
                                .Clients
                                .Client(userConnection.ConnectionId)
                                .SendAsync("NotificationReceived", notification);
                        }
                    }
                }

            }
        }

        private string ExtractMessageContent(string content)
        {
            const string pattern = @"(\{(?:.*?)\})";
            foreach (var item in Regex.Split(content, pattern))
            {
                if (item.StartsWith("{") && item.EndsWith("}"))
                {
                    content = content.Replace(item, _resourceService.GetString(item.Trim()));
                }
            }
            return content;
        }
    }
}