﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GHM.Notifications.Api.Infrastructure.Models
{
    public class ScheduleJobNotification
    {
        public string ScheduleJobId { get; set; }
        public string NotificationId { get; set; }
    }
}
