﻿using GHM.Notifications.Api.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GHM.Notifications.Api.Infrastructure.IRepository
{
    public interface IJobNotificationRepository
    {
        Task<List<ScheduleJobNotification>> getAllByJobType(string scheduleJobId);
    }
}
