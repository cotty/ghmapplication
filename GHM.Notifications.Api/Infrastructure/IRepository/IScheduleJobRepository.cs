﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Infrastructure.Constants;
using GHM.Notifications.Api.Infrastructure.Models;

namespace GHM.Notifications.Api.Infrastructure.IRepository
{
    public interface IScheduleJobRepository
    {
        Task<bool> CheckExist(string tenantId, JobType jobType, string jobId);

        Task<int> Insert(ScheduleJob scheduleJob);

        Task<ScheduleJob> GetInfo(string tenantId, JobType jobType, string jobTypeId, bool isReadOnly);

        Task<int> Update(ScheduleJob info);

        Task<ScheduleJob> GetDetail(string tenantId, JobType jobType, string jobTypeId);
    }
}
