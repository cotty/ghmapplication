﻿using GHM.Infrastructure.SqlServer;
using GHM.Notifications.Api.Infrastructure.IRepository;
using GHM.Notifications.Api.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GHM.Notifications.Api.Infrastructure.Repository
{
    public class JobNotificationRepository : RepositoryBase, IJobNotificationRepository
    {
        private readonly IRepository<ScheduleJobNotification> _scheduleJobNotification;
        public JobNotificationRepository(IDbContext context): base(context)
        {
            _scheduleJobNotification = Context.GetRepository<ScheduleJobNotification>();
        }
        public async Task<List<ScheduleJobNotification>> getAllByJobType(string scheduleJobId)
        {
            return await _scheduleJobNotification.GetsAsync(true, x => x.ScheduleJobId == scheduleJobId);
        }
    }
}
