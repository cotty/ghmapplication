﻿using GHM.Infrastructure.Constants;
using GHM.Infrastructure.SqlServer;
using GHM.Notifications.Api.Infrastructure.IRepository;
using GHM.Notifications.Api.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GHM.Notifications.Api.Infrastructure.Repository
{
    public class ScheduleJobRepository : RepositoryBase, IScheduleJobRepository
    {
        private readonly IRepository<ScheduleJob> _scheduleJobRepository;
        public ScheduleJobRepository(IDbContext context): base(context)
        {
            _scheduleJobRepository = Context.GetRepository<ScheduleJob>();
        }
        public async Task<bool> CheckExist(string tenantId, JobType jobType, string jobId)
        {
            return await _scheduleJobRepository.ExistAsync(x => x.TenantId == tenantId && x.JobType == jobType && x.JobTypeId == jobId);
        }

        public async Task<ScheduleJob> GetDetail(string tenantId, JobType jobType, string jobTypeId)
        {
            return await _scheduleJobRepository.GetAsync(true, x => x.TenantId == tenantId && x.JobType == jobType && x.JobTypeId == jobTypeId && !x.IsDelete);
        }

        public async Task<ScheduleJob> GetInfo(string tenantId, JobType jobType, string jobTypeId, bool isReadOnly = false)
        {
            return await _scheduleJobRepository.GetAsync(isReadOnly, x => x.TenantId == tenantId && x.JobType == jobType && x.JobTypeId == jobTypeId);
        }

        public async Task<int> Insert(ScheduleJob scheduleJob)
        {
            _scheduleJobRepository.Create(scheduleJob);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(ScheduleJob info)
        {
            return await Context.SaveChangesAsync();
        }
    }
}
