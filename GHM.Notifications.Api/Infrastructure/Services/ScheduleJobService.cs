﻿using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.SqlServer;
using GHM.Notifications.Api.Infrastructure.IRepository;
using GHM.Notifications.Api.Infrastructure.IServices;
using GHM.Notifications.Api.Infrastructure.ModelMeta;
using GHM.Notifications.Api.Infrastructure.Models;
using GHM.Notifications.Api.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GHM.Notifications.Api.Infrastructure.Services
{
    public class ScheduleJobService : IScheduleJobService
    {
        private readonly IScheduleJobRepository _scheduleJobRepository;
        private readonly INotificationRepository _notificationRepository;
        private readonly IJobNotificationRepository _jobNotificationRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmNotificationResource> _resourceService;
        public ScheduleJobService(IResourceService<SharedResource> sharedResource,
            INotificationRepository notificationRepository,
            IResourceService<GhmNotificationResource> resourceService,
            IJobNotificationRepository jobNotificationRepository,
            IScheduleJobRepository scheduleJobRepository)
        {
            _sharedResourceService = sharedResource;
            _resourceService = resourceService;
            _jobNotificationRepository = jobNotificationRepository;
            _notificationRepository = notificationRepository;
            _scheduleJobRepository = scheduleJobRepository;
        }
        public async Task<bool> CheckExist(string tenantId, JobType jobType, string jobId)
        {
            return await _scheduleJobRepository.CheckExist(tenantId, jobType, jobId);
        }

        public async Task<ActionResultResponse<ScheduleJob>> GetDetailByJobTypeId(JobType jobType, string tenantId, string jobTypeId)
        {
            var info = await _scheduleJobRepository.GetDetail(tenantId, jobType, jobTypeId);

            return new ActionResultResponse<ScheduleJob>
            {
                Code = 1,
                Data = info
            };
        }

        public async Task<ActionResultResponse> Insert(ScheduleJobMeta scheduleJobMeta)
        {
            var checkExist = await _scheduleJobRepository.CheckExist(scheduleJobMeta.TenantId, scheduleJobMeta.JobType, scheduleJobMeta.JobTypeId);

            if (checkExist)
                return new ActionResultResponse(-1, _resourceService.GetString("Schedule job does exist"));

            var scheduleJob = new ScheduleJob
            {
                Content = scheduleJobMeta.Content,
                Date = scheduleJobMeta.Date,
                JobType = scheduleJobMeta.JobType,
                JobTypeId = scheduleJobMeta.JobTypeId,
                TenantId = scheduleJobMeta.TenantId,
                ScheduleType = scheduleJobMeta.ScheduleType,
                Hour = scheduleJobMeta.Hour,
                Minutes = scheduleJobMeta.Minutes,
                CreatorId = scheduleJobMeta.UserId,
                CreatorFullName = scheduleJobMeta.UserFullName
            };

            return await _scheduleJobRepository.Insert(scheduleJob) < 0 ? new ActionResultResponse(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong))
                : new ActionResultResponse(1, _resourceService.GetString("Insert schedule job succesfully"));
        }

        public async Task<ActionResultResponse> Update(ScheduleJobMeta scheduleJobMeta)
        {
            var info = await _scheduleJobRepository.GetInfo(scheduleJobMeta.TenantId, scheduleJobMeta.JobType, scheduleJobMeta.JobTypeId, true);

            if (info == null)
                return new ActionResultResponse(-1, _resourceService.GetString("schedule job does exist"));

            info.Date = scheduleJobMeta.Date;
            info.Hour = scheduleJobMeta.Hour;
            info.Minutes = scheduleJobMeta.Minutes;
            info.LastUpdateUserId = scheduleJobMeta.UserId;
            info.LastUpdateUserFullName = scheduleJobMeta.UserFullName;
            info.LastUpdateTime = DateTime.Now;

            return await _scheduleJobRepository.Update(info) < 0 ? new ActionResultResponse(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong))
                : new ActionResultResponse(1, _resourceService.GetString("Update schedule job succesfully"));
        }

        private async Task<int> InsertNotification(string tenantId, JobType jobType, string jobTypeId, List<NotificationModel> notifications,string scheduleJobId)
        {

            if(!string.IsNullOrEmpty(scheduleJobId)) {
                var jobNotifications = await _jobNotificationRepository.getAllByJobType(scheduleJobId);
                    
                var listNotificationDelete = new List<Notification>();
                foreach(var items in jobNotifications)
                {
                    var item = await _notificationRepository.GetInfo(items.NotificationId, true);
                    listNotificationDelete.Add(item);
                }

                var resultDelete = await _notificationRepository.Deletes(listNotificationDelete);

                if (resultDelete < 0)
                    return -3;
            }
            var listNotification = new List<Notification>();

            foreach(var x in notifications)
            {
                var item = new Notification
                {
                    Content = x.Content,
                    IsSend = x.IsSend,
                    ReceiverId = x.ReceiverId,
                    SenderId = x.SenderId,
                    SenderFullName = x.SenderFullName,
                    SenderAvatar = x.SenderAvatar,
                    Type = (NotificationType)x.Type,
                    Url = x.Url,
                    TenantId = x.TenantId,
                    Id = x.Id,
                    Title = x.Title,
                };

                listNotification.Add(item);
            }

            var result = await _notificationRepository.Inserts(listNotification);

            return result;
        }
    }
}
