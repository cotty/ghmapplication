﻿using GHM.Events;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Models;
using GHM.Notifications.Api.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GHM.Notifications.Api.Infrastructure.ModelMeta
{
    public class ScheduleJobMeta
    {
        public string Content { get; set; }
        public JobType JobType { get; set; }
        public DateTime Date { get; set; }
        public string JobTypeId { get; set; }
        public string TenantId { get; set; }
        public string UserId { get; set; }
        public string UserFullName { get; set; }
        public string UserAvatar { get; set; }
        public ScheduleType ScheduleType { get; set; }
        public bool IsDelete { get; set; }
        public int Hour { get; set; }
        public int Minutes { get; set; }
        public List<NotificationModel> Notifications { get; set; }
    }
}
