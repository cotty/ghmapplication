﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Models;
using GHM.Notifications.Api.Infrastructure.ModelMeta;
using GHM.Notifications.Api.Infrastructure.Models;

namespace GHM.Notifications.Api.Infrastructure.IServices
{
    public interface IScheduleJobService
    {
        Task<ActionResultResponse> Insert(ScheduleJobMeta scheduleJobMeta);

        Task<bool> CheckExist(string tenantId, JobType jobType, string jobId);

        Task<ActionResultResponse> Update(ScheduleJobMeta scheduleJobMeta);

        Task<ActionResultResponse<ScheduleJob>> GetDetailByJobTypeId(JobType jobType, string tenantId, string jobTypeId);
    }
}
