﻿using System.Collections.Generic;
using GHM.Hr.Domain.Models;

namespace GHM.Hr.Domain.ModelMetas
{
    public class CriteriaMeta
    {
        public string TenantId { get; set; }

        public bool IsActive { get; set; }

        public string GroupId { get; set; }

        public byte Point { get; set; }

        public string ConcurrencyStamp { get; set; }

        public int Order { get; set; }

        public List<CriteriaTranslation> Translations { get; set; }
    }
}
