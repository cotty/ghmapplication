﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.Models;

namespace GHM.Hr.Domain.ModelMetas
{
    public class UserMeta
    {
        public string Id { get; set; }

        public string FullName { get; set; }

        public string UserName { get; set; }

        public string Avatar { get; set; }

        public DateTime Birthday { get; set; } // Ngày sinh

        public string IdCardNumber { get; set; } // Số CMND

        //[DisplayName(@"Ngày cấp")]
        public DateTime? IdCardDateOfIssue { get; set; } // Ngày cấp
        public Gender Gender { get; set; } // Giới tính: 0 - Nam 1: Nữ 2: Giới tính khác

        public int? Ethnic { get; set; } // Dân tộc

        public byte? Denomination { get; set; } // Tôn giáo: Null là không theo tôn giáo nào. 1: Phật giáo 2: Thiên chúa giáo 3: hindu

        public string Tin { get; set; } // Mã số thuế cá nhân (Tax Identification number )

        public DateTime? JoinedDate { get; set; }

        public string BankingNumber { get; set; } // Số tài khoản ngân hàng

        public int? NationalId { get; set; }

        public int? ProvinceId { get; set; }

        public int? DistrictId { get; set; }

        public MarriedStatus? MarriedStatus { get; set; } // Tình trạng hôn nhân: 0 - Chưa kết hôn 1: Đã kết hôn 2: Đã ly thân 3: Đã ly hôn

        public int OfficeId { get; set; }

        public string TitleId { get; set; }

        public string PositionId { get; set; }

        public UserType UserType { get; set; }

        public string PassportId { get; set; }

        public DateTime? PassportDateOfIssue { get; set; }

        public int? EnrollNumber { get; set; }

        public string CardNumber { get; set; }

        public string Ext { get; set; }

        public UserStatus Status { get; set; }

        public string ConcurrencyStamp { get; set; }
        public AcademicRank? AcademicRank { get; set; }
        public List<UserTranslation> UserTranslations { get; set; }
        public List<UserContact> UserContacts { get; set; }
    }
}
