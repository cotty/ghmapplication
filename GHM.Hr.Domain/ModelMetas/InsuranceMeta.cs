﻿using System;

namespace GHM.Hr.Domain.ModelMetas
{
    public class InsuranceMeta
    {
        public int Id { get; set; }
        public bool Type { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public decimal Premium { get; set; }
        public string UserId { get; set; }
        public string Note { get; set; }
    }
}
