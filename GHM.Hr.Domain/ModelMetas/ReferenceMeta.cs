﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GHM.Hr.Domain.ModelMetas
{    
    public class ReferenceMeta
    {
        public int? Id { get; set; }

        [DisplayName(@"Tên tài liệu")]
        [Required(ErrorMessage = "Không được phép để trống.")]
        [MaxLength(250, ErrorMessage = "không được phép vượt quá 100 ký tự.")]
        public string Name { get; set; }
        public string UnsignName { get; set; }

        [DisplayName(@"Tên tác giả/Tổ chức")]
        [Required(ErrorMessage = "Không được phép để trống.")]
        [MaxLength(250, ErrorMessage = "không được phép vượt quá 100 ký tự.")]
        public string Author { get; set; }

        [DisplayName(@"Nguồn trích dẫn")]        
        [MaxLength(250, ErrorMessage = "không được phép vượt quá 100 ký tự.")]
        public string Source { get; set; }

        public int? Year { get; set; }

        [DisplayName("Đường dẫn")]
        [MaxLength(500, ErrorMessage = "không được phép vượt quá 100 ký tự.")]
        public string Link { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
