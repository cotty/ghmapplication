﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GHM.Hr.Domain.ModelMetas
{   
    public class TrainingHistoryMeta
    {
        public bool Type { get; set; }
        public string CourseId { get; set; }
        public string CourseName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string UserId { get; set; }
        public string Result { get; set; }   
        public string CoursePlaceId { get; set; }
        public string CoursePlaceName { get; set; }
        public bool IsHasCertificate { get; set; }
        public string ConcurrencyStamp { get; set; }
    }
}
