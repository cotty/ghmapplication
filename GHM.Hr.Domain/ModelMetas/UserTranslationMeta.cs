﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GHM.Hr.Domain.ModelMetas
{
    public class UserTranslationMeta
    {
        [Key]
        public string UserId { get; set; }
        /// <summary>
        /// Mã ngôn ngữ
        /// </summary>
        [Key]
        public string LanguageId { get; set; }
        /// <summary>
        /// Tên phòng ban.
        /// </summary>

        [DisplayName(@"Phòng ban")]
        [Required(ErrorMessage = "Vui lòng chọn phòng ban.")]
        public string OfficeName { get; set; }
        /// <summary>
        /// Tên chức danh.
        /// </summary>
        [DisplayName(@"Chức danh")]
        [Required(ErrorMessage = "Vui lòng chọn chức danh.")]
        public string PositionName { get; set; }
        /// <summary>
        /// Tên chức vụ.
        /// </summary>
        [DisplayName(@"Chức vụ")]
        [Required(ErrorMessage = "Vui lòng chọn chức vụ.")]
        public string TitleName { get; set; }
        /// <summary>
        /// Quốc tịch.
        /// </summary>
        public string Nationality { get; set; }
        /// <summary>
        /// Quê quán.
        /// </summary>
        public string NativeCountry { get; set; }
        /// <summary>
        /// Hộ khẩu thường trú.
        /// </summary>
        public string ResidentRegister { get; set; }
        /// <summary>
        /// Địa chỉ liên hệ.
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// Nơi làm việc
        /// </summary>
        public string Workplace { get; set; }
        /// <summary>
        /// Nơi cấp hộ chiếu.
        /// </summary>
        public string PassportPlaceOfIssue { get; set; }

        /// <summary>
        /// Tên ngân hàng
        /// </summary>
        public string BankName { get; set; }

        /// <summary>
        /// Chi nhánh ngân hàng
        /// </summary>
        public string BranchBank { get; set; }

        /// <summary>
        /// Nơi cấp chưng minh thư.
        /// </summary>
        public string IdCardPlaceOfIssue { get; set; }
    }
}
