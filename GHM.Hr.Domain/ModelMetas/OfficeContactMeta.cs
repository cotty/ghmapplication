﻿namespace GHM.Hr.Domain.ModelMetas
{
    public class OfficeContactMeta
    {
        public int OfficeId { get; set; }
        public string UserId { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Fax { get; set; }
    }
}
