﻿namespace GHM.Hr.Domain.ModelMetas
{
    public class LaborContractFormMeta
    {
        public string LaborContractTypeId { get; set; }
        public string FileId { get; set; } // File đính kèm
        public string FileName { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string Name { get; set; }
    }
}
