﻿using System;
using System.Collections.Generic;
using System.Text;
using GHM.Hr.Domain.Constants;

namespace GHM.Hr.Domain.ModelMetas
{
    public class UserAssessmentMeta
    {
        public string UserId { get; set; }
        public AssessmentStatus Status { get; set; }
        public string Reason { get; set; }
    }
}
