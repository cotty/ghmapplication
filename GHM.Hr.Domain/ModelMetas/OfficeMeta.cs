﻿using System.Collections.Generic;
using GHM.Hr.Domain.Constants;

namespace GHM.Hr.Domain.ModelMetas
{
    public class OfficeMeta
    {
        public string Code { get; set; }
        public OfficeType OfficeType { get; set; }
        public int Order { get; set; }
        public int? ParentId { get; set; }
        public bool IsActive { get; set; }
        public List<OfficeTranslationMeta> Translations { get; set; }
        public List<OfficeContactMeta> OfficeContacts { get; set; }
    }
}
