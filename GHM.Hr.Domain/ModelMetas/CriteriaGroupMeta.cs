﻿using System.Collections.Generic;
using GHM.Hr.Domain.Models;

namespace GHM.Hr.Domain.ModelMetas
{
    public class CriteriaGroupMeta
    {
        public string TenantId { get; set; }

        public string ConcurrencyStamp { get; set; }

        public bool IsActive { get; set; }

        public bool IsDelete { get; set; }

        public int Order { get; set; }

        public string UserId { get; set; }

        public string FullName { get; set; }

        public List<CriteriaGroupTranslation> Translations { get; set; }
    }
}
