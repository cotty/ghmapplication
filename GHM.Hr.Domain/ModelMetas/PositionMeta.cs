﻿using System.Collections.Generic;

namespace GHM.Hr.Domain.ModelMetas
{
    public class PositionMeta
    {
        public string TitleId { get; set; }

        public int Order { get; set; }

        public bool IsActive { get; set; }

        public bool IsManager { get; set; }

        public bool IsMultiple { get; set; }

        public string ConcurrencyStamp { get; set; }

        public List<PositionTranslationMeta> Translations { get; set; }

        public List<int> OfficeIds { get; set; }
    }
}
