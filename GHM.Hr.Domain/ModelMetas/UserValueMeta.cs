﻿using GHM.Hr.Domain.Constants;

namespace GHM.Hr.Domain.ModelMetas
{
    public class UserValueMeta
    {
        public string Name { get; set; }
        /*
         * Loại giá trị: 0: Loại hợp đồng.
         * 1: Công ty trong BHXH.
         * 2: Công ty trong quá trình làm việc.
         * 3: Chương trình đào tạo.
         * 4: Nơi đào tạo.
         * 5: Phòng ban QTCT.
         * 6: Chức danh QTCT.
         * 7: Hồ sơ giấy tờ.
         */
        public UserValueType Type { get; set; }
    }
}
