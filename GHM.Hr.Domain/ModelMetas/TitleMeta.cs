﻿using System.Collections.Generic;
using GHM.Hr.Domain.Models;

namespace GHM.Hr.Domain.ModelMetas
{
    public class TitleMeta
    {
        public bool IsActive { get; set; }
        public string ConcurrencyStamp { get; set; }
        public int Order { get; set; }
        public List<TitleTranslation> Translations { get; set; }
    }
}
