﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Hr.Domain.ModelMetas
{
    public class UserRecordsManagementMeta
    {
        public string UserId { get; set; }
        public string ValueId { get; set; }
        public string Note { get; set; }
        public bool IsSelected { get; set; }
        public string AttachmentId { get; set; }
        public string AttachmentName { get; set; }
        public string Title { get; set; }
        public string ConcurrencyStamp { get; set; }
    }
}
