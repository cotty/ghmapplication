﻿namespace GHM.Hr.Domain.ModelMetas
{
    public class OfficeTranslationMeta
    {
        public int OfficeId { get; set; }

        public string LanguageId { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }

        public string Address { get; set; }

        public string Description { get; set; }
    }
}
