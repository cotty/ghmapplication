﻿using GHM.Hr.Domain.Constants;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GHM.Hr.Domain.ModelMetas
{    
    public class CommendationDisciplineMeta
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public CommendationDisciplineType Type { get; set; }
        public DateTime Time { get; set; }
        public string DecisionNo { get; set; }
        public string Reason { get; set; }
        public decimal? Money { get; set; }
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string AttachmentUrl { get; set; }
    }
}
