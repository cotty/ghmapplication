﻿namespace GHM.Hr.Domain.ModelMetas
{
    public class PositionTranslationMeta
    {
        public string LanguageId { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }

        public string Description { get; set; }

        public string OtherRequire { get; set; }

        public string Responsibility { get; set; }

        public string Purpose { get; set; }
    }
}
