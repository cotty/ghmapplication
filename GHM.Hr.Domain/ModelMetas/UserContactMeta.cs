﻿using GHM.Hr.Domain.Constants;

namespace GHM.Hr.Domain.ModelMetas
{
    public class UserContactMeta
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public ContactType ContactType { get; set; }        
        public string ContactValue { get; set; }
        public string ConcurrencyStamp { get; set; }
    }
}
