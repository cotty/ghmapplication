﻿using GHM.Hr.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Hr.Domain.ModelMetas
{
    public class CommendationDisciplineCategoryMeta
    {
        public string Name { get; set; }
        public CommendationDisciplineType Type { get; set; }
    }
}
