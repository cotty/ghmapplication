﻿using GHM.Hr.Domain.Constants;
using System;

namespace GHM.Hr.Domain.ModelMetas
{
    public class LaborContractMeta
    {
        public string Type { get; set; }

        public string No { get; set; }

        public string UserId { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public string Note { get; set; }

        public string FileId { get; set; } // 1 file dinh kem

        public string FileName { get; set; }

        public bool IsUse { get; set; }

        public string LaborContractFormId { get; set; }

        public DateTime? RegisterDate { get; set; }

        public string TaskDescription { get; set; } // Mô tả công việc

        public string Time { get; set; } // Thời gian làm việc

        public int? SalaryCalculationUnit { get; set; } // Đơn vị tính lương. Ngày, Tuần, Tháng, Năm

        public decimal? BasicSalary { get; set; } // Lương cở bản

        public decimal? ProbationarySalary { get; set; } // Lương thử việc

        public decimal? OfficialSalary { get; set; } // Lương chính thức

        public UserStatus Status { get; set; } // Đối tượng, 0 dịch vụ, 1 học việc, 2 thử việc, 3 chính thức

        public string ConcurrencyStamp { get; set; }

        public LaborContractMeta()
        {
            IsUse = false;
            BasicSalary = 0;
            ProbationarySalary = 0m;
            OfficialSalary = 0m;
            Status = UserStatus.Collaborators;
        }
    }
}
