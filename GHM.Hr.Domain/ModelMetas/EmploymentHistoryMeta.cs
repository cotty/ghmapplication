﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GHM.Hr.Domain.ModelMetas
{
    public class EmploymentHistoryMeta
    {

        public int Id { get; set; }

        [DisplayName(@"Người dùng")]
        [Required(ErrorMessage = "Không được để trống.")]
        public string UserId { get; set; }

        public string FullName { get; set; }

        [DisplayName(@"Quá trình")]
        [Required(ErrorMessage = "Không được để trống.")]
        public bool Type { get; set; }

        [DisplayName(@"Từ ngày")]
        [Required(ErrorMessage = "Không được để trống.")]
        public DateTime FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public int OfficeId { get; set; }

        [DisplayName(@"Phòng ban")]
        [Required(ErrorMessage = "Không được để trống.")]
        public string OfficeName { get; set; }

        public int TitleId { get; set; }

        [DisplayName(@"Chức danh")]
        [Required(ErrorMessage = "Không được để trống.")]
        public string TitleName { get; set; }

        public int? CompanyId { get; set; }

        [DisplayName(@"Công ty")]
        public string CompanyName { get; set; }

        [DisplayName(@"Ghi chú")]
        [MaxLength(4000, ErrorMessage = "Không được phép vượt quá 4000 ký tự.")]
        public string Note { get; set; }

        public bool IsCurrent { get; set; }
    }
}
