﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Hr.Domain.ModelMetas
{
    public class UserAssessmentCriteriaCommentMeta
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public string Content { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string UserAssessmentCriteriaId { get; set; }
    }
}
