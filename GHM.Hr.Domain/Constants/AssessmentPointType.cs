﻿namespace GHM.Hr.Domain.Constants
{
    public enum AssessmentPointType
    {
        /// <summary>
        /// Điểm tự đánh giá.
        /// </summary>
        User,
        /// <summary>
        /// Điểm QLTT đánh giá.
        /// </summary>
        Manager,
        /// <summary>
        /// Điểm QLPD đánh giá.
        /// </summary>
        Approver,
        /// <summary>
        /// Điểm chuẩn
        /// </summary>
        StandardPoint
    }
}
