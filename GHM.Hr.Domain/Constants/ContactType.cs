﻿namespace GHM.Hr.Domain.Constants
{
    public enum ContactType
    {
        HomePhone,
        MobilePhone,
        Email,
        Fax
    }
}
