﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Hr.Domain.Constants
{
    public enum UserType
    {
        /// <summary>
        /// Nhân viên.
        /// </summary>
        Staff,
        /// <summary>
        /// Trưởng đơn vị.
        /// </summary>
        Leader,
        /// <summary>
        /// Phó đơn vị.
        /// </summary>
        ViceLeader
    }
}
