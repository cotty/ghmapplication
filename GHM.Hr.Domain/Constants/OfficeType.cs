﻿namespace GHM.Hr.Domain.Constants
{
    /// <summary>
    /// Loại đơn vị: Nhân sự, ban giám đốc, Đơn vị thường, Đơn vị đầu cuối.
    /// </summary>
    public enum OfficeType
    {
        /// <summary>
        /// Đơn vị thường.
        /// </summary>
        Normal,
        /// <summary>
        /// Nhân sự.
        /// </summary>
        Hr,
        /// <summary>
        /// Ban giám đốc.
        /// </summary>
        Directors,
        /// <summary>
        /// Đơn vị đầu cuối.
        /// </summary>
        Company
    }
}
    