﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Hr.Domain.Constants
{
    public enum CommendationDisciplineType
    {
        /// <summary>
        /// Khen thưởng
        /// </summary>
        Commendation,
        /// <summary>
        /// Kỷ luật
        /// </summary>
        Discipline
    }
}
