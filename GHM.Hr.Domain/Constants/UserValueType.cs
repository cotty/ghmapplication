﻿namespace GHM.Hr.Domain.Constants
{
    public enum UserValueType
    {
        LaborContract, // Loại hợp đồng
        Company, // Công ty
        TrainingCourse, // Khóa đào tạo
        TrainingPlace, // Nơi đào tạo
        EmploymentHistoryOffice, // Phòng ban QTCT
        EmploymentHistoryTitle, // Chức danh QTCT
        UserRecordsManagement, // Hồ sơ giấy tờ
        AcademicLevel, // Trình độ học vấn
        AcademicLevelDegree, // Bằng cấp
        AcademicLevelSchool, // Trường học
        AcademicLevelSpecialize, // Chuyên môn
        RelatedUser, // moi quan he

    }
}
