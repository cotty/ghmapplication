﻿namespace GHM.Hr.Domain.Constants
{
    public enum AcademicRank
    {
        /// <summary>
        /// Thạc Sỹ
        /// </summary>
        Master,
        /// <summary>
        /// Tiến sỹ
        /// </summary>
        PhD,
        /// <summary>
        /// Giáo sư
        /// </summary>
        Professor
    }
}
