﻿namespace GHM.Hr.Domain.Constants
{
    public enum AssessmentStatus
    {
        /// <summary>
        /// Mới.
        /// </summary>
        New,
        /// <summary>
        /// Chờ QLTT duyệt.
        /// </summary>
        WaitingManagerApprove,
        /// <summary>
        /// QLTT không duyệt.
        /// </summary>
        ManagerDecline,
        /// <summary>
        /// QLTT duyệt chờ QLPD duyệt.
        /// </summary>
        ManagerApproveWaitingApproverApprove,
        /// <summary>
        /// QLPD duyệt.
        /// </summary>
        ApproverApprove,
        /// <summary>
        /// QLPD không duyệt.
        /// </summary>
        ApproverDecline,
        /// <summary>
        /// Hủy kết quả. Sử dụng trong trường hợp đánh giá lại (Bắt buộc phải nhập lý do hủy).
        /// </summary>
        Cancel,
        /// <summary>
        /// QLTT đã duyệt. Được sử dụng trong trường hợp người dùng không có QLPD.
        /// </summary>
        ManagerApproved
    }
}
