﻿namespace GHM.Hr.Domain.Constants
{
    public enum AssessmentType
    {
       /// <summary>
       /// Người dùng đăng ký.
       /// </summary>
        User,
        /// <summary>
        /// Quản lý trực tiếp.
        /// </summary>
        Manager,
        /// <summary>
        /// Quản lý phê duyệt.
        /// </summary>
        Approver
    }
}
