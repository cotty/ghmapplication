﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Hr.Domain.Constants
{
    public enum UserStatus
    {
        /// <summary>        
        /// 0: Dịch vụ - CTV        
        /// </summary>        
        Collaborators,
        /// <summary>
        /// Học việc
        /// </summary>
        Apprentice,
        /// <summary>
        /// Thử việc
        /// </summary>
        Probation,
        /// <summary>
        /// Chính thức.
        /// </summary>
        Official,
        /// <summary>
        /// Thai sản.
        /// </summary>
        Maternity,
        /// <summary>
        /// Thôi việc.
        /// </summary>
        Discontinue,
        /// <summary>
        /// Nghỉ hửu
        /// </summary>
        Retirement
    }
}
