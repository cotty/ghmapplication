﻿namespace GHM.Hr.Domain.Constants
{
    /// <summary>
    /// Trạng thái phòng ban.
    /// 0: Đơn vị thường.
    /// 1: Phòng ban sát nhập.
    /// 2: Phan ban bị giải thể.
    /// </summary>
    public enum OfficeStatus
    {
        /// <summary>
        /// Đơn vị hiện tại.
        /// </summary>
        Normal,
        /// <summary>
        /// Phòng ban sát nhập.
        /// </summary>
        MergeOffice,
        /// <summary>
        /// Đơn vị bị giải thể.
        /// </summary>
        DissolutionOffice,
    }
}
