﻿using System.Collections.Generic;
using GHM.Hr.Domain.Constants;

namespace GHM.Hr.Domain.ViewModels
{
    public class OfficeEditViewModel
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public OfficeType OfficeType { get; set; }

        public int Order { get; set; }

        public OfficeStatus Status { get; set; }

        public int? ParentId { get; set; }

        public bool IsActive { get; set; }

        public List<OfficeTranslationViewModel> OfficeTranslations { get; set; }

        public List<OfficeContactViewModel> OfficeContacts { get; set; }
    }
}
