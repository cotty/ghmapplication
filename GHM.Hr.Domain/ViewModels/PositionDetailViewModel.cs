﻿using System.Collections.Generic;

namespace GHM.Hr.Domain.ViewModels
{
    public class PositionDetailViewModel
    {
        public string Id { get; set; }
        public bool IsActive { get; set; }
        public bool IsManager { get; set; }
        public bool IsMultiple { get; set; }
        public string TitleId { get; set; }
        public string ConcurrencyStamp { get; set; }
        public int Order { get; set; }
        public IEnumerable<PositionTranslationViewModel> PositionTranslations { get; set; }
        public IEnumerable<OfficePositionDetailViewModel> OfficesPositions { get; set; }
    }

    public class OfficePositionDetailViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
