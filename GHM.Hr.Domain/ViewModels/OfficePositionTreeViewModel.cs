﻿namespace GHM.Hr.Domain.ViewModels
{
    public class OfficePositionTreeViewModel
    {
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
        public int? ParentOfficeId { get; set; }
        public string PositionId { get; set; }
        public string PositionName { get; set; }
        public string OfficeIdPath { get; set; }
    }
}
