﻿using System;
using GHM.Hr.Domain.Constants;

namespace GHM.Hr.Domain.ViewModels
{
    public class ExportUserViewModel
    {
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string Id { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string TitleName { get; set; }
        public string PositionName { get; set; }
        public Gender? Gender { get; set; }
        public DateTime? Birthday { get; set; }
        public string PhoneNumber { get; set; }
        public string ResidentRegister { get; set; }
        public string Adress { get; set; }
        public string IdCardNumber { get; set; }
        public DateTime? IdCardDateOfIssue { get; set; }
        public string IdCardPlaceOfIssue { get; set; }
        public UserStatus Status { get; set; }
        public string LaborContractType { get; set; }
        public DateTime? OutDate { get; set; }
        public string SocialInsuranceNumber { get; set; }
        public string Tin { get; set; }
        public string BankingNumber { get; set; }
        public string BankName { get; set; }
        public string BranchBank { get; set; }
        public DateTime? JoinedDate { get; set; }
        public string Address { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
