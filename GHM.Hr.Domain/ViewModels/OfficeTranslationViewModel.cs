﻿namespace GHM.Hr.Domain.ViewModels
{
    public class OfficeTranslationViewModel
    {
        public string LanguageId { get; set; }

        public string Name { get; set; }

        public string ParentName { get; set; }

        public string ShortName { get; set; }

        public string Address { get; set; }

        public string Description { get; set; }
    }
}
