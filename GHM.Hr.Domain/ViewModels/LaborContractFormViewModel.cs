﻿namespace GHM.Hr.Domain.ViewModels
{
    public class LaborContractFormViewModel
    {
        public string Id { get; set; }
        public string LaborContractTypeId { get; set; }
        public string LaborContractTypeName { get; set; }
        public string FileId{ get; set; } // File đính kèm
        public bool IsActive { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
    }
}



