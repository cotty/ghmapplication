﻿using System.Collections.Generic;
using GHM.Hr.Domain.Constants;

namespace GHM.Hr.Domain.ViewModels
{
    public class UserAssessmentViewModel
    {
        public string Id { get; set; }

        public string UserId { get; set; }

        public byte Month { get; set; }

        public byte Quartar { get; set; }

        public int Year { get; set; }

        public string FullName { get; set; }

        public string PositionName { get; set; }

        public string OfficeName { get; set; }

        public AssessmentStatus Status { get; set; }

        public decimal TotalPoint { get; set; }

        public decimal TotalUserPoint { get; set; }

        public decimal? TotalManagerPoint { get; set; }

        public decimal? TotalApproverPoint { get; set; }

        public string ManagerUserId { get; set; }

        public string ApproverUserId { get; set; }

        public bool AllowAssessment { get; set; }

        public List<UserAssessmentCriteriaViewModel> UserAssessmentCriterias { get; set; }
    }
}
