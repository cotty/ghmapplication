﻿using System;
using System.Collections.Generic;
using GHM.Hr.Domain.Models;

namespace GHM.Hr.Domain.ViewModels
{
    public class TitleDetailViewModel
    {
        public string Id { get; set; }
        public bool IsActive { get; set; }
        public string ConcurrencyStamp { get; set; }
        public int Order { get; set; }
        public List<TitleTranslationViewModel> Translations { get; set; }
    }

    public class TitleTranslationViewModel
    {
        public string LanguageId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Description { get; set; }
    }
}
