﻿namespace GHM.Hr.Domain.ViewModels
{
    public class OfficePositionSearchViewModel
    {
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string PositionId { get; set; }
        public string PositionName { get; set; }
        public string TitlId { get; set; }
        public string TitleName { get; set; }
        public bool IsManager { get; set; }
        public int TotalUsers { get; set; }
        public bool IsMultiple { get; set; }
    }
}
