﻿namespace GHM.Hr.Domain.ViewModels
{
    public class TitleSearchForSelectViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
