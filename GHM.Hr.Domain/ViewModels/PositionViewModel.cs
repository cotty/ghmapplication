﻿using System;

namespace GHM.Hr.Domain.ViewModels
{
    public class PositionViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public string ShortName { get; set; }
        public bool IsManager { get; set; }
        public bool IsMultiple { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreateTime { get; set; }
    }
}
