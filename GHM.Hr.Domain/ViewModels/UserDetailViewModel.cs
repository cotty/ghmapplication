﻿using GHM.Hr.Domain.Constants;
using GHM.Infrastructure.Constants;
using System;
using System.Collections.Generic;
using UserType = GHM.Hr.Domain.Constants.UserType;

namespace GHM.Hr.Domain.ViewModels
{
    public class UserDetailViewModel
    {
        public string Id { get; set; }

        public string FullName { get; set; }

        public string UserName { get; set; }

        public string Avatar { get; set; }

        public DateTime? Birthday { get; set; } // Ngày sinh

        public string IdCardNumber { get; set; } // Số CMND

        public DateTime? IdCardDateOfIssue { get; set; } // Ngày cấp

        public Gender? Gender { get; set; } // Giới tính: 0 - Nam 1: Nữ 2: Giới tính khác

        public int? Ethnic { get; set; } // Dân tộc

        public string EthnicName { get; set; }

        public string DenominationName { get; set; }

        public string Tin { get; set; } // Mã số thuế cá nhân (Tax Identification number )

        public string BankingNumber { get; set; } // Số tài khoản ngân hàng

        public int? NationalId { get; set; }

        public string Nationality { get; set; }

        public int? ProvinceId { get; set; }

        public string ProvinceName { get; set; }

        public int? DistrictId { get; set; }

        public string DistrictName { get; set; }

        public MarriedStatus? MarriedStatus { get; set; } // Tình trạng hôn nhân: 0 - Chưa kết hôn 1: Đã kết hôn 2: Đã ly thân 3: Đã ly hôn

        public UserStatus Status { get; set; } // Trạng thái 0: Thử việc 1: Bình thường 2: Nghỉ ốm 3: nghỉ phép 4: Nghỉ thai sản 5: Thôi việc 6: Nghỉ hưu

        public int OfficeId { get; set; }
        public string OfficeIdPath { get; set; }
        public string PositionId { get; set; }
        public string TitleId { get; set; }

        // 0: Khác 1: Trưởng đơn vị 2:Phó đơn vị
        public UserType UserType { get; set; }

        public string PassportId { get; set; }

        public DateTime? PassportDateOfIssue { get; set; }

        public int? EnrollNumber { get; set; }
        public byte? Denomination { get; set; }
        public string CardNumber { get; set; }
        public bool IsAdmin { get; set; }
        public string Ext { get; set; }
        public string ConcurrencyStamp { get; set; }
        public TitlePrefixing TitlePrefixing { get; set; }
        public AcademicRank? AcademicRank { get; set; }
        public DateTime? JoinedDate { get; set; }
        public List<UserTranslationViewModel> UserTranslations { get; set; }
        public List<UserContactViewModel> UserContacts { get; set; }
    }
}
