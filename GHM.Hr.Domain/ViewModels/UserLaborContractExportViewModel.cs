﻿using System;

namespace GHM.Hr.Domain.ViewModels
{
    public class UserLaborContractExportViewModel
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public int? OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string Titlename { get; set; }
        public string PositionName { get; set; }
        public string ContractNo { get; set; }
        public string ContractType { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public bool? IsUse { get; set; }
    }
}
