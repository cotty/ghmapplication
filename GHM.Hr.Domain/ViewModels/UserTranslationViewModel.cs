﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Hr.Domain.ViewModels
{
    public class UserTranslationViewModel
    {
        public string UserId { get; set; }
        public string LanguageId { get; set; }
        public string OfficeName { get; set; }
        public string PositionName { get; set; }
        public string TitleName { get; set; }
        public string Nationality { get; set; }
        public string NativeCountry { get; set; }
        public string ResidentRegister { get; set; }
        public string Address { get; set; }
        public string Workplace { get; set; }
        public string PassportPlaceOfIssue { get; set; }
        public string BankName { get; set; }
        public string BranchBank { get; set; }
        public string IdCardPlaceOfIssue { get; set; }
    }
}
