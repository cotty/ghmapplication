namespace GHM.Hr.Domain.ViewModels
{
    public class ExportUserRecordManagermentViewModel
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string OfficeName { get; set; }
        public int? Profile { get; set; }
        public int? IdCardNumber { get; set; }
        public int? RegistrationBook { get; set; }
        public int? Degress { get; set; }
        public int? Certification { get; set; }
        public int? HealthCertification { get; set; }
        public int? BirthCertification { get; set; }
        public int? SecureCommitment { get; set; }
    }

}
