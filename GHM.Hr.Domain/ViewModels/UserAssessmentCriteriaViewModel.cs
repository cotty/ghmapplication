﻿namespace GHM.Hr.Domain.ViewModels
{
    public class UserAssessmentCriteriaViewModel
    {
        public string Id { get; set; }
        public string CriteriaGroupId { get; set; }
        public string CriteriaGroupName { get; set; }
        public string CriteriaId { get; set; }
        public string CriteriaName { get; set; }
        public decimal Point { get; set; }
        public decimal UserPoint { get; set; }
        public decimal? ManagerPoint { get; set; }
        public decimal? ApproverPoint { get; set; }
        public string Note { get; set; }
        public string Description { get; set; }
        public string Sanction { get; set; }

    }
}
