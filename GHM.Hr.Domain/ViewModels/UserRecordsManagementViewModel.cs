﻿using System;

namespace GHM.Hr.Domain.ViewModels
{
    public class UserRecordsManagementViewModel
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string ValueId { get; set; }
        public string ValueName { get; set; }
        public string Title { get; set; }
        public bool IsSelected { get; set; }
        public string Note { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string AttachmentId { get; set; }
        public string AttachmentName { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
