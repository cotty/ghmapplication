﻿using GHM.Hr.Domain.Constants;

namespace GHM.Hr.Domain.ViewModels
{
    public class OfficeSearchViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Code { get; set; }
        public string IdPath { get; set; }
        public bool IsActive { get; set; }
        public int? ParentId { get; set; }
        public string ParentName { get; set; }
        public int ChildCount { get; set; }
        public OfficeType OfficeType { get; set; }
    }
}
