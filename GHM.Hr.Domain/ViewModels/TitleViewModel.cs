﻿using System;

namespace GHM.Hr.Domain.ViewModels
{
    public class TitleViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Description { get; set; }
        public DateTime CreateTime { get; set; }
        public bool IsActive { get; set; }
        public int Order { get; set; }        
    }
}
