﻿namespace GHM.Hr.Domain.ViewModels
{
    public class OfficeShortInfoViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ParentName { get; set; }

        public string ShortName { get; set; }

        public string Address { get; set; }

        public string Description { get; set; }

        public string Code { get; set; }

        public string OfficeIdPath { get; set; }

        public bool IsActive { get; set; }
    }
}
