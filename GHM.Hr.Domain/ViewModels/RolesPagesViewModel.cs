﻿namespace GHM.Hr.Domain.ViewModels
{
    public class RolesPagesViewModel
    {
        public int PageId { get; set; }
        public int Permission { get; set; }
    }
}
