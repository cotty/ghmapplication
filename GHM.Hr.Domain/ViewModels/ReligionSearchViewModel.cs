﻿namespace GHM.Hr.Domain.ViewModels
{
    public class ReligionSearchViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
