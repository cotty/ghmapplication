﻿using GHM.Hr.Domain.Constants;
using System;

namespace GHM.Hr.Domain.ViewModels
{
    public class UserLaborContractDetailViewModel
    {
        public string Id { get; set; }
        public string No { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Type { get; set; }
        public string TypeName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreatorId { get; set; }
        public string Note { get; set; }
        public string FileId { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public bool IsUse { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string LaborContractFormId { get; set; } // Mẫu hợp đồng
        public string LaborContractFormName { get; set; }
        public DateTime? RegisterDate { get; set; }
        public string TaskDescription { get; set; } // Mô tả công việc
        public string Time { get; set; } // Thời gian làm việc
        public int? SalaryCalculationUnit { get; set; } // Đơn vị tính lương. Ngày, Tuần, Tháng, Năm
        public decimal? BasicSalary { get; set; } // Lương cở bản
        public decimal? ProbationarySalary { get; set; } // Lương thử việc
        public decimal? OfficialSalary { get; set; } // Lương chính thức
        public UserStatus? Status { get; set; } // Đối tượng, 0 dịch vụ, 1 học việc, 2 thử việc, 3 chính thức
    }
}
