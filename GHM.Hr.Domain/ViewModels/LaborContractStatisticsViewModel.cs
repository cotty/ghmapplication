﻿namespace GHM.Hr.Domain.ViewModels
{
    public class LaborContractStatisticsViewModel
    {
        public int Total { get; set; }
        public int InUse { get; set; }
        public int ExpiresInThisMonth { get; set; }
        public int ExpiresInNextMonth { get; set; }
    }
}
