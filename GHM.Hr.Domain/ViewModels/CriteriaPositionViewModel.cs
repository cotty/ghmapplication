﻿namespace GHM.Hr.Domain.ViewModels
{
    public class CriteriaPositionViewModel
    {
        public string CriteriaId { get; set; }
        public string PositionId { get; set; }
        public string CriteriaName { get; set; }
        public decimal Point { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public string Sanction { get; set; }
        public string GroupId { get; set; }
        public string GroupName { get; set; }
    }
}
