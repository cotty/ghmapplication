﻿using GHM.Hr.Domain.Constants;

namespace GHM.Hr.Domain.ViewModels
{
    public class UserContactViewModel
    {
        public string Id { get; set; }
        public ContactType ContactType { get; set; }
        public string ContactValue { get; set; }
        public string ConcurrencyStamp { get; set; }
    }
}
