﻿using System.Collections.Generic;
using GHM.Hr.Domain.Constants;

namespace GHM.Hr.Domain.ViewModels
{
    public class OfficeDetailViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ParentName { get; set; }

        public string ShortName { get; set; }

        public string Address { get; set; }

        public string Description { get; set; }

        public string Code { get; set; }

        public OfficeType OfficeType { get; set; }

        //public int Order { get; set; }

        public OfficeStatus Status { get; set; }

        //public int? ParentId { get; set; }

        public bool IsActive { get; set; }

        //public int ChildCount { get; set; }      

        //public List<OfficeTranslationViewModel> OfficeTranslations { get; set; }

        public List<OfficeContactViewModel> OfficeContacts { get; set; }
    }
}
