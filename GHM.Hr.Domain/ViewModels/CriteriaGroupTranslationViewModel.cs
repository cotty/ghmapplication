﻿namespace GHM.Hr.Domain.ViewModels
{
    public class CriteriaGroupTranslationViewModel
    {
        public string CriteriaGroupId { get; set; }
        public string LanguageId { get; set; }
        public string Name { get; set; }
        public string UnsignName { get; set; }
        public string Description { get; set; }
    }
}
