﻿using GHM.Hr.Domain.Constants;
using System;

namespace GHM.Hr.Domain.ViewModels
{
    public class UserLaborContractViewModel
    {
        public string Id { get; set; }
        public string No { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string OfficeName { get; set; }
        public string TitleName { get; set; }
        public string PositionName { get; set; }
        public string Type { get; set; }
        public string TypeName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime CreateTime { get; set; }
        public string FileId { get; set; }
        public string FileName { get; set; }
        public bool IsUse { get; set; }
        public string LaborContractFormId { get; set; }
        public string LaborContractFormName { get; set; }
        public DateTime? RegisterDate { get; set; }
        public UserStatus? Status { get; set; }
    }
}
