﻿namespace GHM.Hr.Domain.ViewModels
{
    public class CriteriaViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public bool IsActive { get; set; }

        public string Description { get; set; }

        public string Sanction { get; set; }

        public decimal Point { get; set; }

        public string GroupId { get; set; }

        public string GroupName { get; set; }

        public int Order { get; set; }
    }
}
