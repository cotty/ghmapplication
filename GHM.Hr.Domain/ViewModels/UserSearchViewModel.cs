﻿using GHM.Hr.Domain.Constants;
using System;

namespace GHM.Hr.Domain.ViewModels
{
    public class UserSearchViewModel
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public UserStatus Status { get; set; }
        public string Avatar { get; set; }
        public string PositionId { get; set; }
        public string PositionName { get; set; }
        public string TitleId { get; set; }
        public string TitleName { get; set; }
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string OfficeIdPath { get; set; }
        public DateTime? JoinedDate { get; set; }
        public DateTime? BirthDay { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string ApproverFullName { get; set; }
        public string ManagerFullName { get; set; }
        public MarriedStatus? MarriedStatus { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string Tin { get; set; }
        public decimal? DefaultHolidayQuantity { get; set; }
        public string IdCardNumber { get; set; }
    }
}
