﻿using GHM.Infrastructure.Constants;

namespace GHM.Hr.Domain.ViewModels
{
    public class OfficeContactViewModel
    {
        public string Id { get; set; }

        public string UserId { get; set; }

        public string FullName { get; set; }

        public string Avatar { get; set; }

        public string OfficeName { get; set; }

        public string PositionName { get; set; }

        public TitlePrefixing TitlePrefixing { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string Fax { get; set; }
    }
}
