﻿namespace GHM.Hr.Domain.ViewModels
{
    public class PositionSearchForSelectViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
