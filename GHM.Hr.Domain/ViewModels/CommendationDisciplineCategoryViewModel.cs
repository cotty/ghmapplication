﻿using GHM.Hr.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Hr.Domain.ViewModels
{
    public class CommendationDisciplineCategoryViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public CommendationDisciplineType Type { get; set; }
        public string ConcurrencyStamp { get; set; }
    }
}
