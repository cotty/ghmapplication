﻿namespace GHM.Hr.Api.Infrastructure.Models.ViewModels
{
    public class UserSearchForManagerConfigViewModel
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public string PositionName { get; set; }
        public string PositionId { get; set; }
        public string ManagerId { get; set; }
        public string ManagerName { get; set; }
        public string ApproveId { get; set; }
        public string ApproveName { get; set; }
    }
}
