﻿using GHM.Hr.Domain.Models;

namespace GHM.Hr.Domain.ViewModels
{
    public class LaborContractFormDetailViewModel
    {
        public string Id { get; set; }
        public string LaborContractTypeId { get; set; }
        public string LaborContractTypeName { get; set; }
        public string FileId { get; set; } // File đính kèm
        public bool IsActive { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
    }
}
