﻿namespace GHM.Hr.Domain.ViewModels
{
    public class UserSuggestionViewModel
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string Name { get; set; }        
        public string OfficeName { get; set; }
        public string PositionName { get; set; }
        public string Avatar { get; set; }
        public string Image { get; set; }
    }
}
