﻿using MediatR;

namespace GHM.Hr.Domain.Commands
{
    public class UpdateManagerCommand : INotification
    {
        /// <summary>
        /// Mã khách hàng update
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Mã người dùng update
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Mã quản lý.
        /// </summary>
        public string ManagerId { get; set; }

        /// <summary>
        /// Trường hợp true là QLTT false là quản lý phê duyệt.
        /// </summary>
        public bool IsManager { get; set; }
    }
}
