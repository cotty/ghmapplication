﻿using System;
using System.Collections.Generic;

namespace GHM.Hr.Domain.Models
{
    public class Criteria
    {
        public string Id { get; set; }

        public string TenantId { get; set; }

        public string GroupId { get; set; }

        public bool IsActive { get; set; }

        public bool IsDelete { get; set; }

        public decimal Point { get; set; }

        public DateTime CreateTime { get; set; }

        public string ConcurrencyStamp { get; set; }

        public int Order { get; set; }

        public List<CriteriaTranslation> Translations { get; set; }

        public Criteria()
        {
            IsActive = true;
            IsDelete = false;
            Point = 0;
            Id = Guid.NewGuid().ToString();
            ConcurrencyStamp = Guid.NewGuid().ToString();
            CreateTime = DateTime.Now;

            Translations = new List<CriteriaTranslation>();
        }
    }

}
