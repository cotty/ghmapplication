
using System;
using System.Collections.Generic;
using GHM.Hr.Domain.Constants;

namespace GHM.Hr.Domain.Models
{
    public class UserAssessment
    {
        public string TenantId { get; set; }

        public string Id { get; set; }

        /// <summary>
        /// Mã nhân sự đánh giá.
        /// </summary>        
        public string UserId { get; set; }

        /// <summary>
        /// Mã chức danh nhân sự đánh giá.
        /// </summary>
        public string PositionId { get; set; }

        /// <summary>
        /// Mã phòng ban nhân sự đánh giá.
        /// </summary>
        public int OfficeId { get; set; }


        /// <summary>
        /// Thời gian tạo
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// Thời gian gửi đánh giá.
        /// </summary>
        public DateTime? SendTime { get; set; }

        /// <summary>
        /// Tháng
        /// </summary>
        public byte Month { get; set; }

        /// <summary>
        /// Quý
        /// </summary>
        public byte Quarter { get; set; }

        /// <summary>
        /// Năm
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Trạng thái đánh giá.
        /// </summary>
        public AssessmentStatus Status { get; set; }

        /// <summary>
        /// Mã quản lý trực tiếp.
        /// </summary>
        public string ManagerUserId { get; set; }

        /// <summary>
        /// Mã quản lý phê duyệt.
        /// </summary>
        public string ApproverUserId { get; set; }

        /// <summary>
        /// Tổng điểm.
        /// </summary>
        public decimal TotalPoint { get; set; }

        /// <summary>
        /// Tổng điểm nhân sự tự chấm.
        /// </summary>
        public decimal TotalUserPoint { get; set; }

        /// <summary>
        /// Tổng điểm QLTT tự chấm.
        /// </summary>
        public decimal? TotalManagerPoint { get; set; }

        /// <summary>
        /// Tổng điểm QLPD chấm.
        /// </summary>
        public decimal? TotalApproverPoint { get; set; }

        /// <summary>
        /// Nhân sự ghi chú.
        /// </summary>
        public string UserNote { get; set; }

        /// <summary>
        /// QLTT ghi chú.
        /// </summary>
        public string ManagerNote { get; set; }

        /// <summary>
        /// QLPD ghi chú.
        /// </summary>
        public string ApproverNote { get; set; }

        /// <summary>
        /// Thời gian QLTT duyệt
        /// </summary>
        public DateTime? ManagerApproveTime { get; set; }

        /// <summary>
        /// Thời gian QLPD duyệt.
        /// </summary>
        public DateTime? ApproverApproveTime { get; set; }

        /// <summary>
        /// Trạng thái hoàn thành.
        /// </summary>
        public DateTime? EndTime { get; set; }

        public string ConcurrencyStamp { get; set; }

        public string ManagerDeclineReason { get; set; }

        public string ApproverDeclineReason { get; set; }

        public List<UserAssessmentCriteria> UserAssessmentCriterias { get; set; }

        public UserAssessment()
        {
            Id = Guid.NewGuid().ToString();
            ConcurrencyStamp = Guid.NewGuid().ToString();
            CreateTime = DateTime.Now;
            Status = AssessmentStatus.New;
        }
    }

}
