﻿using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Models;

namespace GHM.Hr.Domain.Models
{
    public class OfficeContact : EntityBase<string>
    {
        /// <summary>
        /// Mã phòng ban.
        /// </summary>
        public int OfficeId { get; set; }

        /// <summary>
        /// Mã người liên hệ.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Tên người liên hệ.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Ảnh đại diện.
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// Tiền tố trước tên 1 người thường sử dụng trong tiếng anh VD: Ông, Bà, Bác sỹ.
        /// </summary>
        public TitlePrefixing TitlePrefixing { get; set; }

        /// <summary>
        /// Email người liên hệ
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///  Số điện thoại người liên hệ.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Số fax.
        /// </summary>
        public string Fax { get; set; }

        public bool IsDelete { get; set; }
    }
}
