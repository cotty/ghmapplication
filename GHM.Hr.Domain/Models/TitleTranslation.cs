﻿namespace GHM.Hr.Domain.Models
{
    public class TitleTranslation
    {
        public string TenantId { get; set; }

        public string TitleId { get; set; }

        public string LanguageId { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }

        public string UnsignName { get; set; }

        public string Description { get; set; }

        public Title Title { get; set; }
    }
}
