﻿using System.ComponentModel.DataAnnotations;

namespace GHM.Hr.Domain.Models
{
    public class CriteriaTitleTranslation
    {
        /// <summary>
        /// Mã tiêu chí.
        /// </summary>
        [Key, MaxLength(50)]
        public string CriteriaId { get; set; }

        /// <summary>
        /// Mã tiêu chí.
        /// </summary>
        [Key, MaxLength(50)]
        public string TitleId { get; set; }

        /// <summary>
        /// Mã ngôn ngữ
        /// </summary>
        [Key, MaxLength(50)]
        public string LanguageId { get; set; }

        /// <summary>
        /// Tên tiêu chí.
        /// </summary>
        [Required, MaxLength(1000)]
        public string Name { get; set; }

        /// <summary>
        /// Tên không dấu
        /// </summary>
        [Required, MaxLength(50)]
        public string UnsignName { get; set; }
        
        /// <summary>
        /// Mô tả tiêu chí
        /// </summary>
        [Required, MaxLength(4000)]
        public string Description { get; set; }

        /// <summary>
        /// Quy định chế tài.
        /// </summary>
        [Required, MaxLength(4000)]
        public string Sanction { get; set; }

        /// <summary>
        /// Ghi chú.
        /// </summary>
        [MaxLength(4000)]
        public string Note { get; set; }
    }
}
