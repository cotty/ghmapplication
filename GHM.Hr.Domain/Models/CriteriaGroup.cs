﻿using System;
using System.Collections.Generic;

namespace GHM.Hr.Domain.Models
{
    public class CriteriaGroup
    {
        public string Id { get; set; }

        public string TenantId { get; set; }

        public bool IsActive { get; set; }

        public bool IsDelete { get; set; }

        public int Order { get; set; }

        public string ConcurrencyStamp { get; set; }

        public List<CriteriaGroupTranslation> Translations { get; set; }

        public CriteriaGroup()
        {
            Id = Guid.NewGuid().ToString();
            ConcurrencyStamp = Guid.NewGuid().ToString();
            IsActive = true;
            IsDelete = false;
            Order = 0;
            Translations = new List<CriteriaGroupTranslation>();
        }
    }
}
