﻿namespace GHM.Hr.Domain.Models
{
    public class UserHolidayRemaining
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public int Year { get; set; }
        public decimal HolidayRemaining { get; set; }
        public decimal TotalHolidays { get; set; }
        public string TenantId { get; set; }
    }
}
