﻿using System;

namespace GHM.Hr.Domain.Models
{
    public class Config
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public string LanguageId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string ConcurrencyStamp { get; set; }
        public DateTime CreateTime { get; set; }

        public Config()
        {
            Id = Guid.NewGuid().ToString();
            CreateTime = DateTime.Now;
            ConcurrencyStamp = Guid.NewGuid().ToString();
        }
    }
}
