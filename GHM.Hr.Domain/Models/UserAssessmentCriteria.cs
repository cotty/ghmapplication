﻿using System;
using System.Collections.Generic;

namespace GHM.Hr.Domain.Models
{
    public class UserAssessmentCriteria
    {
        /// <summary>
        /// Mã tiêu chí đánh giá nhân sự.
        /// </summary>        
        public string Id { get; set; }

        /// <summary>
        /// Mã kỳ đánh giá.
        /// </summary>        
        public string UserAssessmentId { get; set; }

        /// <summary>
        /// Mã tiêu chí đánh giá.
        /// </summary>
        public string CriteriaId { get; set; }

        public decimal Point { get; set; }

        /// <summary>
        /// Điểm nhân sự tự chấm.
        /// </summary>
        public decimal UserPoint { get; set; }

        /// <summary>
        /// Điểm QLTT chấm.
        /// </summary>
        public decimal? ManagerPoint { get; set; }

        /// <summary>
        /// Điểm QLPD chấm.
        /// </summary>
        public decimal? ApproverPoint { get; set; }

        public string Note { get; set; }

        public UserAssessment UserAssessment { get; set; }

        public List<UserAssessmentCriteriaComment> UserAssessmentCriteriaComments { get; set; }

        public UserAssessmentCriteria()
        {
            Id = Guid.NewGuid().ToString();
        }
    }

}
