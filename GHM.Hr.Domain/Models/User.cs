﻿using GHM.Hr.Domain.Constants;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Models;
using System;
using UserType = GHM.Hr.Domain.Constants.UserType;

namespace GHM.Hr.Domain.Models
{
    public class User : EntityBase<string>
    {
        /// <summary>
        /// Mã khách hàng.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Họ tên nhân viên.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Tên không dấu hỗ trợ tìm kiếm.
        /// </summary>
        public string UnsignName { get; set; }

        public string UserName { get; set; }

        public string Avatar { get; set; }

        /// <summary>
        /// Họ
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Tên đệm.
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Tên.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Ngày tháng năm sinh.
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// Số CMT
        /// </summary>
        public string IdCardNumber { get; set; }

        /// <summary>
        /// Ngày cấp CMT
        /// </summary>
        public DateTime? IdCardDateOfIssue { get; set; }

        /// <summary>
        /// Giới tính.
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// Mã quốc gia
        /// </summary>
        public int? NationalId { get; set; }

        /// <summary>
        /// Mã dân tộc.
        /// </summary>
        public int? Ethnic { get; set; }

        /// <summary>
        /// Tên dân tộc.
        /// </summary>
        public string EthnicName { get; set; }

        /// <summary>
        /// Tôn giáo: Null là không theo tôn giáo nào. 1: Phật giáo 2: Thiên chúa giáo 3: hindu
        /// </summary>
        public byte? Denomination { get; set; }

        public string DenominationName { get; set; }

        /// <summary>
        /// Mã số thuế cá nhân (Tax Identification number )
        /// </summary>
        public string Tin { get; set; }

        /// <summary>
        /// Ngày rời khỏi công ty
        /// </summary>
        public DateTime? OutDate { get; set; }

        /// <summary>
        /// Mã Tỉnh thành
        /// </summary>
        public int? ProvinceId { get; set; }

        /// <summary>
        /// Tên tỉnh thành
        /// </summary>
        public string ProvinceName { get; set; }

        /// <summary>
        /// Mã Quận huyện.
        /// </summary>
        public int? DistrictId { get; set; }

        /// <summary>
        /// Tên quận huyện.
        /// </summary>
        public string DistrictName { get; set; }

        /// <summary>
        /// Tình trạng hôn nhân: 0 - Chưa kết hôn 1: Đã kết hôn 2: Đã ly thân 3: Đã ly hôn.
        /// </summary>
        public MarriedStatus? MarriedStatus { get; set; }

        /// <summary>
        /// Trạng thái
        /// </summary>        
        public UserStatus Status { get; set; }

        ///// <summary>
        ///// Mã phòng ban.
        ///// </summary>
        public int OfficeId { get; set; }

        /// <summary>
        /// IdPath phòng ban.
        /// </summary>
        public string OfficeIdPath { get; set; }

        /// <summary>
        /// Mã chức vụ.
        /// </summary>
        public string TitleId { get; set; }

        /// <summary>
        /// Mã Chức danh.
        /// </summary>
        public string PositionId { get; set; }

        /// <summary>
        /// 0: Khác
        /// 1: Trưởng đơn vị
        /// 2: Phó đơn vị
        /// </summary>
        public UserType UserType { get; set; }

        /// <summary>
        /// Mã QLTT.
        /// </summary>
        public string ManagerUserId { get; set; }

        /// <summary>
        /// Tên QLTT.
        /// </summary>
        public string ManagerFullName { get; set; }

        /// <summary>
        /// Mã QLPD.
        /// </summary>
        public string ApproverUserId { get; set; }

        /// <summary>
        /// Tên QLPD.
        /// </summary>
        public string ApproverFullName { get; set; }

        /// <summary>
        /// Mã hộ chiếu.
        /// </summary>
        public string PassportId { get; set; }

        /// <summary>
        /// Ngày cấp hộ chiếu.
        /// </summary>
        public DateTime? PassportDateOfIssue { get; set; }

        /// <summary>
        /// Mã chấm công.
        /// </summary>
        public int? EnrollNumber { get; set; }

        /// <summary>
        /// Mã thẻ chấm công.
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// Tiền tố
        /// </summary>
        public TitlePrefixing TitlePrefixing { get; set; }

        /// <summary>
        /// Học hàm học vị
        /// </summary>
        public AcademicRank? AcademicRank { get; set; }

        /// <summary>
        /// Số tài khoản ngân hàng
        /// </summary>
        public string BankingNumber { get; set; }

        public decimal? HolidayRemain { get; set; }
        public decimal? LastYearHolidayRemain { get; set; }
        public decimal? DefaultHolidayQuantity { get; set; }

        /// <summary>
        /// Số máy lẻ
        /// </summary>
        public string Ext { get; set; }
        /// <summary>
        /// Ngày vào làm việc
        /// </summary>
        public DateTime? JoinedDate { get; set; }

        /// <summary>
        /// Có phải admin
        /// </summary>
        public bool IsAdmin { get; set; }

        /// <summary>
        /// Đánh dấu trạng thái xóa bản ghi.
        /// </summary>
        public bool IsDelete { get; set; }

        public User()
        {
            MarriedStatus = 0;
            Status = UserStatus.Probation;
            CreateTime = DateTime.Now;
            UserType = UserType.Staff;
            TitlePrefixing = TitlePrefixing.Dr;
            IsDelete = false;
            IsAdmin = false;
        }
    }
}
