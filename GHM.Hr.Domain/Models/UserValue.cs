using System;
using System.ComponentModel.DataAnnotations;
using GHM.Hr.Domain.Constants;

namespace GHM.Hr.Domain.Models
{
    public class UserValue
    {
        // Gia tri
        public string Id { get; set; }
        public string Name { get; set; }
        public string UnsignName { get; set; }
        public string TenantId { get; set; }
        /*
         * Loại giá trị: 0: Loại hợp đồng.
         * 1: Công ty trong BHXH.
         * 2: Công ty trong quá trình làm việc.
         * 3: Chương trình đào tạo.
         * 4: Nơi đào tạo.
         * 5: Phòng ban QTCT.
         * 6: Chức danh QTCT.
         * 7: Hồ sơ giấy tờ.
         * 11: moi quan he
         */
        public UserValueType Type { get; set; }

        public UserValue()
        {
            Id = Guid.NewGuid().ToString();
        }
    }  
}
