﻿using System.ComponentModel.DataAnnotations;

namespace GHM.Hr.Domain.Models
{
    public class UserTranslation
    {
        /// <summary>
        /// Mã nhân viên
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// Mã ngôn ngữ
        /// </summary>
        public string LanguageId { get; set; }
        /// <summary>
        /// Tên phòng ban.
        /// </summary>
        public string OfficeName { get; set; }
        /// <summary>
        /// Tên chức danh.
        /// </summary>
        public string PositionName { get; set; }
        /// <summary>
        /// Tên chức vụ.
        /// </summary>
        public string TitleName { get; set; }
        /// <summary>
        /// Quốc tịch.
        /// </summary>
        public string Nationality { get; set; }
        /// <summary>
        /// Quê quán.
        /// </summary>
        public string NativeCountry { get; set; }        
        /// <summary>
        /// Hộ khẩu thường trú.
        /// </summary>
        public string ResidentRegister { get; set; }
        /// <summary>
        /// Địa chỉ liên hệ.
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// Nơi cấp hộ chiếu.
        /// </summary>
        public string PassportPlaceOfIssue { get; set; }

        /// <summary>
        /// Tên ngân hàng
        /// </summary>
        public string BankName { get; set; }

        /// <summary>
        /// Chi nhánh ngân hàng
        /// </summary>
        public string BranchBank { get; set; }

        /// <summary>
        /// Nơi cấp chưng minh thư.
        /// </summary>
        public string IdCardPlaceOfIssue { get; set; }
    }
}
