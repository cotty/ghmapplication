﻿using System;

namespace GHM.Hr.Domain.Models
{
    public class CriteriaGroupTranslation
    {

        public string TenantId { get; set; }

        public string CriteriaGroupId { get; set; }

        public string LanguageId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string UnsignName { get; set; }

        public bool IsDelete { get; set; }

        public CriteriaGroup CriteriaGroup { get; set; }

        public CriteriaGroupTranslation()
        {
        }
    }
}
