﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Hr.Domain.Models
{
    public class File
    {
        public string TenantId { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public long Size { get; set; }

        public string Url { get; set; }

        public string CreatorAvatar { get; set; }

        public string Extension { get; set; }

        public int? FolderId { get; set; }

        public bool? IsTrash { get; set; } // 0: Có lưu trữ trong dirver, 1: không lưu trữ trong driver
    }
}
