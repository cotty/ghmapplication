
using System;
using System.ComponentModel.DataAnnotations;

namespace GHM.Hr.Domain.Models
{
    public class UserInsurance
    {
        public string Id { get; set; }
        // Loại đóng bảo hiểm: 0: Trước khi vào công ty 1: Trong công ty
        public bool Type { get; set; }
        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string UnsignName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public decimal Premium { get; set; }
        public string Note { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public DateTime CreateTime { get; set; }
        public string TenantId { get; set; }
        public string ConcurrencyStamp { get; set; }

        public UserInsurance()
        { 
            CreateTime = DateTime.Now;
        }
    }

}
