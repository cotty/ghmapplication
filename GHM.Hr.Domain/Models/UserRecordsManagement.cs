using System;
using System.ComponentModel.DataAnnotations;

namespace GHM.Hr.Domain.Models
{
    public class UserRecordsManagement
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        public string ValueId { get; set; }
        public string Note { get; set; }
        public string AttachmentId { get; set; }
        public bool IsSelected { get; set; }
        public string TenantId { get; set; }
        public string AttachmentName { get; set; }
        public string ConcurrencyStamp { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreateTime { get; set; }
        public UserRecordsManagement()
        {
            IsSelected = false;
            Id = Guid.NewGuid().ToString();
            ConcurrencyStamp = Id;
            IsDelete = false;
            CreateTime = DateTime.Now;

        }
    }
}
