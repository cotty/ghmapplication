
using System;
using System.Collections.Generic;
using GHM.Infrastructure.Models;

namespace GHM.Hr.Domain.Models
{
    public class Title : EntityBase<string>
    {
        public int OldTitleId { get; set; }

        /// <summary>
        /// Mã Khách hàng.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Trạng thái xóa.
        /// </summary>
        public bool IsDelete { get; set; }

        /// <summary>
        /// Trạng thái sử dụng.
        /// </summary>
        public bool IsActive { get; set; }

        public List<TitleTranslation> Translations { get; set; }

        public Title()
        {
            IsDelete = false;
            CreateTime = DateTime.Now;
            IsActive = true;

            Translations = new List<TitleTranslation>();
        }
    }

}
