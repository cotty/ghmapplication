using System;
using GHM.Hr.Domain.Constants;

namespace GHM.Hr.Domain.Models
{
    public class UserPositioncs
    {
        /// <summary>
        /// Mã nhân sự.
        /// </summary>        
        public string UserId { get; set; }

        /// <summary>
        /// Mã chức vụ.
        /// </summary>
        public string PositionId { get; set; }

        /// <summary>
        /// Mã phòng ban.
        /// </summary>
        public int OfficeId { get; set; }

        /// <summary>
        /// IdPath của phòng ban
        /// </summary>
        public string OfficeIdPath { get; set; }

        /// <summary>
        /// Là trưởng đơn vị, phó đơn vị, nhân viên.
        /// </summary>
        public UserType UserType { get; set; }

        /// <summary>
        /// Là quản lý.
        /// </summary>        
        public bool IsManager { get; set; }

        /// <summary>
        /// Là kiêm nhiệm hay không.
        /// </summary>
        public bool IsDefault { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime? ToDate { get; set; }
    }

}
