﻿namespace GHM.Hr.Domain.Models
{
    public class AssessmentTimeConfig
    {
        public int? FromDay { get; set; }
        public int? ToDay { get; set; }
    }
}
