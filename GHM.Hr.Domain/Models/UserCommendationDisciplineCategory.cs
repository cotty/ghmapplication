using System;
using System.ComponentModel.DataAnnotations;
using GHM.Hr.Domain.Constants;

namespace GHM.Hr.Domain.Models
{
    // Khen thưởng - kỷ luật.
    public class UserCommendationDisciplineCategory
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string UnsignName { get; set; }
        public CommendationDisciplineType Type { get; set; }
        public string TenantId { get; set; }
        public string ConcurrencyStamp { get; set; }
        public bool IsDelete { get; set; }
        
        public UserCommendationDisciplineCategory()
        {
            Id = Guid.NewGuid().ToString();
            ConcurrencyStamp = Id;
            IsDelete = false;
        }
    }

}
