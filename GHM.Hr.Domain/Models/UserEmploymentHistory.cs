using System;

namespace GHM.Hr.Domain.Models
{
    public class UserEmploymentHistory
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public bool Type { get; set; } //False là ngoài công ty.True là trong công ty
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string TitleId { get; set; }
        public string TitleName { get; set; }
        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string Note { get; set; }
        public bool IsDelete { get; set; }
        public bool IsCurrent { get; set; }
        public string UnsignName { get; set; }
        public DateTime CreateTime { get; set; }
        public string TenantId { get; set; }
        public string ConcurrencyStamp { get; set; }

        public UserEmploymentHistory()
        {
            IsDelete = false;
            CreateTime = DateTime.Now;
        }
    }
}
