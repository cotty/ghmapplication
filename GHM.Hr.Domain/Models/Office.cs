using GHM.Hr.Domain.Constants;
using GHM.Infrastructure.Models;

namespace GHM.Hr.Domain.Models
{
    public class Office : EntityBase<int>
    {
        /// <summary>
        /// Mã khách hàng.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Mã đơn vị người dùng tự nhập.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Loại đơn vị: 
        /// </summary>
        public OfficeType OfficeType { get; set; }

        /// <summary>
        /// Thứ tự sắp xếp.
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Thứ tự sắp xếp theo cây thư mục.
        /// </summary>
        public string OrderPath { get; set; }

        /// <summary>
        /// Trạng thái phòng ban.
        /// </summary>
        public OfficeStatus Status { get; set; }

        /// <summary>
        /// Trạng thái xóa
        /// </summary>
        public bool IsDelete { get; set; }

        /// <summary>
        /// IdPath của phòng ban.
        /// </summary>
        public string IdPath { get; set; }

        /// <summary>
        /// Mã đơn vị cha
        /// </summary>
        public int? ParentId { get; set; }

        /// <summary>
        /// Trạng thái sử dụng.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Cấp độ phòng ban.
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// Số lượng phòng ban con.
        /// </summary>
        public int ChildCount { get; set; }

        ///// <summary>
        ///// Là công ty độc lập sẽ chỉ xem được các phòng ban hoặc công ty con của công ty này mà không được phép xem các công ty ngang hàng.
        ///// </summary>
        //public bool IsCompany { get; set; }

        /// <summary>
        /// Mã của khu làm việc bị giới hạn. VD mỗi công ty độc lập sẽ là 1 root và các phòng ban thuộc cty độc lập 
        /// sẽ có RootId là id của công ty độc lập.
        /// </summary>
        public int RootId { get; set; }

        /// <summary>
        /// Là IdPath của các root. VD 1 công ty độc lập nằm trong 1 cty khác. Thì RootIdPath sẽ RootIdPath của công ty cha 
        /// kèm theo RootId của công ty hiện tại
        /// </summary>
        public string RootIdPath { get; set; }

        public Office()
        {
            Order = 0;
            Status = OfficeStatus.Normal;
            IsDelete = false;
            IsActive = true;
            Level = 0;
            ChildCount = 0;
        }
    }

}
