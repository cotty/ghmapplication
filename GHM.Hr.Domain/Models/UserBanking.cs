﻿namespace GHM.Hr.Domain.Models
{
    public class UserBanking
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string AccountNumber{ get; set; }
        public string Branch { get; set; }
    }
}
