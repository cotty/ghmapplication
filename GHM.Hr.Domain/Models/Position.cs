
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GHM.Hr.Domain.Models
{
    public class Position
    {
        /// <summary>
        /// Mã chức vụ.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Mã chức vụ cũ để backup. Sẽ xóa sau khi đồng bộ hệ thống xong.
        /// </summary>
        public int OldPositionId { get; set; }

        /// <summary>
        /// Mã chức vụ.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Trạng thái xóa.
        /// </summary>
        public bool IsDelete { get; set; }

        /// <summary>
        /// Thứ tự sắp xếp.
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Ngày giờ tạo.
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// Mã kiểm tra lúc update.
        /// </summary>
        public string ConcurrencyStamp { get; set; }

        /// <summary>
        /// Trạng thái kích hoạt.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Trạng thái có đánh gia thành tích không.
        /// </summary>
        public bool IsHasAssessment { get; set; }

        /// <summary>
        ///  Có phải là quản lý không.
        /// </summary>
        public bool IsManager { get; set; }

        /// <summary>
        /// Có cho phép chọn nhiều không.
        /// </summary>
        public bool IsMultiple { get; set; }

        /// <summary>
        /// Mã chức danh là gì.
        /// </summary>
        public string TitleId { get; set; }

        public List<PositionTranslation> Translations { get; set; }

        public Position()
        {
            IsDelete = false;
            Order = 0;
            CreateTime = DateTime.Now;
        }
    }
}
