﻿using System;

namespace GHM.Hr.Domain.Models
{
    public class UserAssessmentCriteriaComment
    {
        public string Id { get; set; }
        public string UserAssessmentCriteriaId { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public string Content { get; set; }
        public DateTime CreateTime { get; set; }
        public string ConcurrencyStamp { get; set; }
        public DateTime? LastUpdate { get; set; }
        public bool IsDelete { get; set; }

        public UserAssessmentCriteria UserAssessmentCriteria { get; set; }

        public UserAssessmentCriteriaComment()
        {
            var guidid = Guid.NewGuid().ToString();
            Id = guidid;
            ConcurrencyStamp = guidid;
            CreateTime = DateTime.Now;
            IsDelete = false;
        }
    }
}
