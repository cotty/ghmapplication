﻿using GHM.Hr.Domain.Constants;
using GHM.Infrastructure.Models;

namespace GHM.Hr.Domain.Models
{
    public class UserContact : EntityBase<string>
    {
        public string UserId { get; set; }
        public ContactType ContactType { get; set; }
        public string ContactValue { get; set; }
        public string TenantId { get; set; }
    }
}
