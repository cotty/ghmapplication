﻿namespace GHM.Hr.Domain.Models
{
    public class CriteriaTranslation
    {
        public string CriteriaId { get; set; }

        public string LanguageId { get; set; }

        public string Name { get; set; }

        public string UnsignName { get; set; }

        public string Description { get; set; }

        public string Sanction { get; set; }

        public string TenantId { get; set; }

        public bool IsDelete { get; set; }

        public Criteria Criteria { get; set; }

        public CriteriaTranslation()
        {
            IsDelete = false;
        }
    }
}
