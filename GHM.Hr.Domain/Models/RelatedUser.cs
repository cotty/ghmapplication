﻿using GHM.Hr.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Hr.Domain.Models
{
    public class RelatedUser
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public string UserId { get; set; }
        public string ValueId { get; set; }
        public string ValueName { get; set; }
        public string FullName { get; set; }
        public Gender Gender { get; set; }
        public DateTime BirthDay { get; set; }
        public string IdCardNumber { get; set; }
        public DateTime IdCardDateOfIssue { get; set; }
        public string IdCardPlaceOfIssue { get; set; }
        public string Phone { get; set; }
        public int? NationalId { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public string DenominationName { get; set; } // Ton giao
        public int? Ethnic { get; set; } // Dan toc
        public string Job { get; set; }
        public string PermanentAddress { get; set; } // Dia chi thuong chu
        public string ContactAddress { get; set; }
        public DateTime CreateTime { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
        public bool IsDelete { get; set; }

        public RelatedUser()
        {
            Id = Guid.NewGuid().ToString();
            CreateTime = DateTime.Now;
            ConcurrencyStamp = Id;
            IsDelete = false;
        }
    }
}
