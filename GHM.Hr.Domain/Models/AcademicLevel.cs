using System;

namespace GHM.Hr.Domain.Models
{
    public class AcademicLevel
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public string AcademicLevelId { get; set; } // Mã trình độ học vấn
        public string AcademicLevelName { get; set; } // Tên trình độ học vấn
        public string DegreeId { get; set; } // Mã học vị
        public string DegreeName { get; set; } // Tên học vị
        public string SchoolId { get; set; } // Mã trường học
        public string SchoolName { get; set; } // Tên trường học
        public string SpecializeId { get; set; } // Mã chuyên ngành
        public string SpecializeName { get; set; } // Tên chuyên ngành
        public string UserId { get; set; } // Mã nhân viên
        public bool IsDelete { get; set; }
        public string Note { get; set; } // Ghi chú
        public string ConcurrencyStamp { get; set; }

        public AcademicLevel()
        {
            IsDelete = false;
            Id = Guid.NewGuid().ToString();
            ConcurrencyStamp = Guid.NewGuid().ToString(); 
        }
    }
}
