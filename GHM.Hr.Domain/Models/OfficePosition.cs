using System.Collections.Generic;

namespace GHM.Hr.Domain.Models
{
    public class OfficePosition
    {
        /// <summary>
        /// Mã chức vụ
        /// </summary>      
        public string PositionId { get; set; }

        /// <summary>
        /// Mã phòng ban.
        /// </summary>
        public int OfficeId { get; set; }

        /// <summary>
        /// Mã chức vụ
        /// </summary>
        public string TitleId { get; set; }

        /// <summary>
        /// Tổng số người có chức vụ này.
        /// </summary>
        public int TotalUser { get; set; }

        public OfficePosition()
        {
            TotalUser = 0;
        }
    }

}
