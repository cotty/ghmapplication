
using System;
using System.ComponentModel.DataAnnotations;

namespace GHM.Hr.Domain.Models
{
    public class UserTrainingHistory
    {
        public string Id { get; set; }
        public bool Type { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string CourseId { get; set; }
        public string CourseName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Result { get; set; }
        public string CoursePlaceId { get; set; }
        public string CoursePlaceName { get; set; }
        public bool IsHasCertificate { get; set; }
        public string UnsignName { get; set; }
        public DateTime CreateTime { get; set; }
        public bool IsDelete { get; set; }
        public string TenantId { get; set; }
        public string ConcurrencyStamp { get; set; }

        public UserTrainingHistory()
        {
            Id = Guid.NewGuid().ToString();
            IsDelete = false;
            CreateTime = DateTime.Now;
            ConcurrencyStamp = Id;
        }
    }

}
