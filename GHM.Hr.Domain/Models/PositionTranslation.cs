﻿namespace GHM.Hr.Domain.Models
{
    public class PositionTranslation
    {
        /// <summary>
        /// Mã khách hàng.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Mã chức vụ.
        /// </summary>
        public string PositionId { get; set; }

        /// <summary>
        /// Mã ngôn ngữ.
        /// </summary>
        public string LanguageId { get; set; }

        /// <summary>
        /// Tên chức danh.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Tên chức danh không dấu. Hỗ trợ tìm kiếm.
        /// </summary>
        public string UnsignName { get; set; }

        /// <summary>
        /// Tên ngắn của chức danh.
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Mô tả chức danh.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Tên chức danh theo ngôn ngữ.
        /// </summary>
        public string TitleName { get; set; }

        /// <summary>
        /// Yêu cầu khác.
        /// </summary>
        public string OtherRequire { get; set; }

        /// <summary>
        /// Pham vi trách nhiệm.
        /// </summary>
        public string Responsibility { get; set; }

        /// <summary>
        /// Mục đích.
        /// </summary>
        public string Purpose { get; set; }

        public Position Position { get; set; }
    }
}
