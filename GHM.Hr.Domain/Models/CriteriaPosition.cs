﻿using System.ComponentModel.DataAnnotations;

namespace GHM.Hr.Domain.Models
{
    public class CriteriaPosition
    {
        /// <summary>
        /// Mã tiêu chí.
        /// </summary>
        public string CriteriaId { get; set; }

        /// <summary>
        /// Mã tiêu chí.
        /// </summary>
        public string PositionId { get; set; }

        /// <summary>
        /// Mã khách hàng
        /// </summary>
        public string TenantId { get; set; }

        public bool IsActive { get; set; }

        public CriteriaPosition()
        {
            IsActive = true;
        }
    }
}
