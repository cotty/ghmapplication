
using System;
using System.ComponentModel.DataAnnotations;

namespace GHM.Hr.Domain.Models
{
    public class UserCommendationDiscipline
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public bool IsCommendation { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime ApplyTime { get; set; }
        public string DecisionNo { get; set; }
        public string Reason { get; set; }
        public decimal? Money { get; set; }
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string UnsignName { get; set; }
        public string AttachmentUrl { get; set; }
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string TitleId { get; set; }
        public string TitleName { get; set; }
        public int Month { get; set; }
        public int Quarter { get; set; }
        public int Year { get; set; }
        public bool IsDelete { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
        public string TenantId { get; set; }
        public string ConcurrencyStamp { get; set; }
        public bool IsApprove { get; set; }
        public UserCommendationDiscipline()
        {
            IsDelete = false;
            CreateTime = DateTime.Now;
            Id = Guid.NewGuid().ToString();
            ConcurrencyStamp = Id;
            IsApprove = false;
        }
    }

}
