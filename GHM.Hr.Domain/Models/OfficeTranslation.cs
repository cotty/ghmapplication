﻿namespace GHM.Hr.Domain.Models
{
    public class OfficeTranslation
    {
        /// <summary>
        /// Cần RootId cho việc check trùng tên.
        ///        
        /// </summary>
        /// 
        public int RootId { get; set; }

        public int OfficeId { get; set; }

        public string LanguageId { get; set; }

        /// <summary>
        /// Tên đơn vị
        /// </summary>        
        public string Name { get; set; }

        /// <summary>
        /// Tên đơn vị cha
        /// </summary>
        public string ParentName { get; set; }

        /// <summary>
        /// Tên đường dẫn đơn vị
        /// </summary>
        public string NamePath { get; set; }

        /// <summary>
        /// Tên không dấu để tìm kiếm
        /// </summary>
        public string UnsignName { get; set; }

        /// <summary>
        /// Tên ngắn đơn vị
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Địa chỉ đơn vị
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Mô tả đơn vị
        /// </summary>
        public string Description { get; set; }
    }
}
