using System;

namespace GHM.Hr.Domain.Models
{
    public class LaborContractForm
    {
        public string Id { get; set; }
        public string LaborContractTypeId { get; set; }
        public string FileId { get; set; } // File đính kèm
        public string FileName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public string Description { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string TenantId { get; set; }
        public string Name { get; set; }
        public string UnsignName { get; set; }

        public LaborContractForm()
        {
            var id = Guid.NewGuid().ToString();
            IsDelete = false;
            Id = id;
            ConcurrencyStamp = id; 
        }
    }
}
