﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Domain.IServices
{
    public interface IOfficeContactService
    {
        Task<ActionResultResponse<string>> Insert(OfficeContactMeta officeContactMeta);

        Task<List<ActionResultResponse<string>>> Inserts(List<OfficeContactMeta> officeContactMetas);

        Task<ActionResultResponse> Update(OfficeContactMeta officeContactMeta);

        Task<ActionResultResponse> Update(string id, OfficeContactMeta officeContactMeta);

        Task<ActionResultResponse> Delete(int officeId, string userId);

        Task<SearchResult<OfficeContactViewModel>> GetsByOfficeId(int officeId, string languageId);
    }
}
