﻿using System.Threading.Tasks;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Domain.IServices
{
    public interface ICriteriaService
    {
        Task<ActionResultResponse> Insert(CriteriaMeta criteriaMeta);

        Task<ActionResultResponse> Update(string id, CriteriaMeta criteriaMeta);

        Task<ActionResultResponse> Delete(string tenantId, string id);

        Task<ActionResultResponse<Criteria>> GetDetail(string tenantId, string id);

        Task<SearchResult<Suggestion<string>>> GetsAllCriteriaGroup(string tenantId, string languageId);

        Task<SearchResult<CriteriaViewModel>> Search(string tenantId, string languageId, string keyword, string groupId, bool? isActive, int page, int pageSize);

        Task<ActionResultResponse> InsertGroup(CriteriaGroupMeta criteriaGroupMeta);

        Task<ActionResultResponse> UpdateGroup(string id, CriteriaGroupMeta criteriaGroupMeta);

        Task<ActionResultResponse> DeleteGroup(string tenantId, string userId, string fullName, string id);

        Task<SearchResult<CriteriaGroupViewModel>> SearchGroup(string tenantId, string languageId, string keyword, bool? isActive,
            int page, int pageSize);

        Task<ActionResultResponse<CriteriaGroup>> GetGroupDetail(string tenantId, string id);
    }
}
