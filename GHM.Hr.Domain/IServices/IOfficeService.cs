﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Domain.IServices
{
    public interface IOfficeService
    {
        Task<List<OfficeSearchViewModel>> GetsAll(string tenantId, string languageId);

        Task<SearchResult<OfficeSearchViewModel>> Search(string currentUserId, string tenantId, string languageId, string keyword,
            bool? isActive, int page, int pageSize);

        Task<SearchResult<OfficeSuggestionViewModel>> SearchForSuggestion(string tenantId, string languageId, string keyword, int page, int pageSize);

        Task<SearchResult<OfficeSearchViewModel>> SearchTree(string currentUserId, string tenantId, string languageId, string keyword, bool? isActive,
            int page, int pageSize);

        Task<List<TreeData>> GetFullOfficeTree(string tenantId, string languageId);

        Task<List<TreeData>> GetOfficeUserTree(string tenantId, string userId, string languageId);

        Task<ActionResultResponse> Insert(string tenantId, OfficeMeta officeMeta);

        Task<ActionResultResponse> Update(string tenantId, int officeId, OfficeMeta officeMeta);

        Task<ActionResultResponse> Delete(string tenantId, int id);

        Task<ActionResultResponse<OfficeDetailViewModel>> GetDetail(string tenantId, string languageId, int id);

        Task<ActionResultResponse<OfficeEditViewModel>> GetDetailForEdit(string tenantId, string languageId, int id);

        #region Office contacts
        Task<ActionResultResponse<string>> InsertContact(string tenantId, int officeId, OfficeContactMeta officeContactMeta);

        Task<ActionResultResponse> UpdateContact(string tenantId, int officeId, string contactId, OfficeContactMeta officeContactMeta);

        Task<ActionResultResponse> DeleteContact(string tenantId, int officeId, string contactId);
        #endregion
    }
}
