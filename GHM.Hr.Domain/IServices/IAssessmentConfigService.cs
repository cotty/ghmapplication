﻿using System.Threading.Tasks;
using GHM.Hr.Domain.Models;
using GHM.Infrastructure.Models;

namespace GHM.Hr.Domain.IServices
{
    public interface IAssessmentConfigService
    {
        Task<ActionResultResponse> SaveAssessmentTime(string tenantId, int fromDay, int toDay);

        Task<ActionResultResponse<AssessmentTimeConfig>> GetAssessmentTime(string tenantId);
    }
}
