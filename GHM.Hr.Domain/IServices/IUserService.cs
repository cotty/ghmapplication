﻿using GHM.Hr.Api.Infrastructure.Models.ViewModels;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ShortUserInfoViewModel = GHM.Infrastructure.ViewModels.ShortUserInfoViewModel;

namespace GHM.Hr.Domain.IServices
{
    public interface IUserService
    {
        Task<bool> VerifyLeader(string leaderId, string userId);
        Task<bool> VerifyLeader(string leaderId, int officeId);
        Task<ActionResultResponse> Insert(string tenantId, UserMeta userMeta);
        Task<ActionResultResponse> Delete(string tenantId, string userId);
        Task<ActionResultResponse<string>> Update(string id, string tenantId, UserMeta userMeta);
        Task<ActionResultResponse> UpdateAvatar(string tenantId, string userId, string avatar);
        Task<ActionResultResponse> UpdateStatus(string tenantId, string userId, UserStatus status);
        Task<SearchResult<UserSearchViewModel>> Search(string tenantId, string languageId, string keyword, UserStatus? status,
           string officeIdPath, string positionId, Gender? gender, byte? monthBirthDay, int? yearBirthday, AcademicRank? academicRank,
            int page, int pageSize);
        Task<SearchResult<ExportUserViewModel>> GetListExportUser(string tenantId, string languageId, string keyword, UserStatus? status, string officeIdPath,
            string positionId, Gender? gender, byte? monthBirthDay, int? yearBirthday, AcademicRank? academicRank);
        Task<UserDetailViewModel> GetUserDetail(string tenantId, string id);
        Task<ActionResultResponse> UpdateDirectManager(string tenantId, string id, string managerId);
        Task<ActionResultResponse> UpdateApproveManager(string tenantId, string id, string approveId);
        Task<SearchResult<UserSuggestionViewModel>> GetUserForSuggestion(string tenantId, string languageId, string keyword,
            int? officeId, int page, int pageSize);
        Task<SearchResult<UserSearchForManagerConfigViewModel>> SearchForConfigManager(string tenantId, string languageId, string keyword, int officeId, byte? type,
            bool? isGetStaffFromChildOffice, int page, int pageSize);
        Task<ActionResultResponse> UpdateDirectManagerAndApproveManager(string tenantId, List<string> userIds, string managerId, string approveId);

        Task<ShortUserInfoViewModel> GetShortUserInfo(string tenantId, string userId, string languageId);

        Task<List<ShortUserInfoViewModel>> GetShortUserInfoByListUserId(string tenantId, List<string> userIds, string languageId);

        Task<ShortUserInfoViewModel> GetShorUserInfoOfLeaderOffice(string tenantId, int officeId, string languageId);

        Task<ShortUserInfoViewModel> GetDirectManagerInfo(string tenantId, string userId, string languageId);

        //Lay danh sach nhan vien cung cap hoac cap duoi
        Task<SearchResult<ShortUserInfoViewModel>> GetListUserChildren(string tenantId, string userId, string languageId,
            bool isShowChildren, int page, int pageSize);

        Task<ActionResultResponse<bool>> CheckUserNameExists(string userName);
 
    }
}
