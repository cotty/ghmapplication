﻿using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Hr.Domain.IServices
{
    public interface ITrainingHistoryService
    {
        Task<SearchResult<UserTrainingHistory>> Search(string tenantId, string keyword, string userId, bool? type, 
            string courseId,string courseName, string coursePlaceId,
            string coursePlaceName, bool? isHasCertification, DateTime? fromDate, DateTime? toDate, int page, int pageSize);

        Task<ActionResultResponse<string>> Insert(string tenantId, string userId, string fullName, TrainingHistoryMeta training);

        Task<ActionResultResponse<string>> Update(string id, string tenantId, string userId, string fullName, TrainingHistoryMeta training);

        Task<ActionResultResponse> Delete(string id, string tenantId);
    }
}
