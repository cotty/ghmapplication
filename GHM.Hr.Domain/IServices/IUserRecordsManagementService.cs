﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Domain.IServices
{
    public interface IUserRecordsManagementService
    {
        Task<ActionResultResponse<string>> Insert(string tenantId, UserRecordsManagementMeta userRecordsManagementMeta);

        Task<SearchResult<UserRecordsManagementViewModel>> Search(string tenantId, string userId,string valueId, string title, DateTime? fromDate, DateTime? toDate, int page, int pageSize);

        Task<ActionResultResponse<string>> Update(string tenantId, string id, UserRecordsManagementMeta userRecordsManagementMeta);

        Task<ActionResultResponse> Delete(string tenantId, string id);
    }
}
