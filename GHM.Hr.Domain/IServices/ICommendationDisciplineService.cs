﻿using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Hr.Domain.IServices
{
    public interface ICommendationDisciplineService
    {
        Task<SearchResult<UserCommendationDiscipline>> Search(string keyword, string userId, bool? type, string categoryId, DateTime? fromDate, DateTime? toDate, int? officeId, string titleId, int page, int pageSize, out object totalRows);

        Task<ActionResultResponse<string>> Insert(string tenantId, string userId, string fullName, CommendationDisciplineMeta commendationDisciplineMeta);

        Task<ActionResultResponse<string>> Update(string id, string tenantId, CommendationDisciplineMeta commendationDisciplineMeta);

        Task<ActionResultResponse> Delete(string tenantId, string id);
    }
}
