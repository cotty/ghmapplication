﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Domain.IServices
{
    public interface ITitleService
    {
        Task<ActionResultResponse> Insert(string tenantId, TitleMeta titleMeta);

        Task<ActionResultResponse> Update(string tenantId, string id, TitleMeta titleMeta);

        Task<ActionResultResponse> Delete(string tenantId, string id);

        Task<ActionResultResponse<TitleDetailViewModel>> GetDetail(string tenantId, string id);

        Task<SearchResult<TitleViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive,
            int page, int pageSize);

        Task<List<TitleSearchForSelectViewModel>> SearchForSelect(string tenantId, string languageId, string keyword);
    }
}
