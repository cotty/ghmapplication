﻿using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Hr.Domain.IServices
{
    public interface IRelatedUserService
    {
        Task<SearchResult<RelatedUserViewModel>> Search(string tenantId, string keyword, string userId, string valueId, string fullName, string idCardNumber, string phone, int page, int pageSize);

        Task<ActionResultResponse<string>> Insert(string tenantId, string id, string fullName, RelatedUserMeta relatedUserMeta);

        Task<ActionResultResponse<string>> Update(string tenantId, string id, RelatedUserMeta relatedUserMeta);

        Task<ActionResultResponse> Delete(string tenantId, string id);
    }
}
