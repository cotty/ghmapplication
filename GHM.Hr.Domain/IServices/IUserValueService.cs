﻿using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using System.Threading.Tasks;

namespace GHM.Hr.Domain.IServices
{
    public interface IUserValueService
    {
        Task<ActionResultResponse<string>> Insert(string tenantId, UserValueMeta userValueMeta);

        Task<ActionResultResponse> Update(string tenantId, string id, UserValueMeta userValueMeta);

        Task<ActionResultResponse> Delete(string tenantId, string id);

        Task<SearchResult<UserValueViewModel>> Search(string tenantId, string keyword, UserValueType type, int page, int pageSize);
    }
}
