﻿using System.Threading.Tasks;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Domain.IServices
{
    public interface IUserAssessmentCriteriaCommentService
    {
        Task<ActionResultResponse<string>> Insert(string tenantId, UserAssessmentCriteriaCommentMeta userAssessmentCriteriaCommentMeta);

        Task<ActionResultResponse<string>> Update(string id, UserAssessmentCriteriaCommentMeta userAssessmentCriteriaCommentMeta);

        Task<ActionResultResponse> Delete(string userId, string id);

        Task<SearchResult<UserAssessmentCriteriaComment>> Search(string tenantId, string userId, string userAssessmentCriteriaId, int page,
            int pageSize);
    }
}
