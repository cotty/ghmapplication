﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Domain.IServices
{
    public interface ICriteriaGroupService
    {
        Task<ActionResultResponse> Insert(CriteriaGroupMeta criteriaGroupMeta);

        Task<ActionResultResponse> Update(string id, CriteriaGroupMeta criteriaGroupMeta);

        Task<ActionResultResponse> Delete(string tenantId, string id);

        Task<ActionResultResponse<CriteriaGroup>> GetGroupDetail(string tenantId, string id);

        Task<SearchResult<CriteriaGroupViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive, int page,
            int pageSize);
    }
}
