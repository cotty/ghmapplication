﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Domain.IServices
{
    public interface IComendationDisciplineCategoryService
    {
        Task<SearchResult<CommendationDisciplineCategoryViewModel>> Search(string tenantId, string keyword, string name, CommendationDisciplineType type, int page, int pageSize);

        Task<ActionResultResponse<string>> InsertAsync(string tenantId, CommendationDisciplineCategoryMeta commendationDisciplineCatoryMeta);

        Task<ActionResultResponse<string>> UpdateAsync(string tenantId, string id, CommendationDisciplineCategoryMeta commendationDisciplineCategoryMeta);

        Task<ActionResultResponse> Delete(string tenantId, string id);
    }
}
