﻿using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Hr.Domain.IServices
{
    public interface ILaborContactService
    {
        Task<ActionResultResponse<string>> Insert(string tenantId, string currenctUserId, string languageId, LaborContractMeta laborContractMeta);

        Task<ActionResultResponse<string>> Update(string tenantId, string id, string langugeId, LaborContractMeta laborContractMeta);

        Task<ActionResultResponse> Delete(string tenantId, string id);

        Task<SearchResult<UserLaborContractViewModel>> Search(string tenantId, string languageId, string keyword, string userId,
            int? officeId, string titleId, string positionId, string type,
            DateTime? fromDate, DateTime? toDate, bool? isUse, byte? month, int page, int pageSize);

        Task<SearchResult<UserLaborContractViewModel>> SearchExpires(string tenantId, string languageId, string keyword,
            string userId,  bool isNext, string type,
            DateTime? fromDate, DateTime? toDate, int page, int pageSize);

        Task<SearchResult<UserLaborContractExportViewModel>> GetListLaborContractExport(string tenantId, string languageId, string officeIdPath);

        Task<ActionResultResponse<string>> InsertLaborContractForm(string tenantId, LaborContractFormMeta laborContractFormMeta);

        Task<ActionResultResponse> UpdateLaborContractForm(string tenantId, string id, LaborContractFormMeta laborContractFormMeta);

        Task<ActionResultResponse> DeleteLaborContractForm(string tenantId, string id);

        Task<SearchResult<Suggestion<string>>> GetLaborContractFormByType(string tenantId, string keyword, string type, int page, int pageSize);

        Task<SearchResult<LaborContractFormViewModel>> SearchLaborContractForm(string tenantId, string keyword, string type, int page, int pageSize);

        Task<ActionResultResponse<LaborContractFormDetailViewModel>> GetLaborContractFormDetail(string tenantId, string id);

        Task<ActionResultResponse<UserLaborContractDetailViewModel>> GetLaborContractDetail(string tenantId, string id);
    }
}
