﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Domain.IServices
{
    public interface IPositionService
    {
        Task<ActionResultResponse> Insert(string tenantId, PositionMeta positionMeta);

        Task<ActionResultResponse> Update(string tenantId, string id, PositionMeta positionMeta);

        Task<ActionResultResponse> Delete(string tenantId, string id);

        Task<ActionResultResponse<PositionDetailViewModel>> GetDetail(string tenantId, string languageId, string id);

        Task<SearchResult<PositionViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive
            , bool? isManager, bool? isMultiple, int page, int pageSize);

        Task<ActionResultResponse<TitleSearchForSelectViewModel>> GetTitleByPositionId(string positionId, string languageId, string tenantId);

        Task<List<PositionSearchForSelectViewModel>> SearchForSelect(string tenantId, string languageId, string keyword);
    }
}
