﻿using System.Threading.Tasks;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Domain.IServices
{
    public interface ICriteriaPositionService
    {
        Task<ActionResultResponse> Insert(CriteriaPosition criteriaPosition);

        Task<ActionResultResponse> Delete(string tenantId, string criteriaId, string positionId);

        Task<SearchResult<CriteriaPositionViewModel>> Search(string tenantId, string languageId, string positionId,
            bool? isActive);

        Task<ActionResultResponse> UpdateActiveStatus(string tenantId, string criteriaId, string positionId, bool isActive);
    }
}
