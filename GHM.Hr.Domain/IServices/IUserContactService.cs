﻿using GHM.Hr.Domain.ModelMetas;
using GHM.Infrastructure.Models;
using System.Threading.Tasks;

namespace GHM.Hr.Domain.IServices
{
    public interface IUserContactService
    {
        Task<ActionResultResponse<string>> Insert(string tenantId, UserContactMeta userContactMeta);
        Task<ActionResultResponse> Update(string tenantId, string id, UserContactMeta userContactMeta);
        Task<ActionResultResponse> Delete(string tenantId, string id);
    }
}
