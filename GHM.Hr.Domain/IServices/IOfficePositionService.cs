﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using OfficeTreeViewModel = GHM.Hr.Domain.ViewModels.OfficeTreeViewModel;

namespace GHM.Hr.Domain.IServices
{
    public interface IOfficePositionService
    {
        Task<ActionResultResponse> Insert(string tenantId, int officeId, string positionId);

        Task<ActionResultResponse> Inserts(string tenantId, int officeId, string[] positionIds);

        Task<ActionResultResponse> Delete(string tenantId, int officeId, string positionId);

        Task<SearchResult<OfficePositionSearchViewModel>> Search(string tenantId, string languageId,
            string keyword, int officeId, int page, int pageSize);

        Task<ActionResultResponse<List<OfficeTreeViewModel>>> GetTree(string tenantId, string languageId);
    }
}
