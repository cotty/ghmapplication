﻿using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using System.Threading.Tasks;

namespace GHM.Hr.Domain.IServices
{
    public interface IAcademicLevelService
    {
        Task<ActionResultResponse<string>> Insert(string tenantId, AcademicLevelMeta academicLevelMeta);

        Task<ActionResultResponse<string>> Update(string teanntId, string id, AcademicLevelMeta academicLevelMeta);

        Task<ActionResultResponse> Delete(string tenantId, string id);

        Task<ActionResultResponse<AcademicLevelDetailViewModel>> GetDetail(string tenantId, string id);

        Task<SearchResult<AcademicLevelViewModel>> Search(string tenantId, string userId, string levelId, string degreeId, string schoolId,
            string specializeId, int page, int pageSize);
    }
}
