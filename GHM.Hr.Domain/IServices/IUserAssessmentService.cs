﻿using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Domain.IServices
{
    public interface IUserAssessmentService
    {
        Task<ActionResultResponse<string>> Register(string tenantId, string languageId, string userId, byte month, int year);

        Task<ActionResultResponse<UserAssessmentViewModel>> GetDetail(string tenantId, string languageId, string userId,
            string currentUserId, byte month, int year);

        Task<ActionResultResponse<UserAssessmentViewModel>> GetDetail(string tenantId, string languageId, string userId, string id);

        Task<SearchResult<UserAssessmentViewModel>> SearchMyAssessment(string tenantId, string languageId,
            string userId, int year);

        Task<SearchResult<UserAssessmentViewModel>> ManagerSearch(string tenantId, string languageId, string currentUserId, string keyword,
            byte? month, int? year, int page, int pageSize);

        Task<SearchResult<UserAssessmentViewModel>> ApproverSearch(string tenantId, string languageId, string currentUserId,
            string keyword, byte? month, int? year, int page, int pageSize);

        Task<ActionResultResponse> UpdateUserAssessmentCriteria(string tenantId, string currentUserId, string id,
            AssessmentType type, UserAssessmentCriteria userAssessmentCriteria);

        Task<ActionResultResponse> UpdateStatus(string tenantId, string currentUserId, string currentUserFullName, string currentUserAvatar,
            string id, UserAssessmentMeta userAssessmentMeta);

        Task<SearchResult<UserAssessmentViewModel>> SearchResult(string tenantId, string languageId, string userId,
            string keyword, AssessmentStatus? status, int? officeId, byte? month, int year, int page, int pageSize);

        Task<ActionResultResponse> UpdateUserAssessmentManager(string tenantId, string userId, string managerId, bool isManager);

        Task<ActionResultResponse> RequestReassessment(string tenantId, string positionId, byte month, int year, bool applyForAll);
    }
}
