﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;

namespace GHM.Hr.Domain.IRepository
{
    public interface IUserValueRepository
    {
        Task<int> Insert(UserValue userValue);

        Task<int> Update(UserValue userValue);

        Task<List<UserValue>> GetAllValueByType(string tenantId, UserValueType type);

        Task<UserValue> GetInfo(string tenantId, string id, bool isReadOnly = false);

        Task<UserValue> GetInfo(string tenantId, string id, UserValueType type, bool isReadOnly = false);

        Task<UserValue> GetValueByName(string tenantId, string name, UserValueType type, bool isReadOnly = false);

        Task<bool> CheckNameExists(string tenantId, string id, string name, UserValueType type);

        Task<List<UserValueViewModel>> Search(string tenantId, string keyword, UserValueType type, int page, int pageSize, out int totalRows);

        Task<UserValue> GetInfoIfNotExistsInsert(string tenantId, string id, string value, UserValueType type);

        Task<int> Delete(UserValue userValue);

        Task<bool> CheckExist(string tenantId, string courseId, string courseName, UserValueType type);

        Task<bool> CheckExist(string tenantId, string id);
    }
}
