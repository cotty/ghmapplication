﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;

namespace GHM.Hr.Domain.IRepository
{
    public interface IEmploymentHistoryRepository
    {
        Task<int> Insert(UserEmploymentHistory employment);

        Task<int> Update(UserEmploymentHistory employment);

        Task<int> Delete(string tenantId, string id);

        Task<UserEmploymentHistory> GetInfo(string tenantId, string id, bool isReadOnly = false);

        Task<bool> CheckExists(string tenantId, string id, string userId, string companyId, int officeId,
            string titleId, DateTime fromDate, DateTime? toDate);

        Task<List<UserEmploymentHistory>> Search(string tenantId, string keyword, string userId, bool? type, string companyId,
            bool? isCurrent, DateTime? fromDate, DateTime? toDate, int page, int pageSize, out int totalRows);
    }
}
