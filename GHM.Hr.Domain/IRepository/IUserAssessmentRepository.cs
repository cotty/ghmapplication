﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;

namespace GHM.Hr.Domain.IRepository
{
    public interface IUserAssessmentRepository
    {
        Task<bool> CheckExists(string tenantId, string userId, string positionId, byte month, int year);

        Task<int> Insert(UserAssessment userAssessment);

        Task<int> UpdateTotalPoint(UserAssessment userAssessment);

        Task<int> UpdateTotalUserPoint(UserAssessment userAssessment);

        Task<int> UpdateTotalManagerPoint(UserAssessment userAssessment);

        Task<int> UpdateTotalApproverPoint(UserAssessment userAssessment);

        Task<int> UpdateStatus(UserAssessment userAssessment);

        Task<UserAssessment> GetInfo(string tenantId, string id, bool isReadonly = false);

        Task<UserAssessment> GetInfo(string tenantId, string userId, byte month, int year, bool isReadonly = false);

        Task<List<UserAssessmentViewModel>> GetByYear(string tenantId, string languageId, string userId, int year);

        Task<List<UserAssessmentViewModel>> UserSearchAssessment(string tenantId, string languageId, string userId, int year);

        Task<List<UserAssessmentViewModel>> ManagerSearchForApprove(string tenantId, string languageId, string userId, string keyword,
            byte? month, int? year, int page, int pageSize, out int totalRows);

        Task<List<UserAssessmentViewModel>> ApproverSearchForApprove(string tenantId, string languageId, string userId, string keyword,
            byte? month, int? year, int page, int pageSize, out int totalRows);

        Task<List<UserAssessmentViewModel>> SearchResult(string tenantId, string languageId, string userId, string keyword, AssessmentStatus? status,
            int? officeId, byte? month, int year, int page, int pageSize, out int totalRows);

        Task<UserAssessmentViewModel> GetDetail(string tenantId, string languageId, string id);

        Task<UserAssessmentViewModel> GetDetail(string tenantId, string languageId, string id, string userId);

        Task<UserAssessmentViewModel> GetDetail(string tenantId, string languageId, string userId, byte month, int year);

        Task<UserAssessment> GetUnFinishedAssessment(string tenantId, string userId, bool isReadOnly = false);

        Task<int> UpdateManager(UserAssessment userAssessment);

        Task<string> GetUserAssessmentId(string tenantId, string userId, byte month, int year);

        Task<List<UserAssessment>> SearchForReassessment(string tenantId, string positionId, byte month, int year, bool applyForAll,
            int page, int pageSize, out int totalRows);
    }
}
