﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;

namespace GHM.Hr.Domain.IRepository
{
    public interface ICriteriaRepository
    {
        Task<int> Insert(Criteria criteria);

        Task<int> Update(Criteria criteria);

        Task<int> Delete(string tenantId, string id);

        Task<Criteria> GetInfo(string tenantId, string id, bool isReadOnly = false);

        Task<bool> CheckExistsByGroupId(string tenantId, string groupId);

        Task<List<CriteriaViewModel>> Search(string tenantId, string languageId, string keyword, string groupId,
            bool? isActive, int page, int pageSize, out int totalRows);        
    }
}
