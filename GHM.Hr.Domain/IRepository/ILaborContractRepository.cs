﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;

namespace GHM.Hr.Domain.IRepository
{
    public interface ILaborContractRepository
    {
        Task<int> Insert(UserLaborContract laborContract);

        Task<int> Update(UserLaborContract laborContract);

        Task<int> UpdateList(List<UserLaborContract> laborContracts);

        Task<int> Delete(string tenantId, string id);

        Task<bool> CheckExists(string tenantId, string type, string laborContractFormId, string userId);

        Task<bool> CheckNoExists(string tenatnId, string id, string no);

        Task<UserLaborContract> GetInfo(string tenantId, string id, bool isReadOnly = false);

        Task<LaborContractStatisticsViewModel> GetStatisticsResult();

        Task<List<UserLaborContract>> GetListUserLaborContract(string tenantId, string userId, string id, bool isReadOnly = false);

        Task<int> CountLaborContractByForm(string tenantId, string laborContractFormId);

        Task<List<UserLaborContractViewModel>> Search(string tenantId, string languageId, string keyword, string userId,
             int? officeId, string titleId, string positionId, string type,
            DateTime? fromDate, DateTime? toDate, bool? isUse, byte? month, int page, int pageSize, out int totalRows);        

        Task<List<UserLaborContractViewModel>> SearchExpires(string tenantId, string languageId, string keyword, string userId, bool isNext, string type,
            DateTime? fromDate, DateTime? toDate, int page, int pageSize, out int totalRows);

        Task<List<UserLaborContractExportViewModel>> GetListLaborContractExport(string tenantId, string languageId, string officeIdPath);

        Task<int> CountUserLaborContactByUserId(string tenantId, string userId);

        Task<int> TotalLaborExpriedInMonth(string tenantId);

        Task<int> TotalUserHaveNotLaborContract(string tenantId);

        Task<bool> CheckUserHaveLaborContractActive(string tenantId, string userId);
    }
}
