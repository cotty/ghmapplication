﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;

namespace GHM.Hr.Domain.IRepository
{
    public interface IAcademicLevelRepository
    {
        Task<int> Insert(AcademicLevel academicLevel);

        Task<int> Update(AcademicLevel academicLevel);

        Task<int> Delete(string tenantId, string id);

        Task<AcademicLevel> GetInfo(string tennantId, string id, bool isReadOnly = false);

        Task<List<AcademicLevelViewModel>> Search(string tenantId, string userId, string levelId, string degreeId, string schoolId,
            string specializeId, int page, int pageSize, out int totalRows);

        Task<bool> CheckIsExists(string tenantId, string userId, string levelId, string degreeId, string schoolId, string specializeId);
    }
}
