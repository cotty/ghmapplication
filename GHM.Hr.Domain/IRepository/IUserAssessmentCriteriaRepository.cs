﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;

namespace GHM.Hr.Domain.IRepository
{
    public interface IUserAssessmentCriteriaRepository
    {
        Task<int> Insert(List<UserAssessmentCriteria> listUserassessmentCriteria);

        Task<int> UserUpdatePoint(UserAssessmentCriteria userAssessmentCriteria);

        Task<int> ManagerUpdatePoint(UserAssessmentCriteria userAssessmentCriteria);

        Task<int> ApproverUpdatePoint(UserAssessmentCriteria userAssessmentCriteria);

        Task<int> UpdateNote(string userAssessmentId, string criteriaId, string note);

        Task<UserAssessmentCriteria> GetInfo(string id, bool isReadonly = false);

        Task<List<UserAssessmentCriteriaViewModel>> GetListAssessmentCriteria(string userAssessmentId,
            string languageId);

        Task<decimal> GetTotalUserPoint(string userAssessmentId);

        Task<decimal> GetTotalManagerPoint(string userAssessmentId);

        Task<decimal> GetTotalApproverPoint(string userAssessmentId);

        Task<List<UserAssessmentCriteria>> GetListAssessmentCriteria(string userAssessmentId);

        Task<int> Updates(List<UserAssessmentCriteria> userAssessmentCriterias);

        Task<bool> CheckExistsByCriteriaId(string criteriaId);
    }
}
