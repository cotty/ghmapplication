﻿using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Hr.Domain.IRepository
{
    public interface IUserContactRepository
    {
        Task<int> Insert(UserContact userContact);
        Task<int> Inserts(List<UserContact> userContacts);
        Task<int> Update(UserContact userContact);
        Task<bool> CheckExistsByValue(string tenantId, string id, string userId, ContactType contactType, string contactValue);
        Task<List<UserContactViewModel>> GetByUserId(string tenantId, string userId);
        Task<int> Delete(string tenantId, string id);
        Task<int> DeleteByUserdId(string tenantId, string userId);
        Task<UserContact> GetInfo(string tenantId, string id, bool isReadonly = false);
        Task<UserContact> GetInfoByValue(string tenantId, string id, string userId, ContactType contactType, string contactValue, bool isReadOnly = false);
    }
}
