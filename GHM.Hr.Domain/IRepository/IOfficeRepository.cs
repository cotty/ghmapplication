﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;

namespace GHM.Hr.Domain.IRepository
{
    public interface IOfficeRepository
    {
        Task<int> Insert(Office office);

        Task<int> Update(Office office);

        Task<int> UpdateOfficeIdPath(int id, string idPath);

        Task<int> UpdateOfficeRoot(int id, int rootId, string rootIdPath);

        Task<int> UpdateChildCount(int id, int childCount);

        Task<int> UpdateChildrenIdPath(string oldIdPath, string newIdPath);

        Task<int> SaveChangeAsync();

        Task<int> Delete(int officeId);

        Task<int> ForceDelete(int officeId);

        Task<Office> GetInfo(int officeId, bool isReadOnly = false);

        Task<Office> GetInfo(string officeIdPath, bool isReadOnly = false);

        Task<OfficeDetailViewModel> GetInfoByLanguage(string tenantId, int officeId, string languageId);

        Task<OfficeDetailViewModel> GetInfoByLanguage(int officeId, string languageId);

        Task<int> GetChildCount(int id);

        Task<bool> CheckExistsByCode(int officeId, string code);

        Task<bool> CheckOfficeExistsByTenantId(int officeId, string tenantId);

        Task<List<OfficeSuggestionViewModel>> SearchForSuggestion(string tenantId, string languageId, string keyword,
            int page, int pageSize, out int totalRows);

        Task<List<OfficeSearchViewModel>> GetAllActivatedOffice(string tenantId, string languageId);

        Task<List<OfficeSearchViewModel>> GetActivedOfficeByRootId(string tenantId, string languageId, int rootId);

        Task<List<OfficeSearchViewModel>> SearchOfficeByRootId(string tenantId, string languageId, int rootId, string keyword, bool? isActive, int page, int pageSize, out int totalRows);

        Task<List<OfficeSearchViewModel>> GetOfficeUserTree(string tenantId, string languageId, string officeIdPath);

        Task<List<OfficeUserTreeViewModel>> GetOfficeUserTree(int? parentId, string languageId);

        Task<List<OfficeSearchViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive, int page, int pageSize, out int totalRows);

        Task<Office> GetActiveInfo(int officeId, bool isReadOnly = false);

        Task<OfficeShortInfoViewModel> GetSortInfo(string tenantId, string languageId, int officeId);
    }
}
