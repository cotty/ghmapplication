﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;

namespace GHM.Hr.Domain.IRepository
{
    public interface ITrainingHistoryRepository
    {
        Task<int> Insert(UserTrainingHistory training);

        Task<int> Update(UserTrainingHistory training);

        Task<int> Delete(string tenantId, string id);

        Task<UserTrainingHistory> GetInfo(string tenantId, string id, bool isReadOnly = false);

        Task<bool> CheckExists(string tenantId, string userId, DateTime fromDate, DateTime toDate, string courseId);

        Task<bool> CheckExist(string tenantId, string id);

        Task<List<UserTrainingHistory>> Search(string tenantId, string keyword, string userId, bool? type, string courseId,string courseName, string coursePlaceId, string coursePlaceName, 
            bool? isHasCertificate, DateTime? fromDate, DateTime? toDate, int page, int pageSize,out int totalRows);

        Task<bool> CheckExist(string tenantId, string userId, string courseId, string courseName, string coursePlaceId, string coursePlaceName);
    }
}
