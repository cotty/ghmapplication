﻿using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Hr.Domain.IRepository
{
    public interface IRelatedUserRepository
    {
        Task<RelatedUser> GetInfo(string tenantId, string id, bool isReadOnly = false);

        Task<int> SaveAsync(RelatedUser info);

        Task<bool> CheckNameExist(string tenantId, string userId, string fullName);

        Task<int> Insert(RelatedUser relatedUser);

        Task<List<RelatedUserViewModel>> Search(string tenantId, string keyword, string userId,string valueId, string fullName, string idCardNumber, string phone, int page, int pageSize, out int totalRows);
    }
}
