﻿using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Hr.Domain.IRepository
{
    public interface ICommendationDisciplineCategoryRepository
    {
        Task<UserCommendationDisciplineCategory> GetInfoAsync(string tenantId, string id, bool isReadOnly);

        Task<int> SaveAsync(UserCommendationDisciplineCategory info);

        Task<bool> CheckNameExist(string tenantId, string name, CommendationDisciplineType type);

        Task<int> InsertAsync(UserCommendationDisciplineCategory commendationDisciplineCategory);

        Task<List<CommendationDisciplineCategoryViewModel>> Search(string tenantId, string keyword, string name, CommendationDisciplineType? type, int page, int pageSize, out int TotalRows);

        Task<bool> CheckExist(string tenantId, int categoryId);
    }
}
