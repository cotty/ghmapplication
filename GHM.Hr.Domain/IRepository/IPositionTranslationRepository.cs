﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;

namespace GHM.Hr.Domain.IRepository
{
    public interface IPositionTranslationRepository
    {
        Task<int> Save(PositionTranslation positionTranslation);

        Task<int> Insert(PositionTranslation positionTranslation);

        Task<int> Inserts(List<PositionTranslation> positionTranslations);

        Task<int> Delete(string tenantId, string positionId, string languageId);

        Task<int> Update(PositionTranslation positionTranslation);

        Task<int> DeleteByPositionId(string tenantId, string positionId);

        Task<PositionTranslation> GetInfo(string tenantId, string positionId, string languageId, bool isReadOnly = false);

        Task<PositionTranslation> GetInfo(string positionId, string languageId, bool isReadOnly = false);

        Task<List<PositionTranslationViewModel>> GetsByPositionId(string tenantId, string positionId);

        Task<bool> CheckExists(string positionId, string tenantId, string languageId, string name);

        Task<bool> CheckShortNameExists(string positionId, string tenantId, string languageId, string shortName);
    }
}
