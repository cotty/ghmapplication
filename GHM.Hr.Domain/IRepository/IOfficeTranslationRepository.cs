﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;

namespace GHM.Hr.Domain.IRepository
{
    public interface IOfficeTranslationRepository
    {
        Task<int> Insert(OfficeTranslation officeTranslation);

        Task<int> Update(OfficeTranslation officeTranslation);

        Task<int> Inserts(List<OfficeTranslation> officeTranslations);

        Task<int> ForceDeleteByOfficeId(int officeId);

        Task<int> SaveChangeAsync();

        Task<bool> CheckExistsByShortName(int rootId, int officeId, string languageId, string shortName);

        Task<bool> CheckExistsByName( int rootId, int officeId, string languageId, string name);

        Task<OfficeTranslation> GetInfo(int officeId, string languageId, bool isReadonly = false);

        Task<List<OfficeTranslationViewModel>> GetsByOfficeId(int officeId);
    }
}
