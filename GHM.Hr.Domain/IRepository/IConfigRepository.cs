﻿using System.Threading.Tasks;
using GHM.Hr.Domain.Models;

namespace GHM.Hr.Domain.IRepository
{
    public interface IConfigRepository
    {
        Task<Config> GetInfo(string tenantId, string languageId, string key, bool isReadOnly = false);

        Task<int> Insert(Config config);

        Task<int> Update(Config config);
    }
}
