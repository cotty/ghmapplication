﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Hr.Api.Infrastructure.Models.ViewModels;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Domain.IRepository
{
    public interface IUserRepository
    {
        Task<User> GetInfo(string id, bool isReadonly = false);

        Task<User> GetInfo(string tenantId, string id, bool isReadonly = false);

        Task<User> GetInfo(string tenantId, int enrollNumber, bool isReadonly = false);

        Task<User> GetInfoByUserName(string tenantId, string userId, string userName, bool isReadOnly = false);

        Task<User> GetInfoByIdCardNumber(string tenantId, string userId, string idCardNumber, bool isReadOnly = false);

        int GetMaxEnrollNumbeValue(string tenantId);

        Task<User> GetInfoByUserName(string userName, bool isReadonly = false);

        Task<int> Insert(User user);

        Task<int> Update(User user);

        Task<int> UpdateAvatar(string tenantId, string userId, string avatar);

        Task<int> UpdateStatus(string tenantId, string id, UserStatus status);

        Task<int> UpdateDirectManager(string tenantId, string id, string managerId);

        Task<int> UpdateApproveManager(string tenantId, string id, string approverId);

        Task<int> UpdateDirectManagerAndApproveManager(List<string> userIds, string managerId, string approveId);

        Task<List<UserSearchViewModel>> Search(string tenantId, string languageId, string keyword, UserStatus? status, string officeIdPath,
            string positionId, Gender? gender, byte? monthBirthDay, int? yearBirthday, AcademicRank? academicRank,
            int page, int pageSize, out int totalRows);

        Task<List<UserSuggestionViewModel>> GetUserForSuggestion(string tenantId, string languageId, string keyword,
            int? officeId, int page, int pageSize, out int totalRows);

        Task<List<ExportUserViewModel>> GetListExportUser(string tenantId, string languageId, string keyword, UserStatus? status, string officeIdPath,
            string positionId, Gender? gender, byte? monthBirthDay, int? yearBirthday, AcademicRank? academicRank);

        Task<List<T>> Search<T>(Expression<Func<User, T>> projector, string keyword, bool? isAdmin, bool? isActive, string status, string officeIdPath,
            int page, int pageSize, out int totalRows);

        Task<List<T>> Search<T>(Expression<Func<User, T>> projector, string name, bool? isAdmin,
            int page, int pageSize, out int totalRows);

        Task<List<T>> SearchUserForSuggestion<T>(Expression<Func<User, T>> projector, string name, string officeIdPath, int rootId,
            int page, int pageSize, out int totalRows);

        Task<List<T>> SearchUserForSuggestion<T>(Expression<Func<User, T>> projector, string name, int rootId);

        Task<List<UserSearchForManagerConfigViewModel>> SearchForConfigManager(string tenantId, string languageId, string keyword, int officeId, byte? type,
            bool? isGetStaffFromChildOffice, int page, int pageSize, out int totalRows);

        User GetByUserName(string tenantId, string userName);

        Task<User> GetByUserNameAsync(string tenantId,  string userName);

        Task<int> CreateUserAdmin(string id, string fullName, string password, bool isSuperAdmin, bool isActive, List<int> permissions, List<int> categories);

        Task<int> UpdateUserAdmin(string userId, string email, string fullName, bool isSuperAdmin, bool isActive, List<int> permissions, List<int> categories);

        Task<int> Delete(string tenantId, string userId);

        Task<int> ForceDelete(string tenantId, string userId);

        Task<int> ActiveUser(string tenantId, string userId, bool isActive);

        Task<int> Count(Expression<Func<User, bool>> spec);

        int InsertUserConnection(string userId, string connectionId);

        int DeleteUserConnection(string connectionId);

        Task<int> DeleteUserConnectionByUserId(string userId);

        Task<bool> CheckIdExists(string tenantId, string userId);

        Task<bool> CheckUserNameExists(string userId, string userName);

        Task<bool> CheckIdCardNumberExists(string tenantId, string id, string idCardNumber);

        Task<bool> CheckIsManagerOrApprover(string tenantId, string userId, string managerId);

        Task<List<User>> GetUserByListIds(List<string> userIds);       

        Task<int> UpdateOfficeIdPathByOfficeId(string tenantId, int officeId, string officeIdPath);

        Task<int> UpdateOfficeNameAndOfficeIdPathByListOfficeId(List<int> listOfficeId);

        Task<List<User>> GetListMyEmployee(string tenantId, string managerUserId);

        Task<bool> CheckHasPermissionByLeaderOfficeIdPath(string tenantId, string userId, string officeIdPath);

        Task<List<ShortUserInfoViewModel>> GetListUserByOfficeIdPath(string tenantId, string officeIdPath, string languageId, int page, int pageSize, out int totalRows);

        Task<List<ShortUserInfoViewModel>> GetListUserByOfficeId(string tenantId, int officeId, string languageId, int page, int pageSize, out int totalRows);

        Task<string> GetCardNumberByUserId(string tenantId, string userId);

        Task<bool> CheckOfficeIdUser(string tenantId, int officeId);

        Task<ShortUserInfoViewModel> GetUserInfo(string tenantId, string userId, string languageId);

        Task<List<ShortUserInfoViewModel>> GetShortUserInfoByListUserId(string tenantId, List<string> userIds, string languageId);

        Task<ShortUserInfoViewModel> GetShorUserInfoOfLeaderOffice(string tenantId, int officeId, string languageId);      

        Task<int> GetTotalUserByPosition(string tenantId, string positionId);

        #region 
        #endregion
    }
}
