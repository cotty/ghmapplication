﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;

namespace GHM.Hr.Domain.IRepository
{
    public interface ICriteriaLibraryRepository
    {    
        #region Group
        Task<List<CriteriaGroup>> GetAllGroup();
        #endregion

        Task<List<Criteria>> Search(string keyword, string groupId, bool? isActive, int page, int pageSize, out int totalRows);

        Task<int> Insert(CriteriaMeta criteriaMeta);

        Task<int> Update(CriteriaMeta criteriaMeta);

        Task<int> UpdateIsActive(string id, bool isActive);

        Task<int> Delete(string id);

        Task<Criteria> GetInfo(string id, bool isReadOnly = false);

        Task<bool> CheckNameExists(string id, string name);

        Task<bool> CheckGroupExits(string id);

        Task<CriteriaGroup> GetGroupInfo(string id, bool? isActive, bool isReadOnly = false);
    }
}
