﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;

namespace GHM.Hr.Domain.IRepository
{
    public interface ICommendationDisciplineRepository
    {
        Task<int> Insert(CommendationDisciplineMeta commendationDisciplineMeta);

        Task<int> Update(CommendationDisciplineMeta commendationDisciplineMeta);

        Task<int> Delete(UserCommendationDiscipline info);

        Task<bool> CheckExistsByDecisionNo(string id, string userId, string decisionNo);

        Task<UserCommendationDiscipline> GetInfo(string tenantId, string id, bool isReadonly = false);

        Task<List<UserCommendationDiscipline>> Search(string keyword, string userId, bool? type, string categoryId, DateTime? fromDate,
            DateTime? toDate, int? officeId, string titleId, int page, int pageSize, out int totalRows);

        #region Category
        Task<List<UserCommendationDisciplineCategory>> GetAllCategoryByType(CommendationDisciplineType? type);

        Task<UserCommendationDisciplineCategory> GetCategoryInfo(string id);
        #endregion
    }
}
