﻿using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Infrastructure.Models;

namespace GHM.Hr.Domain.IRepository
{
    public interface ICriteriaGroupRepository
    {
        Task<List<CriteriaGroupViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive, int page,
            int pageSize, out int totalRows);

        Task<int> Insert(CriteriaGroup criteriaGroup);

        Task<int> Update(CriteriaGroup criteriaGroup);

        Task<int> Delete(string tenantI, string id);

        Task<bool> CheckIdExists(string tenantId, string id);

        Task<CriteriaGroup> GetInfo(string tenantId, string id, bool isReadOnly = false);

        Task<List<Suggestion<string>>> Suggestion(string tenantId, string languageId);        
    }
}
