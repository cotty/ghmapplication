﻿using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Hr.Domain.IRepository
{
    public interface ICriteriaGroupTranslationRepository
    {
        Task<int> Insert(CriteriaGroupTranslation criteriaGroupTranslation);

        Task<int> Inserts(List<CriteriaGroupTranslation> criteriaGroupTranslations);

        Task<int> Update(CriteriaGroupTranslation criteriaGroupTranslation);

        Task<int> Delete(string tenantId, string criteriaGroupId, string languageId);

        Task<bool> CheckExists(string tenantId, string id, string languageId, string name);

        //Task<int> DeleteByCriteriaGroupTranslation(string criteriaGroupId);
        //Task<List<CriteriaGroupTranslationViewModel>> SearchByCriteriaGroupId(string criteriaGroupId);

        Task<CriteriaGroupTranslation> GetInfo(string tenantId, string criteriaGroupId, string languageId, bool isReadOnly = false);

        Task<List<CriteriaGroupTranslation>> GetsAllByCriteriaGroupId(string tenantId, string criteriaGroupId, bool isReadOnly = false);

        Task<bool> CheckExists(string criteriaGroupId, string languageId, string name);

        Task<int> Updates(List<CriteriaGroupTranslation> criteriaGroupTranslations);

        Task<int> DeleteByCriteriaGroupId(string tenantId, string criteriaGroupId);

        Task<bool> CheckIdExists(string tenantId, string id, string name);
    }
}
