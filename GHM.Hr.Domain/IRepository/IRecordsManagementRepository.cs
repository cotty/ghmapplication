﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;

namespace GHM.Hr.Domain.IRepository
{
    public interface IRecordsManagementRepository
    {
        Task<int> Save(List<UserRecordsManagement> listRecords);

        Task<int> Update(UserRecordsManagement records);

        Task<UserRecordsManagement> GetInfo(string tenantId, string id, bool isReadOnly = false);

        Task<List<UserRecordsManagementViewModel>> GetListRecordsByUserId(string tenantId, string userId);

        Task<List<ExportUserRecordManagermentViewModel>> GetListRecordExport(string tenantId, string officeIdPath);

        Task<bool> CheckExist(string tenantId, string id);

        Task<int> SaveAsync(UserRecordsManagement info);

        Task<bool> CheckExist(string tenantId, string title, string userId, string valueId);

        Task<List<UserRecordsManagementViewModel>> Search(string tenantId, string userId, string valueId, string title, DateTime? fromDate, DateTime? toDate, int page, int pageSize, out int totalRows);

        Task<int> Insert(UserRecordsManagement records);
    }
}
