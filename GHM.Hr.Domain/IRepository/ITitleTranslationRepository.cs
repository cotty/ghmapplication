﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;

namespace GHM.Hr.Domain.IRepository
{
    public interface ITitleTranslationRepository
    {
        Task<int> Insert(TitleTranslation titleTranslation);

        Task<int> Update(TitleTranslation titleTranslation);

        Task<int> Inserts(List<TitleTranslation> titleTranslations);

        Task<int> Delete(string tenantId, string titleId, string languageId);

        Task<int> ForceDeleteByTitleId(string tenantId, string titleId);

        Task<int> DeleteByTitleId(string tenantId, string titleId);

        Task<TitleTranslation> GetInfo(string tenantId, string titleId, string languageId, bool isReadOnly = false);

        Task<List<TitleTranslation>> GetsByTitleId(string tenantId, string titleId);

        Task<bool> CheckExists(string titleId, string tenantId, string languageId, string name);

        Task<bool> CheckShortNameExists(string titleId, string tenantId, string languageId, string shortName);
    }
}
