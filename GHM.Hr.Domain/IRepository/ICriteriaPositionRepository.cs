﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;

namespace GHM.Hr.Domain.IRepository
{
    public interface ICriteriaPositionRepository
    {
        Task<List<CriteriaPositionViewModel>> Search(string tenantId, string languageId, string positionId, bool? isActive);

        Task<int> Insert(CriteriaPosition criteriaPosition);

        Task<int> UpdateIsActive(string criteriaId, string positionId, bool isActive);

        Task<int> Delete(string tenantId, string criteriaId, string positionId);

        Task<CriteriaPosition> GetInfo(string tenantId, string criteriaId, string positionId, bool isReadOnly = false);

        Task<bool> CheckExists(string tenantId, string criteriaId, string positionId);

        Task<List<CriteriaPosition>> GetAllCriteriaByPositionId(string positionId);

        Task<int> UpdateNameByCriteriaId(string criteriaId, string name);

        Task<int> UpdateActiveStatus(string tenantId, string criteriaId, string positionId, bool isActive);

        Task<List<CriteriaPositionViewModel>> GetsByPositionId(string tenantId, string positionId, bool isReadOnly = false);
    }
}
