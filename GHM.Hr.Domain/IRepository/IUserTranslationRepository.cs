﻿using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Hr.Domain.IRepository
{
    public interface IUserTranslationRepository
    {
        Task<int> Save(UserTranslation userTranslation);
        Task<int> Insert(UserTranslation userTranslation);
        Task<int> Inserts(List<UserTranslation> userTranslation);
        Task<int> Delete(string userId, string languageId);
        Task<int> DeleteByUserId(string userId);
        Task<UserTranslation> GetInfo(string userId, string languageId, bool isReadOnly = false);
        Task<List<UserTranslationViewModel>> GetByUserId(string userId);
        Task<int> UpdatePositionNameByPositionId(string tenantId, string positionId, string positionName, string languageId);
        Task<int> UpdateOfficeNameByOfficeId(string tenantId, int officeId, string officeName, string languageId);
    }
}
