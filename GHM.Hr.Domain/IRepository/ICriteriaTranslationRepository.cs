﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;

namespace GHM.Hr.Domain.IRepository
{
    public interface ICriteriaTransaltionRepository
    {
        Task<bool> CheckExists(string tenantId, string criteriaId, string languageId, string name);

        Task<CriteriaTranslation> GetInfo(string tenantId, string criteriaId, string languageId, bool isReadOnly = false);

        Task<int> Insert(CriteriaTranslation criteriaTranslation);

        Task<int> Update(CriteriaTranslation criteriaTranslation);

        Task<int> DeleteByCriteriaId(string tenantId, string id);

        Task<List<CriteriaTranslation>> GetsByCriteriaId(string tenantId, string criteriaId);
    }
}
