﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;

namespace GHM.Hr.Domain.IRepository
{
    public interface IOfficePositionRepository
    {
        Task<bool> CheckExistsByTitleId(string titleId);

        Task<bool> CheckExistsByPositionId(string positionId);

        Task<bool> CheckExistsByOfficeId(int officeId);

        Task<bool> CheckExists(string positionId, int officeId);

        Task<int> Insert(OfficePosition officePosition);

        Task<int> Inserts(List<OfficePosition> officePositions);

        Task<int> UpdateUserNo(string titleId, int officeId, int userNo);

        Task<int> Delete(string positionId, int officeId);

        Task<int> DeleteByTitleId(string titleId);

        Task<int> DeleteByOfficeId(int officeId);

        Task<OfficePosition> GetInfo(string titleId, int officeId, bool isReadOnly = false);

        Task<OfficePosition> GetInfoByPositionIdAndOfficeId(string positionId, int officeId, bool isReadOnly = false);

        Task<List<OfficePosition>> GetsByPositionId(string positionId);

        Task<List<OfficePositionDetailViewModel>> GetsOfficeInfoByPositionId(string languageId, string positionId);

        Task<List<OfficePositionTreeViewModel>> GetAllOfficePositionForRenderTree(string tenantId, string languageId);

        Task<List<OfficePositionSearchViewModel>> SearchByPositionId(string tenantId, string languageId, string positionId,
            int page, int pageSize, out int totalRows);

        Task<List<OfficePositionSearchViewModel>> SearchByOfficeId(string tenantId, string languageId,
            string keyword, int officeId, int page, int pageSize, out int totalRows);
    }
}
