﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;

namespace GHM.Hr.Domain.IRepository
{
    public interface ITitleRepository
    {
        Task<List<TitleViewModel>> Search(string tenantId, string languageId, string keyword,
            bool? isActive, int page, int pageSize, out int totalRows);

        Task<List<TitleSearchForSelectViewModel>> SearchForSelect(string tenantId, string languageId, string keyword);

        Task<int> Insert(Title title);

        Task<int> Update();

        Task<int> Delete(string tenantId, string titleId);

        Task<int> ForceDelete(string titleId);

        Task<Title> GetInfo(string tenantId, string id, bool isReadonly = false);
    }
}
