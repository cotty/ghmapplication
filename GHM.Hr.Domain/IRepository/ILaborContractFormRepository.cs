﻿using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Hr.Domain.IRepository
{
    public interface ILaborContractFormRepository
    {
        Task<int> Insert(LaborContractForm laborContractForm);

        Task<int> Update(LaborContractForm laborContractForm);

        Task<int> Delete(string tenantId, string id);

        Task<LaborContractForm> GetInfo(string tenantId, string id, bool isReadOnly = false);

        Task<bool> CheckExists(string tenantId, string id, string laborContractTypeId, string unsignName);

        Task<List<LaborContractFormViewModel>> Search(string tenantId, string keyword, string type,
            int page, int pageSize, out int totalRows);

        Task<List<Suggestion<string>>> GetLaborContractFormByType(string tenantId, string keyword, string type,
            int page, int pageSize, out int totalRows);
    }
}
