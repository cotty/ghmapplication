﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;

namespace GHM.Hr.Domain.IRepository
{
    public interface IUserAssessmentCriteriaCommentRepository
    {
        Task<int> Insert(UserAssessmentCriteriaComment userAssessmentCriteriaComment);

        Task<int> Update(UserAssessmentCriteriaComment userAssessmentCriteriaComment);

        Task<int> Delete(string id);

        Task<UserAssessmentCriteriaComment> GetInfo(string id, bool isReadOnly = false);

        Task<List<UserAssessmentCriteriaComment>> Search(string userAssessmentCriteriaId, int page, int pageSize, out int totalRows);
    }
}
