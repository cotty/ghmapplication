﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;

namespace GHM.Hr.Domain.IRepository
{
    public interface IUserPositionRepository
    {
        Task<int> Insert(UserPositioncs userPosition);

        Task<int> Update(UserPositioncs userPosition);

        Task<int> Update(UserPositioncs userPosition, int oldTitleId, int oldOfficeId);

        Task<int> UpdateDefaultState(string userId, bool isDefault);

        Task<int> UpdateOfficeIdPath(string oldOfficeIdPath, string officeIdPath);

        //Task<int> UpdateOfficeIdPathByListOfficeId(List<int> listOfficeId);

        Task<int> Delete(string userId, string positionId, int officeId);

        Task<int> DeleteByUserId(string tenantId, string userId);

        Task<bool> CheckExistsByPositionId(string positionId);

        Task<bool> CheckExistsByOfficeId(int officeId);

        Task<bool> CheckExistsByOfficeIdAndPositionId(int officeId, string positionId);

        Task<bool> CheckExists(string userId, string positionId, int officeId, bool? isDefault = false);

        Task<UserPositioncs> GetInfo(string userId, string positionId, int officeId, bool isReadonly = false);

        Task<bool> CheckExitsByPositionId(string positionId);

        Task<UserPositioncs> GetDefaultInfo(string userId, bool isDefault, bool isReadonly = false);

        Task<UserPositioncs> GetDefaultInfo(string userId, string positionId, int officeId, bool isDefault, bool isReadonly = false);
    }
}
