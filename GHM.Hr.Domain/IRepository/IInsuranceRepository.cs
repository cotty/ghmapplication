﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;

namespace GHM.Hr.Domain.IRepository
{
    public interface IInsuranceRepository
    {
        Task<int> Insert(UserInsurance insuranceMeta);

        Task<int> Update(UserInsurance insuranceMeta);

        Task<int> Delete(string tenantId, string id);

        Task<UserInsurance> GetInfo(string tenantId, string id, bool isReadOnly = false);

        Task<bool> CheckExists(string tenantId, string id, string userId, DateTime fromDate, DateTime? toDate);

        Task<List<UserInsurance>> Search(string tenantId, string keyword, string userId, bool? type,
            DateTime? fromDate, DateTime? toDate, int page, int pageSize, out int totalRows);
    }
}

