﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;

namespace GHM.Hr.Domain.IRepository
{
    public interface IOfficeContactRepository
    {
        Task<int> Insert(OfficeContact officeContact);

        Task<int> Inserts(List<OfficeContact> officeContacts);

        Task<int> Update(OfficeContact officeContact);

        Task<int> Delete(string id);

        Task<int> Delete(int officeId, string userId);

        Task<int> DeleteByOfficeId(int officeId);

        Task<List<OfficeContactViewModel>> GetsByOfficeId(int officeId, string languageId);

        Task<OfficeContact> GetInfo(string id, bool isReadOnly = false);

        Task<OfficeContact> GetInfo(int officeId, string userId, bool isReadOnly = false);

        Task<bool> CheckPhoneNumberExists(int officeId, string userId, string phoneNumber);

        Task<bool> CheckEmailExists(int officeId, string userId, string email);

        Task<bool> CheckUserExists(int officeId, string userId);
    }
}
