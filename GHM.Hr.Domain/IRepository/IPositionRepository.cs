﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;

namespace GHM.Hr.Domain.IRepository
{
    public interface IPositionRepository
    {
        Task<List<PositionViewModel>> Search(string tenantId, string languageId, string keyword,
            bool? isActive, bool? isManager, bool? isMultiple, int page, int pageSize, out int totalRows);

        Task<int> Insert(Position position);

        Task<int> Update(Position position);

        Task<int> Delete(string id, string tenantId);

        Task<int> ForceDelete(string id);

        Task<Position> GetInfo(string id, bool isReadOnly = false);

        Task<Position> GetInfo(string id, string tenantId, bool isReadOnly = false);

        Task<bool> CheckExistsByTitleId(string tenantId, string titleId);     

        Task<List<PositionSearchForSelectViewModel>> SearchForSelect(string tenantId, string languageId, string keyword);

    }
}
