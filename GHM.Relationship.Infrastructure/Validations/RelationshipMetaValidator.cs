﻿using FluentValidation;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using GHM.Relationship.Domain.ModelMetas;
using GHM.Relationship.Domain.Resources;

namespace GHM.Relationship.Infrastructure.Validations
{
    public class RelationshipMetaValidator : AbstractValidator<RelationshipMeta>
    {
        public RelationshipMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmRelationshipResource> resourceService)
        {
            RuleFor(x => x.UserId)
                .NotNull().WithMessage(sharedResourceService.GetString("Please select {0}.",
                    resourceService.GetString("user")))
                .MaximumLength(50).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    resourceService.GetString("user id"), 50));

            RuleFor(x => x.RelationalUserId)
                .NotNull().WithMessage(sharedResourceService.GetString("Please select {0}.",
                    resourceService.GetString("relational user")))
                .MaximumLength(50).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    resourceService.GetString("relational user id"), 50));

            RuleFor(x => x.RelationshipTypeId)
                .NotNull().WithMessage(sharedResourceService.GetString("Please select {0}.",
                    resourceService.GetString("relationship type")));

            RuleFor(x => x.Description)                
                .MaximumLength(500).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    resourceService.GetString("description"), 500));
        }
    }
}
