﻿using FluentValidation;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using GHM.Relationship.Domain.ModelMetas;
using GHM.Relationship.Domain.Resources;

namespace GHM.Relationship.Infrastructure.Validations
{
    public class RelationshipTypeMetaValidator : AbstractValidator<RelationshipTypeMeta>
    {
        public RelationshipTypeMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmRelationshipResource> resourceService)
        {
            RuleFor(x => x.Kind)
                .NotNull()
                .WithMessage(sharedResourceService.GetString("Please select {0}.",
                    resourceService.GetString("kind of relationship type")));

            RuleFor(x => x.RelationshipTypeTranslations)
                .Must(x => x.Count > 0)
                .WithMessage(sharedResourceService.GetString("Please select at least one language."));

            RuleForEach(x => x.RelationshipTypeTranslations).SetValidator(new RelationshipTypeTranslationMetaValidator(sharedResourceService, resourceService));
        }
    }
    public class RelationshipTypeTranslationMetaValidator : AbstractValidator<RelationshipTypeTranslationMeta>
    {
        public RelationshipTypeTranslationMetaValidator(IResourceService<SharedResource> sharedResourceService, 
            IResourceService<GhmRelationshipResource> resourceService)
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("please enter {0}.",
                    resourceService.GetString("relationship type name")))
                .MaximumLength(256).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    resourceService.GetString("relationship type name"), 256));
           
            RuleFor(x => x.Description)
                .MaximumLength(500).WithMessage(sharedResourceService.GetString(
                    "{0} is not allowed over {1} characters.",
                    sharedResourceService.GetString("Description"), 500));

            RuleFor(x => x.LanguageId)
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                    sharedResourceService.GetString("Language code")))
                .MaximumLength(10).WithMessage(sharedResourceService.GetString(
                    "{0} is not allowed over {1} characters.",
                    sharedResourceService.GetString("Language code"), 10));           
        }
    }
}
