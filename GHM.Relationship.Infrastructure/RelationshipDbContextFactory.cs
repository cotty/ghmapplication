﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace GHM.Relationship.Infrastructure
{
public    class RelationshipDbContextFactory : IDesignTimeDbContextFactory<RelationshipDbContext>
    {
        public RelationshipDbContext CreateDbContext(string[] args)
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);

            var configs = configurationBuilder.Build();
            var connectionString = configs.GetConnectionString("RelationshipConnectionString");

            var optionsBuilder = new DbContextOptionsBuilder<RelationshipDbContext>();
            optionsBuilder.UseSqlServer(connectionString);
            return new RelationshipDbContext(optionsBuilder.Options);
        }
    }
}
