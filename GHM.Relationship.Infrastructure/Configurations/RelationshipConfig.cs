﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Relationship.Infrastructure.Configurations
{
    public class RelationshipConfig : IEntityTypeConfiguration<Domain.Models.Relationship>
    {
        public void Configure(EntityTypeBuilder<Domain.Models.Relationship> builder)
        {
            builder.Property(x => x.UserId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.TenantId).IsUnicode(false).IsRequired().HasMaxLength(50);
            builder.Property(x => x.RelationalUserId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.RelationshipTypeId).IsRequired();
            builder.Property(x => x.UserFullName).IsRequired().HasMaxLength(50);
            builder.Property(x => x.RelationalUserFullName).IsRequired().HasMaxLength(50);
            builder.Property(x => x.UnsignName).IsRequired().HasMaxLength(500);
            builder.Property(x => x.Description).IsRequired(false).HasMaxLength(500);
            builder.Property(x => x.ConcurrencyStamp).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.ToTable("Relationships").HasKey(x => new { x.UserId, x.RelationalUserId });
        }
    }
}