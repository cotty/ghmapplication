﻿using GHM.Relationship.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Relationship.Infrastructure.Configurations
{
    public class RelationshipTypeTranslationConfig : IEntityTypeConfiguration<RelationshipTypeTranslation>
    {
        public void Configure(EntityTypeBuilder<RelationshipTypeTranslation> builder)
        {
            builder.Property(x => x.RelationshipTypeId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.LanguageId).IsRequired().HasMaxLength(10).IsUnicode(false);
            builder.Property(x => x.Name).IsRequired().HasMaxLength(256);
            builder.Property(x => x.UnsignName).IsRequired().HasMaxLength(256).IsUnicode(false);
            builder.Property(x => x.Description).IsRequired(false).HasMaxLength(500);
            builder.ToTable("RelationshipTypeTranslations").HasKey(x => new { x.TenantId, x.LanguageId, x.RelationshipTypeId });
        }
    }
}
