﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Infrastructure.Extensions;
using GHM.Relationship.Domain.IRepository;
using GHM.Relationship.Domain.IServices;
using GHM.Relationship.Domain.ModelMetas;
using GHM.Relationship.Domain.Models;
using GHM.Relationship.Domain.Resources;
using GHM.Relationship.Domain.ViewModels;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;

namespace GHM.Relationship.Infrastructure.Services
{
    public class RelationshipTypeService : IRelationshipTypeService
    {
        private readonly IRelationshipTypeRepository _relationshipTypeRepository;
        private readonly IRelationshipTypeTranslationRepository _relationshipTypeTranslationRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmRelationshipResource> _relationshipResource;

        public RelationshipTypeService(IRelationshipTypeRepository relationshipTypeRepository, IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmRelationshipResource> relationshipResource, IRelationshipTypeTranslationRepository relationshipTypeTranslationRepository)
        {
            _relationshipTypeRepository = relationshipTypeRepository;
            _sharedResourceService = sharedResourceService;
            _relationshipResource = relationshipResource;
            _relationshipTypeTranslationRepository = relationshipTypeTranslationRepository;
        }

        public async Task<List<RelationshipTypeSearchViewModel>> GetsAll(string tenantId, string languageId, bool? isActive)
        {
            return await _relationshipTypeRepository.GetsAll(tenantId, languageId, isActive);
        }
        public async Task<ActionResultResponse> Insert(string tenantId, RelationshipTypeMeta typeRelationshipMeta)
        {
            var relationshipType = new RelationshipType
            {
                Id = Guid.NewGuid().ToString(),
                TenantId = tenantId,
                IsActive = typeRelationshipMeta.IsActive,
                Kind = typeRelationshipMeta.Kind
            };

            var result = await _relationshipTypeRepository.Insert(relationshipType);
            if (result <= 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            return await AddNewRelationTypeTranslations();

            #region Local functions
            async Task<ActionResultResponse> AddNewRelationTypeTranslations()
            {
                var translations = new List<RelationshipTypeTranslation>();
                foreach (var relationshipTypeTranslation in typeRelationshipMeta.RelationshipTypeTranslations)
                {
                    // Check name exists.
                    var isNameExists = await _relationshipTypeTranslationRepository.CheckExists(tenantId, relationshipType.Id,
                        relationshipTypeTranslation.LanguageId, relationshipTypeTranslation.Name);
                    if (isNameExists)
                    {
                        await RollbackInsertRelationType();
                        return new ActionResultResponse(-1, _relationshipResource.GetString("Relationship type name already exists."));
                    }

                    translations.Add(new RelationshipTypeTranslation
                    {
                        TenantId = tenantId,
                        Name = relationshipTypeTranslation.Name.Trim(),
                        Description = relationshipTypeTranslation.Description?.Trim(),
                        LanguageId = relationshipTypeTranslation.LanguageId.Trim(),
                        UnsignName = relationshipTypeTranslation.Name.Trim().StripVietnameseChars().ToUpper(),
                        RelationshipTypeId = relationshipType.Id
                    });
                }

                var resultInsertTranslation = await _relationshipTypeTranslationRepository.Inserts(translations);
                if (resultInsertTranslation > 0)
                    return new ActionResultResponse(resultInsertTranslation, _relationshipResource
                        .GetString("Add new relationship type successful."));

                await RollbackInsertRelationType();
                await RollbackInsertRelationshipTypeTranslation();
                return new ActionResultResponse(resultInsertTranslation, _relationshipResource
                    .GetString("Relationship type name already exists."));
            }

            async Task RollbackInsertRelationType()
            {
                await _relationshipTypeRepository.ForceDelete(relationshipType.Id);
            }

            async Task RollbackInsertRelationshipTypeTranslation()
            {
                await _relationshipTypeTranslationRepository.ForceDeleteByRelationshipTypeId(relationshipType.Id);
            }
            #endregion
        }

        public async Task<ActionResultResponse> Update(string tenantId, string id, RelationshipTypeMeta typeRelationshipMeta)
        {
            var relationTypeinfo = await _relationshipTypeRepository.GetInfo(id);
            if (relationTypeinfo == null)
                return new ActionResultResponse(-2, _relationshipResource.GetString("Relationship type does not exists."));

            if (relationTypeinfo.TenantId != tenantId)
                return new ActionResultResponse(-3, _sharedResourceService.GetString("You do not have permission to to this action."));

            relationTypeinfo.IsActive = typeRelationshipMeta.IsActive;
            relationTypeinfo.Kind = typeRelationshipMeta.Kind;

            // Update relationship type info.
            await _relationshipTypeRepository.Update(relationTypeinfo);

            // Update relationship type info by language.
            return await UpdateRelationshipTranslation();

            #region Local functions

            async Task<ActionResultResponse> UpdateRelationshipTranslation()
            {
                var listNewTranslations = new List<RelationshipTypeTranslation>();
                foreach (var relationshipTypeTranslationMeta in typeRelationshipMeta.RelationshipTypeTranslations)
                {
                    var relationTypeTranslationInfo = await _relationshipTypeTranslationRepository.GetInfo(tenantId, id,
                        relationshipTypeTranslationMeta.LanguageId);

                    // Check name exists.
                    var isNameExists = await _relationshipTypeTranslationRepository.CheckExists(tenantId, relationTypeinfo.Id,
                        relationshipTypeTranslationMeta.LanguageId, relationshipTypeTranslationMeta.Name);
                    if (isNameExists)
                    {
                        return new ActionResultResponse(-1, _relationshipResource.GetString("Relationship type name already exists."));
                    }

                    if (relationTypeTranslationInfo == null)
                    {
                        listNewTranslations.Add(new RelationshipTypeTranslation
                        {
                            TenantId = tenantId,
                            Name = relationshipTypeTranslationMeta.Name.Trim(),
                            Description = relationshipTypeTranslationMeta.Description?.Trim(),
                            LanguageId = relationshipTypeTranslationMeta.LanguageId.Trim(),
                            UnsignName = relationshipTypeTranslationMeta.Name.Trim().StripVietnameseChars().ToUpper(),
                            RelationshipTypeId = relationTypeinfo.Id
                        });
                    }
                    else
                    {
                        relationTypeTranslationInfo.Name = relationshipTypeTranslationMeta.Name.Trim();
                        relationTypeTranslationInfo.Description = relationshipTypeTranslationMeta.Description?.Trim();
                        relationTypeTranslationInfo.UnsignName = relationshipTypeTranslationMeta.Name.Trim()
                            .StripVietnameseChars().ToUpper();
                        await _relationshipTypeTranslationRepository.Update(relationTypeTranslationInfo);
                    }
                }

                await _relationshipTypeTranslationRepository.Inserts(listNewTranslations);
                return new ActionResultResponse(1, _relationshipResource.GetString("Update relationship type successful."));
            }
            #endregion
        }

        public async Task<ActionResultResponse> Delete(string tenantId, string id)
        {
            var isExists = await _relationshipTypeRepository.CheckExists(id, tenantId);
            if (!isExists)
                return new ActionResultResponse(-1, _relationshipResource.GetString("Type relationship does not exists."));

            var result = await _relationshipTypeRepository.Delete(id);
            return new ActionResultResponse(result, result > 0
                ? _relationshipResource.GetString("Delete type relationship successful.")
                : _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
        }

        //public async Task<ActionResultResponse> ForceDeleteDelete(string tenantId, string id)
        //{
        //    var info = await _relationshipTypeRepository.GetInfo(id);
        //    if (info == null)
        //        return new ActionResultResponse(-1, _relationshipResource.GetString("Type relationship does not exists."));

        //    if (info.TenantId != tenantId)
        //        return new ActionResultResponse(-2, _sharedResourceService.GetString("You do not have permission to to this action."));

        //    var result = await _relationshipTypeRepository.ForceDelete(id);
        //    return new ActionResultResponse(result, _relationshipResource.GetString("Delete type relationship successful."));
        //}

        public async Task<ActionResultResponse<RelationshipTypeViewModel>> GetDetail(string tenantId, string id)
        {
            var info = await _relationshipTypeRepository.GetInfo(id, true);
            if (info == null)
                return new ActionResultResponse<RelationshipTypeViewModel>(-1, _relationshipResource.GetString("Relationship type does not exists."));

            if (info.TenantId != tenantId)
                return new ActionResultResponse<RelationshipTypeViewModel>(-2, _sharedResourceService.GetString("You do not have permission to do this action."));

            var typeRelationshipDetail = new RelationshipTypeViewModel
            {
                Id = info.Id,
                Kind = info.Kind,
                IsActive = info.IsActive,
                RelationshipTypeTranslations = await _relationshipTypeTranslationRepository.GetsByRelationshipTypeId(info.Id)
            };
            return new ActionResultResponse<RelationshipTypeViewModel>
            {
                Code = 1,
                Data = typeRelationshipDetail
            };
        }

        public async Task<List<Suggestion<string>>> GetsForSuggestions(string tenantId, string languageId)
        {
            return await _relationshipTypeRepository.GetsForSuggestion(tenantId, languageId);
        }
    }
}
