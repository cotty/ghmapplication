﻿using System.Threading.Tasks;
using GHM.Relationship.Domain.IRepository;
using GHM.Relationship.Domain.IServices;
using GHM.Relationship.Domain.ModelMetas;
using GHM.Relationship.Domain.Resources;
using GHM.Relationship.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.Services;
using GHM.Infrastructure.ViewModels;
using Microsoft.Extensions.Configuration;

namespace GHM.Relationship.Infrastructure.Services
{
    public class RelationshipService : IRelationshipService
    {
        private readonly IRelationshipRepository _relationshipRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmRelationshipResource> _relationshipResource;
        private readonly IConfiguration _configuration;


        public RelationshipService(IRelationshipRepository relationshipRepository,
            IResourceService<SharedResource> sharedResourceService, IResourceService<GhmRelationshipResource> relationshipResource,
            IConfiguration configuration)
        {
            _relationshipRepository = relationshipRepository;
            _sharedResourceService = sharedResourceService;
            _relationshipResource = relationshipResource;
            _configuration = configuration;
        }

        public async Task<ActionResultResponse> Insert(string tenantId, RelationshipMeta relationshipMetas)
        {


            var relationship = new Domain.Models.Relationship
            {
                UserId = relationshipMetas.UserId,
                RelationalUserId = relationshipMetas.RelationalUserId,
                RelationshipTypeId = relationshipMetas.RelationshipTypeId,
                IsActive = relationshipMetas.IsActive,
                Description = relationshipMetas.Description,
                TenantId = tenantId
            };

            var info = await _relationshipRepository.GetInfo(relationship.UserId, relationship.RelationalUserId, true);
            if (info != null)
                return new ActionResultResponse(-1, _relationshipResource.GetString("Relationship does exists."));

            var apiUrls = _configuration.GetApiUrl();
            if (apiUrls == null)
                return new ActionResultResponse(-2, _sharedResourceService.GetString("Missing some configuration. Please contact with administrator."));

            var httpClientService = new HttpClientService();
            var infoCustomer = await httpClientService
                .GetAsync<ActionResultResponse<PatientViewModel>>($"{apiUrls.PatientApiUrl}/patients/{relationshipMetas.UserId}");
            if (infoCustomer == null)
                return new ActionResultResponse(-3, _sharedResourceService.GetString("Custome does not exists"));

            var patientInfo = infoCustomer.Data;
            relationship.UserFullName = patientInfo.Name;
            relationship.UserBirthday = patientInfo.Birthday;
            relationship.UserGender = patientInfo.Gender;

            var infoCustomerRelationship = await httpClientService
                .GetAsync<ActionResultResponse<PatientViewModel>>($"{apiUrls.PatientApiUrl}/patients/{relationshipMetas.RelationalUserId}");
            if (infoCustomerRelationship == null)
                return new ActionResultResponse(-4, _sharedResourceService.GetString("Custome relationship does not exists"));



            var patientRelationshipInfo = infoCustomerRelationship.Data;

            relationship.RelationalUserFullName = patientRelationshipInfo.Name;
            relationship.RelationalUserBirthday = patientRelationshipInfo.Birthday;
            relationship.RelationalUserGender = patientRelationshipInfo.Gender;

            relationship.UnsignName =
                $"{relationship.UserFullName.StripVietnameseChars()} {relationship.RelationalUserFullName.StripVietnameseChars()}"
                    .ToUpper();

            var isExists = await _relationshipRepository.CheckExists(relationshipMetas.UserId, relationship.RelationalUserId);
            if (isExists)
                return new ActionResultResponse(-5, _relationshipResource.GetString("Relationship already exists."));

            var result = await _relationshipRepository.Insert(relationship);
            return new ActionResultResponse(result, result <= 0
                ? _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                : _relationshipResource.GetString("Add new relationship successful."));
        }

        public async Task<ActionResultResponse> Update(string tenantId, string userId, string relationalUserId, RelationshipMeta relationshipMetas)
        {
            var info = await _relationshipRepository.GetInfo(userId, relationalUserId);
            if (info == null)
                return new ActionResultResponse(-1, _relationshipResource.GetString("Relationship does not exists."));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString("You do not have permission to to this action."));

            if (info.UserId != relationalUserId)
                return new ActionResultResponse(-3, _sharedResourceService.GetString("User does not exists."));

            if (info.RelationalUserId != relationalUserId)
                return new ActionResultResponse(-4, _sharedResourceService.GetString("relational user does not exists."));

            info.RelationshipTypeId = relationshipMetas.RelationshipTypeId;
            info.IsActive = relationshipMetas.IsActive;
            info.Description = relationshipMetas.Description.Trim();

            var apiUrls = _configuration.GetApiUrl();
            if (apiUrls == null)
                return new ActionResultResponse(-5, _sharedResourceService.GetString("Missing some configuration. Please contact with administrator."));

            var httpClientService = new HttpClientService();
            var infoCustomer = await httpClientService.GetAsync<ActionResultResponse<PatientViewModel>>($"{apiUrls.PatientApiUrl}/patients/{info.UserId}");
            if (infoCustomer == null)
                return new ActionResultResponse(-6, _sharedResourceService.GetString("Custome does not exists"));

            var patientInfo = infoCustomer.Data;
            info.UserFullName = patientInfo.Name;
            info.UserBirthday = patientInfo.Birthday;
            info.UserGender = patientInfo.Gender;

            var infoCustomerRelationship = await httpClientService.GetAsync<ActionResultResponse<PatientViewModel>>($"{apiUrls.PatientApiUrl}/patients/{info.RelationalUserId}");
            if (infoCustomerRelationship == null)
                return new ActionResultResponse(-7, _sharedResourceService.GetString("Custome relationship does not exists"));



            var patientRelationshipInfo = infoCustomerRelationship.Data;

            info.RelationalUserFullName = patientRelationshipInfo.Name;
            info.RelationalUserBirthday = patientRelationshipInfo.Birthday;
            info.RelationalUserGender = patientRelationshipInfo.Gender;

            info.UnsignName =
                $"{info.UserFullName.StripVietnameseChars()} {info.RelationalUserFullName.StripVietnameseChars()}"
                    .ToUpper();


            var result = await _relationshipRepository.Update(info);
            if (result <= 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            return new ActionResultResponse(result, _relationshipResource.GetString("Update relationship successful."));
        }

        public async Task<ActionResultResponse> Delete(string tenantId, string userId, string relationalUserId)
        {
            var info = await _relationshipRepository.GetInfo(userId, relationalUserId);
            if (info == null)
                return new ActionResultResponse(-1, _relationshipResource.GetString("Relationship does not exists."));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString("You do not have permission to to this action."));

            var result = await _relationshipRepository.Delete(userId, relationalUserId);
            return new ActionResultResponse(result, _relationshipResource.GetString("Delete relationship successful."));
        }

        public async Task<ActionResultResponse> ForceDelete(string tenantId, string userId, string relationalUserId)
        {
            var info = await _relationshipRepository.GetInfo(userId, relationalUserId);
            if (info == null)
                return new ActionResultResponse(-1, _relationshipResource.GetString("Relationship does not exists."));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString("You do not have permission to to this action."));

            var result = await _relationshipRepository.ForceDelete(userId, relationalUserId);
            return new ActionResultResponse(result, _relationshipResource.GetString("Delete relationship successful."));
        }

        public async Task<SearchResult<Domain.Models.Relationship>> Search(string tenantId, string keyword, bool? isActive, int page, int pageSize)
        {
            var items = await _relationshipRepository.Search(tenantId, keyword, isActive, page, pageSize, out var totalRows);
            return new SearchResult<Domain.Models.Relationship>
            {
                Items = items,
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse<Domain.Models.Relationship>> GetDetail(string tenantId, string userId, string relationalUserId)
        {
            var info = await _relationshipRepository.GetInfo(userId, relationalUserId, true);
            if (info == null)
                return new ActionResultResponse<Domain.Models.Relationship>(-1, _relationshipResource.GetString("Relationship does not exists."));

            if (info.TenantId != tenantId)
                return new ActionResultResponse<Domain.Models.Relationship>(-2, _sharedResourceService.GetString("You do not have permission to do this action."));

            var relationshipDetail = new Domain.Models.Relationship
            {
                UserId = info.UserId,
                RelationalUserId = info.RelationalUserId,
                RelationshipTypeId = info.RelationshipTypeId,
                UserFullName = info.UserFullName,
                UserBirthday = info.UserBirthday,
                UserGender = info.UserGender,
                RelationalUserFullName = info.RelationalUserFullName,
                RelationalUserBirthday = info.RelationalUserBirthday,
                RelationalUserGender = info.RelationalUserGender,
                UnsignName = info.UnsignName,
                Description = info.Description,
                IsActive = info.IsActive,

            };
            return new ActionResultResponse<Domain.Models.Relationship>
            {
                Code = 1,
                Data = relationshipDetail
            };
        }
    }
}
