﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GHM.Relationship.Infrastructure.Migrations
{
    public partial class InitialDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Relationships",
                columns: table => new
                {
                    TenantId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    RelationalUserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    RelationshipTypeId = table.Column<string>(nullable: false),
                    UserFullName = table.Column<string>(maxLength: 50, nullable: false),
                    UserBirthday = table.Column<DateTime>(nullable: false),
                    UserGender = table.Column<int>(nullable: false),
                    RelationalUserFullName = table.Column<string>(maxLength: 50, nullable: false),
                    RelationalUserBirthday = table.Column<DateTime>(nullable: false),
                    RelationalUserGender = table.Column<int>(nullable: false),
                    UnsignName = table.Column<string>(maxLength: 500, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    IsDelete = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    LastUpdate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Relationships", x => new { x.UserId, x.RelationalUserId });
                });

            migrationBuilder.CreateTable(
                name: "RelationshipTypes",
                columns: table => new
                {
                    Id = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(maxLength: 50, nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    LastUpdateUserId = table.Column<string>(nullable: true),
                    LastUpdateUserFullName = table.Column<string>(nullable: true),
                    TenantId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Kind = table.Column<int>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RelationshipTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RelationshipTypeTranslations",
                columns: table => new
                {
                    TenantId = table.Column<string>(nullable: false),
                    RelationshipTypeId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    LanguageId = table.Column<string>(unicode: false, maxLength: 10, nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    UnsignName = table.Column<string>(unicode: false, maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RelationshipTypeTranslations", x => new { x.TenantId, x.LanguageId, x.RelationshipTypeId });
                    table.ForeignKey(
                        name: "FK_RelationshipTypeTranslations_RelationshipTypes_RelationshipTypeId",
                        column: x => x.RelationshipTypeId,
                        principalTable: "RelationshipTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RelationshipTypeTranslations_RelationshipTypeId",
                table: "RelationshipTypeTranslations",
                column: "RelationshipTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Relationships");

            migrationBuilder.DropTable(
                name: "RelationshipTypeTranslations");

            migrationBuilder.DropTable(
                name: "RelationshipTypes");
        }
    }
}
