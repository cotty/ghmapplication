﻿// <auto-generated />
using System;
using GHM.Relationship.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace GHM.Relationship.Infrastructure.Migrations
{
    [DbContext(typeof(RelationshipDbContext))]
    [Migration("20180619045414_InitialDb")]
    partial class InitialDb
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.0-rtm-30799")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("GHM.Relationship.Domain.Models.Relationship", b =>
                {
                    b.Property<string>("UserId")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("RelationalUserId")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("ConcurrencyStamp")
                        .IsRequired()
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("Description")
                        .HasMaxLength(500);

                    b.Property<bool>("IsActive");

                    b.Property<bool>("IsDelete");

                    b.Property<DateTime?>("LastUpdate");

                    b.Property<DateTime>("RelationalUserBirthday");

                    b.Property<string>("RelationalUserFullName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int>("RelationalUserGender");

                    b.Property<string>("RelationshipTypeId")
                        .IsRequired();

                    b.Property<string>("TenantId")
                        .IsRequired()
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("UnsignName")
                        .IsRequired()
                        .HasMaxLength(500);

                    b.Property<DateTime>("UserBirthday");

                    b.Property<string>("UserFullName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int>("UserGender");

                    b.HasKey("UserId", "RelationalUserId");

                    b.ToTable("Relationships");
                });

            modelBuilder.Entity("GHM.Relationship.Domain.Models.RelationshipType", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("ConcurrencyStamp")
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreateTime");

                    b.Property<bool>("IsActive");

                    b.Property<bool>("IsDelete");

                    b.Property<int>("Kind");

                    b.Property<DateTime?>("LastUpdate");

                    b.Property<string>("LastUpdateUserFullName");

                    b.Property<string>("LastUpdateUserId");

                    b.Property<string>("TenantId")
                        .IsRequired()
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.HasKey("Id");

                    b.ToTable("RelationshipTypes");
                });

            modelBuilder.Entity("GHM.Relationship.Domain.Models.RelationshipTypeTranslation", b =>
                {
                    b.Property<string>("TenantId");

                    b.Property<string>("LanguageId")
                        .HasMaxLength(10)
                        .IsUnicode(false);

                    b.Property<string>("RelationshipTypeId")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("Description")
                        .HasMaxLength(500);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.Property<string>("UnsignName")
                        .IsRequired()
                        .HasMaxLength(256)
                        .IsUnicode(false);

                    b.HasKey("TenantId", "LanguageId", "RelationshipTypeId");

                    b.HasIndex("RelationshipTypeId");

                    b.ToTable("RelationshipTypeTranslations");
                });

            modelBuilder.Entity("GHM.Relationship.Domain.Models.RelationshipTypeTranslation", b =>
                {
                    b.HasOne("GHM.Relationship.Domain.Models.RelationshipType", "RelationshipType")
                        .WithMany("RelationshipTypeTranslations")
                        .HasForeignKey("RelationshipTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
