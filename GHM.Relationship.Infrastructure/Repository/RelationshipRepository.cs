﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Relationship.Domain.IRepository;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;
namespace GHM.Relationship.Infrastructure.Repository
{
    public class RelationshipRepository : RepositoryBase, IRelationshipRepository
    {
        private readonly IRepository<Domain.Models.Relationship> _relationshipRepository;

        public RelationshipRepository(IDbContext context) : base(context)
        {
            _relationshipRepository = Context.GetRepository<Domain.Models.Relationship>();
        }

        public async Task<int> Insert(Domain.Models.Relationship relationship)
        {
            _relationshipRepository.Create(relationship);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(Domain.Models.Relationship relationship)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string userId, string relationalUserId)
        {
            var info = await GetInfo(userId, relationalUserId);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(string userId, string relationalUserId)
        {
            var info = await GetInfo(userId, relationalUserId);
            if (info == null)
                return -1;

            _relationshipRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<bool> CheckExists(string userId, string relationalUserId)
        {
            return await _relationshipRepository.ExistAsync(x => x.UserId == userId && x.RelationalUserId == relationalUserId && !x.IsDelete);
        }

        public async Task<Domain.Models.Relationship> GetInfo(string userId, string relationalUserId, bool isReadOnly = false)
        {
            return await _relationshipRepository.GetAsync(isReadOnly, x => x.UserId == userId && x.RelationalUserId == relationalUserId && !x.IsDelete);
        }

        public  Task<List<Domain.Models.Relationship>> Search(string tenantId,  string keyword,
            bool? isActive, int page, int pageSize,
            out int totalRows)
        {
            Expression<Func<Domain.Models.Relationship, bool>> spec = t => !t.IsDelete && t.TenantId == tenantId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
            }

            if (isActive.HasValue)
            {
                spec = spec.And(x => x.IsActive == isActive.Value);
            }

            var query = Context.Set<Domain.Models.Relationship>().Where(spec).Select(x => new Domain.Models.Relationship()
            {
                UserId = x.UserId,
                RelationalUserId = x.RelationalUserId,
                RelationshipTypeId = x.RelationshipTypeId,
                UserFullName = x.UserFullName,
                UserBirthday = x.UserBirthday,
                UserGender = x.UserGender,
                RelationalUserFullName = x.RelationalUserFullName,
                RelationalUserBirthday =  x.RelationalUserBirthday,
                RelationalUserGender = x.RelationalUserGender,
                UnsignName = x.UnsignName,
                Description = x.Description

            });

            totalRows = _relationshipRepository.Count(spec);
            return query.OrderByDescending(x => new { x.UserId, x.RelationalUserId })
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();

          
        }
    }
}
