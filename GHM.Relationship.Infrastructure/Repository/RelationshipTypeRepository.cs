﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Relationship.Domain.IRepository;
using GHM.Relationship.Domain.Models;
using GHM.Relationship.Domain.ViewModels;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;

namespace GHM.Relationship.Infrastructure.Repository
{
    public class RelationshipTypeRepository : RepositoryBase, IRelationshipTypeRepository
    {
        private readonly IRepository<RelationshipType> _typeRelationshipRepository;
        public RelationshipTypeRepository(IDbContext context) : base(context)
        {
            _typeRelationshipRepository = Context.GetRepository<RelationshipType>();
        }

        public async Task<int> Insert(RelationshipType typeRelationship)
        {
            _typeRelationshipRepository.Create(typeRelationship);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(RelationshipType typeRelationship)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string id)
        {
            var info = await GetInfo(id);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(string id)
        {
            var info = await GetInfo(id);
            if (info == null)
                return -1;

            _typeRelationshipRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<bool> CheckExists(string id, string tenantId)
        {
            return await _typeRelationshipRepository.ExistAsync(
                x => x.Id == id && x.TenantId == tenantId && !x.IsDelete);
        }

        public async Task<List<RelationshipTypeSearchViewModel>> GetsAll(string tenantId, string languageId, bool? isActive)
        {
            Expression<Func<RelationshipType, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId;
            if (isActive.HasValue)
            {
                spec = spec.And(x => x.IsActive == isActive.Value);
            }
            var query = Context.Set<RelationshipType>().Where(spec)
                .Join(Context.Set<RelationshipTypeTranslation>().Where(x => x.LanguageId == languageId),
                rt => rt.Id, rtt => rtt.RelationshipTypeId, (rt, rtt) => new RelationshipTypeSearchViewModel
                {
                    Id = rt.Id,
                    Kind = rt.Kind,
                    IsActive = rt.IsActive,
                    Name = rtt.Name,
                    CreateTime = rt.CreateTime,
                    Description = rtt.Description
                });

            return await query.ToListAsync();
        }

        public async Task<RelationshipType> GetInfo(string id, bool isReadOnly = false)
        {
            return await _typeRelationshipRepository.GetAsync(isReadOnly, x => x.Id == id && !x.IsDelete);
        }

        public async Task<List<T>> GetAllTypeRelationship<T>(Expression<Func<RelationshipType, T>> projector, string tenantId)
        {
            return await _typeRelationshipRepository.GetsAsAsync(projector, x => x.IsActive && !x.IsDelete && x.TenantId == tenantId);
        }

        public async Task<List<Suggestion<string>>> GetsForSuggestion(string tenantId, string languageId)
        {
            var query = Context.Set<RelationshipType>().Where(x => x.TenantId == tenantId)
                .Join(Context.Set<RelationshipTypeTranslation>().Where(x => x.LanguageId == languageId), r => r.Id,
                    rt => rt.RelationshipTypeId,
                    (r, rt) => new Suggestion<string>
                    {
                        Id = r.Id,
                        Name = rt.Name
                    });

            return await query.AsNoTracking().ToListAsync();
        }
    }
}
