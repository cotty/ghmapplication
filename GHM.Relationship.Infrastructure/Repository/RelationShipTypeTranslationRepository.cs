﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Infrastructure.SqlServer;
using GHM.Relationship.Domain.IRepository;
using GHM.Relationship.Domain.Models;

namespace GHM.Relationship.Infrastructure.Repository
{
    public class RelationshipTypeTranslationRepository : RepositoryBase, IRelationshipTypeTranslationRepository
    {
        private readonly IRepository<RelationshipTypeTranslation> _relationShipTypeTranslationRepository;
        public RelationshipTypeTranslationRepository(IDbContext context) : base(context)
        {
            _relationShipTypeTranslationRepository = Context.GetRepository<RelationshipTypeTranslation>();
        }

        public async Task<bool> CheckExists(string tenantId, string id, string languageId, string name)
        {
            name = name.Trim();
            return await _relationShipTypeTranslationRepository.ExistAsync(x =>
                x.TenantId == tenantId && x.LanguageId == languageId
                && x.RelationshipTypeId != id && x.Name == name);
        }

        public async Task<int> Insert(RelationshipTypeTranslation relationshipTypeTranslation)
        {
            _relationShipTypeTranslationRepository.Create(relationshipTypeTranslation);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<RelationshipTypeTranslation> relationshipTypeTranslations)
        {
            _relationShipTypeTranslationRepository.Creates(relationshipTypeTranslations);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(RelationshipTypeTranslation relationshipTypeTranslation)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDeleteByRelationshipTypeId(string relationshipTypeId)
        {
            var relationShipTypeTranslations =
                await _relationShipTypeTranslationRepository.GetsAsync(false,
                    x => x.RelationshipTypeId == relationshipTypeId);
            if (relationShipTypeTranslations == null)
                return -1;

            _relationShipTypeTranslationRepository.Deletes(relationShipTypeTranslations);
            return await Context.SaveChangesAsync();
        }

        public async Task<RelationshipTypeTranslation> GetInfo(string tenantId, string relationshipTypeId, string languageId, bool isReadOnly = false)
        {
            return await _relationShipTypeTranslationRepository.GetAsync(isReadOnly, x =>
                x.RelationshipTypeId == relationshipTypeId && x.TenantId == tenantId && x.LanguageId == languageId);
        }

        public async Task<List<RelationshipTypeTranslation>> GetsByRelationshipTypeId(string relationshipTypeId)
        {
            return await _relationShipTypeTranslationRepository.GetsAsync(true,
                x => x.RelationshipTypeId == relationshipTypeId);
        }
    }
}
