﻿using GHM.Infrastructure.SqlServer;
using GHM.Relationship.Domain.Models;
using GHM.Relationship.Infrastructure.Configurations;
using Microsoft.EntityFrameworkCore;

namespace GHM.Relationship.Infrastructure
{
    public class RelationshipDbContext : DbContextBase
    {
        public RelationshipDbContext(DbContextOptions<RelationshipDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Configurations:
            builder.ApplyConfiguration(new RelationshipTypeConfig());
            builder.ApplyConfiguration(new RelationshipTypeTranslationConfig());
            builder.ApplyConfiguration(new RelationshipConfig());

            builder.Entity<RelationshipTypeTranslation>()
                .HasOne(rl => rl.RelationshipType)
                .WithMany(rlt => rlt.RelationshipTypeTranslations);
        }
    }
}
