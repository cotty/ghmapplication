﻿using GHM.Infrastructure;
using Microsoft.AspNetCore.Mvc;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GHM.Tasks.Api.Controllers
{
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class AttachmentController : GhmControllerBase
    {
        [Route("download/{path}/{fileName}"), AcceptVerbs("POST")]
        public IActionResult DownloadFile(string path, string fileName)
        {
            var net = new System.Net.WebClient();
            var data = net.DownloadData(path);
            var content = new System.IO.MemoryStream(data);
            var contentType = "APPLICATION/octet-stream";

            return File(content, contentType, fileName);
        }
    }
}
