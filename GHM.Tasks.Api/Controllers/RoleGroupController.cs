﻿using System.Threading.Tasks;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Tasks.Api.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/role-groups")]
    public class RoleGroupController : GhmControllerBase
    {
        private readonly IRoleGroupService _taskRoleGroupService;
        public RoleGroupController(IRoleGroupService taskRoleGroupService)
        {
            _taskRoleGroupService = taskRoleGroupService;
        }

        [AcceptVerbs("GET")]
        [AllowPermission(PageId.TaskRoleGroup, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string keyword, int page = 1, int pageSize = 20)
        {
            var result = await _taskRoleGroupService.Search(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, keyword, page, pageSize);
            if (result.Code < 0)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.TaskRoleGroup, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]RoleGroupMeta roleGroupMeta)
        {
            roleGroupMeta.CreatorId = CurrentUser.Id;
            roleGroupMeta.CreatorFullName = CurrentUser.FullName;
            var result = await _taskRoleGroupService.Insert(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, roleGroupMeta);
            if (result.Code < 0)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.TaskRoleGroup, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody]RoleGroupMeta roleGroupMeta)
        {
            var result = await _taskRoleGroupService.Update(CurrentUser.TenantId, id, CurrentUser.Id, CurrentUser.FullName, roleGroupMeta);
            if (result.Code < 0)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.TaskRoleGroup, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _taskRoleGroupService.Delete(CurrentUser.TenantId, id);
            if (result.Code < 0)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        [Route("select"), AcceptVerbs("GET")]
        [AllowPermission(PageId.TaskRoleGroup, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> GetForSelect()
        {
            return Ok(await _taskRoleGroupService.SearchAll(CurrentUser.TenantId));
        }
    }
}