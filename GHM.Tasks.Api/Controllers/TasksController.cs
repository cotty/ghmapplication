﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using GHM.Infrastructure.Helpers;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using Hangfire;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace GHM.Tasks.Api.Controllers
{
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class TasksController : GhmControllerBase
    {
        private readonly ITasksService _tasksService;
        private readonly ITasksParticipantsService _tasksParticipantsService;
        private readonly ITasksCheckListsService _taskChecklistService;
        private readonly IMediator _mediator;

        public TasksController(IMediator mediator,
            ITasksService tasksService, ITasksParticipantsService tasksParticipantsService, ITasksCheckListsService taskChecklistService)
        {
            _mediator = mediator;
            _tasksService = tasksService;
            _tasksParticipantsService = tasksParticipantsService;
            _taskChecklistService = taskChecklistService;
        }

        [Route("filter"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.TaskList, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search([FromBody]TaskFilter taskFilter)
        {
            taskFilter.CurrentUserId = string.IsNullOrEmpty(taskFilter.CurrentUserId) ? CurrentUser.Id : taskFilter.CurrentUserId;
            taskFilter.TenantId = CurrentUser.TenantId;

            var result = await _tasksService.Search(taskFilter);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.TaskList, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Detail(long id)
        {
            var result = await _tasksService.GetDetail(CurrentUser.TenantId, id, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.TaskList, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(long id)
        {
            var result = await _tasksService.Delete(CurrentUser.TenantId, id, CurrentUser.Id, CurrentUser.FullName);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.TaskList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(long id, [FromBody]TaskMeta taskMeta)
        {
            taskMeta.UserId = CurrentUser.Id;
            taskMeta.FullName = CurrentUser.FullName;
            taskMeta.Avatar = CurrentUser.Avatar;
            var result = await _tasksService.Update(CurrentUser.TenantId, id, taskMeta);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}/status/{status}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TaskList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateStatus([FromBody] TaskStatusMeta taskStatus)
        {
            var result = await _tasksService.UpdateStatus(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar,
                taskStatus);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}/updatePercentCompleted"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TaskList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateResult(long id, [FromBody]int percentCompleted)
        {
            var result = await _tasksService.UpdatePercentCompleted(CurrentUser.TenantId, id, percentCompleted);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}/{userId}"), AcceptVerbs("PUT")]
        [AllowPermission(PageId.TaskList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateResponsible(long id, string userId)
        {
            var result = await _tasksService.UpdateResponsible(CurrentUser.TenantId, id, userId, CurrentUser.Id, CurrentUser.FullName);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [AcceptVerbs("POST")]
        [AllowPermission(PageId.TaskList, Permission.Insert), ValidateModel]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]TaskMeta taskMeta)
        {
            taskMeta.TenantId = CurrentUser.TenantId;
            taskMeta.UserId = CurrentUser.Id;
            taskMeta.FullName = CurrentUser.FullName;
            var result = await _tasksService.Insert(taskMeta);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}/child"), AcceptVerbs("GET")]
        [AllowPermission(PageId.TargetList, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetTaskChildTree(long id)
        {
            return Ok(await _tasksService.GetTaskChildTree(CurrentUser.TenantId, CurrentUser.Id, id));
        }

        #region Tasks participant.
        [Route("{id}/participants"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.TaskList, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> SaveParticipant(long id, [FromBody]List<TaskParticipantMeta> participant)
        {
            var result = await _tasksParticipantsService.Save(CurrentUser.TenantId, id, participant);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}/participants"), AcceptVerbs("GET")]
        [AllowPermission(PageId.TaskList, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetParticipants(long id)
        {
            var result = await _tasksParticipantsService.GetsAllByTaskId(CurrentUser.TenantId, id);
            return Ok(result);
        }

        [Route("{id}/participants/{userId}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.TaskList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateRoleParticipants(long id, string userId, [FromBody]TaskParticipantMeta taskParticipantMeta)
        {
            var result = await _tasksParticipantsService.Update(CurrentUser.TenantId, id, userId, taskParticipantMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}/participants/{userId}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.TaskList, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> DeleteParticipants(long id, string userId)
        {
            var result = await _tasksParticipantsService.Delete(CurrentUser.TenantId, id, userId, CurrentUser.Id);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }
        #endregion

        #region Task logs
        [Route("{id}/histories"), AcceptVerbs("GET")]
        [AllowPermission(PageId.TaskList, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> SearchLogs(long id, int page, int pageSize)
        {
            var result = await _tasksService.SearchHistory(CurrentUser.TenantId, id, CurrentUser.Id, page, pageSize);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }
        #endregion

        [Route("{id}/notification"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TaskList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateNotification(long id, [FromBody]TaskNotificationMeta taskNotificationMeta)
        {
            var result = await _tasksService.UpdateNotification(id, CurrentUser.TenantId, CurrentUser.Id, CurrentUser.UserName, CurrentUser.Avatar, taskNotificationMeta);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}/notification"), AcceptVerbs("GET")]
        [AllowPermission(PageId.TaskList, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetNotification(long id)
        {
            var result = await _tasksService.GetNotificationDetail(id, CurrentUser.TenantId);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }


        #region Checklist
        [Route("{taskId}/checklists"), AcceptVerbs("POST")]
        [ValidateModel]
        [CheckPermission]
        public async Task<IActionResult> InsertCheckLists(long taskId, [FromBody]CheckListsMeta checklistMeta)
        {
            checklistMeta.UserId = CurrentUser.Id;
            checklistMeta.FullName = CurrentUser.FullName;
            checklistMeta.TenantId = CurrentUser.TenantId;
            checklistMeta.TaskId = taskId;
            var result = await _tasksService.InsertChecklist(checklistMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{taskId}/checklists/{checklistId}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TaskList, Permission.View)]
        [ValidateModel]
        public async Task<IActionResult> UpdateCheckLists(long taskId, string checklistId, [FromBody]CheckListsMeta checklistMeta)
        {
            checklistMeta.UserId = CurrentUser.Id;
            checklistMeta.FullName = CurrentUser.FullName;
            checklistMeta.TenantId = CurrentUser.TenantId;
            checklistMeta.TaskId = taskId;
            var result = await _tasksService.UpdateChecklist(checklistId, checklistMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{taskId}/checklists/{checklistId}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.TaskList, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> DeleteCheckLists(long taskId, string checklistId)
        {
            var result = await _tasksService.DeleteChecklist(CurrentUser.TenantId, CurrentUser.Id, taskId, checklistId);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{taskId}/checklists/{checklistId}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.TaskList, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> UpdateCompletedStatus(long taskId, string checklistId, bool isCompleted)
        {
            var result = await _tasksService.UpdateCompleteStatus(CurrentUser.TenantId, CurrentUser.Id, taskId, checklistId, isCompleted);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
        #endregion

        #region Attachment
        [Route("{tasksId}/attachment"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TaskList, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> InsertAttachment(long tasksId, [FromBody]AttachmentMeta attachmentMeta)
        {
            var result = await _tasksService.InsertAttachment(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName,
                CurrentUser.Avatar, tasksId, attachmentMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{tasksId}/attachment/{attachmentId}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.TaskList, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> DeleteAttachment(long tasksId, string attachmentId)
        {
            var result = await _tasksService.DeleteAttachment(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar, tasksId, attachmentId);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{tasksId}/attachments"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TaskList, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> InsertListAttachment(long tasksId, [FromBody]List<AttachmentMeta> attachments)
        {
            var result = await _tasksService.InsertAttachments(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar,
                tasksId, attachments);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
        #endregion

        #region report
        [Route("search-report"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TaskReport, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> SearchForReport([FromBody]TaskReportFilter taskReportFilter)
        {
            var result = await _tasksService.SearchForReport(CurrentUser.TenantId, taskReportFilter);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("report-detail/{page?}/{pageSize?}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TaskReport, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> ReportDetail([FromBody]TaskReportFilter taskReportFilter, int page = 1, int pageSize = 20)
        {
            var result = await _tasksService.SearchReportDetail(CurrentUser.TenantId, taskReportFilter, page, pageSize);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("export-excel-report"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TaskReport, Permission.Export)]
        [CheckPermission]
        public async Task<IActionResult> ExportReport([FromBody]TaskReportFilter taskReportFilter)
        {
            using (var xls = new ExcelPackage())
            {
                var sheet = xls.Workbook.Worksheets.Add("Sheet1");
                var colorTitle = ColorTranslator.FromHtml("#003300");
                var colorHeader = ColorTranslator.FromHtml("#3598dc");
                var colorWhite = ColorTranslator.FromHtml("#ffffff");
                var colorBlue = ColorTranslator.FromHtml("#4FC1F5");
                var colorGreen = ColorTranslator.FromHtml("#9BCB62");
                var colorRed = ColorTranslator.FromHtml("#F5564A");
                var colorOrange = ColorTranslator.FromHtml("#FFC928");
                var colorGray = ColorTranslator.FromHtml("#B0B1B0");

                ExcelHelper.CreateCellTable(sheet, 1, 1, 1, 8,
                    taskReportFilter.TaskReportType == TaskReportType.statisticsByPersonal ? "Báo cáo công việc theo nhân viên"
                    : "Báo cáo công việc theo đơn vị", new CustomExcelStyle
                    {
                        IsBold = true,
                        FontSize = 20,
                        IsMerge = true,
                        HorizontalAlign = ExcelHorizontalAlignment.Center
                    });
                ExcelHelper.CreateCellTable(sheet, 2, 1, 2, 4, "Thời gian:", new CustomExcelStyle
                {
                    IsBold = true,
                    FontSize = 11,
                    IsMerge = true,
                    HorizontalAlign = ExcelHorizontalAlignment.Right
                });

                ExcelHelper.CreateCellTable(sheet, 2, 5, 2, 8, taskReportFilter.StartDate?.ToString("dd/MM/yyyy") + " - " + taskReportFilter.EndDate?.ToString("dd/MM/yyyy"), new CustomExcelStyle
                {
                    IsBold = true,
                    FontSize = 11,
                    IsMerge = true,
                    HorizontalAlign = ExcelHorizontalAlignment.Left
                });

                ExcelHelper.CreateCellTable(sheet, 3, 1, 3, 4, "Tìm kiếm thời gian theo:", new CustomExcelStyle
                {
                    IsBold = true,
                    FontSize = 11,
                    IsMerge = true,
                    HorizontalAlign = ExcelHorizontalAlignment.Right
                });

                ExcelHelper.CreateCellTable(sheet, 3, 5, 3, 8, GetTypeSearchTimeName(taskReportFilter.TypeSearchTime), new CustomExcelStyle
                {
                    IsBold = true,
                    FontSize = 11,
                    IsMerge = true,
                    HorizontalAlign = ExcelHorizontalAlignment.Left
                });

                ExcelHelper.CreateCellTable(sheet, 4, 1, 4, 4, "Bảng mục tiêu:", new CustomExcelStyle
                {
                    IsBold = true,
                    FontSize = 11,
                    IsMerge = true,
                    HorizontalAlign = ExcelHorizontalAlignment.Right
                });

                ExcelHelper.CreateCellTable(sheet, 4, 5, 4, 8, !string.IsNullOrEmpty(taskReportFilter.TableTargetName) ? taskReportFilter.TableTargetName : "Tất cả", new CustomExcelStyle
                {
                    IsBold = true,
                    IsMerge = true,
                    FontSize = 11,
                    HorizontalAlign = ExcelHorizontalAlignment.Left
                });

                ExcelHelper.CreateCellTable(sheet, 5, 1, 5, 4, "Loại công việc:", new CustomExcelStyle
                {
                    IsBold = true,
                    FontSize = 11,
                    IsMerge = true,
                    HorizontalAlign = ExcelHorizontalAlignment.Right
                });

                ExcelHelper.CreateCellTable(sheet, 5, 5, 5, 8, taskReportFilter.TaskType == TasksKind.Personal ? "Cá nhân"
                    : taskReportFilter.TaskType == TasksKind.Group ? "Nhóm" : "Tất cả", new CustomExcelStyle
                    {
                        IsBold = true,
                        IsMerge = true,
                        FontSize = 11,
                        HorizontalAlign = ExcelHorizontalAlignment.Left
                    });

                ExcelHelper.CreateCellTable(sheet, 6, 1, 6, 4, "Đơn vị", new CustomExcelStyle
                {
                    IsBold = true,
                    FontSize = 11,
                    IsMerge = true,
                    HorizontalAlign = ExcelHorizontalAlignment.Right
                });

                ExcelHelper.CreateCellTable(sheet, 6, 5, 6, 8, !string.IsNullOrEmpty(taskReportFilter.OfficeName) ? taskReportFilter.OfficeName : "Tất cả", new CustomExcelStyle
                {
                    IsBold = true,
                    FontSize = 11,
                    IsMerge = true,
                    HorizontalAlign = ExcelHorizontalAlignment.Left
                });

                if (taskReportFilter.TaskReportType == TaskReportType.statisticsByPersonal || taskReportFilter.TaskReportType == TaskReportType.listByPersonal)
                {
                    ExcelHelper.CreateCellTable(sheet, 7, 1, 7, 4, "Nhân viên", new CustomExcelStyle
                    {
                        IsBold = true,
                        FontSize = 11,
                        IsMerge = true,
                        HorizontalAlign = ExcelHorizontalAlignment.Right
                    });
                    ExcelHelper.CreateCellTable(sheet, 7, 5, 7, 8, !string.IsNullOrEmpty(taskReportFilter.FullName) ? taskReportFilter.FullName : "Tất cả", new CustomExcelStyle
                    {
                        IsBold = true,
                        FontSize = 11,
                        IsMerge = true,
                        HorizontalAlign = ExcelHorizontalAlignment.Left
                    });
                }

                if (taskReportFilter.TaskReportType == TaskReportType.statisticsByPersonal || taskReportFilter.TaskReportType == TaskReportType.statisticsByUnits)
                {
                    ExcelHelper.CreateHeaderTable(sheet, 8, 1, 9, 1, "STT", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 2, 9, 2, taskReportFilter.TaskReportType == TaskReportType.statisticsByPersonal ? "Nhân viên"
                        : "Đơn vị", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 3, 9, 3, "Tổng số công việc", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 4, 8, 5, "Chưa hoàn thành", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 6, 8, 7, "Hoàn thành", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 8, 9, 8, "Hủy", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 9, 4, "Đúng tiến độ", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 9, 5, "Quá hạn", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 9, 6, "Đúng tiến độ", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 9, 7, "Chậm tiến độ", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);

                    var rowIndex = 10;
                    var totalTaskActived = 0;
                    var totalProgressNotComplete = 0;
                    var totalOverDueNotComplete = 0;
                    var totalProgressComplete = 0;
                    var totalOverDueComplete = 0;
                    var totalTotalDestroy = 0;
                    var listReport = await _tasksService.SearchForReport(CurrentUser.TenantId, taskReportFilter);

                    foreach (var item in listReport.Items)
                    {
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 1, rowIndex - 9, ExcelHorizontalAlignment.Center, false);
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 2, taskReportFilter.TaskReportType == TaskReportType.statisticsByPersonal ? item.FullName : item.OfficeName, ExcelHorizontalAlignment.Left, false, true);
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 3, item.TotalTask, ExcelHorizontalAlignment.Center, false, true);
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 4, item.ProgressNotComplete, new CustomExcelStyle
                        {
                            HorizontalAlign = ExcelHorizontalAlignment.Center,
                            Border = ExcelBorderStyle.Thin,
                            Color = colorBlue
                        });
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 5, item.OverDueNotComplete, new CustomExcelStyle
                        {
                            HorizontalAlign = ExcelHorizontalAlignment.Center,
                            Border = ExcelBorderStyle.Thin,
                            Color = colorRed
                        });
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 6, item.ProgressComplete, new CustomExcelStyle
                        {
                            HorizontalAlign = ExcelHorizontalAlignment.Center,
                            Border = ExcelBorderStyle.Thin,
                            Color = colorGreen
                        });
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 7, item.OverDueComplete, new CustomExcelStyle
                        {
                            HorizontalAlign = ExcelHorizontalAlignment.Center,
                            Border = ExcelBorderStyle.Thin,
                            Color = colorOrange
                        });
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 8, item.TotalDestroy, new CustomExcelStyle
                        {
                            HorizontalAlign = ExcelHorizontalAlignment.Center,
                            Border = ExcelBorderStyle.Thin,
                            Color = colorGray
                        });

                        rowIndex++;
                        totalTaskActived += item.TotalTask;
                        totalProgressNotComplete += item.ProgressNotComplete;
                        totalOverDueNotComplete += item.OverDueNotComplete;
                        totalProgressComplete += item.ProgressComplete;
                        totalOverDueComplete += item.OverDueComplete;
                        totalTotalDestroy += item.TotalDestroy;
                    }

                    ExcelHelper.CreateCellTable(sheet, rowIndex, 1, rowIndex + 1, 2, "Tổng", new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        IsMerge = true,
                        Border = ExcelBorderStyle.Thin,
                        IsBold = true
                    });

                    ExcelHelper.CreateCellTable(sheet, rowIndex, 3, rowIndex + 1, 3, totalTaskActived, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        IsMerge = true,
                        Border = ExcelBorderStyle.Thin,
                        IsBold = true
                    });

                    ExcelHelper.CreateCellTable(sheet, rowIndex, 4, totalProgressNotComplete, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        Border = ExcelBorderStyle.Thin,
                        Color = colorBlue,
                    });

                    ExcelHelper.CreateCellTable(sheet, rowIndex, 5, totalOverDueNotComplete, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        Border = ExcelBorderStyle.Thin,
                        IsBold = true,
                        Color = colorRed,
                    });

                    ExcelHelper.CreateCellTable(sheet, rowIndex, 6, totalProgressComplete, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        Border = ExcelBorderStyle.Thin,
                        IsBold = true,
                        Color = colorGreen,
                    });

                    ExcelHelper.CreateCellTable(sheet, rowIndex, 7, totalOverDueComplete, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        Border = ExcelBorderStyle.Thin,
                        IsBold = true,
                        Color = colorOrange,
                    });

                    ExcelHelper.CreateCellTable(sheet, rowIndex, 8, rowIndex + 1, 8, totalTotalDestroy, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        IsMerge = true,
                        Border = ExcelBorderStyle.Thin,
                        IsBold = true,
                        Color = colorGray
                    });

                    ExcelHelper.CreateCellTable(sheet, rowIndex + 1, 4, rowIndex + 1, 5, totalProgressNotComplete + totalOverDueNotComplete, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        IsMerge = true,
                        Border = ExcelBorderStyle.Thin,
                        IsBold = true
                    });

                    ExcelHelper.CreateCellTable(sheet, rowIndex + 1, 6, rowIndex + 1, 7, totalProgressComplete + totalOverDueComplete, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        IsMerge = true,
                        Border = ExcelBorderStyle.Thin,
                        IsBold = true
                    });

                    ExcelHelper.CreateCellTable(sheet, rowIndex + 3, 1, rowIndex + 3, 8, "Báo cáo được kết xuất vào ngày " + DateTime.Now.ToString("dd/MM/yyyy HH:mm"), new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Left,
                        IsMerge = true,
                        IsBold = true,
                        Color = colorGray
                    });
                }
                else
                {
                    ExcelHelper.CreateHeaderTable(sheet, 8, 1, "STT", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 2, "Tên công việc", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 3, "Loại công việc", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 4, "Nhân viên", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 5, "% Hoàn thành", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 6, "Check list", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 7, "Trạng thái", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 8, "Ngày bắt đầu", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 9, "Hạn hoàn thành", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 10, "Ngày hoàn thành", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 11, "Ngày tạo", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 12, "Mô tả", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 13, "Mục tiêu", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);

                    var listReport = await _tasksService.SearchReportDetail(CurrentUser.TenantId, taskReportFilter, 1, int.MaxValue);
                    var rowIndex = 9;
                    foreach (var item in listReport.Items)
                    {
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 1, rowIndex - 8, ExcelHorizontalAlignment.Center, false);
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 2, item.Name, ExcelHorizontalAlignment.Left, false, true);
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 3, item.Type == TasksKind.Group ? "Nhóm" : "Cá nhân", ExcelHorizontalAlignment.Center, false, true);
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 4, item.ResponsibleFullName, ExcelHorizontalAlignment.Left, false, true);
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 5, item.PercentCompleted, ExcelHorizontalAlignment.Right, false, true);
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 6, $"{item.ChecklistCount}/{item.CompletedChecklistCount}", ExcelHorizontalAlignment.Right, false, true);
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 7, GetStatusName(item.Status), ExcelHorizontalAlignment.Left, false, true);
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 8, item.StartDate?.ToString("dd/MM/yyyy HH:mm"), ExcelHorizontalAlignment.Left, false, true);
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 9, item.EstimateEndDate.ToString("dd/MM/yyyy"), ExcelHorizontalAlignment.Left, false, true);
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 10, item.EndDate?.ToString("dd/MM/yyyy HH:mm"), ExcelHorizontalAlignment.Left, false, true);
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 11, item.CreateTime.ToString("dd/MM/yyyy HH:mm"), ExcelHorizontalAlignment.Left, false, true);
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 12, item.Description, ExcelHorizontalAlignment.Left, false, true);
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 13, item.TargetName, ExcelHorizontalAlignment.Left, false, true);
                        rowIndex++;
                    }
                }

                sheet.Cells.Style.WrapText = true;
                sheet.Column(2).Width = 30;
                sheet.Column(3).Width = 15;
                sheet.Column(4).Width = 15;
                sheet.Column(5).Width = 15;
                sheet.Column(6).Width = 15;
                sheet.Column(7).Width = 15;
                sheet.Row(1).Height = 30;
                sheet.Row(8).Height = 30;
                sheet.Row(9).Height = 25;
                sheet.Cells.Style.Font.Size = 11;
                sheet.Row(1).Style.Font.Size = 20;
                sheet.Cells.Style.Font.Name = "Times New Roman";
                var stream = new MemoryStream(xls.GetAsByteArray())
                {
                    // Reset Stream Position
                    //Position = 0
                };

                var fileName = "Báo cáo công việc đơn vị.xlsx";
                var fileType = "application/vnd.ms-excel";
                return File(stream, fileType, fileName);
            }

        }

        private string GetTypeSearchTimeName(TypeSearchTime typeSearchTime)
        {
            switch (typeSearchTime)
            {
                case TypeSearchTime.StarteDateToEndDate:
                    return "Khoảng ngày: Ngày bắt đầu - Hạn hoàn thành";
                case TypeSearchTime.StartDate:
                    return "Ngày bắt đầu";
                case TypeSearchTime.EndDate:
                    return "Hạn hoàn thành";
                case TypeSearchTime.CreateDate:
                    return "Ngày tạo";
                case TypeSearchTime.CompleteDate:
                    return "Ngày hoàn thành";
                default:
                    return "Không xác định";
            }
        }

        private string GetStatusName(TasksStatus status)
        {
            switch (status)
            {
                case TasksStatus.NotStartYet:
                    return "Chưa bắt đầu";
                case TasksStatus.DeclineFinish:
                    return "Từ chối";
                case TasksStatus.InProgress:
                    return "Đang xử lý";
                case TasksStatus.PendingToFinish:
                    return "Chờ duyệt";
                case TasksStatus.Completed:
                    return "Hoàn thành";
                default:
                    return "Không xác định";
            }
        }
        #endregion
    }
}