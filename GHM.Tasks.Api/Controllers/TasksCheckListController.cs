﻿using System.Threading.Tasks;
using GHM.Infrastructure;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Tasks.Api.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/checklists")]
    public class TasksCheckListController : GhmControllerBase
    {
        private readonly ITasksCheckListsService _tasksCheckListsService;

        public TasksCheckListController(ITasksCheckListsService tasksCheckListsService)
        {
            _tasksCheckListsService = tasksCheckListsService;
        }

        [Route("{taskId}/{type}"), AcceptVerbs("GET")]
        [CheckPermission]
        public async Task<IActionResult> SearchByTasksType(long taskId, TaskType type)
        {
            var result = await _tasksCheckListsService.SearchByTasksTypeId(CurrentUser.TenantId, taskId, type);
            if(result.Code < 0)
                return BadRequest(result);

            return Ok(result);            
        }

        [AcceptVerbs("POST"), ValidateModel]
        [CheckPermission]
        public async Task<IActionResult> InsertCheckList([FromBody]CheckListsMeta checkList)
        {
            var result = await _tasksCheckListsService.Insert(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, checkList);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("POST"), ValidateModel] 
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody]CheckListsMeta checkListsMeta)
        {
            var result = await _tasksCheckListsService.Update(CurrentUser.TenantId, id, CurrentUser.Id, CurrentUser.FullName, checkListsMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("DELETE")]
        [CheckPermission]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _tasksCheckListsService.ForceDelete(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, id);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);                    
        }
    }
}