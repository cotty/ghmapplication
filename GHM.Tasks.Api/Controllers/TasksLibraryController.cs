﻿using System.Threading.Tasks;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Tasks.Api.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/tasks-libraries")]
    public class TasksLibraryController : GhmControllerBase
    {
        public ITasksGroupLibraryService _tasksGroupLibraryService;
        public ITasksLibraryService _tasksLibraryService;
        public TasksLibraryController(ITasksGroupLibraryService tasksGroupLibraryService, ITasksLibraryService taskLibraryService)
        {
            _tasksGroupLibraryService = tasksGroupLibraryService;
            _tasksLibraryService = taskLibraryService;
        }

        [Route("group"), AcceptVerbs("GET")]
        [AllowPermission(PageId.TaskLibrary, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetsAll()
        {
            var result = await _tasksGroupLibraryService.GetAllTasksLibrary(CurrentUser.TenantId);
            return Ok(result);
        }

        [Route("group"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.TaskLibrary, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> InsertTasksLibraryGroup([FromBody]TasksGroupLibraryMeta tasksGroupLibraryMeta)
        {
            var result = await _tasksGroupLibraryService.Insert(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, tasksGroupLibraryMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("group/{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.TaskLibrary, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateTasksGroupLibrary(string id, [FromBody]TasksGroupLibraryMeta tasksGroupLibraryMeta)
        {
            var result = await _tasksGroupLibraryService.Update(CurrentUser.TenantId, id, CurrentUser.Id, CurrentUser.FullName, tasksGroupLibraryMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("group/{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.TaskLibrary, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> DeleteTasksGroupLibrary(string id)
        {
            var result = await _tasksGroupLibraryService.ForceDelete(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, id);
            if (result.Code < 0)
                return BadRequest(result);
 
            return Ok(result);
        }

        [Route("group/{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.TaskLibrary, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> GetTasksGroupLibraryDetail(string id)
        {
            var result = await _tasksGroupLibraryService.GetDetail(CurrentUser.TenantId, id);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.TaskLibrary, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> InsertTasksLibrary([FromBody]TasksLibraryMeta tasksLibraryMeta)
        {
            var result = await _tasksLibraryService.Insert(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, tasksLibraryMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.TaskLibrary, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateTasksLibrary(long id, [FromBody]TasksLibraryMeta tasksLibraryMeta)
        {
            var result = await _tasksLibraryService.Update(CurrentUser.TenantId, id, CurrentUser.Id, CurrentUser.FullName, tasksLibraryMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.TaskLibrary, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> DeleteTasksLibrary(long id)
        {
            var result = await _tasksLibraryService.ForceDelete(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, id);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
    }
}