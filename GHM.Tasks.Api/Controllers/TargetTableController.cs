﻿using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace GHM.Tasks.Api.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/target-tables")]
    public class TargetTableController : GhmControllerBase
    {
        private readonly ITargetTableService _targetTableService;
        private readonly ITargetGroupService _targetGroupService;
        public TargetTableController(ITargetTableService targetTableService, ITargetGroupService targetGroupService)
        {
            _targetTableService = targetTableService;
            _targetGroupService = targetGroupService;
        }

        [AcceptVerbs("GET")]
        [AllowPermission(PageId.TargetTable, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search()
        {
            var result = await _targetTableService.Search(CurrentUser.TenantId);

            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.TargetTable, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]TargetTableMeta targetTableMeta)
        {
            var result = await _targetTableService.Insert(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, targetTableMeta);

            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.TargetTable, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody]TargetTableMeta targetTableMeta)
        {
            var result = await _targetTableService.Update(id, CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, targetTableMeta);

            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.TargetTable, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _targetTableService.Delete(id, CurrentUser.TenantId);

            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}/status/{status}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetTable, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateStatus(string id, TargetTableStatus status)
        {
            var result = await _targetTableService.UpdateStatus(CurrentUser.TenantId, id, status);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}/order/{order}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetTable, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateOrder(string id, int order)
        {
            var result = await _targetTableService.UpdateOrder(CurrentUser.TenantId, id, order);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}/detail"), AcceptVerbs("GET")]
        [AllowPermission(PageId.TargetTable, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> GetDetail(string id)
        {
            var result = await _targetTableService.GetDetail(CurrentUser.TenantId, id, CurrentUser.Id, CurrentUser.FullName);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        #region TargetGroup
        [Route("{targetTableId}/groups"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.TargetTable, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> InsertTargetGroup(string targetTableId, [FromBody]TargetGroupMeta targetGroupMeta)
        {
            var result = await _targetGroupService.Insert(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, targetTableId, targetGroupMeta);

            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{targetTableId}/groups/{targetGroupId}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.TargetTable, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateTargetGroup(string targetTableId, string targetGroupId, [FromBody]TargetGroupMeta targetGroupMeta)
        {
            var result = await _targetGroupService.Update(targetGroupId, CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, targetGroupMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{targetTableId}/groups/{targetGroupId}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.TargetTable, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> InsertTargetGroup(string targetTableId, string targetGroupId)
        {
            var result = await _targetGroupService.Delete(targetGroupId, CurrentUser.TenantId);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{targetTableId}/groups/{targetGroupId}/order/{order}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetTable, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateOrderTargetGroup(string targetTableId, string targetGroupId, int order)
        {
            var result = await _targetGroupService.UpdateOrder(CurrentUser.TenantId, targetGroupId, order);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{targetTableId}/groups/{targetGroupId}/color"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetTable, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateOrderTargetGroup(string targetTableId, string targetGroupId, string color)
        {
            var result = await _targetGroupService.UpdateColor(CurrentUser.TenantId, targetGroupId, color);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("search-for-suggestion"), AcceptVerbs("GET")]
        public async Task<IActionResult> SearchForSuggestion()
        {
            var result = await _targetTableService.GetForSuggestion(CurrentUser.TenantId);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{targetTableId}/target-groups"), AcceptVerbs("GET")]
        public async Task<IActionResult> SearchTargetGroupByTargetTableId(string targetTableId)
        {
            var result = await _targetGroupService.GetTargetGroupByTargetTableId(CurrentUser.TenantId, targetTableId);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }
        #endregion
    }
}