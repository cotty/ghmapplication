﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Infrastructure;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Infrastructure.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Tasks.Api.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class LogsController : GhmControllerBase
    {
        private readonly ITasksService _taskService;
        private readonly ITargetService _targetService;

        public LogsController(ITasksService taskService, ITargetService targetService)
        {
            _taskService = taskService;
            _targetService = targetService;
        }

        [AcceptVerbs("GET")]
        public async Task<IActionResult> Search(ObjectType objectType, long objectId, int page = 1, int pageSize = 20)
        {
            SearchResult<Log> result = new SearchResult<Log>(-1, string.Empty);
            switch (objectType)
            {
                case ObjectType.Task:
                    result = await _taskService.SearchHistory(CurrentUser.TenantId, objectId, CurrentUser.Id, page,
                        pageSize);
                    break;

                case ObjectType.Target:
                    result = await _targetService.SearchHistory(CurrentUser.TenantId, objectId, CurrentUser.Id, page,
                        pageSize);
                    break;
            }

            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }
    }
}