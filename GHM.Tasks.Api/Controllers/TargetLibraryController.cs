﻿using System.Threading.Tasks;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Tasks.Api.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/target-libraries")]
    public class TargetLibraryController : GhmControllerBase
    {
        public ITargetGroupLibraryService _targetGroupLibraryService;
        public ITargetLibraryService _targetLibraryService;
        public TargetLibraryController(ITargetGroupLibraryService targetGroupLibraryService, ITargetLibraryService targetLibraryService)
        {
            _targetGroupLibraryService = targetGroupLibraryService;
            _targetLibraryService = targetLibraryService;
        }

        [Route("group"), AcceptVerbs("GET")]
        [AllowPermission(PageId.TargetLibrary, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetsAll()
        {
            var result = await _targetGroupLibraryService.GetAllAndTargetLibrary(CurrentUser.TenantId);
            return Ok(result);
        }

        [Route("group"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.TargetLibrary, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> InsertTargetLibraryGroup([FromBody]TargetGroupLibraryMeta targetGroupLibraryMeta)
        {
            var result = await _targetGroupLibraryService.Insert(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, targetGroupLibraryMeta);
            if(result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("group/{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.TargetLibrary, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateTargetGroupLibrary(string id,[FromBody]TargetGroupLibraryMeta targetGroupLibraryMeta)
        {
            var result = await _targetGroupLibraryService.Update(CurrentUser.TenantId, id, CurrentUser.Id, CurrentUser.FullName, targetGroupLibraryMeta);
            if(result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("group/{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.TargetLibrary, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> DeleteTargetGroupLibrary(string id)
        {
            var result = await _targetGroupLibraryService.ForceDelete(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, id);
            if(result.Code < 0)
                return BadRequest(result); 
            
            return Ok(result);
        }

        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.TargetLibrary, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> InsertTargetLibrary([FromBody]TargetLibraryMeta targetGroupLibraryMeta)
        {
            var result = await _targetLibraryService.Insert(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, targetGroupLibraryMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.TargetLibrary, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateTargetLibrary(string id, [FromBody]TargetLibraryMeta targetGroupLibraryMeta)
        {
            var result = await _targetLibraryService.Update(CurrentUser.TenantId, id, CurrentUser.Id, CurrentUser.FullName, targetGroupLibraryMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.TargetLibrary, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> DeleteTargetLibrary(string id)
        {
            var result = await _targetLibraryService.ForceDelete(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, id);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
    }
}