﻿using GHM.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Tasks.Api.Controllers
{
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/task-logs")]
    public class TasksLogsController : GhmControllerBase
    {

    }
}
