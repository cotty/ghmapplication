﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Infrastructure;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Tasks.Api.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/comments")]
    public class CommentController : GhmControllerBase
    {
        private ICommentService _commentService;
        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        [Route("{objectType}/{objectId}"),AcceptVerbs("GET")]
        public async Task<IActionResult> GetsAllByObjectId(ObjectType objectType, long objectId , int page = 1, int pageSize = 20)
        {
            var result = await _commentService.GetsAllByObject(CurrentUser.TenantId, CurrentUser.Id, objectType, objectId, page, pageSize);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [AcceptVerbs("POST"), ValidateModel]
        public async Task<IActionResult> Post([FromBody]CommentMeta commentMeta)
        {
            var result = await _commentService.Post(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar, commentMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [AcceptVerbs("DELETE")]
        [Route("{id}")]
        [CheckPermission]
        public async Task<IActionResult> Delete(long id)
        {
            var result = await _commentService.ForceDelete(CurrentUser.TenantId, CurrentUser.Id, id);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
    }
}