﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Filters;
using GHM.Infrastructure.Helpers;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace GHM.Tasks.Api.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/targets")]
    public class TargetController : GhmControllerBase
    {
        private readonly ITargetService _targetService;
        public TargetController(ITargetService targetService)
        {
            _targetService = targetService;
        }

        [AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]TargetMeta targetMeta)
        {
            var result = await _targetService.Insert(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar, targetMeta);
            if (result.Code < 0)
                return BadRequest(result);
            return Ok(result);
        }

        #region
        // update Target Name 
        [Route("{id}/name"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateTargetName(long id, [FromBody]TargetUpdateMeta targetMeta)
        {
            var result = await _targetService.UpdateTargetName(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar,
                id, targetMeta.Name);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        // update description 
        [Route("{id}/description"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateTargetDescription(long id, [FromBody]TargetUpdateMeta targetMeta)
        {
            var result = await _targetService.UpdateTargetDescription(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar,
                id, targetMeta.Description);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
        // update date time  
        [Route("{id}/date"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateTargetDate(long id, [FromBody]TargetUpdateMeta targetMeta)
        {
            var result = await _targetService.UpdateTargetDate(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar, id,
                targetMeta.StartDate, targetMeta.EndDate);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}/complete-date"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateTargetComleteDate(long id, [FromBody]TargetUpdateMeta targetMeta)
        {
            var result = await _targetService.UpdateTargetCompleteDate(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar,
                id, targetMeta.CompleteDate);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}/targetGroupId"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateTargetGroupId(long id, [FromBody]TargetUpdateMeta targetMeta)
        {
            var result = await _targetService.UpdateTargetGroupId(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar,
                id, targetMeta.GroupId);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        // update percentCompleted  % hoan thanh
        [Route("{id}/percentCompleted/{percentCompleted}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateTargePercentCompleted(long id, double percentCompleted)
        {
            var result = await _targetService.UpdateTargetPercentCompleted(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar,
                id, percentCompleted);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        // update Status complete 
        [Route("{id}/status/{status}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateTargeStatusComplete(long id, TargetStatus status)
        {
            var result = await _targetService.UpdateTargetStatus(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar, id, status);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        //update TargetTable and group
        [Route("{id}/tableGroup"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateTargetTableGroup(long id, [FromBody]TargetUpdateMeta targetMeta)
        {
            var result = await _targetService.UpdateTargetTableGroup(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar, id,
                targetMeta.TableId, targetMeta.GroupId);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        // update Measurement method
        [Route("{id}/measurementMethod"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateMeasurementMethod(long id, [FromBody]TargetUpdateMeta targetMeta)
        {
            var result = await _targetService.UpdateMeasurementMethod(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar,
                id, targetMeta.MeasurementMethod);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        //update weight
        [Route("{id}/weight/{weight}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateWeight(long id, double weight)
        {
            var result = await _targetService.UpdateWeight(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar, id, weight);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        //update MethodCaculateResult
        [Route("{id}/methodCalculateResult/{methodCalculateResult}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateMethodCalculateResult(long id, TargetMethodCalculateResultType methodCalculateResult)
        {
            var result = await _targetService.UpdateMethodCalculateResult(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar,
                id, methodCalculateResult);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        //Update level of sharing
        [Route("{id}/levelOfSharing/{levelOfSharing}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateLevelOfSharing(long id, LevelOfSharing levelOfSharing)
        {
            var result = await _targetService.UpdateLevelOfSharing(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar, id, levelOfSharing);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        //update role Tartget participants
        [Route("{id}/roleParticipant/{roleParticipant}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateRoleParticipant(string id, int roleParticipant)
        {
            var result = await _targetService.UpdateRoleParticipant(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar,
                id, roleParticipant);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        // Insert Target participants
        [Route("targetParticipants"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> InsertTarGetParticipants([FromBody]TargetParticipantsMeta targetParticipantsMeta)
        {
            var result = await _targetService.InsertParticipants(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar,
                targetParticipantsMeta);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
        [Route("{id}/targetParticipants"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> DeleteTargetParticipants(string id)
        {
            var result = await _targetService.DeleteParticipants(CurrentUser.TenantId, id, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        #endregion
        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Delete(long id)
        {
            var result = await _targetService.ForceDelete(CurrentUser.TenantId, CurrentUser.Id, id);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> GetDetail(long id)
        {
            var result = await _targetService.GetDetail(CurrentUser.TenantId, id, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        #region Search
        [Route("personals"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> SearchTargetPersonal(string userId, string keyword,
            DateTime? startDate, DateTime? endDate, string targetTableId, TargetStatus? status,
            [FromBody]List<int> listType)
        {
            var result = await _targetService.SearchTargetPersonal(CurrentUser.TenantId, CurrentUser.Id, userId, keyword,
                startDate, endDate, targetTableId, status, listType);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("offices"), AcceptVerbs("GET")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> SearchTargetOffice(int? officeId, string keyword, DateTime? startDate, DateTime? endDate, string targetTableId, TargetStatus? status)
        {
            var result = await _targetService.SearchTargetOffice(CurrentUser.TenantId, officeId, CurrentUser.Id, keyword, startDate, endDate, targetTableId, status);
            if (result.Code < 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("suggestions"), AcceptVerbs("GET")]
        [CheckPermission]
        public async Task<IActionResult> SearchSuggestion(string keyword, string targetTableId, TargetType? type, bool? isGetFinish)
        {
            var result = await _targetService.SearchSuggestion(CurrentUser.TenantId, CurrentUser.Id, keyword, targetTableId, type, isGetFinish);
            return Ok(result);
        }

        [Route("equally-weights/{type}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> EquallyWeight(TargetType type, string targetGroupId, long? parentId)
        {
            var result = await _targetService.EquallyWeight(CurrentUser.TenantId, type, targetGroupId, parentId);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}/get-parent-child"), AcceptVerbs("GET")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> GetTargetChildTree(long id)
        {
            var result = await _targetService.GetTargetChildTree(CurrentUser.TenantId, CurrentUser.Id, id);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
        #endregion

        #region Attachment
        [Route("{targetId}/attachment"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> InsertAttachment(long targetId, [FromBody]AttachmentMeta attachmentMeta)
        {
            var result = await _targetService.InsertAttachment(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName,
                CurrentUser.Avatar, targetId, attachmentMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{targetId}/attachment/{attachmentId}"), AcceptVerbs("Delete")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> DeleteAttachment(long targetId, string attachmentId)
        {
            var result = await _targetService.DeleteAttachment(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar, targetId, attachmentId);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{targetId}/attachments"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetList, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> InsertListAttachment(long targetId, [FromBody]List<AttachmentMeta> attachments)
        {
            var result = await _targetService.InsertAttachments(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar,
                targetId, attachments);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        #endregion

        #region Report
        [Route("report"), AcceptVerbs("POST")]
        [AllowPermission(PageId.Target, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> SearchForReport([FromBody]TargetFilterReport targetFilterReport)
        {
            var result = await _targetService.SearchForReport(CurrentUser.TenantId, targetFilterReport.StartDate, targetFilterReport.EndDate,
                targetFilterReport.TypeSearchTime, targetFilterReport.TableTargetId, targetFilterReport.TargetType,
                targetFilterReport.TargetReportType, targetFilterReport.OfficeId, targetFilterReport.UserId);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("report-detail/{page?}/{pageSize?}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.Target, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> SearchReportDetail([FromBody]TargetFilterReport targetFilterReport, int page = 1, int pageSize = 20)
        {
            var result = await _targetService.SearchReportDetail(CurrentUser.TenantId, targetFilterReport, page, pageSize);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("export-excel-report"), AcceptVerbs("POST")]
        [AllowPermission(PageId.TargetReport, Permission.Export)]
        [CheckPermission]
        public async Task<IActionResult> ExportReport([FromBody]TargetFilterReport targetFilterReport)
        {
            if (targetFilterReport.TargetReportType == TargetReportType.statisticsByPersonal || targetFilterReport.TargetReportType == TargetReportType.statisticsByUnits)
            {
                var listReport = await _targetService.SearchForReport(CurrentUser.TenantId, targetFilterReport.StartDate, targetFilterReport.EndDate,
                    targetFilterReport.TypeSearchTime, targetFilterReport.TableTargetId, targetFilterReport.TargetType,
                    targetFilterReport.TargetReportType, targetFilterReport.OfficeId, targetFilterReport.UserId);

                using (var xls = new ExcelPackage())
                {
                    var sheet = xls.Workbook.Worksheets.Add("Sheet1");
                    var colorTitle = ColorTranslator.FromHtml("#003300");
                    var colorHeader = ColorTranslator.FromHtml("#3598dc");
                    var colorWhite = ColorTranslator.FromHtml("#ffffff");
                    var colorBlue = ColorTranslator.FromHtml("#4FC1F5");
                    var colorGreen = ColorTranslator.FromHtml("#9BCB62");
                    var colorRed = ColorTranslator.FromHtml("#F5564A");
                    var colorOrange = ColorTranslator.FromHtml("#FFC928");
                    var colorGray = ColorTranslator.FromHtml("#B0B1B0");

                    ExcelHelper.CreateCellTable(sheet, 1, 1, 1, 8,
                        targetFilterReport.TargetReportType == TargetReportType.statisticsByPersonal ? "Báo cáo mục tiêu theo nhân viên"
                        : "Báo cáo mục tiêu theo đơn vị", new CustomExcelStyle
                        {
                            IsBold = true,
                            FontSize = 20,
                            IsMerge = true,
                            HorizontalAlign = ExcelHorizontalAlignment.Center
                        });
                    ExcelHelper.CreateCellTable(sheet, 2, 1, 2, 4, "Thời gian:", new CustomExcelStyle
                    {
                        IsBold = true,
                        FontSize = 11,
                        IsMerge = true,
                        HorizontalAlign = ExcelHorizontalAlignment.Right
                    });

                    ExcelHelper.CreateCellTable(sheet, 2, 5, 2, 8, targetFilterReport.StartDate?.ToString("dd/MM/yyyy") + " - " + targetFilterReport.EndDate?.ToString("dd/MM/yyyy"), new CustomExcelStyle
                    {
                        IsBold = true,
                        FontSize = 11,
                        IsMerge = true,
                        HorizontalAlign = ExcelHorizontalAlignment.Left
                    });

                    ExcelHelper.CreateCellTable(sheet, 3, 1, 3, 4, "Tìm kiếm thời gian theo:", new CustomExcelStyle
                    {
                        IsBold = true,
                        FontSize = 11,
                        IsMerge = true,
                        HorizontalAlign = ExcelHorizontalAlignment.Right
                    });

                    ExcelHelper.CreateCellTable(sheet, 3, 5, 3, 8, GetTypeSearchTimeName(targetFilterReport.TypeSearchTime), new CustomExcelStyle
                    {
                        IsBold = true,
                        FontSize = 11,
                        IsMerge = true,
                        HorizontalAlign = ExcelHorizontalAlignment.Left
                    });

                    ExcelHelper.CreateCellTable(sheet, 4, 1, 4, 4, "Bảng mục tiêu:", new CustomExcelStyle
                    {
                        IsBold = true,
                        FontSize = 11,
                        IsMerge = true,
                        HorizontalAlign = ExcelHorizontalAlignment.Right
                    });

                    ExcelHelper.CreateCellTable(sheet, 4, 5, 4, 8, !string.IsNullOrEmpty(targetFilterReport.TableTargetName) ? targetFilterReport.TableTargetName : "Tất cả", new CustomExcelStyle
                    {
                        IsBold = true,
                        IsMerge = true,
                        FontSize = 11,
                        HorizontalAlign = ExcelHorizontalAlignment.Left
                    });

                    ExcelHelper.CreateCellTable(sheet, 5, 1, 5, 4, "Loại mục tiêu:", new CustomExcelStyle
                    {
                        IsBold = true,
                        FontSize = 11,
                        IsMerge = true,
                        HorizontalAlign = ExcelHorizontalAlignment.Right
                    });

                    ExcelHelper.CreateCellTable(sheet, 5, 5, 5, 8, targetFilterReport.TargetType == TargetType.Personal ? "Nhân viên"
                        : targetFilterReport.TargetType == TargetType.Office ? "Đơn vị" : "Tất cả", new CustomExcelStyle
                        {
                            IsBold = true,
                            IsMerge = true,
                            FontSize = 11,
                            HorizontalAlign = ExcelHorizontalAlignment.Left
                        });

                    ExcelHelper.CreateCellTable(sheet, 6, 1, 6, 4, "Đơn vị", new CustomExcelStyle
                    {
                        IsBold = true,
                        FontSize = 11,
                        IsMerge = true,
                        HorizontalAlign = ExcelHorizontalAlignment.Right
                    });

                    ExcelHelper.CreateCellTable(sheet, 6, 5, 6, 8, !string.IsNullOrEmpty(targetFilterReport.OfficeName) ? targetFilterReport.OfficeName : "Tất cả", new CustomExcelStyle
                    {
                        IsBold = true,
                        FontSize = 11,
                        IsMerge = true,
                        HorizontalAlign = ExcelHorizontalAlignment.Left
                    });

                    if (targetFilterReport.TargetReportType == TargetReportType.statisticsByPersonal)
                    {
                        ExcelHelper.CreateCellTable(sheet, 7, 1, 7, 4, "Nhân viên", new CustomExcelStyle
                        {
                            IsBold = true,
                            FontSize = 11,
                            IsMerge = true,
                            HorizontalAlign = ExcelHorizontalAlignment.Right
                        });
                        ExcelHelper.CreateCellTable(sheet, 7, 5, 7, 8, !string.IsNullOrEmpty(targetFilterReport.FullName) ? targetFilterReport.FullName : "Tất cả", new CustomExcelStyle
                        {
                            IsBold = true,
                            FontSize = 11,
                            IsMerge = true,
                            HorizontalAlign = ExcelHorizontalAlignment.Left
                        });
                    }

                    ExcelHelper.CreateHeaderTable(sheet, 8, 1, 9, 1, "STT", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 2, 9, 2, targetFilterReport.TargetReportType == TargetReportType.statisticsByPersonal ? "Nhân viên"
                        : "Đơn vị", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 3, 9, 3, "Tổng mục tiêu hoạt động", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 4, 8, 5, "Chưa hoàn thành", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 6, 8, 7, "Hoàn thành", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 8, 8, 9, 8, "Hủy", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 9, 4, "Đúng tiến độ", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 9, 5, "Quá hạn", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 9, 6, "Đúng tiến độ", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                    ExcelHelper.CreateHeaderTable(sheet, 9, 7, "Chậm tiến độ", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);

                    var rowIndex = 10;
                    var totalTargetActived = 0;
                    var totalProgressNotComplete = 0;
                    var totalOverDueNotComplete = 0;
                    var totalProgressComplete = 0;
                    var totalOverDueComplete = 0;
                    var totalTotalDestroy = 0;
                    foreach (var item in listReport.Items)
                    {
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 1, rowIndex - 9, ExcelHorizontalAlignment.Center, false);
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 2, targetFilterReport.TargetReportType == TargetReportType.statisticsByPersonal ? item.FullName : item.OfficeName, ExcelHorizontalAlignment.Left, false, true);
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 3, item.TotalTarget, ExcelHorizontalAlignment.Center, false, true);
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 4, item.ProgressNotComplete, new CustomExcelStyle
                        {
                            HorizontalAlign = ExcelHorizontalAlignment.Center,
                            Border = ExcelBorderStyle.Thin,
                            Color = colorBlue
                        });
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 5, item.OverDueNotComplete, new CustomExcelStyle
                        {
                            HorizontalAlign = ExcelHorizontalAlignment.Center,
                            Border = ExcelBorderStyle.Thin,
                            Color = colorRed
                        });
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 6, item.ProgressComplete, new CustomExcelStyle
                        {
                            HorizontalAlign = ExcelHorizontalAlignment.Center,
                            Border = ExcelBorderStyle.Thin,
                            Color = colorGreen
                        });
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 7, item.OverDueComplete, new CustomExcelStyle
                        {
                            HorizontalAlign = ExcelHorizontalAlignment.Center,
                            Border = ExcelBorderStyle.Thin,
                            Color = colorOrange
                        });
                        ExcelHelper.CreateCellTable(sheet, rowIndex, 8, item.TotalDestroy, new CustomExcelStyle
                        {
                            HorizontalAlign = ExcelHorizontalAlignment.Center,
                            Border = ExcelBorderStyle.Thin,
                            Color = colorGray
                        });

                        rowIndex++;
                        totalTargetActived += item.TotalTarget;
                        totalProgressNotComplete += item.ProgressNotComplete;
                        totalOverDueNotComplete += item.OverDueNotComplete;
                        totalProgressComplete += item.ProgressComplete;
                        totalOverDueComplete += item.OverDueComplete;
                        totalTotalDestroy += item.TotalDestroy;
                    }

                    ExcelHelper.CreateCellTable(sheet, rowIndex, 1, rowIndex + 1, 2, "Tổng", new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        IsMerge = true,
                        Border = ExcelBorderStyle.Thin,
                        IsBold = true
                    });

                    ExcelHelper.CreateCellTable(sheet, rowIndex, 3, rowIndex + 1, 3, totalTargetActived, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        IsMerge = true,
                        Border = ExcelBorderStyle.Thin,
                        IsBold = true
                    });

                    ExcelHelper.CreateCellTable(sheet, rowIndex, 4, totalProgressNotComplete, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        Border = ExcelBorderStyle.Thin,
                        Color = colorBlue,
                    });

                    ExcelHelper.CreateCellTable(sheet, rowIndex, 5, totalOverDueNotComplete, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        Border = ExcelBorderStyle.Thin,
                        IsBold = true,
                        Color = colorRed,
                    });

                    ExcelHelper.CreateCellTable(sheet, rowIndex, 6, totalProgressComplete, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        Border = ExcelBorderStyle.Thin,
                        IsBold = true,
                        Color = colorGreen,
                    });

                    ExcelHelper.CreateCellTable(sheet, rowIndex, 7, totalOverDueComplete, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        Border = ExcelBorderStyle.Thin,
                        IsBold = true,
                        Color = colorOrange,
                    });

                    ExcelHelper.CreateCellTable(sheet, rowIndex, 8, rowIndex + 1, 8, totalTotalDestroy, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        IsMerge = true,
                        Border = ExcelBorderStyle.Thin,
                        IsBold = true,
                        Color = colorGray
                    });

                    ExcelHelper.CreateCellTable(sheet, rowIndex + 1, 4, rowIndex + 1, 5, totalProgressNotComplete + totalOverDueNotComplete, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        IsMerge = true,
                        Border = ExcelBorderStyle.Thin,
                        IsBold = true
                    });

                    ExcelHelper.CreateCellTable(sheet, rowIndex + 1, 6, rowIndex + 1, 7, totalProgressComplete + totalOverDueComplete, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        IsMerge = true,
                        Border = ExcelBorderStyle.Thin,
                        IsBold = true
                    });

                    ExcelHelper.CreateCellTable(sheet, rowIndex + 3, 1, rowIndex + 3, 8, "Báo cáo được kết xuất vào ngày " + DateTime.Now.ToString("dd/MM/yyyy HH:mm"), new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Left,
                        IsMerge = true,
                        IsBold = true,
                        Color = colorGray
                    });

                    sheet.Cells.Style.WrapText = true;
                    sheet.Column(2).Width = 30;
                    sheet.Column(3).Width = 20;
                    sheet.Column(4).Width = 15;
                    sheet.Column(5).Width = 15;
                    sheet.Column(6).Width = 15;
                    sheet.Column(7).Width = 15;
                    sheet.Row(1).Height = 30;
                    sheet.Row(8).Height = 25;
                    sheet.Row(9).Height = 25;
                    sheet.Cells.Style.Font.Size = 11;
                    sheet.Row(1).Style.Font.Size = 20;
                    sheet.Cells.Style.Font.Name = "Times New Roman";
                    var stream = new MemoryStream(xls.GetAsByteArray())
                    {
                        // Reset Stream Position
                        Position = 0
                    };

                    var fileName = "Báo cáo mục tiêu đơn vị.xlsx";
                    var fileType = "application/vnd.ms-excel";
                    return File(stream, fileType, fileName);
                }
            }
            else
            {
                return Ok(1);
            }
        }
        #endregion Export

        #region private
        private string GetTypeSearchTimeName(TypeSearchTime typeSearchTime)
        {
            switch (typeSearchTime)
            {
                case TypeSearchTime.StarteDateToEndDate:
                    return "Khoảng ngày: Ngày bắt đầu - Hạn hoàn thành";
                case TypeSearchTime.StartDate:
                    return "Ngày bắt đầu";
                case TypeSearchTime.EndDate:
                    return "Hạn hoàn thành";
                case TypeSearchTime.CreateDate:
                    return "Ngày tạo";
                case TypeSearchTime.CompleteDate:
                    return "Ngày hoàn thành";
                default:
                    return "Không xác định";
            }
        }
        #endregion
    }
}