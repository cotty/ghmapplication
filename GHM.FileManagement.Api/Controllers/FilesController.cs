﻿using System.Threading.Tasks;
using GHM.FileManagement.Domain.IServices;
using GHM.FileManagement.Domain.ModelMetas;
using GHM.Infrastructure.CustomAttributes;
using Microsoft.AspNetCore.Mvc;
using GHM.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using GHM.Infrastructure.Filters;
using GHM.Infrastructure.Constants;
using System.IO;
using GHM.Infrastructure.Models;

namespace GHM.FileManagement.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class FilesController : GhmControllerBase
    {
        private readonly IFileService _fileService;

        public FilesController(IFileService fileService)
        {
            _fileService = fileService;
        }

        [Route("uploads"), AcceptVerbs("POST")]
        //[AllowPermission(PageId.WebsiteFile, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> UploadFile(FileUploadMeta files)
        {
            var result = await _fileService.UploadFiles(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName,
                CurrentUser.Avatar, files.FolderId, files.IsTrash, files.IsPublic, files.FormFileCollection);
            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        //[AllowPermission(PageId.WebsiteFile, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody]FileMeta fileMeta)
        {
            var result = await _fileService.Update(CurrentUser.TenantId, CurrentUser.Id, id, fileMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("DELETE")]
        // [AllowPermission(PageId.WebsiteFile, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> DeleteFile(string id)
        {
            var result = await _fileService.Delete(CurrentUser.TenantId, CurrentUser.Id, id);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("GET")]
        //[AllowPermission(PageId.WebsiteFile, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetFile(string id)
        {
            var result = await _fileService.GetInfo(CurrentUser.TenantId, id);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("download/{id}"), AcceptVerbs("POST")]
        [CheckPermission]
        public async Task<IActionResult> Download(string id)
        {
            var fileInfo = await _fileService.GetInfo(CurrentUser.TenantId, id, true);
            if (fileInfo.Code <= 0)
                return BadRequest(fileInfo);

            var info = fileInfo.Data;

            var memory = new MemoryStream();
            var path = Path.Combine(
                           Directory.GetCurrentDirectory(),
                           "", info.Url);

            FileInfo file = new FileInfo(path);
            if (file.Exists)
            {
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return File(memory, info.Type, Path.GetFileName(info.Url));
            }
            else
            {
                return BadRequest(NotFound());
            }
        }        

        // Get api for service other
        [Route("{tenantId}/{id}"), AcceptVerbs("GET")]
        //[AllowPermission(PageId.WebsiteFile, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetFileInfo(string tenantId, string id)
        {
            var result = await _fileService.GetInfo(tenantId, id);
            return Ok(result);
        }
    }
}