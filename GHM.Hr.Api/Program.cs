﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace GHM.Hr.Api
{
    ///<Summary>
    /// Gets the answer
    ///</Summary>
    public class Program
    {
        ///<Summary>
        /// Gets the answer
        ///</Summary>
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
