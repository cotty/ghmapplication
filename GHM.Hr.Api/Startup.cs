﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using FluentValidation.AspNetCore;
using GHM.Hr.Infrastructure;
using GHM.Hr.Infrastructure.AutofacModules;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.ModelBinders;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using NSwag;
using NSwag.AspNetCore;
using NSwag.SwaggerGeneration.Processors.Security;
using GHM.Infrastructure.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace GHM.Hr.Api
{
    ///<Summary>
    /// Gets the answer
    ///</Summary>
    public class Startup
    {
        ///<Summary>
        /// Gets the answer
        ///</Summary>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        public IConfiguration Configuration { get; }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Config IIS support.
            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });

            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
            });

            // Add Swagger support.            
            //services.AddSwaggerDocument(document =>
            //{
            //    document.DocumentProcessors.Add(new SecurityDefinitionAppender("oauth2", new SwaggerSecurityScheme
            //    {
            //        Type = SwaggerSecuritySchemeType.OAuth2,
            //        Description = "Foo",
            //        Flow = SwaggerOAuth2Flow.Implicit,
            //        AuthorizationUrl = "http://localhost:5000/connect/authorize",
            //        Scopes = new Dictionary<string, string>
            //                {
            //                    {"GHM_Hr_Api", "Write access to protected resources"}
            //                }
            //    }));
            //    document.OperationProcessors.Add(
            //        new OperationSecurityScopeProcessor("oauth2"));
            //});

            services.AddMemoryCache();
            services.AddMediatR();
            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .Services
                .AddHttpClient<IHttpClientService, HttpClientService, HttpClientOptions>(Configuration, nameof(HttpClientOptions));
            services.AddMvcCore(options =>
            {
                options.Conventions.Add(new DefaultFromBodyBindingConvention());
                options.ModelBinderProviders.Insert(0, new DateTimeModelBinderProvider());
            }).AddAuthorization(options =>
                {
                    options.AddPolicy("server", builder => { builder.RequireScope("GHM_Hr_Api"); });
                })
                .AddJsonFormatters()
                //.AddJsonOptions(options =>
                //{
                //    options.SerializerSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                //})
                .AddFluentValidation();

            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new RequestCulture("vi-VN");
                options.SupportedCultures = new List<CultureInfo>
                {
                        new CultureInfo("vi-VN"),
                        new CultureInfo("en-US")
                };
                options.RequestCultureProviders.Clear();
            });

            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options =>
                {
                    string authority = Configuration.GetApiUrl("Authority");
                    options.Authority = !string.IsNullOrEmpty(authority) ? authority : "http://localhost:5000";
                    options.RequireHttpsMetadata = false;
                    options.ApiName = "GHM_Hr_Api";
                    options.ApiSecret = Configuration.GetClientSecret();
                    options.EnableCaching = true;
                    options.CacheDuration = TimeSpan.FromDays(1);
                });

            services.AddDbContext<HrDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("HrConnectionString"));
            });

            // Config Autofac.
            ContainerBuilder container = new ContainerBuilder();
            container.Populate(services);

            container.RegisterModule(new ApplicationModule(Configuration.GetConnectionString("HrConnectionString")));
            container.RegisterModule(new ValidationModule());
            AutofacServiceProvider autofacServiceProvider = new AutofacServiceProvider(container.Build());
            return autofacServiceProvider;
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseAuthentication();
            app.UseDeveloperExceptionPage();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
                app.UseHttpsRedirection();
            }
            #region Localizations
            CultureInfo[] supportedCultures = new[]
            {
                new CultureInfo("vi-VN"),
                new CultureInfo("en"),
                new CultureInfo("en-US"),
            };

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("vi-VN"),
                // Formatting numbers, dates, etc.
                SupportedCultures = supportedCultures,
                // UI strings that we have localized.
                SupportedUICultures = supportedCultures
            });
            #endregion

            #region Allow Origins
            string[] allowOrigins = Configuration.GetSection("AllowOrigins")
                .GetChildren().Select(x => x.Value).ToArray();
            app.UseCors(builder =>
            {
                builder.WithOrigins(allowOrigins);
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
                builder.AllowCredentials();
            });
            #endregion

            //app.UseSwagger();
            //app.UseSwaggerUi3(settings =>
            //{
            //    settings.OAuth2Client = new OAuth2ClientSettings
            //    {
            //        ClientId = "0015e451-965a-4270-9ba5-fd29ed647fe1",
            //        ClientSecret = "@Hoang123456",
            //        AppName = "server.swagger",
            //        Realm = "server.swagger",

            //    };
            //});
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
