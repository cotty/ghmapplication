﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Hr.Api.Controllers
{
    ///<Summary>
    /// Gets the answer
    ///</Summary>
    [Authorize]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/related-user")]
    public class RelatedUserController : GhmControllerBase
    {
        private readonly IRelatedUserService _relatedUserService;

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        public RelatedUserController(IRelatedUserService relatedUserService)
        {
            _relatedUserService = relatedUserService;
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [AcceptVerbs("GET")]
        [AllowPermission(PageId.User, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string keyword, string userId, string valueId, string fullName, 
            string idCardNumber, string phone, int page = 1, int pageSize = 20) 
        {
            var result = await _relatedUserService.Search(CurrentUser.TenantId, keyword, userId, valueId, fullName, idCardNumber, phone, page, pageSize);

            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.User, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]RelatedUserMeta relatedUserMeta)
        {
            var result = await _relatedUserService.Insert(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, relatedUserMeta);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.User, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody]RelatedUserMeta relatedUserMeta)
        {
            var result = await _relatedUserService.Update(CurrentUser.TenantId, id, relatedUserMeta);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.User, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _relatedUserService.Delete(CurrentUser.TenantId, id);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
    }
}