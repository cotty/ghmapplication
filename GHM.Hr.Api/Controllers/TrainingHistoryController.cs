﻿using System;
using System.Threading.Tasks;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Hr.Api.Controllers
{
    /// <summary>
    /// Api Training History
    /// </summary>
    [Authorize]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class TrainingHistoryController : GhmControllerBase
    {
        private readonly ITrainingHistoryService _trainingHistoryService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="trainingHistoryService"></param>
        public TrainingHistoryController(ITrainingHistoryService trainingHistoryService)
        {
            _trainingHistoryService = trainingHistoryService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <param name="courseId"></param>
        /// <param name="courseName"></param>
        /// <param name="coursePlaceId"></param>
        /// <param name="coursePlaceName"></param>
        /// <param name="isHasCertification"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        #region Training History
        [AcceptVerbs("GET")]
        [AllowPermission(PageId.User, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string keyword, string userId, bool? type, string courseId, string courseName, string coursePlaceId, string coursePlaceName,
            bool? isHasCertification, DateTime? fromDate, DateTime? toDate, int page = 1, int pageSize = 20)
        {

           var result = await _trainingHistoryService.Search(CurrentUser.TenantId, keyword, userId, type, courseId, courseName, coursePlaceId, coursePlaceName, isHasCertification, fromDate, toDate, page, pageSize);
            return Ok(result);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="training"></param>
        /// <returns></returns>
        [AcceptVerbs("POST")]
        [AllowPermission(PageId.User, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]TrainingHistoryMeta training)
        {
            var result = await _trainingHistoryService.Insert(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, training);
            if (result.Code < 0)
            {
                return BadRequest(result);
            }
            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="training"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("POST")]    
        [AllowPermission(PageId.User, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody]TrainingHistoryMeta training)
        {
            var result = await _trainingHistoryService.Update(id, CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, training);
            if (result.Code < 0)
            {
                return BadRequest(result);
            }
            return Ok(result);
        }

        /// <summary>
        /// Xoa qua trinh dao tao
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.User, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _trainingHistoryService.Delete(id, CurrentUser.TenantId);
            if (result.Code < 0)
            {
                return BadRequest(result);
            }
            return Ok(result);
        }
        #endregion
    }
}
