﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeTreeViewModel = GHM.Infrastructure.ViewModels.OfficeTreeViewModel;

namespace GHM.Hr.Api.Controllers
{
    ///<Summary>
    /// Gets the answer
    ///</Summary>
    [Authorize]
    [Produces("application/json")]
    [Route("api/v1/offices-positions")]
    public class OfficePositionController : GhmControllerBase
    {
        private readonly IOfficePositionService _officePositionService;

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        public OfficePositionController(IOfficePositionService officePositionService)
        {
            _officePositionService = officePositionService;
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [AcceptVerbs("GET")]
        [AllowPermission(PageId.Office, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string keyword, int officeId, int page = 1, int pageSize = 20)
        {
            var result = await _officePositionService.Search(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name,
                keyword, officeId, page, pageSize);
            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{officeId}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.OfficePosition, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert(int officeId, [FromBody]string[] positionIds)
        {
            var result = await _officePositionService.Inserts(CurrentUser.TenantId, officeId, positionIds);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{officeId}/{positionId}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.OfficePosition, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(int officeId, string positionId)
        {
            var result = await _officePositionService.Delete(CurrentUser.TenantId, officeId, positionId);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }


        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("tree"), AcceptVerbs("GET")]
        [AllowPermission(PageId.OfficePosition, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> GetTree()
        {
            var result = await _officePositionService.GetTree(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }
    }
}