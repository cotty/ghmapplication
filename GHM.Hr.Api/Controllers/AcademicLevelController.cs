﻿using System.Threading.Tasks;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Hr.Api.Controllers
{
    /// <summary>
    /// Api Trình độ bằng cấp
    /// </summary>
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/academic-levels")]    
    public class AcademicLevelController : GhmControllerBase
    {
        private readonly IAcademicLevelService _academicLevelService;
        /// <summary>
        /// Controller trình độ bằng cấp
        /// </summary>
        /// <param name="academicLevelService"></param>
        public AcademicLevelController(IAcademicLevelService academicLevelService)
        {
            _academicLevelService = academicLevelService;
        }

        /// <summary>
        /// Thêm học vấn
        /// </summary>
        /// <param name="academicLevelMeta"></param>
        /// <returns></returns>
        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.User, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody] AcademicLevelMeta academicLevelMeta)
        {
            var result = await _academicLevelService.Insert(CurrentUser.TenantId, academicLevelMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Cập nhật học vấn
        /// </summary>
        /// <param name="id"></param>
        /// <param name="academicLevelMeta"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.User, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody] AcademicLevelMeta academicLevelMeta)
        {
            var result = await _academicLevelService.Update(CurrentUser.TenantId, id, academicLevelMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Xóa học vấn
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.User, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _academicLevelService.Delete(CurrentUser.TenantId, id);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Tìm kiếm
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="levelId"></param>
        /// <param name="degreeId"></param>
        /// <param name="schoolId"></param>
        /// <param name="specializeId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [AcceptVerbs("GET")]
        [AllowPermission(PageId.User, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string userId, string levelId, string degreeId, string schoolId,
            string specializeId, int page, int pageSize)
        {
            return Ok(await _academicLevelService.Search(CurrentUser.TenantId, userId, levelId, degreeId, schoolId, specializeId, page, pageSize));
        }
    }
}
