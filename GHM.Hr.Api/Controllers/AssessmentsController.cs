﻿using System.Drawing;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using GHM.Infrastructure.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace GHM.Hr.Api.Controllers
{
    ///<Summary>
    /// Gets the answer
    ///</Summary>
    [Authorize]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class AssessmentsController : GhmControllerBase
    {
        private readonly IUserAssessmentService _userAssessmentService;
        private readonly IUserAssessmentCriteriaCommentService _userAssessmentCriteriaCommentService;

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        public AssessmentsController(IUserAssessmentService userAssessmentService, IUserAssessmentCriteriaCommentService userAssessmentCriteriaCommentService)
        {
            _userAssessmentService = userAssessmentService;
            _userAssessmentCriteriaCommentService = userAssessmentCriteriaCommentService;
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("register")]
        public async Task<IActionResult> Register(byte month, int year)
        {
            var result = await _userAssessmentService.Register(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name,
                CurrentUser.Id, month, year);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{id}/{userId}")]
        public async Task<IActionResult> GetDetailByUser(string id, string userId, byte month, int year)
        {
            var result = await _userAssessmentService.GetDetail(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, userId,
                CurrentUser.Id, month, year);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }
        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("my-assessments")]
        public async Task<IActionResult> SearchMyAssessment(int year)
        {
            var result = await _userAssessmentService.SearchMyAssessment(CurrentUser.TenantId,
                CultureInfo.CurrentCulture.Name, CurrentUser.Id, year);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }
        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("managers")]
        public async Task<IActionResult> ManagerSearch(string keyword, byte? month, int? year, int page = 1, int pageSize = 20)
        {
            var result = await _userAssessmentService.ManagerSearch(CurrentUser.TenantId,
                CultureInfo.CurrentCulture.Name, CurrentUser.Id, keyword, month, year, page, pageSize);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }
        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("approvers")]
        public async Task<IActionResult> ApproverSearch(string keyword, byte? month, int? year, int page = 1, int pageSize = 20)
        {
            var result = await _userAssessmentService.ApproverSearch(CurrentUser.TenantId,
                CultureInfo.CurrentCulture.Name, CurrentUser.Id, keyword, month, year, page, pageSize);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("results")]
        public async Task<IActionResult> SearchResult(string keyword, AssessmentStatus? status, int? officeId, byte? month,
            int year, int page = 1, int pageSize = 20)
        {
            var result = await _userAssessmentService.SearchResult(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, CurrentUser.Id,
                keyword, status, officeId, month, year, page, pageSize);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{id}/{type}"), ValidateModel]
        public async Task<IActionResult> UpdateCriteriaInfo(string id, AssessmentType type, [FromBody]UserAssessmentCriteria userAssessmentCriteria)
        {
            var result = await _userAssessmentService.UpdateUserAssessmentCriteria(CurrentUser.TenantId, CurrentUser.Id,
                id, type, userAssessmentCriteria);
            if (result.Code < 0)
                return BadRequest(result);
            return Ok(result);
        }
        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        public async Task<IActionResult> UpdateStatus(string id, [FromBody]UserAssessmentMeta userAssessmentMeta)
        {
            var result = await _userAssessmentService.UpdateStatus(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar,
                id, userAssessmentMeta);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }
        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{id}"), AcceptVerbs("GET")]
        public async Task<IActionResult> GetDetail(string id)
        {
            var result = await _userAssessmentService.GetDetail(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name,
                CurrentUser.Id, id);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }
        ///<Summary>
        /// Gets the answer
        ///</Summary>
        #region Export
        [Route("export-result"), AcceptVerbs("GET")]
        [AllowPermission(PageId.User, Permission.Export)]
        [CheckPermission]
        public async Task<IActionResult> ExportResult(string keyword, AssessmentStatus? status, int? officeId, byte? month,
            int year)
        {
            var result = await _userAssessmentService.SearchResult(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, CurrentUser.Id,
                 keyword, status, officeId, month, year, 1, int.MaxValue);

            using (var xls = new ExcelPackage())
            {
                var sheet = xls.Workbook.Worksheets.Add("Sheet1");
                var colorTitle = ColorTranslator.FromHtml("#003300");
                var colorHeader = ColorTranslator.FromHtml("#3598dc");
                var colorWhite = ColorTranslator.FromHtml("#ffffff");

                ExcelHelper.CreateCellTable(sheet, 2, 1, 2, 9, "Tổng hợp kết quả đánh giá", new CustomExcelStyle { IsBold = true, FontSize = 14, IsMerge = true, HorizontalAlign = ExcelHorizontalAlignment.Center });

                ExcelHelper.CreateCellTable(sheet, 4, 1, 5, 1, "Tháng", new CustomExcelStyle {HorizontalAlign = ExcelHorizontalAlignment.Center, IsMerge = true, IsBold = true, BackgroundColor = colorHeader, Color = colorWhite, Border = ExcelBorderStyle.Thin });
                ExcelHelper.CreateCellTable(sheet, 4, 2, 5,2 , "Họ và tên", new CustomExcelStyle { HorizontalAlign = ExcelHorizontalAlignment.Center, IsMerge = true, IsBold = true, BackgroundColor = colorHeader, Color = colorWhite, Border = ExcelBorderStyle.Thin });
                ExcelHelper.CreateCellTable(sheet, 4, 3, 5, 3, "Phòng ban", new CustomExcelStyle { HorizontalAlign = ExcelHorizontalAlignment.Center, IsMerge = true, IsBold = true, BackgroundColor = colorHeader, Color = colorWhite, Border = ExcelBorderStyle.Thin });
                ExcelHelper.CreateCellTable(sheet, 4, 4, 5, 4, "Chức danh", new CustomExcelStyle { HorizontalAlign = ExcelHorizontalAlignment.Center, IsMerge = true, IsBold = true, BackgroundColor = colorHeader, Color = colorWhite, Border = ExcelBorderStyle.Thin });
                ExcelHelper.CreateCellTable(sheet, 4, 5, 5, 5, "Trạng thái", new CustomExcelStyle { HorizontalAlign = ExcelHorizontalAlignment.Center, IsMerge = true, IsBold = true, BackgroundColor = colorHeader, Color = colorWhite, Border = ExcelBorderStyle.Thin });
                ExcelHelper.CreateCellTable(sheet, 4, 6, 4, 9, "Điểm", new CustomExcelStyle { HorizontalAlign = ExcelHorizontalAlignment.Center, IsMerge = true, IsBold = true, BackgroundColor = colorHeader, Color = colorWhite, Border = ExcelBorderStyle.Thin });
                ExcelHelper.CreateCellTable(sheet, 5, 6, "Chuẩn", new CustomExcelStyle { HorizontalAlign = ExcelHorizontalAlignment.Center, IsMerge = true, IsBold = true, BackgroundColor = colorHeader, Color = colorWhite, Border = ExcelBorderStyle.Thin });
                ExcelHelper.CreateCellTable(sheet, 5, 7, "Tự đánh giá", new CustomExcelStyle { HorizontalAlign = ExcelHorizontalAlignment.Center, IsMerge = true, IsBold = true, BackgroundColor = colorHeader, Color = colorWhite, Border = ExcelBorderStyle.Thin });
                ExcelHelper.CreateCellTable(sheet, 5, 8, "QLTT", new CustomExcelStyle { HorizontalAlign = ExcelHorizontalAlignment.Center, IsMerge = true, IsBold = true, BackgroundColor = colorHeader, Color = colorWhite, Border = ExcelBorderStyle.Thin });
                ExcelHelper.CreateCellTable(sheet, 5, 9, "QLPD", new CustomExcelStyle { HorizontalAlign = ExcelHorizontalAlignment.Center, IsMerge = true, IsBold = true, BackgroundColor = colorHeader, Color = colorWhite, Border = ExcelBorderStyle.Thin });

                sheet.View.FreezePanes(6, 1);
                var rowIndex = 6;
                foreach (var user in result.Items)
                {
                    ExcelHelper.CreateCellTable(sheet, "A" + rowIndex, user.Month, ExcelHorizontalAlignment.Center, false);
                    ExcelHelper.CreateCellTable(sheet, "B" + rowIndex, user.FullName, ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "C" + rowIndex, user.OfficeName, ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "D" + rowIndex, user.PositionName, ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "E" + rowIndex, GetAssessmentStatusName(user.Status), ExcelHorizontalAlignment.Center, false, true);
                    ExcelHelper.CreateCellTable(sheet, "F" + rowIndex, user.TotalPoint, ExcelHorizontalAlignment.Right, false, true);
                    ExcelHelper.CreateCellTable(sheet, "G" + rowIndex, user.TotalUserPoint, ExcelHorizontalAlignment.Right, false, true);
                    ExcelHelper.CreateCellTable(sheet, "H" + rowIndex, user.TotalManagerPoint, ExcelHorizontalAlignment.Right, false, true);
                    ExcelHelper.CreateCellTable(sheet, "I" + rowIndex, user.TotalApproverPoint, ExcelHorizontalAlignment.Right, false, true);                                    
                    rowIndex++;
                }

                sheet.Cells.Style.WrapText = true;
                sheet.Column(2).Width = 20;
                sheet.Column(3).Width = 25;
                sheet.Column(4).Width = 25;
                sheet.Row(4).Height = 25;
                sheet.Row(5).Height = 25;
                sheet.Cells.Style.Font.Size = 11;
                sheet.Cells.Style.Font.Name = "Times New Roman";
                var stream = new MemoryStream(xls.GetAsByteArray())
                {
                    // Reset Stream Position
                    Position = 0
                };

                var fileName = "Tổng hợp đánh giá thành tích.xlsx";
                var fileType = "application/vnd.ms-excel";
                return File(stream, fileType, fileName);
            }
        }
        #endregion
        ///<Summary>
        /// Gets the answer
        ///</Summary>
        #region User assessment criteria comments.
        [Route("criterias/comments"), AcceptVerbs("POST")]
        public async Task<IActionResult> InsertComment(string userAssessmentCriteriaId,
            [FromBody]UserAssessmentCriteriaCommentMeta userAssessmentCriteriaCommentMeta)
        {
            userAssessmentCriteriaCommentMeta.UserId = CurrentUser.Id;
            userAssessmentCriteriaCommentMeta.FullName = CurrentUser.FullName;
            userAssessmentCriteriaCommentMeta.Avatar = CurrentUser.Avatar;
            var result = await _userAssessmentCriteriaCommentService.Insert(CurrentUser.TenantId, userAssessmentCriteriaCommentMeta);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("criterias/comments/{id}"), AcceptVerbs("POST")]
        public async Task<IActionResult> UpdateComment(string id,
            [FromBody]UserAssessmentCriteriaCommentMeta userAssessmentCriteriaCommentMeta)
        {
            userAssessmentCriteriaCommentMeta.UserId = CurrentUser.Id;
            var result = await _userAssessmentCriteriaCommentService.Update(id, userAssessmentCriteriaCommentMeta);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("criterias/comments/{id}"), AcceptVerbs("DELETE")]
        public async Task<IActionResult> DeleteComment(string id)
        {
            var result = await _userAssessmentCriteriaCommentService.Delete(CurrentUser.Id, id);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("criterias/{userAssessmentCriteriaId}/comments"), AcceptVerbs("GET")]
        public async Task<IActionResult> SearchComments(string userAssessmentCriteriaId, int page = 1, int pageSize = 10)
        {
            var result = await _userAssessmentCriteriaCommentService.Search(CurrentUser.TenantId, CurrentUser.Id,
                userAssessmentCriteriaId, page, pageSize);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("request-reassessment/{positionId}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.CriteriaConfig, Permission.Insert, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> RequestReassessment(string positionId, byte month, int year, bool applyForAll)
        {
            var result = await _userAssessmentService.RequestReassessment(CurrentUser.TenantId, positionId, month, year, applyForAll);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }
        #endregion

        private string GetAssessmentStatusName( AssessmentStatus assessmentStatus)
        {
            switch (assessmentStatus)
            {
                case AssessmentStatus.New:
                return "Mới";
                case AssessmentStatus.ApproverDecline:
                    return "QLPD không duyệt";
                case AssessmentStatus.ApproverApprove:
                    return "QLPD đã duyệt";
                case AssessmentStatus.WaitingManagerApprove:
                    return "Chờ QLTT duyệt";
                case AssessmentStatus.ManagerApproveWaitingApproverApprove:
                    return "QLTT duyệt chờ QLPD duyệt";
                case AssessmentStatus.ManagerDecline:
                    return "QLTT không duyệt";
                case AssessmentStatus.Cancel:
                    return "Đã hủy";
                case AssessmentStatus.ManagerApproved:
                    return "QLTT đã duyệt";
                default:
                    return "Không xác định";
            }
        }
    }

}