﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Hr.Api.Controllers
{
    /// <summary>
    /// Api OfficeController
    /// </summary>
    [Authorize]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class OfficesController : GhmControllerBase
    {
        private readonly IOfficeService _officeService;
        private readonly IOfficeRepository _officeRepository;
        /// <summary>
        /// Controller Office
        /// </summary>
        /// <param name="officeService"></param>
        /// <param name="officeRepository"></param>
        public OfficesController(IOfficeService officeService, IOfficeRepository officeRepository)
        {
            _officeService = officeService;
            _officeRepository = officeRepository;
        }

        /// <summary>
        /// Check if current user is admin. Allow get all office else just get office belong to current user RootId
        /// </summary>
        /// <param name="keyword">Từ khóa tìm kiếm</param>
        /// <param name="isActive">Đã kích hoạt chưa</param>
        /// <param name="page">Trang hiện tại</param>
        /// <param name="pageSize">Số bản ghi trên trang</param>
        /// <returns></returns>
        [AcceptVerbs("GET")]
        [AllowPermission(PageId.Office, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string keyword, bool? isActive, int page = 1, int pageSize = 20)
        {
            return Ok(await _officeService.Search(CurrentUser.Id, CurrentUser.TenantId, CultureInfo.CurrentCulture.Name,
                keyword, isActive, page, pageSize));
        }       

        /// <summary>
        /// Thêm office
        /// </summary>
        /// <param name="officeMeta"></param>
        /// <returns></returns>
        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.Office, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]OfficeMeta officeMeta)
        {
            var result = await _officeService.Insert(CurrentUser.TenantId, officeMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Cập nhật office
        /// </summary>
        /// <param name="id"></param>
        /// <param name="officeMeta"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.Office, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(int id, [FromBody]OfficeMeta officeMeta)
        {
            var result = await _officeService.Update(CurrentUser.TenantId, id, officeMeta);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        /// <summary>
        /// Lấy chi tiết đê xem
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.Office, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Detail(int id)
        {
            var result = await _officeService.GetDetail(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, id);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Lấy chi tiết để sửa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("edit/{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.Office, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Edit(int id)
        {
            var result = await _officeService.GetDetailForEdit(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, id);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Xóa office
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.Office, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _officeService.Delete(CurrentUser.TenantId, id);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        /// <summary>
        /// Lấy danh sách officeTree
        /// </summary>
        /// <returns></returns>
        [Route("trees"), AcceptVerbs("GET")]
        [AllowPermission(PageId.AssessmentResult, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetOfficeTree()
        {
            return Ok(await _officeService.GetFullOfficeTree(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name));
        }

        /// <summary>
        /// Lấy danh sách office tree
        /// </summary>
        /// <returns></returns>
        [Route("user-trees"), AcceptVerbs("GET")]
        [CheckPermission]
        public async Task<IActionResult> GetOfficeUserTree()
        {
            var officeUserTree = await _officeService.GetOfficeUserTree(CurrentUser.TenantId, CurrentUser.Id, CultureInfo.CurrentCulture.Name);

            return Ok(officeUserTree);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        //[Route("get-office-tree-lazy"), AcceptVerbs("GET")]
        //public async Task<IActionResult> GetOfficeUserTree(int? parentId)
        //{
        //    var officeUserTree = await _officeRepository.GetOfficeUserTree(parentId);
        //    return Ok(GenerateOfficeUserTree(officeUserTree, parentId));
        //    return Ok(1);
        //}

        /// <summary>
        /// Hàm tìm kiếm nhanh
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Route("suggestions"), AcceptVerbs("GET")]
        [CheckPermission]
        public async Task<IActionResult> SearchForSuggestion(string keyword, int page = 1, int pageSize = 20)
        {
            return Ok(await _officeService.SearchForSuggestion(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, keyword, page, pageSize));
        }

        #region Office Contacts.
        /// <summary>
        /// Thêm contact
        /// </summary>
        /// <param name="officeId"></param>
        /// <param name="officeContactMeta"></param>
        /// <returns></returns>
        [Route("{officeId}/contacts"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.Office, Permission.Insert, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> AddContact(int officeId, [FromBody]OfficeContactMeta officeContactMeta)
        {
            var result = await _officeService.InsertContact(CurrentUser.TenantId, officeId, officeContactMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="officeId"></param>
        /// <param name="contactId"></param>
        /// <param name="officeContactMeta"></param>
        /// <returns></returns>
        [Route("{officeId}/contacts/{contactId}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.Office, Permission.Insert, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateContact(int officeId, string contactId, [FromBody]OfficeContactMeta officeContactMeta)
        {
            var result = await _officeService.UpdateContact(CurrentUser.TenantId, officeId, contactId, officeContactMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="officeId"></param>
        /// <param name="contactId"></param>
        /// <returns></returns>
        [Route("{officeId}/contacts/{contactId}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.Office, Permission.Insert, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> DeleteContact(int officeId, string contactId)
        {
            var result = await _officeService.DeleteContact(CurrentUser.TenantId, officeId, contactId);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }
        #endregion

        #region Internal Service Call.
        /// <summary>
        /// Lấy thông tin Office
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize("server")]
        [Route("info/{id}"), AcceptVerbs("GET")]
        public async Task<IActionResult> OfficeInfo(int id)
        {
            var officeInfo = await _officeRepository.GetInfoByLanguage(id, CultureInfo.CurrentCulture.Name);
            if (officeInfo == null)
                return BadRequest(null);

            return Ok(officeInfo);
        }

        /// <summary>
        /// Lấy thông tin tóm tắt
        /// </summary>
        /// <param name="tenantId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize("server")]
        [Route("{tenantId}/office-short-info/{id}"), AcceptVerbs("GET")]
        public async Task<IActionResult> GetShortInfo(string tenantId, int id)
        {
            var officeInfo = await _officeRepository.GetSortInfo(tenantId, CultureInfo.CurrentCulture.Name, id);
            if (officeInfo == null)
                return BadRequest(null);

            return Ok(officeInfo);
        }
        #endregion
    }
}