﻿using System.Globalization;
using System.Threading.Tasks;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Hr.Api.Controllers
{
    /// <summary>
    /// Api tiêu chí
    /// </summary>
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class CriteriasController : GhmControllerBase
    {
        private readonly ICriteriaService _criteriaService;
        /// <summary>
        /// Controller tiêu chí
        /// </summary>
        /// <param name="criteriaService"></param>
        public CriteriasController(ICriteriaService criteriaService)
        {
            _criteriaService = criteriaService;
        }

        #region Criteria
        /// <summary>
        /// Api tìm kiếm thư viện tiêu chí
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="groupId"></param>
        /// <param name="isActive"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [AcceptVerbs("GET")]
        [AllowPermission(PageId.CriteriaLibrary, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string keyword, string groupId, bool? isActive, int page = 1, int pageSize = 20)
        {
            var result =
                await _criteriaService.Search(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, keyword, groupId, isActive, page, pageSize);
            return Ok(result);
        }

        /// <summary>
        /// Tìm kiếm thư viện tiêu chí đã active
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="groupId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Route("search"), AcceptVerbs("GET")]
        [AllowPermission(PageId.CriteriaConfig, Permission.Insert, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> SearchActivated(string keyword, string groupId, int page = 1, int pageSize = 20)
        {
            var result =
                await _criteriaService.Search(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, keyword, groupId, true, page, pageSize);
            return Ok(result);
        }

        /// <summary>
        /// Thêm mới thư viện tiêu chí
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.CriteriaLibrary, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]CriteriaMeta criteria)
        {
            criteria.TenantId = CurrentUser.TenantId;
            var result =
                await _criteriaService.Insert(criteria);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Cập nhật thư viện tiêu chí
        /// </summary>
        /// <param name="id"></param>
        /// <param name="criteriaMeta"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.CriteriaLibrary, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody]CriteriaMeta criteriaMeta)
        {
            criteriaMeta.TenantId = CurrentUser.TenantId;
            var result =
                await _criteriaService.Update(id, criteriaMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Xóa thư viện tiêu chí
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.CriteriaLibrary, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string id)
        {
            var result =
                await _criteriaService.Delete(CurrentUser.TenantId, id);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Laauys chi tiết thư viện tiêu chí
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.CriteriaLibrary, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetDetail(string id)
        {
            var result =
                await _criteriaService.GetDetail(CurrentUser.TenantId, id);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }
        #endregion

        #region Criteria groups
        /// <summary>
        /// Tìm kiếm nhóm thư viện
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="isActive"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Route("groups"), AcceptVerbs("GET")]
        [AllowPermission(PageId.CriteriaGroup, Permission.View)]
        public async Task<IActionResult> SearchGroup(string keyword, bool? isActive, int page = 1, int pageSize = 20)
        {
            var result =
                await _criteriaService.SearchGroup(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, keyword, isActive, page, pageSize);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Thêm nhóm thư viện
        /// </summary>
        /// <param name="criteriaGroupMeta"></param>
        /// <returns></returns>
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Route("groups"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.CriteriaGroup, Permission.Insert)]
        public async Task<IActionResult> InsertGroup([FromBody]CriteriaGroupMeta criteriaGroupMeta)
        {
            criteriaGroupMeta.TenantId = CurrentUser.TenantId;
            criteriaGroupMeta.UserId = CurrentUser.Id;
            criteriaGroupMeta.FullName = CurrentUser.FullName;
            var result =
                await _criteriaService.InsertGroup(criteriaGroupMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Cập nhật nhóm thư viện
        /// </summary>
        /// <param name="id"></param>
        /// <param name="criteriaGroupMeta"></param>
        /// <returns></returns>
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Route("groups/{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.CriteriaGroup, Permission.Update)]
        public async Task<IActionResult> UpdateGroup(string id, [FromBody]CriteriaGroupMeta criteriaGroupMeta)
        {
            criteriaGroupMeta.TenantId = CurrentUser.TenantId;
            criteriaGroupMeta.UserId = CurrentUser.Id;
            criteriaGroupMeta.FullName = CurrentUser.FullName;
            var result =
                await _criteriaService.UpdateGroup(id, criteriaGroupMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Xóa nhóm thư viện
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Route("groups/{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.CriteriaGroup, Permission.Delete)]
        public async Task<IActionResult> DeleteGroup(string id)
        {
            var result =
                await _criteriaService.DeleteGroup(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, id);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Xem chi tiết nhóm thư viện tiêu chí
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Route("groups/{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.CriteriaGroup, Permission.View)]
        public async Task<IActionResult> GetGroupDetail(string id)
        {
            var result =
                await _criteriaService.GetGroupDetail(CurrentUser.TenantId, id);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Tìm kiếm nhóm thư viện tiêu chí
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Route("groups/suggestions"), AcceptVerbs("GET")]
        public async Task<IActionResult> GetsAllGroups(string id)
        {
            var result =
                await _criteriaService.GetsAllCriteriaGroup(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }
        #endregion
    }
}