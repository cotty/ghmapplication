﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Hr.Api.Controllers
{
    /// <summary>
    /// Api User value
    /// </summary>
    [Authorize]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/user-values")]
    public class UserValueController : GhmControllerBase
    {
        private readonly IUserValueService _userValueService;
        /// <summary>
        /// Controller UserValue
        /// </summary>
        /// <param name="userValueService"></param>
        public UserValueController(IUserValueService userValueService)
        {
            _userValueService = userValueService;
        }

        /// <summary>
        /// Lấy giá userValue
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="type"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Route("{type}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.User, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string keyword, UserValueType type, int page = 1, int pageSize = 20)
        {
            return Ok(await _userValueService.Search(CurrentUser.TenantId, keyword, type, page, pageSize));
        }

        /// <summary>
        /// Thêm userValue
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.User, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]UserValueMeta userValueMeta)
        {
            var result = await _userValueService.Insert(CurrentUser.TenantId, userValueMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Cập nhật userValue
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userValueMeta"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.User, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody]UserValueMeta userValueMeta)
        {
            var result = await _userValueService.Update(CurrentUser.TenantId, id, userValueMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Xóa user value
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.User, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _userValueService.Delete(CurrentUser.TenantId, id);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }
    }
}
