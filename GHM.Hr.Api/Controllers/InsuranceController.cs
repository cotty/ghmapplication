﻿using System;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.CustomAttributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Hr.Api.Controllers
{
    //[Authorize]
    //[Produces("application/json")]
    //[Route("api/[controller]")]
    //public class InsuranceController : GhmControllerBase
    //{
    //    private readonly IInsuranceRepository _insuranceRepository;
    //    private readonly IUserValueRepository _userValueRepository;

    //    public InsuranceController(IInsuranceRepository insuranceRepository, IUserValueRepository userValueRepository)
    //    {
    //        _insuranceRepository = insuranceRepository;
    //        _userValueRepository = userValueRepository;
    //    }

    //    [Route("insert"), AcceptVerbs("POST"), ValidateModel]
    //    //[CheckPermission(new[] { Permission.Insert }, PageId.Insurance)]
    //    public async Task<IActionResult> Insert(InsuranceMeta insurance)
    //    {
    //        var result = await _insuranceRepository.Insert(insurance);
    //        if (result > 0)
    //        {
    //            // TODO: Insert log
    //        }
    //        return Ok(result);
    //    }

    //    [Route("update"), AcceptVerbs("POST"), ValidateModel]
    //    //[CheckPermission(new[] { Permission.Update }, PageId.Insurance)]
    //    public async Task<IActionResult> Update(InsuranceMeta insurance)
    //    {
    //        var result = await _insuranceRepository.Update(insurance);
    //        if (result > 0)
    //        {
    //            // TODO: Insert log
    //        }
    //        return Ok(result);
    //    }

    //    [Route("delete"), AcceptVerbs("GET"), ValidateModel]
    //    //[CheckPermission(new[] { Permission.Delete }, PageId.Insurance)]
    //    public async Task<IActionResult> Delete(int id)
    //    {
    //        var result = await _insuranceRepository.Delete(id);
    //        if (result > 0)
    //        {
    //            // TODO: Insert log
    //        }
    //        return Ok(result);
    //    }

    //    [Route("search"), AcceptVerbs("GET")]
    //    //[CheckPermission(new[] { Permission.View }, PageId.Insurance)]
    //    public async Task<IActionResult> Search(string keyword, string userId, bool? type, DateTime? fromDate, DateTime? toDate, int page = 1, int pageSize = 20)
    //    {
    //        return Ok(new
    //        {
    //            items = await _insuranceRepository.Search(keyword, userId, type, fromDate, toDate, page, pageSize, out var totalRows),
    //            totalRows
    //        });
    //    }

    //    [Route("search-company"), AcceptVerbs("GET")]
    //    //[CheckPermission(new[] { Permission.Insert, Permission.Update, Permission.View }, PageId.Insurance)]
    //    public async Task<IActionResult> SearchCompany(string keyword)
    //    {
    //        return Ok(await _userValueRepository.Search(keyword, UserValueType.Insurance));
    //    }
    //}
}
