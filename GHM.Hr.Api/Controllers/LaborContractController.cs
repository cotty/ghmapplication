﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Hr.Api.Controllers
{
    /// <summary>
    /// Api hợp đồng lao động
    /// </summary>
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/labor-contracts")]
    public class LaborContractController : GhmControllerBase
    {
        private readonly ILaborContactService _laborContactService;

        /// <summary>
        /// Quản lý mẫu hợp đồng lao động
        /// </summary>
        /// <param name="laborContactService"></param>
        public LaborContractController(ILaborContactService laborContactService)
        {
            _laborContactService = laborContactService;
    }

        #region Hợp đồng lao động
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tenantId"></param>
        /// <param name="languageId"></param>
        /// <param name="keyword"></param>
        /// <param name="userId"></param>
        /// <param name="officeId"></param>
        /// <param name="titleId"></param>
        /// <param name="positionId"></param>
        /// <param name="type"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="isUse"></param>
        /// <param name="month"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Route(""), AcceptVerbs("GET")]
        [AllowPermission(PageId.LaborContract, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string tenantId, string languageId, string keyword, string userId,
            int? officeId, string titleId, string positionId, string type,
            DateTime? fromDate, DateTime? toDate, bool? isUse, byte? month, int page = 1, int pageSize = 20)
        {

            var result = await _laborContactService.Search(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, keyword, userId,
                officeId, titleId, positionId, type,
                fromDate, toDate, isUse, month, page, pageSize);
            return Ok(result);
        }       

        /// <summary>
        /// Thêm mới hợp đông lao động
        /// </summary>
        /// <param name="laborContractMeta"></param>
        /// <returns></returns>
        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.LaborContract, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody] LaborContractMeta laborContractMeta)
        {
            var result = await _laborContactService.Insert(CurrentUser.TenantId, CurrentUser.Id,
                CultureInfo.CurrentCulture.Name, laborContractMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Cập nhật hợp đồng lao động
        /// </summary>
        /// <param name="id"></param>
        /// <param name="laborContractMeta"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.LaborContract, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody]LaborContractMeta laborContractMeta)
        {
            var result = await _laborContactService.Update(CurrentUser.TenantId, id, CultureInfo.CurrentCulture.Name, laborContractMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Xóa mẫu hợp đồng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.LaborContract, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _laborContactService.Delete(CurrentUser.TenantId, id);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.LaborContractorForm, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetDetailUserLaborContract(string id)
        {
            var result = await _laborContactService.GetLaborContractDetail(CurrentUser.TenantId, id);
            return Ok(result);
        }
        #endregion

        #region Mẫu hợp đồng lao động
        /// <summary>
        /// Thêm mới mẫu hợp đồng
        /// </summary>
        /// <param name="laborContractFormMeta"></param>
        /// <returns></returns>
        [Route("insert-form"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.LaborContractorForm, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> InsertLaborContractForm([FromBody] LaborContractFormMeta laborContractFormMeta)
        {
            var result = await _laborContactService.InsertLaborContractForm(CurrentUser.TenantId, laborContractFormMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Cập nhật mẫu hợp đồng
        /// </summary>
        /// <param name="id"></param>
        /// <param name="laborContractFormMeta"></param>
        /// <returns></returns>
        [Route("update-form/{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.LaborContractorForm, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody]LaborContractFormMeta laborContractFormMeta)
        {
            var result = await _laborContactService.UpdateLaborContractForm(CurrentUser.TenantId, id, laborContractFormMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Xóa mẫu hợp đồng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("delete-form/{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.LaborContractorForm, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> DeleteLaborContractForm(string id)
        {
            var result = await _laborContactService.DeleteLaborContractForm(CurrentUser.TenantId, id);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Tìm kiếm mẫu hợp đồng
        /// </summary>
        /// <param name="keyword"></param>       
        /// <param name="type"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Route("search-form"), AcceptVerbs("GET")]
        [AllowPermission(PageId.LaborContractorForm, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> SearchForm(string keyword, string type, int page = 1, int pageSize = 20)
        {
            var result = await _laborContactService.SearchLaborContractForm(CurrentUser.TenantId, keyword, type, page, pageSize);
            return Ok(result);
        }

        /// <summary>
        /// Láy chi tiết mẫu hợp đồng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("get-form/{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.LaborContractorForm, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetDetailContractForm(string id)
        {
            var result = await _laborContactService.GetLaborContractFormDetail(CurrentUser.TenantId, id);
            return Ok(result);
        }

        /// <summary>
        /// Lấy mẫu hợp đồng theo loại hợp đồng
        /// </summary>
        /// <param name="type"></param>
        /// <param name="keyword"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>        
        [Route("get-by-type/{type?}"), AcceptVerbs("GET")]
        [CheckPermission]
        public async Task<IActionResult> GetLaborContractFormSuggestionByType(string type, string keyword,int page = 1, int pageSize = 20)
        {
            var result = await _laborContactService.GetLaborContractFormByType(CurrentUser.TenantId, keyword, type, page, pageSize);
            return Ok(result);
        }
        #endregion
    }
}
