﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Hr.Api.Controllers
{
    ///<Summary>
    /// Gets the answer
    ///</Summary>
    [Authorize]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class CommendationDisciplineCategoryController : GhmControllerBase
    {
        private readonly IComendationDisciplineCategoryService _comendationDisciplineCategoryService;
        ///<Summary>
        /// Gets the answer
        ///</Summary>
        public CommendationDisciplineCategoryController(IComendationDisciplineCategoryService comendationDisciplineCategoryService)
        {
            _comendationDisciplineCategoryService = comendationDisciplineCategoryService;
        }
        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [AcceptVerbs("GET")]
        [AllowPermission(PageId.User, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> SearchAsync(string keyword, string name, CommendationDisciplineType type, int page = 1, int pageSize = 20)
        {
            var result = await _comendationDisciplineCategoryService.Search(CurrentUser.TenantId, keyword, name, type, page, pageSize);

            return Ok(result);
        }
        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [AcceptVerbs("POST")]
        [AllowPermission(PageId.User, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]CommendationDisciplineCategoryMeta commendationDisciplineCatoryMeta)
        {
            var result = await _comendationDisciplineCategoryService.InsertAsync(CurrentUser.TenantId, commendationDisciplineCatoryMeta);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{id}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.User, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, CommendationDisciplineCategoryMeta commendationDisciplineCategoryMeta)
        {
            var result = await _comendationDisciplineCategoryService.UpdateAsync(CurrentUser.TenantId, id, commendationDisciplineCategoryMeta);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.User, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _comendationDisciplineCategoryService.Delete(CurrentUser.TenantId, id);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
    }
}