﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Hr.Api.Controllers
{
    ///<Summary>
    /// Gets the answer
    ///</Summary>
    [Authorize]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class UserRecordsManagementController : GhmControllerBase
    {
        private readonly IUserRecordsManagementService _userRecordsManagementService;

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        public UserRecordsManagementController(IUserRecordsManagementService userRecordsManagementService)
        {
            _userRecordsManagementService = userRecordsManagementService;
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.User, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]UserRecordsManagementMeta userRecordsManagementMeta)
        {
            var result = await _userRecordsManagementService.Insert(CurrentUser.TenantId, userRecordsManagementMeta);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [AcceptVerbs("GET")]
        [AllowPermission(PageId.User, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string userId,string valueId, string Title, DateTime? fromDate, DateTime? toDate, int page = 1, int pageSize = 20)
        {
            var result = await _userRecordsManagementService.Search(CurrentUser.TenantId, userId, valueId, Title, fromDate, toDate, page = 1, pageSize = 20);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{id}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.User, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody]UserRecordsManagementMeta userRecordsManagementMeta)
        {
            var result = await _userRecordsManagementService.Update(CurrentUser.TenantId, id, userRecordsManagementMeta);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.User, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _userRecordsManagementService.Delete(CurrentUser.TenantId, id);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
    }
}

