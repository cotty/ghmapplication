﻿using System.Globalization;
using System.Threading.Tasks;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Hr.Api.Controllers
{
    ///<Summary>
    /// Gets the answer
    ///</Summary>
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class TitlesController : GhmControllerBase
    {
        private readonly ITitleService _titleService;

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        public TitlesController(ITitleService titleService)
        {
            _titleService = titleService;
        }

        /// <summary>
        /// Thêm mới chức danh
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.Title, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]TitleMeta title)
        {
            var result = await _titleService.Insert(CurrentUser.TenantId, title);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }


        /// <summary>
        /// Cập nhật chức danh
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.Title, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody]TitleMeta title)
        {
            var result = await _titleService.Update(CurrentUser.TenantId, id, title);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        /// <summary>
        /// Xóa chức danh
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.Title, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _titleService.Delete(CurrentUser.TenantId, id);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Tìm kiếm chức danh
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="isActive"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [AcceptVerbs("GET")]
        [AllowPermission(PageId.Title, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string keyword, bool? isActive, int page = 1, int pageSize = 20)
        {
            var result = await _titleService.Search(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, keyword,
                isActive, page, pageSize);

            if (result.Code < 0)
                return BadRequest(result);
            return Ok(result);
        }

        /// <summary>
        /// Lấy danh sách chức danh hoạt động
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        [Route("activated"), AcceptVerbs("GET")]
        [AllowPermission(PageId.Title, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetAllActivated(string keyword)
        {
            return Ok(await _titleService.SearchForSelect(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name,
                keyword));
        }

        /// <summary>
        /// Lấy chi tiết chức danh
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.Title, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetDetail(string id)
        {
            return Ok(await _titleService.GetDetail(CurrentUser.TenantId, id));
        }
    }
}