﻿using System.Globalization;
using System.Threading.Tasks;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Hr.Api.Controllers
{
    ///<Summary>
    /// Gets the answer
    ///</Summary>
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]s")]
    public class PositionController : GhmControllerBase
    {
        private readonly IPositionService _positionService;

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        public PositionController(IPositionService positionService)
        {
            _positionService = positionService;
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [AcceptVerbs("GET")]
        [AllowPermission(PageId.Position, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string keyword, bool? isManager, bool? isMultiple,
            bool? isActive, int page = 1, int pageSize = 20)
        {
            var result = await _positionService.Search(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, keyword,
                isActive, isManager, isMultiple, page, pageSize);
            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("suggestions"), AcceptVerbs("GET")]
        [AllowPermission(PageId.Position, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> SearchForSuggestion(string keyword, int page = 1, int pageSize = 20)
        {
            return Ok(await _positionService.SearchForSelect(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, keyword));
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("activated"), AcceptVerbs("GET")]
        public async Task<IActionResult> GetAllActivated(string keyword)
        {
            return Ok(await _positionService.SearchForSelect(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name,
                keyword));
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.Position, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]PositionMeta position)
        {
            var result = await _positionService.Insert(CurrentUser.TenantId, position);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.Position, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody]PositionMeta position)
        {
            var result = await _positionService.Update(CurrentUser.TenantId, id, position);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.Position, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _positionService.Delete(id, CurrentUser.TenantId);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.Position, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Detail(string id)
        {
            var result = await _positionService.GetDetail(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, id);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("titles/{id}"), AcceptVerbs("GET")]
        public async Task<IActionResult> GetTitleByPositionId(string id)
        {
            var result = await _positionService.GetTitleByPositionId(id, CultureInfo.CurrentCulture.Name, CurrentUser.TenantId);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
    }
}