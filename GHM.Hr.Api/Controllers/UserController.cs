﻿using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using GHM.Infrastructure.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace GHM.Hr.Api.Controllers
{
    /// <summary>
    /// Api User
    /// </summary>
    [Authorize]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class UsersController : GhmControllerBase
    {
        private readonly IUserService _userService;
        private readonly IUserContactService _userContactServce;
        /// <summary>
        /// UserContoller
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="userContactService"></param>
        public UsersController(IUserService userService, IUserContactService userContactService)
        {
            _userService = userService;
            _userContactServce = userContactService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="leaderId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Route("verification-leader/{leaderId}/{userId}"), AcceptVerbs("GET")]
        public async Task<IActionResult> VerficationLeader(string leaderId, string userId)
        {
            return Ok(await _userService.VerifyLeader(leaderId, userId));
        }

        /// <summary>
        /// Check xem có phải là leader
        /// </summary>
        /// <param name="leaderId"></param>
        /// <param name="officeId"></param>
        /// <returns></returns>
        [Route("verification-leader/{leaderId}/{officeId}"), AcceptVerbs("GET")]
        public async Task<IActionResult> VerficationLeader(string leaderId, int officeId)
        {
            return Ok(await _userService.VerifyLeader(leaderId, officeId));
        }

        /// <summary>
        /// Tìm kiếm thông tin nhân viên
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="status"></param>
        /// <param name="officeIdPath"></param>
        /// <param name="positionId"></param>
        /// <param name="gender"></param>
        /// <param name="monthBirthDay"></param>
        /// <param name="yearBirthday"></param>
        /// <param name="academicRank"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [AcceptVerbs("GET")]
        [AllowPermission(PageId.User, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string keyword, UserStatus? status, string officeIdPath, string positionId,
            Gender? gender, byte? monthBirthDay, int? yearBirthday, AcademicRank? academicRank, int page = 1, int pageSize = 20)
        {
            var result = await _userService.Search(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, keyword, status,
               officeIdPath, positionId, gender, monthBirthDay, yearBirthday, academicRank, page, pageSize);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Lấy thông tin suggestion
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="officeId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Route("suggestions"), AcceptVerbs("GET")]
        [CheckPermission]
        public async Task<IActionResult> GetUserForSuggestion(string keyword, int? officeId, int page = 1, int pageSize = 20)
        {
            var result = await _userService.GetUserForSuggestion(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, keyword,
                officeId, page, pageSize);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Lay danh sach nhan vien cung cap hoac ca cap duoi
        /// </summary>
        /// <param name="isShowChildren"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Route("user-children/{isShowChildren}"), AcceptVerbs("GET")]
        [CheckPermission]
        public async Task<IActionResult> GetUserChildren(bool isShowChildren, int page = 1, int pageSize = 20)
        {
            var result = await _userService.GetListUserChildren(CurrentUser.TenantId, CurrentUser.Id, CultureInfo.CurrentCulture.Name, isShowChildren, page, pageSize);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Kiểm tra username có tồn tại
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [Route("check-exists-userName/{userName}"), AcceptVerbs("POST")]
        [CheckPermission]
        public async Task<IActionResult> GetUserChildren(string userName)
        {
            var result = await _userService.CheckUserNameExists(userName);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Thêm mới nhân viên
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.User, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]UserMeta user)
        {
            //user.EnrollNumber = 123;
            var result = await _userService.Insert(CurrentUser.TenantId, user);

            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        /// <summary>
        /// Cập nhật nhân viên
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.User, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody]UserMeta user)
        {
            var result = await _userService.Update(id, CurrentUser.TenantId, user);

            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Xóa nhân viên
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.User, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _userService.Delete(CurrentUser.TenantId, id);

            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        /// <summary>
        /// Cập nhật ảnh đại diện
        /// </summary>
        /// <param name="id"></param>
        /// <param name="avatar"></param>
        /// <returns></returns>
        [Route("{id}/update-avatar"), AcceptVerbs("POST")]
        [AllowPermission(PageId.User, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateAvatar(string id, string avatar)
        {
            var result = await _userService.UpdateAvatar(CurrentUser.TenantId, id, avatar);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Cập nhật trạng thái
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [Route("update-status"), AcceptVerbs("POST")]
        [AllowPermission(PageId.User, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateStatus(string id, UserStatus status)
        {
            var result = await _userService.UpdateStatus(CurrentUser.TenantId, id, status);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Lấy thông tin chi tiết của nhân viên
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.User, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetDetail(string id)
        {
            return Ok(await _userService.GetUserDetail(CurrentUser.TenantId, id));
        }

        /// <summary>
        /// Lấy thông tin nhân viên
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Route("short-user-info"), AcceptVerbs("GET")]
        [CheckPermission]
        public async Task<IActionResult> GetShortUserInfoInUseController(string userId)
        {
            var result = await _userService.GetShortUserInfo(CurrentUser.TenantId, CurrentUser.Id, CultureInfo.CurrentCulture.Name);

            return Ok(result);
        }

        /// <summary>
        /// Lấy thông tin nhân viên
        /// </summary>
        /// <param name="tenantId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Route("{tenantId}/short-user-info/{userId}"), AcceptVerbs("GET")]
        [CheckPermission]
        public async Task<IActionResult> GetShortUserInfo(string tenantId, string userId)
        {
            var result = await _userService.GetShortUserInfo(tenantId, userId, CultureInfo.CurrentCulture.Name);

            return Ok(result);
        }       

        /// <summary>
        /// Lấy danh sách nhân viên theo ids
        /// </summary>
        /// <param name="tenantId"></param>
        /// <param name="userIds"></param>
        /// <returns></returns>
        [Route("{tenantId}/short-user-info"), AcceptVerbs("POST")]
        [CheckPermission]
        public async Task<IActionResult> GetShortUserInfoByListUserId(string tenantId, [FromBody]List<string> userIds)
        {
            return Ok(await _userService.GetShortUserInfoByListUserId(tenantId, userIds, CultureInfo.CurrentCulture.Name));
        }

        /// <summary>
        /// Lấy thông tin người đứng đầu phòng ban
        /// </summary>
        /// <param name="tenantId"></param>
        /// <param name="officeId"></param>
        /// <returns></returns>
        [Route("{tenantId}/short-user-info-leader-office/{officeId}"), AcceptVerbs("GET")]
        [CheckPermission]
        public async Task<IActionResult> GetShorUserInfoOfLeaderOffice(string tenantId, int officeId)
        {
            var result = await _userService.GetShorUserInfoOfLeaderOffice(tenantId, officeId, CultureInfo.CurrentCulture.Name);

            return Ok(result);
        }

        /// <summary>
        /// Lấy thông tin người quản lý trực tiếp
        /// </summary>
        /// <param name="tenantId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Route("{tenantId}/direct-manager/{userId}"), AcceptVerbs("GET")]
        [CheckPermission]
        public async Task<IActionResult> GetDirectManagerInfo(string tenantId, string userId)
        {
            var result = await _userService.GetDirectManagerInfo(tenantId, userId, CultureInfo.CurrentCulture.Name);

            return Ok(result);
        }

        #region UserContact
        /// <summary>
        /// Thêm thông tin liên hệ
        /// </summary>
        /// <param name="userContact"></param>
        /// <returns></returns>
        [Route("insert-user-contact"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.User, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> InsertUserContact([FromBody] UserContactMeta userContact)
        {
            var result = await _userContactServce.Insert(CurrentUser.TenantId, userContact);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Cập nhật thông tin liên hệ
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userContact"></param>
        /// <returns></returns>
        [Route("update-user-contact"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.User, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateUserContact(string id, [FromBody]UserContactMeta userContact)
        {
            var result = await _userContactServce.Update(CurrentUser.TenantId, id, userContact);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Xóa thông tin liên hệ
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("delete-user-contact"), AcceptVerbs("POST")]
        [AllowPermission(PageId.User, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> DeleteUserContact(string id)
        {
            var result = await _userContactServce.Delete(CurrentUser.TenantId, id);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }
        #endregion
        #region Exports
        /// <summary>
        /// Xuất danh sách nhân viên
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="status"></param>
        /// <param name="officeIdPath"></param>
        /// <param name="positionId"></param>
        /// <param name="gender"></param>
        /// <param name="monthBirthDay"></param>
        /// <param name="yearBirthday"></param>
        /// <param name="academicRank"></param>
        /// <returns></returns>
        [Route("export-user"), AcceptVerbs("GET")]
        [AllowPermission(PageId.User, Permission.Export)]
        [CheckPermission]
        public async Task<IActionResult> Export(string keyword, UserStatus? status, string officeIdPath, string positionId,
            Gender? gender, byte? monthBirthDay, int? yearBirthday, AcademicRank? academicRank)
        {
            var listExportUser = await _userService.GetListExportUser(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, keyword, status,
              officeIdPath, positionId, gender, monthBirthDay, yearBirthday, academicRank);

            using (var xls = new ExcelPackage())
            {
                var sheet = xls.Workbook.Worksheets.Add("Sheet1");
                var colorTitle = ColorTranslator.FromHtml("#003300");
                var colorHeader = ColorTranslator.FromHtml("#3598dc");
                var colorWhite = ColorTranslator.FromHtml("#ffffff");

                ExcelHelper.CreateCellTable(sheet, 2, 1, 2, 9, "Danh sách nhân viên", new CustomExcelStyle { IsBold = true, FontSize=14, IsMerge = true, HorizontalAlign = ExcelHorizontalAlignment.Center});

                ExcelHelper.CreateHeaderTable(sheet, "A4", "STT", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "B4", "Phòng ban/Bộ phận", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "C4", "Tài khoản", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "D4", "Họ và tên", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "E4", "Chức danh", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "F4", "Chức vụ", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "G4", "Giới tính", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "H4", "Ngày sinh", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "I4", "Số điện thoại cá nhân", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "J4", "Hộ khẩu thường trú", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "K4", "Địa chỉ liên hệ", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "L4", "Số CMT", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "M4", "Ngày cấp", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "N4", "Nơi cấp", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "O4", "Đối tượng nhân viên", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "P4", "Loại hợp đồng hiện tại", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "Q4", "Từ ngày", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "R4", "Đến ngày", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "S4", "Số sổ BHXH", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "T4", "MST cá nhân", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "U4", "Số tài khoản Ngân hàng", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "V4", "Chi nhánh", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);
                ExcelHelper.CreateHeaderTable(sheet, "W4", "Ngày bắt đầu làm việc", ExcelHorizontalAlignment.Center, false, colorHeader, colorWhite, null, true, true, true);

                sheet.View.FreezePanes(5, 5);
                var rowIndex = 5;                
                foreach (var user in listExportUser.Items)
                {
                    ExcelHelper.CreateCellTable(sheet, "A" + rowIndex, rowIndex - 4, ExcelHorizontalAlignment.Center, false);
                    ExcelHelper.CreateCellTable(sheet, "B" + rowIndex, user.OfficeName, ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "C" + rowIndex, user.UserName, ExcelHorizontalAlignment.Center, false, true);
                    ExcelHelper.CreateCellTable(sheet, "D" + rowIndex, user.FullName, ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "E" + rowIndex, user.TitleName, ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "F" + rowIndex, user.PositionName, ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "G" + rowIndex, user.Gender == 0 ? "Nữ" : (int)user.Gender == 1 ? "Nam" : "Giới tính khác", ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "H" + rowIndex, user.Birthday.HasValue ? user.Birthday.Value.ToString("dd/MM/yyyy") : "", ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "I" + rowIndex, user.PhoneNumber, ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "J" + rowIndex, user.ResidentRegister, ExcelHorizontalAlignment.Left, true, true);
                    ExcelHelper.CreateCellTable(sheet, "K" + rowIndex, user.Address, ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "L" + rowIndex, user.IdCardNumber, ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "M" + rowIndex, user.IdCardDateOfIssue.HasValue ? user.IdCardDateOfIssue.Value.ToString("dd/MM/yyyy") : "", ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "N" + rowIndex, user.IdCardPlaceOfIssue, ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "O" + rowIndex, user.Status == 0 ? "Dịch vu - CTV" : (int)user.Status == 1 ? "Học việc" : (int)user.Status == 2 ? "Thử việc" : (int)user.Status == 3 ? "Chính thức" : (int)user.Status == 4 ? "Thai sản" : (int)user.Status == 5 ? "Thôi việc" : (int)user.Status == 6 ? "Nghỉ hưu" : "", ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "P" + rowIndex, user.LaborContractType, ExcelHorizontalAlignment.Center, false, true);
                    ExcelHelper.CreateCellTable(sheet, "Q" + rowIndex, user.FromDate.HasValue ? user.FromDate.Value.ToString("dd/MM/yyyy") : "", ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "R" + rowIndex, user.ToDate.HasValue ? user.ToDate.Value.ToString("dd/MM/yyyy") : "", ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "S" + rowIndex, user.SocialInsuranceNumber, ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "T" + rowIndex, user.Tin, ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "U" + rowIndex, user.BankingNumber, ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "V" + rowIndex, user.BranchBank, ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, "W" + rowIndex, user.JoinedDate.HasValue ? user.JoinedDate.Value.ToString("dd/MM/yyyy") : "", ExcelHorizontalAlignment.Left, false, true);
                    rowIndex++;
                }

                sheet.Cells.Style.WrapText = true;
                sheet.Column(2).Width = 30;
                sheet.Column(4).Width = 20;
                sheet.Column(5).Width = 20;
                sheet.Column(6).Width = 25;
                sheet.Column(10).Width = 40;
                sheet.Column(11).Width = 40;
                sheet.Column(14).Width = 40;
                sheet.Cells.Style.Font.Size = 11;
                sheet.Cells.Style.Font.Name = "Times New Roman";
                var stream = new MemoryStream(xls.GetAsByteArray())
                {
                    // Reset Stream Position
                    Position = 0
                };

                var fileName = "DANH-SACH-NHAN-SU.xlsx";
                var fileType = "application/vnd.ms-excel";               
                return File(stream, fileType, fileName);                
            }
        }

        //[Route("export-labor-contract"), AcceptVerbs("GET")]
        //public async Task<HttpResponseMessage> ExportLaborContract(string officeIdPath)
        //{
        //    var listUserLabor = await _laborContractRepository.GetListLaborContractExport(officeIdPath);
        //    using (var xls = new ExcelPackage())
        //    {
        //        var sheet = xls.Workbook.Worksheets.Add("Sheet1");
        //        sheet.Column(3).Width = 30;
        //        sheet.Column(4).Width = 30;
        //        sheet.Column(6).Width = 20;
        //        sheet.Column(7).Width = 20;
        //        sheet.Column(8).Width = 20;
        //        var colorGreen = ColorTranslator.FromHtml("#007455");
        //        var colorWhite = ColorTranslator.FromHtml("#ffffff");
        //        ExcelHelper.CreateHeaderTable(sheet, "A1", "STT", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, false);
        //        ExcelHelper.CreateHeaderTable(sheet, "B1", "Mã nhân viên", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, false);
        //        ExcelHelper.CreateHeaderTable(sheet, "C1", "Tên nhân viên", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, false);
        //        ExcelHelper.CreateHeaderTable(sheet, "D1", "Phòng ban", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, false);
        //        ExcelHelper.CreateHeaderTable(sheet, "E1", "Số hợp đồng", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, false);
        //        ExcelHelper.CreateHeaderTable(sheet, "F1", "Loại hợp đồng", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, false, false);
        //        ExcelHelper.CreateHeaderTable(sheet, "G1", "Từ ngày", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, false);
        //        ExcelHelper.CreateHeaderTable(sheet, "H1", "Đến ngày", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, false);

        //        var rowIndex = 2;
        //        foreach (var user in listUserLabor)
        //        {
        //            ExcelHelper.CreateHeaderTable(sheet, "A" + rowIndex, rowIndex - 1, ExcelHorizontalAlignment.Center, true, false);
        //            ExcelHelper.CreateHeaderTable(sheet, "B" + rowIndex, user.Id, ExcelHorizontalAlignment.Left, true, false);
        //            ExcelHelper.CreateHeaderTable(sheet, "C" + rowIndex, user.FullName, ExcelHorizontalAlignment.Left, true, false);
        //            ExcelHelper.CreateHeaderTable(sheet, "D" + rowIndex, user.OfficeName, ExcelHorizontalAlignment.Center, true, false);
        //            ExcelHelper.CreateHeaderTable(sheet, "E" + rowIndex, user.ContractNo, ExcelHorizontalAlignment.Left, true, false);
        //            ExcelHelper.CreateHeaderTable(sheet, "F" + rowIndex, user.ContractType, ExcelHorizontalAlignment.Left, true, false);
        //            ExcelHelper.CreateHeaderTable(sheet, "G" + rowIndex, user.FromDate.HasValue ? user.FromDate.Value.ToString("dd/MM/yyyy") : "", ExcelHorizontalAlignment.Left, true, false);
        //            ExcelHelper.CreateHeaderTable(sheet, "H" + rowIndex, user.ToDate.HasValue ? user.ToDate.Value.ToString("dd/MM/yyyy") : "", ExcelHorizontalAlignment.Left, true, false);
        //            rowIndex++;
        //        }

        //        HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
        //        var stream = new MemoryStream(xls.GetAsByteArray());
        //        // Reset Stream Position
        //        stream.Position = 0;
        //        response.Content = new StreamContent(stream);
        //        response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        //        response.Content.Headers.ContentDisposition.FileName = "DANH-SACH-HOP-DONG.xlsx";
        //        response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
        //        response.Content.Headers.ContentLength = stream.Length;
        //        return response;
        //    }
        //}

        //[Route("export-user-record"), AcceptVerbs("GET")]
        //public async Task<HttpResponseMessage> ExportUserRecord(string officeIdPath)
        //{
        //    var listUserLabor = await _recordsManagementRepository.GetListRecordExport(officeIdPath);
        //    using (var xls = new ExcelPackage())
        //    {
        //        var sheet = xls.Workbook.Worksheets.Add("Sheet1");
        //        sheet.Column(2).Width = 15;
        //        sheet.Column(3).Width = 30;
        //        sheet.Column(4).Width = 30;
        //        var colorGreen = ColorTranslator.FromHtml("#007455");
        //        var colorWhite = ColorTranslator.FromHtml("#ffffff");
        //        ExcelHelper.CreateHeaderTable(sheet, "A1", "STT", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, false);
        //        ExcelHelper.CreateHeaderTable(sheet, "B1", "Mã nhân viên", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, false);
        //        ExcelHelper.CreateHeaderTable(sheet, "C1", "Tên nhân viên", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, false);
        //        ExcelHelper.CreateHeaderTable(sheet, "D1", "Phòng ban", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, false);
        //        ExcelHelper.CreateHeaderTable(sheet, "E1", "Sơ yếu lý lịch", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, true);
        //        ExcelHelper.CreateHeaderTable(sheet, "F1", "CMND", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, false);
        //        ExcelHelper.CreateHeaderTable(sheet, "G1", "Sổ hộ khẩu", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, true);
        //        ExcelHelper.CreateHeaderTable(sheet, "H1", "Bằng cấp", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, false);
        //        ExcelHelper.CreateHeaderTable(sheet, "I1", "Chứng chỉ hành nghề", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, true);
        //        ExcelHelper.CreateHeaderTable(sheet, "J1", "Giấy khám sức khỏe", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, true);
        //        ExcelHelper.CreateHeaderTable(sheet, "K1", "Giấy khai sinh", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, true);
        //        ExcelHelper.CreateHeaderTable(sheet, "L1", "Cam kết bảo mật thông tin", ExcelHorizontalAlignment.Center, false, colorGreen, colorWhite, null, true, true, true);

        //        var rowIndex = 2;
        //        foreach (var user in listUserLabor)
        //        {
        //            ExcelHelper.CreateHeaderTable(sheet, "A" + rowIndex, rowIndex - 1, ExcelHorizontalAlignment.Center, true, false);
        //            ExcelHelper.CreateHeaderTable(sheet, "B" + rowIndex, user.Id, ExcelHorizontalAlignment.Left, true, false);
        //            ExcelHelper.CreateHeaderTable(sheet, "C" + rowIndex, user.FullName, ExcelHorizontalAlignment.Left, true, false);
        //            ExcelHelper.CreateHeaderTable(sheet, "D" + rowIndex, user.OfficeName, ExcelHorizontalAlignment.Center, true, false);
        //            ExcelHelper.CreateHeaderTable(sheet, "E" + rowIndex, user.Profile.HasValue && user.Profile.Value == 1 ? "X" : "", ExcelHorizontalAlignment.Center, true, false);
        //            ExcelHelper.CreateHeaderTable(sheet, "F" + rowIndex, user.IdCardNumber.HasValue && user.IdCardNumber.Value == 1 ? "X" : "", ExcelHorizontalAlignment.Center, true, false);
        //            ExcelHelper.CreateHeaderTable(sheet, "G" + rowIndex, user.RegistrationBook.HasValue && user.RegistrationBook.Value == 1 ? "X" : "", ExcelHorizontalAlignment.Center, true, false);
        //            ExcelHelper.CreateHeaderTable(sheet, "H" + rowIndex, user.Degress.HasValue && user.Degress.Value == 1 ? "X" : "", ExcelHorizontalAlignment.Center, true, false);
        //            ExcelHelper.CreateHeaderTable(sheet, "I" + rowIndex, user.Certification.HasValue && user.Certification.Value == 1 ? "X" : "", ExcelHorizontalAlignment.Center, true, false);
        //            ExcelHelper.CreateHeaderTable(sheet, "J" + rowIndex, user.HealthCertification.HasValue && user.HealthCertification.Value == 1 ? "X" : "", ExcelHorizontalAlignment.Center, true, false);
        //            ExcelHelper.CreateHeaderTable(sheet, "K" + rowIndex, user.BirthCertification.HasValue && user.BirthCertification.Value == 1 ? "X" : "", ExcelHorizontalAlignment.Center, true, false);
        //            ExcelHelper.CreateHeaderTable(sheet, "L" + rowIndex, user.SecureCommitment.HasValue && user.SecureCommitment.Value == 1 ? "X" : "", ExcelHorizontalAlignment.Center, true, false);
        //            rowIndex++;
        //        }

        //        HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
        //        var stream = new MemoryStream(xls.GetAsByteArray());
        //        // Reset Stream Position
        //        stream.Position = 0;
        //        response.Content = new StreamContent(stream);
        //        response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        //        response.Content.Headers.ContentDisposition.FileName = "BAO-CAO-QUAN-LY-HO-SO.xlsx";
        //        response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
        //        response.Content.Headers.ContentLength = stream.Length;
        //        return response;
        //    }
        //}

        #endregion        
    }
}