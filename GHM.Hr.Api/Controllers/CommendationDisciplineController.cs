﻿using System;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Hr.Api.Controllers
{
    ///<Summary>
    /// Gets the answer
    ///</Summary>
    [Authorize]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class CommendationDisciplineController : GhmControllerBase
    {
        private readonly ICommendationDisciplineService _commendationDisciplineService;

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        public CommendationDisciplineController(ICommendationDisciplineService commendationDisciplineService)
        {
            _commendationDisciplineService = commendationDisciplineService;
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [AcceptVerbs("GET")]
        [AllowPermission(PageId.User, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string keyword, string userId, bool? type,
            string categoryId, DateTime? fromDate, DateTime? toDate, int? officeId, string titleId, int page = 1,
            int pageSize = 20)
        {

            var result = await _commendationDisciplineService.Search(keyword, userId, type, categoryId, fromDate,
                 toDate, officeId, titleId, page, pageSize, out var totalRows);

            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.User, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]CommendationDisciplineMeta commendationDisciplineMeta)
        {
            var result = await _commendationDisciplineService.Insert(CurrentUser.TenantId,CurrentUser.Id, CurrentUser.FullName, commendationDisciplineMeta);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.User, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody]CommendationDisciplineMeta commendationDisciplineMeta)
        {
            var result = await _commendationDisciplineService.Update(id, CurrentUser.TenantId, commendationDisciplineMeta);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{id}"),AcceptVerbs("DELETE")]
        [AllowPermission(PageId.User, Permission.Delete)]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _commendationDisciplineService.Delete(CurrentUser.TenantId, id);
            if (result.Code < 0)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }
    }
}

