﻿using System.Globalization;
using System.Threading.Tasks;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.Models;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Hr.Api.Controllers
{
    ///<Summary>
    /// Gets the answer
    ///</Summary>
    [Authorize]
    [ApiController]
    [Route("api/v{version:apiVersion}/criterias-positions")]
    public class CriteriaPositionController : GhmControllerBase
    {
        private readonly ICriteriaPositionService _criteriaPositionService;

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        public CriteriaPositionController(ICriteriaPositionService criteriaPositionService)
        {
            _criteriaPositionService = criteriaPositionService;
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [AllowPermission(PageId.CriteriaConfig, Permission.Insert, Permission.Update, Permission.View)]
        [CheckPermission, AcceptVerbs("GET")]
        public async Task<IActionResult> Search(bool? isActive, string positionId)
        {
            var result = await _criteriaPositionService.Search(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name,
                positionId, isActive);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [AllowPermission(PageId.CriteriaConfig, Permission.Insert)]
        [CheckPermission, AcceptVerbs("POST")]
        public async Task<IActionResult> Insert([FromBody]CriteriaPosition criteriaPosition)
        {
            criteriaPosition.TenantId = CurrentUser.TenantId;
            var result = await _criteriaPositionService.Insert(criteriaPosition);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }


        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{positionId}/{criteriaId}")]
        [AllowPermission(PageId.CriteriaConfig, Permission.Insert)]
        [CheckPermission, AcceptVerbs("POST")]
        public async Task<IActionResult> Update(string criteriaId, string positionId, [FromBody]bool isActive)
        {
            var result = await _criteriaPositionService.UpdateActiveStatus(CurrentUser.TenantId, criteriaId, positionId, isActive);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }


        ///<Summary>
        /// Gets the answer
        ///</Summary>
        [Route("{positionId}/{criteriaId}")]
        [AllowPermission(PageId.CriteriaConfig, Permission.Insert)]
        [CheckPermission, AcceptVerbs("DELETE")]
        public async Task<IActionResult> Delete(string criteriaId, string positionId)
        {
            var result = await _criteriaPositionService.Delete(CurrentUser.TenantId, criteriaId, positionId);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }
    }
}