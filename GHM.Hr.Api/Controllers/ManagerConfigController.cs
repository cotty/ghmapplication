﻿using GHM.Hr.Domain.IServices;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace GHM.Hr.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/manager-configs")]
    public class ManagerConfigController : GhmControllerBase
    {
        private readonly IUserService _userService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userSerice"></param>
        public ManagerConfigController(IUserService userSerice)
        {
            _userService = userSerice;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="officeId"></param>
        /// <param name="type"></param>
        /// <param name="isGetStaffFromChildOffice"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [AcceptVerbs("GET")]
        [AllowPermission(PageId.ManagerConfig, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetUserForConfig(string keyword, int officeId, byte? type,
            bool? isGetStaffFromChildOffice, int page = 1, int pageSize = 20)
        {
            var result = await _userService.SearchForConfigManager(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, keyword,
               officeId, type, isGetStaffFromChildOffice, page, pageSize);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="managerId"></param>
        /// <returns></returns>
        [Route("manager-direct"), AcceptVerbs("POST")]
        [AllowPermission(PageId.ManagerConfig, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateManagerDirect(string userId, string managerId)
        {
            var result = await _userService.UpdateDirectManager(CurrentUser.TenantId, userId, managerId);

            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="approveId"></param>
        /// <returns></returns>
        [Route("manager-approve"), AcceptVerbs("POST")]
        [AllowPermission(PageId.ManagerConfig, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateManagerApprove(string userId, string approveId)
        {
            var result = await _userService.UpdateApproveManager(CurrentUser.TenantId, userId, approveId);

            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userIds"></param>
        /// <param name="managerId"></param>
        /// <param name="approveId"></param>
        /// <returns></returns>
        [Route("update-manager-by-list-user"), AcceptVerbs("POST")]
        [AllowPermission(PageId.ManagerConfig, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateManager([FromBody]List<string> userIds, string managerId, string approveId)
        {
            var result = await _userService.UpdateDirectManagerAndApproveManager(CurrentUser.TenantId, userIds, managerId, approveId);

            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }
    }
}