﻿using System;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Hr.Api.Controllers
{
    //[Authorize]
    //[Produces("application/json")]
    //[Route("api/v1/employment-history")]
    //public class EmploymentHistoryController : GhmControllerBase
    //{
    //    private readonly IEmploymentHistoryRepository _employmentHistoryRepository;
    //    private readonly IUserValueRepository _userValueRepository;

    //    public EmploymentHistoryController(IUserValueRepository userValueRepository, IEmploymentHistoryRepository employmentHistoryRepository)
    //    {            
    //        _userValueRepository = userValueRepository;
    //        _employmentHistoryRepository = employmentHistoryRepository;
    //    }

    //    #region Employment History
    //    [Route("search"), AcceptVerbs("GET")]
    //    //[CheckPermission(new[] { Permission.View }, PageId.EmploymentHistory)]
    //    public async Task<IActionResult> Search(string keyword, string userId, bool? type, int? companyId, bool? isCurrent, DateTime? fromDate, DateTime? toDate, int page = 1, int pageSize = 20)
    //    {
    //        return Ok(new
    //        {
    //            items = await _employmentHistoryRepository.Search(keyword, userId, type, companyId, isCurrent, fromDate, toDate, page, pageSize, out var totalRows),
    //            totalRows
    //        });
    //    }

    //    [Route("insert"), AcceptVerbs("POST"), ValidateModel]
    //    //[CheckPermission(new[] { Permission.Insert }, PageId.EmploymentHistory)]
    //    public async Task<IActionResult> Insert([FromBody]EmploymentHistoryMeta employment)
    //    {
    //        if (employment.ToDate.HasValue && DateTime.Compare(employment.ToDate.Value, employment.FromDate) < 0)
    //            return Ok(-5);

    //        if (string.IsNullOrEmpty(employment.UserId))
    //            return Ok(-6);

    //        var result = await _employmentHistoryRepository.Insert(employment);
    //        if (result > 0)
    //        {
    //            // TODO: Insert log
    //        }
    //        return Ok(result);
    //    }

    //    [Route("update"), AcceptVerbs("POST"), ValidateModel]
    //    //[CheckPermission(new[] { Permission.Update }, PageId.EmploymentHistory)]
    //    public async Task<IActionResult> Update([FromBody]EmploymentHistoryMeta employment)
    //    {

    //        if (employment.ToDate.HasValue && DateTime.Compare(employment.ToDate.Value, employment.FromDate) < 0)
    //            return Ok(-6);

    //        var result = await _employmentHistoryRepository.Update(employment);
    //        if (result > 0)
    //        {
    //            // TODO: Insert log
    //        }
    //        return Ok(result);
    //    }

    //    [Route("delete"), AcceptVerbs("DELETE")]
    //    //[CheckPermission(new[] { Permission.Delete }, PageId.EmploymentHistory)]
    //    public async Task<IActionResult> Delete(int id)
    //    {
    //        var result = await _employmentHistoryRepository.Delete(id);
    //        if (result > 0)
    //        {
    //            // TODO: Insert log
    //        }
    //        return Ok(result);
    //    }

    //    [Route("search-company"), AcceptVerbs("GET")]
    //    //[CheckPermission(new[] { Permission.Insert, Permission.Update }, PageId.EmploymentHistory)]
    //    public async Task<IActionResult> SearchCompany()
    //    {
    //        return Ok(await _userValueRepository.Search(string.Empty, UserValueType.EmploymentHistoryCompany));
    //    }

    //    [Route("search-title"), AcceptVerbs("GET")]
    //    //[CheckPermission(new[] { Permission.Insert, Permission.Update }, PageId.EmploymentHistory)]
    //    public async Task<IActionResult> SearchTitle(string keyword)
    //    {
    //        return Ok(await _userValueRepository.Search(keyword, UserValueType.EmploymentHistoryTitle));
    //    }

    //    [Route("search-office"), AcceptVerbs("GET")]
    //    //[CheckPermission(new[] { Permission.Insert, Permission.Update }, PageId.EmploymentHistory)]
    //    public async Task<IActionResult> SearchOffice(string keyword)
    //    {
    //        return Ok(await _userValueRepository.Search(keyword, UserValueType.EmploymentHistoryOffice));
    //    }
    //    #endregion
    //}
}
