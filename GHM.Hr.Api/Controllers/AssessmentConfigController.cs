﻿using System.Threading.Tasks;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.Models;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Hr.Api.Controllers
{
    /// <summary>
    /// Câu hình tiêu chí đánh giá
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/v{version:apiVersion}/assessment-configs")]
    public class AssessmentConfigController : GhmControllerBase
    {
        private readonly IAssessmentConfigService _assessmentConfigService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="assessmentConfigService"></param>
        public AssessmentConfigController(IAssessmentConfigService assessmentConfigService)
        {
            _assessmentConfigService = assessmentConfigService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="assessmentTimeConfig"></param>
        /// <returns></returns>
        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.AssessmentConfig, Permission.Insert, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Save([FromBody]AssessmentTimeConfig assessmentTimeConfig)
        {
            var result = await _assessmentConfigService.SaveAssessmentTime(CurrentUser.TenantId, assessmentTimeConfig.FromDay.Value,
                assessmentTimeConfig.ToDay.Value);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs("GET")]
        [AllowPermission(PageId.AssessmentConfig, Permission.Insert, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> GetAssessmentTime()
        {
            var result = await _assessmentConfigService.GetAssessmentTime(CurrentUser.TenantId);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }
    }
}