﻿using System.Globalization;
using System.Threading.Tasks;
using GHM.Relationship.Domain.IServices;
using GHM.Relationship.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.CustomAttributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Relationship.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]

    public class RelationshipsController : GhmControllerBase
    {
        private readonly IRelationshipService _relationshipService;
        private readonly IRelationshipTypeService _relationshipTypeService;

        public RelationshipsController(IRelationshipService relationshipService, IRelationshipTypeService relationshipTypeService)
        {
            _relationshipService = relationshipService;
            _relationshipTypeService = relationshipTypeService;
        }

        [AcceptVerbs("GET")]
        //[CheckPermission(new[] { Permission.View }, PageId.jod, PageId.Mail)]
        public async Task<IActionResult> Search(string keyword, bool? isActive, int page, int pageSize)
        {
            var result = await _relationshipService.Search(CurrentUser.TenantId, keyword, isActive, page, pageSize);
            //var result = await _relationshipService.Search("1", keyword, isActive, 1, 10);
            return Ok(result);
        }

        [AcceptVerbs("POST"), ValidateModel]
        //[CheckPermission(PageId.jod, Permission.Insert)]
        public async Task<IActionResult> Insert([FromBody]RelationshipMeta relationshipMetas)
        {
            var result = await _relationshipService.Insert(CurrentUser.TenantId, relationshipMetas);
            //  var result = await _relationshipService.Insert("1", relationshipMetas);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{customerCode}/{customerRelationshipCode}"), AcceptVerbs("POST"), ValidateModel]
        //[CheckPermission(PageId.jod, Permission.Update)]
        public async Task<IActionResult> Update(string customerCode, string customerRelationshipCode, [FromBody]RelationshipMeta relationshipMetas)
        {
            var result = await _relationshipService.Update(CurrentUser.TenantId, customerCode, customerRelationshipCode, relationshipMetas);
            //  var result = await _relationshipService.Update("1", customerCode, customerRelationshipCode, relationshipMetas);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{customerCode}/{customerRelationshipCode}"), AcceptVerbs("GET")]
        //[CheckPermission(PageId.jod, Permission.View)]
        public async Task<IActionResult> Detail(string customerCode, string customerRelationshipCode)
        {
            var result = await _relationshipService.GetDetail(CurrentUser.TenantId, customerCode, customerRelationshipCode);
            //  var result = await _relationshipService.GetDetail("1", customerCode, customerRelationshipCode);
            if (result.Code < 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{customerCode}/{customerRelationshipCode}"), AcceptVerbs("DELETE")]
        //[CheckPermission(new[] { Permission.Delete }, PageId.jod)]
        public async Task<IActionResult> Delete(string customerCode, string customerRelationshipCode)
        {
            var result = await _relationshipService.Delete(CurrentUser.TenantId, customerCode, customerRelationshipCode);
            //    var result = await _relationshipService.ForceDelete("1", customerCode, customerRelationshipCode);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        #region Relationship Types
        [Route("types"), AcceptVerbs("GET")]
        //[CheckPermission(PageId.RelationShipType, Permission.View)]
        public async Task<IActionResult> TypeSuggestion()
        {
            return Ok(await _relationshipTypeService.GetsForSuggestions(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name));
        }
        #endregion
    }
}