﻿using System.Globalization;
using System.Threading.Tasks;
using GHM.Relationship.Domain.IServices;
using GHM.Relationship.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Relationship.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/relationship-type")]
  public class RelationshipTypeController : GhmControllerBase
    {
        private readonly IRelationshipTypeService _typeRelationshipService;
        
        public RelationshipTypeController(IRelationshipTypeService typeRelationshipService)
        {
            _typeRelationshipService = typeRelationshipService;
        }

        [AcceptVerbs("GET")]
        [AllowPermission(PageId.RelationShipType, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetAll(bool? isActive)
        {            
            return Ok(await _typeRelationshipService.GetsAll(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, isActive));
        }

        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.RelationShipType, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]RelationshipTypeMeta typeRelationshipMeta)
        {
            var result = await _typeRelationshipService.Insert(CurrentUser.TenantId, typeRelationshipMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.RelationShipType, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody]RelationshipTypeMeta typeRelationshipMeta)
        {
            var result = await _typeRelationshipService.Update(CurrentUser.TenantId, id, typeRelationshipMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.RelationShipType, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Detail(string id)
        {
            var result = await _typeRelationshipService.GetDetail(CurrentUser.TenantId, id);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.RelationShipType, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _typeRelationshipService.Delete(CurrentUser.TenantId, id);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

    }
}