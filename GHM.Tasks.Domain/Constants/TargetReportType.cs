﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Tasks.Domain.Constants
{
    public enum TargetReportType
    {
        statisticsByUnits,
        statisticsByPersonal,
        listTargetEmployee,
        generalTargetAndTask
    }
}
