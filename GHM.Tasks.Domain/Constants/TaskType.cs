﻿namespace GHM.Tasks.Domain.Constants
{
    public enum TaskType
    {
        TaskLibrary,
        Task
    }
}
