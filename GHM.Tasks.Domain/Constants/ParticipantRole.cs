﻿namespace GHM.Tasks.Domain.Constants
{
    public enum ParticipantRole
    {
        /// <summary>
        /// Người tham gia.
        /// </summary>
        Participant = 1,
        /// <summary>
        /// Người theo dõi
        /// </summary>
        Observer = 2
    }
}
