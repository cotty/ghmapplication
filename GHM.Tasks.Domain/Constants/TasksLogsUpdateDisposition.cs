﻿namespace GHM.Tasks.Domain.Constants
{
    public enum TasksLogsUpdateDisposition
    {
        TaskInsert,
        TaskUpdateStatus,
        TaskChangeDeadline,
        TaskUpdateName,
        TaskUpdateParticipant,
        TaskUpdateEstimateStartDate,
        TaskUpdateEstimateEndDate,
        TaskUpdateResponsible,
        TaskUpdateIsResponsibleAllowUpdate,
    }
}
