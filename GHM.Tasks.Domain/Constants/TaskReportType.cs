﻿namespace GHM.Tasks.Domain.Constants
{
    public enum TaskReportType
    {
        statisticsByUnits,
        statisticsByPersonal,
        listByPersonal
    }
}
