﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Tasks.Domain.Constants
{
    public enum TypeNotification
    {
        None,
        EveryDay,
        EveryMonth,
        EveryYear
    }
}
