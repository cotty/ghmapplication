﻿namespace GHM.Tasks.Domain.Constants
{
    public enum TargetType
    {
        Office,
        Personal,
        Participants,
        All
    }
}
