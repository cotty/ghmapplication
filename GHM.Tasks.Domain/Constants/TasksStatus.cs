﻿namespace GHM.Tasks.Domain.Constants
{
    public enum TasksStatus
    {
        NotStartYet,
        InProgress,
        Completed,
        Canceled,
        PendingToFinish,
        DeclineFinish,
    }
}
