﻿namespace GHM.Tasks.Domain.Constants
{
    public enum TaskMethodCalculateResult
    {
        CheckList,
        Manually,
        TaskChild
    }
}
