﻿namespace GHM.Tasks.Domain.Constants
{
    public enum TargetRole
    {
        Read = 1,
        Notification = 2,
        Disputation = 4, 
        Report = 8, // Báo cáo
        WriteTask = 16, // Tạo công việc trên mục tiêu
        WriteTarget = 32, // Có quyền cập nhật mục tiêu;
        WriteTargetChild = 64// Tạo mục tiêu con;
    }
}
