﻿namespace GHM.Tasks.Domain.Constants
{
    public static class LogTitleMessage
    {
        public const string UpdateTask = "Update task";
        public const string SendTaskReminder = "Send task reminder";
        public const string ViewTaskDetail = "View task detail";
    }

    public static class LogDescriptionMessage
    {
        public const string UpdateEstimateStartDate = "Update estimate start date";
        public const string UpdateEstimateEndDate = "Update estimate end date";
        public const string UpdateDescription = "Update description";
        public const string UpdateName = "Update title";
        public const string AddChecklist = "Add checklist";
        public const string AddParticipant = "Add participant";
        public const string UpdateTarget = "Update target";
        public const string UpdateMethodCaculateResult = "Calculator method change";
        public const string UpdateProgress = "Update progress";
        public const string UpdateStatus = "Change status";
    }
}
