﻿namespace GHM.Tasks.Domain.Constants
{
    public enum TaskRole
    {
        Read = 1,
        Notification = 2,
        Comment = 4,
        Report = 8,
        Write = 16,
        Full = 63
    }
}
