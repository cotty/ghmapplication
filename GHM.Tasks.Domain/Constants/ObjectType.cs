﻿namespace GHM.Tasks.Domain.Constants
{
    public enum ObjectType
    {
        Target,
        Task
    }
}
