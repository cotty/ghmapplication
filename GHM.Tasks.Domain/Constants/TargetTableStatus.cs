﻿namespace GHM.Tasks.Domain.Constants
{
    public enum TargetTableStatus
    {
        UnderConstruction, // Đang xây dựng
        Using, // Đang sử dụng
        StopUsing // Dừng sử dụng
    }
}
