﻿namespace GHM.Tasks.Domain.Constants
{
    public enum TypeSearchTime
    { 
        StarteDateToEndDate,
        CreateDate,
        StartDate,
        EndDate,
        CompleteDate,
    }
}
