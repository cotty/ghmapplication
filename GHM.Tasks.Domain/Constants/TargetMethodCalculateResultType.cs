﻿namespace GHM.Tasks.Domain.Constants
{
    public enum TargetMethodCalculateResultType
    {
        TargetChild, // theo Mục tiêu con hoặc theo công việc con
        Manually // tự nhập
    }
}
