﻿namespace GHM.Tasks.Domain.Constants
{
    public enum TargetStatus
    {
        New,
        Finish,
        All
    }
}
