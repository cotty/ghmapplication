﻿namespace GHM.Tasks.Domain.Constants
{
    public enum LogType
    {
        /// <summary>
        /// Giá trị nhập vào.
        /// </summary>
        Value,
        /// <summary>
        /// Hệ thống sau này phục vụ đa ngôn ngữ
        /// </summary>
        System,
        /// <summary>
        /// Ngày tháng
        /// </summary>
        DateTime,
        /// <summary>
        /// Người dùng.
        /// </summary>
        User,
        /// <summary>
        /// Json
        /// </summary>
        Json
    }

    public enum LogUpdateDisposition
    {
        TaskInsert,
        TaskUpdateStatus,
        TaskChangeDeadline,
        TaskUpdateName,
        TaskUpdateParticipant,
        TaskUpdateEstimateStartDate,
        TaskUpdateEstimateEndDate,
        TaskUpdateResponsible,
        TaskUpdateIsResponsibleAllowUpdate,
        TaskViewDetail,
        TaskUpdateDescription,
        TaskUpdateProgress,
        TaskUpdateCalculatorResultMethod,
        TaskAddParticipant,
        TaskRemoveParticipant,

        TargetInsert,
        TargetUpdateName,
        TargetUpdatePercentCompleted,
        TargetInsertParticipant,
        TargetUpdateRoleParticipant,
        TargetUpdateMethodCaculateResult,
        TargetUpdateWeight,
        TargetUpdateTargetDate,
        TargetUpdateTargetCompleteDate,
        TargetUpdateDescription,
        TargetGetDetail,
        TargetUpdateLevelOfSharing,
        TargetUpdateTargetGroupId,
        TargetDeleteParticipants,
        TargetDeleteAttachment,
        TargetInsertAttachment,
        TargetInsertAttachments,
        TargetUpdateMeasurementMethod,
        TargetUpdateStatus,
        TargetUpdateTargetTable,
    }
}
