﻿namespace GHM.Tasks.Domain.ViewModels
{
    public class TargetTableForSuggestionViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
