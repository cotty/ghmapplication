﻿namespace GHM.Tasks.Domain.ViewModels
{
    public class TargetGroupSearchViewModel
    {
        public string Id { get; set; }
        public string TargetTableId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? Order { get; set; }
        public string Color { get; set; }
        public bool? IsActive { get; set; }
        public string ConcurrencyStamp { get; set; }
    }
}
