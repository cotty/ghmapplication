﻿using GHM.Tasks.Domain.Constants;

namespace GHM.Tasks.Domain.ViewModels
{
    public class TargetSuggestionViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long? ParentId { get; set; }
        public TargetType Type { get; set; }
        public string FullName { get; set; }
        public string OfficeName { get; set; }
        public string IdPath { get; set; }
        public int ChildCount { get; set; }
    }
}
