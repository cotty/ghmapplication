﻿using GHM.Tasks.Domain.Constants;
using System;

namespace GHM.Tasks.Domain.ViewModels
{
    public class TargetTableSearchViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public TargetTableStatus? Status { get; set; }
        public int? Order { get; set; } // Số thứ tự
    }
}
