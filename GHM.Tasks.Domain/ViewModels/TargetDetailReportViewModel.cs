﻿using GHM.Tasks.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Tasks.Domain.ViewModels
{
    public class TargetDetailReportViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public double? PercentCompleted { get; set; }
        public double? Weight { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        public TargetStatus? Status { get; set; }
        public int ChildCount { get; set; }
        public int? TotalTask { get; set; }
        public int? TotalComment { get; set; }
        public string OfficeName { get; set; }
        public int? OfficeId { get; set; }
        public bool IsDelete { get; set; }
    }
}
