﻿using GHM.Tasks.Domain.Constants;
using System.Collections.Generic;

namespace GHM.Tasks.Domain.ViewModels
{
    public class ParticipantViewModel
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public List<RoleParticipantTaskViewModel> TaskRoleGroupParticipants { get; set; }
    }
}
