﻿using GHM.Tasks.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Tasks.Domain.ViewModels
{
    public class RoleParticipantTaskViewModel
    {
        public string TaskParticipantsId { get; set; }
        public string RoleGroupId { get; set; }
        public TaskRole Role { get; set; }
    }
}
