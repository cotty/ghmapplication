﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Tasks.Domain.ViewModels
{
    public class TaskRoleParticipantsViewModel
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public int? OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string PositionName { get; set; }
        public List<RoleGroupViewModel> RoleGroupIds { get; set; }
    }
}