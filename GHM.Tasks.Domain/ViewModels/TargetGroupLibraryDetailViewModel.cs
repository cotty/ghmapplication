﻿namespace GHM.Tasks.Domain.ViewModels
{
    public class TargetGroupLibraryDetailViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public string Description { get; set; }
        public string ConcurrencyStamp { get; set; }
    }
}
