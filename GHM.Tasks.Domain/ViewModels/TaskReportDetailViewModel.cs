﻿using GHM.Tasks.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Tasks.Domain.ViewModels
{
    public class TaskReportDetailViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? EstimateStartDate { get; set; }
        public DateTime? EstimateEndDate { get; set; }
        public decimal PercentCompleted { get; set; }
        public int ChecklistCount { get; set; }
        public int CompletedChecklistCount { get; set; }
        public int CommentCount { get; set; }
        public DateTime? ApproveTime { get; set; }
        public DateTime? CompletedDate { get; set; }
        public bool IsDelete { get; set; }
        public TasksStatus Status { get; set; }
    }
}
