﻿using System.Collections.Generic;

namespace GHM.Tasks.Domain.ViewModels
{
    public class ReportTaskViewModel
    {
        public int? OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public int TotalTask { get; set; }
        public int ProgressNotComplete { get; set; }
        public int OverDueNotComplete { get; set; }
        public int ProgressComplete { get; set; }
        public int OverDueComplete { get; set; }
        public int TotalDestroy { get; set; }
        public List<TaskReportDetailViewModel> Task { get; set; }
    }
}
