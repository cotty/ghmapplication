﻿namespace GHM.Tasks.Domain.ViewModels
{
    public class TargetGroupSuggestionViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
