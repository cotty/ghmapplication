﻿namespace GHM.Tasks.Domain.ViewModels
{
    public class TargetLibraryViewModel
    {
        public string Id { get; set; }
        public string TargetGroupLibraryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string MeasurementMethod { get; set; }
        public string ConcurrencyStamp { get; set; }
    }
}
