﻿using System.Collections.Generic;

namespace GHM.Tasks.Domain.ViewModels
{
    public class TaskTreeViewModel
    {
        public long Id { get; set; }
        public long? ParentId { get; set; }      
        public string Name { get; set; }
        public string IdPath { get; set; }
        public TasksSearchViewModel Data { get; set; }
        public State State { get; set; }
        public List<TaskTreeViewModel> Children { get; set; }
        public int? ChildCount { get; set; }

        public TaskTreeViewModel()
        {
            State = new State
            {
                Opened = false,
                Selected = false,
                Disabled = false
            };
        }
    }
}
