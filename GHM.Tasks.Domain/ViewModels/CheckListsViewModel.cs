﻿using GHM.Tasks.Domain.Constants;

namespace GHM.Tasks.Domain.ViewModels
{
    public class CheckListsViewModel
    {
        public string Id { get; set; }
        public long TaskId { get; set; }
        public string TaskName { get; set; }
        public string Name { get; set; }
        public TaskType Type { get; set; } // Loại check list dùng cho công việc , 0 Thư viện công việc 1 Công việc
        public bool IsComplete { get; set; } // trang thai toan thanh
        public string ConcurrencyStamp { get; set; }
    }
}
