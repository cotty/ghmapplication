﻿using GHM.Tasks.Domain.Constants;
using System.Collections.Generic;

namespace GHM.Tasks.Domain.ViewModels
{
    public class TaskLibraryViewModel
    {
        public long Id { get; set; }
        public string TaskGroupLibraryId { get; set; }
        public string TaskGroupLibraryName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ConcurrencyStamp { get; set; }
        public TaskMethodCalculateResult MethodCalculateResult { get; set; }
        public List<CheckListsViewModel> CheckListViewModel { get;set; }
    }
}
