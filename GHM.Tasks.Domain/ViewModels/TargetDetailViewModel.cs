﻿using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.Models;
using System;
using System.Collections.Generic;

namespace GHM.Tasks.Domain.ViewModels
{
    public class TargetDetailViewModel
    {
        public long Id { get; set; }
        public string TargetTableId { get; set; } // Id của bảng mục tiêu
        public string TargetGroupId { get; set; } // Id của nhóm mục tiêu
        public string TargetGroupName { get; set; } // Tên nhóm mục tiêu
        public string TargetTableName { get; set; }
        public string TargetGroupColor { get; set; } // Màu nhóm mục tiêu
        public long? ParentId { get; set; }
        public string ParentName { get; set; }
        public int ChildCount { get; set; }
        public string IdPath { get; set; }
        public string Name { get; set; } // Tên mục tiêu
        public TargetType Type { get; set; } // Loại mục tiêu. O Mục tiêu cá nhân 1 Mục tiêu đơn vị.
        public string UserId { get; set; } // Mã nhân viên phụ trách thực hiện mục tiêu nếu là mục tiêu cá nhân
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public string TitleId { get; set; }
        public string TitleName { get; set; }
        public int Role { get; set; } // enum TaskRole
        public LevelOfSharing LevelOfSharing { get; set; }
        public string MeasurementMethod { get; set; }
        public TargetMethodCalculateResultType MethodCalculateResult { get; set; }
        public int? OfficeId { get; set; } // Mã phòng ban phụ trách mục tiêu nếu mục tiêu là đơn vị
        public string OfficeName { get; set; }
        public double? Weight { get; set; } // Trọng số
        public DateTime StartDate { get; set; } // Ngày bắt đầu
        public DateTime EndDate { get; set; } // Ngày kết thúc
        public DateTime? CompletedDate { get; set; }
        public double? PercentCompleted { get; set; } // Phần trăm hoàn thành
        public TargetStatus? Status { get; set; } // Trạng thái. 0 Chưa hoàn thành 1 Hoàn thành
        public string Description { get; set; }
        public int? TotalTask { get; set; }
        public int? TotalTaskComplete { get; set; }
        public int? TotalAttachment { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
        public DateTime CreateTime { get; set; }
        public List<TargetParticipant> Participants { get; set; }
        public List<Target> Targets { get; set; }
        public List<Attachment> Attachments { get; set; }
        public List<TargetTreeViewModel> TargetChildren { get; set; }
        public int OverdueDate { get; set; }
        public bool IsDelete { get; set; }
    }
}
