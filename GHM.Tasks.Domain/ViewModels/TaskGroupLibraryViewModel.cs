﻿using System.Collections.Generic;

namespace GHM.Tasks.Domain.ViewModels
{
    public class TaskGroupLibraryViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public List<TaskLibraryViewModel> TasksLibraries { get; set; }
    }
}
