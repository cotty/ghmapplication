﻿using System.Collections.Generic;

namespace GHM.Tasks.Domain.ViewModels
{
    public class TargetGroupLibraryViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ConcurrencyStamp { get; set; }
        public List<TargetLibraryViewModel> TargetLibrary { get; set; }
    }
}
