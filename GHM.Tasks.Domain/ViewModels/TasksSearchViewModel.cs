﻿using System;
using GHM.Tasks.Domain.Constants;

namespace GHM.Tasks.Domain.ViewModels
{
    public class TasksSearchViewModel
    {
        public long Id { get; set; }
        public long? ParentId { get; set; }
        public string IdPath { get; set; }
        public string Name { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
        public string CreatorAvatar { get; set; }
        public string CreatorPositionId { get; set; }
        public string CreatorPositionName { get; set; }
        public int CreatorOfficeId { get; set; }
        public string CreatorOfficeName { get; set; }
        public string CreatorOfficeIdPath { get; set; }
        public string ResponsibleId { get; set; }
        public string ResponsibleFullName { get; set; }
        public string ResponsibleAvatar { get; set; }
        public string ResponsiblePositionId { get; set; }
        public string ResponsiblePositionName { get; set; }
        public int ResponsibleOfficeId { get; set; }
        public string ResponsibleOfficeName { get; set; }
        public string ResponsibleOfficeIdPath { get; set; }
        public TasksStatus Status { get; set; }
        public int ChildCount { get; set; }
        public DateTime EstimateStartDate { get; set; }
        public DateTime EstimateEndDate { get; set; }
        public double EstimateCount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsIdea { get; set; }
        public long TargetId { get; set; }
        public string TargetName { get; set; }
        public double? OverdueDate { get; set; }

        public bool? IsAllowResponsibleUpdate { get; set; }
        public decimal PercentCompleted { get; set; }
        public int ChecklistCount { get; set; }
        public int CompletedChecklistCount { get; set; }
        public int CommentCount { get; set; }
        public string ConcurrencyStamp { get; set; }
        public TasksKind? Type { get; set; }

        public TasksSearchViewModel()
        {

        }

        public TasksSearchViewModel(long id, long? parentId, string idPath, string name, DateTime createTime, string creatorId, string creatorFullName, string creatorAvatar,
            string creatorPositionId, string creatorPositionName, int creatorOfficeId, string creatorOfficeName, string creatorOfficeIdPath,
            string responsibleId, string responsibleFullName, string responsibleAvatar, string responsiblePositionId,
            string responsiblePositionName, int responsibleOfficeId, string responsibleOfficeName, string responsibleOfficeIdPath,
            TasksStatus status, int childCount, int? groupId, string groupName, DateTime estimateStartDate, DateTime estimateEndDate,
            int estimateCount, DateTime? startDate, DateTime? endDate, bool isIdea, string managerUserId, bool? isAllowResponsibleUpdate, string concurrencyStamp, TasksKind type)
        {
            Id = id;
            ParentId = parentId;
            IdPath = idPath;
            Name = name;
            CreateTime = createTime;
            CreatorId = creatorId;
            CreatorFullName = creatorFullName;
            CreatorAvatar = creatorAvatar;
            CreatorPositionId = creatorPositionId;
            CreatorPositionName = creatorPositionName;
            CreatorOfficeId = creatorOfficeId;
            CreatorOfficeName = creatorOfficeName;
            CreatorOfficeIdPath = creatorOfficeIdPath;
            ResponsibleId = responsibleId;
            ResponsibleFullName = responsibleFullName;
            ResponsibleAvatar = responsibleAvatar;
            ResponsiblePositionId = responsiblePositionId;
            ResponsiblePositionName = responsiblePositionName;
            ResponsibleOfficeId = responsibleOfficeId;
            ResponsibleOfficeName = responsibleOfficeName;
            ResponsibleOfficeIdPath = responsibleOfficeIdPath;
            Status = status;
            ChildCount = childCount;
            EstimateStartDate = estimateStartDate;
            EstimateEndDate = estimateEndDate;
            EstimateCount = estimateCount;
            StartDate = startDate;
            EndDate = endDate;
            IsIdea = isIdea;
            IsAllowResponsibleUpdate = isAllowResponsibleUpdate;
            ConcurrencyStamp = concurrencyStamp;
            Type = type;
        }
    }
}
