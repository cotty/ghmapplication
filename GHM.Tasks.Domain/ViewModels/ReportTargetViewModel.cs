﻿namespace GHM.Tasks.Domain.ViewModels
{
    public class ReportTargetViewModel
    {
        public int? OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public int TotalTarget { get; set; }
        public int ProgressNotComplete { get; set; } // Chưa hoàn thành 
        public int OverDueNotComplete { get; set; } // Quá hạn chưa hoàn thành
        public int ProgressComplete { get; set; } // Đúng tiến độ
        public int OverDueComplete { get; set; } // Chậm tiến độ
        public int TotalDestroy {get;set;} // Đã hủy
    }
}
