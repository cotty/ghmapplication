﻿using GHM.Tasks.Domain.Constants;

namespace GHM.Tasks.Domain.ViewModels
{
    public class RoleGroupViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public TaskRole Role { get; set; }
    }
}