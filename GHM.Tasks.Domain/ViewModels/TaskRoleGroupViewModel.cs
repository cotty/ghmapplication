﻿using GHM.Tasks.Domain.Constants;

namespace GHM.Tasks.Domain.ViewModels
{
    public class TaskRoleGroupViewModel
    {
        public string ParticipantId { get; set; }
        public string UserId { get; set; }
        public TaskRole Role { get; set; }
    }
}