﻿using GHM.Tasks.Domain.Constants;
using System;
using System.Collections.Generic;

namespace GHM.Tasks.Domain.ViewModels
{
    public class TargetTableDetailViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public TargetTableStatus? Status { get; set; }
        public int? Order { get; set; } // Số thứ tự
        public string ConcurrencyStamp { get; set; }
        public List<TargetGroupSearchViewModel> TargetGroups { get; set; }
    }
}
