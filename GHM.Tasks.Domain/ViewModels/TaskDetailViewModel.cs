﻿using System;
using System.Collections.Generic;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.Models;

namespace GHM.Tasks.Domain.ViewModels
{
    public class TaskDetailViewModel : TaskViewModel
    {
        public long TargetId { get; set; }
        public TaskMethodCalculateResult MethodCalculatorResult { get; set; }
        public string Description { get; set; }
        public string ConcurrencyStamp { get; set; }
        public bool IsAllowApprove { get; set; }
        public List<CheckListsViewModel> Checklists { get; set; }
        public List<TaskRoleParticipantsViewModel> Participants { get; set; }
        public List<Attachment> Attachments { get; set; }
        public List<RoleGroup> RoleGroups { get; set; }
        public List<TaskTreeViewModel> TaskChild { get; set; }
    }
}
