﻿using System;
using GHM.Tasks.Domain.Constants;

namespace GHM.Tasks.Domain.ViewModels
{
    public class TaskViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long? ParentId { get; set; }
        public string ParentName { get; set; }
        public string IdPath { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
        public string CreatorAvatar { get; set; }
        public string CreatorPositionName { get; set; }
        public string CreatorOfficeName { get; set; }
        public string ResponsibleId { get; set; }
        public string ResponsibleFullName { get; set; }
        public string ResponsibleAvatar { get; set; }
        public string ResponsiblePositionName { get; set; }
        public string ReponsibleOfficeIdPath { get; set; }
        public string ResponsibleOfficeName { get; set; }
        public TasksStatus Status { get; set; }
        public TasksKind? Type { get; set; }
        public DateTime EstimateStartDate { get; set; }
        public DateTime EstimateEndDate { get; set; }
        public double EstimateCount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal PercentCompleted { get; set; }
        public int ChecklistCount { get; set; }
        public int CompletedChecklistCount { get; set; }
        public int CommentCount { get; set; }
        public string TargetName { get; set; }
        public int OverdueDate { get; set; }
        public int ChildCount { get; set; }
        public string Description { get; set; }
    }
}
