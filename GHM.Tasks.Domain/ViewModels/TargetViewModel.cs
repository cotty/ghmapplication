﻿using GHM.Tasks.Domain.Constants;
using System;

namespace GHM.Tasks.Domain.ViewModels
{
    public class TargetViewModel
    {
        public long Id { get; set; }
        public string TargetTableId { get; set; } // Id của bảng mục tiêu
        public string TargetTableName { get; set; }
        public int? TargetTableOrder { get; set; }
        public string TargetGroupId { get; set; } // Id của nhóm mục tiêu
        public string TargetGroupName { get; set; } // Tên nhóm mục tiêu
        public string TargetGroupColor { get; set; } // Màu nhóm mục tiêu
        public int? TargetGroupOrder { get; set; }
        public long? ParentId { get; set; }
        public int ChildCount { get; set; }
        public int ChildCountComplete { get; set; }
        public string IdPath { get; set; }
        public string Name { get; set; } // Tên mục tiêu
        public string Description { get; set; }
        public TargetType Type { get; set; } // Loại mục tiêu. O Mục tiêu cá nhân 1 Mục tiêu đơn vị.
        public string UserId { get; set; } // Mã nhân viên phụ trách thực hiện mục tiêu nếu là mục tiêu cá nhân
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public TargetMethodCalculateResultType MethodCalculateResult { get; set; }
        public int? OfficeId { get; set; } // Mã phòng ban phụ trách mục tiêu nếu mục tiêu là đơn vị
        public string OfficeName { get; set; }
        public double? Weight { get; set; } // Trọng số
        public DateTime StartDate { get; set; } // Ngày bắt đầu
        public DateTime EndDate { get; set; } // Ngày kết thúc
        public DateTime? CompletedDate { get; set; }
        public DateTime CreateTime { get; set; }// Ngayf tao
        public double? PercentCompleted { get; set; } // Phần trăm hoàn thành
        public TargetStatus? Status { get; set; } // Trạng thái. 0 Chưa hoàn thành 1 Hoàn thành 2 Quá hạn
        public int? TotalTask { get; set; }
        public int? TotalTaskComplete { get; set; }
        public int? TotalComment { get; set; }
        public bool IsWireTarget { get; set; }
        public int OverdueDate { get; set; }
    }
}
