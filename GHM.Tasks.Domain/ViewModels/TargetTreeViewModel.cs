﻿using System.Collections.Generic;

namespace GHM.Tasks.Domain.ViewModels
{
    public class TargetTreeViewModel
    {
        public long Id { get; set; }
        public long? ParentId { get; set; }
        public string TargetTableId { get; set; } // Id của bảng mục tiêu
        public string TargetTableName { get; set; } // Ten bang muc tieu
        public int? TargetTableOrder { get; set; }
        public string TargetGroupId { get; set; } // Id của nhóm mục tiêu
        public string TargetGroupName { get; set; } // Tên nhóm mục tiêu
        public string TargetGroupColor { get; set; } // Màu nhóm mục tiêu
        public int? TargetGroupOrder { get; set; }
        public string Name { get; set; }
        public string IdPath { get; set; }
        public TargetViewModel Data { get; set; }
        public State State { get; set; }
        public List<TargetTreeViewModel> Children { get; set; }
        public int? ChildCount { get; set; }

        public TargetTreeViewModel()
        {
            State = new State
            {
                Opened = false,
                Selected = false,
                Disabled = false
            };
        }
    }

    public class State
    {
        public bool? Opened { get; set; }
        public bool? Selected { get; set; }
        public bool? Disabled { get; set; }

        public State()
        {
            Opened = false;
            Selected = false;
            Disabled = false;
        }
    }
}
