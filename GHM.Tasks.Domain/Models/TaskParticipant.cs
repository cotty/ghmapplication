﻿using System.Collections.Generic;

namespace GHM.Tasks.Domain.Models
{
    public class TaskParticipant
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public long TaskId { get; set; } // Mã mục tiêu hoặc mà công việc
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public string PositionId { get; set; } // Chuc vu
        public string PositionName { get; set; }
        public int? OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string OfficeIdPath { get; set; }
        public virtual List<TaskRoleGroup> TaskRoleGroup { get; set; }
    }
}
