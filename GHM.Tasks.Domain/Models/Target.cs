﻿using GHM.Infrastructure.Models;
using GHM.Tasks.Domain.Constants;
using System;

namespace GHM.Tasks.Domain.Models
{
    public class Target : EntityBase<long>
    {
        public string TenantId { get; set; }
        public string TargetTableId { get; set; } // Id của bảng mục tiêu
        public string TargetGroupId { get; set; } // Id của nhóm mục tiêu
        public long? ParentId { get; set; }
        public int ChildCount { get; set; }
        public int? ChildCountComplete { get; set; }
        public string IdPath { get; set; }
        public string Name { get; set; } // Tên mục tiêu
        public string UnsignName { get; set; }
        public TargetType Type { get; set; } // Loại mục tiêu. O Mục tiêu cá nhân 1 Mục tiêu đơn vị.
        public string UserId { get; set; } // Mã nhân viên phụ trách thực hiện mục tiêu
        public string FullName { get; set; }
        public string TitleId { get; set; }
        public string TitleName { get; set; }
        public string Avatar { get; set; } // Avatar người thực hiện
        public string OfficeIdPath { get; set; }
        public int? OfficeId { get; set; } // Mã phòng ban phụ trách mục tiêu nếu mục tiêu là đơn vị
        public string OfficeName { get; set; }
        public string Description { get; set; }
        public LevelOfSharing LevelOfSharing { get; set; } // Mức độ chia sẻ.  0 Riêng 1 Chung
        public double? Weight { get; set; } // Trọng số
        public DateTime StartDate { get; set; } // Ngày bắt đầu
        public DateTime EndDate { get; set; } // Ngày kết thúc
        public DateTime? CompletedDate { get; set; } // Ngày hoàn thành mục tiêu
        public string MeasurementMethod { get; set; }
        public TargetMethodCalculateResultType MethodCalculateResult { get; set; } // Phương pháp tính kết quả
        public double? PercentCompleted { get; set; } // Phần trăm hoàn thành
        public TargetStatus? Status { get; set; } // Trạng thái. 0 Chưa hoàn thành 1 Hoàn thành 2 Quá hạn
        public int? TotalTask { get; set; }
        public int? TotalTaskComplete { get; set; }
        public int? TotalComment { get; set; }
        public bool IsDelete { get; set; }

        public Target()
        {
            IsDelete = false;
            MethodCalculateResult = TargetMethodCalculateResultType.TargetChild;
            Status = 0;
            TotalTask = 0;
            TotalTaskComplete = 0;
            TotalComment = 0;
            IsDelete = false;
            LastUpdate = DateTime.Now;
            ChildCountComplete = 0;
        }
    }
}
