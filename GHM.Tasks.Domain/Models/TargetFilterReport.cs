﻿using GHM.Tasks.Domain.Constants;
using System;

namespace GHM.Tasks.Domain.Models
{
    public class TargetFilterReport
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public TypeSearchTime TypeSearchTime { get; set; }
        public string TableTargetId { get; set; }
        public string TableTargetName { get; set; }
        public TargetType TargetType { get; set; }
        public int? OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public TargetReportType TargetReportType { get; set; }
        public TargetStatus? Status { get; set; }
    }
}
