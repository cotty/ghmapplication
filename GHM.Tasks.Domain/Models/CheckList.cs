﻿using System;
using GHM.Infrastructure.Models;
using GHM.Tasks.Domain.Constants;

namespace GHM.Tasks.Domain.Models
{
    public class CheckList : EntityBase<string>
    {
        public string TenantId { get; set; }
        public long TaskId { get; set; }
        public string Name { get; set; }
        public bool IsDelete { get; set; }
        public TaskType Type { get; set; } // Loại check list dùng cho công việc , 0 Thư viện công việc 1 Công việc
        public bool Status { get; set; } // Trạng thái hoàn thành, 0 chưa hoàn thành 1 đã hoàn thành     

        public CheckList()
        {
            Id = Guid.NewGuid().ToString();
            Status = false;
            IsDelete = false;
            LastUpdate = DateTime.Now;
        }
    }
}
