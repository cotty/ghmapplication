﻿namespace GHM.Tasks.Domain.Models
{
    public class TaskGroup
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public string Name { get; set; }
        public string UnsignName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public string Description { get; set; }
        public bool IsPublish { get; set; }

        public TaskGroup()
        {
            IsDelete = false;
        }
    }
}
