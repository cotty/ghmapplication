﻿namespace GHM.Tasks.Domain.Models
{
    public class TaskGroupShare
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public string GroupId { get; set; }
        public string GroupName { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string OfficeIdPath { get; set; }
    }
}
