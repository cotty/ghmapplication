﻿using GHM.Infrastructure.Models;
using System;
using System.Collections.Generic;

namespace GHM.Tasks.Domain.Models
{
    public class TargetGroupLibrary : EntityBase<string>
    {
        public string TenantId { get; set; }
        public string Name { get; set; }       
        public bool IsDelete { get; set; }
        public List<TargetLibrary> TargetLibrary { get; set; }
        public TargetGroupLibrary()
        {
            IsDelete = false;
            LastUpdate = DateTime.Now;
        }
    }
}
