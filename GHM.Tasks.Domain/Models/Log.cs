﻿using GHM.Tasks.Domain.Constants;
using System;

namespace GHM.Tasks.Domain.Models
{
    public class Log
    {
        public string Id { get; set; }

        /// <summary>
        /// Vị trí 
        /// </summary>
        public LogUpdateDisposition Disposition { get; set; }
        public string TenantId { get; set; }
        public long ObjectId { get; set; }

        /// <summary>
        /// Loại log 0: Mục tiêu 1: Công việc
        /// </summary>
        public ObjectType ObjectType { get; set; }

        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public string FromValue { get; set; }
        public string ToValue { get; set; }
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 0: Value 1: System (Dạng system sau này phục vụ cho đa ngôn ngữ) 2: Ngày tháng 3: Người dùng 4: Json
        /// </summary>
        public LogType Type { get; set; }

        public Log()
        {
            Id = Guid.NewGuid().ToString();
            CreateTime = DateTime.Now;
            Type = 0;
        }
    }
}
