﻿using System;

namespace GHM.Tasks.Domain.Models
{
    public class TargetParticipant
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public long TargetId { get; set; } // Mã mục tiêu hoặc mà công việc
        public string UserId { get; set; }
        public bool IsDelete { get; set; }
        public string FullName { get; set; }
        public string Image { get; set; }
        public string TitleId { get; set; }
        public string TitleName { get; set; }
        public int? OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string OfficeIdPath { get; set; }
        public int Role { get; set; }
        public string ConcurrencyStamp { get; set; }
        public TargetParticipant()
        {
            Id = Guid.NewGuid().ToString();
            ConcurrencyStamp = Guid.NewGuid().ToString();
            IsDelete = false;
        }
    }
}
