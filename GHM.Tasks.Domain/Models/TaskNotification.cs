﻿using GHM.Tasks.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Tasks.Domain.Models
{
    public class TaskNotification
    {
        public string Id { get; set; }
        public long TaskId { get; set; }
        public string TenantId { get; set; }
        public bool IsNotification { get; set; }
        public int Hours { get; set; }
        public int Minutes { get; set; }
        public DateTime? Dates { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
        public string CreatorAvatar { get; set; }
        public DateTime CreateTime { get; set; }
        public bool IsSendNotificationEmail {get;set;}
        public bool IsSendNotification { get; set; }
        public string ConcurrencyStamp { get; set; }
        public TypeNotification Type { get; set; }
        public TaskNotification()
        {
            Id = Guid.NewGuid().ToString();
            ConcurrencyStamp = Guid.NewGuid().ToString();
            CreateTime = DateTime.Now;
        }
    }
}
