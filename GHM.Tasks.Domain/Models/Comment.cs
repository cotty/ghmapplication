﻿using GHM.Tasks.Domain.Constants;
using System;

namespace GHM.Tasks.Domain.Models
{
    public class Comment
    {
        public long Id { get; set; }
        public string TenantId { get; set; }
        public string UserId { get; set; } // Mã người dùng
        public string FullName { get; set; } // Tên người comment
        public string Content { get; set; } // Nội dung comment
        public long ObjectId { get; set; } // Đối tượng tham chiếu
        public ObjectType ObjectType { get; set; } // Loại đối tượng tham chiếu. Xem trong fileEnum c#
        public long? ParentId { get; set; } // Mã comment cha
        public DateTime CreateTime { get; set; } // Thời gian tạo
        public int ChildCount { get; set; } // Số lượng bình luận con
        public int LikeCount { get; set; } // Số lượng like comment
        public bool IsDelete { get; set; } // Đã bị xoá hay chưa
        public string Image { get; set; } // Ảnh người comment
        public string UrlAttachment { get; set; }

        public Comment()
        {
            CreateTime = DateTime.Now;
            IsDelete = false;
        }
    }
}
