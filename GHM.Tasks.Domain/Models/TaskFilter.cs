﻿using System;
using GHM.Infrastructure.Models;
using GHM.Tasks.Domain.Constants;

namespace GHM.Tasks.Domain.Models
{
    public class TaskFilter : Pagination
    {
        public string TenantId { get; set; }
        public string CurrentUserId { get; set; }
        public string Keyword { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public long? TargetId { get; set; }
        public TasksKind Type { get; set; }
        public TasksStatus? Status { get; set; }
        /// <summary>
        /// Loại công việc:
        /// 0: Cá nhân.
        /// 1: Phối hợp.
        /// 2: Cả 2.
        /// 3: Tạo việc
        /// </summary>
        public WorkingType WorkingType { get; set; }
    }

    public enum WorkingConfig
    {
        OnGoing,
        Canceled
    }

    public enum WorkingType
    {
        Personal,
        Combination,
        Both,
        Create
    }
}
