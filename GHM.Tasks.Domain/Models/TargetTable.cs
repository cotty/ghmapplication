﻿using GHM.Infrastructure.Models;
using GHM.Tasks.Domain.Constants;
using System;

namespace GHM.Tasks.Domain.Models
{
    public class TargetTable : EntityBase<string>
    {
        public string TenantId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsDelete { get; set; }
        public TargetTableStatus? Status { get; set; } // Trạng thái 0 Đang thực hiện 1 Đang sử dụng 2 Ngừng sửa dụng
        public int? Order { get; set; } // Số thứ tự
        public TargetTableType Type { get; set; }

        public TargetTable()
        {
            IsDelete = false;
            Status = TargetTableStatus.UnderConstruction;
            LastUpdate = DateTime.Now;
        }
    }
}
