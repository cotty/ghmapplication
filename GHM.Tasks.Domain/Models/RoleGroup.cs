﻿using System;
using GHM.Tasks.Domain.Constants;

namespace GHM.Tasks.Domain.Models
{
    public class RoleGroup
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public TaskRole Role { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }

        public RoleGroup()
        {
            Id = Guid.NewGuid().ToString();
            ConcurrencyStamp = Guid.NewGuid().ToString();
        }
    }
}
