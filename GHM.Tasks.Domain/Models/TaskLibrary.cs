﻿using GHM.Infrastructure.Models;
using GHM.Tasks.Domain.Constants;
using System;

namespace GHM.Tasks.Domain.Models
{
    public class TaskLibrary : EntityBase<long>
    {
        public string TenantId { get; set; }
        public string TaskGroupLibraryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskMethodCalculateResult MethodCalculateResult { get; set; }
        public bool IsDelete { get; set; }

        public TaskLibrary()
        {
            IsDelete = false;
            LastUpdate = DateTime.Now;
        }
    }
}
