﻿using GHM.Infrastructure.Models;
using System;

namespace GHM.Tasks.Domain.Models
{
    public class TaskGroupLibrary : EntityBase<string>
    {
        public string TenantId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public bool IsDelete { get; set; }

        public TaskGroupLibrary()
        {
            IsDelete = false;
            Order = 0;
            LastUpdate = DateTime.Now;
        }
    }
}
