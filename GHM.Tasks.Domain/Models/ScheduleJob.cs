﻿using GHM.Infrastructure.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Tasks.Domain.Models
{
    public class ScheduleJob
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public string Content { get; set; }
        public JobType JobType { get; set; }
        public DateTime Date { get; set; }
        public string JobTypeId { get; set; }
        public ScheduleType ScheduleType { get; set; }
        public bool IsDelete { get; set; }
        public int Hour { get; set; }
        public int Minutes { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        public string LastUpdateUserId { get; set; }
        public string LastUpdateUserFullName { get; set; }
        public ScheduleJob()
        {
            CreateTime = DateTime.Now;
            Id = Guid.NewGuid().ToString();
            Hour = 0;
            Minutes = 0;
        }
    }
}
