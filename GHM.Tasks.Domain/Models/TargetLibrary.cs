﻿using GHM.Infrastructure.Models;
using System;

namespace GHM.Tasks.Domain.Models
{
    public class TargetLibrary: EntityBase<string>
    {
        public string TenantId { get; set; }
        public string TargetGroupLibraryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string MeasurementMethod { get; set; }
        public bool IsDelete { get; set; }

        public TargetLibrary()
        {
            IsDelete = false;
            LastUpdate = DateTime.Now;
        }
    }
}
