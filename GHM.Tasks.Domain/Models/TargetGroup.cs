﻿using GHM.Infrastructure.Models;
using System;

namespace GHM.Tasks.Domain.Models
{
    public class TargetGroup : EntityBase<string>
    {
        public string TenantId { get; set; }
        public string TargetTableId { get; set; }
        public string Name { get; set; } // Tên nhóm mục tiêu
        public string Description { get; set; }
        public int? Order { get; set; }
        public string Color { get; set; } // Mã màu hiển thị
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }

        public TargetGroup()
        {
            IsDelete = false;
            Order = 0;
            LastUpdate = DateTime.Now;
        }
    }
}
