﻿using GHM.Infrastructure.Models;
using GHM.Tasks.Domain.Constants;
using System;
using GHM.Infrastructure.Helpers;

namespace GHM.Tasks.Domain.Models
{
    public class Tasks : EntityBase<long>
    {
        public string TenantId { get; set; }
        public long TargetId { get; set; }
        public string Name { get; set; }
        public DateTime? ApproveTime { get; set; }
        public int ChildCount { get; set; }
        public string CreatorAvatar { get; set; }
        public int CreatorOfficeId { get; set; }
        public string CreatorOfficeIdPath { get; set; }
        public string CreatorOfficeName { get; set; }
        public string CreatorPositionId { get; set; }
        public string CreatorPositionName { get; set; }
        public int Day { get; set; }
        public DateTime? Deadline { get; set; }
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public double EstimateCount { get; set; }
        public DateTime EstimateEndDate { get; set; }
        public DateTime EstimateStartDate { get; set; }
        public string Explanation { get; set; }
        public long? FromTemplateId { get; set; }
        public string IdPath { get; set; }
        public bool IsAllowResponsibleUpdate { get; set; }
        public bool IsIdea { get; set; }
        public bool IsTemplate { get; set; }
        public int Month { get; set; }
        public long? ParentId { get; set; }
        public string ParentName { get; set; }
        public byte Quarter { get; set; }
        public TasksStatus Status { get; set; }
        public int? TotalDays { get; set; }
        public string UnsignName { get; set; }
        public int Year { get; set; }
        public decimal PercentCompleted { get; set; } // Phần trăm hoàn thành
        public string ResponsibleAvatar { get; set; }
        public string ResponsibleFullName { get; set; }
        public string ResponsibleId { get; set; } // Người phụ trách
        public int ResponsibleOfficeId { get; set; }
        public string ResponsibleOfficeIdPath { get; set; }
        public string ResponsibleOfficeName { get; set; }
        public string ResponsiblePositionId { get; set; }
        public string ResponsiblePositionName { get; set; }
        public bool IsDelete { get; set; }
        public int ChecklistCount { get; set; }
        public int CompletedChecklistCount { get; set; }
        public int CommentCount { get; set; }
        public TasksKind? Type { get; set; }
        public TaskMethodCalculateResult MethodCalculateResult { get; set; }

        public Tasks()
        {
            ChildCount = 0;
            EstimateCount = 0;
            IsAllowResponsibleUpdate = false;
            IsIdea = false;
            IsTemplate = false;
            IsDelete = false;
            Day = DateTime.Now.Day;
            Month = DateTime.Now.Month;
            Year = DateTime.Now.Year;
            Quarter = DateTime.Now.GetQuarter();
            IdPath = "0";
            PercentCompleted = 0;
            ChecklistCount = 0;
            CompletedChecklistCount = 0;
            CommentCount = 0;
            Type = TasksKind.Personal;
            LastUpdate = DateTime.Now;
        }
    }
}
