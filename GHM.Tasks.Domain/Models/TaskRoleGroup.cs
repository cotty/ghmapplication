﻿namespace GHM.Tasks.Domain.Models
{
    public class TaskRoleGroup
    {
        public string TaskParticipantId { get; set; }
        public string RoleGroupId { get; set; }
        public string TenantId { get; set; }

        public virtual TaskParticipant TaskParticipant { get; set; }
    }
}
