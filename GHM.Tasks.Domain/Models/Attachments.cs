﻿using GHM.Tasks.Domain.Constants;
using System;

namespace GHM.Tasks.Domain.Models
{
    public class Attachment
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public long ObjectId { get; set; } // Mã mục tiêu hoặc công việc
        public ObjectType ObjectType { get; set; } // 0: Mục tiêu 1: Công việc
        public string FileId { get; set; }
        public string FileName { get; set; }
        public int DonwloadCount { get; set; }
        public bool IsDelete { get; set; }
        public string Type { get; set; } // Loại tệp tin
        public string Extension { get; set; }
        public string Url { get; set; }
        public int ViewCount { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreatorFullName { get; set; }
        public string CreatorId { get; set; }
        public string ConcurrencyStamp { get; set; }

        public Attachment()
        {
            IsDelete = false;
            CreateTime = DateTime.Now;
            ConcurrencyStamp = Guid.NewGuid().ToString();
        }
    }
}
