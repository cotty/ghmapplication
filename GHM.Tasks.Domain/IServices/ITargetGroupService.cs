﻿using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.ViewModels;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IServices
{
    public interface ITargetGroupService
    {
        Task<ActionResultResponse<string>> Insert(string tenantId, string creatorId, string creatorFullName, string targetTableId,
            TargetGroupMeta targetGroupMeta);

        Task<ActionResultResponse<string>> Update(string id, string tenantId, string lastUpdateUserId, string lastUpdateFullName,
            TargetGroupMeta targetGroupMeta);

        Task<ActionResultResponse> Delete(string id, string tenantId);

        Task<ActionResultResponse> UpdateOrder(string tenantId, string id, int order);

        Task<ActionResultResponse> UpdateColor(string tenantId, string id, string color);

        Task<SearchResult<TargetGroupSuggestionViewModel>> GetTargetGroupByTargetTableId(string tenantId, string targetTableId);
    }
}
