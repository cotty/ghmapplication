﻿using System.Threading.Tasks;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.Models;

namespace GHM.Tasks.Domain.IServices
{
    public interface ITasksLogsService
    {
        Task<SearchResult<Log>> GetsByTaskId(long taskId, string currentUserId, int page, int pageSize);
    }
}
