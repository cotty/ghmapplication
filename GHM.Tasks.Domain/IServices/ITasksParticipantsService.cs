﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Infrastructure.Models;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;

namespace GHM.Tasks.Domain.IServices
{
    public interface ITasksParticipantsService
    {
        Task<ActionResultResponse> Save(string tennantId, long taskId, List<TaskParticipantMeta> taskParticipants);

        Task<List<ParticipantViewModel>> GetsByTaskId(string tenantId, long taskId, string currentUserId);

        Task<ActionResultResponse> Delete(string tenantId, long taskId, string userId, string currentUserId);

        Task<ActionResultResponse> Update(string tenantId, long taskId, string userId, TaskParticipantMeta taskParticipantMeta);

        Task<List<TaskRoleParticipantsViewModel>> GetsAllByTaskId(string tenantId, long taskId);
    }
}
