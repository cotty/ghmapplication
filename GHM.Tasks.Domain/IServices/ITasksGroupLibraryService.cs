﻿using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IServices
{
    public interface ITasksGroupLibraryService
    {
        Task<SearchResult<TaskGroupLibrary>> Search(string tenantId, string keyword, int page, int pageSize);

        Task<ActionResultResponse> ForceDelete(string tenantId, string userId, string userFullName, string id);

        Task<ActionResultResponse<string>> Update(string tenantId, string id, string userId, string userFullName, TasksGroupLibraryMeta tasksGroupLibraryMeta);

        Task<ActionResultResponse<string>> Insert(string tenantId, string userId, string userFullName, TasksGroupLibraryMeta tasksGroupLibraryMeta);

        Task<SearchResult<TaskGroupLibraryViewModel>> GetAllTasksLibrary(string tenantId);

        Task<ActionResultResponse<TargetGroupLibraryDetailViewModel>> GetDetail(string tenantId, string id);

    }
}
