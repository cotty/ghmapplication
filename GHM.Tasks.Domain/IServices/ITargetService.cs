﻿using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IServices
{
    public interface ITargetService
    {
        Task<ActionResultResponse> ForceDelete(string tenantId, string userId, long id);

        Task<ActionResultResponse<long>> Insert(string tenantId, string userId, string userFullName, string avatar, TargetMeta targetMeta);

        Task<ActionResultResponse<string>> InsertParticipants(string tenantId, string userId, string fullName, string avatar,
            TargetParticipantsMeta targetParticipantsMeta);

        Task<ActionResultResponse> UpdateRoleParticipant(string tenantId, string userId, string fullName, string avatar, 
            string id, int roleParticipants);

        Task<ActionResultResponse> UpdateMethodCalculateResult(string tenantId, string userId, string fullName, string avatar,
            long id, TargetMethodCalculateResultType methodCalculateResult);

        Task<ActionResultResponse> UpdateWeight(string tenantId, string userId, string fullName, string avatar, long id, double weight);

        Task<ActionResultResponse> UpdateMeasurementMethod(string tenantId, string userId, string fullName, string avatar,
            long id, string measurementMethod);

        Task<ActionResultResponse> UpdateTargetTableGroup(string tenantId, string userId, string fullName, string avatar,
            long id, string targetTableId, string targetGroupId);

        Task<ActionResultResponse> UpdateTargetPercentCompleted(string tenantId, string userId, string fullName, string avatar, long id, double percentCompleted);

        Task<ActionResultResponse> UpdateTargetStatus(string tenantId, string userId, string fullName, string avatar, long id, TargetStatus status);

        Task<ActionResultResponse> UpdateTargetDate(string tenantId, string userId, string fullName, string avatar,
            long id, DateTime starDate, DateTime endDate);

        Task<ActionResultResponse> UpdateTargetCompleteDate(string tenantId, string userId, string fullName, string avatar, long id, DateTime completeDate);

        Task<ActionResultResponse> UpdateTargetDescription(string tenantId, string userId, string fullName, string avatar, long id, string description);

        Task<ActionResultResponse> UpdateTargetName(string tenantId, string userId, string fullName, string avatar, long id, string name);

        Task<ActionResultResponse<TargetDetailViewModel>> GetDetail(string tenantId, long id, string userId, string fullName, string avatar);

        Task<ActionResultResponse> UpdateLevelOfSharing(string tenantId, string userId, string fullName, string avatar, long id, LevelOfSharing levelOfSharing);

        Task<ActionResultResponse> UpdateTargetGroupId(string tenantId, string userId, string fullName, string avatar, long id, string groupId);

        Task<SearchResult<TargetTreeViewModel>> SearchTargetOffice(string tenantId, int? officeId, string currentUserId, string keyword,
            DateTime? startDate, DateTime? endDate, string targetTableId, TargetStatus? status);

        Task<SearchResult<TargetTreeViewModel>> SearchTargetPersonal(string tenantId, string currentUserId,
            string userId, string keyword, DateTime? startDate, DateTime? endDate, string targetTableId, TargetStatus? status, List<int> listType);

        Task<ActionResultResponse> DeleteParticipants(string tenantId, string id, string userId, string fullName, string avatar);

        Task<SearchResult<TargetTreeViewModel>> SearchSuggestion(string tenantId, string userId, string keyWord, string targetTableId,
            TargetType? targetType, bool? isGetFinish);

        Task<ActionResultResponse> EquallyWeight(string tenantId, TargetType type, string targetGroupId, long? parentId);

        Task<SearchResult<TargetTreeViewModel>> GetTargetChildTree(string tenantId, string userId, long id);

        #region Attachments
        Task<ActionResultResponse> DeleteAttachment(string tenantId, string userId, string fullName, string avatar, long targetId, string attachmentId);

        Task<ActionResultResponse> InsertAttachment(string tenantId, string userId, string fullName, string avatar, long targetId,
            AttachmentMeta attachmentMeta);

        Task<ActionResultResponse<List<Attachment>>> InsertAttachments(string tenantId, string userId, string fullName, string avatar, long targetId, List<AttachmentMeta> attachmentMeta);
        #endregion

        #region History
        Task<SearchResult<Log>> SearchHistory(string tenantId, long targetId, string userId, int page, int pageSize);
        #endregion

        #region Report
        Task<SearchResult<ReportTargetViewModel>> SearchForReport(string tenantId, DateTime? startDate, DateTime? endDate, TypeSearchTime typeSearchTime,
          string tableTargetId, TargetType typeTarget, TargetReportType targetReportType, int? officeId, string userId);

        Task<SearchResult<TargetViewModel>> SearchReportDetail(string tenantId, TargetFilterReport targetFilterReport, int page, int pageSize);
        #endregion
    }
}
