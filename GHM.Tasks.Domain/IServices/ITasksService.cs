﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Tasks.Domain.IServices
{
    public interface ITasksService
    {
        Task<SearchResult<TasksSearchViewModel>> Search(TaskFilter taskFilter);

        Task<ActionResultResponse> UpdateResponsible(string tenantId, long id, string responsibleId, string currentUserId, string fullName);

        Task<ActionResultResponse> Delete(string tenantId, long id, string currentUserId, string fullName);

        Task<ActionResultResponse<long>> Insert(TaskMeta taskMeta);
      
        Task<ActionResultResponse<string>> UpdateStatus(string tenantId, string userId, string fullName,string avatar, TaskStatusMeta task);

        Task<ActionResultResponse<string>> UpdateDeadline(string tenantId, string userId, string fullName, long taskId,
            string concurrencyStamp, DateTime startDate, DateTime endDate);

        Task<ActionResultResponse<TaskDetailViewModel>> GetDetail(string tenantId, long taskId, string userId, string fullName, string avatar);

        Task<ActionResultResponse> UpdateNotification(long id1, string tenantId, string userId, string userName,
            string avatar, TaskNotificationMeta taskNotificationMeta);

        Task<ActionResultResponse<string>> Update(string tenantId, long taskId, TaskMeta taskMeta);

        Task<SearchResult<Log>> SearchHistory(string tenantId, long taskId, string userId, int page, int pageSize);

        Task<ActionResultResponse<dynamic>> InsertChecklist(CheckListsMeta checklistMeta);

        Task<ActionResultResponse<string>> UpdateChecklist(string id, CheckListsMeta checklistMeta);

        Task<ActionResultResponse> DeleteChecklist(string tenantId, string userId, long taskId, string checklistId);

        Task<ActionResultResponse<string>> UpdateCompleteStatus(string tenantId, string userId, long taskId, string checklistId, bool isComplete);

        #region Attachments
        Task<ActionResultResponse> DeleteAttachment(string tenantId, string userId, string fullName, string avatar, long taskId, string attachmentId);

        Task<ActionResultResponse> InsertAttachment(string tenantId, string userId, string fullName, string avatar, long taskId,
            AttachmentMeta attachmentMeta);

        Task<ActionResultResponse<List<Attachment>>> InsertAttachments(string tenantId, string userId, string fullName, string avatar, long taskId, List<AttachmentMeta> attachmentMeta);
        #endregion
        Task SendNotificationToParticipants(string tenantId, string creatorId, string creatorFullName, string creatorAvatar, long taskId, string taskName);

        Task<ActionResultResponse> UpdatePercentCompleted(string tenantId, long id, int percentCompleted);

        Task<SearchResult<TaskTreeViewModel>> GetTaskChildTree(string tenantId, string userId, long taskId);

        Task<SearchResult<ReportTaskViewModel>> SearchForReport(string tenantId, TaskReportFilter taskReportFilter);


        Task<SearchResult<TaskViewModel>> SearchReportDetail(string tenantId, TaskReportFilter taskReportFilter, int page, int pageSize);

        Task<ActionResultResponse<TaskNotification>> GetNotificationDetail(long id, string tenantId);
    }
}
