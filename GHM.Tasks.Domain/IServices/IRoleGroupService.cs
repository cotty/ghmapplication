﻿using System.Collections.Generic;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using System.Threading.Tasks;
using GHM.Tasks.Domain.ViewModels;

namespace GHM.Tasks.Domain.IServices
{
    public interface IRoleGroupService
    {
        Task<SearchResult<RoleGroup>> Search(string tenantId, string creatorId, string fullName, string keyword, int page, int pageSize);

        Task<ActionResultResponse> Insert(string tenantId, string creatorId, string creatorFullName, RoleGroupMeta roleGroupMeta);

        Task<ActionResultResponse> Update(string tenantId, string id, string lastUpdateUserId, string lastUpdateFullName, RoleGroupMeta roleGroupMeta);

        Task<ActionResultResponse> Delete(string tenantId, string roleGroupId);

        Task<List<RoleGroupViewModel>> SearchAll(string tenantId);
    }
}
