﻿using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IServices
{
    public interface ITargetGroupLibraryService
    {
        Task<SearchResult<TargetGroupLibrary>> Search(string tenantId, string keyword, int page, int pageSize);
        Task<ActionResultResponse> ForceDelete(string tenantId, string userId, string userFullName, string id);
        Task<ActionResultResponse> Update(string tenantId, string id,string userId, string userFullName, TargetGroupLibraryMeta targetGroupLibraryMeta);
        Task<ActionResultResponse> Insert(string tenantId, string userId, string userFullName, TargetGroupLibraryMeta targetGroupLibraryMeta);
        Task<SearchResult<TargetGroupLibraryViewModel>> GetAllAndTargetLibrary(string tenantId);
    }
}
