﻿using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IServices
{
    public interface ITasksCheckListsService
    {
        Task<SearchResult<CheckListsViewModel>> SearchByTasksTypeId(string tenantId, long taskId, TaskType type);

        Task<ActionResultResponse> ForceDelete(string tenantId, string userId, string userFullName, string id);

        Task<ActionResultResponse<string>> Update(string tenantId, string id, string userId, string userFullName, CheckListsMeta checkList);

        Task<ActionResultResponse<string>> Insert(string tenantId, string userId, string userFullName, CheckListsMeta checkList);
    }
}
