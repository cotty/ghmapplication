﻿using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IServices
{
    public interface ICommentService
    {
        Task<ActionResultResponse<Comment>> Post(string tenantId, string userId, string fullName, string avatar, CommentMeta commentMeta);

        Task<ActionResultResponse> ForceDelete(string tenantId, string userId, long id);

        Task<SearchResult<Comment>> GetsAllByObject(string tenantId, string userId, ObjectType objectType, long objectId, int page, int pageSize);
    }
}
