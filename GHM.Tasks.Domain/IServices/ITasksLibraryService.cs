﻿using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IServices
{
    public interface ITasksLibraryService
    {
        Task<SearchResult<TaskLibrary>> Search(string tenantId, string keyword, int page, int pageSize);

        Task<ActionResultResponse> ForceDelete(string tenantId, string userId, string userFullName, long id);

        Task<ActionResultResponse<string>> Update(string tenantId, long id, string userId, string userFullName, TasksLibraryMeta tasksGroupLibraryMeta);

        Task<ActionResultResponse<TaskLibrary>> Insert(string tenantId, string userId, string userFullName, TasksLibraryMeta tasksGroupLibraryMeta);
    }
}
