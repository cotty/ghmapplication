﻿using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.ViewModels;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IServices
{
    public interface ITargetTableService
    {
        Task<ActionResultResponse<string>> Insert(string tenantId, string creatorId, string creatorFullName,
            TargetTableMeta targetTableMeta);

        Task<ActionResultResponse<string>> Update(string id, string tenantId, string lastUpdateUserId, string lastUpdateFullName,
            TargetTableMeta targetTableMeta);

        Task<ActionResultResponse> Delete(string id, string tenantId);

        Task<ActionResultResponse> UpdateStatus(string tenantId, string id, TargetTableStatus status);

        Task<ActionResultResponse> UpdateOrder(string tenantId, string id, int order);

        Task<SearchResult<TargetTableSearchViewModel>> Search(string tenantId);

        Task<ActionResultResponse<TargetTableDetailViewModel>> GetDetail(string tenantId, string id, string creatorId, string cretorFullName);

        Task<SearchResult<TargetTableForSuggestionViewModel>> GetForSuggestion(string tenantId);
    }
}
