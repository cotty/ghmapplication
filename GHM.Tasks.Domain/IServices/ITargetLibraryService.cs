﻿using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IServices
{
    public interface ITargetLibraryService
    {
        Task<SearchResult<TargetGroupLibrary>> Search(string tenantId, string keyword, int page, int pageSize);

        Task<ActionResultResponse> ForceDelete(string tenantId, string userId, string userFullName, string id);

        Task<ActionResultResponse<string>> Update(string tenantId, string id, string userId, string userFullName, TargetLibraryMeta targetLibraryMeta);

        Task<ActionResultResponse<string>> Insert(string tenantId, string userId, string userFullName, TargetLibraryMeta targetLibraryMeta);
    }
}
