﻿using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IRepository
{
    public interface ITargetRepository
    {
        Task<List<TargetViewModel>> SearchTargetPersonal(string tenantId, string currentUserId, string currentOfficeIdPath,
            string userId, string keyword, DateTime? startDate, DateTime? endDate, string targetTableId, TargetStatus? status, List<int> listType);

        Task<List<TargetViewModel>> SearchTargetOffice(string tenantId, int officeId, string currentOfficeIdPath, string currentUserId,
            string keyword, DateTime? startDate, DateTime? endDate, string targetTableId, TargetStatus? status);

        Task<int> Delete(string tenantId, long id);

        Task<int> ForceDelete(string tenantId, long id);

        Task<int> Update(Target target);

        Task<int> Insert(Target target);

        Task<Target> GetInfo(string tenantId, long id, bool isReadOnly = false);

        Task<bool> CheckNameExist(string tenantId, string targetGroupId, string targetTableId, string name);

        Task<int> UpdateIdPath(long id, string idPath);

        Task<int> GetChildCount(string tenantId, long id);

        Task<int> GetChildCountComplete(string tenantId, long id);

        Task<int> UpdateChildCount(string tenantId, long id);

        Task<int> UpdateTotalComment(string tenantId, long id, int totalComment);

        Task<int> UpdateTotalTask(string tenantId, long id, int totalTask, int toalTaskComplete);

        Task<bool> CheckExist(string tenantId, long id);

        Task<int> GetCountByGroup(string tenantId, string targetGroupId);

        Task<int> GetCountByTargetTable(string tenantId, string targetTableId);

        Task<bool> CheckExists(string tenantId, long targetId, TargetType targetType);

        Task<List<TargetViewModel>> SearchSuggestion(string tenantId, string userId, string keyword, string targetTableId, TargetType? tagetType, bool? isGetFinish = true);

        Task<double?> SumWeightByParentId(string tenantId, string targetGroupId, long? parentId);

        Task<List<TargetViewModel>> GetTargetChildById(string tenantId, string userId, string idPath);

        Task<int> EquallyWeight(string tenantId, TargetType type, string targetGroupId, long? parentId);

        Task<List<Target>> GetAllTargetParent(string tenantId, long? parentId, string idPath, bool isReadOnly = false);

        Task<List<Target>> GetAllTargetChildren(string tenantId, string idPath, bool isReadOnly = false);

        Task<List<Target>> GetTargetChildren(string tenantId, long? parentId, bool isReadOnly = false);

        Task<List<ReportTargetViewModel>> SearchForReport(string tenantId, DateTime? startDate, DateTime? endDate, TypeSearchTime typeSearchTime,
            string tableTargetId, TargetType typeTarget, TargetReportType targetReportType, int? officeId, string userId, out int totalRows);

        Task<List<TargetViewModel>> SearchReportDetail(string tenantId, TargetFilterReport targetFilterReport, int page, int pageSize, out int totalRows);
        
        Task<int> GeneralPercentCompletedOfTarget(string tenantId, long? parentId, string idPath);

        Task<int> UpdatePercentCompleteRecursive(string tenantId, List<Target> listTargets, long? parentId);

        #region CheckPermision
        Task<bool> CheckPermission(string tenantId, Target targetInfo, string userId, TargetRole role);
        #endregion
    }
}
