﻿using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IRepository
{
    public interface ITasksCheckListsRepository
    {
        Task<List<CheckListsViewModel>> SearchByTasksTypeId(string tenantId, long taskId, TaskType type);

        Task<int> ForceDelete(string tenantId, string id);

        Task<int> UpdateAsync(string tenantId, string id, CheckList checkList);

        Task<int> Update(CheckList checkList);

        Task<int> InsertAsync(CheckList checkList);

        Task<CheckList> GetInfo(string tenantId, string id, bool isReadOnly = false);

        Task<CheckList> GetInfo(string tenantId, long taskId, string id, bool isReadOnly = false);

        Task<bool> CheckExistsByName(string tenantId, long taskId, TaskType type, string name);

        Task<int> Delete(string tenantId, long taskId, string checklistId);

        Task<int> UpdateCompleteStatus(string tenantId, long taskId, string checklistId, bool isComplete);

        Task<decimal> GetCompletedPercent(string tenantId, long taskId, TaskType type);

        Task<int> GetTotalCheckListByTask(string tenantId, long taskId, TaskType type, bool isCountComplete = false);
    }
}
