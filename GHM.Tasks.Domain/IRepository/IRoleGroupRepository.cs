﻿using GHM.Tasks.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Tasks.Domain.ViewModels;

namespace GHM.Tasks.Domain.IRepository
{
    public interface IRoleGroupRepository
    {
        Task<List<RoleGroup>> Search(string tenantId, string keyword, int page, int pageSize, out int totalRows);

        Task<int> Insert(string tenantId, RoleGroup roleGroup);

        Task<int> Inserts(string tenantId, List<RoleGroup> roleGroups);

        Task<int> Update(RoleGroup roleGroup);

        Task<int> Delete(string tenantId, string keyword);

        Task<RoleGroup> GetInfo(string tenantId, string keyword);

        Task<RoleGroup> GetInfoById(string tenantId, string id);

        Task<bool> CheckExistsByName(string tenantId, string name);

        Task<List<RoleGroupViewModel>> GetAll(string tenantId);
    }
}
