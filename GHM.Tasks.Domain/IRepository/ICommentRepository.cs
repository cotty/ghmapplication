﻿using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IRepository
{
    public interface ICommentRepository
    {
        Task<bool> CheckExist(string tenantId, long? id);

        Task<int> ForceDelete(string tenantId, long id);

        Task<Comment> GetAsync(string tenantId, long? id);

        Task<int> Insert(Comment comment);

        Task<int> UpdateChildCount(string tenantId, long? parentId);

        Task<int> GetTotalCommentObjectId(string tenantId, long objectId, ObjectType objectType);

        Task<List<Comment>> GetsAllByObjectId(string tenantId, ObjectType objectType, long objectId, int page, int pageSize, out int totalRows);
    }
}
