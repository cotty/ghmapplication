﻿using GHM.Tasks.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IRepository
{
    public interface ITasksLibraryRepository
    {
        Task<List<TaskGroupLibrary>> Search(string tenantId, long keyword, int page, int pageSize);

        Task<int> ForceDelete(string tenantId, string userId, string userFullName, long id);

        Task<int> Update(string tenantId, long id, TaskLibrary targetLibrary);

        Task<int> Insert(TaskLibrary taskLibrary);

        Task<TaskLibrary> GetInfo(string tenantId, long id, bool isReadOnly = false);

        Task<bool> CheckExistsByName(string tenantId, string taskGroupLibraryId, string name);
    }
}
