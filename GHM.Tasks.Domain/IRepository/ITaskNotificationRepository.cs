﻿using GHM.Tasks.Domain.Models;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IRepository
{
    public interface ITaskNotificationRepository
    {
        Task<TaskNotification> GetInfo(string id, string tenantId);

        Task<int> Insert(TaskNotification taskNotification);

        Task<int> Update(TaskNotification info);

        Task<TaskNotification> GetInfoByTaskId(long taskId, string tenantId, bool readOnly);
    }
}
