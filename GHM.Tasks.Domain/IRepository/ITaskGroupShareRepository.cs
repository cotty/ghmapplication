﻿using GHM.Tasks.Domain.Models;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IRepository
{
    public interface ITaskGroupShareRepository
    {
        Task<int> Insert(TaskGroupShare taskGroupShare);

        Task<int> Update(TaskGroupShare taskGroupShare);

        Task<TaskGroupShare> GetInfo(string tenantId, string taskGroupShareId, bool isReadOnly = false);

        Task<int> Delete(string tenantId, string taskGroupShareId);

        Task<bool> CheckExistsUserId(string tenantId, string groupId, string userId);
    }
}
