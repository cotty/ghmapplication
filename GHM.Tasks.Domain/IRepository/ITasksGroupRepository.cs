﻿using GHM.Tasks.Domain.Models;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IRepository
{
    public interface ITaskGroupRepository
    {
        Task<int> Insert(TaskGroup taskGroup);

        Task<int> Update(TaskGroup taskGroup);

        Task<TaskGroup> GetInfo(string tenantId, string taskGroupId, bool isReadOnly = false);

        Task<int> Delete(string tenantId, string taskGroupId);

        Task<bool> CheckExistsName(string tenantId, string name);

        Task<int> CountByActive(string tenantId);

        Task<int> SyncOrder(string tenantId);
    }
}
