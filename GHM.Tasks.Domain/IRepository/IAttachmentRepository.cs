﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.Models;

namespace GHM.Tasks.Domain.IRepository
{
    public interface IAttachmentRepository
    {
        Task<int> InsertAttachments(List<Attachment> attachments);

        Task<int> InsertAttachment(Attachment attachment);

        Task<int> Delete(string tenantId, string id);

        Task<bool> CheckExistsByFileIdObjectId(string tenantId, long objectId, ObjectType objectType, string fileId);

        Task<Attachment> GetInfo(string tenantId, string id, bool isReadOnly = false);

        Task<List<Attachment>> GetsAllByObjectId(string tenantId, ObjectType objectType, long objectId,
            int page, int pageSize, out int totalAttachment);
    }
}
