﻿using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IRepository
{
    public interface ITargetTableRepository
    {
        Task<int> Insert(TargetTable targetTable);

        Task<int> Update(TargetTable targetTable);

        Task<TargetTable> GetInfo(string tenantId, string targetTableId, bool isReadOnly = false);

        Task<int> Delete(string tenantId, string targetTableId);

        Task<List<TargetTableSearchViewModel>> Search(string tenantId);

        Task<bool> CheckExistsName(string tenantId, string name);

        Task<int> CountByActive(string tenantId);

        Task<int> SyncOrder(string tenantId);

        Task<List<TargetTableForSuggestionViewModel>> GetForSuggestion(string tenantId);
    }
}
