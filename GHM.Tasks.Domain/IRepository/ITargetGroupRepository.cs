﻿using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IRepository
{
    public interface ITargetGroupRepository
    {
        Task<int> Insert(TargetGroup targetGroup);

        Task<int> Inserts(List<TargetGroup> targetGroups);

        Task<int> Update(TargetGroup targetGroup);

        Task<int> Delete(string tenantId, string id);

        Task<TargetGroup> GetInfo(string tenantId, string id, bool isReadOnly = false);

        Task<List<TargetGroupSearchViewModel>> Search(string tenantId, string targetTableId);

        Task<bool> CheckExistsName(string tenantId, string targetTableId, string name);

        Task<int> CountByTargetTableId(string tenantId, string targetTableId);

        Task<int> SyncOrder(string tenantId, string targetTableId);

        Task<List<TargetGroupSuggestionViewModel>> GetTargetGroupByTargetTable(string tenantId, string targetTableId);

        Task<bool> CheckExist(string tenantId, string id);      
    }
}
