﻿using GHM.Tasks.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IRepository
{
    public interface ITargetParticipantRepository
    {
        Task<TargetParticipant> GetInfo(string tenantId, string id, bool isReadOnly = false);

        Task<TargetParticipant> GetInfo(string tenantId, long targetId, string userId, bool isReadOnly = false);

        Task<int> Update(TargetParticipant info);

        Task<int> InsertListParticipant(string tenantId, string userFullName, string userId,
            List<TargetParticipant> targetParticipantsMeta);

        Task<bool> CheckExistsByTargetIdUserId(string tenantId, long targetId, string userId);

        Task<int> FoceDeleteByTargetId(string tenantId, long targetId);

        Task<List<TargetParticipant>> GetListByTargetId(string tenantId, long id, out int totalRows);

        Task<int> InsertParticipants(TargetParticipant targetParticipant);

        Task<int> ForceDelete(string tenantId, string id);
    }
}
