﻿
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IRepository
{
    public interface ITargetGroupLibraryRepository
    {
        Task<List<TargetGroupLibrary>> Search(string tenantId, string keyword, int page, int pageSize);

        Task<int> ForceDelete(string tenantId, string userId, string userFullName, string id);

        Task<int> Update(TargetGroupLibrary targetGroupLibrary);

        Task<int> Insert(TargetGroupLibrary targetLibrary);

        Task<TargetGroupLibrary> GetInfo(string tenantId, string id, bool isReadOnly = false);

        Task<bool> CheckInfoByName(string tenantId, string name);

        Task<List<TargetGroupLibraryViewModel>> GetAllAndTargetLibrary(string tenatId);
      
    }
}
