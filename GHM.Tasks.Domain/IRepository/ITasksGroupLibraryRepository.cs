﻿using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IRepository
{
    public interface ITasksGroupLibraryRepository
    {
        Task<List<TaskGroupLibrary>> Search(string tenantId, string keyword, int page, int pageSize);

        Task<int> ForceDelete(string tenantId, string userId, string userFullName, string id);

        Task<int> Update(TaskGroupLibrary taskGroupLibrary);

        Task<int> Insert(TaskGroupLibrary taskGroupLibrary);

        Task<TaskGroupLibrary> GetInfo(string tenantId, string id, bool isReadOnly = false);

        Task<bool> CheckExistsByName(string tenantId, string name);

        Task<List<TaskGroupLibraryViewModel>> GetAllTasksLibrary(string tenantId);
    }
}
