﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;

namespace GHM.Tasks.Domain.IRepository
{
    public interface ITaskRoleGroupRepository
    {
        Task<List<TaskRoleGroupViewModel>> GetDetail(string tenantId, long taskId, string userId);

        Task<List<TaskRoleParticipantsViewModel>> GetDetail(string tenantId, long taskId);

        Task<int> Insert(List<TaskRoleGroup> taskRoleGroups);

        Task<bool> CheckExist(string tenantId, string taskParticipantsId, string roleGroupId);

        Task<int> Insert(string id, string roleGroupId, string tenantId);

        Task<List<TaskRoleGroup>> GetAllByParticipantId(string tenantId, string id);

        Task<int> Deletes(List<TaskRoleGroup> listRoleGroup);

        Task<int> DeleteAllById(string participantId, string tenantId);
    }
}
