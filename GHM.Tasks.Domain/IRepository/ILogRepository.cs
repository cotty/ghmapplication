﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.Models;

namespace GHM.Tasks.Domain.IRepository
{
    public interface ILogRepository
    {
        Task<int> Insert(Log log);

        Task<List<Log>> Search(string tenantId, ObjectType logType, long id, int page, int pageSize,
            out int totalRows);
    }
}
