﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;

namespace GHM.Tasks.Domain.IRepository
{
    using GHM.Tasks.Domain.ModelMetas;
    using Tasks = Models.Tasks;

    public interface ITasksRepository
    {
        Task<List<T>> Search<T>(Expression<Func<Tasks, T>> projector, string tenantId, string keyword, int? officeId, string userId, byte? status,
            DateTime? estmateStateDate, DateTime? estimateEndDate, int page, int pageSize, out int totalRows);

        Task<List<T>> SearchForManager<T>(Expression<Func<Tasks, T>> projector, string keyword, string userId, byte? status, DateTime? fromDateStart, DateTime? toDateStart, DateTime? fromDateEnd, DateTime? toDateEnd,
            bool? isOverdue, int page, int pageSize, out int totalRows);

        Task<List<T>> SearchForManager<T>(Expression<Func<Tasks, T>> projector, string keyword, List<string> listUser, byte? status, DateTime? fromDateStart, DateTime? toDateStart, DateTime? fromDateEnd, DateTime? toDateEnd,
            bool? isOverdue, int page, int pageSize, out int totalRows);

        Task<long> Insert(Tasks task);

        Task<List<Tasks>> GetsSubTasks(string tenantId, long parentId);

        Task<int> Update(Tasks task);

        Task<bool> CheckInForByName(string tenantId, string name);

        Task<ActionResultResponse> Update(Tasks task, string userId, string fullName);

        Task<int> UpdateChildCount(string tenantId, long taskId);

        Task<int> GetChildCountByTaskId(string tenantId, long taskId);

        Task<ActionResultResponse> UpdateDeadline(long id, DateTime deadline, string userId, string fullName);

        Task<List<ActionResultResponse>> UpdatesApproveFinish(List<long> taskIds, bool isApprove, string userId, string fullName);

        Task<Tasks> GetInfo(string tenantId, long id, bool isReadOnly = false);

        Task<bool> CheckExist(string tenantId, long id);

        Task<ActionResultResponse> Delete(string tenantId, long taskId);

        #region Tasks search by type
        Task<SearchResult<TasksSearchViewModel>> SearchProgressTask(string keyword, string currentUserId, string userId,
            int? officeId, DateTime? estimateStartDate, DateTime? estimateEndDate
            , int page, int pageSize);

        Task<SearchResult<TasksSearchViewModel>> SearchMyTaskOwned(string keyword, TasksStatus? status,
            string currentUserId, string userId, int? officeId,
            DateTime? estimateStartDate, DateTime? estimateEndDate, int page, int pageSize);

        Task<SearchResult<TasksSearchViewModel>> SearchParticipantOrFollowed(string keyword, TasksStatus? status,
            string currentUserId, string userId, int? officeId, bool isParticipant,
            DateTime? estimateStartDate, DateTime? estimateEndDate, int page, int pageSize);

        Task<SearchResult<TasksSearchViewModel>> SearchAllTask(string keyword, TasksStatus? status,
            string currentUserId, string userId, int? officeId,
            DateTime? estimateStartDate, DateTime? estimateEndDate, int page, int pageSize);

        Task<SearchResult<TasksSearchViewModel>> SearchTaskOverdue(string keyword, TasksStatus? status,
            string currentUserId, string userId, int? officeId,
            DateTime? estimateStartDate, DateTime? estimateEndDate, int page, int pageSize);

        #endregion
        Task<int> UpdateIdPath(string tenantId, long taskId, string taskIdPath);

        Task<List<TasksSearchViewModel>> Search(TaskFilter taskFilter, out int totalRows);

        Task<TaskDetailViewModel> GetDetail(string tenantId, long taskId);

        Task<int> GetTotalTaskByTargetId(string tenantId, long targetId);

        Task<int> GetTotalTaskCompleteByTargetId(string tenantId, long targetId);

        Task<int> UpdateTotalComment(string tenantId, long id, int totalComment);

        Task<int> DeleteTaskChild(string tenantId, long id);

        Task<List<TasksSearchViewModel>> GetTaskChilByTaskId(string tenantId, string idPath);

        Task<List<ReportTaskViewModel>> SearchForReport(string tenantId, TaskReportFilter taskFilterReport);

        Task<List<TaskViewModel>> SearchReportDetail(string tenantId, TaskReportFilter taskFilterReport, int page, int pageSize, out int totalRows);

        Task<int> GeneralPercentCompletedOfTask(string tenantId, long? parentId, string idPath);

        Task<int> UpdatePercentCompleteRecursive(string tenantId, List<Tasks> listTask, long? parentId);

        Task<List<Tasks>> GetAllTaskParent(string tenantId, long? parentId, string idPath, bool isReadOnly = false);

        Task<List<Tasks>> GetTaskChildren(string tenantId, long? parentId, bool isReadOnly = false);
    }
}
