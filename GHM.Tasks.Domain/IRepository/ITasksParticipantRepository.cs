﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;

namespace GHM.Tasks.Domain.IRepository
{
    public interface ITasksParticipantRepository : IRepositoryBase
    {
        Task<int> Save(TaskParticipant participant);

        Task<int> Inserts(List<TaskParticipant> listParticipant);

        Task<List<TaskParticipant>> GetsByTaskId(string tenantId, long taskId, bool isReadOnly = false);

        Task<bool> CheckExists(long taskId, string userId);

        Task<int> Delete(string tenantId, long taskId, string userId);

        Task<List<ParticipantViewModel>> GetsByTaskId(string tenantId, long taskId);

        // get all and role group
        Task<List<TaskRoleParticipantsViewModel>> GetsAllByTaskId(string tenantId, long taskId);

        Task<bool> CheckExist(string tenantId, long taskId, string userId);

        Task<TaskParticipant> GetInfo(string tenantId, long taskId, string userId);

        Task<int> FoceDeleteByTaskId(string tenantId, long taskId);
    }
}
