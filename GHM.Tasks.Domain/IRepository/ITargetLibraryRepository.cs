﻿using GHM.Tasks.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Tasks.Domain.IRepository
{
    public interface ITargetLibraryRepository
    {
        Task<List<TargetLibrary>> Search(string tenantId, string keyword, int page, int pageSize);

        Task<int> ForceDelete(string tenantId, string id);

        Task<int> Update(TargetLibrary targetLibrary);

        Task<int> Insert(TargetLibrary targetLibrary);

        Task<TargetLibrary> GetInfo(string tenantId, string id, bool isReadOnly = false);

        Task<bool> CheckInfoByName(string tenantId, string targetGroupLibraryId, string name);
    }
}
