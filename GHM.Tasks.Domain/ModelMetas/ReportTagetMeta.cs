﻿using GHM.Tasks.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Tasks.Domain.ModelMetas
{
    public class ReportTagetMeta
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public TypeSearchTime TypeSearchTime { get; set; }
        public string TargetTableId { get; set; }
        public TargetType TargetType { get; set; }
        public bool ShowOfficeHaveTarget { get; set; }
    }
}
