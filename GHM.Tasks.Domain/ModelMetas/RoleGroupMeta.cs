﻿using GHM.Tasks.Domain.Constants;

namespace GHM.Tasks.Domain.ModelMetas
{
    public class RoleGroupMeta
    {
        public TaskRole Role { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
    }
}
