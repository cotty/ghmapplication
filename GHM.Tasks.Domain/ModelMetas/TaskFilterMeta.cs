﻿using GHM.Tasks.Domain.Constants;
using System;

namespace GHM.Tasks.Domain.ModelMetas
{
    public class TaskReportFilter
    {
        public TaskReportType TaskReportType { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public TypeSearchTime TypeSearchTime { get; set; }
        public int? OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string TargetName { get; set; }
        public long? TargetId { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string TableTargetId { get; set; }
        public string TableTargetName { get; set; }
        public TasksStatus? Status { get; set; }
        public TasksKind? TaskType { get; set; }
    }
}
