﻿using GHM.Tasks.Domain.Constants;
using System;

namespace GHM.Tasks.Domain.ModelMetas
{
    public class AttachmentMeta
    {
        public long ObjectId { get; set; } // Mã mục tiêu hoặc công việc
        public ObjectType ObjectType { get; set; } // 0: Mục tiêu 1: Công việc
        public string FileId { get; set; }
        public string FileName { get; set; }
        public string Type { get; set; } // Loại tệp tin
        public string Extension { get; set; }
        public string Url { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreatorFullName { get; set; }
        public string CreatorId { get; set; }
    }
}
