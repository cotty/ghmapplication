﻿namespace GHM.Tasks.Domain.ModelMetas
{
    public class TargetGroupLibraryMeta
    {
        public string Name { get; set; }
        public string ConcurrencyStamp { get; set; }
    }
}
