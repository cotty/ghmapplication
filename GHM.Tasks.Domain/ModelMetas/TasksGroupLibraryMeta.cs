﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Tasks.Domain.ModelMetas
{
    public class TasksGroupLibraryMeta
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ConcurrencyStamp { get; set; }
    }
}
