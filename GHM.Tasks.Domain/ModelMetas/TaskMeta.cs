﻿using System;
using System.Collections.Generic;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;

namespace GHM.Tasks.Domain.ModelMetas
{
    public class TaskMeta
    {
        public string Name { get; set; }
        public DateTime EstimateStartDate { get; set; }
        public DateTime EstimateEndDate { get; set; }
        public DateTime? EndDate { get; set; }
        public long TargetId { get; set; }
        public TasksKind Type { get; set; }
        public string ResponsibleId { get; set; }
        public TasksStatus Status { get; set; }
        public string Description { get; set; }
        public string TenantId { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public long? ParentId { get; set; }
        public string ConcurrencyStamp { get; set; }
        public TaskMethodCalculateResult MethodCalculatorResult { get; set; }
        public decimal PercentCompleted { get; set; }

        public List<TaskParticipantMeta> Participants { get; set; }
        public List<AttachmentMeta> Attachments { get; set; }
    }

    public class TaskParticipantMeta
    {
        public string UserId { get; set; }
        public List<RoleGroupViewModel> RoleGroupIds { get; set; }
    }

    public class TaskStatusMeta
    {
        public long TaskId { get; set; }
        public string ConcurrencyStamp { get; set; }
        public TasksStatus Status { get; set; }
        public string Explanation { get; set; }
    }
}
