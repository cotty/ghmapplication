﻿namespace GHM.Tasks.Domain.ModelMetas
{
    public class TargetParticipantsMeta
    {
        public long TargetId { get; set; } // Mã mục tiêu
        public string UserId { get; set; }
        public int Role { get; set; }
        public string FullName { get; set; }
        public string Image { get; set; }
    }
}
