﻿using GHM.Tasks.Domain.Constants;

namespace GHM.Tasks.Domain.ModelMetas
{
    public class CheckListsMeta
    {
        public string TenantId { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public long TaskId { get; set; }
        public string Name { get; set; }
        public TaskType Type { get; set; } // Loại check list dùng cho công việc , 0 Thư viện công việc 1 Công việc
        public bool Status { get; set; } // terang thai toan thanh
        public string ConcurrencyStamp { get; set; }
    }
}
