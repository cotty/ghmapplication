﻿using GHM.Infrastructure.Constants;
using GHM.Tasks.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Tasks.Domain.ModelMetas
{
    public class TaskNotificationMeta
    {
        public string Id { get; set; }
        public bool IsNotification { get; set; }
        public DateTime Dates { get; set; }
        public int Hours { get; set; }
        public int Minutes { get; set; }
        public bool IsSendNotificationEmail { get; set; }
        public bool IsSendNotification { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string TasksName { get; set; }
        public ScheduleType ScheduleType { get; set; }
    }
}
