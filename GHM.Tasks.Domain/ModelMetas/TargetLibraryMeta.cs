﻿namespace GHM.Tasks.Domain.ModelMetas
{
    public class TargetLibraryMeta
    {
        public string TargetGroupLibraryId { get; set; }
        public string TargetGroupLibraryName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string MeasurementMethod { get; set; }
        public string ConcurrencyStamp { get; set; }
    }
}
