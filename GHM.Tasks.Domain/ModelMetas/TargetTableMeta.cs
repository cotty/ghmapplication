﻿using GHM.Tasks.Domain.Constants;
using System;

namespace GHM.Tasks.Domain.ModelMetas
{
    public class TargetTableMeta
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public TargetTableType Type { get; set; }
        public string ConcurrencyStamp { get; set; }
    }
}
