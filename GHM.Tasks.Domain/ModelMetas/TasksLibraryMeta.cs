﻿using GHM.Tasks.Domain.Constants;

namespace GHM.Tasks.Domain.ModelMetas
{
    public class TasksLibraryMeta
    {
        public string TaskGroupLibraryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskMethodCalculateResult MethodCalculateResult { get; set; }
        public string ConcurrencyStamp { get; set; }
    }
}
