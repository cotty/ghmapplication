﻿using GHM.Tasks.Domain.Constants;
using System;
using System.Collections.Generic;

namespace GHM.Tasks.Domain.ModelMetas
{
    public class TargetMeta
    {
        public string TargetTableId { get; set; } // Id của bảng mục tiêu
        public string TargetGroupId { get; set; } // Id của nhóm mục tiêu
        public long? ParentId { get; set; }
        public string Name { get; set; } // Tên mục tiêu
        public TargetType Type { get; set; } // Loại mục tiêu. 1 Mục tiêu cá nhân 0 Mục tiêu đơn vị.
        public string UserId { get; set; } // Mã nhân viên phụ trách thực hiện mục tiêu nếu là mục tiêu cá nhân
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public string TitleId { get; set; }
        public string TitleName { get; set; }
        public int? OfficeId { get; set; } // Mã phòng ban phụ trách mục tiêu nếu mục tiêu là đơn vị
        public string OfficeName { get; set; }
        public string OfficeIdPath { get; set; }
        public string Description { get; set; }
        public LevelOfSharing LevelOfSharing { get; set; } // Mức độ chia sẻ.  0 Riêng 1 Chung
        public double? Weight { get; set; } // Trọng số
        public DateTime StartDate { get; set; } // Ngày bắt đầu
        public DateTime EndDate { get; set; } // Ngày kết thúc
        public string MeasurementMethod { get; set; }
        public TargetMethodCalculateResultType MethodCalculateResult { get; set; } // Phương pháp tính kết quả
        public double? PercentCompleted { get; set; } // Phần trăm hoàn thành
        public TargetStatus? Status { get; set; } // Trạng thái. 0 Chưa hoàn thành 1 Hoàn thành
        public string ConcurrencyStamp { get; set; }
        public List<TargetParticipantsMeta> TargetParticipants { get; set; }
        public List<AttachmentMeta> TargetAttachments { get; set; }
    }

    public class TargetUpdateMeta
    {
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CompleteDate { get; set; }
        public string Name { get; set; }
        public string TableId { get; set; }
        public string GroupId { get; set; }
        public string MeasurementMethod { get; set; }
    }
}
