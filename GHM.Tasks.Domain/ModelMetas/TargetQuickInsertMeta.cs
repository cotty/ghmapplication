﻿using GHM.Tasks.Domain.Constants;
using System;

namespace GHM.Tasks.Domain.ModelMetas
{
    public class TargetQuickInsertMeta
    {
        public long? ParentId { get; set; }
        public string Name { get; set; } // Tên mục tiêu
        public string UserId { get; set; } // Mã nhân viên phụ trách thực hiện mục tiêu nếu là mục tiêu cá nhân     
        public int? OfficeId { get; set; } // Mã phòng ban phụ trách mục tiêu nếu mục tiêu là đơn vị
        public string Description { get; set; }
        public LevelOfSharing LevelOfSharing { get; set; } // Mức độ chia sẻ.  0 Riêng 1 Chung
        public double? Weight { get; set; } // Trọng số
        public DateTime StartDate { get; set; } // Ngày bắt đầu
        public DateTime EndDate { get; set; } // Ngày kết thúc
        public string ConcurrencyStamp { get; set; }
    }
}
