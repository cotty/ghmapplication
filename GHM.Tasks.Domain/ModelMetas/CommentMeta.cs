﻿using GHM.Tasks.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Tasks.Domain.ModelMetas
{
    public class CommentMeta
    {
        public string Content { get; set; } // Nội dung comment
        public long ObjectId { get; set; } // Đối tượng tham chiếu
        public ObjectType ObjectType { get; set; } // Loại đối tượng tham chiếu. Xem trong fileEnum c#
        public long? ParentId { get; set; } // Mã comment cha
        public string Image { get; set; } // Ảnh người comment
        public string UrlAttachment { get; set; }
    }
}
