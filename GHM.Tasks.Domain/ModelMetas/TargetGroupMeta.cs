﻿namespace GHM.Tasks.Domain.ModelMetas
{
    public class TargetGroupMeta
    {
        public string Name { get; set; } // Tên nhóm mục tiêu
        public string Description { get; set; }
        public string Color { get; set; } // Mã màu hiển thị
        public string ConcurrencyStamp { get; set; }
    }
}
