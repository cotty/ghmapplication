﻿using GHM.Core.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Core.Infrastructure.Configurations
{
    public class AppSettingConfig : IEntityTypeConfiguration<AppSetting>
    {
        public void Configure(EntityTypeBuilder<AppSetting> builder)
        {
            builder.Property(x => x.Key).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.TenantId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.ConcurrencyStamp).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.CreateTime).IsRequired().HasColumnType("datetime");
            builder.Property(x => x.LastUpdate).IsRequired(false).HasColumnType("datetime");
            builder.Property(x => x.CreatorId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.CreatorFullName).IsRequired().HasMaxLength(50).IsUnicode();
            builder.Property(x => x.GroupId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.LanguageId).IsRequired().HasMaxLength(10).IsUnicode(false);
            builder.Property(x => x.LastUpdateFullName).IsRequired(false).HasMaxLength(50).IsUnicode();
            builder.Property(x => x.LastUpdateUserId).IsRequired(false).HasMaxLength(50).IsUnicode();
            builder.Property(x => x.Value).IsRequired().HasMaxLength(4000).IsUnicode();
            builder.ToTable("AppSettings").HasKey(x => new { x.TenantId, x.Key });
        }
    }
}
