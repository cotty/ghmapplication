﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Core.Domain.IRepository;
using GHM.Core.Domain.Models;
using GHM.Core.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;

namespace GHM.Core.Infrastructure.Repository
{
    public class TenantRepository : RepositoryBase, ITenantRepository
    {
        private readonly IRepository<Tenant> _tenantRepository;
        public TenantRepository(IDbContext context) : base(context)
        {
            _tenantRepository = Context.GetRepository<Tenant>();
        }

        public async Task<int> Insert(Tenant tenant)
        {
            _tenantRepository.Create(tenant);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(Tenant tenant)
        {         
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateActiveStatus(string id, bool isActive)
        {
            var info = await GetInfo(id);
            if (info == null)
                return -1;

            info.IsActive = isActive;
            return await Context.SaveChangesAsync();
        }

        public async Task<Tenant> GetInfo(string id)
        {
            return await _tenantRepository.GetAsync(false, x => x.Id == id);
        }

        public async Task<bool> CheckExists(string id, string phoneNumber, string email)
        {
            return await _tenantRepository.ExistAsync(x =>
                x.Id != id && (x.PhoneNumber == phoneNumber || x.Email == email));
        }

        public Task<List<TenantSearchViewModel>> Search(string keyword, bool? isActive, int page, int pageSize, out int totalRows)
        {
            Expression<Func<Tenant, bool>> spec = x => true;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripVietnameseChars().ToLower();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (isActive.HasValue)
                spec = spec.And(x => x.IsActive == isActive.Value);

            var sort = Context.Filters.Sort<Tenant, DateTime>(x => x.CreateTime, true);
            var paging = Context.Filters.Page<Tenant>(page, pageSize);

            totalRows = _tenantRepository.Count(spec);
            return _tenantRepository.GetsAsAsync(x => new TenantSearchViewModel
            {
                Name = x.Name,
                Id = x.Id,
                CreateTime = x.CreateTime,
                Address = x.Address,
                Email = x.Email,
                PhoneNumber = x.PhoneNumber,
                Note = x.Note,
                IsActive = x.IsActive
            }, spec, sort, paging);
        }

        public async Task<int> Delete(string id)
        {
            var tenantInfo = await GetInfo(id);
            if (tenantInfo == null)
                return -1;

             _tenantRepository.Delete(tenantInfo);
            return await Context.SaveChangesAsync();
        }
    }
}
