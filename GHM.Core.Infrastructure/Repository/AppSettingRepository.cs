﻿using System;
using System.Threading.Tasks;
using GHM.Core.Domain.IRepository;
using GHM.Core.Domain.Models;
using GHM.Core.Domain.ViewModels;
using GHM.Infrastructure.SqlServer;

namespace GHM.Core.Infrastructure.Repository
{
    public class AppSettingRepository : RepositoryBase, IAppSettingRepository
    {
        private readonly IRepository<AppSetting> _appSettingRepository;
        public AppSettingRepository(IDbContext context) : base(context)
        {
            _appSettingRepository = Context.GetRepository<AppSetting>();
        }

        public async Task<int> Save(AppSetting appSetting)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Insert(AppSetting appSetting)
        {
            _appSettingRepository.Create(appSetting);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(AppSetting appSetting)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<AppSettingViewModel> GetSetting(string tenantId)
        {
            throw new NotImplementedException();
        }

        public async Task<AppSetting> GetInfo(string tenantId, string key, string languageId, bool isReadOnly = false)
        {
            return await _appSettingRepository.GetAsync(isReadOnly,
                x => x.TenantId == tenantId && x.LanguageId == languageId && x.Key == key);
        }
    }
}
