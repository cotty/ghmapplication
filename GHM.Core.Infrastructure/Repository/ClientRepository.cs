﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Core.Domain.IRepository;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;
using Client = GHM.Core.Domain.Models.Client;

namespace GHM.Core.Infrastructure.Repository
{
    public class ClientRepository : RepositoryBase, IClientRepository
    {
        private readonly IRepository<Client> _clientRepository;

        public ClientRepository(IDbContext context) : base(context)
        {
            _clientRepository = Context.GetRepository<Client>();
        }

        public async Task<Client> GetInfo(string clientId, bool isReadOnly = false)
        {
            return await _clientRepository.GetAsync(isReadOnly, x => x.ClientId == clientId && !x.IsDelete);
        }

        public async Task<int> Insert(Client client)
        {
            _clientRepository.Create(client);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(Client client)
        {
            var clientInfo = await GetInfo(client.ClientId);
            if (clientInfo == null)
                return -1;

            clientInfo = client;
            Context.Entry(clientInfo).State = EntityState.Modified;
            Context.Entry(clientInfo).Property(x => x.ClientId).IsModified = false;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string clientId)
        {
            var info = await GetInfo(clientId);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<bool> CheckExists(string clientId)
        {
            return await _clientRepository.ExistAsync(x => x.ClientId == clientId && !x.IsDelete);
        }

        public async Task<bool> CheckNameExists(string clientId, string clientName)
        {
            return await _clientRepository.ExistAsync(x => x.ClientId != clientId && x.ClientName == clientName && !x.IsDelete);
        }

        public Task<List<Client>> Search(string keyword, bool? enabled, int page, int pageSize, out int totalRows)
        {
            Expression<Func<Client, bool>> spec = x => !x.IsDelete;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripVietnameseChars().ToLower();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (enabled.HasValue)
            {
                spec = spec.And(x => x.Enabled == enabled.Value);
            }

            var sort = Context.Filters.Sort<Client, DateTime>(a => a.CreateTime);
            var paging = Context.Filters.Page<Client>(page, pageSize);

            totalRows = _clientRepository.Count(spec);
            return _clientRepository.GetsAsync(true, spec, sort, paging);
        }
    }
}
