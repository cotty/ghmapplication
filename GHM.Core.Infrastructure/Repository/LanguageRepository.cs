﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Core.Domain.IRepository;
using GHM.Core.Domain.Models;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.SqlServer;

namespace GHM.Core.Infrastructure.Repository
{
    public class LanguageRepository : RepositoryBase, ILanguageRepository
    {
        private readonly IRepository<Language> _languageRepository;
        public LanguageRepository(IDbContext context) : base(context)
        {
            _languageRepository = Context.GetRepository<Language>();
        }

        public async Task<int> Insert(Language language)
        {
            _languageRepository.Create(language);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(Language language)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<List<T>> GetAllLanguage<T>(Expression<Func<Language, T>> projector)
        {
            return await _languageRepository.GetsAsAsync(projector, x => x.IsActive);
        }
    }
}
