﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using GHM.Core.Domain.ModelMetas;
using GHM.Core.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Helpers.Validations;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;

namespace GHM.Core.Infrastructure.Validations
{
    public class ClientMetaValidator : AbstractValidator<ClientMeta>
    {
        public ClientMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmCoreResource> resourceService)
        {
            RuleFor(x => x.ClientName).NotEmpty().WithMessage("Tên client không được để trống.");
            RuleFor(x => x.ClientName).MaximumLength(250).WithMessage("Tên client phải nhỏ hơn 250 ký tự.");
            RuleFor(x => x.ClientAllowedGrantTypes)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseSelect, resourceService.GetString("grant type")));
            RuleFor(x => x.ClientSecret).NotEmpty().When(x => x.RequireClientSecret).WithMessage("Vui lòng nhập nội dung khóa bí mật ứng dụng.");
        }
    }
}
