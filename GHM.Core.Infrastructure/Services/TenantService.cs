﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Core.Domain.IRepository;
using GHM.Core.Domain.IServices;
using GHM.Core.Domain.ModelMetas;
using GHM.Core.Domain.Models;
using GHM.Core.Domain.Resources;
using GHM.Core.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;

namespace GHM.Core.Infrastructure.Services
{
    public class TenantService : ITenantService
    {
        private readonly ITenantRepository _tenantRepository;
        private readonly ITenantLanguageRepository _tenantLanguageRepository;
        private readonly ITenantPageRepository _tenantPageRepository;
        private readonly IUserAccountRepository _userAccountRepository;
        private readonly IUserRoleRepository _userRoleRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmCoreResource> _resourceService;

        public TenantService(ITenantRepository tenantRepository, IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmCoreResource> resourceService, ITenantLanguageRepository tenantLanguageRepository,
            ITenantPageRepository tenantPageRepository, IUserAccountRepository userAccountRepository, IUserRoleRepository userRoleRepository)
        {
            _tenantRepository = tenantRepository;
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
            _tenantLanguageRepository = tenantLanguageRepository;
            _tenantPageRepository = tenantPageRepository;
            _userAccountRepository = userAccountRepository;
            _userRoleRepository = userRoleRepository;
        }

        public async Task<ActionResultResponse> Insert(TenantMeta tenantMeta)
        {
            if (tenantMeta.Languages == null || !tenantMeta.Languages.Any())
                return new ActionResultResponse(-1, _resourceService.GetString("Please select at least one language."));

            var tenantId = Guid.NewGuid().ToString();
            var isExists = await _tenantRepository.CheckExists(tenantId, tenantMeta.PhoneNumber, tenantMeta.Email);
            if (isExists)
                return new ActionResultResponse(-2, _resourceService.GetString("Email or phone number already exists."));

            var tenant = new Tenant(tenantId, tenantMeta.Name, tenantMeta.Email, tenantMeta.PhoneNumber, tenantMeta.Address,
                tenantMeta.IsActive, tenantMeta.Note, tenantMeta.Logo);

            var result = await _tenantRepository.Insert(tenant);
            if (result <= 0)
                return new ActionResultResponse(-3, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            // Insert tenant languages.
            if (tenantMeta.Languages != null && tenantMeta.Languages.Any())
            {
                var listLanguage = new List<TenantLanguage>();
                foreach (var tenantMetaLanguage in tenantMeta.Languages)
                {
                    listLanguage.Add(new TenantLanguage
                    {
                        TenantId = tenant.Id,
                        LanguageId = tenantMetaLanguage.LanguageId,
                        Name = tenantMetaLanguage.Name,
                        IsActive = tenantMetaLanguage.IsActive,
                        IsDefault = tenantMetaLanguage.IsDefault,
                        IsDelete = false
                    });
                }

                var countTenantLanguageIsDefault = listLanguage.Count(x => x.IsDefault);
                if (countTenantLanguageIsDefault > 1 || countTenantLanguageIsDefault == 0)
                {
                    await RolllBackInsertTenant();
                    return new ActionResultResponse(-4, _sharedResourceService.GetString("Có 2 ngôn ngữ mặc định. Vui lòng chỉ chọn một ngôn ngữ mặc định."));
                }

                var resultInsertLanguage = await _tenantLanguageRepository.Inserts(listLanguage);
                if (resultInsertLanguage <= 0)
                {
                    await RolllBackInsertTenant();
                    return new ActionResultResponse(-5, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
                }
            }

            if (tenantMeta.PageIds != null && tenantMeta.PageIds.Any())
            {
                var listTenantPage = new List<TenantPage>();
                foreach (var pageId in tenantMeta.PageIds)
                {
                    listTenantPage.Add(new TenantPage
                    {
                        TenantId = tenant.Id,
                        PageId = pageId,
                        IsDelete = false
                    });
                }

                var resultInsertPage = await _tenantPageRepository.Inserts(listTenantPage);
                if (resultInsertPage <= 0)
                {
                    await RollBackTenantLanguage();
                    await RolllBackInsertTenant();

                    return new ActionResultResponse(-6, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
                }
            }

            // Insert UserAccount
            UserAccount userAccountInfo = await _userAccountRepository.GetAccountInfoByUserName(tenantMeta.UserName, true);
            if (userAccountInfo != null)
            {
                await RollBackTenantpage();
                await RollBackTenantLanguage();
                await RolllBackInsertTenant();
                return new ActionResultResponse(-1, _sharedResourceService.GetString("Tài khoản đã tồn tại."));
            }

            byte[] passwordSalt = Generate.GenerateRandomBytes(Generate.PasswordSaltLength);
            byte[] passwordHash = Generate.GetInputPasswordHash(tenantMeta.Password, passwordSalt);

            UserAccount userAccount = new UserAccount
            {
                TenantId = tenant.Id,
                Id = tenant.Id,
                UserName = tenantMeta.UserName.Trim(),
                NormalizedUserName = tenantMeta.UserName.Trim().ToUpper(),
                FullName = tenantMeta.Name.Trim(),
                Avatar = null,
                PasswordHash = Convert.ToBase64String(passwordHash),
                PasswordSalt = passwordSalt,
            };

            int resultInsertAcount = await _userAccountRepository.Insert(userAccount);
            if (resultInsertAcount <= 0)
            {
                await RollBackTenantpage();
                await RollBackTenantLanguage();
                await RolllBackInsertTenant();
            }

            // Insert User Role
            await _userRoleRepository.Insert(tenant.Id, "SuperAdministrator");

            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                : _resourceService.GetString("Add new tenant successful."));

            async Task RolllBackInsertTenant()
            {
                await _tenantRepository.Delete(tenant.Id);
            }

            async Task RollBackTenantLanguage()
            {
                await _tenantLanguageRepository.DeleteByTenantId(tenant.Id);
            }

            async Task RollBackTenantpage()
            {
                await _tenantPageRepository.DeleteByTenantId(tenant.Id);
            }
        }

        public async Task<ActionResultResponse> Update(string id, TenantMeta tenantMeta)
        {
            if (tenantMeta.Languages == null || !tenantMeta.Languages.Any())
                return new ActionResultResponse(-1, _resourceService.GetString("Please select at least one language."));

            var isExists = await _tenantRepository.CheckExists(id, tenantMeta.PhoneNumber, tenantMeta.Email);
            if (isExists)
                return new ActionResultResponse(-2, _resourceService.GetString("Email or phone number already exists."));

            var tenantInfo = await _tenantRepository.GetInfo(id);
            if (tenantInfo == null)
                return new ActionResultResponse(-3, _resourceService.GetString("Tenant does not exists."));

            tenantInfo.Name = tenantMeta.Name.Trim();
            tenantInfo.Email = tenantMeta.Email?.Trim();
            tenantInfo.PhoneNumber = tenantMeta.PhoneNumber?.Trim();
            tenantInfo.IsActive = tenantMeta.IsActive;
            tenantInfo.Address = tenantMeta.Address?.Trim();
            tenantInfo.Note = tenantMeta.Note;
            tenantInfo.UnsignName = $"{tenantInfo.Name.StripVietnameseChars().ToUpper()} {tenantInfo.Email?.StripVietnameseChars().ToUpper()} {tenantInfo.PhoneNumber?.StripVietnameseChars().ToUpper()}";
            tenantInfo.Logo = tenantMeta.Logo;

            var result = await _tenantRepository.Update(tenantInfo);
            if (result < 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            // Update TenantLanguage
            var countTenantLanguageIsDefault = tenantMeta.Languages.Count(x => x.IsDefault);
            if (countTenantLanguageIsDefault > 1 || countTenantLanguageIsDefault == 0)
                return new ActionResultResponse(-4, _sharedResourceService.GetString("Vui lòng chỉ chọn một ngôn ngữ mặc định."));

            await _tenantLanguageRepository.DeleteByTenantId(id);
            if (tenantMeta.Languages != null && tenantMeta.Languages.Any())
            {
                var listLanguage = new List<TenantLanguage>();
                foreach (var tenantMetaLanguage in tenantMeta.Languages)
                {
                    listLanguage.Add(new TenantLanguage
                    {
                        TenantId = id,
                        LanguageId = tenantMetaLanguage.LanguageId,
                        Name = tenantMetaLanguage.Name,
                        IsActive = tenantMetaLanguage.IsActive,
                        IsDefault = tenantMetaLanguage.IsDefault,
                        IsDelete = false
                    });
                }

                var resultInsertLanguage = await _tenantLanguageRepository.Inserts(listLanguage);
                if (resultInsertLanguage <= 0)
                    return new ActionResultResponse(-5, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
            }

            // Update TenantPage
            await _tenantPageRepository.DeleteByTenantId(id);
            if (tenantMeta.PageIds != null && tenantMeta.PageIds.Any())
            {
                var listTenantPage = new List<TenantPage>();
                foreach (var pageId in tenantMeta.PageIds)
                {
                    listTenantPage.Add(new TenantPage()
                    {
                        TenantId = id,
                        PageId = pageId,
                        IsDelete = false
                    });
                }

                await _tenantPageRepository.Inserts(listTenantPage);
            }

            return new ActionResultResponse(1, _resourceService.GetString("Update tenant {0} successful.", tenantInfo.Name));
        }

        public async Task<ActionResultResponse> UpdateActiveStatus(string id, bool isActive)
        {
            var result = await _tenantRepository.UpdateActiveStatus(id, isActive);
            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                : _resourceService.GetString(isActive ? "Active tenant successful." : "Deactive tenant successful."));
        }

        public async Task<SearchResult<TenantSearchViewModel>> Search(string keyword, bool? isActive, int page, int pageSize)
        {
            return new SearchResult<TenantSearchViewModel>
            {
                Items = await _tenantRepository.Search(keyword, isActive, page, pageSize, out var totalRows),
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse<TenantDetailViewModel>> Detail(string id)
        {
            var tenantInfo = await _tenantRepository.GetInfo(id);
            if (tenantInfo == null)
                return new ActionResultResponse<TenantDetailViewModel>(-3, _resourceService.GetString("Tenant does not exists."));

            var listTenantLanguage = await _tenantLanguageRepository.GetLanguageByTenantId(id);
            var listPage = await _tenantPageRepository.GetListByTenantId(id, true);
            var userAccountInfo = await _userAccountRepository.GetInfo(id, true);

            var tenantDetail = new TenantDetailViewModel
            {
                Id = tenantInfo.Id,
                Name = tenantInfo.Name,
                PhoneNumber = tenantInfo.PhoneNumber,
                Email = tenantInfo.Email,
                Address = tenantInfo.Address,
                IsActive = tenantInfo.IsActive,
                Note = tenantInfo.Note,
                Logo = tenantInfo.Logo,
                UserName = userAccountInfo?.UserName,
                Password = "123456",
                Languages = listTenantLanguage,
                PageIds = listPage != null && listPage.Any() ? listPage.Select(x => x.PageId).ToList() : null,
            };

            return new ActionResultResponse<TenantDetailViewModel>
            {
                Code = 1,
                Data = tenantDetail
            };
        }
    }
}
