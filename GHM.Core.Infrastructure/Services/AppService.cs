﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using GHM.Core.Domain.IRepository;
using GHM.Core.Domain.IServices;
using GHM.Core.Domain.Models;
using GHM.Core.Domain.Resources;
using GHM.Core.Domain.ViewModels;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using UserSetting = GHM.Core.Domain.ViewModels.UserSetting;

namespace GHM.Core.Infrastructure.Services
{
    public class AppService : IAppService
    {
        private readonly IAppSettingRepository _appSettingRepository;
        private readonly IPageRepository _pageRepository;
        private readonly IUserRoleRepository _userRoleRepository;
        private readonly IRolePageRepository _rolePageRepository;
        private readonly IUserSettingRepository _userSettingRepository;
        private readonly ITenantLanguageRepository _tenantLanguageRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmCoreResource> _resourceService;

        public AppService(IPageRepository pageRepository, IRolePageRepository rolePageRepository, IUserSettingRepository userSettingRepository,
            ITenantLanguageRepository tenantLanguageRepository, IUserRoleRepository userRoleRepository, IAppSettingRepository appSettingRepository,
            IResourceService<SharedResource> sharedResourceService, IResourceService<GhmCoreResource> resourceService)
        {
            _pageRepository = pageRepository;
            _rolePageRepository = rolePageRepository;
            _userSettingRepository = userSettingRepository;
            _tenantLanguageRepository = tenantLanguageRepository;
            _userRoleRepository = userRoleRepository;
            _appSettingRepository = appSettingRepository;
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
        }

        public async Task<AppSettingViewModel> GetAppSettings(string userId, string tenantId, string languageId)
        {
            var appSetting = new AppSettingViewModel();
            var isSupperAdmin = await _userRoleRepository.CheckIsSupperAdmin(userId);
            var logoSetting = await _appSettingRepository.GetInfo(tenantId,
                ClassHelper.GetPropertyNameAsKey<GeneralSetting>("LogoUrl"), CultureInfo.CurrentCulture.Name);
            var defaultAppTitle = await _appSettingRepository.GetInfo(tenantId,
                ClassHelper.GetPropertyNameAsKey<GeneralSetting>("Title"), CultureInfo.CurrentCulture.Name);
            appSetting.UserSettings = await _userSettingRepository.GetsByUserId(x => new UserSetting
            {
                Key = x.Key,
                Value = x.Value
            }, userId);
            appSetting.Permissions = await _rolePageRepository.GetsByUserId(userId);
            appSetting.Languages = await _tenantLanguageRepository.GetAllLanguageActived(tenantId);
            appSetting.Pages = isSupperAdmin
            ? await _pageRepository.GetAllActivePage(languageId, tenantId)
            : await _pageRepository.GetPagesByUserId(userId, languageId);
            appSetting.LogoUrl = logoSetting != null ? logoSetting.Value : string.Empty;
            appSetting.DefaultAppTitle = defaultAppTitle != null ? defaultAppTitle.Value : string.Empty;
            return appSetting;
        }

        public async Task<List<TenantLanguageViewModel>> GetLanguages(string tenantId)
        {
            return await _tenantLanguageRepository.GetAllLanguage(tenantId);
        }

        public async Task<List<RolesPagesViewModel>> GetPermissions(string userId)
        {
            return await _rolePageRepository.GetsByUserId(userId);
        }

        public async Task<List<UserSetting>> GetSettings(string userId)
        {
            return await _userSettingRepository.GetsByUserId(x => new UserSetting
            {
                Key = x.Key,
                Value = x.Value
            }, userId);
        }

        public async Task<ActionResultResponse> SaveGeneralSetting(string tenantId, string userId, string fullName, GeneralSetting generalSetting)
        {
            var properties = generalSetting.GetType().GetProperties();
            foreach (var property in properties)
            {
                var value = property.GetValue(generalSetting)?.ToString();
                if (!string.IsNullOrEmpty(value))
                {
                    var key = ClassHelper.GetPropertyNameAsKey<GeneralSetting>(property.Name);
                    var appSettingInfo = await _appSettingRepository.GetInfo(tenantId, key, CultureInfo.CurrentCulture.Name);
                    if (appSettingInfo != null)
                    {
                        appSettingInfo.Value = value;
                        appSettingInfo.LastUpdate = DateTime.Now;
                        appSettingInfo.LastUpdateUserId = userId;
                        appSettingInfo.LastUpdateFullName = fullName;
                        await _appSettingRepository.Update(appSettingInfo);
                    }
                    else
                    {
                        var appSetting = new AppSetting
                        {
                            Key = ClassHelper.GetPropertyNameAsKey<GeneralSetting>(property.Name),
                            Value = value,
                            TenantId = tenantId,
                            CreatorId = userId,
                            CreatorFullName = fullName,
                            GroupId = ClassHelper.ClassName<AppSetting>(),
                            LanguageId = CultureInfo.CurrentCulture.Name,
                        };
                        await _appSettingRepository.Insert(appSetting);
                    }
                }
            }

            return new ActionResultResponse(1,
                _sharedResourceService.GetString(SuccessMessage.UpdateSuccessful,
                    _resourceService.GetString("App setting")));
        }

        public async Task<ActionResultResponse<GeneralSetting>> GetGeneralSetting(string tenantId)
        {
            var generalSetting = new GeneralSetting();
            foreach (var property in generalSetting.GetType().GetProperties())
            {
                var setting = await _appSettingRepository.GetInfo(tenantId,
                    ClassHelper.GetPropertyNameAsKey<GeneralSetting>(property.Name), CultureInfo.CurrentCulture.Name, true);

                if (setting == null)
                    continue;

                property.SetValue(generalSetting, setting.Value, null);
            }

            return new ActionResultResponse<GeneralSetting>
            {
                Data = generalSetting
            };
        }
    }
}
