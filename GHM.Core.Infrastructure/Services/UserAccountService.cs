﻿using GHM.Core.Domain.IRepository;
using GHM.Core.Domain.IServices;
using GHM.Core.Domain.ModelMetas;
using GHM.Core.Domain.Resources;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using System;
using System.Threading.Tasks;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.ViewModels;
using GHM.Core.Domain.ViewModels;

namespace GHM.Core.Infrastructure.Services
{
    public class UserAccountService : IUserAccountService
    {
        private readonly IUserAccountRepository _userAccountRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmCoreResource> _resourceService;

        public UserAccountService(IUserAccountRepository userAccountRepository, IResourceService<SharedResource> shareResourceService,
            IResourceService<GhmCoreResource> resourceService)
        {
            _userAccountRepository = userAccountRepository;
            _sharedResourceService = shareResourceService;
            _resourceService = resourceService;
        }

        public async Task<ActionResultResponse> InsertUserAccount(AccountUserMeta userAccountMeta)
        {
            UserAccount userAccountInfo = await _userAccountRepository.GetInfoByUserName(userAccountMeta.TenantId, userAccountMeta.UserName, true);
            if (userAccountInfo != null && userAccountInfo.Id != userAccountMeta.UserId)
            {
                return new ActionResultResponse(-1, _sharedResourceService.GetString("UserName already exists"));
            }

            byte[] passwordSalt = Generate.GenerateRandomBytes(Generate.PasswordSaltLength);
            byte[] passwordHash = Generate.GetInputPasswordHash(userAccountMeta.Password, passwordSalt);

            UserAccount userAccount = new UserAccount
            {
                TenantId = userAccountMeta.TenantId,
                Id = userAccountMeta.UserId,
                UserName = userAccountMeta.UserName.Trim(),
                NormalizedUserName = userAccountMeta.UserName.Trim().ToUpper(),
                FullName = userAccountMeta.FullName.Trim(),
                Avatar = userAccountMeta.Avatar,
                PasswordHash = Convert.ToBase64String(passwordHash),
                PasswordSalt = passwordSalt,
                IsActive = userAccountMeta.IsActive
            };

            int result = await _userAccountRepository.Insert(userAccount);
            if (result <= 0)
            {
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
            }

            return new ActionResultResponse(result, _sharedResourceService.GetString("Update UserAccount success"));
        }

        public async Task<ActionResultResponse> UpdateUserAccount(string userId, AccountUserMeta userAccountMeta)
        {
            UserAccount userAccountInfo = await _userAccountRepository.GetInfo(userId, false);
            if (userAccountInfo == null)
                return new ActionResultResponse(-1, _sharedResourceService.GetString("UserAccount does not exists"));

            userAccountInfo.FullName = userAccountMeta.FullName;
            userAccountInfo.Avatar = userAccountMeta.Avatar;
            userAccountInfo.IsActive = userAccountMeta.IsActive;

            int result = await _userAccountRepository.UpdateUserAccount(userAccountInfo);
            if (result <= 0)
            {
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
            }

            return new ActionResultResponse(result, _sharedResourceService.GetString("Insert UserAccount success"));
        }

        public async Task<ActionResultResponse> UpdatePassword(string userId, UpdatePasswordMeta updatePasswordMeta)
        {
            UserAccount accountInfo = await _userAccountRepository.GetInfo(userId);
            if (accountInfo == null)
            {
                return new ActionResultResponse(-1, _sharedResourceService.GetString("You do not have permission to do this action."));
            }

            byte[] oldPasswordSalt = accountInfo.PasswordSalt;
            byte[] oldPasswordHash = Generate.GetInputPasswordHash(updatePasswordMeta.OldPassword.Trim(), oldPasswordSalt);

            if (Convert.ToBase64String(oldPasswordHash) != accountInfo.PasswordHash && !updatePasswordMeta.IsResetPassword)
            {
                return new ActionResultResponse(-2, _resourceService.GetString("Old password does not match."));
            }

            byte[] passwordSalt = Generate.GenerateRandomBytes(Generate.PasswordSaltLength);
            byte[] passwordHash = Generate.GetInputPasswordHash(updatePasswordMeta.NewPassword.Trim(), passwordSalt);

            accountInfo.PasswordSalt = passwordSalt;
            accountInfo.PasswordHash = Convert.ToBase64String(passwordHash);
            int result = await _userAccountRepository.UpdatePassword(userId, accountInfo.PasswordSalt, accountInfo.PasswordHash);
            return new ActionResultResponse(result, result > 0
                ? _resourceService.GetString("Change password successful.")
                : _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
        }

        public async Task<ActionResultResponse> DeleteUserAccount(string userId)
        {
            UserAccount userAccountInfo = await _userAccountRepository.GetInfo(userId);
            if (userAccountInfo == null)
            {
                return new ActionResultResponse(-1, _resourceService.GetString("UserAccount does not exists"));
            }

            int result = await _userAccountRepository.DeleteAccount(userAccountInfo);
            return new ActionResultResponse(result, result > 0
                ? _resourceService.GetString("Delete account successful.")
                : _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
        }

        public async Task<ActionResultResponse<UserInfoViewModel>> GetUserInfo(string id)
        {
            var userInfo = await _userAccountRepository.GetUserInfo(id);
            var result = userInfo == null ? -1 : 1;
            return new ActionResultResponse<UserInfoViewModel>
            {
                Code = result,
                Message = result > 0 ? string.Empty : _sharedResourceService.GetString(ErrorMessage.NotExists, _resourceService.GetString("User")),
                Data = userInfo
            };
        }

        public async Task<bool> ValidateCredentials(string userName, string password)
        {
            var userInfo = await _userAccountRepository.GetInfoByUserName(userName);
            if (userInfo == null)
                return false;

            int totalFailCount;
            if (userInfo.LockoutEnabled && userInfo.LockoutEnd.HasValue)
            {
                if (DateTime.Compare(userInfo.LockoutEnd.Value.DateTime, DateTime.Now) < 0)
                {
                    // Reset access fail count.
                    await _userAccountRepository.ResetLockout(userInfo.UserName);
                    // Validate password.
                    var validateResult = ValidatePassword(userInfo, userName, password, out totalFailCount);
                    if (!validateResult)
                        return false;
                }
                else
                    return false;

                return true;
            }
            var isValidPassword = ValidatePassword(userInfo, userName, password, out totalFailCount);
            return isValidPassword;
        }

        public async Task<UserAccount> FindByUserName(string userName)
        {
            return await _userAccountRepository.GetInfoByUserName(userName, true);
        }

        private bool ValidatePassword(UserAccount userAccount, string userName, string password, out int totalFailCount)
        {
            var passwordHash = Convert.ToBase64String(Generate.GetInputPasswordHash(password.Trim(), userAccount.PasswordSalt));
            if (!passwordHash.Equals(userAccount.PasswordHash))
            {
                // Increase fail count.
                var failCount = userAccount.AccessFailedCount;
                failCount += 1;
                _userAccountRepository.UpdateAccessFailCount(userAccount.UserName, failCount, userAccount.LockoutEnabled);
                totalFailCount = failCount;
                return false;
            }
            if (userAccount.AccessFailedCount > 0)
            {
                _userAccountRepository.UpdateAccessFailCount(userAccount.UserName, 0, userAccount.LockoutEnabled);
            }
            totalFailCount = 0;
            return true;
        }

        public async Task<SearchResult<UserAccountViewModel>> Search(string tenantId, string keyword, bool? isActive, int page, int pageSize)
        {
            return new SearchResult<UserAccountViewModel>
            {
                Items = await _userAccountRepository.Search(tenantId, keyword, isActive, page, pageSize, out int totalRows),
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse> ResetLockout(string userName)
        {
            var result = await _userAccountRepository.ResetLockout(userName);
            if (result == -1)
                return new ActionResultResponse(-1, "Tài khoản không tồn tại hoặc chưa kích hoạt");

            return new ActionResultResponse(result, $"Bạn đã mở khóa tài khoản {userName} thành công.");
        }

        public async Task<ActionResultResponse> LockAccount(string userName)
        {
            var result = await _userAccountRepository.LockAccount(userName);
            if (result == -1)
                return new ActionResultResponse(-1, "Tài khoản không tồn tại hoặc chưa kích hoạt ");

            return new ActionResultResponse(result, $"Bạn đã khóa  tài khoản {userName} thành công.");
        }

        public async Task<ActionResultResponse> UpdateIsActive(string userName, bool isActive)
        {
            var info = await _userAccountRepository.GetAccountInfoByUserName(userName);
            if (info == null)
                return new ActionResultResponse(-1, "Tài khoản không tồn tại");

            info.IsActive = isActive;
            var result = await _userAccountRepository.UpdateUserAccount(info);

            return new ActionResultResponse(result, result <= 0 ? "Có gì đó không đúng. Vui lòng liên hệ với quản trị viên" : "Cập nhật trạng thái thành công");
        }
    }
}
