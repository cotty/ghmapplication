﻿using System;
using System.Threading;
using System.Threading.Tasks;
using GHM.Core.Domain.IRepository;
using GHM.Core.Domain.IServices;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.Models;

namespace GHM.Core.Infrastructure.Services
{
    public class UserStoreService : IUserStoreService
    {
        private readonly IUserAccountRepository _userAccountRepository;

        public UserStoreService(IUserAccountRepository userAccountRepository)
        {
            _userAccountRepository = userAccountRepository;
        }

        public async Task<bool> ValidateCredentials(string userName, string password)
        {
            var userInfo = await _userAccountRepository.GetInfoByUserName(userName);
            if (userInfo == null)
            {
                return false;
            }

            int totalFailCount;
            if (userInfo.LockoutEnabled && userInfo.LockoutEnd.HasValue)
            {
                if (DateTime.Compare(userInfo.LockoutEnd.Value.DateTime, DateTime.Now) < 0)
                {
                    // Reset access fail count.
                    _userAccountRepository.ResetLockout(userInfo.UserName);

                    // Validate password.
                    var validateResult = ValidatePassword(userInfo, userName, password, out totalFailCount);
                    if (!validateResult)
                        return false;
                }
                else
                {
                    return false;
                }

                return true;
            }
            var isValidPassword = ValidatePassword(userInfo, userName, password, out totalFailCount);
            return isValidPassword;
        }

        public async Task<UserAccount> FindByUsername(string userName)
        {
            return await _userAccountRepository.FindByNameAsync(userName, new CancellationToken());
        }

        private bool ValidatePassword(UserAccount userAccount, string userName, string password, out int totalFailCount)
        {
            var passwordHash = Convert.ToBase64String(Generate.GetInputPasswordHash(password.Trim(), userAccount.PasswordSalt));
            if (!passwordHash.Equals(userAccount.PasswordHash))
            {
                // Increase fail count.
                var failCount = userAccount.AccessFailedCount;
                failCount += 1;
                _userAccountRepository.UpdateAccessFailCount(userAccount.UserName, failCount, userAccount.LockoutEnabled);
                totalFailCount = failCount;
                return false;
            }
            if (userAccount.AccessFailedCount > 0)
            {
                _userAccountRepository.UpdateAccessFailCount(userAccount.UserName, 0, userAccount.LockoutEnabled);
            }
            totalFailCount = 0;
            return true;
        }
    }
}
