﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GHM.Core.Domain.IRepository;
using GHM.Core.Domain.IServices;
using GHM.Core.Domain.ModelMetas;
using GHM.Core.Domain.Models;
using GHM.Core.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Stores;
using Client = GHM.Core.Domain.Models.Client;
using ErrorMessage = GHM.Infrastructure.Constants.ErrorMessage;

namespace GHM.Core.Infrastructure.Services
{
    public class ClientService : IClientService, IClientStore
    {
        private readonly IClientRepository _clientRepository;
        private readonly IClientAllowedGrantTypeRepository _clientAllowedGrantTypeRepository;
        private readonly IClientPostLogoutRedirectUrisRepository _clientPostLogoutRedirectUrisRepository;
        private readonly IClientRedirectUrisRepository _clientRedirectUrisRepository;
        private readonly IClientSecretRepository _clientSecretRepository;
        private readonly IClientPropertyRepository _clientPropertyRepository;
        private readonly IClientClaimRepository _clientClaimRepository;
        private readonly IClientIdentityProviderRestrictionRepository _clientIdentityProviderRestrictionRepository;
        private readonly IClientAllowedCorsOriginsRepository _clientAllowedCorsOriginsRepository;
        private readonly IClientAllowedScopesRepository _clientAllowedScopesRepository;


        //private readonly IResourceService<SharedResource> _sharedResourceService;
        //private readonly IResourceService<GhmCoreResource> _resourceService;

        public ClientService(IClientRepository clientRepository,
            //IResourceService<SharedResource> sharedResourceService,
            //IResourceService<GhmCoreResource> resourceService,
            IClientPostLogoutRedirectUrisRepository clientPostLogoutRedirectUrisRepository,
            IClientAllowedGrantTypeRepository clientAllowedGrantTypeRepository, IClientRedirectUrisRepository clientRedirectUrisRepository,
            IClientSecretRepository clientSecretRepository, IClientPropertyRepository clientPropertyRepository,
            IClientClaimRepository clientClaimRepository, IClientIdentityProviderRestrictionRepository clientIdentityProviderRestrictionRepository,
            IClientAllowedCorsOriginsRepository clientAllowedCorsOriginsRepository, IClientAllowedScopesRepository clientAllowedScopesRepository)
        {
            _clientRepository = clientRepository;
            //_sharedResourceService = sharedResourceService;
            //_resourceService = resourceService;
            _clientPostLogoutRedirectUrisRepository = clientPostLogoutRedirectUrisRepository;
            _clientAllowedGrantTypeRepository = clientAllowedGrantTypeRepository;
            _clientRedirectUrisRepository = clientRedirectUrisRepository;
            _clientSecretRepository = clientSecretRepository;
            _clientPropertyRepository = clientPropertyRepository;
            _clientClaimRepository = clientClaimRepository;
            _clientIdentityProviderRestrictionRepository = clientIdentityProviderRestrictionRepository;
            _clientAllowedCorsOriginsRepository = clientAllowedCorsOriginsRepository;
            _clientAllowedScopesRepository = clientAllowedScopesRepository;
        }

        public async Task<SearchResult<Client>> Search(string keyword, bool? isEnable, int page, int pageSize)
        {
            var items = await _clientRepository.Search(keyword, isEnable, page, pageSize, out int totalRows);
            return new SearchResult<Client>(items, totalRows);
        }

        public async Task<ActionResultResponse<Client>> Insert(ClientMeta clientMeta)
        {
            var isIdExists = await _clientRepository.CheckExists(clientMeta.ClientId);
            if (isIdExists)
                return new ActionResultResponse<Client>(-1, "Client Id đã tồn tại. Vui lòng kiểm tra lại.");

            var isExists = await _clientRepository.CheckNameExists(clientMeta.ClientId, clientMeta.ClientName);
            if (isExists)
                return new ActionResultResponse<Client>(-1, "Tên client đã tồn tại. Vui lòng kiểm tra lại.");

            var client = MapToClient(clientMeta);
            client.ProtocolType = IdentityServerConstants.ProtocolTypes.OpenIdConnect;
            var result = await _clientRepository.Insert(client);
            if (result <= 0)
                return new ActionResultResponse<Client>(-2, ErrorMessage.SomethingWentWrong);

            // Insert client post logout redirect uri.
            if (!string.IsNullOrEmpty(clientMeta.ClientPostLogoutRedirectUris))
            {
                var listClientPostLogoutRedirectUris = clientMeta.ClientPostLogoutRedirectUris.Split(',');
                if (listClientPostLogoutRedirectUris.Any())
                {
                    var clientPostLogoutRedirectUris =
                        MapToPostLogoutRedirectUris(clientMeta.ClientId, listClientPostLogoutRedirectUris);
                    foreach (var clientPostLogoutRedirectUri in clientPostLogoutRedirectUris)
                    {
                        await _clientPostLogoutRedirectUrisRepository.Insert(clientPostLogoutRedirectUri);
                    }
                }
            }

            // Insert client redirect uri.
            if (!string.IsNullOrEmpty(clientMeta.ClientPostLogoutRedirectUris))
            {
                var listClientPostLogoutRedirectUris = clientMeta.ClientPostLogoutRedirectUris.Split(',');
                if (listClientPostLogoutRedirectUris.Any())
                {
                    var clientRedirectUris =
                        MapToClientRedirectUris(clientMeta.ClientId, listClientPostLogoutRedirectUris);
                    foreach (var clientRedirectUri in clientRedirectUris)
                    {
                        await _clientRedirectUrisRepository.Insert(clientRedirectUri);
                    }
                }
            }

            // Insert client secret.
            if (!string.IsNullOrEmpty(clientMeta.ClientSecret))
            {

                var listClientSecret = clientMeta.ClientSecret.Split(',');
                if (listClientSecret.Any())
                {
                    var clientSecrets =
                        MapToClientSecret(clientMeta.ClientId, listClientSecret);
                    foreach (var clientSecret in clientSecrets)
                    {
                        await _clientSecretRepository.Insert(clientSecret);
                    }
                }
            }

            if (!string.IsNullOrEmpty(clientMeta.ClientAllowedScopes))
            {
                var listClientAllowedScopes = clientMeta.ClientAllowedScopes.Split(',');
                if (listClientAllowedScopes.Any())
                {
                    var clientAllowedScopes =
                        MapToClientAllowedScope(clientMeta.ClientId, listClientAllowedScopes);
                    foreach (var clientAllowedScope in clientAllowedScopes)
                    {
                        await _clientAllowedScopesRepository.Insert(clientAllowedScope);
                    }
                }
            }

            return new ActionResultResponse<Client>(1, "Thêm mới client thành công.");
        }

        public async Task<ActionResultResponse<Client>> Update(ClientMeta clientMeta)
        {
            var clientInfo = await _clientRepository.GetInfo(clientMeta.ClientId);
            if (clientInfo == null)
                return new ActionResultResponse<Client>(-1, "Tên client không tồn tại.");

            clientInfo.ClientId = clientMeta.ClientId;
            clientInfo.IdentityTokenLifetime = clientMeta.IdentityTokenLifetime;
                clientInfo.ClientName = clientMeta.ClientName;
            clientInfo.AbsoluteRefreshTokenLifetime = clientMeta.AbsoluteRefreshTokenLifetime;
            clientInfo.AccessTokenLifetime = clientMeta.AccessTokenLifetime;
            clientInfo.AccessTokenType = clientMeta.AccessTokenType;
            clientInfo.AllowAccessTokensViaBrowser = clientMeta.AllowAccessTokensViaBrowser;
            clientInfo.AllowOfflineAccess = clientMeta.AllowOfflineAccess;
            clientInfo.AllowPlainTextPkce = clientMeta.AllowPlainTextPkce;
            clientInfo.AllowRememberConsent = clientMeta.AllowRememberConsent;
            clientInfo.AlwaysIncludeUserClaimsInIdToken = clientMeta.AlwaysIncludeUserClaimsInIdToken;
            clientInfo.AlwaysSendClientClaims = clientMeta.AlwaysSendClientClaims;
            clientInfo.AuthorizationCodeLifetime = clientMeta.AuthorizationCodeLifetime;
            clientInfo.BackChannelLogoutSessionRequired = clientMeta.BackChannelLogoutSessionRequired;
            clientInfo.BackChannelLogoutUri = clientMeta.BackChannelLogoutUri;
            clientInfo.ClientClaimsPrefix = clientMeta.ClientClaimsPrefix;
            clientInfo.ClientUri = clientMeta.ClientUri;
            clientInfo.ConsentLifetime = clientMeta.ConsentLifetime;
            clientInfo.EnableLocalLogin = clientMeta.EnableLocalLogin;
            clientInfo.Enabled = clientMeta.Enabled;
            clientInfo.FrontChannelLogoutSessionRequired = clientMeta.FrontChannelLogoutSessionRequired;
            clientInfo.FrontChannelLogoutUri = clientMeta.FrontChannelLogoutUri;
            clientInfo.IncludeJwtId = clientMeta.IncludeJwtId;
            clientInfo.LogoUri = clientMeta.LogoUri;
            clientInfo.ClientAllowedGrantTypes = clientMeta.ClientAllowedGrantTypes;
            clientInfo.PairWiseSubjectSalt = clientMeta.PairWiseSubjectSalt;
            clientInfo.ProtocolType = clientMeta.ProtocolType;
            clientInfo.RefreshTokenExpiration = clientMeta.RefreshTokenExpiration;
            clientInfo.RefreshTokenUsage = clientMeta.RefreshTokenUsage;
            clientInfo.RequireClientSecret = clientMeta.RequireClientSecret;
            clientInfo.RequireConsent = clientMeta.RequireConsent;
            clientInfo.RequirePkce = clientMeta.RequirePkce;
            clientInfo.SlidingRefreshTokenLifetime = clientMeta.SlidingRefreshTokenLifetime;
            clientInfo.UpdateAccessTokenClaimsOnRefresh = clientMeta.UpdateAccessTokenClaimsOnRefresh;
            clientInfo.UnsignName = clientMeta.ClientName.StripVietnameseChars();

            var result = await _clientRepository.Update(clientInfo);
            if (result <= 0)
                return new ActionResultResponse<Client>(-2, ErrorMessage.SomethingWentWrong);

            // Update client post logout redirect uri.
            //if (!string.IsNullOrEmpty(clientMeta.ClientPostLogoutRedirectUris))
            //{
            //    //_clientPostLogoutRedirectUrisRepository.Delete()
            //    var listClientPostLogoutRedirectUris = clientMeta.ClientPostLogoutRedirectUris.Split(',');
            //    if (listClientPostLogoutRedirectUris.Any())
            //    {
            //        var clientPostLogoutRedirectUris =
            //            MapToPostLogoutRedirectUris(clientMeta.ClientId, listClientPostLogoutRedirectUris);
            //        foreach (var clientPostLogoutRedirectUri in clientPostLogoutRedirectUris)
            //        {
            //            await _clientPostLogoutRedirectUrisRepository.Insert(clientPostLogoutRedirectUri);
            //        }
            //    }
            //}

            // Update client redirect uri.
            if (!string.IsNullOrEmpty(clientMeta.ClientPostLogoutRedirectUris))
            {
                await _clientRedirectUrisRepository.DeleteByClientId(clientInfo.ClientId);
                var listClientPostLogoutRedirectUris = clientMeta.ClientPostLogoutRedirectUris.Split(',');
                if (listClientPostLogoutRedirectUris.Any())
                {
                    var clientRedirectUris =
                        MapToClientRedirectUris(clientMeta.ClientId, listClientPostLogoutRedirectUris);
                    foreach (var clientRedirectUri in clientRedirectUris)
                    {
                        await _clientRedirectUrisRepository.Insert(clientRedirectUri);
                    }
                }
            }

            // Update client secret.
            //if (!string.IsNullOrEmpty(clientMeta.ClientSecret))
            //{
               
            //    var listClientSecret = clientMeta.ClientSecret.Split(',');
            //    if (listClientSecret.Any())
            //    {
            //        var clientSecrets =
            //            MapToClientSecret(clientMeta.ClientId, listClientSecret);
            //        foreach (var clientSecret in clientSecrets)
            //        {
            //            await _clientSecretRepository.Insert(clientSecret);
            //        }
            //    }
            //}

            if (!string.IsNullOrEmpty(clientMeta.ClientAllowedScopes))
            {
                await _clientAllowedScopesRepository.DeleteByCLientId(clientInfo.ClientId);
                var listClientAllowedScopes = clientMeta.ClientAllowedScopes.Split(',');
                if (listClientAllowedScopes.Any())
                {
                    var clientAllowedScopes =
                        MapToClientAllowedScope(clientMeta.ClientId, listClientAllowedScopes);
                    foreach (var clientAllowedScope in clientAllowedScopes)
                    {
                        await _clientAllowedScopesRepository.Insert(clientAllowedScope);
                    }
                }
            }

            return new ActionResultResponse<Client>(1, "Cập nhật client thành công.");
        }

        public async Task<ActionResultResponse> Delete(string clientId)
        {
            var clientInfo = await _clientRepository.GetInfo(clientId);
            if (clientInfo == null)
                return new ActionResultResponse<Client>(-1, "Tên client không tồn tại.");

            var result = await _clientRepository.Delete(clientId);
            return new ActionResultResponse(result, result <= 0 ? "Có gì đó không đúng. Vui lòng liên hệ vơi quản trị viên."
                : "Xóa client thành công");
        }

        public async Task<IdentityServer4.Models.Client> FindClientByIdAsync(string clientId)
        {
            var clientInfo = await _clientRepository.GetInfo(clientId, true);
            if (clientInfo == null)
                return null;

            // Get identity client object.
            //var listAllowedGrantTypes = await _clientAllowedGrantTypeRepository.GetByClientId(clientId);
            var listPostLogoutRedirectUris =
                await _clientPostLogoutRedirectUrisRepository.GetByClientId(clientId);
            var listClientSecrets = await _clientSecretRepository.GetByClientId(clientId);
            var listRedirectUris = await _clientRedirectUrisRepository.GetByClientId(clientId);
            var listProperties = await _clientPropertyRepository.GetsByClientId(clientId);
            var listClaims = await _clientClaimRepository.GetsClaimByClienId(clientId);
            var listIdentityProviderRestriction =
                await _clientIdentityProviderRestrictionRepository.GetsByClientId(clientId);
            var listAllowedCorsOrigins = await _clientAllowedCorsOriginsRepository.GetsByClientId(clientId);
            var listAllowedScope = await _clientAllowedScopesRepository.GetsByClientId(clientId);

            return MapToIdentityClient(clientInfo, listPostLogoutRedirectUris, listRedirectUris, listClientSecrets,
                listProperties, listClaims, listIdentityProviderRestriction, listAllowedCorsOrigins, listAllowedScope);
        }


        #region Privates
        private IdentityServer4.Models.Client MapToIdentityClient(Client client,
            List<ClientPostLogoutRedirectUris> listPostLogoutRedirectUris,
            List<ClientRedirectUris> listClientRedirectUris, List<ClientSecret> listClientSecrets,
            List<ClientProperty> listClientProperty,
            List<ClientClaim> listClaims,
            List<ClientIdentityProviderRestriction> listIdentityProviderRestrictions,
            List<ClientAllowedCorsOrigin> listAllowedCorsOrigins,
            List<ClientAllowedScope> listClientAllowedScopes)
        {
            var propertyDictionary = new Dictionary<string, string>();
            listClientProperty.ForEach(x =>
            {
                propertyDictionary.Add(x.Key, x.Value);
            });

            var identityClient = new IdentityServer4.Models.Client
            {
                ClientId = client.ClientId,
                IdentityTokenLifetime = client.IdentityTokenLifetime,
                ClientName = client.ClientName,
                AbsoluteRefreshTokenLifetime = client.AbsoluteRefreshTokenLifetime,
                AccessTokenLifetime = client.AccessTokenLifetime,
                AccessTokenType = client.AccessTokenType,
                AllowAccessTokensViaBrowser = client.AllowAccessTokensViaBrowser,
                AllowOfflineAccess = client.AllowOfflineAccess,
                AllowPlainTextPkce = client.AllowPlainTextPkce,
                AllowRememberConsent = client.AllowRememberConsent,
                AlwaysIncludeUserClaimsInIdToken = client.AlwaysIncludeUserClaimsInIdToken,
                AlwaysSendClientClaims = client.AlwaysSendClientClaims,
                AuthorizationCodeLifetime = client.AuthorizationCodeLifetime,
                BackChannelLogoutSessionRequired = client.BackChannelLogoutSessionRequired,
                BackChannelLogoutUri = client.BackChannelLogoutUri,
                ClientClaimsPrefix = client.ClientClaimsPrefix,
                ClientUri = client.ClientUri,
                ConsentLifetime = client.ConsentLifetime,
                EnableLocalLogin = client.EnableLocalLogin,
                Enabled = client.Enabled,
                FrontChannelLogoutSessionRequired = client.FrontChannelLogoutSessionRequired,
                FrontChannelLogoutUri = client.FrontChannelLogoutUri,
                IncludeJwtId = client.IncludeJwtId,
                LogoUri = client.LogoUri,
                PairWiseSubjectSalt = client.PairWiseSubjectSalt,
                ProtocolType = client.ProtocolType,
                RefreshTokenExpiration = client.RefreshTokenExpiration,
                RefreshTokenUsage = client.RefreshTokenUsage,
                RequireClientSecret = client.RequireClientSecret,
                RequireConsent = client.RequireConsent,
                RequirePkce = client.RequirePkce,
                SlidingRefreshTokenLifetime = client.SlidingRefreshTokenLifetime,
                UpdateAccessTokenClaimsOnRefresh = client.UpdateAccessTokenClaimsOnRefresh,
                AllowedGrantTypes = GetGrantTypes(client.ClientAllowedGrantTypes),
                ClientSecrets = listClientSecrets.Select(x => new Secret
                {
                    Value = x.Value,
                    Description = x.Description,
                    Type = x.Type,
                    Expiration = x.Expiration
                }).ToList(),
                PostLogoutRedirectUris = listPostLogoutRedirectUris.Select(x => x.Uri).ToList(),
                RedirectUris = listClientRedirectUris.Select(x => x.Uri).ToList(),
                AllowedCorsOrigins = listAllowedCorsOrigins.Select(x => x.Domain).ToList(),
                AllowedScopes = listClientAllowedScopes.Distinct().Select(x => x.Scope).ToList(),
                IdentityProviderRestrictions = listIdentityProviderRestrictions.Select(x => x.IdentityProviderRestriction).ToList(),
                Properties = propertyDictionary,
                Claims = listClaims.Select(x => new Claim(x.ClaimType, x.ClaimValue)).ToList()
            };
            return identityClient;
        }

        private static Client MapToClient(ClientMeta clientMeta)
        {
            var client = new Client
            {
                ClientId = clientMeta.ClientId,
                IdentityTokenLifetime = clientMeta.IdentityTokenLifetime,
                ClientName = clientMeta.ClientName,
                AbsoluteRefreshTokenLifetime = clientMeta.AbsoluteRefreshTokenLifetime,
                AccessTokenLifetime = clientMeta.AccessTokenLifetime,
                AccessTokenType = clientMeta.AccessTokenType,
                AllowAccessTokensViaBrowser = clientMeta.AllowAccessTokensViaBrowser,
                AllowOfflineAccess = clientMeta.AllowOfflineAccess,
                AllowPlainTextPkce = clientMeta.AllowPlainTextPkce,
                AllowRememberConsent = clientMeta.AllowRememberConsent,
                AlwaysIncludeUserClaimsInIdToken = clientMeta.AlwaysIncludeUserClaimsInIdToken,
                AlwaysSendClientClaims = clientMeta.AlwaysSendClientClaims,
                AuthorizationCodeLifetime = clientMeta.AuthorizationCodeLifetime,
                BackChannelLogoutSessionRequired = clientMeta.BackChannelLogoutSessionRequired,
                BackChannelLogoutUri = clientMeta.BackChannelLogoutUri,
                ClientClaimsPrefix = clientMeta.ClientClaimsPrefix,
                ClientUri = clientMeta.ClientUri,
                ConsentLifetime = clientMeta.ConsentLifetime,
                EnableLocalLogin = clientMeta.EnableLocalLogin,
                Enabled = clientMeta.Enabled,
                FrontChannelLogoutSessionRequired = clientMeta.FrontChannelLogoutSessionRequired,
                FrontChannelLogoutUri = clientMeta.FrontChannelLogoutUri,
                IncludeJwtId = clientMeta.IncludeJwtId,
                LogoUri = clientMeta.LogoUri,
                ClientAllowedGrantTypes = clientMeta.ClientAllowedGrantTypes,
                PairWiseSubjectSalt = clientMeta.PairWiseSubjectSalt,
                ProtocolType = clientMeta.ProtocolType,
                RefreshTokenExpiration = clientMeta.RefreshTokenExpiration,
                RefreshTokenUsage = clientMeta.RefreshTokenUsage,
                RequireClientSecret = clientMeta.RequireClientSecret,
                RequireConsent = clientMeta.RequireConsent,
                RequirePkce = clientMeta.RequirePkce,
                SlidingRefreshTokenLifetime = clientMeta.SlidingRefreshTokenLifetime,
                UpdateAccessTokenClaimsOnRefresh = clientMeta.UpdateAccessTokenClaimsOnRefresh,
                UnsignName = clientMeta.ClientName.StripVietnameseChars()
            };
            return client;
        }

        private ICollection<string> GetGrantTypes(string grantType)
        {
            switch (grantType)
            {
                case "Implicit":
                    return GrantTypes.Implicit;
                case "ImplicitAndClientCredentials":
                    return GrantTypes.ImplicitAndClientCredentials;
                case "Code":
                    return GrantTypes.Code;
                case "CodeAndClientCredentials":
                    return GrantTypes.CodeAndClientCredentials;
                case "Hybrid":
                    return GrantTypes.Hybrid;
                case "HybridAndClientCredentials":
                    return GrantTypes.HybridAndClientCredentials;
                case "ClientCredentials":
                    return GrantTypes.ClientCredentials;
                case "ResourceOwnerPassword":
                    return GrantTypes.ResourceOwnerPassword;
                case "ResourceOwnerPasswordAndClientCredentials":
                    return GrantTypes.ResourceOwnerPasswordAndClientCredentials;
                default: return new List<string>();
            }
        }

        private List<ClientPostLogoutRedirectUris> MapToPostLogoutRedirectUris(string clientId, string[] clientPostLogoutRedirectUris)
        {
            if (clientPostLogoutRedirectUris == null || !clientPostLogoutRedirectUris.Any())
                return null;

            var result = new List<ClientPostLogoutRedirectUris>();
            foreach (var uri in clientPostLogoutRedirectUris)
            {
                result.Add(new ClientPostLogoutRedirectUris(clientId, uri.Trim()));
            }
            return result;
        }

        private List<ClientRedirectUris> MapToClientRedirectUris(string clientId, string[] clientRedirectUris)
        {
            if (clientRedirectUris == null || !clientRedirectUris.Any())
                return null;

            var result = new List<ClientRedirectUris>();
            foreach (var uri in clientRedirectUris)
            {
                result.Add(new ClientRedirectUris(clientId, uri.Trim()));
            }
            return result;
        }

        private List<ClientSecret> MapToClientSecret(string clientId, string[] clientSecrets)
        {
            if (clientSecrets == null || !clientSecrets.Any())
                return null;

            var result = new List<ClientSecret>();
            foreach (var secret in clientSecrets)
            {
                result.Add(new ClientSecret(clientId, new Secret(secret.Trim().Sha256())));
            }
            return result;
        }

        private List<ClientAllowedScope> MapToClientAllowedScope(string clientId, string[] clientAllowedScopes)
        {
            if (clientAllowedScopes == null || !clientAllowedScopes.Any())
                return null;

            var result = new List<ClientAllowedScope>();
            foreach (var scope in clientAllowedScopes)
            {
                result.Add(new ClientAllowedScope(clientId, scope.Trim()));
            }
            return result;
        }
        #endregion
    }
}
