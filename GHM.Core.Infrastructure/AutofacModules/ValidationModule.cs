﻿using Autofac;
using FluentValidation;
using GHM.Core.Domain.ModelMetas;
using GHM.Core.Infrastructure.Validations;

namespace GHM.Core.Infrastructure.AutofacModules
{
    public class ValidationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ClientMetaValidator>()
                .As<IValidator<ClientMeta>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PageMetaValidator>()
                .As<IValidator<PageMeta>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<TenantValidator>()
                .As<IValidator<TenantMeta>>()
                .InstancePerLifetimeScope();
        }
    }
}
