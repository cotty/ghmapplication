﻿using System;
using System.Globalization;
using System.Linq;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using FluentValidation.AspNetCore;
using GHM.Authentication.Validators;
using GHM.Core.Infrastructure;
using GHM.Core.Infrastructure.AutofacModules;
using GHM.Core.Infrastructure.Repository;
using GHM.Core.Infrastructure.Services;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.SqlServer;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace GHM.Authentication
{
    public class Startup
    {
        public IConfiguration Configuration { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Config IIS support.
            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
            });

            services.AddOptions();
            services.AddCors();
            services.AddMvcCore().AddJsonFormatters()
                .AddFluentValidation()
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                .AddDataAnnotationsLocalization(); ;
            // Identity Services.
            services.AddTransient<IDbContext, CoreDbContext>();
            services.AddTransient<IUserStore<UserAccount>, UserAccountRepository>();
            services.AddTransient<IClientStore, ClientService>();
            services.AddTransient<IResourceStore, ResourceRepository>();
            //services.AddTransient<ICustomAuthorizeRequestValidator, CustomAuthorizeRequestValidator>();

            services.AddDbContext<CoreDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("CoreConnectionString"));
            });

            #region Identity config
            var defaultLockoutTimeSpan = Configuration.ConfigIdentity("DefaultLockoutTimeSpan");
            var maxFailedAccessAttempts = Configuration.ConfigIdentity("MaxFailedAccessAttempts");
            services.AddIdentity<UserAccount, Core.Domain.Models.Role>(options =>
                {
                    options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(defaultLockoutTimeSpan != null ? int.Parse(defaultLockoutTimeSpan) : 5);
                    options.Lockout.MaxFailedAccessAttempts = maxFailedAccessAttempts != null ? int.Parse(maxFailedAccessAttempts) : 5;
                    options.Lockout.AllowedForNewUsers = true;
                })
                .AddEntityFrameworkStores<CoreDbContext>()
                .AddDefaultTokenProviders();
            services.AddIdentityServer(
                options =>
                {
                    //options.UserInteraction = new IdentityServer4.Configuration.UserInteractionOptions
                    //{
                    //    LoginUrl = "http://localhost:4300/login/connect-oidc"
                    //};
                    //options.Cors.CorsPaths.Add(new Microsoft.AspNetCore.Http.PathString("/Account/Login"));
                }
        )
                //.AddSigningCredential("id_rsa")
                .AddDeveloperSigningCredential()
                .AddInMemoryPersistedGrants()
                .AddResourceStore<ResourceRepository>()
                .AddClientStore<ClientService>()
                .AddProfileService<ProfileRepository>()
                //.AddInMemoryIdentityResources(IdentityConfig.GetIdentityResources())
                //.AddInMemoryApiResources(IdentityConfig.GetApiResources())
                //.AddInMemoryClients(IdentityConfig.GetClients())
                //.AddProfileService<ProfileRepository>()
                .AddAspNetIdentity<UserAccount>()
                .AddResourceOwnerValidator<ResourceOwnerPasswordValidator>();            
            #endregion

            #region Autofac Config.
            // Config Autofac.
            var container = new ContainerBuilder();
            container.Populate(services);

            container.RegisterModule(new ApplicationModule(Configuration.GetConnectionString("CoreConnectionString")));
            var autofacServiceProvider = new AutofacServiceProvider(container.Build());
            return autofacServiceProvider;
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            #region Localizations
            var supportedCultures = new[]
            {
                new CultureInfo("vi-VN"),
                new CultureInfo("en"),
                new CultureInfo("en-US"),
            };

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("vi-VN"),
                // Formatting numbers, dates, etc.
                SupportedCultures = supportedCultures,
                // UI strings that we have localized.
                SupportedUICultures = supportedCultures
            });
            #endregion

            #region Allow Origin
            var allowOrigins = Configuration.GetSection("AllowOrigins")
                .GetChildren().Select(x => x.Value).ToArray();
            app.UseCors(builder =>
            {
                builder.WithOrigins(allowOrigins);
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
                builder.AllowCredentials();
            });
            #endregion
            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseHttpsRedirection();
            app.UseIdentityServer();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}