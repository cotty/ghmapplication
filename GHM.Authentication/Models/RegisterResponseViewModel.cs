﻿
using GHM.Infrastructure.Models;

namespace GHM.Authentication.Models
{
    public class RegisterResponseViewModel
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }

        public RegisterResponseViewModel(UserAccount user)
        {
            Id = user.Id;
            FullName = user.FullName;
            Email = user.Email;
        }
    }
}
