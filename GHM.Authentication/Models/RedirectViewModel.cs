﻿

namespace GHM.Authentication.Models
{
    public class RedirectViewModel
    {
        public string RedirectUrl { get; set; }
    }
}
