﻿using System;
using System.Threading.Tasks;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using GHM.Patient.Domain.IRepository;
using GHM.Patient.Domain.IServices;
using GHM.Patient.Domain.ModelMetas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Patient.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class PatientsController : GhmControllerBase
    {
        private readonly IPatientService _patientService;
        private readonly IPatientContactService _patientContactService;
        private readonly IContactPatientService _contactPatientService;
        private readonly IPatientRepository _patientRepository;

        public PatientsController(IPatientService patientService, IPatientContactService patientContactService,
            IContactPatientService contactPatientService, IPatientRepository patientRepository)
        {
            _patientService = patientService;
            _patientContactService = patientContactService;
            _contactPatientService = contactPatientService;
            _patientRepository = patientRepository;
        }

        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.Customer, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]PatientMeta patientMeta)
        {
            var result = await _patientService.Insert(CurrentUser.TenantId, patientMeta);
            //var result = await _patientService.Insert("1", patientMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.Customer, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody]PatientMeta patientMeta)
        {
            var result = await _patientService.Update(CurrentUser.TenantId, id, patientMeta);
            //var result = await _patientService.Update("1", id, patientMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.Customer, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _patientService.ForceDelete(CurrentUser.TenantId, id);
            //var result = await _patientService.ForceDelete("1", id);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.Customer, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetDetail(string id)
        {
            var result = await _patientService.GetDetail(id);
            //var result = await _patientsubjectService.GetDetail("MaKH", "vi", id);
            if (result.Code < 0)
                return BadRequest(result);
            return Ok(result);
        }

        [AcceptVerbs("GET")]
        [AllowPermission(PageId.Customer, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string keyword, DateTime? createDate, int page = 1, int pageSize = 20)
        {
            var result = await _patientService.Search(CurrentUser.TenantId, keyword, createDate, page, pageSize);
            return Ok(result);
        }

        #region PatientContact
        [Route("{patientId}/patientContacts")]
        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.Customer, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert(string patientId, [FromBody]PatientContactMeta patientContactMeta)
        {
            var result = await _patientContactService.Insert(CurrentUser.TenantId, patientContactMeta);
            //var result = await _patientContactService.Insert("1", patientContactMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{patientId}/patientContacts/{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.Customer, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string patientId, string id, [FromBody]PatientContactMeta patientContactMeta)
        {
            var result = await _patientContactService.Update(CurrentUser.TenantId, id, patientContactMeta);
            //var result = await _patientContactService.Update("1", id, patientContactMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{patientId}/patientContacts/{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.Customer, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string patientId, string id)
        {
            var result = await _patientContactService.ForceDelete(CurrentUser.TenantId, id);
            //var result = await _patientContactService.ForceDelete("1", id);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
        #endregion

        #region ContactPatient
        [Route("{patientId}/contactPatients")]
        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.Customer, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert(string patientId, [FromBody]ContactPatientMeta contactPatientMeta)
        {
            var result = await _contactPatientService.Insert(CurrentUser.TenantId, contactPatientMeta);
            //var result = await _contactPatientService.Insert("1", contactPatientMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{patientId}/contactPatients/{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.Customer, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string patientId, string id, [FromBody]ContactPatientMeta contactPatientMeta)
        {
            var result = await _contactPatientService.Update(CurrentUser.TenantId, id, contactPatientMeta);
            //var result = await _contactPatientService.Update("1", id, contactPatientMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{patientId}/contactPatients/{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.Customer, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> DeleteContactPatient(string patientId, string id)
        {
            var result = await _contactPatientService.ForceDelete(CurrentUser.TenantId, id);
            //var result = await _contactPatientService.ForceDelete("1", id);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
        #endregion
    }
}