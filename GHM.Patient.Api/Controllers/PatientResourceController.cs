﻿using System.Globalization;
using System.Threading.Tasks;
using GHM.Patient.Domain.IServices;
using GHM.Patient.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Patient.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/patient-resources")]

    public class PatientResourceController : GhmControllerBase
    {

        private readonly IPatientResourceService _patientResourceService;

        public PatientResourceController(IPatientResourceService patientResourceService)
        {
            _patientResourceService = patientResourceService;
        }

        /// <param name="keyword">Từ khóa tìm kiếm</param>
        /// <param name="isActive">Đã kích hoạt chưa</param>
        /// <param name="page">Trang hiện tại</param>
        /// <param name="pageSize">Số bản ghi trên trang</param>
        /// <returns></returns>
        [AcceptVerbs("GET")]
        [AllowPermission(PageId.CustomerResource, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> SearchPatientResource(string keyword, bool? isActive, int page = 1, int pageSize = 20)
        {
            var result = await _patientResourceService.Search(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, keyword, isActive, page, pageSize);
            //   var result = await _patientResourceService.Search("1", "vi", keyword, isActive, page, pageSize);
            return Ok(result);
        }

        [AcceptVerbs("POST")]
        [AllowPermission(PageId.CustomerResource, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> InsertPatientResource([FromBody]PatientResourceMeta patientResourceMeta)
        {
            var result = await _patientResourceService.Insert(CurrentUser.TenantId, patientResourceMeta);
            //   var result = await _patientResourceService.Insert("1", patientResourceMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.CustomerResource, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdatePatientResource(string id, [FromBody]PatientResourceMeta patientResourceMeta)
        {
            var result = await _patientResourceService.Update(CurrentUser.TenantId, id, patientResourceMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.CustomerResource, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> DetailPatientResource(string id)
        {
            var result = await _patientResourceService.GetDetail(CurrentUser.TenantId, id);
            //   var result = await _patientResourceService.GetDetail("1", id);
            if (result.Code < 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.CustomerResource, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> DeletePatientResource(string id)
        {
            var result = await _patientResourceService.Delete(CurrentUser.TenantId, id);
            //   var result = await _patientResourceService.Delete("1", id);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("get-for-select"), AcceptVerbs("GET")]
        public async Task<IActionResult> SearchForSelect(string keyword, int page = 1, int pageSize = 20)
        {
            return Ok(await _patientResourceService.SearchForSelect(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, keyword, page, pageSize));
        }
    }
}