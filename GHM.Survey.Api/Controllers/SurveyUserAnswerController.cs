﻿using System.Globalization;
using System.Threading.Tasks;
using GHM.Infrastructure;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using GHM.Survey.Domain.IServices;
using GHM.Survey.Domain.ModelMetas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Survey.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/survey-user-answers")]

    public class SurveyUserAnswerController : GhmControllerBase
    {
        private readonly ISurveyQuestionUserService _surveyQuestionUserService;
        private readonly ISurveyUserAnswerService _surveyUserAnswerService;
        private readonly ISurveyUserAnswerTimeService _surveyUserAnswerTimeService;

        public SurveyUserAnswerController(ISurveyQuestionUserService surveyQuestionUserService, ISurveyUserAnswerService surveyUserAnswerService, ISurveyUserAnswerTimeService surveyUserAnswerTimeService)
        {
            _surveyQuestionUserService = surveyQuestionUserService;
            _surveyUserAnswerService = surveyUserAnswerService;
            _surveyUserAnswerTimeService = surveyUserAnswerTimeService;
        }

        [AcceptVerbs("POST"), ValidateModel]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]SurveyUserAnswerMeta surveyUserAnswerMeta)
        {
            var result = await _surveyUserAnswerService.Insert(CurrentUser.Id, surveyUserAnswerMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{surveyUserAnswerId}"), AcceptVerbs("POST"), ValidateModel]
        //[CheckPermission(PageId.ConfigPatientSubject, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string surveyUserAnswerId, [FromBody]SurveyUserAnswerMeta surveyUserAnswerMeta)
        {
            var result = await _surveyUserAnswerService.Update(surveyUserAnswerId, CurrentUser.Id, surveyUserAnswerMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{surveyUserAnswerId}"), AcceptVerbs("DELETE"), ValidateModel]
        [CheckPermission]
        //[CheckPermission(PageId.ConfigPatientSubject, Permission.Update)]
        public async Task<IActionResult> Delete(string surveyUserAnswerId)
        {
            var result = await _surveyUserAnswerService.Delete(surveyUserAnswerId);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("detail/{surveyId}/{surveyUserId}"), AcceptVerbs("GET")]
        [CheckPermission]
        //[CheckPermission(PageId.ConfigPatientSubject, Permission.View)]
        public async Task<IActionResult> DetailSurveyQuestionUser(string surveyId, string surveyUserId, int page = 1, int pageSize = 20)
        {
            var result = await _surveyUserAnswerService.DetailSurveyQuestionUser(CurrentUser.TenantId, CurrentUser.Id, surveyId, surveyUserId, 
                CultureInfo.CurrentCulture.Name, page, pageSize);
            if (result.Code < 0)
                return Forbid();

            return Ok(result);
        }
    }
}