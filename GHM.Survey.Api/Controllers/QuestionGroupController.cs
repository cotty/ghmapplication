﻿using System.Globalization;
using System.Threading.Tasks;
using GHM.Survey.Domain.IServices;
using GHM.Survey.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.CustomAttributes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Filters;

namespace GHM.Survey.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/question-groups")]
    public class QuestionGroupController : GhmControllerBase
    {
        private readonly IQuestionGroupService _questionGroupService;

        public QuestionGroupController(IQuestionGroupService questionGroupService)
        {
            _questionGroupService = questionGroupService;
        }

        /// <param name="keyword">Từ khóa tìm kiếm</param>
        /// <param name="isActive">Đã kích hoạt chưa</param>
        /// <param name="page">Trang hiện tại</param>
        /// <param name="pageSize">Số bản ghi trên trang</param>
        /// <returns></returns>
        [AcceptVerbs("GET")]
        [AllowPermission(PageId.SurveyGroupQuestion, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> SearchQuestionGroup(string keyword, bool? isActive, int page = 1, int pageSize = 20)
        {
            var result = await _questionGroupService.Search(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, keyword, isActive, page, pageSize);
            //  var result = await _questionGroupService.Search( "1", "vi", "16", isActive, page, pageSize);
            return Ok(result);
        }

        [Route("alls"), AcceptVerbs("GET")]
        [AllowPermission(PageId.SurveyGroupQuestion, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetAll()
        {
            var result = await _questionGroupService.GetsAll(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name);
            //   var result = await _questionGroupService.GetsAll("1", "vi");
            return Ok(result);
        }

        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.SurveyGroupQuestion, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> InsertQuestionGroup([FromBody]QuestionGroupMeta questionGroupMeta)
        {
            var result = await _questionGroupService.Insert(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, questionGroupMeta);
            //   var result = await _questionGroupService.Insert("1","admin","admin them moi", questionGroupMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.SurveyGroupQuestion, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateQuestionGroup(int id, [FromBody]QuestionGroupMeta questionGroupMeta)
        {
           var result = await _questionGroupService.Update(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, id, questionGroupMeta);
          //   var result = await _questionGroupService.Update("7fa8058b-29bd-4184-b7a7-eef4c4a5a5a5", "7fa8058b-29bd-4184-b7a7-eef4c4a5a5a5", "Nguyễn Huy Hoàng", id, questionGroupMeta);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.SurveyGroupQuestion, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> DetailQuestionGroup(int id)
        {
            var result = await _questionGroupService.GetDetail(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, id);
            //   var result = await _questionGroupService.GetDetail("1", "vi", id);
            if (result.Code < 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.SurveyGroupQuestion, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> DeleteQuestionGroup(int id)
        {
           var result = await _questionGroupService.Delete(CurrentUser.TenantId, id);
         //   var result = await _questionGroupService.Delete("1", id);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("trees"), AcceptVerbs("GET")]
        [CheckPermission]
        public async Task<IActionResult> GetQuestionGroupTree()
        {
            var trees = await _questionGroupService.GetFullQuestionGroupTree(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name);
            //  var trees = await _questionGroupService.GetFullQuestionGroupTree("1", "vi");
            return Ok(trees);
        }

        [Route("suggestions"), AcceptVerbs("GET")]
        [CheckPermission]
        public async Task<IActionResult> GetAllQuestionGroupForSelect(string keyword, int page = 1, int pageSize = 20)
        {
            return Ok(await _questionGroupService.GetQuestionGroupForSelect(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, keyword, page, pageSize));
        }
    }
}