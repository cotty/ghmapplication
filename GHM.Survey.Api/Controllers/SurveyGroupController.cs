﻿using System.Globalization;
using System.Threading.Tasks;
using GHM.Survey.Domain.IServices;
using GHM.Survey.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Survey.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/survey-groups")]
    public class SurveyGroupController : GhmControllerBase
    {
        private readonly ISurveyGroupService _surveyGroupService;

        public SurveyGroupController(ISurveyGroupService surveyGroupService)
        {
            _surveyGroupService = surveyGroupService;
        }

        /// <param name="keyword">Từ khóa tìm kiếm</param>
        /// <param name="isActive">Đã kích hoạt chưa</param>
        /// <param name="page">Trang hiện tại</param>
        /// <param name="pageSize">Số bản ghi trên trang</param>
        /// <returns></returns>
        [AcceptVerbs("GET")]
        [AllowPermission(PageId.SurveyCategory, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> SearchSurveyGroup(string keyword, bool? isActive, int page = 1, int pageSize = 20)
        {
            var result = await _surveyGroupService.Search(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name,
                keyword, isActive, page, pageSize);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("alls"), AcceptVerbs("GET")]
        [AllowPermission(PageId.SurveyCategory, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _surveyGroupService.GetsAll(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name));
        }

        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.SurveyCategory, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> InsertSurveyGroup([FromBody]SurveyGroupMeta surveyGroupMeta)
        {
            var result = await _surveyGroupService.Insert(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, surveyGroupMeta);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.SurveyCategory, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateSurveyGroup(int id, [FromBody]SurveyGroupMeta surveyGroupMeta)
        {
            var result = await _surveyGroupService.Update(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, id, surveyGroupMeta);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.SurveyCategory, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> DetailSurveyGroup(int id)
        {
            var result = await _surveyGroupService.GetDetail(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, id);
            if (result.Code < 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.SurveyCategory, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> DeleteSurveyGroup(int id)
        {
            var result = await _surveyGroupService.Delete(CurrentUser.TenantId, id);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("trees"), AcceptVerbs("GET")]
        [AllowPermission(PageId.SurveyCategory, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetSurveyGroupTree()
        {
            return Ok(await _surveyGroupService.GetFullSurveyGroupTree(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name));
        }

        [Route("suggestions"), AcceptVerbs("GET")]
        [AllowPermission(PageId.SurveyCategory, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetAllSurveyGroupForSelect(string keyword, int page = 1, int pageSize = 20)
        {
            var result = await _surveyGroupService
                .GetSurveyGroupForSelect(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, keyword, page,
                    pageSize);
            return Ok(result);
        }
    }
}