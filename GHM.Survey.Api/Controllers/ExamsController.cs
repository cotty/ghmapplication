﻿using System.Globalization;
using System.Threading.Tasks;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using GHM.Survey.Domain.IServices;
using GHM.Survey.Domain.ModelMetas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Survey.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]

    public class ExamsController : GhmControllerBase
    {
        private readonly ISurveyUserAnswerTimeService _surveyUserAnswerTimeService;
        private readonly IExamService _examService;

        public ExamsController(ISurveyUserAnswerTimeService surveyUserAnswerTimeService, IExamService examService)
        {
            _surveyUserAnswerTimeService = surveyUserAnswerTimeService;
            _examService = examService;
        }

        [AcceptVerbs("DELETE")]
        //[CheckPermission(PageId.ConfigPatientSubject, Permission.View)]
        public async Task<IActionResult> Delete(string surveyUserAnswerTimesId)
        {
            var result = await _surveyUserAnswerTimeService.Delete(surveyUserAnswerTimesId);
            if (result.Code < 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("time/{surveyUserAnswerTimesId}"), AcceptVerbs("GET")]        
        public async Task<IActionResult> GetSurveyTime(string surveyUserAnswerTimesId)
        {
            return Ok(await _surveyUserAnswerTimeService.GetSurveyTime(surveyUserAnswerTimesId));
        }

        [AcceptVerbs("GET")]        
        public async Task<IActionResult> CheckExistsBySurveyIdAndSurveyUserId(string surveyId, string surveyUserId)
        {
            var result = await _surveyUserAnswerTimeService.CheckExistsBySurveyIdAndSurveyUserId(surveyId, surveyUserId);
            return Ok(result);
        }

        [Route("finish-do-exam/{surveyId}/{surveyUserId}/{surveyUserAnswerTimeId}"), AcceptVerbs("POST"), ValidateModel]
        //[CheckPermission(PageId.Survey, Permission.Update)]
        public async Task<IActionResult> FinishDoExam(string surveyId, string surveyUserId, string surveyUserAnswerTimeId)
        {
            var result = await _surveyUserAnswerTimeService.FinishDoExam(surveyId, surveyUserId, surveyUserAnswerTimeId);
            return Ok(result);
        }

        [Route("overviews/{surveyId}"), AcceptVerbs("POST")]
        public async Task<IActionResult> Overviews(string surveyId)
        {
            var result = await _examService.GetOverview(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, surveyId,
                CurrentUser.Id);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("start/{surveyId}"), AcceptVerbs("GET")]
        public async Task<IActionResult> StartExam(string surveyId)
        {
            var result = await _examService.Start(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, surveyId, CurrentUser.Id);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        // TODO: Change after tested.
        [Route("answer"), AcceptVerbs("POST")]
        public async Task<IActionResult> SaveAnswer([FromBody]UserAnswerMeta userAnswer)
        {
            var result = await _examService.SaveAnswer(CurrentUser.Id, userAnswer);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("detail/{surveyId}/{surveyUserId}/{surveyUserAnswerTimeId}/manager/{isManager}"), AcceptVerbs("GET")]
        public async Task<IActionResult> ExamDetail(string surveyId, string surveyUserId, string surveyUserAnswerTimeId, bool isManager)
        {
            var result = await _examService.GetExamDetail(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, 
                surveyId, surveyUserId, surveyUserAnswerTimeId,isManager);
            if (result.Code <= 0)
                return Forbid();

            return Ok(result);
        }
    }
}