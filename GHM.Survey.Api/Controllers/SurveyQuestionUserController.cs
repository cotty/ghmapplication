﻿using GHM.Infrastructure;
using GHM.Survey.Domain.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Survey.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/survey-question-user")]
    public class SurveyQuestionUserController : GhmControllerBase
    {
        private readonly ISurveyQuestionUserService _surveyQuestionUserService;

        public SurveyQuestionUserController(ISurveyQuestionUserService surveyQuestionUserService)
        {
            _surveyQuestionUserService = surveyQuestionUserService;
        }  
    }
}