﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Threading.Tasks;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Filters;
using GHM.Infrastructure.Helpers;
using GHM.Survey.Domain.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace GHM.Survey.Api.Controllers
{
    [Authorize]
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class ReportsController : GhmControllerBase
    {
        private readonly IReportService _reportService;
        private readonly ISurveyService _surveyService;
        private readonly ISurveyUserAnswerTimeService _surveyUserAnswerTimeService;

        public ReportsController(IReportService reportService, ISurveyUserAnswerTimeService surveyUserAnswerTimeService,
            ISurveyService surveyService)
        {
            _reportService = reportService;
            _surveyUserAnswerTimeService = surveyUserAnswerTimeService;
            _surveyService = surveyService;
        }

        [AcceptVerbs("GET")]
        [AllowPermission(PageId.SurveyReport, Permission.View)]
        [CheckPermission]
        public IActionResult Search(string keyword, int? surveyGroupId, DateTime? startDate, DateTime? endDate, int page = 1,
            int pageSize = 10)
        {
            var result = _reportService.SearchReport(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name,
                keyword, surveyGroupId, startDate, endDate, page, pageSize);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{surveyId}/users"), AcceptVerbs("GET")]
        [AllowPermission(PageId.SurveyReport, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> SurveyUsersReport(string surveyId, string keyword, int page = 1, int pageSize = 10)
        {
            var result = await _reportService.SearchUsersReport(CurrentUser.TenantId, surveyId, keyword, page, pageSize);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{surveyId}/{surveyUserId}"), AcceptVerbs("GET")]
        public async Task<IActionResult> SurveyReportDetail(string surveyId, string surveyUserId)
        {
            var result = await _reportService.SearchUsersReportDetail(CurrentUser.TenantId, surveyId, surveyUserId);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{surveyId}/{surveyUserId}/times/${surveyUserAnswerTime}"), AcceptVerbs("GET")]
        public async Task<IActionResult> SurveyUserAnswerTimesDetail(string surveyId, string surveyUserId)
        {
            var result =
                await _reportService.SearchReportSurveyUserAnswerTimes(CurrentUser.TenantId, surveyId, surveyUserId);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{surveyId}/{surveyUserAnswerTimeId}/question-groups"), AcceptVerbs("GET")]
        public async Task<IActionResult> SearchQuestionGroup(string surveyId, string surveyUserAnswerTimeId)
        {
            var result = await _reportService.GetQuestionGroupReport(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name,
                surveyId, surveyUserAnswerTimeId);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{surveyId}/{surveyUserId}/{surveyUserAnswerTimeId}/view-result/{isViewResult}"), AcceptVerbs("POST")]
        public async Task<IActionResult> UpdateIsViewResult(string surveyId, string surveyUserId, string surveyUserAnswerTimeId, bool isViewResult)
        {
            var result = await _surveyUserAnswerTimeService.UpdateIsViewResult(CurrentUser.TenantId,
                surveyId, surveyUserId, surveyUserAnswerTimeId, isViewResult);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("all-view-result/{isViewResult}"), AcceptVerbs("POST")]
        public async Task<IActionResult> UpdateAllIsViewResult([FromBody]List<string> surveyUserAnswerTimeIds, bool isViewResult)
        {
            var result = await _surveyUserAnswerTimeService.UpdateAllIsViewResult(surveyUserAnswerTimeIds, isViewResult);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("users"), AcceptVerbs("GET")]
        public async Task<IActionResult> GetListSurveyByUserId(string keyWord, int? surveyGroupId,
             DateTime? startDate, DateTime? endDate, int page, int pageSize)
        {
            var result = await _reportService.GetSurveyByUserId(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, CurrentUser.Id,
                keyWord, surveyGroupId, startDate, endDate, page, pageSize);

            return Ok(result);
        }

        #region Export
        [Route("export-report-user/{surveyId}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.TargetReport, Permission.Export)]
        [CheckPermission]
        public async Task<IActionResult> ExportReport(string surveyId)
        {        
            var listReport = await _reportService.SearchUsersReport(CurrentUser.TenantId, surveyId, string.Empty, 1, int.MaxValue);

            using (var xls = new ExcelPackage())
            {
                var sheet = xls.Workbook.Worksheets.Add("Sheet1");
                var colorTitle = ColorTranslator.FromHtml("#003300");
                var colorHeader = ColorTranslator.FromHtml("#3598dc");
                var colorWhite = ColorTranslator.FromHtml("#ffffff");

                ExcelHelper.CreateCellTable(sheet, 1, 1, 1, 8, "Danh sách người tham gia", new CustomExcelStyle
                    {
                        IsBold = true,
                        FontSize = 20,
                        IsMerge = true,
                        HorizontalAlign = ExcelHorizontalAlignment.Center
                    });                

                ExcelHelper.CreateHeaderTable(sheet, 4, 1, "STT", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                ExcelHelper.CreateHeaderTable(sheet, 4, 2, "Họ tên", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                ExcelHelper.CreateHeaderTable(sheet, 4, 3, "Phòng ban", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                ExcelHelper.CreateHeaderTable(sheet, 4, 4, "Chức danh", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                ExcelHelper.CreateHeaderTable(sheet, 4, 5, "Số câu trả lời", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                ExcelHelper.CreateHeaderTable(sheet, 4, 6, "Số câu trả lời đúng", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                ExcelHelper.CreateHeaderTable(sheet, 4, 7, "Số lần thi", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);
                ExcelHelper.CreateHeaderTable(sheet, 4, 8, "Thời gian làm bài(phút)", ExcelHorizontalAlignment.Center, true, colorHeader, colorWhite, 13, true);

                var rowIndex = 5;                
                foreach (var item in listReport.Items)
                {
                    ExcelHelper.CreateCellTable(sheet, rowIndex, 1, rowIndex - 4, ExcelHorizontalAlignment.Center, false);
                    ExcelHelper.CreateCellTable(sheet, rowIndex, 2,  item.FullName, ExcelHorizontalAlignment.Left, false, true);
                    ExcelHelper.CreateCellTable(sheet, rowIndex, 3, item.OfficeName, ExcelHorizontalAlignment.Center, false, true);
                    ExcelHelper.CreateCellTable(sheet, rowIndex, 4, item.PositionName, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Left,
                        Border = ExcelBorderStyle.Thin,
                    });
                    ExcelHelper.CreateCellTable(sheet, rowIndex, 5, $"{item.TotalUserAnswers}/{item.TotalQuestions}", new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        Border = ExcelBorderStyle.Thin,
                    });
                    ExcelHelper.CreateCellTable(sheet, rowIndex, 6, $"{item.TotalCorrectAnswers}/{item.TotalQuestions}", new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        Border = ExcelBorderStyle.Thin,
                    });
                    ExcelHelper.CreateCellTable(sheet, rowIndex, 7, item.TotalTimes, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        Border = ExcelBorderStyle.Thin,

                    });
                    ExcelHelper.CreateCellTable(sheet, rowIndex, 8, item.TotalSeconds/60, new CustomExcelStyle
                    {
                        HorizontalAlign = ExcelHorizontalAlignment.Center,
                        Border = ExcelBorderStyle.Thin
                    });
                    rowIndex++;                   
                }
                
                sheet.Cells.Style.WrapText = true;
                sheet.Column(2).Width = 40;
                sheet.Column(3).Width = 30;
                sheet.Column(4).Width = 30;
                sheet.Column(5).Width = 20;
                sheet.Column(6).Width = 20;
                sheet.Column(7).Width = 20;
                sheet.Row(1).Height = 30;
                sheet.Row(4).Height = 30;
                sheet.Cells.Style.Font.Size = 11;
                sheet.Row(1).Style.Font.Size = 20;
                sheet.Cells.Style.Font.Name = "Times New Roman";
                var stream = new System.IO.MemoryStream(xls.GetAsByteArray())
                {
                    // Reset Stream Position
                    //Position = 0
                };

                var fileName = "Dánh sách người tham gia thi.xlsx";
                var fileType = "application/vnd.ms-excel";
                return File(stream, fileType, fileName);
            }
        }
        #endregion
    }
}