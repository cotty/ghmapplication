﻿using System.Threading.Tasks;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using GHM.Survey.Domain.IServices;
using GHM.Survey.Domain.ModelMetas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Survey.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/question-approves")]

    public class QuestionApproverConfigController : GhmControllerBase
    {
        private readonly IQuestionApproverConfigService _questionApproverConfigService;

        public QuestionApproverConfigController(IQuestionApproverConfigService questionApproverConfigService)
        {
            _questionApproverConfigService = questionApproverConfigService;
        }

        [AcceptVerbs("GET")]
        [AllowPermission(PageId.SurveyConfigApprover, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string keyword, int page = 1, int pageSize = 20)
        {
            var result = await _questionApproverConfigService.SearchAsync(CurrentUser.TenantId, keyword, page, pageSize);
            return Ok(result);
        }

        [Route("{userId}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.SurveyConfigApprover, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert(string userId)
        {
            var result = await _questionApproverConfigService.Insert(CurrentUser.TenantId, userId);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }        

        [Route("{userId}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.SurveyConfigApprover, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string userId)
        {
            var result = await _questionApproverConfigService.Delete(CurrentUser.TenantId, userId);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
    }
}