﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using GHM.Survey.Domain.Constants;
using GHM.Survey.Domain.IServices;
using GHM.Survey.Domain.ModelMetas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Survey.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class QuestionsController : GhmControllerBase
    {
        private readonly IQuestionService _questionService;

        public QuestionsController(IQuestionService questionService)
        {
            _questionService = questionService;
        }

        [Route("{isSend}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.SurveyQuestion, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]QuestionMeta questionMeta, bool isSend)
        {
            var result = await _questionService.Insert(CurrentUser.TenantId, string.Empty, isSend, CurrentUser.Id, CurrentUser.FullName,
                CurrentUser.Avatar, questionMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{versionId}/{questionId}/{isSend}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.SurveyQuestion, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string versionId, string questionId, bool isSend, [FromBody]QuestionMeta questionMeta)
        {
            var result = await _questionService.Update(CurrentUser.TenantId, versionId, questionId, isSend,
                CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar, questionMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{questionversionId}"), AcceptVerbs("DELETE"), ValidateModel]
        [AllowPermission(PageId.SurveyQuestion, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string questionversionId)
        {
            var result = await _questionService.Delete(CurrentUser.Id, questionversionId);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("deletes"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.SurveyQuestion, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> DeleteListQuestion([FromBody]List<string> questionversionIds)
        {
            var result = await _questionService.DeleteListQuestion(questionversionIds);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{questionversionId}/status/{questionStatus}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.SurveyQuestion, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> ChangeQuestionStatus(string questionVersionId, QuestionStatus questionStatus, string reason)
        {
            var result = await _questionService.ChangeQuestionStatus(CurrentUser.TenantId, questionVersionId, questionStatus, reason,
                CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("change-list-question-status"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.SurveyQuestion, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> ChangListeQuestionStatus([FromBody]ChangeListQuestionStatusMeta listQuestionStatusMeta)
        {
            var result = await _questionService.ChangeListQuestionStatus(CurrentUser.TenantId, listQuestionStatusMeta.QuestionVersionIds,
                listQuestionStatusMeta.QuestionStatus, listQuestionStatusMeta.Reason, CurrentUser.Id, CurrentUser.FullName,
                CurrentUser.Avatar);

            return Ok(result);
        }

        [Route("{versionId}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.SurveyQuestion, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetDetail(string versionId)
        {
            var result = await _questionService.GetDetail(CurrentUser.TenantId, CurrentUser.Id, versionId);
            if (result.Code < 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("approverUser/{approverUserId}/language/{languageId}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.SurveyQuestion, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetAllQuestionByApproverUserId(string tenantId, string approverUserId, string languageId)
        {
            var result = await _questionService.GetAllQuestionByApproverUserId(CurrentUser.TenantId, approverUserId, languageId);
            if (result.Code < 0)
                return BadRequest(result);
            return Ok(result);
        }

        [AcceptVerbs("GET")]
        [AllowPermission(PageId.SurveyQuestion, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string keyword, QuestionType? questionType, int? questionGroupId, string creatorId,
            QuestionStatus? questionStatus, int page = 1, int pageSize = 20)
        {
            var result = await _questionService.Search(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name,
               keyword, questionType, questionGroupId, creatorId, CurrentUser.Id, questionStatus, page, pageSize);
            return Ok(result);
        }

        [Route("approves"), AcceptVerbs("GET")]
        [AllowPermission(PageId.SurveyQuestionApprove, Permission.View, Permission.Approve)]
        [CheckPermission]
        public async Task<IActionResult> SearchForApprove(string keyword, QuestionType? questionType, int? questionGroupId, string creatorId,
            QuestionStatus? questionStatus, int page = 1, int pageSize = 20)
        {
            var result = await _questionService.SearchQuestionForApprove(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, CurrentUser.Id,
                keyword, questionType, questionGroupId, creatorId, page, pageSize);
            return Ok(result);
        }

        [Route("suggestions"), AcceptVerbs("GET")]
        [AllowPermission(PageId.SurveyQuestion, Permission.View, Permission.Insert, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> SearchSuggestion(string keyword, int? questionGroupId, QuestionType? type, SurveyType ? surveyType, int page = 1, int pageSize = 10)
        {
            var result = await _questionService.SearchForSuggestion(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name,
                keyword, questionGroupId, type, surveyType, page, pageSize);

            return Ok(result);
        }

        [Route("decline-reasons/{questionVersionId}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.SurveyQuestionApprove, Permission.Approve)]
        [CheckPermission]
        public async Task<IActionResult> UpdateDeclineReason(string questionVersionId, string declineReason)
        {
            var result = await _questionService.UpdateDeclineReason(questionVersionId, declineReason);

            return Ok(result);
        }
    }
}