﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using GHM.Survey.Domain.IServices;
using GHM.Survey.Domain.ModelMetas;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Survey.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class SurveysController : GhmControllerBase
    {
        private readonly ISurveyService _surveyService;
        public SurveysController(ISurveyService surveyService)
        {
            _surveyService = surveyService;
        }

        [AcceptVerbs("GET")]
        [AllowPermission(PageId.Survey, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string keyword, int? surveyGroupId,
            DateTime? startDate, DateTime? endDate, bool? isActive, int page = 1, int pageSize = 20)
        {
            var result = await _surveyService.Search(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, keyword,
                surveyGroupId, startDate, endDate, isActive, page, pageSize);
            return Ok(result);
        }

        [AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.Survey, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert([FromBody]SurveyMeta surveyMeta)
        {
            var result = await _surveyService.Insert(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar,
                surveyMeta);

            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.Survey, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string id, [FromBody]SurveyMeta surveyMeta)
        {
            var result = await _surveyService.Update(CurrentUser.TenantId, CurrentUser.Id, CurrentUser.FullName, CurrentUser.Avatar, id,
                surveyMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}/questions"), AcceptVerbs("GET")]
        [AllowPermission(PageId.Survey, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> GetSurveyQuestions(string id)
        {
            var result = await _surveyService.GetSurveyQuestions(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, id);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}/questions"), AcceptVerbs("POST")]
        [AllowPermission(PageId.Survey, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateSurveyQuestions(string id, [FromBody]SurveyListQuestionGroupMeta surveyQusetionGroupMeta)
        {
            var result = await _surveyService.SaveQuestions(CurrentUser.TenantId, id, CurrentUser.Id, CurrentUser.FullName,
                CurrentUser.Avatar, surveyQusetionGroupMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.Survey, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _surveyService.Delete(CurrentUser.TenantId, id);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.Survey, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Detail(string id)
        {
            var result = await _surveyService.GetDetail(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, id);
            if (result.Code < 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("require-survey")]
        public async Task<IActionResult> GetRequireSurvey()
        {
            return Ok(1);
        }

    }
}