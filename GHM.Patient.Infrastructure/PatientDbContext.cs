﻿using GHM.Infrastructure.SqlServer;
using GHM.Patient.Infrastructure.Configurations;
using Microsoft.EntityFrameworkCore;

namespace GHM.Patient.Infrastructure
{
    public class PatientDbContext : DbContextBase
    {
        public PatientDbContext(DbContextOptions<PatientDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Configurations:
            builder.ApplyConfiguration(new JobConfig());
            builder.ApplyConfiguration(new JobTranslationConfig());
            builder.ApplyConfiguration(new PatientResourceConfig());
            builder.ApplyConfiguration(new PatientResourceTranslationConfig());
            builder.ApplyConfiguration(new PatientConfig());
            builder.ApplyConfiguration(new PatientSubjectConfig());
            builder.ApplyConfiguration(new PatientSubjectTranslationConfig());
            builder.ApplyConfiguration(new PatientContactConfig());
            builder.ApplyConfiguration(new PatientRelativesContactConfig());
        }
    }
}
