﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Patient.Domain.IRepository;
using GHM.Patient.Domain.Models;
using GHM.Patient.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;

namespace GHM.Patient.Infrastructure.Repository
{
    public class PatientResourceRepository : RepositoryBase, IPatientResourceRepository
    {

        private readonly IRepository<PatientResource> _patientResourceRepository;

        public PatientResourceRepository(IDbContext context) : base(context)
        {
            _patientResourceRepository = Context.GetRepository<PatientResource>();
        }

        public Task<List<PatientResourceViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive, int page, int pageSize, out int totalRows)
        {
            Expression<Func<PatientResource, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId;
            Expression<Func<PatientResourceTranslation, bool>> specTranslation = pt => pt.LanguageId == languageId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            if (isActive.HasValue)
            {
                spec = spec.And(x => x.IsActive == isActive.Value);
            }

            var query = Context.Set<PatientResource>().Where(spec)
                .Join(Context.Set<PatientResourceTranslation>().Where(specTranslation), x => x.Id, pt => pt.PatientResourceId, (x, pt) =>
                    new PatientResourceViewModel
                    {
                        Id = x.Id,
                        Name = pt.Name,
                        CreateTime = x.CreateTime,
                        Description = pt.Description,
                        IsActive = x.IsActive,
                        Order = x.Order
                    }).AsNoTracking();

            totalRows = _patientResourceRepository.Count(spec);


            return query
                .OrderBy(x => x.Order)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<int> Insert(PatientResource patientResource)
        {
            _patientResourceRepository.Create(patientResource);
            return await Context.SaveChangesAsync();
        }


        public async Task<int> Update(PatientResource patientResource)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string patientResourceId)
        {
            var info = await GetInfo(patientResourceId);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(string patientResourceId)
        {
            var info = await GetInfo(patientResourceId);
            if (info == null)
                return -1;

            _patientResourceRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<PatientResource> GetInfo(string id, bool isReadonly = false)
        {
            return await _patientResourceRepository.GetAsync(isReadonly, x => x.Id == id);
        }

        public Task<List<PatientResourceForSelectViewModel>> SearchForSelect(string tenantId, string languageId, string keyword, int page, int pageSize, out int totalRows)
        {
            Expression<Func<PatientResource, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId && x.IsActive;
            Expression<Func<PatientResourceTranslation, bool>> specTranslation = t => t.LanguageId == languageId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            var query = Context.Set<PatientResource>().Where(spec)
                .Join(Context.Set<PatientResourceTranslation>().Where(specTranslation), patient => patient.Id,
                    patientTranslation => patientTranslation.PatientResourceId,
                    (patient, patientTranslation) => new { patient, patientTranslation })
                    .OrderBy(x => x.patient.Order)
                    .Select(x => new PatientResourceForSelectViewModel
                    {
                        Id = x.patient.Id,
                        Name = x.patientTranslation.Name,
                    });

            totalRows = _patientResourceRepository.Count(spec);
            return query
                .AsNoTracking()
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }
    }
}
