﻿using GHM.Infrastructure.SqlServer;
using GHM.Patient.Domain.IRepository;
using GHM.Patient.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Patient.Infrastructure.Repository
{
  public  class PatientSubjectTranslationRepository: RepositoryBase, IPatientSubjectTranslationRepository
    {
        private readonly IRepository<PatientSubjectTranslation> _patientSubjectTranslationRepository;
        public PatientSubjectTranslationRepository(IDbContext context): base(context)
        {
            _patientSubjectTranslationRepository = context.GetRepository<PatientSubjectTranslation>();
        }

        public async Task<int> Insert(PatientSubjectTranslation patientSubjectTranslation)
        {
            _patientSubjectTranslationRepository.Create(patientSubjectTranslation);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<PatientSubjectTranslation> patientSubjectTranslations)
        {
            _patientSubjectTranslationRepository.Creates(patientSubjectTranslations);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(PatientSubjectTranslation patientSubjectTranslation)
        {
            return await Context.SaveChangesAsync();
        }

        //public async Task<int> Delete(string patientSubjectTranslationsId)
        //{
        //    var info = await GetInfo(patientSubjectTranslationsId);
        //    if (info == null)
        //        return -1;

        //    info.IsDelete = true;
        //    return await Context.SaveChangesAsync();
        //}

        public async Task<int> ForceDelete(string PatientSubjectId)
        {
            var info = await _patientSubjectTranslationRepository.GetsAsync(false, x => x.PatientSubjectId  == PatientSubjectId);
            if (info == null || !info.Any())
                return -1;

            _patientSubjectTranslationRepository.Deletes(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<PatientSubjectTranslation> GetInfo(string patientsubjectId, string languageId, bool isReadOnly = false)
        {
            return await _patientSubjectTranslationRepository.GetAsync(isReadOnly, x => x.PatientSubjectId  == patientsubjectId && x.LanguageId == languageId);
        }
        
        public async Task<bool> CheckUserNameExists(string patientsubjectId, string languageId, string name)
        {
            return await _patientSubjectTranslationRepository.ExistAsync(x => x.PatientSubjectId != patientsubjectId && x.LanguageId == languageId && x.Name == name);
        }

        public async Task<List<PatientSubjectTranslation>> GetsByPatientSubjectId(string patientsubjectId)
        {
            return await _patientSubjectTranslationRepository.GetsAsync(true, x => x.PatientSubjectId== patientsubjectId);
        }
       
    }
}
