﻿using GHM.Infrastructure.SqlServer;
using GHM.Patient.Domain.IRepository;
using System;
using System.Collections.Generic;
using GHM.Patient.Domain.Models;
using System.Threading.Tasks;
using System.Linq.Expressions;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using System.Linq;
using GHM.Patient.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace GHM.Patient.Infrastructure.Repository
{
    public class PatientSubjectRepository : RepositoryBase, IPatientSubjectRepository 
    {
        private readonly IRepository<PatientSubject> _patientsubjectRepository;

        public PatientSubjectRepository(IDbContext context) : base(context)
        {
            _patientsubjectRepository = Context.GetRepository<PatientSubject>();
        }

        public Task<List<PatientSubjectViewModel>> Search(string tenantId, string languageId, string keyword, float? totalReduction, 
                        bool? isActive, int page, int pageSize, out int totalRows)
        {
            Expression<Func<PatientSubject, bool>> spec = t => !t.IsDelete && t.TenantId == tenantId;
            Expression<Func<PatientSubjectTranslation, bool>> specTranslation = tt => tt.LanguageId == languageId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            if (isActive.HasValue)
            {
                spec = spec.And(x => x.IsActive == isActive.Value);
            }

            if(totalReduction.HasValue)
            {
                spec = spec.And(x => x.TotalReduction == totalReduction.Value);
            }

            var query = Context.Set<PatientSubject>().Where(spec)
                .Join(Context.Set<PatientSubjectTranslation>().Where(specTranslation), t => t.Id, tt => tt.PatientSubjectId, (t, tt) =>
                    new PatientSubjectViewModel
                    {
                        Id = t.Id,
                        Order = t.Order,
                        TotalReduction = t.TotalReduction,
                        IsActive = t.IsActive,
                        Description = tt.Description,
                        Name = tt.Name ,
                    }).AsNoTracking();

            totalRows = _patientsubjectRepository.Count(spec);
            return query
                .OrderBy(x => x.Order)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<int> Insert(PatientSubject patientsubject)
        {
            _patientsubjectRepository.Create(patientsubject);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(PatientSubject patientsubject)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string patientsubjectId)
        {
            var info = await GetInfo(patientsubjectId);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(string patientsubjectId)
        {
            var info = await GetInfo(patientsubjectId);
            if (info == null)
                return -1;

            _patientsubjectRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<PatientSubject> GetInfo(string patientsubjectId, bool isReadOnly = false)
        {
            return await _patientsubjectRepository.GetAsync(isReadOnly, x => x.Id == patientsubjectId);
        }

        public async Task<PatientSubject> GetInfo(string patientsubjectId, string tenantId, bool isReadOnly = false)
        {
            return await _patientsubjectRepository.GetAsync(isReadOnly, x => x.TenantId == tenantId && x.Id == patientsubjectId);// && !x.IsDelete);
        }

        public async Task<bool> CheckExistsByPatientSubjectId(string patientsubjecId)
        {
            return await _patientsubjectRepository.ExistAsync(x => x.Id == patientsubjecId && !x.IsDelete);
        }

        public async Task<List<PatientSubject>> GetAll(string tenantId, bool isReadOnly = false)
        {
            return await _patientsubjectRepository.GetsAsync(isReadOnly, x => !x.IsDelete);
        }

    }
}
