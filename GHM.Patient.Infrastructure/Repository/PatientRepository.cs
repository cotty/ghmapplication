﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Autofac.Builder;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using GHM.Infrastructure.ViewModels;
using GHM.Patient.Domain.IRepository;
using GHM.Patient.Domain.Models;
using GHM.Patient.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace GHM.Patient.Infrastructure.Repository
{
    public class PatientRepository : RepositoryBase, IPatientRepository
    {
        private readonly IRepository<Domain.Models.Patient> _patientRepository;

        public PatientRepository(IDbContext context) : base(context)
        {
            _patientRepository = Context.GetRepository<Domain.Models.Patient>();
        }

        public Task<List<PatientSearchViewModel>> Search(string tenantId, string keyword, DateTime? createDate, int page, int pageSize, out int totalRows)
        {
            Expression<Func<GHM.Patient.Domain.Models.Patient, bool>> spec = t => t.TenantId == tenantId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
            }

            if (createDate.HasValue)
            {
                createDate = createDate.Value.Date;
                spec = spec.And(x => x.CreateTime.Date == createDate);
            }

            var query = Context.Set<Domain.Models.Patient>().Where(spec).Select(x => new PatientSearchViewModel
            {
                Id = x.Id,
                PatientCode = x.PatientCode,
                FullName = x.FullName,
                Birthday = x.Birthday,
                Address = x.Address
            });

            totalRows = _patientRepository.Count(spec);
            return query.OrderByDescending(x => x.FullName)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<int> Insert(Domain.Models.Patient patient)
        {
            _patientRepository.Create(patient);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(string id)
        {
            var info = await GetInfo(id);
            if (info == null)
                return -1;

            _patientRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(Domain.Models.Patient patient)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateUnsignName(string id, string unsignName)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<Domain.Models.Patient> GetInfo(string id, bool isReadOnly = false)
        {
            return await _patientRepository.GetAsync(isReadOnly, x => x.Id == id);
        }
    }

    public class ContactPatientRepository : RepositoryBase, IContactPatientRepository
    {
        private readonly IRepository<PatientRelativesContact> _contactPatientRepository;

        public ContactPatientRepository(IDbContext context) : base(context)
        {
            _contactPatientRepository = Context.GetRepository<PatientRelativesContact>();
        }

        public async Task<int> DeleteByPatientId(string patientId)
        {
            var listRelativesContacts = await _contactPatientRepository.GetsAsync(false, x => x.PatientId == patientId);
            if (listRelativesContacts == null || !listRelativesContacts.Any())
                return -1;

            _contactPatientRepository.Deletes(listRelativesContacts);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(string Id)
        {
            var info = await _contactPatientRepository.GetsAsync(false, x => x.Id == Id);
            if (info == null || !info.Any())
                return -1;

            _contactPatientRepository.Deletes(info);
            return await Context.SaveChangesAsync();
        }

        public Task<List<ContactPatientViewModel>> GetByPatientId(string patinetId)
        {
            return Context.Set<PatientRelativesContact>().AsNoTracking().Where(x => x.PatientId == patinetId)
                          .Select(x => new ContactPatientViewModel
                          {
                              Id = x.Id,
                              PatientId = x.PatientId,
                              FullName = x.FullName,
                              PhoneNumber = x.PhoneNumber,
                              ConcurrencyStamp = x.ConcurrencyStamp
                          }).ToListAsync();
        }

        public async Task<PatientRelativesContact> GetInfo(string id)
        {
            return await _contactPatientRepository.GetAsync(false, x => x.Id == id);
        }

        public async Task<int> Insert(PatientRelativesContact contactPatient)
        {
            _contactPatientRepository.Create(contactPatient);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<PatientRelativesContact> contactPatients)
        {
            _contactPatientRepository.Creates(contactPatients);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(PatientRelativesContact contactPatient)
        {
            return await Context.SaveChangesAsync();
        }
    }

    public class PatientContactRepository : RepositoryBase, IPatientContactRepository
    {
        private readonly IRepository<PatientContact> _patientContactRepository;

        public PatientContactRepository(IDbContext context) : base(context)
        {
            _patientContactRepository = Context.GetRepository<Domain.Models.PatientContact>();
        }

        public async Task<int> DeleteByPatientId(string patientId)
        {
            var listPatientContacts = await _patientContactRepository.GetsAsync(false, x => x.PatientId == patientId);
            if (listPatientContacts == null || !listPatientContacts.Any())
                return -1;

            _patientContactRepository.Deletes(listPatientContacts);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(string Id)
        {
            var info = await _patientContactRepository.GetsAsync(false, x => x.Id == Id);
            if (info == null || !info.Any())
                return -1;

            _patientContactRepository.Deletes(info);
            return await Context.SaveChangesAsync();
        }

        public Task<List<PatientContactViewModel>> GetByPatientId(string patientId)
        {
            return Context.Set<PatientContact>().AsNoTracking().Where(c => c.PatientId == patientId)
                .Select(x => new PatientContactViewModel
                {
                    Id = x.Id,
                    PatientId = x.PatientId,
                    ContactType = x.ContactType,
                    ContactValue = x.ContactValue,
                    ConcurrencyStamp = x.ConcurrencyStamp,
                }).ToListAsync();
        }

        public async Task<PatientContact> GetInfo(string Id)
        {
            return await _patientContactRepository.GetAsync(false, x => x.Id == Id);
        }

        public async Task<int> Insert(PatientContact patientContact)
        {
            _patientContactRepository.Create(patientContact);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<PatientContact> patientContacts)
        {
            _patientContactRepository.Creates(patientContacts);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(PatientContact patientContact)
        {
            return await Context.SaveChangesAsync();
        }
    }
}
