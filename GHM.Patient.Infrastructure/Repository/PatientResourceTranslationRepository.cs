﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Patient.Domain.IRepository;
using GHM.Patient.Domain.Models;
using GHM.Patient.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using GHM.Patient.Domain.ModelMetas;
using Microsoft.EntityFrameworkCore;
namespace GHM.Patient.Infrastructure.Repository
{
    public class PatientResourceTranslationRepository : RepositoryBase, IPatientResourceTranslationRepository
    {

        private readonly IRepository<PatientResourceTranslation> _patientResourceTranslationRepository;

        public  PatientResourceTranslationRepository(IDbContext context) : base(context)
        {
            _patientResourceTranslationRepository = Context.GetRepository<PatientResourceTranslation>();
        }

        public async Task<int> Insert(PatientResourceTranslation patientResourceTranslation)
        {
            _patientResourceTranslationRepository.Create(patientResourceTranslation);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<PatientResourceTranslation> patientResourceTranslations)
        {
            _patientResourceTranslationRepository.Creates(patientResourceTranslations);
            return await Context.SaveChangesAsync();
        }

 

        public async Task<int> Update(PatientResourceTranslation patientResourceTranslation)
        {
            return await Context.SaveChangesAsync();
        }



        public async Task<int> Delete(string patientResourceId, string languageId)
        {
            var info = await GetInfo(patientResourceId, languageId);
            if (info == null)
                return -1;

            _patientResourceTranslationRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeleteByPatientResourceId(string patientResourceId)
        {
            var listPatientResourceLanguages = await GetsByPatientResourceId(patientResourceId);
            _patientResourceTranslationRepository.Deletes(listPatientResourceLanguages);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDeleteByPatientResourceId(string patientResourceId)
        {
            var patientResourceTranslations = await _patientResourceTranslationRepository.GetsAsync(false, x => x.PatientResourceId == patientResourceId);
            if (!patientResourceTranslations.Any())
                return -1;

            _patientResourceTranslationRepository.Deletes(patientResourceTranslations);
            return await Context.SaveChangesAsync();
        }

        public async Task<PatientResourceTranslation> GetInfo(string patientResourceId, string languageId, bool isReadOnly = false)
        {
            return await _patientResourceTranslationRepository.GetAsync(isReadOnly, x => x.PatientResourceId == patientResourceId && x.LanguageId == languageId);
        }

        public async Task<List<PatientResourceTranslation>> GetsByPatientResourceId(string patientResourceId)
        {
            return await _patientResourceTranslationRepository.GetsAsync(true, x => x.PatientResourceId == patientResourceId);
        }
        public async Task<bool> CheckExists(string patientResourceId, string languageId, string name)
        {
            name = name.Trim();
            return await _patientResourceTranslationRepository.ExistAsync(x =>
                x.PatientResourceId != patientResourceId && x.LanguageId == languageId && x.Name == name);
        }

    
    }
}
