﻿//using GHM.Hr.Domain.Models;
//using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using GHM.Patient.Domain.IRepository;
using GHM.Patient.Domain.IServices;
using GHM.Patient.Domain.ModelMetas;
using GHM.Patient.Domain.Models;
using GHM.Patient.Domain.Resources;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using GHM.Patient.Domain.ViewModels;
using GHM.Infrastructure.Extensions;

namespace GHM.Patient.Infrastructure.Services
{
    public class PatientSubjectService : IPatientSubjectService
    {
        private readonly IPatientSubjectRepository _patientsubjectRepository;
        private readonly IPatientSubjectTranslationRepository _patientsubjectTranslationRepository;
      
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmPatientResource> _patientResourceService;

        public PatientSubjectService(IPatientSubjectRepository patientsubjectRepository, IPatientSubjectTranslationRepository patientsubjectTranslationRepository, 
                    IResourceService<SharedResource> sharedResourceService, IResourceService<GhmPatientResource> patientResourceService)
        {
            _patientsubjectRepository = patientsubjectRepository;
            _patientsubjectTranslationRepository = patientsubjectTranslationRepository;
            _sharedResourceService = sharedResourceService;
            _patientResourceService = patientResourceService;
        }

        public async Task<ActionResultResponse> Insert(string tenantId, PatientSubjectMeta patientsubjectMeta)
        {
            if (!patientsubjectMeta.PatientSubjectTranslations.Any())
                return new ActionResultResponse(-1, _sharedResourceService.GetString("Please enter at least one language."));

            var patientsubjectId = Guid.NewGuid().ToString();
            var patientsubject = new PatientSubject
            {
                Id = patientsubjectId,
                //ConcurrencyStamp = patientsubjectId,
                IsActive = patientsubjectMeta.IsActive,
                //CreateTime = DateTime.Now,
                Order = patientsubjectMeta.Order,
                TotalReduction = patientsubjectMeta.TotalReduction,
                //IsDelete = false,
                TenantId = tenantId,
            };

            var result = await _patientsubjectRepository.Insert(patientsubject);
            if (result <= 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            var patientsubjectTranslations = new List<PatientSubjectTranslation>();
            foreach (var patientsubjectTranslationMeta in patientsubjectMeta.PatientSubjectTranslations)
            {
                var isNameExists = await _patientsubjectTranslationRepository.CheckUserNameExists(patientsubjectTranslationMeta.PatientSubjectId,
                                patientsubjectTranslationMeta.LanguageId , patientsubjectTranslationMeta.Name);
                if (isNameExists)
                {
                    await RollbackInsert(patientsubjectId);
                    return new ActionResultResponse(-3, _patientResourceService.GetString("Name: \"{0}\" already exists.", patientsubjectTranslationMeta.Name));
                }


                patientsubjectTranslations.Add(new PatientSubjectTranslation
                {
                    PatientSubjectId = patientsubjectId,
                    LanguageId = patientsubjectTranslationMeta.LanguageId.Trim(),
                    Name = patientsubjectTranslationMeta.Name.Trim(),
                    UnsignName = patientsubjectTranslationMeta.Name.Trim().StripVietnameseChars().ToUpper(),
                    Description = patientsubjectTranslationMeta.Description?.Trim(),
                });
            }

            // Insert PatientSubject translations.
            var resultInsertTranslation = await _patientsubjectTranslationRepository.Inserts(patientsubjectTranslations);
            if (resultInsertTranslation > 0)
                return new ActionResultResponse(resultInsertTranslation, _patientResourceService.GetString("Add new PatientSubject successful."));

            await RollbackInsert(patientsubjectId);
            return new ActionResultResponse(-5, _patientResourceService.GetString("Can not insert new PatientSubject. Please contact with administrator."));
        }

        public async Task<ActionResultResponse> Delete(string tenantId, string id)
        {
            var info = await _patientsubjectRepository.GetInfo(id, tenantId);
            if (info == null)
                return new ActionResultResponse(-1, _patientResourceService.GetString("PatientSubject does not exists."));

            // Check PatientSubject reference.
            //var isUsed = await _patientsubjectRepository.CheckExistsByPatientSubjectId(info.Id);
            //if (isUsed)
            //    return new ActionResultResponse(-2, _patientResourceService.GetString("PatientSubject is used in office. Can not delete this PatientSubject."));

            var result = await _patientsubjectRepository.Delete(id);
            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                : _patientResourceService.GetString("Delete PatientSubject successful."));
        }
                

        public async Task<SearchResult<PatientSubjectViewModel>> Search(string tenantId, string languageId, string keyword, float? totalReduction, 
                    bool? isActive, int page, int pageSize)
        {
            var items = await _patientsubjectRepository.Search(tenantId, languageId, keyword, totalReduction, isActive, page, pageSize, out var totalRows);
            return new SearchResult<PatientSubjectViewModel>
            {
                Items = items,
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse> Update(string tenantId, string id, PatientSubjectMeta patientsubjectMeta)
        {
            if (!patientsubjectMeta.PatientSubjectTranslations.Any())
                return new ActionResultResponse(-1, _sharedResourceService.GetString("Please enter at least one language."));

            var patientsubjectInfo = await _patientsubjectRepository.GetInfo(id,  tenantId);
            if (patientsubjectInfo == null)
                return new ActionResultResponse(-2, _patientResourceService.GetString("PatientSubject does not exists."));

            if (patientsubjectInfo.ConcurrencyStamp != patientsubjectMeta.ConcurrencyStamp)
                return new ActionResultResponse(-3, _patientResourceService.GetString("The PatientSubject already updated by other people. You can not update this PatientSubject."));

            patientsubjectInfo.LastUpdate = DateTime.Now;
            patientsubjectInfo.IsActive = patientsubjectMeta.IsActive;
            patientsubjectInfo.Order = patientsubjectMeta.Order;
            patientsubjectInfo.TotalReduction = patientsubjectMeta.TotalReduction;
            patientsubjectInfo.ConcurrencyStamp = Guid.NewGuid().ToString();

            foreach (var patientsubjectTranslation in patientsubjectMeta.PatientSubjectTranslations)
            {
                var isNameExists = await _patientsubjectTranslationRepository.CheckUserNameExists(patientsubjectInfo.Id,
                                        patientsubjectTranslation.LanguageId , patientsubjectTranslation.Name);
                if (isNameExists)
                    return new ActionResultResponse(-5, _patientResourceService.GetString("Name: \"{0}\" already exists.", patientsubjectTranslation.Name));

                var patientsubjectPatientTranslationInfo = await _patientsubjectTranslationRepository.GetInfo(patientsubjectInfo.Id,
                    patientsubjectTranslation.LanguageId);

                if (patientsubjectPatientTranslationInfo != null)
                {
                    patientsubjectPatientTranslationInfo.Name = patientsubjectTranslation.Name.Trim();
                    patientsubjectPatientTranslationInfo.Description = patientsubjectTranslation.Description?.Trim();
                    patientsubjectPatientTranslationInfo.UnsignName = patientsubjectTranslation.Name.StripVietnameseChars().ToUpper();
                    await _patientsubjectTranslationRepository.Update(patientsubjectPatientTranslationInfo);
                }
                else
                {
                    patientsubjectTranslation.PatientSubjectId = patientsubjectInfo.Id;
                    patientsubjectTranslation.Name = patientsubjectTranslation.Name.Trim();
                    patientsubjectTranslation.Description = patientsubjectTranslation.Description?.Trim();
                    patientsubjectTranslation.UnsignName = patientsubjectTranslation.Name.StripVietnameseChars().ToUpper();
                    await _patientsubjectTranslationRepository.Insert(patientsubjectTranslation);
                }
            }

            await _patientsubjectRepository.Update(patientsubjectInfo);
            return new ActionResultResponse(1, _patientResourceService.GetString("Update PatientSubject successful."));
        }

        private async Task RollbackInsert(string patientsubjectId)
        {
            await _patientsubjectRepository.ForceDelete(patientsubjectId);
            await _patientsubjectTranslationRepository.ForceDelete(patientsubjectId);
        }

        public async Task<ActionResultResponse<PatientSubjectDetailViewModel>> GetDetail(string tenantId, string languageId, string id)
        {
            var patientsubjectInfo = await _patientsubjectRepository.GetInfo(id, tenantId);
            if (patientsubjectInfo == null)
                return new ActionResultResponse<PatientSubjectDetailViewModel>(-1, _patientResourceService.GetString("Patient subject does not exists."));

            if (patientsubjectInfo.TenantId != tenantId)
                return new ActionResultResponse<PatientSubjectDetailViewModel>(-2, _patientResourceService.GetString("You do not have permission to view this patient subject info."));

            var patientsubjectTranslations = await _patientsubjectTranslationRepository.GetsByPatientSubjectId(id);
            var patientSubjectDetailViewModel = new PatientSubjectDetailViewModel
            {
                IsActive = patientsubjectInfo.IsActive,
                Order = patientsubjectInfo.Order,
                ConcurrencyStamp = patientsubjectInfo.ConcurrencyStamp,
                TotalReduction = patientsubjectInfo.TotalReduction,
                PatientSubjectTranslations = patientsubjectTranslations.Select(x => new PatientSubjectTranslationViewModel
                {
                    LanguageId = x.LanguageId,
                    Name = x.Name,
                    Description = x.Description,
                    UnsignName =x.UnsignName
                }).ToList()
            };
            return new ActionResultResponse<PatientSubjectDetailViewModel>
            {
                Code = 1,
                Data = patientSubjectDetailViewModel
            };
        }

    }
}
