﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Core.Domain.Models;
using GHM.Core.Domain.ViewModels;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.Services;
using GHM.Infrastructure.ViewModels;
using GHM.Patient.Domain.Constants;
using GHM.Patient.Domain.IRepository;
using GHM.Patient.Domain.IServices;
using GHM.Patient.Domain.ModelMetas;
using GHM.Patient.Domain.Models;
using GHM.Patient.Domain.Resources;
using GHM.Patient.Domain.ViewModels;
using Microsoft.Extensions.Configuration;

namespace GHM.Patient.Infrastructure.Services
{
    public class PatientService : IPatientService
    {
        private readonly IPatientRepository _patientRepository;
        private readonly IContactPatientRepository _contactPatientRepository;
        private readonly IPatientContactRepository _patientContactRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmPatientResource> _patientResourceService;
        private readonly IConfiguration _configuration;

        public PatientService(IPatientRepository patientRepository, IContactPatientRepository contactPatientRepository,
            IPatientContactRepository patientContactRepository, IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmPatientResource> patientResourceService, IConfiguration configuration)
        {
            _patientRepository = patientRepository;
            _contactPatientRepository = contactPatientRepository;
            _patientContactRepository = patientContactRepository;
            _sharedResourceService = sharedResourceService;
            _patientResourceService = patientResourceService;
            _configuration = configuration;
        }

        public async Task<ActionResultResponse> ForceDelete(string tenantId, string id)
        {
            var info = await _patientRepository.GetInfo(id);
            if (info == null)
                return new ActionResultResponse(-1, _patientResourceService.GetString("Patient does not exists."));

            var result = await _patientRepository.ForceDelete(id);
            return new ActionResultResponse(result, result <= 0
                ? _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                : _patientResourceService.GetString("Delete Patient successful."));
        }

        public async Task<ActionResultResponse<PatientDetailViewModel>> GetDetail(string patientId)
        {
            var patientInfo = await _patientRepository.GetInfo(patientId, true);
            if (patientInfo == null)
                return new ActionResultResponse<PatientDetailViewModel>(-1, _patientResourceService.GetString("Patient does not exists"));

            var listContactPatient = await _contactPatientRepository.GetByPatientId(patientId);
            var listPatientContact = await _patientContactRepository.GetByPatientId(patientId);

            var patientDetail = new PatientDetailViewModel
            {
                Id = patientInfo.Id,
                ConcurrencyStamp = patientInfo.ConcurrencyStamp,
                PatientCode = patientInfo.PatientCode,
                FullName = patientInfo.FullName,
                Birthday = patientInfo.Birthday,
                Gender = patientInfo.Gender,
                PatientResourceId = patientInfo.PatientResourceId,
                IdCardNumber = patientInfo.IdCardNumber,
                JobId = patientInfo.JobId,
                NationalId = patientInfo.NationalId,
                EthnicId = patientInfo.EthnicId,
                ReligionId = patientInfo.ReligionId,
                ProvinceId = patientInfo.ProvinceId,
                DistrictId = patientInfo.DistrictId,
                Address = patientInfo.Address,

                DistrictName = patientInfo.DistrictName,
                EthnicName = patientInfo.EthnicName,
                NationalName = patientInfo.NationalName,
                ProvinceName = patientInfo.ProvinceName,
                ReligionName = patientInfo.ReligionName,

                ContactPatients = listContactPatient,
                PatientContacts = listPatientContact

            };

            return new ActionResultResponse<PatientDetailViewModel>
            {
                Data = patientDetail,
            };
        }

        public async Task<ActionResultResponse> Insert(string tenantId, PatientMeta patientMeta)
        {
            var patientId = Guid.NewGuid().ToString();
            Domain.Models.Patient patient = new Domain.Models.Patient
            {
                Id = patientId,
                TenantId = tenantId,
                FullName = patientMeta.FullName,
                UnsignName = patientMeta.FullName.Trim().StripVietnameseChars().ToUpper(),
                Birthday = patientMeta.Birthday,
                Gender = patientMeta.Gender,
                PatientResourceId = patientMeta.PatientResourceId,
                IdCardNumber = patientMeta.IdCardNumber,
                JobId = patientMeta.JobId,
                NationalId = patientMeta.NationalId,
                NationalName = string.Empty,
                EthnicId = patientMeta.EthnicId,
                EthnicName = string.Empty,
                ReligionId = patientMeta.ReligionId,
                ReligionName = string.Empty,
                ProvinceId = patientMeta.ProvinceId,
                DistrictId = patientMeta.DistrictId,
                DistrictName = string.Empty,
                Address = patientMeta.Address
            };

            if (patientMeta.NationalId.HasValue)
            {
                var nationalInfo = await GetNationalInfo(patientMeta.NationalId.Value);
                if (nationalInfo == null)
                    return new ActionResultResponse(-1,
                        _sharedResourceService.GetString("National does not exists."));

                patient.NationalName = nationalInfo.Name;
            }
            if (patientMeta.ProvinceId.HasValue)
            {
                var provinceInfo = await GetProvinceInfo(patientMeta.ProvinceId.Value);
                if (provinceInfo == null)
                    return new ActionResultResponse(-2,
                        _sharedResourceService.GetString("Province does not exists. Please try again."));

                patient.ProvinceName = provinceInfo.Name;
            }
            if (patientMeta.DistrictId.HasValue)
            {
                var districtInfo = await GetDistrictInfo(patientMeta.DistrictId.Value);
                if (districtInfo == null)
                    return new ActionResultResponse(-3,
                        _sharedResourceService.GetString("Province does not exists. Please try again."));

                patient.DistrictName = districtInfo.Name;
            }
            if (patientMeta.EthnicId.HasValue)
            {
                var ethnicInfo = await GetEthnicInfo(patientMeta.EthnicId.Value);
                if (ethnicInfo == null)
                    return new ActionResultResponse(-4,
                        _sharedResourceService.GetString("District doest not exists. Please try again."));

                patient.EthnicName = ethnicInfo.Name;
            }
            if (patientMeta.ReligionId.HasValue)
            {
                var religionInfo = await GetReligionInfo(patientMeta.ReligionId.Value);
                if (religionInfo == null)
                    return new ActionResultResponse(-5,
                        _sharedResourceService.GetString("Religion doest not exists. Please try again."));

                patient.ReligionName = religionInfo.Name;
            }
            var resultInsert = await _patientRepository.Insert(patient);
            if (resultInsert <= 0)
                return new ActionResultResponse(resultInsert,
                    _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            var resutlAddContacts = await AddPatientContacts(patientId, patientMeta.PatientContacts);
            if (resutlAddContacts.Code < 0)
                return resutlAddContacts;

            var resultAddRelativesContacts = await AddRelativesContacts(patientId, patientMeta.PatientRelativesContacts);
            if (resultAddRelativesContacts.Code < 0)
                return resultAddRelativesContacts;

            return new ActionResultResponse(resultInsert, _patientResourceService.GetString("Add new patient successful."));
        }

        public async Task<SearchResult<PatientSearchViewModel>> Search(string tenantId, string keyword, DateTime? createDate, int page, int pageSize)
        {
            var items = await _patientRepository.Search(tenantId, keyword, createDate, page, pageSize, out int totalRows);
            return new SearchResult<PatientSearchViewModel>
            {
                Items = items,
                TotalRows = totalRows,
            };
        }

        public async Task<ActionResultResponse> Update(string tenantId, string id, PatientMeta patientMeta)
        {
            var patient = await _patientRepository.GetInfo(id);
            if (patient == null)
                return new ActionResultResponse(-1, _patientResourceService.GetString("Patient does not exists."));

            if (patient.ConcurrencyStamp != patientMeta.ConcurrencyStamp)
                return new ActionResultResponse(-2,
                    _patientResourceService.GetString("The Patient already updated by other people. You can not update this Patient."));

            if (patient.TenantId != tenantId)
                return new ActionResultResponse(-3,
                _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var oldProvinceId = patient.ProvinceId;
            var oldDistrictId = patient.DistrictId;
            var oldNationalId = patient.NationalId;
            var oldReligionId = patient.ReligionId;
            var oldEthnicId = patient.EthnicId;

            if (oldProvinceId != patientMeta.ProvinceId)
            {
                if (patientMeta.ProvinceId.HasValue)
                {
                    var provinceInfo = await GetProvinceInfo(patientMeta.ProvinceId.Value);
                    if (provinceInfo == null)
                        return new ActionResultResponse(-4,
                            _sharedResourceService.GetString("Province does not exists. Please try again."));

                    patient.ProvinceId = provinceInfo.Id;
                    patient.ProvinceName = provinceInfo.Name;
                }
                else
                {
                    patient.ProvinceId = null;
                    patient.ProvinceName = null;
                }
            }

            if (oldDistrictId != patientMeta.DistrictId)
            {
                if (patientMeta.DistrictId.HasValue)
                {
                    var districtInfo = await GetDistrictInfo(patientMeta.DistrictId.Value);
                    if (districtInfo == null)
                        return new ActionResultResponse(-5,
                            _sharedResourceService.GetString("District does not exists. Please try again."));

                    patient.DistrictId = districtInfo.Id;
                    patient.DistrictName = districtInfo.Name;
                }
                else
                {
                    patient.DistrictId = null;
                    patient.DistrictName = null;
                }
            }

            if (oldNationalId != patientMeta.NationalId)
            {
                if (patientMeta.NationalId.HasValue)
                {
                    var nationalInfo = await GetNationalInfo(patientMeta.NationalId.Value);
                    if (nationalInfo == null)
                        return new ActionResultResponse(-6,
                            _sharedResourceService.GetString("Nation does not exists. Please try again."));

                    patient.NationalId = nationalInfo.Id;
                    patient.NationalName = nationalInfo.Name;
                }
                else
                {
                    patient.NationalId = null;
                    patient.NationalName = null;
                }
            }

            if (oldReligionId != patientMeta.ReligionId)
            {
                if (patientMeta.ReligionId.HasValue)
                {
                    var religionInfo = await GetReligionInfo(patientMeta.ReligionId.Value);
                    if (religionInfo == null)
                        return new ActionResultResponse(-7,
                            _sharedResourceService.GetString("Religion does not exists. Please try again."));

                    patient.ReligionId = religionInfo.Id;
                    patient.ReligionName = religionInfo.Name;
                }
                else
                {
                    patient.ReligionId = null;
                    patient.ReligionName = null;
                }
            }
            if (oldEthnicId != patientMeta.EthnicId)
            {
                if (patientMeta.EthnicId.HasValue)
                {
                    var ethnicInfo = await GetEthnicInfo(patientMeta.EthnicId.Value);
                    if (ethnicInfo == null)
                        return new ActionResultResponse(-8, _sharedResourceService.GetString("Ethnic does not exists."));

                    patient.EthnicId = ethnicInfo.Id;
                    patient.EthnicName = ethnicInfo.Name;
                }
                else
                {
                    patient.EthnicId = null;
                    patient.EthnicName = null;
                }
            }

            patient.ConcurrencyStamp = Guid.NewGuid().ToString();
            patient.FullName = patientMeta.FullName;
            patient.UnsignName = patientMeta.FullName.StripVietnameseChars().ToUpper();
            patient.Birthday = patientMeta.Birthday;
            patient.Gender = patientMeta.Gender;
            patient.PatientResourceId = patientMeta.PatientResourceId;
            patient.IdCardNumber = patientMeta.IdCardNumber;
            patient.JobId = patientMeta.JobId;
            patient.Address = patientMeta.Address;

            await _patientRepository.Update(patient);
            await UpdateContacts();
            await UpdateRelativesContacts();

            return new ActionResultResponse(1, _patientResourceService.GetString("Update Patient successful."));

            async Task UpdateContacts()
            {
                // Delete all existing contacts.
                await _patientContactRepository.DeleteByPatientId(patient.Id);

                // Add new contacts.
                await AddPatientContacts(patient.Id, patientMeta.PatientContacts);
            }

            async Task UpdateRelativesContacts()
            {
                // Delete all relatives contacts.
                await _contactPatientRepository.DeleteByPatientId(patient.Id);

                // Add new relatives contacts.
                await AddRelativesContacts(patient.Id, patientMeta.PatientRelativesContacts);
            }
        }

        private async Task RollbackInsert(string patientId)
        {
            await _patientRepository.ForceDelete(patientId);
            await _contactPatientRepository.ForceDelete(patientId);
            await _patientContactRepository.ForceDelete(patientId);
        }

        /// <summary>
        /// Lấy thông tin quốc gia
        /// </summary>
        /// <param name="nationalId">Mã quốc gia</param>
        /// <returns></returns>
        private async Task<NationalViewModel> GetNationalInfo(int nationalId)
        {
            var apiUrls = _configuration.GetApiUrl();
            if (apiUrls == null)
                return null;

            return await new HttpClientService().GetAsync<NationalViewModel>($"{apiUrls.CoreApiUrl}/nationals/{nationalId}");
        }

        /// <summary>
        /// Lấy thông tin tỉnh thành theo Id
        /// </summary>
        /// <param name="provinceId">Mã Tỉnh/TP</param>
        /// <returns></returns>
        private async Task<ProvinceViewModel> GetProvinceInfo(int provinceId)
        {
            var apiUrls = _configuration.GetApiUrl();
            if (apiUrls == null)
                return null;

            return await new HttpClientService().GetAsync<ProvinceViewModel>($"{apiUrls.CoreApiUrl}/nationals/provinces/{provinceId}");
        }

        /// <summary>
        /// Lấy thông tin quận huyện.
        /// </summary>
        /// <param name="districtId">Mã Quận/Huyện</param>
        /// <returns></returns>
        private async Task<DistrictViewModel> GetDistrictInfo(int districtId)
        {
            var apiUrls = _configuration.GetApiUrl();
            if (apiUrls == null)
                return null;

            return await new HttpClientService()
                .GetAsync<DistrictViewModel>($"{apiUrls.CoreApiUrl}/nationals/provinces/districts/{districtId}");
        }


        /// <summary>
        /// Lấy thông tin quận huyện theo Id
        /// </summary>
        /// <param name="ethnicId"></param>
        /// <returns></returns>
        private async Task<Ethnic> GetEthnicInfo(int ethnicId)
        {
            var apiUrls = _configuration.GetApiUrl();
            if (apiUrls == null)
                return null;

            return await new HttpClientService().GetAsync<Ethnic>($"{apiUrls.CoreApiUrl}/nationals/ethnics/{ethnicId}");
        }

        /// <summary>
        /// Lấy về thông tin tôn giáo
        /// </summary>
        /// <param name="religionId">Mã tôn giáo</param>
        /// <returns></returns>
        private async Task<ReligionSearchViewModel> GetReligionInfo(int religionId)
        {
            var apiUrls = _configuration.GetApiUrl();
            if (apiUrls == null)
                return null;

            return await new HttpClientService().GetAsync<ReligionSearchViewModel>($"{apiUrls.CoreApiUrl}/nationals/religions/{religionId}");
        }

        private string GenerateUnsignName(Domain.Models.Patient patientInfo, List<PatientContact> listPatientContacts)
        {
            var unsignName = patientInfo.FullName.Trim().StripVietnameseChars().ToUpper();
            return listPatientContacts.Aggregate(unsignName,
                (current, patientContact) => current + $" {patientContact.ContactValue.Trim().StripVietnameseChars().ToUpper()}");
        }

        private async Task<ActionResultResponse> UpdatePatientUnsignName(string patientId)
        {
            var patientInfo = await _patientRepository.GetInfo(patientId);
            if (patientInfo == null)
                return new ActionResultResponse(-1, _patientResourceService.GetString("Patient info does not exists"));

            var listPatientContacts = await _patientContactRepository.GetByPatientId(patientId);
            if (listPatientContacts == null || !listPatientContacts.Any())
                return new ActionResultResponse(-2, _patientResourceService.GetString("Nothing change for update."));

            var unsignName = patientInfo.FullName.Trim().StripVietnameseChars().ToUpper();
            listPatientContacts.Aggregate(unsignName,
                (current, patientContact) => current + $" {patientContact.ContactValue.Trim().StripVietnameseChars().ToUpper()}");

            patientInfo.UnsignName = unsignName;
            var result = await _patientRepository.UpdateUnsignName(patientInfo.Id, unsignName);
            return new ActionResultResponse(result, result < 0
                ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : result == 0
                    ? _sharedResourceService.GetString(ErrorMessage.NothingChanges)
                    : _patientResourceService.GetString("Update patient successful."));
        }

        private async Task<ActionResultResponse> AddPatientContacts(string patientId, List<PatientContact> listPatientContacts)
        {
            if (listPatientContacts != null && listPatientContacts.Count > 0)
            {
                var patientContacts = new List<PatientContact>();
                foreach (var patientContactMeta in listPatientContacts)
                {
                    if (patientContacts.Any(x => x.ContactType == patientContactMeta.ContactType
                     && x.ContactValue.Trim() == patientContactMeta.ContactValue.Trim()))
                        continue;

                    if (patientContactMeta.ContactType == ContactType.MobilePhone && !patientContactMeta.ContactValue.IsPhoneNumber()
                        || patientContactMeta.ContactType == ContactType.Email && !patientContactMeta.ContactValue.IsEmailAddress())
                        continue;

                    patientContacts.Add(new PatientContact
                    {
                        PatientId = patientId,
                        ContactType = patientContactMeta.ContactType,
                        ContactValue = patientContactMeta.ContactValue
                    });
                }

                if (patientContacts.Count > 0)
                {
                    var resultInsert = await _patientContactRepository.Inserts(patientContacts);
                    if (resultInsert <= 0)
                    {
                        await RollbackInsert(patientId);
                        return new ActionResultResponse(resultInsert,
                            _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
                    }

                    // Update patient unsignName.
                    await UpdatePatientUnsignName(patientId);
                }
            }
            return new ActionResultResponse(0, _patientResourceService.GetString(ErrorMessage.NothingChanges));
        }

        private async Task<ActionResultResponse> AddRelativesContacts(string patientId, List<PatientRelativesContact> listPatientRelativesContacts)
        {
            if (listPatientRelativesContacts != null && listPatientRelativesContacts.Count > 0)
            {
                var patientRelativesContacts = new List<PatientRelativesContact>();
                foreach (var patientRelativesContact in listPatientRelativesContacts)
                {
                    if (patientRelativesContacts.Any(x =>
                        x.FullName.Trim() == patientRelativesContact.FullName.Trim()
                        && x.PhoneNumber.Trim() == patientRelativesContact.PhoneNumber.Trim()))
                        continue;

                    patientRelativesContacts.Add(new PatientRelativesContact
                    {
                        PatientId = patientId,
                        FullName = patientRelativesContact.FullName,
                        PhoneNumber = patientRelativesContact.PhoneNumber
                    });
                }

                if (patientRelativesContacts.Count > 0)
                {
                    var resultInsert = await _contactPatientRepository.Inserts(patientRelativesContacts);
                    if (resultInsert <= 0)
                    {
                        await RollbackInsert(patientId);
                        return new ActionResultResponse(resultInsert,
                            _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
                    }
                }
                return new ActionResultResponse(1, _patientResourceService.GetString("Add new relatives contacts successful."));
            }
            return new ActionResultResponse(0, _sharedResourceService.GetString(ErrorMessage.NothingChanges));
        }
    }

    /// <summary>
    /// Người thân của bệnh nhân
    /// </summary>
    public class ContactPatientService : IContactPatientService
    {
        private readonly IPatientRepository _patientRepository;
        private readonly IContactPatientRepository _contactPatientRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmPatientResource> _patientResourceService;

        public ContactPatientService(IPatientRepository patientRepository, IContactPatientRepository contactPatientRepository,
                        IResourceService<SharedResource> sharedResourceService, IResourceService<GhmPatientResource> patientResourceService)
        {
            _patientRepository = patientRepository;
            _contactPatientRepository = contactPatientRepository;
            _sharedResourceService = sharedResourceService;
            _patientResourceService = patientResourceService;
        }

        public async Task<ActionResultResponse> ForceDelete(string tenantId, string id)
        {
            var info = await _contactPatientRepository.GetInfo(id);
            if (info == null)
                return new ActionResultResponse(-1, _patientResourceService.GetString("ContactPatient does not exists."));

            var result = await _contactPatientRepository.ForceDelete(id);
            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                : _patientResourceService.GetString("Delete ContactPatient successful."));
        }

        public async Task<ActionResultResponse<ContactPatientDetailViewModel>> GetDetail(string patientId)
        {
            var patientInfo = await _patientRepository.GetInfo(patientId);
            if (patientInfo == null)
                return new ActionResultResponse<ContactPatientDetailViewModel>(-1, _patientResourceService.GetString("ContactPatient subject does not exists."));

            var contactPatients = await _contactPatientRepository.GetByPatientId(patientId);
            var contactPatientDetailViewModel = new ContactPatientDetailViewModel
            {
                ConcurrencyStamp = patientInfo.ConcurrencyStamp,
                ContactPatients = contactPatients.Select(x => new ContactPatientViewModel
                {
                    FullName = x.FullName,
                    PhoneNumber = x.PhoneNumber
                }).ToList()
            };
            return new ActionResultResponse<ContactPatientDetailViewModel>
            {
                Code = 1,
                Data = contactPatientDetailViewModel
            };
        }

        public async Task<ActionResultResponse<string>> Insert(string tenantId, ContactPatientMeta contactPatientMeta)
        {
            var contactPatient = new PatientRelativesContact
            {
                FullName = contactPatientMeta.FullName,
                PatientId = contactPatientMeta.PatientId,
                PhoneNumber = contactPatientMeta.PhoneNumber
            };

            var resultInsert = await _contactPatientRepository.Insert(contactPatient);
            if (resultInsert > 0)
                return new ActionResultResponse<string>(resultInsert,
                    _patientResourceService.GetString("Add new contact successful."), string.Empty, contactPatient.Id);

            await RollbackInsert(contactPatient.Id);
            return new ActionResultResponse<string>(-4, _patientResourceService.GetString("Can not insert new ContactPatient. Please contact with administrator."));
        }

        public async Task<ActionResultResponse> Update(string tenantId, string id, ContactPatientMeta contactPatientMeta)
        {
            var contactPatientInfo = await _contactPatientRepository.GetInfo(id);
            if (contactPatientInfo == null)
                return new ActionResultResponse(-1, _patientResourceService.GetString("ContactPatient does not exists."));

            if (contactPatientInfo.ConcurrencyStamp != contactPatientMeta.ConcurrencyStamp)
                return new ActionResultResponse(-2, _patientResourceService.GetString("The ContactPatient already updated by other people. You can not update this ContactPatient."));

            contactPatientInfo.ConcurrencyStamp = Guid.NewGuid().ToString();
            contactPatientInfo.FullName = contactPatientMeta.FullName;
            contactPatientInfo.PatientId = contactPatientMeta.PatientId;
            contactPatientInfo.PhoneNumber = contactPatientMeta.PhoneNumber;
            await _contactPatientRepository.Update(contactPatientInfo);

            return new ActionResultResponse(1, _patientResourceService.GetString("Update ContactPatient successful."));
        }

        private async Task RollbackInsert(string Id)
        {
            await _contactPatientRepository.ForceDelete(Id);
        }

    }

    /// <summary>
    /// Điện thoại hoặc Email của bệnh nhân
    /// </summary>
    public class PatientContactService : IPatientContactService
    {

        private readonly IPatientRepository _patientRepository;
        private readonly IPatientContactRepository _patientContactRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmPatientResource> _patientResourceService;

        public PatientContactService(IPatientRepository patientRepository, IPatientContactRepository patientContactRepository,
                    IResourceService<SharedResource> sharedResourceService, IResourceService<GhmPatientResource> patientResourceService)
        {
            _patientRepository = patientRepository;
            _patientContactRepository = patientContactRepository;
            _sharedResourceService = sharedResourceService;
            _patientResourceService = patientResourceService;
        }

        public async Task<ActionResultResponse> ForceDelete(string tenantId, string id)
        {
            var info = await _patientContactRepository.GetInfo(id);
            if (info == null)
                return new ActionResultResponse(-1, _patientResourceService.GetString("PatientContact does not exists."));

            var result = await _patientContactRepository.ForceDelete(id);
            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString("Something went wrong. Please PatientContact with administrator.")
                : _patientResourceService.GetString("Delete PatientContact successful."));
        }

        public async Task<ActionResultResponse<PatientContactDetailViewModel>> GetDetail(string patientId)
        {
            var patientInfo = await _patientRepository.GetInfo(patientId);
            if (patientInfo == null)
                return new ActionResultResponse<PatientContactDetailViewModel>(-1, _patientResourceService.GetString("PatientContact subject does not exists."));

            var patientContacts = await _patientContactRepository.GetByPatientId(patientId);
            var patientContactDetailViewModel = new PatientContactDetailViewModel
            {
                ConcurrencyStamp = patientInfo.ConcurrencyStamp,
                ContactPatients = patientContacts.Select(x => new PatientContactViewModel
                {
                    ContactType = x.ContactType,
                    ContactValue = x.ContactValue
                }).ToList()
            };
            return new ActionResultResponse<PatientContactDetailViewModel>
            {
                Code = 1,
                Data = patientContactDetailViewModel
            };
        }

        public async Task<ActionResultResponse<string>> Insert(string tenantId, PatientContactMeta patientContactMeta)
        {
            PatientContact patientContact = new PatientContact
            {
                PatientId = patientContactMeta.PatientId,
                ContactType = patientContactMeta.ContactType,
                ContactValue = patientContactMeta.ContactValue
            };

            var resultInsert = await _patientContactRepository.Insert(patientContact);
            if (resultInsert > 0)
            {
                //var patient = await _patientRepository.GetInfo(patientContactMeta.PatientId);
                //if (patientContactMeta.ContactType == ContactType.Email)
                //{
                //    patient.Email = patientContactMeta.ContactValue;
                //}
                //else
                //{
                //    patient.PhoneNumber = patientContactMeta.ContactValue;
                //}
                //patient.UnsignName = await Unsign(patientContactMeta);
                //await _patientRepository.Update(patient);
                await UpdatePatientUnsignName(patientContactMeta.PatientId);
                return new ActionResultResponse<string>(resultInsert, _patientResourceService.GetString("Add new PatientContact successful."), string.Empty, patientContact.Id);
            }

            await RollbackInsert(patientContact.Id);
            return new ActionResultResponse<string>(-4, _patientResourceService.GetString("Can not insert new PatientContact. Please contact with administrator."));
        }

        public async Task<ActionResultResponse> Update(string tenantId, string id, PatientContactMeta patientContactMeta)
        {
            var patientContactInfo = await _patientContactRepository.GetInfo(id);
            if (patientContactInfo == null)
                return new ActionResultResponse(-2, _patientResourceService.GetString("PatientContact does not exists."));

            if (patientContactInfo.ConcurrencyStamp != patientContactMeta.ConcurrencyStamp)
                return new ActionResultResponse(-3, _patientResourceService.GetString("The PatientContact already updated by other people. You can not update this PatientContact."));

            patientContactInfo.ConcurrencyStamp = Guid.NewGuid().ToString();
            patientContactInfo.ContactType = patientContactMeta.ContactType;
            patientContactInfo.ContactValue = patientContactMeta.ContactValue;
            var result = await _patientContactRepository.Update(patientContactInfo);
            if (result <= 0)
                return new ActionResultResponse(-4, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await UpdatePatientUnsignName(patientContactInfo.PatientId);
            return new ActionResultResponse(1, _patientResourceService.GetString("Update PatientContact successful."));
        }

        private async Task RollbackInsert(string id)
        {
            await _patientContactRepository.ForceDelete(id);
        }

        private async Task<ActionResultResponse> UpdatePatientUnsignName(string patientId)
        {
            var patientInfo = await _patientRepository.GetInfo(patientId);
            if (patientInfo == null)
                return new ActionResultResponse(-1, _patientResourceService.GetString("Patient info does not exists"));

            var listPatientContacts = await _patientContactRepository.GetByPatientId(patientId);
            if (listPatientContacts == null || !listPatientContacts.Any())
                return new ActionResultResponse(-2, _patientResourceService.GetString("Nothing change for update."));

            var unsignName = patientInfo.FullName.Trim().StripVietnameseChars().ToUpper();
            listPatientContacts.Aggregate(unsignName,
                (current, patientContact) => current + $" {patientContact.ContactValue.Trim().StripVietnameseChars().ToUpper()}");

            var result = await _patientRepository.Update(patientInfo);
            return new ActionResultResponse(result, result < 0
                ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : result == 0
                    ? _sharedResourceService.GetString(ErrorMessage.NothingChanges)
                    : _patientResourceService.GetString("Update patient successful."));
        }
    }

}
