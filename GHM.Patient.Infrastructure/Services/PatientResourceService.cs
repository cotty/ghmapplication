﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Patient.Domain.IRepository;
using GHM.Patient.Domain.IServices;
using GHM.Patient.Domain.ModelMetas;
using GHM.Patient.Domain.Models;
using GHM.Patient.Domain.Resources;
using GHM.Patient.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;

namespace GHM.Patient.Infrastructure.Services
{
    public class PatientResourceService : IPatientResourceService
    {

        private readonly IPatientResourceRepository _patientResourceRepository;
        private readonly IPatientResourceTranslationRepository _patientResourceTranslationRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmPatientResource> _patientResourceService;

        public PatientResourceService(IPatientResourceRepository patientResourceRepository, IPatientResourceTranslationRepository patientResourceTranslationRepository,
            IResourceService<SharedResource> sharedResourceService, IResourceService<GhmPatientResource> patientResourceService)
        {
            _patientResourceRepository = patientResourceRepository;
            _patientResourceTranslationRepository = patientResourceTranslationRepository;
            _sharedResourceService = sharedResourceService;
            _patientResourceService = patientResourceService;
        }

        public async Task<SearchResult<PatientResourceViewModel>> Search(string tenantId, string languageId, string keyword,
                bool? isActive, int page, int pageSize)
        {
            var items = await _patientResourceRepository.Search(tenantId, languageId, keyword, isActive, page, pageSize, out var totalRows);
            return new SearchResult<PatientResourceViewModel>
            {
                Items = items,
                TotalRows = totalRows
            };
        }
        public async Task<ActionResultResponse<string>> Insert(string tenantId, PatientResourceMeta patientResourceMeta)
        {

            //if (!patientResourceMeta.PatientResourceTranslations.Any())
            //    return new ActionResultResponse(-1, _sharedResourceService.GetString("Please enter at least one language."));

            var patientResourceTranslations = new List<PatientResourceTranslation>();
            var patientResourceId = Guid.NewGuid().ToString();

            // Insert new patient source.
            var resultInsertPatientResource = await _patientResourceRepository.Insert(new PatientResource
            {
                Id = patientResourceId,
                ConcurrencyStamp = patientResourceId,
                IsActive = patientResourceMeta.IsActive,
                CreateTime = DateTime.Now,
                IsDelete = false,
                TenantId = tenantId,
                Order = patientResourceMeta.Order
            });

            if (resultInsertPatientResource <= 0)
                return new ActionResultResponse<string>(resultInsertPatientResource, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            foreach (var patientResourceTranslation in patientResourceMeta.PatientResourceTranslations)
            {
                // Check name exists.
                var isNameExists = await _patientResourceTranslationRepository.CheckExists(patientResourceId,
                    patientResourceTranslation.LanguageId, patientResourceTranslation.Name);
                if (isNameExists)
                {
                    await RollbackInsert();
                    return new ActionResultResponse<string>(-2, _patientResourceService.GetString("Patient resource name: \"{0}\" already exists.", patientResourceTranslation.Name));
                }

                var patientResourceTranslationInsert = new PatientResourceTranslation()
                {
                    PatientResourceId = patientResourceId,

                    LanguageId = patientResourceTranslation.LanguageId.Trim(),
                    Name = patientResourceTranslation.Name.Trim(),
                    Description = patientResourceTranslation.Description?.Trim(),
                    UnsignName = patientResourceTranslation.Name.StripVietnameseChars().ToUpper()
                };

                patientResourceTranslations.Add(patientResourceTranslationInsert);
            }

            // Insert patient source translations.
            var resultInsertTranslation = await _patientResourceTranslationRepository.Inserts(patientResourceTranslations);
            if (resultInsertTranslation > 0)
                return new ActionResultResponse<string>(resultInsertPatientResource, _patientResourceService.GetString("Add new patient resource successful."), string.Empty, patientResourceId);

            await RollbackInsert();
            return new ActionResultResponse<string>(-3, _patientResourceService.GetString("Can not insert new patient resource. Please contact with administrator."));

            async Task RollbackInsert()
            {
                await _patientResourceRepository.ForceDelete(patientResourceId);
                await _patientResourceTranslationRepository.ForceDeleteByPatientResourceId(patientResourceId);
            }
        }

        public async Task<ActionResultResponse> Update(string tenantId, string id, PatientResourceMeta patientResourceMeta)
        {

            //if (!patientResourceMeta.PatientResourceTranslations.Any())
            //    return new ActionResultResponse(-1, _sharedResourceService.GetString("Please enter at least one language."));

            var info = await _patientResourceRepository.GetInfo(id);
            if (info == null)
                return new ActionResultResponse(-2, _patientResourceService.GetString("Patient resource does not exists."));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-3, _sharedResourceService.GetString("You do not have permission to to this action."));

            if (info.ConcurrencyStamp != patientResourceMeta.ConcurrencyStamp)
                return new ActionResultResponse(-4, _patientResourceService.GetString("The patient resource already updated by other people. You can not update this patient source."));

            info.Order = patientResourceMeta.Order;
            info.IsActive = patientResourceMeta.IsActive;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();
            info.LastUpdate = DateTime.Now;

            foreach (var patientResourceTranslation in patientResourceMeta.PatientResourceTranslations)
            {
                var isNameExists = await _patientResourceTranslationRepository.CheckExists(info.Id,
                    patientResourceTranslation.LanguageId, patientResourceTranslation.Name);
                if (isNameExists)
                    return new ActionResultResponse(-5, _patientResourceService.GetString("Patient resource name: \"{0}\" already exists.", patientResourceTranslation.Name));


                var patientResourceTranslationInfo =
                    await _patientResourceTranslationRepository.GetInfo(info.Id, patientResourceTranslation.LanguageId);
                if (patientResourceTranslationInfo != null)
                {
                    patientResourceTranslationInfo.Name = patientResourceTranslation.Name.Trim();
                    patientResourceTranslationInfo.Description = patientResourceTranslation.Description?.Trim();
                    patientResourceTranslationInfo.UnsignName = patientResourceTranslation.Name.StripVietnameseChars().ToUpper();
                    await _patientResourceTranslationRepository.Update(patientResourceTranslationInfo);
                }
                else
                {
                    var patientResourceTranslationInsert = new PatientResourceTranslation()
                    {
                        PatientResourceId = id,
                        LanguageId = patientResourceTranslation.LanguageId.Trim(),
                        Name = patientResourceTranslation.Name.Trim(),
                        Description = patientResourceTranslation.Description?.Trim(),
                        UnsignName = patientResourceTranslation.Name.StripVietnameseChars().ToUpper()
                    };
                    await _patientResourceTranslationRepository.Insert(patientResourceTranslationInsert);
                }
            }
            await _patientResourceRepository.Update(info);
            return new ActionResultResponse(1, _patientResourceService.GetString("Update patient resource successful."));

        }
        public async Task<ActionResultResponse> Delete(string tenantId, string id)
        {
            var info = await _patientResourceRepository.GetInfo(id);
            if (info == null)
                return new ActionResultResponse(-1, _patientResourceService.GetString("Patient resource does not exists. Please try again."));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString("You do not have permission to to this action."));

            var result = await _patientResourceRepository.Delete(id);
            return new ActionResultResponse(result, _patientResourceService.GetString("Delete patient resource successful."));
        }

        public async Task<ActionResultResponse<PatientResourceDetailViewModel>> GetDetail(string tenantId, string id)
        {
            var patientResourceInfo = await _patientResourceRepository.GetInfo(id);
            if (patientResourceInfo == null)
                return new ActionResultResponse<PatientResourceDetailViewModel>(-1, _patientResourceService.GetString("Patient resource does not exists."));

            if (patientResourceInfo.TenantId != tenantId)
                return new ActionResultResponse<PatientResourceDetailViewModel>(-2, _sharedResourceService.GetString("You do not have permission to do this action."));

            var patientResourceTranslations = await _patientResourceTranslationRepository.GetsByPatientResourceId(id);
            var patientResourceDetail = new PatientResourceDetailViewModel
            {
                Id = patientResourceInfo.Id,
                IsActive = patientResourceInfo.IsActive,
                ConcurrencyStamp = patientResourceInfo.ConcurrencyStamp,
                Order = patientResourceInfo.Order,
                PatientResourceTranslations = patientResourceTranslations.Select(x => new PatientResourceTranslationViewModel
                {
                    LanguageId = x.LanguageId,
                    Name = x.Name,
                    Description = x.Description
                }).ToList()
            };
            return new ActionResultResponse<PatientResourceDetailViewModel>
            {
                Code = 1,
                Data = patientResourceDetail
            };
        }

        public async Task<List<PatientResourceForSelectViewModel>> SearchForSelect(string tenantId, string languageId, string keyword, int page, int pageSize)
        {
            return await _patientResourceRepository.SearchForSelect(tenantId, languageId, keyword, page, pageSize, out var totalRows);
        }
    }
}
