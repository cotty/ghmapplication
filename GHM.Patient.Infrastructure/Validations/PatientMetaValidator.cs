﻿using FluentValidation;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using GHM.Patient.Domain.ModelMetas;
using GHM.Patient.Domain.Resources;

namespace GHM.Patient.Infrastructure.Validations
{


    /// <summary>
    /// Validator giúp validate nội dung của object được gửi lên từ phía client. (Ở đây đang sử dụng FluentValidation.AspNetCore).
    /// </summary>
    class PatientMetaValidator : AbstractValidator<PatientMeta>
    {
        public PatientMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmPatientResource> resourceService)
        {
            //RuleFor(x => x.Name).NotEmpty().WithMessage("Vui lòng nhập họ tên nhân viên.")
            //    .MaximumLength(50).WithMessage("Tên nhân viên không được phép vượt quá 50 ký tự.");
        }
    }
}
