﻿using System;
using System.Text.RegularExpressions;
using FluentValidation;
using GHM.Patient.Domain.Constants;
using GHM.Patient.Domain.ModelMetas;
using GHM.Patient.Domain.Resources;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
namespace GHM.Patient.Infrastructure.Validations
{
   public class PatientResourceMetaValidator : AbstractValidator<PatientResourceMeta>
    {
        public PatientResourceMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmPatientResource> patientResourceService)
        {
          

            RuleFor(x => x.Order)
                .NotNull()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.", patientResourceService.GetString("Order")));

            RuleFor(x => x.IsActive)
                .NotNull()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.", patientResourceService.GetString("Active status")));

            RuleFor(x => x.PatientResourceTranslations)
                .Must((patientResource, patientResourceTranslation) => patientResource.PatientResourceTranslations?.Count > 0)
                .WithMessage(sharedResourceService.GetString("Please select at least one language."));

            RuleForEach(x => x.PatientResourceTranslations).SetValidator(new PatientResourceTranslationMetaValidator(sharedResourceService, patientResourceService));
          
    
        }

    }

    public class PatientResourceTranslationMetaValidator : AbstractValidator<PatientResourceTranslationMeta>
    {
        public PatientResourceTranslationMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmPatientResource> patientResourceService)
        {

            RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                    patientResourceService.GetString("Patient resource name")))
                .MaximumLength(256).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    patientResourceService.GetString("Patient resourc name"), 256));


            RuleFor(x => x.Description)
                .MaximumLength(500).WithMessage(sharedResourceService.GetString(
                    "{0} is not allowed over {1} characters.",
                    sharedResourceService.GetString("Description"), 500));

            RuleFor(x => x.LanguageId)
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                    sharedResourceService.GetString("Language code")))
                .MaximumLength(10).WithMessage(sharedResourceService.GetString(
                    "{0} is not allowed over {1} characters.",
                    sharedResourceService.GetString("Language code"), 10));
            
        }
    }

}
