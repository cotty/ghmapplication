﻿using FluentValidation;
using GHM.Patient.Domain.ModelMetas;

namespace GHM.Patient.Infrastructure.Validations
{
    public class PatientSubjectTranslationMetaValidator : AbstractValidator<PatientSubjectTranslationMeta>
    {
        public PatientSubjectTranslationMetaValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Vui lòng nhập tên đối tượng khách hàng.")
               .MaximumLength(250).WithMessage("Tên đối tượng không được phép vượt quá 250 ký tự.");

        }
           
    }
}
