﻿using System;
using System.Collections.Generic;
using System.Text;
using GHM.Patient.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Patient.Infrastructure.Configurations
{
    public class PatientContactConfig : IEntityTypeConfiguration<Domain.Models.PatientContact>
    {
        public void Configure(EntityTypeBuilder<PatientContact> builder)
        {
            builder.Property(x => x.ContactValue).IsRequired().HasMaxLength(100).IsUnicode();
            builder.ToTable("PatientContacts");
        }
    }
}
