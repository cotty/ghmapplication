﻿using GHM.Patient.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Patient.Infrastructure.Configurations
{
    public class PatientRelativesContactConfig : IEntityTypeConfiguration<PatientRelativesContact>
    {
        public void Configure(EntityTypeBuilder<PatientRelativesContact> builder)
        {
            builder.Property(x => x.FullName).IsRequired().HasMaxLength(100).IsUnicode(true);
            builder.Property(x => x.PhoneNumber).IsRequired().HasMaxLength(50).IsUnicode();
            builder.ToTable("PatientRelativesContacts");
        }
    }
}
