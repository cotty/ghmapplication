﻿using Microsoft.EntityFrameworkCore;
using GHM.Patient.Domain.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Patient.Infrastructure.Configurations
{
    public class PatientSubjectTranslationConfig : IEntityTypeConfiguration<PatientSubjectTranslation>
    {
        public void Configure(EntityTypeBuilder<PatientSubjectTranslation> builder)
        {
            builder.Property(x => x.PatientSubjectId).IsRequired();
            builder.Property(x => x.LanguageId).IsRequired().HasMaxLength(30).IsUnicode(false);
            builder.Property(x => x.Name).IsRequired().HasMaxLength(250).IsUnicode();
            builder.Property(x => x.UnsignName).IsRequired().HasMaxLength(250).IsUnicode(false);
            builder.Property(x => x.Description).IsRequired(false).HasMaxLength(4000).IsUnicode();
            builder.ToTable("PatientSubjectTranslations").HasKey(x => new {x.PatientSubjectId, x.LanguageId });
        }
    }
}
