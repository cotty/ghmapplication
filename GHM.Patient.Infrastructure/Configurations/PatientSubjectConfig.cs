﻿using Microsoft.EntityFrameworkCore;
using GHM.Patient.Domain.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Patient.Infrastructure.Configurations
{
    public class PatientSubjectConfig : IEntityTypeConfiguration<PatientSubject>
    {
        public void Configure(EntityTypeBuilder<PatientSubject> builder)
        {
            builder.Property(x => x.TenantId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.ConcurrencyStamp).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.TotalReduction).IsRequired(false);
            builder.ToTable("PatientSubjects").HasKey(x => x.Id);
        }
    }
}
