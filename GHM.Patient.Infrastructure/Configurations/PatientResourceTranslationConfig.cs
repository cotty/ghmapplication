﻿using GHM.Patient.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace GHM.Patient.Infrastructure.Configurations
{
  public  class PatientResourceTranslationConfig : IEntityTypeConfiguration<PatientResourceTranslation>
    {
        public void Configure(EntityTypeBuilder<PatientResourceTranslation> builder)
        {
            builder.Property(x => x.PatientResourceId).IsRequired().HasMaxLength(50);
            builder.Property(x => x.LanguageId).IsRequired().HasMaxLength(10).IsUnicode(false);
            builder.Property(x => x.Name).IsRequired().HasMaxLength(256);
            builder.Property(x => x.UnsignName).IsRequired().HasMaxLength(256).IsUnicode(false);
            builder.Property(x => x.Description).IsRequired(false).HasMaxLength(500);
            builder.ToTable("PatientResourceTranslations").HasKey(x => new { x.PatientResourceId, x.LanguageId });
        }
    }
}
