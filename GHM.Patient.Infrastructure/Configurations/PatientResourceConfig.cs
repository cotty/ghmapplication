﻿using GHM.Patient.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace GHM.Patient.Infrastructure.Configurations
{
 public   class PatientResourceConfig : IEntityTypeConfiguration<PatientResource>
    {
        public void Configure(EntityTypeBuilder<PatientResource> builder)
        {
            builder.Property(x => x.Id).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.TenantId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.ConcurrencyStamp).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.ToTable("PatientResources").HasKey(x => x.Id);
        }
    }
}
