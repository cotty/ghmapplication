﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GHM.Patient.Infrastructure.Migrations
{
    public partial class UpdatePatientsProvinceName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ProvincName",
                table: "Patients",
                newName: "ProvinceName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ProvinceName",
                table: "Patients",
                newName: "ProvincName");
        }
    }
}
