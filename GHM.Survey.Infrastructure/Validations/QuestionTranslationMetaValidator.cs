﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Resources;

namespace GHM.Survey.Infrastructure.Validations
{
    public class QuestionTranslationMetaValidator : AbstractValidator<QuestionTranslationMeta>
    {
        public QuestionTranslationMetaValidator(IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmSurveyResource> resourceService)
        {
            RuleFor(x => x.Name)
                .NotNull()
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.PleaseEnter, resourceService.GetString("question name")))
                .MaximumLength(256)
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.MustNotExceed,
                    resourceService.GetString("Question name"), 256));

            RuleFor(x => x.Content)
                .NotNull()
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.PleaseEnter, resourceService.GetString("question content")));

            RuleFor(x => x.Explain)
                .MaximumLength(4000)
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.MustNotExceed, resourceService.GetString("question explain"), 4000));
        }
    }
}
