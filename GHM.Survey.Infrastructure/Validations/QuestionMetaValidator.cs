﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using GHM.Survey.Domain.Constants;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Resources;

namespace GHM.Survey.Infrastructure.Validations
{
    public class QuestionMetaValidator : AbstractValidator<QuestionMeta>
    {
        public QuestionMetaValidator(IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmSurveyResource> resourceService)
        {
            RuleFor(x => x.Type)
                .NotNull()
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.PleaseSelect, resourceService.GetString("question type")));

            RuleFor(x => x.QuestionGroupId)
                .NotNull()
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.PleaseSelect, resourceService.GetString("question group")));

            RuleFor(x => x.IsActive)
                .NotNull()
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.NotNull, resourceService.GetString("Active status")));

            RuleFor(x => x.Point)
                .Must(x => x < int.MaxValue)
                .When(x => x.Point.HasValue)
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.MustLessThan, resourceService.GetString("Question point"), int.MaxValue));

            RuleFor(x => x.Translations)
                .Must(x => x.Count > 0)
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.SelectLanguage));

            RuleFor(x => x.TotalAnswer)
                .NotNull().When(x => x.Type == QuestionType.SelfResponded)
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.PleaseEnter, resourceService.GetString("total answer")));

            RuleForEach(x => x.Translations).SetValidator(new QuestionTranslationMetaValidator(sharedResourceService, resourceService));
        }
    }


}
