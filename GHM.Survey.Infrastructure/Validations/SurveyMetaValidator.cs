﻿using System;
using System.Globalization;
using FluentValidation;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Resources;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.Helpers.Validations;

namespace GHM.Survey.Infrastructure.Validations
{
    public class SurveyMetaValidator : AbstractValidator<SurveyMeta>
    {
        public SurveyMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmSurveyResource> surveyResourceService)
        {
            RuleFor(x => x.IsActive)
                .NotNull()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.", surveyResourceService.GetString("Active status")));

            RuleFor(x => x.SurveyTranslations)
                .Must(x => x != null && x.Count > 0)
                .WithMessage(sharedResourceService.GetString("Please select at least one language."));

            RuleForEach(x => x.SurveyTranslations).SetValidator(new SurveyTranslationMetaValidator(sharedResourceService, surveyResourceService));
        }
    }

    public class SurveyTranslationMetaValidator : AbstractValidator<SurveyTranslationMeta>
    {
        public SurveyTranslationMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmSurveyResource> surveyResourceService)
        {
            RuleFor(x => x.Name)
                .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.",
                    surveyResourceService.GetString("survey name")))
                .MaximumLength(256).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    surveyResourceService.GetString("survey name"), 256));

            //RuleFor(x => x.Description)
            //    .MaximumLength(500).WithMessage(sharedResourceService.GetString(
            //        "{0} is not allowed over {1} characters.",
            //        sharedResourceService.GetString("Description"), 500));

            RuleFor(x => x.LanguageId)
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                    sharedResourceService.GetString("Language code")))
                .MaximumLength(10).WithMessage(sharedResourceService.GetString(
                    "{0} is not allowed over {1} characters.",
                    sharedResourceService.GetString("Language code"), 10));
        }
    }
}
