﻿using FluentValidation;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Resources;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;

namespace GHM.Survey.Infrastructure.Validations
{
    public class SurveyGroupMetaValidator : AbstractValidator<SurveyGroupMeta>
    {
        public SurveyGroupMetaValidator(IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmSurveyResource> surveyResourceService)
        {
            RuleFor(x => x.Order)
                .NotNull()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.", surveyResourceService.GetString("Order")));

            RuleFor(x => x.IsActive)
                .NotNull()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.", surveyResourceService.GetString("Active status")));

            RuleFor(x => x.SurveyGroupTranslations)
                .Must(x => x.Count > 0)
                .WithMessage(sharedResourceService.GetString("Please select at least one language."));

            RuleForEach(x => x.SurveyGroupTranslations).SetValidator(new SurveyGroupTranslationMetaValidator(sharedResourceService,
                surveyResourceService));
        }
    }


    public class SurveyGroupTranslationMetaValidator : AbstractValidator<SurveyGroupTranslationMeta>
    {
        public SurveyGroupTranslationMetaValidator(IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmSurveyResource> surveyResourceService)
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                    surveyResourceService.GetString("survey group name")))
                .MaximumLength(256).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    surveyResourceService.GetString("survey group name"), 256));            

            RuleFor(x => x.LanguageId)
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                    sharedResourceService.GetString("Language code")))
                .MaximumLength(10).WithMessage(sharedResourceService.GetString(
                    "{0} is not allowed over {1} characters.",
                    sharedResourceService.GetString("Language code"), 10));
        }
    }
}
