﻿using FluentValidation;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Resources;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.Helpers.Validations;

namespace GHM.Survey.Infrastructure.Validations
{
 public   class QuestionGroupMetaValidator : AbstractValidator<QuestionGroupMeta>
    {
        public QuestionGroupMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmSurveyResource> surveyResourceService)
        {
            RuleFor(x => x.Order)
                .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.", surveyResourceService.GetString("Order")));

            RuleFor(x => x.IsActive)
                .NotNull()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.", surveyResourceService.GetString("Active status")));

            RuleFor(x => x.QuestionGroupTranslations)
                .Must(x => x.Count > 0)
                .WithMessage(sharedResourceService.GetString("Please select at least one language."));

            RuleForEach(x => x.QuestionGroupTranslations).SetValidator(new QuestionGroupTranslationMetaValidator(sharedResourceService, surveyResourceService));
        }
    }


    public class QuestionGroupTranslationMetaValidator : AbstractValidator<QuestionGroupTranslationMeta>
    {
    public QuestionGroupTranslationMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmSurveyResource> surveyResourceService)
        {
            RuleFor(x => x.Name)
                .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.",
                    surveyResourceService.GetString("question group name")))
                .MaximumLength(256).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    surveyResourceService.GetString("question group name"), 256));

            //RuleFor(x => x.Description)
            //    .MaximumLength(500).WithMessage(sharedResourceService.GetString(
            //        "{0} is not allowed over {1} characters.",
            //        sharedResourceService.GetString("Description"), 500));

            RuleFor(x => x.LanguageId)
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                    sharedResourceService.GetString("Language code")))
                .MaximumLength(10).WithMessage(sharedResourceService.GetString(
                    "{0} is not allowed over {1} characters.",
                    sharedResourceService.GetString("Language code"), 10));
        }
    }
}
