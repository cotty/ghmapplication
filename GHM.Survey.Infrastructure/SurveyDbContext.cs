﻿using GHM.Infrastructure.SqlServer;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace GHM.Survey.Infrastructure
{
    public class SurveyDbContext : DbContextBase
    {
        public SurveyDbContext(DbContextOptions<SurveyDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            #region Query
            builder.Query<SurveyUserQuestionAnswer>();
            builder.Query<SurveyUserQuestionAnswerDetailViewModel>();
            builder.Query<SurveyUsersReport>();
            builder.Query<SurveyReport>();
            builder.Query<QuestionGroupReport>();
            #endregion

            // Configurations:
            //builder.ApplyConfiguration(new SurveyGroupConfig());

            //QuestionGroup
            builder.Entity<QuestionGroup>().ToTable("QuestionGroups").HasKey(t => new { t.Id });

            //QuestionGroupTranslation
            builder.Entity<QuestionGroupTranslation>()
                .ToTable("QuestionGroupTranslations")
                .HasKey(t => new { t.TenantId, t.QuestionGroupId, t.LanguageId });

            builder.Entity<QuestionGroupTranslation>()
            .HasOne(qg => qg.QuestionGroup)
            .WithMany(qgt => qgt.QuestionGroupTranslations)
            .HasForeignKey(qgt => qgt.QuestionGroupId);

            //SurveyGroup
            builder.Entity<SurveyGroup>().ToTable("SurveyGroups").HasKey(t => new { t.Id });

            //SurveyGroupTranslation
            builder.Entity<SurveyGroupTranslation>()
                .ToTable("SurveyGroupTranslations")
                .HasKey(t => new { t.TenantId, t.SurveyGroupId, t.LanguageId });

            builder.Entity<SurveyGroupTranslation>()
                .HasOne(sg => sg.SurveyGroup)
                .WithMany(sgt => sgt.SurveyGroupTranslations)
                .HasForeignKey(sgt => sgt.SurveyGroupId);


            builder.Entity<QuestionApproverConfig>().ToTable("QuestionApproverConfigs").HasKey(t => new { t.UserId });
            builder.Entity<Question>().ToTable("Questions").HasKey(t => new { t.VersionId });
            builder.Entity<QuestionTranslation>()
               .ToTable("QuestionTranslations")
               .HasKey(t => new { t.TenantId, t.LanguageId, t.QuestionVersionId });

            builder.Entity<Answer>().ToTable("Answers").HasKey(t => new { t.Id });
            builder.Entity<AnswerTranslation>().ToTable("AnswerTranslations").HasKey(t => new { t.AnswerId, t.LanguageId });

            //Survey
            builder.Entity<Domain.Models.Survey>().ToTable("Surveys").HasKey(t => new { t.Id });

            //SurveyTranslation
            builder.Entity<SurveyTranslation>()
                .ToTable("SurveyTranslations")
                .HasKey(t => new { t.SurveyId, t.LanguageId });

            builder.Entity<SurveyTranslation>()
                .HasOne(sg => sg.Survey)
                .WithMany(sgt => sgt.SurveyTranslations)
                .HasForeignKey(sgt => sgt.SurveyId);

            builder.Entity<SurveyQuestionUser>().ToTable("SurveyQuestionUsers").HasKey(t => new
            {
                t.SurveyId,
                t.QuestionVersionId,
                t.SurveyUserId,
                t.SurveyUserAnswerTimesId
            });

            builder.Entity<SurveyUserAnswerTime>().ToTable("SurveyUserAnswerTimes").HasKey(t => new { t.Id });

            builder.Entity<SurveyUserAnswerTime>().ToTable("SurveyUserAnswerTimes").HasKey(t => new { t.Id });

            builder.Entity<SurveyUserAnswer>().ToTable("SurveyUserAnswers").HasKey(t => new { t.Id });

            builder.Entity<SurveyQuestionAnswerLogic>().ToTable("SurveyQuestionAnswerLogics").HasKey(x => new
            {
                x.SurveyId,
                x.QuestionVersionId,
                x.AnswerId,
                x.ToQuestionVersionId
            });

            //SurveyQuestion
            builder.Entity<QuestionForUser>()
                .ToTable("QuestionForUsers")
                .HasKey(t => new { t.AnswerId });

            //SurveyUser
            builder.Entity<SurveyUser>()
                .ToTable("SurveyUsers")
                .HasKey(t => new { t.Id });

            //SurveyQuestion
            builder.Entity<SurveyQuestion>()
                .ToTable("SurveyQuestions")
                .HasKey(t => new { t.SurveyId, t.QuestionVersionId });

            //SurveyQuestionGroup
            builder.Entity<SurveyQuestionGroup>()
                .ToTable("SurveyQuestionGroups")
                .HasKey(t => new { t.SurveyId, t.QuestionGroupId });

            //SurveyQuestionGroup
            builder.Entity<QuestionForUser>()
                .ToTable("QuestionForUsers")
                .HasKey(t => new { t.AnswerId });
        }
    }
}
