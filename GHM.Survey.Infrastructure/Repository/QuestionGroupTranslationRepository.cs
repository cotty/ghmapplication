﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
using GHM.Infrastructure.SqlServer;

namespace GHM.Survey.Infrastructure.Repository
{
    public class QuestionGroupTranslationRepository : RepositoryBase, IQuestionGroupTranslationRepository
    {
        private readonly IRepository<QuestionGroupTranslation> _questionGroupTranslation;
        public QuestionGroupTranslationRepository(IDbContext context) : base(context)
        {
            _questionGroupTranslation = Context.GetRepository<QuestionGroupTranslation>();
        }

        public async Task<string> GetQuestionGroupName(int questionGroupId, string languageId)
        {
            return await _questionGroupTranslation.GetAsAsync(x => x.Name, x => x.QuestionGroupId == questionGroupId
                                                                                && x.LanguageId == languageId);
        }

        public async Task<int> Insert(QuestionGroupTranslation questionGroupTranslation)
        {
            _questionGroupTranslation.Create(questionGroupTranslation);
            return await Context.SaveChangesAsync();
        }


        public async Task<int> Inserts(List<QuestionGroupTranslation> questionGroupTranslations)
        {
            _questionGroupTranslation.Creates(questionGroupTranslations);
            return await Context.SaveChangesAsync();
        }


        public async Task<int> Update(QuestionGroupTranslation questionGroupTranslation)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDeleteByQuestionGroupId(int questionGroupId)
        {
            var info = await _questionGroupTranslation.GetsAsync(false, x => x.QuestionGroupId == questionGroupId);
            if (info == null || !info.Any())
                return -1;

            _questionGroupTranslation.Deletes(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<bool> CheckExistsByName(int questionGroupId, string tenantId, string languageId, string name)
        {
            name = name.Trim();
            return await _questionGroupTranslation.ExistAsync(x =>
                x.QuestionGroupId != questionGroupId && x.TenantId == tenantId && x.LanguageId == languageId && x.Name == name);
        }

        public async Task<QuestionGroupTranslation> GetInfo(int questionGroupId, string languageId, bool isReadonly = false)
        {
            return await _questionGroupTranslation.GetAsync(isReadonly, c => c.QuestionGroupId == questionGroupId
            && c.LanguageId == languageId);
        }

        public async Task<List<QuestionGroupTranslationViewModel>> GetsByQuestionGroupId(int questionGroupId)
        {
            return await _questionGroupTranslation.GetsAsAsync(x => new QuestionGroupTranslationViewModel
            {
                Name = x.Name,
                Description = x.Description,
                LanguageId = x.LanguageId,
                ParentName = x.ParentName
            }, x => x.QuestionGroupId == questionGroupId);
        }
    }
}
