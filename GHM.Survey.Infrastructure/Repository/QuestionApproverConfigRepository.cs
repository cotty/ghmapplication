﻿using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GHM.Survey.Infrastructure.Repository
{
    public class QuestionApproverConfigRepository : RepositoryBase, IQuestionApproverConfigRepository
    {
        private readonly IRepository<QuestionApproverConfig> _questionApproverConfigRepository;

        public QuestionApproverConfigRepository(IDbContext context) : base(context)
        {
            _questionApproverConfigRepository = Context.GetRepository<QuestionApproverConfig>();
        }

        public async Task<bool> CheckExistsUserId(string tenantId, string userId)
        {
            return await _questionApproverConfigRepository.ExistAsync(x => x.UserId == userId && x.TenantId == tenantId);
        }

        public async Task<IReadOnlyCollection<QuestionApproverConfig>> GetAllApprover(string tenantId)
        {
            return await _questionApproverConfigRepository.GetsAsync(true, x => x.TenantId == tenantId);
        }

        public async Task<int> ForceDelete(string tenantId, string userId)
        {
            var questionapproverconfigInfo = await GetInfo(tenantId, userId);
            if (questionapproverconfigInfo == null)
                return -1;

            _questionApproverConfigRepository.Delete(questionapproverconfigInfo);
            return await Context.SaveChangesAsync();
        }

        public async Task<QuestionApproverConfig> GetInfo(string tenantId, string userId)
        {
            return await _questionApproverConfigRepository.GetAsync(false, x => x.UserId == userId && x.TenantId == tenantId);
        }

        public async Task<int> Insert(QuestionApproverConfig questionApproverConfig)
        {
            _questionApproverConfigRepository.Create(questionApproverConfig);
            return await Context.SaveChangesAsync();
        }

        // Refactor GiangNV: Tạo thêm UnsignName để lọc người dùng.
        public Task<List<QuestionApproverConfigSearchViewModel>> Search(string tenantId, string keyword, int page, int pageSize, out int totalRows)
        {
            Expression<Func<QuestionApproverConfig, bool>> spec = x => x.TenantId == tenantId;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            var query = Context.Set<QuestionApproverConfig>()
                .Where(spec)
                .Select(x => new QuestionApproverConfigSearchViewModel
                {
                    UserId = x.UserId,
                    PositionId = x.PositionId,
                    OfficeId = x.OfficeId,
                    FullName = x.FullName,
                    Avatar = x.Avatar,
                    OfficeName = x.OfficeName,
                    PositionName = x.PositionName,
                }).AsNoTracking();

            totalRows = query.Count();
            return query
                .OrderByDescending(x => x.FullName)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<int> Update(QuestionApproverConfig questionApproverConfig)
        {
            return await Context.SaveChangesAsync();
        }
    }
}
