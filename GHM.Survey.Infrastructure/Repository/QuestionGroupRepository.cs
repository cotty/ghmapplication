﻿using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GHM.Survey.Infrastructure.Repository
{
    public class QuestionGroupRepository : RepositoryBase, IQuestionGroupRepository
    {
        private readonly IRepository<QuestionGroup> _questionGroupRepository;
        public QuestionGroupRepository(IDbContext context) : base(context)
        {
            _questionGroupRepository = Context.GetRepository<QuestionGroup>();
        }

        public async Task<bool> CheckExists(int id)
        {
            return await _questionGroupRepository.ExistAsync(x => x.Id == id && !x.IsDelete);
        }

        public async Task<int> Insert(QuestionGroup questionGroup)
        {
            _questionGroupRepository.Create(questionGroup);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(QuestionGroup questionGroup)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateQuestionGroupIdPath(int id, string idPath)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateChildCount(int id, int childCount)
        {
            QuestionGroup info = await GetInfo(id);
            if (info == null)
            {
                return -1;
            }

            info.ChildCount = childCount;
            return await Context.SaveChangesAsync();
        }
        public async Task<int> UpdateChildrenIdPath(string oldIdPath, string newIdPath)
        {
            List<QuestionGroup> childrenQuestionGroup = await _questionGroupRepository.GetsAsync(false, x => !x.IsDelete && x.IdPath.StartsWith(oldIdPath + "."));
            if (childrenQuestionGroup == null || !childrenQuestionGroup.Any())
            {
                return -1;
            }

            foreach (QuestionGroup questionGroup in childrenQuestionGroup)
            {
                questionGroup.IdPath = $"{newIdPath}.{questionGroup.Id}";
            }
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateTotalQuestion(int id, int totalQuestion)
        {
            QuestionGroup info = await GetInfo(id);
            if (info == null)
            {
                return -1;
            }

            info.TotalQuestion = totalQuestion;
            return await Context.SaveChangesAsync();
        }


        public async Task<int> Delete(int questionGroupId)
        {
            QuestionGroup info = await GetInfo(questionGroupId);
            if (info == null)
            {
                return -1;
            }

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(int questionGroupId)
        {
            QuestionGroup info = await GetInfo(questionGroupId);
            if (info == null)
            {
                return -1;
            }

            _questionGroupRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<QuestionGroup> GetInfo(int questionGroupId, bool isReadOnly = false)
        {
            return await _questionGroupRepository.GetAsync(isReadOnly, x => x.Id == questionGroupId && !x.IsDelete);
        }

        public async Task<QuestionGroup> GetInfo(string questionGroupIdPath, bool isReadOnly = false)
        {
            return await _questionGroupRepository.GetAsync(isReadOnly, x => x.IdPath == questionGroupIdPath && !x.IsDelete);
        }

        public async Task<int> GetChildCount(int id)
        {
            return await _questionGroupRepository.CountAsync(x => x.ParentId == id && !x.IsDelete);
        }

        public async Task<List<QuestionGroupSearchViewModel>> GetAllActivatedQuestionGroup(string tenantId, string languageId)
        {
            IQueryable<QuestionGroupSearchViewModel> query = Context.Set<QuestionGroup>().Where(x => x.IsActive && !x.IsDelete && x.TenantId == tenantId)
                .Join(Context.Set<QuestionGroupTranslation>().Where(x => x.LanguageId == languageId), o => o.Id,
                    ot => ot.QuestionGroupId, (o, ot) => new QuestionGroupSearchViewModel
                    {
                        Id = o.Id,
                        Name = ot.Name,
                        Description = ot.Description,
                        IsActive = o.IsActive,
                        ParentId = o.ParentId,
                        IdPath = o.IdPath,
                        ChildCount = o.ChildCount,
                        TotalQuestion = o.TotalQuestion
                    });
            return await query
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<List<QuestionGroupSearchViewModel>> GetActivedQuestionGroup(string tenantId, string languageId)
        {
            IQueryable<QuestionGroupSearchViewModel> query = Context.Set<QuestionGroup>().Where(x => x.IsActive && !x.IsDelete && x.TenantId == tenantId)
                .Join(Context.Set<QuestionGroupTranslation>().Where(x => x.LanguageId == languageId), o => o.Id,
                    ot => ot.QuestionGroupId, (o, ot) => new QuestionGroupSearchViewModel
                    {
                        Id = o.Id,
                        Name = ot.Name,
                        Description = ot.Description,
                        IsActive = o.IsActive,
                        ParentId = o.ParentId,
                        IdPath = o.IdPath,
                        ChildCount = o.ChildCount,
                        TotalQuestion = o.TotalQuestion
                    });
            return await query
                .AsNoTracking()
                .ToListAsync();
        }

        public Task<List<QuestionGroupForSelectViewModel>> GetAllQuestionGroupForSelect(string tenantId, string languageId, string keyword,
            int page, int pageSize, out int totalRows)
        {
            Expression<Func<QuestionGroup, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId && x.IsActive;
            Expression<Func<QuestionGroupTranslation, bool>> specTranslation = x => x.LanguageId == languageId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            IQueryable<QuestionGroupForSelectViewModel> query = Context.Set<QuestionGroup>().Where(spec)
                .Join(Context.Set<QuestionGroupTranslation>().Where(specTranslation), o => o.Id,
                    ot => ot.QuestionGroupId, (o, ot) => new QuestionGroupForSelectViewModel
                    {
                        Id = o.Id,
                        Name = ot.Name,
                        TotalQuestions = o.TotalQuestion
                    });

            totalRows = query.Count();

            return query
                .AsNoTracking()
                .OrderBy(c => c.Name)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        public Task<List<QuestionGroupSearchViewModel>> SearchQuestionGroup(string tenantId, string languageId, string keyword,
            bool? isActive, int page, int pageSize, out int totalRows)
        {
            Expression<Func<QuestionGroup, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId;
            Expression<Func<QuestionGroupTranslation, bool>> specTranslation = x => x.LanguageId == languageId;
            if (isActive.HasValue)
            {
                spec = spec.And(x => x.IsActive == isActive.Value);
            }

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            IQueryable<QuestionGroupSearchViewModel> query = Context.Set<QuestionGroup>().Where(spec)
                .Join(Context.Set<QuestionGroupTranslation>().Where(specTranslation), o => o.Id, ot => ot.QuestionGroupId,
                (o, ot) => new QuestionGroupSearchViewModel
                {
                    Id = o.Id,
                    IdPath = o.IdPath,
                    Name = ot.Name,
                    Description = ot.Description,
                    IsActive = o.IsActive,
                    ChildCount = o.ChildCount,
                    TotalQuestion = o.TotalQuestion
                });

            totalRows = query.Count();
            return query
                .AsNoTracking()
                .OrderBy(x => x.IdPath)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        public Task<List<QuestionGroupSearchViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive, int page,
            int pageSize, out int totalRows)
        {
            Expression<Func<QuestionGroup, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId;
            Expression<Func<QuestionGroupTranslation, bool>> specTranslation = x => x.LanguageId == languageId;
            if (isActive.HasValue)
            {
                spec = spec.And(x => x.IsActive == isActive.Value);
            }

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            IQueryable<QuestionGroupSearchViewModel> query = Context.Set<QuestionGroup>().Where(spec)
                .Join(Context.Set<QuestionGroupTranslation>().Where(specTranslation), o => o.Id, ot => ot.QuestionGroupId,
                (o, ot) => new QuestionGroupSearchViewModel
                {
                    Id = o.Id,
                    IdPath = o.IdPath,
                    Name = ot.Name,
                    Description = ot.Description,
                    IsActive = o.IsActive,
                    ParentId = o.ParentId,
                    ChildCount = o.ChildCount,
                    TotalQuestion = o.TotalQuestion
                });

            totalRows = query.Count();
            return query
                .AsNoTracking()
                .OrderBy(x => x.IdPath).Skip((page - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<List<QuestionGroupReport>> SearchReport(string surveyId, string surveyUserAnswerTimesId, string languageId)
        {
            SqlParameter inputSurveyId = new SqlParameter("@SurveyId", surveyId);
            SqlParameter inputSurveyUserAnswerTimesId =
                    new SqlParameter("@SurveyUserAnswerTimesId", surveyUserAnswerTimesId);
            SqlParameter inputlanguageId = new SqlParameter("@LanguageId", languageId);

            Task<List<QuestionGroupReport>> result = Context.Query<QuestionGroupReport>().FromSql(
                        "EXEC sp_SurveyQuestionGroupReport @SurveyId, @SurveyUserAnswerTimesId, @LanguageId",
                        inputSurveyId, inputSurveyUserAnswerTimesId, inputlanguageId)
                    .ToListAsync();
            return await result;
        }
    }
}
