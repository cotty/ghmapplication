﻿using GHM.Infrastructure.SqlServer;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace GHM.Survey.Infrastructure.Repository
{
    public class QuestionTranslationRepository : RepositoryBase, IQuestionTranslationRepository
    {
        private readonly IRepository<QuestionTranslation> _questiontranslationRepository;

        public QuestionTranslationRepository(IDbContext context) : base(context)
        {
            _questiontranslationRepository = Context.GetRepository<QuestionTranslation>();
        }

        public async Task<List<QuestionTranslation>> GetsByQuestionVersionId(string versionId)
        {
            return await _questiontranslationRepository.GetsAsync(false, x => x.QuestionVersionId == versionId);
        }

        public async Task<int> UpdateQuestionGroupName(string tenantId, string languageId, int questionGroupId,
            string questionGroupName)
        {

            var input_tenantId = new SqlParameter
            {
                ParameterName = "@tenantId",
                SqlDbType = SqlDbType.VarChar,
                Direction = ParameterDirection.Input,
                Size = 50,
                Value = tenantId
            };
            var input_languageId = new SqlParameter
            {
                ParameterName = "@languageId",
                SqlDbType = SqlDbType.VarChar,
                Direction = ParameterDirection.Input,
                Size = 50,
                Value = languageId
            };
            var input_questionGroupId = new SqlParameter
            {
                ParameterName = "@questionGroupId",
                SqlDbType = SqlDbType.Int,
                Direction = ParameterDirection.Input,
                Size = 50,
                Value = questionGroupId
            };
            var input_questionGroupName = new SqlParameter
            {
                ParameterName = "@questionGroupName",
                SqlDbType = SqlDbType.NVarChar,
                Direction = ParameterDirection.Input,
                Size = 500,
                Value = questionGroupName
            };

            var result = await Context.Database.ExecuteSqlCommandAsync("EXEC [dbo].[spUpdateQuestionGroupName] @tenantId, @languageId, @questionGroupId, " +
                                                                       "@questionGroupName", input_tenantId, input_languageId, input_questionGroupId, input_questionGroupName);
            return result;

        }

        public async Task<int> ForceDeleteByQuestionVersionId(string questionVersionId)
        {
            var questionTranslations =
                await _questiontranslationRepository.GetsAsync(false, x => x.QuestionVersionId == questionVersionId);
            if (questionTranslations == null || !questionTranslations.Any())
                return -1;

            _questiontranslationRepository.Deletes(questionTranslations);
            return await Context.SaveChangesAsync();
        }

        public async Task<QuestionTranslation> GetInfo(string tenantId, string languageId, string questionVersionId, bool isReadOnly = false)
        {
            return await _questiontranslationRepository.GetAsync(isReadOnly, x => x.TenantId == tenantId && x.LanguageId == languageId && x.QuestionVersionId == questionVersionId);
        }

        public async Task<int> Insert(QuestionTranslation questionTranslation)
        {
            _questiontranslationRepository.Create(questionTranslation);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<QuestionTranslation> questionTranslations)
        {
            _questiontranslationRepository.Creates(questionTranslations);
            return await Context.SaveChangesAsync();
        }

        public Task<int> Update(QuestionTranslation questiontranslation)
        {
            throw new NotImplementedException();
        }
    }
}
