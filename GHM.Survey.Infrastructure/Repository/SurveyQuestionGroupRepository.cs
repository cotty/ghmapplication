﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml.FormulaParsing.ExpressionGraph;

namespace GHM.Survey.Infrastructure.Repository
{
    public class SurveyQuestionGroupRepository : RepositoryBase, ISurveyQuestionGroupRepository
    {
        private readonly IRepository<SurveyQuestionGroup> _surveyQuestionGroup;
        public SurveyQuestionGroupRepository(IDbContext context) : base(context)
        {
            _surveyQuestionGroup = Context.GetRepository<SurveyQuestionGroup>();
        }

        public async Task<int> Insert(SurveyQuestionGroup surveyQuestionGroup)
        {
            _surveyQuestionGroup.Create(surveyQuestionGroup);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(SurveyQuestionGroup surveyQuestionGroup)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string surveyId, int questionGroupId)
        {
            var info = await _surveyQuestionGroup.GetsAsync(false, x => x.SurveyId == surveyId && x.QuestionGroupId == questionGroupId);
            if (info == null || !info.Any())
                return -1;

            _surveyQuestionGroup.Deletes(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeleteBysurveyId(string surveyId)
        {
            var info = await _surveyQuestionGroup.GetsAsync(false, x => x.SurveyId == surveyId);
            if (info == null || !info.Any())
                return -1;

            _surveyQuestionGroup.Deletes(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<SurveyQuestionGroup> surveyQuestionGroups)
        {
            _surveyQuestionGroup.Creates(surveyQuestionGroups);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Deletes(List<SurveyQuestionGroup> surveyQuestionGroups)
        {
            if (surveyQuestionGroups == null || !surveyQuestionGroups.Any())
                return -1;

            _surveyQuestionGroup.Deletes(surveyQuestionGroups);
            return await Context.SaveChangesAsync();
        }

        public async Task<List<SurveyQuestionGroupViewModel>> GetInfo(string surveyId, int questionGroupId, bool isReadOnly = false)
        {
            return await _surveyQuestionGroup.GetsAsAsync(x => new SurveyQuestionGroupViewModel
            {
                SurveyId = x.SurveyId,
                Id = x.QuestionGroupId,
                TotalQuestions = x.TotalQuestion
            }, x => x.SurveyId == surveyId && x.QuestionGroupId == questionGroupId);
        }

        public async Task<List<SurveyQuestionGroupViewModel>> GetsBySurveyId(string surveyId, string languageId)
        {
            Expression<Func<SurveyQuestionGroup, bool>> spec = x => x.SurveyId == surveyId;
            Expression<Func<QuestionGroupTranslation, bool>> specTranslation = x => x.LanguageId == languageId;
            var query = Context.Set<SurveyQuestionGroup>().Where(spec)
                .Join(Context.Set<QuestionGroupTranslation>().Where(specTranslation), sqg => sqg.QuestionGroupId,
                    qgt => qgt.QuestionGroupId,
                    (sqg, qgt) => new SurveyQuestionGroupViewModel
                    {
                        SurveyId = sqg.SurveyId,
                        Id = sqg.QuestionGroupId,
                        TotalQuestions = sqg.TotalQuestion,
                        Name = qgt.Name
                    }).AsNoTracking();
            return await query.ToListAsync();
        }

        public async Task<bool> CheckExistsBysurveyIdQuestionGroupId(string surveyId, int questionGroupId)
        {
            return await _surveyQuestionGroup.ExistAsync(x => x.SurveyId == surveyId && x.QuestionGroupId == questionGroupId);
        }
    }
}
