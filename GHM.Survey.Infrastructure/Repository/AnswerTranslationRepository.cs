﻿using GHM.Infrastructure.SqlServer;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Survey.Infrastructure.Repository
{
    public class AnswerTranslationRepository : RepositoryBase, IAnswerTranslationRepository
    {
        private readonly IRepository<AnswerTranslation> _answertranslationRepository;

        public AnswerTranslationRepository(IDbContext context) : base(context)
        {
            _answertranslationRepository = Context.GetRepository<AnswerTranslation>();
        }
        public async Task<int> Insert(AnswerTranslation answertranslation)
        {
            _answertranslationRepository.Create(answertranslation);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<AnswerTranslation> answertranslations)
        {
            _answertranslationRepository.Creates(answertranslations);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(AnswerTranslation answertranslation)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDeleteByAnswerId(string answerId)
        {
            var answerTranslations = await _answertranslationRepository.GetsAsync(false, x => x.AnswerId == answerId);
            if (answerTranslations == null || answerTranslations.Any())
                return -1;

            _answertranslationRepository.Deletes(answerTranslations);
            return await Context.SaveChangesAsync();
        }
    }
}
