﻿using GHM.Infrastructure.SqlServer;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Survey.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;
using GHM.Infrastructure.Helpers;

namespace GHM.Survey.Infrastructure.Repository
{
    public class AnswerRepository : RepositoryBase, IAnswerRepository
    {
        private readonly IRepository<Answer> _answerRepository;
        private readonly IRepository<AnswerTranslation> _answerTranslationRepository;

        public AnswerRepository(IDbContext context) : base(context)
        {
            _answerRepository = Context.GetRepository<Answer>();
            _answerTranslationRepository = Context.GetRepository<AnswerTranslation>();
        }

        public async Task<int> Delete(string answerId)
        {
            var answerInfo = await GetInfo(answerId);
            if (answerInfo == null)
                return -1;
            answerInfo.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDeleteByQuestionVersionId(string questionVersionId)
        {
            var answers = await _answerRepository.GetsAsync(false, x => x.QuestionVersionId == questionVersionId);
            if (answers == null || !answers.Any())
                return -1;

            _answerRepository.Deletes(answers);
            return await Context.SaveChangesAsync();
        }

        public async Task<Answer> GetInfo(string answerId)
        {
            return await _answerRepository.GetAsync(false, x => x.Id == answerId);
        }

        public async Task<AnswerDetailByLanguageViewModel> GetInfo(string questionId, string answerId, string languageId)
        {
            return await Context.Set<Answer>()
                .Where(x => x.QuestionVersionId == questionId && x.Id == answerId && !x.IsDelete)
                .Join(Context.Set<AnswerTranslation>().Where(x => x.LanguageId == languageId), a => a.Id,
                    at => at.AnswerId,
                    (a, at) => new AnswerDetailByLanguageViewModel
                    {
                        Id = a.Id,
                        Name = at.Name
                    }).FirstOrDefaultAsync();
        }

        public async Task<List<AnswerTranslation>> GetsAnswerTranslationByAnswerId(string answerId)
        {
            return await _answerTranslationRepository.GetsAsync(false, x => x.AnswerId == answerId);
        }

        public async Task<List<Answer>> GetsByQuestionVersionId(string questionVersionId)
        {
            return await _answerRepository.GetsAsync(false, x => x.QuestionVersionId == questionVersionId);
        }

        public async Task<int> Insert(Answer answer)
        {
            _answerRepository.Create(answer);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Insert(List<Answer> answers)
        {
            _answerRepository.Creates(answers);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> TotalAnswer(string questionVersionId, bool? isCorrect)
        {
            Expression<Func<Answer, bool>> spec = x => x.QuestionVersionId == questionVersionId;

            if (isCorrect.HasValue)
            {
                spec = spec.And(x => x.IsCorrect == isCorrect);
            }

            return await _answerRepository.CountAsync(spec);
        }

        public async Task<List<AnswerSuggestionViewModel>> GetsByQuestionVersionIds(List<string> questionVersionIds, string languageId)
        {
            Expression<Func<Answer, bool>> spec = x => !x.IsDelete && questionVersionIds.Contains(x.QuestionVersionId);

            var query = Context.Set<Answer>().Where(spec)
                .Join(Context.Set<AnswerTranslation>().Where(x => x.LanguageId == languageId), a => a.Id,
                    at => at.AnswerId,
                    (a, at) => new AnswerSuggestionViewModel
                    {
                        Id = a.Id,
                        QuestionVersionId = a.QuestionVersionId,
                        Name = at.Name,
                        Order = a.Order
                    });
            return await query.ToListAsync();
        }

        public async Task<int> Update(Answer answer)
        {
            return await Context.SaveChangesAsync();
        }
    }

}
