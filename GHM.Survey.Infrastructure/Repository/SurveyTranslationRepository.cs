﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;

namespace GHM.Survey.Infrastructure.Repository
{
    public class SurveyTranslationRepository : RepositoryBase, ISurveyTranslationRepository
    {
        private readonly IRepository<SurveyTranslation> _surveyTranslation;
        public SurveyTranslationRepository(IDbContext context) : base(context)
        {
            _surveyTranslation = Context.GetRepository<SurveyTranslation>();
        }
        public async Task<int> Insert(SurveyTranslation surveyTranslation)
        {
            _surveyTranslation.Create(surveyTranslation);
            return await Context.SaveChangesAsync();
        }
        public async Task<int> Inserts(List<SurveyTranslation> surveyTranslations)
        {
            _surveyTranslation.Creates(surveyTranslations);
            return await Context.SaveChangesAsync();
        }
        public async Task<int> Update(SurveyTranslation surveyTranslation)
        {
            return await Context.SaveChangesAsync();
        }
        public async Task<int> Delete(string surveyId)
        {
            var info = await _surveyTranslation.GetsAsync(false, x => x.SurveyId == surveyId);
            if (info == null || !info.Any())
                return -1;

            _surveyTranslation.Deletes(info);
            return await Context.SaveChangesAsync();
        }
        public async Task<bool> CheckExists(string surveyId, string languageId, string name)
        {
            name = name.Trim();
            return await _surveyTranslation.ExistAsync(x =>
                x.SurveyId != surveyId && x.LanguageId == languageId && x.Name == name);
        }

        public async Task<int> UpdateSurveyGroupName(string tenantId, string languageId, int surveyGroupId, string surveyGroupName)
        {
            Expression<Func<Domain.Models.Survey, bool>> spec = x => x.SurveyGroupId == surveyGroupId && x.TenantId == tenantId;
            Expression<Func<SurveyTranslation, bool>> specTranslation = t => t.LanguageId == languageId;

            var query = Context.Set<Domain.Models.Survey>().Where(spec)
                .Join(Context.Set<SurveyTranslation>().Where(specTranslation), survey => survey.Id,
                    surveyTranslation => surveyTranslation.SurveyId, (survey, surveyTranslation) =>
                        new SurveyViewModel()
                        {
                            SurveyGroupId = survey.SurveyGroupId,
                            SurveyGroupName = surveyTranslation.Name,
                            Id = survey.Id
                        });

            foreach (var survey in query.AsNoTracking().ToList())
            {
                var info = await _surveyTranslation.GetsAsync(false, x => x.SurveyId == survey.Id);
                if (info == null || !info.Any())
                    return -1;

                foreach (var surveyTranslation in info)
                {
                    var infoTranslation =
                        await _surveyTranslation.GetAsync(false, x => x.SurveyId == surveyTranslation.SurveyId);
                    infoTranslation.SurveyGroupName = surveyGroupName;

                }
            }
            return await Context.SaveChangesAsync();
        }

        public async Task<SurveyTranslation> GetInfo(string surveyId, string languageId, bool isReadOnly = false)
        {
            return await _surveyTranslation.GetAsync(isReadOnly, x => x.SurveyId == surveyId && x.LanguageId == languageId);
        }

        public async Task<string> GetSurveyName(string surveyId, string languageId)
        {
            return await _surveyTranslation.GetAsAsync(x => x.Name,
                x => x.LanguageId == languageId && x.SurveyId == surveyId);
        }

        public async Task<List<SurveyTranslationViewModel>> GetsBySurveyId(string surveyId)
        {
            return await _surveyTranslation.GetsAsAsync(x => new SurveyTranslationViewModel
            {
                Name = x.Name,
                Description = x.Description,
                LanguageId = x.LanguageId,
                UnsignName = x.UnsignName,
                SurveyGroupName = x.SurveyGroupName
            }, x => x.SurveyId == surveyId);
        }
    }
}
