﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace GHM.Survey.Infrastructure.Repository
{
    public class SurveyUserRepository : RepositoryBase, ISurveyUserRepository
    {
        private readonly IRepository<SurveyUser> _surveyUserRepository;
        public SurveyUserRepository(IDbContext context) : base(context)
        {
            _surveyUserRepository = Context.GetRepository<SurveyUser>();
        }

        public async Task<int> Insert(SurveyUser surveyUser)
        {
            _surveyUserRepository.Create(surveyUser);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(SurveyUser surveyUser)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string surveyId, string userId)
        {
            var info = await _surveyUserRepository.GetsAsync(false, x => x.SurveyId == surveyId && x.UserId == userId);
            if (info == null || !info.Any())
                return -1;

            _surveyUserRepository.Deletes(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Deletes(string surveyId, List<string> userIds)
        {
            var listSurveyUsers =
                await _surveyUserRepository.GetsAsync(false, x => x.SurveyId == surveyId && userIds.Contains(x.UserId));
            if (listSurveyUsers == null || !listSurveyUsers.Any())
                return -1;

            _surveyUserRepository.Deletes(listSurveyUsers);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeletebysurveyId(string surveyId)
        {
            var info = await _surveyUserRepository.GetsAsync(false, x => x.SurveyId == surveyId);
            if (info == null || !info.Any())
                return -1;

            _surveyUserRepository.Deletes(info);
            return await Context.SaveChangesAsync();
        }


        public async Task<int> Inserts(List<SurveyUser> surveyUsers)
        {
            _surveyUserRepository.Creates(surveyUsers);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Deletes(List<SurveyUser> surveyUsers)
        {
            if (surveyUsers == null || !surveyUsers.Any())
                return -1;

            _surveyUserRepository.Deletes(surveyUsers);
            return await Context.SaveChangesAsync();
        }

        public async Task<SurveyUser> GetInfo(string id, bool isReadOnly = false)
        {
            return await _surveyUserRepository.GetAsync(isReadOnly, x => x.Id == id);
        }

        public async Task<SurveyUser> GetInfoById(string id, bool isReadOnly = false)
        {
            return await _surveyUserRepository.GetAsync(isReadOnly, x => x.Id == id);
        }

        public async Task<SurveyUser> GetInfo(string surveyId, string userId, bool isReadOnly = false)
        {
            return await _surveyUserRepository.GetAsync(isReadOnly, x => x.UserId == userId && x.SurveyId == surveyId);
        }

        public async Task<List<SurveyUser>> GetListSurveyUser(string surveyId, bool isReadOnly = false)
        {
            return await _surveyUserRepository.GetsAsync(isReadOnly, x => x.SurveyId == surveyId);
        }

        public async Task<bool> CheckExistsBySurveyIdUserId(string surveyId, string userId)
        {
            return await _surveyUserRepository.ExistAsync(x => x.SurveyId == surveyId && x.UserId == userId);
        }

        public async Task<int> CountSurveyUser(string surveyId)
        {
            return await _surveyUserRepository.CountAsync(x => x.SurveyId == surveyId);
        }

        public async Task<SurveyUser> GetInfoByUserId(string surveyId, string userId, bool isReadOnly = false)
        {
            return await _surveyUserRepository.GetAsync(isReadOnly, x => x.SurveyId == surveyId && x.UserId == userId);
        }

        public List<SurveyUsersReport> SearchSurveyUsersReport(string surveyId, string keyword, int page, int pageSize, out int totalRows)
        {
            var surveyIdParam = new SqlParameter("@surveyId", surveyId);
            var keywordParam = new SqlParameter("@keyword", (object)keyword ?? DBNull.Value);
            var pageParam = new SqlParameter("@page", page);
            var pageSizeParam = new SqlParameter("@pageSize", pageSize);
            var totalRowsParam = new SqlParameter("@TotalRows", 0)
            {
                Direction = ParameterDirection.Output
            };

            var result = Context.Query<SurveyUsersReport>().FromSql(
                    "EXEC sp_SurveyUsersReport @surveyId, @keyword, @page, @pageSize, @totalRows OUTPUT",
                    surveyIdParam, keywordParam, pageParam, pageSizeParam, totalRowsParam)
                .ToList();

            totalRows = totalRowsParam.Value == null ? 0 : Convert.ToInt32(totalRowsParam.Value);
            return result;
        }

        public Task<List<SurveyUserReportViewModel>> GetListSurveyByUserId(string tenantId, string languageId, string userId, string keyWord, int? surveyGroupId,
             DateTime? startDate, DateTime? endDate, int page, int pageSize, out int totalRows)
        {
            Expression<Func<SurveyUser, bool>> spec = x => x.UserId == userId;
            Expression<Func<Domain.Models.Survey, bool>> specSurvey = x => !x.IsDelete && x.Status == Domain.Constants.SurveyStatus.Approved
                                                         && x.TenantId == tenantId && x.TotalQuestion > 0;
            Expression<Func<SurveyTranslation, bool>> specSurveyTranslation = x => x.LanguageId == languageId;

            if (!string.IsNullOrEmpty(keyWord))
            {
                keyWord = keyWord.Trim().StripVietnameseChars().ToUpper();
                specSurveyTranslation = specSurveyTranslation.And(x => x.UnsignName.Contains(keyWord));
            }

            if (surveyGroupId.HasValue)
            {
                specSurvey = specSurvey.And(x => x.SurveyGroupId == surveyGroupId);
            }

            if (startDate.HasValue)
            {
                startDate = startDate.Value.Date.AddMilliseconds(-1);
                specSurvey = specSurvey.And(x => x.StartDate >= startDate);
            }

            if (endDate.HasValue)
            {
                endDate = endDate.Value.Date.AddDays(1).AddMilliseconds(-1);
                specSurvey = specSurvey.And(x => x.StartDate <= endDate);
            }

            var query = Context.Set<SurveyUser>().Where(spec)
                       .Join(Context.Set<Domain.Models.Survey>().Where(specSurvey), su => su.SurveyId, s => s.Id,
                       (su, s) => new {
                           SurveyId = s.Id,
                           SurveyUserId = su.Id,
                           s.StartDate,
                           s.EndDate,
                           s.LimitedTime
                       }).Join(Context.Set<SurveyTranslation>().Where(specSurveyTranslation), s => s.SurveyId, st => st.SurveyId,
                       (s, st) => new
                       {
                           s.SurveyId,
                           s.SurveyUserId,
                           SurveyName = st.Name,
                           s.StartDate,
                           s.EndDate,
                           s.LimitedTime
                       })
                       .GroupJoin(Context.Set<SurveyUserAnswerTime>().Where(x => x.StartTime != null),
                          ssu => ssu.SurveyUserId, sua => sua.SurveyUserId, (ssu, sua) => new { ssu, sua })
                         .SelectMany(x => x.sua.DefaultIfEmpty(), (x, sua) => new SurveyUserReportViewModel {
                             SurveyId = x.ssu.SurveyId,
                             SurveyUserId = x.ssu.SurveyUserId,
                             SurveyName = x.ssu.SurveyName,
                             StartDate = x.ssu.StartDate,
                             EndDate = x.ssu.EndDate,
                             LimitedTime = x.ssu.LimitedTime,
                             StartTimeExam = sua.StartTime,
                             EndTimeExam = sua.EndTime,
                             IsViewResult = sua.IsViewResult,
                             //TotalTimeExam =  sua != null && sua.EndTime.HasValue ? (sua.EndTime.Value - sua.StartTime).TotalMinutes : 0,
                             SurveyUserTimeId = sua.Id
                         }).Distinct().AsNoTracking();

            totalRows = query.Count();
            return query.OrderByDescending(x => x.StartDate)
                   .Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
        }
    }
}
