﻿using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.SqlServer;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.Models;
using GHM.Survey.Domain.Constants;

namespace GHM.Survey.Infrastructure.Repository
{
    public class QuestionRepository : RepositoryBase, IQuestionRepository
    {
        private readonly IRepository<Question> _questionRepository;
        private readonly IRepository<QuestionTranslation> _questionTranslationRepository;

        public QuestionRepository(IDbContext context) : base(context)
        {
            _questionRepository = Context.GetRepository<Question>();
            _questionTranslationRepository = Context.GetRepository<QuestionTranslation>();
        }

        public async Task<bool> CheckExistsByQuestionGroupId(int questionGroupId)
        {
            return await _questionRepository.ExistAsync(x => x.QuestionGroupId == questionGroupId);
        }

        public async Task<Question> GetInfo(string versionId, bool isReadOnly = false)
        {
            return await _questionRepository.GetAsync(isReadOnly, x => x.VersionId == versionId && !x.IsDelete);
        }

        public async Task<Question> GetInfo(string tenantId, string questionId)
        {
            return await _questionRepository.GetAsync(false, x => x.VersionId == questionId && x.TenantId == tenantId
                && !x.ToDate.HasValue && !x.IsDelete);
        }

        public async Task<List<Question>> GetsInfo(string tenantId, List<string> questionIds)
        {
            return await _questionRepository.GetsAsync(true, x => questionIds.Contains(x.VersionId) && !x.IsDelete && x.IsActive);
        }

        public async Task<Question> GetInfoRecord(string versionId)
        {
            return await _questionRepository.GetAsync(false, x => x.VersionId == versionId);
        }

        public async Task<bool> CheckExistsByQuestionId(string questionId, string tenantId, bool isReadOnly = false)
        {
            return await _questionRepository.ExistAsync(x => x.TenantId == tenantId && x.Id == questionId);
        }

        /// <summary>
        /// Kiểm tra Nội dung câu hỏi đã có chưa 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        public async Task<bool> CheckExistsByQuestionName(string name, string languageId)
        {
            string unsignName = name.Trim().StripVietnameseChars().ToUpper();
            return await _questionTranslationRepository.ExistAsync(x =>
                x.UnsignName == unsignName && x.LanguageId == languageId);
        }

        /// <summary>
        /// Xóa version câu hỏi
        /// </summary>
        /// <param name="questionVersionId"></param>
        /// <returns></returns>
        public async Task<int> Delete(string questionVersionId)
        {
            var questionInfo = await GetInfo(questionVersionId);
            if (questionInfo == null)
                return -1;

            //if (questionInfo.Status==QuestionStatus.Approved)
            //    return -2;

            questionInfo.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(string tenantId, string versionId, string questionId)
        {
            var questionInfo = await GetInfo(tenantId, questionId, versionId);
            if (questionInfo == null)
                return -1;

            if (questionInfo.Status == QuestionStatus.Approved)
                return -2;

            _questionRepository.Delete(questionInfo);
            return await Context.SaveChangesAsync();
        }

        /// <summary>
        /// Danh sách câu hỏi theo nhóm câu hỏi
        /// </summary>
        /// <param name="questionGroupId"></param>
        /// <returns></returns>
        public async Task<List<Question>> GetAllQuestionByGroupQuestionId(int questionGroupId)
        {
            return await _questionRepository.GetsAsync(false, x => x.QuestionGroupId == questionGroupId);
        }

        /// <summary>
        /// Danh sách các Vertion câu hỏi theo câu hỏi
        /// </summary>
        /// <param name="tenantId">Mã khách hàng</param>
        /// <param name="questionId">Mã câu hỏi</param>
        /// <param name="versionId">Mã version của câu hỏi</param>
        /// <param name="isReadOnly">Chỉ đọc</param>
        /// <returns></returns>
        public async Task<Question> GetInfo(string tenantId, string questionId, string versionId,
            bool isReadOnly = false)
        {
            return await _questionRepository.GetAsync(isReadOnly,
                x => x.TenantId == tenantId && x.Id == questionId && x.VersionId == versionId);
        }

        public Task<List<QuestionViewModel>> SearchForApprove(string tenantId, string languageId, string keyword, QuestionType? questionType,
            int? questionGroupId, string creatorId, int page, int pageSize, out int totalRows)
        {
            Expression<Func<Question, bool>> spec = x => x.TenantId == tenantId && !x.IsDelete && !x.ToDate.HasValue && x.Status == QuestionStatus.Pending;
            Expression<Func<QuestionTranslation, bool>> specTranslation = x => x.LanguageId == languageId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            if (questionType.HasValue)
                spec = spec.And(x => x.Type == questionType.Value);

            if (questionGroupId.HasValue)
                spec = spec.And(x => x.QuestionGroupId == questionGroupId);

            if (!string.IsNullOrEmpty(creatorId))
                spec = spec.And(x => x.CreatorId == creatorId);

            var query = Context.Set<Question>().Where(spec)
                .Join(Context.Set<QuestionTranslation>().Where(specTranslation),
                    question => question.VersionId,
                    questionTranslation => questionTranslation.QuestionVersionId,
                    (question, questionTranslation) => new QuestionViewModel
                    {
                        Id = question.Id,
                        VersionId = question.VersionId,
                        Name = questionTranslation.Name,
                        GroupName = questionTranslation.GroupName,
                        CreatorId = question.CreatorId,
                        FullName = question.CreatorFullName,
                        Status = question.Status,
                        Type = question.Type,
                        CreateTime = question.CreateTime,
                        IsActive = question.IsActive
                    }).AsNoTracking();

            totalRows = query.Count();
            return query
                .OrderByDescending(x => x.CreateTime)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        /// <summary>
        /// Thêm mới câu hỏi
        /// </summary>
        /// <param name="question"></param>
        /// <returns></returns>
        public async Task<int> Insert(Question question)
        {
            _questionRepository.Create(question);
            return await Context.SaveChangesAsync();
        }

        public Task<List<QuestionViewModel>> Search(string tenantId, string languageId, string keyword,
            QuestionType? questionType, int? questionGroupId, string creatorId, string currentUserId, QuestionStatus? questionStatus,
            int page, int pageSize, out int totalRows)
        {
            Expression<Func<Question, bool>> spec = x => x.TenantId == tenantId && !x.IsDelete && !x.ToDate.HasValue;
            Expression<Func<QuestionTranslation, bool>> specTranslation = x => x.LanguageId == languageId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            if (questionType.HasValue)
                spec = spec.And(x => x.Type == questionType.Value);

            if (questionGroupId.HasValue)
                spec = spec.And(x => x.QuestionGroupId == questionGroupId.Value);

            if (!string.IsNullOrEmpty(creatorId))
                spec = spec.And(x => x.CreatorId.Equals(creatorId));

            if (questionStatus.HasValue)
            {
                spec = questionStatus.Value == QuestionStatus.Approved
                    ? spec.And(x => x.Status == questionStatus.Value)
                    : spec.And(x => x.Status == questionStatus.Value && x.CreatorId == currentUserId);
            }
            else
            {
                spec = spec.And(x =>
                    x.Status == QuestionStatus.Approved ||
                    x.Status == QuestionStatus.Draft && x.CreatorId == currentUserId ||
                    x.Status == QuestionStatus.Pending && x.CreatorId == currentUserId ||
                    x.Status == QuestionStatus.Decline && x.CreatorId == currentUserId
                );
            }

            var query = Context.Set<Question>().Where(spec)
                .Join(Context.Set<QuestionTranslation>().Where(specTranslation),
                    question => question.VersionId,
                    questionTranslation => questionTranslation.QuestionVersionId,
                    (question, questionTranslation) => new QuestionViewModel
                    {
                        Id = question.Id,
                        VersionId = question.VersionId,
                        Name = questionTranslation.Name,
                        GroupName = questionTranslation.GroupName,
                        CreatorId = question.CreatorId,
                        FullName = question.CreatorFullName,
                        Status = question.Status,
                        Type = question.Type,
                        CreateTime = question.CreateTime,
                        IsActive = question.IsActive
                    }).AsNoTracking();

            totalRows = query.Count();
            return query
                .OrderByDescending(x => x.CreateTime)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<int> Update(Question question)
        {
            return await Context.SaveChangesAsync();

        }

        /// <summary>
        /// Danh sách câu hỏi theo người duyệt
        /// </summary>
        /// <param name="approverUserId"></param>
        /// <returns></returns>
        public async Task<List<QuestionViewModel>> GetAllQuestionByApproverUserId(string tenantId, string approverUserId, string languageId)
        {
            Expression<Func<Question, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId;
            Expression<Func<QuestionTranslation, bool>> specTranslation = x => x.LanguageId == languageId;
            if (!string.IsNullOrEmpty(approverUserId))
            {
                spec = spec.And(x => x.ApproverUserId == approverUserId);
            }

            var query = Context.Set<Question>().AsNoTracking().Where(spec)
                .Join(Context.Set<QuestionTranslation>().AsNoTracking().Where(specTranslation),
                    question => question.VersionId,
                    questionTranslation => questionTranslation.QuestionVersionId, (question, questionTranslation) =>
                        new QuestionViewModel
                        {
                            Id = question.Id,
                            VersionId = question.VersionId,
                            Name = questionTranslation.Name,
                            GroupName = questionTranslation.GroupName,
                            FullName = question.CreatorFullName,
                            Status = question.Status,
                            Type = question.Type,
                            IsActive = question.IsActive
                        });

            return await query.AsNoTracking().ToListAsync();
        }

        public Task<List<QuestionSuggestionViewModel>> SearchForSuggestion(string tenantId, string languageId, string keyword,
            int? questionGroupId, QuestionType? type, SurveyType? surveyType, int page, int pageSize, out int totalRows)
        {
            Expression<Func<Question, bool>> spec = x =>
                !x.IsDelete && x.IsActive && x.Status == QuestionStatus.Approved && x.TenantId == tenantId && !x.ToDate.HasValue;

            Expression<Func<QuestionTranslation, bool>> specTranslation = x =>
                x.LanguageId == languageId;

            if (questionGroupId.HasValue)
                spec = spec.And(x => x.QuestionGroupId == questionGroupId.Value);

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            if (type.HasValue)
                spec = spec.And(x => x.Type == type);

            if (surveyType.HasValue)
            {
                if(surveyType == SurveyType.Logic)
                {
                    spec = spec.And(x => x.Type == QuestionType.Logic);
                }
                else
                {
                    spec = spec.And(x => x.Type != QuestionType.Logic);
                }
            }

            var query = Context.Set<Question>().Where(spec)
                .Join(Context.Set<QuestionTranslation>().Where(specTranslation), q => q.VersionId,
                    qt => qt.QuestionVersionId,
                    (q, qt) => new QuestionSuggestionViewModel
                    {
                        Id = q.VersionId,
                        Name = qt.Name,
                        Type = q.Type,
                        TotalAnswer = q.TotalAnswer
                    }).AsNoTracking();

            totalRows = query.Count();
            return query
                .OrderBy(x => x.Id)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        //public async Task<Question> GetInfo(string versionId, bool isReadOnly = false)
        //{
        //    return await _questionRepository.GetAsync(isReadOnly, x => x.VersionId == versionId);
        //}

        public async Task<int> ChangeQuestionStatus(string questionVersionId, QuestionStatus questionStatus)
        {
            return await Context.SaveChangesAsync();
        }

        public Task<int> GetCountQuestionByGroupQuestionId(int questionGroupId)
        {
            return Context.Set<Question>().AsNoTracking()
                .Where(x => x.QuestionGroupId == questionGroupId && !x.IsDelete && !x.ToDate.HasValue && x.Status == QuestionStatus.Approved)
                .Select(x => x.Id).Distinct().CountAsync();
        }

        public async Task<int> UpdateDeclineReason(string questionVersionId, string declineReason)
        {
            var questionInfo = await GetInfoRecord(questionVersionId);
            if (questionInfo == null)
                return -1;

            if (questionInfo.Status == QuestionStatus.Approved)
                return -2;

            questionInfo.DeclineReason = declineReason;
            questionInfo.Status = QuestionStatus.Decline;
            return await Context.SaveChangesAsync();
        }
    }
}