﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;
namespace GHM.Survey.Infrastructure.Repository
{
    public class SurveyGroupRepository : RepositoryBase, ISurveyGroupRepository
    {
        private readonly IRepository<SurveyGroup> _surveyGroupRepository;
        public SurveyGroupRepository(IDbContext context) : base(context)
        {
            _surveyGroupRepository = Context.GetRepository<SurveyGroup>();
        }

        public async Task<bool> CheckExists(int id)
        {
            return await _surveyGroupRepository.ExistAsync(x => x.Id == id && !x.IsDelete && x.IsActive);
        }

        public async Task<int> Insert(SurveyGroup surveyGroup)
        {
            _surveyGroupRepository.Create(surveyGroup);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(SurveyGroup surveyGroup)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateSurveyGroupIdPath(int id, string idPath)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateChildCount(int id, int childCount)
        {
            var info = await GetInfo(id);
            if (info == null)
                return -1;

            info.ChildCount = childCount;
            return await Context.SaveChangesAsync();
        }
        public async Task<int> UpdateChildrenIdPath(string oldIdPath, string newIdPath)
        {
            var childrenSurveyGroup = await _surveyGroupRepository.GetsAsync(false, x => !x.IsDelete && x.IdPath.StartsWith(oldIdPath + "."));
            if (childrenSurveyGroup == null || !childrenSurveyGroup.Any())
                return -1;

            foreach (var surveyGroup in childrenSurveyGroup)
            {
                surveyGroup.IdPath = $"{newIdPath}.{surveyGroup.Id}";
            }
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(int surveyGroupId)
        {
            var info = await GetInfo(surveyGroupId);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(int surveyGroupId)
        {
            var info = await GetInfo(surveyGroupId);
            if (info == null)
                return -1;

            _surveyGroupRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<SurveyGroup> GetInfo(int surveyGroupId, bool isReadOnly = false)
        {
            return await _surveyGroupRepository.GetAsync(isReadOnly, x => x.Id == surveyGroupId && !x.IsDelete);
        }

        public async Task<SurveyGroup> GetInfo(string surveyGroupIdPath, bool isReadOnly = false)
        {
            return await _surveyGroupRepository.GetAsync(isReadOnly, x => x.IdPath == surveyGroupIdPath && !x.IsDelete);
        }

        public async Task<int> GetChildCount(int id)
        {
            return await _surveyGroupRepository.CountAsync(x => x.ParentId == id && !x.IsDelete);
        }

        public async Task<List<SurveyGroupSearchViewModel>> GetAllActivatedSurveyGroup(string tenantId, string languageId)
        {
            var query = Context.Set<SurveyGroup>().Where(x => x.IsActive && !x.IsDelete && x.TenantId == tenantId)
                .Join(Context.Set<SurveyGroupTranslation>().Where(x => x.LanguageId == languageId), o => o.Id,
                    ot => ot.SurveyGroupId, (o, ot) => new SurveyGroupSearchViewModel
                    {
                        Id = o.Id,
                        Name = ot.Name,
                        Description = ot.Description,
                        IsActive = o.IsActive,
                        ParentId = o.ParentId,
                        IdPath = o.IdPath,
                        ChildCount = o.ChildCount
                    });
            return await query
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<List<SurveyGroupSearchViewModel>> GetActivedSurveyGroup(string tenantId, string languageId)
        {
            var query = Context.Set<SurveyGroup>().Where(x => x.IsActive && !x.IsDelete && x.TenantId == tenantId)
                .Join(Context.Set<SurveyGroupTranslation>().Where(x => x.LanguageId == languageId), o => o.Id,
                    ot => ot.SurveyGroupId, (o, ot) => new SurveyGroupSearchViewModel
                    {
                        Id = o.Id,
                        Name = ot.Name,
                        Description = ot.Description,
                        IsActive = o.IsActive,
                        ParentId = o.ParentId,
                        IdPath = o.IdPath,
                        ChildCount = o.ChildCount
                    });
            return await query
                .AsNoTracking()
                .ToListAsync();
        }

        public Task<List<SurveyGroupForSelectViewModel>> GetAllSurveyGroupForSelect(string tenantId, string languageId, string keyword,
            int page, int pageSize, out int totalRows)
        {
            Expression<Func<SurveyGroup, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId && x.IsActive;
            Expression<Func<SurveyGroupTranslation, bool>> specTranslation = x => x.LanguageId == languageId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            var query = Context.Set<SurveyGroup>().Where(spec)
                .Join(Context.Set<SurveyGroupTranslation>().Where(specTranslation), o => o.Id,
                    ot => ot.SurveyGroupId, (o, ot) => new SurveyGroupForSelectViewModel
                    {
                        Id = o.Id,
                        Name = ot.Name,
                    });

            totalRows = query.Count();

            return query
                .AsNoTracking()
                .OrderBy(c => c.Name)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        public Task<List<SurveyGroupSearchViewModel>> SearchSurveyGroup(string tenantId, string languageId, string keyword,
            bool? isActive, int page, int pageSize, out int totalRows)
        {
            Expression<Func<SurveyGroup, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId;
            Expression<Func<SurveyGroupTranslation, bool>> specTranslation = x => x.LanguageId == languageId;
            if (isActive.HasValue)
                spec = spec.And(x => x.IsActive == isActive.Value);

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            var query = Context.Set<SurveyGroup>().Where(spec)
                .Join(Context.Set<SurveyGroupTranslation>().Where(specTranslation), o => o.Id, ot => ot.SurveyGroupId,
                (o, ot) => new SurveyGroupSearchViewModel
                {
                    Id = o.Id,
                    IdPath = o.IdPath,
                    Name = ot.Name,
                    Description = ot.Description,
                    IsActive = o.IsActive,
                    ChildCount = o.ChildCount
                });

            totalRows = query.Count();
            return query
                .AsNoTracking()
                .OrderByDescending(x => x.Name)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        public Task<List<SurveyGroupSearchViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive, int page,
            int pageSize, out int totalRows)
        {
            Expression<Func<SurveyGroup, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId;
            Expression<Func<SurveyGroupTranslation, bool>> specTranslation = x => x.LanguageId == languageId;
            if (isActive.HasValue)
                spec = spec.And(x => x.IsActive == isActive.Value);

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            var query = Context.Set<SurveyGroup>().Where(spec)
                .Join(Context.Set<SurveyGroupTranslation>().Where(specTranslation), o => o.Id, ot => ot.SurveyGroupId,
                (o, ot) => new SurveyGroupSearchViewModel
                {
                    Id = o.Id,
                    IdPath = o.IdPath,
                    Name = ot.Name,
                    Description = ot.Description,
                    IsActive = o.IsActive,
                    ParentId = o.ParentId,
                    ChildCount = o.ChildCount
                });

            totalRows = query.Count();
            return query
                .AsNoTracking()
                .OrderBy(x => x.IdPath).Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }
    }
}
