﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GHM.Infrastructure.SqlServer;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace GHM.Survey.Infrastructure.Repository
{
    public class SurveyQuestionAnswerLogicRepository : RepositoryBase, ISurveyQuestionAnswerLogicRepository
    {
        private readonly IRepository<SurveyQuestionAnswerLogic> _surveyQusetionAnswerLogicRepository;
        public SurveyQuestionAnswerLogicRepository(IDbContext context) : base(context)
        {
            _surveyQusetionAnswerLogicRepository = Context.GetRepository<SurveyQuestionAnswerLogic>();
        }

        public async Task<List<SurveyQuestionAnswerLogicViewModel>> GetsBySurveyId(string surveyId, string languageId)
        {
            var query = Context.Set<SurveyQuestionAnswerLogic>().Where(x => x.SurveyId == surveyId)
                .Join(Context.Set<QuestionTranslation>(), sqal => sqal.ToQuestionVersionId, qt => qt.QuestionVersionId,
                    (sqal, qt) => new SurveyQuestionAnswerLogicViewModel
                    {
                        AnswerId = sqal.AnswerId,
                        ToQuestionVersionId = sqal.ToQuestionVersionId,
                        ToQuestionName = qt.Name
                    }).AsNoTracking();
            return await query.ToListAsync();
        }

        public async Task<int> Inserts(List<SurveyQuestionAnswerLogic> listSurveyQuestionAnswerLogics)
        {
            _surveyQusetionAnswerLogicRepository.Creates(listSurveyQuestionAnswerLogics);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeleteBySurveyId(string surveyId)
        {
            var result = await _surveyQusetionAnswerLogicRepository.GetsAsync(false, x => x.SurveyId == surveyId);
            if (result == null || !result.Any())
                return -1;

            _surveyQusetionAnswerLogicRepository.Deletes(result);
            return await Context.SaveChangesAsync();
        }
    }
}
