﻿using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Survey.Domain.Constants;
using GHM.Survey.Domain.ModelMetas;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;

namespace GHM.Survey.Infrastructure.Repository
{
    public class SurveyRepository : RepositoryBase, ISurveyRepository
    {
        private readonly IRepository<Domain.Models.Survey> _surveyRepository;
        public SurveyRepository(IDbContext context) : base(context)
        {
            _surveyRepository = Context.GetRepository<Domain.Models.Survey>();
        }

        public async Task<bool> CheckSurveyExistsByTenantId(string tenantId, string surveyId)
        {
            return await _surveyRepository.ExistAsync(x => x.TenantId == tenantId && x.Id == surveyId && !x.IsDelete);
        }

        public async Task<int> Insert(Domain.Models.Survey survey)
        {
            _surveyRepository.Create(survey);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(Domain.Models.Survey survey)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string surveyId)
        {
            Domain.Models.Survey info = await GetInfo(surveyId);
            if (info == null)
            {
                return -1;
            }

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(string surveyId)
        {
            Domain.Models.Survey info = await GetInfo(surveyId);
            if (info == null)
            {
                return -1;
            }

            _surveyRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateTotalUser(string surveyId, int totalUser)
        {
            Domain.Models.Survey info = await GetInfo(surveyId);
            if (info == null)
            {
                return -1;
            }

            info.TotalUser = totalUser;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateTotalQuestion(string surveyId, int totalQuestion)
        {
            var info = await GetInfo(surveyId);
            if (info == null)
                return -1;

            info.TotalQuestion = totalQuestion;
            return await Context.SaveChangesAsync();
        }

        public async Task<Domain.Models.Survey> GetInfo(string id, bool isReadonly = false)
        {
            return await _surveyRepository.GetAsync(isReadonly, x => x.Id == id);
        }

        public async Task<Domain.Models.Survey> GetInfo(string tenantId, string id, bool isReadonly = false)
        {
            return await _surveyRepository.GetAsync(isReadonly,
                x => x.TenantId == tenantId && x.Id == id && !x.IsDelete);
        }

        public async Task<bool> CheckExistsBySurveyGroupId(int surveyGroupId)
        {
            return await _surveyRepository.ExistAsync(x => x.SurveyGroupId == surveyGroupId);
        }

        public async Task<List<SurveyQuestionViewModel>> GetListSurveyQuestions(string surveyId, string languageId)
        {
            Expression<Func<SurveyQuestion, bool>> spec = x => x.SurveyId == surveyId;
            Expression<Func<QuestionTranslation, bool>> specTranslation = t => t.LanguageId == languageId;

            IQueryable<SurveyQuestionViewModel> query = Context.Set<SurveyQuestion>().Where(spec)
                .Join(Context.Set<QuestionTranslation>().Where(specTranslation), surveyQuestion => surveyQuestion.QuestionVersionId,
                    questionTranslation => questionTranslation.QuestionVersionId, (surveyQuestion, questionTranslation) =>
                        new SurveyQuestionViewModel()
                        {
                            QuestionVersionId = surveyQuestion.QuestionVersionId,
                            QuestionName = questionTranslation.Name
                        });

            return await query.AsNoTracking().ToListAsync();
        }

        public async Task<List<SurveyQuestionGroupViewModel>> GetListSurveyQuestionGroups(string surveyId, string languageId)
        {
            Expression<Func<SurveyQuestionGroup, bool>> spec = x => x.SurveyId == surveyId;
            Expression<Func<QuestionGroupTranslation, bool>> specTranslation = t => t.LanguageId == languageId;

            IQueryable<SurveyQuestionGroupViewModel> query = Context.Set<SurveyQuestionGroup>().Where(spec)
                .Join(Context.Set<QuestionGroupTranslation>().Where(specTranslation), surveyQuestionGroup => surveyQuestionGroup.QuestionGroupId,
                    questionGroupTranslation => questionGroupTranslation.QuestionGroupId, (surveyQuestionGroup, questionGroupTranslation) =>
                        new SurveyQuestionGroupViewModel
                        {
                            SurveyId = surveyId,
                            Id = surveyQuestionGroup.QuestionGroupId,
                            Name = questionGroupTranslation.Name,
                            TotalQuestions = surveyQuestionGroup.TotalQuestion
                        });

            return await query.AsNoTracking().ToListAsync();
        }

        public async Task<List<SurveyUserQuestionAnswer>> GetSurveyUserQuestionAnswer(string tenantId, string languageId, string surveyId,
            string surveyUserId, string timesId, bool isPrerendering)
        {
            SqlParameter tenantIdParam = new SqlParameter("@tenantId", tenantId);
            SqlParameter languageIdParam = new SqlParameter("@languageId", languageId);
            SqlParameter surveyIdParam = new SqlParameter("@surveyId", surveyId);
            SqlParameter surveyUserIdParam = new SqlParameter("@surveyUserId", surveyUserId);
            SqlParameter timesIdParam = new SqlParameter("@timesId", timesId);
            SqlParameter isPrerenderingParam = new SqlParameter("@isPrerendering", isPrerendering);

            Task<List<SurveyUserQuestionAnswer>> result = Context.Query<SurveyUserQuestionAnswer>().FromSql(
                    "EXEC sp_GetSurveyUserQuestionAnswer @tenantId, @languageId, @surveyId, @surveyUserId, @timesId, @isPrerendering",
                    tenantIdParam, languageIdParam, surveyIdParam, surveyUserIdParam, timesIdParam,
                    isPrerenderingParam)
                .ToListAsync();
            return await result;
        }

        public async Task<List<SurveyUserQuestionAnswer>> GetSurveyUserQuestionLogicAnswer(string languageId, string surveyId,
            string surveyUserId, string timesId)
        {
            return await (
                from su in Context.Set<SurveyUser>()
                join suat in Context.Set<SurveyUserAnswerTime>() on su.Id equals suat.SurveyUserId
                join sq in Context.Set<SurveyQuestion>() on su.SurveyId equals sq.SurveyId
                join q in Context.Set<Question>() on sq.QuestionVersionId equals q.VersionId
                join qt in Context.Set<QuestionTranslation>() on q.VersionId equals qt.QuestionVersionId
                join a in Context.Set<Answer>() on q.VersionId equals a.QuestionVersionId
                join at in Context.Set<AnswerTranslation>() on a.Id equals at.AnswerId
                join sqal in Context.Set<SurveyQuestionAnswerLogic>().Where(x => x.SurveyId == surveyId) on a.Id equals sqal.AnswerId
                into suqa
                from sqal in suqa.DefaultIfEmpty()
                where su.SurveyId == surveyId && qt.LanguageId == languageId
                      && at.LanguageId == languageId && suat.Id == timesId
                select new SurveyUserQuestionAnswer
                {
                    TimeId = suat.Id,
                    QuestionVersionId = q.VersionId,
                    QuestionType = q.Type,
                    QuestionContent = qt.Content,
                    QuestionOrder = sq.Order,
                    TotalAnswer = q.TotalAnswer,
                    QuestionName = qt.Name,
                    AnswerId = a.Id,
                    AnswerName = at.Name,
                    ToQuestionVersionId = sqal != null ? sqal.ToQuestionVersionId : null,
                }
            )
            .OrderBy(x => x.QuestionOrder)
            .ToListAsync();
        }

        public async Task<List<SurveyUserQuestionAnswerDetailViewModel>> GetSurveyUserQuestionLogicAnswerDetail(string tenantId, string languageId,
            string surveyId, string surveyUserId, string timesId)
        {
            SqlParameter tenantIdParam = new SqlParameter("@tenantId", tenantId);
            SqlParameter languageIdParam = new SqlParameter("@languageId", languageId);
            SqlParameter surveyIdParam = new SqlParameter("@surveyId", surveyId);
            SqlParameter surveyUserIdParam = new SqlParameter("@surveyUserId", surveyUserId);
            SqlParameter timesIdParam = new SqlParameter("@timesId", timesId);

            Task<List<SurveyUserQuestionAnswerDetailViewModel>> result = Context.Query<SurveyUserQuestionAnswerDetailViewModel>().FromSql(
                "EXEC sp_GetSurveyUserQuestionLogicAnswerDetail @tenantId, @languageId, @surveyId, @surveyUserId, @timesId",
                tenantIdParam, languageIdParam, surveyIdParam, surveyUserIdParam, timesIdParam)
            .ToListAsync();

            return await result;
        }

        public List<SurveyReport> SearchReport(string tenantId, string languageId, string keyword, int? surveyGroupId, DateTime? startDate,
            DateTime? endDate, int page, int pageSize, out int totalRows)
        {

            SqlParameter tenantIdParam = new SqlParameter("@tenantId", tenantId);
            SqlParameter languageIdParam = new SqlParameter("@languageId", languageId);
            SqlParameter surveyGroupIdParam = new SqlParameter("@surveyGroupId", (object)surveyGroupId ?? DBNull.Value);
            SqlParameter keywordParam = new SqlParameter("@keyword", (object)keyword ?? DBNull.Value);
            SqlParameter startDateParam = new SqlParameter("@startDate", (object)startDate ?? DBNull.Value);
            SqlParameter endDateParam = new SqlParameter("@endDate", (object)endDate ?? DBNull.Value);
            SqlParameter pageParam = new SqlParameter("@page", page);
            SqlParameter pageSizeParam = new SqlParameter("@pageSize", pageSize);
            SqlParameter totalRowsParam = new SqlParameter("@TotalRows", 0)
            {
                Direction = ParameterDirection.Output
            };

            List<SurveyReport> result = Context.Query<SurveyReport>().FromSql(
                    "EXEC sp_SurveyReport @tenantId, @languageId, @surveyGroupId, @startDate, @endDate,  @keyword, @page, @pageSize, @TotalRows OUTPUT",
                    tenantIdParam, languageIdParam, surveyGroupIdParam, startDateParam, endDateParam, keywordParam,
                    pageParam, pageSizeParam, totalRowsParam)
                .ToList();

            totalRows = totalRowsParam.Value == null ? 0 : Convert.ToInt32(totalRowsParam.Value);
            return result;
        }

        public Task<List<SurveyViewModel>> Search(string tenantId, string languageId, string keyword, int? surveyGroupId,
            DateTime? startDate, DateTime? endDate, bool? isActive, int page, int pageSize, out int totalRows)
        {
            Expression<Func<Domain.Models.Survey, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId;
            Expression<Func<SurveyTranslation, bool>> specTranslation = t => t.LanguageId == languageId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            if (isActive.HasValue)
            {
                spec = spec.And(x => x.IsActive);
            }

            if (surveyGroupId.HasValue)
            {
                spec = spec.And(x => x.SurveyGroupId == surveyGroupId.Value);
            }

            if (startDate.HasValue)
            {
                spec = spec.And(x => x.StartDate >= startDate.Value.Date.AddMilliseconds(-1));
            }

            if (endDate.HasValue)
            {
                spec = spec.And(x => x.StartDate <= endDate.Value.Date.AddDays(1).AddMilliseconds(-1));
            }

            IQueryable<SurveyViewModel> query = Context.Set<Domain.Models.Survey>().Where(spec)
                .Join(Context.Set<SurveyTranslation>().Where(specTranslation), survey => survey.Id,
                    surveyTranslation => surveyTranslation.SurveyId, (survey, surveyTranslation) =>
                        new SurveyViewModel
                        {
                            Id = survey.Id,
                            SurveyGroupId = survey.SurveyGroupId,
                            IsActive = survey.IsActive,
                            IsRequire = survey.IsRequire,
                            TotalQuestion = survey.TotalQuestion,
                            TotalUser = survey.TotalUser,
                            LimitedTimes = survey.LimitedTimes,
                            LimitedTime = survey.LimitedTime,
                            Status = survey.Status,
                            StartDate = survey.StartDate,
                            EndDate = survey.EndDate,
                            Type = survey.Type,
                            Name = surveyTranslation.Name,
                            Description = surveyTranslation.Description,
                            SurveyGroupName = surveyTranslation.SurveyGroupName,
                            IsPreRendering = survey.IsPreRendering,
                            CreateTime = survey.CreateTime
                        }).AsNoTracking();
            totalRows = query.Count();

            return query
                .OrderByDescending(x => x.CreateTime)
                .Skip((page - 1) * pageSize)
                .Take(pageSize).AsNoTracking().ToListAsync();
        }



        public async Task<List<SurveyUserQuestionAnswerDetailViewModel>> GetSurveyUserQuestionAnswerDetail(string tenantId, string languageId,
            string surveyId, string surveyUserId, string timesId, bool isPrerendering, bool isManager)
        {
            SqlParameter tenantIdParam = new SqlParameter("@tenantId", tenantId);
            SqlParameter languageIdParam = new SqlParameter("@languageId", languageId);
            SqlParameter surveyIdParam = new SqlParameter("@surveyId", surveyId);
            SqlParameter surveyUserIdParam = new SqlParameter("@surveyUserId", surveyUserId);
            SqlParameter timesIdParam = new SqlParameter("@timesId", timesId);
            SqlParameter isPrerenderingParam = new SqlParameter("@isPrerendering", isPrerendering);
            SqlParameter isManagerParam = new SqlParameter("@isManager", isManager);

            Task<List<SurveyUserQuestionAnswerDetailViewModel>> result = Context.Query<SurveyUserQuestionAnswerDetailViewModel>().FromSql(
                "EXEC sp_GetSurveyUserQuestionAnswerDetail @tenantId, @languageId, @surveyId, @surveyUserId, @timesId, @isPrerendering, @isManager",
                tenantIdParam, languageIdParam, surveyIdParam, surveyUserIdParam, timesIdParam,
                isPrerenderingParam, isManagerParam)
            .ToListAsync();

            return await result;
        }

        public async Task<List<SurveyInfoViewModel>> UserGetTopSurvey(string tenantId, string languageId, string userId, int top)
        {
            var query = Context.Set<Domain.Models.Survey>().Where(x =>
                    x.TenantId == tenantId && x.IsActive && !x.IsDelete &&
                    (x.StartDate == null ||
                     (x.StartDate.HasValue && x.StartDate <= DateTime.Now) ||
                     (x.EndDate.HasValue && x.EndDate <= DateTime.Now)))
                .Join(Context.Set<SurveyTranslation>().Where(x => x.LanguageId == languageId), s => s.Id,
                    st => st.SurveyId, (s, st) => new { s, st })
                .Join(Context.Set<SurveyUser>().Where(x => x.UserId == userId), sst => sst.s.Id, su => su.SurveyId, (sst, su) => new SurveyInfoViewModel
                {
                    Id = sst.s.Id,
                    Name = sst.st.Name,
                    Type = sst.s.Type
                })
                .OrderBy(x => x.Name)
                .Skip(0)
                .Take(top)
                .AsNoTracking();
            return await query.ToListAsync();
        }
    }
}
