﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
using GHM.Infrastructure.SqlServer;

namespace GHM.Survey.Infrastructure.Repository
{
    public class SurveyGroupTranslationRepository : RepositoryBase, ISurveyGroupTranslationRepository
    {
        private readonly IRepository<SurveyGroupTranslation> _surveyGroupTranslation;
        public SurveyGroupTranslationRepository(IDbContext context) : base(context)
        {
            _surveyGroupTranslation = Context.GetRepository<SurveyGroupTranslation>();
        }

        public async Task<string> GetSurveyGroupName(string tenantId, int surveyGroupId, string languageId)
        {
            return await _surveyGroupTranslation.GetAsAsync(x => x.Name, x => x.SurveyGroupId == surveyGroupId
                                                                                && x.LanguageId == languageId && x.TenantId == tenantId);
        }

        public async Task<int> Insert(SurveyGroupTranslation surveyGroupTranslation)
        {
            _surveyGroupTranslation.Create(surveyGroupTranslation);
            return await Context.SaveChangesAsync();
        }


        public async Task<int> Inserts(List<SurveyGroupTranslation> surveyGroupTranslations)
        {
            _surveyGroupTranslation.Creates(surveyGroupTranslations);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(SurveyGroupTranslation surveyGroupTranslation)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDeleteBySurveyGroupId(string tenantId, int surveyGroupId)
        {
            var info = await _surveyGroupTranslation.GetsAsync(false, x => x.SurveyGroupId == surveyGroupId && x.TenantId == tenantId);
            if (info == null || !info.Any())
                return -1;

            _surveyGroupTranslation.Deletes(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<bool> CheckExistsByName(int surveyGroupId, string tenantId, string languageId, string name)
        {
            name = name.Trim();
            return await _surveyGroupTranslation.ExistAsync(x =>
                x.SurveyGroupId != surveyGroupId && x.TenantId == tenantId && x.LanguageId == languageId && x.Name == name);
        }

        public async Task<SurveyGroupTranslation> GetInfo(string tenantId, int surveyGroupId, string languageId, bool isReadonly = false)
        {
            return await _surveyGroupTranslation.GetAsync(isReadonly, c => c.SurveyGroupId == surveyGroupId && c.LanguageId == languageId && c.TenantId == tenantId);
        }

        public async Task<List<SurveyGroupTranslationViewModel>> GetsBySurveyGroupId(string tenantId, int surveyGroupId)
        {
            return await _surveyGroupTranslation.GetsAsAsync(x => new SurveyGroupTranslationViewModel
            {
                Name = x.Name,
                Description = x.Description,
                LanguageId = x.LanguageId,
                ParentName = x.ParentName
            }, x => x.SurveyGroupId == surveyGroupId && x.TenantId == tenantId);
        }
    }
}
