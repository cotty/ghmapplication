﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.Models;
using GHM.Infrastructure.SqlServer;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace GHM.Survey.Infrastructure.Repository
{
    public class SurveyQuestionRepository : RepositoryBase, ISurveyQuestionRepository
    {
        private readonly IRepository<SurveyQuestion> _surveyQuestionRepository;
        public SurveyQuestionRepository(IDbContext context) : base(context)
        {
            _surveyQuestionRepository = Context.GetRepository<SurveyQuestion>();
        }

        public async Task<int> Insert(SurveyQuestion surveyQuestion)
        {
            _surveyQuestionRepository.Create(surveyQuestion);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeleteBysurveyId(string surveyId)
        {
            var info = await _surveyQuestionRepository.GetsAsync(false, x => x.SurveyId == surveyId);
            if (info == null || !info.Any())
                return -1;
            _surveyQuestionRepository.Deletes(info);

            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<SurveyQuestion> surveyQuestions)
        {
            _surveyQuestionRepository.Creates(surveyQuestions);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string surveyId, string questionVersionId)
        {
            var info = await _surveyQuestionRepository.GetsAsync(false, x => x.SurveyId == surveyId && x.QuestionVersionId == questionVersionId);
            if (info == null || !info.Any())
                return -1;

            _surveyQuestionRepository.Deletes(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Deletes(List<SurveyQuestion> surveyQuestions)
        {
            if (surveyQuestions == null || !surveyQuestions.Any())
                return -1;

            _surveyQuestionRepository.Deletes(surveyQuestions);
            return await Context.SaveChangesAsync();
        }

        public async Task<bool> CheckExistsBySurveyIdQuestionVersionId(string surveyId, string questionVersionId)
        {
            return await _surveyQuestionRepository.ExistAsync(x => x.SurveyId == surveyId && x.QuestionVersionId == questionVersionId);
        }

        public async Task<int> GetTotalQuestion(string surveyId)
        {
            return await _surveyQuestionRepository.CountAsync(x => x.SurveyId == surveyId);
        }

        public async Task<List<SurveyQuestion>> GetListSurveyQuestions(string surveyId, bool isReadOnly = false)
        {
            return await _surveyQuestionRepository.GetsAsync(isReadOnly, x => x.SurveyId == surveyId);
        }

        public List<SurveyQuestionSearchViewModel> GetSurveyQuestions(string surveyId, string languageId)
        {
            var listSurveyQuestionSearchViewModel = new List<SurveyQuestionSearchViewModel>();
            var query = Context.Set<SurveyQuestion>().Where(x => x.SurveyId == surveyId)
                .Join(Context.Set<Question>(), surveyQuestion => surveyQuestion.QuestionVersionId, question => question.VersionId,
                (surveyQuestion, question) => new
                {
                    question.VersionId,
                    question.Type,
                    question.TotalAnswer,
                    surveyQuestion.Point,
                    surveyQuestion.Order,
                })
                .Join(Context.Set<QuestionTranslation>().Where(x => x.LanguageId == languageId),
                    surveyQuestionQuestion => surveyQuestionQuestion.VersionId,
                    questionTranslation => questionTranslation.QuestionVersionId,
                    (surveyQuestionQuestion, questionTranslation) => new
                    {
                        surveyQuestionQuestion.VersionId,
                        surveyQuestionQuestion.Point,
                        surveyQuestionQuestion.Type,
                        surveyQuestionQuestion.TotalAnswer,
                        surveyQuestionQuestion.Order,
                        questionTranslation.Name
                    })
                .GroupJoin(Context.Set<Answer>(),
                    surveyQuestionQuestionTranslation => surveyQuestionQuestionTranslation.VersionId, answer => answer.QuestionVersionId,
                    (surveyQuestionQuestionTranslation, answer) => new
                    {
                        surveyQuestionQuestionTranslation,
                        answer
                    })
                    .SelectMany(x => x.answer.DefaultIfEmpty(), (x, answer) => new
                    {
                        x.surveyQuestionQuestionTranslation.VersionId,
                        x.surveyQuestionQuestionTranslation.Point,
                        x.surveyQuestionQuestionTranslation.Name,
                        x.surveyQuestionQuestionTranslation.Type,
                        x.surveyQuestionQuestionTranslation.TotalAnswer,
                        x.surveyQuestionQuestionTranslation.Order,
                        AnswerId = answer.Id,
                    })
                .GroupJoin(Context.Set<AnswerTranslation>().Where(x => x.LanguageId == languageId), surveyQuestionAnswer => surveyQuestionAnswer.AnswerId,
                    answerTranslation => answerTranslation.AnswerId,
                    (surveyQuestionAnswer, answerTranslation) => new
                    {
                        surveyQuestionAnswer,
                        answerTranslation
                    })
                    .SelectMany(x => x.answerTranslation.DefaultIfEmpty(), (x, answerTranslation) => new
                    {
                        x.surveyQuestionAnswer.VersionId,
                        x.surveyQuestionAnswer.Point,
                        x.surveyQuestionAnswer.Name,
                        x.surveyQuestionAnswer.Type,
                        x.surveyQuestionAnswer.AnswerId,
                        x.surveyQuestionAnswer.TotalAnswer,
                        x.surveyQuestionAnswer.Order,
                        AnswerName = answerTranslation.Name
                    })
                    .AsNoTracking();

            var groupByQuestion = query.GroupBy(x => new { x.VersionId, x.Point, x.Type, x.Name, x.TotalAnswer, x.Order });
            if (!groupByQuestion.Any())
                return listSurveyQuestionSearchViewModel;

            listSurveyQuestionSearchViewModel.AddRange(groupByQuestion.Select(x => new SurveyQuestionSearchViewModel
            {
                Id = x.Key.VersionId,
                Type = x.Key.Type,
                Name = x.Key.Name,
                Point = x.Key.Point,
                TotalAnswer = x.Key.TotalAnswer,
                Order = x.Key.Order,
                Answers = x.Select(answer => new SurveyQuestionAnswerSearchViewModel
                {
                    Id = answer.AnswerId,
                    Name = answer.AnswerName
                }).ToList()
            }).OrderBy(x=> x.Order));

            return listSurveyQuestionSearchViewModel;
        }
    }
}
