﻿using GHM.Infrastructure.SqlServer;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GHM.Survey.Infrastructure.Repository
{
    public class SurveyUserAnswerTimeRepository : RepositoryBase, ISurveyUserAnswerTimeRepository
    {
        private readonly IRepository<SurveyUserAnswerTime> _surveyUserAnswerTimeRepository;

        public SurveyUserAnswerTimeRepository(IDbContext context) : base(context)
        {
            _surveyUserAnswerTimeRepository = Context.GetRepository<SurveyUserAnswerTime>();
        }

        public async Task<int> Insert(SurveyUserAnswerTime surveyUserAnswerTime)
        {
            _surveyUserAnswerTimeRepository.Create(surveyUserAnswerTime);
            return await Context.SaveChangesAsync();
        }

        public async Task<bool> CheckExistsBySurveyId(string surveyId)
        {
            return await _surveyUserAnswerTimeRepository.ExistAsync(x => x.SurveyId == surveyId);
        }

        public async Task<bool> CheckIsActive(string surveyId, string surveyUserId)
        {
            return await _surveyUserAnswerTimeRepository.ExistAsync(x =>
                x.SurveyId == surveyId && x.SurveyUserId == surveyUserId
                && !x.EndTime.HasValue);
        }

        public async Task<bool> CheckExistsBySurveyIdAndSurveyUserId(string surveyId, string surveyUserId)
        {
            return await _surveyUserAnswerTimeRepository.ExistAsync(x => x.SurveyId == surveyId && x.SurveyUserId == surveyUserId);
        }

        public async Task<SurveyUserAnswerTime> GetInfoBySurveyIdAndSurveyUserId(string surveyId, string surveyUserId, bool isReadOnly = false)
        {
            return await _surveyUserAnswerTimeRepository.GetAsync(isReadOnly, x => x.SurveyId == surveyId && x.SurveyUserId == surveyUserId);
        }

        public async Task<int> ForceDelete(string surveyUserAnswerTimesId)
        {
            SurveyUserAnswerTime surveyuseranswertimesInfo = await GetInfo(surveyUserAnswerTimesId);
            if (surveyuseranswertimesInfo == null)
            {
                return -1;
            }

            _surveyUserAnswerTimeRepository.Delete(surveyuseranswertimesInfo);
            return await Context.SaveChangesAsync();
        }

        public async Task<SurveyUserAnswerTime> GetInfo(string surveyUserAnswertimesId, bool isReadOnly = false)
        {
            return await _surveyUserAnswerTimeRepository.GetAsync(isReadOnly, x => x.Id == surveyUserAnswertimesId);
        }

        public async Task<SurveyUserAnswerTime> GetCurrentActiveSurveyTimes(string surveyId, string surveyUserId, bool isReadOnly = false)
        {
            return await _surveyUserAnswerTimeRepository.GetAsync(isReadOnly, x =>
                x.SurveyId == surveyId && x.SurveyUserId == surveyUserId
                && !x.EndTime.HasValue);
        }

        //public async Task<int> GetSurveyTime(string surveyUserAnswerTimesId)
        //{
        //    var result = await GetInfo(surveyUserAnswerTimesId);
        //    return (DateTime.Now.Hour * 60 * 60 + DateTime.Now.Second) - (result.StartTime.Hour * 60 * 60 + result.StartTime.Second);
        //}

        public async Task<List<string>> GetUserIdsBySurveyId(string surveyId)
        {
            Expression<Func<SurveyUser, bool>> spec = x => x.SurveyId == surveyId;
            Expression<Func<SurveyUserAnswerTime, bool>> specAnswer = x => x.SurveyId == surveyId;

            Task<List<string>> query = Context.Set<SurveyUser>().AsNoTracking().Where(spec)
                .Join(Context.Set<SurveyUserAnswerTime>().Where(specAnswer), surveyUser => surveyUser.Id,
                    surveyUserAnswer => surveyUserAnswer.SurveyUserId,
                    (surveyUser, surveyUserAnswer) => new { surveyUser }).Select(x => x.surveyUser.UserId).ToListAsync();

            return await query;

            //var result= (from user in Context.Set<SurveyUser>().AsNoTracking()
            //             join time in Context.Set<SurveyUserAnswerTime>().AsNoTracking()
            //                on user.Id equals time.SurveyUserId
            //                where user.SurveyId == surveyId
            //             select user.UserId).ToList();
        }

        public async Task<int> SurveyUserAnswerTimesIsPreRendering(string surveyId, string surveyUsersId)
        {
            SqlParameter input_surveyId = new SqlParameter { ParameterName = "@surveyId", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Size = 50, Value = surveyId };
            SqlParameter input_surveyUsersId = new SqlParameter { ParameterName = "@surveyUsersId", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Size = 50, Value = surveyUsersId };
            SqlParameter out_error = new SqlParameter { ParameterName = "@error", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output };

            int result = await Context.Database.ExecuteSqlCommandAsync("EXEC [dbo].[spSurveyUserAnswerTimesIsPreRendering] @SurveyId, @surveyUsersId, @error OUTPUT", input_surveyId, input_surveyUsersId, out_error);
            return result;
        }

        public async Task<int> FinishDoExam(string surveyId, string surveyUserId)
        {
            SurveyUserAnswerTime surveyUserAnswerTimeInfo = await GetInfoBySurveyIdAndSurveyUserId(surveyId, surveyUserId);
            if (surveyUserAnswerTimeInfo == null)
            {
                return -1;
            }

            surveyUserAnswerTimeInfo.EndTime = DateTime.Now;

            int totalSeconds = (int)(surveyUserAnswerTimeInfo.EndTime.Value - surveyUserAnswerTimeInfo.StartTime).TotalSeconds;
            surveyUserAnswerTimeInfo.TotalSeconds = totalSeconds;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> GetTotalSurveyUserAnswerTimes(string surveyId, string surveyUserId)
        {
            return await _surveyUserAnswerTimeRepository.CountAsync(x =>
                x.SurveyId == surveyId && x.SurveyUserId == surveyUserId && x.EndTime.HasValue);
        }

        public async Task<List<SurveyUserAnswerTime>> GetBySurveyIdAndUserId(string surveyId, string surveyUserId)
        {
            return await _surveyUserAnswerTimeRepository.GetsAsync(true,
                x => x.SurveyId == surveyId && x.SurveyUserId == surveyUserId);
        }

        public async Task<List<SurveyUserAnswerTime>> GetByListSurveyUserTimesIds(List<string> surveyUserAnswerTimesIds)
        {
            if (surveyUserAnswerTimesIds == null || !surveyUserAnswerTimesIds.Any())
                return null;

            return await _surveyUserAnswerTimeRepository.GetsAsync(false,
                x => surveyUserAnswerTimesIds.Contains(x.Id));
        }

        public async Task<int> UpdateTotalAnswer(string surveyId, string surveyUserId, string surveyUserAnswerTimesId)
        {
            SqlParameter inputSurveyId = new SqlParameter { ParameterName = "@SurveyId", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Size = 50, Value = surveyId };
            SqlParameter inputSurveyUsersId = new SqlParameter { ParameterName = "@SurveyUserId", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Size = 50, Value = surveyUserId };
            SqlParameter inputSurveyUserAnswerTimesId = new SqlParameter { ParameterName = "@SurveyUserAnswerTimesId", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Size = 50, Value = surveyUserAnswerTimesId };

            int result = await Context.Database.ExecuteSqlCommandAsync("EXEC [dbo].[sp_UpdateTotalAnswer] @SurveyId, @SurveyUserId,@SurveyUserAnswerTimesId ", inputSurveyId, inputSurveyUsersId, inputSurveyUserAnswerTimesId);
            return result;
        }

        public async Task<List<SurveyUserAnswerTimeViewModel>> GetListSurveyUserAnswerTimes(string surveyId, string surveyUserId)
        {
            return await _surveyUserAnswerTimeRepository.GetsAsAsync(x => new SurveyUserAnswerTimeViewModel
            {
                Id = x.Id,
                StartTime = x.StartTime,
                EndTime = x.EndTime,
                TotalSeconds = x.TotalSeconds,
                TotalScores = x.TotalScores,
                IsViewResult = x.IsViewResult,
                TotalCorrectAnswers = x.TotalCorrectAnswers,
                TotalCorrectScores = x.TotalCorrectScores,
                TotalQuestions = x.TotalQuestions,
                TotalUserAnswers = x.TotalUserAnswers
            }, x => x.SurveyId == surveyId && x.SurveyUserId == surveyUserId && x.EndTime.HasValue);
        }

        public async Task<List<ReportSurveyUserAnswerTimesViewModel>> SearchReportBySurveyIdAndSurveyUserId(string surveyId, string surveyUserId)
        {
            IQueryable<ReportSurveyUserAnswerTimesViewModel> query = Context.Set<SurveyUser>().Where(x => x.SurveyId == surveyId && x.Id == surveyUserId)
                .Join(
                    Context.Set<SurveyUserAnswerTime>()
                        .Where(x => x.SurveyId == surveyId && x.SurveyUserId == surveyUserId), su => su.Id,
                    suat => suat.SurveyUserId, (su, suat) => new ReportSurveyUserAnswerTimesViewModel
                    {
                        SurveyUserId = su.Id,
                        UserId = su.UserId,
                        Avatar = su.Avatar,
                        FullName = su.FullName,
                        OfficeName = su.OfficeName,
                        PositionName = su.PositionName,
                        StartTime = suat.StartTime,
                        EndTime = suat.EndTime,
                        TotalScores = suat.TotalScores,
                        TotalCorrectAnswers = suat.TotalCorrectAnswers,
                        TotalCorrectScores = suat.TotalCorrectScores,
                        TotalQuestions = suat.TotalQuestions,
                        TotalSecond = suat.TotalSeconds ?? 0
                    });

            return await query
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<int> Update(SurveyUserAnswerTime surveyUserAnswerTime)
        {
            return await Context.SaveChangesAsync();
        }
    }
}
