﻿using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using GHM.Survey.Domain.Constants;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GHM.Survey.Infrastructure.Repository
{
    public class SurveyUserAnswerRepository : RepositoryBase, ISurveyUserAnswerRepository
    {
        private readonly IRepository<SurveyUserAnswer> _surveyUserAnswerRepository;

        public SurveyUserAnswerRepository(IDbContext context) : base(context)
        {
            _surveyUserAnswerRepository = Context.GetRepository<SurveyUserAnswer>();
        }

        public async Task<int> ForceDelete(string surveyUserAnswersId)
        {
            var surveyuseranswerInfo = await GetInfo(surveyUserAnswersId);
            if (surveyuseranswerInfo == null)
                return -1;
            _surveyUserAnswerRepository.Delete(surveyuseranswerInfo);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string surveyId, string questionVersionId, string surveyUserId, string answerId, string surveryUserAnswerTimeId)
        {
            var surveyUserAnswer = await _surveyUserAnswerRepository.GetAsync(false, x =>
                x.SurveyId == surveyId && x.QuestionVersionId == questionVersionId && x.SurveyUserId == surveyUserId
                && x.AnswerId == answerId && x.SurveyUserAnswerTimesId == surveryUserAnswerTimeId);
            if (surveyUserAnswer == null)
                return -1;

            _surveyUserAnswerRepository.Delete(surveyUserAnswer);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string surveyId, string questionVersionId, string surveyUserId, string surveryUserAnswerTimeId)
        {
            var surveyUserAnswer = await _surveyUserAnswerRepository.GetAsync(false, x =>
                x.SurveyId == surveyId && x.QuestionVersionId == questionVersionId && x.SurveyUserId == surveyUserId && x.SurveyUserAnswerTimesId == surveryUserAnswerTimeId);
            if (surveyUserAnswer == null)
                return -1;

            _surveyUserAnswerRepository.Delete(surveyUserAnswer);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDeletes(string surveyUserId, string surveyUserAnswerTimesId)
        {
            var result = await GetInfos(surveyUserAnswerTimesId, surveyUserId);
            if (result == null)
                return -1;
            _surveyUserAnswerRepository.Deletes(result);
            return await Context.SaveChangesAsync();
        }

        public async Task<SurveyUserAnswer> GetInfo(string surveyUserAnswersId, bool isReadOnly = false)
        {
            return await _surveyUserAnswerRepository.GetAsync(isReadOnly, x => x.Id == surveyUserAnswersId);
        }

        public async Task<SurveyUserAnswer> GetInfo(string surveyId, string questionVersionId, string surveyUserId, string surveryUserAnswerTimeId)
        {
            return await _surveyUserAnswerRepository.GetAsync(true, x =>
                x.SurveyId == surveyId && x.QuestionVersionId == questionVersionId
                && x.SurveyUserId == surveyUserId && x.SurveyUserAnswerTimesId == surveryUserAnswerTimeId);
        }

        public async Task<List<SurveyUserAnswer>> GetInfos(string surveyUserAnswerTimesId, string surveyUserId, 
            bool isReadOnly = false)
        {
            return await _surveyUserAnswerRepository.GetsAsync(isReadOnly, x => x.SurveyUserId == surveyUserId && x.SurveyUserAnswerTimesId == surveyUserAnswerTimesId);
        }

        public async Task<int> GetTotalAnswer(string surveyId, string questionVersionId, string surveyUserId, string surveryUserAnswerTimeId, bool? isCorrect)
        {
            Expression<Func<SurveyUserAnswer, bool>> spec = x =>
                x.SurveyId == surveyId && x.QuestionVersionId == questionVersionId
                && x.SurveyUserId == surveyUserId && x.SurveyUserAnswerTimesId == surveryUserAnswerTimeId;

            if (isCorrect.HasValue)
            {
                spec = spec.And(x => x.IsCorrect == isCorrect);
            }

            return await _surveyUserAnswerRepository.CountAsync(spec);
        }

        public async Task<bool> CheckExists(string surveyId, string questionVersionId, string surveyUserId, string surveryUserAnswerTimeId)
        {
            return await _surveyUserAnswerRepository.ExistAsync(x =>
                x.SurveyId == surveyId && x.QuestionVersionId == questionVersionId
                && x.SurveyUserId == surveyUserId && x.SurveyUserAnswerTimesId == surveryUserAnswerTimeId);
        }

        public async Task<bool> CheckExists(string surveyId, string questionVersionId, string surveyUserId,
            string answerId, string surveryUserAnswerTimeId)
        {
            return await _surveyUserAnswerRepository.ExistAsync(x =>
                x.SurveyId == surveyId && x.SurveyUserId == surveyUserId
                && x.QuestionVersionId == questionVersionId &&
                x.AnswerId == answerId && x.SurveyUserAnswerTimesId == surveryUserAnswerTimeId);
        }

        public async Task<int> Insert(SurveyUserAnswer surveyUserAnswer)
        {
            _surveyUserAnswerRepository.Create(surveyUserAnswer);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(SurveyUserAnswer surveyuseranswer)
        {
            return await Context.SaveChangesAsync();
        }

        /// <summary>
        /// Danh sách câu hỏi và các phương án trả lời cho người tham gia
        /// </summary>
        /// <param name="tenantId"></param>
        /// <param name="surveyId"></param>
        /// <param name="surveyUserId"></param>
        /// <param name="languageId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>       
        public Task<List<QuestionForUser>> GetQuestionForUserInSurvey(string tenantId, string surveyId, string surveyUserId,
                string languageId, int page, int pageSize, out int totalRows)
        {
            Expression<Func<SurveyQuestionUser, bool>> spec = x => x.SurveyId == surveyId;
            if (!string.IsNullOrEmpty(surveyUserId))
            {
                spec = spec.And(x => x.SurveyUserId == surveyUserId);
            }
            var queryQuestion = Context.Set<SurveyQuestionUser>().Where(spec)
                        .Join(Context.Set<Question>().Where(x => x.TenantId == tenantId), sq => sq.QuestionVersionId, q => q.VersionId, (sq, q)
                         => new
                         {
                             q.VersionId,
                             q.Type,
                             q.TotalAnswer,
                             Score = q.Point,
                         }).Join(Context.Set<QuestionTranslation>().Where(x => x.LanguageId == languageId && x.TenantId == tenantId),
                        q => q.VersionId, qs => qs.QuestionVersionId, (q, qs) => new
                        {
                            q.VersionId,
                            q.Type,
                            q.TotalAnswer,
                            q.Score,
                            qs.Name,
                            qs.Content,
                            qs.Explain,
                        });

            totalRows = queryQuestion.Count();
            queryQuestion.OrderBy(x => x.Name).Skip((page - 1) * pageSize).Take(pageSize);

            var listQuestionHasAnwser = queryQuestion.Where(x => x.Type == QuestionType.SingleChoice
                                        || x.Type == QuestionType.MultiChoice || x.Type == QuestionType.Rating);
            var listQuestionNoAnswer = queryQuestion.Where(x => x.Type == QuestionType.Essay
                                        || x.Type == QuestionType.Essay);

            var lisAnswer = listQuestionHasAnwser.Join(Context.Set<Answer>(), q => q.VersionId, a => a.QuestionVersionId, (q, a) => new { q, a })
                            .Join(Context.Set<AnswerTranslation>().Where(x => x.LanguageId == languageId), qa => qa.a.Id, at => at.AnswerId,
                            (qa, at) => new
                            {
                                qa.q.VersionId,
                                qa.q.Type,
                                qa.q.Name,
                                qa.q.Content,
                                qa.q.Explain,
                                qa.q.Score,
                                qa.q.TotalAnswer,
                                AnswerId = qa.a.Id,
                                AnswerName = at.Name,
                                AnswerExplain = at.Explain,
                                AnswerOrder = qa.a.Order,
                            });

            var listResultHasAnswer = lisAnswer.GroupJoin(Context.Set<SurveyUserAnswer>()
                            .Where(x => x.SurveyUserId == surveyUserId && x.SurveyId == surveyId),
                            a => a.AnswerId, sua => sua.AnswerId, (a, sua) => new { a, sua })
                            .SelectMany(x => x.sua.DefaultIfEmpty(), (x, sua) => new QuestionForUser
                            {
                                VersionId = x.a.VersionId,
                                QuestionType = x.a.Type,
                                Name = x.a.Name,
                                Content = x.a.Content,
                                Explain = x.a.Explain,
                                Point = x.a.Score,
                                TotalAnswer = x.a.TotalAnswer,
                                AnswerId = x.a.AnswerId,
                                AnswerName = x.a.AnswerName,
                                AnswerExplain = x.a.AnswerExplain,
                                AnswerOrder = x.a.AnswerOrder,
                                SurveyUserAnswerId = sua.Id,
                                AnswerValue = sua.Value,
                            });

            var listResultNoAnswer = listQuestionNoAnswer.GroupJoin(Context.Set<SurveyUserAnswer>()
                            .Where(x => x.SurveyUserId == surveyUserId && x.SurveyId == surveyId),
                            a => a.VersionId, sua => sua.QuestionVersionId, (a, sua) => new { a, sua })
                            .SelectMany(x => x.sua.DefaultIfEmpty(), (x, sua) => new QuestionForUser
                            {
                                VersionId = x.a.VersionId,
                                QuestionType = x.a.Type,
                                Name = x.a.Name,
                                Content = x.a.Content,
                                Explain = x.a.Explain,
                                Point = x.a.Score,
                                TotalAnswer = x.a.TotalAnswer,
                                SurveyUserAnswerId = sua.Id,
                                AnswerValue = sua.Value,
                            });

            var listResult = listResultHasAnswer.Union(listResultNoAnswer);
            return listResult.AsNoTracking().ToListAsync();
        }
    }
}
