﻿using GHM.Infrastructure.SqlServer;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.Models;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System;

namespace GHM.Survey.Infrastructure.Repository
{
    public class SurveyQuestionUserRepository : RepositoryBase, ISurveyQuestionUserRepository
    {
        private readonly IRepository<SurveyQuestionUser> _surveyQuestionUserRepository;

        public SurveyQuestionUserRepository(IDbContext context) : base(context)
        {
            _surveyQuestionUserRepository = Context.GetRepository<SurveyQuestionUser>();
        }

        public async Task<bool> CheckByQuestionVersionId(string questionVersionId)
        {
            return await _surveyQuestionUserRepository.ExistAsync(x => x.QuestionVersionId == questionVersionId);
        }

        public async Task<bool> CheckBySurveyUserIdAndsurveyId(string surveyUserId, string surveyId)
        {
            return await _surveyQuestionUserRepository.ExistAsync(x => x.SurveyUserId == surveyUserId && x.SurveyId == surveyId);
        }

        public async Task<List<SurveyQuestionUser>> GetInfos(string surveyUserId, string surveyId, bool isReadOnly = false)
        {
            return await _surveyQuestionUserRepository.GetsAsync(isReadOnly, x => x.SurveyUserId == surveyUserId && x.SurveyId == surveyId);
        }

        public async Task<int> ForceDelete(string surveyUserId, string surveyId)
        {
            var infors = await GetInfos(surveyUserId, surveyId);
            if (infors == null)
                return -1;

            _surveyQuestionUserRepository.Deletes(infors);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> RanDomQuestion(string surveyId, string userId, string surveyUserAnswerTimesId)
        {
            var input_surveyId = new SqlParameter { ParameterName = "@SurveyId", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Size = 50, Value = surveyId };
            var input_userId = new SqlParameter { ParameterName = "@UserId", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Size = 50, Value = userId };
            var input_surveyUserAnswerTimesId = new SqlParameter
            {
                ParameterName = "@SurveyUserAnswerTimesId",
                SqlDbType = SqlDbType.VarChar,
                Direction = ParameterDirection.Input,
                Size = 50,
                Value = surveyUserAnswerTimesId
            };
            var out_error = new SqlParameter { ParameterName = "@error", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output };

            var result = await Context.Database.ExecuteSqlCommandAsync("EXEC [dbo].[spRanDomQuestion] @SurveyId, @UserId, @SurveyUserAnswerTimesId, @error OUTPUT",
                                    input_surveyId, input_userId, input_surveyUserAnswerTimesId, out_error);
            return (int)out_error.Value;
        }

        public async Task<SurveyQuestionUser> GetInfo(string surveyUserId, string surveyId, string surveyUserAnswerTimesId, string questionVersionId, bool isReadOnly = false)
        {
            return await _surveyQuestionUserRepository.GetAsync(isReadOnly, x => x.SurveyUserId == surveyUserId && x.SurveyId == surveyId
            && x.QuestionVersionId == questionVersionId && x.SurveyUserAnswerTimesId == surveyUserAnswerTimesId);
        }

        public async Task<int> UpdateIsCorrect(string surveyUserId, string surveyId, string surveyUserAnswerTimesId, string questionVersionId, bool isCorrect)
        {
            var info = await GetInfo(surveyUserId, surveyId, surveyUserAnswerTimesId, questionVersionId);
            if (info == null)
                return -1;

            info.IsCorrect = isCorrect;
            return await Context.SaveChangesAsync();
        }
    }
}
