﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Infrastructure.Constants;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.IServices;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.Resources;
using GHM.Survey.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;

namespace GHM.Survey.Infrastructure.Services
{
    public class SurveyGroupService : ISurveyGroupService
    {
        private readonly ISurveyGroupRepository _surveyGroupRepository;
        private readonly ISurveyGroupTranslationRepository _surveyGroupTranslationRepository;
        private readonly ISurveyRepository _surveyRepository;
        private readonly ISurveyTranslationRepository _surveyTranslationRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmSurveyResource> _surveyResourceResource;

        public SurveyGroupService(ISurveyGroupRepository surveyGroupRepository,
            ISurveyGroupTranslationRepository surveyGroupTranslationRepository,
            ISurveyRepository surveyRepository,
            ISurveyTranslationRepository surveyTranslationRepository,
            IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmSurveyResource> surveyResourceResource)
        {
            _surveyGroupRepository = surveyGroupRepository;
            _surveyGroupTranslationRepository = surveyGroupTranslationRepository;
            _surveyRepository = surveyRepository;
            _surveyTranslationRepository = surveyTranslationRepository;
            _sharedResourceService = sharedResourceService;
            _surveyResourceResource = surveyResourceResource;
        }

        public async Task<List<SurveyGroupSearchViewModel>> GetsAll(string tenantId, string languageId)
        {
            return await _surveyGroupRepository.GetAllActivatedSurveyGroup(tenantId, languageId);
        }

        public async Task<SearchResult<SurveyGroupSearchViewModel>> Search(string tenantId, string languageId, string keyword,
            bool? isActive, int page, int pageSize)
        {
            var items = await _surveyGroupRepository.Search(tenantId, languageId, keyword, isActive, page, pageSize,
               out var totalRows);
            return new SearchResult<SurveyGroupSearchViewModel>
            {
                TotalRows = totalRows,
                Items = items
            };
        }

        public async Task<SearchResult<SurveyGroupSearchViewModel>> SearchTree(string tenantId, string languageId, string keyword,
            bool? isActive, int page, int pageSize)
        {
            var items = await _surveyGroupRepository.Search(tenantId, languageId, keyword, isActive, page, pageSize,
              out var totalRows);
            return new SearchResult<SurveyGroupSearchViewModel>
            {
                TotalRows = totalRows,
                Items = items
            };
        }

        public async Task<List<TreeData>> GetFullSurveyGroupTree(string tenantId, string languageId)
        {
            var infoSurveyGroups = await _surveyGroupRepository.GetAllActivatedSurveyGroup(tenantId, languageId);
            if (infoSurveyGroups == null || !infoSurveyGroups.Any())
                return null;

            return RenderTree(infoSurveyGroups, null);

            List<TreeData> RenderTree(List<SurveyGroupSearchViewModel> surveyGroups, int? parentId)
            {
                var tree = new List<TreeData>();
                var parentSurveyGroups = surveyGroups.Where(x => x.ParentId == parentId).ToList();
                if (parentSurveyGroups.Any())
                {
                    parentSurveyGroups.ForEach(surveyGroup =>
                    {
                        var treeData = new TreeData
                        {
                            Id = surveyGroup.Id,
                            Text = surveyGroup.Name,
                            ParentId = surveyGroup.ParentId,
                            IdPath = surveyGroup.IdPath,
                            Data = surveyGroup,
                            ChildCount = surveyGroup.ChildCount,
                            Icon = string.Empty,
                            State = new GHM.Infrastructure.Models.State(),
                            Children = RenderTree(surveyGroups, surveyGroup.Id)
                        };
                        tree.Add(treeData);
                    });
                }
                return tree;
            }

        }

        public async Task<ActionResultResponse<int>> Insert(string tenantId, string creatorId, string creatorFullName,
            SurveyGroupMeta surveyGroupMeta)
        {
            var surveyGroup = new SurveyGroup
            {
                IdPath = "-1",
                IsActive = surveyGroupMeta.IsActive,
                Order = surveyGroupMeta.Order,
                TenantId = tenantId,
                OrderPath = surveyGroupMeta.Order.ToString(),
                CreatorId = creatorId,
                CreatorFullName = creatorFullName
            };

            if (surveyGroupMeta.ParentId.HasValue)
            {
                var parentInfo = await _surveyGroupRepository.GetInfo(surveyGroupMeta.ParentId.Value);
                if (parentInfo == null)
                    return new ActionResultResponse<int>(-2,
                        _surveyResourceResource.GetString("Parent survey group does not exists. Please try again."));

                surveyGroup.ParentId = parentInfo.Id;
                surveyGroup.IdPath = $"{parentInfo.IdPath}.-1";
            }

            var result = await _surveyGroupRepository.Insert(surveyGroup);
            if (result <= 0)
                return new ActionResultResponse<int>(result, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            #region Update current survey group idPath.
            surveyGroup.IdPath = surveyGroup.IdPath.Replace("-1", surveyGroup.Id.ToString());
            await _surveyGroupRepository.UpdateSurveyGroupIdPath(surveyGroup.Id, surveyGroup.IdPath);
            #endregion

            #region Update parent survey group child count.
            if (surveyGroup.ParentId.HasValue)
            {
                var childCount = await _surveyGroupRepository.GetChildCount(surveyGroup.ParentId.Value);
                await _surveyGroupRepository.UpdateChildCount(surveyGroup.ParentId.Value, childCount);
            }
            #endregion

            #region Insert survey group translation.
            var surveyGroupTranslations = new List<SurveyGroupTranslation>();
            foreach (var surveyGroupTranslationMeta in surveyGroupMeta.SurveyGroupTranslations)
            {
                // check name exists.
                var isNameExists = await _surveyGroupTranslationRepository.CheckExistsByName(surveyGroup.Id, surveyGroup.TenantId
                   , surveyGroupTranslationMeta.LanguageId, surveyGroupTranslationMeta.Name);
                if (isNameExists)
                {
                    await RollbackInsert();
                    return new ActionResultResponse<int>(-3,
                        _surveyResourceResource.GetString("Survey group name: \"{0}\" already taken by another survey group. Please try again.",
                        surveyGroupTranslationMeta.Name));
                }
                var surveyGroupTranslation = new SurveyGroupTranslation
                {
                    Name = surveyGroupTranslationMeta.Name.Trim(),
                    Description = surveyGroupTranslationMeta.Description?.Trim(),
                    UnsignName = surveyGroupTranslationMeta.Name.Trim().StripVietnameseChars().ToUpper(),
                    LanguageId = surveyGroupTranslationMeta.LanguageId,
                    SurveyGroupId = surveyGroup.Id,
                    TenantId = surveyGroup.TenantId,
                };
                if (surveyGroupMeta.ParentId.HasValue)
                {
                    var parentName = await _surveyGroupTranslationRepository.GetSurveyGroupName(tenantId, surveyGroupMeta.ParentId.Value,
                        surveyGroupTranslationMeta.LanguageId);
                    if (!string.IsNullOrEmpty(parentName))
                    {
                        surveyGroupTranslation.ParentName = parentName;
                    }
                }
                surveyGroupTranslations.Add(surveyGroupTranslation);
            }

            var resultInsertTranslation = await _surveyGroupTranslationRepository.Inserts(surveyGroupTranslations);
            if (resultInsertTranslation <= 0)
            {
                await RollbackInsert();
                return new ActionResultResponse<int>(resultInsertTranslation,
                    _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
            }
            #endregion

            return new ActionResultResponse<int>(result, _surveyResourceResource.GetString("Add new survey group successful."),
                string.Empty, surveyGroup.Id);

            async Task RollbackInsert()
            {
                await _surveyGroupRepository.ForceDelete(surveyGroup.Id);
            }
        }

        public async Task<ActionResultResponse> Update(string tenantId, string lastUpdateUserId, string lastUpdateFullName,
            int surveyGroupId, SurveyGroupMeta surveyGroupMeta)
        {
            var surveyGroupInfo = await _surveyGroupRepository.GetInfo(surveyGroupId);
            if (surveyGroupInfo == null)
                return new ActionResultResponse(-2, _surveyResourceResource.GetString("Survey group info does not exists. Please try again."));

            if (surveyGroupInfo.TenantId != tenantId)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (surveyGroupInfo.ConcurrencyStamp != surveyGroupMeta.ConcurrencyStamp)
                return new ActionResultResponse(-4,
                    _sharedResourceService.GetString("The survey group already updated by other people. You can not update this survey group."));

            if (surveyGroupMeta.ParentId.HasValue && surveyGroupInfo.Id == surveyGroupMeta.ParentId.Value)
                return new ActionResultResponse(-5, _surveyResourceResource.GetString("Survey group and parent survey group can not be the same.")); ;

            var oldParentId = surveyGroupInfo.ParentId;
            var oldIdPath = surveyGroupInfo.IdPath;

            surveyGroupInfo.IsActive = surveyGroupMeta.IsActive;
            surveyGroupInfo.Order = surveyGroupMeta.Order;
            surveyGroupInfo.ConcurrencyStamp = Guid.NewGuid().ToString();
            surveyGroupInfo.LastUpdate = DateTime.Now;
            surveyGroupInfo.LastUpdateUserId = lastUpdateUserId;
            surveyGroupInfo.LastUpdateFullName = lastUpdateFullName;

            if (surveyGroupInfo.ParentId.HasValue && !surveyGroupMeta.ParentId.HasValue)
            {
                surveyGroupInfo.ParentId = null;
                surveyGroupInfo.IdPath = surveyGroupInfo.Id.ToString();
                surveyGroupInfo.OrderPath = surveyGroupInfo.Order.ToString();
            }
            else if (surveyGroupMeta.ParentId.HasValue && surveyGroupMeta.ParentId != surveyGroupInfo.ParentId)
            {
                var parentInfo = await _surveyGroupRepository.GetInfo(surveyGroupMeta.ParentId.Value);
                if (parentInfo == null)
                    return new ActionResultResponse(-6, _surveyResourceResource.GetString("Parent survey group does not exists. Please try again.")); ;

                surveyGroupInfo.IdPath = $"{parentInfo.IdPath}.{surveyGroupInfo.Id}";
                surveyGroupInfo.ParentId = parentInfo.Id;
                surveyGroupInfo.OrderPath = $"{parentInfo.OrderPath}.{surveyGroupInfo.Order}";
            }

            await _surveyGroupRepository.Update(surveyGroupInfo);

            // Update children IdPath and RootInfo
            if (surveyGroupInfo.IdPath != oldIdPath)
            {
                await _surveyGroupRepository.UpdateChildrenIdPath(oldIdPath, surveyGroupInfo.IdPath);
            }

            // Update parent survey group child count.
            if (surveyGroupInfo.ParentId.HasValue && oldParentId.HasValue && surveyGroupInfo.ParentId.Value != oldParentId.Value)
            {
                // Update old parent survey group child count.
                var oldChildCount = await _surveyGroupRepository.GetChildCount(oldParentId.Value);
                await _surveyGroupRepository.UpdateChildCount(oldParentId.Value, oldChildCount);

                // Update new parent survey group child count.
                var newParentId = surveyGroupInfo.ParentId.Value;
                var newChildCount = await _surveyGroupRepository.GetChildCount(newParentId);
                await _surveyGroupRepository.UpdateChildCount(newParentId, newChildCount);
            }

            //update lai survey group child by Id
            var childCountid = await _surveyGroupRepository.GetChildCount(surveyGroupInfo.Id);
            await _surveyGroupRepository.UpdateChildCount(surveyGroupInfo.Id, childCountid);

            // Update survey group name translation in survey translation.
            if (surveyGroupMeta.SurveyGroupTranslations.Any())
                await UpdateSurveyTranslations();

            // Update survey group translation.
            var resultUpdateTranslation = await UpdateSurveyGroupTranslation();
            return new ActionResultResponse(resultUpdateTranslation.Code <= 0
                ? resultUpdateTranslation.Code
                : 1,
                resultUpdateTranslation.Code <= 0
                ? resultUpdateTranslation.Message
                : _surveyResourceResource.GetString("Update survey group successful."));

            async Task<ActionResultResponse> UpdateSurveyGroupTranslation()
            {
                foreach (var surveyGroupTranslationMeta in surveyGroupMeta.SurveyGroupTranslations)
                {
                    // check name exists.
                    var isNameExists = await _surveyGroupTranslationRepository.CheckExistsByName(surveyGroupInfo.Id, surveyGroupInfo.TenantId,
                        surveyGroupTranslationMeta.LanguageId, surveyGroupTranslationMeta.Name);
                    if (isNameExists)
                    {
                        return new ActionResultResponse(-6,
                            _surveyResourceResource.GetString("Survey group name: \"{0}\" already taken by another survey group. Please try again.",
                            surveyGroupTranslationMeta.Name));
                    }

                    var surveyGroupTranslationInfo =
                        await _surveyGroupTranslationRepository.GetInfo(tenantId, surveyGroupInfo.Id, surveyGroupTranslationMeta.LanguageId);
                    if (surveyGroupTranslationInfo == null)
                    {
                        surveyGroupTranslationInfo = new SurveyGroupTranslation
                        {
                            Name = surveyGroupTranslationMeta.Name.Trim(),
                            Description = surveyGroupTranslationMeta.Description?.Trim(),
                            UnsignName = surveyGroupTranslationMeta.Name.Trim().StripVietnameseChars().ToUpper(),
                            LanguageId = surveyGroupTranslationMeta.LanguageId,
                            SurveyGroupId = surveyGroupInfo.Id,

                        };

                        if (surveyGroupMeta.ParentId.HasValue)
                        {
                            var parentName = await _surveyGroupTranslationRepository.GetSurveyGroupName(tenantId, surveyGroupMeta.ParentId.Value,
                                surveyGroupTranslationMeta.LanguageId);
                            if (!string.IsNullOrEmpty(parentName))
                            {
                                surveyGroupTranslationInfo.ParentName = parentName;
                            }
                        }

                        await _surveyGroupTranslationRepository.Insert(surveyGroupTranslationInfo);
                    }
                    else
                    {
                        surveyGroupTranslationInfo.Name = surveyGroupTranslationMeta.Name.Trim();
                        surveyGroupTranslationInfo.Description = surveyGroupTranslationMeta.Description?.Trim();
                        surveyGroupTranslationInfo.UnsignName = surveyGroupTranslationMeta.Name.StripVietnameseChars().ToUpper();

                        if (surveyGroupMeta.ParentId.HasValue)
                        {
                            var parentName = await _surveyGroupTranslationRepository.GetSurveyGroupName(tenantId, surveyGroupMeta.ParentId.Value,
                                surveyGroupTranslationMeta.LanguageId);
                            if (!string.IsNullOrEmpty(parentName))
                            {
                                surveyGroupTranslationInfo.ParentName = parentName;
                            }
                        }

                        await _surveyGroupTranslationRepository.Update(surveyGroupTranslationInfo);
                    }
                }

                return new ActionResultResponse(1,
                    _surveyResourceResource.GetString("Update survey group successful."));
            }

            async Task<ActionResultResponse> UpdateSurveyTranslations()
            {
                foreach (var surveyTranslation in surveyGroupMeta.SurveyGroupTranslations)
                {
                    await _surveyTranslationRepository.UpdateSurveyGroupName(tenantId,
                        surveyTranslation.LanguageId, surveyGroupId, surveyTranslation.Name);
                }

                return new ActionResultResponse(1,
                    _surveyResourceResource.GetString("Update survey group name successful."));
            }
        }

        public async Task<ActionResultResponse> Delete(string tenantId, int id)
        {
            var surveyGroupInfo = await _surveyGroupRepository.GetInfo(id);
            if (surveyGroupInfo == null)
                return new ActionResultResponse(-1, _surveyResourceResource.GetString("Survey group does not exists. Please try again."));

            if (surveyGroupInfo.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            // Check is has child.
            var surveyGroupChildCount = await _surveyGroupRepository.GetChildCount(surveyGroupInfo.Id);
            if (surveyGroupChildCount > 0)
                return new ActionResultResponse(-3,
                    _surveyResourceResource.GetString("This survey group has children survey group. You can not delete this survey group."));

            var isUsedInSurvey = await _surveyRepository.CheckExistsBySurveyGroupId(surveyGroupInfo.Id);
            if (isUsedInSurvey)
                return new ActionResultResponse(-3,
                    _surveyResourceResource.GetString("This survey group used by survey. You can not delete this survey group."));

            var result = await _surveyGroupRepository.Delete(surveyGroupInfo.Id);
            if (result > 0)
            {
                //Update parent survey group child count.
                if (surveyGroupInfo.ParentId.HasValue)
                {
                    var childCount = await _surveyGroupRepository.GetChildCount(surveyGroupInfo.ParentId.Value);
                    await _surveyGroupRepository.UpdateChildCount(surveyGroupInfo.ParentId.Value, childCount);
                }

                await _surveyGroupTranslationRepository.ForceDeleteBySurveyGroupId(tenantId, id);
                return new ActionResultResponse(result, _surveyResourceResource.GetString("Delete survey group successful."));
            }

            return new ActionResultResponse(result, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
        }

        public async Task<ActionResultResponse<SurveyGroupDetailViewModel>> GetDetail(string tenantId, string languageId, int id)
        {
            var surveyGroupInfo = await _surveyGroupRepository.GetInfo(id, true);
            if (surveyGroupInfo == null)
                return new ActionResultResponse<SurveyGroupDetailViewModel>(-1,
                    _surveyResourceResource.GetString("Survey group does not exists."));

            if (surveyGroupInfo.TenantId != tenantId)
                return new ActionResultResponse<SurveyGroupDetailViewModel>(-2,
                    _surveyResourceResource.GetString(ErrorMessage.SomethingWentWrong));

            var surveyGroupDetail = new SurveyGroupDetailViewModel
            {
                IsActive = surveyGroupInfo.IsActive,
                ParentId = surveyGroupInfo.ParentId,
                Order = surveyGroupInfo.Order,
                ConcurrencyStamp = surveyGroupInfo.ConcurrencyStamp,
                ChildCount = surveyGroupInfo.ChildCount,
                SurveyGroupTranslations = await _surveyGroupTranslationRepository.GetsBySurveyGroupId(tenantId, surveyGroupInfo.Id)
            };

            return new ActionResultResponse<SurveyGroupDetailViewModel>
            {
                Code = 1,
                Data = surveyGroupDetail
            };
        }

        public async Task<List<SurveyGroupForSelectViewModel>> GetSurveyGroupForSelect(string tenantId, string languageId,
            string keyword, int page, int pageSize)
        {
            return await _surveyGroupRepository.GetAllSurveyGroupForSelect(tenantId, languageId, keyword, page, pageSize, out var totalRows);
        }

    }
}

