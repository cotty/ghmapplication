﻿using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.IServices;
using GHM.Survey.Domain.Resources;
using System.Threading.Tasks;

namespace GHM.Survey.Infrastructure.Services
{
    public class SurveyQuestionUserService : ISurveyQuestionUserService
    {
        private readonly ISurveyQuestionUserRepository _surveyquestionuserRepository;

        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmSurveyResource> _surveyResourceService;

        public SurveyQuestionUserService(ISurveyQuestionUserRepository surveyquestionuserRepository, 
            IResourceService<SharedResource> sharedResourceService, IResourceService<GhmSurveyResource> surveyResourceService)
        {
            _surveyquestionuserRepository = surveyquestionuserRepository;
            _sharedResourceService = sharedResourceService;
            _surveyResourceService = surveyResourceService;
        }
      
        /// <summary>
        /// Sinh danh sách câu hỏi khảo sát
        /// </summary>
        /// <param name="surveyId"></param>
        /// <param name="surveyUserId"></param>
        /// <returns></returns>
        public async  Task<int> RanDomQuestionAsync(string surveyId, string surveyUserId, string surveyUserAnswerTimesId)
        {
            return  await  _surveyquestionuserRepository.RanDomQuestion(surveyId, surveyUserId, surveyUserAnswerTimesId);
        }
    }
}
