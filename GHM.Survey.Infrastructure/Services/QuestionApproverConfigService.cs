﻿using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.Services;
using GHM.Infrastructure.ViewModels;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.IServices;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.Resources;
using GHM.Survey.Domain.ViewModels;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using GHM.Infrastructure.Constants;
using ShortUserInfoViewModel = GHM.Survey.Domain.ViewModels.ShortUserInfoViewModel;

namespace GHM.Survey.Infrastructure.Services
{
    public class QuestionApproverConfigService : IQuestionApproverConfigService
    {
        private readonly IQuestionApproverConfigRepository _questionApproverConfigRepository;

        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmSurveyResource> _surveyResourceService;

        private readonly IConfiguration _configuration;

        public QuestionApproverConfigService(IQuestionApproverConfigRepository questionApproverConfigRepository,
            IResourceService<SharedResource> sharedResourceService, IResourceService<GhmSurveyResource> surveyResourceService,
            IConfiguration configuration)
        {
            _questionApproverConfigRepository = questionApproverConfigRepository;
            _sharedResourceService = sharedResourceService;
            _surveyResourceService = surveyResourceService;
            _configuration = configuration;
        }

        public async Task<ActionResultResponse> Delete(string tenantId, string userId)
        {
            var questionApproverConfigInfo = await _questionApproverConfigRepository.GetInfo(tenantId, userId);
            if (questionApproverConfigInfo == null)
                return new ActionResultResponse(-1, _sharedResourceService.GetString("User Approve does not exists."));

            if (questionApproverConfigInfo.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var result = await _questionApproverConfigRepository.ForceDelete(tenantId, userId);
            return new ActionResultResponse(result, result <= 0
                ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                    : _surveyResourceService.GetString("Delete Question successful."));
        }

        public async Task<ActionResultResponse> Insert(string tenantId, string userId)
        {
            var isQuestionApproveConfigExists = await _questionApproverConfigRepository.CheckExistsUserId(tenantId, userId);
            if (isQuestionApproveConfigExists)
                return new ActionResultResponse(-5,
                    _surveyResourceService.GetString("Approver already exists."));

            var userInfo = await new HttpClientService().GetUserInfo(tenantId, userId);
            if (userInfo == null)
                return new ActionResultResponse(-5,
                    _surveyResourceService.GetString("Người dùng không tồn tại."));

            var result = await _questionApproverConfigRepository.Insert(new QuestionApproverConfig
            {
                TenantId = tenantId,
                UserId = userInfo.UserId,
                PositionId = userInfo.PositionId,
                OfficeId = userInfo.OfficeId,
                FullName = userInfo.FullName,
                UnsignName = userInfo.FullName.Trim().StripVietnameseChars().ToUpper(),
                Avatar = userInfo.Avatar,
                OfficeName = userInfo.OfficeName,
                PositionName = userInfo.PositionName,
            });
            if (result <= 0)
            {
                await RollbackInsert();
                return new ActionResultResponse(-5,
                   _surveyResourceService.GetString(ErrorMessage.SomethingWentWrong));
            }

            return new ActionResultResponse(result, _surveyResourceService.GetString("Add new QuestionApproverConfig successful."));

            async Task RollbackInsert()
            {
                await _questionApproverConfigRepository.ForceDelete(tenantId, userId);
            }          
        }        

        public async Task<SearchResult<QuestionApproverConfigSearchViewModel>> SearchAsync(string tenantId, string keyword, int page, int pageSize)
        {
            var items = await _questionApproverConfigRepository.Search(tenantId, keyword, page, pageSize, out var totalRows);
            return new SearchResult<QuestionApproverConfigSearchViewModel>
            {
                Items = items,
                TotalRows = totalRows
            };
        }
    }
}
