﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using GHM.Events;
using GHM.Infrastructure.Constants;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.IServices;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.Resources;
using GHM.Survey.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.Services;
using GHM.Infrastructure.ViewModels;
using GHM.Survey.Domain.Constants;
using Microsoft.Extensions.Configuration;

namespace GHM.Survey.Infrastructure.Services
{
    public class SurveyService : ISurveyService
    {
        private readonly ISurveyRepository _surveyRepository;
        private readonly ISurveyTranslationRepository _surveyTranslationRepository;
        private readonly ISurveyQuestionRepository _surveyQuestionRepository;
        private readonly ISurveyQuestionGroupRepository _surveyQuestionGroupRepository;
        private readonly ISurveyUserRepository _surveyUserRepository;
        private readonly ISurveyGroupTranslationRepository _surveyGroupTranslationRepository;
        private readonly ISurveyUserAnswerTimeRepository _surveyUserAnswerTimeRepository;
        private readonly ISurveyQuestionUserRepository _surveyQuestionUserRepository;
        private readonly ISurveyQuestionAnswerLogicRepository _surveyQuestionAnswerLogicRepository;
        private readonly IQuestionRepository _questionRepository;
        private readonly IAnswerRepository _answerRepository;

        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmSurveyResource> _surveyResourceService;
        private readonly IConfiguration _configuration;

        public SurveyService(ISurveyRepository surveyRepository,
            ISurveyTranslationRepository surveyTranslationRepository,
            ISurveyQuestionRepository surveyQuestionRepository,
            ISurveyQuestionGroupRepository surveyQuestionGroupRepository,
            ISurveyUserRepository surveyUserRepository,
            ISurveyGroupTranslationRepository surveyGroupTranslationRepository,
            ISurveyUserAnswerTimeRepository surveyUserAnswerTimeRepository,
            ISurveyQuestionUserRepository surveyQuestionUserRepository,
            IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmSurveyResource> surveyResourceService,
            IConfiguration configuration, ISurveyQuestionAnswerLogicRepository surveyQuestionAnswerLogicRepository,
            IQuestionRepository questionRepository, IAnswerRepository answerRepository)
        {
            _surveyRepository = surveyRepository;
            _surveyTranslationRepository = surveyTranslationRepository;
            _surveyQuestionRepository = surveyQuestionRepository;
            _surveyQuestionGroupRepository = surveyQuestionGroupRepository;
            _surveyUserRepository = surveyUserRepository;
            _surveyGroupTranslationRepository = surveyGroupTranslationRepository;
            _surveyUserAnswerTimeRepository = surveyUserAnswerTimeRepository;
            _surveyQuestionUserRepository = surveyQuestionUserRepository;
            _sharedResourceService = sharedResourceService;
            _surveyResourceService = surveyResourceService;
            _configuration = configuration;
            _surveyQuestionAnswerLogicRepository = surveyQuestionAnswerLogicRepository;
            _questionRepository = questionRepository;
            _answerRepository = answerRepository;
        }


        public async Task<ActionResultResponse<string>> Insert(string tenantId, string creatorId,
            string creatorFullName, string creatorAvatar, SurveyMeta surveyMeta)
        {
            var surveyId = Guid.NewGuid().ToString();
            if (surveyMeta.StartDate.HasValue && surveyMeta.EndDate.HasValue
                && DateTime.Compare(surveyMeta.StartDate.Value, surveyMeta.EndDate.Value) > 0)
                return new ActionResultResponse<string>(-1,
                  _surveyResourceService.GetString("Start Date must less than Endate. Please contact with administrator."));

            var resultInsertSurvey = await _surveyRepository.Insert(new Domain.Models.Survey
            {
                Id = surveyId,
                SurveyGroupId = surveyMeta.SurveyGroupId,
                ConcurrencyStamp = surveyId,
                IsActive = surveyMeta.IsActive,
                IsRequire = surveyMeta.IsRequire,
                //TotalQuestion = CountTotalQuestions(surveyMeta.Questions, surveyMeta.QuestionGroups),
                LimitedTimes = surveyMeta.LimitedTimes,
                LimitedTime = surveyMeta.LimitedTime,
                StartDate = surveyMeta.StartDate,
                EndDate = surveyMeta.EndDate,
                IsPreRendering = surveyMeta.IsPreRendering,
                TenantId = tenantId,
                CreatorId = creatorId,
                CreatorFullName = creatorFullName,
                Type = surveyMeta.Type
            });

            if (resultInsertSurvey <= 0)
                return new ActionResultResponse<string>(resultInsertSurvey,
                    _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            #region insert survey Translation.
            if (surveyMeta.SurveyTranslations.Count > 0)
            {
                var resultInsertTranslation = await InsertSurveyTranslation();
                if (resultInsertTranslation.Code <= 0)
                {
                    await RollbackInsertSurvey();
                    return resultInsertTranslation;
                }
            }
            #endregion

            #region insert survey question.
            //if (surveyMeta.Questions.Count > 0)
            //{
            //    var resultInsertSurveyQuestion = await InsertSurveyQuestion();
            //    if (resultInsertSurveyQuestion.Code <= 0)
            //    {
            //        await RollbackInsertSurvey();
            //        await RollbackInsertSurveyTranslation();
            //        return resultInsertSurveyQuestion;
            //    }
            //}
            #endregion

            #region insert survey question group.
            //if (surveyMeta.QuestionGroups.Count > 0)
            //{
            //    var resultInsertSurveyQuestionGroup = await InsertSurveyQuestionGroup();
            //    if (resultInsertSurveyQuestionGroup.Code <= 0)
            //    {
            //        await RollbackInsertSurvey();
            //        await RollbackInsertSurveyTranslation();
            //        await RollbackInsertSurveyQuestionGroup();
            //        return resultInsertSurveyQuestionGroup;
            //    }
            //}
            #endregion

            #region insert survey user.
            if (surveyMeta.Users.Count > 0)
            {
                var resultInsertSurveyUsers = await InsertSurveyUsers();
                if (resultInsertSurveyUsers.Code <= 0)
                {
                    await RollbackInsertSurvey();
                    await RollbackInsertSurveyTranslation();
                    await RollbackInsertSurveyQuestionGroup();
                    await RollbackInsertSurveyUsers();
                    return resultInsertSurveyUsers;
                }
            }
            #endregion

            //update totalUser
            var totalUser = await _surveyUserRepository.CountSurveyUser(surveyId);
            await _surveyRepository.UpdateTotalUser(surveyId, totalUser);

            // Tạo đề thi trước
            if (surveyMeta.IsPreRendering)
                await _surveyQuestionUserRepository.RanDomQuestion(surveyId, "", "");

            return new ActionResultResponse<string>(1, _surveyResourceService.GetString("Add new survey successful."),
                string.Empty, surveyId);


            #region Local functions
            async Task<ActionResultResponse<string>> InsertSurveyTranslation()
            {
                var surveyTranslations = new List<SurveyTranslation>();
                foreach (var surveyTranslation in surveyMeta.SurveyTranslations)
                {
                    // Check name exists.
                    var isNameExists = await _surveyTranslationRepository.CheckExists(surveyId,
                        surveyTranslation.LanguageId, surveyTranslation.Name);
                    if (isNameExists)
                    {
                        await RollbackInsertSurvey();
                        return new ActionResultResponse<string>(-1,
                            _surveyResourceService.GetString("Survey name: \"{0}\" already exists.",
                                surveyTranslation.Name));
                    }

                    var surveyTranslationInsert = new SurveyTranslation()
                    {
                        SurveyId = surveyId,
                        LanguageId = surveyTranslation.LanguageId.Trim(),
                        Name = surveyTranslation.Name.Trim(),
                        Description = surveyTranslation.Description?.Trim(),
                        UnsignName = surveyTranslation.Name.StripVietnameseChars().ToUpper()
                    };

                    if (surveyMeta.SurveyGroupId.HasValue)
                    {
                        var infoSurveyGroupname = await _surveyGroupTranslationRepository.GetSurveyGroupName(tenantId,
                            surveyMeta.SurveyGroupId.Value,
                            surveyTranslation.LanguageId.Trim());
                        if (!string.IsNullOrEmpty(infoSurveyGroupname))
                        {
                            surveyTranslationInsert.SurveyGroupName = infoSurveyGroupname;
                        }
                    }

                    surveyTranslations.Add(surveyTranslationInsert);
                }

                var result = await _surveyTranslationRepository.Inserts(surveyTranslations);
                if (result > 0)
                    return new ActionResultResponse<string>(result,
                        _surveyResourceService.GetString("Add new survey translation successful."));

                await RollbackInsertSurveyTranslation();
                await RollbackInsertSurvey();
                return new ActionResultResponse<string>(-2,
                    _surveyResourceService.GetString("Can not insert translation. Please contact with administrator."));
            }

            //async Task<ActionResultResponse<string>> InsertSurveyQuestion()
            //{
            //    var surveyQuestions = new List<SurveyQuestion>();
            //    foreach (var questionVersionId in surveyMeta.Questions)
            //    {
            //        var isQuestionExists =
            //            await _surveyQuestionRepository.CheckExistsBySurveyIdQuestionVersionId(surveyId,
            //                questionVersionId);

            //        if (isQuestionExists) continue;
            //        var surveyQuestionInsert = new SurveyQuestion
            //        {
            //            SurveyId = surveyId,
            //            QuestionVersionId = questionVersionId
            //        };
            //        surveyQuestions.Add(surveyQuestionInsert);
            //    }
            //    var result = await _surveyQuestionRepository.Inserts(surveyQuestions);
            //    if (result > 0)
            //        return new ActionResultResponse<string>(result,
            //            _surveyResourceService.GetString("Add new survey question successful."));

            //    await RollbackInsertSurveyQuestion();
            //    await RollbackInsertSurveyTranslation();
            //    await RollbackInsertSurvey();
            //    return new ActionResultResponse<string>(-3,
            //        _surveyResourceService.GetString("Can not insert questions. Please contact with administrator."));
            //}

            //async Task<ActionResultResponse<string>> InsertSurveyQuestionGroup()
            //{
            //    var surveyQuestionGroups = new List<SurveyQuestionGroup>();
            //    foreach (var surveyQuestionGroup in surveyMeta.QuestionGroups)
            //    {
            //        var isQuestionGroupExists =
            //            await _surveyQuestionGroupRepository.CheckExistsBysurveyIdQuestionGroupId(surveyId,
            //                surveyQuestionGroup.Id);

            //        if (isQuestionGroupExists) continue;
            //        var surveyQuestionGroupInsert = new SurveyQuestionGroup
            //        {
            //            SurveyId = surveyId,
            //            QuestionGroupId = surveyQuestionGroup.Id,
            //            TotalQuestion = surveyQuestionGroup.TotalQuestions,
            //        };
            //        surveyQuestionGroups.Add(surveyQuestionGroupInsert);

            //    }
            //    var result = await _surveyQuestionGroupRepository.Inserts(surveyQuestionGroups);
            //    if (result > 0)
            //        return new ActionResultResponse<string>(result,
            //            _surveyResourceService.GetString("Add new survey question group successful."));

            //    await RollbackInsertSurveyQuestionGroup();
            //    await RollbackInsertSurveyQuestion();
            //    await RollbackInsertSurveyTranslation();
            //    await RollbackInsertSurvey();
            //    return new ActionResultResponse<string>(-4,
            //        _surveyResourceService.GetString("Can not insert question group. Please contact with administrator."));
            //}

            async Task<ActionResultResponse<string>> InsertSurveyUsers()
            {
                var surveyUsers = new List<SurveyUser>();
                var listStringSurveyUsers = new List<string>();
                var apiUrls = _configuration.GetApiUrl();
                if (apiUrls == null)
                    return new ActionResultResponse<string>(-5,
                        _sharedResourceService.GetString(
                            "Missing some configuration. Please contact with administrator."));

                foreach (var userId in surveyMeta.Users)
                {
                    var isUserExists = await _surveyUserRepository.CheckExistsBySurveyIdUserId(surveyId,
                        userId);
                    if (isUserExists) continue;
                    listStringSurveyUsers.Add(userId);
                }

                var resultSurveyUsers = await new HttpClientService()
                    .PostAsync<List<SurveyUserGetViewModel>>($"{apiUrls.HrApiUrl}/users/{tenantId}/short-user-info",
                        listStringSurveyUsers);

                if (resultSurveyUsers == null || !resultSurveyUsers.Any())
                    return new ActionResultResponse<string>(-6,
                        _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

                foreach (var surveyUser in resultSurveyUsers)
                {

                    var surveyUsersInsert = new SurveyUser
                    {
                        SurveyId = surveyId,
                        UserId = surveyUser.UserId,
                        Avatar = surveyUser.Avatar,
                        FullName = surveyUser.FullName,
                        OfficeId = surveyUser.OfficeId ?? 0,
                        OfficeName = surveyUser.OfficeName,
                        PositionId = surveyUser.PositionId,
                        PositionName = surveyUser.PositionName
                    };

                    surveyUsers.Add(surveyUsersInsert);
                }

                var result = await _surveyUserRepository.Inserts(surveyUsers);
                if (result > 0)
                {
                    if (surveyMeta.IsActive)
                        await SendNotificationToParticipants(tenantId, surveyId, creatorId, creatorFullName, creatorAvatar);

                    return new ActionResultResponse<string>(result,
                        _surveyResourceService.GetString("Add participant successful."));
                }

                await RollbackInsertSurveyUsers();
                await RollbackInsertSurveyQuestionGroup();
                await RollbackInsertSurveyQuestion();
                await RollbackInsertSurveyTranslation();
                await RollbackInsertSurvey();
                return new ActionResultResponse<string>(-7,
                    _surveyResourceService.GetString("Can not insert participant. Please contact with administrator."));
            }

            async Task RollbackInsertSurvey()
            {
                await _surveyRepository.ForceDelete(surveyId);
            }

            async Task RollbackInsertSurveyTranslation()
            {
                await _surveyTranslationRepository.Delete(surveyId);
            }

            async Task RollbackInsertSurveyQuestion()
            {
                await _surveyQuestionRepository.DeleteBysurveyId(surveyId);
            }

            async Task RollbackInsertSurveyQuestionGroup()
            {
                await _surveyQuestionGroupRepository.DeleteBysurveyId(surveyId);
            }

            async Task RollbackInsertSurveyUsers()
            {
                await _surveyUserRepository.DeletebysurveyId(surveyId);
            }
            #endregion
        }

        public async Task<ActionResultResponse<string>> Update(string tenantId, string lastUpdateUserId, string lastUpdateFullName,
            string lastUpdateAvatar, string surveyId, SurveyMeta surveyMeta)
        {
            var info = await _surveyRepository.GetInfo(surveyId);
            if (info == null)
                return new ActionResultResponse<string>(-1, _surveyResourceService.GetString("Survey does not exists."));

            if (info.TenantId != tenantId)
                return new ActionResultResponse<string>(-2, _surveyResourceService.GetString("You do not have permission to to this action."));

            if (info.ConcurrencyStamp != surveyMeta.ConcurrencyStamp)
                return new ActionResultResponse<string>(-3,
                    _surveyResourceService.GetString("The survey already updated by other people. you are not allowed to edit the survey information."));

            var isUseBySurveyUserAnswerTimes = await _surveyUserAnswerTimeRepository.CheckExistsBySurveyId(surveyId);
            if (isUseBySurveyUserAnswerTimes)
                return new ActionResultResponse<string>(-4,
                    _surveyResourceService.GetString("Survey has joiner. You can not change this reference infomation."));

            if (surveyMeta.StartDate.HasValue && surveyMeta.EndDate.HasValue
               && DateTime.Compare(surveyMeta.StartDate.Value, surveyMeta.EndDate.Value) > 0)
                return new ActionResultResponse<string>(-1,
                  _surveyResourceService.GetString("Start Date must less than Endate. Please contact with administrator."));

            var oldActiveStatus = info.IsActive;

            info.SurveyGroupId = surveyMeta.SurveyGroupId;
            info.IsActive = surveyMeta.IsActive;
            info.IsRequire = surveyMeta.IsRequire;
            info.TotalQuestion = surveyMeta.TotalQuestion;
            info.LimitedTimes = surveyMeta.LimitedTimes;
            info.LimitedTime = surveyMeta.LimitedTime;
            info.StartDate = surveyMeta.StartDate;
            info.EndDate = surveyMeta.EndDate;
            info.IsPreRendering = surveyMeta.IsPreRendering;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();
            info.LastUpdate = DateTime.Now;
            info.LastUpdateUserId = lastUpdateUserId;
            info.LastUpdateFullName = lastUpdateFullName;
            //info.TotalQuestion = CountTotalQuestions(surveyMeta.Questions, surveyMeta.QuestionGroups);
            info.Type = surveyMeta.Type;
            var resultUpdateSurvey = await _surveyRepository.Update(info);

            foreach (var surveyTranslation in surveyMeta.SurveyTranslations)
            {
                var isNameExists = await _surveyTranslationRepository.CheckExists(info.Id,
                    surveyTranslation.LanguageId, surveyTranslation.Name);
                if (isNameExists)
                    return new ActionResultResponse<string>(-5,
                        _surveyResourceService.GetString("Survey name: \"{0}\" already exists.", surveyTranslation.Name));

                var surveyTranslationInfo =
                    await _surveyTranslationRepository.GetInfo(info.Id, surveyTranslation.LanguageId);
                if (surveyTranslationInfo != null)
                {
                    surveyTranslationInfo.Name = surveyTranslation.Name.Trim();
                    surveyTranslationInfo.Description = surveyTranslation.Description?.Trim();
                    surveyTranslationInfo.UnsignName = surveyTranslation.Name.StripVietnameseChars().ToUpper();

                    if (surveyMeta.SurveyGroupId.HasValue)
                    {
                        var infoSurveyGroupname = await _surveyGroupTranslationRepository.GetSurveyGroupName(tenantId,
                            surveyMeta.SurveyGroupId.Value,
                            surveyTranslation.LanguageId.Trim());
                        if (!string.IsNullOrEmpty(infoSurveyGroupname))
                        {
                            surveyTranslationInfo.SurveyGroupName = infoSurveyGroupname;
                        }
                    }
                    else
                    {
                        surveyTranslationInfo.SurveyGroupName = string.Empty;
                    }
                    await _surveyTranslationRepository.Update(surveyTranslationInfo);
                }
                else
                {
                    var surveyTranslationInsert = new SurveyTranslation
                    {
                        SurveyId = surveyId,
                        LanguageId = surveyTranslation.LanguageId.Trim(),
                        Name = surveyTranslation.Name.Trim(),
                        Description = surveyTranslation.Description?.Trim(),
                        UnsignName = surveyTranslation.Name.StripVietnameseChars().ToUpper()
                    };
                    if (surveyMeta.SurveyGroupId.HasValue)
                    {
                        var infoSurveyGroupname = await _surveyGroupTranslationRepository.GetSurveyGroupName(tenantId,
                            surveyMeta.SurveyGroupId.Value,
                            surveyTranslation.LanguageId.Trim());
                        if (!string.IsNullOrEmpty(infoSurveyGroupname))
                        {
                            surveyTranslationInsert.SurveyGroupName = infoSurveyGroupname;
                        }
                    }

                    await _surveyTranslationRepository.Insert(surveyTranslationInsert);
                }
            }

            // Update question and question groups.
            //if (surveyMeta.Questions.Any() || surveyMeta.QuestionGroups.Any())
            //    await UpdateSurveyQuestionSurveyQuestionGroup();

            // Update users
            if (surveyMeta.Users.Any())
                await UpdateSurveyUser();

            if (resultUpdateSurvey > 0 && !oldActiveStatus && info.IsActive)
            {
                await SendNotificationToParticipants(info.TenantId, info.Id, lastUpdateUserId, lastUpdateFullName,
                    lastUpdateAvatar);
            }

            return new ActionResultResponse<string>
            {
                Code = 1,
                Message = _surveyResourceService.GetString("Update survey successful."),
                Data = info.ConcurrencyStamp
            };

            #region local functions.
            //async Task<ActionResultResponse> UpdateSurveyQuestionSurveyQuestionGroup()
            //{
            //    if (surveyMeta.Questions != null && surveyMeta.Questions.Any())
            //    {
            //        var resultUpdateSurveyQuestions = await UpdateSurveyQuestions();
            //        if (resultUpdateSurveyQuestions.Code <= 0)
            //            return resultUpdateSurveyQuestions;
            //    }

            //    if (surveyMeta.QuestionGroups == null || !surveyMeta.QuestionGroups.Any())
            //        return new ActionResultResponse<string>(1,
            //            _surveyResourceService.GetString("Update survey successful."),
            //            string.Empty, surveyId);

            //    var resultUpdateQuestionGroups = await UpdateQuestionGroups();
            //    if (resultUpdateQuestionGroups.Code <= 0)
            //        return resultUpdateQuestionGroups;

            //    return new ActionResultResponse<string>(1, _surveyResourceService.GetString("Update survey successful."),
            //        string.Empty, surveyId);

            //    // Update survey questions.
            //    async Task<ActionResultResponse> UpdateSurveyQuestions()
            //    {
            //        await _surveyQuestionRepository.DeleteBysurveyId(surveyId);

            //        var surveyQuestions = surveyMeta.Questions.Select(questionVersionId => new SurveyQuestion
            //        {
            //            SurveyId = surveyId,
            //            QuestionVersionId = questionVersionId
            //        }).ToList();

            //        var result = await _surveyQuestionRepository.Inserts(surveyQuestions);
            //        if (result > 0)
            //            return new ActionResultResponse(result,
            //                _surveyResourceService.GetString("Update survey question successful."));

            //        return new ActionResultResponse(-3,
            //            _surveyResourceService.GetString("Can not update question. Please contact with administrator."));
            //    }

            //    // Update survey question groups.
            //    async Task<ActionResultResponse> UpdateQuestionGroups()
            //    {
            //        await _surveyQuestionGroupRepository.DeleteBysurveyId(surveyId);
            //        var surveyQuestionGroups = surveyMeta.QuestionGroups
            //            .Select(surveyQuestionGroup => new SurveyQuestionGroup()
            //            {
            //                SurveyId = surveyId,
            //                QuestionGroupId = surveyQuestionGroup.Id,
            //                TotalQuestion = surveyQuestionGroup.TotalQuestions,
            //            })
            //            .ToList();

            //        var result = await _surveyQuestionGroupRepository.Inserts(surveyQuestionGroups);
            //        if (result > 0)
            //            return new ActionResultResponse(result,
            //                _surveyResourceService.GetString("Update survey question group successful."));

            //        return new ActionResultResponse(-4,
            //            _surveyResourceService.GetString("Can not update question group. Please contact with administrator."));
            //    }
            //}

            async Task<ActionResultResponse> UpdateSurveyUser()
            {
                var apiUrls = _configuration.GetApiUrl();
                if (apiUrls == null)
                    return new ActionResultResponse<string>(-3,
                        _sharedResourceService.GetString("Missing some configuration. Please contact with administrator."));

                var listSurveyUsers = await _surveyUserRepository.GetListSurveyUser(surveyId);
                if (listSurveyUsers == null || !listSurveyUsers.Any())
                {
                    // Update total survey users.
                    await UpdateTotalSurveyUser();

                    // Add new survey users.
                    var resultInsertNew = await AddNewSurveyUser(surveyMeta.Users);
                    if (resultInsertNew.Code > 0 && surveyMeta.IsActive)
                    {
                        // Send notification to added users.
                        await SendNotificationToParticipants(tenantId, info.Id, lastUpdateUserId, lastUpdateFullName,
                            lastUpdateAvatar);
                    }
                    return resultInsertNew;
                }

                // Lấy về danh sách survey user.
                var listExistingSurveyUserIds = listSurveyUsers.Select(x => x.UserId).ToList();

                // Lấy về danh sách thêm.
                var listNewSurveyUsers = surveyMeta.Users.Except(listExistingSurveyUserIds).ToList();

                // Lấy về danh chuẩn bị xóa.
                var listNotExistsInSurveyUserIds = listExistingSurveyUserIds.Where(x => !surveyMeta.Users.Contains(x)).ToList();

                if (!listNewSurveyUsers.Any() && !listNotExistsInSurveyUserIds.Any())
                    return new ActionResultResponse(-5, _surveyResourceService.GetString("Please select user change."));

                // Lấy về danh sách SurveyUserAnswerTimes
                var listUserAnswerTimes = await _surveyUserAnswerTimeRepository.GetUserIdsBySurveyId(surveyId);

                // Lấy danh sách xóa.
                var listDeleteSurveyUserIds = listNotExistsInSurveyUserIds.Except(listUserAnswerTimes).ToList();

                // Delete survey user.
                await _surveyUserRepository.Deletes(surveyId, listDeleteSurveyUserIds);
                if (listNewSurveyUsers.Any())
                {
                    var resultAddedUsers = await AddNewSurveyUser(listNewSurveyUsers);
                    if (resultAddedUsers.Code > 0 && oldActiveStatus && surveyMeta.IsActive)
                    {
                        SendNotificationToAddedParticipant();
                    }
                }

                // Update total survey users.
                await UpdateTotalSurveyUser();

                // Tạo đề thi trước
                if (info.IsPreRendering)
                {
                    await _surveyQuestionUserRepository.RanDomQuestion(info.Id, "", "");
                }

                return new ActionResultResponse<string>(1, _surveyResourceService.GetString("Update survey user successful."),
                    string.Empty, surveyId);

                async Task<ActionResultResponse<string>> AddNewSurveyUser(List<string> userIds)
                {
                    var listUserInfo = await new HttpClientService()
                        .PostAsync<List<SurveyUserGetViewModel>>($"{apiUrls.HrApiUrl}/users/{tenantId}/short-user-info",
                            userIds);

                    if (listUserInfo == null || !listUserInfo.Any())
                        return new ActionResultResponse<string>(-2,
                            _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

                    var surveyUsers = listUserInfo.Select(userInfo => new SurveyUser
                    {
                        SurveyId = surveyId,
                        UserId = userInfo.UserId,
                        Avatar = userInfo.Avatar,
                        FullName = userInfo.FullName,
                        OfficeId = userInfo.OfficeId ?? 0,
                        OfficeName = userInfo.OfficeName,
                        PositionId = userInfo.PositionId,
                        PositionName = userInfo.PositionName
                    }).ToList();

                    var result = await _surveyUserRepository.Inserts(surveyUsers);
                    if (result <= 0)
                        return new ActionResultResponse<string>(result, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

                    await UpdateTotalSurveyUser();
                    return new ActionResultResponse<string>(result, _surveyResourceService.GetString("Add user into survey succesful."));
                }

                async Task UpdateTotalSurveyUser()
                {
                    // Update joinNumber
                    var totalUser = await _surveyUserRepository.CountSurveyUser(surveyId);
                    await _surveyRepository.UpdateTotalUser(surveyId, totalUser);
                }

                void SendNotificationToAddedParticipant()
                {
                    if (!listSurveyUsers.Any()) return;

                    var notificationEvents = listSurveyUsers.Select(x => new Notifications
                    {
                        TenantId = tenantId,
                        Title = $"<b>{lastUpdateFullName}</b> {{asigned you into a survey.}}",
                        Content = "",
                        SenderId = lastUpdateUserId,
                        SenderFullName = lastUpdateFullName,
                        SenderAvatar = lastUpdateAvatar,
                        Url = $"/surveys/overviews/{surveyId}",
                        ReceiverId = x.UserId,
                        Type = (int)NotificationType.Info,
                        IsSystem = false
                    }).ToList();

                    var notificationHelper = new NotificationHelper();
                    foreach (var notificationEvent in notificationEvents)
                    {
                        notificationHelper.Send(notificationEvent);
                    }
                }
            }
            #endregion
        }

        public async Task<ActionResultResponse> Delete(string tenantId, string surveyId)
        {
            var info = await _surveyRepository.GetInfo(surveyId);
            if (info == null)
                return new ActionResultResponse(-1, _surveyResourceService.GetString("Survey does not exists. Please try again."));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString("You do not have permission to to this action."));

            var checkSurveyUserAnswerTimes = await _surveyUserAnswerTimeRepository.CheckExistsBySurveyId(surveyId);
            if (checkSurveyUserAnswerTimes)
                return new ActionResultResponse(-3, _surveyResourceService.GetString("Survey has joiner. You can not change this reference infomation."));

            var result = await _surveyRepository.Delete(surveyId);
            if (result <= 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await _surveyTranslationRepository.Delete(surveyId);
            return new ActionResultResponse(result, _surveyResourceService.GetString("Delete survey successful."));
        }

        public async Task<ActionResultResponse<SurveyDetailViewModel>> GetDetail(string tenantId, string languageId, string surveyId)
        {
            var info = await _surveyRepository.GetInfo(surveyId);
            if (info == null)
                return new ActionResultResponse<SurveyDetailViewModel>(-1, _surveyResourceService.GetString("Survey does not exists."));

            if (info.TenantId != tenantId)
                return new ActionResultResponse<SurveyDetailViewModel>(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var surveyTranslations = await _surveyTranslationRepository.GetsBySurveyId(surveyId);
            var surveyQuestions = _surveyQuestionRepository.GetSurveyQuestions(surveyId, languageId);
            if (info.Type == SurveyType.Logic)
            {
                // Get all logic answers.
                var logicAnswers = await _surveyQuestionAnswerLogicRepository.GetsBySurveyId(surveyId, languageId);
                foreach (var question in surveyQuestions)
                {
                    question.Answers.ForEach(answer =>
                    {
                        answer.ToQuestionVersionId = logicAnswers.FirstOrDefault(x => x.AnswerId == answer.Id)?.ToQuestionVersionId;
                        answer.ToQuestionName = logicAnswers.FirstOrDefault(x => x.AnswerId == answer.Id)?.ToQuestionName;
                    });
                }
            }
            var surveyQuestionGroups = await _surveyRepository.GetListSurveyQuestionGroups(surveyId, languageId);
            var surveyUsers = await _surveyUserRepository.GetListSurveyUser(surveyId);

            var surveyDetail = new SurveyDetailViewModel
            {
                Id = info.Id,
                SurveyGroupId = info.SurveyGroupId,
                IsActive = info.IsActive,
                IsRequire = info.IsRequire,
                IsPreRendering = info.IsPreRendering,
                TotalQuestion = info.TotalQuestion,
                TotalUser = info.TotalUser,
                LimitedTimes = info.LimitedTimes,
                LimitedTime = info.LimitedTime,
                Status = info.Status,
                StartDate = info.StartDate,
                EndDate = info.EndDate,
                Type = info.Type,
                ConcurrencyStamp = info.ConcurrencyStamp,

                SurveyTranslations = surveyTranslations.Select(x => new SurveyTranslationViewModel()
                {
                    LanguageId = x.LanguageId,
                    Name = x.Name,
                    Description = x.Description,
                    SurveyGroupName = x.SurveyGroupName
                }).ToList(),

                Users = surveyUsers.Select(x => new SurveyUserViewModel()
                {
                    UserId = x.UserId,
                    FullName = x.FullName,
                    Avatar = x.Avatar,
                    OfficeId = x.OfficeId,
                    OfficeName = x.OfficeName,
                    PositionId = x.PositionId,
                    PositionName = x.PositionName
                }).ToList(),

                Questions = surveyQuestions,
                QuestionGroups = surveyQuestionGroups

            };
            return new ActionResultResponse<SurveyDetailViewModel>
            {
                Code = 1,
                Data = surveyDetail
            };
        }

        public async Task<ActionResultResponse> SaveQuestions(string tenantId, string surveyId, string currentUserId, string currentUserFullName,
            string currentUserAvatar, SurveyListQuestionGroupMeta surveyQusetionGroupMeta)
        {
            var surveyInfo = await _surveyRepository.GetInfo(tenantId, surveyId, true);
            if (surveyInfo == null)
                return new ActionResultResponse(-1,
                    _surveyResourceService.GetString("Survey does not exists or your do not have permission to do this action."));

            var checkSurveyUserAnswerTimes = await _surveyUserAnswerTimeRepository.CheckExistsBySurveyId(surveyId);
            if (checkSurveyUserAnswerTimes)
                return new ActionResultResponse(-2,
                    _surveyResourceService.GetString("Survey has joiner. You can not change this reference infomation."));

            await SaveSurveyQuestionGroups();
            //var resultUpdateQuestionAnswer = await SaveQuestionAnswerLogic();
            if (surveyQusetionGroupMeta?.Questions != null && surveyQusetionGroupMeta.Questions.Any())
            {
                var resultUpdateQuestionAnswer = await SaveSurveyQuestions();
                if (resultUpdateQuestionAnswer.Code < 0)
                    return resultUpdateQuestionAnswer;
            }

            await UpdateTotalQuestion();
            // Send notification to participants.
            if (surveyInfo.IsActive)
            {
                // Send notification to participants.
                await SendNotificationToParticipants(tenantId, surveyId, currentUserId, currentUserFullName,
                    currentUserAvatar);
            }
            return new ActionResultResponse(1, _surveyResourceService.GetString("Update survey question successful!"));

            async Task<ActionResultResponse> SaveSurveyQuestions()
            {
                var questionIds = surveyQusetionGroupMeta.Questions.Select(x => x.Id).ToList();
                var questionsInfos = await _questionRepository.GetsInfo(tenantId, questionIds);
                if (questionsInfos == null || !questionsInfos.Any())
                    return new ActionResultResponse(-3, _surveyResourceService.GetString("Question does not exists."));

                var questions = new List<SurveyQuestion>();
                if (surveyInfo.Type == SurveyType.Logic)
                {
                    var inValidQuestionCount = questionsInfos.Count(x => x.Type != QuestionType.Logic);
                    if (inValidQuestionCount > 0)
                        return new ActionResultResponse(-4,
                            _surveyResourceService.GetString("Logic survey just accept logic question."));

                    var i = 1;
                    foreach (var questionId in questionIds)
                    {
                        var questionInfo = questionsInfos.Where(x => x.Id == questionId).FirstOrDefault();
                        if (questionInfo != null)
                        {
                            questions.Add(new SurveyQuestion
                            {
                                SurveyId = surveyId,
                                QuestionVersionId = questionInfo.VersionId,
                                Point = questionInfo.Point,
                                Order = i,
                            });
                        }
                        i++;
                    }

                    //questions.AddRange(questionsInfos.Select((question, i) => new SurveyQuestion
                    //{
                    //    SurveyId = surveyId,
                    //    QuestionVersionId = question.VersionId,
                    //    Point = question.Point,
                    //    Order = i
                    //}));

                    if (!questions.Any())
                        return new ActionResultResponse(-5, _surveyResourceService.GetString("Questions does not exists or invalid. Please try again."));

                    // Add answer logics.
                    var listAnswerLogics = new List<SurveyQuestionAnswerLogic>();
                    foreach (var surveyQuestionMeta in surveyQusetionGroupMeta.Questions)
                    {
                        if (surveyQuestionMeta.Answers.Any(x => x.ToQuestionVersionId == surveyQuestionMeta.Id))
                            return new ActionResultResponse(-3,
                                _surveyResourceService.GetString(
                                    "Next logic question can not be the same with previous question."));

                        var questionInfo = questions.FirstOrDefault(x => x.QuestionVersionId == surveyQuestionMeta.Id);
                        if (questionInfo == null)
                            return new ActionResultResponse(-6,
                                _surveyResourceService.GetString(
                                    "Question does not exists or invalid. Please contact with administrator."));

                        foreach (var surveyQuestionAnswerMeta in surveyQuestionMeta.Answers)
                        {
                            if (string.IsNullOrEmpty(surveyQuestionAnswerMeta.ToQuestionVersionId))
                                continue;

                            var answerInfo = await _answerRepository.GetInfo(questionInfo.QuestionVersionId,
                                surveyQuestionAnswerMeta.Id, CultureInfo.CurrentCulture.Name);
                            if (answerInfo == null)
                                return new ActionResultResponse(-7,
                                    _surveyResourceService.GetString("Answer does not exists exists"));

                            var toQuestionInfo = await _questionRepository.GetInfo(tenantId, surveyQuestionAnswerMeta.ToQuestionVersionId);
                            if (toQuestionInfo == null)
                                return new ActionResultResponse(-8,
                                    _surveyResourceService.GetString("Next question info for answer: {0} does not exists.", answerInfo.Name));

                            listAnswerLogics.Add(new SurveyQuestionAnswerLogic
                            {
                                QuestionVersionId = questionInfo.QuestionVersionId,
                                SurveyId = surveyId,
                                AnswerId = answerInfo.Id,
                                ToQuestionVersionId = toQuestionInfo.VersionId
                            });
                        }
                    }

                    var resultInsertAnswerLogic = await _surveyQuestionAnswerLogicRepository.Inserts(listAnswerLogics);
                    if (resultInsertAnswerLogic <= 0)
                        return new ActionResultResponse(-9,
                            _surveyResourceService.GetString("Insert answer fail. Please contact with adminsitrator."));

                    // Delete all survey questions.            
                    await _surveyQuestionRepository.DeleteBysurveyId(surveyId);

                    // Add new survey questions.
                    var result = await _surveyQuestionRepository.Inserts(questions);
                    return new ActionResultResponse(result, result > 0
                        ? _surveyResourceService.GetString("Save question successful.")
                        : _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
                }
                else
                {
                    var i = 1;
                    foreach (var questionId in questionIds)
                    {
                        var questionInfo = questionsInfos.FirstOrDefault(x => x.Id == questionId);
                        if (questionInfo != null)
                        {
                            questions.Add(new SurveyQuestion
                            {
                                SurveyId = surveyId,
                                QuestionVersionId = questionInfo.VersionId,
                                Point = questionInfo.Point,
                                Order = i,
                            });
                        }
                        i++;
                    }

                    if (!questions.Any())
                        return new ActionResultResponse(-5, _surveyResourceService.GetString("Questions does not exists or invalid. Please try again."));

                    // Delete all survey questions.            
                    await _surveyQuestionRepository.DeleteBysurveyId(surveyId);

                    // Add new survey questions.
                    var result = await _surveyQuestionRepository.Inserts(questions);
                    return new ActionResultResponse(result, result > 0
                        ? _surveyResourceService.GetString("Save question successful.")
                        : _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
                }

            }

            async Task SaveSurveyQuestionGroups()
            {
                // Remove old question group.
                await _surveyQuestionGroupRepository.DeleteBysurveyId(surveyId);

                // Add questions.
                var questionGroups = surveyQusetionGroupMeta.QuestionGroups.Where(x => x.TotalQuestions > 0).Select(x =>
                    new SurveyQuestionGroup
                    {
                        SurveyId = surveyId,
                        QuestionGroupId = x.Id,
                        TotalQuestion = x.TotalQuestions
                    }).ToList();
                await _surveyQuestionGroupRepository.Inserts(questionGroups);

            }

            async Task UpdateTotalQuestion()
            {
                var totalQuestions = await _surveyQuestionRepository.GetTotalQuestion(surveyId);

                var questionGroups = surveyQusetionGroupMeta.QuestionGroups.Where(x => x.TotalQuestions > 0).Select(x =>
                  new SurveyQuestionGroup
                  {
                      SurveyId = surveyId,
                      QuestionGroupId = x.Id,
                      TotalQuestion = x.TotalQuestions
                  }).ToList();

                var totalQuestionInQuestionGroup =
                 questionGroups != null && questionGroups.Any()
                     ? questionGroups.Select(x => x.TotalQuestion).Sum()
                     : 0;

                await _surveyRepository.UpdateTotalQuestion(surveyId, totalQuestions + totalQuestionInQuestionGroup);
            }
        }

        public async Task<ActionResultResponse<SurveyQuestionInfoViewModel>> GetSurveyQuestions(string tenantId, string languageId, string surveyId)
        {
            var surveyInfo = await _surveyRepository.GetInfo(tenantId, surveyId, true);
            if (surveyInfo == null)
                return new ActionResultResponse<SurveyQuestionInfoViewModel>(-1, _surveyResourceService.GetString("Survey does not exists."));

            var surveyQuestions = _surveyQuestionRepository.GetSurveyQuestions(surveyId, languageId);
            if (surveyInfo.Type == SurveyType.Logic)
            {
                // Get all logic answers.
                var logicAnswers = await _surveyQuestionAnswerLogicRepository.GetsBySurveyId(surveyId, languageId);
                foreach (var question in surveyQuestions)
                {
                    question.Answers.ForEach(answer =>
                    {
                        answer.ToQuestionVersionId = logicAnswers.FirstOrDefault(x => x.AnswerId == answer.Id)?.ToQuestionVersionId;
                        answer.ToQuestionName = logicAnswers.FirstOrDefault(x => x.AnswerId == answer.Id)?.ToQuestionName;
                    });
                }
            }
            var surveyQuestionGroups = await _surveyQuestionGroupRepository.GetsBySurveyId(surveyId, languageId);
            return new ActionResultResponse<SurveyQuestionInfoViewModel>
            {
                Data = new SurveyQuestionInfoViewModel
                {
                    Questions = surveyQuestions,
                    QuestionGroups = surveyQuestionGroups
                }
            };
        }

        public async Task<ActionResultResponse<SurveyInfoViewModel>> UserGetSurveyInfo(string tenantId, string languageId, string userId,
            string surveyId)
        {
            var info = await _surveyRepository.GetInfo(surveyId);
            if (info == null)
                return new ActionResultResponse<SurveyInfoViewModel>(-1, _surveyResourceService.GetString("Survey does not exists."));

            if (info.TenantId != tenantId)
                return new ActionResultResponse<SurveyInfoViewModel>(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var surveyInfoByLanguage = await _surveyTranslationRepository.GetInfo(surveyId, languageId, true);
            if (surveyInfoByLanguage == null)
                return new ActionResultResponse<SurveyInfoViewModel>(-3,
                    _surveyResourceService.GetString("This survey does not support your language."));

            var surveyQuestions = _surveyQuestionRepository.GetSurveyQuestions(surveyId, languageId);
            if (info.Type == SurveyType.Logic)
            {
                // Get all logic answers.
                var logicAnswers = await _surveyQuestionAnswerLogicRepository.GetsBySurveyId(surveyId, languageId);
                foreach (var question in surveyQuestions)
                {
                    question.Answers.ForEach(answer =>
                    {
                        answer.ToQuestionVersionId = logicAnswers.FirstOrDefault(x => x.AnswerId == answer.Id)?.ToQuestionVersionId;
                        answer.ToQuestionName = logicAnswers.FirstOrDefault(x => x.AnswerId == answer.Id)?.ToQuestionName;
                    });
                }
            }
            var surveyQuestionGroups = await _surveyRepository.GetListSurveyQuestionGroups(surveyId, languageId);
            var surveyUsers = await _surveyUserRepository.GetListSurveyUser(surveyId);

            var surveyDetail = new SurveyInfoViewModel
            {
                Id = info.Id,
                Name = surveyInfoByLanguage.Name,
                Type = info.Type,
                Questions = surveyQuestions,
            };
            return new ActionResultResponse<SurveyInfoViewModel>
            {
                Code = 1,
                Data = surveyDetail
            };
        }

        public async Task<SearchResult<SurveyViewModel>> Search(string tenantId, string languageId, string keyword,
            int? surveyGroupId, DateTime? startDate, DateTime? endDate, bool? isActive, int page, int pageSize)
        {
            var items = await _surveyRepository.Search(tenantId, languageId, keyword, surveyGroupId, startDate, endDate,
                isActive, page, pageSize, out var totalRows);
            return new SearchResult<SurveyViewModel>
            {
                TotalRows = totalRows,
                Items = items
            };
        }

        private static int CountTotalQuestions(IReadOnlyCollection<string> questionIds, IReadOnlyCollection<SurveyQuestionGroupMeta> questionGroups)
        {
            var totalQuestion = questionIds != null && questionIds.Any()
                ? questionIds.Count : 0;
            var totalQuestionInQuestionGroup =
                questionGroups != null && questionGroups.Any()
                    ? questionGroups.Select(x => x.TotalQuestions).Sum()
                    : 0;

            return totalQuestion + totalQuestionInQuestionGroup;
        }

        private async Task SendNotificationToParticipants(string tenantId, string surveyId, string creatorId, string creatorFullName,
            string creatorAvatar)
        {
            var listUsers = await _surveyUserRepository.GetListSurveyUser(surveyId, true);
            var notificationHelper = new NotificationHelper();
            if (listUsers != null && listUsers.Any())
            {
                var notificationEvents = listUsers.Select(x => new Notifications
                {
                    TenantId = tenantId,
                    Title = $"<b>{creatorFullName}</b> {{asigned you into a survey.}}",
                    Content = "",
                    SenderId = creatorId,
                    SenderFullName = creatorFullName,
                    SenderAvatar = creatorAvatar,
                    Url = $"/surveys/overviews/{surveyId}",
                    ReceiverId = x.UserId,
                    Type = (int)NotificationType.Info,
                    IsSystem = false
                }).ToList();
                foreach (var notificationEvent in notificationEvents)
                {
                    notificationHelper.Send(notificationEvent);
                }
            }
        }
    }
}