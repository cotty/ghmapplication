﻿using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using GHM.Survey.Domain.Constants;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.IServices;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.Resources;
using GHM.Survey.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Events;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Helpers;
using Microsoft.Extensions.Configuration;

namespace GHM.Survey.Infrastructure.Services
{
    public class QuestionService : IQuestionService
    {
        private readonly IQuestionRepository _questionRepository;
        private readonly IQuestionTranslationRepository _questionTranslationRepository;
        private readonly IQuestionGroupTranslationRepository _questionGroupTranslationRepository;
        private readonly IQuestionGroupRepository _questionGroupRepository;
        private readonly IAnswerRepository _answerRepository;
        private readonly IAnswerTranslationRepository _answerTranslationRepository;
        private readonly IQuestionApproverConfigRepository _questionApproverConfigRepository;
        private readonly ISurveyQuestionUserRepository _surveyQuestionUserRepository;

        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmSurveyResource> _surveyResourceService;
        //private readonly IEventBus _eventBus;

        // Synchronous message.
        private readonly IConfiguration _configuration;

        public QuestionService(IQuestionRepository questionRepository,
            IQuestionTranslationRepository questionTranslationRepository,
            IQuestionGroupTranslationRepository questionGroupTranslationRepository,
            IQuestionGroupRepository questionGroupRepository,
            IAnswerRepository answerRepositor, IAnswerTranslationRepository answerTranslationRepository,
            IQuestionApproverConfigRepository questionApproverConfigRepository,
            ISurveyQuestionUserRepository surveyQuestionUserRepository,
            IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmSurveyResource> surveyResourceService,
            //IEventBus eventBus,
            IConfiguration configuration)
        {
            _questionRepository = questionRepository;
            _questionTranslationRepository = questionTranslationRepository;
            _questionGroupTranslationRepository = questionGroupTranslationRepository;
            _questionGroupRepository = questionGroupRepository;
            _answerRepository = answerRepositor;
            _answerTranslationRepository = answerTranslationRepository;
            _questionApproverConfigRepository = questionApproverConfigRepository;
            _surveyQuestionUserRepository = surveyQuestionUserRepository;
            _sharedResourceService = sharedResourceService;
            _surveyResourceService = surveyResourceService;
            //_eventBus = eventBus;
            _configuration = configuration;
        }

        public async Task<ActionResultResponse> Insert(string tenantId, string questionId, bool isSend, string creatorId,
            string creatorFullName, string creatorAvatar, QuestionMeta questionMeta)
        {
            var isQuestionGroupExists = await _questionGroupRepository.CheckExists(questionMeta.QuestionGroupId);
            if (!isQuestionGroupExists)
                return new ActionResultResponse(-1,
                    _sharedResourceService.GetString("Question group does not exists or deleted. Please contact with administrator."));

            if ((questionMeta.Type == QuestionType.SingleChoice ||
                 questionMeta.Type == QuestionType.MultiChoice ||
                 questionMeta.Type == QuestionType.Logic) && questionMeta.Answers.Count < 2)
            {
                if (questionMeta.Answers.Count < 2)
                    return new ActionResultResponse(-2,
                        _sharedResourceService.GetString(ValidatorMessage.PleaseSelectAtLeast, 2, _surveyResourceService.GetString("answer")));
            }
            var validAnswerCount = 0;
            switch (questionMeta.Type)
            {
                case QuestionType.SingleChoice:
                case QuestionType.MultiChoice:
                    var correctAnswers = questionMeta.Answers.Where(x => x.IsCorrect).ToList();
                    if (!correctAnswers.Any())
                        return new ActionResultResponse(-3, _sharedResourceService.GetString(ValidatorMessage.PleaseSelectAtLeast, 1,
                            _surveyResourceService.GetString("Correct answer")));

                    validAnswerCount =
                        questionMeta.Answers.Count(x => x.Translations.Any(t => !string.IsNullOrEmpty(t.Name?.Trim())));

                    if (validAnswerCount < 2)
                        return new ActionResultResponse(-4, _sharedResourceService.GetString(ValidatorMessage.PleaseSelectAtLeast, 2,
                            _surveyResourceService.GetString("answers")));

                    foreach (var correctAnswer in correctAnswers)
                    {
                        if (correctAnswer.Translations.Any(x => string.IsNullOrEmpty(x.Name)))
                        {
                            return new ActionResultResponse(-5, _sharedResourceService.GetString(ValidatorMessage.NotNull),
                                _surveyResourceService.GetString("answer name"));
                        }
                    }

                    questionMeta.TotalAnswer = (byte)validAnswerCount;
                    break;
                case QuestionType.Rating:
                    validAnswerCount =
                        questionMeta.Answers.Count(x => x.Translations.Any(t => !string.IsNullOrEmpty(t.Name?.Trim())));
                    if (validAnswerCount < 3)
                        return new ActionResultResponse(-4, _sharedResourceService.GetString(ValidatorMessage.PleaseSelectAtLeast, 3,
                            _surveyResourceService.GetString("answers")));

                    if (questionMeta.Answers.Count < 3)
                        return new ActionResultResponse(-6,
                            _sharedResourceService.GetString(ValidatorMessage.PleaseSelectAtLeast, 3, _surveyResourceService.GetString("answer")));

                    questionMeta.TotalAnswer = (byte)validAnswerCount;
                    break;
                case QuestionType.Essay:
                    break;
                case QuestionType.SelfResponded:
                    //if ()
                    //    return new ActionResultResponse(-6,
                    //        _sharedResourceService.GetString(ValidatorMessage.PleaseEnter, _surveyResourceService.GetString("total answer")));
                    questionMeta.TotalAnswer = !questionMeta.TotalAnswer.HasValue || questionMeta.TotalAnswer.Value == 0
                        ? 1
                        : questionMeta.TotalAnswer;
                    break;
                case QuestionType.Logic:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            questionId = string.IsNullOrEmpty(questionId) ? Guid.NewGuid().ToString() : questionId;
            var question = new Question
            {
                VersionId = questionId,
                Id = questionId,
                QuestionGroupId = questionMeta.QuestionGroupId,
                TenantId = tenantId,
                Status = isSend ? QuestionStatus.Pending : QuestionStatus.Draft,
                CreatorId = creatorId,
                IsActive = questionMeta.IsActive,
                Type = questionMeta.Type,
                Point = questionMeta.Point ?? 0,
                CreatorFullName = creatorFullName,
                //TotalAnswer = questionMeta.Answers != null && questionMeta.Type == QuestionType.MultiChoice
                //                && questionMeta.Type == QuestionType.SingleChoice
                //                && questionMeta.Type == QuestionType.Rating
                //             ? (byte)questionMeta.Answers.Count :
                //              questionMeta.Type == QuestionType.Essay && questionMeta.TotalAnswer.HasValue
                //              ? questionMeta.TotalAnswer.Value : (byte)0
                TotalAnswer = questionMeta.TotalAnswer ?? 0
            };

            // Kiểm tra nếu câu hỏi chưa từng có sẽ cho question.VersionId = question.Id
            var isExistsByQuestionId = await _questionRepository.CheckExistsByQuestionId(question.Id, question.TenantId, true);
            question.VersionId = isExistsByQuestionId ? Guid.NewGuid().ToString() : question.Id;

            var result = await _questionRepository.Insert(question);
            if (result <= 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            // Update question group's total question.
            var resultUpdate = await UpdateTotalQuestionInQuestionGroup(question.QuestionGroupId);
            if (resultUpdate < 0)
                return new ActionResultResponse(resultUpdate, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            var resultInsertTranslation = await InsertQuestionTranslation();
            if (resultInsertTranslation.Code > 0)
            {
                if (isSend)
                    await SendNotificationToApprover(tenantId, creatorId, creatorFullName, creatorAvatar,
                        question.VersionId);

                return resultInsertTranslation;
            }

            await RollbackInsert();
            return new ActionResultResponse(-5,
                _surveyResourceService.GetString("Can not insert new Question. Please contact with administrator."));

            async Task RollbackInsert()
            {
                await _questionRepository.ForceDelete(tenantId, question.Id, question.VersionId);
            }

            async Task RollbackInsertQuestionTranslation()
            {
                await _questionTranslationRepository.ForceDeleteByQuestionVersionId(question.VersionId);
            }

            async Task RollbackInsertAnswer()
            {
                await _answerRepository.ForceDeleteByQuestionVersionId(question.VersionId);
            }

            async Task<ActionResultResponse> InsertQuestionTranslation()
            {
                var questionTranslations = new List<QuestionTranslation>();
                foreach (var questionTranslationMeta in questionMeta.Translations)
                {
                    // Kiểm tra trùng
                    var questionGroupTranslation = await _questionGroupTranslationRepository.GetInfo(question.QuestionGroupId, questionTranslationMeta.LanguageId);
                    if (questionGroupTranslation == null)
                        continue;

                    var questionTranslation = new QuestionTranslation
                    {
                        TenantId = tenantId,
                        LanguageId = questionTranslationMeta.LanguageId,
                        QuestionVersionId = question.VersionId,
                        Name = questionTranslationMeta.Name,
                        Content = questionTranslationMeta.Content,
                        Explain = questionTranslationMeta.Explain,
                        UnsignName = questionTranslationMeta.Name.Trim().StripVietnameseChars().ToUpper(),
                        GroupName = questionGroupTranslation.Name
                    };
                    questionTranslations.Add(questionTranslation);
                }

                result = await _questionTranslationRepository.Inserts(questionTranslations);
                if (result < 0)
                    await RollbackInsert();

                return await InsertAnswers();
            }

            async Task<ActionResultResponse> InsertAnswers()
            {
                switch (questionMeta.Type)
                {
                    case QuestionType.Logic:
                    case QuestionType.SingleChoice:
                    case QuestionType.MultiChoice:
                    case QuestionType.Rating:
                        if (questionMeta.Answers != null)
                        {
                            var listAnswers = new List<Answer>();
                            var listAnswerTranslations = new List<AnswerTranslation>();
                            foreach (var answerMeta in questionMeta.Answers)
                            {
                                if (answerMeta.Translations.Count(x => string.IsNullOrEmpty(x.Name?.Trim())) == answerMeta.Translations.Count)
                                    continue;

                                var answer = new Answer
                                {
                                    Id = Guid.NewGuid().ToString(),
                                    QuestionVersionId = question.VersionId,
                                    IsCorrect = answerMeta.IsCorrect,
                                    Order = answerMeta.Order,
                                    CreatorFullName = creatorFullName,
                                    CreatorId = creatorId,
                                    LastUpdateFullName = creatorFullName,
                                };

                                listAnswers.Add(answer);
                                listAnswerTranslations.AddRange(answerMeta.Translations.Select(answerTranslation => new AnswerTranslation
                                {
                                    AnswerId = answer.Id,
                                    LanguageId = answerTranslation.LanguageId,
                                    Name = answerTranslation.Name?.Trim(),
                                    Explain = answerTranslation.Explain?.Trim()
                                }));
                            }

                            var resultInsertAnswer = await _answerRepository.Insert(listAnswers);
                            if (resultInsertAnswer <= 0)
                            {
                                await RollbackInsertAnswer();
                                await RollbackInsertQuestionTranslation();
                                await RollbackInsert();
                                return new ActionResultResponse(-1, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
                            }

                            result = await _answerTranslationRepository.Inserts(listAnswerTranslations);
                            if (result > 0)
                                return new ActionResultResponse(1, _surveyResourceService.GetString("Add new question successful."));

                            await RollbackInsertQuestionTranslation();
                            await RollbackInsertAnswer();
                            await RollbackInsert();
                            return new ActionResultResponse(-1, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
                        }
                        break;
                    default:
                        return new ActionResultResponse(1, _surveyResourceService.GetString("Add new question successful."));
                }

                return new ActionResultResponse(-1, _surveyResourceService.GetString("Please select question type."));
            }
        }

        public async Task<SearchResult<QuestionViewModel>> Search(string tenantId, string languageId, string keyword,
            QuestionType? questionType, int? questionGroupId, string creatorId, string currentUserId, QuestionStatus? questionStatus,
            int page, int pageSize)
        {
            var items = await _questionRepository.Search(tenantId, languageId, keyword, questionType,
                questionGroupId, creatorId, currentUserId, questionStatus, page, pageSize, out var totalRows);
            return new SearchResult<QuestionViewModel>
            {
                Items = items,
                TotalRows = totalRows,
            };
        }

        public async Task<SearchResult<QuestionViewModel>> SearchQuestionForApprove(string tenantId, string languageId,
            string userId, string keyword, QuestionType? questionType, int? questionGroupId, string creatorId,
            int page, int pageSize)
        {
            var isApprover = await _questionApproverConfigRepository.CheckExistsUserId(tenantId, userId);
            if (!isApprover)
                return new SearchResult<QuestionViewModel>
                {
                    Code = -1,
                    Message = _surveyResourceService.GetString("Approver User is not exists"),
                };

            var items = await _questionRepository.SearchForApprove(tenantId, languageId, keyword, questionType,
                questionGroupId, creatorId, page, pageSize, out var totalRows);
            return new SearchResult<QuestionViewModel>
            {
                Items = items,
                TotalRows = totalRows,
            };
        }

        public async Task<ActionResultResponse> Update(string tenantId, string versionId, string questionId, bool isSend,
            string creatorId, string creatorFullName, string creatorAvatar, QuestionMeta questionMeta)
        {
            var question = await _questionRepository.GetInfo(tenantId, questionId);
            if (question == null)
                return new ActionResultResponse<QuestionDetailViewModel>(-1,
                    _surveyResourceService.GetString("Question does not exists."));

            if (question.ToDate.HasValue)
                return await Insert(tenantId, questionId, isSend, creatorId, creatorFullName, creatorAvatar, questionMeta);

            question.ToDate = DateTime.Now;
            await _questionRepository.Update(question);
            return await Insert(tenantId, questionId, isSend, creatorId, creatorFullName, creatorAvatar, questionMeta);
        }

        public async Task<ActionResultResponse<QuestionDetailViewModel>> GetDetail(string tenantId, string userId, string versionId)
        {
            var questionInfo = await _questionRepository.GetInfo(versionId, true);
            if (questionInfo == null)
                return new ActionResultResponse<QuestionDetailViewModel>(-1,
                    _surveyResourceService.GetString("Question does not exists."));

            if (questionInfo.TenantId != tenantId)
                return new ActionResultResponse<QuestionDetailViewModel>(-2,
                    _surveyResourceService.GetString(ErrorMessage.NotHavePermission));

            var questionTranslations = await _questionTranslationRepository.GetsByQuestionVersionId(versionId);
            var answers = await _answerRepository.GetsByQuestionVersionId(versionId);

            var listAnswers = answers.Select(x => new AnswerViewModel
            {
                Id = x.Id,
                QuestionVersionId = x.QuestionVersionId,
                CreatorId = x.CreatorId,
                LastUpdateUserId = x.LastUpdateUserId,
                IsCorrect = x.IsCorrect,
                IsDelete = x.IsDelete,
                Order = x.Order,
                CreatorFullName = x.CreatorFullName,
                CreateTime = x.CreateTime,
                LastUpdate = x.LastUpdate,
                ConcurrencyStamp = x.ConcurrencyStamp,
                LastUpdateFullName = x.LastUpdateFullName,

            }).ToList();

            foreach (var item in listAnswers)
            {
                var i = await _answerRepository.GetsAnswerTranslationByAnswerId(item.Id);
                item.Translations = i;
            }

            var questionDetail = new QuestionDetailViewModel
            {
                VersionId = questionInfo.VersionId,
                Id = questionInfo.Id,
                QuestionGroupId = questionInfo.QuestionGroupId,
                IsActive = questionInfo.IsActive,
                Status = questionInfo.Status,
                Type = questionInfo.Type,
                Point = questionInfo.Point,
                ConcurrencyStamp = questionInfo.ConcurrencyStamp,
                TotalAnswer = questionInfo.TotalAnswer,
                DeclineReason = questionInfo.DeclineReason,
                CreatorId = questionInfo.CreatorId,
                Translations = questionTranslations.Select(x => new QuestionTranslationViewModel
                {
                    TenantId = x.TenantId,
                    LanguageId = x.LanguageId,
                    QuestionVersionId = x.QuestionVersionId,
                    Name = x.Name,
                    Content = x.Content,
                    Explain = x.Explain,
                    UnsignName = x.UnsignName,
                    QuestionGroupName = x.GroupName,
                }).ToList(),
                Answers = listAnswers.OrderBy(c => c.Order).ToList(),
                IsApprover = await _questionApproverConfigRepository.CheckExistsUserId(tenantId, userId)
            };

            return new ActionResultResponse<QuestionDetailViewModel>
            {
                Code = 1,
                Data = questionDetail
            };
        }

        private async Task<int> UpdateTotalQuestionInQuestionGroup(int questionGroupId)
        {
            var questionCount = await _questionRepository.GetCountQuestionByGroupQuestionId(questionGroupId);
            return await _questionGroupRepository.UpdateTotalQuestion(questionGroupId, questionCount);
        }

        /// <summary>
        /// Danh sách câu hỏi theo người duyệt
        /// </summary>
        /// <param name="tenantId">Mã khách hàng.</param>
        /// <param name="approverUserId">Mã người phê duyệt.</param>
        /// <param name="languageId">Mã ngôn ngữ.</param>
        /// <returns></returns>
        public async Task<SearchResult<QuestionViewModel>> GetAllQuestionByApproverUserId(string tenantId,
            string approverUserId, string languageId)
        {
            var isApproverExists = await _questionApproverConfigRepository.CheckExistsUserId(tenantId, approverUserId);
            if (!isApproverExists)
                return new SearchResult<QuestionViewModel>
                {
                    Code = -1,
                    Message = _surveyResourceService.GetString("Approver does not exists")
                };

            var items = await _questionRepository.GetAllQuestionByApproverUserId(tenantId, approverUserId, languageId);
            return new SearchResult<QuestionViewModel>
            {
                Items = items
            };
        }

        public async Task<SearchResult<QuestionSuggestionViewModel>> SearchForSuggestion(string tenantId, string languageId, string keyword,
            int? surveyGroupId, QuestionType? type, SurveyType? surveyType, int page, int pageSize)
        {
            var questions = await _questionRepository.SearchForSuggestion(tenantId, languageId, keyword, surveyGroupId, type, surveyType,
                page, pageSize, out var totalRows);
            if (questions == null || !questions.Any())
                return new SearchResult<QuestionSuggestionViewModel>
                {
                    Items = questions,
                    TotalRows = totalRows
                };

            var answers =
                await _answerRepository.GetsByQuestionVersionIds(questions.Select(x => x.Id).ToList(), languageId);
            if (answers == null || !answers.Any())
                return new SearchResult<QuestionSuggestionViewModel>
                {
                    Items = questions,
                    TotalRows = totalRows
                };

            foreach (var question in questions)
            {
                question.Answers = answers.Where(x => x.QuestionVersionId == question.Id).ToList();
            }

            return new SearchResult<QuestionSuggestionViewModel>
            {
                Items = questions,
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse> Delete(string userid, string questionVersionId)
        {
            var isExists = await _surveyQuestionUserRepository.CheckByQuestionVersionId(questionVersionId);
            if (isExists)
                return new ActionResultResponse(-1, _surveyResourceService.GetString("Question is used"));

            var item = await _questionRepository.GetInfo(questionVersionId);

            if (item.Status != QuestionStatus.Draft && item.Status != QuestionStatus.Decline)
                return new ActionResultResponse(-1, _surveyResourceService.GetString("Question is pending or approve"));

            if (item.CreatorId != userid)
                return new ActionResultResponse(-1, _surveyResourceService.GetString("User d"));

            var result = await _questionRepository.Delete(questionVersionId);
            if (result <= 0)
                return new ActionResultResponse(-1, _surveyResourceService.GetString("Question version is used"));

            await UpdateTotalQuestionInQuestionGroup(item.QuestionGroupId);

            return new ActionResultResponse(result, _surveyResourceService.GetString("Question version is deleted"));
        }

        public async Task<ActionResultResponse> DeleteListQuestion(List<string> questionVersionIds,
            bool isReadOnly = false)
        {
            foreach (var questionVersionId in questionVersionIds)
            {
                var result = await _questionRepository.Delete(questionVersionId);
                if (result < 0)
                    return new ActionResultResponse(result,
                        _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
            }
            return new ActionResultResponse(1, _surveyResourceService.GetString("Question version is deleted"));
        }

        public async Task<ActionResultResponse> ChangeQuestionStatus(string tenantId, string questionVersionId,
            QuestionStatus questionStatus, string reason, string userId, string fullName, string avatar)
        {
            var isExists = await _surveyQuestionUserRepository.CheckByQuestionVersionId(questionVersionId);
            if (isExists)
                return new ActionResultResponse(-1, _surveyResourceService.GetString("Question version is used."));

            var questionInfo = await _questionRepository.GetInfoRecord(questionVersionId);
            if (questionInfo == null)
                return new ActionResultResponse(-2, _surveyResourceService.GetString("Question does not exists."));

            if (questionInfo.Status == QuestionStatus.Approved)
                return new ActionResultResponse(-3,
                    _surveyResourceService.GetString("Question has been approved. You can not change status of this question."));

            if (questionInfo.Status == questionStatus)
                return new ActionResultResponse(-4,
                    _surveyResourceService.GetString("Please select status."));

            if (questionInfo.TenantId != tenantId)
                return new ActionResultResponse(-5,
                    _surveyResourceService.GetString(ErrorMessage.NotHavePermission));

            if (questionStatus == QuestionStatus.Decline && string.IsNullOrEmpty(reason))
                return new ActionResultResponse(-6,
                    _surveyResourceService.GetString("Please enter decline reason."));

            // Check is has approve permission.
            if (questionStatus == QuestionStatus.Approved || questionStatus == QuestionStatus.Decline)
            {
                var isApprover = await _questionApproverConfigRepository.CheckExistsUserId(tenantId, userId);
                if (!isApprover)
                    return new ActionResultResponse(-6,
                    _surveyResourceService.GetString(ErrorMessage.NotHavePermission));
            }

            var oldStatus = questionInfo.Status;
            questionInfo.Status = questionStatus;
            if (questionStatus == QuestionStatus.Decline)
            {
                questionInfo.DeclineReason = reason;
            }
            var result = await _questionRepository.ChangeQuestionStatus(questionVersionId, questionStatus);
            if (result <= 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            // Send notification to approver.
            if ((oldStatus == QuestionStatus.Draft || oldStatus == QuestionStatus.Decline)
                && questionStatus == QuestionStatus.Pending)
            {
                await SendNotificationToApprover(tenantId, userId, fullName, avatar, questionVersionId);
            }

            // Send notification to creator.
            if ((oldStatus == QuestionStatus.Pending || oldStatus == QuestionStatus.Decline)
                && (questionStatus == QuestionStatus.Approved || questionStatus == QuestionStatus.Decline))
            {
                var isApprove = questionStatus == QuestionStatus.Approved;
                SendApproveMessageToCreator(tenantId, userId, fullName, avatar, questionVersionId, questionInfo.CreatorId, isApprove);
            }

            await UpdateTotalQuestionInQuestionGroup(questionInfo.QuestionGroupId);

            return new ActionResultResponse(result, _surveyResourceService.GetString("Change question status successful."));
        }

        public async Task<List<ActionResultResponse>> ChangeListQuestionStatus(string tenantId, List<string> questionVersionIds,
            QuestionStatus questionStatus, string reason, string userId, string fullName, string avatar)
        {
            var listResult = new List<ActionResultResponse>();
            foreach (var questionVersionId in questionVersionIds)
            {
                var result = await ChangeQuestionStatus(tenantId, questionVersionId, questionStatus, reason, userId,
                    fullName, avatar);
                listResult.Add(result);
            }
            return listResult;
        }

        public async Task<ActionResultResponse> UpdateDeclineReason(string questionVersionId, string declineReason)
        {
            var questionInfo = _questionRepository.GetInfoRecord(questionVersionId);
            if (questionInfo == null)
                _sharedResourceService.GetString("Question does not exists");

            var result = await _questionRepository.UpdateDeclineReason(questionVersionId, declineReason);
            if (result <= 0)
                return new ActionResultResponse(result,
                      _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            return new ActionResultResponse(result, _sharedResourceService.GetString("Update status success"));
        }

        private async Task SendNotificationToApprover(string tenantId, string creatorId, string creatorFullName, string creatorAvatar,
            string questionVersionId)
        {
            // Check if is send. Send question to approve for approve.
            var listApprover = await _questionApproverConfigRepository.GetAllApprover(tenantId);
            var notificationHelper = new NotificationHelper();
            foreach (var approver in listApprover)
            {
                var notification = new Events.Notifications
                {
                    TenantId = tenantId,
                    Title = $"<b>{creatorFullName}</b> {{send a question to you for approve.}}",
                    Content = "",
                    SenderId = creatorId,
                    SenderFullName = creatorFullName,
                    SenderAvatar = creatorAvatar,
                    Url = $"/surveys/questions/{questionVersionId}",
                    ReceiverId = approver.UserId,
                    Type = (int)NotificationType.Info,
                    IsSystem = false
                };
                notificationHelper.Send(notification);
            }

        }

        private void SendApproveMessageToCreator(string tenantId, string senderId, string senderFullName, string senderAvatar,
            string questionVersionId, string receiverId, bool isApprove)
        {
            // TODO: Update for send with rabbitMQ later.
            var message = $"<b>{senderFullName}</b> {{{(isApprove ? "approved" : "declined")} your question.}}";
            var notification = new Events.Notifications
            {
                TenantId = tenantId,
                Title = message,
                Content = "",
                SenderId = senderId,
                SenderFullName = senderFullName,
                SenderAvatar = senderAvatar,
                Url = $"/surveys/questions/{questionVersionId}",
                ReceiverId = receiverId,
                Type = (int)NotificationType.Info,
                IsSystem = false
            };
            new NotificationHelper().Send(notification);
        }
    }
}