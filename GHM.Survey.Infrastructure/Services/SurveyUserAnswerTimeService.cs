﻿using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.IServices;
using GHM.Survey.Domain.Resources;
using System;
using System.Threading.Tasks;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Constants;
using System.Collections.Generic;

namespace GHM.Survey.Infrastructure.Services
{
    public class SurveyUserAnswerTimeService : ISurveyUserAnswerTimeService
    {
        private readonly ISurveyUserAnswerTimeRepository _surveyUserAnswerTimeRepository;
        private readonly ISurveyQuestionUserRepository _surveyQuestionUserRepository;
        private readonly ISurveyUserAnswerRepository _surveyUserAnswerRepository;
        private readonly ISurveyUserRepository _surveyUserRepository;
        private readonly ISurveyRepository _surveyRepository;

        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmSurveyResource> _surveyResourceService;
        public SurveyUserAnswerTimeService(ISurveyUserAnswerTimeRepository surveyuseranswertimeRepository,
            ISurveyQuestionUserRepository surveyQuestionUsersRepository,
            ISurveyUserAnswerRepository surveyUserAnswerRepository, ISurveyRepository surveyRepository,
            ISurveyUserRepository surveyUserRepository,
            IResourceService<SharedResource> sharedResourceService, IResourceService<GhmSurveyResource> surveyResourceService)
        {
            _surveyUserAnswerTimeRepository = surveyuseranswertimeRepository;
            _surveyQuestionUserRepository = surveyQuestionUsersRepository;
            _surveyUserAnswerRepository = surveyUserAnswerRepository;
            _surveyRepository = surveyRepository;
            _surveyUserRepository = surveyUserRepository;
            _sharedResourceService = sharedResourceService;
            _surveyResourceService = surveyResourceService;
        }

        public async Task<bool> CheckExistsBySurveyIdAndSurveyUserId(string surveyId, string surveyUserId)
        {
            return await _surveyUserAnswerTimeRepository.CheckExistsBySurveyIdAndSurveyUserId(surveyId, surveyUserId);
        }

        public async Task<ActionResultResponse> Delete(string surveyUserAnswerTimesId)
        {
            var surveyuseranswertimeinfo = await _surveyUserAnswerTimeRepository.GetInfo(surveyUserAnswerTimesId);
            if (surveyuseranswertimeinfo == null)
                return new ActionResultResponse(-1, _sharedResourceService.GetString("SurveyUserAnswerTime does not exists."));

            var result = await _surveyQuestionUserRepository.ForceDelete(surveyuseranswertimeinfo.SurveyUserId, surveyUserAnswerTimesId);
            if (result <= 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            result = await _surveyUserAnswerRepository.ForceDeletes(surveyuseranswertimeinfo.SurveyId, surveyUserAnswerTimesId);
            if (result <= 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            result = await _surveyUserAnswerTimeRepository.ForceDelete(surveyUserAnswerTimesId);

            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                : _surveyResourceService.GetString("Delete SurveyUserAnswerTime successful."));
        }

        public async Task<int> FinishDoExam(string surveyId, string surveyUserId, string surveyUserAnswerTimeId)
        {
            return await _surveyUserAnswerTimeRepository.UpdateTotalAnswer(surveyId, surveyUserId, surveyUserAnswerTimeId);
        }

        public async Task<int> GetSurveyTime(string surveyUserAnswerTimesId)
        {
            var surveyUserAnswerTimesInfo =
                await _surveyUserAnswerTimeRepository.GetInfo(surveyUserAnswerTimesId, true);
            if (surveyUserAnswerTimesInfo == null)
                return -1;

            var survey = await _surveyRepository.GetInfo(surveyUserAnswerTimesInfo.SurveyId);
            if (survey == null)
                return -2;

            if (survey.LimitedTime == null)
                return -3;

            var totalTime = survey.LimitedTime * 60;
            var totalUsedTime = (int)(DateTime.Now - surveyUserAnswerTimesInfo.StartTime).TotalSeconds;
            var remainingTime = totalTime - totalUsedTime;
            return remainingTime ?? 0;
        }

        public async Task<ActionResultResponse> UpdateIsViewResult(string tenantId, string surveyId, string surveyUserId, string surveyUserAnswerTimeId, bool isViewResult)
        {
            var surveyInfo = await _surveyRepository.GetInfo(tenantId, surveyId, true);
            if (surveyInfo == null)
                return new ActionResultResponse(-1, _surveyResourceService.GetString("Survey does not exists."));

            // Check user is assign to survey.
            var surveyUser = await _surveyUserRepository.GetInfo(surveyUserId, true);
            if (surveyUser == null)
                return new ActionResultResponse(-2, _surveyResourceService.GetString("User does not in survey."));

            var surveyUserAnswerTimeInfo = await _surveyUserAnswerTimeRepository.GetInfo(surveyUserAnswerTimeId);
            if (surveyUserAnswerTimeInfo == null)
                return new ActionResultResponse(-3, _surveyResourceService.GetString("Exam does not exists."));

            surveyUserAnswerTimeInfo.IsViewResult = isViewResult;
            var result = await _surveyUserAnswerTimeRepository.Update(surveyUserAnswerTimeInfo);

            return new ActionResultResponse<string>(result, result > 0
                       ? _surveyResourceService.GetString("Update question successful.")
                       : _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
        }

        public async Task<ActionResultResponse> UpdateAllIsViewResult(List<string> surveyUserAnswerTimesIds, bool isViewResult)
        {
            var listSurveyUserAnswerTimeInfo = await _surveyUserAnswerTimeRepository.GetByListSurveyUserTimesIds(surveyUserAnswerTimesIds);
            if (listSurveyUserAnswerTimeInfo == null)
                return new ActionResultResponse(-3, _surveyResourceService.GetString("Please select exam"));

            foreach (var surveyUserAnswerTimeInfo in listSurveyUserAnswerTimeInfo)
            {
                surveyUserAnswerTimeInfo.IsViewResult = isViewResult;
                var result = await _surveyUserAnswerTimeRepository.Update(surveyUserAnswerTimeInfo);
            }

            return new ActionResultResponse<string>(1, _surveyResourceService.GetString("Update status successful."));
        }
    }
}
