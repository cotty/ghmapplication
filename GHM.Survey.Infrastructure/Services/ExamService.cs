﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Threading.Tasks;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Survey.Domain.Constants;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.IServices;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.Resources;
using GHM.Survey.Domain.ViewModels;

namespace GHM.Survey.Infrastructure.Services
{
    public class ExamService : IExamService
    {
        private readonly ISurveyRepository _surveyRepository;
        private readonly ISurveyQuestionRepository _surveyQuestionRepository;
        private readonly ISurveyTranslationRepository _surveyTranslationRepository;
        private readonly ISurveyUserRepository _surveyUserRepository;
        private readonly ISurveyUserAnswerTimeRepository _surveyUserAnswerTimeRepository;
        private readonly ISurveyQuestionUserRepository _surveyQuestionUserRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmSurveyResource> _resourceService;
        private readonly ISurveyUserAnswerRepository _surveyUserAnswerRepository;
        private readonly IAnswerRepository _answerRepository;
        private readonly IQuestionRepository _questionRepository;
        private readonly ISurveyQuestionGroupRepository _surveyQuestionGroupRepository;

        public ExamService(ISurveyUserAnswerTimeRepository surveyUserAnswerTimeRepository, ISurveyRepository surveyRepository,
            IResourceService<SharedResource> sharedResourceService, IResourceService<GhmSurveyResource> resourceService,
            ISurveyUserRepository surveyUserRepository, ISurveyTranslationRepository surveyTranslationRepository,
            ISurveyQuestionUserRepository surveyQuestionUserRepository, ISurveyUserAnswerRepository surveyUserAnswerRepository,
            IQuestionRepository questionRepository, IAnswerRepository answerRepository,
            ISurveyQuestionRepository surveyQuestionRepository, ISurveyQuestionGroupRepository surveyQuestionGroupRepository)
        {
            _surveyUserAnswerTimeRepository = surveyUserAnswerTimeRepository;
            _surveyRepository = surveyRepository;
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
            _surveyUserRepository = surveyUserRepository;
            _surveyTranslationRepository = surveyTranslationRepository;
            _surveyQuestionUserRepository = surveyQuestionUserRepository;
            _surveyUserAnswerRepository = surveyUserAnswerRepository;
            _questionRepository = questionRepository;
            _answerRepository = answerRepository;
            _surveyQuestionRepository = surveyQuestionRepository;
            _surveyQuestionGroupRepository = surveyQuestionGroupRepository;
        }

        public async Task<ActionResultResponse<ExamOverviewViewModel>> GetOverview(string tenantId, string languageId, string surveyId,
            string userId)
        {
            var surveyInfo = await _surveyRepository.GetInfo(surveyId, true);
            if (surveyInfo == null)
                return new ActionResultResponse<ExamOverviewViewModel>(-1,
                    _resourceService.GetString("Survey does not exists."));

            if (surveyInfo.TenantId != tenantId)
                return new ActionResultResponse<ExamOverviewViewModel>(-2, _resourceService.GetString(ErrorMessage.NotHavePermission));

            // Check user exists in survey.
            var surveyUserInfo = await _surveyUserRepository.GetInfo(surveyId, userId, true);
            if (surveyUserInfo == null)
                return new ActionResultResponse<ExamOverviewViewModel>(-5,
                    _resourceService.GetString("You not have permission to access this survey. Please contact with administrator"));

            if (!surveyInfo.IsActive)
                return new ActionResultResponse<ExamOverviewViewModel>(-3,
                    _resourceService.GetString("Survey inactive. You can not access this survey. Please contact with administrator."));

            if ((surveyInfo.StartDate.HasValue && DateTime.Compare(DateTime.Now, surveyInfo.StartDate.Value) < 0)
                || (surveyInfo.EndDate.HasValue && DateTime.Compare(DateTime.Now, surveyInfo.EndDate.Value) > 0))
                return new ActionResultResponse<ExamOverviewViewModel>(-4,
                    _resourceService.GetString("The survey isn't start or has been ended. You can not access this survey."));

            var surveyTranslation = await _surveyTranslationRepository.GetsBySurveyId(surveyInfo.Id);
            if (surveyTranslation == null || !surveyTranslation.Any())
                return new ActionResultResponse<ExamOverviewViewModel>(-6,
                    _resourceService.GetString("Survey does not exists. Please contact with administrator."));

            var surveyByLanguage = surveyTranslation.FirstOrDefault(x => x.LanguageId == languageId);
            if (surveyByLanguage == null)
                surveyByLanguage = surveyTranslation.FirstOrDefault();

            if (surveyByLanguage == null)
                return new ActionResultResponse<ExamOverviewViewModel>(-8,
                    _resourceService.GetString("Survey does not exists. Please contact with administrator."));

            var examOverView = new ExamOverviewViewModel
            {
                SurveyId = surveyInfo.Id,
                SurveyUserId = surveyUserInfo.Id,
                SurveyName = surveyByLanguage.Name,
                StartDate = surveyInfo.StartDate,
                EndDate = surveyInfo.EndDate,
                TotalQuestion = surveyInfo.TotalQuestion,
                LimitedTime = surveyInfo.LimitedTime,
                LimitedTimes = surveyInfo.LimitedTimes,
                SurveyDescription = surveyByLanguage.Description,
                IsRequired = surveyInfo.IsRequire,
                SurveyUserAnswerTimes =
                    await _surveyUserAnswerTimeRepository.GetBySurveyIdAndUserId(surveyInfo.Id, surveyUserInfo.Id)
            };

            return new ActionResultResponse<ExamOverviewViewModel>
            {
                Data = examOverView,
                Code = 1,
            };
        }

        public async Task<ActionResultResponse<DoExamViewModel>> Start(string tenantId, string languageId, string surveyId, string userId)
        {
            // Get survey info.
            var surveyInfo = await _surveyRepository.GetInfo(tenantId, surveyId, true);
            if (surveyInfo == null)
                return new ActionResultResponse<DoExamViewModel>(-1, _resourceService.GetString("Survey does not exists."));

            // Check user is assign to survey.
            var surveyUser = await _surveyUserRepository.GetInfo(surveyId, userId, true);
            if (surveyUser == null)
                return new ActionResultResponse<DoExamViewModel>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var totalSurveyQuestion = await _surveyQuestionRepository.GetTotalQuestion(surveyId);
            var listQuestionGroup = await _surveyQuestionGroupRepository.GetsBySurveyId(surveyId, languageId);
            if (totalSurveyQuestion == 0 && (listQuestionGroup == null || listQuestionGroup.Count == 0))
                return new ActionResultResponse<DoExamViewModel>(-404, _resourceService.GetString("Survey haven't question."));

            // Get current active times.
            var surveyUserAnswerTimes =
                await _surveyUserAnswerTimeRepository.GetCurrentActiveSurveyTimes(surveyId, surveyUser.Id, true);

            // In case survey user times is null. Create new survey user times.
            var surveyUserTimeId = Guid.NewGuid().ToString();
            if (surveyUserAnswerTimes == null)
            {
                // Get total survey user times has ended.
                var totalSurveyUserTimes =
                    await _surveyUserAnswerTimeRepository.GetTotalSurveyUserAnswerTimes(surveyId, surveyUser.Id);

                if (totalSurveyUserTimes >= surveyInfo.LimitedTimes)
                    return new ActionResultResponse<DoExamViewModel>(-2,
                        _resourceService.GetString("Your survey has been ended. You can not access this survey."));

                surveyUserAnswerTimes = new SurveyUserAnswerTime
                {
                    Id = surveyUserTimeId,
                    SurveyUserId = surveyUser.Id,
                    SurveyId = surveyInfo.Id,
                    StartTime = DateTime.Now,
                    TotalCorrectAnswers = 0,
                    TotalCorrectScores = 0,
                    TotalScores = 0,
                };

                var resultInsertSurveyUserTimes =
                    await _surveyUserAnswerTimeRepository.Insert(surveyUserAnswerTimes);

                if (resultInsertSurveyUserTimes < 0)
                    return new ActionResultResponse<DoExamViewModel>(-2,
                        _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

                // If not prerendering, create survey question for user.
                if (!surveyInfo.IsPreRendering)
                {
                    // Create SurveyQuestionUser.
                    var isRandomQuestionSuccess = await _surveyQuestionUserRepository.RanDomQuestion(surveyId, userId, surveyUserTimeId);
                    if (isRandomQuestionSuccess < 0)
                    {
                        await RollbackInsertSurveyUserAnswerTimes();
                        return new ActionResultResponse<DoExamViewModel>(-3,
                            _resourceService.GetString("Can not generate question. Please contact with administrator."));
                    }
                }
            }
            else
            {
                surveyUserTimeId = surveyUserAnswerTimes.Id;
                if (surveyUserAnswerTimes.EndTime.HasValue)
                    return new ActionResultResponse<DoExamViewModel>(-403, _resourceService.GetString("This exam have end."));
            }

            // Get list question in this survey.
            List<SurveyUserQuestionAnswer> surveyQuestions;
            if (surveyInfo.Type == SurveyType.Normal)
            {
                surveyQuestions = await _surveyRepository.GetSurveyUserQuestionAnswer(tenantId, languageId,
                    surveyInfo.Id, surveyUser.Id, surveyUserTimeId, surveyInfo.IsPreRendering);
            }
            else
            {
                surveyQuestions = await _surveyRepository.GetSurveyUserQuestionLogicAnswer(languageId,
                                surveyInfo.Id, surveyUser.Id, surveyUserTimeId);
            }

            var doExamViewModel = new DoExamViewModel
            {
                SurveyId = surveyInfo.Id,
                SurveyName = await _surveyTranslationRepository.GetSurveyName(surveyInfo.Id, languageId),
                SurveyUserId = surveyUser.Id,
                OfficeName = surveyUser.OfficeName,
                PositionName = surveyUser.PositionName,
                StartTime = surveyUserAnswerTimes.StartTime,
                EndTime = surveyUserAnswerTimes.EndTime,
                TotalSeconds = surveyUserAnswerTimes.TotalSeconds,
                SurveyUserAnswerTimeId = surveyUserAnswerTimes.Id,
                SurveyUserQuestionAnswers = surveyQuestions,
                SurveyType = surveyInfo.Type
            };

            if (surveyInfo.LimitedTime.HasValue && !surveyUserAnswerTimes.EndTime.HasValue)
            {
                doExamViewModel.TotalSeconds = surveyInfo.LimitedTime.Value * 60 -
                                  (int)(DateTime.Now - surveyUserAnswerTimes.StartTime).TotalSeconds;
            }

            return new ActionResultResponse<DoExamViewModel>
            {
                Data = doExamViewModel
            };

            async Task RollbackInsertSurveyUserAnswerTimes()
            {
                await _surveyUserAnswerTimeRepository.ForceDelete(surveyUserTimeId);
            }
        }

        public async Task<ActionResultResponse<string>> SaveAnswer(string userId, UserAnswerMeta userAnswerMeta)
        {
            var surveyInfo = await _surveyRepository.GetInfo(userAnswerMeta.SurveyId, true);
            if (surveyInfo == null)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Survey does not exists."));

            var surveyUserInfo = await _surveyUserRepository.GetInfo(userAnswerMeta.SurveyId, userId, true);
            if (surveyUserInfo == null)
                return new ActionResultResponse<string>(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var surveyUserAnswerTime = await _surveyUserAnswerTimeRepository.GetCurrentActiveSurveyTimes(
                userAnswerMeta.SurveyId,
                surveyUserInfo.Id, true);

            if (surveyUserAnswerTime == null)
                return new ActionResultResponse<string>(-3,
                    _resourceService.GetString("Your survey does not exists."));

            var questionInfo = await _questionRepository.GetInfo(userAnswerMeta.QuestionVersionId, true);
            if (questionInfo == null)
                return new ActionResultResponse<string>(-4, _resourceService.GetString("Question does not exists."));

            if ((questionInfo.Type == QuestionType.SingleChoice || questionInfo.Type == QuestionType.MultiChoice
                || questionInfo.Type == QuestionType.Rating) && string.IsNullOrEmpty(userAnswerMeta.AnswerId))
                return new ActionResultResponse<string>(-6, _sharedResourceService.GetString("Invalid value."));

            var surveyUserAnswerId = Guid.NewGuid().ToString();
            switch (questionInfo.Type)
            {
                case QuestionType.Essay:
                    if (string.IsNullOrEmpty(userAnswerMeta.Value))
                        return new ActionResultResponse<string>(-10, _resourceService.GetString("Please enter your answer content."));

                    if (string.IsNullOrEmpty(userAnswerMeta.SurveyUserAnswerId))
                    {
                        // Check question version exists.
                        var essayUserAnswerInfo = await _surveyUserAnswerRepository.GetInfo(userAnswerMeta.SurveyId,
                            userAnswerMeta.QuestionVersionId,
                            surveyUserInfo.Id, surveyUserAnswerTime.Id);

                        if (essayUserAnswerInfo == null)
                        {
                            // Add new essay answer.
                            var resultInsertEssayAnswer = await _surveyUserAnswerRepository.Insert(new SurveyUserAnswer
                            {
                                Id = surveyUserAnswerId,
                                SurveyId = surveyInfo.Id,
                                Value = userAnswerMeta.Value,
                                QuestionGroupId = questionInfo.QuestionGroupId,
                                QuestionVersionId = userAnswerMeta.QuestionVersionId,
                                AnswerTime = DateTime.Now,
                                SurveyUserId = surveyUserInfo.Id,
                                SurveyUserAnswerTimesId = surveyUserAnswerTime.Id,
                            });
                            return new ActionResultResponse<string>
                            {
                                Code = resultInsertEssayAnswer,
                                Message = resultInsertEssayAnswer > 0
                                    ? _resourceService.GetString("Add new answer successful.")
                                    : resultInsertEssayAnswer == 0
                                    ? _resourceService.GetString("Nothing change.")
                                    : _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong),
                                Data = surveyUserAnswerId
                            };
                        }

                        essayUserAnswerInfo.Value = userAnswerMeta.Value;
                        var resultUpdateEssayAnswer = await _surveyUserAnswerRepository.Update(essayUserAnswerInfo);
                        return new ActionResultResponse<string>(resultUpdateEssayAnswer, resultUpdateEssayAnswer > 0
                            ? _resourceService.GetString("Update question successful.")
                            : _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
                    }

                    var userAnswerInfo = await _surveyUserAnswerRepository.GetInfo(userAnswerMeta.SurveyUserAnswerId);
                    if (userAnswerInfo == null)
                        return new ActionResultResponse<string>(-11,
                            _resourceService.GetString("Sorry your answer not found. Please contact with administrator."));

                    userAnswerInfo.Value = userAnswerMeta.Value;
                    var resultUpdateUserAnswer = await _surveyUserAnswerRepository.Update(userAnswerInfo);
                    return new ActionResultResponse<string>(resultUpdateUserAnswer, resultUpdateUserAnswer > 0
                        ? _resourceService.GetString("Update question successful.")
                        : _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
                case QuestionType.MultiChoice:
                    // Check user answer exists.
                    var isSurveyUserAnswerExists = await _surveyUserAnswerRepository.CheckExists(
                        userAnswerMeta.SurveyId, userAnswerMeta.QuestionVersionId, surveyUserInfo.Id,
                        userAnswerMeta.AnswerId);

                    // If answer already selected remove this answer.
                    if (isSurveyUserAnswerExists)
                    {
                        var resultRemoveUserAnswer = await _surveyUserAnswerRepository.Delete(userAnswerMeta.SurveyId,
                            userAnswerMeta.QuestionVersionId, surveyUserInfo.Id, userAnswerMeta.AnswerId);

                        return new ActionResultResponse<string>(resultRemoveUserAnswer, resultRemoveUserAnswer > 0
                            ? _resourceService.GetString("Update question successful.")
                            : _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
                    }

                    var multiChoiceAnswerInfo = await _answerRepository.GetInfo(userAnswerMeta.AnswerId);
                    if (multiChoiceAnswerInfo == null)
                        return new ActionResultResponse<string>(-12,
                            _resourceService.GetString("Answer does not exists. Please contact with administrator."));

                    var resultInsertUserAnswerMultiChoice = await _surveyUserAnswerRepository.Insert(
                        new SurveyUserAnswer
                        {
                            Id = surveyUserAnswerId,
                            SurveyId = surveyInfo.Id,
                            QuestionGroupId = questionInfo.QuestionGroupId,
                            QuestionVersionId = userAnswerMeta.QuestionVersionId,
                            AnswerTime = DateTime.Now,
                            SurveyUserId = surveyUserInfo.Id,
                            SurveyUserAnswerTimesId = surveyUserAnswerTime.Id,
                            AnswerId = userAnswerMeta.AnswerId,
                            IsCorrect = multiChoiceAnswerInfo.IsCorrect
                        });

                    if (resultInsertUserAnswerMultiChoice <= 0)
                        return new ActionResultResponse<string>(resultInsertUserAnswerMultiChoice, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

                    var countUserAnswerCorrect = await _surveyUserAnswerRepository.GetTotalAnswer(surveyInfo.Id, userAnswerMeta.QuestionVersionId, surveyUserInfo.Id, surveyUserAnswerTime.Id, true);
                    var countUserAnswerInCorrect = await _surveyUserAnswerRepository.GetTotalAnswer(surveyInfo.Id, userAnswerMeta.QuestionVersionId, surveyUserInfo.Id, surveyUserAnswerTime.Id, false);
                    var countAnswerCorrect = await _answerRepository.TotalAnswer(userAnswerMeta.QuestionVersionId, true);
                    var isCorrectMultileChoice = countUserAnswerCorrect == countAnswerCorrect && countUserAnswerInCorrect == 0;

                    await _surveyQuestionUserRepository.UpdateIsCorrect(surveyUserInfo.Id, surveyInfo.Id, surveyUserAnswerTime.Id, userAnswerMeta.QuestionVersionId, isCorrectMultileChoice);

                    return new ActionResultResponse<string>(resultInsertUserAnswerMultiChoice, _resourceService.GetString("Update question successful."));

                case QuestionType.SelfResponded:
                    // Check total survey user answer is over than total question in question info.
                    var totalSurveyUserAnswer = await _surveyUserAnswerRepository.GetTotalAnswer(
                        userAnswerMeta.SurveyId,
                        userAnswerMeta.QuestionVersionId, surveyUserInfo.Id, surveyUserAnswerTime.Id, null);

                    if (string.IsNullOrEmpty(userAnswerMeta.SurveyUserAnswerId))
                    {
                        if (totalSurveyUserAnswer >= questionInfo.TotalAnswer)
                            return new ActionResultResponse<string>(-7, _resourceService.GetString("Your answer is over than allowed."));

                        var result = await _surveyUserAnswerRepository.Insert(
                            new SurveyUserAnswer
                            {
                                Id = surveyUserAnswerId,
                                SurveyId = surveyInfo.Id,
                                QuestionGroupId = questionInfo.QuestionGroupId,
                                QuestionVersionId = userAnswerMeta.QuestionVersionId,
                                AnswerTime = DateTime.Now,
                                SurveyUserId = surveyUserInfo.Id,
                                SurveyUserAnswerTimesId = surveyUserAnswerTime.Id,
                                Value = userAnswerMeta.Value
                            });

                        return new ActionResultResponse<string>
                        {
                            Code = result,
                            Message = result > 0
                                ? _resourceService.GetString("Update question successful.")
                                : _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong),
                            Data = surveyUserAnswerId
                        };
                    }

                    var surveyUserAnswerInfo =
                        await _surveyUserAnswerRepository.GetInfo(userAnswerMeta.SurveyUserAnswerId);
                    if (surveyUserAnswerInfo == null)
                        return new ActionResultResponse<string>(-8,
                            _resourceService.GetString("Sorry your answer not found. Please contact with administrator."));

                    surveyUserAnswerInfo.Value = userAnswerMeta.Value;
                    var resultUpdate = await _surveyUserAnswerRepository.Update(surveyUserAnswerInfo);
                    return new ActionResultResponse<string>(resultUpdate, resultUpdate > 0
                        ? _resourceService.GetString("Update question successful.")
                        : _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
                case QuestionType.SingleChoice:
                case QuestionType.Logic:
                case QuestionType.Rating:
                    // Delete other selected answer.
                    await _surveyUserAnswerRepository.Delete(userAnswerMeta.SurveyId, userAnswerMeta.QuestionVersionId,
                        surveyUserInfo.Id, surveyUserAnswerTime.Id);

                    var answerInfo = await _answerRepository.GetInfo(userAnswerMeta.AnswerId);
                    if (answerInfo == null)
                        return new ActionResultResponse<string>(-12,
                            _resourceService.GetString("Answer does not exists. Please contact with administrator."));

                    // Insert new user answer.
                    var resultInsertSingleChoice = await _surveyUserAnswerRepository.Insert(
                        new SurveyUserAnswer
                        {
                            Id = surveyUserAnswerId,
                            SurveyId = surveyInfo.Id,
                            QuestionGroupId = questionInfo.QuestionGroupId,
                            QuestionVersionId = userAnswerMeta.QuestionVersionId,
                            AnswerTime = DateTime.Now,
                            SurveyUserId = surveyUserInfo.Id,
                            SurveyUserAnswerTimesId = surveyUserAnswerTime.Id,
                            AnswerId = userAnswerMeta.AnswerId,
                            IsCorrect = answerInfo.IsCorrect
                        });

                    if (resultInsertSingleChoice <= 0)
                        return new ActionResultResponse<string>(resultInsertSingleChoice, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

                    await _surveyQuestionUserRepository.UpdateIsCorrect(surveyUserInfo.Id, surveyInfo.Id, surveyUserAnswerTime.Id, userAnswerMeta.QuestionVersionId, answerInfo.IsCorrect);

                    return new ActionResultResponse<string>(resultInsertSingleChoice, _resourceService.GetString("Update question successful."));

                default:
                    return new ActionResultResponse<string>(-9, _resourceService.GetString("Out of range question type."));
            }
        }

        public async Task<ActionResultResponse> Finish(string tenantId, string languageId, string surveyId, string surveyUserId)
        {
            throw new NotImplementedException();
        }

        public async Task<ActionResultResponse<ExamDetailViewModel>> GetExamDetail(string tenantId, string languageId, string surveyId,
            string surveyUserId, string surveyUserTimeId, bool isManager)
        {
            // Get survey info.
            var surveyInfo = await _surveyRepository.GetInfo(tenantId, surveyId, true);
            if (surveyInfo == null)
                return new ActionResultResponse<ExamDetailViewModel>(-1, _resourceService.GetString("Survey does not exists."));

            // Check user is assign to survey.
            var surveyUser = await _surveyUserRepository.GetInfo(surveyUserId, true);
            if (surveyUser == null)
                return new ActionResultResponse<ExamDetailViewModel>(-2, _sharedResourceService.GetString("You not in survey"));

            // In case survey user times is null. Create new survey user times.
            var surveyUserTimeInfo = await _surveyUserAnswerTimeRepository.GetInfo(surveyUserTimeId, true);
            if (surveyUserTimeInfo == null)
                return new ActionResultResponse<ExamDetailViewModel>(-3, _resourceService.GetString("Exam result does not exists."));

            if (surveyUserTimeInfo.EndTime == null)
                return new ActionResultResponse<ExamDetailViewModel>(-4, _resourceService.GetString("The exam has not ended yet."));

            // Get list question in this survey.            
            var listSurveyQuestionDetails = surveyInfo.Type == SurveyType.Normal ? await _surveyRepository.GetSurveyUserQuestionAnswerDetail(tenantId, languageId,
                surveyInfo.Id, surveyUser.Id, surveyUserTimeId, surveyInfo.IsPreRendering, isManager)
                : await _surveyRepository.GetSurveyUserQuestionLogicAnswerDetail(tenantId, languageId,
                surveyInfo.Id, surveyUser.Id, surveyUserTimeId);

            var examDetailViewModel = new ExamDetailViewModel
            {
                SurveyName = await _surveyTranslationRepository.GetSurveyName(surveyInfo.Id, languageId),
                SurveyType = surveyInfo.Type,
                FullName = surveyUser.FullName,
                Avatar = surveyUser.Avatar,
                OfficeName = surveyUser.OfficeName,
                PositionName = surveyUser.PositionName,
                StartTime = surveyUserTimeInfo.StartTime,
                EndTime = surveyUserTimeInfo.EndTime,
                TotalSeconds = surveyUserTimeInfo.TotalSeconds,
                TotalCorrectAnswers = surveyUserTimeInfo.TotalCorrectAnswers,
                IsViewResult = surveyUserTimeInfo.IsViewResult,
                SurveyUserQuestionAnswerDetails = listSurveyQuestionDetails
            };

            return new ActionResultResponse<ExamDetailViewModel>
            {
                Data = examDetailViewModel
            };
        }
    }
}
