﻿using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.IServices;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.Resources;
using GHM.Survey.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Survey.Infrastructure.Services
{
    public class AnswerService : IAnswerService
    {
        private readonly IAnswerRepository _answerRepository;
        private readonly IAnswerTranslationRepository _answerTranslationRepository;

        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmSurveyResource> _surveyResourceService;

        public AnswerService(IAnswerRepository answerRepository, 
            IResourceService<SharedResource> sharedResourceService, IAnswerTranslationRepository answerTranslationRepository,
            IResourceService<GhmSurveyResource> surveyResourceService)
        {
            _answerRepository = answerRepository;
            _answerTranslationRepository = answerTranslationRepository;
            _sharedResourceService = sharedResourceService;
            _surveyResourceService = surveyResourceService;
        }

        public Task<ActionResultResponse> Delete(string answerId)
        {
            throw new NotImplementedException();
        }

        public async Task<ActionResultResponse<AnswerDetailViewModel>> GetDetail(string answerId)
        {
            var answerInfo = await _answerRepository.GetInfo(answerId);
            if (answerInfo == null)
                return new ActionResultResponse<AnswerDetailViewModel>(-1, _surveyResourceService.GetString("Answer subject does not exists."));

            var answerTranslations = await _answerRepository.GetsAnswerTranslationByAnswerId(answerId);
            var answerDetailViewModel = new AnswerDetailViewModel
            {
                Id = answerInfo.Id,
                QuestionVersionId = answerInfo.QuestionVersionId,
                CreatorId = answerInfo.CreatorId,
                LastUpdateUserId = answerInfo.LastUpdateUserId,
                IsCorrect = answerInfo.IsCorrect,
                IsDelete = answerInfo.IsDelete,
                Order = answerInfo.Order,
                CreatorFullName = answerInfo.CreatorFullName,
                CreateTime = answerInfo.CreateTime,
                LastUpdate = answerInfo.LastUpdate,
                ConcurrencyStamp = answerInfo.ConcurrencyStamp,
                LastUpdateFullName = answerInfo.LastUpdateFullName,
                AnswerTranslations = answerTranslations.Select(x => new AnswerTranslationViewModel
                {
                    AnswerId = x.AnswerId,
                    LanguageId = x.LanguageId,
                    Name = x.Name,
                    Explain = x.Explain,
                }).ToList()
            };

            return new ActionResultResponse<AnswerDetailViewModel>
            {
                Code = 1,
                Data = answerDetailViewModel
            };
        }

        public async Task<ActionResultResponse> Insert(AnswerMeta answerMeta)
        {
            if (!answerMeta.Translations.Any())
                return new ActionResultResponse(-1, _sharedResourceService.GetString("Please enter at least one language."));

            var answerId = Guid.NewGuid().ToString();
            var answer = new Answer
            {
                Id = answerMeta.Id,
                QuestionVersionId =  answerMeta.QuestionVersionId,
                //CreatorId = answerMeta.CreatorId,
                //LastUpdateUserId = answerMeta.LastUpdateUserId,
                IsCorrect = answerMeta.IsCorrect,
                //IsDelete = answerMeta.IsDelete,
                Order = answerMeta.Order,
                //CreatorFullName = answerMeta.CreatorFullName,
                //CreateTime = answerMeta.CreateTime,
                //LastUpdate = answerMeta.LastUpdate,
                ConcurrencyStamp = answerMeta.ConcurrencyStamp,
                //LastUpdateFullName = answerMeta.LastUpdateFullName,
            };


            var result = await _answerRepository.Insert(answer);
            if (result <= 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            return new ActionResultResponse(-5, _surveyResourceService.GetString("Can not insert new Answer. Please contact with administrator."));
        }

        public Task<ActionResultResponse> Update(string answerId, AnswerMeta answerMeta)
        {
            throw new NotImplementedException();
        }
    }
}
