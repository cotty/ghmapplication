﻿using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using GHM.Survey.Domain.Constants;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.IServices;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.Resources;
using System;
using System.Threading.Tasks;

namespace GHM.Survey.Infrastructure.Services
{
    public class SurveyUserAnswerService : ISurveyUserAnswerService
    {
        private readonly ISurveyUserAnswerRepository _surveyUserAnswerRepository;
        private readonly ISurveyQuestionUserRepository _surveyQuestionUserRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmSurveyResource> _surveyResourceService;
        private readonly ISurveyUserAnswerTimeRepository _surveyUserAnswerTimeRepository;
        private readonly ISurveyUserRepository _surveyUserReposity;
        private readonly IAnswerRepository _answerReposity;
        private readonly ISurveyRepository _surveyReposity;
        private readonly IQuestionRepository _questionReposity;

        public SurveyUserAnswerService(ISurveyUserAnswerRepository surveyUserAnswerRepository,
            ISurveyQuestionUserRepository surveyQuestionUserRepository, IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmSurveyResource> surveyResourceService, ISurveyUserAnswerTimeRepository surveyUserAnswerTimeRepository,
            ISurveyUserRepository surveyUserReposity, IAnswerRepository answerReposity, ISurveyRepository surveyReposity,
            IQuestionRepository questionReposity)
        {
            _surveyUserAnswerRepository = surveyUserAnswerRepository;
            _surveyQuestionUserRepository = surveyQuestionUserRepository;
            _sharedResourceService = sharedResourceService;
            _surveyResourceService = surveyResourceService;
            _surveyUserAnswerTimeRepository = surveyUserAnswerTimeRepository;
            _surveyUserReposity = surveyUserReposity;
            _answerReposity = answerReposity;
            _surveyReposity = surveyReposity;
            _questionReposity = questionReposity;
        }

        public async Task<ActionResultResponse> Delete(string surveyUserAnswerId)
        {
            var surveyuseranswerinfo = await _surveyUserAnswerRepository.GetInfo(surveyUserAnswerId);
            if (surveyuseranswerinfo == null)
                return new ActionResultResponse(-1, _surveyResourceService.GetString("Answer does not exists."));

            var result = await _surveyUserAnswerRepository.ForceDelete(surveyUserAnswerId);
            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                : _surveyResourceService.GetString("Delete answer successful."));
        }

        public async Task<ActionResultResponse<string>> Insert(string userId, SurveyUserAnswerMeta surveyUserAnswerMeta)
        {
            var surveyUserInfo = await _surveyUserReposity.GetInfoByUserId(surveyUserAnswerMeta.SurveyId, userId);
            if (surveyUserInfo == null)
                return new ActionResultResponse<string>(-1, _surveyResourceService.GetString("You does not exists in survey."));

            var surveyUserAnswerTimeInfo =
               await _surveyUserAnswerTimeRepository.GetInfoBySurveyIdAndSurveyUserId(surveyUserAnswerMeta.SurveyId,
                    surveyUserInfo.Id);

            if (surveyUserAnswerTimeInfo == null)
                return new ActionResultResponse<string>(-2, _surveyResourceService.GetString("Exam not exists."));

            var questionInfo = await _questionReposity.GetInfoRecord(surveyUserAnswerMeta.QuestionVersionId);
            if (questionInfo == null)
                return new ActionResultResponse<string>(-3, _surveyResourceService.GetString("Question not exists."));

            var anwserInfo = await _answerReposity.GetInfo(surveyUserAnswerMeta.AnswerId);
            if (anwserInfo == null && (questionInfo.Type == QuestionType.MultiChoice
                || questionInfo.Type == QuestionType.SingleChoice || questionInfo.Type == QuestionType.Rating))
                return new ActionResultResponse<string>(-4, _surveyResourceService.GetString("Answer does not exists."));

            var surveyuseranswerId = Guid.NewGuid().ToString();
            var surveyuseranswer = new SurveyUserAnswer
            {
                Id = surveyuseranswerId,
                SurveyId = surveyUserAnswerMeta.SurveyId,
                SurveyUserAnswerTimesId = surveyUserAnswerTimeInfo.Id,
                SurveyUserId = surveyUserInfo.Id,
                QuestionVersionId = surveyUserAnswerMeta.QuestionVersionId,
                AnswerId = surveyUserAnswerMeta.AnswerId,
                AnswerTime = DateTime.Now,
                IsCorrect = anwserInfo?.IsCorrect ?? false,
                Value = surveyUserAnswerMeta.Value,
            };

            var result = await _surveyUserAnswerRepository.Insert(surveyuseranswer);
            if (result < 0)
            {
                await RollbackInsert(surveyuseranswerId);
                return new ActionResultResponse<string>(-5, _surveyResourceService.GetString("Can not insert new SurveyUserAnswer. Please contact with administrator."));
            }
            return new ActionResultResponse<string>(result, _sharedResourceService.GetString("Insert successfull"), string.Empty, surveyuseranswer.Id);
        }

        public async Task<ActionResultResponse> Update(string surveyUserAnswerId, string userId, SurveyUserAnswerMeta surveyUserAnswerMeta)
        {
            var surveyUserInfo = await _surveyUserReposity.GetInfoByUserId(surveyUserAnswerMeta.SurveyId, userId);
            if (surveyUserInfo == null)
                return new ActionResultResponse<string>(-1, _surveyResourceService.GetString("You does not exists in survey."));

            var surveyUserAnswerTimeInfo = await _surveyUserAnswerTimeRepository.GetInfoBySurveyIdAndSurveyUserId(surveyUserAnswerMeta.SurveyId,
                                           surveyUserInfo.Id);
            if (surveyUserAnswerTimeInfo == null)
                return new ActionResultResponse<string>(-2, _surveyResourceService.GetString("Exam not exists."));

            //var anwserInfo = await _answerReposity.GetInfo(surveyUserAnswerId);

            var surveyUserAnswerInfo = await _surveyUserAnswerRepository.GetInfo(surveyUserAnswerId);
            if (surveyUserAnswerInfo == null)
                return new ActionResultResponse(-2, _surveyResourceService.GetString("SurveyUserAnswer does not exists."));

            //surveyUserAnswerInfo.AnswerId = surveyUserAnswerId;
            surveyUserAnswerInfo.AnswerTime = DateTime.Now;
            //surveyUserAnswerInfo.IsCorrect = anwserInfo.IsCorrect;
            surveyUserAnswerInfo.Value = surveyUserAnswerMeta.Value;

            var result = await _surveyUserAnswerRepository.Update(surveyUserAnswerInfo);
            if (result <= 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            return new ActionResultResponse(1, _surveyResourceService.GetString("Update SurveyUserAnswer successful."));
        }

        private async Task RollbackInsert(string surveyuseranswerId)
        {
            await _surveyUserAnswerRepository.ForceDelete(surveyuseranswerId);
        }

        /// <summary>
        /// Danh sách câu hỏi và các phương án trả lời cho người tham gia
        /// </summary>
        /// <param name="tenantId"></param>
        /// <param name="surveyId"></param>
        /// <param name="surveyUserId"></param>
        /// <param name="languageId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<SearchResult<QuestionForUser>> DetailSurveyQuestionUser(string tenantId, string userId, string surveyId, string surveyUserId,
            string languageId, int page, int pageSize)
        {
            var serveyInfor = await _surveyReposity.GetInfo(surveyId);
            if (serveyInfor == null)
                return new SearchResult<QuestionForUser>
                {
                    Code = -1,
                    Message = _surveyResourceService.GetString("Not exists survey.")
                };

            if (serveyInfor.IsPreRendering)
            {
                var isExistsSurvey = await _surveyUserAnswerTimeRepository.CheckExistsBySurveyIdAndSurveyUserId(surveyId, surveyUserId);
                if (!isExistsSurvey)
                {
                    var Infor = await _surveyUserAnswerTimeRepository.SurveyUserAnswerTimesIsPreRendering(surveyId, surveyUserId);
                    if (Infor < 0)
                        return new SearchResult<QuestionForUser>
                        {
                            Code = -3,
                            Message = _surveyResourceService.GetString("Create SurveyUserAnswerTime not successful.")
                        };
                }
                var items = await _surveyUserAnswerRepository.GetQuestionForUserInSurvey(tenantId, surveyId, string.Empty,
                    languageId, page, pageSize, out var totalRow);

                return new SearchResult<QuestionForUser>
                {
                    Items = items,
                    TotalRows = totalRow
                };
            }
            else
            {
                var isExistsSurvey = await _surveyUserAnswerTimeRepository.CheckExistsBySurveyIdAndSurveyUserId(surveyId, surveyUserId);
                if (!isExistsSurvey)
                {
                    var isRandomQuestionSuccess = await _surveyQuestionUserRepository.RanDomQuestion(surveyId, userId,string.Empty);
                    if (isRandomQuestionSuccess < 0)
                        return new SearchResult<QuestionForUser>
                        {
                            Code = -4,
                            Message = _surveyResourceService.GetString("Create question not successful.")
                        };
                }
                var items = await _surveyUserAnswerRepository.GetQuestionForUserInSurvey(tenantId, surveyId, surveyUserId,
               languageId, page, pageSize, out int totalRow);

                return new SearchResult<QuestionForUser>
                {
                    Items = items,
                    TotalRows = totalRow
                };
            }
        }
    }
}
