﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Infrastructure.Constants;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.IServices;
using GHM.Survey.Domain.ModelMetas;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.Resources;
using GHM.Survey.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;

namespace GHM.Survey.Infrastructure.Services
{
    public class QuestionGroupService : IQuestionGroupService
    {
        private readonly IQuestionGroupRepository _questionGroupRepository;
        private readonly IQuestionGroupTranslationRepository _questionGroupTranslationRepository;
        private readonly IQuestionRepository _questionRepository;
        private readonly IQuestionTranslationRepository _questionTranslationRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmSurveyResource> _surveyResourceResource;

        public QuestionGroupService(IQuestionGroupRepository questionGroupRepository,
            IQuestionGroupTranslationRepository questionGroupTranslationRepository,
            IQuestionRepository questionRepository,
            IQuestionTranslationRepository questionTranslationRepository,
            IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmSurveyResource> surveyResourceResource)
        {
            _questionGroupRepository = questionGroupRepository;
            _questionGroupTranslationRepository = questionGroupTranslationRepository;
            _questionRepository = questionRepository;
            _questionTranslationRepository = questionTranslationRepository;
            _sharedResourceService = sharedResourceService;
            _surveyResourceResource = surveyResourceResource;
        }

        public async Task<List<QuestionGroupSearchViewModel>> GetsAll(string tenantId, string languageId)
        {
            return await _questionGroupRepository.GetAllActivatedQuestionGroup(tenantId, languageId);
        }

        public async Task<SearchResult<QuestionGroupSearchViewModel>> Search(string tenantId, string languageId, string keyword,
            bool? isActive, int page, int pageSize)
        {
            var items = await _questionGroupRepository.Search(tenantId, languageId, keyword, isActive, page, pageSize,
               out var totalRows);
            return new SearchResult<QuestionGroupSearchViewModel>
            {
                TotalRows = totalRows,
                Items = items
            };
        }

        public async Task<SearchResult<QuestionGroupSearchViewModel>> SearchTree(string tenantId, string languageId, string keyword,
            bool? isActive, int page, int pageSize)
        {
            var items = await _questionGroupRepository.Search(tenantId, languageId, keyword, isActive, page, pageSize,
              out var totalRows);
            return new SearchResult<QuestionGroupSearchViewModel>
            {
                TotalRows = totalRows,
                Items = items
            };
        }

        public async Task<List<TreeData>> GetFullQuestionGroupTree(string tenantId, string languageId)
        {
            var infoQuestionGroups = await _questionGroupRepository.GetAllActivatedQuestionGroup(tenantId, languageId);
            if (infoQuestionGroups == null || !infoQuestionGroups.Any())
                return null;

            return RenderTree(infoQuestionGroups, null);

            List<TreeData> RenderTree(List<QuestionGroupSearchViewModel> questionGroups, int? parentId)
            {
                var tree = new List<TreeData>();
                var parentQuestionGroups = questionGroups.Where(x => x.ParentId == parentId).ToList();
                if (parentQuestionGroups.Any())
                {
                    parentQuestionGroups.ForEach(questionGroup =>
                    {
                        var treeData = new TreeData
                        {
                            Id = questionGroup.Id,
                            Text = questionGroup.Name,
                            ParentId = questionGroup.ParentId,
                            IdPath = questionGroup.IdPath,
                            Data = questionGroup,
                            ChildCount = questionGroup.ChildCount,
                            Icon = string.Empty,
                            State = new GHM.Infrastructure.Models.State(),
                            Children = RenderTree(questionGroups, questionGroup.Id)
                        };
                        tree.Add(treeData);
                    });
                }
                return tree;
            }

        }

        public async Task<ActionResultResponse<int>> Insert(string tenantId, string creatorId, string creatorFullName, QuestionGroupMeta questionGroupMeta)
        {
            var questionGroup = new QuestionGroup
            {
                IdPath = "-1",
                IsActive = questionGroupMeta.IsActive,
                Order = questionGroupMeta.Order,
                TenantId = tenantId,
                OrderPath = questionGroupMeta.Order.ToString(),
                TotalQuestion = 0,
                CreatorId = creatorId,
                CreatorFullName = creatorFullName
            };

            if (questionGroupMeta.ParentId.HasValue)
            {
                var parentInfo = await _questionGroupRepository.GetInfo(questionGroupMeta.ParentId.Value);
                if (parentInfo == null)
                    return new ActionResultResponse<int>(-2, _surveyResourceResource.GetString("Parent question group does not exists. Please try again."));

                questionGroup.ParentId = parentInfo.Id;
                questionGroup.IdPath = $"{parentInfo.IdPath}.-1";
            }

            var result = await _questionGroupRepository.Insert(questionGroup);
            if (result <= 0)
                return new ActionResultResponse<int>(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            #region Update current question group idPath.            

            questionGroup.IdPath = questionGroup.IdPath.Replace("-1", questionGroup.Id.ToString());
            await _questionGroupRepository.UpdateQuestionGroupIdPath(questionGroup.Id, questionGroup.IdPath);
            #endregion

            #region Update parent question group child count.
            if (questionGroup.ParentId.HasValue)
            {
                var childCount = await _questionGroupRepository.GetChildCount(questionGroup.ParentId.Value);
                await _questionGroupRepository.UpdateChildCount(questionGroup.ParentId.Value, childCount);
            }
            #endregion

            #region Insert question group translation.
            var questionGroupTranslations = new List<QuestionGroupTranslation>();
            foreach (var questionGroupTranslationMeta in questionGroupMeta.QuestionGroupTranslations)
            {
                // check name exists.
                var isNameExists = await _questionGroupTranslationRepository.CheckExistsByName(questionGroup.Id, questionGroup.TenantId
                   , questionGroupTranslationMeta.LanguageId, questionGroupTranslationMeta.Name);
                if (isNameExists)
                {
                    await RollbackInsert();
                    return new ActionResultResponse<int>(-3,
                        _surveyResourceResource.GetString("Question group name: \"{0}\" already taken by another question group. Please try again.", questionGroupTranslationMeta.Name));
                }
                var questionGroupTranslation = new QuestionGroupTranslation
                {
                    Name = questionGroupTranslationMeta.Name.Trim(),
                    Description = questionGroupTranslationMeta.Description?.Trim(),
                    UnsignName = questionGroupTranslationMeta.Name.Trim().StripVietnameseChars().ToUpper(),
                    LanguageId = questionGroupTranslationMeta.LanguageId,
                    QuestionGroupId = questionGroup.Id,
                    TenantId = questionGroup.TenantId,
                };
                if (questionGroupMeta.ParentId.HasValue)
                {
                    var parentName = await _questionGroupTranslationRepository.GetQuestionGroupName(questionGroupMeta.ParentId.Value,
                        questionGroupTranslationMeta.LanguageId);
                    if (!string.IsNullOrEmpty(parentName))
                    {
                        questionGroupTranslation.ParentName = parentName;
                    }
                }
                else
                {
                    questionGroupTranslation.ParentName = string.Empty;
                }
                questionGroupTranslations.Add(questionGroupTranslation);
            }

            var resultInsertTranslation = await _questionGroupTranslationRepository.Inserts(questionGroupTranslations);
            if (resultInsertTranslation <= 0)
            {
                await RollbackInsert();
                return new ActionResultResponse<int>(resultInsertTranslation,
                    _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
            }

            #endregion
            return new ActionResultResponse<int>(result, _surveyResourceResource.GetString("Add new question group successful."),
                string.Empty, questionGroup.Id);

            async Task RollbackInsert()
            {
                await _questionGroupRepository.ForceDelete(questionGroup.Id);
            }
        }

        public async Task<ActionResultResponse> Update(string tenantId, string lastUpdateUserId, string lastUpdateFullName,
            int questionGroupId, QuestionGroupMeta questionGroupMeta)
        {
            var questionGroupInfo = await _questionGroupRepository.GetInfo(questionGroupId);
            if (questionGroupInfo == null)
                return new ActionResultResponse(-2, _surveyResourceResource.GetString("Question group does not exists. Please try again."));

            if (questionGroupInfo.TenantId != tenantId)
                return new ActionResultResponse(-3, _sharedResourceService.GetString("You do not have permission to do this action."));

            if (questionGroupInfo.ConcurrencyStamp != questionGroupMeta.ConcurrencyStamp)
                return new ActionResultResponse(-4, _sharedResourceService.GetString("The question group already updated by other people. You can not update this question group."));

            if (questionGroupMeta.ParentId.HasValue && questionGroupInfo.Id == questionGroupMeta.ParentId.Value)
                return new ActionResultResponse(-5, _surveyResourceResource.GetString("Question group and parent question group can not be the same.")); ;

            var oldParentId = questionGroupInfo.ParentId;
            var oldIdPath = questionGroupInfo.IdPath;

            questionGroupInfo.IsActive = questionGroupMeta.IsActive;
            questionGroupInfo.Order = questionGroupMeta.Order;
            questionGroupInfo.ConcurrencyStamp = Guid.NewGuid().ToString();
            questionGroupInfo.LastUpdate = DateTime.Now;
            questionGroupInfo.LastUpdateUserId = lastUpdateUserId;
            questionGroupInfo.LastUpdateFullName = lastUpdateFullName;

            if (questionGroupInfo.ParentId.HasValue && !questionGroupMeta.ParentId.HasValue)
            {
                questionGroupInfo.ParentId = null;
                questionGroupInfo.IdPath = questionGroupInfo.Id.ToString();
                questionGroupInfo.OrderPath = questionGroupInfo.Order.ToString();
            }
            else if (questionGroupMeta.ParentId.HasValue && questionGroupMeta.ParentId != questionGroupInfo.ParentId)
            {
                var parentInfo = await _questionGroupRepository.GetInfo(questionGroupMeta.ParentId.Value);
                if (parentInfo == null)
                    return new ActionResultResponse(-6, _surveyResourceResource.GetString("Parent question group does not exists. Please try again.")); ;

                questionGroupInfo.IdPath = $"{parentInfo.IdPath}.{questionGroupInfo.Id}";
                questionGroupInfo.ParentId = parentInfo.Id;
                questionGroupInfo.OrderPath = $"{parentInfo.OrderPath}.{questionGroupInfo.Order}";
            }

            await _questionGroupRepository.Update(questionGroupInfo);

            // Update children IdPath and RootInfo
            if (questionGroupInfo.IdPath != oldIdPath)
            {
                await _questionGroupRepository.UpdateChildrenIdPath(oldIdPath, questionGroupInfo.IdPath);
            }

            // Update parent question group child count.
            if (questionGroupInfo.ParentId.HasValue && oldParentId.HasValue && questionGroupInfo.ParentId.Value != oldParentId.Value)
            {
                // Update old parent question group child count.
                var oldChildCount = await _questionGroupRepository.GetChildCount(oldParentId.Value);
                await _questionGroupRepository.UpdateChildCount(oldParentId.Value, oldChildCount);

                // Update new parent question group child count.
                var newParentId = questionGroupInfo.ParentId.Value;
                var newChildCount = await _questionGroupRepository.GetChildCount(newParentId);
                await _questionGroupRepository.UpdateChildCount(newParentId, newChildCount);
            }

            // Update lai survey group child by Id
            var childCountid = await _questionGroupRepository.GetChildCount(questionGroupInfo.Id);
            await _questionGroupRepository.UpdateChildCount(questionGroupInfo.Id, childCountid);

            //update question translation

            if (questionGroupMeta.QuestionGroupTranslations.Any())
                await UpdateQuestionGroupTranslation();

            // Update question group translation.
            var resultUpdateTranslation = await UpdateQuestionGroupTranslation();

            if (resultUpdateTranslation.Code > 0)
                await UpdateQuestionTranslations();

            return new ActionResultResponse(resultUpdateTranslation.Code <= 0
                ? resultUpdateTranslation.Code
                : 1,
                resultUpdateTranslation.Code <= 0
                ? resultUpdateTranslation.Message
                : _surveyResourceResource.GetString("Update question group successful."));



            async Task<ActionResultResponse> UpdateQuestionGroupTranslation()
            {
                foreach (var questionGroupTranslationMeta in questionGroupMeta.QuestionGroupTranslations)
                {
                    // check name exists.
                    var isNameExists = await _questionGroupTranslationRepository.CheckExistsByName(questionGroupInfo.Id, questionGroupInfo.TenantId,
                        questionGroupTranslationMeta.LanguageId, questionGroupTranslationMeta.Name);
                    if (isNameExists)
                    {
                        return new ActionResultResponse(-6,
                            _surveyResourceResource.GetString("Question group name: \"{0}\" already taken by another question group. Please try again.",
                            questionGroupTranslationMeta.Name));
                    }

                    var questionGroupTranslationInfo =
                        await _questionGroupTranslationRepository.GetInfo(questionGroupInfo.Id, questionGroupTranslationMeta.LanguageId);
                    if (questionGroupTranslationInfo == null)
                    {
                        questionGroupTranslationInfo = new QuestionGroupTranslation
                        {
                            Name = questionGroupTranslationMeta.Name.Trim(),
                            Description = questionGroupTranslationMeta.Description?.Trim(),
                            UnsignName = questionGroupTranslationMeta.Name.Trim().StripVietnameseChars().ToUpper(),
                            LanguageId = questionGroupTranslationMeta.LanguageId,
                            QuestionGroupId = questionGroupInfo.Id,

                        };

                        if (questionGroupMeta.ParentId.HasValue)
                        {
                            var parentName = await _questionGroupTranslationRepository.GetQuestionGroupName(questionGroupMeta.ParentId.Value,
                                questionGroupTranslationMeta.LanguageId);
                            if (!string.IsNullOrEmpty(parentName))
                            {
                                questionGroupTranslationInfo.ParentName = parentName;
                            }
                        }
                        else
                        {
                            questionGroupTranslationInfo.ParentName = string.Empty;
                        }

                        await _questionGroupTranslationRepository.Insert(questionGroupTranslationInfo);
                    }
                    else
                    {
                        questionGroupTranslationInfo.Name = questionGroupTranslationMeta.Name.Trim();
                        questionGroupTranslationInfo.Description = questionGroupTranslationMeta.Description?.Trim();
                        questionGroupTranslationInfo.UnsignName = questionGroupTranslationMeta.Name.StripVietnameseChars().ToUpper();

                        if (questionGroupMeta.ParentId.HasValue)
                        {
                            var parentName = await _questionGroupTranslationRepository.GetQuestionGroupName(questionGroupMeta.ParentId.Value,
                                questionGroupTranslationMeta.LanguageId);
                            if (!string.IsNullOrEmpty(parentName))
                            {
                                questionGroupTranslationInfo.ParentName = parentName;
                            }
                        }
                        else
                        {
                            questionGroupTranslationInfo.ParentName = string.Empty;
                        }

                        await _questionGroupTranslationRepository.Update(questionGroupTranslationInfo);
                    }

                }

                return new ActionResultResponse(1,
                    _surveyResourceResource.GetString("Update question group successful."));
            }

            async Task<ActionResultResponse> UpdateQuestionTranslations()
            {
                foreach (var questionGroupTranslationMeta in questionGroupMeta.QuestionGroupTranslations)
                {
                    await _questionTranslationRepository.UpdateQuestionGroupName(tenantId,
                        questionGroupTranslationMeta.LanguageId, questionGroupId, questionGroupTranslationMeta.Name);
                }

                return new ActionResultResponse(1,
                    _surveyResourceResource.GetString("Update question group name successful."));
            }
        }

        public async Task<ActionResultResponse> Delete(string tenantId, int id)
        {
            var questionGroupInfo = await _questionGroupRepository.GetInfo(id);
            if (questionGroupInfo == null)
                return new ActionResultResponse(-1, _surveyResourceResource.GetString("Question group does not exists. Please try again."));

            if (questionGroupInfo.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            // Check is has child.
            var questionGroupChildCount = await _questionGroupRepository.GetChildCount(questionGroupInfo.Id);
            if (questionGroupChildCount > 0)
                return new ActionResultResponse(-3,
                    _surveyResourceResource.GetString("This question group has children question group. You can not delete this question group."));

            var isQuestionExists = await _questionRepository.CheckExistsByQuestionGroupId(questionGroupInfo.Id);
            if (isQuestionExists)
                return new ActionResultResponse(-3,
                    _surveyResourceResource.GetString("This question group used by question. You can not delete this question group."));

            var result = await _questionGroupRepository.Delete(questionGroupInfo.Id);
            if (result > 0)
            {
                if (questionGroupInfo.ParentId.HasValue)
                {
                    //Update parent question group child count.
                    var childCount = await _questionGroupRepository.GetChildCount(questionGroupInfo.ParentId.Value);
                    await _questionGroupRepository.UpdateChildCount(questionGroupInfo.ParentId.Value, childCount);
                }

                await _questionGroupTranslationRepository.ForceDeleteByQuestionGroupId(id);              
            }

            return new ActionResultResponse(result, result > 0
                ? _surveyResourceResource.GetString("Delete question group successful.")
                : _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
        }

        public async Task<ActionResultResponse<QuestionGroupDetailViewModel>> GetDetail(string tenantId, string languageId, int id)
        {
            var questionGroupInfo = await _questionGroupRepository.GetInfo(id, true);
            if (questionGroupInfo == null)
                return new ActionResultResponse<QuestionGroupDetailViewModel>(-1,
                    _surveyResourceResource.GetString("Question group does not exists."));

            if (questionGroupInfo.TenantId != tenantId)
                return new ActionResultResponse<QuestionGroupDetailViewModel>(-2,
                    _surveyResourceResource.GetString(ErrorMessage.NotHavePermission));

            var questionGroupDetail = new QuestionGroupDetailViewModel
            {
                IsActive = questionGroupInfo.IsActive,
                ParentId = questionGroupInfo.ParentId,
                Order = questionGroupInfo.Order,
                ConcurrencyStamp = questionGroupInfo.ConcurrencyStamp,
                ChildCount = questionGroupInfo.ChildCount,
                QuestionGroupTranslations = await _questionGroupTranslationRepository.GetsByQuestionGroupId(questionGroupInfo.Id)
            };

            return new ActionResultResponse<QuestionGroupDetailViewModel>
            {
                Code = 1,
                Data = questionGroupDetail
            };
        }

        public async Task<SearchResult<QuestionGroupForSelectViewModel>> GetQuestionGroupForSelect(string tenantId, string languageId,
            string keyword, int page, int pageSize)
        {
            var items = await _questionGroupRepository
                .GetAllQuestionGroupForSelect(tenantId, languageId, keyword, page, pageSize, out var totalRows);
            return new SearchResult<QuestionGroupForSelectViewModel>
            {
                Items = items,
                TotalRows = totalRows
            };
        }

        //public async Task<ActionResultResponse> UpdateTotalQuestion(string tenantId, int questionGroupId, int totalQuestion)
        //{
        //    var questionGroupInfo = await _questionGroupRepository.GetInfo(questionGroupId);
        //    if (questionGroupInfo == null)
        //        return new ActionResultResponse(-2, _surveyResourceResource.GetString("Question group info does not exists. Please try again."));

        //    if (questionGroupInfo.TenantId != tenantId)
        //        return new ActionResultResponse(-3, _sharedResourceService.GetString("You do not have permission to do this action."));

        //    questionGroupInfo.TotalQuestion = totalQuestion;

        //    await _questionGroupRepository.Update(questionGroupInfo);

        //    return new ActionResultResponse(1,
        //        _surveyResourceResource.GetString("Update total question successful."));
        //}
    }
}

