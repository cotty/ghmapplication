﻿using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Survey.Domain.IRepository;
using GHM.Survey.Domain.IServices;
using GHM.Survey.Domain.Models;
using GHM.Survey.Domain.Resources;
using GHM.Survey.Domain.ViewModels;
using System;
using System.Threading.Tasks;

namespace GHM.Survey.Infrastructure.Services
{
    public class ReportService : IReportService
    {
        private readonly ISurveyRepository _surveyRepository;
        private readonly ISurveyUserRepository _surveyUserRepository;
        private readonly ISurveyUserAnswerTimeRepository _surveyUserAnswerTimeRepository;
        private readonly IQuestionGroupRepository _questionGroupRepository;
        private readonly IResourceService<GhmSurveyResource> _resourceService;

        public ReportService(ISurveyUserAnswerTimeRepository surveyUserAnswerTimeRepository, ISurveyRepository surveyRepository,
            IResourceService<GhmSurveyResource> surveyResourceResource, ISurveyUserRepository surveyUserRepository, IQuestionGroupRepository questionGroupRepository)
        {
            _surveyUserAnswerTimeRepository = surveyUserAnswerTimeRepository;
            _surveyRepository = surveyRepository;
            _resourceService = surveyResourceResource;
            _surveyUserRepository = surveyUserRepository;
            _questionGroupRepository = questionGroupRepository;
        }

        public SearchResult<SurveyReport> SearchReport(string tenantId, string languageId, string keyword, int? surveyGroupId,
            DateTime? startDate, DateTime? endDate, int page, int pageSize)
        {
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
            }

            if (startDate.HasValue)
                startDate = startDate.Value.Date;

            if (endDate.HasValue)
                endDate = endDate.Value.Date.AddDays(1).AddMinutes(-1);

            System.Collections.Generic.List<SurveyReport> items = _surveyRepository.SearchReport(tenantId, languageId, keyword, surveyGroupId,
                startDate, endDate, page, pageSize, out int totalRows);
            return new SearchResult<SurveyReport>
            {
                Items = items,
                TotalRows = totalRows
            };
        }

        public async Task<SearchResult<SurveyUsersReport>> SearchUsersReport(string tenantId, string surveyId,
            string keyword, int page, int pageSize)
        {
            bool isExists = await _surveyRepository.CheckSurveyExistsByTenantId(tenantId, surveyId);
            if (!isExists)
            {
                return new SearchResult<SurveyUsersReport>(-1, _resourceService.GetString("Survey does not exists."));
            }

            System.Collections.Generic.List<SurveyUsersReport> items = _surveyUserRepository.SearchSurveyUsersReport(surveyId, keyword, page, pageSize, out int totalRows);
            return new SearchResult<SurveyUsersReport>
            {
                Items = items,
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse<SurveyUsersReportDetailViewModel>> SearchUsersReportDetail(string tenantId, string surveyId,
            string surveyUserId)
        {
            bool isSurveyExists = await _surveyRepository.CheckSurveyExistsByTenantId(tenantId, surveyId);
            if (!isSurveyExists)
            {
                return new ActionResultResponse<SurveyUsersReportDetailViewModel>(-1, _resourceService.GetString("Survey does not exists."));
            }

            SurveyUser surveyUserInfo = await _surveyUserRepository.GetInfo(surveyUserId, true);
            if (surveyUserInfo == null)
            {
                return new ActionResultResponse<SurveyUsersReportDetailViewModel>(-2, _resourceService.GetString("User info does not exists."));
            }

            SurveyUsersReportDetailViewModel userReportDetail = new SurveyUsersReportDetailViewModel
            {
                SurveyId = surveyUserInfo.SurveyId,
                UserId = surveyUserInfo.UserId,
                SurveyUserId = surveyUserInfo.Id,
                FullName = surveyUserInfo.FullName,
                Avatar = surveyUserInfo.Avatar,
                OfficeId = surveyUserInfo.OfficeId,
                OfficeName = surveyUserInfo.OfficeName,
                PositionId = surveyUserInfo.PositionId,
                PositionName = surveyUserInfo.PositionName,
                SurveyUserAnswerTimes = await _surveyUserAnswerTimeRepository.GetListSurveyUserAnswerTimes(surveyId, surveyUserInfo.Id)
            };
            return new ActionResultResponse<SurveyUsersReportDetailViewModel>
            {
                Data = userReportDetail
            };
        }

        public async Task<SearchResult<ReportSurveyUserAnswerTimesViewModel>> SearchReportSurveyUserAnswerTimes(string tenantId,
            string surveyId, string surveyUserId)
        {
            bool isExists = await _surveyRepository.CheckSurveyExistsByTenantId(tenantId, surveyId);
            if (!isExists)
            {
                return new SearchResult<ReportSurveyUserAnswerTimesViewModel>(-1, _resourceService.GetString("Survey does not exists."));
            }

            System.Collections.Generic.List<ReportSurveyUserAnswerTimesViewModel> listSurveyUserTimes =
                await _surveyUserAnswerTimeRepository.SearchReportBySurveyIdAndSurveyUserId(surveyId, surveyUserId);
            return new SearchResult<ReportSurveyUserAnswerTimesViewModel>
            {
                Items = listSurveyUserTimes
            };
        }

        public async Task<SearchResult<QuestionGroupReport>> GetQuestionGroupReport(string tenantId, string languageId,
            string surveyId, string surveyUserAnswerTimeId)
        {
            bool isSurveyExists = await _surveyRepository.CheckSurveyExistsByTenantId(tenantId, surveyId);
            if (!isSurveyExists)
            {
                return new SearchResult<QuestionGroupReport>(-1, _resourceService.GetString("Survey does not exists."));
            }

            System.Collections.Generic.List<QuestionGroupReport> items = await _questionGroupRepository.SearchReport(surveyId, surveyUserAnswerTimeId, languageId);
            return new SearchResult<QuestionGroupReport>
            {
                Items = items
            };
        }

        public async Task<SearchResult<SurveyUserReportViewModel>> GetSurveyByUserId(string tenantId, string languageId, string userId, string keyWord,
            int? surveyGroupId, DateTime? startDate, DateTime? endDate, int page, int pageSize)
        {
            System.Collections.Generic.List<SurveyUserReportViewModel> items = await _surveyUserRepository.GetListSurveyByUserId(tenantId, languageId, userId, keyWord, surveyGroupId, startDate, endDate,
                page, pageSize, out int totalRows);
            return new SearchResult<SurveyUserReportViewModel>
            {
                Items = items,
                TotalRows = totalRows,
            };
        }
    }
}
