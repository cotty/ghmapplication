﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Warehouse.Api.Controllers
{
    public class WebsiteController : Controller
    {
        [Authorize]
        [Produces("application/json")]
        [ApiVersion("1.0")]
        [Route("api/v{version:apiVersion}/website")]
        public IActionResult Index()
        {
            return View();
        }
    }
}