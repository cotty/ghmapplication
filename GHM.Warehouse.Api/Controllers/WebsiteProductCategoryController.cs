﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using GHM.Warehouse.Domain.IServices;
using GHM.Warehouse.Domain.ModelMetas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Warehouse.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/website-category")]
    public class WebsiteProductCategoryController : GhmControllerBase
    {
        private readonly IWebsiteCategoryService _websiteCategoryService;
        public WebsiteProductCategoryController(IWebsiteCategoryService websiteCategoryService)
        {
            _websiteCategoryService = websiteCategoryService;
        }
        [AcceptVerbs("GET")]
        [AllowPermission(PageId.NewsCategory, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetAll(string websiteId, string keyword, string languageId, bool isActive)
        {
            var result = await _websiteCategoryService.GetTreeAsync(CurrentUser.TenantId, websiteId, keyword, languageId ?? CultureInfo.CurrentCulture.Name, isActive);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }
        [Route("tree"), AcceptVerbs("GET")]
        [AllowPermission(PageId.Warehouse, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string websiteId, string keyword,string languageId, bool isActive)
        {
            var result = await _websiteCategoryService.GetTreeAsync(CurrentUser.TenantId, websiteId, null , languageId ?? CultureInfo.CurrentCulture.Name, isActive);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
        [Route("{websiteId}/{id}"), AcceptVerbs("GET")]
        [AllowPermission(PageId.Warehouse, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> GetDetail(string websiteId, int id)
        {
            var result = await _websiteCategoryService.GetDetailAsync(CurrentUser.TenantId, websiteId, id);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{websiteId}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.Warehouse, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Insert(string websiteId, [FromBody]WebsiteCategoryMeta websiteCategoryMeta)
        {
            websiteCategoryMeta.CreatorId = CurrentUser.Id;
            websiteCategoryMeta.CreatorFullName = CurrentUser.FullName;
            websiteCategoryMeta.CreatorAvatar = CurrentUser.Avatar;

            var result = await _websiteCategoryService.InsertAsync(CurrentUser.TenantId, websiteId, websiteCategoryMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{websiteId}/{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.Warehouse, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> Update(string websiteId,int id, [FromBody]WebsiteCategoryMeta websiteCategoryMeta)
        {
            websiteCategoryMeta.CreatorId = CurrentUser.Id;
            websiteCategoryMeta.CreatorFullName = CurrentUser.FullName;
            websiteCategoryMeta.CreatorAvatar = CurrentUser.Avatar;

            var result = await _websiteCategoryService.Update(CurrentUser.TenantId, websiteId, id, websiteCategoryMeta);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{websiteId}/{id}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.NewsCategory, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string websiteId, int id)
        {
            var result = await _websiteCategoryService.Delete(CurrentUser.TenantId, websiteId, id);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }
        [Route("suggestions"), AcceptVerbs("GET")]
        [AllowPermission(PageId.NewsCategory, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Suggestions(string websiteId, string keyword, string languageId, int page, int pageSize)
        {
            var result = await _websiteCategoryService.Suggestions(CurrentUser.TenantId, websiteId, keyword, languageId ?? CultureInfo.CurrentCulture.Name, page, pageSize);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("search-for-select"), AcceptVerbs("GET")]
        [AllowPermission(PageId.NewsCategory, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> SearchForSelect(string websiteId, string keyword, string languageId, int page = 1, int pageSize = 20)
        {
            var result = await _websiteCategoryService.SearchForSelect(CurrentUser.TenantId, websiteId, keyword, languageId ?? CultureInfo.CurrentCulture.Name, page, pageSize);
            if (result.Code <= 0)
                return BadRequest(result);

            return Ok(result);
        }
    }
}