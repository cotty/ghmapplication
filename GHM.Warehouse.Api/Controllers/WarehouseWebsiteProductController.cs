﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Filters;
using GHM.Warehouse.Domain.IServices;
using GHM.Warehouse.Domain.ModelMetas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Warehouse.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/website-product")]

    public class WarehouseWebsiteProductController : GhmControllerBase
    {
        private readonly IWarehouseWebsiteProductService _warehouseWebsiteProductService;
        public WarehouseWebsiteProductController(IWarehouseWebsiteProductService warehouseWebsiteProductService)
        {
            _warehouseWebsiteProductService = warehouseWebsiteProductService;
        }

        [AcceptVerbs("GET")]
        [AllowPermission(PageId.Warehouse, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string websiteId, int? categoryId, string productName, string languageId, int page = 1, int pageSize = 20)
        {
            var result = await _warehouseWebsiteProductService.SearchAsync(CurrentUser.TenantId, websiteId, categoryId, productName, languageId ?? CultureInfo.CurrentCulture.Name, page, pageSize);

            return Ok(result);

        }

        [Route("search-for-select"), AcceptVerbs("GET")]
        [CheckPermission]
        public async Task<IActionResult> SearchForSelect(string websiteId, string keyword, int? categoryId, int page = 1, int pageSize = 20)
        {
            var result = await _warehouseWebsiteProductService.SearchForSelect(CurrentUser.TenantId, CultureInfo.CurrentCulture.Name, websiteId, keyword, categoryId, page, pageSize);
            return Ok(result);
        }
        [AcceptVerbs("GET"), Route("Suggestion")]
        [AllowPermission(PageId.Warehouse, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> SearchForSuggestion(string warehouseId, string websiteId, string productId)
        {
            var result = await _warehouseWebsiteProductService.SearchForSuggestion(CurrentUser.TenantId, warehouseId, websiteId, productId);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);

        }
        [Route("Detail"), AcceptVerbs("GET")]
        [AllowPermission(PageId.Warehouse, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Detail(string websiteId,string productId, string languageId)
        {
            var result = await _warehouseWebsiteProductService.GetDetailAsync(CurrentUser.TenantId, websiteId, productId, languageId);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
        [Route("{websiteId}/{productId}/{languageId}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.Warehouse, Permission.Insert)]
        [CheckPermission]
        public async Task<IActionResult> Update( string websiteId, string productId, string languageId,[FromBody]WebsiteProductMeta websiteProductMeta)
        {
            websiteProductMeta.CreatorId = CurrentUser.Id;
            websiteProductMeta.CreatorFullName = CurrentUser.FullName;
            websiteProductMeta.CreatorAvatar = CurrentUser.Avatar;
            var result = await _warehouseWebsiteProductService.UpdateAsync(CurrentUser.TenantId, websiteId, productId, languageId ?? CultureInfo.CurrentCulture.Name, websiteProductMeta);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{websiteId}/{productId}"), AcceptVerbs("DELETE")]
        [AllowPermission(PageId.Warehouse, Permission.Delete)]
        [CheckPermission]
        public async Task<IActionResult> Delete(string websiteId, string productId)
        {
            var result = await _warehouseWebsiteProductService.DeleteAsync(CurrentUser.TenantId, websiteId, productId);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{websiteId}/{productId}/{isHot}/updateHot"), AcceptVerbs("GET")]
        [AllowPermission(PageId.Warehouse, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateHot(string websiteId, string productid, bool isHot)
        {
            var result = await _warehouseWebsiteProductService.UpdateHot(CurrentUser.TenantId, websiteId, productid, isHot);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{websiteId}/{productId}/{isHomePage}/updateHomePage"), AcceptVerbs("GET")]
        [AllowPermission(PageId.Warehouse, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateHomePage(string websiteId, string productid, bool isHomePage)
        {
            var result = await _warehouseWebsiteProductService.UpdateHomePage(CurrentUser.TenantId, websiteId, productid, isHomePage);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
    }
}