﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Infrastructure;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Filters;
using GHM.Warehouse.Domain.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Warehouse.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{version:ApiVersion}/website-config")]
    public class WarehouseWebsiteConfigController : GhmControllerBase
    {
        private IWarehouseWebsiteConfigService _warehouseWebsiteService;
        public WarehouseWebsiteConfigController(IWarehouseWebsiteConfigService warehouseConfigService)
        {
            _warehouseWebsiteService = warehouseConfigService;
        }

        [AcceptVerbs("GET")]
        [AllowPermission(PageId.Warehouse, Permission.View)]
        public async Task<IActionResult> Search(string warehouseId, string websiteId, int page = 1, int pageSize = 20)
        {
            var result = await _warehouseWebsiteService.SearchAsync(CurrentUser.TenantId, warehouseId, websiteId, page, pageSize);

            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }
    }
}