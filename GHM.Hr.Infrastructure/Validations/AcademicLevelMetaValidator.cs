﻿using FluentValidation;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Helpers.Validations;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;

namespace GHM.Hr.Infrastructure.Validations
{
    class AcademicLevelMetaValidator : AbstractValidator<AcademicLevelMeta>
    {
        public AcademicLevelMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> resourceService)
        {
            RuleFor(x => x.AcademicLevelId)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseSelect,
                    sharedResourceService.GetString("Academic Level")))
               .MaximumLength(50).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("Academic Level"), 50));

            RuleFor(x => x.DegreeId)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseSelect,
                    sharedResourceService.GetString("Degree")))
               .MaximumLength(50).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("Degree"), 50));

            RuleFor(x => x.SchoolId)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseSelect,
                    sharedResourceService.GetString("School name")))
               .MaximumLength(50).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("School name"), 50));

            RuleFor(x => x.SpecializeId)
              .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseSelect,
                  sharedResourceService.GetString("Specialize")))
             .MaximumLength(50).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                  resourceService.GetString("Specialize"), 50));

            RuleFor(x => x.UserId)
             .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseEnter,
                 sharedResourceService.GetString("User")))
            .MaximumLength(50).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                 resourceService.GetString("User"), 50));

            RuleFor(x => x.Note)              
             .MaximumLength(500).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                  resourceService.GetString("Note"), 500));
        }
    }
}
