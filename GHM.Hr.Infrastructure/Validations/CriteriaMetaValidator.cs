﻿using System.Text.RegularExpressions;
using FluentValidation;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Helpers.Validations;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using OfficeOpenXml.FormulaParsing.Utilities;

namespace GHM.Hr.Infrastructure.Validations
{
    public class CriteriaMetaValidator : AbstractValidator<CriteriaMeta>
    {
        public CriteriaMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> resourceService)
        {
            RuleFor(x => x.Point)
                .Must(x => x.IsNumeric())
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.InValid, resourceService.GetString("point")));

            RuleFor(x => x.GroupId)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseSelect,
                    resourceService.GetString("criteria group")))
                .MaximumLength(50).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("criteria group"), 50));

            RuleFor(x => x.Translations)
                .Must(x => x.Count > 0)
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.PleaseEnter,
                    sharedResourceService.GetString("language")));

            RuleForEach(x => x.Translations)
                .SetValidator(new CriteriaTranslationValidator(sharedResourceService, resourceService));
        }
    }

    public class CriteriaTranslationValidator : AbstractValidator<CriteriaTranslation>
    {
        public CriteriaTranslationValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> resourceService)
        {
            RuleFor(x => x.Name)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseEnter,
                    resourceService.GetString("criteria name")))
                .MaximumLength(500).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("criteria name"), 500));

            RuleFor(x => x.Description)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseEnter,
                    resourceService.GetString("criteria description")))
                .MaximumLength(4000).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("criteria description"), 4000));

            RuleFor(x => x.Sanction)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseEnter,
                    resourceService.GetString("sanction")))
                .MaximumLength(4000).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("sanction"), 4000));
        }
    }
}
