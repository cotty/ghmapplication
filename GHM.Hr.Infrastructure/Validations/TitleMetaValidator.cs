﻿using System.Text.RegularExpressions;
using FluentValidation;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;

namespace GHM.Hr.Infrastructure.Validations
{
    public class TitleMetaValidator : AbstractValidator<TitleMeta>
    {
        public TitleMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> resourceService)
        {
            RuleFor(x => x.Translations)
                .Must(x => x.Count > 0)
                .WithMessage(sharedResourceService.GetString("Please select at least one language."));

            RuleForEach(x => x.Translations)
                .SetValidator(new TitleTranslationValidator(sharedResourceService, resourceService));
        }
    }

    public class TitleTranslationValidator : AbstractValidator<TitleTranslation>
    {
        public TitleTranslationValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> resourceService)
        {
            RuleFor(x => x.LanguageId)
                .NotNull()
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                    sharedResourceService.GetString("Language code")))
                .MaximumLength(10).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    sharedResourceService.GetString("Language code"), 10));

            RuleFor(x => x.Name)
                .NotNull()
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                    resourceService.GetString("Title name")))
                .MaximumLength(256).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    resourceService.GetString("Title name"), 256));

            RuleFor(x => x.ShortName)
                .NotNull()
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                    resourceService.GetString("Short name")))
                .MaximumLength(50).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    resourceService.GetString("Short name"), 50))
                .Matches(new Regex(@"^[a-zA-Z0-9]+$"))
                    .WithMessage(sharedResourceService.GetString("{0} just only contain a-z, A-Z, 0-9.", resourceService.GetString("Short name")));

            RuleFor(x => x.Description)
                .MaximumLength(500)
                .WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.", sharedResourceService.GetString("Description"), 500));
        }
    }
}
