﻿using FluentValidation;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.Helpers.Validations;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Hr.Infrastructure.Validations
{
    public class UserMetaValidator : AbstractValidator<UserMeta>
    {
        public UserMetaValidator(IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmHrResource> resourceService)
        {
            RuleFor(x => x.Status)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.NotNull, resourceService.GetString("Status")))
                 .IsInEnum().WithMessage(sharedResourceService.GetString(ValidatorMessage.InValid, resourceService.GetString("Status")));

            RuleFor(x => x.EnrollNumber)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.NotNull, resourceService.GetString("EnrollNumber")))
                 .MustBeNumber(sharedResourceService.GetString(ValidatorMessage.InValid, resourceService.GetString("EnrollNumber")));

            RuleFor(x => x.Birthday)
              .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.NotNull, resourceService.GetString("Birthday")))
              .MustBeValidDate(sharedResourceService.GetString(ValidatorMessage.InValid, resourceService.GetString("Birthday")));

            RuleFor(x => x.JoinedDate)
            .MustBeValidDate(sharedResourceService.GetString(ValidatorMessage.InValid, resourceService.GetString("JoinedDate")));

            RuleFor(x => x.PassportDateOfIssue)
           .MustBeValidDate(sharedResourceService.GetString(ValidatorMessage.InValid, resourceService.GetString("PassportDateOfIssue")));

            RuleFor(x => x.IdCardDateOfIssue)
           .MustBeValidDate(sharedResourceService.GetString(ValidatorMessage.InValid, resourceService.GetString("IdCardDateOfIssue")));

            RuleFor(x => x.Gender)
             .NotNull()
             .WithMessage(sharedResourceService.GetString(ValidatorMessage.NotNull, resourceService.GetString("Gender")));

            RuleFor(x => x.TitleId)
             .NotNull()
             .WithMessage(sharedResourceService.GetString(ValidatorMessage.NotNull, resourceService.GetString("Title")));

            RuleFor(x => x.OfficeId)
           .NotNull()
           .WithMessage(sharedResourceService.GetString(ValidatorMessage.NotNull, resourceService.GetString("Office")));

            RuleFor(x => x.PositionId)
            .NotNull()
            .WithMessage(sharedResourceService.GetString(ValidatorMessage.NotNull, resourceService.GetString("Position")));

            RuleFor(x => x.UserType)
            .NotNull()
            .WithMessage(sharedResourceService.GetString(ValidatorMessage.NotNull, resourceService.GetString("User Type")));

            RuleFor(x => x.FullName)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseEnter,
                    resourceService.GetString("Full Name")))
                .MaximumLength(50)
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength, resourceService.GetString("Full Name"), 50));

            RuleFor(x => x.UserName)
               .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseEnter,
                   resourceService.GetString("UserName")))
               .MaximumLength(50)
               .WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength, resourceService.GetString("UserName"), 50));

            RuleFor(x => x.IdCardNumber)
               .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseEnter,
                   resourceService.GetString("IdCardNumber")))
               .MaximumLength(50)
               .WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength, resourceService.GetString("IdCardNumber"), 20));
        }
    }
}
