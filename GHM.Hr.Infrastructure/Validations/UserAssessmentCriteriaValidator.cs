﻿using FluentValidation;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;

namespace GHM.Hr.Infrastructure.Validations
{
    class UserAssessmentCriteriaValidator : AbstractValidator<UserAssessmentCriteria>
    {
        public UserAssessmentCriteriaValidator(IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmHrResource> resourceService)
        {
            RuleFor(x => x.UserPoint)
                .Must(x => x >= 0)
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.MustGreaterThanOrEqual,
                    resourceService.GetString("user point"), 0));
            RuleFor(x => x.ManagerPoint)
                .Must(x => x >= 0)
                .When(x => x.ManagerPoint.HasValue)
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.MustGreaterThanOrEqual,
                    resourceService.GetString("manager point"), 0));
            RuleFor(x => x.ApproverPoint)
                .Must(x => x >= 0)
                .When(x => x.ApproverPoint.HasValue)
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.MustGreaterThanOrEqual,
                    resourceService.GetString("approver point"), 0));

            RuleFor(x => x.Note)
                .MaximumLength(500)
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("note"), 500));
        }
    }
}
