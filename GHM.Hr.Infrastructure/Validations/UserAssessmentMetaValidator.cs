﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Helpers.Validations;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;

namespace GHM.Hr.Infrastructure.Validations
{
    public class UserAssessmentMetaValidator : AbstractValidator<UserAssessmentMeta>
    {
        public UserAssessmentMetaValidator(IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmHrResource> resourceService)
        {
            RuleFor(x => x.UserId)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.NotNull, resourceService.GetString("User id")));

            RuleFor(x => x.Reason)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseEnter,
                    resourceService.GetString("Reason")))
                .When(x => x.Status == AssessmentStatus.ApproverDecline || x.Status == AssessmentStatus.ManagerDecline)
                .MaximumLength(500)
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength, resourceService.GetString("Reason"), 500));
        }
    }
}
