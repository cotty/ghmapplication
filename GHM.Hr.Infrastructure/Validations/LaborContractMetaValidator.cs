﻿using FluentValidation;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Helpers.Validations;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;

namespace GHM.Hr.Infrastructure.Validations
{
    class LaborContractMetaValidator : AbstractValidator<LaborContractMeta>
    {
        public LaborContractMetaValidator(IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmHrResource> resourceService)
        {
            RuleFor(x => x.Type)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseSelect,
                    sharedResourceService.GetString("Loại hợp đồng")))
               .MaximumLength(50).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("Loại hợp đồng"), 50));

            RuleFor(x => x.No)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseSelect,
                    sharedResourceService.GetString("Số hợp đồng")))
               .MaximumLength(100).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("Số hợp đồng"), 100));

            RuleFor(x => x.UserId)
              .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseEnter,
                  sharedResourceService.GetString("Mã nhân viên")))
             .MaximumLength(50).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                  resourceService.GetString("Mã nhân viên "), 50));

            RuleFor(x => x.FromDate)
                .NotNull().WithMessage(sharedResourceService.GetString(ValidatorMessage.PleaseEnter,
                    sharedResourceService.GetString("Ngày bắt đầu")))
                .MustBeValidDate(sharedResourceService.GetString(ValidatorMessage.InValid,
                    sharedResourceService.GetString("Ngày bắt đầu")));

            RuleFor(x => x.ToDate)
                .MustBeValidDate(sharedResourceService.GetString(ValidatorMessage.InValid,
                    sharedResourceService.GetString("Ngày kết thúc đầu")))
                    .When(x => x.ToDate.HasValue);

            RuleFor(x => x.Note)
               .MaximumLength(500).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("Ghi chú"), 500));

            RuleFor(x => x.FileId)
               .MaximumLength(50).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("Tập tin đính kèm"), 50));

            RuleFor(x => x.LaborContractFormId)
               .MaximumLength(50).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("Mẫu hợp đồng"), 50));

            RuleFor(x => x.RegisterDate)
               .MustBeValidDate(sharedResourceService.GetString(ValidatorMessage.InValid,
                   sharedResourceService.GetString("Ngày ký")))
                   .When(x => x.RegisterDate.HasValue);

            RuleFor(x => x.TaskDescription)
               .MaximumLength(4000).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("Mô tả công việc"), 4000));

            RuleFor(x => x.Time)
               .MaximumLength(256).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("Thời gian làm việc"), 256));

            RuleFor(x => x.BasicSalary)
                .MustBeNumber(sharedResourceService.GetString(ValidatorMessage.MustBeNumber,
                    resourceService.GetString("Lương cơ bản")))
                    .When(x => x.BasicSalary.HasValue);

            RuleFor(x => x.ProbationarySalary)
               .MustBeNumber(sharedResourceService.GetString(ValidatorMessage.MustBeNumber,
                   resourceService.GetString("Lương thử việc")))
                   .When(x => x.ProbationarySalary.HasValue);

            RuleFor(x => x.OfficialSalary)
               .MustBeNumber(sharedResourceService.GetString(ValidatorMessage.MustBeNumber,
                   resourceService.GetString("Lương chính thức")))
                   .When(x => x.OfficialSalary.HasValue);

            RuleFor(x => x.Status)
              .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseSelect, resourceService.GetString("Đối tượng")))
              .IsInEnum().WithMessage(sharedResourceService.GetString(ValidatorMessage.MustBeNumber,
                  resourceService.GetString("Đối tượng")));
        }
    }
}
