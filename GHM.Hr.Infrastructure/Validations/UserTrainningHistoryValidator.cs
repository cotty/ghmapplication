﻿using FluentValidation;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Hr.Infrastructure.Validations
{
    class UserTrainningHistoryValidator : AbstractValidator<TrainingHistoryMeta>
    {
        public UserTrainningHistoryValidator(IResourceService<SharedResource> shareResource, IResourceService<GhmHrResource> ghmHrResource)
        {
            RuleFor(x => x.CourseId).NotNull()
                 .WithMessage(ghmHrResource.GetString("{0} is not null", ghmHrResource.GetString("Course Id"))); ;

            RuleFor(x => x.CourseName).NotNull().MaximumLength(500)
                .WithMessage(shareResource.GetString("{0} is not allowed over {1} characters.", shareResource.GetString("Course Name"), 500));

            RuleFor(x => x.CoursePlaceId).NotNull()
                .WithMessage(ghmHrResource.GetString("{0} is not null", ghmHrResource.GetString("Course place Id")));

            RuleFor(x => x.CoursePlaceName).NotNull().MaximumLength(500)
                .WithMessage(shareResource.GetString("{0} is not allowed over {1}", shareResource.GetString("Course Place Name"), 500));


            RuleFor(x => x.Result).NotNull()
            .MaximumLength(500)
            .WithMessage(shareResource.GetString("{0} is not allowed over {1}", shareResource.GetString("Training Result"), 500));

            RuleFor(x => x.FromDate)
           .NotNull()
           .WithMessage(shareResource.GetString(ValidatorMessage.PleaseSelect,
               shareResource.GetString("from date")));

            RuleFor(x => x.ToDate)
                .NotNull()
                .WithMessage(shareResource.GetString(ValidatorMessage.PleaseSelect,
                    shareResource.GetString("to date")));

            RuleFor(x => x.Type)
                 .NotNull()
                 .WithMessage(ghmHrResource.GetString("{0} is not null", ghmHrResource.GetString("User training type")));
            RuleFor(x => x.IsHasCertificate)
              .NotNull()
              .WithMessage(ghmHrResource.GetString("{0} is not null", ghmHrResource.GetString("Training has certificate")));
        }
    }
}
