﻿using FluentValidation;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.Helpers.Validations;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Hr.Infrastructure.Validations
{
    public class RelatedUserMetaValidator : AbstractValidator<RelatedUserMeta>
    {
        public RelatedUserMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> resourceService)
        {
            RuleFor(x => x.UserId)
               .NotNull()
              .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                  sharedResourceService.GetString("user id")));

            RuleFor(x => x.ValueId)
               .NotNull()
              .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                  sharedResourceService.GetString("value id")));

            RuleFor(x => x.ValueName)
             .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.",
                  sharedResourceService.GetString("value name")))
              .MaximumLength(200).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                  sharedResourceService.GetString("value name"), 150));

            RuleFor(x => x.FullName)
             .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.",
                  sharedResourceService.GetString("Name code")))
              .MaximumLength(200).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                  sharedResourceService.GetString("Name code"), 200));

            RuleFor(x => x.Gender)
            .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.",
                 sharedResourceService.GetString("gender")));

            RuleFor(x => x.BirthDay)
          .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.",
               sharedResourceService.GetString("Birth Day")));

            RuleFor(x => x.IdCardNumber)
          .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.",
               sharedResourceService.GetString("id card number")))
               .MaximumLength(20).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                  sharedResourceService.GetString("id card number"), 150));

            RuleFor(x => x.IdCardDateOfIssue)
         .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.",
              sharedResourceService.GetString("Id Card Date Of Issue")));

            RuleFor(x => x.IdCardPlaceOfIssue)
         .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.",
              sharedResourceService.GetString("Id Card Place Of Issue")))
              .MaximumLength(200).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                 sharedResourceService.GetString("Id Card Place Of Issue"), 150));

            RuleFor(x => x.Phone)
         .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.",
              sharedResourceService.GetString("Phone")))
              .MaximumLength(12).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                 sharedResourceService.GetString("Phone"), 50));

            RuleFor(x => x.NationalId)
         .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.",
              sharedResourceService.GetString("national")));

            RuleFor(x => x.ProvinceId)
        .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.",
             sharedResourceService.GetString("province")));

            RuleFor(x => x.DistrictId)
        .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.",
             sharedResourceService.GetString("district")));

            RuleFor(x => x.DenominationName)
   .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.",
        sharedResourceService.GetString("denomination")))
        .MaximumLength(200).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
           sharedResourceService.GetString("denomination"), 50));

            RuleFor(x => x.Ethnic)
        .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.",
             sharedResourceService.GetString("Ethnic")));

            RuleFor(x => x.Job)
 .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.",
      sharedResourceService.GetString("Job")))
      .MaximumLength(500).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
         sharedResourceService.GetString("Job"), 500));

            RuleFor(x => x.PermanentAddress)
.NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.",
     sharedResourceService.GetString("Permanent address")))
     .MaximumLength(500).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
        sharedResourceService.GetString("Permanent address"), 500));

            RuleFor(x => x.ContactAddress)
.NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.",
     sharedResourceService.GetString("Permanent address")))
     .MaximumLength(500).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
        sharedResourceService.GetString("Permanent address"), 500));
        }
    }
}
