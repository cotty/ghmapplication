﻿using FluentValidation;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Helpers.Validations;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;

namespace GHM.Hr.Infrastructure.Validations
{
    class UserContactMetaValidator : AbstractValidator<UserContactMeta>
    {
        public UserContactMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> resourceService)
        {
            RuleFor(x => x.ContactType)
                .IsInEnum()
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.InValid,
                    sharedResourceService.GetString("User Contact Type")));            

            RuleFor(x => x.ContactValue)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseSelect,
                    sharedResourceService.GetString("User Contact value")))
               .MaximumLength(256).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("User Contact value"), 256));            
        }
    }
}
