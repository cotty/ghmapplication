﻿using System;
using System.Text.RegularExpressions;
using FluentValidation;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;

namespace GHM.Hr.Infrastructure.Validations
{
    public class OfficeMetaValidator : AbstractValidator<OfficeMeta>
    {
        public OfficeMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> hrResourceService)
        {
            RuleFor(x => x.Code)
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.", hrResourceService.GetString("Office code")))
                .MaximumLength(50).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    hrResourceService.GetString("Office code"), 50))
                .Matches(new Regex(@"^[a-zA-Z0-9]+$"))
                .WithMessage(sharedResourceService.GetString("{0} just only contain a-z, A-Z, 0-9.", hrResourceService.GetString("Office code")));

            RuleFor(x => x.OfficeType)
                .NotNull()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.", hrResourceService.GetString("Office type")))
                .Must(x => Enum.IsDefined(typeof(OfficeType), x))
                .WithMessage(sharedResourceService.GetString("{0} invalid.", hrResourceService.GetString("Office type")));

            RuleFor(x => x.Order)
                .NotNull()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.", hrResourceService.GetString("Order")));

            RuleFor(x => x.IsActive)
                .NotNull()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.", hrResourceService.GetString("Active status")));

            RuleFor(x => x.Translations)
                .Must(x => x != null && x.Count > 0)
                .WithMessage(sharedResourceService.GetString("Please select at least one language."));

            RuleForEach(x => x.Translations).SetValidator(new OfficeTranslationMetaValidator(sharedResourceService, hrResourceService));
            //RuleForEach(x => x.OfficeContacts).SetValidator(new OfficeContactMetaValidator(sharedResourceService, hrResourceService));
        }
    }

    public class OfficeTranslationMetaValidator : AbstractValidator<OfficeTranslationMeta>
    {
        public OfficeTranslationMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> hrResourceService)
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                    hrResourceService.GetString("Office name")))
                .MaximumLength(256).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    hrResourceService.GetString("Office name"), 256));

            RuleFor(x => x.ShortName)
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                    hrResourceService.GetString("Short name")))
                .MaximumLength(256).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    hrResourceService.GetString("Short name"), 256));

            RuleFor(x => x.Description)
                .MaximumLength(500).WithMessage(sharedResourceService.GetString(
                    "{0} is not allowed over {1} characters.",
                    sharedResourceService.GetString("Description"), 500));

            RuleFor(x => x.LanguageId)
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                    sharedResourceService.GetString("Language code")))
                .MaximumLength(10).WithMessage(sharedResourceService.GetString(
                    "{0} is not allowed over {1} characters.",
                    sharedResourceService.GetString("Language code"), 10));

            RuleFor(x => x.Address)
                .MaximumLength(500).WithMessage(sharedResourceService.GetString(
                    "{0} is not allowed over {1} characters.",
                    hrResourceService.GetString("Address"), 500));
        }
    }    
}