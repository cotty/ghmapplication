﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Helpers.Validations;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;

namespace GHM.Hr.Infrastructure.Validations
{
    public class CriteriaGroupMetaValidator : AbstractValidator<CriteriaGroupMeta>
    {
        public CriteriaGroupMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> resourceService)
        {           
            RuleFor(x => x.Translations)
                .Must(x => x.Count > 0)
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.PleaseEnter,
                    sharedResourceService.GetString("language")));

            RuleForEach(x => x.Translations)
                .SetValidator(new CriteriaGroupTranslationValidator(sharedResourceService, resourceService));
        }
    }

    public class CriteriaGroupTranslationValidator : AbstractValidator<CriteriaGroupTranslation>
    {
        public CriteriaGroupTranslationValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> resourceService)
        {
            RuleFor(x => x.Name)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseEnter,
                    resourceService.GetString("criteria group name")))
                .MaximumLength(256).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("criteria group name"), 256));

            RuleFor(x => x.Description)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseEnter,
                    resourceService.GetString("criteria group description")))
                .MaximumLength(500).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("criteria group description"), 500));
        }
    }
}
