﻿using FluentValidation;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.Helpers.Validations;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Hr.Infrastructure.Validations
{
    public class UserRecordsManagementValidator : AbstractValidator<UserRecordsManagementMeta>
    {
        public UserRecordsManagementValidator(IResourceService<SharedResource> shareResource, IResourceService<GhmHrResource> ghmHrResource)
        {
            RuleFor(x => x.UserId)
                .NotNullAndEmpty(ghmHrResource.GetString("{0} is not null", ghmHrResource.GetString("User Id")));

            RuleFor(x => x.Title)
                .NotNullAndEmpty(ghmHrResource.GetString("{0} is not null", ghmHrResource.GetString("Title")))
                .MaximumLength(500)
                .WithMessage(shareResource.GetString("{0} is not allowed over {1} characters.", shareResource.GetString("Records Title"), 500));

            RuleFor(x => x.ValueId)
                .NotNullAndEmpty(ghmHrResource.GetString("{0} is not null", ghmHrResource.GetString("Value")))
                .WithMessage(ghmHrResource.GetString("{0} is not null", ghmHrResource.GetString("Value Id")));

            RuleFor(x => x.AttachmentId).MaximumLength(50)
                .WithMessage(shareResource.GetString("{0} is not allowed over {1}", shareResource.GetString("Records attachment id"), 50));

            RuleFor(x => x.AttachmentName)
            .MaximumLength(500)
            .WithMessage(shareResource.GetString("{0} is not allowed over {1}", shareResource.GetString("Records attachment name"), 500));

            RuleFor(x => x.Note)
           .NotNull().MaximumLength(5000)
            .WithMessage(shareResource.GetString("{0} is not allowed over {1}", shareResource.GetString("Records Note"), 5000));

        }
    }
}
