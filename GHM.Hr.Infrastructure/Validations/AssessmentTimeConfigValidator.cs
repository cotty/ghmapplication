﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;

namespace GHM.Hr.Infrastructure.Validations
{
    class AssessmentTimeConfigValidator : AbstractValidator<AssessmentTimeConfig>
    {
        public AssessmentTimeConfigValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> resourceService)
        {
            RuleFor(x => x.FromDay)
                .Must(x => x.HasValue)
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.PleaseSelect,
                    sharedResourceService.GetString("from day")));

            RuleFor(x => x.ToDay)
                .Must(x => x.HasValue)
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.PleaseSelect,
                    sharedResourceService.GetString("to day")));
        }
    }
}
