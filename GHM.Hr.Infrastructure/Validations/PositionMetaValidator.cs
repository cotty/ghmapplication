﻿using System.Text.RegularExpressions;
using FluentValidation;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;

namespace GHM.Hr.Infrastructure.Validations
{
    public class PositionMetaValidator : AbstractValidator<PositionMeta>
    {
        public PositionMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> resourceService)
        {
            RuleFor(x => x.TitleId)
                .NotNull()
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("Please select {0}", resourceService.GetString("Title")))
                .MaximumLength(50)
                .WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    resourceService.GetString("Title id"), 50));

            RuleFor(x => x.IsActive)
                .NotNull()
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("Please select {0}", resourceService.GetString("Active status")));

            RuleFor(x => x.IsMultiple)
                .NotNull()
                .WithMessage(sharedResourceService.GetString("Please select {0}", resourceService.GetString("Multiple status")));

            RuleFor(x => x.IsManager)
                .NotNull()
                .WithMessage(sharedResourceService.GetString("Please select {0}", resourceService.GetString("Manager status")));

            RuleFor(x => x.Translations)
                .Must((title, titleTranslation) => title.Translations.Count > 0)
                .WithMessage(sharedResourceService.GetString("Please select at least one language."));

            RuleForEach(x => x.Translations)
                .SetValidator(new PositionTranslationValidator(sharedResourceService, resourceService));
        }
    }

    public class PositionTranslationValidator : AbstractValidator<PositionTranslationMeta>
    {
        public PositionTranslationValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> resourceService)
        {
            RuleFor(x => x.LanguageId)
                .NotNull()
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                    sharedResourceService.GetString("Language code")))
                .MaximumLength(10).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    sharedResourceService.GetString("Language code"), 10));

            RuleFor(x => x.Name)
                .NotNull()
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                    resourceService.GetString("Position name")))
                .MaximumLength(256).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    resourceService.GetString("Position name"), 256));

            RuleFor(x => x.ShortName)
                .NotNull()
                .NotEmpty()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                    resourceService.GetString("Short name")))
                .MaximumLength(50).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    resourceService.GetString("Short name"), 50))
                .Matches(new Regex(@"^[a-zA-Z0-9]+$"))
                    .WithMessage(sharedResourceService.GetString("{0} just only contain a-z, A-Z, 0-9.", resourceService.GetString("Short name")));

            RuleFor(x => x.Description)
                .MaximumLength(500)
                .WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.", sharedResourceService.GetString("Description"), 500));

            RuleFor(x => x.OtherRequire)
                .MaximumLength(4000)
                .WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.", sharedResourceService.GetString("OtherRequire"), 4000));

            RuleFor(x => x.Purpose)
                .MaximumLength(4000)
                .WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.", sharedResourceService.GetString("Purpose"), 4000));

            RuleFor(x => x.Responsibility)
                .MaximumLength(4000)
                .WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.", sharedResourceService.GetString("Responsibility"), 4000));
        }
    }
}
