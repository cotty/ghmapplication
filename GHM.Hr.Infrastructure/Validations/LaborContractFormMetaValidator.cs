﻿using FluentValidation;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Helpers.Validations;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;

namespace GHM.Hr.Infrastructure.Validations
{
    class LaborContractFormMetaValidator : AbstractValidator<LaborContractFormMeta>
    {
        public LaborContractFormMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> resourceService)
        {
            RuleFor(x => x.LaborContractTypeId)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseSelect,
                    sharedResourceService.GetString("Loại hợp đồng")))
               .MaximumLength(50).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("Loại hợp đồng"), 50));

            RuleFor(x => x.FileId)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseSelect,
                    sharedResourceService.GetString("File")))
               .MaximumLength(50).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("File"), 50));

            RuleFor(x => x.Name)
              .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseEnter,
                  sharedResourceService.GetString("Tên mẫu hợp đồng")))
             .MaximumLength(50).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                  resourceService.GetString("Tên mẫu hợp đồng"), 256));
            
            RuleFor(x => x.Description)
             .MaximumLength(500).WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                  resourceService.GetString("Ghi chú"), 4000));
        }
    }
}
