﻿using FluentValidation;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;

namespace GHM.Hr.Infrastructure.Validations
{
    class UserValueMetaValidator : AbstractValidator<UserValueMeta>
    {
        public UserValueMetaValidator(IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmHrResource> resourceService)
        {
            RuleFor(x => x.Type)
                .IsInEnum()
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.InValid,
                    resourceService.GetString("User value type")));

            RuleFor(x => x.Name)
                .NotNull()
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.NotNull,
                    resourceService.GetString("User Value")))
                .MaximumLength(250)
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.MaxLength,
                    resourceService.GetString("User Value"), 250));
        }
    }
}
