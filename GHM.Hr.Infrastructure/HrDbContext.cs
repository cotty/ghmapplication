﻿using GHM.Hr.Domain.Models;
using GHM.Hr.Infrastructure.Configurations;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;

namespace GHM.Hr.Infrastructure
{
    public class HrDbContext : DbContextBase
    {
        public HrDbContext(DbContextOptions<HrDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Configurations:
            //builder.ApplyConfiguration(new UserConfig());
            builder.ApplyConfiguration(new CriteriaConfig());
            builder.ApplyConfiguration(new CriteriaTranslationConfig());
            builder.ApplyConfiguration(new CriteriaGroupConfig());
            builder.ApplyConfiguration(new CriteriaGroupTranslationConfig());
            builder.ApplyConfiguration(new CriteriaPositionConfig());
            builder.ApplyConfiguration(new OfficeConfig());
            builder.ApplyConfiguration(new OfficeTranslationConfig());
            builder.ApplyConfiguration(new OfficePositionConfig());
            builder.ApplyConfiguration(new OfficeContactConfig());
            builder.ApplyConfiguration(new PositionConfig());
            builder.ApplyConfiguration(new PositionTranslationConfig());
            builder.ApplyConfiguration(new TitleConfig());
            builder.ApplyConfiguration(new TitleTranslationConfig());
            builder.ApplyConfiguration(new UserAssessmentConfig());
            builder.ApplyConfiguration(new UserCommendationDisciplineConfig());
            builder.ApplyConfiguration(new UserCommendationDisciplineCategoryConfig());
            builder.ApplyConfiguration(new UserEmploymentHistoryConfig());
            builder.ApplyConfiguration(new UserPositionConfig());
            builder.ApplyConfiguration(new ConfigConfig());
            builder.ApplyConfiguration(new UserAssessmentCriteriaCommentConfig());
            builder.ApplyConfiguration(new UserRecordsManagementConfig());

            builder.ApplyConfiguration(new UserInsuranceConfig());
            builder.ApplyConfiguration(new UserLaborContractConfig());
            builder.ApplyConfiguration(new UserTrainingHistoryConfig());
            builder.ApplyConfiguration(new UserHolidayRemainingConfig());
            builder.ApplyConfiguration(new UserContactConfig());
            builder.ApplyConfiguration(new UserBankingConfig());
            builder.ApplyConfiguration(new UserAssessmentCriteriaConfig());
            builder.ApplyConfiguration(new UserTranslationConfig());
            builder.ApplyConfiguration(new UserTrainingHistoryConfig());
            builder.ApplyConfiguration(new LaborContractFormConfig());
            builder.ApplyConfiguration(new RelatedUserConfig());
            // Entities Register:
            builder.Entity<User>(x => x.ToTable("Users"));
            builder.Entity<AcademicLevel>(x => x.ToTable("AcademicLevels"));
            builder.Entity<UserAssessmentCriteria>(x => x.ToTable("UserAssessmentCriteria"));
            builder.Entity<UserRecordsManagement>(x => x.ToTable("UserRecordsManagement"));
            builder.Entity<UserValue>(x => x.ToTable("UserValue"));
            builder.Entity<UserTrainingHistory>(x => x.ToTable("UserTrainingHistory"));
            builder.Entity<UserRecordsManagement>(x => x.ToTable("UserRecordsManagement"));
            #region Config foreign keys.           

            #endregion
        }
    }
}
