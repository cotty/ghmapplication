﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class CriteriaGroupTranslationConfig : IEntityTypeConfiguration<CriteriaGroupTranslation>
    {
        public void Configure(EntityTypeBuilder<CriteriaGroupTranslation> builder)
        {
            builder.Property(x => x.TenantId).IsRequired().HasMaxLength(50);
            builder.Property(x => x.CriteriaGroupId).IsRequired().HasMaxLength(50);
            builder.Property(x => x.LanguageId).IsRequired().HasMaxLength(10).IsUnicode(false);
            builder.Property(x => x.Name).IsRequired().HasMaxLength(256);
            builder.Property(x => x.Description).IsRequired(false).HasMaxLength(500);
            builder.Property(x => x.UnsignName).IsRequired().HasMaxLength(256).IsUnicode(false);
            builder.HasOne(ct => ct.CriteriaGroup)
                .WithMany(c => c.Translations);

            builder.ToTable("CriteriaGroupTranslations").HasKey(x => new { x.TenantId, x.LanguageId, x.CriteriaGroupId });
        }
    }
}
