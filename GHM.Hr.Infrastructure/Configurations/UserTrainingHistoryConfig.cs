﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class UserTrainingHistoryConfig : IEntityTypeConfiguration<UserTrainingHistory>
    {
        public void Configure(EntityTypeBuilder<UserTrainingHistory> builder)
        {
            builder.Property(x => x.Id).HasColumnName("Id").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.Type).HasColumnName("Type").IsRequired().HasColumnType("bit");
            builder.Property(x => x.UserId).HasColumnName("UserId").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
            builder.Property(x => x.FullName).HasColumnName("FullName").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
            builder.Property(x => x.CourseId).HasColumnName("CourseId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.CourseName).HasColumnName("CourseName").IsRequired().HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.FromDate).HasColumnName("FromDate").IsRequired().HasColumnType("datetime2");
            builder.Property(x => x.ToDate).HasColumnName("ToDate").IsRequired().HasColumnType("datetime2");
            builder.Property(x => x.Result).HasColumnName("Result").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(4000);
            builder.Property(x => x.CoursePlaceId).HasColumnName("CoursePlaceId").IsRequired(false).IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.CoursePlaceName).HasColumnName("CoursePlaceName").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.IsHasCertificate).HasColumnName("IsHasCertificate").IsRequired().HasColumnType("bit");
            builder.Property(x => x.UnsignName).HasColumnName("UnsignName").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(500);
            builder.Property(x => x.CreateTime).HasColumnName("CreateTime").IsRequired().HasColumnType("datetime2");
            builder.Property(x => x.IsDelete).HasColumnName("IsDelete").IsRequired().HasColumnType("bit");
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.ConcurrencyStamp).HasColumnName("ConcurrencyStamp").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);

            builder.ToTable("UserTrainingHistory").HasKey(x=> x.Id);
        }
    }
}
