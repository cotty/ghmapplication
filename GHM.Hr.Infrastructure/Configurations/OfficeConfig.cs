﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class OfficeConfig : IEntityTypeConfiguration<Office>
    {
        public void Configure(EntityTypeBuilder<Office> builder)
        {
            builder.Property(x => x.TenantId).IsUnicode(false).IsRequired().HasMaxLength(50);
            builder.Property(x => x.Code).IsUnicode(false).IsRequired().HasMaxLength(50);
            builder.Property(x => x.OrderPath).IsUnicode(false).IsRequired().HasMaxLength(500);
            builder.Property(x => x.IdPath).IsUnicode(false).IsRequired().HasMaxLength(500);
            builder.Property(x => x.RootIdPath).IsUnicode(false).IsRequired(false).HasMaxLength(500);
            builder.ToTable("Offices").HasIndex(x => x.IdPath);
        }
    }
}
