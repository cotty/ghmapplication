﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class UserLaborContractConfig : IEntityTypeConfiguration<UserLaborContract>
    {
        public void Configure(EntityTypeBuilder<UserLaborContract> builder)
        {
            builder.Property(x => x.Id).HasColumnName("Id").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.No).HasColumnName("No").IsRequired().HasColumnType("nvarchar").HasMaxLength(100);
            builder.Property(x => x.UserId).HasColumnName("UserId").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
            builder.Property(x => x.FullName).HasColumnName("FullName").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
            builder.Property(x => x.OfficeId).HasColumnName("OfficeId").IsRequired(false).HasColumnType("int");
            builder.Property(x => x.OfficeName).HasColumnName("OfficeName").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.PositionId).HasColumnName("PositionId").IsRequired(false).IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.PositionName).HasColumnName("PositionName").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.TitleId).HasColumnName("TitleId").IsRequired(false).IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.TitleName).HasColumnName("TitleName").IsRequired(false).IsUnicode(false).HasColumnType("varchar").HasMaxLength(256);
            builder.Property(x => x.Type).HasColumnName("Type").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.TypeName).HasColumnName("TypeName").IsRequired().HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.FromDate).HasColumnName("FromDate").IsRequired().HasColumnType("datetime2");
            builder.Property(x => x.ToDate).HasColumnName("ToDate").IsRequired(false).HasColumnType("datetime2");
            builder.Property(x => x.CreateTime).HasColumnName("CreateTime").IsRequired().HasColumnType("datetime2");
            builder.Property(x => x.CreatorId).HasColumnName("CreatorId").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
            builder.Property(x => x.Note).HasColumnName("Note").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(500);
            builder.Property(x => x.FileId).HasColumnName("FileId").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(500);
            builder.Property(x => x.FileName).HasColumnName("FileName").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.IsUse).HasColumnName("IsUse").IsRequired().HasColumnType("bit");
            builder.Property(x => x.UnsignName).HasColumnName("UnsignName").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(500);
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.ConcurrencyStamp).HasColumnName("ConcurrencyStamp").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.LaborContractFormId).HasColumnName("LaborContractFormId").IsRequired(false).IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.LaborContractFormName).HasColumnName("LaborContractFormName").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.RegisterDate).HasColumnName("RegisterDate").IsRequired(false).HasColumnType("date");
            builder.Property(x => x.TaskDescription).HasColumnName("TaskDescription").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(4000);
            builder.Property(x => x.Time).HasColumnName("Time").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.SalaryCalculationUnit).HasColumnName("SalaryCalculationUnit").IsRequired(false).HasColumnType("int");
            builder.Property(x => x.BasicSalary).HasColumnName("BasicSalary").IsRequired(false).HasColumnType("float");
            builder.Property(x => x.ProbationarySalary).HasColumnName("ProbationarySalary").IsRequired(false).HasColumnType("decimal");
            builder.Property(x => x.OfficialSalary).HasColumnName("OfficialSalary").IsRequired(false).HasColumnType("decimal");
            builder.Property(x => x.Status).HasColumnName("Status").IsRequired(false).HasColumnType("int");

            builder.ToTable("UserLaborContract").HasKey(x => x.Id);
        }
    }
}
