﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    class UserAssessmentCriteriaCommentConfig : IEntityTypeConfiguration<UserAssessmentCriteriaComment>
    {
        public void Configure(EntityTypeBuilder<UserAssessmentCriteriaComment> builder)
        {
            builder.Property(x => x.Id).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.UserId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.FullName).IsRequired().HasMaxLength(50).IsUnicode();
            builder.Property(x => x.Avatar).IsRequired(false).HasMaxLength(500).IsUnicode(false);
            builder.Property(x => x.Content).IsRequired(false).HasMaxLength(4000).IsUnicode();
            builder.Property(x => x.CreateTime).IsRequired().HasColumnType("DateTime");
            builder.Property(x => x.LastUpdate).IsRequired(false).HasColumnType("DateTime");
            builder.Property(x => x.ConcurrencyStamp).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.UserAssessmentCriteriaId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.IsDelete).IsRequired().HasColumnType("bit");

            builder.HasOne(x => x.UserAssessmentCriteria)
                .WithMany(x => x.UserAssessmentCriteriaComments);

            builder.ToTable("UserAssessmentCriteriaComments").HasKey(x => x.Id);
        }
    }
}
