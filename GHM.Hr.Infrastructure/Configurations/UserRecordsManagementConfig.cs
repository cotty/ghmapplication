using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using GHM.Hr.Domain.Models;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class UserRecordsManagementConfig : IEntityTypeConfiguration<UserRecordsManagement>
    {
        public void Configure(EntityTypeBuilder<UserRecordsManagement> builder)
        {
            builder.Property(x => x.Id).HasColumnName("Id").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.UserId).HasColumnName("UserId").IsRequired().HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.ValueId).HasColumnName("ValueId").IsRequired().HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.Note).HasColumnName("Note").IsRequired().HasColumnType("nvarchar").HasMaxLength(500);
            builder.Property(x => x.IsSelected).HasColumnName("IsSelected").IsRequired().HasColumnType("bit");
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.ConcurrencyStamp).HasColumnName("ConcurrencyStamp").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.AttachmentId).HasColumnName("AttachmentId").HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.AttachmentName).HasColumnName("AttachmentName").HasColumnType("nvarchar").HasMaxLength(500);
            builder.Property(x => x.Title).HasColumnName("Title").IsRequired().HasColumnType("nvarchar").HasMaxLength(500);
            builder.Property(x => x.IsDelete).HasColumnName("IsDelete").IsRequired().HasColumnType("bit");
            builder.ToTable("UserRecordsManagement").HasKey(x => x.Id);
        }
    }
}
