﻿using System;
using System.Collections.Generic;
using System.Text;
using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class UserAssessmentCriteriaConfig : IEntityTypeConfiguration<UserAssessmentCriteria>
    {
        public void Configure(EntityTypeBuilder<UserAssessmentCriteria> builder)
        {
            builder.Property(x => x.Id).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.UserAssessmentId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.CriteriaId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.UserPoint).IsRequired().HasColumnType("decimal(5,2)");
            builder.Property(x => x.ManagerPoint).IsRequired(false).HasColumnType("decimal(5,2)");
            builder.Property(x => x.ApproverPoint).IsRequired(false).HasColumnType("decimal(5,2)");
            builder.Property(x => x.Note).IsRequired(false).IsUnicode().HasMaxLength(500);

            builder.HasOne(x => x.UserAssessment)
                .WithMany(x => x.UserAssessmentCriterias);

            builder.HasMany(x => x.UserAssessmentCriteriaComments)
                .WithOne(x => x.UserAssessmentCriteria);

            builder.ToTable("UserAssessmentCriterias").HasKey(x => x.Id);
        }
    }
}
