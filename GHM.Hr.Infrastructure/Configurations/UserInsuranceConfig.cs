﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class UserInsuranceConfig : IEntityTypeConfiguration<UserInsurance>
    {
        public void Configure(EntityTypeBuilder<UserInsurance> builder)
        {
            builder.Property(x => x.Id).HasColumnName("Id").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.Type).HasColumnName("Type").IsRequired().HasColumnType("bit");
            builder.Property(x => x.CompanyId).HasColumnName("CompanyId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.CompanyName).HasColumnName("CompanyName").IsRequired().HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.UnsignName).HasColumnName("UnsignName").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(250);
            builder.Property(x => x.FromDate).HasColumnName("FromDate").IsRequired().HasColumnType("datetime2");
            builder.Property(x => x.ToDate).HasColumnName("ToDate").IsRequired(false).HasColumnType("datetime2");
            builder.Property(x => x.Premium).HasColumnName("Premium").IsRequired().HasColumnType("decimal");
            builder.Property(x => x.Note).HasColumnName("Note").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(500);
            builder.Property(x => x.UserId).HasColumnName("UserId").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
            builder.Property(x => x.FullName).HasColumnName("FullName").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
            builder.Property(x => x.CreateTime).HasColumnName("CreateTime").IsRequired().HasColumnType("datetime2");
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.ConcurrencyStamp).HasColumnName("ConcurrencyStamp").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            
            builder.ToTable("UserInsurance").HasKey(x=> x.Id);
        }
    }
}
