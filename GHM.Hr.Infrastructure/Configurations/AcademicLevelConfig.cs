using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;
using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class AcademicLevelConfig : IEntityTypeConfiguration<AcademicLevel>
    {
        public void Configure(EntityTypeBuilder<AcademicLevel> builder)
        {
            builder.Property(x => x.Id).HasColumnName("Id").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(50);
            builder.Property(x => x.AcademicLevelId).HasColumnName("AcademicLevelId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.AcademicLevelName).HasColumnName("AcademicLevelName").IsRequired().HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.DegreeId).HasColumnName("DegreeId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.DegreeName).HasColumnName("DegreeName").IsRequired().HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.SchoolId).HasColumnName("SchoolId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.SchoolName).HasColumnName("SchoolName").IsRequired().HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.SpecializeId).HasColumnName("SpecializeId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.SpecializeName).HasColumnName("SpecializeName").IsRequired().HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.UserId).HasColumnName("UserId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.IsDelete).HasColumnName("IsDelete").IsRequired().HasColumnType("bit");
            builder.Property(x => x.Note).HasColumnName("Note").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(500);
            builder.Property(x => x.ConcurrencyStamp).HasColumnName("ConcurrencyStamp").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);

            builder.ToTable("AcademicLevels").HasKey(x => x.Id);
        }
    }

}
