﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class OfficeTranslationConfig : IEntityTypeConfiguration<OfficeTranslation>
    {
        public void Configure(EntityTypeBuilder<OfficeTranslation> builder)
        {
            builder.Property(x => x.OfficeId).IsRequired();
            builder.Property(x => x.LanguageId).IsRequired().HasMaxLength(10).IsUnicode(false);
            builder.Property(x => x.Description).IsRequired(false).HasMaxLength(500);
            builder.Property(x => x.Address).IsRequired(false).HasMaxLength(500);
            builder.Property(x => x.Name).IsRequired().HasMaxLength(256);
            builder.Property(x => x.NamePath).IsRequired().HasMaxLength(4000);
            builder.Property(x => x.ParentName).IsRequired(false).HasMaxLength(256);
            builder.Property(x => x.ShortName).IsRequired().HasMaxLength(50);
            builder.Property(x => x.UnsignName).IsRequired().HasMaxLength(256);
            builder.ToTable("OfficeTranslations").HasKey(x => new { x.OfficeId, x.LanguageId });
        }
    }
}
