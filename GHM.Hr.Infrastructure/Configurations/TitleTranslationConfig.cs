﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class TitleTranslationConfig : IEntityTypeConfiguration<TitleTranslation>
    {
        public void Configure(EntityTypeBuilder<TitleTranslation> builder)
        {
            builder.Property(x => x.TenantId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.TitleId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.LanguageId).IsRequired().HasMaxLength(10).IsUnicode(false);
            builder.Property(x => x.Name).IsRequired().HasMaxLength(256);
            builder.Property(x => x.ShortName).IsRequired().HasMaxLength(20);
            builder.Property(x => x.UnsignName).IsRequired().HasMaxLength(256).IsUnicode(false);
            builder.Property(x => x.Description).IsRequired(false).HasMaxLength(500);

            builder.HasOne(x => x.Title)
                .WithMany(x => x.Translations);
            builder.ToTable("TitleTranslations").HasKey(x => new { x.TenantId, x.TitleId, x.LanguageId });
        }
    }
}
