﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class CriteriaTranslationConfig : IEntityTypeConfiguration<CriteriaTranslation>
    {
        public void Configure(EntityTypeBuilder<CriteriaTranslation> builder)
        {
            builder.Property(c => c.Name).HasColumnType("nvarchar(256)");
            builder.Property(c => c.LanguageId).HasColumnType("varchar(10)");
            builder.Property(c => c.CriteriaId).HasColumnType("varchar(50)");
            builder.Property(c => c.UnsignName).HasColumnType("nvarchar(500)");
            builder.Property(c => c.Description).HasColumnType("nvarchar(4000)");
            builder.Property(c => c.Sanction).HasColumnType("nvarchar(4000)");
            builder.HasOne(ct => ct.Criteria)
                .WithMany(c => c.Translations);
            builder.ToTable("CriteriaTranslations").HasKey(x => new { x.TenantId, x.CriteriaId, x.LanguageId });
        }
    }
}
