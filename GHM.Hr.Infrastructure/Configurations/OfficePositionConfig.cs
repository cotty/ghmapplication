﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class OfficePositionConfig : IEntityTypeConfiguration<OfficePosition>
    {
        public void Configure(EntityTypeBuilder<OfficePosition> builder)
        {
            builder.Property(x => x.PositionId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.OfficeId).IsRequired();
            builder.Property(x => x.TitleId).IsRequired(false).HasMaxLength(50).IsUnicode(false);           
            builder.ToTable("OfficesPositions").HasKey(x => new { x.OfficeId, x.PositionId });            
        }
    }
}
