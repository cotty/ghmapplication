﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class CriteriaPositionConfig : IEntityTypeConfiguration<CriteriaPosition>
    {
        public void Configure(EntityTypeBuilder<CriteriaPosition> builder)
        {
            builder.Property(x => x.TenantId).IsRequired().HasMaxLength(50);
            builder.Property(x => x.CriteriaId).IsRequired().HasMaxLength(50);
            builder.Property(x => x.PositionId).IsRequired().HasMaxLength(50);
            builder.ToTable("CriteriasPositions").HasKey(x => new { x.CriteriaId, TitleId = x.PositionId });
        }
    }
}
