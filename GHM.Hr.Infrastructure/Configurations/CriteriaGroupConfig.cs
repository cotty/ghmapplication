﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class CriteriaGroupConfig : IEntityTypeConfiguration<CriteriaGroup>
    {
        public void Configure(EntityTypeBuilder<CriteriaGroup> builder)
        {
            builder.Property(x => x.Id).IsRequired().HasMaxLength(50);
            builder.Property(x => x.TenantId).IsRequired().HasMaxLength(50);
            builder.Property(x => x.IsActive).IsRequired();
            builder.Property(x => x.Order).IsRequired();
            builder.Property(x => x.IsDelete).IsRequired();
            builder.Property(x => x.ConcurrencyStamp).IsRequired().HasMaxLength(50);

            builder.HasMany(x => x.Translations)
                .WithOne(x => x.CriteriaGroup);

            builder.ToTable("CriteriaGroups").HasKey(x => x.Id);
        }
    }
}
