﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class OfficeContactConfig : IEntityTypeConfiguration<OfficeContact>
    {
        public void Configure(EntityTypeBuilder<OfficeContact> builder)
        {
            builder.Property(x => x.OfficeId).IsRequired();
            builder.Property(x => x.UserId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.FullName).IsRequired().HasMaxLength(50);
            builder.Property(x => x.Avatar).IsRequired(false).HasMaxLength(500).IsUnicode(false);
            builder.Property(x => x.TitlePrefixing).IsRequired();
            builder.Property(x => x.Email).IsRequired(false).HasMaxLength(500).IsUnicode(false);
            builder.Property(x => x.PhoneNumber).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.Fax).IsRequired(false).HasMaxLength(50).IsUnicode(false);
            builder.ToTable("OfficeContacts");
        }
    }
}
