﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class PositionTranslationConfig : IEntityTypeConfiguration<PositionTranslation>
    {
        public void Configure(EntityTypeBuilder<PositionTranslation> builder)
        {
            builder.Property(x => x.TenantId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.PositionId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.LanguageId).IsRequired().HasMaxLength(10).IsUnicode(false);
            builder.Property(x => x.Name).IsRequired().HasMaxLength(256);
            builder.Property(x => x.ShortName).IsRequired().HasMaxLength(20);
            builder.Property(x => x.UnsignName).IsRequired().HasMaxLength(256).IsUnicode(false);
            builder.Property(x => x.Description).IsRequired(false).HasMaxLength(500);
            builder.Property(x => x.TitleName).IsRequired(false).HasMaxLength(256);
            builder.Property(x => x.Purpose).IsRequired(false).HasMaxLength(4000);
            builder.Property(x => x.OtherRequire).IsRequired(false).HasMaxLength(4000);
            builder.Property(x => x.Responsibility).IsRequired(false).HasMaxLength(4000);
            builder.HasOne(x => x.Position)
                .WithMany(x => x.Translations);
            builder.ToTable("PositionTranslations").HasKey(x => new { x.TenantId, x.PositionId, x.LanguageId });
        }
    }
}
