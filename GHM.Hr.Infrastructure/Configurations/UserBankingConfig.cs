﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class UserBankingConfig : IEntityTypeConfiguration<UserBanking>
    {
        public void Configure(EntityTypeBuilder<UserBanking> builder)
        {
            builder.Property(x => x.Id).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.AccountNumber).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.Branch).IsRequired().HasMaxLength(500);
            builder.Property(x => x.Name).IsRequired().HasMaxLength(500);
            builder.Property(x => x.UserId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.ToTable("UserBankings").HasKey(x => x.Id);
        }
    }
}
