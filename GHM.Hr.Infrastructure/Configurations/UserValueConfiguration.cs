using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;
using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class UserValueConfig : IEntityTypeConfiguration<UserValue>
    {
        public void Configure(EntityTypeBuilder<UserValue> builder)
        {
            builder.Property(x => x.Id).HasColumnName("Id").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.Name).HasColumnName("Name").IsRequired().HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.UnsignName).HasColumnName("UnsignName").IsRequired().HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.Type).HasColumnName("Type").IsRequired().HasColumnType("int");
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);

            builder.ToTable("UserValue").HasKey(x => x.Id);
        }
    }
}
