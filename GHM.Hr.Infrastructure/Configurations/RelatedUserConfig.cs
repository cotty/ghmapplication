﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class RelatedUserConfig : IEntityTypeConfiguration<RelatedUser>
    {
        public void Configure(EntityTypeBuilder<RelatedUser> builder)
        {
            builder.Property(x => x.Id).HasColumnName("Id").IsRequired().IsUnicode().HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired().IsUnicode().HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.UserId).HasColumnName("UserId").IsRequired().IsUnicode().HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.ValueId).HasColumnName("ValueId").IsRequired().IsUnicode().HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.ValueName).HasColumnName("ValueName").IsRequired().IsUnicode().HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.FullName).HasColumnName("FullName").IsRequired().HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.BirthDay).IsRequired().HasColumnType("Birthday");
            builder.Property(x => x.IdCardNumber).HasColumnName("IdCardNumber").IsRequired().IsUnicode().HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.IdCardDateOfIssue).IsRequired().HasColumnType("IdCardDateOfIssue");
            builder.Property(x => x.IdCardPlaceOfIssue).HasColumnName("IdCardPlaceOfIssue").IsRequired().HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.Phone).HasColumnName("Phone").IsRequired().IsUnicode().HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.NationalId).IsRequired().HasColumnType("NationalId");
            builder.Property(x => x.ProvinceId).IsRequired().HasColumnType("ProvinceId");
            builder.Property(x => x.DistrictId).IsRequired().HasColumnType("DistrictId");
            builder.Property(x => x.Ethnic).IsRequired().HasColumnType("Ethnic");
            builder.Property(x => x.DenominationName).HasColumnName("DenominationName").HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.Job).HasColumnName("Job").HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.PermanentAddress).HasColumnName("PermanentAddress").HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.ContactAddress).HasColumnName("ContactAddress").HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.CreatorId).HasColumnName("CreatorId").HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.CreatorFullName).HasColumnName("CreatorFullName").HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.CreateTime).IsRequired().HasColumnType("CreateTime");
            builder.Property(x => x.ConcurrencyStamp).IsRequired().HasColumnType("ConcurrencyStamp");

            builder.ToTable("RelatedUser").HasKey(x => x.Id);
        }
    }
}
