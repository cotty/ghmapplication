﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class UserAssessmentConfig : IEntityTypeConfiguration<UserAssessment>
    {
        public void Configure(EntityTypeBuilder<UserAssessment> builder)
        {
            builder.Property(x => x.Id).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.TenantId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.UserId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.PositionId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.OfficeId).IsRequired();
            builder.Property(x => x.ManagerUserId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.ApproverUserId).IsRequired(false).HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.TotalPoint).IsRequired().HasColumnType("decimal(5,2)");
            builder.Property(x => x.TotalUserPoint).IsRequired().HasColumnType("decimal(5,2)");
            builder.Property(x => x.TotalManagerPoint).IsRequired(false).HasColumnType("decimal(5,2)");
            builder.Property(x => x.TotalApproverPoint).IsRequired(false).HasColumnType("decimal(5,2)");
            builder.Property(x => x.UserNote).IsRequired(false).HasMaxLength(500);
            builder.Property(x => x.ManagerNote).IsRequired(false).HasMaxLength(500);
            builder.Property(x => x.ApproverNote).IsRequired(false).HasMaxLength(500);
            builder.Property(x => x.ManagerApproveTime).IsRequired(false);
            builder.Property(x => x.ApproverApproveTime).IsRequired(false);
            builder.Property(x => x.Month).IsRequired().HasColumnType("byte");
            builder.Property(x => x.Quarter).IsRequired().HasColumnType("byte");
            builder.Property(x => x.Year).IsRequired().HasColumnType("int");
            builder.HasMany(x => x.UserAssessmentCriterias)
                .WithOne(x => x.UserAssessment);
            builder.ToTable("UserAssessments").HasKey(x => x.Id);
        }
    }
}
