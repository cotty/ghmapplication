﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class ConfigConfig : IEntityTypeConfiguration<Config>
    {
        public void Configure(EntityTypeBuilder<Config> builder)
        {
            builder.Property(x => x.Id).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.TenantId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.LanguageId).HasMaxLength(10).IsUnicode(false);
            builder.Property(x => x.Key).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.Value).IsRequired().IsUnicode();
            builder.ToTable("Configs").HasKey(x => x.Id);
        }
    }
}
