﻿using GHM.Hr.Domain.Models;
using GHM.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(x => x.Id).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.UserName).IsRequired().HasMaxLength(30).IsUnicode(false);
            builder.Property(x => x.FullName).IsRequired().HasMaxLength(50).IsUnicode();
            builder.Property(x => x.FirstName).IsRequired().HasMaxLength(15).IsUnicode();
            builder.Property(x => x.MiddleName).IsRequired().HasMaxLength(15).IsUnicode();
            builder.Property(x => x.LastName).IsRequired().HasMaxLength(15).IsUnicode();
            builder.Property(x => x.IdCardNumber).IsRequired(false).HasMaxLength(30).IsUnicode();
            builder.Property(x => x.Birthday).IsRequired();
            builder.Property(x => x.Gender).IsRequired();
            builder.Property(x => x.Tin).IsRequired(false).HasMaxLength(20).IsUnicode(false);
            builder.Property(x => x.Avatar).IsRequired(false).HasMaxLength(500).IsUnicode(false);
            builder.Property(x => x.OfficeId).IsRequired();
            builder.Property(x => x.TitleId).IsRequired();
            builder.Property(x => x.PositionId).IsRequired();
            builder.Property(x => x.BankingNumber).IsRequired(false).HasMaxLength(30).IsUnicode(false);
            builder.Property(x => x.ApproverFullName).IsRequired(false).HasMaxLength(50).IsUnicode();
            builder.Property(x => x.ApproverUserId).IsRequired(false).HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.CardNumber).IsRequired(false).HasMaxLength(20).IsUnicode(false);
            builder.Property(x => x.ProvinceId).IsRequired(false);
            builder.Property(x => x.ProvinceName).IsRequired(false).HasMaxLength(250).IsUnicode();
            builder.Property(x => x.NationalId).IsRequired(false).IsUnicode(false);
            builder.Property(x => x.DistrictId).IsRequired(false);
            builder.Property(x => x.DistrictName).IsRequired(false).HasMaxLength(250).IsUnicode();
            builder.Property(x => x.EthnicName).IsRequired(false).HasMaxLength(250).IsUnicode();
            builder.Property(x => x.ManagerFullName).IsRequired(false).HasMaxLength(50).IsUnicode();
            builder.Property(x => x.ManagerUserId).IsRequired(false).HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.OfficeIdPath).IsRequired(false).HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.PassportId).IsRequired(false).HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.PositionId).IsRequired(false).HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.TenantId).IsRequired(false).HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.UnsignName).IsRequired().HasMaxLength(100).IsUnicode(false);
            builder.Property(x => x.Ext).IsRequired(false).HasMaxLength(15).IsUnicode(false);
            builder.Property(x => x.Denomination).IsRequired(false);
            builder.Property(x => x.DenominationName).IsRequired(false).HasMaxLength(100).IsUnicode();
            builder.Property(x => x.CreatorId).IsRequired(false).HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.CreatorFullName).IsRequired(false).HasMaxLength(50).IsUnicode(true);

            builder.ToTable("Users").HasKey(x => x.Id);
        }
    }
}
