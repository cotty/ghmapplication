using Microsoft.EntityFrameworkCore;
using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace GHM.Hr.Infrastructure.Configurations
{
    public class LaborContractFormConfig : IEntityTypeConfiguration<LaborContractForm>
    {
        public void Configure(EntityTypeBuilder<LaborContractForm> builder)
        {
            builder.Property(x => x.Id).HasColumnName("Id").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.LaborContractTypeId).HasColumnName("LaborContractTypeId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.FileId).HasColumnName("FileId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.FileName).HasColumnName("FileName").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.IsActive).HasColumnName("IsActive").IsRequired().HasColumnType("bit");
            builder.Property(x => x.IsDelete).HasColumnName("IsDelete").IsRequired().HasColumnType("bit");
            builder.Property(x => x.Description).HasColumnName("Description").IsRequired(false).IsUnicode(false).HasColumnType("varchar").HasMaxLength(4000);
            builder.Property(x => x.ConcurrencyStamp).HasColumnName("ConcurrencyStamp").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.Name).HasColumnName("Name").IsRequired().HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.UnsignName).HasColumnName("UnsignName").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(256);

            builder.ToTable("LaborContractForms").HasKey(x => x.Id);
        }
    }
}
