﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class UserCommendationDisciplineConfig : IEntityTypeConfiguration<UserCommendationDiscipline>
    {
        public void Configure(EntityTypeBuilder<UserCommendationDiscipline> builder)
        {
            builder.Property(x => x.Id).HasColumnName("Id").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.UserId).HasColumnName("UserId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.FullName).HasColumnName("FullName").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
            builder.Property(x => x.IsCommendation).HasColumnName("IsCommendation").IsRequired().HasColumnType("bit");
            builder.Property(x => x.CreateTime).HasColumnName("CreateTime").IsRequired().HasColumnType("datetime2");
            builder.Property(x => x.ApplyTime).HasColumnName("ApplyTime").IsRequired().HasColumnType("datetime2");
            builder.Property(x => x.DecisionNo).HasColumnName("DecisionNo").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.Reason).HasColumnName("Reason").IsRequired().HasColumnType("nvarchar").HasMaxLength(4000);
            builder.Property(x => x.Money).HasColumnName("Money").IsRequired(false).HasColumnType("decimal");
            builder.Property(x => x.CategoryId).HasColumnName("CategoryId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.CategoryName).HasColumnName("CategoryName").IsRequired().HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.UnsignName).HasColumnName("UnsignName").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(500);
            builder.Property(x => x.AttachmentUrl).HasColumnName("AttachmentUrl").IsRequired(false).IsUnicode(false).HasColumnType("varchar").HasMaxLength(500);
            builder.Property(x => x.OfficeId).HasColumnName("OfficeId").IsRequired().HasColumnType("int");
            builder.Property(x => x.OfficeName).HasColumnName("OfficeName").IsRequired().HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.TitleId).HasColumnName("TitleId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.TitleName).HasColumnName("TitleName").IsRequired().HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.Month).HasColumnName("Month").IsRequired().HasColumnType("int");
            builder.Property(x => x.Quarter).HasColumnName("Quarter").IsRequired().HasColumnType("int");
            builder.Property(x => x.Year).HasColumnName("Year").IsRequired().HasColumnType("int");
            builder.Property(x => x.IsDelete).HasColumnName("IsDelete").IsRequired().HasColumnType("bit");
            builder.Property(x => x.CreatorId).HasColumnName("CreatorId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.CreatorFullName).HasColumnName("CreatorFullName").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.ConcurrencyStamp).HasColumnName("ConcurrencyStamp").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);

            builder.ToTable("UserCommendationDisciplines").HasKey(x=> x.Id);
        }
    }
}
