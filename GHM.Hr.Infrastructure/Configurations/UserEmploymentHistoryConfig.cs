﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class UserEmploymentHistoryConfig : IEntityTypeConfiguration<UserEmploymentHistory>
    {
        public void Configure(EntityTypeBuilder<UserEmploymentHistory> builder)
        {
            builder.Property(x => x.Id).HasColumnName("Id").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.UserId).HasColumnName("UserId").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
            builder.Property(x => x.FullName).HasColumnName("FullName").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
            builder.Property(x => x.Type).HasColumnName("Type").IsRequired().HasColumnType("bit");
            builder.Property(x => x.FromDate).HasColumnName("FromDate").IsRequired().HasColumnType("datetime2");
            builder.Property(x => x.ToDate).HasColumnName("ToDate").IsRequired(false).HasColumnType("datetime2");
            builder.Property(x => x.OfficeId).HasColumnName("OfficeId").IsRequired().HasColumnType("int");
            builder.Property(x => x.OfficeName).HasColumnName("OfficeName").IsRequired().HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.TitleId).HasColumnName("TitleId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.TitleName).HasColumnName("TitleName").IsRequired().HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.CompanyId).HasColumnName("CompanyId").IsRequired(false).IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.CompanyName).HasColumnName("CompanyName").IsRequired().HasColumnType("nvarchar").HasMaxLength(250);
            builder.Property(x => x.Note).HasColumnName("Note").IsRequired().HasColumnType("nvarchar").HasMaxLength(500);
            builder.Property(x => x.IsDelete).HasColumnName("IsDelete").IsRequired().HasColumnType("bit");
            builder.Property(x => x.IsCurrent).HasColumnName("IsCurrent").IsRequired().HasColumnType("bit");
            builder.Property(x => x.UnsignName).HasColumnName("UnsignName").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(250);
            builder.Property(x => x.CreateTime).HasColumnName("CreateTime").IsRequired().HasColumnType("datetime2");
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.ConcurrencyStamp).HasColumnName("ConcurrencyStamp").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);

            builder.ToTable("UserEmploymentHistory").HasKey(x=> x.Id);
        }
    }
}
