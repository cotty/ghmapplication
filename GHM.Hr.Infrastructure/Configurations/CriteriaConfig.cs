﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class CriteriaConfig : IEntityTypeConfiguration<Criteria>
    {
        public void Configure(EntityTypeBuilder<Criteria> builder)
        {
            builder.Property(c => c.Point).HasColumnType("decimal(5,2)");
            builder.HasMany(c => c.Translations)
                .WithOne(ct => ct.Criteria);
            builder.ToTable("Criterias");
        }
    }    
}
