﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class UserHolidayRemainingConfig : IEntityTypeConfiguration<UserHolidayRemaining>
    {
        public void Configure(EntityTypeBuilder<UserHolidayRemaining> builder)
        {
            builder.Property(x => x.Id).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.HolidayRemaining).IsRequired();
            builder.Property(x => x.TotalHolidays).IsRequired();
            builder.Property(x => x.UserId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.Year).IsRequired();
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);

            builder.ToTable("UserHolidayRemaining").HasKey(x => x.Id);
        }
    }
}
