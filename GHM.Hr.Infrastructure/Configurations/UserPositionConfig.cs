﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class UserPositionConfig : IEntityTypeConfiguration<UserPositioncs>
    {
        public void Configure(EntityTypeBuilder<UserPositioncs> builder)
        {
            builder.Property(x => x.UserId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.PositionId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.OfficeId).IsRequired();
            builder.Property(x => x.OfficeIdPath).IsRequired().HasMaxLength(500).IsUnicode(false);
            builder.Property(x => x.UserType).IsRequired();
            builder.Property(x => x.IsManager).IsRequired().HasDefaultValue(false);
            builder.Property(x => x.IsDefault).IsRequired().HasDefaultValue(false);
            builder.Property(x => x.FromDate).IsRequired();
            builder.Property(x => x.ToDate).IsRequired(false);
            builder.ToTable("UsersPositions").HasKey(x => new { x.UserId, x.PositionId });
        }
    }
}
