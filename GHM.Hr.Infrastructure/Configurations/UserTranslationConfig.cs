﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class UserTranslationConfig : IEntityTypeConfiguration<UserTranslation>
    {
        public void Configure(EntityTypeBuilder<UserTranslation> builder)
        {
            builder.Property(x => x.UserId).IsRequired().HasMaxLength(50).IsUnicode();
            builder.Property(x => x.LanguageId).IsRequired().HasMaxLength(10).IsUnicode();
            builder.Property(x => x.IdCardPlaceOfIssue).IsRequired(false).HasMaxLength(100).IsUnicode();
            builder.Property(x => x.NativeCountry).IsRequired(false).HasMaxLength(500).IsUnicode();
            builder.Property(x => x.ResidentRegister).IsRequired(false).HasMaxLength(500).IsUnicode();
            builder.Property(x => x.Address).IsRequired(false).HasMaxLength(500).IsUnicode();
            builder.Property(x => x.Nationality).IsRequired(false).HasMaxLength(256).IsUnicode();
            builder.Property(x => x.OfficeName).IsRequired().HasMaxLength(256).IsUnicode();
            builder.Property(x => x.TitleName).IsRequired().HasMaxLength(256).IsUnicode();
            builder.Property(x => x.PositionName).IsRequired().HasMaxLength(256).IsUnicode();
            builder.Property(x => x.PassportPlaceOfIssue).IsRequired(false).HasMaxLength(200).IsUnicode();
            builder.Property(x => x.BankName).IsRequired(false).HasMaxLength(100).IsUnicode();
            builder.Property(x => x.BranchBank).IsRequired(false).HasMaxLength(100).IsUnicode();

            builder.ToTable("UserTranslations").HasKey(x => new { x.UserId, x.LanguageId});
        }
    }
}
