﻿using GHM.Hr.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Hr.Infrastructure.Configurations
{
    public class UserContactConfig : IEntityTypeConfiguration<UserContact>
    {
        public void Configure(EntityTypeBuilder<UserContact> builder)
        {
            builder.Property(x => x.Id).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.ContactType).IsRequired();
            builder.Property(x => x.ContactValue).IsRequired().HasMaxLength(256);
            builder.Property(x => x.UserId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);

            builder.ToTable("UserContacts").HasKey(x => x.Id);
        }
    }
}


