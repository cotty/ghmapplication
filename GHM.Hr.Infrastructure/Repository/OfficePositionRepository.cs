﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;

namespace GHM.Hr.Infrastructure.Repository
{
    public class OfficePositionRepository : RepositoryBase, IOfficePositionRepository
    {
        private readonly IRepository<OfficePosition> _officePositionRepository;

        public OfficePositionRepository(IDbContext context) : base(context)
        {
            _officePositionRepository = Context.GetRepository<OfficePosition>();
        }

        public async Task<bool> CheckExistsByTitleId(string titleId)
        {
            return await _officePositionRepository.ExistAsync(x => x.TitleId == titleId);
        }

        public async Task<bool> CheckExistsByPositionId(string positionId)
        {
            return await _officePositionRepository.ExistAsync(x => x.PositionId == positionId);
        }

        public async Task<bool> CheckExistsByOfficeId(int officeId)
        {
            return await _officePositionRepository.ExistAsync(x => x.OfficeId == officeId);
        }

        public async Task<bool> CheckExists(string positionId, int officeId)
        {
            return await _officePositionRepository.ExistAsync(x => x.PositionId == positionId && x.OfficeId == officeId);
        }

        public async Task<int> Insert(OfficePosition officePosition)
        {
            _officePositionRepository.Create(officePosition);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<OfficePosition> officePositions)
        {
            _officePositionRepository.Creates(officePositions);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateUserNo(string titleId, int officeId, int totalUser)
        {
            var info = await GetInfo(titleId, officeId);
            if (info == null)
                return -1;

            info.TotalUser = totalUser;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string positionId, int officeId)
        {
            var info = await GetInfoByPositionIdAndOfficeId(positionId, officeId);
            if (info == null)
                return -1;

            _officePositionRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeleteByTitleId(string titleId)
        {
            var officeTitles = await _officePositionRepository.GetsAsync(false, x => x.TitleId == titleId);
            if (!officeTitles.Any())
                return -1;

            _officePositionRepository.Deletes(officeTitles);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeleteByOfficeId(int officeId)
        {
            var officeTitles = await _officePositionRepository.GetsAsync(false, x => x.OfficeId == officeId);
            if (!officeTitles.Any())
                return -1;

            _officePositionRepository.Deletes(officeTitles);
            return await Context.SaveChangesAsync();
        }

        public async Task<OfficePosition> GetInfo(string titleId, int officeId, bool isReadOnly = false)
        {
            return await _officePositionRepository.GetAsync(isReadOnly,
                x => x.TitleId == titleId && x.OfficeId == officeId);
        }

        public async Task<List<OfficePosition>> GetsByPositionId(string positionId)
        {
            return await _officePositionRepository.GetsAsync(false, x => x.PositionId == positionId);
        }

        public async Task<List<OfficePositionDetailViewModel>> GetsOfficeInfoByPositionId(string languageId, string positionId)
        {
            var query = Context.Set<OfficePosition>().Where(x => x.PositionId == positionId)
                .Join(Context.Set<OfficeTranslation>().Where(x => x.LanguageId == languageId), op => op.OfficeId,
                    ot => ot.OfficeId, (op, ot) =>
                        new OfficePositionDetailViewModel
                        {
                            Id = ot.OfficeId,
                            Name = ot.Name
                        });
            return await query.ToListAsync();
        }

        public async Task<OfficePosition> GetInfoByPositionIdAndOfficeId(string positionId, int officeId, bool isReadOnly = false)
        {
            return await _officePositionRepository.GetAsync(isReadOnly,
                x => x.PositionId == positionId && x.OfficeId == officeId);
        }

        public async Task<List<OfficePositionTreeViewModel>> GetAllOfficePositionForRenderTree(string tenantId, string languageId)
        {
            var query = from o in Context.Set<Office>()
                        join ot in Context.Set<OfficeTranslation>() on o.Id equals ot.OfficeId
                        join op in Context.Set<OfficePosition>() on o.Id equals op.OfficeId into groupOfficePosition
                        from op in groupOfficePosition.DefaultIfEmpty()
                        join pt in Context.Set<PositionTranslation>() on new { op.PositionId, LanguageId = languageId } equals new { pt.PositionId, pt.LanguageId }
                            into groupOfficePositionTranslation
                        from pt in groupOfficePositionTranslation.DefaultIfEmpty()
                        where o.TenantId == tenantId && ot.LanguageId == languageId && !o.IsDelete
                        select new OfficePositionTreeViewModel
                        {
                            OfficeId = o.Id,
                            OfficeName = ot.Name,
                            PositionId = op.PositionId,
                            OfficeIdPath = o.IdPath,
                            ParentOfficeId = o.ParentId,
                            PositionName = pt.Name
                        };
            return await query.AsNoTracking().ToListAsync();
        }

        public Task<List<OfficePositionSearchViewModel>> SearchByPositionId(string tenantId, string languageId, string positionId,
            int page, int pageSize, out int totalRows)
        {
            //var query = Context.Set<Office>().Where(x => x.TenantId == tenantId)
            //    .Join(Context.Set<OfficeTranslation>().Where(x => x.LanguageId == languageId),
            //        office => office.Id, officeTranslation => officeTranslation.OfficeId,
            //        (office, officeTranslation) => new
            //        {
            //            OfficeId = office.Id,
            //            OfficeName = officeTranslation.Name,
            //            office.ParentId,
            //            OfficeIdPath = office.IdPath
            //        })
            //    .Join(Context.Set<OfficePosition>(),
            //        office => office.OfficeId, officeTitle => officeTitle.OfficeId,
            //        (office, officeTitle) => new
            //        {
            //            office.OfficeId,
            //            office.OfficeName,
            //            office.ParentId,
            //            officeTitle.PositionId,
            //            office.OfficeIdPath,
            //            officeTitle.TotalUser
            //        })
            //    .Join(Context.Set<Title>().Where(x => x.Id == titleId && x.TenantId == tenantId), officeTitle => officeTitle.PositionId, title => title.Id,
            //        (officeTitle, title) => new
            //        {
            //            officeTitle.OfficeId,
            //            officeTitle.OfficeName,
            //            officeTitle.OfficeIdPath,
            //            officeTitle.ParentId,
            //            officeTitle.PositionId,
            //            officeTitle.TotalUser,
            //            title.IsManager,
            //            title.IsMultiple
            //        })
            //    .Join(Context.Set<TitleTranslation>().Where(x => x.LanguageId == languageId),
            //        title => title.PositionId, titleTranslation => titleTranslation.PositionId,
            //        (officeTitle, titleTranslation) => new OfficeTitleSearchViewModel
            //        {
            //            PositionId = officeTitle.PositionId,
            //            TitleName = titleTranslation.Name,
            //            OfficeId = officeTitle.OfficeId,
            //            OfficeName = officeTitle.OfficeName,
            //            TotalUsers = officeTitle.TotalUser,
            //            IsManager = officeTitle.IsManager,
            //            IsMultiple = officeTitle.IsMultiple
            //        });

            //totalRows = query.Count();
            //return query.OrderBy(x => x.TitleName)
            //    .Skip((page - 1) * pageSize)
            //    .Take(pageSize)
            //    .ToListAsync();

            throw new NotImplementedException();
        }

        public Task<List<OfficePositionSearchViewModel>> SearchByOfficeId(string tenantId, string languageId,
            string keyword, int officeId, int page, int pageSize, out int totalRows)
        {
            Expression<Func<PositionTranslation, bool>> specTranslation = x => x.LanguageId == languageId;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            var query = Context.Set<Office>().Where(x => x.TenantId == tenantId && x.Id == officeId)
                .Join(Context.Set<OfficeTranslation>().Where(x => x.LanguageId == languageId),
                    office => office.Id, officeTranslation => officeTranslation.OfficeId,
                    (office, officeTranslation) => new
                    {
                        OfficeId = office.Id,
                        OfficeName = officeTranslation.Name,
                        office.ParentId,
                        OfficeIdPath = office.IdPath
                    })
                .Join(Context.Set<OfficePosition>(),
                    office => office.OfficeId, officePosition => officePosition.OfficeId,
                    (office, officePosition) => new
                    {
                        office.OfficeId,
                        office.OfficeName,
                        office.ParentId,
                        officePosition.PositionId,
                        office.OfficeIdPath,
                        officePosition.TotalUser
                    })
                .Join(Context.Set<Position>().Where(x => x.TenantId == tenantId), officePosition => officePosition.PositionId, position => position.Id,
                    (officePosition, position) => new
                    {
                        officePosition.OfficeId,
                        officePosition.OfficeName,
                        officePosition.OfficeIdPath,
                        officePosition.ParentId,
                        officePosition.PositionId,
                        officePosition.TotalUser,
                        position.IsManager,
                        position.IsMultiple
                    })
                .Join(Context.Set<PositionTranslation>().Where(specTranslation),
                    postion => postion.PositionId, positionTranslation => positionTranslation.PositionId,
                    (officePosition, positionTranslation) => new OfficePositionSearchViewModel
                    {
                        PositionId = officePosition.PositionId,
                        PositionName = positionTranslation.Name,
                        OfficeId = officePosition.OfficeId,
                        OfficeName = officePosition.OfficeName,
                        TotalUsers = officePosition.TotalUser,
                        IsManager = officePosition.IsManager,
                        IsMultiple = officePosition.IsMultiple
                    });

            totalRows = query.Count();
            return query.OrderBy(x => x.PositionName).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
        }
    }
}
