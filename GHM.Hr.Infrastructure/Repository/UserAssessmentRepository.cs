﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;

namespace GHM.Hr.Infrastructure.Repository
{
    public class UserAssessmentRepository : RepositoryBase, IUserAssessmentRepository
    {
        private readonly IRepository<UserAssessment> _userAssessmentRepository;
        public UserAssessmentRepository(IDbContext context) : base(context)
        {
            _userAssessmentRepository = Context.GetRepository<UserAssessment>();
        }


        public async Task<bool> CheckExists(string tenantId, string userId, string positionId, byte month, int year)
        {
            return await _userAssessmentRepository.ExistAsync(x =>
                x.TenantId == tenantId && x.UserId == userId && x.PositionId == positionId
                && x.Month == month && x.Year == year && x.Status != AssessmentStatus.Cancel);
        }

        public async Task<int> Insert(UserAssessment userAssessment)
        {
            _userAssessmentRepository.Create(userAssessment);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateTotalPoint(UserAssessment userAssessment)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateTotalUserPoint(UserAssessment userAssessment)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateTotalManagerPoint(UserAssessment userAssessment)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateTotalApproverPoint(UserAssessment userAssessment)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateStatus(UserAssessment userAssessment)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<UserAssessment> GetInfo(string tenantId, string id, bool isReadonly = false)
        {
            return await _userAssessmentRepository.GetAsync(isReadonly,
                x => x.TenantId == tenantId && x.Id == id && x.Status != AssessmentStatus.Cancel);
        }

        public async Task<UserAssessment> GetInfo(string tenantId, string userId, byte month, int year, bool isReadonly = false)
        {
            return await _userAssessmentRepository.GetAsync(isReadonly,
                x => x.TenantId == tenantId && x.UserId == userId && x.Month == month && x.Year == year && x.Status != AssessmentStatus.Cancel);
        }

        public async Task<List<UserAssessmentViewModel>> UserSearchAssessment(string tenantId, string languageId, string userId,
            int year)
        {
            var query = from ua in Context.Set<UserAssessment>()
                        join ot in Context.Set<OfficeTranslation>() on ua.OfficeId equals ot.OfficeId
                        join pt in Context.Set<PositionTranslation>() on ua.PositionId equals pt.PositionId
                        join u in Context.Set<User>() on ua.UserId equals u.Id
                        where ua.TenantId == tenantId && ua.UserId == userId && ua.Year == year
                              && ot.LanguageId == languageId && pt.LanguageId == languageId
                        select new UserAssessmentViewModel
                        {
                            Id = ua.Id,
                            UserId = ua.UserId,
                            Month = ua.Month,
                            Year = ua.Year,
                            OfficeName = ot.Name,
                            PositionName = pt.Name,
                            FullName = u.FullName,
                            TotalPoint = ua.TotalPoint,
                            TotalUserPoint = ua.TotalUserPoint,
                            TotalManagerPoint = ua.TotalManagerPoint,
                            TotalApproverPoint = ua.TotalApproverPoint,
                            ManagerUserId = ua.ManagerUserId,
                            ApproverUserId = ua.ApproverUserId,
                            Status = ua.Status
                        };
            return await query.OrderByDescending(x=> x.Month).ToListAsync();
        }

        public Task<List<UserAssessmentViewModel>> ManagerSearchForApprove(string tenantId, string languageId, string userId, string keyword,
            byte? month, int? year, int page, int pageSize, out int totalRows)
        {
            var query = from ua in Context.Set<UserAssessment>()
                        join ot in Context.Set<OfficeTranslation>() on ua.OfficeId equals ot.OfficeId
                        join pt in Context.Set<PositionTranslation>() on ua.PositionId equals pt.PositionId
                        join u in Context.Set<User>() on ua.UserId equals u.Id
                        where ua.TenantId == tenantId
                              && ua.ManagerUserId == userId
                              && ot.LanguageId == languageId
                              && pt.LanguageId == languageId && pt.TenantId == tenantId
                              && (ua.Status != AssessmentStatus.New
                                  || ua.Status == AssessmentStatus.Cancel)                             
                              && (!month.HasValue || ua.Month == month.Value)
                              && (!year.HasValue || ua.Year == year.Value)
                              && u.TenantId == tenantId && !u.IsDelete && u.Status < UserStatus.Maternity
                        select new UserAssessmentViewModel
                        {
                            Id = ua.Id,
                            UserId = ua.UserId,
                            Month = ua.Month,
                            Year = ua.Year,
                            OfficeName = ot.Name,
                            PositionName = pt.Name,
                            FullName = u.FullName,
                            TotalPoint = ua.TotalPoint,
                            TotalUserPoint = ua.TotalUserPoint,
                            TotalManagerPoint = ua.TotalManagerPoint,
                            TotalApproverPoint = ua.TotalApproverPoint,
                            ManagerUserId = ua.ManagerUserId,
                            ApproverUserId = ua.ApproverUserId,
                            Status = ua.Status
                        };

            totalRows = query.Count();
            return query
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        public Task<List<UserAssessmentViewModel>> ApproverSearchForApprove(string tenantId, string languageId, string userId,
            string keyword, byte? month, int? year, int page, int pageSize, out int totalRows)
        {
            var query = from ua in Context.Set<UserAssessment>()
                        join ot in Context.Set<OfficeTranslation>() on ua.OfficeId equals ot.OfficeId
                        join pt in Context.Set<PositionTranslation>() on ua.PositionId equals pt.PositionId
                        join u in Context.Set<User>() on ua.UserId equals u.Id
                        where ua.TenantId == tenantId
                              && ot.LanguageId == languageId && pt.LanguageId == languageId
                              && (ua.Status == AssessmentStatus.ManagerApproveWaitingApproverApprove
                                  || ua.Status == AssessmentStatus.ApproverApprove
                                  || ua.Status == AssessmentStatus.ApproverDecline)
                              && ua.ApproverUserId == userId
                              && (!month.HasValue || ua.Month == month.Value)
                              && (!year.HasValue || ua.Year == year.Value)
                        select new UserAssessmentViewModel
                        {
                            Id = ua.Id,
                            UserId = ua.UserId,
                            Month = ua.Month,
                            Year = ua.Year,
                            OfficeName = ot.Name,
                            PositionName = pt.Name,
                            FullName = u.FullName,
                            TotalPoint = ua.TotalPoint,
                            TotalUserPoint = ua.TotalUserPoint,
                            TotalManagerPoint = ua.TotalManagerPoint,
                            TotalApproverPoint = ua.TotalApproverPoint,
                            ManagerUserId = ua.ManagerUserId,
                            ApproverUserId = ua.ApproverUserId,
                            Status = ua.Status
                        };

            totalRows = query.Count();
            return query
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        public Task<List<UserAssessmentViewModel>> SearchResult(string tenantId, string languageId, string userId, string keyword,
            AssessmentStatus? status, int? officeId, byte? month, int year, int page, int pageSize, out int totalRows)
        {
            var query = from ua in Context.Set<UserAssessment>()
                        join ot in Context.Set<OfficeTranslation>() on ua.OfficeId equals ot.OfficeId
                        join pt in Context.Set<PositionTranslation>() on ua.PositionId equals pt.PositionId
                        join u in Context.Set<User>() on ua.UserId equals u.Id
                        where ua.TenantId == tenantId
                              && ua.Year == year && (!month.HasValue || ua.Month == month)
                              && (!officeId.HasValue || ua.OfficeId == officeId.Value)
                              && (!status.HasValue || ua.Status == status)
                              && ot.LanguageId == languageId && pt.LanguageId == languageId
                              && (string.IsNullOrEmpty(keyword) || u.UnsignName.Contains(keyword))
                        select new UserAssessmentViewModel
                        {
                            Id = ua.Id,
                            UserId = ua.UserId,
                            Month = ua.Month,
                            Year = ua.Year,
                            OfficeName = ot.Name,
                            PositionName = pt.Name,
                            FullName = u.FullName,
                            TotalPoint = ua.TotalPoint,
                            TotalUserPoint = ua.TotalUserPoint,
                            TotalManagerPoint = ua.TotalManagerPoint,
                            TotalApproverPoint = ua.TotalApproverPoint,
                            ManagerUserId = ua.ManagerUserId,
                            ApproverUserId = ua.ApproverUserId,
                            Status = ua.Status
                        };
            totalRows = query.Count();
            return query
                .OrderByDescending(x=> x.Month)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        public async Task<List<UserAssessmentViewModel>> GetByYear(string tenantId, string languageId, string userId, int year)
        {
            var query = from ua in Context.Set<UserAssessment>()
                        join ot in Context.Set<OfficeTranslation>() on ua.OfficeId equals ot.OfficeId
                        join pt in Context.Set<PositionTranslation>() on ua.PositionId equals pt.PositionId
                        join u in Context.Set<User>() on ua.UserId equals u.Id
                        where ua.TenantId == tenantId && ua.UserId == userId && ua.Year == year
                              && ot.LanguageId == languageId && pt.LanguageId == languageId
                        select new UserAssessmentViewModel
                        {
                            Id = ua.Id,
                            UserId = ua.UserId,
                            Month = ua.Month,
                            Year = ua.Year,
                            OfficeName = ot.Name,
                            PositionName = pt.Name,
                            FullName = u.FullName,
                            TotalPoint = ua.TotalPoint,
                            TotalUserPoint = ua.TotalUserPoint,
                            TotalManagerPoint = ua.TotalManagerPoint,
                            TotalApproverPoint = ua.TotalApproverPoint,
                            ManagerUserId = ua.ManagerUserId,
                            ApproverUserId = ua.ApproverUserId,
                            Status = ua.Status
                        };

            return await query.ToListAsync();
        }

        public Task<List<UserAssessmentViewModel>> SearchResult(string tenantId, string userId, string keyword,
            AssessmentStatus? status, string officeIds,
            byte month, int year, int page, int pageSize, out int totalRows)
        {
            throw new NotImplementedException();
        }

        public async Task<UserAssessmentViewModel> GetDetail(string tenantId, string languageId, string id)
        {
            var query =
                from ua in Context.Set<UserAssessment>()
                join u in Context.Set<User>() on ua.UserId equals u.Id
                join pt in Context.Set<PositionTranslation>() on ua.PositionId equals pt.PositionId
                join ot in Context.Set<OfficeTranslation>() on ua.OfficeId equals ot.OfficeId
                where ua.TenantId == tenantId && ua.Id == id
                      && ot.LanguageId == languageId && pt.LanguageId == languageId
                select new UserAssessmentViewModel
                {
                    Id = ua.Id,
                    Month = ua.Month,
                    Year = ua.Year,
                    Quartar = ua.Quarter,
                    UserId = u.Id,
                    FullName = u.FullName,
                    OfficeName = ot.Name,
                    Status = ua.Status,
                    TotalPoint = ua.TotalPoint,
                    PositionName = pt.Name,
                    TotalUserPoint = ua.TotalUserPoint,
                    TotalApproverPoint = ua.TotalApproverPoint,
                    TotalManagerPoint = ua.TotalManagerPoint,
                    ManagerUserId = ua.ManagerUserId,
                    ApproverUserId = ua.ApproverUserId,
                };
            return await query.FirstOrDefaultAsync();
        }

        public async Task<UserAssessmentViewModel> GetDetail(string tenantId, string languageId, string id, string userId)
        {
            var query =
                from ua in Context.Set<UserAssessment>()
                join u in Context.Set<User>() on ua.UserId equals u.Id
                join pt in Context.Set<PositionTranslation>() on ua.PositionId equals pt.PositionId
                join ot in Context.Set<OfficeTranslation>() on ua.OfficeId equals ot.OfficeId
                where ua.TenantId == tenantId && ua.UserId == userId && ua.Id == id
                      && ot.LanguageId == languageId && pt.LanguageId == languageId
                select new UserAssessmentViewModel
                {
                    Id = ua.Id,
                    Month = ua.Month,
                    Year = ua.Year,
                    Quartar = ua.Quarter,
                    UserId = u.Id,
                    FullName = u.FullName,
                    OfficeName = ot.Name,
                    Status = ua.Status,
                    TotalPoint = ua.TotalPoint,
                    PositionName = pt.Name,
                    TotalUserPoint = ua.TotalUserPoint,
                    TotalApproverPoint = ua.TotalApproverPoint,
                    TotalManagerPoint = ua.TotalManagerPoint,
                    ManagerUserId = ua.ManagerUserId,
                    ApproverUserId = ua.ApproverUserId,
                };
            return await query.FirstOrDefaultAsync();
        }

        public async Task<UserAssessmentViewModel> GetDetail(string tenantId, string languageId, string userId, byte month, int year)
        {
            var query =
                from ua in Context.Set<UserAssessment>()
                join u in Context.Set<User>() on ua.UserId equals u.Id
                join pt in Context.Set<PositionTranslation>() on ua.PositionId equals pt.PositionId
                join ot in Context.Set<OfficeTranslation>() on ua.OfficeId equals ot.OfficeId
                where ua.TenantId == tenantId && ua.UserId == userId && ua.Month == month && ua.Year == year
                      && ot.LanguageId == languageId && pt.LanguageId == languageId
                select new UserAssessmentViewModel
                {
                    Id = ua.Id,
                    Month = ua.Month,
                    Year = ua.Year,
                    Quartar = ua.Quarter,
                    UserId = u.Id,
                    FullName = u.FullName,
                    OfficeName = ot.Name,
                    Status = ua.Status,
                    TotalPoint = ua.TotalPoint,
                    PositionName = pt.Name,
                    TotalUserPoint = ua.TotalUserPoint,
                    TotalApproverPoint = ua.TotalApproverPoint,
                    TotalManagerPoint = ua.TotalManagerPoint,
                    ManagerUserId = ua.ManagerUserId,
                    ApproverUserId = ua.ApproverUserId
                };
            return await query.FirstOrDefaultAsync();
        }

        public async Task<UserAssessment> GetUnFinishedAssessment(string tenantId, string userId, bool isReadOnly = false)
        {
            return await _userAssessmentRepository.GetAsync(isReadOnly, x =>
                x.TenantId == tenantId && x.UserId == userId && x.Status != AssessmentStatus.ApproverApprove
                                           && x.Status != AssessmentStatus.ManagerApproved);
        }

        public async Task<int> UpdateManager(UserAssessment userAssessment)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<string> GetUserAssessmentId(string tenantId, string userId, byte month, int year)
        {
            return await _userAssessmentRepository.GetAsAsync(x => x.Id,
                x => x.TenantId == tenantId && x.UserId == userId && x.Month == month && x.Year == year &&
                     x.Status != AssessmentStatus.Cancel);
        }

        public Task<List<UserAssessment>> SearchForReassessment(string tenantId, string positionId, byte month, int year, bool applyForAll,
             int page, int pageSize, out int totalRows)
        {
            Expression<Func<UserAssessment, bool>> spec = x =>
                x.TenantId == tenantId && x.PositionId == positionId && x.Month == month && x.Year == year;
            if (!applyForAll)
                spec = spec.And(x =>
                    x.Status != AssessmentStatus.ApproverApprove && x.Status != AssessmentStatus.ManagerApproved);

            var sort = Context.Filters.Sort<UserAssessment, string>(x => x.Id, true);
            var paging = Context.Filters.Page<UserAssessment>(page, pageSize);
            totalRows = _userAssessmentRepository.Count(spec);
            return _userAssessmentRepository.GetsAsync(false, spec, sort, paging);
        }
    }
}
