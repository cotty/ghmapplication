﻿using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Hr.Infrastructure.Repository
{
    public class RelatedUserRepository : RepositoryBase, IRelatedUserRepository
    {
        private IRepository<RelatedUser> _relatedUserRepository;
        public RelatedUserRepository(IDbContext context) : base(context)
        {
            _relatedUserRepository = Context.GetRepository<RelatedUser>();
        }
        public async Task<bool> CheckNameExist(string tenantId, string userId, string fullName)
        {
            return await _relatedUserRepository.ExistAsync(x => x.TenantId == tenantId && x.UserId == userId && x.FullName == fullName && !x.IsDelete);
        }

        public async Task<RelatedUser> GetInfo(string tenantId, string id, bool isReadOnly = false)
        {
            return await _relatedUserRepository.GetAsync(isReadOnly, x => x.TenantId == tenantId && x.Id == id && !x.IsDelete);
        }

        public async Task<int> Insert(RelatedUser relatedUser)
        {
            _relatedUserRepository.Create(relatedUser);

            return await Context.SaveChangesAsync();
        }

        public async Task<int> SaveAsync(RelatedUser info)
        {
            return await Context.SaveChangesAsync();
        }

        public Task<List<RelatedUserViewModel>> Search(string tenantId, string keyword, string userId, string valueId, 
            string fullName, string idCardNumber, string phone, int page, int pageSize, out int totalRows)
        {
            Expression<Func<RelatedUser, bool>> spec = x => x.TenantId == tenantId && !x.IsDelete;

            if (!string.IsNullOrEmpty(keyword))
                spec = spec.And(x => x.Id == keyword);

            if (!string.IsNullOrEmpty(userId))
                spec = spec.And(x => x.UserId == userId);

            if (!string.IsNullOrEmpty(valueId))
                spec = spec.And(x => x.ValueId == valueId);

            if (!string.IsNullOrEmpty(fullName))
                spec = spec.And(x => x.FullName == fullName);

            if (!string.IsNullOrEmpty(idCardNumber))
                spec = spec.And(x => x.IdCardNumber == idCardNumber);

            var query = Context.Set<RelatedUser>().Where(spec).Select(x => new RelatedUserViewModel
            {
                Id = x.Id,
                BirthDay = x.BirthDay,
                ConcurrencyStamp = x.ConcurrencyStamp,
                ContactAddress = x.ContactAddress,
                DenominationName = x.DenominationName,
                DistrictId = x.DistrictId,
                Ethnic = x.Ethnic,
                FullName = x.FullName,
                IdCardDateOfIssue = x.IdCardDateOfIssue,
                IdCardNumber = x.IdCardNumber,
                IdCardPlaceOfIssue = x.IdCardPlaceOfIssue,
                Job = x.Job,
                NationalId = x.NationalId,
                PermanentAddress = x.PermanentAddress,
                Phone = x.Phone,
                ProvinceId = x.ProvinceId,
                UserId = x.UserId,
                Gender = x.Gender,
                ValueId = x.ValueId,
                ValueName = x.ValueName, 
            });

            totalRows = query.Count();
            return query.OrderByDescending(x => x.Id).Skip((page - 1) * pageSize).Take(pageSize).AsNoTracking().ToListAsync();
        }
    }
}
