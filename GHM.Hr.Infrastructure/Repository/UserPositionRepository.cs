﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Infrastructure.SqlServer;

namespace GHM.Hr.Infrastructure.Repository
{
    public class UserPositionRepository : RepositoryBase, IUserPositionRepository
    {
        private readonly IRepository<UserPositioncs> _userPositionRepository;
        public UserPositionRepository(IDbContext context) : base(context)
        {
            _userPositionRepository = Context.GetRepository<UserPositioncs>();
        }

        public async Task<int> Insert(UserPositioncs userPosition)
        {
            _userPositionRepository.Create(userPosition);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(UserPositioncs userPosition)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(UserPositioncs userPosition, int oldTitleId, int oldOfficeId)
        {
            //            var info = await GetInfo(userTitle.UserId, oldTitleId, oldOfficeId);
            //            if (info == null)
            //                return -1;
            //
            //            info.UserType = userTitle.UserType;
            //            info.PositionId = userTitle.PositionId;
            //            info.TitleName = userTitle.TitleName;
            //            info.TitleShortName = userTitle.TitleShortName;
            //            info.OfficeId = userTitle.OfficeId;
            //            info.OfficeName = userTitle.OfficeName;
            //            info.OfficeIdPath = userTitle.OfficeIdPath;
            //            info.OfficeShortName = userTitle.OfficeShortName;

            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateDefaultState(string userId, bool isDefault)
        {
            var listUserPositionByUserId = _userPositionRepository.Gets(false, c => c.UserId == userId);
            if (listUserPositionByUserId != null && listUserPositionByUserId.Any())
            {
                foreach (var item in listUserPositionByUserId)
                {
                    item.IsDefault = isDefault;
                }

                return await Context.SaveChangesAsync();
            }

            return 1;
        }

        public async Task<int> UpdateOfficeIdPath(string oldOfficeIdPath, string officeIdPath)
        {
            var listOfficePosition = _userPositionRepository.Gets(false, x => x.OfficeIdPath == oldOfficeIdPath || x.OfficeIdPath.StartsWith(oldOfficeIdPath + "."));
            if (listOfficePosition == null || !listOfficePosition.Any())
                return -1;

            foreach (var item in listOfficePosition)
            {
                item.OfficeIdPath = item.OfficeIdPath == oldOfficeIdPath
                    ? item.OfficeIdPath.Replace(oldOfficeIdPath, officeIdPath)
                    : item.OfficeIdPath.Replace(oldOfficeIdPath + ".", officeIdPath + ".");
            }
            return await Context.SaveChangesAsync();
        }

        //public async Task<int> UpdateOfficeIdPathByListOfficeId(List<string> listOfficeId)
        //{
        //    //var listJoin = Context.Set<Office>().Where(x => listOfficeId.Contains(x.Id) && !x.IsDelete && x.IsActive)
        //    //               .Join(Context.Set<UserTitle>(), o => o.Id, ut => ut.OfficeId, (o, ut) => new { o, ut });

        //    //foreach (var userOffice in listJoin)
        //    //{
        //    //    userOffice.ut.OfficeIdPath = userOffice.o.IdPath;
        //    //    //                userOffice.ut.OfficeName = userOffice.o.Name;
        //    //}

        //    return await Context.SaveChangesAsync();
        //}

        public async Task<int> Delete(string userId, string positionId, int officeId)
        {
            var info = await GetInfo(userId, positionId, officeId);
            if (info == null)
                return -1;

            _userPositionRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeleteByUserId(string tenantId, string userId)
        {
            var listUserPositions = await _userPositionRepository.GetsAsync(false, x => x.UserId == userId);
            if (!listUserPositions.Any())
                return -1;

            _userPositionRepository.Deletes(listUserPositions);
            return Context.SaveChanges();
        }

        public async Task<bool> CheckExistsByPositionId(string positionId)
        {
            return await _userPositionRepository.ExistAsync(x => x.PositionId == positionId);
        }

        public async Task<bool> CheckExistsByOfficeId(int officeId)
        {
            return await _userPositionRepository.ExistAsync(x => x.OfficeId == officeId);
        }

        public async Task<bool> CheckExistsByOfficeIdAndPositionId(int officeId, string positionId)
        {
            return await _userPositionRepository.ExistAsync(x => x.OfficeId == officeId && x.PositionId == positionId);
        }

        public async Task<bool> CheckExists(string userId, string positionId, int officeId, bool? isDefault = false)
        {
            return await _userPositionRepository.ExistAsync(x => x.UserId.Equals(userId) && x.PositionId == positionId && x.OfficeId == officeId && x.IsDefault == isDefault);
        }

        public async Task<UserPositioncs> GetInfo(string userId, string positionId, int officeId, bool isReadonly = false)
        {
            return await _userPositionRepository.GetAsync(isReadonly, x => x.UserId.Equals(userId) && x.PositionId == positionId && x.OfficeId == officeId);
        }

        public async Task<bool> CheckExitsByPositionId(string positionId)
        {
            return await _userPositionRepository.ExistAsync(x => x.PositionId == positionId && x.IsDefault);
        }

        public async Task<UserPositioncs> GetDefaultInfo(string userId, bool isDefault, bool isReadonly = false)
        {
            return await _userPositionRepository.GetAsync(isReadonly, x => x.UserId.Equals(userId) && x.IsDefault);
        }

        public async Task<UserPositioncs> GetDefaultInfo(string userId, string positionId, int officeId, bool isDefault, bool isReadonly = false)
        {
            return await _userPositionRepository.GetAsync(isReadonly, x => x.UserId.Equals(userId) && x.PositionId == positionId && x.OfficeId == officeId && x.IsDefault);
        }

    }
}
