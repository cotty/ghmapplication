﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.SqlServer;

namespace GHM.Hr.Infrastructure.Repository
{
    public class PositionTranslationRepository : RepositoryBase, IPositionTranslationRepository
    {
        private readonly IRepository<PositionTranslation> _positionTranslationRepository;
        public PositionTranslationRepository(IDbContext context) : base(context)
        {
            _positionTranslationRepository = Context.GetRepository<PositionTranslation>();
        }

        public async Task<int> Update(PositionTranslation positionTranslation)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Save(PositionTranslation positionTranslation)
        {
            var info = await GetInfo(positionTranslation.PositionId, positionTranslation.LanguageId);
            if (info == null)
            {
                _positionTranslationRepository.Create(positionTranslation);
                return await Context.SaveChangesAsync();
            }

            info.Name = positionTranslation.Name;
            info.Description = positionTranslation.Description;
            info.ShortName = positionTranslation.ShortName;
            info.UnsignName = positionTranslation.Name.StripVietnameseChars().ToUpper();
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Insert(PositionTranslation positionTranslation)
        {
            _positionTranslationRepository.Create(positionTranslation);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<PositionTranslation> positionTranslations)
        {
            _positionTranslationRepository.Creates(positionTranslations);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string tenantId, string positionId, string languageId)
        {
            var info = await GetInfo(tenantId, positionId, languageId);
            if (info == null)
                return -1;

            _positionTranslationRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeleteByPositionId(string tenantId, string positionId)
        {
            var positionTranslations = await _positionTranslationRepository.GetsAsync(false, x => x.PositionId == positionId && x.TenantId == tenantId);
            if (!positionTranslations.Any())
                return -1;

            _positionTranslationRepository.Deletes(positionTranslations);
            return await Context.SaveChangesAsync();
        }

        public async Task<PositionTranslation> GetInfo(string positionId, string languageId, bool isReadOnly = false)
        {
            return await _positionTranslationRepository.GetAsync(isReadOnly,
                x => x.PositionId == positionId && x.LanguageId == languageId);
        }

        public async Task<PositionTranslation> GetInfo(string tenantId, string positionId, string languageId, bool isReadOnly = false)
        {
            return await _positionTranslationRepository.GetAsync(isReadOnly,
                x => x.TenantId == tenantId && x.PositionId == positionId && x.LanguageId == languageId);
        }

        public async Task<List<PositionTranslationViewModel>> GetsByPositionId(string tenantId, string positionId)
        {
            return await _positionTranslationRepository.GetsAsAsync(x => new PositionTranslationViewModel
            {
                PositionId = x.PositionId,
                LanguageId = x.LanguageId,
                Description = x.Description,
                Name = x.Name,
                ShortName = x.ShortName,
                Responsibility = x.Responsibility,
                Purpose = x.Purpose,
                OtherRequire = x.OtherRequire
            }, x => x.PositionId == positionId && x.TenantId == tenantId);
        }

        public async Task<bool> CheckExists(string positionId, string tenantId, string languageId, string name)
        {
            name = name.Trim();
            return await _positionTranslationRepository.ExistAsync(x => x.TenantId == tenantId &&
                x.PositionId != positionId && x.LanguageId == languageId && x.Name == name);
        }

        public async Task<bool> CheckShortNameExists(string positionId, string tenantId, string languageId, string shortName)
        {
            shortName = shortName.Trim();
            return await _positionTranslationRepository.ExistAsync(x => x.TenantId == tenantId &&
                x.PositionId != positionId && x.LanguageId == languageId && x.ShortName == shortName);
        }
    }
}
