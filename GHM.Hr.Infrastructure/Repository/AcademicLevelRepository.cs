﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;

namespace GHM.Hr.Infrastructure.Repository
{
    public class AcademicLevelRepository : RepositoryBase, IAcademicLevelRepository
    {
        private readonly IRepository<AcademicLevel> _academicLevelRepository;
        private readonly IUserValueRepository _userValueRepository;

        public AcademicLevelRepository(IDbContext context,
            IUserValueRepository userValueRepository) : base(context)
        {
            _userValueRepository = userValueRepository;
            _academicLevelRepository = Context.GetRepository<AcademicLevel>();
        }

        public async Task<int> Insert(AcademicLevel academicLevel)
        {
            _academicLevelRepository.Create(academicLevel);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(AcademicLevel academicLevel)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string tenantId, string id)
        {
            var info = await GetInfo(tenantId, id);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<AcademicLevel> GetInfo(string tenantId, string id, bool isReadOnly = false)
        {
            return await _academicLevelRepository.GetAsync(isReadOnly, x => x.Id == id && x.TenantId == tenantId && !x.IsDelete);
        }

        public Task<List<AcademicLevelViewModel>> Search(string tenantId, string userId, string levelId, string degreeId, string schoolId,
            string specializeId, int page, int pageSize,
            out int totalRows)
        {
            Expression<Func<AcademicLevel, bool>> spec = x => !x.IsDelete && x.UserId.Equals(userId) && x.TenantId == tenantId;

            if (!string.IsNullOrEmpty(userId))
            {
                spec = spec.And(x => x.UserId == userId);
            }

            if (!string.IsNullOrEmpty(levelId))
            {
                spec = spec.And(x => x.SpecializeId == levelId);
            }

            if (!string.IsNullOrEmpty(degreeId))
            {
                spec = spec.And(x => x.DegreeId == degreeId);
            }

            if (!string.IsNullOrEmpty(schoolId))
            {
                spec = spec.And(x => x.SchoolId == schoolId);
            }

            if (!string.IsNullOrEmpty(specializeId))
            {
                spec = spec.And(x => x.SpecializeId == specializeId);
            }

            var result = from academicLevel in Context.Set<AcademicLevel>().Where(spec).AsNoTracking()
                         select new AcademicLevelViewModel
                         {
                             Id = academicLevel.Id,
                             AcademicLevelId = academicLevel.AcademicLevelId,
                             AcademicLevelName = academicLevel.AcademicLevelName,
                             DegreeId = academicLevel.DegreeId,
                             DegreeName = academicLevel.DegreeName,
                             SchoolId = academicLevel.SchoolId,
                             SchoolName = academicLevel.SchoolName,
                             SpecializeId = academicLevel.SpecializeId,
                             SpecializeName = academicLevel.SpecializeName,
                             UserId = academicLevel.UserId,
                             Note = academicLevel.Note,
                             ConcurrencyStamp = academicLevel.ConcurrencyStamp
                         };

            totalRows = result.Count();
            return result.OrderBy(x => x.Id).Skip((page - 1) * pageSize).Take(pageSize).AsNoTracking().ToListAsync();
        }

        public async Task<bool> CheckIsExists(string tenantId, string userId, string levelId, string degreeId,
            string schoolId, string specializeId)
        {
            return await _academicLevelRepository.ExistAsync(x => x.TenantId == tenantId && x.UserId == userId && x.AcademicLevelId == levelId
                        && x.DegreeId == degreeId && x.SchoolId == schoolId && x.SpecializeId == specializeId);
        }
    }
}
