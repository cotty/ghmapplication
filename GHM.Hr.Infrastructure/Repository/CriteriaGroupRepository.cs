﻿using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.Models;

namespace GHM.Hr.Infrastructure.Repository
{
    public class CriteriaGroupRepository : RepositoryBase, ICriteriaGroupRepository
    {
        private readonly IRepository<CriteriaGroup> _criteriaGrouprRepository;
        public CriteriaGroupRepository(IDbContext context) : base(context)
        {
            _criteriaGrouprRepository = Context.GetRepository<CriteriaGroup>();
        }

        public Task<List<CriteriaGroupViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive, int page,
            int pageSize, out int totalRows)
        {
            Expression<Func<CriteriaGroup, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId;
            Expression<Func<CriteriaGroupTranslation, bool>> specTranslation = x => x.LanguageId == languageId && !x.IsDelete;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            if (isActive.HasValue)
            {
                spec = spec.And(x => x.IsActive == isActive);
            }

            var query = Context.Set<CriteriaGroup>().AsNoTracking().Where(spec)
                .Join(Context.Set<CriteriaGroupTranslation>().AsNoTracking()
                    .Where(specTranslation), c => c.Id, ct => ct.CriteriaGroupId, (c, ct) => new { c, ct })
                .Select(x => new CriteriaGroupViewModel
                {
                    Id = x.c.Id,
                    Name = x.ct.Name,
                    Description = x.ct.Description,
                    IsActive = x.c.IsActive,
                    Order = x.c.Order                    
                });

            totalRows = query.Count();
            return query.OrderBy(c => c.Name)
                .Skip(pageSize * (page - 1))
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<int> Insert(CriteriaGroup criteriaGroup)
        {
            _criteriaGrouprRepository.Create(criteriaGroup);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(CriteriaGroup criteriaGroup)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<bool> CheckIdExists(string tenantId, string id)
        {
            return await _criteriaGrouprRepository.ExistAsync(x => x.TenantId == tenantId && x.Id == id);
        }

        public async Task<int> Delete(string tenantId, string id)
        {
            var criteriaGroupInfo = await GetInfo(tenantId, id, false);
            if (criteriaGroupInfo == null)
                return -1;

            criteriaGroupInfo.IsDelete = true;
            return Context.SaveChanges();
        }

        public async Task<CriteriaGroup> GetInfo(string tenantId, string id, bool isReadonly)
        {
            return await _criteriaGrouprRepository.GetAsync(isReadonly, c => c.TenantId == tenantId && c.Id == id && !c.IsDelete);
        }

        public async Task<List<Suggestion<string>>> Suggestion(string tenantId, string languageId)
        {
            var query = from cg in Context.Set<CriteriaGroup>()
                        join cgt in Context.Set<CriteriaGroupTranslation>() on cg.Id equals cgt.CriteriaGroupId
                        where cg.TenantId == tenantId && cgt.LanguageId == languageId
                                                      && cg.IsActive && !cg.IsDelete && !cgt.IsDelete
                        select new Suggestion<string>
                        {
                            Id = cg.Id,
                            Name = cgt.Name
                        };
            return await query.ToListAsync();
        }
    }
}
