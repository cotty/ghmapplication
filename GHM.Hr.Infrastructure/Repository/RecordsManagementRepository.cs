﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;

namespace GHM.Hr.Infrastructure.Repository
{
    public class RecordsManagementRepository : RepositoryBase, IRecordsManagementRepository
    {
        private readonly IRepository<UserRecordsManagement> _recordsRepository;
        private readonly IUserValueRepository _userValueRepository;

        public RecordsManagementRepository(IDbContext context, IUserValueRepository userValueRepository) : base(context)
        {
            _userValueRepository = userValueRepository;
            _recordsRepository = Context.GetRepository<UserRecordsManagement>();
        }

        public async Task<int> Save(List<UserRecordsManagement> listRecords)
        {
            _recordsRepository.Creates(listRecords);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(UserRecordsManagement records)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<UserRecordsManagement> GetInfo(string tenantId, string id, bool isReadOnly = false)
        {
            return await _recordsRepository.GetAsync(isReadOnly, x => x.Id == id && x.TenantId == tenantId);
        }

        public async Task<List<UserRecordsManagement>> GetListInsertedRecordByUserId(string tenantId, string userId)
        {
            return await _recordsRepository.GetsAsync(true, x => x.UserId.Equals(userId) && x.TenantId == tenantId);
        }

        public async Task<List<UserRecordsManagementViewModel>> GetListRecordsByUserId(string tenantId, string userId)
        {
            return await Context.Set<UserValue>().Where(x => x.Type == UserValueType.UserRecordsManagement)
                .GroupJoin(Context.Set<UserRecordsManagement>().Where(x => x.UserId.Equals(userId)), uv => uv.Id, ur => ur.ValueId, (uv, ur) => new { uv, ur })
                .SelectMany(x => x.ur.DefaultIfEmpty(), (x, ur) => new UserRecordsManagementViewModel
                {
                    IsSelected = x.ur.FirstOrDefault() != null && x.ur.FirstOrDefault().IsSelected,
                    Id = x.ur.Any() ? x.ur.FirstOrDefault().Id : "",
                    ValueId = x.uv.Id,
                    ValueName = x.uv.Name,
                    Note = x.ur.Any() ? x.ur.FirstOrDefault().Note : string.Empty,
                    UserId = x.ur.Any() ? x.ur.FirstOrDefault().UserId : string.Empty
                }).ToListAsync();
        }

        public async Task<List<ExportUserRecordManagermentViewModel>> GetListRecordExport(string tenantId, string officeIdPath)
        {
            return null;
        }

        public async Task<bool> CheckExist(string tenantId, string id)
        {
            return await _recordsRepository.ExistAsync(x => x.TenantId == tenantId && x.Id == id && !x.IsDelete);
        }

        public async Task<int> SaveAsync(UserRecordsManagement info)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<bool> CheckExist(string tenantId, string title, string userId, string valueId)
        {
            return await _recordsRepository.ExistAsync(x => x.TenantId == tenantId && x.UserId == userId && x.ValueId == valueId && x.Title == title && !x.IsDelete);
        }

        public Task<List<UserRecordsManagementViewModel>> Search(string tenantId, string userId, string valueId, string title, DateTime? fromDate, DateTime? toDate, int page, int pageSize, out int totalRows)
        {
            Expression<Func<UserRecordsManagement, bool>> spec = x => x.TenantId == tenantId && !x.IsDelete && x.UserId == userId;

            if(!string.IsNullOrEmpty(valueId))
                spec = spec.And(x => x.ValueId.Contains(valueId));

            if (!string.IsNullOrEmpty(title))
                spec = spec.And(x => x.Title.Contains(title));

            if (fromDate.HasValue)
                spec = spec.And(x => x.CreateTime >= fromDate);

            if (toDate.HasValue)
                spec = spec.And(x => x.CreateTime <= toDate);

            var query = Context.Set<UserRecordsManagement>().Where(spec)
                .Join(Context.Set<UserValue>().Where(x => x.TenantId == tenantId), urm => urm.ValueId, uv => uv.Id,
                (urm, uv) => new UserRecordsManagementViewModel
                {
                    Id = urm.Id,
                    ValueId = urm.ValueId,
                    Note = urm.Note,
                    AttachmentId = urm.AttachmentId,
                    ConcurrencyStamp = urm.ConcurrencyStamp,
                    IsSelected = urm.IsSelected,
                    Title = urm.Title,
                    UserId = urm.UserId ,
                    ValueName = uv.Name,
                    AttachmentName = urm.AttachmentName,
                    CreateTime = urm.CreateTime
                });

            totalRows =  query.Count();

            return query.OrderByDescending(x => x.Id).Skip((page - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<int> Insert(UserRecordsManagement records)
        {
            _recordsRepository.Create(records);
            return await Context.SaveChangesAsync();
        }
    }
}
