﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;

namespace GHM.Hr.Infrastructure.Repository
{
    public class OfficeRepository : RepositoryBase, IOfficeRepository
    {
        private readonly IRepository<Office> _officeRepository;

        public OfficeRepository(IDbContext context) : base(context)
        {
            _officeRepository = Context.GetRepository<Office>();
        }


        public async Task<int> Insert(Office office)
        {
            _officeRepository.Create(office);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(Office office)
        {
            return await Context.SaveChangesAsync();
        }

        // Truyền đữ liệu đầu vào nhưng không sử dụng vì sử dụng vì entity framework vẫn đang track object cần update.
        // Nhưng khi chuyển sang db khác. VD như Mongo thì cần update lại dữ liệu theo params truyền vào.
        // Sử dụng thế này để linh hoạt trong khâu chuyển db về sau.
        public async Task<int> UpdateOfficeIdPath(int id, string idPath)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateOfficeRoot(int id, int rootId, string rootIdPath)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateChildCount(int id, int childCount)
        {
            var info = await GetInfo(id);
            if (info == null)
                return -1;

            info.ChildCount = childCount;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateChildrenIdPath(string oldIdPath, string newIdPath)
        {
            var childrenOffice = await _officeRepository.GetsAsync(false, x => !x.IsDelete && x.IdPath.StartsWith(oldIdPath + "."));
            if (childrenOffice == null || !childrenOffice.Any())
                return -1;

            foreach (var office in childrenOffice)
            {
                office.IdPath = $"{newIdPath}.{office.Id}";
            }
            return await Context.SaveChangesAsync();
        }

        public async Task<int> SaveChangeAsync()
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(int officeId)
        {
            var officeInfo = await GetInfo(officeId);
            if (officeInfo == null)
                return -1;

            officeInfo.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(int officeId)
        {
            var info = await GetInfo(officeId);
            if (info == null)
                return -1;

            _officeRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<Office> GetInfo(int officeId, bool isReadOnly = false)
        {
            return await _officeRepository.GetAsync(isReadOnly, x => x.Id == officeId && !x.IsDelete);
        }

        public async Task<Office> GetInfo(string officeIdPath, bool isReadOnly = false)
        {
            return await _officeRepository.GetAsync(isReadOnly, x => x.IdPath == officeIdPath && !x.IsDelete);
        }

        public async Task<OfficeDetailViewModel> GetInfoByLanguage(string tenantId, int officeId, string languageId)
        {
            var query = Context.Set<Office>().Where(x => x.TenantId == tenantId && x.Id == officeId && !x.IsDelete)
                .Join(Context.Set<OfficeTranslation>().Where(x => x.LanguageId == languageId), o => o.Id,
                    ot => ot.OfficeId,
                    (o, ot) => new OfficeDetailViewModel
                    {
                        Id = o.Id,
                        Name = ot.Name,
                        Description = ot.Description,
                        Code = o.Code,
                        Address = ot.Address,
                        IsActive = o.IsActive,
                        ShortName = ot.ShortName,
                        Status = o.Status,
                        OfficeType = o.OfficeType,
                        ParentName = ot.ParentName
                    });
            return await query
                .AsNoTracking()
                .SingleOrDefaultAsync();
        }

        public async Task<OfficeDetailViewModel> GetInfoByLanguage(int officeId, string languageId)
        {
            var query = from o in Context.Set<Office>()
                        join ot in Context.Set<OfficeTranslation>() on o.Id equals ot.OfficeId
                        where o.Id == officeId && ot.LanguageId == languageId && !o.IsDelete
                        select new OfficeDetailViewModel
                        {
                            Id = o.Id,
                            Name = ot.Name,
                            Description = ot.Description,
                            Code = o.Code,
                            Address = ot.Address,
                            IsActive = o.IsActive,
                            ShortName = ot.ShortName,
                            Status = o.Status,
                            OfficeType = o.OfficeType,
                            ParentName = ot.ParentName
                        };
            return await query.FirstOrDefaultAsync();
        }

        public async Task<int> GetChildCount(int id)
        {
            return await _officeRepository.CountAsync(x => x.ParentId == id && !x.IsDelete);
        }

        public async Task<bool> CheckExistsByCode(int officeId, string code)
        {
            return await _officeRepository.ExistAsync(x => x.Id != officeId && x.Code == code && !x.IsDelete);
        }

        public async Task<bool> CheckOfficeExistsByTenantId(int officeId, string tenantId)
        {
            return await _officeRepository.ExistAsync(x => x.TenantId == tenantId && x.Id == officeId && !x.IsDelete);
        }

        public Task<List<OfficeSuggestionViewModel>> SearchForSuggestion(string tenantId, string languageId, string keyword, int page, int pageSize, out int totalRows)
        {
            Expression<Func<OfficeTranslation, bool>> specTranslation = x => x.LanguageId == languageId;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            var query = Context.Set<Office>()
                .Where(x => x.IsActive && !x.IsDelete && x.TenantId == tenantId)
                .Join(Context.Set<OfficeTranslation>().Where(specTranslation), o => o.Id,
                    ot => ot.OfficeId, (o, ot) => new OfficeSuggestionViewModel
                    {
                        Id = o.Id,
                        Name = ot.Name
                    }).AsNoTracking();
            totalRows = query.Count();
            return query
                .OrderByDescending(x => x.Id)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        public async Task<List<OfficeSearchViewModel>> GetAllActivatedOffice(string tenantId, string languageId)
        {
            var query = Context.Set<Office>().Where(x => x.IsActive && !x.IsDelete && x.TenantId == tenantId)
                .Join(Context.Set<OfficeTranslation>().Where(x => x.LanguageId == languageId), o => o.Id,
                    ot => ot.OfficeId, (o, ot) => new OfficeSearchViewModel
                    {
                        Code = o.Code,
                        Id = o.Id,
                        Name = ot.Name,
                        IsActive = o.IsActive,
                        ParentId = o.ParentId,
                        IdPath = o.IdPath,
                        ShortName = ot.ShortName,
                        OfficeType = o.OfficeType,
                        ParentName = ot.ParentName
                    });
            return await query.AsNoTracking()
                .ToListAsync();
        }

        public async Task<List<OfficeSearchViewModel>> GetActivedOfficeByRootId(string tenantId, string languageId, int rootId)
        {
            var query = Context.Set<Office>().Where(x => x.IsActive && !x.IsDelete && x.TenantId == tenantId && x.RootId == rootId)
                .Join(Context.Set<OfficeTranslation>().Where(x => x.LanguageId == languageId), o => o.Id,
                    ot => ot.OfficeId, (o, ot) => new OfficeSearchViewModel
                    {
                        Code = o.Code,
                        Id = o.Id,
                        Name = ot.Name,
                        IsActive = o.IsActive,
                        ParentId = o.ParentId,
                        IdPath = o.IdPath,
                        ShortName = ot.ShortName,
                        OfficeType = o.OfficeType,
                        ParentName = ot.ParentName
                    });
            return await query.ToListAsync();
        }

        public Task<List<OfficeSearchViewModel>> SearchOfficeByRootId(string tenantId, string languageId, int rootId,
            string keyword, bool? isActive, int page,
            int pageSize, out int totalRows)
        {
            Expression<Func<Office, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId && x.RootId == rootId;
            Expression<Func<OfficeTranslation, bool>> specTranslation = x => x.LanguageId == languageId;
            if (isActive.HasValue)
                spec = spec.And(x => x.IsActive == isActive.Value);

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            var query = Context.Set<Office>().Where(spec)
                .Join(Context.Set<OfficeTranslation>().Where(specTranslation), o => o.Id, ot => ot.OfficeId, (o, ot) => new OfficeSearchViewModel
                {
                    Id = o.Id,
                    IdPath = o.IdPath,
                    Name = ot.Name,
                    Code = o.Code,
                    IsActive = o.IsActive,
                    ShortName = ot.ShortName,
                    OfficeType = o.OfficeType
                });

            totalRows = query.Count();
            return query.ToListAsync();
        }

        public async Task<List<OfficeSearchViewModel>> GetOfficeUserTree(string tenantId, string languageId, string officeIdPath)
        {
            var query = Context.Set<Office>().Where(x => x.IsActive && !x.IsDelete && x.TenantId == tenantId
            && (x.IdPath.StartsWith(officeIdPath + '.') || x.IdPath == officeIdPath))
                 .Join(Context.Set<OfficeTranslation>().Where(x => x.LanguageId == languageId), o => o.Id,
                     ot => ot.OfficeId, (o, ot) => new OfficeSearchViewModel
                     {
                         Code = o.Code,
                         Id = o.Id,
                         Name = ot.Name,
                         IsActive = o.IsActive,
                         ParentId = o.ParentId,
                         IdPath = o.IdPath,
                         ShortName = ot.ShortName,
                         OfficeType = o.OfficeType,
                         ParentName = ot.ParentName
                     });
            return await query
                .ToListAsync();
        }

        public async Task<List<OfficeUserTreeViewModel>> GetOfficeUserTree(int? parentId, string languageId)
        {
            //return await Context.Set<Office>()
            //    .Where(x => x.ParentId == parentId)
            //    .Join(Context.Set<OfficeTranslation>().Where(x => x.LanguageId == languageId), o => o.Id,
            //        ot => ot.OfficeId, (o, ot) => new { o, ot })
            //    .GroupJoin(Context.Set<User>(), g => g.o.Id, u => u.OfficeId, (g, u) => new { g, u })
            //    .SelectMany(x => x.u.DefaultIfEmpty(), (x, u) => new OfficeUserTreeViewModel
            //    {
            //        OfficeId = x.g.o.Id,
            //        OfficeName = x.g.ot.Name,
            //        Level = x.g.o.Level,
            //        OfficeIdPath = x.g.o.IdPath,
            //        UserId = u.Id,
            //        PositionId = u.PositionId,
            //        TitleName = u.TitleName,
            //        FullName = u.FullName,
            //        ChildCount = x.g.o.ChildCount,
            //        ParentOfficeId = x.g.o.ParentId
            //    }).ToListAsync();
            return null;
        }

        public Task<List<OfficeSearchViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive, int page, int pageSize,
            out int totalRows)
        {
            Expression<Func<Office, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId;
            Expression<Func<OfficeTranslation, bool>> specTranslation = x => x.LanguageId == languageId;
            if (isActive.HasValue)
                spec = spec.And(x => x.IsActive == isActive.Value);

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            var query = Context.Set<Office>().Where(spec)
                .Join(Context.Set<OfficeTranslation>().Where(specTranslation), o => o.Id, ot => ot.OfficeId, (o, ot) => new OfficeSearchViewModel
                {
                    Id = o.Id,
                    IdPath = o.IdPath,
                    ParentId = o.ParentId,
                    Name = ot.Name,
                    Code = o.Code,
                    IsActive = o.IsActive,
                    ShortName = ot.ShortName,
                    OfficeType = o.OfficeType
                });

            totalRows = query.Count();
            return query
                .OrderBy(x => x.IdPath)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<Office> GetActiveInfo(int officeId, bool isReadOnly = false)
        {
            return await _officeRepository.GetAsync(isReadOnly, x => x.Id == officeId && x.IsActive && !x.IsDelete);
        }

        public async Task<OfficeShortInfoViewModel> GetSortInfo(string tenantId, string languageId, int officeId)
        {
            var query = from o in Context.Set<Office>()
                        join ot in Context.Set<OfficeTranslation>() on o.Id equals ot.OfficeId
                        where o.Id == officeId && ot.LanguageId == languageId && !o.IsDelete && o.TenantId == tenantId
                        select new OfficeShortInfoViewModel
                        {
                            Id = o.Id,
                            Name = ot.Name,
                            Description = ot.Description,
                            Code = o.Code,
                            Address = ot.Address,
                            IsActive = o.IsActive,
                            ShortName = ot.ShortName,
                            ParentName = ot.ParentName,
                            OfficeIdPath = o.IdPath
                        };

            return await query.SingleOrDefaultAsync();
        }
    }
}

