﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;

namespace GHM.Hr.Infrastructure.Repository
{
    public class UserValueRepository : RepositoryBase, IUserValueRepository
    {
        private readonly IRepository<UserValue> _userValueRepository;
        public UserValueRepository(IDbContext context) : base(context)
        {
            _userValueRepository = Context.GetRepository<UserValue>();
        }

        public async Task<int> Insert(UserValue userValue)
        {
            _userValueRepository.Create(userValue);
            return await Context.SaveChangesAsync();
        }

        public async Task<List<UserValue>> GetAllValueByType(string tenanId, UserValueType type)
        {
            return await _userValueRepository.GetsAsync(true, x => x.Type == type && x.TenantId == tenanId);
        }

        public async Task<UserValue> GetInfo(string tenantId, string id, bool isReadOnly = false)
        {
            return await _userValueRepository.GetAsync(isReadOnly, x => x.Id == id && x.TenantId == tenantId);
        }

        public async Task<UserValue> GetInfo(string tenantId, string id, UserValueType type, bool isReadOnly = false)
        {
            return await _userValueRepository.GetAsync(isReadOnly, x => x.Id == id && x.Type == type && x.TenantId == tenantId);
        }

        public async Task<UserValue> GetValueByName(string tenantId, string name, UserValueType type, bool isReadOnly = false)
        {
            return await _userValueRepository.GetAsync(isReadOnly, x => x.Name.ToLower().Equals(name.ToLower()) && x.Type == type && x.TenantId == tenantId);
        }

        public async Task<bool> CheckNameExists(string tenantId, string id, string name, UserValueType type)
        {
            return await _userValueRepository.ExistAsync(x => x.Id != id && x.Name.ToLower().Equals(name.ToLower()) && x.Type == type);
        }

        public Task<List<UserValueViewModel>> Search(string tenantId, string keyword, UserValueType type, int page, int pageSize, out int totalRows)
        {
            Expression<Func<UserValue, bool>> spec = x => x.Type == type && x.TenantId == tenantId;

            if (!string.IsNullOrWhiteSpace(keyword))
            {
                keyword = keyword.StripVietnameseChars();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            var result = from userValue in Context.Set<UserValue>().AsNoTracking().Where(spec)
                         select new UserValueViewModel()
                         {
                             Id = userValue.Id,
                             Name = userValue.Name,
                             Type = userValue.Type
                         };

            totalRows = result.Count();
            return result.OrderBy(x => x.Name).Skip((page - 1) * pageSize).Take(pageSize).AsNoTracking().ToListAsync();
        }

        public async Task<UserValue> GetInfoIfNotExistsInsert(string tenantId, string id, string value, UserValueType type)
        {
            if (string.IsNullOrEmpty(id))
            {
                // Check value exists by name.
                var valueInfo = await GetValueByName(tenantId, value, type);
                if (valueInfo == null)
                {
                    var userValue = new UserValue
                    {
                        Id = new Guid().ToString(),
                        Name = value,
                        UnsignName = value.StripVietnameseChars(),
                        Type = type
                    };

                    await Insert(userValue);
                    return userValue;
                }

                return valueInfo;
            }

            var userValueInfo = await GetInfo(tenantId, id, type);
            if (userValueInfo == null)
            {
                var userValue = new UserValue
                {
                    Id = new Guid().ToString(),
                    Name = value,
                    UnsignName = value.StripVietnameseChars(),
                    Type = type
                };

                await Insert(userValue);
                return userValue;
            }

            return userValueInfo;
        }

        public async Task<int> Update(UserValue userValue)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(UserValue userValue)
        {
            _userValueRepository.Delete(userValue);

            return await Context.SaveChangesAsync();
        }

        public async Task<bool> CheckExist(string tenantId, string courseId, string courseName, UserValueType type)
        {
            return await _userValueRepository.ExistAsync(x => x.TenantId == tenantId && x.Id == courseId && x.Name.Contains(courseName) && x.Type == type);
        }

        public async Task<bool> CheckExist(string tenantId, string id)
        {
            return await _userValueRepository.ExistAsync(x => x.TenantId == tenantId && x.Id == id);
        }
    }
}



