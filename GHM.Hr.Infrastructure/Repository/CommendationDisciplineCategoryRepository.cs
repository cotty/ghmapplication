﻿using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Hr.Infrastructure.Repository
{
    public class CommendationDisciplineCategoryRepository : RepositoryBase, ICommendationDisciplineCategoryRepository
    {
        private readonly IRepository<UserCommendationDisciplineCategory> _userCommendationDislineCategory;
        public CommendationDisciplineCategoryRepository(IDbContext context) : base(context)
        {
            _userCommendationDislineCategory = Context.GetRepository<UserCommendationDisciplineCategory>();
        }

        public Task<bool> CheckExist(string tenantId, int categoryId)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> CheckNameExist(string tenantId, string name, CommendationDisciplineType type)
        {
            return await _userCommendationDislineCategory.ExistAsync(x => x.TenantId == tenantId && x.Name == name && x.Type == type && !x.IsDelete);
        }

        public async Task<UserCommendationDisciplineCategory> GetInfoAsync(string tenantId, string id, bool isReadOnly = false)
        {
            return await _userCommendationDislineCategory.GetAsync(isReadOnly, x => x.TenantId == tenantId && x.Id == id && !x.IsDelete);
        }

        public async Task<int> InsertAsync(UserCommendationDisciplineCategory commendationDisciplineCategory)
        {
            _userCommendationDislineCategory.Create(commendationDisciplineCategory);

            return await Context.SaveChangesAsync();
        }

        public async Task<int> SaveAsync(UserCommendationDisciplineCategory info)
        {
            return await Context.SaveChangesAsync();
        }

        public Task<List<CommendationDisciplineCategoryViewModel>> Search(string tenantId, string keyword, string name, CommendationDisciplineType? type, int page, int pageSize, out int TotalRows)
        {
            Expression<Func<UserCommendationDisciplineCategory, bool>> spec = x => x.TenantId == tenantId && !x.IsDelete;

            if (!string.IsNullOrEmpty(keyword))
                spec = spec.And(x => x.Id == keyword);

            if(!string.IsNullOrEmpty(name))
            {
                var unsignName = name.StripVietnameseChars();

                spec = spec.And(x => x.UnsignName.Contains(unsignName));
            }

            if (type.HasValue)
                spec = spec.And(x => x.Type == type);

            var query = Context.Set<UserCommendationDisciplineCategory>().Where(spec).Select(x => new CommendationDisciplineCategoryViewModel
            {
                Id = x.Id,
                ConcurrencyStamp = x.ConcurrencyStamp,
                Name = x.Name,
                Type = x.Type
            });

            TotalRows = query.Count();
            return query.OrderByDescending(x => x.Id).Skip((page - 1) * pageSize).Take(pageSize).AsNoTracking().ToListAsync();
        }
    }
}
