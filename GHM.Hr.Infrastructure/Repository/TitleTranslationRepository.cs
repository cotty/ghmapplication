﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Infrastructure.SqlServer;

namespace GHM.Hr.Infrastructure.Repository
{
    public class TitleTranslationRepository : RepositoryBase, IRepositoryBase, ITitleTranslationRepository
    {
        private readonly IRepository<TitleTranslation> _titleTranslationRepository;

        public TitleTranslationRepository(IDbContext context) : base(context)
        {
            _titleTranslationRepository = Context.GetRepository<TitleTranslation>();
        }

        public async Task<int> Insert(TitleTranslation titleTranslation)
        {
            _titleTranslationRepository.Create(titleTranslation);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(TitleTranslation titleTranslation)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<TitleTranslation> titleTranslations)
        {
            _titleTranslationRepository.Creates(titleTranslations);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string tenantId, string titleId, string languageId)
        {
            var info = await GetInfo(tenantId, titleId, languageId);
            if (info == null)
                return -1;

            _titleTranslationRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDeleteByTitleId(string tenantId, string titleId)
        {
            var titleTranslations = await _titleTranslationRepository.GetsAsync(false, x => x.TitleId == titleId && x.TenantId == tenantId);
            if (!titleTranslations.Any())
                return -1;

            _titleTranslationRepository.Deletes(titleTranslations);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeleteByTitleId(string tenantId, string titleId)
        {
            var listTitleLanguages = await GetsByTitleId(tenantId, titleId);
            if (!listTitleLanguages.Any())
                return -1;

            _titleTranslationRepository.Deletes(listTitleLanguages);
            return await Context.SaveChangesAsync();
        }

        public async Task<TitleTranslation> GetInfo(string tenantId, string titleId, string languageId, bool isReadOnly = false)
        {
            return await _titleTranslationRepository.GetAsync(isReadOnly, x => x.TenantId == tenantId &&
                x.TitleId == titleId && x.LanguageId == languageId);
        }

        public async Task<List<TitleTranslation>> GetsByTitleId(string tenantId, string titleId)
        {
            return await _titleTranslationRepository.GetsAsync(false, x => x.TitleId == titleId && x.TenantId == tenantId);
        }

        public async Task<bool> CheckExists(string titleId, string tenantId, string languageId, string name)
        {
            name = name.Trim();
            return await _titleTranslationRepository.ExistAsync(x => x.TenantId == tenantId &&
                x.TitleId != titleId && x.LanguageId == languageId && x.Name == name);
        }

        public async Task<bool> CheckShortNameExists(string titleId, string tenantId, string languageId, string shortName)
        {
            shortName = shortName.Trim();
            return await _titleTranslationRepository.ExistAsync(x => x.TenantId == tenantId &&
                x.TitleId != titleId && x.LanguageId == languageId && x.ShortName == shortName);
        }

        public async Task<int> Update()
        {
            return await Context.SaveChangesAsync();
        }
    }
}
