﻿using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.SqlServer;
using GHM.Infrastructure.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GHM.Hr.Infrastructure.Repository
{
    public class LaborContractFormRepository : RepositoryBase, ILaborContractFormRepository
    {
        private readonly IRepository<LaborContractForm> _laborContractFormRepository;

        public LaborContractFormRepository(IDbContext context) : base(context)
        {
            _laborContractFormRepository = Context.GetRepository<LaborContractForm>();
        }

        public async Task<bool> CheckExists(string tenantId, string id, string laborContractTypeId, string unsignName)
        {
            return await _laborContractFormRepository.ExistAsync(x => x.TenantId == tenantId && x.Id != id
            && x.LaborContractTypeId == laborContractTypeId && x.UnsignName.Equals(unsignName));
        }

        public async Task<int> Delete(string tenantId, string id)
        {
            var info = await GetInfo(tenantId, id);
            if (info == null)
                return -1;

            _laborContractFormRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<LaborContractForm> GetInfo(string tenantId, string id, bool isReadOnly = false)
        {
            return await _laborContractFormRepository.GetAsync(isReadOnly, x => x.TenantId == tenantId && x.Id == id && !x.IsDelete);
        }

        public Task<List<Suggestion<string>>> GetLaborContractFormByType(string tenantId, string keyword, string type, int page, int pageSize, out int totalRows)
        {
            Expression<Func<LaborContractForm, bool>> spec = x => x.TenantId == tenantId && !x.IsDelete && x.IsActive;
            if (!string.IsNullOrEmpty(type))
            {
                spec = spec.And(x => x.LaborContractTypeId == type);
            }

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripVietnameseChars().ToUpper();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            var result = from laborContractForm in Context.Set<LaborContractForm>()
                         .Where(spec).AsNoTracking()
                         select new Suggestion<string>
                         {
                             Id = laborContractForm.Id,                             
                             Name = laborContractForm.Name,
                         };

            totalRows = result.Count();
            return result.OrderBy(x=> x.Name).ToListAsync();
        }

        public async Task<int> Insert(LaborContractForm laborContractForm)
        {
            _laborContractFormRepository.Create(laborContractForm);
            return await Context.SaveChangesAsync();
        }

        public Task<List<LaborContractFormViewModel>> Search(string tenantId, string keyword, string type, int page, int pageSize,
            out int totalRows)
        {
            Expression<Func<LaborContractForm, bool>> spec = x => x.TenantId == tenantId && !x.IsDelete;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (!string.IsNullOrEmpty(type))
            {
                spec = spec.And(x => x.LaborContractTypeId == type);
            }

            var result = from laborContractForm in Context.Set<LaborContractForm>()
                         .Where(spec).AsNoTracking()
                         join laborContractTye in Context.Set<UserValue>().Where(x=> x.TenantId == tenantId && x.Type == UserValueType.LaborContract)
                         on laborContractForm.LaborContractTypeId equals laborContractTye.Id
                         select new LaborContractFormViewModel
                         {
                             Id = laborContractForm.Id,
                             LaborContractTypeId = laborContractTye.Id,
                             LaborContractTypeName = laborContractTye.Name,
                             Name = laborContractForm.Name,
                             FileId = laborContractForm.FileId,
                             FileName = laborContractForm.FileName,
                             IsActive = laborContractForm.IsActive
                         };

            totalRows = result.Count();
            return result.OrderBy(x => x.Name)
                .Skip((page - 1) * pageSize).Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<int> Update(LaborContractForm laborContractForm)
        {
            return await Context.SaveChangesAsync();
        }
    }
}
