﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;

namespace GHM.Hr.Infrastructure.Repository
{
    public class InsuranceRepository : RepositoryBase, IInsuranceRepository
    {
        private readonly IRepository<UserInsurance> _insuranceRepository;
        private readonly IUserValueRepository _userValueRepository;
        private readonly IUserRepository _userRepository;

        public InsuranceRepository(IDbContext context, IUserValueRepository userValueRepository, IUserRepository userRepository) : base(context)
        {
            _userValueRepository = userValueRepository;
            _userRepository = userRepository;
            _insuranceRepository = Context.GetRepository<UserInsurance>();
        }

        public async Task<int> Insert(UserInsurance insurance)
        {            
            _insuranceRepository.Create(insurance);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(UserInsurance insurance)
        {            
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string tenantId, string id)
        {
            var info = await GetInfo(tenantId, id);
            if (info == null)
                return -1;

            _insuranceRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<UserInsurance> GetInfo(string tenantId, string id, bool isReadOnly = false)
        {
            return await _insuranceRepository.GetAsync(isReadOnly, x => x.Id == id && x.TenantId == tenantId);
        }

        public async Task<bool> CheckExists(string tenantId, string id, string userId, DateTime fromDate, DateTime? toDate)
        {
            Expression<Func<UserInsurance, bool>> spec = x => x.TenantId == tenantId && x.Id != id
            && x.UserId == userId && x.FromDate <= fromDate && x.ToDate >= fromDate;

            if (toDate.HasValue)
            {
                spec = spec.And(x => x.FromDate <= toDate.Value && x.ToDate >= toDate.Value);
            }

            return await _insuranceRepository.ExistAsync(spec);
        }

        public Task<List<UserInsurance>> Search(string tenantId, string keyword, string userId, bool? type,
            DateTime? fromDate, DateTime? toDate, int page, int pageSize, out int totalRows)
        {
            Expression<Func<UserInsurance, bool>> spec = x => x.UserId.Equals(userId);

            if (!string.IsNullOrWhiteSpace(keyword))
            {
                keyword = keyword.StripVietnameseChars();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (type.HasValue)
            {
                spec = spec.And(x => x.Type == type.Value);
            }

            if (fromDate.HasValue)
            {
                spec = spec.And(x => x.FromDate <= fromDate);
            }

            if (toDate.HasValue)
            {
                spec = spec.And(x => x.ToDate >= toDate.Value);
            }

            var sort = Context.Filters.Sort<UserInsurance, DateTime>(x => x.CreateTime, true);
            var paging = Context.Filters.Page<UserInsurance>(page, pageSize);

            totalRows = _insuranceRepository.Count(spec);
            return _insuranceRepository.GetsAsync(true, spec, sort, paging);
        }
    }
}

