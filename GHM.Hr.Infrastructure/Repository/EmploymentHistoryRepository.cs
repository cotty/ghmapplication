﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;

namespace GHM.Hr.Infrastructure.Repository
{
    public class EmploymentHistoryRepository : RepositoryBase, IEmploymentHistoryRepository
    {
        private readonly IRepository<UserEmploymentHistory> _employmentRepository;

        public EmploymentHistoryRepository(IDbContext context, IUserValueRepository userValueRepository, IOfficeRepository officeRepository, ITitleRepository titleRepository, IUserRepository userRepository) : base(context)
        {
            _employmentRepository = Context.GetRepository<UserEmploymentHistory>();
        }

        public async Task<int> Insert(UserEmploymentHistory employment)
        {            
            _employmentRepository.Create(employment);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(UserEmploymentHistory employment)
        {           
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string tenantId, string id)
        {
            var info = await GetInfo(tenantId, id);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<UserEmploymentHistory> GetInfo(string tenantId, string id, bool isReadOnly = false)
        {
            return await _employmentRepository.GetAsync(isReadOnly, x => x.Id == id && !x.IsDelete && x.TenantId == tenantId);
        }

        public async Task<bool> CheckExists(string tenantId, string id, string userId, string companyId, int officeId, string titleId,
            DateTime fromDate, DateTime? toDate)
        {
            Expression<Func<UserEmploymentHistory, bool>> spec = x => x.Id != id && !x.IsDelete && x.UserId.Equals(userId)
            && x.OfficeId == officeId && x.TitleId == titleId;

            if (string.IsNullOrEmpty(companyId))
                spec = spec.And(x => x.CompanyId == companyId);

            if (toDate.HasValue)
                spec = spec.And(x => x.FromDate <= fromDate && x.ToDate >= fromDate);
            else
                spec = spec.And(x => ((x.FromDate <= fromDate && x.ToDate >= fromDate) || (x.ToDate <= toDate.Value && x.ToDate >= toDate.Value)));

            return await _employmentRepository.ExistAsync(spec);
        }

        public Task<List<UserEmploymentHistory>> Search(string tenantId, string keyword, string userId, bool? type, string companyId,
            bool? isCurrent, DateTime? fromDate, DateTime? toDate, int page,
            int pageSize, out int totalRows)
        {
            Expression<Func<UserEmploymentHistory, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripVietnameseChars();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (!string.IsNullOrEmpty(userId))
            {
                spec = spec.And(x => x.UserId.Equals(userId));
            }

            if (type.HasValue)
            {
                spec = spec.And(x => x.Type == type.Value);
            }

            if (string.IsNullOrEmpty(companyId))
            {
                spec = spec.And(x => x.CompanyId == companyId);
            }

            if (isCurrent.HasValue)
            {
                spec = spec.And(x => x.IsCurrent == isCurrent.Value);
            }

            if (fromDate.HasValue)
            {
                spec = spec.And(x => x.FromDate >= fromDate.Value);
            }

            if (toDate.HasValue)
            {
                spec = spec.And(x => x.ToDate <= toDate.Value);
            }

            var sort = Context.Filters.Sort<UserEmploymentHistory, DateTime>(a => a.CreateTime, true);
            var paging = Context.Filters.Page<UserEmploymentHistory>(page, pageSize);

            totalRows = _employmentRepository.Count(spec);
            return _employmentRepository.GetsAsync(true, spec, sort, paging);
        }      
    }
}
