﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;

namespace GHM.Hr.Infrastructure.Repository
{
    public class OfficeContactRepository : RepositoryBase, IOfficeContactRepository
    {
        private readonly IRepository<OfficeContact> _officeContactRepository;
        public OfficeContactRepository(IDbContext context) : base(context)
        {
            _officeContactRepository = Context.GetRepository<OfficeContact>();
        }

        public async Task<int> Insert(OfficeContact officeContact)
        {
            _officeContactRepository.Create(officeContact);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<OfficeContact> officeContacts)
        {
            _officeContactRepository.Creates(officeContacts);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(OfficeContact officeContact)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string id)
        {
            var info = await _officeContactRepository.GetAsync(false, x => x.Id == id && !x.IsDelete);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(int officeId, string userId)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeleteByOfficeId(int officeId)
        {
            var officeContacts = await _officeContactRepository.GetsAsync(false, x => x.OfficeId == officeId && !x.IsDelete);
            if (officeContacts == null || !officeContacts.Any())
                return -1;

            _officeContactRepository.Deletes(officeContacts);
            return await Context.SaveChangesAsync();
        }

        public async Task<List<OfficeContactViewModel>> GetsByOfficeId(int officeId, string languageId)
        {
            var query = Context.Set<OfficeContact>().Where(x => !x.IsDelete && x.OfficeId == officeId)
                 .Join(Context.Set<UserTranslation>().Where(x => x.LanguageId == languageId),
                        c => c.UserId,
                        ut => ut.UserId,
                        (c, ut) => new OfficeContactViewModel
                        {
                            Id = c.Id,
                            UserId = c.UserId,
                            FullName = c.FullName,
                            OfficeName = ut.OfficeName,
                            PositionName = ut.PositionName,
                            Email = c.Email,
                            PhoneNumber = c.PhoneNumber,
                            Avatar = c.Avatar,
                            TitlePrefixing = c.TitlePrefixing,
                            Fax = c.Fax
                        });
            return await query.AsNoTracking().ToListAsync();
        }

        public async Task<OfficeContact> GetInfo(string id, bool isReadOnly = false)
        {
            return await _officeContactRepository.GetAsync(isReadOnly,
                x => x.Id == id && !x.IsDelete);
        }

        public async Task<OfficeContact> GetInfo(int officeId, string userId, bool isReadOnly = false)
        {
            return await _officeContactRepository.GetAsync(isReadOnly,
                x => x.OfficeId == officeId && x.UserId == userId && !x.IsDelete);
        }

        public async Task<bool> CheckPhoneNumberExists(int officeId, string userId, string phoneNumber)
        {
            phoneNumber = phoneNumber.Trim();
            return await _officeContactRepository.ExistAsync(
                x => x.OfficeId == officeId && x.UserId != userId && x.PhoneNumber == phoneNumber && !x.IsDelete);
        }

        public async Task<bool> CheckEmailExists(int officeId, string userId, string email)
        {
            email = email.Trim();
            return await _officeContactRepository.ExistAsync(
                x => x.OfficeId == officeId && x.UserId != userId && x.Email == email && !x.IsDelete);
        }

        public async Task<bool> CheckUserExists(int officeId, string userId)
        {
            return await _officeContactRepository.ExistAsync(x => x.OfficeId == officeId && x.UserId == userId && !x.IsDelete);
        }
    }
}
