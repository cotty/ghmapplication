﻿using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Infrastructure.SqlServer;

namespace GHM.Hr.Infrastructure.Repository
{
    public class AssessmentRepository : RepositoryBase, IAssessmentRepository
    {
        private readonly IRepository<CriteriaPosition> _criteriaTitleRepository;
        public AssessmentRepository(IDbContext context, IRepository<CriteriaPosition> criteriaTitleRepository) : base(context)
        {
            _criteriaTitleRepository = criteriaTitleRepository;
        }


    }
}
