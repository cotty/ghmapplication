﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;

namespace GHM.Hr.Infrastructure.Repository
{
    public class TitleRepository : RepositoryBase, ITitleRepository
    {
        private readonly IRepository<Title> _titleRepository;
        private readonly IPositionRepository _positionRepository;

        public TitleRepository(IDbContext context, IPositionRepository positionRepository) : base(context)
        {
            _positionRepository = positionRepository;
            _titleRepository = Context.GetRepository<Title>();
        }

        public IOfficePositionRepository OfficeTitleRepository { get; set; }
        public IUserRepository UserRepository { get; set; }

        public Task<List<TitleViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive, int page, int pageSize, out int totalRows)
        {
            Expression<Func<Title, bool>> spec = t => !t.IsDelete && t.TenantId == tenantId;
            Expression<Func<TitleTranslation, bool>> specTranslation = tt => tt.LanguageId == languageId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            if (isActive.HasValue)
            {
                spec = spec.And(x => x.IsActive == isActive.Value);
            }

            var query = Context.Set<Title>().Where(spec)
                .Join(Context.Set<TitleTranslation>().Where(specTranslation), t => t.Id, tt => tt.TitleId, (t, tt) =>
                    new TitleViewModel
                    {
                        Id = t.Id,
                        Name = tt.Name,
                        ShortName = tt.ShortName,
                        CreateTime = t.CreateTime,
                        Description = tt.Description,
                        IsActive = t.IsActive
                    });

            totalRows = _titleRepository.Count(spec);
            return query.OrderByDescending(x => x.Id)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<List<TitleSearchForSelectViewModel>> SearchForSelect(string tenantId, string languageId, string keyword)
        {
            Expression<Func<Title, bool>> spec = x => x.IsActive && !x.IsDelete && x.TenantId == tenantId;
            Expression<Func<TitleTranslation, bool>> specTranslation = x => x.LanguageId == languageId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            var query = Context.Set<Title>().Where(spec)
                .Join(Context.Set<TitleTranslation>().Where(specTranslation), t => t.Id, tt => tt.TitleId, (t, tt) =>
                    new TitleSearchForSelectViewModel
                    {
                        Id = t.Id,
                        Name = tt.Name
                    });
            return await query.AsNoTracking().ToListAsync();
        }

        public async Task<int> Insert(Title title)
        {
            _titleRepository.Create(title);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update()
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string tenantId, string titleId)
        {
            var info = await GetInfo(tenantId, titleId);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(string titleId)
        {
            var info = await GetInfo(titleId);
            if (info == null)
                return -1;

            _titleRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<Title> GetInfo(string id, bool isReadonly = false)
        {
            return await _titleRepository.GetAsync(isReadonly, x => x.Id == id && !x.IsDelete);
        }

        public async Task<Title> GetInfo(string tenantId, string id, bool isReadonly = false)
        {
            return await _titleRepository.GetAsync(isReadonly, x => x.Id == id && x.TenantId == tenantId && !x.IsDelete);
        }
    }
}
