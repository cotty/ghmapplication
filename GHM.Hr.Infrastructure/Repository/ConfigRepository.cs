﻿using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Infrastructure.SqlServer;

namespace GHM.Hr.Infrastructure.Repository
{
    public class ConfigRepository : RepositoryBase, IConfigRepository
    {
        public readonly IRepository<Config> _configRepository;

        public ConfigRepository(IDbContext context) : base(context)
        {
            _configRepository = Context.GetRepository<Config>();
        }

        public async Task<Config> GetInfo(string tenantId, string languageId, string key, bool isReadOnly = false)
        {
            return await _configRepository.GetAsync(isReadOnly,
                x => x.TenantId == tenantId && x.LanguageId == languageId && x.Key == key);
        }

        public async Task<int> Insert(Config config)
        {
            _configRepository.Create(config);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(Config config)
        {
            return await Context.SaveChangesAsync();
        }
    }
}
