﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;

namespace GHM.Hr.Infrastructure.Repository
{
    public class UserAssessmentCriteriaRepository : RepositoryBase, IUserAssessmentCriteriaRepository
    {
        private readonly IRepository<UserAssessmentCriteria> _userAssessmentCriteriaRepository;
        public UserAssessmentCriteriaRepository(IDbContext context) : base(context)
        {
            _userAssessmentCriteriaRepository = Context.GetRepository<UserAssessmentCriteria>();
        }

        public async Task<int> Insert(List<UserAssessmentCriteria> listUserassessmentCriteria)
        {
            _userAssessmentCriteriaRepository.Creates(listUserassessmentCriteria);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UserUpdatePoint(UserAssessmentCriteria userAssessmentCriteria)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ManagerUpdatePoint(UserAssessmentCriteria userAssessmentCriteria)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ApproverUpdatePoint(UserAssessmentCriteria userAssessmentCriteria)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateNote(string userAssessmentId, string criteriaId, string note)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<UserAssessmentCriteria> GetInfo(string id, bool isReadonly = false)
        {
            return await _userAssessmentCriteriaRepository.GetAsync(isReadonly, x => x.Id == id);
        }

        public async Task<List<UserAssessmentCriteriaViewModel>> GetListAssessmentCriteria(string userAssessmentId, string languageId)
        {
            var query = from uac in Context.Set<UserAssessmentCriteria>()
                        join c in Context.Set<Criteria>() on uac.CriteriaId equals c.Id
                        join ct in Context.Set<CriteriaTranslation>() on c.Id equals ct.CriteriaId
                        join cgt in Context.Set<CriteriaGroupTranslation>() on c.GroupId equals cgt.CriteriaGroupId
                        where uac.UserAssessmentId == userAssessmentId && ct.LanguageId == languageId
                        select new UserAssessmentCriteriaViewModel
                        {
                            Id = uac.Id,
                            CriteriaGroupId = cgt.CriteriaGroupId,
                            CriteriaGroupName = cgt.Name,
                            CriteriaId = c.Id,
                            CriteriaName = ct.Name,
                            Point = c.Point,
                            UserPoint = uac.UserPoint,
                            ManagerPoint = uac.ManagerPoint,
                            ApproverPoint = uac.ApproverPoint,
                            Description = ct.Description,
                            Sanction = ct.Sanction,
                            Note = uac.Note
                        };
            return await query.ToListAsync();
        }

        public async Task<decimal> GetTotalUserPoint(string userAssessmentId)
        {
            var query = from uac in Context.Set<UserAssessmentCriteria>()
                        where uac.UserAssessmentId == userAssessmentId
                        select uac.UserPoint;
            return await query.SumAsync();
        }

        public async Task<decimal> GetTotalManagerPoint(string userAssessmentId)
        {
            var query = from uac in Context.Set<UserAssessmentCriteria>()
                        where uac.UserAssessmentId == userAssessmentId
                        select uac.ManagerPoint;
            return (decimal)await query.SumAsync();
        }

        public async Task<decimal> GetTotalApproverPoint(string userAssessmentId)
        {
            var query = from uac in Context.Set<UserAssessmentCriteria>()
                        where uac.UserAssessmentId == userAssessmentId
                        select uac.ApproverPoint;
            return (decimal)await query.SumAsync();
        }

        public async Task<List<UserAssessmentCriteria>> GetListAssessmentCriteria(string userAssessmentId)
        {
            return await _userAssessmentCriteriaRepository.GetsAsync(false,
                x => x.UserAssessmentId == userAssessmentId);
        }

        public async Task<int> Updates(List<UserAssessmentCriteria> userAssessmentCriterias)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<bool> CheckExistsByCriteriaId(string criteriaId)
        {
            return await _userAssessmentCriteriaRepository.ExistAsync(x => x.CriteriaId == criteriaId);
        }
    }
}
