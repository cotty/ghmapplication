﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;

namespace GHM.Hr.Infrastructure.Repository
{
    public class LaborContractRepository : RepositoryBase, ILaborContractRepository
    {
        private readonly IRepository<UserLaborContract> _laborContractRepository;

        public LaborContractRepository(IDbContext context) : base(context)
        {
            _laborContractRepository = Context.GetRepository<UserLaborContract>();
        }

        public async Task<int> Insert(UserLaborContract laborContract)
        {
            _laborContractRepository.Create(laborContract);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(UserLaborContract laborContract)
        {

            var result = await Context.SaveChangesAsync();
            return result;
        }

        public async Task<int> Delete(string tenantId, string id)
        {
            var info = await GetInfo(tenantId, id);
            if (info == null)
                return -1;

            _laborContractRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<bool> CheckExists(string tenantId, string type, string laborContractFormId, string userId)
        {
            return await _laborContractRepository.ExistAsync(x => x.Type == type
            && x.UserId.Equals(userId) && x.TenantId == tenantId && x.LaborContractFormId == laborContractFormId);
        }

        public async Task<bool> CheckNoExists(string teanntId, string id, string no)
        {
            return await _laborContractRepository.ExistAsync(x => x.Id != id && x.No.Equals(no) && x.TenantId == teanntId);
        }

        public async Task<UserLaborContract> GetInfo(string tenantId, string id, bool isReadOnly = false)
        {
            return await _laborContractRepository.GetAsync(isReadOnly, x => x.Id == id && x.TenantId == tenantId);
        }

        public async Task<LaborContractStatisticsViewModel> GetStatisticsResult()
        {
            var today = DateTime.Today;
            var startOfMonth = new DateTime(today.Year, today.Month, 1);
            var endOfMonth = startOfMonth.AddMonths(1).AddMinutes(-1);
            var startOfNextMonth = new DateTime(today.Year, today.AddMonths(1).Month, 1);
            var endOfNextMonth = startOfNextMonth.AddMonths(1).AddMinutes(-1);

            var total = await _laborContractRepository.CountAsync();
            var inUse = await _laborContractRepository.CountAsync(x => x.IsUse);
            var expireInThisMonth = await _laborContractRepository.CountAsync(x => x.ToDate >= startOfMonth && x.ToDate <= endOfMonth && x.IsUse);
            var expireInNextMonth = await _laborContractRepository.CountAsync(x => x.ToDate >= startOfNextMonth && x.ToDate <= endOfNextMonth && x.IsUse);
            return new LaborContractStatisticsViewModel
            {
                Total = total,
                InUse = inUse,
                ExpiresInNextMonth = expireInNextMonth,
                ExpiresInThisMonth = expireInThisMonth
            };
        }

        public Task<List<UserLaborContractViewModel>> Search(string tenantId, string languageId, string keyword, string userId,
            int? officeId, string titleId, string positionId,
            string type, DateTime? fromDate, DateTime? toDate, bool? isUse, byte? month, int page, int pageSize, out int totalRows)
        {
            Expression<Func<UserLaborContract, bool>> spec = x => x.TenantId == tenantId;
            Expression<Func<User, bool>> specUser = x => !x.IsDelete && x.TenantId == tenantId
            && x.Status <= Domain.Constants.UserStatus.Official;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripVietnameseChars();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (!string.IsNullOrEmpty(type))
            {
                spec = spec.And(x => x.Type.Equals(type));
            }

            if (!string.IsNullOrEmpty(userId))
            {
                spec = spec.And(x => x.UserId.Equals(userId));
            }

            // Get All
            if (isUse.HasValue && !month.HasValue)
            {
                spec = spec.And(x => x.IsUse == isUse.Value);
            }

            if (fromDate.HasValue)
            {
                spec = spec.And(x => x.FromDate >= fromDate.Value.Date.AddMilliseconds(-1));
            }

            if (toDate.HasValue)
            {
                spec = spec.And(x => x.ToDate <= toDate.Value.Date.AddDays(1).AddMilliseconds(-1));
            }

            if (officeId.HasValue)
                spec = spec.And(x => x.OfficeId == officeId);

            if (!string.IsNullOrEmpty(positionId))
                spec = spec.And(x => x.PositionId == positionId);

            if (!string.IsNullOrEmpty(titleId))
                spec = spec.And(x => x.TitleId == titleId);

            if (month.HasValue)
                spec = spec.And(x => x.ToDate.HasValue && x.ToDate.Value.Month == month.Value && x.IsUse);

            var result = from labor in Context.Set<UserLaborContract>().Where(spec).AsNoTracking()
                         join user in Context.Set<User>().Where(specUser)
                          on labor.UserId equals user.Id
                         join userTranslation in Context.Set<UserTranslation>().Where(x => x.LanguageId == languageId)
                         on labor.UserId equals userTranslation.UserId
                         select new UserLaborContractViewModel
                         {
                             Id = labor.Id,
                             No = labor.No,
                             UserId = labor.UserId,
                             FullName = labor.FullName,
                             TitleName = userTranslation.TitleName,
                             PositionName = userTranslation.PositionName,
                             OfficeName = userTranslation.OfficeName,
                             FileId = labor.FileId,
                             FileName = labor.FileName,
                             Type = labor.Type,
                             TypeName = labor.TypeName,
                             FromDate = labor.FromDate,
                             ToDate = labor.ToDate,
                             CreateTime = labor.CreateTime,
                             IsUse = labor.IsUse,
                             LaborContractFormId = labor.LaborContractFormId,
                             LaborContractFormName = labor.LaborContractFormName,
                             RegisterDate = labor.RegisterDate,
                             Status = labor.Status
                         };

            totalRows = result.Count();
            return result.OrderBy(x => x.UserId).Skip((page - 1) * pageSize).Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public Task<List<UserLaborContractViewModel>> SearchExpires(string tenantId, string languageId, string keyword,
            string userId, bool isNext, string type, DateTime? fromDate, DateTime? toDate, int page,
            int pageSize, out int totalRows)
        {
            var today = DateTime.Today;
            var startOfMonth = new DateTime(today.Year, today.Month, 1);
            var endOfMonth = startOfMonth.AddMonths(1).AddMinutes(-1);
            Expression<Func<UserLaborContract, bool>> spec = x => x.IsUse;

            if (isNext)
            {
                startOfMonth = new DateTime(today.Month == 12 ? today.AddYears(1).Year : today.Year, today.Month == 12 ? 1 : today.AddMonths(1).Month, 1);
                endOfMonth = startOfMonth.AddMonths(1).AddMinutes(-1);
            }

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripVietnameseChars();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (!string.IsNullOrEmpty(userId))
                spec = spec.And(x => x.UserId.Equals(userId));

            if (!string.IsNullOrEmpty(type))
            {
                var listType = type.Split(',');
                if (listType.Any())
                {
                    spec = spec.And(x => listType.Contains(x.Type));
                }
            }

            if (fromDate.HasValue)
                spec = spec.And(x => x.ToDate >= fromDate.Value && x.ToDate >= startOfMonth);
            else
                spec = spec.And(x => x.ToDate >= startOfMonth);

            if (toDate.HasValue)
                spec = spec.And(x => x.ToDate <= toDate.Value && x.ToDate <= endOfMonth);
            else
                spec = spec.And(x => x.ToDate <= endOfMonth);

            var result = from labor in Context.Set<UserLaborContract>().Where(spec).AsNoTracking()
                         join userTranslation in Context.Set<UserTranslation>().Where(x => x.LanguageId == languageId)
                         on labor.UserId equals userTranslation.UserId
                         select new UserLaborContractViewModel
                         {
                             Id = labor.Id,
                             No = labor.No,
                             UserId = labor.UserId,
                             FullName = labor.FullName,
                             TitleName = userTranslation.TitleName,
                             PositionName = userTranslation.PositionName,
                             OfficeName = userTranslation.OfficeName,
                             Type = labor.Type,
                             TypeName = labor.Type,
                             FromDate = labor.FromDate,
                             ToDate = labor.ToDate,
                             CreateTime = labor.CreateTime,
                             FileId = labor.FileId,
                             FileName = labor.FileName,
                             IsUse = labor.IsUse,
                             LaborContractFormId = labor.LaborContractFormId,
                             LaborContractFormName = labor.LaborContractFormName
                         };

            totalRows = result.Count();
            return result.OrderBy(x => x.CreateTime).Skip((page - 1) * pageSize).Skip(pageSize).ToListAsync();
        }

        public async Task<List<UserLaborContractExportViewModel>> GetListLaborContractExport(string tenantId, string languageId, string officeIdPath)
        {
            Expression<Func<User, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId;
            var queryExport = Context.Set<User>().Where(spec)
                .GroupJoin(Context.Set<UserLaborContract>().Where(x => x.IsUse), u => u.Id, ul => ul.UserId, (u, ul) => new { u, ul })
                .SelectMany(x => x.ul.DefaultIfEmpty(), (x, ul) => new UserLaborContractExportViewModel
                {
                    Id = x.u.Id,
                    FullName = x.u.FullName,
                    ContractNo = ul != null ? ul.No : "",
                    ContractType = ul != null ? ul.TypeName : "",
                    FromDate = ul != null ? ul.FromDate : (DateTime?)null,
                    ToDate = ul != null ? ul.ToDate : null
                }).OrderBy(x => x.OfficeId);
            return await queryExport.ToListAsync();
        }

        public async Task<int> CountLaborContractByForm(string tenantId, string laborContractFormId)
        {
            return await _laborContractRepository.CountAsync(x => x.TenantId == tenantId && x.LaborContractFormId == laborContractFormId);
        }

        public async Task<List<UserLaborContract>> GetListUserLaborContract(string tenantId, string userId, string id, bool isReadOnly = false)
        {
            return await _laborContractRepository.GetsAsync(isReadOnly, x => x.TenantId == tenantId
            && x.UserId == userId && (string.IsNullOrEmpty(id) || x.Id != id));
        }

        public async Task<int> UpdateList(List<UserLaborContract> laborContracts)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> CountUserLaborContactByUserId(string tenantId, string userId)
        {
            return await _laborContractRepository.CountAsync(x => x.UserId == userId && x.TenantId == tenantId);
        }

        public async Task<int> TotalLaborExpriedInMonth(string tenantId)
        {
            return await _laborContractRepository.CountAsync(x => x.TenantId == tenantId && x.IsUse && x.ToDate.HasValue
            && x.ToDate.Value.Month == DateTime.Now.Month);
        }

        public Task<int> TotalUserHaveNotLaborContract(string tenantId)
        {
            var result = from user in Context.Set<User>()
                         join contract in Context.Set<UserLaborContract>().Where(x => x.TenantId == tenantId && x.IsUse) on user.Id equals contract.UserId
                         into g
                         from contract in g.DefaultIfEmpty()
                         where user.TenantId == tenantId && !user.IsDelete && contract == null
                         select contract.Id;

            return result.CountAsync();
        }

        public Task<bool> CheckUserHaveLaborContractActive(string tenantId, string userId)
        {
            return _laborContractRepository.ExistAsync(x => x.TenantId == tenantId && x.IsUse && x.UserId == userId);
        }
    }
}
