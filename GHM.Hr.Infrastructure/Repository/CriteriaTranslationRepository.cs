﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Infrastructure.SqlServer;

namespace GHM.Hr.Infrastructure.Repository
{
    public class CriteriaTranslationRepository : RepositoryBase, ICriteriaTransaltionRepository
    {
        private readonly IRepository<CriteriaTranslation> _criteriaTranslationRepository;
        public CriteriaTranslationRepository(IDbContext context) : base(context)
        {
            _criteriaTranslationRepository = Context.GetRepository<CriteriaTranslation>();
        }

        public async Task<bool> CheckExists(string tenantId, string criteriaId, string languageId, string name)
        {
            return await _criteriaTranslationRepository.ExistAsync(x =>
                x.TenantId == tenantId && x.CriteriaId != criteriaId && x.LanguageId == languageId && x.Name == name);
        }

        public async Task<CriteriaTranslation> GetInfo(string tenantId, string criteriaId, string languageId, bool isReadOnly = false)
        {
            return await _criteriaTranslationRepository.GetAsync(isReadOnly, x =>
                x.TenantId == tenantId && x.CriteriaId == criteriaId && x.LanguageId == languageId);
        }

        public async Task<int> Insert(CriteriaTranslation criteriaTranslation)
        {
            _criteriaTranslationRepository.Create(criteriaTranslation);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(CriteriaTranslation criteriaTranslation)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeleteByCriteriaId(string tenantId, string id)
        {
            var criteriaTranslations = await _criteriaTranslationRepository.GetsAsync(false,
                x => x.TenantId == tenantId && x.CriteriaId == id && !x.IsDelete);
            if (criteriaTranslations == null || !criteriaTranslations.Any())
                return -1;

            foreach (var criteriaTranslation in criteriaTranslations)
            {
                criteriaTranslation.IsDelete = true;
            }

            return await Context.SaveChangesAsync();
        }

        public async Task<List<CriteriaTranslation>> GetsByCriteriaId(string tenantId, string criteriaId)
        {
            return await _criteriaTranslationRepository.GetsAsync(true,
                x => x.TenantId == tenantId && x.CriteriaId == criteriaId && !x.IsDelete);
        }
    }
}
