﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;

namespace GHM.Hr.Infrastructure.Repository
{
    public class PositionRepository : RepositoryBase, IPositionRepository
    {
        private readonly IRepository<Position> _positionRepository;

        public PositionRepository(IDbContext context) : base(context)
        {
            _positionRepository = Context.GetRepository<Position>();
        }

        public Task<List<PositionViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive,
            bool? isManager, bool? isMultiple, int page, int pageSize, out int totalRows)
        {
            Expression<Func<Position, bool>> spec = p => !p.IsDelete && p.TenantId == tenantId;
            Expression<Func<PositionTranslation, bool>> specTranslation = pt => pt.LanguageId == languageId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(pt => pt.UnsignName.Contains(keyword));
            }

            if (isActive.HasValue)
                spec = spec.And(x => x.IsActive == isActive.Value);

            if (isManager.HasValue)
                spec = spec.And(x => x.IsManager == isManager.Value);

            if (isMultiple.HasValue)
                spec = spec.And(x => x.IsMultiple == isMultiple.Value);

            var query = Context.Set<Position>().Where(spec)
                .Join(Context.Set<PositionTranslation>().Where(specTranslation), p => p.Id, pt => pt.PositionId,
                    (p, pt) => new PositionViewModel
                    {
                        Id = p.Id,
                        Name = pt.Name,
                        Description = pt.Description,
                        ShortName = pt.ShortName,
                        Order = p.Order,
                        CreateTime = p.CreateTime,
                        IsManager = p.IsManager,
                        IsMultiple = p.IsMultiple,
                        IsActive = p.IsActive
                    });

            totalRows = query.Count();
            return query
                .AsNoTracking()
                .OrderByDescending(x => x.Id)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<int> Insert(Position position)
        {
            _positionRepository.Create(position);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(Position position)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string id, string tenantId)
        {
            var info = await GetInfo(id, tenantId);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(string id)
        {
            var info = await GetInfo(id);
            if (info == null)
                return -1;

            _positionRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<Position> GetInfo(string id, bool isReadOnly = false)
        {
            return await _positionRepository.GetAsync(isReadOnly, x => !x.IsDelete && x.Id == id);
        }

        public async Task<Position> GetInfo(string id, string tenantId, bool isReadOnly = false)
        {
            return await _positionRepository.GetAsync(isReadOnly,
                x => x.TenantId == tenantId && x.Id == id && !x.IsDelete);
        }

        public async Task<bool> CheckExistsByTitleId(string tenantId, string titleId)
        {
            return await _positionRepository.ExistAsync(x => x.TitleId == titleId && !x.IsDelete && x.TenantId == tenantId);
        }      

        public async Task<List<Position>> GetAll()
        {
            return await _positionRepository.GetsAsync(true, x => !x.IsDelete);
        }

        public async Task<List<PositionSearchForSelectViewModel>> SearchForSelect(string tenantId, string languageId, string keyword)
        {
            Expression<Func<Position, bool>> spec = x => x.IsActive && !x.IsDelete && x.TenantId == tenantId;
            Expression<Func<PositionTranslation, bool>> specTranslation = x => x.LanguageId == languageId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            var query = Context.Set<Position>().Where(spec)
                .Join(Context.Set<PositionTranslation>().Where(specTranslation), t => t.Id, tt => tt.PositionId, (t, tt) =>
                    new PositionSearchForSelectViewModel
                    {
                        Id = t.Id,
                        Name = tt.Name
                    });
            return await query.ToListAsync();
        }
    }
}
