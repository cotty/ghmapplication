﻿using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GHM.Hr.Infrastructure.Repository
{
    public class UserContactRepositiory : RepositoryBase, IUserContactRepository
    {
        private readonly IRepository<UserContact> _userContactRepository;
        public UserContactRepositiory(IDbContext context) : base(context)
        {
            _userContactRepository = Context.GetRepository<UserContact>();
        }

        public async Task<int> Insert(UserContact userContact)
        {
            _userContactRepository.Create(userContact);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<UserContact> userContacts)
        {
            _userContactRepository.Creates(userContacts);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(UserContact userContact)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<UserContact> GetInfo(string tenantId, string id, bool isReadonly = false)
        {
            return await _userContactRepository.GetAsync(isReadonly, c => c.Id == id && c.TenantId == tenantId);
        }

        public Task<List<UserContactViewModel>> GetByUserId(string tenantId, string userId)
        {
            Expression<Func<UserContact, bool>> spec = x => x.UserId == userId && x.TenantId == tenantId;
            var query = Context.Set<UserContact>().AsNoTracking().Where(spec)
                        .Select(c => new UserContactViewModel()
                        {
                            Id = c.Id,
                            ContactType = c.ContactType,
                            ContactValue = c.ContactValue,
                            ConcurrencyStamp = c.ConcurrencyStamp,
                        });

            return query.ToListAsync();
        }

        public async Task<int> Delete(string tenantId, string id)
        {
            var info = await GetInfo(tenantId, id, false);
            if (info == null)
                return -1;

            _userContactRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeleteByUserdId(string tenantId, string userId)
        {
            var listUserContact = _userContactRepository.Gets(false, c => c.UserId == userId && c.TenantId == tenantId);
            _userContactRepository.Deletes(listUserContact);

            return await Context.SaveChangesAsync();
        }

        public async Task<bool> CheckExistsByValue(string tenantId, string id, string userId, ContactType contactType, string contactValue)
        {
            return await _userContactRepository.ExistAsync(c => c.Id != id && c.UserId == userId
            && c.ContactType == contactType && c.ContactValue == contactValue && c.TenantId == tenantId);
        }

        public async Task<UserContact> GetInfoByValue(string tenantId, string id, string userId, ContactType contactType, string contactValue, bool isReadOnly = false)
        {
            return await _userContactRepository.GetAsync(isReadOnly, c=> c.Id != id && c.UserId == userId
            && c.ContactType == contactType && c.ContactValue == contactValue && c.TenantId == tenantId);
        }
    }
}
