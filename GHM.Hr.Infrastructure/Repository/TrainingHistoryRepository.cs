﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;

namespace GHM.Hr.Infrastructure.Repository
{
    public class TrainingHistoryRepository : RepositoryBase, ITrainingHistoryRepository
    {
        private readonly IRepository<UserTrainingHistory> _trainingHistoryRepository;

        public TrainingHistoryRepository(IDbContext context) : base(context)
        {
            _trainingHistoryRepository = Context.GetRepository<UserTrainingHistory>();
        }

        public async Task<int> Insert(UserTrainingHistory training)
        {            
            _trainingHistoryRepository.Create(training);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(UserTrainingHistory training)
        {           
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string tenantId, string id)
        {
            var info = await GetInfo(tenantId, id);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<UserTrainingHistory> GetInfo(string tenantId, string id, bool isReadOnly = false)
        {
            return await _trainingHistoryRepository.GetAsync(isReadOnly, x => x.Id == id && !x.IsDelete && x.TenantId == tenantId);
        }

        public async Task<bool> CheckExists(string tenantId, string userId, DateTime fromDate, DateTime toDate, string courseId)
        {
            return await _trainingHistoryRepository.ExistAsync(x => x.UserId.Equals(userId) &&
            ((x.FromDate <= fromDate && x.ToDate >= fromDate) || x.FromDate <= toDate && x.ToDate >= toDate)
            && x.CourseId == courseId && !x.IsDelete && x.TenantId == tenantId);
        }

        public Task<List<UserTrainingHistory>> Search(string tenantId, string keyword,
            string userId, bool? type, string courseId, 
            string courseName, string coursePlaceId, string coursePlaceName,
            bool? isHasCertificate, DateTime? fromDate, DateTime? toDate, 
            int page, int pageSize, out int totalRows)
        {
            Expression<Func<UserTrainingHistory, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripVietnameseChars();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (!string.IsNullOrEmpty(userId))
            {
                spec = spec.And(x => x.UserId.Equals(userId));
            }

            if (type.HasValue)
            {
                spec = spec.And(x => x.Type == type.Value);
            }

            if (!string.IsNullOrEmpty(courseId))
            {
                spec = spec.And(x => x.CourseId == courseId);
            }

            if (!string.IsNullOrEmpty(courseName))
            {
                spec = spec.And(x => x.CourseName == courseName);
            }

            if (!string.IsNullOrEmpty(coursePlaceId))
            {
                spec = spec.And(x => x.CoursePlaceId == coursePlaceId);
            }
            if (!string.IsNullOrEmpty(coursePlaceName))
            {
                spec = spec.And(x => x.CoursePlaceName == coursePlaceName);
            }

            if (isHasCertificate.HasValue)
            {
                spec = spec.And(x => x.IsHasCertificate == isHasCertificate.Value);
            }

            if (fromDate.HasValue)
            {
                spec = spec.And(x => x.FromDate >= fromDate.Value);
            }

            if (toDate.HasValue)
            {
                spec = spec.And(x => x.ToDate <= toDate.Value);
            }

            var sort = Context.Filters.Sort<UserTrainingHistory, DateTime>(a => a.CreateTime, true);
            var paging = Context.Filters.Page<UserTrainingHistory>(page, pageSize);

            totalRows = _trainingHistoryRepository.Count(spec);
            return _trainingHistoryRepository.GetsAsync(true, spec, sort, paging);
        }

        public async Task<bool> CheckExist(string tenantId, string id)
        {
            return await _trainingHistoryRepository.ExistAsync(x => !x.IsDelete && x.TenantId == tenantId && x.Id == id);
        }

        public async Task<bool> CheckExist(string tenantId,string userId, string courseId, string courseName, string coursePlaceId, string coursePlaceName)
        {
            return await _trainingHistoryRepository.ExistAsync(x => !x.IsDelete && x.TenantId == tenantId && x.CourseId == courseId && x.CoursePlaceId == coursePlaceId && x.CoursePlaceName == coursePlaceName && x.UserId == userId);
        }
    }
}

