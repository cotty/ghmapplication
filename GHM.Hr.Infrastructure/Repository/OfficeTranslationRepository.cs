﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.SqlServer;

namespace GHM.Hr.Infrastructure.Repository
{
    public class OfficeTranslationRepository : RepositoryBase, IOfficeTranslationRepository
    {
        private readonly IRepository<OfficeTranslation> _officeTranslationRepository;
        public OfficeTranslationRepository(IDbContext context) : base(context)
        {
            _officeTranslationRepository = Context.GetRepository<OfficeTranslation>();
        }

        public async Task<int> Insert(OfficeTranslation officeTranslation)
        {
            _officeTranslationRepository.Create(officeTranslation);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(OfficeTranslation officeTranslation)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<OfficeTranslation> officeTranslations)
        {
            _officeTranslationRepository.Creates(officeTranslations);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDeleteByOfficeId(int officeId)
        {
            var officeTranslations = await _officeTranslationRepository.GetsAsync(false, x => x.OfficeId == officeId);
            if (officeTranslations == null || !officeTranslations.Any())
                return -1;

            _officeTranslationRepository.Deletes(officeTranslations);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> SaveChangeAsync()
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<bool> CheckExistsByShortName(int rootId, int officeId, string languageId, string shortName)
        {
            shortName = shortName.Trim();
            return await _officeTranslationRepository.ExistAsync(x =>
                x.RootId == rootId && x.OfficeId != officeId && x.LanguageId == languageId && x.ShortName == shortName);
        }

        public async Task<bool> CheckExistsByName(int rootId, int officeId, string languageId, string name)
        {
            name = name.Trim();
            return await _officeTranslationRepository.ExistAsync(x =>
                x.RootId == rootId && x.OfficeId != officeId && x.LanguageId == languageId && x.Name == name);
        }

        public async Task<OfficeTranslation> GetInfo(int officeId, string languageId, bool isReadOnly = false)
        {
            return await _officeTranslationRepository.GetAsync(isReadOnly, c => c.OfficeId == officeId && c.LanguageId == languageId);
        }

        public async Task<List<OfficeTranslationViewModel>> GetsByOfficeId(int officeId)
        {
            return await _officeTranslationRepository.GetsAsAsync(x => new OfficeTranslationViewModel
            {
                Name = x.Name,
                Description = x.Description,
                LanguageId = x.LanguageId,
                ShortName = x.ShortName,
                Address = x.Address,
                ParentName = x.ParentName
            }, x => x.OfficeId == officeId);
        }
    }
}
