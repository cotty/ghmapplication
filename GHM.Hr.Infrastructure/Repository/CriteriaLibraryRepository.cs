﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;

namespace GHM.Hr.Infrastructure.Repository
{
    public class CriteriaLibraryRepository : RepositoryBase, ICriteriaLibraryRepository
    {
        private readonly IRepository<Criteria> _criteriaLibraryRepository;
        private readonly IRepository<CriteriaGroup> _criteriaGroupRepository;
        //private readonly ICriteriaTitleRepository _criteriaTitleRepository;

        public CriteriaLibraryRepository(IDbContext context) : base(context)
        {
            //_criteriaTitleRepository = criteriaTitleRepository;
            _criteriaLibraryRepository = Context.GetRepository<Criteria>();
            _criteriaGroupRepository = Context.GetRepository<CriteriaGroup>();
        }

        #region Group

        public ICriteriaPositionRepository CriteriaTitleRepository { get; set; }

        public async Task<List<CriteriaGroup>> GetAllGroup()
        {
            return await _criteriaGroupRepository.GetsAsync(true, x => x.IsActive && !x.IsDelete);
        }

        #endregion

        public Task<List<Criteria>> Search(string keyword, string groupId, bool? isActive, int page, int pageSize, out int totalRows)
        {
            Expression<Func<Criteria, bool>> spec = x => !x.IsDelete;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripVietnameseChars();
                //                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (isActive.HasValue)
            {
                spec = spec.And(x => x.IsActive == isActive.Value);
            }

            //            if (groupId)
            //            {
            //                spec = spec.And(x => x.GroupId == groupId.Value);
            //            }

            //            var sort = Context.Filters.Sort<Criteria, int>(x => x.GroupId);
            var paging = Context.Filters.Page<Criteria>(page, pageSize);

            //            totalRows = _criteriaLibraryRepository.Count(spec);
            //            return _criteriaLibraryRepository.GetsAsync(true, spec, sort, paging);
            totalRows = 1;
            return null;
        }

        public async Task<int> Insert(CriteriaMeta criteriaMeta)
        {
            // Check name exists
//            var isNameExists = await CheckNameExists(criteriaMeta.Id, criteriaMeta.Name);
//            if (isNameExists)
//                return -2;
//
//            var groupInfo = await GetGroupInfo(criteriaMeta.GroupId, true, true);
//            if (groupInfo == null)
//                return -3;

            //            _criteriaLibraryRepository.Create(new Criteria
            //            {
            //                Name = criteriaMeta.Name,
            //                UnsignName = criteriaMeta.Name.StripVietnameseChars(),
            //                Description = criteriaMeta.Description,
            //                IsActive = criteriaMeta.IsActive,
            //                Note = criteriaMeta.Note,
            //                Point = criteriaMeta.Point,
            //                Sanction = criteriaMeta.Sanction,
            //                GroupId = groupInfo.Id,
            //                GroupName = groupInfo.Name
            //            });

            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(CriteriaMeta criteriaMeta)
        {
//            var info = await GetInfo(criteriaMeta.Id);
//            if (info == null)
//                return -1;
//
//            // Check name exists
//            var isNameExists = await CheckNameExists(criteriaMeta.Id, criteriaMeta.Name);
//            if (isNameExists)
//                return -2;

//            var oldGroupId = info.GroupId;
            //            var oldName = info.Name;

            //            if (oldGroupId != criteriaMeta.GroupId)
            //            {
            //                var groupInfo = await GetGroupInfo(criteriaMeta.GroupId, true, true);
            //                if (groupInfo == null)
            //                    return -3;
            //
            //                info.GroupId = groupInfo.Id;
            //                info.GroupName = groupInfo.Name;
            //            }
            //
            //            info.Name = criteriaMeta.Name;
            //            info.UnsignName = criteriaMeta.Name.StripVietnameseChars();
            //            info.Description = criteriaMeta.Description;
            //            info.IsActive = criteriaMeta.IsActive;
            //            info.Note = criteriaMeta.Note;
            //            info.Point = criteriaMeta.Point;
            //            info.Sanction = criteriaMeta.Sanction;
            //
            //            var result = await Context.SaveChangesAsync(); ;
            //
            //            if (result > 0 && !criteriaMeta.Name.ToLower().Equals(oldName.ToLower()))
            //            {
            //                // Update lại tên cho những bản ghi cấu hình
            //                //var listCriteia = await _criteriaTitle
            //                //await Context.Set<CriteriasPositions>().Where(x => x.CriteriaId == info.Id)
            //                //    .UpdateAsync(x => new CriteriasPositions
            //                //    {
            //                //        Name = info.Name
            //                //    });
            //                await CriteriaTitleRepository.UpdateNameByCriteriaId(info.Id, info.Name);
            //            }

            return 1;
        }

        public async Task<int> UpdateIsActive(string id, bool isActive)
        {
            var info = await GetInfo(id);
            if (info == null)
                return -1;

            info.IsActive = isActive;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string id)
        {
            // không cho phép xóa trong trường hợp đang có người đánh giá và chưa hoàn thành.
            var info = await GetInfo(id);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();

        }

        public async Task<Criteria> GetInfo(string id, bool isReadOnly = false)
        {
            //            return await _criteriaLibraryRepository.GetAsync(isReadOnly, x => x.Id == id && !x.IsDelete);
            return null;
        }

        public async Task<bool> CheckNameExists(string id, string name)
        {
            //return await _criteriaLibraryRepository.ExistAsync(x => x.Id != id && x.Name.ToLower().Equals(name.ToLower()) && !x.IsDelete);
            return false;
        }

        public async Task<bool> CheckGroupExits(string id)
        {
            //            return await _criteriaGroupRepository.ExistAsync(x => x.Id == id && !x.IsDelete);
            return false;
        }

        public async Task<CriteriaGroup> GetGroupInfo(string id, bool? isActive, bool isReadOnly = false)
        {
            //            Expression<Func<CriteriaGroup, bool>> spec = x => !x.IsDelete && x.Id == id;
            //            if (isActive.HasValue)
            //                spec = spec.And(x => x.IsActive == isActive.Value);
            //
            //            return await _criteriaGroupRepository.GetAsync(isReadOnly, spec);
            return null;
        }
    }
}
