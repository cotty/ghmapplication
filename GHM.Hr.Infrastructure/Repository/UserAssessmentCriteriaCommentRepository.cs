﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Infrastructure.SqlServer;

namespace GHM.Hr.Infrastructure.Repository
{
    public class UserAssessmentCriteriaCommentRepository : RepositoryBase, IUserAssessmentCriteriaCommentRepository
    {
        private readonly IRepository<UserAssessmentCriteriaComment> _userAssessmentCriteriaCommentRepository;
        public UserAssessmentCriteriaCommentRepository(IDbContext context) : base(context)
        {
            _userAssessmentCriteriaCommentRepository = Context.GetRepository<UserAssessmentCriteriaComment>();
        }

        public async Task<int> Insert(UserAssessmentCriteriaComment userAssessmentCriteriaComment)
        {
            _userAssessmentCriteriaCommentRepository.Create(userAssessmentCriteriaComment);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(UserAssessmentCriteriaComment userAssessmentCriteriaComment)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string id)
        {
            var info = await GetInfo(id);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<UserAssessmentCriteriaComment> GetInfo(string id, bool isReadOnly = false)
        {
            return await _userAssessmentCriteriaCommentRepository.GetAsync(isReadOnly, x => x.Id == id && !x.IsDelete);
        }

        public Task<List<UserAssessmentCriteriaComment>> Search(string userAssessmentCriteriaId, int page, int pageSize, out int totalRows)
        {
            Expression<Func<UserAssessmentCriteriaComment, bool>> spec = x =>
                x.UserAssessmentCriteriaId == userAssessmentCriteriaId && !x.IsDelete;

            var sort = Context.Filters.Sort<UserAssessmentCriteriaComment, DateTime>(x => x.CreateTime);
            var paging = Context.Filters.Page<UserAssessmentCriteriaComment>(page, pageSize);

            totalRows = _userAssessmentCriteriaCommentRepository.Count(spec);
            return _userAssessmentCriteriaCommentRepository.GetsAsync(true, spec, sort, paging);
        }
    }
}
