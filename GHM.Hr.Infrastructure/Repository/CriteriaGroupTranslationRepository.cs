﻿using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.SqlServer;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GHM.Hr.Infrastructure.Repository
{
    public class CriteriaGroupTranslationRepository : RepositoryBase, ICriteriaGroupTranslationRepository
    {
        private readonly IRepository<CriteriaGroupTranslation> _criteriaGroupTranslationRepository;

        public CriteriaGroupTranslationRepository(IDbContext context) : base(context)
        {
            _criteriaGroupTranslationRepository = Context.GetRepository<CriteriaGroupTranslation>();
        }

        public async Task<int> Insert(CriteriaGroupTranslation criteriaGroupTranslation)
        {
            _criteriaGroupTranslationRepository.Create(criteriaGroupTranslation);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<CriteriaGroupTranslation> criteriaGroupTranslations)
        {
            _criteriaGroupTranslationRepository.Creates(criteriaGroupTranslations);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(CriteriaGroupTranslation criteriaGroupTranslation)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<List<CriteriaGroupTranslation>> GetsAllByCriteriaGroupId(string tenantId, string criteriaGroupId, bool isReadOnly = false)
        {
            return await _criteriaGroupTranslationRepository.GetsAsync(isReadOnly,
                x => x.TenantId == tenantId && x.CriteriaGroupId == criteriaGroupId && !x.IsDelete);
        }

        public async Task<bool> CheckExists(string criteriaGroupId, string languageId, string name)
        {
            name = name.Trim();
            return await _criteriaGroupTranslationRepository.ExistAsync(c => c.CriteriaGroupId.Equals(criteriaGroupId) && c.LanguageId.Equals(languageId) && c.Name.Equals(name));
        }

        public async Task<int> Updates(List<CriteriaGroupTranslation> criteriaGroupTranslations)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeleteByCriteriaGroupId(string tenantId, string criteriaGroupId)
        {
            var translations = await _criteriaGroupTranslationRepository.GetsAsync(false,
                x => x.TenantId == tenantId && x.CriteriaGroupId == criteriaGroupId);
            if (translations == null || !translations.Any())
                return -1;

            foreach (var criteriaGroupTranslation in translations)
            {
                criteriaGroupTranslation.IsDelete = true;
            }

            return await Context.SaveChangesAsync();
        }

        public async Task<bool> CheckIdExists(string tenantId, string id, string name)
        {
            return await _criteriaGroupTranslationRepository.ExistAsync(x =>
                x.TenantId == tenantId && x.CriteriaGroupId != id && x.Name == name && !x.IsDelete);
        }

        public async Task<int> Delete(string tenantId, string criteriaGroupId, string languageId)
        {
            var info = await GetInfo(tenantId, criteriaGroupId, languageId);
            if (info == null)
                return -1;

            _criteriaGroupTranslationRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<bool> CheckExists(string tenantId, string id, string languageId, string name)
        {
            return await _criteriaGroupTranslationRepository.ExistAsync(x =>
                x.TenantId == tenantId && x.CriteriaGroupId != id && x.LanguageId == languageId
                && x.Name == name && !x.IsDelete);
        }

        //public async Task<int> DeleteByCriteriaGroupTranslation(string criteriaGroupId)
        //{

        //}

        public async Task<CriteriaGroupTranslation> GetInfo(string tenantId, string criteriaGroupId, string languageId, bool isReadOnly = false)
        {
            return await _criteriaGroupTranslationRepository.GetAsync(isReadOnly, x =>
                x.TenantId == tenantId && x.LanguageId == languageId
                                                                                       && x.CriteriaGroupId == criteriaGroupId);
        }
    }
}
