﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;

namespace GHM.Hr.Infrastructure.Repository
{
    public class CriteriaRepository : RepositoryBase, ICriteriaRepository
    {
        private readonly IRepository<Criteria> _criteriaRepository;

        public CriteriaRepository(IDbContext context) : base(context)
        {
            _criteriaRepository = Context.GetRepository<Criteria>();
        }

        public async Task<int> Insert(Criteria criteria)
        {
            _criteriaRepository.Create(criteria);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(Criteria criteria)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string tenantId, string id)
        {
            var criteria = await GetInfo(tenantId, id);
            if (criteria == null)
                return -1;

            criteria.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<Criteria> GetInfo(string tenantId, string id, bool isReadOnly = false)
        {
            return await _criteriaRepository.GetAsync(isReadOnly, x => x.TenantId == tenantId && x.Id == id);
        }

        public async Task<bool> CheckExistsByGroupId(string tenantId, string groupId)
        {
            return await _criteriaRepository.ExistAsync(x =>
                x.TenantId == tenantId && x.GroupId == groupId && !x.IsDelete);
        }

        public Task<List<CriteriaViewModel>> Search(string tenantId, string languageId, string keyword, string groupId, bool? isActive, int page,
            int pageSize, out int totalRows)
        {
            keyword = !string.IsNullOrEmpty(keyword) ? keyword.Trim().StripVietnameseChars().ToUpper() : string.Empty;
            var query = from c in Context.Set<Criteria>()
                        join ct in Context.Set<CriteriaTranslation>() on c.Id equals ct.CriteriaId
                        join cg in Context.Set<CriteriaGroup>() on c.GroupId equals cg.Id
                        join cgt in Context.Set<CriteriaGroupTranslation>() on cg.Id equals cgt.CriteriaGroupId
                        where c.TenantId == tenantId && ct.LanguageId == languageId
                              && (string.IsNullOrEmpty(keyword) || ct.UnsignName.Contains(keyword))
                              && (string.IsNullOrEmpty(groupId) || c.GroupId == groupId)
                              && (!isActive.HasValue || c.IsActive == isActive)
                                                     && !c.IsDelete && !ct.IsDelete
                        select new CriteriaViewModel
                        {
                            Id = c.Id,
                            Name = ct.Name,
                            IsActive = c.IsActive,
                            Point = c.Point,
                            Description = ct.Description,
                            Sanction = ct.Sanction,
                            GroupId = cg.Id,
                            GroupName = cgt.Name,
                            Order = cg.Order
                        };

            totalRows = query.Count();
            return query
                .OrderBy(x => x.Order)
                .Skip(pageSize * (page - 1))
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }
    }
}
