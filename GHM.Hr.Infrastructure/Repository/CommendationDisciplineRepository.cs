﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;

namespace GHM.Hr.Infrastructure.Repository
{
    public class CommendationDisciplineRepository : RepositoryBase, ICommendationDisciplineRepository
    {
        private readonly IRepository<UserCommendationDiscipline> _commendationDisciplineRepository;
        private readonly IUserValueRepository _userValueRepository;
        private readonly IUserRepository _userRepository;
        //private readonly IUserTitleRepository _userTitleRepository;
        private readonly IRepository<UserCommendationDisciplineCategory> _commendationDisciplineCategoryRepository;

        public CommendationDisciplineRepository(IDbContext context, IUserValueRepository userValueRepository, IUserRepository userRepository) : base(context)
        {
            _userValueRepository = userValueRepository;
            _userRepository = userRepository;
            //_userTitleRepository = userTitleRepository;
            _commendationDisciplineRepository = Context.GetRepository<UserCommendationDiscipline>();
            _commendationDisciplineCategoryRepository = Context.GetRepository<UserCommendationDisciplineCategory>();
        }

        public async Task<int> Insert(CommendationDisciplineMeta commendationDisciplineMeta)
        {
            //            var decisionExists = await CheckExistsByDecisionNo(commendationDisciplineMeta.Id, commendationDisciplineMeta.UserId, commendationDisciplineMeta.DecisionNo);
            //            if (decisionExists)
            //                return -1;
            //
            //            var categoryInfo = await GetCategoryInfo(commendationDisciplineMeta.CategoryId);
            //            if (categoryInfo == null)
            //                return -2;
            //
            //            var userInfo = await _userRepository.GetInfo(commendationDisciplineMeta.UserId);
            //            if (userInfo == null)
            //                return -3;
            //
            //            var userTitle = await _userTitleRepository.GetDefaultInfo(commendationDisciplineMeta.UserId, true, true);
            //            if (userTitle == null)
            //                return -4;
            //
            //            var commendationDiscipline = new UserCommendationDiscipline
            //            {
            //                AttachmentUrl = commendationDisciplineMeta.AttachmentUrl,
            //                DecisionNo = commendationDisciplineMeta.DecisionNo,
            //                CreatorId = commendationDisciplineMeta.CreatorId,
            //                CreatorFullName = commendationDisciplineMeta.CreatorFullName,
            //                CategoryId = categoryInfo.Id,
            //                CategoryName = categoryInfo.Name,
            //                FullName = userInfo.FullName,
            //                UserId = userInfo.Id,
            //                PositionId = userTitle.PositionId,
            //                TitleName = userTitle.TitleName,
            //                OfficeId = userTitle.OfficeId,
            //                OfficeName = userTitle.OfficeName,
            //                Month = (byte)commendationDisciplineMeta.Time.Month,
            //                Quarter = (byte)commendationDisciplineMeta.Time.GetQuarter(),
            //                Year = commendationDisciplineMeta.Time.Year,
            //                Time = commendationDisciplineMeta.Time,
            //                Type = commendationDisciplineMeta.Type,
            //                UnsignName =
            //                    $"{commendationDisciplineMeta.FullName.StripVietnameseChars()} {categoryInfo.UnsignName} {userTitle.OfficeName.StripVietnameseChars()} {userTitle.TitleName.StripVietnameseChars()} {commendationDisciplineMeta.DecisionNo}",
            //                Reason = commendationDisciplineMeta.Reason,
            //                Money = commendationDisciplineMeta.Money,
            //            };
            //            _commendationDisciplineRepository.Create(commendationDiscipline);
            //            await Context.SaveChangesAsync();
            //            return commendationDiscipline.Id;
            return 1;
        }

        public async Task<int> Update(CommendationDisciplineMeta commendationDisciplineMeta)
        {
            //            var info = await GetInfo(commendationDisciplineMeta.Id);
            //            if (info == null)
            //                return -5;
            //
            //            var decisionExists = await CheckExistsByDecisionNo(commendationDisciplineMeta.Id, commendationDisciplineMeta.UserId, commendationDisciplineMeta.DecisionNo);
            //            if (decisionExists)
            //                return -1;
            //
            //            var oldCategory = info.CategoryId;
            //
            //            if (oldCategory != commendationDisciplineMeta.CategoryId)
            //            {
            //                var categoryInfo = await GetCategoryInfo(commendationDisciplineMeta.CategoryId);
            //                if (categoryInfo == null)
            //                    return -2;
            //
            //                info.CategoryId = categoryInfo.Id;
            //                info.CategoryName = categoryInfo.Name;
            //            }
            //
            //            info.AttachmentUrl = commendationDisciplineMeta.AttachmentUrl;
            //            info.DecisionNo = commendationDisciplineMeta.DecisionNo;
            //            info.Month = (byte)commendationDisciplineMeta.Time.Month;
            //            info.Quarter = (byte)commendationDisciplineMeta.Time.GetQuarter();
            //            info.Year = commendationDisciplineMeta.Time.Year;
            //            info.Time = commendationDisciplineMeta.Time;
            //            info.Type = commendationDisciplineMeta.Type;
            //            info.UnsignName =
            //                $"{info.FullName.StripVietnameseChars()} {info.CategoryName.StripVietnameseChars()} {info.OfficeName.StripVietnameseChars()} {info.TitleName.StripVietnameseChars()} {commendationDisciplineMeta.DecisionNo}";
            //            info.Reason = commendationDisciplineMeta.Reason;
            //            info.Money = commendationDisciplineMeta.Money;
            //
            //            return await Context.SaveChangesAsync();
            return 1;
        }

        public async Task<int> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<int> Delete(string id)
        {
            var info = await GetInfo(id);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<bool> CheckExistsByDecisionNo(string id, string userId, string decisionNo)
        {
            return await _commendationDisciplineRepository.ExistAsync(x => x.Id != id && x.UserId.Equals(userId) && x.DecisionNo.Equals(decisionNo));
        }

        public async Task<UserCommendationDiscipline> GetInfo(string id, bool isReadOnly = false)
        {
//            return await _commendationDisciplineRepository.GetAsync(isReadOnly, x => x.Id == id && !x.IsDelete);
            return null;
        }

        public Task<List<UserCommendationDiscipline>> Search(string keyword, string userId, bool? type, string categoryId, DateTime? fromDate, DateTime? toDate,
            int? officeId, string titleId, int page, int pageSize, out int totalRows)
        {
            throw new NotImplementedException();
        }

        public Task<List<UserCommendationDiscipline>> Search(string keyword, string userId, bool? type, int? categoryId, DateTime? fromDate, DateTime? toDate, int? officeId, int? titleId, int page, int pageSize,
            out int totalRows)
        {
                       Expression<Func<UserCommendationDiscipline, bool>> spec = p => !p.IsDelete;
            //
            //            if (!string.IsNullOrEmpty(keyword))
            //            {
            //                keyword = keyword.StripVietnameseChars();
              //              spec = spec.And(x => x.UnsignName.Contains(keyword));
            //            }
            //
            //            if (!string.IsNullOrEmpty(userId))
            //            {
            //                spec = spec.And(x => x.UserId.Equals(userId));
            //            }
            //
            //            if (type.HasValue)
            //            {
            //                spec = spec.And(x => x.Type == type.Value);
            //            }
            //
            //            if (categoryId.HasValue)
            //            {
            //                spec = spec.And(x => x.CategoryId == categoryId.Value);
            //            }
            //
            //            if (officeId.HasValue)
            //            {
            //                spec = spec.And(x => x.OfficeId == officeId.Value);
            //            }
            //
            //            if (titleId.HasValue)
            //            {
            //                spec = spec.And(x => x.PositionId == titleId.Value);
            //            }
            //
            //            if (fromDate.HasValue)
            //                spec = spec.And(x => x.Time >= fromDate.Value);
            //
            //            if (toDate.HasValue)
            //                spec = spec.And(x => x.Time <= toDate.Value);
            //
             //          var sort = Context.Filters.Sort<UserCommendationDiscipline, int>(x => x.Id, true);
            //            var paging = Context.Filters.Page<UserCommendationDiscipline>(page, pageSize);
            //
            //            totalRows = _commendationDisciplineRepository.Count(spec);
            //            return _commendationDisciplineRepository.GetsAsync(true, spec, sort, paging);
            totalRows = 1;
            return null;
        }

        #region Category
        public async Task<List<UserCommendationDisciplineCategory>> GetAllCategoryByType(CommendationDisciplineType? type)
        {
            Expression<Func<UserCommendationDisciplineCategory, bool>> spec = x => true;
            if (type.HasValue)
                spec = spec.And(x => x.Type == type);

            return await _commendationDisciplineCategoryRepository.GetsAsync(true, spec);
        }

        public async Task<UserCommendationDisciplineCategory> GetCategoryInfo(string id)
        {
            //            return await _commendationDisciplineCategoryRepository.GetAsync(true, x => x.Id == id);
            return null;
        }

        public Task<int> Delete(UserCommendationDiscipline info)
        {
            throw new NotImplementedException();
        }

        public Task<UserCommendationDiscipline> GetInfo(string tenantId, string id, bool isReadonly = false)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
