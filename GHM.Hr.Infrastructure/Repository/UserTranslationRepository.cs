﻿using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.SqlServer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GHM.Hr.Infrastructure.Repository
{
    public class UserTranslationRepository : RepositoryBase, IUserTranslationRepository
    {
        private readonly IRepository<UserTranslation> _userTranslationRepository;

        public UserTranslationRepository(IDbContext context) : base(context)
        {
            _userTranslationRepository = Context.GetRepository<UserTranslation>();
        }

        public async Task<int> Save(UserTranslation userTranslation)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Insert(UserTranslation userTranslation)
        {
            _userTranslationRepository.Create(userTranslation);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<UserTranslation> userTranslations)
        {
            _userTranslationRepository.Creates(userTranslations);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string userId, string languageId)
        {
            var info = await GetInfo(userId, languageId, false);
            if (info == null)
                return -1;

            _userTranslationRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeleteByUserId(string userId)
        {
            var listUserTranslation = _userTranslationRepository.Gets(false, c => c.UserId == userId);
            _userTranslationRepository.Deletes(listUserTranslation);

            return await Context.SaveChangesAsync();
        }

        public Task<List<UserTranslationViewModel>> GetByUserId(string userId)
        {
            Expression<Func<UserTranslation, bool>> spec = x => x.UserId == userId;
            var query = Context.Set<UserTranslation>().AsNoTracking().Where(spec)
                        .Select(c => new UserTranslationViewModel()
                        {
                            UserId = c.UserId,
                            LanguageId = c.LanguageId,
                            OfficeName = c.OfficeName,
                            PositionName = c.PositionName,
                            TitleName = c.TitleName,
                            Nationality = c.Nationality,
                            NativeCountry = c.NativeCountry,
                            ResidentRegister = c.ResidentRegister,
                            Address = c.Address,
                            PassportPlaceOfIssue = c.PassportPlaceOfIssue,
                            BankName = c.BankName,
                            BranchBank = c.BranchBank,
                            IdCardPlaceOfIssue = c.IdCardPlaceOfIssue,
                        });

            return query.ToListAsync();
        }

        public async Task<UserTranslation> GetInfo(string userId, string languageId, bool isReadOnly = false)
        {
            return await _userTranslationRepository.GetAsync(isReadOnly, c => c.UserId == userId && c.LanguageId == languageId);
        }

        public async Task<int> UpdatePositionNameByPositionId(string tenantId, string positionId, string positionName, string languageId)
        {
            var listUserTranslationByTitleId = Context.Set<User>().Where(c => c.PositionId == positionId && c.TenantId == tenantId && !c.IsDelete)
                                               .Join(Context.Set<UserTranslation>().Where(c => c.LanguageId == languageId), u => u.Id, ut => ut.UserId, (u, ut) => new { u, ut })
                                               .Select(x => x.ut).ToList();

            if (listUserTranslationByTitleId.Any() && listUserTranslationByTitleId != null)
            {
                foreach (var item in listUserTranslationByTitleId)
                {
                    item.PositionName = positionName;
                }
                return await Context.SaveChangesAsync();
            }

            return -1;
        }

        public async Task<int> UpdateOfficeNameByOfficeId(string tenantId, int officeId, string officeName, string languageId)
        {
            var listUserByOfficeId = Context.Set<User>().Where(c => c.OfficeId == officeId && c.TenantId == tenantId && !c.IsDelete)
                                               .Join(Context.Set<UserTranslation>().Where(c => c.LanguageId == languageId), u => u.Id, ut => ut.UserId, (u, ut) => new { u, ut })
                                               .Select(x => x.ut).ToList();

            if (listUserByOfficeId.Any() && listUserByOfficeId != null)
            {
                foreach (var item in listUserByOfficeId)
                {
                    item.OfficeName = officeName;
                }

                return await Context.SaveChangesAsync();
            }

            return 0;
        }
    }
}
