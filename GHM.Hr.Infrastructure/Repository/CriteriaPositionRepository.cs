﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Hr.Infrastructure.Configurations;
using GHM.Infrastructure.SqlServer;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace GHM.Hr.Infrastructure.Repository
{
    public class CriteriaPositionRepository : RepositoryBase, ICriteriaPositionRepository
    {
        private readonly IRepository<CriteriaPosition> _criteriaPositionRepository;
        private readonly ICriteriaLibraryRepository _criteriaLibraryRepository;

        public CriteriaPositionRepository(IDbContext context, ICriteriaLibraryRepository criteriaLibraryRepository) : base(context)
        {
            _criteriaLibraryRepository = criteriaLibraryRepository;
            _criteriaPositionRepository = Context.GetRepository<CriteriaPosition>();
        }

        public IUserAssessmentRepository UserAssessmentRepository { get; set; }

        public async Task<List<CriteriaPositionViewModel>> Search(string tenantId, string languageId, string positionId, bool? isActive)
        {
            var query = from c in Context.Set<Criteria>()
                        join cc in Context.Set<CriteriaPosition>() on c.Id equals cc.CriteriaId
                        join ct in Context.Set<CriteriaTranslation>() on c.Id equals ct.CriteriaId
                        join cgt in Context.Set<CriteriaGroupTranslation>() on c.GroupId equals cgt.CriteriaGroupId
                        where c.TenantId == tenantId && (!isActive.HasValue || cc.IsActive == isActive.Value)
                                                     && cc.PositionId == positionId
                                                     && ct.LanguageId == languageId && cgt.LanguageId == languageId
                        select new CriteriaPositionViewModel
                        {
                            CriteriaId = c.Id,
                            CriteriaName = ct.Name,
                            Sanction = ct.Sanction,
                            Description = ct.Description,
                            IsActive = cc.IsActive,
                            Point = c.Point,
                            PositionId = cc.PositionId,
                            GroupId = c.GroupId,
                            GroupName = cgt.Name
                        };

            return await query.AsNoTracking().ToListAsync();
        }

        public async Task<int> Insert(CriteriaPosition criteriaPosition)
        {
            _criteriaPositionRepository.Create(criteriaPosition);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateIsActive(string criteriaId, string positionId, bool isActive)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Delete(string tenantId, string criteriaId, string positionId)
        {
            var info = await GetInfo(tenantId, criteriaId, positionId);
            if (info == null)
                return -1;

            _criteriaPositionRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<CriteriaPosition> GetInfo(string tenantId, string criteriaId, string positionId, bool isReadOnly = false)
        {
            return await _criteriaPositionRepository.GetAsync(isReadOnly,
                x => x.TenantId == tenantId && x.CriteriaId == criteriaId && x.PositionId == positionId);
        }

        public async Task<bool> CheckExists(string tenantId, string criteriaId, string positionId)
        {
            return await _criteriaPositionRepository.ExistAsync(x =>
                x.TenantId == tenantId && x.PositionId == positionId && x.CriteriaId == criteriaId);
        }

        public async Task<List<CriteriaPosition>> GetAllCriteriaByPositionId(string positionId)
        {
            return await _criteriaPositionRepository.GetsAsync(true, x => x.IsActive && x.PositionId == positionId);
        }

        public async Task<int> UpdateNameByCriteriaId(string criteriaId, string name)
        {
            var listCriteria = await _criteriaPositionRepository.GetsAsync(false, x => x.CriteriaId == criteriaId);
            if (listCriteria == null || !listCriteria.Any()) return -1;
            //foreach (var criteria in listCriteria)
            //{
            //    criteria.Name = name;
            //}
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateActiveStatus(string tenantId, string criteriaId, string positionId, bool isActive)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<List<CriteriaPositionViewModel>> GetsByPositionId(string tenantId, string positionId, bool isReadOnly = false)
        {
            var query = from cp in Context.Set<CriteriaPosition>()
                        join c in Context.Set<Criteria>() on cp.CriteriaId equals c.Id
                        where cp.TenantId == tenantId && cp.PositionId == positionId
                        select new CriteriaPositionViewModel
                        {
                            CriteriaId = cp.CriteriaId,
                            PositionId = cp.PositionId,
                            Point = c.Point
                        };

            return isReadOnly
                ? await query.AsNoTracking().ToListAsync()
                : await query.ToListAsync();
        }

        //private async Task<int> InsertUserAssessmentCriteria(CriteriasPositions criteriaTitle)
        //{
        //    // Insert into list unfinished
        //    // Get list userassessment by positionId
        //    var query = Context.Set<UserAssessment>().Where(x => x.PositionId == criteriaTitle.PositionId && (x.Status == (byte)AssessmentStatus.New || x.Status == (byte)AssessmentStatus.ManagerDecline));
        //    var listUserAssessmentCriteria = new List<UserAssessmentCriteria>();
        //    foreach (var item in query)
        //    {
        //        listUserAssessmentCriteria.Add(new UserAssessmentCriteria
        //        {
        //            CriteriaId = criteriaTitle.CriteriaId,
        //            CriteriaName = criteriaTitle.Name,
        //            CriteriaDescription = criteriaTitle.Description,
        //            CriteriaNote = criteriaTitle.Note,
        //            CriteriaSanction = criteriaTitle.Sanction,
        //            Point = criteriaTitle.Point,
        //            CriteriaGroupId = criteriaTitle.GroupId,
        //            CriteriaGroupName = criteriaTitle.GroupName,
        //            UserAssessmentId = item.Id,
        //            UserId = item.UserId
        //        });
        //    }
        //    return await _userAssessmentCriteriaRepository.Insert(listUserAssessmentCriteria);
        //}

        //private async Task<int> UpdateTotalStandardPointBypositionId(int positionId)
        //{
        //    var query = Context.Set<UserAssessment>().Where(x => x.PositionId == positionId && (x.Status == (byte)AssessmentStatus.New || x.Status == (byte)AssessmentStatus.ManagerDecline))
        //        .Join(Context.Set<UserAssessmentCriteria>(), ua => ua.Id, uac => uac.UserAssessmentId, (ua, uac) => new { ua, uac });

        //    foreach (var item in query)
        //    {
        //        var totalPoint = await _userAssessmentCriteriaRepository.GetTotalPoint(item.uac.UserAssessmentId, (byte)AssessmentType.StandardPoint);
        //        await UserAssessmentRepository.UpdateTotalPoint(item.uac.UserAssessmentId, totalPoint);
        //    }

        //    return await Context.SaveChangesAsync();
        //}
    }


}
