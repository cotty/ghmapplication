﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Hr.Api.Infrastructure.Models.ViewModels;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using GHM.Infrastructure.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
 
namespace GHM.Hr.Infrastructure.Repository
{
    public class UserRepository : RepositoryBase, IUserRepository
    {
        private readonly IRepository<User> _userRepository;
        private readonly IOfficeRepository _officeRepository;

        public UserRepository(IDbContext context, IOfficeRepository officeRepository) : base(context)
        {
            _userRepository = Context.GetRepository<User>();
            _officeRepository = officeRepository;
        }

        public async Task<User> GetInfo(string id, bool isReadonly = false)
        {
            return await _userRepository.GetAsync(isReadonly, x => !x.IsDelete && x.Id.Equals(id));
        }

        public async Task<User> GetInfo(string tenantId, string id, bool isReadonly = false)
        {
            return await _userRepository.GetAsync(isReadonly,
                x => x.TenantId == tenantId && x.Id == id && !x.IsDelete);
        }

        public async Task<User> GetInfo(string tenantId, int enrollNumber, bool isReadonly = false)
        {
            return await _userRepository.GetAsync(isReadonly,
                x => x.EnrollNumber == enrollNumber && !x.IsDelete && x.TenantId == tenantId);
        }

        public async Task<User> GetInfoByUserName(string tenantId, string userId, string userName, bool isReadOnly = false)
        {
            return await _userRepository.GetAsync(isReadOnly,
                x => !x.IsDelete && x.Id != userId && x.UserName == userName && x.TenantId == tenantId);
        }

        public async Task<User> GetInfoByIdCardNumber(string tenantId, string userId, string idCardNumber, bool isReadOnly = false)
        {
            return await _userRepository.GetAsync(isReadOnly, x => x.Id != userId && x.IdCardNumber.Equals(idCardNumber) && x.TenantId == tenantId);
        }

        public int GetMaxEnrollNumbeValue(string tenantId)
        {
            return Context.Set<User>().Where(x => x.TenantId == tenantId).Max(x => x.EnrollNumber.Value);
        }

        public async Task<User> GetInfoByUserName(string userName, bool isReadonly = false)
        {
            return await _userRepository.GetAsync(isReadonly, x => !x.IsDelete && x.UserName.Equals(userName));
        }

        public async Task<int> Insert(User user)
        {
            _userRepository.Create(user);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(User user)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateAvatar(string tenantId, string userId, string avatar)
        {
            var userInfo = await GetInfo(tenantId, userId);
            if (userInfo == null)
                return -1;

            userInfo.Avatar = avatar;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateStatus(string tenantId, string id, UserStatus status)
        {
            var info = await GetInfo(tenantId, id);
            if (info == null)
                return -1;

            info.Status = status;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateDirectManager(string tenantId, string id, string managerId)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateApproveManager(string tenantId, string id, string approverId)
        {
            return await Context.SaveChangesAsync();
        }

        public Task<List<UserSearchViewModel>> Search(string tenantId, string languageId, string keyword, UserStatus? status, string officeIdPath,
            string positionId, Gender? gender, byte? monthBirthDay, int? yearBirthday, AcademicRank? academicRank,
            int page, int pageSize, out int totalRows)
        {
            Expression<Func<User, bool>> spec = x => x.TenantId == tenantId && !x.IsDelete;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (status.HasValue)
            {
                spec = spec.And(x => x.Status == status.Value);
            }

            if (!string.IsNullOrEmpty(officeIdPath))
            {
                officeIdPath = officeIdPath.Trim();
                spec = spec.And(x => x.OfficeIdPath.StartsWith(officeIdPath + ".") || x.OfficeIdPath.Equals(officeIdPath));
            }

            if (!string.IsNullOrEmpty(positionId))
            {
                spec = spec.And(x => x.PositionId == positionId);
            }

            if (gender.HasValue)
            {
                spec = spec.And(x => x.Gender == gender.Value);
            }

            if (monthBirthDay.HasValue)
            {
                spec = spec.And(x => x.Birthday.HasValue && x.Birthday.Value.Month == monthBirthDay.Value);
            }

            if (yearBirthday.HasValue)
            {
                spec = spec.And(x => x.Birthday.HasValue && x.Birthday.Value.Year == yearBirthday.Value);
            }

            if (academicRank.HasValue)
            {
                spec = spec.And(x => x.AcademicRank == academicRank.Value);
            }

            var query = Context.Set<User>().AsNoTracking().Where(spec)
                       .Join(Context.Set<UserTranslation>().Where(c => c.LanguageId == languageId), u => u.Id, ut => ut.UserId, (u, ut) => new
                       {
                           u.Id,
                           u.FullName,
                           u.UserName,
                           u.Status,
                           u.OfficeId,
                           ut.OfficeName,
                           u.OfficeIdPath,
                           u.PositionId,
                           ut.PositionName,
                           u.TitleId,
                           ut.TitleName,
                           u.Gender,
                           u.JoinedDate,
                           u.Avatar,
                           u.ApproverFullName,
                           u.ManagerFullName,
                           u.MarriedStatus,
                           u.ProvinceName,
                           u.DistrictName,
                           u.Tin,
                           u.DefaultHolidayQuantity,
                           u.IdCardNumber,
                           u.Birthday
                       });

            totalRows = query.Count();
            query = query.OrderBy(c => c.UserName).Skip((page - 1) * pageSize).Take(pageSize);
            var queryResult = query
                          .GroupJoin(Context.Set<UserContact>().AsNoTracking().Where(c => c.ContactType == ContactType.MobilePhone),
                          c => c.Id, uc => uc.UserId, (c, uc) => new { c, uc })
                         .SelectMany(x => x.uc.DefaultIfEmpty(), (x, uc) => new { x.c, uc })
                         .GroupBy(x => new
                         {
                             x.c.Id,
                             x.c.FullName,
                             x.c.UserName,
                             x.c.Status,
                             x.c.OfficeId,
                             x.c.OfficeName,
                             x.c.OfficeIdPath,
                             x.c.PositionId,
                             x.c.PositionName,
                             x.c.TitleId,
                             x.c.TitleName,
                             x.c.Gender,
                             x.c.JoinedDate,
                             x.c.Avatar,
                             x.c.ApproverFullName,
                             x.c.ManagerFullName,
                             x.c.MarriedStatus,
                             x.c.ProvinceName,
                             x.c.DistrictName,
                             x.c.Tin,
                             x.c.DefaultHolidayQuantity,
                             x.c.IdCardNumber,
                             x.c.Birthday
                         }).Select(p => new UserSearchViewModel()
                         {
                             Id = p.Key.Id,
                             FullName = p.Key.FullName,
                             UserName = p.Key.UserName,
                             Status = p.Key.Status,
                             OfficeId = p.Key.OfficeId,
                             OfficeName = p.Key.OfficeName,
                             OfficeIdPath = p.Key.OfficeIdPath,
                             PositionId = p.Key.PositionId,
                             PositionName = p.Key.PositionName,
                             TitleId = p.Key.TitleId,
                             TitleName = p.Key.TitleName,
                             JoinedDate = p.Key.JoinedDate,
                             Avatar = p.Key.Avatar,
                             ApproverFullName = p.Key.ApproverFullName,
                             ManagerFullName = p.Key.ManagerFullName,
                             MarriedStatus = p.Key.MarriedStatus,
                             ProvinceName = p.Key.ProvinceName,
                             DistrictName = p.Key.DistrictName,
                             Tin = p.Key.Tin,
                             DefaultHolidayQuantity = p.Key.DefaultHolidayQuantity,
                             IdCardNumber = p.Key.IdCardNumber,
                             BirthDay = p.Key.Birthday,
                             PhoneNumber = p.FirstOrDefault().uc != null ? p.FirstOrDefault().uc.ContactValue : "",
                         });

            return queryResult.AsNoTracking().ToListAsync();
        }

        public Task<List<UserSuggestionViewModel>> GetUserForSuggestion(string tenantId, string languageId, string keyword,
            int? officeId, int page, int pageSize, out int totalRows)
        {
            Expression<Func<User, bool>> spec = x => x.TenantId == tenantId && !x.IsDelete && x.Status < UserStatus.Maternity;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripVietnameseChars().Trim();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (officeId.HasValue)
            {
                spec = spec.And(x => x.OfficeId == officeId);
            }

            var query = Context.Set<User>().AsNoTracking().Where(spec)
                .Join(Context.Set<UserTranslation>().Where(c => c.LanguageId == languageId), u => u.Id, ut => ut.UserId,
                    (u, ut) => new UserSuggestionViewModel
                    {
                        Id = u.Id,
                        FullName = u.FullName,
                        Name = u.FullName,
                        Avatar = u.Avatar,
                        Image = u.Avatar,
                        PositionName = ut.PositionName,
                        OfficeName = ut.OfficeName
                    });

            totalRows = query.Count();
            return query.OrderBy(c => c.FullName).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
        }

        public Task<List<ExportUserViewModel>> GetListExportUser(string tenantId, string languageId, string keyword, UserStatus? status, string officeIdPath,
            string positionId, Gender? gender, byte? monthBirthDay, int? yearBirthday, AcademicRank? academicRank)
        {
            Expression<Func<User, bool>> spec = x => x.TenantId == tenantId && !x.IsDelete;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripVietnameseChars();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (status.HasValue)
            {
                spec = spec.And(x => x.Status == status.Value);
            }

            if (!string.IsNullOrEmpty(officeIdPath))
            {
                officeIdPath = officeIdPath.Trim();
                spec = spec.And(x => x.OfficeIdPath.StartsWith(officeIdPath + ".") || x.OfficeIdPath.Equals(officeIdPath));
            }

            if (!string.IsNullOrEmpty(positionId))
            {
                spec = spec.And(x => x.PositionId == positionId);
            }

            if (gender.HasValue)
            {
                spec = spec.And(x => x.Gender == gender.Value);
            }

            if (monthBirthDay.HasValue)
            {
                spec = spec.And(x => x.Birthday.HasValue && x.Birthday.Value.Month == monthBirthDay.Value);
            }

            if (yearBirthday.HasValue)
            {
                spec = spec.And(x => x.Birthday.HasValue && x.Birthday.Value.Year == yearBirthday.Value);
            }

            if (academicRank.HasValue)
            {
                spec = spec.And(x => x.AcademicRank == academicRank.Value);
            }

            var query = Context.Set<User>().AsNoTracking().Where(spec)
                        .Join(Context.Set<UserTranslation>().AsNoTracking().Where(c => c.LanguageId == languageId), u => u.Id, ut => ut.UserId, (u, ut) => new { u, ut })
                        .Select(x => new
                        {
                            x.u.Id,
                            x.u.FullName,
                            x.u.UserName,
                            x.u.OfficeId,
                            x.u.Gender,
                            x.u.Birthday,
                            x.u.Status,
                            x.u.Tin,
                            x.u.BankingNumber,
                            x.u.JoinedDate,
                            x.u.OutDate,
                            x.u.IdCardNumber,
                            x.u.IdCardDateOfIssue,
                            x.ut.TitleName,
                            x.ut.OfficeName,
                            x.ut.PositionName,
                            x.ut.ResidentRegister,
                            x.ut.Address,
                            x.ut.IdCardPlaceOfIssue,
                            x.ut.BranchBank,
                            x.ut.BankName
                        })
                        .GroupJoin(Context.Set<UserContact>().AsNoTracking().Where(c => c.ContactType == ContactType.MobilePhone), c => c.Id, uc => uc.UserId, (c, uc) => new { c, uc })
                        .SelectMany(x => x.uc.DefaultIfEmpty(), (x, uc) => new { x.c, uc })
                        .GroupBy(x => new
                        {
                            x.c.Id,
                            x.c.FullName,
                            x.c.UserName,
                            x.c.OfficeId,
                            x.c.Gender,
                            x.c.Birthday,
                            x.c.Status,
                            x.c.Tin,
                            x.c.BankingNumber,
                            x.c.JoinedDate,
                            x.c.OutDate,
                            x.c.IdCardNumber,
                            x.c.IdCardDateOfIssue,
                            x.c.TitleName,
                            x.c.OfficeName,
                            x.c.PositionName,
                            x.c.ResidentRegister,
                            x.c.Address,
                            x.c.IdCardPlaceOfIssue,
                            x.c.BranchBank,
                            x.c.BankName,
                        }).Select(p => new
                        {
                            p.Key.Id,
                            p.Key.FullName,
                            p.Key.UserName,
                            p.Key.OfficeId,
                            p.Key.Gender,
                            p.Key.Birthday,
                            p.Key.Status,
                            p.Key.Tin,
                            p.Key.BankingNumber,
                            p.Key.JoinedDate,
                            p.Key.OutDate,
                            p.Key.IdCardNumber,
                            p.Key.IdCardDateOfIssue,
                            p.Key.TitleName,
                            p.Key.OfficeName,
                            p.Key.PositionName,
                            p.Key.ResidentRegister,
                            p.Key.Address,
                            p.Key.IdCardPlaceOfIssue,
                            p.Key.BranchBank,
                            p.Key.BankName,
                            PhoneNumber = p.FirstOrDefault().uc != null ? p.FirstOrDefault().uc.ContactValue : "",
                        })
                        .GroupJoin(Context.Set<UserLaborContract>().Where(x => x.IsUse), g => g.Id, ul => ul.UserId, (g, ul) => new { g, ul })
                        .SelectMany(x => x.ul.DefaultIfEmpty(), (x, ul) => new { x.g, ul })
                        .Select(x => new ExportUserViewModel
                        {
                            OfficeId = x.g.OfficeId,
                            OfficeName = x.g.OfficeName,
                            Id = x.g.Id,
                            FullName = x.g.FullName,
                            UserName = x.g.UserName,
                            TitleName = x.g.TitleName,
                            PositionName = x.g.PositionName,
                            Gender = x.g.Gender,
                            Birthday = x.g.Birthday,
                            PhoneNumber = x.g.PhoneNumber,
                            ResidentRegister = x.g.ResidentRegister,
                            Adress = x.g.Address,
                            IdCardNumber = x.g.IdCardNumber,
                            IdCardDateOfIssue = x.g.IdCardDateOfIssue,
                            IdCardPlaceOfIssue = x.g.IdCardPlaceOfIssue,
                            Status = x.g.Status,
                            LaborContractType = x.ul != null ? x.ul.TypeName : "",
                            JoinedDate = x.g.JoinedDate,
                            FromDate = x.ul != null ? x.ul.FromDate : (DateTime?)null,
                            ToDate = x.ul != null ? x.ul.ToDate : null,
                            Tin = x.g.Tin,
                            BankingNumber = x.g.BankingNumber,
                            BranchBank = x.g.BranchBank,
                            BankName = x.g.BankName,
                            Address = x.g.Address
                        });

            return query.OrderBy(c => c.FullName).ToListAsync();
        }

        public Task<List<T>> Search<T>(Expression<Func<User, T>> projector, string keyword, bool? isAdmin, bool? isActive, string status, string officeIdPath, int page, int pageSize,
            out int totalRows)
        {
            Expression<Func<User, bool>> spec = u => !u.IsDelete;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripVietnameseChars();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            //if (isAdmin.HasValue)
            //{
            //    spec = spec.And(x => x.IsAdmin == isAdmin.Value);
            //}

            if (!string.IsNullOrEmpty(status))
            {
                var listStatus = status.Split(',');
                if (listStatus.Any())
                {
                    var arrayStatus = Array.ConvertAll(listStatus, int.Parse);
                    spec = spec.And(x => arrayStatus.Contains((int)x.Status));
                }
            }

            //if (!string.IsNullOrEmpty(officeIdPath))
            //{
            //    spec = spec.And(x => x.OfficeIdPath.StartsWith(officeIdPath + ".") || x.OfficeIdPath.Equals(officeIdPath));
            //}

            var sort = Context.Filters.Sort<User, string>(a => a.Id, true);
            var paging = Context.Filters.Page<User>(page, pageSize);

            totalRows = _userRepository.Count(spec);
            return _userRepository.GetsAsAsync(projector, spec, sort, paging);
        }

        public Task<List<T>> Search<T>(Expression<Func<User, T>> projector, string name, bool? isAdmin, int page, int pageSize,
            out int totalRows)
        {
            Expression<Func<User, bool>> spec = u => !u.IsDelete && u.Status < UserStatus.Maternity;

            if (!string.IsNullOrEmpty(name))
            {
                name = name.ToLower().StripVietnameseChars();
                spec = spec.And(x => x.UnsignName.ToLower().Contains(name));
            }

            //if (isAdmin.HasValue)
            //{
            //    spec = spec.And(x => x.IsAdmin == isAdmin.Value);
            //}

            var sort = Context.Filters.Sort<User, string>(a => a.FullName);
            var paging = Context.Filters.Page<User>(page, pageSize);

            totalRows = _userRepository.Count(spec);
            return _userRepository.GetsAsAsync(projector, spec, sort, paging);
        }

        public Task<List<T>> SearchUserForSuggestion<T>(Expression<Func<User, T>> projector, string name, string officeIdPath, int rootId, int page, int pageSize,
            out int totalRows)
        {
            //Expression<Func<User, bool>> spec = u => !u.IsDelete && u.Status < UserStatus.Discontinue && u.OfficeRootId == rootId;
            //if (!string.IsNullOrEmpty(name))
            //{
            //    name = name.ToLower().StripVietnameseChars();
            //    spec = spec.And(x => x.UnsignName.ToLower().Contains(name));
            //}
            //if (!string.IsNullOrEmpty(officeIdPath))
            //{
            //    spec = spec.And(x =>
            //        x.OfficeIdPath.Equals(officeIdPath) || x.OfficeIdPath.StartsWith(officeIdPath + "."));
            //}
            //var sort = Context.Filters.Sort<User, string>(a => a.FullName);
            //var paging = Context.Filters.Page<User>(page, pageSize);
            //totalRows = _userRepository.Count(spec);
            //return _userRepository.GetsAsAsync(projector, spec, sort, paging);
            totalRows = 0;
            return null;
        }

        public Task<List<T>> SearchUserForSuggestion<T>(Expression<Func<User, T>> projector, string name, int rootId)
        {
            //Expression<Func<User, bool>> spec = u => !u.IsDelete && u.Status < UserStatus.Discontinue && u.OfficeRootId == rootId;
            //if (!string.IsNullOrEmpty(name))
            //{
            //    name = name.ToLower().StripVietnameseChars();
            //    spec = spec.And(x => x.UnsignName.ToLower().Contains(name));
            //}

            //var sort = Context.Filters.Sort<User, string>(a => a.FullName);
            //var paging = Context.Filters.Page<User>(1, 10);
            //return await _userRepository.GetsAsAsync(projector, spec, sort, paging);
            return null;
        }

        public Task<List<UserSearchForManagerConfigViewModel>> SearchForConfigManager(string tenantId, string languageId, string keyword, int officeId, byte? type,
            bool? isGetStaffFromChildOffice, int page, int pageSize, out int totalRows)
        {
            var officeInfo = Task.Run(() => _officeRepository.GetActiveInfo(officeId, true)).Result;
            if (officeInfo == null)
            {
                totalRows = 0;
                return null;
            }                                                                                                                                                                                                                                                                                                                       

            Expression<Func<UserPositioncs, bool>> specUserPosition = x => x.IsDefault;
            Expression<Func<User, bool>> specUser = x => x.TenantId == tenantId && !x.IsDelete && x.Status < UserStatus.Maternity;

            if (isGetStaffFromChildOffice.HasValue)
            {
                specUserPosition =
                    specUserPosition.And(
                        x =>
                            x.OfficeIdPath.StartsWith(officeInfo.IdPath + ".") ||
                            x.OfficeIdPath.Equals(officeInfo.IdPath));
            }
            else
            {
                specUserPosition = specUserPosition.And(x => x.OfficeId == officeId);
            }

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.ToLower().StripVietnameseChars();
                specUser = specUser.And(x => x.UnsignName.ToLower().Contains(keyword));
            }

            if (type.HasValue)
            {
                // Chưa có QLTT hoặc QLPD
                if (type.Value == 0)
                {
                    specUser = specUser.And(x => x.ManagerUserId == null || x.ApproverUserId == null);
                }

                // Chưa có QLTT và QLPD
                if (type == 1)
                {
                    specUser = specUser.And(x => x.ManagerUserId == null && x.ApproverUserId == null);
                }

                // Chưa có QLTT
                if (type == 2)
                {
                    specUser = specUser.And(x => x.ManagerUserId == null);
                }

                // Chưa có QLPD
                if (type == 3)
                {
                    specUser = specUser.And(x => x.ApproverUserId == null);
                }
            }

            var query = Context.Set<User>().Where(specUser)
                .Join(Context.Set<UserPositioncs>().Where(specUserPosition), u => u.Id, ut => ut.UserId, (u, ut) => new { u, ut })
                .Join(Context.Set<PositionTranslation>().Where(x => x.LanguageId == languageId), uut => uut.ut.PositionId, pt => pt.PositionId, (uut, pt) => new { uut, pt });

            totalRows = query.Count();
            return query.OrderByDescending(x => x.uut.u.FullName).Skip((page - 1) * pageSize).Take(pageSize)
                .Select(x => new UserSearchForManagerConfigViewModel
                {
                    UserId = x.uut.u.Id,
                    FullName = x.uut.u.FullName,
                    Avatar = x.uut.u.Avatar,
                    PositionId = x.uut.ut.PositionId,
                    PositionName = x.pt.Name,
                    ApproveId = x.uut.u.ApproverUserId,
                    ApproveName = x.uut.u.ApproverFullName,
                    ManagerId = x.uut.u.ManagerUserId,
                    ManagerName = x.uut.u.ManagerFullName
                }).ToListAsync();
        }

        public User GetByUserName(string tenantId, string userName)
        {
            return _userRepository.Get(false, x => x.UserName.Equals(userName) && !x.IsDelete && x.Status < UserStatus.Maternity && x.TenantId == tenantId);
        }

        public async Task<User> GetByUserNameAsync(string tenantId, string userName)
        {
            return await _userRepository.GetAsync(true, x => x.UserName.Equals(userName) && !x.IsDelete && x.Status < UserStatus.Maternity && x.TenantId == tenantId);
        }

        public Task<int> CreateUserAdmin(string id, string fullName, string password, bool isSuperAdmin, bool isActive, List<int> permissions,
            List<int> categories)
        {
            throw new NotImplementedException();
        }

        public Task<int> UpdateUserAdmin(string userId, string email, string fullName, bool isSuperAdmin, bool isActive, List<int> permissions,
            List<int> categories)
        {
            throw new NotImplementedException();
        }

        public async Task<int> Delete(string tenantId, string userId)
        {
            var userInfo = await GetInfo(tenantId, userId);
            if (userInfo == null)
                return -1;

            userInfo.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(string tenantId, string userId)
        {
            var userInfo = await GetInfo(tenantId, userId);
            if (userInfo == null)
                return -1;

            _userRepository.Delete(userInfo);
            return await Context.SaveChangesAsync();
        }

        public Task<int> ActiveUser(string tenantId, string userId, bool isActive)
        {
            throw new NotImplementedException();
        }

        public Task<int> Count(Expression<Func<User, bool>> spec)
        {
            throw new NotImplementedException();
        }

        public int InsertUserConnection(string userId, string connectionId)
        {
            throw new NotImplementedException();
        }

        public int DeleteUserConnection(string connectionId)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteUserConnectionByUserId(string userId)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> CheckUserNameExists(string userId, string userName)
        {
            return await _userRepository.ExistAsync(x => !x.Id.Equals(userId) && !x.IsDelete && x.UserName.Equals(userName));
        }

        public async Task<bool> CheckIdExists(string tenantId, string userId)
        {
            return await _userRepository.ExistAsync(x => x.Id.Equals(userId) && !x.IsDelete && x.TenantId == tenantId);
        }

        public async Task<bool> CheckIsManagerOrApprover(string tenantId, string userId, string managerId)
        {
            return await _userRepository.ExistAsync(x => x.Id.Equals(userId) && (x.ManagerUserId.Equals(managerId) || x.ApproverUserId.Equals(managerId)) && x.TenantId == tenantId);
        }

        public async Task<List<User>> GetUserByListIds(List<string> userIds)
        {
            return await _userRepository.GetsAsync(false, x => x.Status < UserStatus.Maternity && !x.IsDelete && userIds.Contains(x.Id));
        }

        public async Task<int> UpdateOfficeIdPathByOfficeId(string tenantId, int officeId, string officeIdPath)
        {
            var listUserByOfficeId = _userRepository.Gets(false, c => c.OfficeId == officeId && c.TenantId == tenantId);
            if (listUserByOfficeId != null && listUserByOfficeId.Any())
            {
                foreach (var user in listUserByOfficeId)
                {
                    user.OfficeIdPath = officeIdPath;
                }

                return await Context.SaveChangesAsync();
            }

            return 0;
        }

        public async Task<int> UpdateOfficeNameAndOfficeIdPathByListOfficeId(List<int> listOfficeId)
        {
            //            var listJoin = Context.Set<Office>().Where(x => listOfficeId.Contains(x.Id) && !x.IsDelete && x.IsActive)
            //                .Join(Context.Set<User>().Where(x => !x.IsDelete), o => o.Id, u => u.OfficeId, (o, u) => new { o, u });
            //
            //            foreach (var userOffice in listJoin)
            //            {
            //                userOffice.u.OfficeIdPath = userOffice.o.IdPath;
            //                userOffice.u.OfficeName = userOffice.o.Name;
            //            }
            //
            //            return await Context.SaveChangesAsync();
            return 1;
        }

        public async Task<List<User>> GetListMyEmployee(string tenantId, string managerUserId)
        {
            return await _userRepository.GetsAsync(true, x => x.ManagerUserId == managerUserId && !x.IsDelete && x.Status != UserStatus.Discontinue && x.TenantId == tenantId);
        }

        public async Task<bool> CheckHasPermissionByLeaderOfficeIdPath(string tenantId, string userId, string officeIdPath)
        {
            return await _userRepository.ExistAsync(x => x.Id == userId && (x.OfficeIdPath.StartsWith(officeIdPath + ".") || x.OfficeIdPath.Equals(officeIdPath))
               && x.TenantId == tenantId && x.UserType == UserType.Leader && !x.IsDelete);
        }

        public Task<List<ShortUserInfoViewModel>> GetListUserByOfficeIdPath(string tenantId, string officeIdPath,
            string languageId, int page, int pageSize, out int totalRows)
        {
            var query = from user in Context.Set<User>().AsNoTracking()
                        join userTranslation in Context.Set<UserTranslation>().AsNoTracking() on user.Id equals userTranslation.UserId
                        where userTranslation.LanguageId == languageId
                              && !user.IsDelete && user.Status < UserStatus.Maternity && user.TenantId == tenantId
                              && (user.OfficeIdPath.Equals(officeIdPath) || user.OfficeIdPath.StartsWith(officeIdPath + "."))
                        select new ShortUserInfoViewModel()
                        {
                            UserId = user.Id,
                            UserName = user.UserName,
                            FullName = user.FullName,
                            Avatar = user.Avatar,
                            OfficeId = user.OfficeId,
                            OfficeIdPath = user.OfficeIdPath,
                            TitleId = user.TitleId,
                            PositionId = user.PositionId,
                            OfficeName = userTranslation.OfficeName,
                            TitleName = userTranslation.TitleName,
                            PositionName = userTranslation.PositionName
                        };

            totalRows = query.Count();
            return query.AsNoTracking().OrderByDescending(x => x.OfficeIdPath).Take(pageSize).Skip((page - 1) * pageSize).ToListAsync();
        }

        public Task<List<ShortUserInfoViewModel>> GetListUserByOfficeId(string tenantId, int officeId, string languageId,
            int page, int pageSize, out int totalRows)
        {
            var query = from user in Context.Set<User>().AsNoTracking()
                        join userTranslation in Context.Set<UserTranslation>().AsNoTracking() on user.Id equals userTranslation.UserId
                        where userTranslation.LanguageId == languageId
                              && !user.IsDelete && user.Status < UserStatus.Maternity && user.TenantId == tenantId
                              && user.OfficeId == officeId
                        select new ShortUserInfoViewModel()
                        {
                            UserId = user.Id,
                            UserName = user.UserName,
                            FullName = user.FullName,
                            Avatar = user.Avatar,
                            OfficeId = user.OfficeId,
                            OfficeIdPath = user.OfficeIdPath,
                            TitleId = user.TitleId,
                            PositionId = user.PositionId,
                            OfficeName = userTranslation.OfficeName,
                            TitleName = userTranslation.TitleName,
                            PositionName = userTranslation.PositionName
                        };

            totalRows = query.Count();
            return query.AsNoTracking().OrderBy(x => x.OfficeIdPath).Take(pageSize).Skip((page - 1) * pageSize).ToListAsync();
        }

        public async Task<string> GetCardNumberByUserId(string tenantId, string userId)
        {
            return await _userRepository.GetAsAsync(user => new { user.CardNumber }.CardNumber, x => x.Id.Equals(userId) && x.TenantId == tenantId);
        }

        public async Task<bool> CheckOfficeIdUser(string tenantId, int officeId)
        {
            return await _userRepository.ExistAsync(c => c.OfficeId == officeId && !c.IsDelete && c.TenantId == tenantId);
        }

        // TODO: Check this function
        public async Task<int> UpdateUserHoliday(string userId, decimal totalAnnualHoliday)
        {
            //var userInfo = await GetInfo(userId);
            //if (userInfo == null)
            //    return -1;

            //var holidayRemain = userInfo.HolidayRemaining;
            //if (totalAnnualHoliday > holidayRemain)
            //    return -2;

            //userInfo.HolidayRemaining = holidayRemain - totalAnnualHoliday;
            //return await Context.SaveChangesAsync();
            return -1;
        }

        #region Private Function()

        public async Task<bool> CheckIdCardNumberExists(string tenantId, string id, string idCardNumber)
        {
            return await _userRepository.ExistAsync(x => !x.IsDelete && !x.Id.Equals(id) && x.IdCardNumber.ToLower().Equals(idCardNumber.ToLower()) && x.TenantId == tenantId);
        }

        public async Task<int> UpdateDirectManagerAndApproveManager(List<string> userIds, string managerId, string approveId)
        {
            return await Context.SaveChangesAsync();
        }

        public Task<ShortUserInfoViewModel> GetUserInfo(string tenantId, string userId, string languageId)
        {
            var query = Context.Set<User>().AsNoTracking().Where(x => !x.IsDelete && x.Id == userId && x.TenantId == tenantId)
                   .Join(Context.Set<UserTranslation>().AsNoTracking().Where(x => x.UserId == userId && x.LanguageId == languageId)
                   , user => user.Id, userTranslation => userTranslation.UserId, (user, userTranslation) => new ShortUserInfoViewModel
                   {
                       UserId = user.Id,
                       UserName = user.UserName,
                       FullName = user.FullName,
                       Avatar = user.Avatar,
                       OfficeId = user.OfficeId,
                       OfficeIdPath = user.OfficeIdPath,
                       TitleId = user.TitleId,
                       PositionId = user.PositionId,
                       OfficeName = userTranslation.OfficeName,
                       TitleName = userTranslation.TitleName,
                       PositionName = userTranslation.PositionName,
                       UserType = (int)user.UserType
                   });

            return query.FirstOrDefaultAsync();
        }

        public Task<ShortUserInfoViewModel> GetShorUserInfoOfLeaderOffice(string tenantId, int officeId, string languageId)
        {
            var query = Context.Set<User>().AsNoTracking().Where(x => !x.IsDelete && x.TenantId == tenantId && x.OfficeId == officeId && x.UserType == UserType.Leader)
                   .Join(Context.Set<UserTranslation>().AsNoTracking().Where(x => x.LanguageId == languageId)
                   , user => user.Id, userTranslation => userTranslation.UserId, (user, userTranslation) => new ShortUserInfoViewModel
                   {
                       UserId = user.Id,
                       UserName = user.UserName,
                       FullName = user.FullName,
                       Avatar = user.Avatar,
                       OfficeId = user.OfficeId,
                       OfficeIdPath = user.OfficeIdPath,
                       TitleId = user.TitleId,
                       PositionId = user.PositionId,
                       OfficeName = userTranslation.OfficeName,
                       TitleName = userTranslation.TitleName,
                       PositionName = userTranslation.PositionName,
                       UserType = (int)user.UserType
                   });

            return query.FirstOrDefaultAsync();
        }

        public Task<List<ShortUserInfoViewModel>> GetShortUserInfoByListUserId(string tenantId, List<string> userIds, string languageId)
        {
            return Context.Set<User>().AsNoTracking().Where(x => !x.IsDelete && userIds.Contains(x.Id) && x.TenantId == tenantId)
                   .Join(Context.Set<UserTranslation>().AsNoTracking().Where(x => x.LanguageId == languageId)
                   , user => user.Id, userTranslation => userTranslation.UserId, (user, userTranslation) => new ShortUserInfoViewModel
                   {
                       UserId = user.Id,
                       UserName = user.UserName,
                       FullName = user.FullName,
                       Avatar = user.Avatar,
                       OfficeId = user.OfficeId,
                       OfficeIdPath = user.OfficeIdPath,
                       TitleId = user.TitleId,
                       PositionId = user.PositionId,
                       OfficeName = userTranslation.OfficeName,
                       TitleName = userTranslation.TitleName,
                       PositionName = userTranslation.PositionName
                   }).ToListAsync();
        }

        public async Task<int> GetTotalUserByPosition(string tenantId, string positionId)
        {
            return await _userRepository.CountAsync(x => x.TenantId == tenantId && x.PositionId == positionId && !x.IsDelete);
        }
        #endregion
    }
}
