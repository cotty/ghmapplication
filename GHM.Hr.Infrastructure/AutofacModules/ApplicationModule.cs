﻿using System.Reflection;
using Autofac;
using GHM.Hr.Domain.Commands;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Resources;
using GHM.Hr.Infrastructure.Repository;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.Services;
using GHM.Infrastructure.SqlServer;
using MediatR;

namespace GHM.Hr.Infrastructure.AutofacModules
{
    public class ApplicationModule : Autofac.Module
    {
        public string ConnectionString { get; }
        public ApplicationModule(string connectionString)
        {
            ConnectionString = connectionString;
        }

        protected override void Load(ContainerBuilder builder)
        {
            var assembly = Assembly.GetExecutingAssembly();
            builder.RegisterType<HrDbContext>()
                .As<IDbContext>()
                .InstancePerLifetimeScope();

            builder.RegisterType<HttpClientService>()
                .As<IHttpClientService>()
                .InstancePerLifetimeScope();

            #region Resources
            builder.RegisterType<ResourceService<SharedResource>>()
                .As<IResourceService<SharedResource>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ResourceService<GhmHrResource>>()
                .As<IResourceService<GhmHrResource>>()
                .InstancePerLifetimeScope();
            #endregion

            //builder.RegisterType<PositionRepository>()
            //    .As<IPositionRepository>()
            //    .InstancePerLifetimeScope();

            //builder.RegisterType<UserRepository>()
            //    .As<IUserRepository>()
            //    .InstancePerLifetimeScope();

            //builder.RegisterType<OfficeRepository>()
            //    .As<IOfficeRepository>()
            //    .InstancePerLifetimeScope();

            //builder.RegisterType<OfficeTitleRepository>()
            //    .As<IOfficeTitleRepository>()
            //    .InstancePerLifetimeScope();

            //builder.RegisterType<UserTitleRepository>()
            //    .As<IUserTitleRepository>()
            //    .InstancePerLifetimeScope();

            //builder.RegisterType<TitleRepository>()
            //    .As<ITitleRepository>()
            //    .InstancePerLifetimeScope();

            //builder.RegisterType<UserAssessmentRepository>()
            //    .As<IUserAssessmentRepository>()
            //    .InstancePerLifetimeScope();

            //builder.RegisterType<UserAssessmentCriteriaRepository>()
            //    .As<IUserAssessmentCriteriaRepository>()
            //    .InstancePerLifetimeScope();

            //builder.RegisterType<CriteriaTitleRepository>()
            //    .As<ICriteriaTitleRepository>()
            //    .InstancePerLifetimeScope();

            //builder.RegisterType<CriteriaLibraryRepository>()
            //    .As<ICriteriaLibraryRepository>()
            //    .InstancePerLifetimeScope();

            //builder.RegisterType<UserValueRepository>()
            //    .As<IUserValueRepository>()
            //    .InstancePerLifetimeScope();

            //builder.RegisterType<AcademicLevelRepository>()
            //    .As<IAcademicLevelRepository>()
            //    .InstancePerLifetimeScope();   

            builder.RegisterType<UserContactRepositiory>()
                .As<IUserContactRepository>()
                .InstancePerLifetimeScope();

            #region Repositories            
            builder.RegisterAssemblyTypes(assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces();
            #endregion

            #region Services            
            builder.RegisterAssemblyTypes(assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces();
            #endregion

            builder.RegisterAssemblyTypes(typeof(IMediator).GetTypeInfo().Assembly)
                .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(typeof(UpdateManagerCommand).GetTypeInfo().Assembly).
                AsClosedTypesOf(typeof(INotificationHandler<>));
        }
    }
}
