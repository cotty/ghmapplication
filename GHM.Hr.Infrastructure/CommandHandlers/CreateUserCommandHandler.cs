﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.Models;
using GHM.Hr.Infrastructure.Commands;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Services;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace GHM.Hr.Infrastructure.CommandHandlers
{
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, ActionResultResponse>
    {
        private readonly IUserRepository _userRepository;
        //private readonly IUserTitleRepository _userTitleRepository;
        private readonly IOfficeRepository _officeRepository;
        private readonly ITitleRepository _titleRepository;
        private readonly IConfiguration _configuration;
        private readonly IHttpClientService _httpClientService;
        private readonly ApiUrlSettings _apiUrls;
        private readonly ApiServiceInfo _apiServiceInfo;

        public CreateUserCommandHandler(IUserRepository userRepository, IUserTitleRepository userTitleRepository, IOfficeRepository officeRepository,
            ITitleRepository titleRepository, IConfiguration configuration, IHttpClientService httpClientService)
        {
            _userRepository = userRepository;
            _userTitleRepository = userTitleRepository;
            _officeRepository = officeRepository;
            _titleRepository = titleRepository;
            _configuration = configuration;
            _httpClientService = httpClientService;

            _apiUrls = _configuration.GetApiUrl();
            _apiServiceInfo = _configuration.GetApiServiceInfo();
        }

        public async Task<ActionResultResponse> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var isIdExists = await _userRepository.CheckIdExists(request.Id);
            if (isIdExists)
                return new ActionResultResponse(-1, "Mã người dùng đã tồn tại. Vui lòng kiểm tra lại hoặc liên hệ với Quản trị viên.", "Rất tiếc");

            if (!string.IsNullOrEmpty(request.IdCardNumber))
            {
                var isIdCardNumberExists = await _userRepository.CheckIdCardNumberExists(request.Id, request.IdCardNumber);
                if (isIdCardNumberExists)
                    return new ActionResultResponse(-2, "Số CMT đã được sử dụng cho người dùng khác. Vui lòng kiểm tra lại hoặc liên hệ với Quản trị viên.", "Rất tiếc");
            }

            var officeInfo = await _officeRepository.GetInfo(request.OfficeId ?? -1, true);
            if (officeInfo == null)
                return new ActionResultResponse(-3, "Thông tin phòng ban không tồn tại. Vui lòng kiểm tra lại hoặc liên hệ với Quản trị viên.", "Rất tiếc");

//            var titleInfo = await _titleRepository.GetInfo(request.TitleId ?? -1, true);
//            if (titleInfo == null)
//                return new ActionResultResponse(-4, "Thông tin chức danh không tồn tại. Vui lòng kiểm tra lại hoặc liên hệ với Quản trị viên.", "Rất tiếc");
//
//            if (!titleInfo.IsMultiple)
//            {
//                var isTitleExists = await _userTitleRepository.CheckExistsByTitleId(titleInfo.Id);
//                if (isTitleExists)
//                    return new ActionResultResponse(-5, "Chức danh này đã được gán cho người dùng khác. Vui lòng kiểm tra lại hoặc liên hệ với Quản trị viên.", "Rất tiếc");
//            }

            // Create user account.            
            var resultInsertAccount = await _httpClientService.PostAsync<ActionResultResponse>($"{_apiUrls.CoreApiUrl}accounts", new Dictionary<string, string>
            {
                {"FullName",  request.FullName},
                {"UserName",  request.UserName},
                {"Password",  request.Password},
                {"Email",  request.Email},
                {"ConfirmPassword",  request.ConfirmPassword}
            });

            if (resultInsertAccount.Code <= 0)
                return new ActionResultResponse(-6, "Tạo tài khoản trên hệ thống thất bại. Vui lòng liên hệ với Quản trị viên.", "Rất tiếc");

            // Create user info.
            var user = new User
            {
                Id = request.Id,
                //                UserId = resultInsertAccount.Data,
                FullName = request.FullName,
                FirstName = request.FullName.GetFirstName(),
                MiddleName = request.FullName.GetMiddleName(),
                LastName = request.FullName.GetLastName(),
                //                Email = request.Email,
                //                PhoneNumber = request.PhoneNumber,
                IdCardDateOfIssue = request.IdCardDateOfIssue,
                IdCardNumber = request.IdCardNumber,
                IdCardPlaceOfIssue = request.IdCardPlaceOfIssue,
//                OfficeId = officeInfo.Id,
//                OfficeName = officeInfo.Name,
//                OfficeIdPath = officeInfo.IdPath,
//                OfficeRootId = officeInfo.RootId,
//                TitleId = titleInfo.Id,
//                TitleName = titleInfo.Name,
//                PositionId = titleInfo.PositionId,
//                PositionName = titleInfo.PositionName,
                Address = request.Address,
                //                BankName = request.BankName,
                //                BankingNumber = request.BankingNumber,
                //                Branch = request.Branch,
                Birthday = request.Birthday,
                Denomination = request.Denomination,
//                Gender = request.Gender,
                JoinedDate = request.JoinedDate,
                MarriedStatus = request.MarriedStatus,
                ChildQuantity = request.ChildQuantity,
                //                TelephoneNumber = request.TelephoneNumber,
                //                Ext = request.Ext,
                Nationality = request.Nationality,
                NativeCountry = request.NativeCountry,
                ResidentRegister = request.ResidentRegister,
                OutDate = request.OutDate,
                SocialInsuranceNumber = request.SocialInsuranceNumber,
                SocialInsurancePlaceOfIssue = request.SocialInsurancePlaceOfIssue,
                Status = request.Status ?? UserStatus.Discontinue,
                Tin = request.Tin,
                Workplace = request.Workplace,
                UnsignName =
                    $"{request.Id} {request.FullName.StripVietnameseChars()} {request.Email} {request.PhoneNumber} {request.Tin}"
                        .ToLower(),
                //                IsHasCertificate = request.IsHasCertificate,
                //                CertificateNote = request.CertificateNote,
                UserType = request.UserType,
                PassportId = request.PassportId,
                PassportPlaceOfIssue = request.PassportPlaceOfIssue,
                PassportDateOfIssue = request.PassportDateOfIssue,
                //                DiscontinuedReason = request.DiscontinuedReason,
                //                GroupUser = request.GroupUser,
                CardNumber = request.CardNumber
            };

            var userDigit = !string.IsNullOrEmpty(_configuration.GetSection("UserIdDigit").Value) ? int.Parse(_configuration.GetSection("UserIdDigit").Value) : 4;
            var lastEnrollNumber = _userRepository.GetMaxEnrollNumbeValue();
            user.EnrollNumber = lastEnrollNumber + 1;
            user.Id = $"NV{new string('0', userDigit - user.EnrollNumber.ToString().Length)}{user.EnrollNumber}";

            // Create user title.
            var resultInsertUser = await _userRepository.Insert(user);
            if (resultInsertUser <= 0)
            {
                // Rollback user account.   
                await RollbackUserAccount(user.Id);
                return new ActionResultResponse(-7, "Thêm mới người dùng thất bại. Vui lòng liên hệ với Quản trị viên.", "Rất tiếc");
            }

//            var resultInsertUserTitle = await _userTitleRepository.Insert(new UserPositioncs
//            {
//                OfficeId = officeInfo.Id,
//                OfficeIdPath = officeInfo.IdPath,
////                OfficeName = officeInfo.Name,
////                OfficeShortName = officeInfo.ShortName,
//                UserId = user.Id,
////                TitleId = titleInfo.Id,
////                TitleName = titleInfo.Name,
////                TitleShortName = titleInfo.ShortName,
//                UserType = request.UserType,
//                IsDefault = true
//            });
            //if (resultInsertUserTitle <= 0)
            //{
            //    // Rollback inserted account, user info
            //    await RollbackUser(user.Id);
            //    return new ActionResultResponse(-8, "Đã có lỗi sảy ra. Vui lòng liên hệ với Quản trị viên.", "Rất tiếc");
            //}

            // Create user work schedule.            
            var resultInsertWorkSchedule = await _httpClientService.PostAsync<ActionResultResponse>($"{_apiUrls.TimekeepingApiUrl}work-schedules", new Dictionary<string, string>
            {
                {"EnrollNumber", user.EnrollNumber.ToString() },
                {"FullName", user.FullName },
//                {"OfficeId", user.OfficeId.ToString() },
//                {"OfficeName", user.OfficeName },
//                {"OfficeIdPath", user.OfficeIdPath },
//                {"TitleName", user.TitleName },
//                {"TitleId", user.TitleId.ToString() },
                {"UnsignName", user.UnsignName },
                {"UserId", user.Id },
            });
            if (resultInsertWorkSchedule == null || resultInsertWorkSchedule.Code <= 0)
            {
                // Rollback insert user account.
                await RollbackUserAccount(user.Id);
                await RollbackUser(user.Id);
//                await RollbackUserTitle(user.Id, user.TitleId, user.OfficeId);
                return resultInsertWorkSchedule;
            }

            // Insert user into timekeeping machine.
            var resultInsertUserIntoMachine = await _httpClientService.PostAsync<ActionResultResponse>($"{_apiUrls.TimekeepingApiUrl}machines/users",
                new Dictionary<string, string>
                {
                    {"EnrollNumber", user.EnrollNumber.ToString()},
                    {"FullName", user.FullName},
                    {"CardNumber", user.CardNumber},
                });

            if (resultInsertUserIntoMachine == null || resultInsertUserIntoMachine.Code <= 0)
            {
                await RollbackUserAccount(user.Id);
                await RollbackUser(user.Id);
//                await RollbackUserTitle(user.Id, user.TitleId, user.OfficeId);
                await RollbackWorkSchedule(user.Id);
                return resultInsertUserIntoMachine;
            }

            return new ActionResultResponse(1, "Thêm mới người dùng thành công.", "Chúc mừng");
        }

        private async Task RollbackUserAccount(string accountId)
        {
            await _httpClientService.DeleteAsync<ActionResultResponse>($"{_apiUrls.CoreApiUrl}accounts/{accountId}");
        }

        private async Task RollbackUser(string id)
        {
            await _userRepository.Delete(id);
        }

        private async Task RollbackUserTitle(string userId, int titleId, int officeId)
        {
            //await _userTitleRepository.Delete(userId, titleId, officeId);
        }

        private async Task RollbackWorkSchedule(string userId)
        {
            await _httpClientService.DeleteAsync<ActionResultResponse>(
                $"{_apiUrls.TimekeepingApiUrl}work-schedules/{userId}");
        }
    }
}
