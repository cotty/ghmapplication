﻿using System;
using System.Threading;
using System.Threading.Tasks;
using GHM.Hr.Domain.Commands;
using GHM.Hr.Domain.IServices;
using MediatR;

namespace GHM.Hr.Infrastructure.CommandHandlers
{
    public class UpdateManagerCommandHandler : INotificationHandler<UpdateManagerCommand>
    {
        private readonly IUserAssessmentService _userAssessmentService;

        public UpdateManagerCommandHandler(IUserAssessmentService userAssessmentService)
        {
            _userAssessmentService = userAssessmentService;
        }

        public async Task Handle(UpdateManagerCommand notification, CancellationToken cancellationToken)
        {
            await _userAssessmentService.UpdateUserAssessmentManager(notification.TenantId, notification.UserId,
                notification.ManagerId, notification.IsManager);
        }
    }
}
