﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace GHM.Hr.Infrastructure
{
    public class HrDbContextFactory : IDesignTimeDbContextFactory<HrDbContext>
    {
        public HrDbContext CreateDbContext(string[] args)
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);

            var configs = configurationBuilder.Build();
            var connectionString = configs.GetConnectionString("HrConnectionString");

            var optionsBuilder = new DbContextOptionsBuilder<HrDbContext>();
            optionsBuilder.UseSqlServer(connectionString);
            return new HrDbContext(optionsBuilder.Options);
        }
    }
}
