﻿using System;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Infrastructure.Services
{
    public class AcademicLevelService : IAcademicLevelService
    {
        private readonly IAcademicLevelRepository _academicLevelRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmHrResource> _resourceService;
        private readonly IUserRepository _userRepository;
        private readonly IUserValueRepository _userValueRepository;

        public AcademicLevelService(IAcademicLevelRepository academicLevelRepository, IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmHrResource> resourceService, IUserRepository userRepository, IUserValueRepository userValueRepository)
        {
            _academicLevelRepository = academicLevelRepository;
            _userRepository = userRepository;
            _userValueRepository = userValueRepository;
            _resourceService = resourceService;
            _sharedResourceService = sharedResourceService;
        }

        public async Task<ActionResultResponse> Delete(string tenantId, string id)
        {
            var result = await _academicLevelRepository.Delete(tenantId, id);
            if (result == -1)
                return new ActionResultResponse(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound,
                        _resourceService.GetString("AcademicLevel")));

            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                                             : _resourceService.GetString("Delete academic level success"));
        }

        public async Task<ActionResultResponse<AcademicLevelDetailViewModel>> GetDetail(string tenantId, string id)
        {
            var info = await _academicLevelRepository.GetInfo(tenantId, id, true);
            if (info == null)
                return new ActionResultResponse<AcademicLevelDetailViewModel>(-1,
                   _sharedResourceService.GetString(ErrorMessage.NotFound,
                       _resourceService.GetString("AcademicLevel")));

            var detail = new AcademicLevelDetailViewModel
            {
                Id = info.Id,
                AcademicLevelId = info.AcademicLevelId,
                AcademicLevelName = info.AcademicLevelName,
                DegreeId = info.DegreeId,
                DegreeName = info.DegreeName,
                SchoolId = info.SchoolId,
                SchoolName = info.SchoolName,
                SpecializeId = info.SpecializeId,
                SpecializeName = info.SpecializeName,
                UserId = info.UserId,
                ConcurrencyStamp = info.ConcurrencyStamp,
                Note = info.Note
            };

            return new ActionResultResponse<AcademicLevelDetailViewModel>
            {
                Code = 1,
                Data = detail
            };
        }

        public async Task<ActionResultResponse<string>> Insert(string tenantId, AcademicLevelMeta academicLevel)
        {
            var academicLevelInfo = await _userValueRepository.GetInfo(tenantId, academicLevel.AcademicLevelId, UserValueType.AcademicLevel, true);
            if (academicLevelInfo == null)
                return new ActionResultResponse<string>(-1,
                       _sharedResourceService.GetString(ErrorMessage.NotFound,
                           _resourceService.GetString("AcademicLevel")));

            var degreeInfo = await _userValueRepository.GetInfo(tenantId, academicLevel.DegreeId, UserValueType.AcademicLevelDegree, true);
            if (degreeInfo == null)
                return new ActionResultResponse<string>(-2,
                           _sharedResourceService.GetString(ErrorMessage.NotFound,
                               _resourceService.GetString("Degree")));

            var specializeInfo = await _userValueRepository.GetInfo(tenantId, academicLevel.SpecializeId, UserValueType.AcademicLevelSpecialize, true);
            if (specializeInfo == null)
                return new ActionResultResponse<string>(-3,
                              _sharedResourceService.GetString(ErrorMessage.NotFound,
                                  _resourceService.GetString("Specialize")));

            var schoolInfo = await _userValueRepository.GetInfo(tenantId, academicLevel.SchoolId, UserValueType.AcademicLevelSchool, true);
            if (schoolInfo == null)
                return new ActionResultResponse<string>(-4,
                                   _sharedResourceService.GetString(ErrorMessage.NotFound,
                                       _resourceService.GetString("School")));

            var isExists = await _academicLevelRepository.CheckIsExists(tenantId, academicLevel.UserId, academicLevel.AcademicLevelId,
                academicLevel.DegreeId, academicLevel.SchoolId, academicLevel.SpecializeId);
            if (isExists)
                return new ActionResultResponse<string>(-1,
                    _sharedResourceService.GetString(ErrorMessage.AlreadyExists,
                        _resourceService.GetString("AcademicLevel")));

            var academicLevelInsert = new AcademicLevel()
            {
                TenantId = tenantId,
                AcademicLevelId = academicLevel.AcademicLevelId,
                AcademicLevelName = academicLevelInfo.Name,
                DegreeId = degreeInfo.Id,
                DegreeName = degreeInfo.Name,
                SchoolId = schoolInfo.Id,
                SchoolName = schoolInfo.Name,
                SpecializeId = specializeInfo.Id,
                SpecializeName = specializeInfo.Name,
                UserId = academicLevel.UserId,
                Note = academicLevel.Note,
            };

            var result = await _academicLevelRepository.Insert(academicLevelInsert);
            if (result <= 0)
                return new ActionResultResponse<string>(result, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            return new ActionResultResponse<string>(result, _resourceService.GetString("Thêm mới trình độ học vấn thành công", "", academicLevelInsert.Id));
        }

        public async Task<SearchResult<AcademicLevelViewModel>> Search(string tenantId, string userId, string levelId, string degreeId, string schoolId,
            string specializeId, int page, int pageSize)
        {
            if (!string.IsNullOrEmpty(userId))
            {
                var userInfo = await _userRepository.GetInfo(tenantId, userId, true);
                if (userInfo == null)
                    return new SearchResult<AcademicLevelViewModel>(-1,
                  _sharedResourceService.GetString(ErrorMessage.NotFound,
                      _resourceService.GetString("User")));
            }

            var listItems = await _academicLevelRepository.Search(tenantId, userId, levelId, degreeId, schoolId, specializeId, page, pageSize, out int totalRows);
            return new SearchResult<AcademicLevelViewModel>
            {
                Items = listItems,
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse<string>> Update(string tenantId, string id, AcademicLevelMeta academicLevel)
        {
            var info = await _academicLevelRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse<string>(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound,
                        _resourceService.GetString("AcademicLevel")));

            if (info.TenantId != tenantId)
                return new ActionResultResponse<string>(-2, _resourceService.GetString(ErrorMessage.NotHavePermission));

            if (info.ConcurrencyStamp != academicLevel.ConcurrencyStamp)
                return new ActionResultResponse<string>(-3, _resourceService.GetString(ErrorMessage.AlreadyUpdatedByAnother));

            if (info.AcademicLevelId != academicLevel.AcademicLevelId)
            {
                var academicLevelInfo = await _userValueRepository.GetInfo(tenantId, academicLevel.AcademicLevelId, UserValueType.AcademicLevel, true);
                if (academicLevelInfo == null)
                    return new ActionResultResponse<string>(-1,
                           _sharedResourceService.GetString(ErrorMessage.NotFound,
                               _resourceService.GetString("AcademicLevel")));

                info.AcademicLevelId = academicLevel.AcademicLevelId;
                info.AcademicLevelName = academicLevelInfo.Name;

            }

            if (info.DegreeId != academicLevel.DegreeId)
            {
                var degreeInfo = await _userValueRepository.GetInfo(tenantId, academicLevel.DegreeId, UserValueType.AcademicLevelDegree, true);
                if (degreeInfo == null)
                    return new ActionResultResponse<string>(-2,
                               _sharedResourceService.GetString(ErrorMessage.NotFound,
                                   _resourceService.GetString("Degree")));

                info.DegreeId = academicLevel.DegreeId;
                info.DegreeName = degreeInfo.Name;
            }

            if (info.SpecializeId != academicLevel.SpecializeId)
            {
                var specializeInfo = await _userValueRepository.GetInfo(tenantId, academicLevel.SpecializeId, UserValueType.AcademicLevelSpecialize, true);
                if (specializeInfo == null)
                    return new ActionResultResponse<string>(-3,
                                  _sharedResourceService.GetString(ErrorMessage.NotFound,
                                      _resourceService.GetString("Specialize")));

                info.SpecializeId = academicLevel.SpecializeId;
                info.SpecializeName = specializeInfo.Name;
            }

            if (info.SchoolId != academicLevel.SchoolId)
            {
                var schoolInfo = await _userValueRepository.GetInfo(tenantId, academicLevel.SchoolId, UserValueType.AcademicLevelSchool, true);
                if (schoolInfo == null)
                    return new ActionResultResponse<string>(-4,
                                       _sharedResourceService.GetString(ErrorMessage.NotFound,
                                           _resourceService.GetString("School")));

                info.SchoolId = academicLevel.SchoolId;
                info.SchoolName = schoolInfo.Name;
            }

            if (info.AcademicLevelId != academicLevel.AcademicLevelId || info.DegreeId != academicLevel.DegreeId
                || info.SpecializeId != academicLevel.SpecializeId || info.SchoolId != academicLevel.SchoolId)
            {
                var isExists = await _academicLevelRepository.CheckIsExists(tenantId, academicLevel.UserId, academicLevel.AcademicLevelId,
                    academicLevel.DegreeId, academicLevel.SchoolId, academicLevel.SpecializeId);

                if (isExists)
                    return new ActionResultResponse<string>(-1,
                        _sharedResourceService.GetString(ErrorMessage.AlreadyExists,
                            _resourceService.GetString("AcademicLevel")));
            }

            info.Note = academicLevel.Note;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();
            var result = await _academicLevelRepository.Update(info);

            return new ActionResultResponse<string>(result, result < 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                     : result == 0 ? _sharedResourceService.GetString(ErrorMessage.NothingChanges) : _resourceService.GetString("Cập nhật trình độ học vấn thành công"), "", info.TenantId);
        }
    }
}
