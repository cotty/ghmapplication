﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Infrastructure.Services
{
    public class TitleService : ITitleService
    {
        private readonly ITitleRepository _titleRepository;
        private readonly ITitleTranslationRepository _titleTranslationRepository;
        private readonly IPositionRepository _positionRepository;
        private readonly IPositionTranslationRepository _positionTranslationRepository;
        // private readonly IOfficeTitleRepository _officeTitleRepository;

        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmHrResource> _hrResourceService;

        public TitleService(IPositionRepository positionRepository, ITitleRepository titleRepository,
            IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> hrResourceService,
            ITitleTranslationRepository titleTranslationRepository, IPositionTranslationRepository positionTranslationRepository)
        {
            _positionRepository = positionRepository;
            _titleRepository = titleRepository;
            _sharedResourceService = sharedResourceService;
            _hrResourceService = hrResourceService;
            _titleTranslationRepository = titleTranslationRepository;
            _positionTranslationRepository = positionTranslationRepository;
            // _officeTitleRepository = officeTitleRepository;
        }

        public async Task<ActionResultResponse> Insert(string tenantId, TitleMeta titleMeta)
        {
            if (!titleMeta.Translations.Any())
                return new ActionResultResponse(-1, _sharedResourceService.GetString("Please enter at least one language."));

            var titleTranslations = new List<TitleTranslation>();
            var titleId = Guid.NewGuid().ToString();

            // Insert new title.
            var resultInsertTitle = await _titleRepository.Insert(new Title
            {
                Id = titleId,
                ConcurrencyStamp = titleId,
                IsActive = titleMeta.IsActive,
                CreateTime = DateTime.Now,
                IsDelete = false,
                TenantId = tenantId,
            });

            if (resultInsertTitle <= 0)
                return new ActionResultResponse(resultInsertTitle, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            // Check name exists.
            foreach (var titleTranslation in titleMeta.Translations)
            {
                var isNameExists = await _titleTranslationRepository.CheckExists(titleId, tenantId,
                    titleTranslation.LanguageId, titleTranslation.Name);
                if (isNameExists)
                {
                    await RollbackInsert(tenantId, titleId);
                    return new ActionResultResponse(-3, _hrResourceService.GetString("Chức danh: \"{0}\" đã tồn tại.", titleTranslation.Name));
                }

                var isShortNameExists = await _titleTranslationRepository.CheckShortNameExists(titleId, tenantId,
                    titleTranslation.LanguageId, titleTranslation.ShortName);
                if (isShortNameExists)
                {
                    await RollbackInsert(tenantId, titleId);
                    return new ActionResultResponse(-4, _hrResourceService.GetString("Tên viết tắt: \"{0}\" đã tồn tại.", titleTranslation.ShortName));
                }

                titleTranslation.TenantId = tenantId;
                titleTranslation.TitleId = titleId;
                titleTranslation.LanguageId = titleTranslation.LanguageId.Trim();
                titleTranslation.Name = titleTranslation.Name.Trim();
                titleTranslation.ShortName = titleTranslation.ShortName.Trim();
                titleTranslation.Description = titleTranslation.Description?.Trim();
                titleTranslation.UnsignName = titleTranslation.Name.StripVietnameseChars().ToUpper();
                titleTranslations.Add(titleTranslation);
            }

            // Insert title translations.
            var resultInsertTranslation = await _titleTranslationRepository.Inserts(titleTranslations);
            if (resultInsertTranslation > 0)
                return new ActionResultResponse(resultInsertTitle, _hrResourceService.GetString("Add new title successful."));

            await RollbackInsert(tenantId, titleId);
            return new ActionResultResponse(-6, _hrResourceService.GetString("Can not insert new title. Please contact with administrator."));
        }

        public async Task<ActionResultResponse> Update(string tenantId, string id, TitleMeta titleMeta)
        {
            if (!titleMeta.Translations.Any())
                return new ActionResultResponse(-1, _sharedResourceService.GetString("Please enter at least one language."));

            var info = await _titleRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-2, _hrResourceService.GetString("Title does not exists."));

            if (info.ConcurrencyStamp != titleMeta.ConcurrencyStamp)
                return new ActionResultResponse(-3, _hrResourceService.GetString("The title already updated by other people. You can not update this title."));

            info.IsActive = titleMeta.IsActive;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();

            foreach (var titleTranslation in titleMeta.Translations)
            {
                var isNameExists = await _titleTranslationRepository.CheckExists(info.Id, tenantId,
                    titleTranslation.LanguageId, titleTranslation.Name);
                if (isNameExists)
                    return new ActionResultResponse(-5, _hrResourceService.GetString("Title name: \"{0}\" already exists.", titleTranslation.Name));

                var isShortNameExists = await _titleTranslationRepository.CheckShortNameExists(info.Id, tenantId,
                    titleTranslation.LanguageId, titleTranslation.ShortName);
                if (isShortNameExists)
                    return new ActionResultResponse(-6, _hrResourceService.GetString("Short name: \"{0}\" already exists.", titleTranslation.ShortName));

                var titleTitleTranslationInfo =
                    await _titleTranslationRepository.GetInfo(tenantId, info.Id, titleTranslation.LanguageId);
                if (titleTitleTranslationInfo != null)
                {
                    titleTitleTranslationInfo.Name = titleTranslation.Name.Trim();
                    titleTitleTranslationInfo.ShortName = titleTranslation.ShortName.Trim();
                    titleTitleTranslationInfo.Description = titleTranslation.Description?.Trim();
                    titleTitleTranslationInfo.UnsignName = titleTranslation.Name.StripVietnameseChars().ToUpper();
                    await _titleTranslationRepository.Update(titleTitleTranslationInfo);
                }
                else
                {
                    titleTranslation.Name = titleTranslation.Name.Trim();
                    titleTranslation.ShortName = titleTranslation.ShortName.Trim();
                    titleTranslation.Description = titleTranslation.Description?.Trim();
                    titleTranslation.UnsignName = titleTranslation.Name.StripVietnameseChars().ToUpper();
                    await _titleTranslationRepository.Insert(titleTranslation);
                }
            }

            await _titleRepository.Update();
            return new ActionResultResponse(1, _hrResourceService.GetString("Update title successful."));
        }

        public async Task<ActionResultResponse> Delete(string tenantId, string id)
        {
            // Check is used by position.
            var isUsed = await _positionRepository.CheckExistsByTitleId(tenantId, id);
            if (isUsed)
                return new ActionResultResponse(-1, _hrResourceService.GetString("This title is used by position."));

            var result = await _titleRepository.Delete(tenantId, id);
            if (result <= 0)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await _titleTranslationRepository.ForceDeleteByTitleId(tenantId, id);
            return new ActionResultResponse(result, _hrResourceService.GetString("Delete title successful."));
        }

        public async Task<ActionResultResponse<TitleDetailViewModel>> GetDetail(string tenantId, string id)
        {
            var titleInfo = await _titleRepository.GetInfo(tenantId, id);
            if (titleInfo == null)
                return new ActionResultResponse<TitleDetailViewModel>(-1, _hrResourceService.GetString("Title does not exists."));

            if (titleInfo.TenantId != tenantId)
                return new ActionResultResponse<TitleDetailViewModel>(-2, _sharedResourceService.GetString("You do not have permission to do this action."));

            var titleTranslations = await _titleTranslationRepository.GetsByTitleId(tenantId, id);
            var titleDetail = new TitleDetailViewModel
            {
                Id = titleInfo.Id,
                IsActive = titleInfo.IsActive,
                ConcurrencyStamp = titleInfo.ConcurrencyStamp,
                Translations = titleTranslations.Select(x => new TitleTranslationViewModel
                {
                    LanguageId = x.LanguageId,
                    Name = x.Name,
                    Description = x.Description,
                    ShortName = x.ShortName
                }).ToList()
            };
            return new ActionResultResponse<TitleDetailViewModel>
            {
                Data = titleDetail
            };
        }

        public async Task<SearchResult<TitleViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive,
            int page, int pageSize)
        {
            var items = await _titleRepository.Search(tenantId, languageId, keyword, isActive, page, pageSize, out var totalRows);
            return new SearchResult<TitleViewModel>
            {
                Items = items,
                TotalRows = totalRows
            };
        }

        public async Task<List<TitleSearchForSelectViewModel>> SearchForSelect(string tenantId, string languageId, string keyword)
        {
            return await _titleRepository.SearchForSelect(tenantId, languageId, keyword);
        }

        private async Task RollbackInsert(string tenantId, string titleId)
        {
            await _titleRepository.ForceDelete(titleId);
            await _titleTranslationRepository.ForceDeleteByTitleId(tenantId, titleId);
        }
    }
}
