﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Infrastructure.Services
{
    public class CriteriaService : ICriteriaService
    {
        private readonly ICriteriaRepository _criteriaRepository;
        private readonly ICriteriaTransaltionRepository _criteriaTransaltionRepository;
        private readonly ICriteriaGroupRepository _criteriaGroupRepository;
        private readonly ICriteriaGroupTranslationRepository _criteriaGroupTranslationRepository;
        private readonly IUserAssessmentCriteriaRepository _userAssessmentCriteriaRepository;

        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmHrResource> _resourceService;

        public CriteriaService(ICriteriaRepository criteriaRepository, ICriteriaTransaltionRepository criteriaTransaltionRepository,
            IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> resourceService,
            ICriteriaGroupRepository criteriaGroupRepository, ICriteriaGroupTranslationRepository criteriaGroupTranslationRepository,
            IUserAssessmentCriteriaRepository userAssessmentCriteriaRepository)
        {
            _criteriaRepository = criteriaRepository;
            _criteriaTransaltionRepository = criteriaTransaltionRepository;
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
            _criteriaGroupRepository = criteriaGroupRepository;
            _criteriaGroupTranslationRepository = criteriaGroupTranslationRepository;
            _userAssessmentCriteriaRepository = userAssessmentCriteriaRepository;
        }

        public async Task<ActionResultResponse> Insert(CriteriaMeta criteriaMeta)
        {
            // Check group exists.
            var isGroupExists =
                await _criteriaGroupRepository.CheckIdExists(criteriaMeta.TenantId, criteriaMeta.GroupId);
            if (!isGroupExists)
                return new ActionResultResponse(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound,
                        _resourceService.GetString("Criteria group")));

            var criteria = new Criteria
            {
                TenantId = criteriaMeta.TenantId,
                GroupId = criteriaMeta.GroupId,
                Point = criteriaMeta.Point,
                IsActive = criteriaMeta.IsActive
            };

            foreach (var criteriaTranslation in criteriaMeta.Translations)
            {
                var isExists = await _criteriaTransaltionRepository.CheckExists(criteria.TenantId, string.Empty,
                    criteriaTranslation.LanguageId, criteriaTranslation.Name.Trim());
                if (isExists)
                    return new ActionResultResponse(-2,
                        _sharedResourceService.GetString(ErrorMessage.AlreadyExists,
                            _resourceService.GetString("criteria name")));

                criteriaTranslation.TenantId = criteriaMeta.TenantId;
                criteriaTranslation.Description = criteriaTranslation.Description?.Trim();
                criteriaTranslation.Sanction = criteriaTranslation.Sanction?.Trim();
                criteriaTranslation.Name = criteriaTranslation.Name?.Trim();
                criteriaTranslation.UnsignName = criteriaTranslation.Name.StripVietnameseChars().ToUpper();
                criteria.Translations.Add(criteriaTranslation);
            }

            var result = await _criteriaRepository.Insert(criteria);
            return new ActionResultResponse(result, result <= 0
                ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _sharedResourceService.GetString(SuccessMessage.AddSuccessful,
                    _resourceService.GetString("criteria")));
        }

        public async Task<ActionResultResponse> Update(string id, CriteriaMeta criteriaMeta)
        {
            var criteria = await _criteriaRepository.GetInfo(criteriaMeta.TenantId, id);
            if (criteria == null)
                return new ActionResultResponse(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound, _resourceService.GetString("Criteria")));

            if (criteriaMeta.ConcurrencyStamp != criteria.ConcurrencyStamp)
                return new ActionResultResponse(-2,
                    _sharedResourceService.GetString(ErrorMessage.AlreadyUpdatedByAnother, _resourceService.GetString("Criteria")));

            if (criteria.GroupId != criteriaMeta.GroupId)
            {
                // Kiểm tra nhóm có tồn tại không.
                var isGroupExists =
                    await _criteriaGroupRepository.CheckIdExists(criteria.TenantId, criteriaMeta.GroupId);
                if (!isGroupExists)
                    return new ActionResultResponse(-3,
                        _sharedResourceService.GetString(ErrorMessage.NotExists, _resourceService.GetString("Criteria group")));

                criteria.GroupId = criteriaMeta.GroupId;
            }

            criteria.IsActive = criteriaMeta.IsActive;
            criteria.ConcurrencyStamp = Guid.NewGuid().ToString();
            criteria.Point = criteriaMeta.Point;
            criteria.Order = criteriaMeta.Order;

            await _criteriaRepository.Update(criteria);

            foreach (var criteriaTranslationMeta in criteriaMeta.Translations)
            {
                var criteriaTranslation = await _criteriaTransaltionRepository.GetInfo(criteria.TenantId, criteria.Id,
                    criteriaTranslationMeta.LanguageId);
                if (criteriaTranslation == null)
                {
                    await _criteriaTransaltionRepository.Insert(new CriteriaTranslation
                    {
                        TenantId = criteria.TenantId,
                        CriteriaId = criteria.Id,
                        Description = criteriaTranslationMeta.Description?.Trim(),
                        Name = criteriaTranslationMeta.Name.Trim(),
                        UnsignName = criteriaTranslationMeta.Name.Trim().StripVietnameseChars().ToUpper(),
                        LanguageId = criteriaTranslationMeta.LanguageId,
                        Sanction = criteriaTranslationMeta.Sanction?.Trim()
                    });
                }
                else
                {
                    criteriaTranslation.Name = criteriaTranslationMeta.Name.Trim();
                    criteriaTranslation.UnsignName = criteriaTranslation.Name.StripVietnameseChars().ToUpper();
                    criteriaTranslation.Description = criteriaTranslationMeta.Description?.Trim();
                    criteriaTranslation.Sanction = criteriaTranslationMeta.Sanction?.Trim();
                    await _criteriaTransaltionRepository.Update(criteriaTranslation);
                }
            }

            return new ActionResultResponse(1,
                _sharedResourceService.GetString(SuccessMessage.UpdateSuccessful,
                    _resourceService.GetString("Criteria")));
        }

        public async Task<ActionResultResponse> Delete(string tenantId, string id)
        {
            // TODO: Check điều kiện xóa tiêu chí.
            // Kiểm tra tiêu chí có đang được sử dụng hay không.
            var isUsed = await _userAssessmentCriteriaRepository.CheckExistsByCriteriaId(id);
            if (isUsed)
                return new ActionResultResponse(-1,
                    _sharedResourceService.GetString(ErrorMessage.AlreadyUsedBy, _resourceService.GetString("Criteria"),
                        _resourceService.GetString("User assessment")));

            var result = await _criteriaRepository.Delete(tenantId, id);
            if (result < 0)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            // Xóa ngôn ngữ tiêu chí
            await _criteriaTransaltionRepository.DeleteByCriteriaId(tenantId, id);

            return new ActionResultResponse(result,
                _sharedResourceService.GetString(SuccessMessage.DeleteSuccessful,
                    _resourceService.GetString("Criteria")));
        }

        public async Task<ActionResultResponse<Criteria>> GetDetail(string tenantId, string id)
        {
            var criteria = await _criteriaRepository.GetInfo(tenantId, id, true);
            if (criteria == null)
                return new ActionResultResponse<Criteria>(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound, _resourceService.GetString("Criteria")));

            criteria.Translations = await _criteriaTransaltionRepository.GetsByCriteriaId(tenantId, id);
            return new ActionResultResponse<Criteria>
            {
                Data = criteria
            };
        }

        public async Task<SearchResult<Suggestion<string>>> GetsAllCriteriaGroup(string tenantId, string languageId)
        {
            var result = await _criteriaGroupRepository.Suggestion(tenantId, languageId);
            return new SearchResult<Suggestion<string>>(result);

        }

        public async Task<SearchResult<CriteriaViewModel>> Search(string tenantId, string languageId, string keyword, string groupId, bool? isActive, int page, int pageSize)
        {
            var result = await _criteriaRepository.Search(tenantId, languageId, keyword, groupId, isActive, page, pageSize,
                out int totalRows);
            return new SearchResult<CriteriaViewModel>
            {
                Items = result,
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse> InsertGroup(CriteriaGroupMeta criteriaGroupMeta)
        {
            // Kiểm tra tên nhóm có tồn tại không.
            var criteriaGroup = new CriteriaGroup
            {
                TenantId = criteriaGroupMeta.TenantId,
                IsActive = criteriaGroupMeta.IsActive,
                Order = criteriaGroupMeta.Order
            };

            foreach (var criteriaGroupTranslation in criteriaGroupMeta.Translations)
            {
                var isExists = await _criteriaGroupTranslationRepository
                    .CheckExists(criteriaGroup.TenantId, criteriaGroup.Id, criteriaGroupTranslation.LanguageId,
                    criteriaGroupTranslation.Name);
                if (isExists)
                    return new ActionResultResponse(-1,
                        _sharedResourceService.GetString(ErrorMessage.AlreadyExists,
                            _resourceService.GetString("criteria group name")));

                criteriaGroupTranslation.TenantId = criteriaGroup.TenantId;
                criteriaGroupTranslation.Name = criteriaGroupTranslation.Name.Trim();
                criteriaGroupTranslation.UnsignName = criteriaGroupTranslation.Name.StripVietnameseChars().ToUpper();
                criteriaGroup.Translations.Add(criteriaGroupTranslation);
            }

            var result = await _criteriaGroupRepository.Insert(criteriaGroup);
            return new ActionResultResponse(result, result <= 0
                ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _sharedResourceService.GetString(SuccessMessage.AddSuccessful, _resourceService.GetString("criteria group")));
        }

        public async Task<ActionResultResponse> UpdateGroup(string id, CriteriaGroupMeta criteriaGroupMeta)
        {
            var criteriaGroup = await _criteriaGroupRepository.GetInfo(criteriaGroupMeta.TenantId, id);
            if (criteriaGroup == null)
                return new ActionResultResponse(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound,
                        _resourceService.GetString("criteria group")));

            criteriaGroup.IsActive = criteriaGroupMeta.IsActive;
            criteriaGroup.Order = criteriaGroupMeta.Order;

            await _criteriaGroupRepository.Update(criteriaGroup);
            foreach (var criteriaGroupTranslation in criteriaGroupMeta.Translations)
            {
                // Kiểm tra tên có tồn tại không.
                var isExists = await _criteriaGroupTranslationRepository
                    .CheckExists(criteriaGroup.TenantId, criteriaGroup.Id, criteriaGroupTranslation.LanguageId,
                        criteriaGroupTranslation.Name);
                if (isExists)
                    return new ActionResultResponse(-2,
                        _sharedResourceService.GetString(ErrorMessage.AlreadyExists,
                            _resourceService.GetString("criteria group name")));

                var criteriaGroupTranslationInfo =
                    await _criteriaGroupTranslationRepository.GetInfo(criteriaGroup.TenantId, id,
                        criteriaGroupTranslation.LanguageId);
                if (criteriaGroupTranslationInfo == null)
                {
                    criteriaGroupTranslationInfo = new CriteriaGroupTranslation
                    {
                        CriteriaGroupId = id,
                        Name = criteriaGroupTranslation.Name.Trim(),
                        UnsignName = criteriaGroupTranslation.Name.Trim().StripVietnameseChars().ToUpper(),
                        TenantId = criteriaGroup.TenantId,
                        Description = criteriaGroupTranslation.Description?.Trim(),
                        LanguageId = criteriaGroupTranslation.LanguageId
                    };
                    var resultInsert = await _criteriaGroupTranslationRepository.Insert(criteriaGroupTranslationInfo);
                    if (resultInsert <= 0)
                        return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
                    criteriaGroup.Translations.Add(criteriaGroupTranslationInfo);
                }
                else
                {
                    criteriaGroupTranslationInfo.Name = criteriaGroupTranslation.Name.Trim();
                    criteriaGroupTranslationInfo.UnsignName = criteriaGroupTranslation.Name.Trim().StripVietnameseChars().ToUpper();
                    criteriaGroupTranslationInfo.Description = criteriaGroupTranslation.Description?.Trim();
                    await _criteriaGroupTranslationRepository.Update(criteriaGroupTranslationInfo);
                }
            }

            return new ActionResultResponse
            {
                Code = 1,
                Message = _sharedResourceService.GetString(SuccessMessage.UpdateSuccessful,
                    _resourceService.GetString("Criteria group"))
            };
        }

        public async Task<ActionResultResponse> DeleteGroup(string tenantId, string userId, string fullName, string id)
        {
            // Kiểm tra nhóm có đang được sử dụng bởi tiêu chí không.
            var isUsed = await _criteriaRepository.CheckExistsByGroupId(tenantId, id);
            if (isUsed)
                return new ActionResultResponse(-1,
                    _sharedResourceService.GetString(ErrorMessage.AlreadyUsedBy,
                        _resourceService.GetString("Criteria group"),
                        _resourceService.GetString("Criteria")));

            // Xóa nhóm tiêu chí.
            var result = await _criteriaGroupRepository.Delete(tenantId, id);
            if (result > 0)
            {
                // Xóa đa ngôn ngữ.
                await _criteriaGroupTranslationRepository.DeleteByCriteriaGroupId(tenantId, id);
            }
            return new ActionResultResponse(result,
                result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _sharedResourceService.GetString(SuccessMessage.DeleteSuccessful, _resourceService.GetString("criteria group")));
        }

        public async Task<SearchResult<CriteriaGroupViewModel>> SearchGroup(string tenantId, string languageId,
            string keyword, bool? isActive, int page, int pageSize)
        {
            var items = await _criteriaGroupRepository.Search(tenantId, languageId, keyword, isActive, page, pageSize,
                out int totalRows);
            return new SearchResult<CriteriaGroupViewModel>(items, totalRows);
        }

        public async Task<ActionResultResponse<CriteriaGroup>> GetGroupDetail(string tenantId, string id)
        {
            var criteria = await _criteriaGroupRepository.GetInfo(tenantId, id, true);
            if (criteria == null)
                return new ActionResultResponse<CriteriaGroup>(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound,
                        _resourceService.GetString("Criteria group")));

            criteria.Translations = await _criteriaGroupTranslationRepository.GetsAllByCriteriaGroupId(tenantId, id, true);
            return new ActionResultResponse<CriteriaGroup>
            {
                Data = criteria
            };
        }
    }
}
