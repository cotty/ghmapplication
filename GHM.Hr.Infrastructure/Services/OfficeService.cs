﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.Services;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Infrastructure.Services
{
    public class OfficeService : IOfficeService
    {
        private readonly IOfficeRepository _officeRepository;
        private readonly IOfficeTranslationRepository _officeTranslationRepository;
        private readonly IOfficeContactService _officeContactService;
        private readonly IOfficeContactRepository _officeContactRepository;
        private readonly IOfficePositionRepository _officePositionRepository;
        private readonly IUserPositionRepository _userPositionRepository;
        private readonly IUserTranslationRepository _userTranslationRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmHrResource> _hrResourceService;

        public OfficeService(IOfficeRepository officeRepository, IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmHrResource> hrResourceService, IOfficeTranslationRepository officeTranslationRepository,
            IOfficeContactService officeContactService, IOfficeContactRepository officeContactRepository, IOfficePositionRepository officePositionRepository,
            IUserPositionRepository userPositionRepository, IUserTranslationRepository userTranslationRepository)
        {
            _officeRepository = officeRepository;
            _sharedResourceService = sharedResourceService;
            _hrResourceService = hrResourceService;
            _officeTranslationRepository = officeTranslationRepository;
            _officeContactService = officeContactService;
            _officeContactRepository = officeContactRepository;
            _officePositionRepository = officePositionRepository;
            _userPositionRepository = userPositionRepository;
            _userTranslationRepository = userTranslationRepository;
        }

        public async Task<List<OfficeSearchViewModel>> GetsAll(string tenantId, string languageId)
        {
            return await _officeRepository.GetAllActivatedOffice(tenantId, languageId);
        }

        public async Task<SearchResult<OfficeSearchViewModel>> Search(string currentUserId, string tenantId, string languageId, string keyword,
            bool? isActive, int page, int pageSize)
        {
            var items = await _officeRepository.Search(tenantId, languageId, keyword, isActive, page, pageSize,
                out var totalRows);
            return new SearchResult<OfficeSearchViewModel>
            {
                TotalRows = totalRows,
                Items = items
            };
        }

        public async Task<SearchResult<OfficeSuggestionViewModel>> SearchForSuggestion(string tenantId, string languageId, string keyword, int page, int pageSize)
        {
            return new SearchResult<OfficeSuggestionViewModel>
            {
                Items = await _officeRepository.SearchForSuggestion(tenantId, languageId, keyword, page, pageSize, out var totalRows),
                TotalRows = totalRows
            };
        }

        public Task<SearchResult<OfficeSearchViewModel>> SearchTree(string currentUserId, string tenantId,
            string languageId, string keyword, bool? isActive, int page, int pageSize)
        {
            throw new NotImplementedException();
        }

        public async Task<List<TreeData>> GetFullOfficeTree(string tenantId, string languageId)
        {
            var offices = await _officeRepository.GetAllActivatedOffice(tenantId, languageId);
            if (offices == null || !offices.Any())
                return null;

            var tree = RenderTree(offices, null);
            return tree;
        }

        public async Task<List<TreeData>> GetOfficeUserTree(string tenantId, string userId, string languageId)
        {
            var currentUserInfo = await new HttpClientService().GetUserInfo(tenantId, userId);
            if (currentUserInfo == null)
                return null;

            var officeInfo = await _officeRepository.GetInfo(currentUserInfo.OfficeId, true);
            if (officeInfo == null)
                return null;

            var offices = await _officeRepository.GetOfficeUserTree(tenantId, languageId, currentUserInfo.OfficeIdPath);
            if (offices == null || !offices.Any())
                return null;

            var tree = RenderTree(offices, officeInfo.ParentId);
            return tree;
        }

        public async Task<ActionResultResponse> Insert(string tenantId, OfficeMeta officeMeta)
        {
            // Check code exists.
            var isCodeExists = await _officeRepository.CheckExistsByCode(-1, officeMeta.Code);
            if (isCodeExists)
                return new ActionResultResponse(-2,
                    _hrResourceService.GetString("Office code: \"{0}\" already used by another office. Please try again.", officeMeta.Code));

            if (officeMeta.OfficeType != OfficeType.Company && !officeMeta.ParentId.HasValue)
                return new ActionResultResponse(-3,
                    _hrResourceService.GetString("Please select office type is company or select this office belong to a company."));

            var office = new Office
            {
                Code = officeMeta.Code.Trim(),
                IdPath = "-1",
                IsActive = officeMeta.IsActive,
                OfficeType = officeMeta.OfficeType,
                Order = officeMeta.Order,
                Status = OfficeStatus.Normal,
                TenantId = tenantId,
                RootId = officeMeta.OfficeType == OfficeType.Company ? -1 : 0,
                RootIdPath = officeMeta.OfficeType == OfficeType.Company ? "-1" : "0",
                OrderPath = officeMeta.Order.ToString()
            };

            if (officeMeta.ParentId.HasValue)
            {
                var parentInfo = await _officeRepository.GetInfo(officeMeta.ParentId.Value);
                if (parentInfo == null)
                    return new ActionResultResponse(-4, _hrResourceService.GetString("Parent office does not exists. Please try again."));

                office.ParentId = parentInfo.Id;
                office.IdPath = $"{parentInfo.IdPath}.-1";
                office.RootId = officeMeta.OfficeType != OfficeType.Company
                    ? parentInfo.RootId
                    : -1;
                office.RootIdPath = officeMeta.OfficeType != OfficeType.Company
                    ? $"{parentInfo.RootIdPath}"
                    : $"{parentInfo.RootIdPath}.-1";
            }

            var result = await _officeRepository.Insert(office);
            if (result <= 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            #region Update RootId and RootIdPath.
            if (office.OfficeType == OfficeType.Company)
            {
                office.RootId = office.Id;
                office.RootIdPath = office.RootIdPath.Replace("-1", office.Id.ToString());
                await _officeRepository.UpdateOfficeRoot(office.Id, office.RootId, office.RootIdPath);
            }
            #endregion

            #region Update current office idPath.            
            office.Level = !string.IsNullOrEmpty(office.RootIdPath) ? office.RootIdPath.Split(".").Length : 1;
            office.IdPath = office.IdPath.Replace("-1", office.Id.ToString());
            await _officeRepository.UpdateOfficeIdPath(office.Id, office.IdPath);
            #endregion

            #region Update parent office child count.
            if (office.ParentId.HasValue)
            {
                var childCount = await _officeRepository.GetChildCount(office.ParentId.Value);
                await _officeRepository.UpdateChildCount(office.ParentId.Value, childCount);
            }
            #endregion

            #region Insert office translation.
            var officeTranslations = new List<OfficeTranslation>();
            foreach (var officeTranslationMeta in officeMeta.Translations)
            {
                // check name exists.
                var isNameExists = await _officeTranslationRepository.CheckExistsByName(office.RootId, office.Id,
                    officeTranslationMeta.LanguageId, officeTranslationMeta.Name);
                if (isNameExists)
                {
                    await RollbackInsert(office.Id);
                    return new ActionResultResponse(-5,
                        _hrResourceService.GetString("Office name: \"{0}\" already taken by another office. Please try again.", officeTranslationMeta.Name));
                }

                var isShortNameExists = await _officeTranslationRepository.CheckExistsByShortName(office.RootId, office.Id,
                    officeTranslationMeta.LanguageId, officeTranslationMeta.ShortName);
                if (isShortNameExists)
                {
                    await RollbackInsert(office.Id);
                    return new ActionResultResponse(-6,
                        _hrResourceService.GetString("Short name: \"{0}\" already taken by another office. Please try again.", officeTranslationMeta.ShortName));
                }

                var officeTranslation = new OfficeTranslation
                {
                    Name = officeTranslationMeta.Name.Trim(),
                    ShortName = officeTranslationMeta.ShortName.Trim(),
                    Address = officeTranslationMeta.Address?.Trim(),
                    Description = officeTranslationMeta.Description?.Trim(),
                    UnsignName = officeTranslationMeta.Name.Trim().StripVietnameseChars().ToUpper(),
                    LanguageId = officeTranslationMeta.LanguageId,
                    NamePath = officeTranslationMeta.Name.Trim().ToUrlString(),
                    OfficeId = office.Id,
                    RootId = office.RootId
                };

                if (officeMeta.ParentId.HasValue)
                {
                    var parentInfo = await _officeTranslationRepository.GetInfo(officeMeta.ParentId.Value,
                        officeTranslationMeta.LanguageId, true);
                    if (parentInfo != null)
                    {
                        officeTranslation.ParentName = parentInfo.Name;
                    }
                }

                officeTranslations.Add(officeTranslation);
            }
            var resultInsertTranslation = await _officeTranslationRepository.Inserts(officeTranslations);
            if (resultInsertTranslation <= 0)
            {
                await RollbackInsert(office.Id);
                return new ActionResultResponse(resultInsertTranslation, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
            }
            #endregion

            #region Inserts office contacts.
            if (officeMeta.OfficeContacts.Any())
            {
                officeMeta.OfficeContacts.ForEach(contact =>
                {
                    contact.OfficeId = office.Id;
                });
                var resultInsertOfficeContact = await _officeContactService.Inserts(officeMeta.OfficeContacts);
                if (resultInsertOfficeContact.Any(x => x.Code <= 0))
                {
                    await RollbackInsert(office.Id);
                    await RollbackInsertOfficeTranslation(office.Id);
                    await RollbackInsertOfficeContact(office.Id);
                    return new ActionResultResponse(-7, resultInsertOfficeContact.FirstOrDefault()?.Message);
                }
            }
            #endregion
            return new ActionResultResponse(result, _hrResourceService.GetString("Add new office successful."));
        }

        public async Task<ActionResultResponse> Update(string tenantId, int officeId, OfficeMeta officeMeta)
        {
            //if (!officeMeta.OfficeTranslations.Any())
            //    return new ActionResultResponse(-1, _hrResourceService.GetString("Please select at least one language."));

            var officeInfo = await _officeRepository.GetInfo(officeId);
            if (officeInfo == null)
                return new ActionResultResponse(-2, _hrResourceService.GetString("Office info does not exists. Please try again."));

            if (officeInfo.TenantId != tenantId)
                return new ActionResultResponse(-3, _sharedResourceService.GetString("You do not have permission to do this action."));

            var isCodeExists = await _officeRepository.CheckExistsByCode(officeId, officeMeta.Code);
            if (isCodeExists)
                return new ActionResultResponse(-4, _hrResourceService.GetString("Office code already exists. Please try again."));

            if (officeMeta.ParentId.HasValue && officeInfo.Id == officeMeta.ParentId.Value)
                return new ActionResultResponse(-5, _hrResourceService.GetString("Office and parent office can not be the same.")); ;

            var oldParentId = officeInfo.ParentId;
            var oldIdPath = officeInfo.IdPath;

            officeInfo.Code = officeMeta.Code.Trim();
            officeInfo.IsActive = officeMeta.IsActive;
            officeInfo.OfficeType = officeMeta.OfficeType;
            officeInfo.Order = officeMeta.Order;
            officeInfo.OfficeType = officeMeta.OfficeType;
            officeInfo.LastUpdate = DateTime.Now;

            if (officeInfo.ParentId.HasValue && !officeMeta.ParentId.HasValue)
            {
                if (officeMeta.OfficeType != OfficeType.Company)
                    return new ActionResultResponse(-6,
                        _hrResourceService.GetString("Please select office type is company or select this office belong to a company."));

                officeInfo.ParentId = null;
                officeInfo.RootId = officeInfo.Id;
                officeInfo.RootIdPath = officeInfo.Id.ToString();
                officeInfo.IdPath = officeInfo.Id.ToString();
                officeInfo.OrderPath = officeInfo.Order.ToString();
            }
            else if (officeMeta.ParentId.HasValue && officeMeta.ParentId != officeInfo.ParentId)
            {
                var parentInfo = await _officeRepository.GetInfo(officeMeta.ParentId.Value);
                if (parentInfo == null)
                    return new ActionResultResponse(-6, _hrResourceService.GetString("Parent office does not exists. Please try again.")); ;

                officeInfo.IdPath = $"{parentInfo.IdPath}.{officeInfo.Id}";
                officeInfo.ParentId = parentInfo.Id;
                officeInfo.OrderPath = $"{parentInfo.OrderPath}.{officeInfo.Order}";
                officeInfo.RootId = officeInfo.OfficeType == OfficeType.Company
                    ? officeInfo.Id
                    : parentInfo.RootId;
                officeInfo.RootIdPath = officeInfo.OfficeType == OfficeType.Company
                    ? $"{parentInfo.RootIdPath}.{officeInfo.Id}"
                    : parentInfo.RootIdPath;
            }

            await _officeRepository.Update(officeInfo);

            // Update children IdPath and RootInfo
            if (officeInfo.IdPath != oldIdPath)
            {
                await _officeRepository.UpdateChildrenIdPath(oldIdPath, officeInfo.IdPath);
            }

            // Update parent office child count.
            if (officeInfo.ParentId.HasValue && oldParentId.HasValue && officeInfo.ParentId.Value != oldParentId.Value)
            {
                // Update old parent office child count.
                var oldChildCount = await _officeRepository.GetChildCount(oldParentId.Value);
                await _officeRepository.UpdateChildCount(oldParentId.Value, oldChildCount);

                // Update new parent office child count.
                var newParentId = officeInfo.ParentId.Value;
                var newChildCount = await _officeRepository.GetChildCount(newParentId);
                await _officeRepository.UpdateChildCount(newParentId, newChildCount);
            }

            // Update office translation.
            var resultUpdateTranslation = await UpdateOfficeTranslation();
            if (resultUpdateTranslation.Code <= 0)
                return new ActionResultResponse(resultUpdateTranslation.Code, resultUpdateTranslation.Message);

            // Update office dependency (UserPosition)
            if (oldIdPath != officeInfo.IdPath)
                await _userPositionRepository.UpdateOfficeIdPath(oldIdPath, officeInfo.IdPath);

            return new ActionResultResponse(1, _hrResourceService.GetString("Update office successful."));

            #region Local functions
            async Task<ActionResultResponse> UpdateOfficeTranslation()
            {
                foreach (var officeTranslationMeta in officeMeta.Translations)
                {
                    // check name exists.
                    var isNameExists = await _officeTranslationRepository.CheckExistsByName(officeInfo.RootId, officeInfo.Id,
                        officeTranslationMeta.LanguageId, officeTranslationMeta.Name);
                    if (isNameExists)
                    {
                        return new ActionResultResponse(-4,
                            _hrResourceService.GetString("Office name: \"{0}\" already taken by another office. Please try again.", officeTranslationMeta.Name));
                    }

                    var isShortNameExists = await _officeTranslationRepository.CheckExistsByShortName(officeInfo.RootId, officeInfo.Id,
                        officeTranslationMeta.LanguageId, officeTranslationMeta.ShortName);
                    if (isShortNameExists)
                    {
                        return new ActionResultResponse(-4,
                            _hrResourceService.GetString("Short name: \"{0}\" already taken by another office. Please try again.", officeTranslationMeta.ShortName));
                    }

                    OfficeTranslation parentOfficeInfoByLanguage = null;
                    if (officeInfo.ParentId.HasValue)
                    {
                        parentOfficeInfoByLanguage = await _officeTranslationRepository.GetInfo(officeInfo.ParentId.Value,
                            officeTranslationMeta.LanguageId, true);
                    }

                    var officeTranslationInfo =
                        await _officeTranslationRepository.GetInfo(officeInfo.Id, officeTranslationMeta.LanguageId);

                    if (officeTranslationInfo == null)
                    {
                        officeTranslationInfo = new OfficeTranslation
                        {
                            Name = officeTranslationMeta.Name.Trim(),
                            ShortName = officeTranslationMeta.ShortName.Trim(),
                            Address = officeTranslationMeta.Address?.Trim(),
                            Description = officeTranslationMeta.Description?.Trim(),
                            UnsignName = officeTranslationMeta.Name.Trim().StripVietnameseChars().ToUpper(),
                            LanguageId = officeTranslationMeta.LanguageId,
                            NamePath = parentOfficeInfoByLanguage != null
                                ? $"{parentOfficeInfoByLanguage.NamePath}/{officeTranslationMeta.Name.Trim().ToUrlString()}"
                                : officeTranslationMeta.Name.Trim().ToUrlString(),
                            OfficeId = officeInfo.Id,
                            RootId = officeInfo.RootId
                        };
                        await _officeTranslationRepository.Insert(officeTranslationInfo);
                    }
                    else
                    {
                        officeTranslationInfo.Name = officeTranslationMeta.Name.Trim();
                        officeTranslationInfo.ShortName = officeTranslationMeta.ShortName.Trim();
                        officeTranslationInfo.Address = officeTranslationMeta.Address?.Trim();
                        officeTranslationInfo.Description = officeTranslationMeta.Description?.Trim();
                        officeTranslationInfo.UnsignName = officeTranslationMeta.Name.StripVietnameseChars().ToUpper();
                        officeTranslationInfo.ParentName =
                            parentOfficeInfoByLanguage != null
                                ? parentOfficeInfoByLanguage.Name
                                : string.Empty;
                        officeTranslationInfo.NamePath = parentOfficeInfoByLanguage != null
                            ? $"{parentOfficeInfoByLanguage.NamePath}/{officeTranslationMeta.Name.Trim().ToUrlString()}"
                            : officeTranslationMeta.Name.Trim().ToUrlString();
                        officeTranslationInfo.RootId = officeInfo.RootId;
                        var resultUpdate = await _officeTranslationRepository.Update(officeTranslationInfo);

                        if (resultUpdate > 0)
                            await _userTranslationRepository.UpdateOfficeNameByOfficeId(tenantId, officeId, officeTranslationMeta.Name, officeTranslationMeta.LanguageId);
                    }
                }

                return new ActionResultResponse(1,
                    _hrResourceService.GetString("Update office successful."));
            }
            #endregion
        }


        public async Task<ActionResultResponse> Delete(string tenantId, int id)
        {
            var officeInfo = await _officeRepository.GetInfo(id);
            if (officeInfo == null)
                return new ActionResultResponse(-1, _hrResourceService.GetString("Office does not exists. Please try again."));

            if (officeInfo.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString("You do not have permission to to this action."));

            // Check is has child.
            if (officeInfo.ChildCount > 0)
                return new ActionResultResponse(-3, _hrResourceService.GetString("This office has children office. You can not delete this office."));

            // Check is use in office position.
            var isUsedByOfficePosition = await _officePositionRepository.CheckExistsByOfficeId(officeInfo.Id);
            if (isUsedByOfficePosition)
                return new ActionResultResponse(-4, _hrResourceService.GetString("Office is in used by position. You can not delete this office."));

            // Check is used by user.
            var isUsedByUser = await _userPositionRepository.CheckExistsByOfficeId(officeInfo.Id);
            if (isUsedByUser)
                return new ActionResultResponse(-5, _hrResourceService.GetString("Office is in used by user. You can not delete this office."));

            var result = await _officeRepository.Delete(officeInfo.Id);
            if (result > 0 && officeInfo.ParentId.HasValue)
            {
                // Update parent office child count.
                var childCount = await _officeRepository.GetChildCount(officeInfo.ParentId.Value);
                await _officeRepository.UpdateChildCount(officeInfo.ParentId.Value, childCount);
                await _officeTranslationRepository.ForceDeleteByOfficeId(id);
            }

            return new ActionResultResponse(result, result > 0
                ? _hrResourceService.GetString("Delete office successful.")
                : _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
        }

        public async Task<ActionResultResponse<OfficeDetailViewModel>> GetDetail(string tenantId, string languageId, int id)
        {
            var officeInfo = await _officeRepository.GetInfoByLanguage(tenantId, id, languageId);
            if (officeInfo == null)
                return new ActionResultResponse<OfficeDetailViewModel>(-1, _hrResourceService.GetString("Office does not exists."));

            officeInfo.OfficeContacts = await _officeContactRepository.GetsByOfficeId(officeInfo.Id, languageId);
            return new ActionResultResponse<OfficeDetailViewModel>
            {
                Code = 1,
                Data = officeInfo
            };
        }

        public async Task<ActionResultResponse<OfficeEditViewModel>> GetDetailForEdit(string tenantId, string languageId, int id)
        {
            var officeInfo = await _officeRepository.GetInfo(id, true);
            if (officeInfo == null)
                return new ActionResultResponse<OfficeEditViewModel>(-1, _hrResourceService.GetString("Office does not exists."));

            if (officeInfo.TenantId != tenantId)
                return new ActionResultResponse<OfficeEditViewModel>(-2, _hrResourceService.GetString("You do not have permission to view this office detail."));

            var officeDetail = new OfficeEditViewModel
            {
                Code = officeInfo.Code,
                IsActive = officeInfo.IsActive,
                ParentId = officeInfo.ParentId,
                Order = officeInfo.Order,
                OfficeType = officeInfo.OfficeType,
                Id = officeInfo.Id,
                Status = officeInfo.Status
            };

            officeDetail.OfficeContacts = await _officeContactRepository.GetsByOfficeId(officeInfo.Id, languageId);
            officeDetail.OfficeTranslations = await _officeTranslationRepository.GetsByOfficeId(id);
            return new ActionResultResponse<OfficeEditViewModel>
            {
                Code = 1,
                Data = officeDetail
            };
        }

        public async Task<ActionResultResponse<string>> InsertContact(string tenantId, int officeId, OfficeContactMeta officeContactMeta)
        {
            var officeInfo = await _officeRepository.GetInfo(officeId, true);
            if (officeInfo == null)
                return new ActionResultResponse<string>(-1, _hrResourceService.GetString("Office does not exists."));

            if (officeInfo.TenantId != tenantId)
                return new ActionResultResponse<string>(-2, _sharedResourceService.GetString("You do not have permission to to this action."));

            officeContactMeta.OfficeId = officeId;
            return await _officeContactService.Insert(officeContactMeta);
        }

        public async Task<ActionResultResponse> UpdateContact(string tenantId, int officeId, string contactId, OfficeContactMeta officeContactMeta)
        {
            var officeInfo = await _officeRepository.GetInfo(officeId, true);
            if (officeInfo == null)
                return new ActionResultResponse(-1, _hrResourceService.GetString("Office does not exists."));

            if (officeInfo.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString("You do not have permission to to this action."));

            return await _officeContactService.Update(contactId, officeContactMeta);
        }

        public async Task<ActionResultResponse> DeleteContact(string tenantId, int officeId, string contactId)
        {
            var officeInfo = await _officeRepository.GetInfo(officeId, true);
            if (officeInfo == null)
                return new ActionResultResponse(-1, _hrResourceService.GetString("Office does not exists."));

            if (officeInfo.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString("You do not have permission to to this action."));

            var result = await _officeContactRepository.Delete(contactId);
            return new ActionResultResponse(result, result == -1
                ? _hrResourceService.GetString("Office does not exists.")
                : result == 0 ? _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                : _hrResourceService.GetString("Delete contact successful."));
        }

        private async Task RollbackInsert(int officeId)
        {
            await _officeRepository.ForceDelete(officeId);
        }

        private async Task RollbackInsertOfficeTranslation(int officeId)
        {
            await _officeTranslationRepository.ForceDeleteByOfficeId(officeId);
        }

        private async Task RollbackInsertOfficeContact(int officeId)
        {
            await _officeContactRepository.DeleteByOfficeId(officeId);
        }

        private List<TreeData> RenderTree(List<OfficeSearchViewModel> offices, int? parentId)
        {
            var tree = new List<TreeData>();
            var parentOffices = offices.Where(x => x.ParentId == parentId).ToList();
            if (parentOffices.Any())
            {
                parentOffices.ForEach(office =>
                {
                    var treeData = new TreeData
                    {
                        Id = office.Id,
                        Text = office.Name,
                        ParentId = office.ParentId,
                        IdPath = office.IdPath,
                        Data = office,
                        ChildCount = office.ChildCount,
                        Icon = String.Empty,
                        State = new GHM.Infrastructure.Models.State(),
                        Children = RenderTree(offices, office.Id)
                    };
                    tree.Add(treeData);
                });
            }
            return tree;
        }
    }
}
