﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Infrastructure.Services
{
    public class CriteriaGroupService : ICriteriaGroupService
    {
        private readonly ICriteriaGroupRepository _criteriaGroupRepository;
        private readonly ICriteriaGroupTranslationRepository _criteriaGroupTranslationRepository;
        private readonly ICriteriaRepository _criteriaRepository;

        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmHrResource> _resourceService;

        public CriteriaGroupService(ICriteriaGroupRepository criteriaGroupRepository,
            ICriteriaGroupTranslationRepository criteriaGroupTranslationRepository, IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmHrResource> resourceService, ICriteriaRepository criteriaRepository)
        {
            _criteriaGroupRepository = criteriaGroupRepository;
            _criteriaGroupTranslationRepository = criteriaGroupTranslationRepository;
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
            _criteriaRepository = criteriaRepository;
        }

        public async Task<ActionResultResponse> Insert(CriteriaGroupMeta criteriaGroupMeta)
        {
            var criteriaGroup = new CriteriaGroup
            {
                IsActive = criteriaGroupMeta.IsActive,
                TenantId = criteriaGroupMeta.TenantId,
                Order = criteriaGroupMeta.Order
            };
            foreach (var translation in criteriaGroupMeta.Translations)
            {
                var isExists = await _criteriaGroupTranslationRepository.CheckExists(criteriaGroupMeta.TenantId,
                    translation.LanguageId, translation.Name);
                if (isExists)
                    return new ActionResultResponse(-1,
                        _sharedResourceService.GetString(ErrorMessage.AlreadyExists, _resourceService.GetString("criteria group name")));

                criteriaGroup.Translations.Add(translation);
            }

            var result = await _criteriaGroupRepository.Insert(criteriaGroup);
            return new ActionResultResponse(result, result <= 0
                                                    ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                    : _sharedResourceService.GetString(SuccessMessage.AddSuccessful, _resourceService.GetString("criteria group name")));
        }

        public async Task<ActionResultResponse> Update(string id, CriteriaGroupMeta criteriaGroupMeta)
        {
            var criteriaGroup = await _criteriaGroupRepository.GetInfo(criteriaGroupMeta.TenantId, id);
            if (criteriaGroup == null)
                return new ActionResultResponse(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound,
                        _resourceService.GetString("criteria group")));

            if (criteriaGroup.ConcurrencyStamp != criteriaGroupMeta.ConcurrencyStamp)
                return new ActionResultResponse(-2,
                    _sharedResourceService.GetString(ErrorMessage.AlreadyUpdatedByAnother,
                        _resourceService.GetString("criteria group")));

            criteriaGroup.IsActive = criteriaGroupMeta.IsActive;
            criteriaGroup.ConcurrencyStamp = Guid.NewGuid().ToString();

            await _criteriaGroupRepository.Update(criteriaGroup);

            var criteriaGroupTranslations = await _criteriaGroupTranslationRepository.GetsAllByCriteriaGroupId(
                criteriaGroupMeta.TenantId,
                criteriaGroup.Id);

            foreach (var translation in criteriaGroupMeta.Translations)
            {
                var criteriaGroupTranslation = criteriaGroupTranslations.FirstOrDefault(x =>
                    x.TenantId == criteriaGroupMeta.TenantId
                    && x.CriteriaGroupId == criteriaGroup.Id && x.LanguageId == translation.LanguageId);
                if (criteriaGroupTranslation == null)
                {
                    translation.CriteriaGroupId = criteriaGroup.Id;
                    await _criteriaGroupTranslationRepository.Insert(translation);
                }
                else
                {
                    criteriaGroupTranslation.Name = translation.Name;
                    criteriaGroupTranslation.UnsignName = translation.Name.StripVietnameseChars().ToUpper();
                    criteriaGroupTranslation.Description = translation.Description;
                }
            }

            await _criteriaGroupTranslationRepository.Updates(criteriaGroupTranslations);

            return new ActionResultResponse(1,
                _sharedResourceService.GetString(SuccessMessage.UpdateSuccessful,
                    _resourceService.GetString("criteria group")));
        }

        public async Task<ActionResultResponse> Delete(string tenantId, string id)
        {
            var isUsedByCriteria = await _criteriaRepository.CheckExistsByGroupId(tenantId, id);
            if (isUsedByCriteria)
                return new ActionResultResponse(-1,
                    _sharedResourceService.GetString(ErrorMessage.AlreadyUsedBy,
                        _resourceService.GetString("criteria group"),
                        _resourceService.GetString("criteria")));

            var result = await _criteriaGroupRepository.Delete(tenantId, id);
            if (result <= 0)
                return new ActionResultResponse(-2,
                    _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            var resultDeleteTranslation = await _criteriaGroupTranslationRepository.DeleteByCriteriaGroupId(tenantId, id);
            return new ActionResultResponse(resultDeleteTranslation, resultDeleteTranslation <= 0
                    ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                    : _sharedResourceService.GetString(SuccessMessage.DeleteSuccessful, _resourceService.GetString("criteria group")));
        }

        public async Task<ActionResultResponse<CriteriaGroup>> GetGroupDetail(string tenantId, string id)
        {
            var criteria = await _criteriaGroupRepository.GetInfo(tenantId, id, true);
            if (criteria == null)
                return new ActionResultResponse<CriteriaGroup>(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound,
                        _resourceService.GetString("Criteria group")));

            criteria.Translations = await _criteriaGroupTranslationRepository.GetsAllByCriteriaGroupId(tenantId, id, true);
            return new ActionResultResponse<CriteriaGroup>
            {
                Data = criteria
            };
        }

        public async Task<SearchResult<CriteriaGroupViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive,
            int page, int pageSize)
        {
            var result = await _criteriaGroupRepository.Search(tenantId, languageId, keyword, isActive, page, pageSize,
                out int totalRows);
            return new SearchResult<CriteriaGroupViewModel>(result, totalRows);
        }
    }
}
