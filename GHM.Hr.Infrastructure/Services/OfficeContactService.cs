﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.Services;
using GHM.Infrastructure.ViewModels;
using Microsoft.Extensions.Configuration;

namespace GHM.Hr.Infrastructure.Services
{
    public class OfficeContactService : IOfficeContactService
    {
        private readonly IOfficeContactRepository _officeContactRepository;
        private readonly IUserRepository _userRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmHrResource> _hrResourceService;
        private readonly IConfiguration _configuration;

        public OfficeContactService(IOfficeContactRepository officeContactRepository,
            IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> hrResourceService,
            IConfiguration configuration, IUserRepository userRepository)
        {
            _officeContactRepository = officeContactRepository;
            _sharedResourceService = sharedResourceService;
            _hrResourceService = hrResourceService;
            _configuration = configuration;
            _userRepository = userRepository;
        }

        public async Task<ActionResultResponse<string>> Insert(OfficeContactMeta officeContactMeta)
        {
            var apiUrls = _configuration.GetApiUrl();
            if (apiUrls == null)
                return new ActionResultResponse<string>(-1, _sharedResourceService.GetString("Missing some configuration. Please contact with administrator."),
                    _hrResourceService.GetString("Add office contact fail."));

            // Check user exists
            var userInfo = await _userRepository.GetInfo(officeContactMeta.UserId, true);
            if (userInfo == null)
                return new ActionResultResponse<string>(-2, _sharedResourceService.GetString("User info does not exists. Please contact with administrator."),
                    _hrResourceService.GetString("Add office contact fail."));

            var isUserExists =
                await _officeContactRepository.CheckUserExists(officeContactMeta.OfficeId, officeContactMeta.UserId);
            if (isUserExists)
                return new ActionResultResponse<string>(-5, _hrResourceService.GetString("Contact for user: \"{0}\" already exists.", userInfo.FullName),
                    _hrResourceService.GetString("Add office contact fail."));

            var officeContact = new OfficeContact
            {
                UserId = userInfo.Id,
                FullName = userInfo.FullName,
                Avatar = userInfo.Avatar,
                TitlePrefixing = userInfo.TitlePrefixing,
                Email = officeContactMeta.Email?.Trim(),
                Fax = officeContactMeta.Fax?.Trim(),
                PhoneNumber = officeContactMeta.PhoneNumber?.Trim(),
                OfficeId = officeContactMeta.OfficeId
            };

            var result = await _officeContactRepository.Insert(officeContact);
            return new ActionResultResponse<string>(result,
                result <= 0
                ? _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                : _hrResourceService.GetString("Add new office contact successful."),
                string.Empty,
                officeContact.Id);
        }

        public async Task<List<ActionResultResponse<string>>> Inserts(List<OfficeContactMeta> officeContactMetas)
        {
            var results = new List<ActionResultResponse<string>>();
            foreach (var officeContact in officeContactMetas)
            {
                results.Add(await Insert(officeContact));
            }
            return results;
        }

        public async Task<ActionResultResponse> Update(OfficeContactMeta officeContactMeta)
        {
            var apiUrls = _configuration.GetApiUrl();
            if (apiUrls == null)
                return new ActionResultResponse(-1,
                    _sharedResourceService.GetString(
                        "Missing some configuration. Please contact with administrator."));

            var officeContactInfo =
                await _officeContactRepository.GetInfo(officeContactMeta.OfficeId, officeContactMeta.UserId);
            if (officeContactInfo == null)
                return new ActionResultResponse(-2, _hrResourceService.GetString("Contact does not exists."));

            if (officeContactInfo.UserId != officeContactMeta.UserId)
            {
                // Check user exists
                var userInfo = await _userRepository.GetInfo(officeContactMeta.UserId, true);
                if (userInfo == null)
                    return new ActionResultResponse(-3,
                        _sharedResourceService.GetString(
                            "User info does not exists. Please contact with administrator."));

                officeContactInfo.UserId = userInfo.Id;
                officeContactInfo.FullName = userInfo.FullName;
                officeContactInfo.Avatar = userInfo.Avatar;
                officeContactInfo.TitlePrefixing = userInfo.TitlePrefixing;
            }

            officeContactInfo.PhoneNumber = officeContactMeta.PhoneNumber?.Trim();
            officeContactInfo.Email = officeContactMeta.Email?.Trim();
            officeContactInfo.Fax = officeContactMeta.Fax.Trim();
            officeContactInfo.LastUpdate = DateTime.Now;
            var result = await _officeContactRepository.Update(officeContactInfo);
            return new ActionResultResponse(result, result <= 0
                ? _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                : _hrResourceService.GetString("Update contact successful."));

        }

        public async Task<ActionResultResponse> Update(string id, OfficeContactMeta officeContactMeta)
        {
            var apiUrls = _configuration.GetApiUrl();
            if (apiUrls == null)
                return new ActionResultResponse(-1, _sharedResourceService.GetString("Missing some configuration. Please contact with administrator."));

            var officeContactInfo =
                await _officeContactRepository.GetInfo(id);
            if (officeContactInfo == null)
                return new ActionResultResponse(-2, _hrResourceService.GetString("Contact does not exists."));

            if (officeContactInfo.UserId != officeContactMeta.UserId)
            {
                // Check user exists
                var userInfo = await new HttpClientService().GetAsync<BriefUser>($"{apiUrls.HrApiUrl}users/{officeContactMeta.UserId}");
                if (userInfo == null)
                    return new ActionResultResponse(-3, _sharedResourceService.GetString("User info does not exists. Please contact with administrator."));

                officeContactInfo.UserId = userInfo.Id;
                officeContactInfo.FullName = userInfo.FullName;
                officeContactInfo.Avatar = userInfo.Avatar;
                officeContactInfo.TitlePrefixing = userInfo.TitlePrefixing;
            }

            officeContactInfo.PhoneNumber = officeContactMeta.PhoneNumber?.Trim();
            officeContactInfo.Email = officeContactMeta.Email?.Trim();
            officeContactInfo.Fax = officeContactMeta.Fax?.Trim();
            var result = await _officeContactRepository.Update(officeContactInfo);
            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                : _hrResourceService.GetString("Update contact successful."));
        }

        public async Task<ActionResultResponse> Delete(int officeId, string userId)
        {
            var officeContactInfo =
                await _officeContactRepository.GetInfo(officeId, userId);
            if (officeContactInfo == null)
                return new ActionResultResponse(-1, _hrResourceService.GetString("Contact does not exists."));

            officeContactInfo.IsDelete = true;
            var result = await _officeContactRepository.Delete(officeId, userId);
            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                : _hrResourceService.GetString("Delete contact successful."));
        }

        public async Task<SearchResult<OfficeContactViewModel>> GetsByOfficeId(int officeId, string languageId)
        {
            var items = await _officeContactRepository.GetsByOfficeId(officeId, languageId);
            return new SearchResult<OfficeContactViewModel>
            {
                Items = items
            };
        }
    }
}
