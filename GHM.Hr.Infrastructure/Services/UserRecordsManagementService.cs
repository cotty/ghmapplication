﻿using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Hr.Infrastructure.Services
{
    public class UserRecordsManagementService : IUserRecordsManagementService
    {
        private IRecordsManagementRepository _recordsManagementRepository;
        private readonly IResourceService<GhmHrResource> _resourceService;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IUserValueRepository _userValueRepository;
        public UserRecordsManagementService(IRecordsManagementRepository recordsManagementRepository,
            IResourceService<GhmHrResource> resourceService, IUserValueRepository userValueRepository,
            IResourceService<SharedResource> sharedResourceService)
        {
            _userValueRepository = userValueRepository;
            _recordsManagementRepository = recordsManagementRepository;
            _resourceService = resourceService;
            _sharedResourceService = sharedResourceService;
        }
        public async Task<ActionResultResponse> Delete(string tenantId, string id)
        {
            var info = await _recordsManagementRepository.GetInfo(tenantId, id);

            if (info == null)
                return new ActionResultResponse(-1, _resourceService.GetString("Records does not exist"));

            info.IsDelete = true;

            return await _recordsManagementRepository.SaveAsync(info) < 0 ? new ActionResultResponse(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)) : new ActionResultResponse(1, _resourceService.GetString("Delete record does successfully!"));
        }

        public async Task<ActionResultResponse<string>> Insert(string tenantId, UserRecordsManagementMeta userRecordsManagementMeta)
        {
            var checkExist = await _recordsManagementRepository.CheckExist(tenantId, userRecordsManagementMeta.Title, userRecordsManagementMeta.UserId, userRecordsManagementMeta.ValueId);

            if (checkExist)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("records does exist"));

            var valueInfo = await _userValueRepository.GetInfo(tenantId, userRecordsManagementMeta.ValueId, true);

            if (valueInfo == null)
                return new ActionResultResponse<string>(-2, _resourceService.GetString("value does exist"));

            var records = new UserRecordsManagement
            {
                ValueId = userRecordsManagementMeta.ValueId,
                UserId = userRecordsManagementMeta.UserId,
                AttachmentId = userRecordsManagementMeta.AttachmentId,
                Title = valueInfo.Name,
                TenantId = tenantId,
                Note = userRecordsManagementMeta.Note,
                IsSelected = userRecordsManagementMeta.IsSelected,
                AttachmentName = userRecordsManagementMeta.AttachmentName
            };


            return await _recordsManagementRepository.Insert(records) < 0 ? new ActionResultResponse<string>(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)) : new ActionResultResponse<string>(1, _resourceService.GetString("Insert records successfully!"), "", records.Id);
        }

        public async Task<SearchResult<UserRecordsManagementViewModel>> Search(string tenantId, string userId, string valueId, string title, DateTime? fromDate, DateTime? toDate, int page, int pageSize)
        {
            var items = await _recordsManagementRepository.Search(tenantId, userId, valueId, title, fromDate, toDate, page, pageSize, out var totalRows);

            return new SearchResult<UserRecordsManagementViewModel>
            {
                Items = items,
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse<string>> Update(string tenantId, string id, UserRecordsManagementMeta userRecordsManagementMeta)
        {

            var info = await _recordsManagementRepository.GetInfo(tenantId, id);

            if (info == null)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Records does not exist"));

            if (!info.ConcurrencyStamp.Contains(userRecordsManagementMeta.ConcurrencyStamp))
                return new ActionResultResponse<string>(-2, _resourceService.GetString("Records was updated before there"));

            if (info.Title != userRecordsManagementMeta.Title)
            {
                var checkTitleExist = await _recordsManagementRepository.CheckExist(tenantId, userRecordsManagementMeta.Title, userRecordsManagementMeta.UserId, userRecordsManagementMeta.ValueId);

                if (checkTitleExist)
                    return new ActionResultResponse<string>(-1, _resourceService.GetString("records name does exist"));
            }
            var valueInfo = await _userValueRepository.GetInfo(tenantId, userRecordsManagementMeta.ValueId);

            if (valueInfo == null)
                return new ActionResultResponse<string>(-2, _resourceService.GetString("value does exist"));

            info.ValueId = userRecordsManagementMeta.ValueId;
            info.UserId = userRecordsManagementMeta.UserId;
            //info.Title = valueInfo.Name;
            info.Note = userRecordsManagementMeta.Note;
            info.AttachmentId = userRecordsManagementMeta.AttachmentId;
            info.IsSelected = userRecordsManagementMeta.IsSelected;
            info.AttachmentName = userRecordsManagementMeta.AttachmentName;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();

            return await _recordsManagementRepository.SaveAsync(info) < 0 ? new ActionResultResponse<string>(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)) : new ActionResultResponse<string>(1, _resourceService.GetString("Update record successfully!"), "", info.ConcurrencyStamp);
        }
    }
}
