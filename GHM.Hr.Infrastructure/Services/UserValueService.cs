﻿using System.Threading.Tasks;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Infrastructure.Services
{
    public class UserValueService : IUserValueService
    {
        private readonly IUserValueRepository _userValueRepository;
        public UserValueService(IUserValueRepository userValueRepository)
        {
            _userValueRepository = userValueRepository;
        }

        public async Task<ActionResultResponse> Delete(string tenantId, string id)
        {
            var info = await _userValueRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, "Giá trị không tồn tại");

            var result = await _userValueRepository.Delete(info);
            return new ActionResultResponse<string>(result, result <= 0 ? "Có gì đó không đúng" : "Xóa giá trị thành công");
        }

        public async Task<ActionResultResponse<string>> Insert(string tenantId, UserValueMeta userValueMeta)
        {
            var isExistsUserValue = await _userValueRepository.CheckNameExists(tenantId, "", userValueMeta.Name, userValueMeta.Type);
            if (isExistsUserValue)
                return new ActionResultResponse<string>(-1, $"{GetValueByUsertValueType(userValueMeta.Type)} đã tồn tại");

            var userValue = new UserValue()
            {
                Name = userValueMeta.Name.Trim(),
                Type = userValueMeta.Type,
                TenantId = tenantId,
                UnsignName = userValueMeta.Name.StripVietnameseChars()
            };

            var result = await _userValueRepository.Insert(userValue);
            return new ActionResultResponse<string>(result, result <= 0 ? "Có gì đó không đúng" : "Thêm mới thành công", "", userValue.Id);
        }

        public async Task<SearchResult<UserValueViewModel>> Search(string tenantId, string keyword, UserValueType type, int page, int pageSize)
        {
            return new SearchResult<UserValueViewModel>()
            {
                Items = await _userValueRepository.Search(tenantId, keyword, type, page, pageSize, out int totalRows),
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse> Update(string tenantId, string id, UserValueMeta userValueMeta)
        {
            var info = await _userValueRepository.GetInfo(tenantId, id);

            var isExistsUserValue = await _userValueRepository.CheckNameExists(tenantId, "", userValueMeta.Name, userValueMeta.Type);
            if (isExistsUserValue)
                return new ActionResultResponse<string>(-1, $"${GetValueByUsertValueType(userValueMeta.Type)} đã tồn tại");

            info.Name = userValueMeta.Name;
            info.UnsignName = userValueMeta.Name.Trim().StripVietnameseChars().ToUpper();

            var result = await _userValueRepository.Update(info);
            return new ActionResultResponse(result, result <= 0 ? "Có gì đó không đúng.Vui lòng liên hệ với quản trị viên." : "Cập nhật thành công.");
        }

        private string GetValueByUsertValueType(UserValueType type)
        {
            switch (type)
            {
                case UserValueType.AcademicLevel:
                    return "Trình độ học vấn";
                case UserValueType.AcademicLevelDegree:
                    return "Bằng cấp";
                case UserValueType.AcademicLevelSchool:
                    return "Trường đào tạo";
                case UserValueType.AcademicLevelSpecialize:
                    return "Chuyên môn";
                case UserValueType.TrainingPlace:
                    return "Nơi đào tạo";
                case UserValueType.TrainingCourse:
                    return "Khóa đào tạo";
                case UserValueType.LaborContract:
                    return "Loại hợp đồng";
                case UserValueType.Company:
                    return "Công ty";
                default:
                    return "";
            }
        }
    }
}
