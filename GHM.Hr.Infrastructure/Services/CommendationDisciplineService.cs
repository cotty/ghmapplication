﻿using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Hr.Infrastructure.Services
{
    public class CommendationDisciplineService : ICommendationDisciplineService
    {
        private readonly ICommendationDisciplineRepository _commendationDisciplineRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmHrResource> _resourceService;
        private readonly IUserRepository _userRepository;
        private readonly ICommendationDisciplineCategoryRepository _commendationDisciplineCategoryRepository;
        private readonly ITitleRepository _titleRepository;
        public CommendationDisciplineService(ICommendationDisciplineRepository commendationDisciplineRepository, 
            IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> resourceService,
            IUserRepository userRepository, ITitleRepository titleRepository, ICommendationDisciplineCategoryRepository commendationDisciplineCategoryRepository)
        {
            _commendationDisciplineRepository = commendationDisciplineRepository;
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
            _userRepository = userRepository;
            _titleRepository = titleRepository;
            _commendationDisciplineCategoryRepository = commendationDisciplineCategoryRepository;
        }

        public async Task<ActionResultResponse> Delete(string tenantId, string id)
        {
            var info = await _commendationDisciplineRepository.GetInfo(tenantId, id, false);

            if (info == null)
                return new ActionResultResponse(-1, _resourceService.GetString("Commendation Discipline does not exist"));

            info.IsDelete = true;

            return await _commendationDisciplineRepository.Delete(info) < 0 ? new ActionResultResponse(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)) 
                : new ActionResultResponse(1, _resourceService.GetString("delete commendation discipline successfully !"));
        }

        public async Task<ActionResultResponse<string>> Insert(string tenantId, string userId, string fullName, CommendationDisciplineMeta commendationDisciplineMeta)
        {
            //var userInfo = await _userRepository.GetUserInfo(tenantId, commendationDisciplineMeta.UserId, CultureInfo.CurrentCulture.Name);

            //if (userInfo == null)
            //    return new ActionResultResponse<string>(-1, _resourceService.GetString("User does not exist"));

            //var categoryExist = await _commendationDisciplineCategoryRepository.CheckExist(tenantId, commendationDisciplineMeta.CategoryId);

            //var commendationDiscipline = new UserCommendationDiscipline
            //{
            //    AttachmentUrl = commendationDisciplineMeta.AttachmentUrl,
            //    CategoryId = commendationDisciplineMeta.CategoryId,
            //    CategoryName = c
            //};
            throw new NotImplementedException();
        }

        public Task<SearchResult<UserCommendationDiscipline>> Search(string keyword, string userId, bool? type, string categoryId, DateTime? fromDate, DateTime? toDate, int? officeId, string titleId, int page, int pageSize, out object totalRows)
        {
            throw new NotImplementedException();
        }

        public Task<ActionResultResponse<string>> Update(string id, string tenantId, CommendationDisciplineMeta commendationDisciplineMeta)
        {
            throw new NotImplementedException();
        }
    }
}
