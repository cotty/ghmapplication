﻿using GHM.Hr.Api.Infrastructure.Models.ViewModels;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.Services;
using GHM.Infrastructure.ViewModels;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Hr.Domain.Commands;
using MediatR;
using ShortUserInfoViewModel = GHM.Infrastructure.ViewModels.ShortUserInfoViewModel;
using GHM.Infrastructure.Constants;
using UserType = GHM.Hr.Domain.Constants.UserType;

namespace GHM.Hr.Infrastructure.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserTranslationRepository _userTranslationRepository;
        private readonly IUserContactRepository _userContactRepository;
        private readonly IUserPositionRepository _userPositionRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IOfficeRepository _officeRepository;
        private readonly ITitleRepository _titleRepository;
        private readonly IConfiguration _configuration;
        private readonly IPositionTranslationRepository _positionTranslationRepository;
        private readonly ITitleTranslationRepository _titleTranslationRepository;
        private readonly IOfficeTranslationRepository _officeTranslationRepository;
        private readonly IResourceService<GhmHrResource> _resourceService;
        private readonly ApiUrlSettings _apiUrls;
        private readonly IPositionRepository _positionRepository;
        private readonly ILaborContractRepository _laborContractRepository;
        private readonly IMediator _mediator;
        private readonly IHttpClientService _httpClientService;

        public UserService(IUserRepository userRepository, IResourceService<SharedResource> sharedResourceService,
            IOfficeRepository officeRepository, ITitleRepository titleRepository, IUserContactRepository userContactRepository,
            IConfiguration configuration, IUserTranslationRepository userTranslationRepository, ILaborContractRepository laborContractRepository,
            IUserPositionRepository userPositionRepository, IPositionTranslationRepository positoryTranslationRepository,
            ITitleTranslationRepository titleTranslationRepository, IOfficeTranslationRepository officeTranslationRepository,
            IHttpClientService httpClientService,
            IResourceService<GhmHrResource> resourceService, IPositionRepository positionRepository, IMediator mediator)
        {
            _userRepository = userRepository;
            _userTranslationRepository = userTranslationRepository;
            _userContactRepository = userContactRepository;
            _userPositionRepository = userPositionRepository;
            _sharedResourceService = sharedResourceService;
            _officeRepository = officeRepository;
            _titleRepository = titleRepository;
            _configuration = configuration;
            _positionTranslationRepository = positoryTranslationRepository;
            _titleTranslationRepository = titleTranslationRepository;
            _officeTranslationRepository = officeTranslationRepository;
            _resourceService = resourceService;
            _positionRepository = positionRepository;
            _laborContractRepository = laborContractRepository;
            _mediator = mediator;
            _httpClientService = httpClientService;
            _apiUrls = _configuration.GetApiUrl();
        }

        public async Task<bool> VerifyLeader(string leaderId, string userId)
        {
            User leaderInfo = await _userRepository.GetInfo(leaderId, true);
            User userInfo = await _userRepository.GetInfo(userId, true);
            if (leaderInfo == null || userInfo == null || leaderInfo.UserType != UserType.Leader)
            {
                return false;
            }

            return false;
        }

        public async Task<bool> VerifyLeader(string leaderId, int officeId)
        {

            User leaderInfo = await _userRepository.GetInfo(leaderId, true);
            //            return leaderInfo != null && leaderInfo.OfficeId == officeId && leaderInfo.UserType == UserType.Leader;
            return false;
        }

        public async Task<ActionResultResponse> Insert(string tenantId, UserMeta userMeta)
        {
            string userId = Guid.NewGuid().ToString();
            ApiUrlSettings apiUrls = _configuration.GetApiUrl();
            if (apiUrls == null)
            {
                return new ActionResultResponse(-14, _sharedResourceService.GetString("Missing some configuration. Please contact with administrator."));
            }

            if (userMeta.UserTranslations != null && !userMeta.UserTranslations.Any())
            {
                return new ActionResultResponse(-15, _sharedResourceService.GetString("Please enter at least one language."));
            }

            bool isExistsByUserName = await _userRepository.CheckUserNameExists(userId, userMeta.UserName);
            if (isExistsByUserName)
            {
                return new ActionResultResponse(-1, _resourceService.GetString("UserName already exists. Please check again"));
            }

            bool isExissByIdCardnNumber = await _userRepository.CheckIdCardNumberExists(tenantId, userId, userMeta.IdCardNumber);
            if (isExissByIdCardnNumber)
            {
                return new ActionResultResponse(-2, _resourceService.GetString("Id card number already exists. Please check agian"));
            }

            Office officeInfo = await _officeRepository.GetInfo(userMeta.OfficeId, true);
            if (officeInfo == null)
            {
                return new ActionResultResponse(-3, _resourceService.GetString("Office doesn't exists."));
            }

            Title titleInfo = await _titleRepository.GetInfo(tenantId, userMeta.TitleId);
            if (titleInfo == null)
            {
                return new ActionResultResponse(-4, _resourceService.GetString("Title does not exists"));
            }

            Position positionInfo = await _positionRepository.GetInfo(userMeta.PositionId, true);
            if (positionInfo == null)
            {
                return new ActionResultResponse(-17, _resourceService.GetString("Position does not exists"));
            }

            if (!positionInfo.IsMultiple)
            {
                var countUserByPosition = await _userRepository.GetTotalUserByPosition(tenantId, userMeta.PositionId);
                if (countUserByPosition > 0)
                    return new ActionResultResponse(-19, _resourceService.GetString("Position does not allow multi user"));
            }

            List<UserTranslation> listUserTranslation = new List<UserTranslation>();


            // Create user account.         
            ActionResultResponse resultInsertAccount = await _httpClientService.PostAsync<ActionResultResponse>($"{apiUrls.CoreApiUrl}/accounts",
                new Dictionary<string, string>{
                    {"UserId",  userId},
                    {"TenantId",  tenantId},
                    {"UserName",  userMeta.UserName.ToLower()},
                    {"FullName",  userMeta.FullName},
                    {"Avatar",  userMeta.Avatar},
                    {"Password",  "12345"},
                    {"IsActive", userMeta.Status <= UserStatus.Official ? true.ToString() : false.ToString()}
                });

            if (resultInsertAccount == null)
            {
                return new ActionResultResponse(-18, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
            }

            if (resultInsertAccount.Code <= 0)
            {
                return resultInsertAccount;
            }

            // Insert User
            User user = new User
            {
                Id = userId,
                TenantId = tenantId,
                TitleId = titleInfo.Id,
                PositionId = positionInfo.Id,
                UserName = userMeta.UserName.Trim().ToLower(),
                FullName = userMeta.FullName.Trim(),
                FirstName = Common.GetFirstName(userMeta.FullName.Trim()),
                MiddleName = Common.GetMiddleName(userMeta.FullName.Trim()),
                LastName = Common.GetLastName(userMeta.FullName.Trim()),
                IdCardDateOfIssue = userMeta.IdCardDateOfIssue,
                IdCardNumber = userMeta.IdCardNumber?.Trim(),
                OfficeId = officeInfo.Id,
                OfficeIdPath = officeInfo.IdPath,
                BankingNumber = userMeta.BankingNumber?.Trim(),
                Birthday = userMeta.Birthday,
                Denomination = userMeta.Denomination,
                Gender = userMeta.Gender,
                Avatar = userMeta.Avatar,
                MarriedStatus = userMeta.MarriedStatus,
                Tin = userMeta.Tin,
                UnsignName =
                    $"{userMeta.FullName.StripVietnameseChars()} {userMeta.UserName.StripVietnameseChars()} {userMeta.Tin}"
                        .ToUpper(),
                PassportId = userMeta.PassportId?.Trim(),
                PassportDateOfIssue = userMeta.PassportDateOfIssue,
                EnrollNumber = userMeta.EnrollNumber,
                CardNumber = userMeta.CardNumber?.Trim(),
                Ext = userMeta.Ext?.Trim(),
                AcademicRank = userMeta.AcademicRank,
                JoinedDate = userMeta.JoinedDate,
                Status = userMeta.Status
            };

            NationalViewModel nationalInfo = new NationalViewModel();
            if (userMeta.NationalId.HasValue)
            {
                nationalInfo = await _httpClientService.GetAsync<NationalViewModel>($"{apiUrls.CoreApiUrl}/nationals/{userMeta.NationalId}");
                if (nationalInfo == null)
                {
                    await RollbackUserAccount(userId);
                    return new ActionResultResponse(-5, _resourceService.GetString("National does not exists"));
                }

                user.NationalId = nationalInfo.Id;
            }

            if (userMeta.ProvinceId.HasValue)
            {
                ProvinceViewModel provinceInfo = await _httpClientService
                    .GetAsync<ProvinceViewModel>($"{apiUrls.CoreApiUrl}/nationals/provinces/{userMeta.ProvinceId}");
                if (provinceInfo == null)
                {
                    await RollbackUserAccount(userId);
                    return new ActionResultResponse(-5, _resourceService.GetString("Province does not exists"));
                }

                user.ProvinceId = provinceInfo.Id;
                user.ProvinceName = provinceInfo.Name;
            }

            if (userMeta.DistrictId.HasValue)
            {
                DistrictViewModel districtInfo = await _httpClientService
                    .GetAsync<DistrictViewModel>($"{apiUrls.CoreApiUrl}/nationals/provinces/districts/{userMeta.DistrictId}");
                if (districtInfo == null)
                {
                    await RollbackUserAccount(userId);
                    return new ActionResultResponse(-6, _resourceService.GetString("District does not exists"));
                }

                user.DistrictId = districtInfo.Id;
                user.DistrictName = districtInfo.Name;
            }

            if (userMeta.Ethnic.HasValue)
            {
                EthnicSearchViewModel ethnicInfo = await _httpClientService
                    .GetAsync<EthnicSearchViewModel>($"{apiUrls.CoreApiUrl}/nationals/ethnics/{userMeta.Ethnic.Value}");
                if (ethnicInfo == null)
                {
                    await RollbackUserAccount(userId);
                    return new ActionResultResponse(-7, _resourceService.GetString("Ethnic does not exists"));
                }

                user.Ethnic = ethnicInfo.Id;
                user.EthnicName = ethnicInfo.Name;
            }

            if (userMeta.Denomination.HasValue)
            {
                ReligionSearchViewModel denominationInfo = await _httpClientService
                    .GetAsync<ReligionSearchViewModel>($"{apiUrls.CoreApiUrl}/nationals/religions/{userMeta.Denomination.Value}");
                if (denominationInfo == null)
                {
                    await RollbackUserAccount(userId);
                    return new ActionResultResponse(-7, _resourceService.GetString("Ethnic does not exists"));
                }

                user.Denomination = (byte)denominationInfo.Id;
                user.DenominationName = denominationInfo.Name;
            }

            int resultUser = await _userRepository.Insert(user);
            if (resultUser <= 0)
            {
                await RollbackUserAccount(userId);
                return new ActionResultResponse(resultUser,
                    _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
            }

            // Insert UserTranslation
            if (userMeta.UserTranslations != null && userMeta.UserTranslations.Any())
            {
                foreach (UserTranslation userTranslation in userMeta.UserTranslations)
                {
                    TitleTranslation titleTranslationInfo = await _titleTranslationRepository.GetInfo(tenantId, titleInfo.Id, userTranslation.LanguageId, true);
                    if (titleTranslationInfo == null)
                    {
                        await RollbackInsertUser(tenantId, userId);
                        await RollbackUserAccount(userId);
                        return new ActionResultResponse(-5,
                            _resourceService.GetString("Title by language: \"{0}\" does not exists.", userTranslation.LanguageId));
                    }

                    PositionTranslation positionTranslationInfo = await _positionTranslationRepository.GetInfo(positionInfo.Id, userTranslation.LanguageId, true);
                    if (positionTranslationInfo == null)
                    {
                        await RollbackInsertUser(tenantId, userId);
                        await RollbackUserAccount(userId);
                        return new ActionResultResponse(-6,
                            _resourceService.GetString("Position by language: \"{0}\" does not exists.", userTranslation.LanguageId));
                    }

                    OfficeTranslation officeTranslationInfo = await _officeTranslationRepository.GetInfo(userMeta.OfficeId, userTranslation.LanguageId, true);
                    if (officeTranslationInfo == null)
                    {
                        await RollbackInsertUser(tenantId, userId);
                        await RollbackUserAccount(userId);
                        return new ActionResultResponse(-7,
                            _resourceService.GetString("Office by languale: \"{0}\" does not exists", userTranslation.LanguageId));
                    }

                    userTranslation.UserId = userId;
                    userTranslation.Address = userTranslation.Address?.Trim();
                    userTranslation.BankName = userTranslation.BankName?.Trim();
                    userTranslation.BranchBank = userTranslation.BranchBank?.Trim();
                    userTranslation.IdCardPlaceOfIssue = userTranslation.IdCardPlaceOfIssue?.Trim();
                    userTranslation.LanguageId = userTranslation.LanguageId?.Trim();
                    userTranslation.Nationality = nationalInfo.Name;
                    userTranslation.NativeCountry = userTranslation.NativeCountry?.Trim();
                    userTranslation.PassportPlaceOfIssue = userTranslation.PassportPlaceOfIssue?.Trim();
                    userTranslation.ResidentRegister = userTranslation.ResidentRegister?.Trim();
                    userTranslation.TitleName = titleTranslationInfo.Name;
                    userTranslation.PositionName = positionTranslationInfo.Name;
                    userTranslation.OfficeName = officeTranslationInfo.Name;

                    listUserTranslation.Add(userTranslation);
                }

                int resultInsertUserTranslation = await _userTranslationRepository.Inserts(listUserTranslation);
                if (resultInsertUserTranslation <= 0)
                {
                    await RollbackInsertUser(tenantId, userId);
                    await RollbackUserAccount(userId);
                    return new ActionResultResponse(resultInsertUserTranslation,
                        _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
                }
            }

            // Insert UserContact
            if (userMeta.UserContacts.Any())
            {
                int resultInsertUserContact = await InsertUserContact(userMeta.UserContacts);
                if (resultInsertUserContact <= 0)
                {
                    return new ActionResultResponse(resultInsertUserContact,
                        _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
                }
            }

            // Insert UserPosition 
            int resultUserPosition = await InsertUserPosition(officeInfo.Id, officeInfo.IdPath, positionInfo.Id);
            if (resultUserPosition <= 0)
            {
                return new ActionResultResponse(resultUserPosition, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
            }

            // Insert WorkSchedule           
            //if (userMeta.EnrollNumber.HasValue)
            //{
            //    var resultInsertWork = await InsertWorkSchedule(userMeta.EnrollNumber.Value, userMeta.FullName.Trim(), userMeta.FullName.StripVietnameseChars(), userId, positionInfo.Id, officeInfo.Id);
            //    if (resultInsertWork <= 0)
            //        return new ActionResultResponse(resultUserPosition, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            //    // Insert user into timekeeping machine.
            //    if (!string.IsNullOrEmpty(userMeta.CardNumber))
            //    {
            //        var resultInsertTimeKeping = await InsertTimekeepingMachine(userMeta.EnrollNumber.Value, userId, user.CardNumber,
            //            user.FullName, officeInfo.Id, positionInfo.Id);

            //        if (resultInsertWork <= 0)
            //            return new ActionResultResponse(resultUserPosition, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
            //    }
            //}

            return new ActionResultResponse(resultUser, _resourceService.GetString("Add new user successful."));
            #region insertusercontact
            async Task<int> InsertUserContact(List<UserContact> userContacts)
            {
                List<UserContact> listUserContact = new List<UserContact>();
                foreach (UserContact userContact in userContacts)
                {
                    userContact.UserId = userId;
                    userContact.Id = Guid.NewGuid().ToString();
                    userContact.TenantId = tenantId;
                    bool isExistsValue = await _userContactRepository.CheckExistsByValue(tenantId, userContact.Id, userContact.UserId, userContact.ContactType,
                        userContact.ContactValue);
                    if (!isExistsValue)
                    {
                        listUserContact.Add(userContact);
                    }
                }

                int resultInserUserContact = await _userContactRepository.Inserts(listUserContact);
                if (resultInserUserContact <= 0)
                {
                    await RollbackInsertUser(tenantId, userId);
                    await RollbackUserAccount(userId);
                    await RollbackUserTranslation(userId);
                }

                return resultInserUserContact;
            }
            #endregion

            #region insertResultUserPosition
            async Task<int> InsertUserPosition(int officeId, string officeIdPath, string positionId)
            {
                int resultUserPositionLocal = await _userPositionRepository.Insert(new UserPositioncs
                {
                    OfficeId = officeId,
                    OfficeIdPath = officeIdPath,
                    UserId = userId,
                    PositionId = positionId,
                    IsDefault = true
                });

                if (resultUserPositionLocal < 0)
                {
                    await RollbackInsertUser(tenantId, userId);
                    await RollbackUserAccount(userId);
                    await RollbackUserTranslation(userId);
                    await RollbackUserContact(tenantId, userId);
                }

                return resultUserPositionLocal;
            }
            #endregion
        }

        public async Task<ActionResultResponse> Delete(string tenantId, string userId)
        {
            User userInfo = await _userRepository.GetInfo(tenantId, userId, true);
            if (userInfo == null)
            {
                return new ActionResultResponse(-1, _resourceService.GetString("User does not exists."));
            }

            var countUserLabor = await _laborContractRepository.CountUserLaborContactByUserId(tenantId, userId);
            if(countUserLabor > 0)
                return new ActionResultResponse(-1, _resourceService.GetString("Nhân viên đã tồn tại hợp đồng."));

            int result = await _userRepository.Delete(tenantId, userId);
            if (result > 0)
            {
                await _userContactRepository.DeleteByUserdId(tenantId, userId);
                await _userTranslationRepository.DeleteByUserId(userId);
                await _userPositionRepository.DeleteByUserId(tenantId, userId);
                await RollbackUserAccount(userId);
                await DeleteUserAccount();
            }

            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                : _sharedResourceService.GetString("Delete User successfull"));

            async Task<ActionResultResponse> DeleteUserAccount()
            {
                ApiUrlSettings apiUrls = _configuration.GetApiUrl();
                return await _httpClientService.DeleteAsync<ActionResultResponse>($"{apiUrls.CoreApiUrl}/{userId}");
            }
        }

        public async Task<ActionResultResponse<string>> Update(string id, string tenantId, UserMeta userMeta)
        {
            ApiUrlSettings apiUrls = _configuration.GetApiUrl();
            if (apiUrls == null)
            {
                return new ActionResultResponse<string>(-14, _sharedResourceService.GetString("Missing some configuration. Please contact with administrator."));
            }

            if (userMeta.UserTranslations == null || !userMeta.UserTranslations.Any())
            {
                return new ActionResultResponse<string>(-15, _sharedResourceService.GetString("Please enter at least one language."));
            }

            User userInfo = await _userRepository.GetInfo(id, false);
            if (userInfo == null)
            {
                return new ActionResultResponse<string>(-1, _resourceService.GetString("User does not exists."));
            }

            if (tenantId != userInfo.TenantId)
                return new ActionResultResponse<string>(-21, _resourceService.GetString(ErrorMessage.NotHavePermission));

            if (userInfo.ConcurrencyStamp != userMeta.ConcurrencyStamp)
            {
                return new ActionResultResponse<string>(-3, _resourceService.GetString("The user already updated by other people. You can not update this user."));
            }

            bool isExistsUserByName = await _userRepository.CheckUserNameExists(id, userMeta.UserName);
            if (isExistsUserByName)
            {
                return new ActionResultResponse<string>(-1, _resourceService.GetString("UserName already exists. Please check again"));
            }

            if (userMeta.IdCardNumber != userInfo.IdCardNumber)
            {
                bool isExistsUserByIdCardNumber = await _userRepository.CheckIdCardNumberExists(tenantId, id, userMeta.IdCardNumber);
                if (isExistsUserByIdCardNumber)
                {
                    return new ActionResultResponse<string>(-2, _resourceService.GetString("Id card number already exists. Please check agian"));
                }
            }

            int oldOfficeId = userInfo.OfficeId;
            string oldPositionId = userInfo.PositionId;
            string oldFullName = userInfo.FullName;

            Office officeInfo = await _officeRepository.GetInfo(userMeta.OfficeId, true);
            if (officeInfo == null)
            {
                return new ActionResultResponse<string>(-4, _resourceService.GetString("Office does not exists"));
            }

            if (oldOfficeId != userMeta.OfficeId)
            {
                userInfo.OfficeId = userMeta.OfficeId;
                userInfo.OfficeIdPath = officeInfo.IdPath;
            }

            Title titleInfo = await _titleRepository.GetInfo(tenantId, userMeta.TitleId);
            if (titleInfo == null)
            {
                return new ActionResultResponse<string>(-5, _resourceService.GetString("Title does not exits"));
            }

            Position positionInfo = await _positionRepository.GetInfo(userMeta.PositionId, true);
            if (positionInfo == null)
            {
                return new ActionResultResponse<string>(-17, _resourceService.GetString("Position does not exists"));
            }

            if (oldPositionId != userMeta.PositionId)
            {
                if (!positionInfo.IsMultiple)
                {
                    var countUserByPosition = await _userRepository.GetTotalUserByPosition(tenantId, userMeta.PositionId);
                    if (countUserByPosition > 0)
                        return new ActionResultResponse<string>(-19, _resourceService.GetString("Position does not allow multi user"));
                }

                userInfo.TitleId = titleInfo.Id;
                userInfo.PositionId = positionInfo.Id;
            }

            NationalViewModel nationalInfo = new NationalViewModel();
            if (userMeta.NationalId.HasValue)
            {
                nationalInfo = await _httpClientService.GetAsync<NationalViewModel>($"{apiUrls.CoreApiUrl}/nationals/{userMeta.NationalId}");
                if (nationalInfo == null)
                {
                    return new ActionResultResponse<string>(-5, _sharedResourceService.GetString("National does not exists"));
                }

                userInfo.NationalId = nationalInfo.Id;
            }

            if (userMeta.ProvinceId.HasValue && userMeta.ProvinceId.Value != userInfo.ProvinceId)
            {
                ProvinceViewModel provinceInfo = await _httpClientService
                    .GetAsync<ProvinceViewModel>($"{apiUrls.CoreApiUrl}/nationals/provinces/{userMeta.ProvinceId}");
                if (provinceInfo == null)
                {
                    return new ActionResultResponse<string>(-5, _sharedResourceService.GetString("Province does not exists"));
                }

                userInfo.ProvinceId = provinceInfo.Id;
                userInfo.ProvinceName = provinceInfo.Name;
            }

            if (userMeta.DistrictId.HasValue && userMeta.DistrictId.Value != userInfo.DistrictId)
            {
                DistrictViewModel districtInfo = await _httpClientService
                    .GetAsync<DistrictViewModel>($"{apiUrls.CoreApiUrl}/nationals/provinces/districts/{userMeta.DistrictId}");
                if (districtInfo == null)
                {
                    return new ActionResultResponse<string>(-6, _sharedResourceService.GetString("District does not exists"));
                }

                userInfo.DistrictId = districtInfo.Id;
                userInfo.DistrictName = districtInfo.Name;
            }

            if (userMeta.Denomination.HasValue && userMeta.Denomination.Value != userInfo.Denomination)
            {
                ReligionSearchViewModel denominationInfo = await _httpClientService
                    .GetAsync<ReligionSearchViewModel>($"{apiUrls.CoreApiUrl}/nationals/religions/{userMeta.Denomination.Value}");
                if (denominationInfo == null)
                {
                    return new ActionResultResponse<string>(-7, _resourceService.GetString("Ethnic does not exists"));
                }

                userInfo.Denomination = (byte)denominationInfo.Id;
                userInfo.DenominationName = denominationInfo.Name;
            }

            if (userMeta.Ethnic.HasValue && userMeta.Ethnic.Value != userInfo.Ethnic)
            {
                EthnicSearchViewModel ethnicInfo = await _httpClientService
                    .GetAsync<EthnicSearchViewModel>($"{apiUrls.CoreApiUrl}/nationals/ethnics/{userMeta.Ethnic.Value}");
                if (ethnicInfo == null)
                {
                    return new ActionResultResponse<string>(-7, _resourceService.GetString("Ethnic does not exists"));
                }

                userInfo.Ethnic = ethnicInfo.Id;
                userInfo.EthnicName = ethnicInfo.Name;
            }

            string oldAvatar = userInfo.Avatar;

            userInfo.FullName = userMeta.FullName;
            userInfo.FirstName = userMeta.FullName.GetFirstName();
            userInfo.MiddleName = userMeta.FullName.GetMiddleName();
            userInfo.LastName = userMeta.FullName.GetLastName();
            userInfo.IdCardDateOfIssue = userMeta.IdCardDateOfIssue;
            userInfo.IdCardNumber = userMeta.IdCardNumber;
            userInfo.BankingNumber = userMeta.BankingNumber;
            userInfo.Birthday = userMeta.Birthday;
            userInfo.Denomination = userMeta.Denomination;
            userInfo.Gender = userMeta.Gender;
            userInfo.Avatar = userMeta.Avatar;
            userInfo.Tin = userMeta.Tin;
            userInfo.UnsignName = $"{userMeta.FullName.StripVietnameseChars()} {userMeta.UserName} {userMeta.Tin}".ToUpper();
            userInfo.PassportId = userMeta.PassportId;
            userInfo.PassportDateOfIssue = userMeta.PassportDateOfIssue;
            userInfo.EnrollNumber = userMeta.EnrollNumber;
            userInfo.CardNumber = userMeta.CardNumber;
            userInfo.Ext = userMeta.Ext;
            userInfo.AcademicRank = userMeta.AcademicRank;
            userInfo.JoinedDate = userMeta.JoinedDate;
            userInfo.UserType = userMeta.UserType;
            userInfo.Status = userMeta.Status;
            userInfo.Gender = userMeta.Gender;
            userInfo.MarriedStatus = userMeta.MarriedStatus;
            userInfo.ConcurrencyStamp = Guid.NewGuid().ToString();

            foreach (UserTranslation userTranslation in userMeta.UserTranslations)
            {
                TitleTranslation titleTranslationInfo = await _titleTranslationRepository.GetInfo(tenantId, titleInfo.Id, userTranslation.LanguageId);
                if (titleTranslationInfo == null)
                {
                    return new ActionResultResponse<string>(-8, _resourceService.GetString("Title by language: \"{0}\" does not exists.",
                        userTranslation.LanguageId));
                }

                PositionTranslation positionTranslationInfo = await _positionTranslationRepository.GetInfo(positionInfo.Id, userTranslation.LanguageId);
                if (positionTranslationInfo == null)
                {
                    return new ActionResultResponse<string>(-9, _resourceService.GetString("Position by language: \"{0}\" does not exists.",
                        userTranslation.LanguageId));
                }

                OfficeTranslation officeTranslationInfo = await _officeTranslationRepository.GetInfo(userMeta.OfficeId, userTranslation.LanguageId);
                if (officeTranslationInfo == null)
                {
                    return new ActionResultResponse<string>(-10, _resourceService.GetString("Office by languale: \"{0}\" does not exists",
                        userTranslation.LanguageId));
                }

                UserTranslation userTranslationInfo = await _userTranslationRepository.GetInfo(id, userTranslation.LanguageId);
                if (userTranslationInfo == null)
                {
                    userTranslationInfo = new UserTranslation
                    {
                        UserId = id,
                        Address = userTranslation.Address?.Trim(),
                        BankName = userTranslation.BankName?.Trim(),
                        BranchBank = userTranslation.BranchBank?.Trim(),
                        IdCardPlaceOfIssue = userTranslation.IdCardPlaceOfIssue?.Trim(),
                        LanguageId = userTranslation.LanguageId?.Trim(),
                        Nationality = nationalInfo.Name,
                        NativeCountry = userTranslation.NativeCountry?.Trim(),
                        PassportPlaceOfIssue = userTranslation.PassportPlaceOfIssue?.Trim(),
                        ResidentRegister = userTranslation.ResidentRegister?.Trim(),
                        TitleName = titleTranslationInfo.Name,
                        PositionName = positionTranslationInfo.Name,
                        OfficeName = officeTranslationInfo.Name,
                    };
                    await _userTranslationRepository.Insert(userTranslationInfo);
                }
                else
                {
                    userTranslationInfo.Address = userTranslation.Address?.Trim();
                    userTranslationInfo.BankName = userTranslation.BankName?.Trim();
                    userTranslationInfo.BranchBank = userTranslation.BranchBank?.Trim();
                    userTranslationInfo.IdCardPlaceOfIssue = userTranslation.IdCardPlaceOfIssue?.Trim();
                    userTranslationInfo.Nationality = nationalInfo.Name;
                    userTranslationInfo.NativeCountry = userTranslation.NativeCountry?.Trim();
                    userTranslationInfo.PassportPlaceOfIssue = userTranslation.PassportPlaceOfIssue?.Trim();
                    userTranslationInfo.ResidentRegister = userTranslation.ResidentRegister?.Trim();
                    userTranslationInfo.TitleName = titleTranslationInfo.Name;
                    userTranslationInfo.PositionName = positionTranslationInfo.Name;
                    userTranslationInfo.OfficeName = officeTranslationInfo.Name;

                    await _userTranslationRepository.Save(userTranslation);
                }
            }

            int result = await _userRepository.Update(userInfo);
            if (result < 0)
            {
                return new ActionResultResponse<string>(result,
                _sharedResourceService.GetString("Something went wrong. Please contact with administrator."), "", userInfo.ConcurrencyStamp);
            }

            // Update UserPosition
            if (userInfo.PositionId != oldPositionId || userInfo.OfficeId != oldOfficeId)
            {
                bool exists = await _userPositionRepository.CheckExists(userInfo.Id, userInfo.PositionId, userInfo.OfficeId);
                if (exists)
                {
                    var userPositionOfficeInfo = await _userPositionRepository.GetInfo(userInfo.Id, userInfo.PositionId, userInfo.OfficeId);
                    if (userPositionOfficeInfo != null)
                    {
                        await _userPositionRepository.UpdateDefaultState(userInfo.Id, false);
                        userPositionOfficeInfo.IsDefault = true;
                        await _userPositionRepository.Update(userPositionOfficeInfo);
                    }
                }
                else
                {
                    UserPositioncs userPositionInfo = await _userPositionRepository.GetDefaultInfo(userInfo.Id, userInfo.PositionId, oldOfficeId, true);
                    if (userPositionInfo == null)
                    {
                        await _userPositionRepository.UpdateDefaultState(userInfo.Id, false);
                        // Insert new position for this user
                        await _userPositionRepository.Insert(new UserPositioncs
                        {
                            OfficeId = userMeta.OfficeId,
                            OfficeIdPath = officeInfo.IdPath,
                            UserId = id,
                            PositionId = positionInfo.Id,
                            IsDefault = true
                        });
                    }
                    else
                    {
                        userPositionInfo.PositionId = positionInfo.Id;
                        userPositionInfo.OfficeId = userMeta.OfficeId;
                        userPositionInfo.OfficeIdPath = officeInfo.IdPath;

                        await _userPositionRepository.Update(userPositionInfo);
                    }
                }
            }

            // Update UserAccount
            if (userMeta.FullName != oldFullName || userMeta.Avatar != oldAvatar)
            {
                ActionResultResponse resultUpdateUserAccount = await _httpClientService.PostAsync<ActionResultResponse>($"{apiUrls.CoreApiUrl}/accounts/{userInfo.Id}",
                    new Dictionary<string, string>
                {
                    {"FullName",  userMeta.FullName},
                    {"UserId",  userInfo.Id},
                    {"TenantId",  tenantId},
                    {"Avatar", userMeta.Avatar },
                    {"IsActive", userMeta.Status <= UserStatus.Official ? true.ToString() : false.ToString()}
                });
            }

            return new ActionResultResponse<string>(result < 0 ? result : 1, result < 0 ?
                _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                 : _sharedResourceService.GetString("Update User success"), "", userInfo.ConcurrencyStamp);
        }

        public async Task<ActionResultResponse> UpdateAvatar(string tenantId, string userId, string avatar)
        {
            ApiUrlSettings apiUrls = _configuration.GetApiUrl();
            if (apiUrls == null)
                return new ActionResultResponse(-14, _sharedResourceService.GetString("Missing some configuration. Please contact with administrator."));

            var userInfo = await _userRepository.GetInfo(tenantId, userId, true);
            if (userInfo == null)
                return new ActionResultResponse(-1, _resourceService.GetString("User does not exists"));

            int result = await _userRepository.UpdateAvatar(tenantId, userId, avatar);
            ActionResultResponse resultUpdateUserAccount = await _httpClientService.PostAsync<ActionResultResponse>($"{apiUrls.CoreApiUrl}/accounts/{userId}",
                   new Dictionary<string, string>
               {
                    {"FullName",  userInfo.FullName},
                    {"UserId",  userInfo.Id},
                    {"TenantId",  tenantId},
                    {"Avatar", avatar },
                    {"IsActive", userInfo.Status <= UserStatus.Official ? true.ToString() : false.ToString()}
               });

            return new ActionResultResponse(result, _resourceService.GetString("Update success"));
        }

        public async Task<ActionResultResponse> UpdateStatus(string tenantId, string userId, UserStatus status)
        {
            int result = await _userRepository.UpdateStatus(tenantId, userId, status);
            if (result == -1)
                return new ActionResultResponse(-1, _resourceService.GetString("User does not exists"));

            return new ActionResultResponse(result, _resourceService.GetString("Update success"));
        }

        public async Task<SearchResult<UserSearchViewModel>> Search(string tenantId, string languageId, string keyword, UserStatus? status,
           string officeIdPath, string positionId, Gender? gender, byte? monthBirthDay, int? yearBirthday, AcademicRank? academicRank,
            int page, int pageSize)
        {
            if (!string.IsNullOrEmpty(officeIdPath))
            {
                var searchOfficeInfo = await _officeRepository.GetInfo(officeIdPath, true);
                if (searchOfficeInfo == null)
                    return new SearchResult<UserSearchViewModel>
                    {
                        Code = -4,
                        Message = _resourceService.GetString("Office does not exists. Please contact administrator")
                    };
            }
            return new SearchResult<UserSearchViewModel>
            {
                Items = await _userRepository.Search(tenantId, languageId, keyword, status, officeIdPath, positionId,
                    gender, monthBirthDay, yearBirthday, academicRank, page, pageSize, out int totalRows),
                TotalRows = totalRows
            };
        }

        public async Task<SearchResult<ExportUserViewModel>> GetListExportUser(string tenantId, string languageId, string keyword, UserStatus? status, string officeIdPath,
            string positionId, Gender? gender, byte? monthBirthDay, int? yearBirthday, AcademicRank? academicRank)
        {
            if (!string.IsNullOrEmpty(officeIdPath))
            {
                var searchOfficeInfo = await _officeRepository.GetInfo(officeIdPath, true);
                if (searchOfficeInfo == null)
                    return new SearchResult<ExportUserViewModel>
                    {
                        Code = -4,
                        Message = _resourceService.GetString("Office does not exists. Please contact administrator")
                    };
            }

            return new SearchResult<ExportUserViewModel>
            {
                Items = await _userRepository.GetListExportUser(tenantId, languageId, keyword, status, officeIdPath, positionId,
                    gender, monthBirthDay, yearBirthday, academicRank),
                TotalRows = 0
            };
        }

        public async Task<UserDetailViewModel> GetUserDetail(string tenantId, string id)
        {
            User userInfo = await _userRepository.GetInfo(tenantId, id, true);
            if (userInfo == null)
                return null;

            List<UserTranslationViewModel> listUserTranslation = await _userTranslationRepository.GetByUserId(id);
            List<UserContactViewModel> listUserContact = await _userContactRepository.GetByUserId(tenantId, id);

            UserDetailViewModel userDetail = new UserDetailViewModel()
            {
                Id = userInfo.Id,
                FullName = userInfo.FullName,
                UserName = userInfo.UserName,
                Avatar = userInfo.Avatar,
                Birthday = userInfo.Birthday,
                IdCardNumber = userInfo.IdCardNumber,
                IdCardDateOfIssue = userInfo.IdCardDateOfIssue,
                Gender = userInfo.Gender,
                Ethnic = userInfo.Ethnic,
                EthnicName = userInfo.EthnicName,
                DenominationName = userInfo.DenominationName,
                Tin = userInfo.Tin,
                BankingNumber = userInfo.BankingNumber,
                NationalId = userInfo.NationalId,
                ProvinceId = userInfo.ProvinceId,
                ProvinceName = userInfo.ProvinceName,
                DistrictId = userInfo.DistrictId,
                DistrictName = userInfo.DistrictName,
                Denomination = userInfo.Denomination,
                MarriedStatus = userInfo.MarriedStatus,
                Status = userInfo.Status,
                OfficeId = userInfo.OfficeId,
                OfficeIdPath = userInfo.OfficeIdPath,
                PositionId = userInfo.PositionId,
                TitleId = userInfo.TitleId,
                UserType = userInfo.UserType,
                PassportId = userInfo.PassportId,
                PassportDateOfIssue = userInfo.PassportDateOfIssue,
                EnrollNumber = userInfo.EnrollNumber,
                CardNumber = userInfo.CardNumber,
                Ext = userInfo.Ext,
                TitlePrefixing = userInfo.TitlePrefixing,
                AcademicRank = userInfo.AcademicRank,
                ConcurrencyStamp = userInfo.ConcurrencyStamp,
                JoinedDate = userInfo.JoinedDate,
                UserTranslations = listUserTranslation,
                UserContacts = listUserContact,
            };

            return userDetail;
        }

        public async Task<ActionResultResponse> UpdateDirectManager(string tenantId, string id, string managerId)
        {
            User userInfo = await _userRepository.GetInfo(id);
            if (userInfo == null)
                return new ActionResultResponse(-1, _resourceService.GetString("User does not exists"));

            if (string.IsNullOrEmpty(managerId))
            {
                userInfo.ManagerUserId = null;
                userInfo.ManagerFullName = null;
            }
            else
            {
                User managerInfo = await _userRepository.GetInfo(managerId, true);
                if (managerInfo == null)
                    return new ActionResultResponse(-1, _resourceService.GetString("Manager direct does not exists"));

                if (userInfo.ApproverUserId == managerInfo.Id)
                    return new ActionResultResponse(-1, _resourceService.GetString("\"{0}\" is manager approve", managerInfo.FullName));

                userInfo.ManagerUserId = managerInfo.Id;
                userInfo.ManagerFullName = managerInfo.FullName;
            }

            int result = await _userRepository.UpdateDirectManager(tenantId, id, managerId);
            if (result <= 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            await _mediator.Publish(new UpdateManagerCommand
            {
                TenantId = userInfo.TenantId,
                UserId = userInfo.Id,
                ManagerId = userInfo.ManagerUserId,
                IsManager = true
            });
            return new ActionResultResponse(result, _resourceService.GetString("Update manager direct User successfull"));
        }

        public async Task<ActionResultResponse> UpdateApproveManager(string tenantId, string id, string approveId)
        {
            User userInfo = await _userRepository.GetInfo(id);
            if (userInfo == null)
            {
                return new ActionResultResponse(-1, _resourceService.GetString("User does not exists"));
            }

            if (string.IsNullOrEmpty(approveId))
            {
                userInfo.ApproverUserId = null;
                userInfo.ApproverFullName = null;
            }
            else
            {
                User approveInfo = await _userRepository.GetInfo(approveId, true);
                if (approveInfo == null)
                {
                    return new ActionResultResponse(-1, _resourceService.GetString("Manager approve does not exists"));
                }

                if (userInfo.ManagerUserId == approveInfo.Id)
                {
                    return new ActionResultResponse(-1, _resourceService.GetString("\"{0}\" is manager direct", approveInfo.FullName));
                }

                userInfo.ApproverUserId = approveInfo.Id;
                userInfo.ApproverFullName = approveInfo.FullName;
            }

            int result = await _userRepository.UpdateApproveManager(tenantId, id, approveId);
            if (result <= 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            await _mediator.Publish(new UpdateManagerCommand
            {
                TenantId = userInfo.TenantId,
                UserId = userInfo.Id,
                ManagerId = userInfo.ApproverUserId,
                IsManager = false
            });

            return new ActionResultResponse(result, _resourceService.GetString("Update manager direct User successfull"));
        }

        public async Task<SearchResult<UserSuggestionViewModel>> GetUserForSuggestion(string tenantId, string languageId, string keyword,
            int? officeId, int page, int pageSize)
        {
            List<UserSuggestionViewModel> items = await _userRepository.GetUserForSuggestion(tenantId, languageId, keyword, officeId, page, pageSize, out int totalRows);
            return new SearchResult<UserSuggestionViewModel>
            {
                Items = items,
                TotalRows = totalRows
            };
        }

        public async Task<SearchResult<UserSearchForManagerConfigViewModel>> SearchForConfigManager(string tenantId, string languageId, string keyword, int officeId, byte? type,
            bool? isGetStaffFromChildOffice, int page, int pageSize)
        {
            Office officeInfo = await _officeRepository.GetActiveInfo(officeId, true);
            if (officeInfo == null)
            {
                return new SearchResult<UserSearchForManagerConfigViewModel>()
                {
                    Code = -1,
                    Message = _resourceService.GetString("Office does not select."),
                };
            }

            return new SearchResult<UserSearchForManagerConfigViewModel>()
            {
                Items = await _userRepository.SearchForConfigManager(tenantId, languageId, keyword, officeId, type,
                 isGetStaffFromChildOffice, page, pageSize, out int totalRows),
                TotalRows = totalRows,
            };
        }

        //Lay danh sach nhan vien cung cap hoac cap duoi
        public async Task<SearchResult<ShortUserInfoViewModel>> GetListUserChildren(string tenantId, string userId,
            string languageId, bool isShowChildren, int page, int pageSize)
        {
            var userInfo = await _httpClientService.GetUserInfo(tenantId, userId);
            if (userInfo == null)
                return new SearchResult<ShortUserInfoViewModel>
                {
                    Items = null,
                };

            if (!isShowChildren)
            {
                return new SearchResult<ShortUserInfoViewModel>
                {
                    Items = await _userRepository.GetListUserByOfficeId(tenantId, userInfo.OfficeId, languageId,
                    page, pageSize, out var totalRow2),
                    TotalRows = totalRow2
                };
            }

            return new SearchResult<ShortUserInfoViewModel>
            {
                Items = await _userRepository.GetListUserByOfficeIdPath(tenantId, userInfo.OfficeIdPath, languageId,
                page, pageSize, out var totalRow1),
                TotalRows = totalRow1
            };
        }

        public async Task<ActionResultResponse<bool>> CheckUserNameExists(string userName)
        {
            return new ActionResultResponse<bool>
            {
                Data = await _userRepository.CheckUserNameExists(string.Empty, userName),
            };
        }

        public async Task<ActionResultResponse> UpdateDirectManagerAndApproveManager(string tenantId, List<string> userIds, string managerId, string approveId)
        {
            if (userIds == null || !userIds.Any())
            {
                return new ActionResultResponse(-1, _resourceService.GetString("Please select one user"));
            }

            if (string.IsNullOrEmpty(managerId) && string.IsNullOrEmpty(approveId))
            {
                return new ActionResultResponse(-10, _resourceService.GetString("Please select manager"));
            }

            User managerInfo = await _userRepository.GetInfo(tenantId, managerId, true);
            if (managerInfo == null && !string.IsNullOrEmpty(managerId))
            {
                return new ActionResultResponse(-3, _resourceService.GetString("Manager direct does not exists"));
            }

            User approveInfo = await _userRepository.GetInfo(tenantId, approveId, true);
            if (approveInfo == null && !string.IsNullOrEmpty(approveId))
            {
                return new ActionResultResponse(-4, _resourceService.GetString("Manager approve does not exists"));
            }

            if ((!string.IsNullOrEmpty(managerId) && managerId.Equals(approveId)))
            {
                return new ActionResultResponse(-5, _resourceService.GetString("Manager direct is manager approve"));
            }

            if ((!string.IsNullOrEmpty(approveId) && approveId.Equals(managerId)))
            {
                return new ActionResultResponse(-6, _resourceService.GetString("Manager approve is manager direct"));
            }

            foreach (string user in userIds)
            {
                User userInfo = await _userRepository.GetInfo(tenantId, user);
                if (userInfo == null)
                {
                    return new ActionResultResponse(-1, _resourceService.GetString("User does not exists"));
                }

                if (userInfo.ManagerUserId == managerId && !string.IsNullOrEmpty(managerId))
                {
                    return new ActionResultResponse(-6, _resourceService.GetString("\"{0}\" already manage direct of \"{1}\"", managerInfo.FullName, userInfo.FullName));
                }

                if (userInfo.ManagerUserId == approveId && !string.IsNullOrEmpty(approveId))
                {
                    return new ActionResultResponse(-7, _resourceService.GetString("\"{0}\" already manage direct of \"{1}\"", approveInfo.FullName, userInfo.FullName));
                }

                if (userInfo.ApproverUserId == approveId && !string.IsNullOrEmpty(approveId))
                {
                    return new ActionResultResponse(-8, _resourceService.GetString("\"{0}\" already manage approve of \"{1}\"", approveInfo.FullName, userInfo.FullName));
                }

                if (userInfo.ApproverUserId == managerId && !string.IsNullOrEmpty(managerId))
                {
                    return new ActionResultResponse(-9, _resourceService.GetString("\"{0}\" already manage approve of \"{1}\"", managerInfo.FullName, userInfo.FullName));
                }

                if (!string.IsNullOrEmpty(managerId))
                {
                    userInfo.ManagerFullName = managerInfo.FullName;
                    userInfo.ManagerUserId = managerId;
                }

                if (!string.IsNullOrEmpty(approveId))
                {
                    userInfo.ApproverUserId = approveId;
                    userInfo.ApproverFullName = approveInfo.FullName;
                }

                await _userRepository.Update(userInfo);
            }

            int result = await _userRepository.UpdateDirectManagerAndApproveManager(userIds, managerId, approveId);
            if (result < 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            return new ActionResultResponse(1, _resourceService.GetString("Update manager direct User successfull"));
        }

        public async Task<ShortUserInfoViewModel> GetShortUserInfo(string tenantId, string userId, string languageId)
        {
            return await _userRepository.GetUserInfo(tenantId, userId, languageId);
        }

        public async Task<List<ShortUserInfoViewModel>> GetShortUserInfoByListUserId(string tenantId, List<string> userIds, string languageId)
        {
            return await _userRepository.GetShortUserInfoByListUserId(tenantId, userIds, languageId);
        }

        public async Task<ShortUserInfoViewModel> GetShorUserInfoOfLeaderOffice(string tenantId, int officeId, string languageId)
        {
            var officeInfo = await _officeRepository.GetInfo(officeId, true);
            if (officeInfo == null)
                return null;

            return await _userRepository.GetShorUserInfoOfLeaderOffice(tenantId, officeId, languageId);
        }

        public async Task<ShortUserInfoViewModel> GetDirectManagerInfo(string tenantId, string userId, string languageId)
        {
            var userInfo = await _userRepository.GetInfo(tenantId, userId, true);
            if (userInfo == null)
                return null;

            return await _userRepository.GetUserInfo(tenantId, userInfo.ManagerUserId, languageId);
        }


        #region Private
        private async Task<long> InsertWorkSchedule(string tenantId, int enrollNumber, string fullName, string unsignName, string userId, string positionId, int officeId)
        {
            ActionResultResponse resultInsertWorkSchedule = await _httpClientService
                .PostAsync<ActionResultResponse>($"{_apiUrls.TimekeepingApiUrl}work-schedules", new Dictionary<string, string>
                    {
                        {"EnrollNumber", enrollNumber.ToString() },
                        {"FullName", fullName },
                        {"UnsignName", unsignName },
                        {"UserId", userId },
                    });

            if (resultInsertWorkSchedule == null || resultInsertWorkSchedule.Code <= 0)
            {
                await RollbackInsertUser(tenantId, userId);
                await RollbackUserAccount(userId);
                await RollbackUserTranslation(userId);
                await RollbackUserContact(tenantId, userId);
                await RollbackUserPosition(userId, positionId, officeId);
            }

            return resultInsertWorkSchedule.Code;
        }

        private async Task<long> InsertTimekeepingMachine(string tenantId, int enrollNumber, string userId, string cardNumber, string fullName, int officeId, string positionId)
        {
            ActionResultResponse resultInsertUserIntoMachine = await _httpClientService.PostAsync<ActionResultResponse>($"{_apiUrls.TimekeepingApiUrl}machines/users",
               new Dictionary<string, string>
               {
                    {"EnrollNumber", enrollNumber.ToString()},
                    {"FullName", fullName},
                    {"CardNumber", cardNumber},
               });

            if (resultInsertUserIntoMachine == null || resultInsertUserIntoMachine.Code <= 0)
            {
                await RollbackInsertUser(tenantId, userId);
                await RollbackUserAccount(userId);
                await RollbackUserTranslation(userId);
                await RollbackUserContact(tenantId, userId);
                await RollbackUserPosition(userId, positionId, officeId);
                await RollbackWorkSchedule(userId);
            }

            return resultInsertUserIntoMachine.Code;
        }

        private async Task RollbackInsertUser(string tenantId, string userId)
        {
            // Rollback title.
            await _userRepository.ForceDelete(tenantId, userId);
        }

        private async Task RollbackUserPosition(string userId, string positionId, int officeId)
        {
            await _userPositionRepository.Delete(userId, positionId, officeId);
        }

        private async Task RollbackWorkSchedule(string userId)
        {
            await _httpClientService.DeleteAsync<ActionResultResponse>(
                $"{_apiUrls.TimekeepingApiUrl}work-schedules/{userId}");
        }

        private async Task RollbackUserAccount(string accountId)
        {
            await _httpClientService.DeleteAsync<ActionResultResponse>($"{_apiUrls.CoreApiUrl}accounts/{accountId}");
        }

        private async Task RollbackUserTranslation(string userId)
        {
            await _userTranslationRepository.DeleteByUserId(userId);
        }

        private async Task RollbackUserContact(string tenantId, string userId)
        {
            await _userContactRepository.DeleteByUserdId(tenantId, userId);
        }
        #endregion
    }
}
