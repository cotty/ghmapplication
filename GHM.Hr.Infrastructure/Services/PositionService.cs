﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.SqlServer;
using GHM.Infrastructure.ViewModels;
using OfficeOpenXml.FormulaParsing.ExpressionGraph.FunctionCompilers;

namespace GHM.Hr.Infrastructure.Services
{
    public class PositionService : RepositoryBase, IPositionService
    {
        private readonly IPositionRepository _positionRepository;
        private readonly IPositionTranslationRepository _positionTranslationRepository;
        private readonly ITitleRepository _titleRepository;
        private readonly ITitleTranslationRepository _titleTranslationRepository;
        private readonly IOfficePositionRepository _officePositionRepository;
        private readonly IOfficeRepository _officeRepository;
        private readonly IUserPositionRepository _userPositionRepository;
        private readonly IUserTranslationRepository _userTranslationRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmHrResource> _hrResourceService;
        public PositionService(IDbContext context, IPositionRepository positionRepository, ITitleRepository titleRepository,
              IUserTranslationRepository userTranslationRepository,
        IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> hrResourceService, IPositionTranslationRepository positionTranslationRepository, ITitleTranslationRepository titleTranslationRepository, IOfficePositionRepository officePositionRepository, IUserPositionRepository userPositionRepository, IOfficeRepository officeRepository) : base(context)
        {
            _positionRepository = positionRepository;
            _titleRepository = titleRepository;
            _sharedResourceService = sharedResourceService;
            _hrResourceService = hrResourceService;
            _positionTranslationRepository = positionTranslationRepository;
            _titleTranslationRepository = titleTranslationRepository;
            _officePositionRepository = officePositionRepository;
            _userPositionRepository = userPositionRepository;
            _officeRepository = officeRepository;
            _userTranslationRepository = userTranslationRepository;
        }


        public async Task<ActionResultResponse> Insert(string tenantId, PositionMeta positionMeta)
        {
            if (!positionMeta.Translations.Any())
                return new ActionResultResponse(-1, _sharedResourceService.GetString("Please enter at least one language."));

            var titleInfo = await _titleRepository.GetInfo(tenantId, positionMeta.TitleId);
            if (titleInfo == null)
                return new ActionResultResponse(-2, _hrResourceService.GetString("Title does not exists."));

            var positionId = Guid.NewGuid().ToString();
            var position = new Position
            {
                Id = positionId,
                ConcurrencyStamp = positionId,
                IsActive = positionMeta.IsActive,
                IsMultiple = positionMeta.IsMultiple,
                IsManager = positionMeta.IsManager,
                CreateTime = DateTime.Now,
                Order = positionMeta.Order,
                IsDelete = false,
                TenantId = tenantId,
                TitleId = titleInfo.Id
            };

            var result = await _positionRepository.Insert(position);
            if (result <= 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            var positionTranslations = new List<PositionTranslation>();
            foreach (var positionTranslationMeta in positionMeta.Translations)
            {
                var isNameExists = await _positionTranslationRepository.CheckExists(positionId, tenantId,
                    positionTranslationMeta.LanguageId, positionTranslationMeta.Name);
                if (isNameExists)
                {
                    await RollbackInsert();
                    return new ActionResultResponse(-3, _hrResourceService.GetString("Position name: \"{0}\" already exists.", positionTranslationMeta.Name));
                }

                var isShortNameExists = await _positionTranslationRepository.CheckShortNameExists(positionId, tenantId,
                    positionTranslationMeta.LanguageId, positionTranslationMeta.ShortName);
                if (isShortNameExists)
                {
                    await RollbackInsert();
                    return new ActionResultResponse(-4, _hrResourceService.GetString("Short name: \"{0}\" already exists.", positionTranslationMeta.ShortName));
                }

                var titleTranslationInfo =
                    await _titleTranslationRepository.GetInfo(tenantId, position.TitleId, positionTranslationMeta.LanguageId,
                        true);

                positionTranslations.Add(new PositionTranslation
                {
                    TenantId = tenantId,
                    PositionId = positionId,
                    Name = positionTranslationMeta.Name.Trim(),
                    ShortName = positionTranslationMeta.ShortName.Trim(),
                    LanguageId = positionTranslationMeta.LanguageId.Trim(),
                    UnsignName = positionTranslationMeta.Name.Trim().StripVietnameseChars().ToUpper(),
                    OtherRequire = positionTranslationMeta.OtherRequire?.Trim(),
                    Description = positionTranslationMeta.Description?.Trim(),
                    Purpose = positionTranslationMeta.Purpose?.Trim(),
                    Responsibility = positionTranslationMeta.Responsibility?.Trim(),
                    TitleName = titleTranslationInfo != null ? titleTranslationInfo.Name : string.Empty
                });
            }

            // Insert position translations.
            var resultInsertTranslation = await _positionTranslationRepository.Inserts(positionTranslations);
            if (resultInsertTranslation <= 0)
            {
                await RollbackInsert();
                return new ActionResultResponse(-5, _hrResourceService.GetString("Can not insert new title. Please contact with administrator."));
            }

            // Add office position references.
            await InsertOfficePosition(positionMeta.OfficeIds);

            return new ActionResultResponse(resultInsertTranslation, _hrResourceService.GetString("Add new position successful."));

            #region Local Functions
            async Task InsertOfficePosition(List<int> officeIds)
            {
                var officePositions = new List<OfficePosition>();
                foreach (var officeId in officeIds)
                {
                    var officeInfo = await _officeRepository.GetInfo(officeId, true);
                    if (officeInfo != null)
                    {
                        officePositions.Add(new OfficePosition
                        {
                            TitleId = position.TitleId,
                            OfficeId = officeInfo.Id,
                            PositionId = positionId,
                            TotalUser = 0
                        });
                    }
                }
                await _officePositionRepository.Inserts(officePositions);
            }

            async Task RollbackInsert()
            {
                await _positionRepository.ForceDelete(positionId);
            }
            #endregion
        }

        public async Task<ActionResultResponse> Update(string tenantId, string id, PositionMeta positionMeta)
        {
            if (!positionMeta.Translations.Any())
                return new ActionResultResponse(-1, _sharedResourceService.GetString("Please enter at least one language."));

            var positionInfo = await _positionRepository.GetInfo(id);
            if (positionInfo == null)
                return new ActionResultResponse(-2, _hrResourceService.GetString("Position does not exists."));

            if (positionInfo.ConcurrencyStamp != positionMeta.ConcurrencyStamp)
                return new ActionResultResponse(-3, _hrResourceService.GetString("The position already updated by another people. You can not update this position."));

            var oldTitleId = positionInfo.TitleId;

            positionInfo.IsActive = positionMeta.IsActive;
            positionInfo.Order = positionMeta.Order;
            positionInfo.ConcurrencyStamp = Guid.NewGuid().ToString();
            positionInfo.IsManager = positionMeta.IsManager;
            positionInfo.IsMultiple = positionMeta.IsMultiple;
            positionInfo.TitleId = positionMeta.TitleId;

            // Save position.
            await _positionRepository.Update(positionInfo);

            // Save office position.
            await SaveOfficePosition();

            // Save position translation.
            return await SaveTranslation();

            #region Local Functions
            async Task<ActionResultResponse> SaveTranslation()
            {
                foreach (var positionTranslationMeta in positionMeta.Translations)
                {
                    var isNameExists = await _positionTranslationRepository.CheckExists(positionInfo.Id, tenantId,
                        positionTranslationMeta.LanguageId, positionTranslationMeta.Name);
                    if (isNameExists)
                        return new ActionResultResponse(-4, _hrResourceService.GetString("Position name already exsits."));

                    var isShortNameExists = await _positionTranslationRepository.CheckShortNameExists(positionInfo.Id, tenantId,
                        positionTranslationMeta.LanguageId, positionTranslationMeta.ShortName);
                    if (isShortNameExists)
                        return new ActionResultResponse(-5, _hrResourceService.GetString("Position short name already exsits."));

                    var positionTranslationInfo =
                        await _positionTranslationRepository.GetInfo(positionInfo.Id, positionTranslationMeta.LanguageId);

                    var titleTranslationInfo =
                        await _titleTranslationRepository.GetInfo(tenantId, positionMeta.TitleId, positionTranslationMeta.LanguageId, true);
                    if (positionTranslationInfo == null)
                    {
                        positionTranslationInfo = new PositionTranslation
                        {
                            PositionId = positionInfo.Id,
                            Name = positionTranslationMeta.Name.Trim(),
                            Description = positionTranslationMeta.Description?.Trim(),
                            OtherRequire = positionTranslationMeta.OtherRequire?.Trim(),
                            LanguageId = positionTranslationMeta.LanguageId,
                            ShortName = positionTranslationMeta.ShortName.Trim(),
                            Purpose = positionTranslationMeta.Purpose,
                            Responsibility = positionTranslationMeta.Responsibility,
                            UnsignName = positionTranslationMeta.Name.Trim().StripVietnameseChars().ToUpper(),
                            TitleName = titleTranslationInfo != null ? titleTranslationInfo.Name : string.Empty
                        };
                        await _positionTranslationRepository.Insert(positionTranslationInfo);
                    }
                    else
                    {
                        positionTranslationInfo.Name = positionTranslationMeta.Name.Trim();
                        positionTranslationInfo.Description = positionTranslationMeta.Description?.Trim();
                        positionTranslationInfo.UnsignName = positionTranslationInfo.Name.StripVietnameseChars().ToUpper();
                        positionTranslationInfo.OtherRequire = positionTranslationMeta.OtherRequire?.Trim();
                        positionTranslationInfo.Responsibility = positionTranslationMeta.Responsibility?.Trim();
                        positionTranslationInfo.Purpose = positionTranslationMeta.Purpose?.Trim();
                        positionTranslationInfo.ShortName = positionTranslationMeta.ShortName.Trim();
                        positionTranslationInfo.TitleName = oldTitleId != positionInfo.TitleId
                            ? titleTranslationInfo != null
                                ? titleTranslationInfo.Name
                                : string.Empty
                            : positionTranslationInfo.TitleName;

                        await _positionTranslationRepository.Update(positionTranslationInfo);
                        await _userTranslationRepository.UpdatePositionNameByPositionId(tenantId, id, positionTranslationMeta.Name, positionTranslationMeta.LanguageId);
                    }
                }
                return new ActionResultResponse(1, _hrResourceService.GetString("Update position successful."));
            }

            async Task SaveOfficePosition()
            {
                var officePositions = await _officePositionRepository.GetsByPositionId(positionInfo.Id);
                if (officePositions == null || !officePositions.Any())
                {
                    await InsertOfficePosition(positionMeta.OfficeIds);
                }
                else
                {
                    var existingOfficePositions =
                        positionMeta.OfficeIds.Intersect(officePositions.Select(x => x.OfficeId));
                    var listNewsOfficePositions =
                        positionMeta.OfficeIds.Where(x => !existingOfficePositions.Contains(x)).ToList();
                    var listRemoveOfficePosition =
                        officePositions.Select(x => x.OfficeId).Except(positionMeta.OfficeIds).ToList();

                    // Add new office position if doesn't exists.
                    if (listNewsOfficePositions.Any())
                    {
                        await InsertOfficePosition(listNewsOfficePositions);
                    }

                    // Remove from office position if doesn't exists in position meta.
                    if (listRemoveOfficePosition.Any())
                    {
                        await RemoveOfficePosition(listRemoveOfficePosition);
                    }
                }
            }

            async Task InsertOfficePosition(List<int> officeIds)
            {
                var officePositions = new List<OfficePosition>();
                foreach (var officeId in officeIds)
                {
                    var officeInfo = await _officeRepository.GetInfo(officeId, true);
                    if (officeInfo != null)
                    {
                        officePositions.Add(new OfficePosition
                        {
                            TitleId = positionInfo.TitleId,
                            OfficeId = officeInfo.Id,
                            PositionId = positionInfo.Id,
                            TotalUser = 0
                        });
                    }
                }
                await _officePositionRepository.Inserts(officePositions);
            }

            async Task RemoveOfficePosition(IEnumerable<int> officeIds)
            {
                foreach (var officeId in officeIds)
                {
                    // Check user exists by office.
                    var userExists =
                        await _userPositionRepository.CheckExistsByOfficeIdAndPositionId(officeId, positionInfo.Id);
                    if (userExists)
                        continue;

                    await _officePositionRepository.Delete(positionInfo.Id, officeId);
                }
            }
            #endregion
        }

        public async Task<ActionResultResponse> Delete(string id, string tenantId)
        {
            var info = await _positionRepository.GetInfo(id);
            if (info == null)
                return new ActionResultResponse(-1, _hrResourceService.GetString("Position does not exists."));

            // Check office reference.
            var isUsedByOffice = await _officePositionRepository.CheckExistsByPositionId(info.Id);
            if (isUsedByOffice)
                return new ActionResultResponse(-2, _hrResourceService.GetString("Position is used in office. Can not delete this position."));

            // Check user reference.
            var isUsedByUser = await _userPositionRepository.CheckExistsByPositionId(info.Id);
            if (isUsedByUser)
                return new ActionResultResponse(-3, _hrResourceService.GetString("Position is used by user. Can not delete this position."));

            var result = await _positionRepository.Delete(id, tenantId);
            if (result <= 0)
                new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            await _positionTranslationRepository.DeleteByPositionId(tenantId, id);
            return new ActionResultResponse(result, _hrResourceService.GetString("Delete position successful."));
        }

        public async Task<ActionResultResponse<PositionDetailViewModel>> GetDetail(string tenantId, string languageId, string id)
        {
            var positionInfo = await _positionRepository.GetInfo(id, true);
            if (positionInfo == null)
                return new ActionResultResponse<PositionDetailViewModel>(-1, _hrResourceService.GetString("Position does not exists."));

            if (positionInfo.TenantId != tenantId)
                return new ActionResultResponse<PositionDetailViewModel>(-2, _hrResourceService.GetString("You do not have permission to view this position."));

            var positionTranslations = await _positionTranslationRepository.GetsByPositionId(tenantId, id);
            var officePositions = await _officePositionRepository.GetsOfficeInfoByPositionId(languageId, positionInfo.Id);
            var positionDetail = new PositionDetailViewModel
            {
                Id = positionInfo.Id,
                IsActive = positionInfo.IsActive,
                IsManager = positionInfo.IsManager,
                IsMultiple = positionInfo.IsMultiple,
                Order = positionInfo.Order,
                TitleId = positionInfo.TitleId,
                ConcurrencyStamp = positionInfo.ConcurrencyStamp,
                PositionTranslations = positionTranslations,
                OfficesPositions = officePositions
            };
            return new ActionResultResponse<PositionDetailViewModel>
            {
                Data = positionDetail
            };
        }

        public async Task<SearchResult<PositionViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive,
            bool? isManager, bool? isMultiple, int page, int pageSize)
        {
            var items = await _positionRepository.Search(tenantId, languageId, keyword, isActive, isManager, isMultiple, page, pageSize,
                out var totalRows);
            return new SearchResult<PositionViewModel>
            {
                Items = items,
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse<TitleSearchForSelectViewModel>> GetTitleByPositionId(string positionId, string languageId, string tenantId)
        {
            var positionInfo = await _positionRepository.GetInfo(positionId, true);
            if (positionInfo == null)
                return new ActionResultResponse<TitleSearchForSelectViewModel>
                {
                    Code = -1,
                    Message = _hrResourceService.GetString("Position does not exists."),
                };

            var titleInfo = await _titleRepository.GetInfo(tenantId, positionInfo.TitleId, true);
            if (titleInfo == null)
                return new ActionResultResponse<TitleSearchForSelectViewModel>
                {
                    Code = -2,
                    Message = _hrResourceService.GetString("Title does not exists."),
                };

            var titleSerchForSelect = Context.Set<TitleTranslation>().Where(x => x.LanguageId == languageId && x.TitleId == titleInfo.Id)
                .Select(x => new TitleSearchForSelectViewModel
                {
                    Id = titleInfo.Id,
                    Name = x.Name
                }).FirstOrDefault();

            if (titleSerchForSelect == null)
                return new ActionResultResponse<TitleSearchForSelectViewModel>
                {
                    Code = -3,
                    Message = _hrResourceService.GetString("Title does not exists for language."),
                };

            return new ActionResultResponse<TitleSearchForSelectViewModel>
            {
                Data = titleSerchForSelect
            };
        }

        public async Task<List<PositionSearchForSelectViewModel>> SearchForSelect(string tenantId, string languageId, string keyword)
        {
            return await _positionRepository.SearchForSelect(tenantId, languageId, keyword);
        }
    }
}
