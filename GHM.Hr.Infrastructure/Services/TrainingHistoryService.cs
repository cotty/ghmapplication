﻿using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Hr.Infrastructure.Services
{
    public class TrainningHistoryService : ITrainingHistoryService
    {
        private readonly ITrainingHistoryRepository _trainingHistoryRepository;
        private readonly IResourceService<GhmHrResource> _resourceService;
        private readonly IUserRepository _userRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IUserValueRepository _userValueRepository;
        public TrainningHistoryService(ITrainingHistoryRepository trainingHistoryRepository, IResourceService<GhmHrResource> resourceService,
            IResourceService<SharedResource> sharedResource, IUserRepository userRepository,
            IUserValueRepository userValueRepository)
        {
            _trainingHistoryRepository = trainingHistoryRepository;
            _resourceService = resourceService;
            _sharedResourceService = sharedResource;
            _userValueRepository = userValueRepository;
            _userRepository = userRepository;
        }
        public async Task<ActionResultResponse> Delete(string id, string tenantId)
        {
            var checkExist = await _trainingHistoryRepository.CheckExist(tenantId, id);
            if (!checkExist)
                return new ActionResultResponse(-1, _resourceService.GetString("Training history does not exist"));

            var result = await _trainingHistoryRepository.Delete(tenantId, id);

            if (result < 0)
                return new ActionResultResponse(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            return new ActionResultResponse(1, _resourceService.GetString("Delete training history successfully!"));
        }

        public async Task<ActionResultResponse<string>> Insert(string tenantId, string userId, string fullName, TrainingHistoryMeta training)
        {
            var courseExist = await _userValueRepository.CheckExist(tenantId, training.CourseId, training.CourseName, Domain.Constants.UserValueType.TrainingCourse);
            if (!courseExist)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Course doest not exist"));

            var trainingPlaceExist = await _userValueRepository.CheckExist(tenantId, training.CoursePlaceId, training.CoursePlaceName, Domain.Constants.UserValueType.TrainingPlace);
            if (!trainingPlaceExist)
                return new ActionResultResponse<string>(-2, _resourceService.GetString("Training place does not eixst"));

          
            var userInfo = await _userRepository.GetInfo(training.UserId);
            if (userInfo == null)
                return new ActionResultResponse<string>(-4, _resourceService.GetString("User does not exist"));

            var trainingHistoryExist = await _trainingHistoryRepository.CheckExist(tenantId, userInfo.Id, training.CourseId, training.CourseName, training.CoursePlaceId, training.CoursePlaceName);
            if (trainingHistoryExist)
                return new ActionResultResponse<string>(-3, _resourceService.GetString("Trainning history does exist"));

            var userTrainingHistory = new UserTrainingHistory
            {
                CourseId = training.CourseId,
                CourseName = training.CourseName,
                CoursePlaceId = training.CoursePlaceId,
                CoursePlaceName = training.CoursePlaceName,
                FromDate = training.FromDate,
                ToDate = training.ToDate,
                FullName = userInfo.FullName,
                UserId = userInfo.Id,
                Result = training.Result,
                IsHasCertificate = training.IsHasCertificate,
                UnsignName = fullName.StripVietnameseChars(),
                TenantId = tenantId
            };
            var result = await _trainingHistoryRepository.Insert(userTrainingHistory);
            if (result < 0)
                return new ActionResultResponse<string>(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            return new ActionResultResponse<string>(1, _resourceService.GetString("Insert training history successfully !"), "", userTrainingHistory.Id);
        }

        public async Task<SearchResult<UserTrainingHistory>> Search(string tenantId, string keyword, string userId,
            bool? type, string courseId,string courseName, 
            string coursePlaceId, string coursePalceName, bool? isHasCertification,
            DateTime? fromDate, DateTime? toDate, int page, int pageSize)
        {
            var items = await _trainingHistoryRepository.Search(tenantId, keyword, userId, type, courseId, courseName, coursePlaceId, coursePalceName, isHasCertification, fromDate, toDate, page, pageSize, out var totalRows);

            return new SearchResult<UserTrainingHistory>
            {
                Items = items,
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse<string>> Update(string id, string tenantId, string userId, string fullName, TrainingHistoryMeta training)
        {
            var info = await _trainingHistoryRepository.GetInfo(tenantId, id, false);

            if (info == null)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("TrainingHistory does not Exist"));

            if (info.ConcurrencyStamp != training.ConcurrencyStamp)
                return new ActionResultResponse<string>(-3, _resourceService.GetString("Training history was updated before"));

            var courseExist = await _userValueRepository.CheckExist(tenantId, training.CourseId, training.CourseName, Domain.Constants.UserValueType.TrainingCourse);
            if (!courseExist)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Course doest not exist"));

            var trainingPlaceExist = await _userValueRepository.CheckExist(tenantId, training.CoursePlaceId, training.CoursePlaceName, Domain.Constants.UserValueType.TrainingPlace);

            if (!trainingPlaceExist)
                return new ActionResultResponse<string>(-2, _resourceService.GetString("Training place does not eixst"));

            info.CourseId = training.CourseId;
            info.CourseName = training.CourseName;
            info.CoursePlaceId = training.CoursePlaceId;
            info.CoursePlaceName = training.CoursePlaceName;
            info.FromDate = training.FromDate;
            info.ToDate = training.ToDate;
            info.Result = training.Result;
            info.IsHasCertificate = training.IsHasCertificate;
            info.Type = training.Type;
            info.ConcurrencyStamp = new Guid().ToString();

            return await _trainingHistoryRepository.Update(info) < 0 ? new ActionResultResponse<string>(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)) : new ActionResultResponse<string>(1, _resourceService.GetString("Update training success fully"), "", info.ConcurrencyStamp);
        }
    }
}
