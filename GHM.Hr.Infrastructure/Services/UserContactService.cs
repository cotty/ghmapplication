﻿using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using System;
using System.Threading.Tasks;

namespace GHM.Hr.Infrastructure.Services
{
    public class UserContactService : IUserContactService
    {
        private readonly IUserContactRepository _userContactRepository;
        private readonly IResourceService<GhmHrResource> _resourceService;
        private readonly IResourceService<SharedResource> _sharedResourceService;

        public UserContactService(IUserContactRepository userContactRepository, IResourceService<GhmHrResource> resourceService, IResourceService<SharedResource> sharedResourceService)
        {
            _userContactRepository = userContactRepository;
            _resourceService = resourceService;
            _sharedResourceService = sharedResourceService;
        }

        public async Task<ActionResultResponse<string>> Insert(string tennantId, UserContactMeta userContactMeta)
        {
            var id = Guid.NewGuid().ToString();
            if (await _userContactRepository.CheckExistsByValue(tennantId, id, userContactMeta.UserId, userContactMeta.ContactType, userContactMeta.ContactValue))
                return new ActionResultResponse<string>(-1, _resourceService.GetString("User Contact already exists"));

            var userContact = new UserContact()
            {
                Id = id,
                TenantId = tennantId,
                ContactType = userContactMeta.ContactType,
                ContactValue = userContactMeta.ContactValue,
                UserId = userContactMeta.UserId
            };

            var result = await _userContactRepository.Insert(userContact);
            if (result <= 0)
                return new ActionResultResponse<string>(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            return new ActionResultResponse<string>(result, _resourceService.GetString("Insert info contact success"), string.Empty, userContact.Id);
        }

        public async Task<ActionResultResponse> Update(string tenantId, string id, UserContactMeta userContactMeta)
        {
            var info = await _userContactRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, _resourceService.GetString("Info contact does not exists"));

            if (info.ConcurrencyStamp != userContactMeta.ConcurrencyStamp)
                return new ActionResultResponse(-3, _resourceService.GetString("The userconatct already updated by other people. You can not update this usercontact."));

            if (await _userContactRepository.CheckExistsByValue(tenantId, info.Id, info.UserId, info.ContactType, info.ContactValue))
                return new ActionResultResponse(-2, _resourceService.GetString("Info contact already exists"));

            info.ContactType = userContactMeta.ContactType;
            info.ContactValue = userContactMeta.ContactValue;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();

            var result = await _userContactRepository.Update(info);
            if (result <= 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            return new ActionResultResponse(result, _resourceService.GetString("Update info contact success"));
        }

        public async Task<ActionResultResponse> Delete(string tenantId,string id)
        {
            var info = await _userContactRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, _resourceService.GetString("Info contact does not exists"));

            var result = await _userContactRepository.Delete(tenantId, id);
            if (result <= 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));
            return new ActionResultResponse(result, _resourceService.GetString("Delete info contact success"));
        }
    }
}
