﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GHM.Events;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml.ConditionalFormatting;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;

namespace GHM.Hr.Infrastructure.Services
{
    public class UserAssessmentService : IUserAssessmentService
    {
        private readonly IUserAssessmentRepository _userAssessmentRepository;
        private readonly IUserAssessmentCriteriaRepository _userAssessmentCriteriaRepository;
        private readonly IAssessmentConfigService _assessmentConfigService;
        private readonly IUserRepository _userRepository;
        private readonly ICriteriaPositionRepository _criteriaPositionRepository;

        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmHrResource> _resourceService;


        public UserAssessmentService(IUserAssessmentRepository userAssessmentRepository,
            IUserAssessmentCriteriaRepository userAssessmentCriteriaRepository, IAssessmentConfigService assessmentConfigService,
            IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> resourceService,
            IUserRepository userRepository, ICriteriaPositionRepository criteriaPositionRepository)
        {
            _userAssessmentRepository = userAssessmentRepository;
            _userAssessmentCriteriaRepository = userAssessmentCriteriaRepository;
            _assessmentConfigService = assessmentConfigService;
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
            _userRepository = userRepository;
            _criteriaPositionRepository = criteriaPositionRepository;
        }

        public async Task<ActionResultResponse<string>> Register(string tenantId, string languageId, string userId,
            byte month, int year)
        {
            if (month > DateTime.Now.Month && year >= DateTime.Now.Year)
                return new ActionResultResponse<string>(-1,
                    _resourceService.GetString("Assessment time can not be greater than current date"));

            var validateAssessmentTime = await CheckAssessmentTime(tenantId);
            if (validateAssessmentTime.Code <= 0)
                return validateAssessmentTime;

            // Kiểm tra người dùng đã đăng ký chưa.
            var userAssessmentId = await _userAssessmentRepository.GetUserAssessmentId(tenantId, userId, month, year);
            if (!string.IsNullOrEmpty(userAssessmentId))
                return new ActionResultResponse<string>
                {
                    Data = userAssessmentId
                };

            // Thêm mới kỳ đánh giá thành tích.
            return await AddUserAssessment();

            // Lấy về thông tin chi tiết đánh giá.
            #region Local functions
            async Task<ActionResultResponse<string>> AddUserAssessment()
            {
                // Lấy về thông tin người dùng đăng ký.
                var userInfo = await _userRepository.GetInfo(tenantId, userId, true);
                if (userInfo == null)
                    return new ActionResultResponse<string>(-3,
                        _sharedResourceService.GetString(ErrorMessage.NotFound,
                            _resourceService.GetString("User info")));

                if (string.IsNullOrEmpty(userInfo.ManagerUserId))
                    return new ActionResultResponse<string>(-4,
                        _resourceService.GetString(
                            "Please contact the administrator for configuration manager."));

                // Lấy về danh sách tiêu chí cấu hình theo chức vụ.
                var listCriteria = await _criteriaPositionRepository.GetsByPositionId(tenantId, userInfo.PositionId);
                if (listCriteria == null || !listCriteria.Any())
                    return new ActionResultResponse<string>(-5,
                            _resourceService.GetString(
                                "Position haven't config criteria yet. Please contact with administrator."));
                var userAssessment = new UserAssessment
                {
                    TenantId = tenantId,
                    Year = year,
                    Month = month,
                    Quarter = (new DateTime(year, month, 1)).GetQuarter(),
                    ApproverUserId = userInfo.ApproverUserId,
                    ManagerUserId = userInfo.ManagerUserId,
                    OfficeId = userInfo.OfficeId,
                    PositionId = userInfo.PositionId,
                    UserId = userInfo.Id,
                    UserAssessmentCriterias = listCriteria.Select(x => new UserAssessmentCriteria
                    {
                        CriteriaId = x.CriteriaId,
                        Point = x.Point,
                        UserPoint = x.Point
                    }).ToList(),
                };
                userAssessment.TotalPoint = userAssessment.UserAssessmentCriterias.Sum(x => x.Point);
                userAssessment.TotalUserPoint = userAssessment.TotalPoint;

                var resultInsert = await _userAssessmentRepository.Insert(userAssessment);
                if (resultInsert <= 0)
                    return new ActionResultResponse<string>(resultInsert, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

                return new ActionResultResponse<string>
                {
                    Message = _resourceService.GetString("Register assessment successful."),
                    Data = userAssessment.Id
                };
            }
            #endregion
        }

        public async Task<ActionResultResponse<UserAssessmentViewModel>> GetDetail(string tenantId, string languageId, string userId,
            string currentUserId, byte month, int year)
        {
            var userAssessment = await _userAssessmentRepository.GetDetail(tenantId, languageId, userId, month, year);
            if (userAssessment == null)
                return new ActionResultResponse<UserAssessmentViewModel>(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound, _resourceService.GetString("assessment")));

            userAssessment.UserAssessmentCriterias =
                await _userAssessmentCriteriaRepository.GetListAssessmentCriteria(userAssessment.Id, languageId);
            var checkAssessmentTime = await CheckAssessmentTime(tenantId);
            userAssessment.AllowAssessment = checkAssessmentTime.Code > 0;
            return new ActionResultResponse<UserAssessmentViewModel>
            {
                Data = userAssessment
            };
        }

        public async Task<ActionResultResponse<UserAssessmentViewModel>> GetDetail(string tenantId, string languageId, string userId, string id)
        {
            var userAssessment = await _userAssessmentRepository.GetDetail(tenantId, languageId, id);
            if (userAssessment == null)
                return new ActionResultResponse<UserAssessmentViewModel>(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound, _resourceService.GetString("assessment")));

            if (userAssessment.UserId != userId && userAssessment.ManagerUserId != userId &&
                userAssessment.ApproverUserId != userId)
                return new ActionResultResponse<UserAssessmentViewModel>(-403,
                    _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            userAssessment.UserAssessmentCriterias =
                await _userAssessmentCriteriaRepository.GetListAssessmentCriteria(userAssessment.Id, languageId);
            var checkAssessmentTime = await CheckAssessmentTime(tenantId);
            userAssessment.AllowAssessment = checkAssessmentTime.Code > 0;
            return new ActionResultResponse<UserAssessmentViewModel>
            {
                Data = userAssessment
            };
        }

        public async Task<SearchResult<UserAssessmentViewModel>> SearchMyAssessment(string tenantId, string languageId, string userId, int year)
        {
            var result = await _userAssessmentRepository.UserSearchAssessment(tenantId, languageId, userId, year);
            return new SearchResult<UserAssessmentViewModel>(result);
        }

        public async Task<SearchResult<UserAssessmentViewModel>> ManagerSearch(string tenantId, string languageId, string currentUserId,
            string keyword, byte? month, int? year, int page, int pageSize)
        {
            var result = await _userAssessmentRepository.ManagerSearchForApprove(tenantId, languageId, currentUserId,
                keyword, month, year, page, pageSize, out int totalRows);
            return new SearchResult<UserAssessmentViewModel>(result, totalRows);
        }

        public async Task<SearchResult<UserAssessmentViewModel>> ApproverSearch(string tenantId, string languageId, string currentUserId,
            string keyword, byte? month, int? year, int page, int pageSize)
        {
            var result = await _userAssessmentRepository.ApproverSearchForApprove(tenantId, languageId, currentUserId,
                keyword, month, year, page, pageSize, out int totalRows);
            return new SearchResult<UserAssessmentViewModel>(result, totalRows);
        }

        public async Task<ActionResultResponse> UpdateUserAssessmentCriteria(string tenantId, string currentUserId, string id, AssessmentType type,
            UserAssessmentCriteria userAssessmentCriteria)
        {
            var userAssessmentInfo = await _userAssessmentRepository.GetInfo(tenantId, id, true);
            if (userAssessmentInfo == null)
                return new ActionResultResponse(-1, _sharedResourceService.GetString(ErrorMessage.NotFound),
                    _resourceService.GetString("Assessment"));

            var resultCheckAssessmentTime = await CheckAssessmentTime(tenantId);
            if (resultCheckAssessmentTime.Code <= 0)
                return resultCheckAssessmentTime;

            int result = 0;
            var userAssessmentCriteriaInfo = await _userAssessmentCriteriaRepository.GetInfo(userAssessmentCriteria.Id);
            if (userAssessmentCriteriaInfo == null)
                return new ActionResultResponse(-2,
                    _sharedResourceService.GetString(ErrorMessage.NotFound,
                        _resourceService.GetString("Assessment criteria")));

            switch (type)
            {
                case AssessmentType.User:
                    // Kiểm tra bản đánh giá có phải của người dùng hay không.
                    if (userAssessmentInfo.UserId != currentUserId)
                        return new ActionResultResponse(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

                    if (userAssessmentCriteria.UserPoint > userAssessmentCriteriaInfo.Point)
                        return new ActionResultResponse(-3,
                            _resourceService.GetString("Point can not greater than standard point."));

                    var oldUserPoint = userAssessmentCriteriaInfo.UserPoint;
                    userAssessmentCriteriaInfo.UserPoint = userAssessmentCriteria.UserPoint;
                    userAssessmentCriteriaInfo.Note = userAssessmentCriteria.Note;
                    result = await _userAssessmentCriteriaRepository.UserUpdatePoint(userAssessmentCriteriaInfo);
                    if (result > 0 && oldUserPoint != userAssessmentCriteriaInfo.UserPoint)
                    {
                        // Cập nhật lại tổng điểm của người dùng tự chấm.
                        await UpdateAssessmentTotalPoint(AssessmentType.User, userAssessmentInfo.TenantId, userAssessmentInfo.Id);
                    }
                    break;
                case AssessmentType.Manager:
                    if (!userAssessmentCriteria.ManagerPoint.HasValue)
                        return new ActionResultResponse(-3,
                            _sharedResourceService.GetString(ValidatorMessage.PleaseEnter,
                                _resourceService.GetString("manager point")));

                    // Kiểm tra bản đánh giá có phải của người dùng hay không.
                    if (userAssessmentInfo.ManagerUserId != currentUserId)
                        return new ActionResultResponse(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

                    if (userAssessmentCriteria.ManagerPoint > userAssessmentCriteriaInfo.Point)
                        return new ActionResultResponse(-4,
                            _resourceService.GetString("Point can not greater than standard point."));

                    var oldManagerPoint = userAssessmentCriteriaInfo.ManagerPoint;
                    userAssessmentCriteriaInfo.ManagerPoint = userAssessmentCriteria.ManagerPoint;
                    userAssessmentCriteriaInfo.Note = userAssessmentCriteria.Note;
                    result = await _userAssessmentCriteriaRepository.UserUpdatePoint(userAssessmentCriteriaInfo);
                    if (result > 0 && oldManagerPoint != userAssessmentCriteriaInfo.ManagerPoint)
                    {
                        // Cập nhật lại tổng điểm của người dùng tự chấm.
                        await UpdateAssessmentTotalPoint(AssessmentType.Manager, userAssessmentInfo.TenantId, userAssessmentInfo.Id);
                    }
                    break;
                case AssessmentType.Approver:
                    if (!userAssessmentCriteria.ApproverPoint.HasValue)
                        return new ActionResultResponse(-5,
                            _sharedResourceService.GetString(ValidatorMessage.PleaseEnter,
                                _resourceService.GetString("approver point")));

                    // Kiểm tra bản đánh giá có phải của người dùng hay không.
                    if (userAssessmentInfo.ApproverUserId != currentUserId)
                        return new ActionResultResponse(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

                    if (userAssessmentCriteria.ApproverPoint > userAssessmentCriteriaInfo.Point)
                        return new ActionResultResponse(-6,
                            _resourceService.GetString("Point can not greater than standard point."));

                    var oldApproverPoint = userAssessmentCriteriaInfo.ApproverPoint;
                    userAssessmentCriteriaInfo.ApproverPoint = userAssessmentCriteria.ApproverPoint;
                    userAssessmentCriteriaInfo.Note = userAssessmentCriteria.Note;
                    result = await _userAssessmentCriteriaRepository.UserUpdatePoint(userAssessmentCriteriaInfo);
                    if (result > 0 && oldApproverPoint != userAssessmentCriteriaInfo.ApproverPoint)
                    {
                        // Cập nhật lại tổng điểm của người dùng tự chấm.
                        await UpdateAssessmentTotalPoint(AssessmentType.Approver, userAssessmentInfo.TenantId, userAssessmentInfo.Id);
                    }
                    break;
            }

            return new ActionResultResponse(result, result < 0
                                                    ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                    : _resourceService.GetString(SuccessMessage.UpdateSuccessful, _resourceService.GetString("assessment criteria")));
        }

        private async Task<int> UpdateAssessmentTotalPoint(AssessmentType assessmentType, string tenantId, string userAssessmentId)
        {
            var userAssessment = await _userAssessmentRepository.GetInfo(tenantId, userAssessmentId);
            if (userAssessment == null)
                return -1;

            int result = 0;
            switch (assessmentType)
            {
                case AssessmentType.User:
                    // Lấy về tổng số điểm người dùng.
                    var totalUserPoint = await _userAssessmentCriteriaRepository.GetTotalUserPoint(userAssessmentId);
                    userAssessment.TotalUserPoint = totalUserPoint;
                    result = await _userAssessmentRepository.UpdateTotalUserPoint(userAssessment);
                    break;
                case AssessmentType.Manager:
                    // Lấy về tổng số điểm của QLTT.
                    var totalManagerPoint = await _userAssessmentCriteriaRepository.GetTotalManagerPoint(userAssessmentId);
                    userAssessment.TotalManagerPoint = totalManagerPoint;
                    result = await _userAssessmentRepository.UpdateTotalManagerPoint(userAssessment);
                    break;
                case AssessmentType.Approver:
                    // Lấy về tổng số điểm QLPD.
                    var totalApproverPoint = await _userAssessmentCriteriaRepository.GetTotalApproverPoint(userAssessmentId);
                    userAssessment.TotalApproverPoint = totalApproverPoint;
                    result = await _userAssessmentRepository.UpdateTotalApproverPoint(userAssessment);
                    break;
            }

            return result;
        }

        [Route("{id}"), AcceptVerbs("POST")]
        public async Task<ActionResultResponse> UpdateStatus(string tenantId, string currentUserId, string currentUserFullName, string currentUserAvatar,
            string id, UserAssessmentMeta userAssessmentMeta)
        {
            var userAssessment = await _userAssessmentRepository.GetInfo(tenantId, id);
            if (userAssessment == null)
                return new ActionResultResponse(-1, _sharedResourceService.GetString(ErrorMessage.NotFound,
                    _resourceService.GetString("Assessment")));

            var validateAssessmentTime = await CheckAssessmentTime(tenantId);
            if (validateAssessmentTime.Code <= 0)
                return validateAssessmentTime;

            int result = 0;
            var notificationHelper = new NotificationHelper();
            switch (userAssessmentMeta.Status)
            {
                case AssessmentStatus.Cancel:
                    break;
                case AssessmentStatus.WaitingManagerApprove:
                    if (userAssessment.Status != AssessmentStatus.New && userAssessment.Status != AssessmentStatus.ManagerDecline)
                        return new ActionResultResponse(-1,
                            _resourceService.GetString(
                                "You can not update this status. Please contact with administrator."));

                    userAssessment.Status = AssessmentStatus.WaitingManagerApprove;
                    result = await _userAssessmentRepository.UpdateStatus(userAssessment);
                    if (result > 0)
                    {
                        // Cập nhật điểm của QLTT = điểm của người dùng tự chấm.
                        await SetDefaultManagerPoint(userAssessment.TenantId, userAssessment.Id);
                        UserSendNotification();
                    }
                    break;
                case AssessmentStatus.ManagerApproveWaitingApproverApprove:
                    if (userAssessment.Status != AssessmentStatus.WaitingManagerApprove && userAssessment.Status != AssessmentStatus.ApproverDecline)
                        return new ActionResultResponse(-2,
                            _resourceService.GetString(
                                "You can not approve this assessment. Please contact with administrator."));

                    userAssessment.Status = !string.IsNullOrEmpty(userAssessment.ApproverUserId)
                        ? AssessmentStatus.ManagerApproveWaitingApproverApprove
                        : userAssessment.Status = AssessmentStatus.ManagerApproved;
                    userAssessment.ManagerApproveTime = DateTime.Now;
                    result = await _userAssessmentRepository.UpdateStatus(userAssessment);
                    if (result > 0)
                    {
                        await SetDefaultApproverPoint(userAssessment.TenantId, userAssessment.Id);
                        ManagerApproveNotification();
                    }
                    break;
                case AssessmentStatus.ManagerDecline:
                    if (userAssessment.Status != AssessmentStatus.WaitingManagerApprove && userAssessment.Status != AssessmentStatus.ApproverDecline)
                        return new ActionResultResponse(-3,
                            _resourceService.GetString(
                                "You can not decline this assessment. Please contact with administrator."));

                    userAssessment.Status = AssessmentStatus.ManagerDecline;
                    userAssessment.ManagerDeclineReason = userAssessmentMeta.Reason;
                    result = await _userAssessmentRepository.UpdateStatus(userAssessment);
                    if (result > 0)
                    {
                        ManagerDeclineNotification();
                    }
                    break;
                case AssessmentStatus.ApproverApprove:
                    if (userAssessment.Status != AssessmentStatus.ManagerApproveWaitingApproverApprove && userAssessment.Status != AssessmentStatus.ApproverDecline)
                        return new ActionResultResponse(-4,
                            _resourceService.GetString(
                                "You can not approve this assessment. Please contact with administrator."));

                    userAssessment.Status = AssessmentStatus.ApproverApprove;
                    userAssessment.ApproverApproveTime = DateTime.Now;
                    result = await _userAssessmentRepository.UpdateStatus(userAssessment);
                    if (result > 0)
                    {
                        ApproverApproveNotification();
                    }
                    break;
                case AssessmentStatus.ApproverDecline:
                    if (userAssessment.Status != AssessmentStatus.ManagerApproveWaitingApproverApprove && userAssessment.Status != AssessmentStatus.ApproverApprove)
                        return new ActionResultResponse(-5,
                            _resourceService.GetString(
                                "You can not decline this assessment. Please contact with administrator."));

                    userAssessment.Status = AssessmentStatus.ApproverDecline;
                    userAssessment.ApproverDeclineReason = userAssessmentMeta.Reason;
                    result = await _userAssessmentRepository.UpdateStatus(userAssessment);
                    if (result > 0)
                    {
                        ApproverDeclineNotification();
                    }
                    break;
            }

            return new ActionResultResponse(result, result <= 0
                ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _sharedResourceService.GetString("Change assessment status successful."));

            #region Local function

            void UserSendNotification()
            {
                var notification = new Events.Notifications
                {
                    TenantId = tenantId,
                    Title = $"<b>{currentUserFullName}</b> {{send an assessment to you for approve.}}",
                    Content = "",
                    SenderId = currentUserId,
                    SenderFullName = currentUserFullName,
                    SenderAvatar = currentUserAvatar,
                    Url = $"/assessment/register?id={userAssessment.Id}&type={AssessmentType.Manager}",
                    ReceiverId = userAssessment.ManagerUserId,
                    Type = (int)NotificationType.Info,
                    IsSystem = false
                };
                notificationHelper.Send(notification);
            }

            void ManagerDeclineNotification()
            {
                var notification = new Events.Notifications
                {
                    TenantId = tenantId,
                    Title = $"<b>{currentUserFullName}</b> {{decline your assessment.}}",
                    Content = "",
                    SenderId = currentUserId,
                    SenderFullName = currentUserFullName,
                    SenderAvatar = currentUserAvatar,
                    Url = $"/assessment/register?id={userAssessment.Id}&type={AssessmentType.User}",
                    ReceiverId = userAssessment.UserId,
                    Type = (int)NotificationType.Info,
                    IsSystem = false
                };
                notificationHelper.Send(notification);
            }

            void ManagerApproveNotification()
            {
                // Gửi thông báo lên QLPD
                if (!string.IsNullOrEmpty(userAssessment.ApproverUserId))
                {
                    var notificationToApprover = new Events.Notifications
                    {
                        TenantId = tenantId,
                        Title = $"<b>{currentUserFullName}</b> {{Approve assessment waiting your review.}}",
                        SenderId = currentUserId,
                        SenderFullName = currentUserFullName,
                        SenderAvatar = currentUserAvatar,
                        Url = $"/assessment/register?id={userAssessment.Id}&type={AssessmentType.Approver}",
                        ReceiverId = userAssessment.ApproverUserId,
                        Type = (int)NotificationType.Info,
                        IsSystem = false
                    };
                    notificationHelper.Send(notificationToApprover);
                }

                // Gửi thông báo về cho người dùng.
                var notificationToUser = new Events.Notifications
                {
                    TenantId = tenantId,
                    Title = $"<b>{currentUserFullName}</b> {{approver your assessment and waiting for approver review.}}",
                    SenderId = currentUserId,
                    SenderFullName = currentUserFullName,
                    SenderAvatar = currentUserAvatar,
                    Url = $"/assessment/register?id={userAssessment.Id}&type={AssessmentType.User}",
                    ReceiverId = userAssessment.UserId,
                    Type = (int)NotificationType.Info,
                    IsSystem = false
                };
                // Gửi thông báo về cho người dùng.
                notificationHelper.Send(notificationToUser);
            }

            void ApproverDeclineNotification()
            {
                // Gửi thông báo cho QLTT
                var notification = new Events.Notifications
                {
                    TenantId = tenantId,
                    Title = $"<b>{currentUserFullName}</b> {{decline assessment.}}",
                    SenderId = currentUserId,
                    SenderFullName = currentUserFullName,
                    SenderAvatar = currentUserAvatar,
                    Url = $"/assessment/register?id={userAssessment.Id}&type={AssessmentType.Manager}",
                    ReceiverId = userAssessment.ManagerUserId,
                    Type = (int)NotificationType.Info,
                    IsSystem = false
                };
                notificationHelper.Send(notification);
            }

            void ApproverApproveNotification()
            {
                // Gửi thông báo cho QLTT
                var notificationToManager = new Events.Notifications
                {
                    TenantId = tenantId,
                    Title = $"<b>{currentUserFullName}</b> {{approve your staff assessment .}}",
                    SenderId = currentUserId,
                    SenderFullName = currentUserFullName,
                    SenderAvatar = currentUserAvatar,
                    Url = $"/assessment/register?id={userAssessment.Id}&type={AssessmentType.Manager}",
                    ReceiverId = userAssessment.ManagerUserId,
                    Type = (int)NotificationType.Info,
                    IsSystem = false
                };

                // Gửi thông báo cho QLTT
                var notificationToUser = new Events.Notifications
                {
                    TenantId = tenantId,
                    Title = $"<b>{currentUserFullName}</b> {{approve your assessment.}}",
                    SenderId = currentUserId,
                    SenderFullName = currentUserFullName,
                    SenderAvatar = currentUserAvatar,
                    Url = $"/assessment/register?id={userAssessment.Id}&type={AssessmentType.User}",
                    ReceiverId = userAssessment.UserId,
                    Type = (int)NotificationType.Info,
                    IsSystem = false
                };
                notificationHelper.Send(notificationToManager);
                notificationHelper.Send(notificationToUser);
            }
            #endregion
        }

        public async Task<SearchResult<UserAssessmentViewModel>> SearchResult(string tenantId, string languageId, string userId,
            string keyword, AssessmentStatus? status, int? officeId, byte? month, int year, int page, int pageSize)
        {
            var result = await _userAssessmentRepository.SearchResult(tenantId, languageId, userId,
                keyword, status, officeId, month, year, page, pageSize, out int totalRows);
            return new SearchResult<UserAssessmentViewModel>(result, totalRows);
        }

        public async Task<ActionResultResponse> UpdateUserAssessmentManager(string tenantId, string userId, string managerId, bool isManager)
        {
            var userAssessment = await _userAssessmentRepository.GetUnFinishedAssessment(tenantId, userId);
            if (userAssessment == null)
                return new ActionResultResponse(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound,
                        _resourceService.GetString("user assessment")));

            if (isManager)
                userAssessment.ManagerUserId = managerId;
            else
                userAssessment.ApproverUserId = managerId;

            var result = await _userAssessmentRepository.UpdateManager(userAssessment);
            return new ActionResultResponse(result, result <= 0 ?
                _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                    : _sharedResourceService.GetString(SuccessMessage.UpdateSuccessful, _resourceService.GetString("user assessment")));
        }

        public async Task<ActionResultResponse> RequestReassessment(string tenantId, string positionId, byte month, int year, bool applyForAll)
        {
            // Kiểm tra thời gian đánh giá lại và thời gian hiện tại.
            var reassessmentTime = new DateTime(year, month, 1);
            var today = DateTime.Today;

            var diffDays = (today - reassessmentTime).TotalDays;
            if (diffDays > 89)
                return new ActionResultResponse(-1, _resourceService.GetString("You can not request reassessment more than 2 months"));

            // Lấy về danh sách yêu cầu đánh giá lại.
            await UpdateReassessment(1);

            // Lấy về danh sách đánh giá trong tháng.
            return new ActionResultResponse(1, _resourceService.GetString("Request reassessment successful."));

            async Task UpdateReassessment(int page)
            {
                int pageSize = 10;
                var listUserAssessment =
                    await _userAssessmentRepository.SearchForReassessment(tenantId, positionId, month, year, applyForAll, page, pageSize, out int totalRows);

                var totalPages = Math.Ceiling(totalRows / (float)pageSize);
                if (totalPages > page)
                {
                    for (int i = page; i < totalPages; i++)
                    {
                        await UpdateReassessment(i + 1);
                    }
                }

                foreach (var userAssessment in listUserAssessment)
                {
                    userAssessment.Status = AssessmentStatus.Cancel;
                    await _userAssessmentRepository.UpdateStatus(userAssessment);
                }
            }
        }

        private async Task SetDefaultApproverPoint(string tenantId, string userAssessmentId)
        {
            var userAssessmentCriterias =
                await _userAssessmentCriteriaRepository.GetListAssessmentCriteria(userAssessmentId);
            if (userAssessmentCriterias != null && userAssessmentCriterias.Any())
            {
                foreach (var userAssessmentCriteria in userAssessmentCriterias)
                {
                    if (userAssessmentCriteria.ApproverPoint.HasValue)
                        continue;

                    userAssessmentCriteria.ApproverPoint = userAssessmentCriteria.ManagerPoint;
                }

                await _userAssessmentCriteriaRepository.Updates(userAssessmentCriterias);
                await UpdateAssessmentTotalPoint(AssessmentType.Approver, tenantId, userAssessmentId);
            }
        }

        private async Task SetDefaultManagerPoint(string tenantId, string userAssessmentId)
        {
            var userAssessmentCriterias =
                await _userAssessmentCriteriaRepository.GetListAssessmentCriteria(userAssessmentId);
            if (userAssessmentCriterias != null && userAssessmentCriterias.Any())
            {
                foreach (var userAssessmentCriteria in userAssessmentCriterias)
                {
                    if (userAssessmentCriteria.ManagerPoint.HasValue)
                        continue;

                    userAssessmentCriteria.ManagerPoint = userAssessmentCriteria.UserPoint;
                }

                await _userAssessmentCriteriaRepository.Updates(userAssessmentCriterias);
                await UpdateAssessmentTotalPoint(AssessmentType.Manager, tenantId, userAssessmentId);
            }
        }

        private async Task<ActionResultResponse<string>> CheckAssessmentTime(string tenantId)
        {
            // Kiểm tra kỳ đánh giá có nằm trong thời gian cho phép không.
            var assessmentTimeConfigResult = await _assessmentConfigService.GetAssessmentTime(tenantId);
            if (assessmentTimeConfigResult.Code <= 0)
                return new ActionResultResponse<string>(-1, assessmentTimeConfigResult.Message);

            var now = DateTime.Now;
            AssessmentTimeConfig assessmentConfig = assessmentTimeConfigResult.Data;
            int toDateMonth = assessmentConfig.FromDay <= assessmentConfig.ToDay
                ? now.Month
                : now.Month < 12 ? now.Month + 1 : 1;
            int toDateYear = assessmentConfig.FromDay <= assessmentConfig.ToDay
                ? now.Year
                : now.Month == 12 ? now.Year + 1 : now.Year;
            if (!assessmentConfig.FromDay.HasValue || !assessmentConfig.ToDay.HasValue)
                return new ActionResultResponse<string>(-2, _resourceService.GetString("Please config assessment time."));

            var daysInMonth = DateTime.DaysInMonth(toDateYear, toDateMonth);
            var toDateDay = assessmentConfig.ToDay > daysInMonth ? daysInMonth : assessmentConfig.ToDay;

            var fromDate = new DateTime(now.Year, now.Month, (int)assessmentConfig.FromDay);
            var toDate = new DateTime(toDateYear, toDateMonth, (int)toDateDay, 23, 59, 59);

            if (DateTime.Compare(DateTime.Now, fromDate) < 0)
            {
                var totalMinutes = Math.Round((fromDate - DateTime.Now).TotalMinutes);
                var minuteRemain = Math.Round(totalMinutes % 60);
                var hours = Math.Round(totalMinutes / 60);
                var hourRemain = Math.Round(hours % 24);
                var dayRemains = Math.Round(hours / 24);

                return new ActionResultResponse<string>(-3,
                    _resourceService.GetString(
                        "The assessment has not opened yet. Please come back after {0} days {1} hours {2} minutes",
                        Math.Ceiling(dayRemains), Math.Ceiling(hourRemain), Math.Ceiling(minuteRemain)));
            }

            if (DateTime.Compare(DateTime.Now, toDate) > 0)
                return new ActionResultResponse<string>(-4,
                    _resourceService.GetString("Your assessment already ended."));

            return new ActionResultResponse<string>();
        }
    }
}
