﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using OfficeTreeViewModel = GHM.Hr.Domain.ViewModels.OfficeTreeViewModel;
using State = GHM.Hr.Domain.ViewModels.State;

namespace GHM.Hr.Infrastructure.Services
{
    public class OfficePositionService : IOfficePositionService
    {
        private readonly IOfficeRepository _officeRepository;
        private readonly IPositionRepository _positionRepository;
        private readonly IOfficePositionRepository _officePositionRepository;
        private readonly IUserPositionRepository _userPositionRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmHrResource> _hrResourceService;

        public OfficePositionService(IOfficePositionRepository officePositionRepository, IOfficeRepository officeRepository,
            IResourceService<GhmHrResource> hrResourceService, IResourceService<SharedResource> sharedResourceService,
            IPositionRepository positionRepository, IUserPositionRepository userPositionRepository)
        {
            _officePositionRepository = officePositionRepository;
            _officeRepository = officeRepository;
            _hrResourceService = hrResourceService;
            _sharedResourceService = sharedResourceService;
            _positionRepository = positionRepository;
            _userPositionRepository = userPositionRepository;
        }

        public async Task<ActionResultResponse> Insert(string tenantId, int officeId, string positionId)
        {
            var isOfficeExists = await _officeRepository.CheckOfficeExistsByTenantId(officeId, tenantId);
            if (!isOfficeExists)
                return new ActionResultResponse(-1, _hrResourceService.GetString("Office does not exists."));

            var positionInfo = await _positionRepository.GetInfo(positionId, true);
            if (positionInfo == null)
                return new ActionResultResponse(-2, _hrResourceService.GetString("Position does not exists."));

            var isExists = await _officePositionRepository.CheckExists(positionId, officeId);
            if (isExists)
                return new ActionResultResponse(-3, _hrResourceService.GetString("Position already added to this office."));

            var officePosition = new OfficePosition
            {
                OfficeId = officeId,
                PositionId = positionInfo.Id,
                TitleId = positionInfo.TitleId
            };
            var result = await _officePositionRepository.Insert(officePosition);
            return new ActionResultResponse(result, result <= 0
                ? _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                : _hrResourceService.GetString("Add postion to office successful."));
        }

        public async Task<ActionResultResponse> Inserts(string tenantId, int officeId, string[] positionIds)
        {
            if (positionIds == null || positionIds.Length == 0)
                return new ActionResultResponse(-1, _hrResourceService.GetString("Please select at least one position."));

            var isOfficeExists = await _officeRepository.CheckOfficeExistsByTenantId(officeId, tenantId);
            if (!isOfficeExists)
                return new ActionResultResponse(-2, _hrResourceService.GetString("Office does not exists."));

            var officePositions = new List<OfficePosition>();
            foreach (var id in positionIds)
            {
                var isExists = await _officePositionRepository.CheckExists(id, officeId);
                var positionInfo = await _positionRepository.GetInfo(id, true);
                if (!isExists && positionInfo != null)
                    officePositions.Add(new OfficePosition
                    {
                        OfficeId = officeId,
                        PositionId = positionInfo.Id,
                        TitleId = positionInfo.TitleId
                    });
            }

            var result = await _officePositionRepository.Inserts(officePositions);
            return new ActionResultResponse(result, result < 0
                ? _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                : _hrResourceService.GetString("Add position into office successful."));
        }

        public async Task<ActionResultResponse> Delete(string tenantId, int officeId, string positionId)
        {
            var isOfficeExists = await _officeRepository.CheckOfficeExistsByTenantId(officeId, tenantId);
            if (!isOfficeExists)
                return new ActionResultResponse(-1, _hrResourceService.GetString("Office does not exists."));

            var isUsedByUser = await _userPositionRepository.CheckExistsByOfficeIdAndPositionId(officeId, positionId);
            if (isUsedByUser)
                return new ActionResultResponse(-2, _hrResourceService.GetString("This position already used by user. You can not delete this position."));

            var result = await _officePositionRepository.Delete(positionId, officeId);
            if (result <= 0)
                return new ActionResultResponse(result, result == -1
                    ? _hrResourceService.GetString("Office does not exists.")
                    : _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            return new ActionResultResponse(result, _hrResourceService.GetString("Remove position out of office succesful."));
        }

        public async Task<SearchResult<OfficePositionSearchViewModel>> Search(string tenantId, string languageId,
            string keyword, int officeId, int page, int pageSize)
        {
            var items = await _officePositionRepository.SearchByOfficeId(tenantId, languageId, keyword, officeId, page, pageSize, out var totalRows);
            return new SearchResult<OfficePositionSearchViewModel>
            {
                Items = items,
                TotalRows = totalRows,
            };
        }

        public async Task<ActionResultResponse<List<OfficeTreeViewModel>>> GetTree(string tenantId, string languageId)
        {
            var officePositions = await _officePositionRepository.GetAllOfficePositionForRenderTree(tenantId, languageId);
            if (officePositions == null || !officePositions.Any())
                return new ActionResultResponse<List<OfficeTreeViewModel>>(-1, _sharedResourceService.GetString(ErrorMessage.NotFound),
                    _hrResourceService.GetString("office position tree"));

            var result = RenderOfficePositionTree(officePositions, null);
            return new ActionResultResponse<List<OfficeTreeViewModel>>
            {
                Data = result
            };
        }

        private List<OfficeTreeViewModel> RenderOfficePositionTree(List<OfficePositionTreeViewModel> listOfficePositions, int? parentId)
        {
            var offices = listOfficePositions.Where(x => x.ParentOfficeId == parentId).ToList();
            var tree = new List<OfficeTreeViewModel>();
            if (offices.Any())
            {
                var officeGroup = offices.GroupBy(x => new { x.OfficeId, x.OfficeName });
                foreach (var groupItem in officeGroup)
                {
                    var officeNode = new OfficeTreeViewModel
                    {
                        Id = groupItem.Key.OfficeId.ToString(),
                        Text = groupItem.Key.OfficeName,
                        Children = new List<OfficeTreeViewModel>(),
                        Icon = "fa fa-building-o",
                        Data = new { IsOffice = true },
                        ParentId = null,
                        State = new State
                        {
                            Opened = true
                        }
                    };

                    foreach (var position in groupItem)
                    {
                        officeNode.Children.Add(new OfficeTreeViewModel
                        {
                            Id = position.PositionId,
                            Text = position.PositionName,
                            Icon = "fa fa-graduation-cap",
                            Data = new { IsOffice = false },
                            ParentId = position.OfficeId,
                            Children = new List<OfficeTreeViewModel>(),
                            State = new State
                            {
                                Opened = true
                            }
                        });
                    }

                    var listChildOffice = RenderOfficePositionTree(listOfficePositions, groupItem.Key.OfficeId);
                    listChildOffice.ForEach(x => officeNode.Children.Add(x));
                    tree.Add(officeNode);
                }
            }

            return tree;
        }
    }
}
