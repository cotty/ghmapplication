﻿using System;
using System.Threading.Tasks;
using GHM.Events;
using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Infrastructure.Services
{
    public class UserAssessmentCriteriaCommentService : IUserAssessmentCriteriaCommentService
    {
        private readonly IUserAssessmentCriteriaCommentRepository _userAssessmentCriteriaCommentRepository;
        private readonly IUserAssessmentRepository _userAssessmentRepository;
        private readonly IUserAssessmentCriteriaRepository _userAssessmentCriteriaRepository;

        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmHrResource> _resourceService;


        public UserAssessmentCriteriaCommentService(IUserAssessmentCriteriaCommentRepository userAssessmentCriteriaCommentRepository,
            IUserAssessmentRepository userAssessmentRepository, IUserAssessmentCriteriaRepository userAssessmentCriteriaRepository,
            IResourceService<SharedResource> sharedResourceService, IResourceService<GhmHrResource> resourceService)
        {
            _userAssessmentCriteriaCommentRepository = userAssessmentCriteriaCommentRepository;
            _userAssessmentRepository = userAssessmentRepository;
            _userAssessmentCriteriaRepository = userAssessmentCriteriaRepository;
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
        }

        public async Task<ActionResultResponse<string>> Insert(string tenantId, UserAssessmentCriteriaCommentMeta userAssessmentCriteriaCommentMeta)
        {
            // Lấy về thông tin tiêu chí.
            var userAssessmentCriteria =
                await _userAssessmentCriteriaRepository.GetInfo(userAssessmentCriteriaCommentMeta.UserAssessmentCriteriaId,
                    true);
            if (userAssessmentCriteria == null)
                return new ActionResultResponse<string>(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound,
                        _resourceService.GetString("User assessment criteria")));

            // Kiểm tra người dùng hiện tại có quyền comment trên đánh giá hay không.
            var userAssessment =
                await _userAssessmentRepository.GetInfo(tenantId, userAssessmentCriteria.UserAssessmentId, true);
            if (userAssessment == null)
                return new ActionResultResponse<string>(-2,
                    _sharedResourceService.GetString(ErrorMessage.NotFound,
                        _resourceService.GetString("User assessment")));

            if (userAssessment.UserId != userAssessmentCriteriaCommentMeta.UserId
                && userAssessment.ManagerUserId != userAssessmentCriteriaCommentMeta.UserId
                && userAssessment.ApproverUserId != userAssessmentCriteriaCommentMeta.UserId)
                return new ActionResultResponse<string>(-403,
                    _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var userAssessmentCriteriaComment = new UserAssessmentCriteriaComment
            {
                UserId = userAssessmentCriteriaCommentMeta.UserId,
                FullName = userAssessmentCriteriaCommentMeta.FullName,
                Avatar = userAssessmentCriteriaCommentMeta.Avatar,
                Content = userAssessmentCriteriaCommentMeta.Content,
                UserAssessmentCriteriaId = userAssessmentCriteriaCommentMeta.UserAssessmentCriteriaId
            };
            var result = await _userAssessmentCriteriaCommentRepository.Insert(userAssessmentCriteriaComment);
            if (result <= 0)
                return new ActionResultResponse<string>(result, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await SendNotification();

            return new ActionResultResponse<string>(result, _sharedResourceService.GetString(SuccessMessage.AddSuccessful,
                    _resourceService.GetString("Comment")), string.Empty, userAssessmentCriteriaComment.Id);

            async Task SendNotification()
            {
                if (userAssessment.Status != AssessmentStatus.Cancel && userAssessment.Status != AssessmentStatus.New)
                {
                    var notificationHelper = new NotificationHelper();
                    if (userAssessmentCriteriaCommentMeta.UserId == userAssessment.UserId)
                    {
                        // Gửi thông báo đến QLTT và QLPD.
                        var notificationToManager = new Events.Notifications
                        {
                            TenantId = tenantId,
                            Title = $"<b>{userAssessmentCriteriaCommentMeta.FullName}</b> {{just comment on an assessment that related to you.}}",
                            Content = "",
                            SenderId = userAssessmentCriteriaCommentMeta.UserId,
                            SenderFullName = userAssessmentCriteriaCommentMeta.FullName,
                            SenderAvatar = userAssessmentCriteriaCommentMeta.Avatar,
                            Url = $"/assessment/register?id={userAssessment.Id}&type={AssessmentType.Manager}&showComment=true&userAssessmentCriteriaId={userAssessmentCriteriaCommentMeta.UserAssessmentCriteriaId}",
                            ReceiverId = userAssessment.ManagerUserId,
                            Type = (int)NotificationType.Info,
                            IsSystem = false
                        };

                        if (!string.IsNullOrEmpty(userAssessment.ApproverUserId))
                        {
                            var notificationToApprover = new Events.Notifications
                            {
                                TenantId = tenantId,
                                Title = $"<b>{userAssessmentCriteriaCommentMeta.FullName}</b> {{just comment on an assessment that related to you.}}",
                                Content = "",
                                SenderId = userAssessmentCriteriaCommentMeta.UserId,
                                SenderFullName = userAssessmentCriteriaCommentMeta.FullName,
                                SenderAvatar = userAssessmentCriteriaCommentMeta.Avatar,
                                Url = $"/assessment/register?id={userAssessment.Id}&type={AssessmentType.Approver}&showComment=true&userAssessmentCriteriaId={userAssessmentCriteriaCommentMeta.UserAssessmentCriteriaId}",
                                ReceiverId = userAssessment.ApproverUserId,
                                Type = (int)NotificationType.Info,
                                IsSystem = false
                            };
                            notificationHelper.Send(notificationToApprover);
                        }

                        notificationHelper.Send(notificationToManager);
                    }

                    if (userAssessmentCriteriaCommentMeta.UserId == userAssessment.ManagerUserId)
                    {
                        // Gửi thông báo đến Người đăng ký và QLPD.
                        // Gửi thông báo đến QLTT và QLPD.
                        var notificationToUser = new Events.Notifications
                        {
                            TenantId = tenantId,
                            Title = $"<b>{userAssessmentCriteriaCommentMeta.FullName}</b> {{just comment on your assessment.}}",
                            Content = "",
                            SenderId = userAssessmentCriteriaCommentMeta.UserId,
                            SenderFullName = userAssessmentCriteriaCommentMeta.FullName,
                            SenderAvatar = userAssessmentCriteriaCommentMeta.Avatar,
                            Url = $"/assessment/register?id={userAssessment.Id}&type={AssessmentType.User}&showComment=true&userAssessmentCriteriaId={userAssessmentCriteriaCommentMeta.UserAssessmentCriteriaId}",
                            ReceiverId = userAssessment.UserId,
                            Type = (int)NotificationType.Info,
                            IsSystem = false
                        };

                        if (!string.IsNullOrEmpty(userAssessment.ApproverUserId))
                        {
                            var notificationToApprover = new Events.Notifications
                            {
                                TenantId = tenantId,
                                Title = $"<b>{userAssessmentCriteriaCommentMeta.FullName}</b> {{just comment on an assessment that related to you.}}",
                                Content = "",
                                SenderId = userAssessmentCriteriaCommentMeta.UserId,
                                SenderFullName = userAssessmentCriteriaCommentMeta.FullName,
                                SenderAvatar = userAssessmentCriteriaCommentMeta.Avatar,
                                Url = $"/assessment/register?id={userAssessment.Id}&type={AssessmentType.Approver}&showComment=true&userAssessmentCriteriaId={userAssessmentCriteriaCommentMeta.UserAssessmentCriteriaId}",
                                ReceiverId = userAssessment.ApproverUserId,
                                Type = (int)NotificationType.Info,
                                IsSystem = false
                            };
                            notificationHelper.Send(notificationToApprover);
                        }

                        notificationHelper.Send(notificationToUser);
                    }

                    if (userAssessmentCriteriaCommentMeta.UserId == userAssessment.ApproverUserId)
                    {
                        // Gửi thông báo đến Người đăng ký và QLTT.
                        var notificationToUser = new Events.Notifications
                        {
                            TenantId = tenantId,
                            Title = $"<b>{userAssessmentCriteriaCommentMeta.FullName}</b> {{just comment on your assessment.}}",
                            Content = "",
                            SenderId = userAssessmentCriteriaCommentMeta.UserId,
                            SenderFullName = userAssessmentCriteriaCommentMeta.FullName,
                            SenderAvatar = userAssessmentCriteriaCommentMeta.Avatar,
                            Url = $"/assessment/register?id={userAssessment.Id}&type={AssessmentType.User}&showComment=true&userAssessmentCriteriaId={userAssessmentCriteriaCommentMeta.UserAssessmentCriteriaId}",
                            ReceiverId = userAssessment.UserId,
                            Type = (int)NotificationType.Info,
                            IsSystem = false
                        };

                        var notificationToManager = new Events.Notifications
                        {
                            TenantId = tenantId,
                            Title = $"<b>{userAssessmentCriteriaCommentMeta.FullName}</b> {{just comment on an assessment that related to you.}}",
                            Content = "",
                            SenderId = userAssessmentCriteriaCommentMeta.UserId,
                            SenderFullName = userAssessmentCriteriaCommentMeta.FullName,
                            SenderAvatar = userAssessmentCriteriaCommentMeta.Avatar,
                            Url = $"/assessment/register?id={userAssessment.Id}&type={AssessmentType.Manager}&showComment=true&userAssessmentCriteriaId={userAssessmentCriteriaCommentMeta.UserAssessmentCriteriaId}",
                            ReceiverId = userAssessment.ManagerUserId,
                            Type = (int)NotificationType.Info,
                            IsSystem = false
                        };

                        notificationHelper.Send(notificationToUser);
                        notificationHelper.Send(notificationToManager);
                    }
                }
            }
        }

        public async Task<ActionResultResponse<string>> Update(string id, UserAssessmentCriteriaCommentMeta userAssessmentCriteriaCommentMeta)
        {
            var userAsssessmentCriteriaCommentInfo =
                await _userAssessmentCriteriaCommentRepository.GetInfo(id);
            if (userAsssessmentCriteriaCommentInfo == null)
                return new ActionResultResponse<string>(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound,
                        _resourceService.GetString("User assessment criteria")));

            if (userAsssessmentCriteriaCommentInfo.UserId != userAssessmentCriteriaCommentMeta.UserId)
                return new ActionResultResponse<string>(-403,
                    _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (userAsssessmentCriteriaCommentInfo.ConcurrencyStamp != userAssessmentCriteriaCommentMeta.ConcurrencyStamp)
                return new ActionResultResponse<string>(-2,
                    _sharedResourceService.GetString(ErrorMessage.AlreadyUpdatedByAnother));

            userAsssessmentCriteriaCommentInfo.Content = userAssessmentCriteriaCommentMeta.Content;
            userAsssessmentCriteriaCommentInfo.LastUpdate = DateTime.Now;
            userAsssessmentCriteriaCommentInfo.ConcurrencyStamp = Guid.NewGuid().ToString();
            var result = await _userAssessmentCriteriaCommentRepository.Update(userAsssessmentCriteriaCommentInfo);
            return new ActionResultResponse<string>(result, result <= 0
                ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _sharedResourceService.GetString(SuccessMessage.UpdateSuccessful,
                    _resourceService.GetString("Comment")), string.Empty, userAsssessmentCriteriaCommentInfo.ConcurrencyStamp);
        }

        public async Task<ActionResultResponse> Delete(string userId, string id)
        {
            var userAssessmentCriteriaComment = await _userAssessmentCriteriaCommentRepository.GetInfo(id, true);
            if (userAssessmentCriteriaComment == null)
                return new ActionResultResponse(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound,
                        _resourceService.GetString("User assessment criteria")));

            if (userAssessmentCriteriaComment.UserId != userId)
                return new ActionResultResponse(-403,
                    _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var result = await _userAssessmentCriteriaCommentRepository.Delete(id);
            return new ActionResultResponse(result, result <= 0
                ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _sharedResourceService.GetString(SuccessMessage.DeleteSuccessful,
                    _resourceService.GetString("Comment")));
        }

        public async Task<SearchResult<UserAssessmentCriteriaComment>> Search(string tenantId, string userId,
            string userAssessmentCriteriaId, int page, int pageSize)
        {
            // Lấy về thông tin tiêu chí.
            var userAssessmentCriteria =
                await _userAssessmentCriteriaRepository.GetInfo(userAssessmentCriteriaId, true);
            if (userAssessmentCriteria == null)
                return new SearchResult<UserAssessmentCriteriaComment>(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound,
                        _resourceService.GetString("User assessment criteria")));

            // Kiểm tra người dùng hiện tại có quyền comment trên đánh giá hay không.
            var userAssessment =
                await _userAssessmentRepository.GetInfo(tenantId, userAssessmentCriteria.UserAssessmentId, true);
            if (userAssessment == null)
                return new SearchResult<UserAssessmentCriteriaComment>(-2,
                    _sharedResourceService.GetString(ErrorMessage.NotFound,
                        _resourceService.GetString("User assessment")));

            if (userAssessment.UserId != userId
                && userAssessment.ManagerUserId != userId
                && userAssessment.ApproverUserId != userId)
                return new SearchResult<UserAssessmentCriteriaComment>(-403,
                    _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var items = await _userAssessmentCriteriaCommentRepository.Search(userAssessmentCriteriaId, page, pageSize,
                out int totalRows);
            return new SearchResult<UserAssessmentCriteriaComment>(items, totalRows);
        }
    }
}
