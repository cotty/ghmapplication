﻿using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Hr.Infrastructure.Services
{
    public class RelatedUserService : IRelatedUserService
    {
        private readonly IRelatedUserRepository _relatedUserRepository;
        private readonly IResourceService<GhmHrResource> _resourceService;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IUserValueRepository _userValueRepository;
        public RelatedUserService(IRelatedUserRepository relatedUserRepository,
            IResourceService<GhmHrResource> resourceService, IResourceService<SharedResource> sharedResourceService, IUserValueRepository userValueRepository)
        {
            _relatedUserRepository = relatedUserRepository;
            _userValueRepository = userValueRepository;
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
        }
        public async Task<ActionResultResponse> Delete(string tenantId, string id)
        {
            var info = await _relatedUserRepository.GetInfo(tenantId, id);

            if (info == null)
                return new ActionResultResponse(-1, _resourceService.GetString("realted user does not exist"));

            info.IsDelete = true;

            return await _relatedUserRepository.SaveAsync(info) < 0 ? new ActionResultResponse(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)) :
                new ActionResultResponse(1, _resourceService.GetString("Insert related user successfully!"));
        }

        public async Task<ActionResultResponse<string>> Insert(string tenantId, string userId, string fullName, RelatedUserMeta relatedUserMeta)
        {
            var checkNameExist = await _relatedUserRepository.CheckNameExist(tenantId, userId, fullName.Trim());

            if (checkNameExist)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Name does exist"));

            var userValueExist = await _userValueRepository.CheckExist(tenantId, relatedUserMeta.ValueId);

            if (!userValueExist)
                return new ActionResultResponse<string>(-2, _resourceService.GetString("Value does not exist"));

            var relatedUser = new RelatedUser
            {
                BirthDay = relatedUserMeta.BirthDay,
                ContactAddress = relatedUserMeta.ContactAddress,
                DenominationName = relatedUserMeta.DenominationName,
                DistrictId = relatedUserMeta.DistrictId,
                Ethnic = relatedUserMeta.Ethnic,
                FullName = relatedUserMeta.FullName.Trim(),
                IdCardDateOfIssue = relatedUserMeta.IdCardDateOfIssue,
                IdCardNumber = relatedUserMeta.IdCardNumber,
                IdCardPlaceOfIssue = relatedUserMeta.IdCardPlaceOfIssue,
                Job = relatedUserMeta.Job,
                NationalId = relatedUserMeta.NationalId,
                PermanentAddress = relatedUserMeta.PermanentAddress,
                Phone = relatedUserMeta.Phone,
                ProvinceId = relatedUserMeta.ProvinceId,
                UserId = relatedUserMeta.UserId,
                ValueId = relatedUserMeta.ValueId,
                Gender = relatedUserMeta.Gender,
                CreatorId = userId,
                CreatorFullName = fullName,
                TenantId = tenantId,
                ValueName = relatedUserMeta.ValueName
            };

            return await _relatedUserRepository.Insert(relatedUser) < 0 ? new ActionResultResponse<string>(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong))
                : new ActionResultResponse<string>(1, _resourceService.GetString("Insert related user successfully!"), "", relatedUser.Id);
        }

        public async Task<SearchResult<RelatedUserViewModel>> Search(string tenantId, string keyword, string userId, string valueId, string fullName, string idCardNumber, string phone, int page, int pageSize)
        {
            var items = await _relatedUserRepository.Search(tenantId, keyword, userId, valueId, fullName, idCardNumber, phone, page, pageSize, out var totalRows);

            return new SearchResult<RelatedUserViewModel>
            {
                Items = items,
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse<string>> Update(string tenantId, string id, RelatedUserMeta relatedUserMeta)
        {
            var info = await _relatedUserRepository.GetInfo(tenantId, id);

            if (info == null)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("related user does not exist"));

            if (info.ConcurrencyStamp != relatedUserMeta.ConcurrencyStamp)
                return new ActionResultResponse<string>(-2, _resourceService.GetString("related user updated before !"));

            if(relatedUserMeta.FullName != info.FullName)
            {
                var nameExist = await _relatedUserRepository.CheckNameExist(tenantId, relatedUserMeta.UserId, relatedUserMeta.FullName.Trim());

                if (nameExist)
                    return new ActionResultResponse<string>(-3, _resourceService.GetString("related name does exist!"));
            }

            var valueExist = await _userValueRepository.CheckExist(tenantId, relatedUserMeta.ValueId);

            if (!valueExist)
                return new ActionResultResponse<string>(-4, _resourceService.GetString("user value does not exist"));

            info.BirthDay = relatedUserMeta.BirthDay;
            info.ContactAddress = relatedUserMeta.ContactAddress;
            info.DenominationName = relatedUserMeta.DenominationName;
            info.DistrictId = relatedUserMeta.DistrictId;
            info.Ethnic = relatedUserMeta.Ethnic;
            info.ValueName = relatedUserMeta.ValueName;
            info.FullName = relatedUserMeta.FullName.Trim();
            info.IdCardDateOfIssue = relatedUserMeta.IdCardDateOfIssue;
            info.IdCardNumber = relatedUserMeta.IdCardNumber;
            info.IdCardPlaceOfIssue = relatedUserMeta.IdCardPlaceOfIssue;
            info.Job = relatedUserMeta.Job;
            info.Gender = relatedUserMeta.Gender;
            info.NationalId = relatedUserMeta.NationalId;
            info.PermanentAddress = relatedUserMeta.PermanentAddress;
            info.Phone = relatedUserMeta.Phone;
            info.ProvinceId = relatedUserMeta.ProvinceId;
            info.ValueId = relatedUserMeta.ValueId;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();

            return await _relatedUserRepository.SaveAsync(info) < 0 ? new ActionResultResponse<string>(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong))
                : new ActionResultResponse<string>(1, _resourceService.GetString("Update related user successfully!"), "", info.ConcurrencyStamp);
        }
    }
}
