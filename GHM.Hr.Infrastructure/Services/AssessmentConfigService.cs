﻿using System;
using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;

namespace GHM.Hr.Infrastructure.Services
{
    public class AssessmentConfigService : IAssessmentConfigService
    {
        private readonly IConfigRepository _configRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmHrResource> _resourceService;

        public AssessmentConfigService(IConfigRepository configRepository, IResourceService<GhmHrResource> resourceService,
            IResourceService<SharedResource> sharedResourceService)
        {
            _configRepository = configRepository;
            _resourceService = resourceService;
            _sharedResourceService = sharedResourceService;
        }

        public async Task<ActionResultResponse> SaveAssessmentTime(string tenantId, int fromDay, int toDay)
        {
            var keyFromDay = ClassHelper.GetPropertyNameAsKey<AssessmentTimeConfig>("FromDay");
            var keyToDay = ClassHelper.GetPropertyNameAsKey<AssessmentTimeConfig>("ToDay");

            // Đánh giá từ ngày.
            var fromDayConfig = await _configRepository.GetInfo(tenantId, null, keyFromDay);
            if (fromDayConfig == null)
            {
                await _configRepository.Insert(new Config
                {
                    TenantId = tenantId,
                    Key = keyFromDay,
                    Value = fromDay.ToString()
                });
            }
            else
            {
                if (fromDayConfig.Value != fromDay.ToString())
                {
                    fromDayConfig.Value = fromDay.ToString();
                    await _configRepository.Update(fromDayConfig);
                }
            }

            // Đánh giá đến ngày.
            var toDayConfig = await _configRepository.GetInfo(tenantId, null, keyToDay);
            if (toDayConfig == null)
            {
                await _configRepository.Insert(new Config
                {
                    TenantId = tenantId,
                    Key = keyToDay,
                    Value = toDay.ToString()
                });
            }
            else
            {
                if (toDayConfig.Value != toDay.ToString())
                {
                    toDayConfig.Value = toDay.ToString();
                    await _configRepository.Update(fromDayConfig);
                }
            }

            return new ActionResultResponse(1,
                _sharedResourceService.GetString(SuccessMessage.UpdateSuccessful,
                    _resourceService.GetString("Assessment time config")));
        }

        public async Task<ActionResultResponse<AssessmentTimeConfig>> GetAssessmentTime(string tenantId)
        {
            var keyFromDay = ClassHelper.GetPropertyNameAsKey<AssessmentTimeConfig>("FromDay");
            var keyToDay = ClassHelper.GetPropertyNameAsKey<AssessmentTimeConfig>("ToDay");

            // Đánh giá từ ngày.
            var fromDayConfig = await _configRepository.GetInfo(tenantId, null, keyFromDay);
            var toDayConfig = await _configRepository.GetInfo(tenantId, null, keyToDay);
            return new ActionResultResponse<AssessmentTimeConfig>
            {
                Data = new AssessmentTimeConfig
                {
                    FromDay = fromDayConfig != null ? int.Parse(fromDayConfig.Value) : (int?)null,
                    ToDay = toDayConfig != null ? int.Parse(toDayConfig.Value) : (int?)null,
                }
            };
        }
    }
}
