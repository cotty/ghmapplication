﻿using System.Threading.Tasks;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;

namespace GHM.Hr.Infrastructure.Services
{
    public class CriteriaPositionService : ICriteriaPositionService
    {
        private readonly ICriteriaPositionRepository _criteriaPositionRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmHrResource> _resourceService;


        public CriteriaPositionService(IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmHrResource> resourceService, ICriteriaPositionRepository criteriaPositionRepository)
        {
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
            _criteriaPositionRepository = criteriaPositionRepository;
        }

        public async Task<ActionResultResponse> Insert(CriteriaPosition criteriaPosition)
        {
            var isExists =
                await _criteriaPositionRepository.CheckExists(criteriaPosition.TenantId, criteriaPosition.CriteriaId,
                    criteriaPosition.PositionId);
            if (isExists)
                return new ActionResultResponse(-1,
                    _sharedResourceService.GetString(ErrorMessage.AlreadyExists,
                        _resourceService.GetString("Criteria position")));
            
            var result = await _criteriaPositionRepository.Insert(criteriaPosition);
            return new ActionResultResponse(result, result <= 0
                                                    ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                    : _sharedResourceService.GetString(SuccessMessage.AddSuccessful, _resourceService.GetString("Criteria position")));
        }

        public async Task<ActionResultResponse> Delete(string tenantId, string criteriaId, string positionId)
        {
            var result = await _criteriaPositionRepository.Delete(tenantId, criteriaId, positionId);
            return new ActionResultResponse(result, result <= 0
                ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _sharedResourceService.GetString(SuccessMessage.DeleteSuccessful, _resourceService.GetString("Criteria position")));
        }

        public async Task<SearchResult<CriteriaPositionViewModel>> Search(string tenantId, string languageId, string positionId, bool? isActive)
        {
            if (string.IsNullOrEmpty(positionId))
                return new SearchResult<CriteriaPositionViewModel>(-1,
                    _sharedResourceService.GetString(ValidatorMessage.PleaseSelect,
                        _resourceService.GetString("Position")));

            return new SearchResult<CriteriaPositionViewModel>(
                await _criteriaPositionRepository.Search(tenantId, languageId, positionId, isActive));
        }

        public async Task<ActionResultResponse> UpdateActiveStatus(string tenantId, string criteriaId, string positionId, bool isActive)
        {
            var criteriaPosition = await _criteriaPositionRepository.GetInfo(tenantId, criteriaId, positionId);
            if (criteriaPosition == null)
                return new ActionResultResponse<string>(-1, _sharedResourceService.GetString(ErrorMessage.NotFound),
                    _resourceService.GetString("Criteria position"));

            criteriaPosition.IsActive = isActive;            
            var result = await _criteriaPositionRepository.UpdateActiveStatus(tenantId, criteriaId, positionId, isActive);
            return new ActionResultResponse(result, result <= 0
                ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _sharedResourceService.GetString(SuccessMessage.UpdateSuccessful,
                    _resourceService.GetString("Criteria position")));
        }
    }
}
