﻿using GHM.Hr.Domain.Constants;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using System;
using System.Threading.Tasks;

namespace GHM.Hr.Infrastructure.Services
{
    public class CommendationDisciplineCategoryService : IComendationDisciplineCategoryService
    {
        private readonly ICommendationDisciplineCategoryRepository _commendationDisciplineCategoryRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmHrResource> _resourceService;

        public CommendationDisciplineCategoryService(ICommendationDisciplineCategoryRepository commendationDisciplineRepository, IResourceService<SharedResource> sharedResource,
            IResourceService<GhmHrResource> resourceService)
        {
            _commendationDisciplineCategoryRepository = commendationDisciplineRepository;
            _sharedResourceService = sharedResource;
            _resourceService = resourceService;
        }
        public async Task<ActionResultResponse> Delete(string tenantId, string id)
        {
            var info = await _commendationDisciplineCategoryRepository.GetInfoAsync(tenantId, id, false);

            if (info == null)
                return new ActionResultResponse(-1, _resourceService.GetString("comendation discipline category does not exist"));

            info.IsDelete = true;

            return await _commendationDisciplineCategoryRepository.SaveAsync(info) < 0 ? new ActionResultResponse(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong))
                : new ActionResultResponse(1, _resourceService.GetString("Delete category successfully!"));
        }

        public async Task<ActionResultResponse<string>> InsertAsync(string tenantId, CommendationDisciplineCategoryMeta commendationDisciplineCatoryMeta)
        {
            var checkNameExist = await _commendationDisciplineCategoryRepository.CheckNameExist(tenantId, commendationDisciplineCatoryMeta.Name, commendationDisciplineCatoryMeta.Type);

            if (checkNameExist)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Commendation discipline category does exist"));

            var commendationDisciplineCategory = new UserCommendationDisciplineCategory
            {
                Name = commendationDisciplineCatoryMeta.Name,
                UnsignName = commendationDisciplineCatoryMeta.Name.StripVietnameseChars(),
                Type = commendationDisciplineCatoryMeta.Type,
                TenantId = tenantId
            };

            return await _commendationDisciplineCategoryRepository.InsertAsync(commendationDisciplineCategory) < 0 ? new ActionResultResponse<string>(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)) :
                new ActionResultResponse<string>(1, _resourceService.GetString("Insert commendation discipline category successfully !"), "", commendationDisciplineCategory.Id);
        }

        public async Task<SearchResult<CommendationDisciplineCategoryViewModel>> Search(string tenantId, string keyword, string name, CommendationDisciplineType type, int page, int pageSize)
        {
            var item = await _commendationDisciplineCategoryRepository.Search(tenantId, keyword, name, type, page, pageSize, out var totalRows);

            return new SearchResult<CommendationDisciplineCategoryViewModel>
            {
                Items = item,
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse<string>> UpdateAsync(string tenantId, string id, CommendationDisciplineCategoryMeta commendationDisciplineCategoryMeta)
        {
            var info = await _commendationDisciplineCategoryRepository.GetInfoAsync(tenantId, id, false);

            if (info == null)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Commendation discipline does not exist"));

            if(info.Name != commendationDisciplineCategoryMeta.Name)
            {
                var checkNameExist = await _commendationDisciplineCategoryRepository.CheckNameExist(tenantId, commendationDisciplineCategoryMeta.Name, commendationDisciplineCategoryMeta.Type);

                if (checkNameExist)
                    return new ActionResultResponse<string>(-2, _resourceService.GetString("Name does exist"));
            }

            info.Name = commendationDisciplineCategoryMeta.Name;
            info.UnsignName = commendationDisciplineCategoryMeta.Name.StripVietnameseChars();
            info.Type = info.Type;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();

            return await _commendationDisciplineCategoryRepository.SaveAsync(info) < 0 ? new ActionResultResponse<string>(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong))
                : new ActionResultResponse<string>(1, _resourceService.GetString("Update commendation discipline successfully!"));
        }
    }
}
