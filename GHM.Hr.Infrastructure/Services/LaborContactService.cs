﻿using DocumentFormat.OpenXml.Packaging;
using GHM.Hr.Domain.IRepository;
using GHM.Hr.Domain.IServices;
using GHM.Hr.Domain.ModelMetas;
using GHM.Hr.Domain.Models;
using GHM.Hr.Domain.Resources;
using GHM.Hr.Domain.ViewModels;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.Services;
using GHM.Infrastructure.ViewModels;
using Microsoft.Extensions.Configuration;
using Novacode;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using File = GHM.Hr.Domain.Models.File;

namespace GHM.Hr.Infrastructure.Services
{
    public class LaborContactService : ILaborContactService
    {
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmHrResource> _resourceService;
        private readonly ILaborContractRepository _laborContractRepository;
        private readonly ILaborContractFormRepository _laborContractFormRepository;
        private readonly IUserValueRepository _userValueRepository;
        private readonly IUserRepository _userRepository;
        private readonly IConfiguration _configuration;
        private readonly IHttpClientService _httpClient;

        public LaborContactService(ILaborContractFormRepository laborContractFormRepository, IUserValueRepository userValueRepository,
            ILaborContractRepository laborContractRepository, IResourceService<SharedResource> sharedResourceService,
            IConfiguration configuration, IUserRepository userRepository, IHttpClientService httpClient,
            IResourceService<GhmHrResource> resourceService)
        {
            _laborContractFormRepository = laborContractFormRepository;
            _laborContractRepository = laborContractRepository;
            _userValueRepository = userValueRepository;
            _sharedResourceService = sharedResourceService;
            _userRepository = userRepository;
            _resourceService = resourceService;
            _configuration = configuration;
            _httpClient = httpClient;
        }

        public async Task<ActionResultResponse> Delete(string tenantId, string id)
        {
            var result = await _laborContractRepository.Delete(tenantId, id);
            if (result == -1)
                return new ActionResultResponse(-1, _resourceService.GetString("Hợp đồng không tồn tại"));

            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _resourceService.GetString("Xóa hợp đồng thành công"));
        }

        public async Task<ActionResultResponse> DeleteLaborContractForm(string tenantId, string id)
        {
            var countLaborContractForm = await _laborContractRepository.CountLaborContractByForm(tenantId, id);
            if (countLaborContractForm > 0)
                return new ActionResultResponse(-2, _resourceService.GetString("Mẫu hợp đồng đã được sử dụng"));

            var result = await _laborContractFormRepository.Delete(tenantId, id);
            if (result == -1)
                return new ActionResultResponse(-1, _resourceService.GetString("Mẫu hợp đồng không tồn tại"));

            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _resourceService.GetString("Xóa mẫu hợp đồng thành công"));
        }

        public async Task<ActionResultResponse<UserLaborContractDetailViewModel>> GetLaborContractDetail(string tenantId, string id)
        {
            string userId = Guid.NewGuid().ToString();
            ApiUrlSettings apiUrls = _configuration.GetApiUrl();

            if (apiUrls == null)
                return new ActionResultResponse<UserLaborContractDetailViewModel>(-14, _sharedResourceService.GetString("Có gì đó không đúng. Vui lòng liên hệ với quản trị viên."));

            var info = await _laborContractRepository.GetInfo(tenantId, id, true);
            if (info == null)
                return new ActionResultResponse<UserLaborContractDetailViewModel>(-1, _resourceService.GetString("Hợp đồng không tồn tại"));

            var fileInfo = await _httpClient.GetAsync<ActionResultResponse<File>>($"{apiUrls.FileApiUrl}/files/{tenantId}/{info.FileId}");

            return new ActionResultResponse<UserLaborContractDetailViewModel>
            {
                Data = new UserLaborContractDetailViewModel
                {
                    Id = info.Id,
                    No = info.No,
                    UserId = info.UserId,
                    FullName = info.FullName,
                    Type = info.Type,
                    TypeName = info.TypeName,
                    FromDate = info.FromDate,
                    ToDate = info.ToDate,
                    CreateTime = info.CreateTime,
                    Note = info.Note,
                    FileId = info.FileId,
                    FileName = info.FileName,
                    IsUse = info.IsUse,
                    ConcurrencyStamp = info.ConcurrencyStamp,
                    LaborContractFormId = info.LaborContractFormId,
                    LaborContractFormName = info.LaborContractFormName,
                    RegisterDate = info.RegisterDate,
                    TaskDescription = info.TaskDescription,
                    Time = info.Time,
                    SalaryCalculationUnit = info.SalaryCalculationUnit,
                    BasicSalary = info.BasicSalary,
                    ProbationarySalary = info.ProbationarySalary,
                    OfficialSalary = info.OfficialSalary,
                    Status = info.Status,
                    FileExtension = fileInfo?.Data?.Extension
                },
                Code = 1
            };
        }

        public async Task<SearchResult<Suggestion<string>>> GetLaborContractFormByType(string tenantId, string keyword, string type, int page, int pageSize)
        {
            return new SearchResult<Suggestion<string>>
            {
                Items = await _laborContractFormRepository.GetLaborContractFormByType(tenantId, keyword, type, page, pageSize, out int totalRows),
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse<LaborContractFormDetailViewModel>> GetLaborContractFormDetail(string tenantId, string id)
        {
            string userId = Guid.NewGuid().ToString();
            ApiUrlSettings apiUrls = _configuration.GetApiUrl();

            if (apiUrls == null)
                return new ActionResultResponse<LaborContractFormDetailViewModel>(-14, _sharedResourceService.GetString("Có gì đó không đúng. Vui lòng liên hệ với quản trị viên."));

            var info = await _laborContractFormRepository.GetInfo(tenantId, id, true);
            if (info == null)
                return new ActionResultResponse<LaborContractFormDetailViewModel>(-1, "Mẫu hợp đồng không tồn tại");

            var laborContractTypeInfo = await _userValueRepository.GetInfo(tenantId, info.LaborContractTypeId, true);
            if (laborContractTypeInfo == null)
                return new ActionResultResponse<LaborContractFormDetailViewModel>(-2, "Loại hợp đồng không tồn tại");

            var fileInfo = await _httpClient.GetAsync<ActionResultResponse<File>>($"{apiUrls.FileApiUrl}/files/{tenantId}/{info.FileId}");

            // Đọc file Docx
            //try
            //{
            //    var filePath = $"http://localhost:5010/{fileInfo.Data.Url}";
            //    Stream streamObject = Common.GetStreamFromUrl(filePath);
            //    DocX doc = DocX.Load(streamObject);

            //    int TotalLists = doc.Lists.Count;
            //    using (WordprocessingDocument doc1 = WordprocessingDocument.Open(streamObject, true))
            //    {
            //        HtmlConverterSettings convSettings = new HtmlConverterSettings();
            //        XElement html = HtmlConverter.ConvertToHtml(doc1, convSettings);
            //        var html1 = new XDocument(new XDocumentType("html", null, null, null),
            //                                                                html);
            //        var htmlString = html1.ToString(SaveOptions.DisableFormatting);
            //        var test = html.ToString();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    string ErrorMessage = ex.Message;
            //}

            return new ActionResultResponse<LaborContractFormDetailViewModel>
            {
                Data = new LaborContractFormDetailViewModel
                {
                    Id = info.Id,
                    LaborContractTypeId = info.LaborContractTypeId,
                    LaborContractTypeName = laborContractTypeInfo.Name,
                    FileId = info.FileId,
                    FileName = info.FileName,
                    Name = info.Name,
                    IsActive = info.IsActive,
                    Description = info.Description,
                    ConcurrencyStamp = info.ConcurrencyStamp,
                    FileExtension = fileInfo?.Data?.Extension
                }
            };
        }

        public async Task<SearchResult<UserLaborContractExportViewModel>> GetListLaborContractExport(string tenantId, string languageId, string officeIdPath)
        {
            return new SearchResult<UserLaborContractExportViewModel>
            {
                Items = await _laborContractRepository.GetListLaborContractExport(tenantId, languageId, officeIdPath),
            };
        }

        public async Task<ActionResultResponse<string>> Insert(string tenantId, string currenctUserId, string languageId,
            LaborContractMeta laborContractMeta)
        {
            if (laborContractMeta.ToDate.HasValue && (DateTime.Compare(laborContractMeta.ToDate.Value, laborContractMeta.FromDate) < 0))
                return new ActionResultResponse<string>(-7, _resourceService.GetString("Ngày kết thúc không được nhỏ hơn ngày bắt đầu"));

            if (laborContractMeta.RegisterDate.HasValue && (DateTime.Compare(laborContractMeta.RegisterDate.Value, laborContractMeta.FromDate) < 0))
                return new ActionResultResponse<string>(-8, _resourceService.GetString("Ngày ký không được nhỏ hơn ngày bắt đầu"));

            var userInfo = await _userRepository.GetInfo(laborContractMeta.UserId);
            if (userInfo == null)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Nhân viên không tồn tại"));

            var isExistsNo = await _laborContractRepository.CheckNoExists(tenantId, "", laborContractMeta.No);
            if (isExistsNo)
                return new ActionResultResponse<string>(-8, _resourceService.GetString("Số hợp đông đã tồn tại"));

            var laborContractTypeInfo = await _userValueRepository.GetInfo(tenantId, laborContractMeta.Type, true);
            if (laborContractTypeInfo == null)
                return new ActionResultResponse<string>(-4, _resourceService.GetString("Loại hợp đồng không tồn tại"));

            var id = Guid.NewGuid().ToString();
            var laborContract = new UserLaborContract()
            {
                Id = id,
                ConcurrencyStamp = id,
                TenantId = tenantId,
                No = laborContractMeta.No?.Trim(),
                UserId = userInfo.Id,
                FullName = userInfo.FullName,
                OfficeId = userInfo.OfficeId,
                TitleId = userInfo.TitleId,
                PositionId = userInfo.PositionId,
                Type = laborContractMeta.Type,
                TypeName = laborContractTypeInfo.Name,
                FromDate = laborContractMeta.FromDate,
                ToDate = laborContractMeta.ToDate,
                CreatorId = currenctUserId,
                Note = laborContractMeta.Note?.Trim(),
                FileId = laborContractMeta.FileId,
                FileName = laborContractMeta.FileName,
                IsUse = laborContractMeta.IsUse,
                UnsignName = $"{userInfo.FullName}{laborContractMeta.No}".StripVietnameseChars().ToUpper(),
                RegisterDate = laborContractMeta.RegisterDate,
                TaskDescription = laborContractMeta.TaskDescription?.Trim(),
                Time = laborContractMeta.Time?.Trim(),
                SalaryCalculationUnit = laborContractMeta.SalaryCalculationUnit,
                BasicSalary = laborContractMeta.BasicSalary,
                ProbationarySalary = laborContractMeta.ProbationarySalary,
                OfficialSalary = laborContractMeta.OfficialSalary,
                Status = laborContractMeta.Status
            };

            if (!string.IsNullOrEmpty(laborContractMeta.LaborContractFormId))
            {
                var laborContractFormInfo = await _laborContractFormRepository.GetInfo(tenantId, laborContractMeta.LaborContractFormId, true);
                if (laborContractFormInfo == null)
                    return new ActionResultResponse<string>(-3, _resourceService.GetString("Mẫu hợp đồng không tồn tại"));

                if (laborContractFormInfo.LaborContractTypeId != laborContractMeta.Type)
                    return new ActionResultResponse<string>(-5, _resourceService.GetString($"Mẫu hợp đồng" +
                        $" {laborContractFormInfo.Name} không thuộc loại hợp đông {laborContractTypeInfo.Name}"));

                laborContract.LaborContractFormId = laborContractFormInfo.Id;
                laborContract.LaborContractFormName = laborContractFormInfo.Name;
            }

            var result = await _laborContractRepository.Insert(laborContract);
            if (result <= 0)
                return new ActionResultResponse<string>(-10, _resourceService.GetString("Có gì đó không đúng. Vui lòng liên hệ với quản trị viên"));

            // Update status user by userLaborContract
            userInfo.Status = laborContractMeta.Status;
            await _userRepository.Update(userInfo);

            if (laborContractMeta.IsUse)
            {
                var listUserLaborContract = await _laborContractRepository.GetListUserLaborContract(tenantId, laborContractMeta.UserId,
                    laborContract.Id);

                if (listUserLaborContract != null && listUserLaborContract.Any())
                {
                    foreach (var contract in listUserLaborContract)
                    {
                        contract.IsUse = false;
                    }
                }
                await _laborContractRepository.UpdateList(listUserLaborContract);
            }

            return new ActionResultResponse<string>(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong) :
                    _resourceService.GetString("Thêm mới hợp đồng thành công"), "", laborContract.Id);
        }

        public async Task<ActionResultResponse<string>> InsertLaborContractForm(string tenantId, LaborContractFormMeta laborContractFormMeta)
        {
            var isExistsName = await _laborContractFormRepository.CheckExists(tenantId, "", laborContractFormMeta.LaborContractTypeId, laborContractFormMeta.Name);
            if (isExistsName)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Mẫu hợp đồng đã tồn tại"));

            var laborContractTypeInfo = await _userValueRepository.GetInfo(tenantId, laborContractFormMeta.LaborContractTypeId, true);
            if (laborContractTypeInfo == null)
                return new ActionResultResponse<string>(-2, _resourceService.GetString("Loại hợp đồng không tồn tại"));

            var laborContractForm = new LaborContractForm()
            {
                LaborContractTypeId = laborContractFormMeta.LaborContractTypeId,
                TenantId = tenantId,
                Name = laborContractFormMeta.Name.Trim(),
                Description = laborContractFormMeta.Description?.Trim(),
                IsActive = laborContractFormMeta.IsActive,
                FileId = laborContractFormMeta.FileId,
                FileName = laborContractFormMeta.FileName,
                UnsignName = laborContractFormMeta.Name.Trim().StripVietnameseChars().ToUpper()
            };

            var result = await _laborContractFormRepository.Insert(laborContractForm);
            return new ActionResultResponse<string>(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _resourceService.GetString("Thêm mới mẫu hợp đồng thành công"), "", laborContractForm.Id);
        }

        public async Task<SearchResult<UserLaborContractViewModel>> Search(string tenantId, string languageId, string keyword,
            string userId, int? officeId, string titleId, string positionId,
            string type, DateTime? fromDate, DateTime? toDate, bool? isUse, byte? month, int page, int pageSize)
        {
            return new SearchResult<UserLaborContractViewModel>
            {
                Items = await _laborContractRepository.Search(tenantId, languageId, keyword,
                userId, officeId, titleId, positionId, type, fromDate, toDate, isUse, month, page, pageSize, out int totalRows),
                TotalRows = totalRows
            };
        }

        public async Task<SearchResult<UserLaborContractViewModel>> SearchExpires(string tenantId, string languageId, string keyword,
            string userId, bool isNext, string type, DateTime? fromDate, DateTime? toDate, int page, int pageSize)
        {
            return new SearchResult<UserLaborContractViewModel>
            {
                Items = await _laborContractRepository.SearchExpires(tenantId, languageId, keyword, userId, isNext, type, fromDate, toDate, page,
                pageSize, out int totalRows),
                TotalRows = totalRows
            };
        }

        public async Task<SearchResult<LaborContractFormViewModel>> SearchLaborContractForm(string tenantId, string keyword, string type,
            int page, int pageSize)
        {
            return new SearchResult<LaborContractFormViewModel>
            {
                Items = await _laborContractFormRepository.Search(tenantId, keyword, type, page, pageSize, out int totalRows),
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse<string>> Update(string tenantId, string id, string languageId, LaborContractMeta laborContractMeta)
        {
            if (laborContractMeta.ToDate.HasValue && (DateTime.Compare(laborContractMeta.ToDate.Value, laborContractMeta.FromDate) < 0))
                return new ActionResultResponse<string>(-7, _resourceService.GetString("Ngày kết thúc không được nhỏ hơn ngày bắt đầu"));

            if (laborContractMeta.RegisterDate.HasValue && (DateTime.Compare(laborContractMeta.ToDate.Value, laborContractMeta.FromDate) < 0))
                return new ActionResultResponse<string>(-8, _resourceService.GetString("Ngày ký không được nhỏ hơn ngày bắt đầu"));

            var laborContractInfo = await _laborContractRepository.GetInfo(tenantId, id);
            if (laborContractInfo == null)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Hợp đồng không tồn tại"));

            if (laborContractInfo.ConcurrencyStamp != laborContractMeta.ConcurrencyStamp)
                return new ActionResultResponse<string>(-2, _sharedResourceService.GetString(ErrorMessage.AlreadyUpdatedByAnother));

            if (laborContractInfo.UserId != laborContractMeta.UserId)
                return new ActionResultResponse<string>(-3, _resourceService.GetString("Bạn không thể thay đổi nhân viên."));

            if (laborContractInfo.No != laborContractMeta.No)
            {
                var isExistsNo = await _laborContractRepository.CheckNoExists(tenantId, "", laborContractMeta.No);
                if (isExistsNo)
                    return new ActionResultResponse<string>(-5, _resourceService.GetString("Số hợp đông đã tồn tại"));

                laborContractInfo.No = laborContractMeta.No.Trim();
            }

            if (laborContractInfo.LaborContractFormId != laborContractMeta.LaborContractFormId)
            {
                if (!string.IsNullOrEmpty(laborContractMeta.LaborContractFormId))
                {
                    var laborContractFormInfo = await _laborContractFormRepository.GetInfo(tenantId, laborContractMeta.LaborContractFormId, true);
                    if (laborContractFormInfo == null)
                        return new ActionResultResponse<string>(-6, _resourceService.GetString("Mẫu hợp đồng không tồn tại"));

                    if (laborContractFormInfo.LaborContractTypeId != laborContractMeta.Type)
                        return new ActionResultResponse<string>(-7, _resourceService.GetString($"Mẫu hợp đồng" +
                            $" {laborContractFormInfo.Name} không thuộc loại hợp đồng"));

                    laborContractInfo.LaborContractFormId = laborContractMeta.LaborContractFormId;
                    laborContractInfo.LaborContractFormName = laborContractFormInfo.Name;
                }
                else
                {
                    laborContractInfo.LaborContractFormId = "";
                    laborContractInfo.LaborContractFormName = "";
                }
            }

            if (laborContractInfo.Type != laborContractMeta.Type)
            {
                var laborContractTypeInfo = await _userValueRepository.GetInfo(tenantId, laborContractMeta.Type, true);
                if (laborContractTypeInfo == null)
                    return new ActionResultResponse<string>(-8, _resourceService.GetString("Loại hợp đồng không tồn tại"));

                laborContractInfo.Type = laborContractMeta.Type;
                laborContractInfo.TypeName = laborContractTypeInfo.Name;
            }

            var oldStatus = laborContractInfo.Status;

            laborContractInfo.ConcurrencyStamp = Guid.NewGuid().ToString();
            laborContractInfo.FromDate = laborContractMeta.FromDate;
            laborContractInfo.ToDate = laborContractMeta.ToDate;
            laborContractInfo.Note = laborContractMeta.Note?.Trim();
            laborContractInfo.FileId = laborContractMeta.FileId;
            laborContractInfo.FileName = laborContractMeta.FileName;
            laborContractInfo.IsUse = laborContractMeta.IsUse;
            laborContractInfo.UnsignName = $"{laborContractInfo.FullName}{laborContractMeta.No}".StripVietnameseChars().ToUpper();
            laborContractInfo.RegisterDate = laborContractMeta.RegisterDate;
            laborContractInfo.TaskDescription = laborContractMeta.TaskDescription?.Trim();
            laborContractInfo.Time = laborContractMeta.Time?.Trim();
            laborContractInfo.SalaryCalculationUnit = laborContractMeta.SalaryCalculationUnit;
            laborContractInfo.BasicSalary = laborContractMeta.BasicSalary;
            laborContractInfo.ProbationarySalary = laborContractMeta.ProbationarySalary;
            laborContractInfo.OfficialSalary = laborContractMeta.OfficialSalary;
            laborContractInfo.Status = laborContractMeta.Status;

            var result = await _laborContractRepository.Update(laborContractInfo);
            if (result <= 0)
                return new ActionResultResponse<string>(-10, _resourceService.GetString("Có gì đó không đúng. Vui lòng liên hệ với quản trị viên"));

            if (laborContractMeta.IsUse)
            {
                var listUserLaborContract = await _laborContractRepository.GetListUserLaborContract(tenantId, laborContractMeta.UserId, id);
                if (listUserLaborContract != null && listUserLaborContract.Any())
                {
                    foreach (var contract in listUserLaborContract)
                    {
                        contract.IsUse = false;
                    }
                }

                await _laborContractRepository.UpdateList(listUserLaborContract);
            }

            if (oldStatus != laborContractMeta.Status && laborContractMeta.IsUse)
            {
                var userInfoUpdate = await _userRepository.GetInfo(laborContractMeta.UserId);
                if (userInfoUpdate != null)
                {
                    userInfoUpdate.Status = laborContractMeta.Status;
                    await _userRepository.Update(userInfoUpdate);
                }
            }

            return new ActionResultResponse<string>(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong) :
                    _resourceService.GetString("Cập nhật hợp đồng thành công"), "", laborContractInfo.ConcurrencyStamp);
        }

        public async Task<ActionResultResponse> UpdateLaborContractForm(string tenantId, string id, LaborContractFormMeta laborContractFormMeta)
        {
            var info = await _laborContractFormRepository.GetInfo(tenantId, id, false);
            if (info == null)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Mẫu hợp đồng không tồn tại"));

            if (info.TenantId != tenantId)
                return new ActionResultResponse<string>(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (info.ConcurrencyStamp != laborContractFormMeta.ConcurrencyStamp)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.AlreadyUpdatedByAnother));

            var isExistsName = await _laborContractFormRepository.CheckExists(tenantId, id, laborContractFormMeta.LaborContractTypeId, laborContractFormMeta.Name);
            if (isExistsName)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Mẫu hợp đồng đã tồn tại"));

            if (info.LaborContractTypeId != laborContractFormMeta.LaborContractTypeId)
            {
                var laborContractTypeInfo = await _userValueRepository.GetInfo(tenantId, laborContractFormMeta.LaborContractTypeId, true);
                if (laborContractTypeInfo == null)
                    return new ActionResultResponse<string>(-5, _resourceService.GetString("Loại hợp đồng không tồn tại"));
            }

            info.LaborContractTypeId = laborContractFormMeta.LaborContractTypeId;
            info.Name = laborContractFormMeta.Name.Trim();
            info.Description = laborContractFormMeta.Description?.Trim();
            info.IsActive = laborContractFormMeta.IsActive;
            info.UnsignName = laborContractFormMeta.Name.Trim().StripVietnameseChars().ToUpper();
            info.ConcurrencyStamp = Guid.NewGuid().ToString();
            info.FileId = laborContractFormMeta.FileId;
            info.FileName = laborContractFormMeta.FileName;

            var result = await _laborContractFormRepository.Update(info);
            return new ActionResultResponse<string>(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _resourceService.GetString("Cập nhật loại hợp đồng thành công"), "", info.ConcurrencyStamp);
        }
    }
}
