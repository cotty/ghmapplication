﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Patient.Domain.ViewModels
{
    public class ContactPatientDetailViewModel
    {
        public string PatientId { get; set; }

        public string ConcurrencyStamp { get; set; }

        public List<ContactPatientViewModel> ContactPatients { get; set; }
    }
}
