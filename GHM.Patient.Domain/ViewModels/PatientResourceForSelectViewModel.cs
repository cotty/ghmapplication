﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Patient.Domain.ViewModels
{
    public class PatientResourceForSelectViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
