﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Patient.Domain.ViewModels
{
    public class JobForSelectViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
