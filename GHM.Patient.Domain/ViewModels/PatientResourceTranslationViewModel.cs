﻿
namespace GHM.Patient.Domain.ViewModels
{
   public class PatientResourceTranslationViewModel
    {
        public string LanguageId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
