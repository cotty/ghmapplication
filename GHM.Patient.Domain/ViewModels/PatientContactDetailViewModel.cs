﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Patient.Domain.ViewModels
{
    public class PatientContactDetailViewModel
    {
        public string PatientId { get; set; }
        public string ConcurrencyStamp { get; set; }

        public List<PatientContactViewModel> ContactPatients { get; set; }
    }
}
