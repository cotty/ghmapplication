﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Patient.Domain.ViewModels
{
    public class PatientSubjectViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public int Order { get; set; }
        public float? TotalReduction { get; set; }
        //public List<PatientSubjectTranslationViewModel> PatientSubjectTranslations { get; set; }
    }
}
