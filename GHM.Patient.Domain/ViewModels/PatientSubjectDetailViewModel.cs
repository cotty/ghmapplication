﻿using GHM.Patient.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Patient.Domain.ViewModels
{
    public class PatientSubjectDetailViewModel
    {
        public string PatientSubjectId { get; set; }
        public string ConcurrencyStamp { get; set; }
        public bool IsActive { get; set; }
        public int Order { get; set; }
        public float? TotalReduction { get; set; }
        public List<PatientSubjectTranslationViewModel> PatientSubjectTranslations { get; set; }
    }
}
