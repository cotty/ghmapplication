﻿
using GHM.Patient.Domain.Models;
using System.Collections.Generic;

namespace GHM.Patient.Domain.ViewModels
{
 public   class PatientResourceDetailViewModel
    {
        public string Id { get; set; }
        public bool IsActive { get; set; }
        public string ConcurrencyStamp { get; set; }
        public int Order { get; set; }
        public List<PatientResourceTranslationViewModel> PatientResourceTranslations { get; set; }
    }
}
