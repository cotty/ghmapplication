﻿
namespace GHM.Patient.Domain.ViewModels
{
    public class JobTranslationViewModel
    {
        public string LanguageId { get; set; }

        public string Name { get; set; }

        public string ParentName { get; set; }

        public string Description { get; set; }
    }
}
