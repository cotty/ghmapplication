﻿using System;

namespace GHM.Patient.Domain.ViewModels
{
    public class PatientSearchViewModel
    {
        public string Id { get; set; }

        /// <summary>
        /// Mã bệnh nhân
        /// </summary>
        public string PatientCode { get; set; }

        /// <summary>
        /// Ten bệnh nhân
        /// </summary>
        public string FullName { get; set; }

        // chua co SoDienThoai, Email
        public string PhoneNumber { get; set; }

        public string Email { get; set; }
        /// <summary>
        /// Địa chỉ 
        /// </summary>
        public string Address { get; set; }

        public string ContactPerson { get; set; }

        public string ContactPhoneNumber { get; set; }
        
        public DateTime? Birthday { get; set; }        

        /// <summary>
        /// Ngày khám gần nhất
        /// </summary>
        public DateTime? LastDay { get; set; }

    }   
}
