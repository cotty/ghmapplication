﻿using GHM.Patient.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Patient.Domain.ViewModels
{
    public class PatientDetailViewModel
    {
        public string Id { get; set; }


        public string PatientCode { get; set; }

        /// <summary>
        /// Ten bệnh nhân
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Ngày sinh 
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// Giới tính
        /// </summary>
        public int? Gender { get; set; } 

        /// <summary>
        /// Nguồn khách
        /// </summary>
        public string PatientResourceId { get; set; }

        /// <summary>
        /// Số CMND
        /// </summary>
        public string IdCardNumber { get; set; }

        /// <summary>
        /// Nghề nghiệp
        /// </summary>
        public int? JobId { get; set; }

        /// <summary>
        /// Quốc gia
        /// </summary>
        public int? NationalId { get; set; }

        public string NationalName { get; set; }
        /// <summary>
        /// Dân tộc
        /// </summary>
        public int? EthnicId { get; set; }

        public string EthnicName { get; set; }

        /// <summary>
        /// Tôn giáo
        /// </summary>
        public int? ReligionId { get; set; }

        public string ReligionName { get; set; }
        /// <summary>
        /// Tỉnh thành
        /// </summary>
        public int? ProvinceId { get; set; }

        public string ProvinceName { get; set; }

        /// <summary>
        /// Quận huyệnh
        /// </summary>
        public int? DistrictId { get; set; }

        public string DistrictName { get; set; }

        /// <summary>
        /// Địa chỉ liên hệ
        /// </summary>
        public string Address { get; set; }

        public string ConcurrencyStamp { get; set; }

        public List<ContactPatientViewModel> ContactPatients { get; set; }

        public List<PatientContactViewModel> PatientContacts { get; set; }

    }

    public class ContactPatientViewModel
    {
        public string Id { get; set; }

        public string PatientId { get; set; }

        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

        public string ConcurrencyStamp { get; set; }
    }

    public class PatientContactViewModel
    {
        public string Id { get; set; }

        public string PatientId { get; set; }

        public ContactType ContactType { get; set; }

        public string ContactValue { get; set; }

        public string ConcurrencyStamp { get; set; }

    }
}
