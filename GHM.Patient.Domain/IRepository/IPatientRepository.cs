﻿using GHM.Patient.Domain.Models;
using GHM.Patient.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Patient.Domain.IRepository
{
    /// <summary>
    /// Interface khai báo các phương thức sẽ có để thao tác với bảng trong db.
    /// </summary>
    public interface IPatientRepository
    {
        Task<List<PatientSearchViewModel>> Search(string tenantId, string keyword, DateTime? createDate, int page, int pageSize, out int totalRows);

        Task<int> Insert(Models.Patient patient);

        Task<int> Update(Models.Patient patient);

        Task<int> UpdateUnsignName(string id, string unsignName);

        Task<Models.Patient> GetInfo(string id, bool isReadOnly = false);

        Task<int> ForceDelete(string id);

    }

    public interface IContactPatientRepository
    {
        Task<int> Insert(PatientRelativesContact contactPatient);

        Task<int> Inserts(List<PatientRelativesContact> contactPatients);

        Task<int> Update(PatientRelativesContact contactPatient);

        Task<PatientRelativesContact> GetInfo(string id);

        Task<int> DeleteByPatientId(string patientId);

        Task<int> ForceDelete(string id);

        Task<List<ContactPatientViewModel>> GetByPatientId(string patinetId);

    }

    public interface IPatientContactRepository
    {
        Task<int> Insert(PatientContact patientContact);

        Task<int> Inserts(List<PatientContact> patientContacts);

        Task<int> Update(PatientContact patientContact);

        Task<PatientContact> GetInfo(string id);

        Task<int> DeleteByPatientId(string patientId);

        Task<int> ForceDelete(string id);

        Task<List<PatientContactViewModel>> GetByPatientId(string patinetId);
    }
}
