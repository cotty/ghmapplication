﻿using GHM.Patient.Domain.Models;
using GHM.Patient.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Patient.Domain.IRepository
{
    public interface IPatientSubjectRepository
    {
        Task<List<PatientSubjectViewModel>> Search(string tenantId, string languageId, string keyword,float? totalReduction,
            bool? isActive, int page, int pageSize, out int totalRows);

        Task<int> Insert(PatientSubject patientsubject);

        Task<int> Update(PatientSubject patientsubject);

        Task<int> Delete(string patientsubjectId);

        Task<int> ForceDelete(string patientsubjectId);

        Task<PatientSubject> GetInfo(string patientsubjectId, bool isReadOnly = false);

        Task<PatientSubject> GetInfo(string patientsubjectId, string tenantId, bool isReadOnly = false);

        Task<bool> CheckExistsByPatientSubjectId(string patientsubjectId);

        Task<List<PatientSubject>> GetAll(string tenantId, bool isReadOnly = false);


    }
}
