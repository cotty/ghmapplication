﻿using GHM.Patient.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Patient.Domain.IRepository
{
    public interface IPatientSubjectTranslationRepository
    {
        Task<int> Insert(PatientSubjectTranslation patientSubjectTranslation);

        Task<int> Inserts(List<PatientSubjectTranslation> patientSubjectTranslations);

        Task<int> Update(PatientSubjectTranslation patientSubjectTranslation);

        //Task<int> Delete(string patientsubjectId, string languageId);

        //Task<int> Delete(string PatientSubjectId);

        Task<int> ForceDelete(string PatientSubjectId);

        Task<PatientSubjectTranslation> GetInfo(string patientsubjectId, string languageId, bool isReadOnly = false);

        Task<List<PatientSubjectTranslation>> GetsByPatientSubjectId(string patientsubjectId);

        Task<bool> CheckUserNameExists(string patientsubjectId, string languageId, string name);
    }
}
