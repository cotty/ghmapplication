﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Patient.Domain.Models;
using GHM.Patient.Domain.ViewModels;
namespace GHM.Patient.Domain.IRepository
{
  public  interface IPatientResourceRepository
    {
        Task<List<PatientResourceViewModel>> Search(string tenantId, string languageId, string keyword,
        bool? isActive, int page, int pageSize, out int totalRows);


        Task<int> Insert(PatientResource patientResource);

        Task<int> Update(PatientResource patientResource);

        Task<int> Delete(string patientSourceId);

        Task<int> ForceDelete(string patientSourceId);

        Task<PatientResource> GetInfo(string id, bool isReadonly = false);

        Task<List<PatientResourceForSelectViewModel>> SearchForSelect(string tenantId, string languageId, string keyword, int page, int pageSize, out int totalRows);
    }
}
