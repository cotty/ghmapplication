﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Patient.Domain.ModelMetas;
using GHM.Patient.Domain.Models;
using GHM.Patient.Domain.ViewModels;

namespace GHM.Patient.Domain.IRepository
{
    public interface IPatientResourceTranslationRepository
    {
        Task<int> Insert(PatientResourceTranslation patientResourceTranslation);

        Task<int> Update(PatientResourceTranslation patientResourceTranslation);

        Task<int> Inserts(List<PatientResourceTranslation> patientResourceTranslations);

        Task<int> Delete(string patientResourceId, string languageId);

        Task<int> ForceDeleteByPatientResourceId(string patientResourceId);

        Task<int> DeleteByPatientResourceId(string patientResourceId);

        Task<PatientResourceTranslation> GetInfo(string patientResourceId, string languageId, bool isReadOnly = false);

        Task<List<PatientResourceTranslation>> GetsByPatientResourceId(string patientResourceId);

        Task<bool> CheckExists(string patientResourceId, string languageId, string name);


    }
}
