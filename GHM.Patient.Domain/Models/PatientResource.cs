﻿using System;
using GHM.Infrastructure.Models;
namespace GHM.Patient.Domain.Models
{
  public  class PatientResource : EntityBase<string>
    {
        /// <summary>
        /// Mã khách hàng.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Thứ tự sắp xếp.
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Trạng thái xóa
        /// </summary>
        public bool IsDelete { get; set; }

        /// <summary>
        /// Trạng thái sử dụng.
        /// </summary>
        public bool IsActive { get; set; }

        public PatientResource()
        {
            Order = 0;
            IsDelete = false;
            CreateTime = DateTime.Now;
            IsActive = true;
        }
    }
}
