﻿using GHM.Infrastructure.Models;

namespace GHM.Patient.Domain.Models
{
    public class PatientSubjectTranslation 
    {
        //Đối tượng bệnh nhân
        public string PatientSubjectId { get; set; }

        public string LanguageId { get; set; }

        public string Name { get; set; }

        public string UnsignName { get; set; }

        public string Description { get; set; }

        //public bool IsDelete { get; set; }

    }
}
