﻿using GHM.Infrastructure.Models;

namespace GHM.Patient.Domain.Models
{
    public class PatientRelativesContact : EntityBase<string>
    {
        public string PatientId { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
    }
}
