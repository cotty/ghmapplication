﻿using GHM.Infrastructure.Models;
using GHM.Patient.Domain.Constants;

namespace GHM.Patient.Domain.Models
{
    public class PatientContact : EntityBase<string>
    {
        public string PatientId { get; set; }
        public ContactType ContactType { get; set; }
        public string ContactValue { get; set; }
    }
}
