﻿namespace GHM.Patient.Domain.Constants
{
    public enum Gender
    {
        /// <summary>
        /// Nữ.
        /// </summary>
        Female,
        /// <summary>
        /// Nam.
        /// </summary>
        Male,       
        /// <summary>
        /// Khác.
        /// </summary>
        Other
    }
}
