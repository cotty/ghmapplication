﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Patient.Domain.Constants
{
    public enum ContactType
    {
        HomePhone,
        MobilePhone,
        Email,
        Fax
    }
}
