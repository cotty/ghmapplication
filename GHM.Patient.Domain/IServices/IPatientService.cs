﻿using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Patient.Domain.Models;
using GHM.Patient.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Patient.Domain.IServices
{
    /// <summary>
    /// Service dùng để sử lý business logic.
    /// </summary>
    public interface IPatientService
    {
        Task<ActionResultResponse> Insert(string tenantId, ModelMetas.PatientMeta patientMeta);

        Task<ActionResultResponse> Update(string tenantId, string id, ModelMetas.PatientMeta patientMeta);

        Task<ActionResultResponse> ForceDelete(string tenantId, string PatientId);

        Task<ActionResultResponse<PatientDetailViewModel>> GetDetail(string patientId);

        Task<SearchResult<PatientSearchViewModel>> Search(string tenantId, string keyword, DateTime? createDate, int page, int pageSize);
    }

    public interface IContactPatientService
    {
        Task<ActionResultResponse<string>> Insert(string tenantId, ModelMetas.ContactPatientMeta contactPatientMeta);

        Task<ActionResultResponse> Update(string tenantId, string id, ModelMetas.ContactPatientMeta contactPatientMeta);

        Task<ActionResultResponse> ForceDelete(string tenantId, string id);
    }

    public interface IPatientContactService
    {
        Task<ActionResultResponse<string>> Insert(string tenantId, ModelMetas.PatientContactMeta patientContactMeta);

        Task<ActionResultResponse> Update(string tenantId, string id, ModelMetas.PatientContactMeta patientContactMeta);

        Task<ActionResultResponse> ForceDelete(string tenantId, string id);
    }

}
