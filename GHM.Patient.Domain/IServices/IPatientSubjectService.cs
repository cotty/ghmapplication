﻿using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Patient.Domain.ModelMetas;
using GHM.Patient.Domain.ViewModels;
using System.Threading.Tasks;

namespace GHM.Patient.Domain.IServices
{
    public interface IPatientSubjectService
    {
        Task<ActionResultResponse> Insert(string tenantId, PatientSubjectMeta patientsubjectMeta);

        Task<ActionResultResponse> Update(string tenantId, string id, PatientSubjectMeta patientsubjectMeta);

        Task<ActionResultResponse> Delete(string tenantId, string id);

        Task<ActionResultResponse<PatientSubjectDetailViewModel>> GetDetail(string tenantId, string languageId, string id);

        Task<SearchResult<PatientSubjectViewModel>> Search(string tenantId, string languageId, string keyword, float? totalReduction, bool? isActive
            , int page, int pageSize);
    }
}
