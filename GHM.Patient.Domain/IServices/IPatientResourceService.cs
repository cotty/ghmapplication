﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Patient.Domain.ModelMetas;
using GHM.Patient.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
namespace GHM.Patient.Domain.IServices
{
  public  interface IPatientResourceService
    {
        Task<ActionResultResponse<string>> Insert(string tenantId, PatientResourceMeta patientResourceMeta);

        Task<ActionResultResponse> Update(string tenantId,string id, PatientResourceMeta patientResourceMeta);

        Task<ActionResultResponse> Delete(string tenantId,string id);

        Task<ActionResultResponse<PatientResourceDetailViewModel>> GetDetail(string tenantId, string id);

        Task<SearchResult<PatientResourceViewModel>> Search(string tenantId, string languageId, string keyword, bool? isActive,
            int page, int pageSize);

        Task<List<PatientResourceForSelectViewModel>> SearchForSelect(string tenantId, string languageId, string keyword, int page, int pageSize);
    }
}
