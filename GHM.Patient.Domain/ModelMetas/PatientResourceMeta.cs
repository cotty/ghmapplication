﻿using System.Collections.Generic;
using GHM.Patient.Domain.Models;

namespace GHM.Patient.Domain.ModelMetas
{
    public class PatientResourceMeta
    {
        public bool IsActive { get; set; }
        public string ConcurrencyStamp { get; set; }
        public int Order { get; set; }
        public List<PatientResourceTranslationMeta> PatientResourceTranslations { get; set; }
    }
}
