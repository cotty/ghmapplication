﻿
using GHM.Patient.Domain.Constants;
using GHM.Patient.Domain.Models;
using System;
using System.Collections.Generic;

namespace GHM.Patient.Domain.ModelMetas
{
    /// <summary>
    /// Model meta được gửi lên từ truy vấn của client.
    /// </summary>
    public class PatientMeta
    {
        /// <summary>
        /// Mã bệnh nhân
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// Ngày sinh 
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// Giới tính
        /// </summary>
        public int? Gender { get; set; }

        /// <summary>
        /// Nguồn khách
        /// </summary>
        public string PatientResourceId { get; set; }

        /// <summary>
        /// Số CMND
        /// </summary>
        public string IdCardNumber { get; set; }

        /// <summary>
        /// Nghề nghiệp
        /// </summary>
        public int? JobId { get; set; }

        /// <summary>
        /// Quốc gia
        /// </summary>
        public int? NationalId { get; set; }

        /// <summary>
        /// Dân tộc
        /// </summary>
        public int? EthnicId { get; set; }

        /// <summary>
        /// Tôn giáo
        /// </summary>
        public int? ReligionId { get; set; }

        /// <summary>
        /// Tỉnh thành
        /// </summary>
        public int? ProvinceId { get; set; }

        /// <summary>
        /// Quận huyệnh
        /// </summary>
        public int? DistrictId { get; set; }

        /// <summary>
        /// Địa chỉ liên hệ
        /// </summary>
        public string Address { get; set; }

        public string ConcurrencyStamp { get; set; }

        public List<PatientRelativesContact> PatientRelativesContacts { get; set; }

        public List<PatientContact> PatientContacts { get; set; }
    }

    public class ContactPatientMeta
    {
        public string PatientId { get; set; }

        public string ConcurrencyStamp { get; set; }

        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

    }

    public class PatientContactMeta
    {
        public string PatientId { get; set; }

        public string ConcurrencyStamp { get; set; }

        public ContactType ContactType { get; set; }

        public string ContactValue { get; set; }

    }

}
