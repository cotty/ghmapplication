﻿using GHM.Patient.Domain.Models;
using System.Collections.Generic;

namespace GHM.Patient.Domain.ModelMetas
{
    public class PatientSubjectMeta
    {
        public string PatientSubjectId{ get; set; }
        public string ConcurrencyStamp{ get; set; }
        public bool IsActive { get; set; }
        public int Order { get; set; }
        public float? TotalReduction { get; set; }
        public List<PatientSubjectTranslation> PatientSubjectTranslations { get; set; }
    }
}
