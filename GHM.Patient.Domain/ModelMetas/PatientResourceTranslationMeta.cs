﻿
namespace GHM.Patient.Domain.ModelMetas
{
public    class PatientResourceTranslationMeta
    {
        public string PatientResourceId { get; set; }

        public string LanguageId { get; set; }

        public string Name { get; set; }

        public string UnsignName { get; set; }

        public string Description { get; set; }
    }
}
