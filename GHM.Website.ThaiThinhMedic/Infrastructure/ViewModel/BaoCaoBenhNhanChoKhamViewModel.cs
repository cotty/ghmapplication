﻿using System;

namespace GHM.Website.ThaiThinhMedic.Infrastructure.ViewModels
{
    public class BaoCaoBenhNhanChoKhamViewModel
    {
        public string HoTenBenhNhan { get; set; }
        public DateTime? NgayThangNamSinh { get; set; }
        public string Tuoi { get; set; }
        public bool? MaGioiTinh { get; set; }
        public string GioiTinh { get; set; }
        public string SoDienThoai { get; set; }
        public string MaBenhNhan { get; set; }
        public long? IDPhieuKham { get; set; }
        public string SoPhieuKham { get; set; }
        public string MaBacSy { get; set; }
        public string HoTen { get; set; }
        public string TenDichVu { get; set; }
        public int? LanDen { get; set; }
        public int? STT { get; set; }
        public string TenPhongKham { get; set; }
        public string MaPhongKham { get; set; }
        public string DiaChi { get; set; }
        public string SoPhong { get; set; }
        public int? TrangThai { get; set; }
        public string GioDatHen { get; set; }
    }
}
