namespace GHM.Website.ThaiThinhMedic.Infrastructure.ViewModels
{
    public class DmKhoaSelectAllViewModel
    {
        public string MaKhoa { get; set; }
        public string TenKhoa { get; set; }
        public string TruongKhoa { get; set; }
        public string DiaChiKhoa { get; set; }
        public string SoDienThoaiKhoa { get; set; }
        public string GhiChu { get; set; }
        public bool? LSCLS { get; set; }
        public int? STT { get; set; }
        public bool? LLV { get; set; }
        public string STTKhoa { get; set; }
    }

}
