﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GHM.Website.ThaiThinhMedic.Infrastructure.Constants
{
    public enum SliderType
    {
        /// <summary>
        /// Slider trang chủ.
        /// </summary>
        HomePageSlider,
        /// <summary>
        /// Popup trang chủ.
        /// </summary>
        HomePopup
    }
}
