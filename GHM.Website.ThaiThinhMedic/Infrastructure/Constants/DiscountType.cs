﻿namespace GHM.Website.ThaiThinhMedic.Infrastructure.Constants
{
    public enum DiscountType
    {
        Percent = 1,
        Money = 2
    }
}
