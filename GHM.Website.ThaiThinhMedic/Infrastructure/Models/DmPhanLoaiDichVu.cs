
namespace GHM.Website.ThaiThinhMedic.Infrastructure.Models
{
    public class DmPhanLoaiDichVu
    {
        public string MaPhanLoaiDichVu { get; set; }
        public string TenPhanLoaiDichVu { get; set; }
        public string MaLoaiDichVu { get; set; }
        public string GhiChu { get; set; }
        public int? Stt { get; set; }
    }

}
