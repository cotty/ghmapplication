﻿namespace GHM.Website.ThaiThinhMedic.Infrastructure.Models
{
    public class NguoiThamGiaCung
    {
        public int ID { get; set; }
        public int IDQuaTang { get; set; }
        public string TenNguoiThamGiaCung { get; set; }
    }
}
