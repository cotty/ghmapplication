﻿namespace GHM.Website.ThaiThinhMedic.Infrastructure.Models
{
    /// <summary>
    /// Danh mục Phân loại dịch vụ
    /// </summary>
    public class ServiceCategory
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ServiceTypeId { get; set; }
    }
}
