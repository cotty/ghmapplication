﻿namespace GHM.Website.ThaiThinhMedic.Infrastructure.Models
{
    /// <summary>
    /// Danh mục Loại dịch vụ
    /// </summary>
    public class ServiceType
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
