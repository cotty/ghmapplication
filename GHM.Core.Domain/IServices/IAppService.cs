﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Core.Domain.Models;
using GHM.Core.Domain.ViewModels;
using GHM.Infrastructure.Models;
using UserSetting = GHM.Core.Domain.ViewModels.UserSetting;

namespace GHM.Core.Domain.IServices
{
    public interface IAppService
    {
        Task<AppSettingViewModel> GetAppSettings(string userId, string tenantId, string languageId);

        Task<List<TenantLanguageViewModel>> GetLanguages(string tenantId);

        Task<List<RolesPagesViewModel>> GetPermissions(string userId);

        Task<List<UserSetting>> GetSettings(string userId);

        Task<ActionResultResponse> SaveGeneralSetting(string tenantId, string userId, string fullName, GeneralSetting generalSetting);

        Task<ActionResultResponse<GeneralSetting>> GetGeneralSetting(string tenantId);
    }
}
