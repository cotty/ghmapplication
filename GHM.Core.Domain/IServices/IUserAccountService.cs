﻿using GHM.Core.Domain.ModelMetas;
using GHM.Infrastructure.Models;
using System.Threading.Tasks;
using GHM.Infrastructure.ViewModels;
using GHM.Core.Domain.ViewModels;

namespace GHM.Core.Domain.IServices
{
    public interface IUserAccountService
    {
        Task<SearchResult<UserAccountViewModel>> Search(string tenantId, string keyword, bool? isActive, int page, int pageSize);

        Task<ActionResultResponse> InsertUserAccount(AccountUserMeta userAccount);

        Task<ActionResultResponse> UpdateUserAccount(string userId, AccountUserMeta userAccount);

        Task<ActionResultResponse> UpdatePassword(string userId, UpdatePasswordMeta updatePasswordMeta);

        Task<ActionResultResponse> DeleteUserAccount(string userId);

        Task<ActionResultResponse<UserInfoViewModel>> GetUserInfo(string id);

        Task<bool> ValidateCredentials(string userName, string password);

        Task<UserAccount> FindByUserName(string userName);

        Task<ActionResultResponse> ResetLockout(string userName);

        Task<ActionResultResponse> LockAccount(string userName);

        Task<ActionResultResponse> UpdateIsActive(string userName, bool isActive);

    }
}
