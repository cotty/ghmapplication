﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Core.Domain.ModelMetas;
using GHM.Core.Domain.Models;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;

namespace GHM.Core.Domain.IServices
{
    public interface IClientService
    {
        Task<SearchResult<Client>> Search(string keyword, bool? isEnable, int page, int pageSize);

        Task<ActionResultResponse<Client>> Insert(ClientMeta clientMeta);

        Task<ActionResultResponse<Client>> Update(ClientMeta clientMeta);

        Task<ActionResultResponse> Delete(string clientId);
    }
}
