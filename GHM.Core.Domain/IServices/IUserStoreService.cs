﻿using System.Threading.Tasks;
using GHM.Infrastructure.Models;

namespace GHM.Core.Domain.IServices
{
    public interface IUserStoreService
    {
        Task<bool> ValidateCredentials(string userName, string password);

        Task<UserAccount> FindByUsername(string userName);
    }
}
