﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Core.Domain.Models;
using GHM.Core.Domain.ViewModels;
using GHM.Infrastructure.Models;

namespace GHM.Core.Domain.IServices
{
    public interface ITenantLanguageService
    {
        Task<ActionResultResponse> Save(TenantLanguage tenantLanguage);

        Task<ActionResultResponse> UpdateActiveStatus(string tenantId, string languageId, bool isActive);

        Task<ActionResultResponse> UpdateDefaultStatus(string tenantId, string languageId, bool isDefault);

        Task<ActionResultResponse> Delete(string tenantId, string languageId);

        Task<List<TenantLanguageViewModel>> GetsByTenantId(string tenantId);
    }
}
