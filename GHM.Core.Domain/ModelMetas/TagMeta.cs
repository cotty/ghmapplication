﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Core.Domain.ModelMetas
{
    public class TagMeta
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
