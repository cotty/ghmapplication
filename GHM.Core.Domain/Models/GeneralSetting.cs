﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Core.Domain.Models
{
    public class GeneralSetting
    {
        public string Title { get; set; }
        public string FaviconUrl { get; set; }
        public string LogoUrl { get; set; }
    }
}
