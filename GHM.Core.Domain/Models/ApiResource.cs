﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GHM.Core.Domain.Models
{
    public class ApiResource : IdentityServer4.Models.ApiResource
    {        
        public ApiResource()
        {

        }

        public ApiResource(string name, string description, string displayName, bool enabled)
        {
            Name = name;
            Description = description;
            DisplayName = displayName;
            Enabled = enabled;
        }
    }
}
