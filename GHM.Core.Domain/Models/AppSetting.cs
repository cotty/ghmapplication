﻿using System;

namespace GHM.Core.Domain.Models
{
    public class AppSetting
    {
        public string TenantId { get; set; }
        public string Key { get; set; }
        public string LanguageId { get; set; }
        public string GroupId { get; set; }
        public string Value { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? LastUpdate { get; set; }
        public string LastUpdateUserId { get; set; }
        public string LastUpdateFullName { get; set; }
        public string ConcurrencyStamp { get; set; }

        public AppSetting()
        {
            CreateTime = DateTime.Now;
            ConcurrencyStamp = Guid.NewGuid().ToString();
        }
    }
}
