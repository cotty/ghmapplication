﻿using GHM.Core.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GHM.Core.Domain.IRepository
{
    public interface ITenantPageRepository
    {
        Task<bool> CheckExists(string tenantId, int pageId);

        Task<int> Insert(TenantPage tenantPage);

        Task<int> Update(TenantPage tenantPage);

        Task<int> Inserts(List<TenantPage> tenantPages);

        Task<int> Delete(string tenantId, int pageId);

        Task<int> Deletes(List<TenantPage> tenantPages);

        Task<TenantPage> GetInfo(string tenantId, int pageId, bool isReadOnly = false);

        Task<List<TenantPage>> GetListByPageId(int pageId, bool isReadOnly = false);

        Task<List<TenantPage>> GetListByTenantId(string tenantId, bool isReadOnly = false);

        Task<int> DeleteByTenantId(string tenantId);

        Task<int> DeleteByPageId(int pageId);
    }
}
