﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Core.Domain.Models;
using GHM.Infrastructure.Models;

namespace GHM.Core.Domain.IRepository
{
    public interface ILanguageRepository
    {
        Task<int> Insert(Language language);
        Task<int> Update(Language language);
        Task<List<T>> GetAllLanguage<T>(Expression<Func<Language, T>> projector);
    }
}
