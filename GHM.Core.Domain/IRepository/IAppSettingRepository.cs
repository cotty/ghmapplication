﻿using System.Threading.Tasks;
using GHM.Core.Domain.Models;
using GHM.Core.Domain.ViewModels;

namespace GHM.Core.Domain.IRepository
{
    public interface IAppSettingRepository
    {
        Task<int> Save(AppSetting appSetting);

        Task<int> Insert(AppSetting appSetting);

        Task<int> Update(AppSetting appSetting);

        Task<AppSettingViewModel> GetSetting(string tenantId);

        Task<AppSetting> GetInfo(string tenantId, string key, string languageId, bool isReadOnly = false);
    }
}
