﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Core.Domain.Models.ConfigViewModels;
using GHM.Core.Domain.ViewModels;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using Microsoft.AspNetCore.Identity;

namespace GHM.Core.Domain.IRepository
{
    public interface IUserAccountRepository : IUserPasswordStore<UserAccount>
    {
        Task<int> Insert(UserAccount userAccount);
        Task<BriefUser> GetCurrentUser(string id);

        Task<List<UserAccountSearchViewModel>> SearchForSelect(string keyword, bool? isActive, int page, int pageSize, out int totalRows);

        Task<List<UserAccountViewModel>> Search(string tenantId, string keyword, bool? isActive, int page, int pageSize, out int totalRows);

        Task<UserAccount> GetInfo(string id, bool isReadOnly = false);

        /// <summary>
        /// Lấy thông tin tài khoản by username không check isActive
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="isReadOnly"></param>
        /// <returns></returns>
        Task<UserAccount> GetAccountInfoByUserName(string userName, bool isReadOnly = false);

        Task<UserAccount> GetInfoByUserName(string tenantId, string userName, bool isReadOnly = false);
        /// <summary>
        /// Lấy thông tin tài khoản có check isActive
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="isReadOnly"></param>
        /// <returns></returns>
        Task<UserAccount> GetInfoByUserName(string userName, bool isReadOnly = false);

        Task<bool> CheckUserNameExists(string id, string userName);

        Task<bool> ValidateCredentialsAsync(string userName, string password);

        Task<bool> CheckEmailExists(string id, string email);

        Task<bool> CheckExistsByUserId(string userId);

        int UpdateAccessFailCount(string userName, int failCount, bool lockoutOnFailure = false);

        Task<int> ResetLockout(string userName);

        Task<int> LockAccount(string userName);

        Task<int> UpdateUserAccount(UserAccount userAccount);

        Task<int> UpdatePassword(string userId, byte[] passwordSalt, string passwordHash);

        Task<int> DeleteAccount(UserAccount userAccount);

        Task<UserInfoViewModel> GetUserInfo(string id);
    }
}
