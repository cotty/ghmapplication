(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-configs-config-module"],{

/***/ "./src/app/modules/configs/account/account-form/account-form.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/modules/configs/account/account-form/account-form.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #accountFormModal size=\"md\"\r\n          (hidden)=\"onModalHidden()\">\r\n    <nh-modal-header class=\"uppercase\">\r\n        {isUpdate, select, 0 {Add new account} 1 {Update account}}\r\n    </nh-modal-header>\r\n    <form class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content>\r\n            <div class=\"form-group\" [class.has-error]=\"formErrors?.userName\">\r\n                <label\r\n                    i18n-ghmLabel=\"@@userName\"\r\n                    ghmLabel=\"Username\"\r\n                    class=\"col-sm-4 control-label\" [required]=\"true\"></label>\r\n                <div class=\"col-sm-8\">\r\n                    <input type=\"text\" class=\"form-control\"\r\n                           i18n-placeholder=\"@@enterUserNamePlaceholder\"\r\n                           placeholder=\"Enter username.\"\r\n                           formControlName=\"userName\" autocomplete=\"off\">\r\n                    <span class=\"help-block\">\r\n                        {\r\n                        formErrors?.userName,\r\n                        select,\r\n                        required {Please enter username.}\r\n                        maxlength {Username must not exceed 50 characters.}\r\n                        pattern {Username must be number from 0 to 9 or characters from a to z.}\r\n                        }\r\n                    </span>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\"\r\n                 *ngIf=\"!isUpdate\"\r\n                 [class.has-error]=\"formErrors?.password\">\r\n                <label i18n-ghmLabel=\"@@password\"\r\n                       ghmLabel=\"Password\"\r\n                       class=\"col-sm-4 control-label\" [required]=\"true\"></label>\r\n                <div class=\"col-sm-8\">\r\n                    <input type=\"password\" class=\"form-control\"\r\n                           i18n-placeholder=\"@@enterPasswordPlaceholder\"\r\n                           placeholder=\"Enter password\"\r\n                           formControlName=\"password\" autocomplete=\"off\">\r\n                    <span class=\"help-block\">\r\n                        {\r\n                        formErrors?.password,\r\n                        select,\r\n                        required {Please enter password.} maxlength {Password must not exceed 50 characters.}\r\n                        }\r\n                    </span>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\"\r\n                 *ngIf=\"!isUpdate\"\r\n                 [class.has-error]=\"formErrors?.confirmPassword\">\r\n                <label i18n-ghmLabel=\"@@confirmPassword\"\r\n                       ghmLabel=\"Confirm password\"\r\n                       class=\"col-sm-4 control-label\" [required]=\"true\"></label>\r\n                <div class=\"col-sm-8\">\r\n                    <input type=\"password\" class=\"form-control\"\r\n                           i18n-placeholder=\"@@enterConfirmPlaceholder\"\r\n                           placeholder=\"Enter confirm password\"\r\n                           formControlName=\"confirmPassword\" autocomplete=\"off\">\r\n                    <span class=\"help-block\">\r\n                        {\r\n                        formErrors?.confirmPassword,\r\n                        select,\r\n                        required {Please confirm password.} maxlength {Confirm password must not exceed 50 characters.}\r\n                        }\r\n                    </span>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\"\r\n                 [class.has-error]=\"formErrors?.fullName\">\r\n                <label i18n-ghmLabel=\"@@fullName\"\r\n                       ghmLabel=\"Fullname\"\r\n                       class=\"col-sm-4 control-label\" [required]=\"true\"></label>\r\n                <div class=\"col-sm-8\">\r\n                    <input type=\"text\" class=\"form-control\"\r\n                           i18n-placeholder=\"@@enterFullNamePlaceholder\"\r\n                           placeholder=\"Enter fullname\"\r\n                           formControlName=\"fullName\" autocomplete=\"off\">\r\n                    <span class=\"help-block\">\r\n                        {\r\n                        formErrors?.fullName,\r\n                        select,\r\n                        required {Please enter fullname.} maxlength {Fullname must not exceed 50 characters.}\r\n                        }\r\n                    </span>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\"\r\n                 [class.has-error]=\"formErrors?.phoneNumber\">\r\n                <label i18n-ghmLabel=\"@@phoneNumber\"\r\n                       ghmLabel=\"Phone number\"\r\n                       class=\"col-sm-4 control-label\" [required]=\"true\"></label>\r\n                <div class=\"col-sm-8\">\r\n                    <input type=\"text\" class=\"form-control\"\r\n                           i18n-placeholder=\"@@enterPhoneNumberPlaceholder\"\r\n                           placeholder=\"Enter phone number\"\r\n                           formControlName=\"phoneNumber\" autocomplete=\"off\">\r\n                    <span class=\"help-block\">\r\n                        {\r\n                        formErrors?.phoneNumber,\r\n                        select,\r\n                        required {Please enter phone number.}\r\n                        maxlength {Phone number must not exceed 50 characters.}\r\n                        pattern {Invalid phone number.}\r\n                        }\r\n                    </span>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\"\r\n                 [class.has-error]=\"formErrors?.email\">\r\n                <label i18n-ghmLabel=\"@@email\"\r\n                       ghmLabel=\"Email\"\r\n                       class=\"col-sm-4 control-label\"\r\n                       [required]=\"true\"></label>\r\n                <div class=\"col-sm-8\">\r\n                    <input type=\"text\" class=\"form-control\"\r\n                           i18n-placeholder=\"@@enterEmailPlaceholder\"\r\n                           placeholder=\"Enter email.\"\r\n                           formControlName=\"email\" autocomplete=\"off\">\r\n                    <span class=\"help-block\">\r\n                        {\r\n                        formErrors?.email,\r\n                        select,\r\n                        required {Please enter email.}\r\n                        maxlength {Email must not exceed 500 characters.}\r\n                        pattern {Invalid email.}\r\n                        }\r\n                    </span>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <div class=\"col-sm-8 col-sm-offset-4\">\r\n                    <mat-slide-toggle color=\"primary\" formControlName=\"isActive\">\r\n                        {model.value.isActive, select, 0 {InActive} 1 {Active}}\r\n                    </mat-slide-toggle>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer class=\"text-right\">\r\n            <mat-checkbox [checked]=\"isCreateAnother\" (change)=\"isCreateAnother = !isCreateAnother\"\r\n                          *ngIf=\"!isUpdate\"\r\n                          class=\"cm-mgr-5\"\r\n                          color=\"primary\"\r\n                          i18n=\"@@createAnother\">\r\n                Create another\r\n            </mat-checkbox>\r\n            <button class=\"btn btn-primary cm-mgr-5\" i18n=\"@@saveFormButton\">\r\n                {isUpdate, select, 0 {Add} 1 {Save}}\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn default\" nh-dismiss=\"true\" i18n=\"@@close\">\r\n                Close\r\n            </button>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/configs/account/account-form/account-form.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/modules/configs/account/account-form/account-form.component.ts ***!
  \********************************************************************************/
/*! exports provided: AccountFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountFormComponent", function() { return AccountFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _models_account_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/account.model */ "./src/app/modules/configs/account/models/account.model.ts");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _account_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../account.service */ "./src/app/modules/configs/account/account.service.ts");









var AccountFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](AccountFormComponent, _super);
    function AccountFormComponent(fb, toastr, accountService) {
        var _this = _super.call(this) || this;
        _this.fb = fb;
        _this.toastr = toastr;
        _this.accountService = accountService;
        _this.account = new _models_account_model__WEBPACK_IMPORTED_MODULE_3__["Account"]();
        return _this;
    }
    AccountFormComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    AccountFormComponent.prototype.onModalHidden = function () {
        this.model.reset(new _models_account_model__WEBPACK_IMPORTED_MODULE_3__["Account"]());
        this.isUpdate = false;
        if (this.isModified) {
            this.saveSuccessful.emit();
            this.clearFormError(this.formErrors);
        }
    };
    AccountFormComponent.prototype.add = function () {
        this.isUpdate = false;
        this.accountFormModal.open();
    };
    AccountFormComponent.prototype.edit = function (id, account) {
        this.id = id;
        this.account = account;
        this.isUpdate = true;
        this.model.patchValue(account);
        this.accountFormModal.open();
    };
    AccountFormComponent.prototype.save = function () {
        var _this = this;
        if (this.isUpdate) {
            this.model.patchValue({
                password: '1',
                confirmPassword: '1'
            });
        }
        var isValid = this.validateModel();
        if (isValid) {
            this.isSaving = true;
            this.account = this.model.value;
            if (this.isUpdate) {
                this.accountService.update(this.id, this.account)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.toastr.success(result.message);
                    _this.isModified = true;
                    _this.accountFormModal.dismiss();
                });
            }
            else {
                this.accountService.insert(this.account)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.toastr.success(result.message);
                    _this.isModified = true;
                    if (_this.isCreateAnother) {
                        _this.model.reset();
                    }
                    else {
                        _this.accountFormModal.dismiss();
                    }
                });
            }
        }
    };
    AccountFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.renderFormError(['userName', 'fullName', 'email', 'phoneNumber', 'password', 'confirmPassword']);
        this.validationMessages = this.renderFormErrorMessage([
            { 'userName': ['required', 'maxlength', 'pattern'] },
            { 'fullName': ['required', 'maxlength'] },
            { 'email': ['required', 'maxlength', 'pattern'] },
            { 'phoneNumber': ['required', 'maxlength', 'pattern'] },
            { 'password': ['required'] },
            { 'confirmPassword': ['required'] }
        ]);
        this.model = this.fb.group({
            userName: [this.account.userName, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[a-z0-9]+([-_\\.][a-z0-9]+)*[a-z0-9]$')
                ]],
            fullName: [this.account.fullName, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)
                ]],
            email: [this.account.email, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(500),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$')
                ]],
            password: [this.account.password, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)
                ]],
            confirmPassword: [this.account.confirmPassword, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)
                ]],
            phoneNumber: [this.account.phoneNumber, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$')
                ]],
            isActive: [this.account.isActive],
        });
        this.subscribers.modelValueChanges = this.model.valueChanges.subscribe(function () { return _this.validateModel(false); });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('accountFormModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_6__["NhModalComponent"])
    ], AccountFormComponent.prototype, "accountFormModal", void 0);
    AccountFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-account-form',
            template: __webpack_require__(/*! ./account-form.component.html */ "./src/app/modules/configs/account/account-form/account-form.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"],
            _account_service__WEBPACK_IMPORTED_MODULE_8__["AccountService"]])
    ], AccountFormComponent);
    return AccountFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_4__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/configs/account/account.component.html":
/*!****************************************************************!*\
  !*** ./src/app/modules/configs/account/account.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@listAccountTitle\">List account.</span>\r\n    <small i18n=\"@@configModuleTitle\">Configs</small>\r\n</h1>\r\n\r\n<form class=\"form-inline cm-mgb-10\" (ngSubmit)=\"search(1)\">\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <input type=\"text\" class=\"form-control\"\r\n               i18n-placeholder=\"@@enterUserNameOrEmailOrPhoneNumberPlaceholder\"\r\n               placeholder=\"Enter username or email or phone number.\"\r\n               name=\"searchInput\" [(ngModel)]=\"keyword\">\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <nh-select\r\n            [data]=\"[{id: true, name: 'Đã kích hoạt'},{id: false, name: 'Chưa kích hoạt'}]\"\r\n            [title]=\"'-- Tất cả trạng thái --'\"\r\n            [(value)]=\"isActive\"\r\n            (onSelectItem)=\"onStatusSelected($event)\"></nh-select>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <button class=\"btn blue\" type=\"submit\">\r\n            <i class=\"fa fa-search\" *ngIf=\"!isSearching\"></i>\r\n            <i class=\"fa fa-pulse fa-spinner\" *ngIf=\"isSearching\"></i>\r\n        </button>\r\n    </div>\r\n    <div class=\"form-group cm-mgl-5\">\r\n        <button class=\"btn btn-light\" type=\"button\" (click)=\"resetFormSearch()\">\r\n            <i class=\"fa fa-refresh\"></i>\r\n        </button>\r\n    </div>\r\n    <div class=\"form-group cm-mgl-5 pull-right\">\r\n        <button class=\"btn blue cm-mgr-5\" *ngIf=\"permission.add\"\r\n                type=\"button\" (click)=\"add()\" i18n=\"@@add\">\r\n            Add\r\n        </button>\r\n    </div>\r\n</form>\r\n<table class=\"table table-striped table-hover\">\r\n    <thead>\r\n    <tr>\r\n        <th class=\"middle center w50\" i18n=\"@@no\">No</th>\r\n        <th class=\"middle center w150\" i18n=\"@@userName\">Username</th>\r\n        <th class=\"middle w250\" i18n=\"@@fullName\">Fullname</th>\r\n        <th class=\"middle w200\" i18n=\"@@email\">Email</th>\r\n        <th class=\"middle w200\" i18n=\"@@phoneNumber\">Phone number</th>\r\n        <th class=\"middle\" i18n=\"@@isActive\">Is Active</th>\r\n        <th class=\"middle text-right w150\" i18n=\"@@actions\">Actions</th>\r\n    </tr>\r\n    </thead>\r\n    <tbody>\r\n    <tr *ngFor=\"let account of listItems$ | async; let i = index\">\r\n        <td class=\"center middle\">{{ (currentPage - 1) * pageSize + i + 1 }}</td>\r\n        <td class=\"center middle\">{{ account.userName }}</td>\r\n        <td class=\"middle\">{{ account.fullName }}</td>\r\n        <td class=\"middle\">{{ account.email }}</td>\r\n        <td class=\"middle\">{{ account.phoneNumber }}</td>\r\n        <td class=\"middle\">\r\n                <span class=\"badge\" [class.badge-danger]=\"!account.isActive\"\r\n                      [class.badge-success]=\"account.isActive\">\r\n                    {account.isActive, select, 0 {InActive} 1 {Activated}}\r\n                </span>\r\n        </td>\r\n        <td class=\"middle text-right\">\r\n            <button type=\"button\" class=\"btn blue btn-outline btn-sm\" (click)=\"edit(account)\">\r\n                <i class=\"fa fa-edit\"></i>\r\n            </button>\r\n            <button type=\"button\" class=\"btn red btn-outline btn-sm\" [swal]=\"confirmDelete\"\r\n                    (confirm)=\"delete(account.id)\">\r\n                <i class=\"fa fa-trash-o\"></i>\r\n            </button>\r\n        </td>\r\n    </tr>\r\n    </tbody>\r\n</table>\r\n<ghm-paging [totalRows]=\"totalRows\"\r\n            [currentPage]=\"currentPage\"\r\n            [pageShow]=\"6\"\r\n            [isDisabled]=\"isSearching\"\r\n            [pageSize]=\"pageSize\"\r\n            (pageClick)=\"search($event)\"\r\n></ghm-paging>\r\n<swal\r\n    #confirmDelete\r\n    i18n-title=\"@@titleConfirmDeleteAccount\"\r\n    i18n-text=\"@@textConfirmDeleteAccount\"\r\n    title=\"Are you sure want to delete this account?\"\r\n    text=\"Warning: After delete you can not recover this account.\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Accept\"\r\n    cancelButtonText=\"Cancel\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n<app-account-form (saveSuccessful)=\"search(1)\"></app-account-form>\r\n"

/***/ }),

/***/ "./src/app/modules/configs/account/account.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/modules/configs/account/account.component.ts ***!
  \**************************************************************/
/*! exports provided: AccountComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountComponent", function() { return AccountComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _account_form_account_form_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./account-form/account-form.component */ "./src/app/modules/configs/account/account-form/account-form.component.ts");
/* harmony import */ var _models_account_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./models/account.model */ "./src/app/modules/configs/account/models/account.model.ts");
/* harmony import */ var _account_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./account.service */ "./src/app/modules/configs/account/account.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _configs_page_id_config__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../configs/page-id.config */ "./src/app/configs/page-id.config.ts");









var AccountComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](AccountComponent, _super);
    function AccountComponent(pageId, toastr, accountService) {
        var _this = _super.call(this) || this;
        _this.pageId = pageId;
        _this.toastr = toastr;
        _this.accountService = accountService;
        _this.search(1);
        return _this;
    }
    AccountComponent.prototype.ngOnInit = function () {
        this.appService.setupPage(this.pageId.CONFIG, this.pageId.CONFIG_ACCOUNT, 'Quản lý tài khoản', 'Cấu hình');
    };
    AccountComponent.prototype.onStatusSelected = function (status) {
        this.isActive = status ? status.id : null;
        this.search(1);
    };
    AccountComponent.prototype.resetFormSearch = function () {
        this.isActive = null;
        this.keyword = '';
        this.search(1);
    };
    AccountComponent.prototype.search = function (currentPage) {
        var _this = this;
        this.currentPage = currentPage;
        this.isSearching = true;
        this.listItems$ = this.accountService.search(this.keyword, this.isActive, this.currentPage, this.pageSize)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.isSearching = false; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (result) {
            _this.totalRows = result.totalRows;
            return result.items;
        }));
    };
    AccountComponent.prototype.add = function () {
        // console.log(this.accountFormComponent);
        this.accountFormComponent.add();
    };
    AccountComponent.prototype.edit = function (account) {
        this.accountFormComponent.edit(account.id, new _models_account_model__WEBPACK_IMPORTED_MODULE_4__["Account"](account.userName, account.fullName, account.email, account.phoneNumber, account.isActive, account.concurrencyStamp));
    };
    AccountComponent.prototype.delete = function (id) {
        var _this = this;
        this.subscribers.deleteAccount = this.accountService.delete(id)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
            _this.search(1);
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_account_form_account_form_component__WEBPACK_IMPORTED_MODULE_3__["AccountFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _account_form_account_form_component__WEBPACK_IMPORTED_MODULE_3__["AccountFormComponent"])
    ], AccountComponent.prototype, "accountFormComponent", void 0);
    AccountComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-account',
            template: __webpack_require__(/*! ./account.component.html */ "./src/app/modules/configs/account/account.component.html"),
            providers: [_account_service__WEBPACK_IMPORTED_MODULE_5__["AccountService"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_page_id_config__WEBPACK_IMPORTED_MODULE_8__["PAGE_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"],
            _account_service__WEBPACK_IMPORTED_MODULE_5__["AccountService"]])
    ], AccountComponent);
    return AccountComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/configs/account/account.service.ts":
/*!************************************************************!*\
  !*** ./src/app/modules/configs/account/account.service.ts ***!
  \************************************************************/
/*! exports provided: AccountService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountService", function() { return AccountService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");






var AccountService = /** @class */ (function () {
    function AccountService(appConfig, spinnerService, http) {
        this.appConfig = appConfig;
        this.spinnerService = spinnerService;
        this.http = http;
        this.url = 'api/v1/core/accounts';
        this.url = "" + this.appConfig.API_GATEWAY_URL + this.url;
    }
    AccountService.prototype.search = function (keyword, isActive, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        return this.http.get("" + this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('isActive', isActive != null && isActive !== undefined ? isActive.toString() : '')
                .set('page', page ? page.toString() : '')
                .set('pageSize', pageSize ? pageSize.toString() : '')
        });
    };
    AccountService.prototype.insert = function (account) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post("" + this.url, account)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    AccountService.prototype.update = function (id, account) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/" + id, account)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    AccountService.prototype.delete = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.delete(this.url + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    AccountService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_4__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_2__["SpinnerService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], AccountService);
    return AccountService;
}());



/***/ }),

/***/ "./src/app/modules/configs/account/models/account.model.ts":
/*!*****************************************************************!*\
  !*** ./src/app/modules/configs/account/models/account.model.ts ***!
  \*****************************************************************/
/*! exports provided: Account */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Account", function() { return Account; });
var Account = /** @class */ (function () {
    function Account(userName, fullName, email, phoneNumber, isActive, concurrencyStamp) {
        this.userName = userName;
        this.fullName = fullName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.isActive = isActive != null && isActive !== undefined ? isActive : true;
        this.concurrencyStamp = concurrencyStamp;
    }
    return Account;
}());



/***/ }),

/***/ "./src/app/modules/configs/approver/approver.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/modules/configs/approver/approver.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvY29uZmlncy9hcHByb3Zlci9hcHByb3Zlci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/modules/configs/approver/approver.component.html":
/*!******************************************************************!*\
  !*** ./src/app/modules/configs/approver/approver.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@listApproverTitle\">List approver.</span>\r\n    <small i18n=\"@@configModuleTitle\">Configs</small>\r\n</h1>\r\n\r\n<form class=\"form-inline cm-mgb-10\" (ngSubmit)=\"search(1)\">\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <input type=\"text\" class=\"form-control\"\r\n               i18n-placeholder=\"@@enterUserNameOrEmailOrPhoneNumberPlaceholder\"\r\n               placeholder=\"Enter username or email or phone number.\"\r\n               name=\"searchInput\" [(ngModel)]=\"keyword\">\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <nh-select\r\n                i18n-title=\"@@pleaseSelectTypeTitle\"\r\n                title=\"-- Please select type --\"\r\n                [data]=\"approverConfigTypes\"\r\n                (onSelectItem)=\"onSelectApproverConfigType($event, true)\"></nh-select>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <button class=\"btn blue\" type=\"submit\">\r\n            <i class=\"fa fa-search\" *ngIf=\"!isSearching\"></i>\r\n            <i class=\"fa fa-pulse fa-spinner\" *ngIf=\"isSearching\"></i>\r\n        </button>\r\n    </div>\r\n    <div class=\"form-group cm-mgl-5\">\r\n        <button class=\"btn btn-light\" type=\"button\" (click)=\"resetFormSearch()\">\r\n            <i class=\"fa fa-refresh\"></i>\r\n        </button>\r\n    </div>\r\n    <div class=\"form-group cm-mgl-5 pull-right\">\r\n        <button class=\"btn blue cm-mgr-5\" *ngIf=\"permission.add\"\r\n                type=\"button\" (click)=\"add()\" i18n=\"@@add\">\r\n            Add\r\n        </button>\r\n    </div>\r\n</form>\r\n<table class=\"table table-striped table-hover\">\r\n    <thead>\r\n    <tr>\r\n        <th class=\"middle center w50\" i18n=\"@@no\">No</th>\r\n        <th class=\"middle\" i18n=\"@@user\">User</th>\r\n        <th class=\"middle center\" i18n=\"@@type\">Type</th>\r\n        <th class=\"middle center w50\" i18n=\"@@actions\">Actions</th>\r\n    </tr>\r\n    </thead>\r\n    <tbody>\r\n    <tr *ngFor=\"let item of listItems$ | async; let i = index\">\r\n        <td class=\"center middle\">{{ (currentPage - 1) * pageSize + i + 1 }}</td>\r\n        <td class=\"middle\">\r\n            <div class=\"media\">\r\n                <div class=\"media-left\">\r\n                    <a href=\"#\">\r\n                        <img class=\"media-object avatar-md\"\r\n                             ghmImage\r\n                             [src]=\"item.avatar\" alt=\"{{ item.fullName }}\">\r\n                    </a>\r\n                </div>\r\n                <div class=\"media-body\">\r\n                    <h4 class=\"media-heading\">{{ item.fullName }}</h4>\r\n                    <span>{{ item.userName }}</span>\r\n                </div>\r\n            </div>\r\n        </td>\r\n        <td class=\"center middle\">\r\n            <span class=\"badge\"\r\n                  [class.badge-primary]=\"item.type === 0\"\r\n                  [class.badge-red]=\"item.type === 1\"\r\n                  [class.badge-success]=\"item.type === 2\">\r\n                {item.type, select, 0 {News} 1 {Event} 2 {Product}}\r\n            </span>\r\n        </td>\r\n        <td class=\"middle text-right\">\r\n            <button type=\"button\" class=\"btn red btn-outline\"\r\n                    i18n-matTooltip=\"@@delete\"\r\n                    matTooltip=\"Delete\"\r\n                    [swal]=\"confirmDelete\"\r\n                    (confirm)=\"delete(item.userId, item.type)\">\r\n                <i class=\"fa fa-trash-o\"></i>\r\n            </button>\r\n        </td>\r\n    </tr>\r\n    </tbody>\r\n    <tfoot>\r\n    <tr>\r\n        <td></td>\r\n        <td>\r\n            <ghm-user-suggestion\r\n                    (userSelected)=\"onUserSelected($event)\"\r\n            ></ghm-user-suggestion>\r\n        </td>\r\n        <td>\r\n            <nh-select\r\n                    i18n-title=\"@@pleaseSelectTypeTitle\"\r\n                    title=\"-- Please select type --\"\r\n                    [data]=\"approverConfigTypes\"\r\n                    (onSelectItem)=\"onSelectApproverConfigType($event)\"></nh-select>\r\n        </td>\r\n        <td class=\"center\">\r\n            <button type=\"button\" class=\"btn blue\"\r\n                    i18n-matTooltip=\"@@save\"\r\n                    matTooltip=\"Save\"\r\n                    (click)=\"save()\">\r\n                <i class=\"fa fa-save\"></i>\r\n            </button>\r\n        </td>\r\n    </tr>\r\n    </tfoot>\r\n</table>\r\n\r\n<ghm-paging [totalRows]=\"totalRows\"\r\n            [currentPage]=\"currentPage\"\r\n            [pageShow]=\"6\"\r\n            [isDisabled]=\"isSearching\"\r\n            [pageSize]=\"pageSize\"\r\n            (pageClick)=\"search($event)\"\r\n></ghm-paging>\r\n\r\n<swal\r\n        #confirmDelete\r\n        i18n-title=\"@@confirmDeleteApproveConfigTitle\"\r\n        i18n-text=\"@@confirmDeleteApproveConfigText\"\r\n        title=\"Are you sure for delete this approver?\"\r\n        text=\"You can't recover this approver after delete.\"\r\n        type=\"question\"\r\n        i18n-confirmButtonText=\"@@accept\"\r\n        i18n-cancelButtonText=\"@@cancel\"\r\n        confirmButtonText=\"Accept\"\r\n        cancelButtonText=\"Cancel\">\r\n</swal>"

/***/ }),

/***/ "./src/app/modules/configs/approver/approver.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/modules/configs/approver/approver.component.ts ***!
  \****************************************************************/
/*! exports provided: ApproverComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApproverComponent", function() { return ApproverComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _approver_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./approver.service */ "./src/app/modules/configs/approver/approver.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var ApproverComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](ApproverComponent, _super);
    function ApproverComponent(route, toastr, approverService) {
        var _this = _super.call(this) || this;
        _this.route = route;
        _this.toastr = toastr;
        _this.approverService = approverService;
        _this.approverConfigTypes = [];
        _this.hasError = false;
        return _this;
    }
    ApproverComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscribers.getTypes = this.approverService.getTypes()
            .subscribe(function (result) { return _this.approverConfigTypes = result; });
        this.listItems$ = this.route.data.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            var data = result.data;
            if (data) {
                _this.totalRows = data.totalRows;
                return data.items;
            }
        }));
    };
    ApproverComponent.prototype.onSelectApproverConfigType = function (item, isSearch) {
        if (isSearch === void 0) { isSearch = false; }
        if (isSearch) {
            this.typeSearch = item.id;
            this.search(1);
        }
        else {
            this.type = item.id;
        }
    };
    ApproverComponent.prototype.onUserSelected = function (user) {
        this.userId = user.id;
    };
    ApproverComponent.prototype.search = function (currentPage) {
        var _this = this;
        this.currentPage = currentPage;
        this.isSearching = true;
        this.listItems$ = this.approverService.search(this.keyword, this.typeSearch, this.currentPage, this.pageSize)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.isSearching = false; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.totalRows = result.totalRows;
            return result.items;
        }));
    };
    ApproverComponent.prototype.resetFormSearch = function () {
        this.keyword = '';
        this.search(1);
    };
    ApproverComponent.prototype.save = function () {
        var _this = this;
        if (!this.userId || this.type == null || this.type === undefined) {
            this.hasError = true;
        }
        else {
            this.hasError = false;
            this.approverService.insert(this.userId, this.type)
                .subscribe(function (result) {
                _this.toastr.show(result.message);
                _this.search(1);
            });
        }
    };
    ApproverComponent.prototype.delete = function (userId, type) {
        var _this = this;
        this.approverService.delete(userId, type)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
        });
    };
    ApproverComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-approver',
            template: __webpack_require__(/*! ./approver.component.html */ "./src/app/modules/configs/approver/approver.component.html"),
            styles: [__webpack_require__(/*! ./approver.component.css */ "./src/app/modules/configs/approver/approver.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            _approver_service__WEBPACK_IMPORTED_MODULE_2__["ApproverService"]])
    ], ApproverComponent);
    return ApproverComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_4__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/configs/approver/approver.service.ts":
/*!**************************************************************!*\
  !*** ./src/app/modules/configs/approver/approver.service.ts ***!
  \**************************************************************/
/*! exports provided: ApproverService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApproverService", function() { return ApproverService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");







var ApproverService = /** @class */ (function () {
    function ApproverService(appConfig, spinnerService, http) {
        this.appConfig = appConfig;
        this.spinnerService = spinnerService;
        this.http = http;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].apiGatewayUrl + "api/v1/core/approver-configs";
    }
    ApproverService.prototype.resolve = function (route, state) {
        var params = route.queryParams;
        return this.search(params.keyword, params.isActive);
    };
    ApproverService.prototype.search = function (keyword, type, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 20; }
        return this.http.get("" + this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('type', type != null && type !== undefined ? type.toString() : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString())
        });
    };
    ApproverService.prototype.insert = function (userId, type) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .post(this.url + "/" + userId + "/" + type, {})
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    ApproverService.prototype.delete = function (userId, type) {
        var _this = this;
        this.spinnerService.show();
        return this.http.delete(this.url + "/" + userId + "/" + type)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    ApproverService.prototype.getTypes = function () {
        return this.http.get(this.url + "/types");
    };
    ApproverService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_3__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__["SpinnerService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ApproverService);
    return ApproverService;
}());



/***/ }),

/***/ "./src/app/modules/configs/client/client-form/client-form.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/modules/configs/client/client-form/client-form.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"portlet light\">\r\n    <div class=\"portlet-title\">\r\n        <div class=\"caption font-green-sharp\">\r\n            <i class=\"icon-share font-green-sharp\"></i>\r\n            <span class=\"caption-subject bold uppercase\">\r\n                {isUpdate, select, 0 {Thêm mới} 1 {Cập nhật}}\r\n            </span>\r\n        </div>\r\n    </div>\r\n    <form class=\"horizontal-form\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <div class=\"portlet-body form\">\r\n            <div class=\"form-body\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-6\">\r\n                        <div class=\"form-group\">\r\n                            <label ghmLabel=\"Client ID\" class=\"control-label\"></label>\r\n                            <div type=\"text\" value=\"\"\r\n                                 class=\"form-control disabled\"\r\n                                 placeholder=\"Nhập mã trang\">\r\n                                {{model.value.clientId}}\r\n                            </div>\r\n                        </div><!-- END: clientId -->\r\n                        <div class=\"form-group\">\r\n                            <label ghmLabel=\"Client Name\" class=\"control-label\" [required]=\"true\"></label>\r\n                            <input type=\"text\" class=\"form-control\" placeholder=\"Enter client name\"\r\n                                   formControlName=\"clientName\">\r\n                        </div><!-- END: clientName -->\r\n                        <div class=\"form-group\">\r\n                            <label ghmLabel=\"Refresh token usage\" class=\"control-label\"></label>\r\n                            <nh-select\r\n                                title=\"-- Select refresh token usage --\"\r\n                                [data]=\"[{id: 0, name: 'Reuse'}, {id: 1, name: 'One time only'}]\"\r\n                                formControlName=\"refreshTokenUsage\"\r\n                            ></nh-select>\r\n                        </div><!-- END: Refresh token usage -->\r\n                        <div class=\"form-group\">\r\n                            <label ghmLabel=\"Refresh Token Expiration\" class=\"control-label\"></label>\r\n                            <nh-select\r\n                                title=\"-- Select refresh token expiration --\"\r\n                                [data]=\"[{id: 0, name: 'Sliding'}, {id: 1, name: 'Absolute'}]\"\r\n                                formControlName=\"refreshTokenExpiration\"\r\n                            ></nh-select>\r\n                        </div><!-- END: Refresh Token Expiration -->\r\n                        <div class=\"form-group\">\r\n                            <label ghmLabel=\"Allowed Grant Types\" class=\"control-label\"></label>\r\n                            <nh-select\r\n                                title=\"-- Select grant types --\"\r\n                                [data]=\"listGrantTypes\"\r\n                                formControlName=\"clientAllowedGrantTypes\"\r\n                            ></nh-select>\r\n                        </div><!-- END: Allowed Grant Types -->\r\n                        <div class=\"form-group\">\r\n                            <label ghmLabel=\"Allowed Scopes\" class=\"control-label\"></label>\r\n                            <textarea type=\"text\" class=\"form-control\" rows=\"3\"\r\n                                      placeholder=\"Please enter allowed scope separate by comma.\"\r\n                                      formControlName=\"clientAllowedScopes\"></textarea>\r\n                        </div><!-- END: Allowed Scopes -->\r\n                        <div class=\"form-group\">\r\n                            <label ghmLabel=\"Allowed Cors Origins\" class=\"control-label\"></label>\r\n                            <textarea class=\"form-control\"\r\n                                      rows=\"3\"\r\n                                      placeholder=\"Please enter allowed cors origin separate by comma.\"\r\n                                      formControlName=\"clientAllowedCorsOrigins\"></textarea>\r\n                        </div><!-- END: Refresh Token Expiration -->\r\n                    </div><!-- END: first-col -->\r\n                    <div class=\"col-sm-6\">\r\n                        <div class=\"form-group\">\r\n                            <mat-slide-toggle formControlName=\"enabled\" color=\"primary\"> Enabled</mat-slide-toggle>\r\n                        </div>\r\n                        <div class=\"form-group\">\r\n                            <mat-slide-toggle formControlName=\"allowOfflineAccess\" color=\"primary\"> Allowed Offline\r\n                                Access\r\n                            </mat-slide-toggle>\r\n                        </div><!-- END: Allowed Offline Access -->\r\n                        <div class=\"form-group\">\r\n                            <mat-slide-toggle formControlName=\"requireClientSecret\" color=\"primary\"> Require Client\r\n                                Secret\r\n                            </mat-slide-toggle>\r\n                        </div>\r\n                        <div class=\"form-group\">\r\n                            <label ghmLabel=\"Client Secret\" class=\"control-label\"\r\n                                   [required]=\"model.value.requireClientSecret\"></label>\r\n                            <div>\r\n                                <div class=\"input-group\">\r\n                                    <input [attr.type]=\"isShowSecret ? 'text' : 'password'\" class=\"form-control\"\r\n                                           placeholder=\"Enter secret\"\r\n                                           formControlName=\"clientSecret\">\r\n                                    <span class=\"input-group-btn\">\r\n                                        <button class=\"btn btn-primary\" type=\"button\" matTooltip=\"Show secret\"\r\n                                                matTooltipPosition=\"above\" (click)=\"toggleShowSecret()\">\r\n                                            <!--<i class=\"flaticon-medical\"></i>-->\r\n                                            <i class=\"fa fa-eye\" *ngIf=\"isShowSecret\"></i>\r\n                                            <i class=\"fa fa-eye-slash\" *ngIf=\"!isShowSecret\"></i>\r\n                                        </button>\r\n                                    </span>\r\n                                </div><!-- /input-group -->\r\n                            </div>\r\n                        </div><!-- END: clientSecret -->\r\n                        <div class=\"form-group\">\r\n                            <label ghmLabel=\"Absolute Refresh Token Lifetime\" class=\"control-label\"></label>\r\n                            <div class=\"input-group\">\r\n                                <div class=\"input-group\">\r\n                                    <div class=\"input-group\">\r\n                                        <input type=\"text\" class=\"form-control\"\r\n                                               placeholder=\"Enter absolute refresh token lifetime\"\r\n                                               formControlName=\"absoluteRefreshTokenLifetime\">\r\n                                        <div class=\"input-group-append\">\r\n                                            <span class=\"input-group-text\"\r\n                                                  id=\"absoluteRefreshTokenLifetime\">seconds</span>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <span class=\"help-block\">\r\n                                Maximum lifetime of a refresh token in seconds. Defaults to 2592000 seconds / 30 days\r\n                            </span>\r\n                        </div><!-- END: AbsoluteRefreshTokenLifetime -->\r\n                        <div class=\"form-group\">\r\n                            <label ghmLabel=\"Sliding Refresh Token Lifetime\" class=\"control-label\"></label>\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" class=\"form-control\"\r\n                                       placeholder=\"Enter sliding refresh token lifetime\"\r\n                                       formControlName=\"slidingRefreshTokenLifetime\">\r\n                                <span class=\"input-group-addon\" id=\"basic-addon2\">\r\n                                     <span class=\"input-group-text\"\r\n                                           id=\"slidingRefreshTokenLifeTime\">seconds</span>\r\n                                </span>\r\n                            </div>\r\n                            <span class=\"help-block\">\r\n                                Sliding lifetime of a refresh token in seconds. Defaults to 1296000 seconds / 15 days\r\n                            </span>\r\n                        </div><!-- END: SlidingRefreshTokenLifetime -->\r\n                    </div><!-- END: second-col -->\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"portlet-foot\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-12\">\r\n                    <button mat-raised-button color=\"primary\" class=\"cm-mgr-5\">\r\n                        Lưu\r\n                    </button>\r\n                    <button type=\"button\" mat-raised-button mat-dialog-close>\r\n                        Hủy bỏ\r\n                    </button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </form>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/configs/client/client-form/client-form.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/modules/configs/client/client-form/client-form.component.ts ***!
  \*****************************************************************************/
/*! exports provided: ClientFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientFormComponent", function() { return ClientFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _client_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../client.model */ "./src/app/modules/configs/client/client.model.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _client_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../client.service */ "./src/app/modules/configs/client/client.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");












var ClientFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](ClientFormComponent, _super);
    function ClientFormComponent(data, router, route, fb, toastr, utilService, spinnerService, clientService) {
        var _this = _super.call(this) || this;
        _this.data = data;
        _this.router = router;
        _this.route = route;
        _this.fb = fb;
        _this.toastr = toastr;
        _this.utilService = utilService;
        _this.spinnerService = spinnerService;
        _this.clientService = clientService;
        _this.client = new _client_model__WEBPACK_IMPORTED_MODULE_7__["Client"]();
        _this.isShowSecret = false;
        _this.listGrantTypes = [];
        _this.listPostRedirectLogoutUris = [];
        _this.listIdentityProviderRestrictions = [];
        _this.clientData = _this.data.client;
        return _this;
    }
    ClientFormComponent.prototype.ngOnInit = function () {
        this.renderListGrantTypes();
        this.buildForm();
        if (this.clientData) {
            this.isUpdate = true;
            this.model.patchValue(this.clientData);
        }
        else {
            this.isUpdate = false;
        }
        // this.subscribers.queryParams = this.route.queryParams.subscribe(params => {
        //     if (params.clientId) {
        //         this.isUpdate = true;
        //         this.clientService.getDetail(params.clientId)
        //             .subscribe(client => {
        //                 this.model.patchValue(client);
        //             });
        //     } else {
        //         this.isUpdate = false;
        //         this.getClientId();
        //     }
        // });
    };
    ClientFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            this.client = this.model.value;
            this.spinnerService.show('Đang lưu thông tin client. Vui lòng đợi...');
            if (this.isUpdate) {
                this.clientService.update(this.client.clientId, this.client)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () { return _this.spinnerService.hide(); }))
                    .subscribe(function (result) {
                    _this.toastr.show(result.message, result.title);
                });
            }
            else {
                this.clientService.insert(this.client)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () { return _this.spinnerService.hide(); }))
                    .subscribe(function (result) {
                    _this.toastr.show(result.message, result.title);
                    _this.model.reset(new _client_model__WEBPACK_IMPORTED_MODULE_7__["Client"]());
                    _this.getClientId();
                });
            }
        }
    };
    ClientFormComponent.prototype.toggleShowSecret = function () {
        this.isShowSecret = !this.isShowSecret;
    };
    ClientFormComponent.prototype.getClientId = function () {
        var _this = this;
        this.spinnerService.show('Đang tạo mã Client. Vui lòng đợi...');
        this.clientService.getCientId()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () { return _this.spinnerService.hide(); }))
            .subscribe(function (clientId) { return _this.model.patchValue({ clientId: clientId }); });
    };
    ClientFormComponent.prototype.renderListGrantTypes = function () {
        this.listGrantTypes = [
            { id: 'Implicit', name: 'Implicit' },
            { id: 'ImplicitAndClientCredentials', name: 'ImplicitAndClientCredentials' },
            { id: 'Code', name: 'Code' },
            { id: 'CodeAndClientCredentials', name: 'CodeAndClientCredentials' },
            { id: 'Hybrid', name: 'Hybrid' },
            { id: 'HybridAndClientCredentials', name: 'HybridAndClientCredentials' },
            { id: 'ClientCredentials', name: 'ClientCredentials' },
            { id: 'ResourceOwnerPassword', name: 'ResourceOwnerPassword' },
            { id: 'ResourceOwnerPasswordAndClientCredentials', name: 'ResourceOwnerPasswordAndClientCredentials' },
        ];
    };
    ClientFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.utilService.renderFormError(['']);
        this.validationMessages = {};
        this.model = this.fb.group({
            'clientId': [this.client.clientId],
            'clientName': [this.client.clientName, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(450)
                ]],
            'absoluteRefreshTokenLifetime': [this.client.absoluteRefreshTokenLifetime],
            'accessTokenLifetime': [this.client.accessTokenLifetime],
            'accessTokenType': [this.client.accessTokenType],
            'allowAccessTokensViaBrowser': [this.client.allowAccessTokensViaBrowser],
            'allowOfflineAccess': [this.client.allowOfflineAccess],
            'allowPlainTextPkce': [this.client.allowPlainTextPkce],
            'allowRememberConsent': [this.client.allowRememberConsent],
            'alwaysIncludeUserClaimsInIdToken': [this.client.alwaysIncludeUserClaimsInIdToken],
            'alwaysSendClientClaims': [this.client.alwaysSendClientClaims],
            'authorizationCodeLifetime': [this.client.authorizationCodeLifetime],
            'backChannelLogoutSessionRequired': [this.client.backChannelLogoutSessionRequired],
            'backChannelLogoutUri': [this.client.backChannelLogoutUri],
            'clientAllowedGrantTypes': [this.client.clientAllowedGrantTypes, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
                ]],
            'clientClaimsPrefix': [this.client.clientClaimsPrefix],
            'clientUri': [this.client.clientUri],
            'consentLifetime': [this.client.consentLifetime],
            'enableLocalLogin': [this.client.enableLocalLogin],
            'enabled': [this.client.enabled],
            'frontChannelLogoutSessionRequired': [this.client.frontChannelLogoutSessionRequired],
            'frontChannelLogoutUri': [this.client.frontChannelLogoutUri],
            'identityTokenLifetime': [this.client.identityTokenLifetime],
            'includeJwtId': [this.client.includeJwtId],
            'logoUri': [this.client.logoUri],
            'pairWiseSubjectSalt': [this.client.pairWiseSubjectSalt],
            'protocolType': [this.client.protocolType],
            'refreshTokenExpiration': [this.client.refreshTokenExpiration],
            'refreshTokenUsage': [this.client.refreshTokenUsage],
            'requireClientSecret': [this.client.requireClientSecret],
            'requireConsent': [this.client.requireConsent],
            'requirePkce': [this.client.requirePkce],
            'slidingRefreshTokenLifetime': [this.client.slidingRefreshTokenLifetime],
            'updateAccessTokenClaimsOnRefresh': [this.client.updateAccessTokenClaimsOnRefresh],
            'clientAllowedScopes': [this.client.clientAllowedScopes],
            'clientAllowedCorsOrigins': [this.client.clientAllowedCorsOrigins],
            'clientSecret': [this.client.clientSecret],
            'userCodeType': [this.client.userCodeType],
            'deviceCodeLifetime': [this.client.deviceCodeLifetime]
        });
        this.model.valueChanges.subscribe(function () { return _this.utilService.onValueChanged(_this.model, _this.formErrors, _this.validationMessages); });
    };
    ClientFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-client-form',
            template: __webpack_require__(/*! ./client-form.component.html */ "./src/app/modules/configs/client/client-form/client-form.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_11__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_8__["UtilService"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_9__["SpinnerService"],
            _client_service__WEBPACK_IMPORTED_MODULE_10__["ClientService"]])
    ], ClientFormComponent);
    return ClientFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_6__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/configs/client/client.component.html":
/*!**************************************************************!*\
  !*** ./src/app/modules/configs/client/client.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"m-portlet\">\r\n    <div class=\"m-portlet__head\">\r\n        <div class=\"m-portlet__head-caption\">\r\n            <div class=\"m-portlet__head-title\">\r\n\t\t\t\t\t\t\t<span class=\"m-portlet__head-icon m--hide\">\r\n\t\t\t\t\t\t\t\t<i class=\"flaticon-computer\"></i>\r\n\t\t\t\t\t\t\t</span>\r\n                <h3 class=\"m-portlet__head-text\">\r\n                    Danh sách Client.\r\n                </h3>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"m-portlet__body\">\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-12\">\r\n                <form class=\"form-inline\" (ngSubmit)=\"search(1)\">\r\n                    <div class=\"form-group\">\r\n                        <input class=\"form-control\" placeholder=\"Nhập từ khoá tìm kiếm\"\r\n                               (keyup)=\"keyword = searchBox.value\" #searchBox>\r\n                    </div>\r\n                    <!--<div class=\"form-group\">-->\r\n                    <!--<nh-select-->\r\n                    <!--[data]=\"[{id: 1, name: 'Kích hoạt'}, {id: 0, name: 'Chưa kích hoạt'}]\"-->\r\n                    <!--[title]=\"'&#45;&#45; Tất cả trạng thái &#45;&#45;'\"-->\r\n                    <!--[(value)]=\"moduleIdSearch\"-->\r\n                    <!--(onSelectItem)=\"search(1)\"></nh-select>-->\r\n                    <!--</div>-->\r\n                    <div class=\"form-group\">\r\n                        <button mat-raised-button color=\"primary\" type=\"submit\">\r\n                            <i class=\"fa fa-search\"></i>\r\n                            Tìm kiếm\r\n                        </button>\r\n                    </div>\r\n                    <div class=\"form-group pull-right\">\r\n                        <button type=\"button\" mat-raised-button color=\"primary\" (click)=\"addNew()\">\r\n                            <i class=\"fa fa-plus\"></i>\r\n                            Thêm mới\r\n                        </button>\r\n                    </div>\r\n                </form>\r\n            </div>\r\n        </div>\r\n        <hr/>\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-12\">\r\n                <div class=\"table-responsive\">\r\n                    <table\r\n                        class=\"table table-striped table-bordered table-hover table-full-width dataTable no-footer table-main\">\r\n                        <thead>\r\n                        <tr>\r\n                            <th class=\"center w50\">STT</th>\r\n                            <th class=\"center\">Mã Client</th>\r\n                            <th class=\"center\">Tên Client</th>\r\n                            <th class=\"center\">Grant type</th>\r\n                            <th class=\"center\">Enable</th>\r\n                            <th class=\"center w150\" *ngIf=\"permission.edit || permission.delete\">Sửa, Xóa</th>\r\n                        </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                        <tr *ngFor=\"let client of listItems$ | async; let i = index\">\r\n                            <td class=\"center middle\">{{(currentPage - 1) * pageSize + i + 1}}</td>\r\n                            <td class=\"middle\">{{ client.clientId }}</td>\r\n                            <td class=\"middle\">{{ client.clientName }}</td>\r\n                            <td class=\"middle\">{{ client.clientAllowedGrantTypes }}</td>\r\n                            <td class=\"w100 center middle\">\r\n                                <mat-slide-toggle color=\"primary\" [checked]=\"client.enabled\"></mat-slide-toggle>\r\n                            </td>\r\n                            <td class=\"center middle w100\" *ngIf=\"permission.edit || permission.delete\">\r\n                                <a *ngIf=\"permission.edit\" mat-mini-fab color=\"primary\" href=\"javascript://\"\r\n                                   (click)=\"edit(client)\">\r\n                                    <i class=\"fa fa-pencil\"></i>\r\n                                </a>\r\n                                <a *ngIf=\"permission.delete\" mat-mini-fab color=\"warn\"\r\n                                   class=\"btn btn-danger btn-sm\" (click)=\"delete(page)\">\r\n                                    <i class=\"fa fa-trash-o\"></i>\r\n                                </a>\r\n                            </td>\r\n                        </tr>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <ghm-paging [totalRows]=\"totalRows\" [currentPage]=\"currentPage\" [pageShow]=\"6\" (pageClick)=\"search($event)\"\r\n                    [isDisabled]=\"isSearching\" [pageName]=\"'Client'\"></ghm-paging>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/configs/client/client.component.ts":
/*!************************************************************!*\
  !*** ./src/app/modules/configs/client/client.component.ts ***!
  \************************************************************/
/*! exports provided: ClientComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientComponent", function() { return ClientComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _configs_page_id_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../configs/page-id.config */ "./src/app/configs/page-id.config.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _client_form_client_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./client-form/client-form.component */ "./src/app/modules/configs/client/client-form/client-form.component.ts");
/* harmony import */ var _client_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./client.service */ "./src/app/modules/configs/client/client.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");









var ClientComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](ClientComponent, _super);
    function ClientComponent(pageId, route, dialog, clientService) {
        var _this = _super.call(this) || this;
        _this.route = route;
        _this.dialog = dialog;
        _this.clientService = clientService;
        _this.listItems$ = _this.route.data.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (result) {
            console.log(result.data);
            var data = result.data;
            _this.totalRows = data.totalRows;
            return data.items;
        }));
        _this.appService.setupPage(pageId.CONFIG, pageId.CONFIG_CLIENT, 'Cấu hình', 'Cấu hình');
        return _this;
    }
    ClientComponent.prototype.ngOnInit = function () {
    };
    ClientComponent.prototype.search = function (currentPage) {
        var _this = this;
        this.currentPage = currentPage;
        this.listItems$ = this.clientService.search(this.keyword, this.isEnable, this.currentPage, this.pageSize)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (result) {
            _this.totalRows = result.totalRows;
            return result.items;
        }));
    };
    ClientComponent.prototype.addNew = function () {
        this.dialog.open(_client_form_client_form_component__WEBPACK_IMPORTED_MODULE_5__["ClientFormComponent"], { data: {} });
    };
    ClientComponent.prototype.edit = function (client) {
        this.dialog.open(_client_form_client_form_component__WEBPACK_IMPORTED_MODULE_5__["ClientFormComponent"], { data: { client: client } });
    };
    ClientComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-client',
            template: __webpack_require__(/*! ./client.component.html */ "./src/app/modules/configs/client/client.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_page_id_config__WEBPACK_IMPORTED_MODULE_3__["PAGE_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialog"],
            _client_service__WEBPACK_IMPORTED_MODULE_6__["ClientService"]])
    ], ClientComponent);
    return ClientComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/configs/client/client.model.ts":
/*!********************************************************!*\
  !*** ./src/app/modules/configs/client/client.model.ts ***!
  \********************************************************/
/*! exports provided: Client */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Client", function() { return Client; });
var Client = /** @class */ (function () {
    function Client() {
        this.ACCESS_TOKEN_TYPE = {
            jwt: 0,
            reference: 1
        };
        this.TOKEN_USAGE = {
            reuse: 0,
            oneTimeOnly: 1
        };
        this.TOKEN_EXPIRATION = {
            sliding: 0,
            absolute: 1
        };
        this.enabled = true;
        this.requireConsent = false;
        this.requirePkce = false;
        this.requireClientSecret = false;
        this.allowPlainTextPkce = false;
        this.allowOfflineAccess = true;
        this.allowAccessTokensViaBrowser = false;
        this.frontChannelLogoutSessionRequired = true;
        this.backChannelLogoutSessionRequired = true;
        this.enableLocalLogin = false;
        // Token
        this.identityTokenLifetime = 300;
        this.accessTokenLifetime = 3600;
        this.authorizationCodeLifetime = 300;
        this.absoluteRefreshTokenLifetime = 2592000;
        this.slidingRefreshTokenLifetime = 1296000;
        this.refreshTokenUsage = this.TOKEN_USAGE.oneTimeOnly;
        this.refreshTokenExpiration = this.TOKEN_EXPIRATION.absolute;
        this.updateAccessTokenClaimsOnRefresh = true;
        this.accessTokenType = this.ACCESS_TOKEN_TYPE.jwt;
        this.includeJwtId = false;
        this.clientClaimsPrefix = 'ghm_client';
        this.requireConsent = false;
        this.allowRememberConsent = true;
        this.alwaysIncludeUserClaimsInIdToken = false;
        this.alwaysSendClientClaims = false;
    }
    return Client;
}());



/***/ }),

/***/ "./src/app/modules/configs/client/client.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/modules/configs/client/client.service.ts ***!
  \**********************************************************/
/*! exports provided: ClientService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientService", function() { return ClientService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");





var ClientService = /** @class */ (function () {
    function ClientService(appConfig, http) {
        this.appConfig = appConfig;
        this.http = http;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiGatewayUrl + "api/v1/core/clients";
    }
    ClientService.prototype.resolve = function (route, state) {
        var queryParams = route.queryParams;
        var keyword = queryParams.keyword;
        var enabled = queryParams.enabled;
        var page = queryParams.page;
        var pageSize = queryParams.pageSize;
        return this.search(keyword, enabled, page, pageSize);
    };
    ClientService.prototype.search = function (keyword, enabled, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 20; }
        return this.http.get("" + this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('enabled', enabled != null && enabled !== undefined ? enabled.toString() : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString())
        });
    };
    ClientService.prototype.getCientId = function () {
        return this.http.get(this.url + "/get-client-id");
    };
    ClientService.prototype.getDetail = function (clientId) {
        return this.http.get(this.url + "/get-detail", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
                .set('clientId', clientId)
        });
    };
    ClientService.prototype.insert = function (client) {
        return this.http.post("" + this.url, client);
    };
    ClientService.prototype.update = function (id, client) {
        return this.http.post(this.url + "/" + id, client);
    };
    ClientService.prototype.delete = function (clientId) {
        return this.http.delete("" + this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
                .set('clientId', clientId)
        });
    };
    ClientService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], ClientService);
    return ClientService;
}());



/***/ }),

/***/ "./src/app/modules/configs/config-routing.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/modules/configs/config-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: ConfigRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigRoutingModule", function() { return ConfigRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _page_page_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./page/page.component */ "./src/app/modules/configs/page/page.component.ts");
/* harmony import */ var _client_client_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./client/client.component */ "./src/app/modules/configs/client/client.component.ts");
/* harmony import */ var _client_client_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./client/client.service */ "./src/app/modules/configs/client/client.service.ts");
/* harmony import */ var _tenant_tenant_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tenant/tenant.component */ "./src/app/modules/configs/tenant/tenant.component.ts");
/* harmony import */ var _tenant_tenant_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./tenant/tenant.service */ "./src/app/modules/configs/tenant/tenant.service.ts");
/* harmony import */ var _role_role_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./role/role.component */ "./src/app/modules/configs/role/role.component.ts");
/* harmony import */ var _language_language_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./language/language.component */ "./src/app/modules/configs/language/language.component.ts");
/* harmony import */ var _page_page_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./page/page.service */ "./src/app/modules/configs/page/page.service.ts");
/* harmony import */ var _role_role_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./role/role.service */ "./src/app/modules/configs/role/role.service.ts");
/* harmony import */ var _user_setting_user_setting_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./user-setting/user-setting.component */ "./src/app/modules/configs/user-setting/user-setting.component.ts");
/* harmony import */ var _role_role_form_role_form_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./role/role-form/role-form.component */ "./src/app/modules/configs/role/role-form/role-form.component.ts");
/* harmony import */ var _system_system_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./system/system.component */ "./src/app/modules/configs/system/system.component.ts");















var routes = [
    {
        path: '',
        component: _role_role_component__WEBPACK_IMPORTED_MODULE_8__["RoleComponent"]
    },
    {
        path: 'tenants',
        component: _tenant_tenant_component__WEBPACK_IMPORTED_MODULE_6__["TenantComponent"],
    },
    {
        path: 'pages',
        component: _page_page_component__WEBPACK_IMPORTED_MODULE_3__["PageComponent"],
        resolve: {
            data: _page_page_service__WEBPACK_IMPORTED_MODULE_10__["PageService"]
        }
    },
    {
        path: 'clients',
        resolve: {
            data: _client_client_service__WEBPACK_IMPORTED_MODULE_5__["ClientService"]
        },
        component: _client_client_component__WEBPACK_IMPORTED_MODULE_4__["ClientComponent"]
    },
    {
        path: 'roles',
        resolve: {
            data: _role_role_service__WEBPACK_IMPORTED_MODULE_11__["RoleService"]
        },
        component: _role_role_component__WEBPACK_IMPORTED_MODULE_8__["RoleComponent"]
    },
    {
        path: 'roles/add',
        component: _role_role_form_role_form_component__WEBPACK_IMPORTED_MODULE_13__["RoleFormComponent"]
    },
    {
        path: 'roles/:id',
        component: _role_role_form_role_form_component__WEBPACK_IMPORTED_MODULE_13__["RoleFormComponent"]
    },
    {
        path: 'languages',
        component: _language_language_component__WEBPACK_IMPORTED_MODULE_9__["LanguageComponent"]
    },
    {
        path: 'settings',
        component: _user_setting_user_setting_component__WEBPACK_IMPORTED_MODULE_12__["UserSettingComponent"]
    },
    {
        path: 'systems',
        component: _system_system_component__WEBPACK_IMPORTED_MODULE_14__["SystemComponent"]
    }
];
var ConfigRoutingModule = /** @class */ (function () {
    function ConfigRoutingModule() {
    }
    ConfigRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
            providers: [_client_client_service__WEBPACK_IMPORTED_MODULE_5__["ClientService"], _tenant_tenant_service__WEBPACK_IMPORTED_MODULE_7__["TenantService"], _page_page_service__WEBPACK_IMPORTED_MODULE_10__["PageService"], _role_role_service__WEBPACK_IMPORTED_MODULE_11__["RoleService"]]
        })
    ], ConfigRoutingModule);
    return ConfigRoutingModule;
}());



/***/ }),

/***/ "./src/app/modules/configs/config.component.html":
/*!*******************************************************!*\
  !*** ./src/app/modules/configs/config.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "Hello from config component\r\n"

/***/ }),

/***/ "./src/app/modules/configs/config.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/modules/configs/config.component.ts ***!
  \*****************************************************/
/*! exports provided: ConfigComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigComponent", function() { return ConfigComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shareds_services_app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shareds/services/app.service */ "./src/app/shareds/services/app.service.ts");
/* harmony import */ var _configs_page_id_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../configs/page-id.config */ "./src/app/configs/page-id.config.ts");




var ConfigComponent = /** @class */ (function () {
    function ConfigComponent(pageId, appService) {
        this.appService = appService;
        this.appService.setupPage(pageId.CONFIG, pageId.CONFIG, 'Cấu hình', 'Quản lý cấu hình');
    }
    ConfigComponent.prototype.ngOnInit = function () {
    };
    ConfigComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-config',
            template: __webpack_require__(/*! ./config.component.html */ "./src/app/modules/configs/config.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_page_id_config__WEBPACK_IMPORTED_MODULE_3__["PAGE_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _shareds_services_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"]])
    ], ConfigComponent);
    return ConfigComponent;
}());



/***/ }),

/***/ "./src/app/modules/configs/config.module.ts":
/*!**************************************************!*\
  !*** ./src/app/modules/configs/config.module.ts ***!
  \**************************************************/
/*! exports provided: ConfigModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigModule", function() { return ConfigModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _page_page_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./page/page.component */ "./src/app/modules/configs/page/page.component.ts");
/* harmony import */ var _config_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./config-routing.module */ "./src/app/modules/configs/config-routing.module.ts");
/* harmony import */ var _shareds_components_nh_select_nh_select_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shareds/components/nh-select/nh-select.module */ "./src/app/shareds/components/nh-select/nh-select.module.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../shareds/components/nh-modal/nh-modal.module */ "./src/app/shareds/components/nh-modal/nh-modal.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shareds_components_ghm_paging_ghm_paging_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../shareds/components/ghm-paging/ghm-paging.module */ "./src/app/shareds/components/ghm-paging/ghm-paging.module.ts");
/* harmony import */ var _page_page_form_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./page/page-form.component */ "./src/app/modules/configs/page/page-form.component.ts");
/* harmony import */ var _client_client_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./client/client.component */ "./src/app/modules/configs/client/client.component.ts");
/* harmony import */ var _shareds_layouts_layout_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../shareds/layouts/layout.module */ "./src/app/shareds/layouts/layout.module.ts");
/* harmony import */ var _config_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./config.component */ "./src/app/modules/configs/config.component.ts");
/* harmony import */ var _tenant_tenant_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./tenant/tenant.component */ "./src/app/modules/configs/tenant/tenant.component.ts");
/* harmony import */ var _tenant_tenant_form_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./tenant/tenant-form.component */ "./src/app/modules/configs/tenant/tenant-form.component.ts");
/* harmony import */ var _role_role_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./role/role.component */ "./src/app/modules/configs/role/role.component.ts");
/* harmony import */ var _role_role_form_role_form_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./role/role-form/role-form.component */ "./src/app/modules/configs/role/role-form/role-form.component.ts");
/* harmony import */ var _role_role_detail_role_detail_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./role/role-detail/role-detail.component */ "./src/app/modules/configs/role/role-detail/role-detail.component.ts");
/* harmony import */ var _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @toverux/ngx-sweetalert2 */ "./node_modules/@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
/* harmony import */ var _shareds_components_nh_tree_nh_tree_module__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../../shareds/components/nh-tree/nh-tree.module */ "./src/app/shareds/components/nh-tree/nh-tree.module.ts");
/* harmony import */ var _user_setting_user_setting_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./user-setting/user-setting.component */ "./src/app/modules/configs/user-setting/user-setting.component.ts");
/* harmony import */ var _shareds_components_nh_image_nh_image_module__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../../shareds/components/nh-image/nh-image.module */ "./src/app/shareds/components/nh-image/nh-image.module.ts");
/* harmony import */ var _shareds_components_ghm_select_picker_ghm_select_picker_module__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../../shareds/components/ghm-select-picker/ghm-select-picker.module */ "./src/app/shareds/components/ghm-select-picker/ghm-select-picker.module.ts");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../../core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _shareds_components_nh_user_picker_nh_user_picker_module__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ../../shareds/components/nh-user-picker/nh-user-picker.module */ "./src/app/shareds/components/nh-user-picker/nh-user-picker.module.ts");
/* harmony import */ var _shareds_components_ghm_file_explorer_ghm_file_explorer_module__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ../../shareds/components/ghm-file-explorer/ghm-file-explorer.module */ "./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.module.ts");
/* harmony import */ var _shareds_pipe_datetime_format_datetime_format_module__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ../../shareds/pipe/datetime-format/datetime-format.module */ "./src/app/shareds/pipe/datetime-format/datetime-format.module.ts");
/* harmony import */ var _shareds_components_nh_datetime_picker_nh_date_module__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ../../shareds/components/nh-datetime-picker/nh-date.module */ "./src/app/shareds/components/nh-datetime-picker/nh-date.module.ts");
/* harmony import */ var _shareds_components_tinymce_tinymce_module__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ../../shareds/components/tinymce/tinymce.module */ "./src/app/shareds/components/tinymce/tinymce.module.ts");
/* harmony import */ var _shareds_components_ghm_user_suggestion_ghm_user_suggestion_module__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ../../shareds/components/ghm-user-suggestion/ghm-user-suggestion.module */ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.module.ts");
/* harmony import */ var _shareds_components_nh_suggestion_nh_suggestion_module__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ../../shareds/components/nh-suggestion/nh-suggestion.module */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.module.ts");
/* harmony import */ var _shareds_components_nh_dropdown_nh_dropdown_module__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ../../shareds/components/nh-dropdown/nh-dropdown.module */ "./src/app/shareds/components/nh-dropdown/nh-dropdown.module.ts");
/* harmony import */ var _account_account_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./account/account.component */ "./src/app/modules/configs/account/account.component.ts");
/* harmony import */ var _account_account_form_account_form_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./account/account-form/account-form.component */ "./src/app/modules/configs/account/account-form/account-form.component.ts");
/* harmony import */ var _language_language_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./language/language.component */ "./src/app/modules/configs/language/language.component.ts");
/* harmony import */ var _approver_approver_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./approver/approver.component */ "./src/app/modules/configs/approver/approver.component.ts");
/* harmony import */ var _shareds_components_nh_context_menu_nh_context_menu_module__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ../../shareds/components/nh-context-menu/nh-context-menu.module */ "./src/app/shareds/components/nh-context-menu/nh-context-menu.module.ts");
/* harmony import */ var _system_system_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./system/system.component */ "./src/app/modules/configs/system/system.component.ts");
/* harmony import */ var _client_client_form_client_form_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./client/client-form/client-form.component */ "./src/app/modules/configs/client/client-form/client-form.component.ts");








































var ConfigModule = /** @class */ (function () {
    function ConfigModule() {
    }
    ConfigModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_5__["CommonModule"], _shareds_layouts_layout_module__WEBPACK_IMPORTED_MODULE_12__["LayoutModule"], _config_routing_module__WEBPACK_IMPORTED_MODULE_3__["ConfigRoutingModule"], _shareds_components_nh_select_nh_select_module__WEBPACK_IMPORTED_MODULE_4__["NhSelectModule"], _shareds_components_nh_image_nh_image_module__WEBPACK_IMPORTED_MODULE_22__["NhImageModule"], _shareds_components_nh_user_picker_nh_user_picker_module__WEBPACK_IMPORTED_MODULE_25__["NhUserPickerModule"], _shareds_components_tinymce_tinymce_module__WEBPACK_IMPORTED_MODULE_29__["TinymceModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatCheckboxModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatPaginatorModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSlideToggleModule"], _shareds_pipe_datetime_format_datetime_format_module__WEBPACK_IMPORTED_MODULE_27__["DatetimeFormatModule"], _shareds_components_nh_datetime_picker_nh_date_module__WEBPACK_IMPORTED_MODULE_28__["NhDateModule"], _shareds_components_nh_dropdown_nh_dropdown_module__WEBPACK_IMPORTED_MODULE_32__["NhDropdownModule"],
                _shareds_components_nh_modal_nh_modal_module__WEBPACK_IMPORTED_MODULE_7__["NhModalModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTooltipModule"], _shareds_components_nh_tree_nh_tree_module__WEBPACK_IMPORTED_MODULE_20__["NHTreeModule"], _shareds_components_ghm_file_explorer_ghm_file_explorer_module__WEBPACK_IMPORTED_MODULE_26__["GhmFileExplorerModule"], _shareds_components_ghm_user_suggestion_ghm_user_suggestion_module__WEBPACK_IMPORTED_MODULE_30__["GhmUserSuggestionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTabsModule"], _shareds_components_nh_suggestion_nh_suggestion_module__WEBPACK_IMPORTED_MODULE_31__["NhSuggestionModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatIconModule"], _shareds_components_nh_context_menu_nh_context_menu_module__WEBPACK_IMPORTED_MODULE_37__["NhContextMenuModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatDialogModule"],
                _shareds_components_ghm_select_picker_ghm_select_picker_module__WEBPACK_IMPORTED_MODULE_23__["GhmSelectPickerModule"], _core_core_module__WEBPACK_IMPORTED_MODULE_24__["CoreModule"], _shareds_components_ghm_paging_ghm_paging_module__WEBPACK_IMPORTED_MODULE_9__["GhmPagingModule"], _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_19__["SweetAlert2Module"].forRoot({
                    buttonsStyling: false,
                    customClass: 'modal-content',
                    confirmButtonClass: 'btn blue cm-mgr-5',
                    cancelButtonClass: 'btn',
                    // confirmButtonText: 'Đồng ý',
                    showCancelButton: true,
                })
            ],
            exports: [],
            declarations: [_page_page_component__WEBPACK_IMPORTED_MODULE_2__["PageComponent"], _page_page_form_component__WEBPACK_IMPORTED_MODULE_10__["PageFormComponent"], _client_client_component__WEBPACK_IMPORTED_MODULE_11__["ClientComponent"], _account_account_component__WEBPACK_IMPORTED_MODULE_33__["AccountComponent"], _account_account_form_account_form_component__WEBPACK_IMPORTED_MODULE_34__["AccountFormComponent"],
                _client_client_form_client_form_component__WEBPACK_IMPORTED_MODULE_39__["ClientFormComponent"], _config_component__WEBPACK_IMPORTED_MODULE_13__["ConfigComponent"], _tenant_tenant_component__WEBPACK_IMPORTED_MODULE_14__["TenantComponent"], _tenant_tenant_form_component__WEBPACK_IMPORTED_MODULE_15__["TenantFormComponent"], _role_role_component__WEBPACK_IMPORTED_MODULE_16__["RoleComponent"], _role_role_form_role_form_component__WEBPACK_IMPORTED_MODULE_17__["RoleFormComponent"], _role_role_detail_role_detail_component__WEBPACK_IMPORTED_MODULE_18__["RoleDetailComponent"],
                _language_language_component__WEBPACK_IMPORTED_MODULE_35__["LanguageComponent"], _user_setting_user_setting_component__WEBPACK_IMPORTED_MODULE_21__["UserSettingComponent"],
                _approver_approver_component__WEBPACK_IMPORTED_MODULE_36__["ApproverComponent"], _system_system_component__WEBPACK_IMPORTED_MODULE_38__["SystemComponent"]
            ],
            entryComponents: [_client_client_form_client_form_component__WEBPACK_IMPORTED_MODULE_39__["ClientFormComponent"]],
            providers: [],
        })
    ], ConfigModule);
    return ConfigModule;
}());



/***/ }),

/***/ "./src/app/modules/configs/language/language.component.html":
/*!******************************************************************!*\
  !*** ./src/app/modules/configs/language/language.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "hello from language component\r\n"

/***/ }),

/***/ "./src/app/modules/configs/language/language.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/modules/configs/language/language.component.ts ***!
  \****************************************************************/
/*! exports provided: LanguageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LanguageComponent", function() { return LanguageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LanguageComponent = /** @class */ (function () {
    function LanguageComponent() {
    }
    LanguageComponent.prototype.ngOnInit = function () { };
    LanguageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-language',
            template: __webpack_require__(/*! ./language.component.html */ "./src/app/modules/configs/language/language.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LanguageComponent);
    return LanguageComponent;
}());



/***/ }),

/***/ "./src/app/modules/configs/page/models/page-translation.model.ts":
/*!***********************************************************************!*\
  !*** ./src/app/modules/configs/page/models/page-translation.model.ts ***!
  \***********************************************************************/
/*! exports provided: PageTranslation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageTranslation", function() { return PageTranslation; });
var PageTranslation = /** @class */ (function () {
    function PageTranslation() {
    }
    return PageTranslation;
}());



/***/ }),

/***/ "./src/app/modules/configs/page/models/page.model.ts":
/*!***********************************************************!*\
  !*** ./src/app/modules/configs/page/models/page.model.ts ***!
  \***********************************************************/
/*! exports provided: Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Page", function() { return Page; });
var Page = /** @class */ (function () {
    function Page(id, isActive, url, icon, bgColor, order, parentId) {
        this.id = id;
        this.isActive = true;
        this.url = url ? url : '';
        this.icon = icon ? icon : '';
        this.bgColor = bgColor;
        this.order = 0;
        this.parentId = parentId;
    }
    return Page;
}());



/***/ }),

/***/ "./src/app/modules/configs/page/page-form.component.html":
/*!***************************************************************!*\
  !*** ./src/app/modules/configs/page/page-form.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #pageFormModal size=\"md\"\r\n          (show)=\"onModalShown()\"\r\n          (hidden)=\"onModalHidden()\">\r\n    <nh-modal-header>\r\n        <h4 class=\"modal-title\">\r\n            <i class=\"fa fa-file-text-o\"></i>\r\n            {{isUpdate ? \"Cập nhật trang\" : \"Thêm mới trang\"}}\r\n        </h4>\r\n    </nh-modal-header>\r\n    <form class=\"horizontal-form\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content class=\"form-body\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-12\">\r\n                    <div class=\"tabbable-custom\">\r\n                        <ul class=\"nav nav-tabs \" *ngIf=\"languages.length > 0\">\r\n                            <li [class.active]=\"item.id === currentLanguage\"\r\n                                *ngFor=\"let item of languages; let i = index\">\r\n                                <a href=\"javascript://\" (click)=\"currentLanguage = item.id\">\r\n                                    {{ item.name }}\r\n                                </a>\r\n                            </li>\r\n                        </ul>\r\n                        <div class=\"tab-content\">\r\n                            <div class=\"tab-pane active\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"col-sm-6\">\r\n                                        <div class=\"form-group\" [class.has-error]=\"formErrors.id\">\r\n                                            <label ghmLabel=\"Mã trang\" class=\"control-label\" [required]=\"true\"></label>\r\n                                            <input type=\"text\" id=\"pageId\" class=\"form-control\"\r\n                                                   autocomplete=\"off\"\r\n                                                   placeholder=\"Nhập mã trang\"\r\n                                                   formControlName=\"id\"\r\n                                                   [attr.disabled]=\"isUpdate ? '' : null\"/>\r\n                                            <div class=\"help-block\" *ngIf=\"formErrors.id\">{{formErrors.id}}</div>\r\n                                        </div>\r\n                                        <span formArrayName=\"modelTranslations\">\r\n                                            <div class=\"form-group\"\r\n                                                 *ngFor=\"let modelTranslation of modelTranslations.controls; index as i\"\r\n                                                 [hidden]=\"modelTranslation.value.languageId !== currentLanguage\"\r\n                                                 [formGroupName]=\"i\"\r\n                                                 [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.name\">\r\n                                                <label ghmLabel=\"Tên trang\"\r\n                                                       class=\"control-label\"\r\n                                                       [required]=\"true\"\r\n                                                ></label>\r\n                                                <input type=\"text\" class=\"form-control\" placeholder=\"Nhập tên trang\"\r\n                                                       formControlName=\"name\">\r\n                                            </div>\r\n                                        </span>\r\n                                        <div class=\"form-group\" [class.has-error]=\"formErrors.url\">\r\n                                            <label ghmLabel=\"Đường dẫn\" class=\"control-label\"></label>\r\n                                            <input type=\"text\" class=\"form-control\" placeholder=\"Nhập đường dẫn\"\r\n                                                   formControlName=\"url\">\r\n                                            <div class=\"help-block\" *ngIf=\"formErrors.url\">{{formErrors.url}}</div>\r\n                                        </div>\r\n                                        <span formArrayName=\"modelTranslations\">\r\n                                            <div class=\"form-group\"\r\n                                                 *ngFor=\"let modelTranslation of modelTranslations.controls; index as i\"\r\n                                                 [hidden]=\"modelTranslation.value.languageId !== currentLanguage\"\r\n                                                 [formGroupName]=\"i\"\r\n                                                 [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.description\">\r\n                                                <label ghmLabel=\"Mô tả\"\r\n                                                       class=\"control-label\"\r\n                                                ></label>\r\n                                                <textarea class=\"form-control\" rows=\"3\"\r\n                                                          placeholder=\"Nhập nội dung mô tả\"\r\n                                                          formControlName=\"description\"\r\n                                                ></textarea>\r\n                                            </div>\r\n                                        </span>\r\n                                    </div><!-- END: left col -->\r\n                                    <div class=\"col-sm-6\">\r\n                                        <div class=\"form-group\">\r\n                                            <label ghmLabel=\"Trang cha\" class=\"control-label\"></label>\r\n                                            <nh-dropdown-tree\r\n                                                [title]=\"'-- Chọn trang cha --'\"\r\n                                                [data]=\"pageTree\"\r\n                                                [width]=\"350\"\r\n                                                formControlName=\"parentId\"\r\n                                            ></nh-dropdown-tree>\r\n                                            <div class=\"help-block\"></div>\r\n                                        </div>\r\n                                        <div class=\"form-group\" [class.has-error]=\"formErrors.icon\">\r\n                                            <label ghmLabel=\"Icon\" class=\"control-label\"></label>\r\n                                            <input type=\"text\" class=\"form-control\" placeholder=\"Nhập icon\"\r\n                                                   formControlName=\"icon\">\r\n                                            <div class=\"help-block\" *ngIf=\"formErrors.icon\">{{formErrors.icon}}</div>\r\n                                        </div>\r\n                                        <div class=\"form-group\" [class.has-error]=\"formErrors.bgColor\">\r\n                                            <label ghmLabel=\"Màu nền\" class=\"control-label\"></label>\r\n                                            <input type=\"text\" class=\"form-control\" placeholder=\"Nhập màu nền\"\r\n                                                   formControlName=\"bgColor\">\r\n                                            <div class=\"help-block\" *ngIf=\"formErrors.bgColor\">{{formErrors.bgColor}}\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"form-group\">\r\n                                            <label ghmLabel=\"Thứ tự\" class=\"control-label\"></label>\r\n                                            <input type=\"text\" class=\"form-control\" placeholder=\"Nhập thứ tự\"\r\n                                                   formControlName=\"order\">\r\n                                            <div class=\"help-block\"></div>\r\n                                        </div>\r\n                                        <div class=\"form-group\">\r\n                                            <mat-slide-toggle color=\"primary\" formControlName=\"isActive\">Kích hoạt\r\n                                            </mat-slide-toggle>\r\n                                        </div>\r\n                                    </div><!-- END: right col -->\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer class=\"text-right\">\r\n            <mat-checkbox\r\n                class=\"cm-mgr-5\"\r\n                color=\"primary\"\r\n                name=\"isCreateAnother\"\r\n                *ngIf=\"!isUpdate\"\r\n                i18n=\"@@isCreateAnother\"\r\n                [(checked)]=\"isCreateAnother\"\r\n                (change)=\"isCreateAnother = !isCreateAnother\"> Create another\r\n            </mat-checkbox>\r\n            <ghm-button classes=\"btn btn-primary\" [loading]=\"isSaving\" i18n=\"@@save\">\r\n                Save\r\n            </ghm-button>\r\n            <ghm-button type=\"button\" icon=\"fa fa-times\" classes=\"btn btn-light cm-mgl-5\" [loading]=\"isSaving\"\r\n                        nh-dismiss=\"true\" i18n=\"@@cancel\">\r\n                Cancel\r\n            </ghm-button>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/configs/page/page-form.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/modules/configs/page/page-form.component.ts ***!
  \*************************************************************/
/*! exports provided: PageFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageFormComponent", function() { return PageFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _models_page_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./models/page.model */ "./src/app/modules/configs/page/models/page.model.ts");
/* harmony import */ var _page_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./page.service */ "./src/app/modules/configs/page/page.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _shareds_services_language_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../shareds/services/language.service */ "./src/app/shareds/services/language.service.ts");
/* harmony import */ var _models_page_translation_model__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./models/page-translation.model */ "./src/app/modules/configs/page/models/page-translation.model.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");













var PageFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](PageFormComponent, _super);
    function PageFormComponent(fb, toastr, utilService, pageService) {
        var _this = _super.call(this) || this;
        _this.fb = fb;
        _this.toastr = toastr;
        _this.utilService = utilService;
        _this.pageService = pageService;
        _this.page = new _models_page_model__WEBPACK_IMPORTED_MODULE_3__["Page"]();
        _this.onPageFormClose = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.pageTree = [];
        _this.pageTranslation = new _models_page_translation_model__WEBPACK_IMPORTED_MODULE_10__["PageTranslation"]();
        _this.translationFormErrors = {};
        _this.translationValidationMessage = [];
        _this.buildFormLanguage = function (language) {
            _this.translationFormErrors[language] = _this.utilService.renderFormError(['name', 'description']);
            _this.translationValidationMessage[language] = _this.utilService.renderFormErrorMessage([
                { 'name': ['required', 'maxlength'] },
                { 'description': ['maxlength'] },
            ]);
            var pageTranslationModel = _this.fb.group({
                name: [_this.pageTranslation.name, [
                        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(256)
                    ]],
                languageId: [language],
                description: [_this.pageTranslation.description, [
                        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(500)
                    ]]
            });
            pageTranslationModel.valueChanges.subscribe(function (data) { return _this.utilService.onValueChanged(pageTranslationModel, _this.translationFormErrors[language], _this.translationValidationMessage[language]); });
            return pageTranslationModel;
        };
        return _this;
    }
    Object.defineProperty(PageFormComponent.prototype, "pageTranslations", {
        get: function () {
            return this.model.get('pageTranslations');
        },
        enumerable: true,
        configurable: true
    });
    PageFormComponent.prototype.ngOnInit = function () {
        this.renderForm();
        this.getPageTree();
    };
    PageFormComponent.prototype.add = function () {
        this.pageFormModal.open();
        this.isUpdate = false;
    };
    PageFormComponent.prototype.edit = function (page) {
        var _this = this;
        this.pageFormModal.open();
        this.isUpdate = true;
        this.pageService.getLanguageDetail(page.id)
            .subscribe(function (result) {
            if (result.data) {
                var pageData_1 = result.data;
                _this.model.patchValue({
                    id: pageData_1.id,
                    parentId: pageData_1.parentId,
                    url: pageData_1.url,
                    icon: pageData_1.icon,
                    bgColor: pageData_1.bgColor,
                    order: pageData_1.order
                });
                if (pageData_1.pageTranslation && pageData_1.pageTranslation.length > 0) {
                    _this.languages.forEach(function (language, index) {
                        var pageTranslationFormGroup = _this.modelTranslations.at(index);
                        if (pageTranslationFormGroup) {
                            var pageTranslationInfo = lodash__WEBPACK_IMPORTED_MODULE_11__["find"](pageData_1.pageTranslation, function (pageTranslation) {
                                return pageTranslationFormGroup.value.languageId === pageTranslation.languageId;
                            });
                            if (pageTranslationInfo) {
                                pageTranslationFormGroup.patchValue(pageTranslationInfo);
                            }
                            else {
                                pageTranslationFormGroup.patchValue({ name: '', description: '' });
                            }
                        }
                    });
                }
            }
        });
    };
    PageFormComponent.prototype.onModalShown = function () {
        this.utilService.focusElement('pageId');
        this.isModified = false;
    };
    PageFormComponent.prototype.onModalHidden = function () {
        this.resetModel();
        if (this.isModified) {
            this.saveSuccessful.emit();
        }
    };
    PageFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        var isLanguageValid = this.checkLanguageValid();
        if (isValid && isLanguageValid) {
            this.page = this.model.value;
            this.page.pageTranslations = this.modelTranslations.getRawValue();
            this.isSaving = true;
            if (this.isUpdate) {
                this.pageService
                    .update(this.page)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_12__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.isModified = true;
                    _this.toastr.success(result.message);
                    _this.pageFormModal.dismiss();
                });
            }
            else {
                this.pageService
                    .insert(this.page)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_12__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.utilService.focusElement('pageId');
                    _this.isModified = true;
                    _this.toastr.success(result.message);
                    _this.getPageTree();
                    if (_this.isCreateAnother) {
                        _this.resetModel();
                    }
                    else {
                        _this.pageFormModal.dismiss();
                    }
                });
            }
        }
    };
    // private renderLanguageData() {
    //     this.languages = this.appService.languages;
    //     this.languageData = this.languages.map((language: LanguageSearchViewModel) => {
    //         if (language.isDefault) {
    //             this.currentLanguage = language.languageId;
    //         }
    //         return {id: language.languageId, name: language.name, isSelected: language.isDefault};
    //     });
    //     this.renderPageTranslation();
    // }
    PageFormComponent.prototype.renderForm = function () {
        this.buildForm();
        this.renderTranslationFormArray(this.buildFormLanguage);
    };
    PageFormComponent.prototype.resetModel = function () {
        this.model.patchValue({
            id: null,
            name: '',
            isActive: true,
            url: '',
            icon: '',
            bgColor: '',
            order: 0
        });
        this.modelTranslations.controls.forEach(function (pageTranslation) {
            pageTranslation.patchValue({
                name: '',
                description: ''
            });
        });
    };
    PageFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.utilService.renderFormError(['id', 'icon', 'bgColor', 'url']);
        this.validationMessages = {
            id: {
                required: 'Mã trang không được để trống',
            },
            icon: {
                maxlength: 'Icon không được phép vượt quá 50 ký tự.',
            },
            bgColor: {
                maxlength: 'Mã màu nền không được phép lớn hơn 10 ký tự.'
            },
            url: {
                maxlength: 'Url không được phép vượt quá 500 ký tự.'
            }
        };
        this.model = this.fb.group({
            'id': [this.page.id, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
                ]],
            'isActive': [this.page.isActive],
            'url': [this.page.url, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(500)
                ]],
            'icon': [this.page.icon, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)
                ]],
            'order': [this.page.order],
            'parentId': [this.page.parentId],
            'bgColor': [this.page.bgColor, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(10)
                ]],
            'modelTranslations': this.fb.array([])
        });
        this.model.valueChanges.subscribe(function (data) { return _this.utilService.onValueChanged(_this.model, _this.formErrors, _this.validationMessages); });
    };
    // private buildFormLanguage(language: string): FormGroup {
    //     this.translationFormErrors[language] = this.utilService.renderFormError(['name', 'description']);
    //     this.translationValidationMessage[language] = {
    //         name: {
    //             required: 'Vui lòng nhập tên trang',
    //             maxLength: 'Tên trang không được phép vượt quá 256 ký tự.'
    //         },
    //         description: {
    //             maxLength: 'Mô tả không được phép vượt quá 500 ký tự.'
    //         }
    //     };
    //
    // }
    PageFormComponent.prototype.getPageTree = function () {
        var _this = this;
        this.pageService.getPageTree()
            .subscribe(function (result) {
            _this.pageTree = result;
        });
    };
    PageFormComponent.prototype.renderPageTranslation = function () {
        var _this = this;
        this.pageTranslationFormArray = this.model.get('pageTranslations');
        this.languages.forEach(function (language) {
            _this.pageTranslationFormArray.push(_this.buildFormLanguage(language.id));
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('pageFormModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_8__["NhModalComponent"])
    ], PageFormComponent.prototype, "pageFormModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], PageFormComponent.prototype, "page", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], PageFormComponent.prototype, "onPageFormClose", void 0);
    PageFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-page-form',
            template: __webpack_require__(/*! ./page-form.component.html */ "./src/app/modules/configs/page/page-form.component.html"),
            providers: [_shareds_services_language_service__WEBPACK_IMPORTED_MODULE_9__["LanguageService"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_6__["UtilService"],
            _page_service__WEBPACK_IMPORTED_MODULE_4__["PageService"]])
    ], PageFormComponent);
    return PageFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_7__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/configs/page/page.component.html":
/*!**********************************************************!*\
  !*** ./src/app/modules/configs/page/page.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@listPageTitle\">List pages</span>\r\n    <small i18n=\"@@ConfigModuleTitle\">Config module</small>\r\n</h1>\r\n\r\n<div class=\"row cm-mgb-10\">\r\n    <div class=\"col-sm-12\">\r\n        <form class=\"form-inline\" (ngSubmit)=\"search()\">\r\n            <div class=\"form-group\">\r\n                <input class=\"form-control cm-mgr-5\" placeholder=\"Nhập từ khoá tìm kiếm\"\r\n                       name=\"keyword\"\r\n                       [(ngModel)]=\"keyword\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <nh-select\r\n                    [class]=\"'cm-mgr-5'\"\r\n                    [data]=\"[{id: true, name: 'Kích hoạt'}, {id: false, name: 'Chưa kích hoạt'}]\"\r\n                    [title]=\"'-- Tất cả --'\"\r\n                    [(value)]=\"isActive\"\r\n                    (onSelectItem)=\"search()\"></nh-select>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <ghm-button classes=\"btn blue\" icon=\"fa fa-search\" [loading]=\"isSearching\"></ghm-button>\r\n            </div>\r\n            <div class=\"form-group pull-right\" *ngIf=\"permission.add\">\r\n                <button type=\"button\" class=\"btn blue\" (click)=\"add()\" *ngIf=\"permission.add\"\r\n                        i18n=\"@@add\">\r\n                    Add\r\n                </button>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <table class=\"table table-striped table-hover table-full-width dataTable no-footer\">\r\n            <thead>\r\n            <tr>\r\n                <th class=\"center w50\" i18n=\"@@no\">No</th>\r\n                <th i18n=\"@@pageName\">Page name</th>\r\n                <th class=\"w100\">Icon</th>\r\n                <th i18n=\"@@url\">Url</th>\r\n                <th class=\"w100\" i18n=\"@@public\">Public</th>\r\n                <th class=\"w100\" i18n=\"@@active\">Active</th>\r\n                <th class=\"text-right w150\" *ngIf=\"permission.edit || permission.delete\"></th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            <tr *ngFor=\"let page of listItems$ | async; let i = index\"\r\n                nhContextMenuTrigger=\"\" [nhContextMenuTriggerFor]=\"nhMenu\"\r\n                [nhContextMenuData]=\"page\">\r\n                <td class=\"center middle\">{{(currentPage - 1) * pageSize + i + 1}}</td>\r\n                <td class=\"middle cursor-pointer\" (click)=\"edit(page)\">\r\n                    <a href=\"javascritp://\">\r\n                        <span [innerHtml]=\"page.namePrefix\"></span>\r\n                        {{page.name}}\r\n                    </a>\r\n                </td>\r\n                <td class=\"w100 center middle\">\r\n                    <i [class]=\"page.icon\"></i>\r\n                </td>\r\n                <td>\r\n                    {{page.url}}\r\n                </td>\r\n                <td class=\"center middle\">\r\n                    <!--<i *ngIf=\"page.isPublic\"-->\r\n                    <!--class=\"fa fa-check color-green size-16\"></i>-->\r\n                </td>\r\n                <td class=\"center middle\">\r\n                    <span class=\"badge\"\r\n                          [class.badge-success]=\"page.isActive\"\r\n                          [class.badge-danger]=\"!page.isActive\">\r\n                        {{ page.isActive ? 'Đã kích hoạt' : 'Chưa kích hoạt' }}\r\n                    </span>\r\n                </td>\r\n                <td class=\"text-right middle\" *ngIf=\"permission.edit || permission.delete\">\r\n                    <ghm-button type=\"button\" classes=\"btn blue btn-sm\" icon=\"fa fa-edit\"\r\n                        matTooltip=\"Update\"\r\n                        *ngIf=\"permission.edit\"\r\n                        (click)=\"edit(page)\">\r\n                        </ghm-button>\r\n                        <ghm-button icon=\"fa fa-trash-o\" classes=\"btn red btn-sm\"\r\n                                    matTooltip=\"Delete\"\r\n                                    *ngIf=\"permission.delete\"\r\n                                    [swal]=\"{ title: 'Bạn có chắc chắn muốn xóa trang: ' + page.name + ' không?' }\"\r\n                                    (confirm)=\"delete(page.id)\"></ghm-button>\r\n                </td>\r\n            </tr>\r\n            </tbody>\r\n        </table>\r\n    </div>\r\n</div>\r\n<app-page-form [page]=\"page\" (saveSuccessful)=\"search()\"></app-page-form>\r\n\r\n\r\n<nh-menu #nhMenu>\r\n    <nh-menu-item (clicked)=\"edit($event)\">\r\n        <mat-icon class=\"menu-icon\">edit</mat-icon>\r\n        <span i18n=\"@@edit\">Edit</span>\r\n    </nh-menu-item>\r\n    <nh-menu-item (clicked)=\"delete($event.id)\">\r\n        <mat-icon class=\"menu-icon\">delete</mat-icon>\r\n        <span i18n=\"@@edit\">Delete</span>\r\n    </nh-menu-item>\r\n</nh-menu>\r\n"

/***/ }),

/***/ "./src/app/modules/configs/page/page.component.ts":
/*!********************************************************!*\
  !*** ./src/app/modules/configs/page/page.component.ts ***!
  \********************************************************/
/*! exports provided: PageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageComponent", function() { return PageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _models_page_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./models/page.model */ "./src/app/modules/configs/page/models/page.model.ts");
/* harmony import */ var _page_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./page.service */ "./src/app/modules/configs/page/page.service.ts");
/* harmony import */ var _configs_page_id_config__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../configs/page-id.config */ "./src/app/configs/page-id.config.ts");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _page_form_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./page-form.component */ "./src/app/modules/configs/page/page-form.component.ts");
/* harmony import */ var _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../shareds/models/filter-link.model */ "./src/app/shareds/models/filter-link.model.ts");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! rxjs/internal/operators */ "./node_modules/rxjs/internal/operators/index.js");
/* harmony import */ var rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_15__);
















var PageComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](PageComponent, _super);
    function PageComponent(pageId, title, router, route, location, toastr, utilService, spinnerService, pageService) {
        var _this = _super.call(this) || this;
        _this.pageId = pageId;
        _this.title = title;
        _this.router = router;
        _this.route = route;
        _this.location = location;
        _this.toastr = toastr;
        _this.utilService = utilService;
        _this.spinnerService = spinnerService;
        _this.pageService = pageService;
        _this.page = new _models_page_model__WEBPACK_IMPORTED_MODULE_7__["Page"]();
        _this.listItems$ = _this.route.data.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["map"])(function (result) {
            return result.data;
        }));
        _this.subscribers.queryParams = _this.route.queryParams.subscribe(function (params) {
            _this.keyword = params.keyword;
            _this.isActive = params.isActive;
        });
        return _this;
    }
    PageComponent.prototype.ngOnInit = function () {
        this.appService.setupPage(this.pageId.CONFIG, this.pageId.CONFIG_PAGE, 'Cấu hình', 'Cấu hình trang');
    };
    PageComponent.prototype.search = function () {
        var _this = this;
        this.renderFilterLink();
        this.isSearching = true;
        this.listItems$ = this.pageService.search(this.keyword, this.isActive)
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_15__["finalize"])(function () { return _this.isSearching = false; }));
    };
    PageComponent.prototype.delete = function (id) {
        var _this = this;
        this.subscribers.deletePage = this.pageService.delete(id)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
            _this.search();
        });
    };
    PageComponent.prototype.changeActiveStatus = function (page) {
        page.isActive = !page.isActive;
    };
    PageComponent.prototype.add = function () {
        this.pageFormComponent.add();
    };
    PageComponent.prototype.edit = function (page) {
        this.pageFormComponent.edit(page);
    };
    PageComponent.prototype.sayHello = function () {
        console.log('hello');
    };
    PageComponent.prototype.renderFilterLink = function () {
        var path = '/config/pages';
        var query = this.utilService.renderLocationFilter([
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_12__["FilterLink"]('keyword', this.keyword),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_12__["FilterLink"]('clientId', this.clientId),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_12__["FilterLink"]('isActive', this.isActive),
        ]);
        this.location.go(path, query);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_page_form_component__WEBPACK_IMPORTED_MODULE_11__["PageFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _page_form_component__WEBPACK_IMPORTED_MODULE_11__["PageFormComponent"])
    ], PageComponent.prototype, "pageFormComponent", void 0);
    PageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-page-component',
            template: __webpack_require__(/*! ./page.component.html */ "./src/app/modules/configs/page/page.component.html"),
            preserveWhitespaces: false
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_page_id_config__WEBPACK_IMPORTED_MODULE_9__["PAGE_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_6__["UtilService"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_13__["SpinnerService"],
            _page_service__WEBPACK_IMPORTED_MODULE_8__["PageService"]])
    ], PageComponent);
    return PageComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_10__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/configs/role/models/role.model.ts":
/*!***********************************************************!*\
  !*** ./src/app/modules/configs/role/models/role.model.ts ***!
  \***********************************************************/
/*! exports provided: Role */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Role", function() { return Role; });
var Role = /** @class */ (function () {
    function Role() {
    }
    return Role;
}());



/***/ }),

/***/ "./src/app/modules/configs/role/role-detail/role-detail.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/modules/configs/role/role-detail/role-detail.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #roleDetailModal size=\"md\">\r\n    <nh-modal-header>\r\n        <i class=\"fa fa-user-secret\" aria-hidden=\"true\"></i>\r\n        <ng-container i18n=\"@@roleDetailModalTitle\">Detail Role: \"<span class=\"bold\">{{ role?.name }}</span>\"\r\n        </ng-container>\r\n    </nh-modal-header>\r\n\r\n    <nh-modal-content>\r\n        <form action=\"\" class=\"form-horizontal\">\r\n            <div class=\"form-body\">\r\n                <div class=\"form-group\">\r\n                    <label i18n-ghmLabel=\"@@roleName\" ghmLabel=\"Role name\" class=\"col-sm-4 control-label\"\r\n                           [required]=\"true\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <div class=\"form-control\">{{ role?.name }}</div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <label i18n-ghmLabel=\"@@description\" ghmLabel=\"Description\" class=\"col-sm-4 control-label\"\r\n                           [required]=\"true\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <div class=\"form-control height-auto\">{{ role?.description }}</div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <div class=\"col-sm-12\">\r\n                        <table class=\"table table-striped\">\r\n                            <thead>\r\n                            <tr>\r\n                                <th class=\"center\" i18n=\"@@No\">No</th>\r\n                                <th class=\"\" i18n=\"@@pageName\">Page name</th>\r\n                                <th class=\"center\" i18n=\"@@view\">View</th>\r\n                                <th class=\"center\" i18n=\"@@insert\">Add</th>\r\n                                <th class=\"center\" i18n=\"@@update\">Edit</th>\r\n                                <th class=\"center\" i18n=\"@@delete\">Delete</th>\r\n                                <th class=\"center\" i18n=\"@@export\">Export</th>\r\n                                <th class=\"center\" i18n=\"@@print\">Print</th>\r\n                                <th class=\"center\" i18n=\"@@approve\">Approve</th>\r\n                                <th class=\"center\" i18n=\"@@report\">Report</th>\r\n                            </tr>\r\n                            </thead>\r\n                            <tbody>\r\n                            <tr *ngFor=\"let page of pages$ | async; let i = index\">\r\n                                <td class=\"center middle w50\">{{ i + 1 }}</td>\r\n                                <td class=\"middle\">{{ page.pageName }}</td>\r\n                                <td class=\"center middle w50\">\r\n                                    <i class=\"fa fa-check color-green\" *ngIf=\"page.view\"></i>\r\n                                </td>\r\n                                <td class=\"center middle w50\">\r\n                                    <i class=\"fa fa-check color-green\" *ngIf=\"page.add\"></i>\r\n                                </td>\r\n                                <td class=\"center middle w50\">\r\n                                    <i class=\"fa fa-check color-green\" *ngIf=\"page.edit\"></i>\r\n                                </td>\r\n                                <td class=\"center middle w50\">\r\n                                    <i class=\"fa fa-check color-green\" *ngIf=\"page.delete\"></i>\r\n                                </td>\r\n                                <td class=\"center middle w50\">\r\n                                    <i class=\"fa fa-check color-green\" *ngIf=\"page.export\"></i>\r\n                                </td>\r\n                                <td class=\"center middle w50\">\r\n                                    <i class=\"fa fa-check color-green\" *ngIf=\"page.print\"></i>\r\n                                </td>\r\n                                <td class=\"center middle w50\">\r\n                                    <i class=\"fa fa-check color-green\" *ngIf=\"page.approve\"></i>\r\n                                </td>\r\n                                <td class=\"center middle w50\">\r\n                                    <i class=\"fa fa-check color-green\" *ngIf=\"page.report\"></i>\r\n                                </td>\r\n                            </tr>\r\n                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <div class=\"col-sm-8 col-sm-offset-4\">\r\n                        <ghm-button type=\"button\" classes=\"btn btn-default\"\r\n                                    nh-dismiss=\"true\"\r\n                                    i18n=\"@@close\">\r\n                            Close\r\n                        </ghm-button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </form><!-- END form role info -->\r\n    </nh-modal-content>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/configs/role/role-detail/role-detail.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/modules/configs/role/role-detail/role-detail.component.ts ***!
  \***************************************************************************/
/*! exports provided: RoleDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleDetailComponent", function() { return RoleDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_role_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/role.model */ "./src/app/modules/configs/role/models/role.model.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _role_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../role.service */ "./src/app/modules/configs/role/role.service.ts");





var RoleDetailComponent = /** @class */ (function () {
    function RoleDetailComponent(roleService) {
        this.roleService = roleService;
        this.role = null;
    }
    RoleDetailComponent.prototype.ngOnInit = function () {
    };
    RoleDetailComponent.prototype.show = function (role) {
        this.role = role;
        this.pages$ = this.roleService.getRolesPages(this.role.id).pipe();
        this.detailModal.open();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__["NhModalComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__["NhModalComponent"])
    ], RoleDetailComponent.prototype, "detailModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_role_model__WEBPACK_IMPORTED_MODULE_2__["Role"])
    ], RoleDetailComponent.prototype, "role", void 0);
    RoleDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-role-detail',
            template: __webpack_require__(/*! ./role-detail.component.html */ "./src/app/modules/configs/role/role-detail/role-detail.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_role_service__WEBPACK_IMPORTED_MODULE_4__["RoleService"]])
    ], RoleDetailComponent);
    return RoleDetailComponent;
}());



/***/ }),

/***/ "./src/app/modules/configs/role/role-form/role-form.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/modules/configs/role/role-form/role-form.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@roleFormPageTitle\">{isUpdate, select, 0 {Add new role} 1 {Update role}}</span>\r\n    <small i18n=\"@@configModuleTitle\">Configs</small>\r\n</h1>\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <form class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n            <div class=\"form-body\">\r\n                <div class=\"portlet light bordered\">\r\n                    <div class=\"portlet-title\">\r\n                        <div class=\"caption font-red-sunglo\">\r\n                            <i class=\"icon-share font-red-sunglo\"></i>\r\n                            <span class=\"caption-subject bold uppercase\" i18n=\"@@roleInfo\"> Thông tin quyền </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"portlet-body\">\r\n                        <div class=\"form-group\">\r\n                            <div class=\"col-sm-12\">\r\n                                <ghm-alert [type]=\"message?.type\" *ngIf=\"message?.content\"\r\n                                           i18n=\"@@roleFormMessage\">\r\n                                    {\r\n                                    message?.content, select,\r\n                                    inValid {Please select at least permission for each page.}\r\n                                    selectOne {Please select at least one page.}\r\n                                    }\r\n                                </ghm-alert>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group\" [class.has-error]=\"formErrors.name\">\r\n                            <label i18n-ghmLabel=\"@@roleName\" ghmLabel=\"Tên quyền\" class=\"col-sm-4 control-label\"\r\n                                   [required]=\"true\"></label>\r\n                            <div class=\"col-sm-8\">\r\n                                <input type=\"text\" class=\"form-control\"\r\n                                       i18n-placeholder=\"@@enterRoleNamePlaceholder\"\r\n                                       placeholder=\"Vui lòng nhập tên quyền.\" id=\"name\"\r\n                                       formControlName=\"name\">\r\n                                <span class=\"help-block\"\r\n                                      i18n=\"@@roleNameValidationMessage\" *ngIf=\"formErrors.name\">\r\n                                {formErrors.name, select, required {Vui lòng nhập tên quyền} maxlength {Tên quyền không được phép vượt quá 256 ký tự} other {}}\r\n                        </span>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group\" [class.has-error]=\"formErrors.description\">\r\n                            <label i18n-ghmLabel=\"@@description\" ghmLabel=\"Mô tả\" class=\"col-sm-4 control-label\"\r\n                                   [required]=\"true\"></label>\r\n                            <div class=\"col-sm-8\">\r\n                                    <textarea class=\"form-control\" rows=\"3\"\r\n                                              i18n-placeholder=\"@@enterDescriptionPlaceHolder\"\r\n                                              placeholder=\"Vui lòng nhập nội dung mô tả.\"\r\n                                              formControlName=\"description\"></textarea>\r\n                                <span class=\"help-block\" i18n=\"@@roleDescriptionValidationMessage\"\r\n                                      *ngIf=\"formErrors.description\">\r\n                            {formErrors.description, select, maxlength {Tên quyền không được phép vượt quá 256 ký tự.}}\r\n                        </span>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <!--<div class=\"form-group\">-->\r\n                <!--<label i18n-ghmLabel=\"@@pages\" ghmLabel=\"Pages\" class=\"col-sm-4 control-label\"></label>-->\r\n                <!--<div class=\"col-sm-8\">-->\r\n                <!--<logic type=\"logic\" class=\"btn btn-primary cm-mgb-10\" i18n=\"@@selectPage\"-->\r\n                <!--(click)=\"showSelectPage()\">Select page-->\r\n                <!--</logic>-->\r\n                <!--</div>-->\r\n                <!--</div>-->\r\n                <div class=\"form-group\">\r\n                    <div class=\"col-sm-6\">\r\n                        <div class=\"portlet light bordered\">\r\n                            <div class=\"portlet-title\">\r\n                                <div class=\"caption font-red-sunglo\">\r\n                                    <i class=\"icon-share font-red-sunglo\"></i>\r\n                                    <span class=\"caption-subject bold uppercase\" i18n=\"@@pages\"> Trang</span>\r\n                                </div>\r\n                                <div class=\"actions\">\r\n                                    <a href=\"javascript:;\" class=\"btn btn-circle blue btn-outline\" i18n=\"@@add\"\r\n                                       (click)=\"showSelectPage()\">\r\n                                        Thêm\r\n                                    </a>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"portlet-body\">\r\n                                <div class=\"table-responsive\">\r\n                                    <table class=\"table table-striped\">\r\n                                        <thead>\r\n                                        <tr>\r\n                                            <th class=\"center\" i18n=\"@@No\">STT</th>\r\n                                            <th class=\"\" i18n=\"@@pageName\">Tên trang</th>\r\n                                            <th class=\"center\" i18n=\"@@full\">Full</th>\r\n                                            <th class=\"center\" i18n=\"@@view\">Xem</th>\r\n                                            <th class=\"center\" i18n=\"@@insert\">Thêm</th>\r\n                                            <th class=\"center\" i18n=\"@@update\">Sửa</th>\r\n                                            <th class=\"center\" i18n=\"@@delete\">Xóa</th>\r\n                                            <th class=\"center\" i18n=\"@@export\">Export</th>\r\n                                            <th class=\"center\" i18n=\"@@print\">In</th>\r\n                                            <th class=\"center\" i18n=\"@@approve\">Duyệt</th>\r\n                                            <th class=\"center\" i18n=\"@@report\">Báo cáo</th>\r\n                                            <th class=\"center\" i18n=\"@@actions\"></th>\r\n                                        </tr>\r\n                                        </thead>\r\n                                        <tbody>\r\n                                        <tr *ngFor=\"let page of selectedPages; let i = index\">\r\n                                            <td class=\"center middle w50\">{{ i + 1 }}</td>\r\n                                            <td class=\"middle\">\r\n                                                <span [innerHTML]=\"page.pageName\"></span>\r\n                                            </td>\r\n                                            <td class=\"center middle w50\">\r\n                                                <mat-checkbox [(checked)]=\"page.full\"\r\n                                                              (change)=\"changeFullPermission(page)\"\r\n                                                              color=\"primary\"></mat-checkbox>\r\n                                            </td>\r\n                                            <td class=\"center middle w50\">\r\n                                                <mat-checkbox [(checked)]=\"page.view\"\r\n                                                              (change)=\"changePermission(page, permissionConst.view)\"\r\n                                                              color=\"primary\"></mat-checkbox>\r\n                                            </td>\r\n                                            <td class=\"center middle w50\">\r\n                                                <mat-checkbox [(checked)]=\"page.add\"\r\n                                                              (change)=\"changePermission(page, permissionConst.add)\"\r\n                                                              color=\"primary\"></mat-checkbox>\r\n                                            </td>\r\n                                            <td class=\"center middle w50\">\r\n                                                <mat-checkbox [(checked)]=\"page.edit\"\r\n                                                              (change)=\"changePermission(page, permissionConst.edit)\"\r\n                                                              color=\"primary\"></mat-checkbox>\r\n                                            </td>\r\n                                            <td class=\"center middle w50\">\r\n                                                <mat-checkbox [(checked)]=\"page.delete\"\r\n                                                              (change)=\"changePermission(page, permissionConst.delete)\"\r\n                                                              color=\"primary\"></mat-checkbox>\r\n                                            </td>\r\n                                            <td class=\"center middle w50\">\r\n                                                <mat-checkbox [(checked)]=\"page.export\"\r\n                                                              (change)=\"changePermission(page, permissionConst.export)\"\r\n                                                              color=\"primary\"></mat-checkbox>\r\n                                            </td>\r\n                                            <td class=\"center middle w50\">\r\n                                                <mat-checkbox [(checked)]=\"page.print\"\r\n                                                              (change)=\"changePermission(page, permissionConst.print)\"\r\n                                                              color=\"primary\"></mat-checkbox>\r\n                                            </td>\r\n                                            <td class=\"center middle w50\">\r\n                                                <mat-checkbox [(checked)]=\"page.approve\"\r\n                                                              (change)=\"changePermission(page, permissionConst.approve)\"\r\n                                                              color=\"primary\"></mat-checkbox>\r\n                                            </td>\r\n                                            <td class=\"center middle w50\">\r\n                                                <mat-checkbox [(checked)]=\"page.report\"\r\n                                                              (change)=\"changePermission(page, permissionConst.report)\"\r\n                                                              color=\"primary\"></mat-checkbox>\r\n                                            </td>\r\n                                            <td class=\"center middle w50\">\r\n                                                <button type=\"button\" class=\"btn btn-sm btn-danger\"\r\n                                                        (click)=\"deletePage(page)\">\r\n                                                    <i class=\"fa fa-trash-o\"></i>\r\n                                                </button>\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div><!-- END: list pages -->\r\n                    <div class=\"col-sm-6\">\r\n                        <div class=\"portlet light bordered\">\r\n                            <div class=\"portlet-title\">\r\n                                <div class=\"caption font-red-sunglo\">\r\n                                    <i class=\"icon-share font-red-sunglo\"></i>\r\n                                    <span class=\"caption-subject bold uppercase\" i18n=\"@@users\"> Người dùng</span>\r\n                                </div>\r\n                                <div class=\"actions\">\r\n                                    <a href=\"javascript:;\" class=\"btn btn-circle blue btn-outline\" i18n=\"@@add\"\r\n                                       (click)=\"showUsers()\">\r\n                                        Thêm\r\n                                    </a>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"portlet-body\">\r\n                                <ul class=\"wrapper-list-users cm-pdl-0\">\r\n                                    <li class=\"user-item\" *ngFor=\"let user of listSelectedUsers\">\r\n                                        <div class=\"avatar-wrapper\">\r\n                                            <img class=\"avatar-sm rounded-avatar\"\r\n                                                 ghmImage\r\n                                                 src=\"{{ user.avatar }}\"\r\n                                                 alt=\"{{ user.fullName }}\">\r\n                                        </div><!-- END: .avatar-wrapper -->\r\n                                        <div class=\"user-info\">\r\n                                            <svg width=\"20\" height=\"20\" viewBox=\"0 0 20 20\" focusable=\"false\"\r\n                                                 role=\"presentation\"\r\n                                                 (click)=\"removeUser(user.id)\">\r\n                                                <path\r\n                                                    d=\"M12 10.586L6.707 5.293a1 1 0 0 0-1.414 1.414L10.586 12l-5.293 5.293a1 1 0 0 0 1.414 1.414L12 13.414l5.293 5.293a1 1 0 0 0 1.414-1.414L13.414 12l5.293-5.293a1 1 0 1 0-1.414-1.414L12 10.586z\"\r\n                                                    fill=\"currentColor\">\r\n                                                </path>\r\n                                            </svg>\r\n                                            <span class=\"full-name\">{{ user.fullName }}</span>\r\n                                            <div class=\"description\"> {{ user.description }}</div>\r\n                                        </div><!-- END: .info -->\r\n                                    </li>\r\n                                </ul>\r\n                            </div>\r\n                        </div>\r\n                    </div><!-- END: list users -->\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <div class=\"col-sm-8 col-sm-offset-4 text-right\">\r\n                        <mat-checkbox [checked]=\"isCreateAnother\" (change)=\"isCreateAnother = !isCreateAnother\"\r\n                                      *ngIf=\"!isUpdate\"\r\n                                      i18n=\"@@isCreateAnother\"\r\n                                      class=\"cm-mgr-5\"\r\n                                      color=\"primary\">\r\n                            Create another\r\n                        </mat-checkbox>\r\n                        <ghm-button classes=\"btn btn-primary cm-mgr-5\" [loading]=\"isSaving\" i18n=\"@@save\">\r\n                            Lưu\r\n                        </ghm-button>\r\n                        <ghm-button type=\"button\" classes=\"btn btn-default\"\r\n                                    (click)=\"closeForm()\"\r\n                                    i18n=\"@@cancel\">\r\n                            Hủy bỏ\r\n                        </ghm-button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </form><!-- END form role info -->\r\n    </div>\r\n</div>\r\n\r\n<ghm-select-picker\r\n    title=\"Chọn trang phân quyền\"\r\n    allTitle=\"Tất cả trang\"\r\n    selectedTitle=\"Trang được chọn\"\r\n    [source]=\"listPages\"\r\n    (accepted)=\"onAcceptedSelectPage($event)\"\r\n></ghm-select-picker>\r\n\r\n<nh-user-picker\r\n    i18n-title=\"@@selectParticipant\"\r\n    i18n-allTitle=\"@@listUser\"\r\n    i18n-selectedTitle=\"@@listSelectedUser\"\r\n    title=\"Select participant\"\r\n    allTitle=\"List user\"\r\n    selectedTitle=\"List selected user\"\r\n    [selectedItems]=\"listSelectedUsers\"\r\n    (accepted)=\"onAcceptSelectUsers($event)\"\r\n></nh-user-picker>\r\n"

/***/ }),

/***/ "./src/app/modules/configs/role/role-form/role-form.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/modules/configs/role/role-form/role-form.component.ts ***!
  \***********************************************************************/
/*! exports provided: RoleFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleFormComponent", function() { return RoleFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_role_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/role.model */ "./src/app/modules/configs/role/models/role.model.ts");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _role_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../role.service */ "./src/app/modules/configs/role/role.service.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _shareds_components_ghm_select_picker_ghm_select_picker_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../shareds/components/ghm-select-picker/ghm-select-picker.component */ "./src/app/shareds/components/ghm-select-picker/ghm-select-picker.component.ts");
/* harmony import */ var _page_page_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../page/page.service */ "./src/app/modules/configs/page/page.service.ts");
/* harmony import */ var _shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../../shareds/constants/permission.const */ "./src/app/shareds/constants/permission.const.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shareds_components_nh_user_picker_nh_user_picker_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../../../shareds/components/nh-user-picker/nh-user-picker.component */ "./src/app/shareds/components/nh-user-picker/nh-user-picker.component.ts");
















var RoleFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](RoleFormComponent, _super);
    function RoleFormComponent(fb, route, router, toastr, utilService, pageService, roleService) {
        var _this = _super.call(this) || this;
        _this.fb = fb;
        _this.route = route;
        _this.router = router;
        _this.toastr = toastr;
        _this.utilService = utilService;
        _this.pageService = pageService;
        _this.roleService = roleService;
        _this.role = new _models_role_model__WEBPACK_IMPORTED_MODULE_2__["Role"]();
        _this.listPages = [];
        _this.selectedPages = [];
        _this.permissionConst = _shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_13__["Permission"];
        _this.listSelectedUsers = [];
        _this.subscribers.routeParams = _this.route.params.subscribe(function (params) {
            if (params.id) {
                _this.isUpdate = true;
                _this.id = params.id;
                _this.roleService.getRoleDetail(_this.id)
                    .subscribe(function (roleDetail) {
                    _this.selectedPages = roleDetail.rolePages;
                    _this.model.patchValue(roleDetail);
                    _this.listSelectedUsers = roleDetail.users;
                });
            }
        });
        return _this;
    }
    RoleFormComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    RoleFormComponent.prototype.onAcceptedSelectPage = function (pages) {
        var _this = this;
        // const listNewPages = [];
        lodash__WEBPACK_IMPORTED_MODULE_10__["each"](pages, function (page) {
            var existingPage = lodash__WEBPACK_IMPORTED_MODULE_10__["find"](_this.selectedPages, function (selectedPage) {
                return selectedPage.pageId === page.id;
            });
            if (!existingPage) {
                var newPage = {
                    pageId: page.id,
                    pageName: page.name,
                    full: false,
                    view: false,
                    add: false,
                    edit: false,
                    delete: false,
                    export: false,
                    print: false,
                    approve: false,
                    report: false
                };
                _this.selectedPages.push(newPage);
            }
        });
    };
    RoleFormComponent.prototype.onAcceptSelectUsers = function (users) {
        this.listSelectedUsers = users;
        // if (this.isUpdate) {
        //     this.roleService.updateUsers(this.id, this.listSelectedUsers)
        //         .subscribe((result: ActionResultViewModel) => {
        //             this.toastr.success(result.message);
        //         });
        // }
    };
    RoleFormComponent.prototype.showUsers = function () {
        this.userPickerComponent.show();
    };
    RoleFormComponent.prototype.closeForm = function () {
        this.router.navigateByUrl('/config/roles');
    };
    RoleFormComponent.prototype.removeUser = function (userId) {
        var _this = this;
        this.roleService.removeUser(this.id, userId)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
            lodash__WEBPACK_IMPORTED_MODULE_10__["remove"](_this.listSelectedUsers, function (selectedUser) {
                return selectedUser.id === userId;
            });
        });
    };
    RoleFormComponent.prototype.changePermission = function (page, permission) {
        switch (permission) {
            case this.permissionConst.view:
                page.view = !page.view;
                break;
            case this.permissionConst.add:
                page.add = !page.add;
                break;
            case this.permissionConst.edit:
                page.edit = !page.edit;
                break;
            case this.permissionConst.delete:
                page.delete = !page.delete;
                break;
            case this.permissionConst.export:
                page.export = !page.export;
                break;
            case this.permissionConst.report:
                page.report = !page.report;
                break;
            case this.permissionConst.approve:
                page.approve = !page.approve;
                break;
            case this.permissionConst.print:
                page.print = !page.print;
                break;
        }
        var permissions = this.calculatePermissions(page);
        page.full = this.roleService.checkHasFullPermission(permissions);
        // if (this.isUpdate) {
        //     this.subscribers.updatePermission = this.roleService.updatePermissions(this.id, page.pageId, permissions).subscribe();
        // }
    };
    RoleFormComponent.prototype.changeFullPermission = function (page) {
        page.full = !page.full;
        page.view = page.full;
        page.add = page.full;
        page.edit = page.full;
        page.delete = page.full;
        page.report = page.full;
        page.print = page.full;
        page.approve = page.full;
        page.export = page.full;
        // if (this.isUpdate) {
        //     const permissions = this.calculatePermissions(page);
        //     this.subscribers.updatePermission = this.roleService.updatePermissions(this.id, page.pageId, permissions).subscribe();
        // }
    };
    RoleFormComponent.prototype.add = function () {
        this.isUpdate = false;
        this.roleFormModal.open();
    };
    RoleFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (this.selectedPages.length === 0) {
            this.setMessage('danger', 'selectOne');
            return;
        }
        if (isValid) {
            this.role = this.model.value;
            this.role.rolesPagesViewModels = this.mapPermissionToPermissionValue();
            this.role.users = this.listSelectedUsers;
            var isRolePagePermissionValid = this.validatePagePermission(this.role.rolesPagesViewModels);
            if (!isRolePagePermissionValid) {
                this.setMessage('danger', 'inValid');
                return;
            }
            this.isSaving = true;
            this.resetMessage();
            if (this.isUpdate) {
                this.roleService
                    .update(this.id, this.role)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function () {
                    _this.isUpdate = false;
                    _this.isModified = true;
                    _this.closeForm();
                });
            }
            else {
                this.roleService
                    .insert(this.role)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function () {
                    _this.isModified = true;
                    _this.model.reset(new _models_role_model__WEBPACK_IMPORTED_MODULE_2__["Role"]());
                    if (!_this.isCreateAnother) {
                        _this.closeForm();
                    }
                });
            }
        }
    };
    RoleFormComponent.prototype.deletePage = function (page) {
        // if (this.isUpdate) {
        //     this.subscribers.deletePermission = this.roleService.deletePermission(this.id, page.pageId)
        //         .subscribe((result: ActionResultViewModel) => this.removePermission(page.pageId));
        // } else {
        this.removePermission(page.pageId);
        // }
    };
    RoleFormComponent.prototype.showSelectPage = function () {
        var _this = this;
        if (!this.listPages || this.listPages.length === 0) {
            this.subscribers.getListPages = this.pageService.getActivatedPages()
                .subscribe(function (result) { return _this.listPages = result; });
        }
        this.ghmSelectPickerComponent.show();
    };
    RoleFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.utilService.renderFormError(['name', 'description']);
        this.validationMessages = this.utilService.renderFormErrorMessage([
            { 'name': ['required', 'maxlength'] },
            { 'description': ['maxlength'] }
        ]);
        this.model = this.fb.group({
            'name': [this.role.name, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_8__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_8__["Validators"].maxLength(256)
                ]],
            'description': [this.role.description, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_8__["Validators"].maxLength(256)
                ]],
            'concurrencyStamp': [this.role.concurrencyStamp]
        });
        this.subscribers.modelValueChanges =
            this.model.valueChanges.subscribe(function () { return _this.utilService.onValueChanged(_this.model, _this.formErrors, _this.validationMessages); });
    };
    RoleFormComponent.prototype.mapPermissionToPermissionValue = function () {
        var _this = this;
        return this.selectedPages.map(function (page) {
            return {
                pageId: page.pageId,
                pageName: page.pageName,
                idPath: '',
                permissions: _this.calculatePermissions(page)
            };
        });
    };
    RoleFormComponent.prototype.calculatePermissions = function (page) {
        var permissions = 0;
        if (page.view) {
            permissions += _shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_13__["Permission"].view;
        }
        if (page.add) {
            permissions += _shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_13__["Permission"].add;
        }
        if (page.edit) {
            permissions += _shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_13__["Permission"].edit;
        }
        if (page.delete) {
            permissions += _shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_13__["Permission"].delete;
        }
        if (page.export) {
            permissions += _shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_13__["Permission"].export;
        }
        if (page.print) {
            permissions += _shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_13__["Permission"].print;
        }
        if (page.approve) {
            permissions += _shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_13__["Permission"].approve;
        }
        if (page.report) {
            permissions += _shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_13__["Permission"].report;
        }
        return permissions;
    };
    RoleFormComponent.prototype.validatePagePermission = function (pagePermissions) {
        var inValidCount = lodash__WEBPACK_IMPORTED_MODULE_10__["countBy"](pagePermissions, function (pagePermission) {
            return pagePermission.permissions === 0;
        }).true;
        return !inValidCount || inValidCount === 0;
    };
    RoleFormComponent.prototype.removePermission = function (pageId) {
        lodash__WEBPACK_IMPORTED_MODULE_10__["remove"](this.selectedPages, function (selectedPage) {
            return selectedPage.pageId === pageId;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('roleFormModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_7__["NhModalComponent"])
    ], RoleFormComponent.prototype, "roleFormModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_shareds_components_ghm_select_picker_ghm_select_picker_component__WEBPACK_IMPORTED_MODULE_11__["GhmSelectPickerComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_select_picker_ghm_select_picker_component__WEBPACK_IMPORTED_MODULE_11__["GhmSelectPickerComponent"])
    ], RoleFormComponent.prototype, "ghmSelectPickerComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_shareds_components_nh_user_picker_nh_user_picker_component__WEBPACK_IMPORTED_MODULE_15__["NhUserPickerComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_user_picker_nh_user_picker_component__WEBPACK_IMPORTED_MODULE_15__["NhUserPickerComponent"])
    ], RoleFormComponent.prototype, "userPickerComponent", void 0);
    RoleFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-role-form',
            template: __webpack_require__(/*! ./role-form.component.html */ "./src/app/modules/configs/role/role-form/role-form.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_14__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_14__["Router"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_5__["UtilService"],
            _page_page_service__WEBPACK_IMPORTED_MODULE_12__["PageService"],
            _role_service__WEBPACK_IMPORTED_MODULE_4__["RoleService"]])
    ], RoleFormComponent);
    return RoleFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_3__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/configs/role/role.component.html":
/*!**********************************************************!*\
  !*** ./src/app/modules/configs/role/role.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@rolePageTitle\">Roles</span>\r\n    <small i18n=\"@@configModuleTitle\">Configs</small>\r\n</h1>\r\n\r\n<div class=\"row cm-mgb-10\">\r\n    <div class=\"col-sm-12\">\r\n        <form class=\"form-inline\" (ngSubmit)=\"search(1)\">\r\n            <div class=\"form-group\">\r\n                <input type=\"text\" class=\"form-control\" i18n-placeholder=\"@@keywordSearch\"\r\n                       placeholder=\"Enter keyword for search please.\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <ghm-button icon=\"fa fa-search\" classes=\"btn btn-primary\" [loading]=\"isSearching\"></ghm-button>\r\n            </div>\r\n            <div class=\"form-group pull-right\">\r\n                <a *ngIf=\"permission.add\"\r\n                   class=\"btn btn-primary\"\r\n                   routerLink=\"/config/roles/add\"\r\n                   i18n=\"@@add\">\r\n                    Add\r\n                </a>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <table class=\"table table-stripped table-hover\">\r\n            <thead>\r\n            <tr>\r\n                <th class=\"center middle w50\" i18n=\"@@no\">No</th>\r\n                <th class=\"middle\" i18n=\"@@roleName\">Role name</th>\r\n                <th class=\"middle\" i18n=\"@@description\">Description</th>\r\n                <th class=\"middle\" i18n=\"@@roleType\">Role type</th>\r\n                <th class=\"text-right middle w150\" i18n=\"@@action\"\r\n                    *ngIf=\"permission.view || permission.edit || permission.delete\">Action\r\n                </th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            <tr *ngFor=\"let item of listItems$ | async; let i = index\">\r\n                <td class=\"center middle\">{{ (currentPage - 1) * pageSize + i + 1 }}</td>\r\n                <td class=\"middle\"><a href=\"javascript://\" (click)=\"detail(item.id)\">{{ item.name }}</a></td>\r\n                <td class=\"middle\">{{ item.description }}</td>\r\n                <td class=\"middle\" i18n=\"@@roleType\">{item.type, select, 0 {Built in} 1 {Custom}}</td>\r\n                <td class=\"text-right middle\" *ngIf=\"permission.view || permission.edit || permission.delete\">\r\n                    <!--<ghm-logic icon=\"fa fa-eye\" classes=\"btn dark btn-outlet btn-sm\"-->\r\n                                <!--*ngIf=\"item.type !== 0\"-->\r\n                                <!--(clicked)=\"detail(item)\"></ghm-logic>-->\r\n                    <a *ngIf=\"permission.edit && item.type !== 0\"\r\n                       class=\"btn blue btn-sm\"\r\n                       routerLink=\"/config/roles/{{ item.id }}\">\r\n                        <i class=\"fa fa-edit\"></i>\r\n                    </a>\r\n                    <!--<ghm-logic icon=\"fa fa-edit\" classes=\"btn blue btn-outline btn-sm\"-->\r\n                    <!--*ngIf=\"item.type !== 0\"-->\r\n                    <!--(clicked)=\"edit(item)\"></ghm-logic>-->\r\n                    <ghm-button icon=\"fa fa-trash-o\" classes=\"btn red btn-sm\"\r\n                                *ngIf=\"item.type !== 0\"\r\n                                [swal]=\"deleteRole\"\r\n                                (confirm)=\"delete(item.id)\"></ghm-button>\r\n                </td>\r\n            </tr>\r\n            </tbody>\r\n        </table>\r\n        <ghm-paging [totalRows]=\"totalRows\" [currentPage]=\"currentPage\" [pageShow]=\"6\" (pageClick)=\"search($event)\"\r\n                    [isDisabled]=\"isSearching\" [pageName]=\"'quyền'\"></ghm-paging>\r\n    </div>\r\n</div>\r\n\r\n<!--<app-role-form (saveSuccessful)=\"search(1)\"></app-role-form>-->\r\n<!--<app-role-detail></app-role-detail>-->\r\n\r\n<swal\r\n    i18n=\"@@confirmDeleteRole\"\r\n    i18n-title\r\n    i18n-text\r\n    #deleteRole\r\n    title=\"Are you sure for delete this role?\"\r\n    text=\"You can't recover this role after delete.\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n"

/***/ }),

/***/ "./src/app/modules/configs/role/role.component.ts":
/*!********************************************************!*\
  !*** ./src/app/modules/configs/role/role.component.ts ***!
  \********************************************************/
/*! exports provided: RoleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleComponent", function() { return RoleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _role_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./role.service */ "./src/app/modules/configs/role/role.service.ts");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _role_detail_role_detail_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./role-detail/role-detail.component */ "./src/app/modules/configs/role/role-detail/role-detail.component.ts");









var RoleComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](RoleComponent, _super);
    function RoleComponent(route, spinnerService, toastr, roleService) {
        var _this = _super.call(this) || this;
        _this.route = route;
        _this.spinnerService = spinnerService;
        _this.toastr = toastr;
        _this.roleService = roleService;
        return _this;
    }
    RoleComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.appService.setupPage(this.pageId.CONFIG, this.pageId.CONFIG_ROLE, 'Quản lý quyền', 'Danh sách quyền');
        this.listItems$ = this.route.data.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            var data = result.data;
            _this.totalRows = data.totalRows;
            return data.items;
        }));
    };
    RoleComponent.prototype.search = function (currentPage) {
        var _this = this;
        this.currentPage = currentPage;
        this.isSearching = true;
        this.listItems$ = this.roleService.search(this.keyword, this.currentPage)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.isSearching = false; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.totalRows = result.totalRows;
            return result.items;
        }));
    };
    RoleComponent.prototype.delete = function (id) {
        var _this = this;
        this.spinnerService.show('Đang xóa quyền. Vui lòng đợi...');
        this.roleService.delete(id)
            .subscribe(function () {
            _this.search(_this.currentPage);
        });
    };
    RoleComponent.prototype.detail = function (role) {
        this.roleDetailComponent.show(role);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_role_detail_role_detail_component__WEBPACK_IMPORTED_MODULE_8__["RoleDetailComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _role_detail_role_detail_component__WEBPACK_IMPORTED_MODULE_8__["RoleDetailComponent"])
    ], RoleComponent.prototype, "roleDetailComponent", void 0);
    RoleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-role',
            template: __webpack_require__(/*! ./role.component.html */ "./src/app/modules/configs/role/role.component.html"),
            providers: [_role_service__WEBPACK_IMPORTED_MODULE_3__["RoleService"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__["SpinnerService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"],
            _role_service__WEBPACK_IMPORTED_MODULE_3__["RoleService"]])
    ], RoleComponent);
    return RoleComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/configs/system/system.component.html":
/*!**************************************************************!*\
  !*** ./src/app/modules/configs/system/system.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@listWarehousePageTitle\">Cấu hình hệ thống.</span>\r\n    <small i18n=\"@@productModuleTitle\">Cấu hình</small>\r\n</h1>\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <form action=\"\" class=\"form-horizontal\" [formGroup]=\"model\" (ngSubmit)=\"save()\">\r\n            <div class=\"form-body\">\r\n                <div class=\"form-group\">\r\n                    <label class=\"col-md-2 control-label\">Tiêu đề trang</label>\r\n                    <div class=\"col-md-10\">\r\n                        <input type=\"text\" class=\"form-control\" placeholder=\"Vui lòng nhập tiêu đề trang\"\r\n                               formControlName=\"title\">\r\n                        <!--<span class=\"help-block\"> A block of help text. </span>-->\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <label class=\"col-md-2 control-label\">Favicon</label>\r\n                    <div class=\"col-md-10\">\r\n                        <img src=\"\" alt=\"\" class=\"img-responsive\">\r\n                        <ghm-file-upload\r\n                            text=\"Chọn favicon\"\r\n                            (uploaded)=\"onFaviconUploaded($event)\"\r\n                        ></ghm-file-upload>\r\n                        <img src=\"{{ baseFileUrl }}{{ model.value.faviconUrl }}\" alt=\"\">\r\n                        <!--<span class=\"help-block\"> A block of help text. </span>-->\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <label class=\"col-md-2 control-label\">Logo</label>\r\n                    <div class=\"col-md-10\">\r\n                        <ghm-file-upload\r\n                            text=\"Chọn logo\"\r\n                            (uploaded)=\"onLogoUploaded($event)\"\r\n                        ></ghm-file-upload>\r\n                        <img src=\"{{ baseFileUrl }}{{ model.value.logoUrl }}\" alt=\"\">\r\n                        <!--<span class=\"help-block\"> A block of help text. </span>-->\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <div class=\"col-md-10 col-md-offset-2\">\r\n                        <button class=\"btn blue\">\r\n                            Lưu\r\n                        </button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </form><!-- END: form -->\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/configs/system/system.component.ts":
/*!************************************************************!*\
  !*** ./src/app/modules/configs/system/system.component.ts ***!
  \************************************************************/
/*! exports provided: SystemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SystemComponent", function() { return SystemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");





var SystemComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](SystemComponent, _super);
    function SystemComponent(toastr) {
        var _this = _super.call(this) || this;
        _this.toastr = toastr;
        _this.baseFileUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].fileUrl;
        return _this;
    }
    SystemComponent.prototype.ngOnInit = function () {
        this.appService.setupPage(this.pageId.CONFIG, this.pageId.CONFIG_SYSTEM, 'Cấu hình hệ thống.', 'Cấu hình.');
        this.buildForm();
        this.getGeneralSetting();
    };
    SystemComponent.prototype.onFaviconUploaded = function (data) {
        if (data) {
            this.model.patchValue({
                faviconUrl: data[0].url
            });
        }
    };
    SystemComponent.prototype.onLogoUploaded = function (data) {
        if (data) {
            this.model.patchValue({
                logoUrl: data[0].url
            });
        }
    };
    SystemComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.validateModel();
        if (isValid) {
            this.subscribers.saveConfig = this.appService.saveConfig(this.model.value)
                .subscribe(function (result) {
                _this.toastr.success(result.message);
            });
        }
    };
    SystemComponent.prototype.getGeneralSetting = function () {
        var _this = this;
        this.appService.getGeneralSetting()
            .subscribe(function (generalSetting) {
            console.log(generalSetting);
            _this.model.patchValue(generalSetting);
        });
    };
    SystemComponent.prototype.buildForm = function () {
        this.model = this.formBuilder.group({
            title: [],
            faviconUrl: [],
            logoUrl: []
        });
    };
    SystemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-system',
            template: __webpack_require__(/*! ./system.component.html */ "./src/app/modules/configs/system/system.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]])
    ], SystemComponent);
    return SystemComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/configs/tenant/tenant-form.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/modules/configs/tenant/tenant-form.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #tenantFormModal size=\"md\"\r\n          (hidden)=\"tenantFormModalHidden()\"\r\n          (shown)=\"tenantFormModalShown()\">\r\n    <nh-modal-header>\r\n        <i class=\"fa fa-desktop\"></i>\r\n        <span class=\"cm-mgl-5\">{isUpdate, select, 0 {Add new tenant} 1 {Update tenant info}}</span>\r\n    </nh-modal-header>\r\n    <form class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content>\r\n            <mat-tab-group [selectedIndex]=\"0\"\r\n                           (selectedTabChange)=\"onTabChange($event)\">\r\n                <mat-tab i18n-label=\"@@tenantInfo\" label=\"Tenant info\">\r\n                    <div class=\"col-sm-12 cm-mgt-10\">\r\n                        <div class=\"form-group\" [class.has-error]=\"formErrors.name\">\r\n                            <label ghmLabel=\"Tên khách hàng\"\r\n                                   class=\"col-sm-3 control-label\"\r\n                                   [required]=\"true\"\r\n                            ></label>\r\n                            <div class=\"col-sm-9\">\r\n                                <input type=\"text\" value=\"\"\r\n                                       id=\"name\"\r\n                                       class=\"form-control\"\r\n                                       placeholder=\"Nhập tên tenant\"\r\n                                       formControlName=\"name\"/>\r\n                                <span class=\"help-block\" *ngIf=\"formErrors.name\"> {{formErrors.name}} </span>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group\" [class.has-error]=\"formErrors.email\">\r\n                            <label ghmLabel=\"Email\"\r\n                                   class=\"col-sm-3 control-label\"\r\n                                   [required]=\"true\"\r\n                            ></label>\r\n                            <div class=\"col-sm-9\">\r\n                                <input type=\"text\" value=\"\" class=\"form-control\"\r\n                                       placeholder=\"Nhập email\"\r\n                                       formControlName=\"email\"/>\r\n                                <span class=\"help-block\" *ngIf=\"formErrors.email\"> {{formErrors.email}} </span>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group\" [class.has-error]=\"formErrors.phoneNumber\">\r\n                            <label ghmLabel=\"Số điện thoại\"\r\n                                   class=\"col-sm-3 control-label\"\r\n                                   [required]=\"true\"\r\n                            ></label>\r\n                            <div class=\"col-sm-9\">\r\n                                <input type=\"text\" value=\"\" class=\"form-control\"\r\n                                       placeholder=\"Nhập số điện thoại\"\r\n                                       formControlName=\"phoneNumber\"/>\r\n                                <span class=\"help-block\"\r\n                                      *ngIf=\"formErrors.phoneNumber\"> {{formErrors.phoneNumber}} </span>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group\" [class.has-error]=\"formErrors.address\">\r\n                            <label ghmLabel=\"Địa chỉ\"\r\n                                   class=\"col-sm-3 control-label\"\r\n                                   [required]=\"true\"\r\n                            ></label>\r\n                            <div class=\"col-sm-9\">\r\n                    <textarea type=\"text\" rows=\"3\" class=\"form-control\"\r\n                              placeholder=\"Nhập địa chỉ\"\r\n                              formControlName=\"address\">\r\n                    </textarea>\r\n                                <span class=\"help-block\" *ngIf=\"formErrors.address\"> {{formErrors.address}} </span>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group\">\r\n                            <label ghmLabel=\"Ghi chú\"\r\n                                   class=\"col-sm-3 control-label\"\r\n                            ></label>\r\n                            <div class=\"col-sm-9\">\r\n                    <textarea type=\"text\" rows=\"3\" class=\"form-control\"\r\n                              placeholder=\"Nhập ghi chú\"\r\n                              formControlName=\"note\">\r\n                    </textarea>\r\n                                <div class=\"alert alert-danger\" *ngIf=\"formErrors.note\">\r\n                                    {{ formErrors.note }}\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group\">\r\n                            <label i18n-ghmLabel=\"@@supportedLanguages\" ghmLabel=\"Supported languages\"\r\n                                   class=\"col-sm-3 control-label\"\r\n                            ></label>\r\n                            <div class=\"col-sm-9\">\r\n                                <nh-suggestion\r\n                                        [multiple]=\"true\"\r\n                                        [loading]=\"isSearching\"\r\n                                        [sources]=\"languageSuggestions\"\r\n                                        (itemSelected)=\"onLanguageSelected($event)\"\r\n                                        (itemRemoved)=\"onLanguageRemoved($event)\"\r\n                                        (searched)=\"onSearchLanguage($event)\"></nh-suggestion>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group\">\r\n                            <div class=\"col-sm-9 col-sm-offset-3\">\r\n                                <mat-checkbox color=\"primary\" formControlName=\"isActive\"> Kích hoạt</mat-checkbox>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </mat-tab><!-- END: tenant info -->\r\n                <mat-tab i18n-label=\"@@pages\" label=\"Pages\">\r\n\r\n                </mat-tab><!-- END: Tenant pages -->\r\n            </mat-tab-group>\r\n        </nh-modal-content>\r\n        <nh-modal-footer class=\"text-right\">\r\n            <button class=\"btn blue cm-mgr-5\" i18n=\"@@save\" [class.disabled]=\"isSaving\">\r\n                Save\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-light\"\r\n                    nh-dismiss\r\n                    i18n=\"@@cancel\"\r\n                    [class.disabled]=\"isSaving\">\r\n                Cancel\r\n            </button>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/configs/tenant/tenant-form.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/modules/configs/tenant/tenant-form.component.ts ***!
  \*****************************************************************/
/*! exports provided: TenantFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TenantFormComponent", function() { return TenantFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _tenant_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tenant.model */ "./src/app/modules/configs/tenant/tenant.model.ts");
/* harmony import */ var _tenant_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tenant.service */ "./src/app/modules/configs/tenant/tenant.service.ts");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _page_models_page_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../page/models/page.model */ "./src/app/modules/configs/page/models/page.model.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _shareds_services_language_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../shareds/services/language.service */ "./src/app/shareds/services/language.service.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _shareds_constants_pattern_const__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../shareds/constants/pattern.const */ "./src/app/shareds/constants/pattern.const.ts");














var TenantFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TenantFormComponent, _super);
    function TenantFormComponent(fb, utilService, toastr, tenantService, languageService) {
        var _this = _super.call(this) || this;
        _this.fb = fb;
        _this.utilService = utilService;
        _this.toastr = toastr;
        _this.tenantService = tenantService;
        _this.languageService = languageService;
        _this.isSearching = false;
        _this.tenant = new _tenant_model__WEBPACK_IMPORTED_MODULE_2__["Tenant"]();
        _this.languages = [];
        _this.languageSuggestions = [];
        _this.selectedLanguages = [];
        _this.isActive = false;
        _this.isDefault = false;
        return _this;
    }
    TenantFormComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    TenantFormComponent.prototype.onItemSelected = function (language) {
        if (language.id == null) {
            return;
        }
        var languageInfo = lodash__WEBPACK_IMPORTED_MODULE_11__["find"](this.selectedLanguages, function (languageItem) {
            return languageItem.languageId === language.id;
        });
        if (languageInfo) {
            this.toastr.warning('Ngôn ngữ đã tồn tại trong danh sách được chọn.');
            return;
        }
        language.data.isActive = true;
        this.selectedLanguages.push(language.data);
        if (this.isUpdate) {
            this.subscribers.addLanguage = this.tenantService.addLanguage(this.tenant.id, {
                languageId: language.id,
                name: language.name,
                isActive: true,
                isDefault: false
            }).subscribe();
        }
    };
    TenantFormComponent.prototype.onTabChange = function (event) {
        if (event.index === 1) {
        }
    };
    TenantFormComponent.prototype.onSearchLanguage = function (event) {
        var _this = this;
        this.isSearching = true;
        this.subscribers.suggestions = this.languageService.suggestion(event)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["finalize"])(function () { return _this.isSearching = false; }))
            .subscribe(function (result) { return _this.languageSuggestions = result; });
    };
    TenantFormComponent.prototype.onLanguageSelected = function (event) {
        if (event) {
            var selectedLanguage = lodash__WEBPACK_IMPORTED_MODULE_11__["find"](this.selectedLanguages, function (language) {
                return language.id === event.id;
            });
            if (!selectedLanguage) {
                this.selectedLanguages.push({
                    languageId: event.id,
                    name: event.name,
                    isDefault: false,
                    isActive: true
                });
            }
        }
    };
    TenantFormComponent.prototype.onLanguageRemoved = function (event) {
        if (event) {
            lodash__WEBPACK_IMPORTED_MODULE_11__["remove"](this.selectedLanguages, function (language) {
                return language.languageId === event.id;
            });
        }
    };
    TenantFormComponent.prototype.removeLanguage = function (language) {
        var _this = this;
        if (this.isUpdate) {
            this.subscribers.removeLanguage = this.tenantService.removeLanguage(this.tenant.id, language.languageId)
                .subscribe(function (result) {
                _this.removeSelectedLanguage(language);
            });
        }
        else {
            this.removeSelectedLanguage(language);
        }
    };
    TenantFormComponent.prototype.changeLanguageDefaultStatus = function (language) {
        lodash__WEBPACK_IMPORTED_MODULE_11__["each"](this.selectedLanguages, function (selectedLanguage) {
            selectedLanguage.isDefault = false;
        });
        language.isDefault = !language.isDefault;
        if (this.isUpdate) {
            this.tenantService.updateTenantLanguageDefaultStatus(this.tenant.id, language.languageId, language.isDefault)
                .subscribe();
        }
    };
    TenantFormComponent.prototype.changeLanguageActiveStatus = function (language) {
        language.isActive = !language.isActive;
        if (this.isUpdate) {
            this.tenantService.updateTenantLanguageActiveStatus(this.tenant.id, language.languageId, language.isActive)
                .subscribe();
        }
    };
    TenantFormComponent.prototype.changeIsDefault = function () {
        this.isDefault = !this.isDefault;
    };
    TenantFormComponent.prototype.changeIsActive = function () {
        this.isActive = !this.isActive;
    };
    // saveLanguage() {
    //     if (!this.languageId) {
    //         this.toastr.error('Please select at least language');
    //         return;
    //     }
    //
    //     if (this.isUpdate) {
    //         this.tenantService.saveLanguage(this.languageId, this.isDefault, this.isActive)
    //             .subscribe((result: ActionResultViewModel) => {
    //                 this.toastr.success(result.message);
    //             });
    //     } else {
    //         this.selectedLanguages.push({
    //             languageId: this.languageId,
    //             name: this.
    //             isDefault: this.isDefault,
    //             isActive: this.isActive
    //         });
    //     }
    // }
    TenantFormComponent.prototype.tenantFormModalShown = function () {
        var _this = this;
        if (this.languages.length === 0) {
            this.subscribers.getAllLanguages = this.languageService.getAllLanguage()
                .subscribe(function (result) {
                _this.languages = result.map(function (language) {
                    language.isActive = false;
                    return {
                        id: language.languageId,
                        name: language.name,
                        selected: false,
                        data: language
                    };
                });
            });
        }
        this.utilService.focusElement('name');
    };
    TenantFormComponent.prototype.tenantFormModalHidden = function () {
        if (this.isModified) {
            this.saveSuccessful.emit();
        }
    };
    TenantFormComponent.prototype.add = function () {
        this.isUpdate = false;
        this.resetForm();
        this.tenantFormModal.open();
    };
    TenantFormComponent.prototype.edit = function (tenant) {
        var _this = this;
        this.isUpdate = true;
        this.tenant = tenant;
        this.model.patchValue(tenant);
        this.subscribers.getLanguage = this.tenantService.getLanguage(tenant.id)
            .subscribe(function (result) { return _this.selectedLanguages = result; });
        this.tenantFormModal.open();
    };
    TenantFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (this.selectedLanguages.length === 0) {
            this.toastr.error('Vui lòng chọn ít nhất một ngôn ngữ.');
            return;
        }
        var defaultLanguage = lodash__WEBPACK_IMPORTED_MODULE_11__["find"](this.selectedLanguages, function (selectedLanguage) {
            return selectedLanguage.isDefault;
        });
        // if (!defaultLanguage) {
        //     this.toastr.error('Vui lòng chọn ít nhất 1 ngôn ngữ mặc định.');
        //     return;
        // }
        if (isValid) {
            this.tenant = this.model.value;
            this.tenant.languages = this.selectedLanguages.map(function (selectedLanguage) {
                return {
                    languageId: selectedLanguage.languageId,
                    isActive: selectedLanguage.isActive,
                    isDefault: selectedLanguage.isDefault
                };
            });
            this.isSaving = true;
            if (this.isUpdate) {
                this.tenantService.update(this.tenant)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.model.patchValue(new _page_models_page_model__WEBPACK_IMPORTED_MODULE_7__["Page"]());
                    _this.isUpdate = false;
                    _this.isModified = true;
                    _this.tenantFormModal.dismiss();
                    return;
                });
            }
            else {
                this.tenantService.insert(this.tenant)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.utilService.focusElement('pageId');
                    _this.isModified = true;
                    _this.model.reset(new _tenant_model__WEBPACK_IMPORTED_MODULE_2__["Tenant"]());
                    _this.utilService.focusElement('name');
                    return;
                });
            }
        }
    };
    TenantFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.utilService.renderFormError(['name', 'email', 'phoneNumber', 'address', 'note']);
        this.validationMessages = {
            'name': {
                'required': 'Tên trang không được để trống',
                'maxLength': 'Tên trang không được vượt quá 256 ký tự'
            },
            'email': {
                'required': 'Vui lòng nhập email.',
                'maxLength': 'Email không được phép vượt quá 256',
                'pattern': 'Email không đúng định dạng.'
            },
            'phoneNumber': {
                'required': 'Vui lòng nhập số điện thoại.',
                'maxLength': 'Số điện thoại không được phép vượt quá 50 ký tự.',
                'pattern': 'Số điện thoại không đúng định dạng.'
            },
            'address': {
                'required': 'Vui lòng nhập địa chỉ.',
                'maxLength': 'Địa chỉ không được phép vượt quá 500 ký tự.'
            },
            'note': {
                'maxLength': 'Nội dung mô tả không được phép vượt quá 500 ký tự..',
            },
        };
        this.model = this.fb.group({
            'id': [this.tenant.id],
            'name': [this.tenant.name, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].maxLength(256)
                ]],
            'email': [this.tenant.email, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].maxLength(256),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].pattern(_shareds_constants_pattern_const__WEBPACK_IMPORTED_MODULE_13__["Pattern"].email)
                ]],
            'phoneNumber': [this.tenant.phoneNumber, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].maxLength(50),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].pattern(_shareds_constants_pattern_const__WEBPACK_IMPORTED_MODULE_13__["Pattern"].phoneNumber)
                ]],
            'address': [this.tenant.address, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].maxLength(500)
                ]],
            'note': [this.tenant.note, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].maxLength(500)
                ]],
            'isActive': [this.tenant.isActive],
        });
        this.model.valueChanges.subscribe(function (data) {
            return _this.utilService.onValueChanged(_this.model, _this.formErrors, _this.validationMessages, false);
        });
    };
    TenantFormComponent.prototype.removeSelectedLanguage = function (language) {
        lodash__WEBPACK_IMPORTED_MODULE_11__["remove"](this.selectedLanguages, function (selectedLanguage) {
            return selectedLanguage.languageId === language.languageId;
        });
    };
    TenantFormComponent.prototype.resetForm = function () {
        this.model.reset(new _tenant_model__WEBPACK_IMPORTED_MODULE_2__["Tenant"]());
        this.selectedLanguages = [];
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('tenantFormModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_8__["NhModalComponent"])
    ], TenantFormComponent.prototype, "tenantFormModal", void 0);
    TenantFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tenant-form',
            template: __webpack_require__(/*! ./tenant-form.component.html */ "./src/app/modules/configs/tenant/tenant-form.component.html"),
            providers: [_shareds_services_language_service__WEBPACK_IMPORTED_MODULE_10__["LanguageService"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_5__["UtilService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_12__["ToastrService"],
            _tenant_service__WEBPACK_IMPORTED_MODULE_3__["TenantService"],
            _shareds_services_language_service__WEBPACK_IMPORTED_MODULE_10__["LanguageService"]])
    ], TenantFormComponent);
    return TenantFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_4__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/configs/tenant/tenant.component.html":
/*!**************************************************************!*\
  !*** ./src/app/modules/configs/tenant/tenant.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@listTenantTitle\">Tenants</span>\r\n    <small i18n=\"@@ConfigModuleTitle\">Config module</small>\r\n</h1>\r\n\r\n<div class=\"row cm-mgb-10\">\r\n    <div class=\"col-sm-12\">\r\n        <form action=\"\" class=\"form-inline\" (ngSubmit)=\"search(1)\">\r\n            <div class=\"form-group\">\r\n                <input type=\"text\"\r\n                       i18n-placeholder=\"@@enterKeywordPlaceholder\"\r\n                       placeholder=\"Enter keyword\" class=\"form-control cm-mgr-5\"\r\n                       name=\"keyword\" [(ngModel)]=\"keyword\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <ghm-button classes=\"btn blue\" icon=\"fa fa-search\"\r\n                            i18n-matTooltip=\"@@search\"\r\n                            matTooltip=\"Search\"\r\n                            [loading]=\"isSearching\"></ghm-button>\r\n            </div>\r\n            <div class=\"form-group pull-right\">\r\n                <ghm-button classes=\"btn blue\" type=\"button\" (clicked)=\"add()\" i18n=\"@@add\">\r\n                    Add\r\n                </ghm-button>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <table class=\"table table-striped table-hover\">\r\n            <thead>\r\n            <tr>\r\n                <th class=\"center middle w50\" i18n=\"@@no\">No</th>\r\n                <th class=\"center middle\" i18n=\"@@tenantName\">Tenant name</th>\r\n                <th class=\"center middle w150\" i18n=\"@@email\">Email</th>\r\n                <th class=\"center middle w100\" i18n=\"@@phoneNumber\">Phone number</th>\r\n                <th class=\"center middle w100\" i18n=\"@@status\">Status</th>\r\n                <th class=\"center middle w50\" *ngIf=\"permission.edit\" i18n=\"@@actions\">Actions</th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            <tr *ngFor=\"let item of listItems$ | async; let i = index\">\r\n                <td class=\"center middle\">{{ (currentPage - 1) * pageSize + i + 1 }}</td>\r\n                <td class=\"middle\">{{ item.name }}</td>\r\n                <td class=\"middle\">{{ item.email }}</td>\r\n                <td class=\"middle\">{{ item.phoneNumber }}</td>\r\n                <td class=\"middle center\">\r\n                    <span class=\"badge\"\r\n                          [class.badge-danger]=\"!item.isActive\"\r\n                          [class.badge-success]=\"item.isActive\">\r\n                        {item.isActive, select, 0 {InActive} 1 {Activated}}\r\n                    </span>\r\n                </td>\r\n                <td class=\"center middle\" *ngIf=\"permission.edit\">\r\n                    <ghm-button type=\"button\" classes=\"btn blue btn-sm\"\r\n                                icon=\"fa fa-edit\"\r\n                                i18n-matTooltip=\"@@edit\"\r\n                                matTooltip=\"Edit\"\r\n                                (click)=\"edit(item)\">\r\n                    </ghm-button>\r\n                </td>\r\n            </tr>\r\n            </tbody>\r\n        </table>\r\n\r\n        <ghm-paging [totalRows]=\"totalRows\" [currentPage]=\"currentPage\" [pageShow]=\"6\" (pageClick)=\"search($event)\"\r\n                    [isDisabled]=\"isSearching\" [pageName]=\"'tenant'\"></ghm-paging>\r\n    </div>\r\n</div>\r\n\r\n<app-tenant-form (saveSuccessful)=\"search(1)\"></app-tenant-form>\r\n"

/***/ }),

/***/ "./src/app/modules/configs/tenant/tenant.component.ts":
/*!************************************************************!*\
  !*** ./src/app/modules/configs/tenant/tenant.component.ts ***!
  \************************************************************/
/*! exports provided: TenantComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TenantComponent", function() { return TenantComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _tenant_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./tenant.service */ "./src/app/modules/configs/tenant/tenant.service.ts");
/* harmony import */ var _tenant_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tenant-form.component */ "./src/app/modules/configs/tenant/tenant-form.component.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");







var TenantComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TenantComponent, _super);
    function TenantComponent(toastr, tenantService) {
        var _this = _super.call(this) || this;
        _this.toastr = toastr;
        _this.tenantService = tenantService;
        return _this;
    }
    TenantComponent.prototype.ngOnInit = function () {
        this.appService.setupPage(this.pageId.CONFIG, this.pageId.CONFIG_TENANT, 'Quản lý Tenant', 'Danh sách tenant');
        this.search(1);
    };
    TenantComponent.prototype.search = function (currentPage) {
        var _this = this;
        this.currentPage = currentPage;
        this.isSearching = true;
        this.listItems$ = this.tenantService.search(this.keyword, this.isActive, this.currentPage, this.pageSize)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.isSearching = false; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.totalRows = result.totalRows;
            return result.items;
        }));
    };
    TenantComponent.prototype.add = function () {
        this.tenantFormComponent.add();
    };
    TenantComponent.prototype.edit = function (tenant) {
        this.tenantFormComponent.edit(tenant);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_tenant_form_component__WEBPACK_IMPORTED_MODULE_5__["TenantFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _tenant_form_component__WEBPACK_IMPORTED_MODULE_5__["TenantFormComponent"])
    ], TenantComponent.prototype, "tenantFormComponent", void 0);
    TenantComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tenant',
            template: __webpack_require__(/*! ./tenant.component.html */ "./src/app/modules/configs/tenant/tenant.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            _tenant_service__WEBPACK_IMPORTED_MODULE_4__["TenantService"]])
    ], TenantComponent);
    return TenantComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/configs/tenant/tenant.model.ts":
/*!********************************************************!*\
  !*** ./src/app/modules/configs/tenant/tenant.model.ts ***!
  \********************************************************/
/*! exports provided: Tenant */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tenant", function() { return Tenant; });
var Tenant = /** @class */ (function () {
    function Tenant() {
        this.isActive = true;
    }
    return Tenant;
}());



/***/ }),

/***/ "./src/app/modules/configs/tenant/tenant.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/modules/configs/tenant/tenant.service.ts ***!
  \**********************************************************/
/*! exports provided: TenantService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TenantService", function() { return TenantService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _shareds_services_app_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../shareds/services/app.service */ "./src/app/shareds/services/app.service.ts");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");








var TenantService = /** @class */ (function () {
    function TenantService(appConfig, http, spinnerService, appService) {
        this.http = http;
        this.spinnerService = spinnerService;
        this.appService = appService;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].apiGatewayUrl + "api/v1/core/tenants";
        this.url = "" + appConfig.API_GATEWAY_URL + this.url;
    }
    TenantService.prototype.search = function (keyword, isActive, page, pageSize) {
        if (pageSize === void 0) { pageSize = 1; }
        return this.http.get(this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('isActive', isActive != null && isActive !== undefined ? keyword : '')
                .set('page', page ? page.toString() : '')
                .set('pageSize', pageSize ? pageSize.toString() : '')
        });
    };
    TenantService.prototype.insert = function (tenant) {
        return this.http.post(this.url, tenant);
    };
    TenantService.prototype.update = function (tenant) {
        var _this = this;
        return this.http.post(this.url + "/" + tenant.id, {
            name: tenant.name,
            isActive: tenant.isActive,
            phoneNumber: tenant.phoneNumber,
            logo: tenant.logo,
            email: tenant.email,
            address: tenant.address,
            note: tenant.note,
            languages: tenant.languages
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            _this.appService.showActionResultMessage(result);
            return result;
        }));
    };
    TenantService.prototype.updateActiveStatus = function (id, isActive) {
        var _this = this;
        return this.http.post(this.url + "/" + id + "/" + isActive, {})
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            _this.appService.showActionResultMessage(result);
            return result;
        }));
    };
    TenantService.prototype.getLanguage = function (tenantId) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/" + tenantId + "/languages")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return result.items;
        }));
    };
    TenantService.prototype.addLanguage = function (tenantId, language) {
        var _this = this;
        return this.http.post(this.url + "/" + tenantId + "/languages", language)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            _this.appService.showActionResultMessage(result);
            return result;
        }));
    };
    TenantService.prototype.removeLanguage = function (tenantId, languageId) {
        var _this = this;
        return this.http.delete(this.url + "/" + tenantId + "/languages/" + languageId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            _this.appService.showActionResultMessage(result);
            return result;
        }));
    };
    TenantService.prototype.updateTenantLanguageActiveStatus = function (tenantId, languageId, isActive) {
        var _this = this;
        return this.http.get(this.url + "/" + tenantId + "/language/" + languageId + "/active/" + isActive)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            _this.appService.showActionResultMessage(result);
            return result;
        }));
    };
    TenantService.prototype.updateTenantLanguageDefaultStatus = function (tenantId, languageId, isDefault) {
        var _this = this;
        return this.http.get(this.url + "/" + tenantId + "/language/" + languageId + "/default/" + isDefault)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            _this.appService.showActionResultMessage(result);
            return result;
        }));
    };
    TenantService.prototype.saveLanguage = function (languageId, isDefault, isActive) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .post(this.url + "/languages", {
            languageId: languageId,
            isDefalt: isDefault,
            isActive: isActive
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    TenantService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_3__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_6__["SpinnerService"],
            _shareds_services_app_service__WEBPACK_IMPORTED_MODULE_5__["AppService"]])
    ], TenantService);
    return TenantService;
}());



/***/ }),

/***/ "./src/app/modules/configs/user-setting/user-setting.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/modules/configs/user-setting/user-setting.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!DOCTYPE html>\r\n<html lang=\"en\">\r\n<head>\r\n    <meta charset=\"UTF-8\">\r\n    <title>Title</title>\r\n</head>\r\n<body>\r\n\r\n</body>\r\n</html>\r\n"

/***/ }),

/***/ "./src/app/modules/configs/user-setting/user-setting.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/modules/configs/user-setting/user-setting.component.ts ***!
  \************************************************************************/
/*! exports provided: UserSettingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserSettingComponent", function() { return UserSettingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var UserSettingComponent = /** @class */ (function () {
    function UserSettingComponent() {
    }
    UserSettingComponent.prototype.ngOnInit = function () {
    };
    UserSettingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user-setting',
            template: __webpack_require__(/*! ./user-setting.component.html */ "./src/app/modules/configs/user-setting/user-setting.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], UserSettingComponent);
    return UserSettingComponent;
}());



/***/ }),

/***/ "./src/app/shareds/components/nh-image/nh-image.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/shareds/components/nh-image/nh-image.component.ts ***!
  \*******************************************************************/
/*! exports provided: NhImageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NhImageComponent", function() { return NhImageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");



var NhImageComponent = /** @class */ (function () {
    function NhImageComponent() {
        this.cssClass = 'img-circle';
        this.mode = 'crop';
        this.width = 40;
        this.height = 40;
        this.errorImageUrl = '/assets/images/noavatar.png';
        this.baseUrl = '';
        this.propagateChange = function () {
        };
    }
    NhImageComponent_1 = NhImageComponent;
    Object.defineProperty(NhImageComponent.prototype, "value", {
        get: function () {
            return this._value;
        },
        set: function (value) {
            this._value = value;
        },
        enumerable: true,
        configurable: true
    });
    NhImageComponent.prototype.ngOnInit = function () {
    };
    NhImageComponent.prototype.onImageError = function () {
        this.value = this.errorImageUrl;
    };
    NhImageComponent.prototype.registerOnChange = function (fn) {
        this.propagateChange = fn;
    };
    NhImageComponent.prototype.writeValue = function (value) {
        this.value = value;
    };
    NhImageComponent.prototype.registerOnTouched = function () {
    };
    var NhImageComponent_1;
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhImageComponent.prototype, "alt", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhImageComponent.prototype, "cssClass", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhImageComponent.prototype, "mode", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhImageComponent.prototype, "width", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhImageComponent.prototype, "height", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhImageComponent.prototype, "errorImageUrl", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhImageComponent.prototype, "baseUrl", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
    ], NhImageComponent.prototype, "value", null);
    NhImageComponent = NhImageComponent_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'nh-image',
            template: "\n        <img alt=\"\" [class]=\"cssClass\"\n             src=\"{{ value }}\"\n             alt=\"{{ alt }}\"\n             (error)=\"onImageError()\"/>\n    ",
            providers: [
                { provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALUE_ACCESSOR"], useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () { return NhImageComponent_1; }), multi: true }
            ],
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NhImageComponent);
    return NhImageComponent;
}());



/***/ }),

/***/ "./src/app/shareds/components/nh-image/nh-image.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/shareds/components/nh-image/nh-image.module.ts ***!
  \****************************************************************/
/*! exports provided: NhImageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NhImageModule", function() { return NhImageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _nh_image_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./nh-image.component */ "./src/app/shareds/components/nh-image/nh-image.component.ts");

/**
 * Created by HoangNH on 3/2/2017.
 */



var NhImageModule = /** @class */ (function () {
    function NhImageModule() {
    }
    NhImageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]],
            declarations: [_nh_image_component__WEBPACK_IMPORTED_MODULE_3__["NhImageComponent"]],
            exports: [_nh_image_component__WEBPACK_IMPORTED_MODULE_3__["NhImageComponent"]]
        })
    ], NhImageModule);
    return NhImageModule;
}());



/***/ }),

/***/ "./src/app/shareds/components/tinymce/tinymce.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/shareds/components/tinymce/tinymce.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "div.tinymce-editor {\n  height: auto !important; }\n  div.tinymce-editor p {\n    margin: 0 0 !important; }\n  .mce-fullscreen {\n  margin-top: 50px !important;\n  border: 0;\n  padding: 0;\n  margin: 0;\n  overflow: hidden;\n  height: 100%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL3RpbnltY2UvRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFwcFxcc2hhcmVkc1xcY29tcG9uZW50c1xcdGlueW1jZVxcdGlueW1jZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHVCQUF1QixFQUFBO0VBRDNCO0lBR1Esc0JBQXNCLEVBQUE7RUFJOUI7RUFDSSwyQkFBMkI7RUFDM0IsU0FBUztFQUNULFVBQVU7RUFDVixTQUFTO0VBQ1QsZ0JBQWdCO0VBQ2hCLFlBQVksRUFBQSIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZHMvY29tcG9uZW50cy90aW55bWNlL3RpbnltY2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJkaXYudGlueW1jZS1lZGl0b3Ige1xyXG4gICAgaGVpZ2h0OiBhdXRvICFpbXBvcnRhbnQ7XHJcbiAgICBwIHtcclxuICAgICAgICBtYXJnaW46IDAgMCAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG59XHJcblxyXG4ubWNlLWZ1bGxzY3JlZW4ge1xyXG4gICAgbWFyZ2luLXRvcDogNTBweCAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyOiAwO1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/shareds/components/tinymce/tinymce.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/shareds/components/tinymce/tinymce.component.ts ***!
  \*****************************************************************/
/*! exports provided: TinymceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TinymceComponent", function() { return TinymceComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");



var TinymceComponent = /** @class */ (function () {
    function TinymceComponent() {
        this.inline = false;
        this.menu = {
            file: { title: 'File', items: 'newdocument | print' },
            edit: { title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall' },
            // insert: {title: 'Insert', items: 'link media | template hr '},
            view: { title: 'View', items: 'visualaid | preview | fullscreen ' },
            format: {
                title: 'Format',
                items: 'bold italic underline strikethrough superscript subscript | formats | removeformat '
            },
            table: { title: 'Table', items: 'inserttable tableprops deletetable | cell row column' },
            tools: { title: 'Tools', items: 'code ' }
        };
        this.onEditorKeyup = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onBlur = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.propagateChange = function () {
        };
    }
    TinymceComponent_1 = TinymceComponent;
    Object.defineProperty(TinymceComponent.prototype, "content", {
        get: function () {
            return this._content;
        },
        set: function (val) {
            this._content = val;
        },
        enumerable: true,
        configurable: true
    });
    TinymceComponent.prototype.ngAfterViewInit = function () {
        this.initEditor();
    };
    TinymceComponent.prototype.ngOnDestroy = function () {
        tinymce.remove("" + this.elementId);
    };
    TinymceComponent.prototype.initEditor = function () {
        var _this = this;
        setTimeout(function () {
            tinymce.remove("#" + _this.elementId);
            tinymce.init({
                selector: "#" + _this.elementId,
                plugins: ['fullscreen', 'link', 'autolink', 'paste', 'image', 'table', 'textcolor', 'print', 'preview', 'spellchecker',
                    'colorpicker', 'fullscreen', 'code', 'lists', 'emoticons', 'wordcount'],
                toolbar: 'insertfile undo redo | | fontselect | fontsizeselect | bold italic ' +
                    '| alignleft aligncenter alignright alignjustify ' +
                    '| bullist numlist outdent indent | link image | fullscreen',
                fontsize_formats: '8pt 9pt 10pt 11pt 12pt 13pt 14pt 18pt 24pt 36pt',
                skin_url: '/assets/skins/lightgray',
                menu: _this.menu,
                inline: _this.inline,
                setup: function (editor) {
                    _this.editor = editor;
                    editor.on('keyup', function (event) {
                        var content = editor.getContent();
                        _this.content = content;
                        _this.propagateChange(content);
                        _this.onEditorKeyup.emit({
                            text: editor.getContent({ format: 'text' }),
                            content: _this.content
                        });
                    });
                    editor.on('change', function (event) {
                        var contentChange = editor.getContent();
                        _this.content = contentChange;
                        _this.propagateChange(_this.content);
                        _this.onChange.emit({
                            text: editor.getContent({ format: 'text' }),
                            content: _this.content
                        });
                    });
                    editor.on('blur', function (event) {
                        var contentChange = editor.getContent();
                        _this.content = contentChange;
                        _this.propagateChange(_this.content);
                        _this.onBlur.emit({
                            text: editor.getContent({ format: 'text' }),
                            content: _this.content
                        });
                    });
                }
            });
        });
    };
    TinymceComponent.prototype.setContent = function (content) {
        this.content = content;
        var editor = tinymce.get(this.elementId);
        if (editor != null) {
            editor.setContent(this.content != null ? this.content : '');
        }
    };
    TinymceComponent.prototype.append = function (data, editorId) {
        var editor = !editorId ? tinymce.get(this.elementId) : tinymce.get(editorId);
        if (editor != null) {
            editor.execCommand('mceInsertContent', false, data);
        }
    };
    TinymceComponent.prototype.registerOnChange = function (fn) {
        this.propagateChange = fn;
    };
    TinymceComponent.prototype.destroy = function () {
        tinymce.remove("#" + this.elementId);
    };
    TinymceComponent.prototype.writeValue = function (value) {
        this.content = value;
        var editor = tinymce.get(this.elementId);
        this.initEditor();
    };
    TinymceComponent.prototype.registerOnTouched = function () {
    };
    var TinymceComponent_1;
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], TinymceComponent.prototype, "elementId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], TinymceComponent.prototype, "height", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TinymceComponent.prototype, "inline", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TinymceComponent.prototype, "menu", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TinymceComponent.prototype, "onEditorKeyup", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TinymceComponent.prototype, "onChange", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TinymceComponent.prototype, "onBlur", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
    ], TinymceComponent.prototype, "content", null);
    TinymceComponent = TinymceComponent_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            selector: 'tinymce',
            template: "\n        <div class=\"form-control tinymce-editor\" id=\"{{elementId}}\" *ngIf=\"inline\"\n             [ngStyle]=\"{'height': height + 'px'}\">\n            <span [innerHTML]=\"content\"></span>\n        </div>\n        <textarea *ngIf=\"!inline\" id=\"{{elementId}}\" [ngStyle]=\"{'height': height + 'px'}\"\n                  value=\"{{content}}\"></textarea>\n    ",
            providers: [
                { provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALUE_ACCESSOR"], useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () { return TinymceComponent_1; }), multi: true }
            ],
            styles: [__webpack_require__(/*! ./tinymce.component.scss */ "./src/app/shareds/components/tinymce/tinymce.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TinymceComponent);
    return TinymceComponent;
}());



/***/ }),

/***/ "./src/app/shareds/components/tinymce/tinymce.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/shareds/components/tinymce/tinymce.module.ts ***!
  \**************************************************************/
/*! exports provided: TinymceModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TinymceModule", function() { return TinymceModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _tinymce_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tinymce.component */ "./src/app/shareds/components/tinymce/tinymce.component.ts");




var TinymceModule = /** @class */ (function () {
    function TinymceModule() {
    }
    TinymceModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]],
            exports: [_tinymce_component__WEBPACK_IMPORTED_MODULE_3__["TinymceComponent"]],
            declarations: [_tinymce_component__WEBPACK_IMPORTED_MODULE_3__["TinymceComponent"]],
            providers: [],
        })
    ], TinymceModule);
    return TinymceModule;
}());



/***/ }),

/***/ "./src/app/shareds/constants/pattern.const.ts":
/*!****************************************************!*\
  !*** ./src/app/shareds/constants/pattern.const.ts ***!
  \****************************************************/
/*! exports provided: Pattern */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Pattern", function() { return Pattern; });
var Pattern = {
    phoneNumber: '^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$',
    email: '^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$',
    whiteSpace: '.*\\S.*'
};


/***/ }),

/***/ "./src/app/shareds/services/language.service.ts":
/*!******************************************************!*\
  !*** ./src/app/shareds/services/language.service.ts ***!
  \******************************************************/
/*! exports provided: LanguageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LanguageService", function() { return LanguageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");








var LanguageService = /** @class */ (function () {
    function LanguageService(appConfig, spinnerService, toastr, http) {
        this.spinnerService = spinnerService;
        this.toastr = toastr;
        this.http = http;
        this.url = 'api/v1/core/languages';
        this.url = "" + appConfig.API_GATEWAY_URL + this.url;
    }
    LanguageService.prototype.getListSupportedLanguage = function () {
        if (localStorage) {
            var language = localStorage.getItem('_lang');
            if (!language) {
                return this.http.get(this.url);
            }
            else {
                var languages = JSON.parse(language);
                return new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](languages);
            }
        }
        else {
            return this.http.get(this.url);
        }
    };
    LanguageService.prototype.getAllLanguage = function () {
        return this.http.get(this.url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) { return result.items; }));
    };
    LanguageService.prototype.getALlLanguage = function () {
        return this.http.get(this.url);
    };
    LanguageService.prototype.search = function () {
        var _this = this;
        this.spinnerService.show();
        return this.http.get("" + this.url, {})
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            return result;
        }));
    };
    LanguageService.prototype.insert = function (language) {
        var _this = this;
        return this.http.post(this.url + "/tenant", {
            languageId: language.languageId,
            isActive: language.isActive,
            isDefault: language.isDefault,
            name: language.name,
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    LanguageService.prototype.updateStatus = function (languageId, isActive) {
        var _this = this;
        return this.http.post(this.url + "/tenant/" + languageId + "/active", {}, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
                .set('isActive', isActive.toString())
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    LanguageService.prototype.updateDefault = function (languageId, isDefault) {
        var _this = this;
        return this.http.post(this.url + "/tenant/" + languageId + "/default", {}, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
                .set('isDefault', isDefault.toString())
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    LanguageService.prototype.delete = function (languageId) {
        var _this = this;
        return this.http.delete(this.url + "/tenant/" + languageId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    LanguageService.prototype.suggestion = function (keyword) {
        return this.http
            .get(this.url + "/suggestions", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            return result.items;
        }));
    };
    LanguageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_6__["SpinnerService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], LanguageService);
    return LanguageService;
}());



/***/ })

}]);
//# sourceMappingURL=modules-configs-config-module.js.map