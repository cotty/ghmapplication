(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-hr-assessment-assessment-module"],{

/***/ "./src/app/modules/hr/assessment/approve/approve.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/modules/hr/assessment/approve/approve.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaHIvYXNzZXNzbWVudC9hcHByb3ZlL2FwcHJvdmUuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/modules/hr/assessment/approve/approve.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/modules/hr/assessment/approve/approve.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@listWarehousePageTitle\">Duyệt đánh giá hiệu quả công việc</span>\r\n    <small i18n=\"@@productModuleTitle\">Đánh giá hiệu quả công việc</small>\r\n</h1>\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <form class=\"form-inline\" (ngSubmit)=\"search(1)\">\r\n            <div class=\"form-group cm-mgr-5\">\r\n                <ghm-select [data]=\"listMonths\"\r\n                            [liveSearch]=\"true\"\r\n                            [selectedItem]=\"{id: month, name: month}\"\r\n                            title=\"-- Chọn năm --\"\r\n                            (itemSelected)=\"month = $event.id\"></ghm-select>\r\n            </div>\r\n            <div class=\"form-group cm-mgr-5\">\r\n                <ghm-select [data]=\"listYears\"\r\n                            [liveSearch]=\"\"\r\n                            [selectedItem]=\"{id: year, name: year}\"\r\n                            title=\"-- Chọn năm --\"\r\n                            (itemSelected)=\"year = $event.id\"></ghm-select>\r\n            </div>\r\n            <div class=\"form-group cm-mgr-5\">\r\n                <ghm-select\r\n                    title=\"-- Lọc theo --\"\r\n                    [data]=\"listTypes\"\r\n                    [selectedItem]=\"{id: type, name: type === 1 ? 'Nhân viên do tôi quản lý trực tiếp' : 'Nhân viên do tôi phê duyệt'}\"\r\n                    (itemSelected)=\"onTypeSelect($event)\"></ghm-select>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <ghm-input [icon]=\"'fa fa-search'\" placeholder=\"Nhập tên nhân viên cần tìm.\"\r\n                           name=\"keyword\"\r\n                           (remove)=\"search(1)\"\r\n                           [(ngModel)]=\"keyword\"></ghm-input>\r\n            </div>\r\n            <div class=\"form-group cm-mgr-5\">\r\n                <button class=\"btn blue\">\r\n                    <i class=\"fa fa-search\"></i>\r\n                </button>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</div>\r\n\r\n<app-assessment-table\r\n    [data]=\"listItems\"\r\n    [type]=\"type\"\r\n></app-assessment-table>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/assessment/approve/approve.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/modules/hr/assessment/approve/approve.component.ts ***!
  \********************************************************************/
/*! exports provided: ApproveComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApproveComponent", function() { return ApproveComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _services_user_assessment_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/user-assessment.service */ "./src/app/modules/hr/assessment/services/user-assessment.service.ts");
/* harmony import */ var _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../register-form/register-form.component */ "./src/app/modules/hr/assessment/register-form/register-form.component.ts");





var ApproveComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](ApproveComponent, _super);
    function ApproveComponent(userAssessmentService) {
        var _this = _super.call(this) || this;
        _this.userAssessmentService = userAssessmentService;
        _this.type = _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_4__["AssessmentType"].managerApprove;
        _this.listTypes = [
            { id: _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_4__["AssessmentType"].managerApprove, name: 'Nhân viên do tôi quản lý trực tiếp' },
            { id: _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_4__["AssessmentType"].approverApprove, name: 'Nhân viên do tôi phê duyệt' }
        ];
        _this.listMonths = [];
        _this.listYears = [];
        _this.month = new Date().getMonth() + 1;
        _this.year = new Date().getFullYear();
        _this.renderListMonth();
        _this.renderListYear();
        _this.search(1);
        return _this;
    }
    ApproveComponent.prototype.ngOnInit = function () {
        this.appService.setupPage(this.pageId.ASSESSMENT, this.pageId.ASSESSMENT_APPROVE, 'Duyệt đánh giá hiệu quả công việc', ' Đánh giá hiệu quả công việc');
        this.search(1);
    };
    ApproveComponent.prototype.onTypeSelect = function (type) {
        if (this.type == type.id) {
            return;
        }
        this.type = type.id;
        this.search(1);
    };
    ApproveComponent.prototype.search = function (currentPage) {
        var _this = this;
        this.currentPage = currentPage;
        if (this.type === 1) {
            this.userAssessmentService.managerSearch(this.keyword, this.month, this.year, this.currentPage, this.pageSize)
                .subscribe(function (result) {
                _this.totalRows = result.totalRows;
                _this.listItems = result.items;
            });
        }
        else {
            this.userAssessmentService.approverSearch(this.keyword, this.month, this.year, this.currentPage, this.pageSize)
                .subscribe(function (result) {
                _this.totalRows = result.totalRows;
                _this.listItems = result.items;
            });
        }
    };
    ApproveComponent.prototype.renderListMonth = function () {
        for (var i = 1; i <= 12; i++) {
            this.listMonths = this.listMonths.concat([{
                    id: i, name: i.toString()
                }]);
        }
    };
    ApproveComponent.prototype.renderListYear = function () {
        var year = 2018;
        var diff = new Date().getFullYear() - year;
        for (var i = 0; i <= diff; i++) {
            var currentYear = year + i;
            this.listYears = this.listYears.concat([{
                    id: currentYear, name: currentYear.toString()
                }]);
        }
    };
    ApproveComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-approve',
            template: __webpack_require__(/*! ./approve.component.html */ "./src/app/modules/hr/assessment/approve/approve.component.html"),
            providers: [_services_user_assessment_service__WEBPACK_IMPORTED_MODULE_3__["UserAssessmentService"]],
            styles: [__webpack_require__(/*! ./approve.component.css */ "./src/app/modules/hr/assessment/approve/approve.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_user_assessment_service__WEBPACK_IMPORTED_MODULE_3__["UserAssessmentService"]])
    ], ApproveComponent);
    return ApproveComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/assessment/assessment-detail/assessment-detail.component.html":
/*!******************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/assessment-detail/assessment-detail.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row cm-mgb-20\">\r\n    <div class=\"col-sm-12\">\r\n        <h4 class=\"star-title\">Đăng ký đánh giá hiệu quả công việc tháng {{ month }} năm {{ year }}</h4>\r\n        <div class=\"star-title-decoration\">\r\n            <i class=\"fa fa-star-o\"></i>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<form action=\"\" class=\"form-horizontal cm-mgb-10\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"control-label col-md-3\" ghmLabel=\"Họ và tên\"></label>\r\n                        <div class=\"col-md-9\">\r\n                            <div class=\"form-control\" readonly>{{ userAssessment?.fullName }}</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <!--/span-->\r\n                <div class=\"col-md-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"control-label col-md-3\" ghmLabel=\"Chức vụ\"></label>\r\n                        <div class=\"col-md-9\">\r\n                            <div class=\"form-control\" readonly>{{ userAssessment?.positionName }}</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <!--/span-->\r\n                <div class=\"col-md-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"control-label col-md-3\" ghmLabel=\"Bộ phần\"></label>\r\n                        <div class=\"col-md-9\">\r\n                            <div class=\"form-control\" readonly>{{ userAssessment?.officeName }}</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <!--/span-->\r\n                <div class=\"col-md-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"control-label col-md-3\" ghmLabel=\"Trạng thái\"></label>\r\n                        <div class=\"col-md-9\">\r\n                            <div class=\"form-control\" readonly>\r\n                                {{ userAssessment?.statusName }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <!--/span-->\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <div class=\"table-responsive\">\r\n            <table class=\"table table-bordered table-stripped table-hover\">\r\n                <thead class=\"bg-primary\">\r\n                <tr>\r\n                    <th rowspan=\"2\" class=\"center middle w50\">STT</th>\r\n                    <th rowspan=\"2\" class=\"center middle w250\">Nội dung đánh giá</th>\r\n                    <th colspan=\"4\" class=\"center middle\">Điểm</th>\r\n                    <th rowspan=\"2\" class=\"center middle w250\">Chỉ tiêu đánh giá tháng/tiêu chuẩn</th>\r\n                    <th rowspan=\"2\" class=\"center middle w250\">Quy định chế tài</th>\r\n                    <th rowspan=\"2\" class=\"center middle w250\">Ghi chú</th>\r\n                    <th rowspan=\"2\" class=\"w50 center\"></th>\r\n                </tr>\r\n                <tr>\r\n                    <th class=\"w100 center\">Chuẩn</th>\r\n                    <th class=\"w100 center\">Tự đánh giá</th>\r\n                    <th class=\"w100 center\">QLTT</th>\r\n                    <th class=\"w100 center\">QLPD</th>\r\n                </tr>\r\n                </thead>\r\n                <tbody>\r\n                <ng-container *ngFor=\"let group of criteriaGroups; let i = index\">\r\n                    <tr class=\"bg-success\">\r\n                        <td class=\"center\">{{ (i + 1) }}</td>\r\n                        <td>{{ group.groupName }}</td>\r\n                        <td class=\"text-right middle\">{{ group.totalPoint }}</td>\r\n                        <td class=\"text-right middle\">{{ group.totalUserPoint }}</td>\r\n                        <td class=\"text-right middle\">{{ group.totalManagerPoint }}</td>\r\n                        <td class=\"text-right middle\">{{ group.totalApproverPoint }}</td>\r\n                        <td colspan=\"4\"></td>\r\n                    </tr>\r\n                    <tr *ngFor=\"let criteria of group.criterias; let j = index\">\r\n                        <td class=\"center middle\">{{ (j + 1) }}</td>\r\n                        <td>{{ criteria?.criteriaName }}</td>\r\n                        <td class=\"text-right middle\">\r\n                            {{ criteria?.point }}\r\n                        </td>\r\n                        <td class=\"text-right middle\">\r\n                            {{ criteria?.userPoint}}\r\n                        </td>\r\n                        <td class=\"text-right middle\">\r\n                            {{ criteria?.managerPoint }}\r\n                        </td>\r\n                        <td class=\"text-right middle\">\r\n                            {{ criteria?.approverPoint }}\r\n                        </td>\r\n                        <td>{{ criteria?.description }}</td>\r\n                        <td>{{ criteria?.sanction }}</td>\r\n                        <td class=\"center\">\r\n                            {{ criteria?.note }}\r\n                        </td>\r\n                        <td class=\"center\">\r\n                            <button type=\"button\" class=\"btn blue btn-sm\">\r\n                                <i class=\"fa fa-commenting-o\"></i>\r\n                            </button>\r\n                        </td>\r\n                    </tr>\r\n                </ng-container>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class=\"row cm-mgt-10\">\r\n    <div class=\"col-sm-12 center\">\r\n        <button type=\"button\" class=\"btn btn-primary cm-mgr-5\"\r\n                *ngIf=\"type === assessmentType.userRegister  && (userAssessment?.status === assessmentStatus.new\r\n                || userAssessment?.status === assessmentStatus.managerDecline)\"\r\n                [swal]=\"confirmSendAssessment\"\r\n                (confirm)=\"approve()\">\r\n            Gửi\r\n        </button>\r\n        <ng-container\r\n            *ngIf=\"(type === assessmentType.managerApprove && (userAssessment?.status === assessmentStatus.waitingManagerApprove\r\n            || userAssessment?.status === assessmentStatus.approverDecline)) || (type === assessmentType.approverApprove\r\n            && (userAssessment?.status === assessmentStatus.approverDecline || userAssessment?.status === assessmentStatus.managerApproveWaitingApproverApprove))\">\r\n            <button type=\"button\" class=\"btn btn-success cm-mgr-5\"\r\n                    [swal]=\"confirmApprove\"\r\n                    (confirm)=\"approve()\">\r\n                Duyệt\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-danger cm-mgr-5\"\r\n                    [swal]=\"confirmDelince\" (confirm)=\"decline($event)\">\r\n                Không duyệt\r\n            </button>\r\n        </ng-container>\r\n        <button type=\"button\" class=\"btn btn-default\" (click)=\"back()\">\r\n            Trở về trang danh sách\r\n        </button>\r\n    </div>\r\n</div>\r\n\r\n<swal\r\n    #confirmSendAssessment\r\n    i18n=\"@@confirmSendAssessment\"\r\n    title=\"Bạn có chắc chắn muốn gửi đánh giá hiệu quả công việc?\"\r\n    text=\"Lưu ý: Sau khi gửi bạn không thể thay đổi thông tin đánh giá.\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<swal\r\n    #confirmApprove\r\n    i18n=\"@@confirmApproveAssessment\"\r\n    title=\"Bạn có chắc chắn muốn duyệt bản đánh giá hiệu quả công việc này?\"\r\n    text=\"Lưu ý: Sau khi duyệt bạn không thể thay đổi thông tin đánh giá.\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<swal\r\n    #confirmDelince\r\n    i18n=\"@@confirmDeclineAssessment\"\r\n    title=\"Bạn có chắc chắn không duyệt bản đánh giá hiệu quả công việc này?\"\r\n    text=\"Lưu ý: Sau khi không duyệt bản đánh giá sẽ phải thực hiện đánh giá lại.\"\r\n    type=\"question\"\r\n    input=\"textarea\"\r\n    [showCancelButton]=\"true\">\r\n</swal>\r\n\r\n"

/***/ }),

/***/ "./src/app/modules/hr/assessment/assessment-detail/assessment-detail.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/assessment-detail/assessment-detail.component.ts ***!
  \****************************************************************************************/
/*! exports provided: AssessmentDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssessmentDetailComponent", function() { return AssessmentDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../my-assessment/user-assessment.viewmodel */ "./src/app/modules/hr/assessment/my-assessment/user-assessment.viewmodel.ts");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../register-form/register-form.component */ "./src/app/modules/hr/assessment/register-form/register-form.component.ts");
/* harmony import */ var _services_user_assessment_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/user-assessment.service */ "./src/app/modules/hr/assessment/services/user-assessment.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_8__);









var AssessmentDetailComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](AssessmentDetailComponent, _super);
    function AssessmentDetailComponent(route, router, toastr, userAssessmentService) {
        var _this = _super.call(this) || this;
        _this.route = route;
        _this.router = router;
        _this.toastr = toastr;
        _this.userAssessmentService = userAssessmentService;
        _this.criteriaGroups = [];
        _this.assessmentType = _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_5__["AssessmentType"];
        _this.assessmentStatus = _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_3__["AssessmentStatus"];
        _this.subscribers.routeData = _this.route.data.subscribe(function (data) {
            _this.type = data.type;
        });
        _this.subscribers.routeParams = _this.route.params.subscribe(function (params) {
            if (params.id) {
                _this.getDetail(params.id);
            }
        });
        return _this;
    }
    AssessmentDetailComponent.prototype.ngOnInit = function () {
    };
    AssessmentDetailComponent.prototype.approve = function () {
        var _this = this;
        var oldStatus = this.userAssessment.status;
        this.userAssessment.status = this.type === _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_5__["AssessmentType"].userRegister ? this.assessmentStatus.waitingManagerApprove
            : this.type === _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_5__["AssessmentType"].managerApprove ? this.assessmentStatus.managerApproveWaitingApproverApprove
                : this.type === _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_5__["AssessmentType"].approverApprove ? this.assessmentStatus.approverApprove
                    : null;
        this.userAssessmentService.updateStatus(this.userAssessment.id, this.userAssessment)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
            if (_this.type === _this.assessmentType.userRegister) {
                _this.router.navigateByUrl('/assessment/my-assessment');
            }
            else {
                _this.router.navigateByUrl('/assessment/approve');
            }
        }, function (error) {
            _this.userAssessment.status = oldStatus;
            _this.userAssessment.statusName = _this.userAssessmentService.getAssessmentStatusName(_this.userAssessment.status);
        });
    };
    AssessmentDetailComponent.prototype.decline = function (reason) {
        var _this = this;
        if (!reason) {
            this.toastr.warning('Vui lòng nhập lý do không duyệt.');
            return;
        }
        var oldStatus = this.userAssessment.status;
        this.userAssessment.status = this.type === _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_5__["AssessmentType"].managerApprove ? this.assessmentStatus.managerDecline
            : this.type === _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_5__["AssessmentType"].approverApprove ? this.assessmentStatus.approverDecline
                : null;
        this.userAssessment.reason = reason;
        this.userAssessmentService.updateStatus(this.userAssessment.id, this.userAssessment)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
            if (_this.type === _this.assessmentType.userRegister) {
                _this.router.navigateByUrl('/assessment/my-assessment');
            }
            else {
                _this.router.navigateByUrl('/assessment/approve');
            }
        }, function (error) {
            _this.userAssessment.status = oldStatus;
            _this.userAssessment.statusName = _this.userAssessmentService.getAssessmentStatusName(_this.userAssessment.status);
        });
    };
    AssessmentDetailComponent.prototype.back = function () {
        this.router.navigateByUrl('/assessment/my-assessment');
    };
    AssessmentDetailComponent.prototype.getDetail = function (id) {
        var _this = this;
        this.criteriaGroups = [];
        this.subscribers.getDetail = this.userAssessmentService.getDetail(id)
            .subscribe(function (data) {
            if (data) {
                _this.userAssessment = data;
                _this.type = _this.userAssessment.userId === _this.currentUser.id ? _this.assessmentType.userRegister
                    : _this.userAssessment.managerUserId === _this.currentUser.id ? _this.assessmentType.managerApprove
                        : _this.userAssessment.approverUserId === _this.currentUser.id ? _this.assessmentType.approverApprove : null;
                var groups = lodash__WEBPACK_IMPORTED_MODULE_8__["groupBy"](data.userAssessmentCriterias, 'criteriaGroupId');
                for (var key in groups) {
                    if (groups.hasOwnProperty(key)) {
                        _this.criteriaGroups = _this.criteriaGroups.concat([{
                                groupId: key,
                                groupName: groups[key][0].criteriaGroupName,
                                totalPoint: lodash__WEBPACK_IMPORTED_MODULE_8__["sumBy"](groups[key], 'point'),
                                totalUserPoint: lodash__WEBPACK_IMPORTED_MODULE_8__["sumBy"](groups[key], 'userPoint'),
                                totalManagerPoint: lodash__WEBPACK_IMPORTED_MODULE_8__["sumBy"](groups[key], 'managerPoint'),
                                totalApproverPoint: lodash__WEBPACK_IMPORTED_MODULE_8__["sumBy"](groups[key], 'approverPoint'),
                                criterias: groups[key].map(function (criteria) {
                                    return criteria;
                                })
                            }]);
                    }
                }
            }
        });
    };
    AssessmentDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-assessment-detail',
            template: __webpack_require__(/*! ./assessment-detail.component.html */ "./src/app/modules/hr/assessment/assessment-detail/assessment-detail.component.html"),
            providers: [_services_user_assessment_service__WEBPACK_IMPORTED_MODULE_6__["UserAssessmentService"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"],
            _services_user_assessment_service__WEBPACK_IMPORTED_MODULE_6__["UserAssessmentService"]])
    ], AssessmentDetailComponent);
    return AssessmentDetailComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_4__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/assessment/assessment-result/assessment-result.component.css":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/assessment-result/assessment-result.component.css ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaHIvYXNzZXNzbWVudC9hc3Nlc3NtZW50LXJlc3VsdC9hc3Nlc3NtZW50LXJlc3VsdC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/modules/hr/assessment/assessment-result/assessment-result.component.html":
/*!******************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/assessment-result/assessment-result.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@listWarehousePageTitle\">Tổng hợp kết quả đánh giá</span>\r\n    <small i18n=\"@@productModuleTitle\">Đánh giá hiệu quả công việc</small>\r\n</h1>\r\n\r\n<form class=\"form-inline\" (ngSubmit)=\"search()\">\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <ghm-input name=\"keyword\"\r\n                   icon=\"fa fa-search\"\r\n                   placeholder=\"Nhập tên nhân viên cần tìm\"\r\n                   (remove)=\"search(1)\"\r\n                   [(ngModel)]=\"keyword\"></ghm-input>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <nh-dropdown-tree\r\n            title=\"-- Lọc theo phòng ban --\"\r\n            [data]=\"officeTreeData\"\r\n            (nodeSelected)=\"onNodeSelected($event)\"></nh-dropdown-tree>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <ghm-select [data]=\"listStatus\"\r\n                    title=\"-- Lọc theo trạng thái --\"\r\n                    [(ngModel)]=\"status\"\r\n                    name=\"status\"></ghm-select>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <ghm-select [data]=\"listMonths\"\r\n                    title=\"-- Lọc theo tháng --\"\r\n                    name=\"month\"\r\n                    [(ngModel)]=\"month\"></ghm-select>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <ghm-select [data]=\"listYears\"\r\n                    title=\"-- Lọc theo năm --\"\r\n                    name=\"year\"\r\n                    [(ngModel)]=\"year\"></ghm-select>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <button class=\"btn blue\">\r\n            <i class=\"fa fa-search\"></i>\r\n        </button>\r\n    </div>\r\n    <div class=\"form-group pull-right\">\r\n        <button type=\"button\" class=\"btn blue\">\r\n            Xuất file\r\n        </button>\r\n    </div>\r\n</form>\r\n\r\n<app-assessment-table\r\n    [data]=\"listItems\"\r\n></app-assessment-table>\r\n<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <ghm-paging [totalRows]=\"totalRows\"\r\n                    [currentPage]=\"currentPage\"\r\n                    [pageShow]=\"6\"\r\n                    [isDisabled]=\"isSearching\"\r\n                    [pageSize]=\"pageSize\"\r\n                    (pageClick)=\"search($event)\"\r\n        ></ghm-paging>\r\n    </div>\r\n</div>\r\n\r\n<!--<div class=\"row\">-->\r\n<!--<div class=\"col-sm-12\">-->\r\n<!--<table class=\"table table-bordered table-stripped table-hover\">-->\r\n<!--<thead>-->\r\n<!--<tr>-->\r\n<!--<th class=\"center middle w50\" rowspan=\"2\">STT</th>-->\r\n<!--<th class=\"center middle w100\" rowspan=\"2\">Mã NV</th>-->\r\n<!--<th class=\"center middle w250\" rowspan=\"2\">Họ tên</th>-->\r\n<!--<th class=\"center middle\" rowspan=\"2\">Chức vụ</th>-->\r\n<!--<th class=\"center middle\" rowspan=\"2\">Phòng ban</th>-->\r\n<!--<th class=\"center middle\" rowspan=\"2\">Trạng thái</th>-->\r\n<!--<th class=\"center middle\" colspan=\"4\">Điểm</th>-->\r\n<!--</tr>-->\r\n<!--<tr>-->\r\n<!--<th class=\"center w100 w100\">Chuẩn</th>-->\r\n<!--<th class=\"center w100 w100\">Tự đánh giá</th>-->\r\n<!--<th class=\"center w100 w100\">QLTT</th>-->\r\n<!--<th class=\"center w100 w100\">QLPD</th>-->\r\n<!--</tr>-->\r\n<!--</thead>-->\r\n<!--<tbody>-->\r\n<!--<tr *ngFor=\"let item of listItems$ | async; let i = index\">-->\r\n<!--<td></td>-->\r\n<!--<td></td>-->\r\n<!--<td></td>-->\r\n<!--<td></td>-->\r\n<!--<td></td>-->\r\n<!--<td></td>-->\r\n<!--<td></td>-->\r\n<!--<td></td>-->\r\n<!--<td></td>-->\r\n<!--<td></td>-->\r\n<!--</tr>-->\r\n<!--</tbody>-->\r\n<!--</table>-->\r\n<!--</div>-->\r\n<!--</div>-->\r\n"

/***/ }),

/***/ "./src/app/modules/hr/assessment/assessment-result/assessment-result.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/assessment-result/assessment-result.component.ts ***!
  \****************************************************************************************/
/*! exports provided: AssessmentResultComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssessmentResultComponent", function() { return AssessmentResultComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _services_user_assessment_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/user-assessment.service */ "./src/app/modules/hr/assessment/services/user-assessment.service.ts");




var AssessmentResultComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](AssessmentResultComponent, _super);
    function AssessmentResultComponent(userAssessment) {
        var _this = _super.call(this) || this;
        _this.userAssessment = userAssessment;
        _this.listStatus = [];
        _this.listMonths = [];
        _this.listYears = [];
        _this.officeTreeData = [];
        _this.month = new Date().getMonth() + 1;
        _this.year = new Date().getFullYear();
        _this.renderListMonth();
        _this.renderListYear();
        _this.userAssessment.getOfficeTree()
            .subscribe(function (result) {
            _this.officeTreeData = result;
        });
        _this.listStatus = _this.userAssessment.getListStatus();
        _this.month = new Date().getMonth() + 1;
        _this.year = new Date().getFullYear();
        return _this;
    }
    AssessmentResultComponent.prototype.ngOnInit = function () {
        this.appService.setupPage(this.pageId.ASSESSMENT, this.pageId.ASSESSMENT_RESULT, 'Danh sách kết quả đánh giá', 'Đánh giá hiệu quả công việc');
        this.search();
    };
    AssessmentResultComponent.prototype.onNodeSelected = function (office) {
        if (office) {
            this.officeId = office.id;
        }
        else {
            this.officeId = null;
        }
    };
    AssessmentResultComponent.prototype.search = function (currentPage) {
        var _this = this;
        if (currentPage === void 0) { currentPage = 1; }
        this.currentPage = currentPage;
        this.subscribers.searchResult = this.userAssessment.searchResult(this.keyword, this.officeId, this.status, this.month, this.year, this.currentPage, this.pageSize)
            .subscribe(function (result) {
            _this.totalRows = result.totalRows;
            _this.listItems = result.items;
        });
    };
    AssessmentResultComponent.prototype.renderListYear = function () {
        var year = 2018;
        var diff = new Date().getFullYear() - year;
        for (var i = 0; i <= diff; i++) {
            var currentYear = year + i;
            this.listYears = this.listYears.concat([{
                    id: currentYear, name: currentYear.toString()
                }]);
        }
    };
    AssessmentResultComponent.prototype.renderListMonth = function () {
        for (var i = 1; i <= 12; i++) {
            this.listMonths = this.listMonths.concat([{
                    id: i, name: i.toString()
                }]);
        }
    };
    AssessmentResultComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-assessment-result',
            template: __webpack_require__(/*! ./assessment-result.component.html */ "./src/app/modules/hr/assessment/assessment-result/assessment-result.component.html"),
            providers: [_services_user_assessment_service__WEBPACK_IMPORTED_MODULE_3__["UserAssessmentService"]],
            styles: [__webpack_require__(/*! ./assessment-result.component.css */ "./src/app/modules/hr/assessment/assessment-result/assessment-result.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_user_assessment_service__WEBPACK_IMPORTED_MODULE_3__["UserAssessmentService"]])
    ], AssessmentResultComponent);
    return AssessmentResultComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/assessment/assessment-table/assessment-table.component.css":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/assessment-table/assessment-table.component.css ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaHIvYXNzZXNzbWVudC9hc3Nlc3NtZW50LXRhYmxlL2Fzc2Vzc21lbnQtdGFibGUuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/modules/hr/assessment/assessment-table/assessment-table.component.html":
/*!****************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/assessment-table/assessment-table.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"table-responsive cm-mgt-10\">\r\n    <table class=\"table table-striped table-bordered table-advance table-hover\">\r\n        <thead>\r\n        <tr>\r\n            <th class=\"center middle w70\" rowspan=\"2\">Tháng</th>\r\n            <th class=\"center middle w250\" rowspan=\"2\">Họ và tên</th>\r\n            <th class=\"center middle w150\" rowspan=\"2\">Chức danh</th>\r\n            <th class=\"center middle w150\" rowspan=\"2\">Trạng thái</th>\r\n            <th class=\"center middle\" colspan=\"4\">Điểm</th>\r\n            <th class=\"center middle\" rowspan=\"2\"></th>\r\n        </tr>\r\n        <tr>\r\n            <th class=\"center middle w100\">Chuẩn</th>\r\n            <th class=\"center middle w100\">Tự đánh giá</th>\r\n            <th class=\"center middle w100\">QLTT</th>\r\n            <th class=\"center middle w100\">QLPD</th>\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n        <tr *ngFor=\"let item of data\">\r\n            <td class=\"center middle\">{{ item.month }}</td>\r\n            <td class=\"middle\">\r\n                <a routerLink=\"/assessment/register\"\r\n                   [queryParams]=\"{id: item.id, type: type}\"\r\n                   *ngIf=\"(type == assessmentType.userRegister && currentUser?.id === item.userId)\r\n                        || (type === assessmentType.managerApprove && currentUser?.id === item.userId)\r\n                        || (type === assessmentType.managerApprove || type === assessmentType.approverApprove); else readOnlyTemplate\"\r\n                   class=\"bold\">{{ item.fullName }}</a>\r\n                <ng-template #readOnlyTemplate>\r\n                    {{ item.fullName }}\r\n                </ng-template>\r\n            </td>\r\n            <td class=\"middle\">{{ item.positionName }}</td>\r\n            <td class=\"middle center\">\r\n                <span class=\"badge\"\r\n                      [class.badge-danger]=\"item.status === assessmentStatus.managerDecline || item.status === assessmentStatus.approverDecline || item.status === assessmentStatus.cancel\"\r\n                      [class.badge-success]=\"item.status === assessmentStatus.approverApprove || item.status === assessmentStatus.managerApproved\"\r\n                      [class.badge-info]=\"item.status=== assessmentStatus.new || item.status === assessmentStatus.managerApproveWaitingApproverApprove\r\n                        || item.status === assessmentStatus.waitingManagerApprove\">{{ item.statusName }}</span>\r\n            </td>\r\n            <td class=\"text-right middle\">{{ item.totalPoint }}</td>\r\n            <td class=\"text-right middle\">{{ item.totalUserPoint }}</td>\r\n            <td class=\"text-right middle\">{{ item.totalManagerPoint }}</td>\r\n            <td class=\"text-right middle\">{{ item.totalApproverPoint }}</td>\r\n            <td class=\"center middle w100\">\r\n                <a routerLink=\"/assessment/register\"\r\n                   [queryParams]=\"{id: item.id}\"\r\n                   *ngIf=\"item.userId === currentUser?.id && (item.status === assessmentStatus.new || item.status === assessmentStatus.managerDecline)\"\r\n                   matTooltip=\"Tiếp tục đăng ký\"\r\n                   class=\"btn blue btn-sm\">\r\n                    <i class=\"fa fa-arrow-right\" aria-hidden=\"true\"></i>\r\n                </a>\r\n                <ng-container *ngIf=\"(type === assessmentType.managerApprove && currentUser?.id === item.managerUserId\r\n                    && (item.status === assessmentStatus.waitingManagerApprove || item.status === assessmentStatus.approverDecline))\r\n                    || (type == assessmentType.approverApprove && currentUser?.id === item.approverUserId\r\n                    && (item.status === assessmentStatus.managerApproveWaitingApproverApprove || item.status === assessmentStatus.approverDecline))\">\r\n                    <a href=\"javascript://\"\r\n                       matTooltip=\"Duyệt\"\r\n                       class=\"btn btn-success btn-sm\"\r\n                       [swal]=\"confirmApprove\"\r\n                       (confirm)=\"approve(item)\">\r\n                        <i class=\"fa fa-check\" aria-hidden=\"true\"></i>\r\n                    </a>\r\n                    <a href=\"javascript://\"\r\n                       matTooltip=\"Không duyệt\"\r\n                       class=\"btn btn-danger btn-sm\"\r\n                       [swal]=\"confirmDelince\"\r\n                       (confirm)=\"decline(item, $event)\">\r\n                        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>\r\n                    </a>\r\n                </ng-container>\r\n                <a class=\"btn default btn-sm\"\r\n                   matTooltip=\"Chi tiết\"\r\n                   routerLink=\"/assessment/register\"\r\n                   [queryParams]=\"{id: item.id, type: type}\"\r\n                   *ngIf=\"(type == assessmentType.userRegister && currentUser?.id === item.userId)\r\n                        || (type === assessmentType.managerApprove && currentUser?.id === item.userId)\r\n                        || (type === assessmentType.managerApprove || type === assessmentType.approverApprove)\">\r\n                    <i class=\"fa fa-eye\"></i>\r\n                </a>\r\n            </td>\r\n        </tr>\r\n        </tbody>\r\n    </table>\r\n</div>\r\n\r\n<swal\r\n    #confirmSendAssessment\r\n    i18n=\"@@confirmSendAssessment\"\r\n    title=\"Bạn có chắc chắn muốn gửi đánh giá hiệu quả công việc?\"\r\n    text=\"Lưu ý: Sau khi gửi bạn không thể thay đổi thông tin đánh giá.\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<swal\r\n    #confirmApprove\r\n    i18n=\"@@confirmApproveAssessment\"\r\n    title=\"Bạn có chắc chắn muốn duyệt bản đánh giá hiệu quả công việc này?\"\r\n    text=\"Lưu ý: Sau khi duyệt bạn không thể thay đổi thông tin đánh giá.\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<swal\r\n    #confirmDelince\r\n    i18n=\"@@confirmDeclineAssessment\"\r\n    title=\"Bạn có chắc chắn không duyệt bản đánh giá hiệu quả công việc này?\"\r\n    text=\"Lưu ý: Sau khi không duyệt bản đánh giá sẽ phải thực hiện đánh giá lại.\"\r\n    type=\"question\"\r\n    input=\"textarea\"\r\n    [showCancelButton]=\"true\">\r\n</swal>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/assessment/assessment-table/assessment-table.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/assessment-table/assessment-table.component.ts ***!
  \**************************************************************************************/
/*! exports provided: AssessmentTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssessmentTableComponent", function() { return AssessmentTableComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../my-assessment/user-assessment.viewmodel */ "./src/app/modules/hr/assessment/my-assessment/user-assessment.viewmodel.ts");
/* harmony import */ var _shareds_services_app_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../shareds/services/app.service */ "./src/app/shareds/services/app.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../register-form/register-form.component */ "./src/app/modules/hr/assessment/register-form/register-form.component.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _services_user_assessment_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/user-assessment.service */ "./src/app/modules/hr/assessment/services/user-assessment.service.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_8__);









var AssessmentTableComponent = /** @class */ (function () {
    function AssessmentTableComponent(router, appService, toastr, userAssessmentService) {
        this.router = router;
        this.appService = appService;
        this.toastr = toastr;
        this.userAssessmentService = userAssessmentService;
        this.assessmentType = _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_5__["AssessmentType"];
        this.assessmentStatus = _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_2__["AssessmentStatus"];
        this.currentUser = this.appService.currentUser;
    }
    AssessmentTableComponent.prototype.ngOnInit = function () {
    };
    AssessmentTableComponent.prototype.approve = function (userAssessment) {
        var _this = this;
        var oldStatus = userAssessment.status;
        userAssessment.status = this.type === _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_5__["AssessmentType"].userRegister ? this.assessmentStatus.waitingManagerApprove
            : this.type === _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_5__["AssessmentType"].managerApprove ? this.assessmentStatus.managerApproveWaitingApproverApprove
                : this.type === _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_5__["AssessmentType"].approverApprove ? this.assessmentStatus.approverApprove
                    : null;
        this.userAssessmentService.updateStatus(userAssessment.id, userAssessment)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
            var userAssessmentInfo = lodash__WEBPACK_IMPORTED_MODULE_8__["find"](_this.data, function (item) {
                return item.id === userAssessment.id;
            });
            if (userAssessmentInfo) {
                if (_this.type === _this.assessmentType.managerApprove && !userAssessment.approverUserId) {
                    userAssessmentInfo.status = _this.assessmentStatus.managerApproved;
                }
                else {
                    userAssessmentInfo.status = userAssessment.status;
                }
                userAssessmentInfo.statusName = _this.userAssessmentService.getAssessmentStatusName(userAssessmentInfo.status);
            }
        }, function (error) {
            var userAssessmentInfo = lodash__WEBPACK_IMPORTED_MODULE_8__["find"](_this.data, function (item) {
                return item.id === userAssessment.id;
            });
            if (userAssessmentInfo) {
                userAssessmentInfo.status = oldStatus;
                userAssessmentInfo.statusName = _this.userAssessmentService.getAssessmentStatusName(userAssessmentInfo.status);
            }
        });
    };
    AssessmentTableComponent.prototype.decline = function (userAssessment, reason) {
        var _this = this;
        if (!reason) {
            this.toastr.warning('Vui lòng nhập lý do không duyệt.');
            return;
        }
        var oldStatus = userAssessment.status;
        userAssessment.status = this.type === _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_5__["AssessmentType"].managerApprove ? this.assessmentStatus.managerDecline
            : this.type === _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_5__["AssessmentType"].approverApprove ? this.assessmentStatus.approverDecline
                : null;
        userAssessment.reason = reason;
        this.userAssessmentService.updateStatus(userAssessment.id, userAssessment)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
            var userAssessmentInfo = lodash__WEBPACK_IMPORTED_MODULE_8__["find"](_this.data, function (item) {
                return item.id === userAssessment.id;
            });
            if (userAssessmentInfo) {
                userAssessmentInfo.status = userAssessment.status;
                userAssessmentInfo.statusName = _this.userAssessmentService.getAssessmentStatusName(userAssessmentInfo.status);
            }
        }, function (error) {
            var userAssessmentInfo = lodash__WEBPACK_IMPORTED_MODULE_8__["find"](_this.data, function (item) {
                return item.id === userAssessment.id;
            });
            if (userAssessmentInfo) {
                userAssessmentInfo.status = oldStatus;
                userAssessmentInfo.statusName = _this.userAssessmentService.getAssessmentStatusName(userAssessmentInfo.status);
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], AssessmentTableComponent.prototype, "data", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], AssessmentTableComponent.prototype, "type", void 0);
    AssessmentTableComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-assessment-table',
            template: __webpack_require__(/*! ./assessment-table.component.html */ "./src/app/modules/hr/assessment/assessment-table/assessment-table.component.html"),
            providers: [_services_user_assessment_service__WEBPACK_IMPORTED_MODULE_7__["UserAssessmentService"]],
            styles: [__webpack_require__(/*! ./assessment-table.component.css */ "./src/app/modules/hr/assessment/assessment-table/assessment-table.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _shareds_services_app_service__WEBPACK_IMPORTED_MODULE_3__["AppService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"],
            _services_user_assessment_service__WEBPACK_IMPORTED_MODULE_7__["UserAssessmentService"]])
    ], AssessmentTableComponent);
    return AssessmentTableComponent;
}());



/***/ }),

/***/ "./src/app/modules/hr/assessment/assessment.module.ts":
/*!************************************************************!*\
  !*** ./src/app/modules/hr/assessment/assessment.module.ts ***!
  \************************************************************/
/*! exports provided: AssessmentModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssessmentModule", function() { return AssessmentModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _criteria_criteria_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./criteria/criteria.component */ "./src/app/modules/hr/assessment/criteria/criteria.component.ts");
/* harmony import */ var _approve_approve_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./approve/approve.component */ "./src/app/modules/hr/assessment/approve/approve.component.ts");
/* harmony import */ var _my_assessment_my_assessment_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./my-assessment/my-assessment.component */ "./src/app/modules/hr/assessment/my-assessment/my-assessment.component.ts");
/* harmony import */ var _config_config_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./config/config.component */ "./src/app/modules/hr/assessment/config/config.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _shareds_components_nh_datetime_picker_nh_date_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../shareds/components/nh-datetime-picker/nh-date.module */ "./src/app/shareds/components/nh-datetime-picker/nh-date.module.ts");
/* harmony import */ var _shareds_components_nh_dropdown_nh_dropdown_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../shareds/components/nh-dropdown/nh-dropdown.module */ "./src/app/shareds/components/nh-dropdown/nh-dropdown.module.ts");
/* harmony import */ var _shareds_components_nh_select_nh_select_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../shareds/components/nh-select/nh-select.module */ "./src/app/shareds/components/nh-select/nh-select.module.ts");
/* harmony import */ var _criteria_criteria_form_criteria_form_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./criteria/criteria-form/criteria-form.component */ "./src/app/modules/hr/assessment/criteria/criteria-form/criteria-form.component.ts");
/* harmony import */ var _criteria_criteria_detail_criteria_detail_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./criteria/criteria-detail/criteria-detail.component */ "./src/app/modules/hr/assessment/criteria/criteria-detail/criteria-detail.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _assessment_table_assessment_table_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./assessment-table/assessment-table.component */ "./src/app/modules/hr/assessment/assessment-table/assessment-table.component.ts");
/* harmony import */ var _assessment_detail_assessment_detail_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./assessment-detail/assessment-detail.component */ "./src/app/modules/hr/assessment/assessment-detail/assessment-detail.component.ts");
/* harmony import */ var _assessment_result_assessment_result_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./assessment-result/assessment-result.component */ "./src/app/modules/hr/assessment/assessment-result/assessment-result.component.ts");
/* harmony import */ var _criteria_servies_criteria_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./criteria/servies/criteria.service */ "./src/app/modules/hr/assessment/criteria/servies/criteria.service.ts");
/* harmony import */ var _shareds_components_tinymce_tinymce_module__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../../shareds/components/tinymce/tinymce.module */ "./src/app/shareds/components/tinymce/tinymce.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shareds_components_ghm_paging_ghm_paging_module__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../../../shareds/components/ghm-paging/ghm-paging.module */ "./src/app/shareds/components/ghm-paging/ghm-paging.module.ts");
/* harmony import */ var _shareds_components_nh_tree_nh_tree_module__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../../../shareds/components/nh-tree/nh-tree.module */ "./src/app/shareds/components/nh-tree/nh-tree.module.ts");
/* harmony import */ var _criteria_criteria_position_criteria_position_selection_criteria_position_selection_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./criteria/criteria-position/criteria-position-selection/criteria-position-selection.component */ "./src/app/modules/hr/assessment/criteria/criteria-position/criteria-position-selection/criteria-position-selection.component.ts");
/* harmony import */ var _criteria_criteria_position_criteria_position_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./criteria/criteria-position/criteria-position.component */ "./src/app/modules/hr/assessment/criteria/criteria-position/criteria-position.component.ts");
/* harmony import */ var _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./register-form/register-form.component */ "./src/app/modules/hr/assessment/register-form/register-form.component.ts");
/* harmony import */ var _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @toverux/ngx-sweetalert2 */ "./node_modules/@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
/* harmony import */ var _register_form_comment_comment_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./register-form/comment/comment.component */ "./src/app/modules/hr/assessment/register-form/comment/comment.component.ts");
/* harmony import */ var _criteria_group_group_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./criteria/group/group.component */ "./src/app/modules/hr/assessment/criteria/group/group.component.ts");
/* harmony import */ var _criteria_group_group_form_group_form_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./criteria/group/group-form/group-form.component */ "./src/app/modules/hr/assessment/criteria/group/group-form/group-form.component.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_module__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ../../../shareds/components/nh-modal/nh-modal.module */ "./src/app/shareds/components/nh-modal/nh-modal.module.ts");
/* harmony import */ var _shareds_components_ghm_input_ghm_input_module__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ../../../shareds/components/ghm-input/ghm-input.module */ "./src/app/shareds/components/ghm-input/ghm-input.module.ts");
/* harmony import */ var _shareds_components_ghm_select_ghm_select_module__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ../../../shareds/components/ghm-select/ghm-select.module */ "./src/app/shareds/components/ghm-select/ghm-select.module.ts");

































var routes = [
    {
        path: 'config',
        component: _config_config_component__WEBPACK_IMPORTED_MODULE_6__["ConfigComponent"],
    },
    {
        path: 'my-assessment',
        component: _my_assessment_my_assessment_component__WEBPACK_IMPORTED_MODULE_5__["MyAssessmentComponent"]
    },
    {
        path: 'approve',
        component: _approve_approve_component__WEBPACK_IMPORTED_MODULE_4__["ApproveComponent"],
    },
    {
        path: 'criteria',
        component: _criteria_criteria_component__WEBPACK_IMPORTED_MODULE_3__["CriteriaComponent"],
        resolve: { data: _criteria_servies_criteria_service__WEBPACK_IMPORTED_MODULE_18__["CriteriaService"] }
    },
    {
        path: 'criteria/group',
        component: _criteria_group_group_component__WEBPACK_IMPORTED_MODULE_28__["GroupComponent"]
    },
    {
        path: 'criteria-position',
        component: _criteria_criteria_position_criteria_position_component__WEBPACK_IMPORTED_MODULE_24__["CriteriaPositionComponent"],
    },
    {
        path: 'detail/:id',
        component: _assessment_detail_assessment_detail_component__WEBPACK_IMPORTED_MODULE_16__["AssessmentDetailComponent"],
    },
    {
        path: 'result',
        component: _assessment_result_assessment_result_component__WEBPACK_IMPORTED_MODULE_17__["AssessmentResultComponent"],
    },
    {
        path: 'register',
        component: _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_25__["RegisterFormComponent"],
        data: {
            type: 0
        }
    },
    {
        path: 'manager-approve/:id',
        component: _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_25__["RegisterFormComponent"],
        data: {
            type: 1
        }
    },
    {
        path: 'approver-approve/:id',
        component: _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_25__["RegisterFormComponent"],
        data: {
            type: 2
        }
    }
];
var AssessmentModule = /** @class */ (function () {
    function AssessmentModule() {
    }
    AssessmentModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"].forChild(routes), _core_core_module__WEBPACK_IMPORTED_MODULE_8__["CoreModule"], _shareds_components_nh_datetime_picker_nh_date_module__WEBPACK_IMPORTED_MODULE_9__["NhDateModule"], _shareds_components_nh_dropdown_nh_dropdown_module__WEBPACK_IMPORTED_MODULE_10__["NhDropdownModule"], _shareds_components_nh_select_nh_select_module__WEBPACK_IMPORTED_MODULE_11__["NhSelectModule"], _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatDialogModule"], _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatButtonModule"], _shareds_components_tinymce_tinymce_module__WEBPACK_IMPORTED_MODULE_19__["TinymceModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_20__["ReactiveFormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_20__["FormsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatCheckboxModule"],
                _shareds_components_ghm_paging_ghm_paging_module__WEBPACK_IMPORTED_MODULE_21__["GhmPagingModule"], _shareds_components_nh_tree_nh_tree_module__WEBPACK_IMPORTED_MODULE_22__["NHTreeModule"], _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatTooltipModule"], _shareds_components_nh_modal_nh_modal_module__WEBPACK_IMPORTED_MODULE_30__["NhModalModule"], _shareds_components_ghm_input_ghm_input_module__WEBPACK_IMPORTED_MODULE_31__["GhmInputModule"], _shareds_components_ghm_select_ghm_select_module__WEBPACK_IMPORTED_MODULE_32__["GhmSelectModule"],
                _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_26__["SweetAlert2Module"]
            ],
            declarations: [_criteria_criteria_component__WEBPACK_IMPORTED_MODULE_3__["CriteriaComponent"], _approve_approve_component__WEBPACK_IMPORTED_MODULE_4__["ApproveComponent"], _my_assessment_my_assessment_component__WEBPACK_IMPORTED_MODULE_5__["MyAssessmentComponent"], _config_config_component__WEBPACK_IMPORTED_MODULE_6__["ConfigComponent"],
                _criteria_criteria_form_criteria_form_component__WEBPACK_IMPORTED_MODULE_12__["CriteriaFormComponent"], _criteria_criteria_detail_criteria_detail_component__WEBPACK_IMPORTED_MODULE_13__["CriteriaDetailComponent"], _assessment_table_assessment_table_component__WEBPACK_IMPORTED_MODULE_15__["AssessmentTableComponent"], _assessment_detail_assessment_detail_component__WEBPACK_IMPORTED_MODULE_16__["AssessmentDetailComponent"], _assessment_result_assessment_result_component__WEBPACK_IMPORTED_MODULE_17__["AssessmentResultComponent"],
                _criteria_criteria_position_criteria_position_selection_criteria_position_selection_component__WEBPACK_IMPORTED_MODULE_23__["CriteriaPositionSelectionComponent"], _criteria_criteria_position_criteria_position_component__WEBPACK_IMPORTED_MODULE_24__["CriteriaPositionComponent"], _register_form_register_form_component__WEBPACK_IMPORTED_MODULE_25__["RegisterFormComponent"], _register_form_comment_comment_component__WEBPACK_IMPORTED_MODULE_27__["CommentComponent"], _criteria_group_group_component__WEBPACK_IMPORTED_MODULE_28__["GroupComponent"],
                _criteria_group_group_form_group_form_component__WEBPACK_IMPORTED_MODULE_29__["GroupFormComponent"]],
            entryComponents: [_criteria_criteria_form_criteria_form_component__WEBPACK_IMPORTED_MODULE_12__["CriteriaFormComponent"], _criteria_criteria_detail_criteria_detail_component__WEBPACK_IMPORTED_MODULE_13__["CriteriaDetailComponent"], _criteria_criteria_position_criteria_position_selection_criteria_position_selection_component__WEBPACK_IMPORTED_MODULE_23__["CriteriaPositionSelectionComponent"], _register_form_comment_comment_component__WEBPACK_IMPORTED_MODULE_27__["CommentComponent"],
                _criteria_group_group_form_group_form_component__WEBPACK_IMPORTED_MODULE_29__["GroupFormComponent"]],
            providers: [_criteria_servies_criteria_service__WEBPACK_IMPORTED_MODULE_18__["CriteriaService"]]
        })
    ], AssessmentModule);
    return AssessmentModule;
}());



/***/ }),

/***/ "./src/app/modules/hr/assessment/config/assessment-config.service.ts":
/*!***************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/config/assessment-config.service.ts ***!
  \***************************************************************************/
/*! exports provided: AssessmentConfigService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssessmentConfigService", function() { return AssessmentConfigService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../environments/environment */ "./src/environments/environment.ts");







var AssessmentConfigService = /** @class */ (function () {
    function AssessmentConfigService(appConfig, spinnerService, http) {
        this.appConfig = appConfig;
        this.spinnerService = spinnerService;
        this.http = http;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].apiGatewayUrl + "api/v1/hr/assessment-configs";
    }
    AssessmentConfigService.prototype.saveAssessmentTime = function (fromDay, toDay) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .post(this.url, {
            fromDay: fromDay,
            toDay: toDay
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    AssessmentConfigService.prototype.getAssessmentTime = function () {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .get(this.url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    AssessmentConfigService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__["SpinnerService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], AssessmentConfigService);
    return AssessmentConfigService;
}());



/***/ }),

/***/ "./src/app/modules/hr/assessment/config/config.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/modules/hr/assessment/config/config.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaHIvYXNzZXNzbWVudC9jb25maWcvY29uZmlnLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/modules/hr/assessment/config/config.component.html":
/*!********************************************************************!*\
  !*** ./src/app/modules/hr/assessment/config/config.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@listWarehousePageTitle\">Cấu hình</span>\r\n    <small i18n=\"@@productModuleTitle\">Đánh giá hiệu quả công việc</small>\r\n</h1>\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-md-6\">\r\n        <div class=\"portlet light bordered\">\r\n            <div class=\"portlet-title\">\r\n                <div class=\"caption font-green-sharp\">\r\n                    <i class=\"icon-clock font-green-sharp\"></i>\r\n                    <span class=\"caption-subject bold uppercase\">Thời gian đánh giá hiệu quả công việc</span>\r\n                </div>\r\n            </div>\r\n            <div class=\"portlet-body\">\r\n                <form action=\"\" class=\"form-horizontal\" (ngSubmit)=\"saveAssessmentTime()\">\r\n                    <div class=\"form-group\">\r\n                        <label for=\"\" class=\"control-label col-sm-4\" ghmLabel=\"Đánh giá từ ngày\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <nh-select\r\n                                [liveSearch]=\"true\"\r\n                                [data]=\"listDays\"\r\n                                [(ngModel)]=\"fromDay\"\r\n                                name=\"fromDay\"\r\n                                title=\"-- Chọn ngày --\"></nh-select>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"\" class=\"control-label col-sm-4\" ghmLabel=\"Đánh giá đến ngày\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <nh-select\r\n                                [liveSearch]=\"true\"\r\n                                [data]=\"listDays\"\r\n                                [(ngModel)]=\"toDay\"\r\n                                name=\"toDay\"\r\n                                title=\"-- Chọn ngày --\"></nh-select>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <div class=\"col-sm-8 col-sm-offset-4\">\r\n                            <button class=\"btn blue\">\r\n                                Lưu\r\n                            </button>\r\n                        </div>\r\n                    </div>\r\n                </form>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/assessment/config/config.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/modules/hr/assessment/config/config.component.ts ***!
  \******************************************************************/
/*! exports provided: ConfigComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigComponent", function() { return ConfigComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _assessment_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./assessment-config.service */ "./src/app/modules/hr/assessment/config/assessment-config.service.ts");





var ConfigComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](ConfigComponent, _super);
    function ConfigComponent(toastr, assessmentConfigService) {
        var _this = _super.call(this) || this;
        _this.toastr = toastr;
        _this.assessmentConfigService = assessmentConfigService;
        _this.listDays = [];
        for (var i = 1; i < 32; i++) {
            _this.listDays = _this.listDays.concat([{
                    id: i, name: i.toString()
                }]);
        }
        return _this;
    }
    ConfigComponent.prototype.ngOnInit = function () {
        this.appService.setupPage(this.pageId.ASSESSMENT, this.pageId.ASSESSMENT_CONFIG, 'Cấu hình', 'Đánh giá hiệu quả CV');
        this.getAssessmentTime();
    };
    ConfigComponent.prototype.saveAssessmentTime = function () {
        var _this = this;
        if (!this.fromDay) {
            this.toastr.warning('Vui lòng chọn thời gian bắt đầu đánh giá.');
            return;
        }
        if (!this.toDay) {
            this.toastr.warning('Vui lòng chọn thời gian kết thúc đánh giá.');
            return;
        }
        this.subscribers.saveAssessmentTime = this.assessmentConfigService.saveAssessmentTime(this.fromDay, this.toDay)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
        });
    };
    ConfigComponent.prototype.getAssessmentTime = function () {
        var _this = this;
        this.subscribers.getAssessmentTime = this.assessmentConfigService.getAssessmentTime()
            .subscribe(function (result) {
            var data = result.data;
            if (data) {
                _this.fromDay = data.fromDay;
                _this.toDay = data.toDay;
            }
        });
    };
    ConfigComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-config',
            template: __webpack_require__(/*! ./config.component.html */ "./src/app/modules/hr/assessment/config/config.component.html"),
            providers: [_assessment_config_service__WEBPACK_IMPORTED_MODULE_4__["AssessmentConfigService"]],
            styles: [__webpack_require__(/*! ./config.component.css */ "./src/app/modules/hr/assessment/config/config.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            _assessment_config_service__WEBPACK_IMPORTED_MODULE_4__["AssessmentConfigService"]])
    ], ConfigComponent);
    return ConfigComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/criteria-detail/criteria-detail.component.css":
/*!**********************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/criteria-detail/criteria-detail.component.css ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaHIvYXNzZXNzbWVudC9jcml0ZXJpYS9jcml0ZXJpYS1kZXRhaWwvY3JpdGVyaWEtZGV0YWlsLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/criteria-detail/criteria-detail.component.html":
/*!***********************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/criteria-detail/criteria-detail.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form action=\"\" class=\"form-horizontal\">\r\n    <div class=\"form-group\">\r\n        <label for=\"\" class=\"label-control col-sm-4\" ghmLabel=\"Tên tiêu chí\" [required]=\"true\"></label>\r\n        <div class=\"col-sm-8\">\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Nhập tên tiêu chí\">\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label for=\"\" class=\"label-control col-sm-4\" ghmLabel=\"Nhóm tiêu chí\" [required]=\"true\"></label>\r\n        <div class=\"col-sm-8\">\r\n            <nh-select [data]=\"listGroups\"\r\n                       title=\"-- Chọn nhóm tiêu chí --\"\r\n            ></nh-select>\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label for=\"\" class=\"label-control col-sm-4\" ghmLabel=\"Điểm chuẩn\" [required]=\"true\"></label>\r\n        <div class=\"col-sm-8\">\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Nhập điểm chuẩn\">\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label for=\"\" class=\"label-control col-sm-4\" ghmLabel=\"Chỉ tiêu đánh giá tháng/tiêu chuẩn\"\r\n               [required]=\"true\"></label>\r\n        <div class=\"col-sm-8\">\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Nhập điểm chuẩn\">\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label for=\"\" class=\"label-control col-sm-4\" ghmLabel=\"Quy định chế tài\" [required]=\"true\"></label>\r\n        <div class=\"col-sm-8\">\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Nhập điểm chuẩn\">\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-8 col-sm-offset-4\">\r\n            <mat-slide-toggle>Sử dụng</mat-slide-toggle>\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-8 col-sm-offset-4\">\r\n            <button class=\"btn blue\">Lưu</button>\r\n            <button type=\"button\" class=\"btn btn-light\">Đóng</button>\r\n        </div>\r\n    </div>\r\n</form>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/criteria-detail/criteria-detail.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/criteria-detail/criteria-detail.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: CriteriaDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CriteriaDetailComponent", function() { return CriteriaDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CriteriaDetailComponent = /** @class */ (function () {
    function CriteriaDetailComponent() {
        this.listGroups = [];
    }
    CriteriaDetailComponent.prototype.ngOnInit = function () {
    };
    CriteriaDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-criteria-detail',
            template: __webpack_require__(/*! ./criteria-detail.component.html */ "./src/app/modules/hr/assessment/criteria/criteria-detail/criteria-detail.component.html"),
            styles: [__webpack_require__(/*! ./criteria-detail.component.css */ "./src/app/modules/hr/assessment/criteria/criteria-detail/criteria-detail.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CriteriaDetailComponent);
    return CriteriaDetailComponent;
}());



/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/criteria-form/criteria-form.component.css":
/*!******************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/criteria-form/criteria-form.component.css ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaHIvYXNzZXNzbWVudC9jcml0ZXJpYS9jcml0ZXJpYS1mb3JtL2NyaXRlcmlhLWZvcm0uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/criteria-form/criteria-form.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/criteria-form/criteria-form.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"portlet light cm-mgb-0\">\r\n    <div class=\"portlet-title\">\r\n        <div class=\"caption font-green-sharp\">\r\n            <i class=\"icon-speech font-green-sharp\"></i>\r\n            <span class=\"caption-subject bold uppercase\">\r\n                <span *ngIf=\"readonly; else editTitleTemplate\">Chi tiết tiêu chí</span>\r\n                <ng-template #editTitleTemplate>\r\n                    {isUpdate, select, 0 {Thêm mới tiêu chí} 1 {Cập nhật tiêu chí} other {}}\r\n                </ng-template>\r\n            </span>\r\n        </div>\r\n    </div>\r\n    <form action=\"\" class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <div class=\"portlet-body form\">\r\n            <div class=\"form-body\">\r\n                <div formArrayName=\"translations\">\r\n                    <div class=\"form-group\" *ngIf=\"languages && languages.length > 1\">\r\n                        <label i18n-ghmLabel=\"@@language\" ghmLabel=\"Language\"\r\n                               class=\"col-sm-4 control-label\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <ghm-select [data]=\"languages\"\r\n                                       i18n-title=\"@@pleaseSelectLanguage\"\r\n                                       title=\"-- Chọn ngôn ngữ --\"\r\n                                       name=\"language\"\r\n                                       [(value)]=\"currentLanguage\"\r\n                                       (itemSelected)=\"currentLanguage = $event.id\"></ghm-select>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group\"\r\n                         *ngFor=\"let modelTranslation of translations.controls; index as i\"\r\n                         [hidden]=\"modelTranslation.value.languageId !== currentLanguage\"\r\n                         [formGroupName]=\"i\"\r\n                         [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.name\">\r\n                        <label for=\"\" class=\"label-control col-sm-4 control-label\" ghmLabel=\"Tên tiêu chí\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <ghm-input placeholder=\"Nhập tên tiêu chí\"\r\n                                       formControlName=\"name\"></ghm-input>\r\n                            <span class=\"help-block\"\r\n                                  *ngIf=\"translationFormErrors[modelTranslation.value.languageId]?.name\"\r\n                                  i18n=\"criteriaNameValidationMessage\">\r\n                            {translationFormErrors[modelTranslation.value.languageId].name, select, required {Vui lòng nhập tên tiêu chí.} maxlength {Tên tiêu chí không được phép vượt quá 256 ký tự.} other {}}\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\"\r\n                     [class.has-error]=\"formErrors?.groupId\">\r\n                    <label for=\"\" class=\"label-control col-sm-4 control-label\" ghmLabel=\"Nhóm tiêu chí\"\r\n                           [required]=\"true\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <div class=\"input-group\">\r\n                            <ghm-select [data]=\"listGroups\"\r\n                                       title=\"-- Chọn nhóm tiêu chí --\"\r\n                                       [readonly]=\"readonly\"\r\n                                       formControlName=\"groupId\"\r\n                            ></ghm-select>\r\n                            <span class=\"input-group-btn\" *ngIf=\"criteriaGroupPermission?.add\">\r\n                                <button class=\"btn blue\" type=\"button\" (click)=\"addGroup()\">\r\n                                    <i class=\"fa fa-plus\"></i>\r\n                                </button>\r\n                            </span>\r\n                        </div><!-- /input-group -->\r\n                        <span class=\"help-block\"\r\n                              *ngIf=\"formErrors?.groupId\"\r\n                              i18n=\"criteriaGroupValidationMessage\">\r\n                            {formErrors.groupId, select, required {Vui lòng chọn nhóm tiêu chí.} other {}}\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\"\r\n                     [class.has-error]=\"formErrors?.point\">\r\n                    <label for=\"\" class=\"label-control col-sm-4 control-label\" ghmLabel=\"Điểm chuẩn\"\r\n                           [required]=\"true\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <ghm-input placeholder=\"Nhập điểm chuẩn\"\r\n                                   formControlName=\"point\"></ghm-input>\r\n                        <span class=\"help-block\"\r\n                              *ngIf=\"formErrors?.point\"\r\n                              i18n=\"criteriaPointValidationMessage\">\r\n                            {formErrors.point, select, required {Vui lòng nhập điểm chuẩn.} isValid {Điểm chuẩn phải là số.} other {}}\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n                <div formArrayName=\"translations\">\r\n                    <div class=\"form-group\"\r\n                         *ngFor=\"let modelTranslation of translations.controls; index as i\"\r\n                         [hidden]=\"modelTranslation.value.languageId !== currentLanguage\"\r\n                         [formGroupName]=\"i\"\r\n                         [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.name\">\r\n                        <label for=\"\" class=\"label-control col-sm-4 control-label\"\r\n                               ghmLabel=\"Chỉ tiêu đánh giá tháng/tiêu chuẩn\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <!--<tinymce-->\r\n                            <!--#descriptionEditor-->\r\n                            <!--elementId=\"criteriaDescription{{ modelTranslation.value.languageId }}\"-->\r\n                            <!--formControlName=\"description\"></tinymce>-->\r\n                            <textarea class=\"form-control no-resizeable h100\" formControlName=\"description\"></textarea>\r\n                            <span class=\"help-block\"\r\n                                  *ngIf=\"translationFormErrors[modelTranslation.value.languageId]?.description\"\r\n                                  i18n=\"criteriaPointValidationMessage\">\r\n                                {translationFormErrors[modelTranslation.value.languageId].point, select, required {Vui lòng nhập Chỉ tiêu đánh giá tháng/tiêu chuẩn.} maxlength {Chỉ tiêu đánh giá tháng/tiêu chuẩn không được phép vượt quá 4000 ký tự.} other {}}\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group\"\r\n                         *ngFor=\"let modelTranslation of translations.controls; index as i\"\r\n                         [hidden]=\"modelTranslation.value.languageId !== currentLanguage\"\r\n                         [formGroupName]=\"i\"\r\n                         [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.name\">\r\n                        <label for=\"\" class=\"label-control col-sm-4 control-label\" ghmLabel=\"Quy định chế tài\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <!--<tinymce-->\r\n                            <!--#sanctionEditor-->\r\n                            <!--elementId=\"criteriaSanction{{ modelTranslation.value.languageId }}\"-->\r\n                            <!--formControlName=\"sanction\"-->\r\n                            <!--&gt;</tinymce>-->\r\n                            <textarea class=\"form-control no-resizeable h100\" formControlName=\"sanction\"></textarea>\r\n                            <span class=\"help-block\"\r\n                                  *ngIf=\"translationFormErrors[modelTranslation.value.languageId]?.sanction\"\r\n                                  i18n=\"criteriaPointValidationMessage\">\r\n                                {translationFormErrors[modelTranslation.value.languageId].sanction, select, required {Vui lòng nhập quy định chế tài.} maxlength {Quy định chế tài không được phép vượt quá 4000 ký tự.} other {}}\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <div class=\"col-sm-8 col-sm-offset-4\">\r\n                        <mat-slide-toggle\r\n                            color=\"primary\"\r\n                            formControlName=\"isActive\">Sử dụng\r\n                        </mat-slide-toggle>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"portlet-foot\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-12 text-right\">\r\n                    <mat-checkbox\r\n                        class=\"cm-mgr-5\"\r\n                        color=\"primary\"\r\n                        name=\"isCreateAnother\"\r\n                        i18n=\"@@isCreateAnother\"\r\n                        *ngIf=\"!isUpdate\"\r\n                        [(checked)]=\"isCreateAnother\"\r\n                        (change)=\"isCreateAnother = !isCreateAnother\"> Tiếp tục thêm\r\n                    </mat-checkbox>\r\n                    <button color=\"primary\" class=\"btn blue cm-mgr-5\" *ngIf=\"!readonly\">\r\n                        Lưu\r\n                    </button>\r\n                    <button type=\"button\" class=\"btn btn-light\" [mat-dialog-close]=\"isCriteriaGroupModified\">\r\n                        Hủy bỏ\r\n                    </button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </form>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/criteria-form/criteria-form.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/criteria-form/criteria-form.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: CriteriaFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CriteriaFormComponent", function() { return CriteriaFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _servies_criteria_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../servies/criteria.service */ "./src/app/modules/hr/assessment/criteria/servies/criteria.service.ts");
/* harmony import */ var _models_criteria_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../models/criteria.model */ "./src/app/modules/hr/assessment/criteria/models/criteria.model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _group_group_form_group_form_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../group/group-form/group-form.component */ "./src/app/modules/hr/assessment/criteria/group/group-form/group-form.component.ts");
/* harmony import */ var _validators_number_validator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../../validators/number.validator */ "./src/app/validators/number.validator.ts");











var CriteriaFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](CriteriaFormComponent, _super);
    function CriteriaFormComponent(data, dialog, toastr, numberValidator, criteriaService) {
        var _this = _super.call(this) || this;
        _this.data = data;
        _this.dialog = dialog;
        _this.toastr = toastr;
        _this.numberValidator = numberValidator;
        _this.criteriaService = criteriaService;
        _this.listGroups = [];
        _this.readonly = false;
        _this.criteria = new _models_criteria_model__WEBPACK_IMPORTED_MODULE_5__["Criteria"]();
        _this.criteriaTranslation = new _models_criteria_model__WEBPACK_IMPORTED_MODULE_5__["CriteriaTranslation"]();
        _this.isCriteriaGroupModified = false;
        _this.buildFormLanguage = function (language) {
            _this.translationFormErrors[language] = _this.renderFormError(['name', 'description']);
            _this.translationValidationMessage[language] = _this.renderFormErrorMessage([
                { 'name': ['required', 'maxlength'] },
                { 'description': ['required', 'maxlength'] },
            ]);
            var translationModel = _this.formBuilder.group({
                languageId: [language],
                name: [{ value: _this.criteriaTranslation.name, disabled: _this.readonly }, [
                        _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required,
                        _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(256)
                    ]],
                description: [{ value: _this.criteriaTranslation.description, disabled: _this.readonly }, [
                        _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required,
                        _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(4000)
                    ]],
                sanction: [{ value: _this.criteriaTranslation.sanction, disabled: _this.readonly }, [
                        _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required,
                        _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(4000)
                    ]]
            });
            translationModel.valueChanges.subscribe(function (data) { return _this.validateTranslation(false); });
            return translationModel;
        };
        return _this;
        // if (this.listGroups.length <= 0) {
        //     this.subscribers.getGroupSuggestions = this.criteriaService.groupSuggestion('', 1, 20)
        //         .subscribe((result: SearchResultViewModel<NhSuggestion>) => {
        //             this.listGroups = result.items;
        //         });
        // }
    }
    CriteriaFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.criteriaGroupPermission = this.appService.getPermissionByPageId(this.pageId.CRITERIA_GROUP);
        this.renderForm();
        if (this.data) {
            if (this.data.id) {
                this.id = this.data.id;
                this.isUpdate = true;
                this.subscribers.getDetail = this.criteriaService.getDetail(this.id)
                    .subscribe(function (result) {
                    _this.model.patchValue(result.data);
                });
            }
            if (this.data.groups) {
                this.listGroups = this.data.groups;
            }
            else {
                this.getCriteriaGroups();
            }
            if (this.data.readonly != null && this.data.readonly !== undefined) {
                this.readonly = this.data.readonly;
                for (var key in this.model.controls) {
                    if (this.readonly) {
                        this.model.controls[key].disable();
                    }
                }
            }
        }
    };
    CriteriaFormComponent.prototype.addGroup = function () {
        var _this = this;
        var criteriaGroupDialog = this.dialog.open(_group_group_form_group_form_component__WEBPACK_IMPORTED_MODULE_9__["GroupFormComponent"], {
            id: 'criteriaGroupForm'
        });
        criteriaGroupDialog.afterClosed().subscribe(function (result) {
            if (result) {
                _this.isCriteriaGroupModified = result;
                _this.getCriteriaGroups();
            }
        });
    };
    CriteriaFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.validateModel();
        var isLanguageValid = this.validateLanguage();
        if (isValid && isLanguageValid) {
            this.criteria = this.model.value;
            this.isSaving = true;
            if (this.isUpdate) {
                this.criteriaService
                    .update(this.id, this.criteria)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return (_this.isSaving = false); }))
                    .subscribe(function (result) {
                    _this.toastr.success(result.message);
                    _this.dialog.closeAll();
                });
            }
            else {
                this.criteriaService
                    .insert(this.criteria)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return (_this.isSaving = false); }))
                    .subscribe(function (result) {
                    _this.toastr.success(result.message);
                    if (!_this.isCreateAnother) {
                        _this.dialog.closeAll();
                    }
                    else {
                        _this.resetModel();
                    }
                });
            }
        }
    };
    CriteriaFormComponent.prototype.renderForm = function () {
        this.buildForm();
        this.renderTranslationArray(this.buildFormLanguage);
    };
    CriteriaFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.renderFormError(['isActive', 'point', 'groupId']);
        this.validationMessages = this.renderFormErrorMessage([
            { 'isActive': ['required'] },
            { 'point': ['required', 'isValid'] },
            { 'groupId': ['required', 'maxlength'] },
        ]);
        this.model = this.formBuilder.group({
            isActive: [{ value: this.criteria.isActive, disabled: this.readonly }, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required
                ]],
            groupId: [this.criteria.groupId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(50)
                ]],
            point: [{ value: this.criteria.point, disabled: this.readonly }, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required,
                    this.numberValidator.isValid
                ]],
            concurrencyStamp: [this.criteria.concurrencyStamp],
            translations: this.formBuilder.array([])
        });
        this.model.valueChanges.subscribe(function () { return _this.validateModel(false); });
    };
    CriteriaFormComponent.prototype.resetModel = function () {
        this.id = null;
        this.isUpdate = false;
        this.model.patchValue(new _models_criteria_model__WEBPACK_IMPORTED_MODULE_5__["Criteria"]());
        this.translations.controls.forEach(function (model) {
            model.patchValue({
                name: '',
                description: '',
                sanction: ''
            });
        });
        this.clearFormError(this.formErrors);
        this.clearFormError(this.translationFormErrors);
    };
    CriteriaFormComponent.prototype.getCriteriaGroups = function () {
        var _this = this;
        this.subscribers.getGroupSuggestions = this.criteriaService.groupSuggestion('', 1, 100)
            .subscribe(function (result) {
            _this.listGroups = result.items;
        });
    };
    CriteriaFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-criteria-form',
            template: __webpack_require__(/*! ./criteria-form.component.html */ "./src/app/modules/hr/assessment/criteria/criteria-form/criteria-form.component.html"),
            providers: [_validators_number_validator__WEBPACK_IMPORTED_MODULE_10__["NumberValidator"]],
            styles: [__webpack_require__(/*! ./criteria-form.component.css */ "./src/app/modules/hr/assessment/criteria/criteria-form/criteria-form.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_8__["ToastrService"],
            _validators_number_validator__WEBPACK_IMPORTED_MODULE_10__["NumberValidator"],
            _servies_criteria_service__WEBPACK_IMPORTED_MODULE_4__["CriteriaService"]])
    ], CriteriaFormComponent);
    return CriteriaFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/criteria-position/criteria-position-selection/criteria-position-selection.component.css":
/*!****************************************************************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/criteria-position/criteria-position-selection/criteria-position-selection.component.css ***!
  \****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaHIvYXNzZXNzbWVudC9jcml0ZXJpYS9jcml0ZXJpYS1wb3NpdGlvbi9jcml0ZXJpYS1wb3NpdGlvbi1zZWxlY3Rpb24vY3JpdGVyaWEtcG9zaXRpb24tc2VsZWN0aW9uLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/criteria-position/criteria-position-selection/criteria-position-selection.component.html":
/*!*****************************************************************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/criteria-position/criteria-position-selection/criteria-position-selection.component.html ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"portlet light\">\r\n    <div class=\"portlet-title\">\r\n        <div class=\"caption font-green-sharp\">\r\n            <span class=\"caption-subject bold uppercase\"> Danh sách tiêu chí đánh giá hiệu quả công việc.</span>\r\n        </div>\r\n    </div>\r\n    <div class=\"portlet-body\">\r\n        <div class=\"col-sm-12\">\r\n            <form action=\"\" class=\"form-inline\" (ngSubmit)=\"search()\">\r\n                <div class=\"form-group cm-mgr-5\">\r\n                    <input type=\"text\" class=\"form-control\" placeholder=\"Nhập tên tiêu chí cần tìm\"\r\n                           [(ngModel)]=\"keyword\" name=\"keyword\">\r\n                </div>\r\n                <div class=\"form-group cm-mgr-5\">\r\n                    <nh-select\r\n                        [data]=\"listGroups\"\r\n                        title=\"-- Tất cả --\"\r\n                        [(ngModel)]=\"groupId\" name=\"groupId\"></nh-select>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <button class=\"btn blue\">\r\n                        <i class=\"fa fa-search\"></i>\r\n                    </button>\r\n                </div>\r\n            </form>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <!--<div class=\"table-responsive\">-->\r\n                <table class=\"table table-striped table-bordered table-hover\">\r\n                    <thead>\r\n                    <tr>\r\n                        <th class=\"center middle w50\"></th>\r\n                        <th class=\"center middle w50\">STT</th>\r\n                        <th class=\"center middle w250\">Tên tiêu chí</th>\r\n                        <th class=\"center middle w250\">Nhóm tiêu chí</th>\r\n                        <th class=\"center middle w100\">Điểm chuẩn</th>\r\n                        <th class=\"center middle w250\">Chỉ tiêu đánh giá tháng/tiêu chuẩn</th>\r\n                        <th class=\"center middle w250\">Quy định chế tài</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                    <tr *ngFor=\"let item of listItems$ | async; let i = index\">\r\n                        <td class=\"center\">\r\n                            <button type=\"button\" class=\"btn blue btn-sm\"\r\n                                    *ngIf=\"!item.isSelected; else buttonSelectedTemplate\"\r\n                                    (click)=\"select(item)\">\r\n                                Chọn\r\n                            </button>\r\n                            <ng-template #buttonSelectedTemplate>\r\n                                <button type=\"button\" class=\"btn blue\" disabled>\r\n                                    Đã chọn\r\n                                </button>\r\n                            </ng-template>\r\n                        </td>\r\n                        <td class=\"center middle\">{{ (currentPage - 1) * pageSize + i + 1 }}</td>\r\n                        <td>{{ item.name }}</td>\r\n                        <td>{{ item.groupName }}</td>\r\n                        <td>{{ item.point }}</td>\r\n                        <td>{{ item.description }}</td>\r\n                        <td>{{ item.sanction }}</td>\r\n                    </tr>\r\n                    </tbody>\r\n                </table>\r\n            <!--</div>-->\r\n        </div>\r\n    </div>\r\n    <div class=\"portlet-foot text-right\">\r\n        <button type=\"button\" class=\"btn btn-default\" [mat-dialog-close]=\"hasChange\">\r\n            Đóng\r\n        </button>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/criteria-position/criteria-position-selection/criteria-position-selection.component.ts":
/*!***************************************************************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/criteria-position/criteria-position-selection/criteria-position-selection.component.ts ***!
  \***************************************************************************************************************************************/
/*! exports provided: CriteriaPositionSelectionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CriteriaPositionSelectionComponent", function() { return CriteriaPositionSelectionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _servies_criteria_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../servies/criteria.service */ "./src/app/modules/hr/assessment/criteria/servies/criteria.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _servies_criteria_config_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../servies/criteria-config.service */ "./src/app/modules/hr/assessment/criteria/servies/criteria-config.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");









var CriteriaPositionSelectionComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](CriteriaPositionSelectionComponent, _super);
    function CriteriaPositionSelectionComponent(data, toastr, criteriaService, criteriaConfigService) {
        var _this = _super.call(this) || this;
        _this.data = data;
        _this.toastr = toastr;
        _this.criteriaService = criteriaService;
        _this.criteriaConfigService = criteriaConfigService;
        _this.selectedIds = [];
        _this.listGroups = [];
        if (_this.data) {
            _this.selectedIds = _this.data.selectedIds;
            _this.listGroups = _this.data.listGroups;
            _this.positionId = _this.data.positionId;
        }
        return _this;
    }
    CriteriaPositionSelectionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.search();
        this.subscribers.getGroups = this.criteriaService.groupSuggestion('')
            .subscribe(function (result) {
            _this.listGroups = result.items;
        });
    };
    CriteriaPositionSelectionComponent.prototype.search = function (currentPage) {
        var _this = this;
        if (currentPage === void 0) { currentPage = 1; }
        this.currentPage = currentPage;
        this.listItems$ = this.criteriaService.searchActivated(this.keyword, this.groupId, this.currentPage)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            var items = lodash__WEBPACK_IMPORTED_MODULE_6__["cloneDeep"](result.items);
            items.forEach(function (criteria) {
                criteria.isSelected = lodash__WEBPACK_IMPORTED_MODULE_6__["indexOf"](_this.selectedIds, criteria.id) >= 0;
            });
            return items;
        }));
    };
    CriteriaPositionSelectionComponent.prototype.select = function (criteria) {
        var _this = this;
        if (!this.positionId) {
            this.toastr.warning('Vui lòng chọn chức vụ.');
            return;
        }
        this.criteriaConfigService.insert(this.positionId, criteria.id)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
            criteria.isSelected = true;
            _this.hasChange = true;
        });
    };
    CriteriaPositionSelectionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-criteria-position-selection',
            template: __webpack_require__(/*! ./criteria-position-selection.component.html */ "./src/app/modules/hr/assessment/criteria/criteria-position/criteria-position-selection/criteria-position-selection.component.html"),
            providers: [_servies_criteria_config_service__WEBPACK_IMPORTED_MODULE_7__["CriteriaConfigService"]],
            styles: [__webpack_require__(/*! ./criteria-position-selection.component.css */ "./src/app/modules/hr/assessment/criteria/criteria-position/criteria-position-selection/criteria-position-selection.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, ngx_toastr__WEBPACK_IMPORTED_MODULE_8__["ToastrService"],
            _servies_criteria_service__WEBPACK_IMPORTED_MODULE_2__["CriteriaService"],
            _servies_criteria_config_service__WEBPACK_IMPORTED_MODULE_7__["CriteriaConfigService"]])
    ], CriteriaPositionSelectionComponent);
    return CriteriaPositionSelectionComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_4__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/criteria-position/criteria-position.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/criteria-position/criteria-position.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@listWarehousePageTitle\">Cấu hình tiêu chí đánh giá</span>\r\n    <small i18n=\"@@productModuleTitle\">Đánh giá hiệu quả công việc</small>\r\n</h1>\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-sm-4\">\r\n        <div class=\"portlet light bordered\">\r\n            <div class=\"portlet-title\">\r\n                <div class=\"caption font-green-sharp\">\r\n                    <i class=\"icon-share font-green-sharp\"></i>\r\n                    <span class=\"caption-subject bold uppercase\"> Chức vụ </span>\r\n                </div>\r\n            </div>\r\n            <div class=\"portlet-body\" style=\"height: 450px; max-height: 450px; overflow: auto;\">\r\n                <nh-tree [data]=\"officeTree\"\r\n                         [isMultiple]=\"false\"\r\n                         (nodeSelected)=\"onItemSelected($event)\"></nh-tree>\r\n            </div>\r\n        </div>\r\n    </div><!-- END: list office -->\r\n    <div class=\"col-sm-8\">\r\n        <div class=\"portlet light bordered\">\r\n            <div class=\"portlet-title\">\r\n                <div class=\"caption font-green-sharp\">\r\n                    <i class=\"icon-share font-green-sharp\"></i>\r\n                    <span class=\"caption-subject bold uppercase\"> Danh sách tiêu chí </span>\r\n                </div>\r\n            </div>\r\n            <div class=\"portlet-body\">\r\n                <form action=\"\" class=\"form-inline\">\r\n                    <div class=\"form-group\">\r\n                        <nh-select [data]=\"listStatus\"\r\n                                   title=\"-- Tất cả trạng thái --\"\r\n                                   (itemSelected)=\"onStatusSelected($event)\"></nh-select>\r\n                    </div>\r\n                    <div class=\"form-group pull-right\">\r\n                        <button type=\"button\" class=\"btn blue cm-mgr-5\"\r\n                                [disabled]=\"!positionId\"\r\n                                (click)=\"requestReassessment()\">\r\n                            Yêu cầu đánh giá lại\r\n                        </button>\r\n                        <button type=\"button\" class=\"btn blue\" (click)=\"add()\" [disabled]=\"!positionId\">Thêm</button>\r\n                    </div>\r\n                </form><!-- END: filter form -->\r\n                <div class=\"table-responsive\">\r\n                    <table class=\"table table-bordered table-stripped table-hover\">\r\n                        <thead>\r\n                        <tr>\r\n                            <th class=\"center middle w50\">STT</th>\r\n                            <th class=\"center middle w250\">Tên tiêu chí</th>\r\n                            <th class=\"center middle w100\">Điểm chuẩn</th>\r\n                            <th class=\"center middle w50\">Sử dụng</th>\r\n                            <th class=\"center middle w250\">Chỉ tiêu đánh giá tháng/tiêu chuẩn</th>\r\n                            <th class=\"center middle w250\">Quy định chế tài</th>\r\n                            <th class=\"center middle\"></th>\r\n                        </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                        <ng-container *ngFor=\"let group of listItems; let i = index\">\r\n                            <tr class=\"bg-info\">\r\n                                <td class=\"center middle\">{{ i + 1 }}</td>\r\n                                <td>{{ group.groupName }}</td>\r\n                                <td class=\"text-right middle\">{{ group.totalPoint }}</td>\r\n                                <td colspan=\"4\"></td>\r\n                            </tr>\r\n                            <tr *ngFor=\"let criteria of group.criterias; let j = index\">\r\n                                <td class=\"center middle\">{{ i + 1 }}</td>\r\n                                <td>{{ criteria.criteriaName }}</td>\r\n                                <td class=\"text-right middle\">{{ criteria.point }}</td>\r\n                                <td class=\"center middle\">\r\n                                    <mat-checkbox color=\"primary\"\r\n                                                  [checked]=\"criteria.isActive\"\r\n                                                  [disabled]=\"!permission.edit\"\r\n                                                  (change)=\"onChangeStatus(criteria)\"></mat-checkbox>\r\n                                </td>\r\n                                <td>\r\n                                    {{ criteria.description }}\r\n                                </td>\r\n                                <td>{{ criteria.sanction }}</td>\r\n                                <td class=\"center\">\r\n                                    <button class=\"btn btn-danger btn-sm\"\r\n                                            [swal]=\"confirmDeleteCriteriaPosition\"\r\n                                            (confirm)=\"delete(group.criterias, criteria)\">\r\n                                        <i class=\"fa fa-trash-o\"></i>\r\n                                    </button>\r\n                                </td>\r\n                            </tr>\r\n                        </ng-container>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div><!-- END: list criteria -->\r\n</div>\r\n\r\n<swal\r\n    #confirmDeleteCriteriaPosition\r\n    title=\"Bạn có chắc chắn muốn hủy bỏ tiêu chí này?\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<swal\r\n    #confirmRequestReassessment\r\n    title=\"Bạn có chắc chắn muốn yêu cầu người dùng đánh giá lại?\"\r\n    text=\"Lưu ý: sau khi yêu cầu đánh giá lại kết quả đánh giá của người dùng dẽ bị hủy.\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"false\">\r\n</swal>\r\n\r\n<nh-modal #requestReassessmentModal>\r\n    <nh-modal-header>\r\n        Yêu cầu đánh giá lại\r\n    </nh-modal-header>\r\n    <form action=\"\" class=\"form-horizontal\" (ngSubmit)=\"confirmRequest()\">\r\n        <nh-modal-content>\r\n            <div class=\"form-group\">\r\n                <div class=\"col-sm-12\">\r\n                    <div class=\"alert alert-info\">\r\n                        <b>Lưu ý: Chỉ có thể yêu cầu đánh giá lại 2 tháng gần nhất.</b>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"\" class=\"col-sm-3 control-label\" ghmLabel=\"Tháng\"></label>\r\n                <div class=\"col-sm-9\">\r\n                    <nh-select\r\n                        name=\"month\"\r\n                        title=\"-- Chọn tháng --\"\r\n                        [liveSearch]=\"true\"\r\n                        [data]=\"listMonth\"\r\n                        [(ngModel)]=\"month\"></nh-select>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"\" class=\"col-sm-3 control-label\" ghmLabel=\"Năm\"></label>\r\n                <div class=\"col-sm-9\">\r\n                    <nh-select\r\n                        name=\"year\"\r\n                        title=\"-- Chọn năm --\"\r\n                        [liveSearch]=\"true\"\r\n                        [data]=\"listYear\"\r\n                        [(ngModel)]=\"year\"></nh-select>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <div class=\"col-sm-9 col-sm-offset-3\">\r\n                    <div>\r\n                        <mat-checkbox [checked]=\"applyForAll\" color=\"primary\">\r\n                            <span>Áp dụng tất cả</span>\r\n                        </mat-checkbox>\r\n                    </div>\r\n                    <span class=\"help-text\">\r\n                        <ng-container *ngIf=\"applyForAll; else applyForInCompleteTemplate\">Áp dụng tất cả bài bản đánh giá.</ng-container>\r\n                        <ng-template #applyForInCompleteTemplate>\r\n                            Chỉ áp dụng đối với các bản đánh giá chưa hoàn thành.\r\n                        </ng-template>\r\n                    </span>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <button class=\"btn blue cm-mgr-5\">Gửi yêu cầu</button>\r\n            <button type=\"button\" class=\"btn default\" nh-dismiss>Hủy bỏ</button>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/criteria-position/criteria-position.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/criteria-position/criteria-position.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: CriteriaPositionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CriteriaPositionComponent", function() { return CriteriaPositionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _servies_criteria_position_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../servies/criteria-position.service */ "./src/app/modules/hr/assessment/criteria/servies/criteria-position.service.ts");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _criteria_position_selection_criteria_position_selection_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./criteria-position-selection/criteria-position-selection.component */ "./src/app/modules/hr/assessment/criteria/criteria-position/criteria-position-selection/criteria-position-selection.component.ts");
/* harmony import */ var _services_user_assessment_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/user-assessment.service */ "./src/app/modules/hr/assessment/services/user-assessment.service.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");











var CriteriaPositionComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](CriteriaPositionComponent, _super);
    function CriteriaPositionComponent(dialog, toastr, utilService, criteriaPositionService, userAssessmentService) {
        var _this = _super.call(this) || this;
        _this.dialog = dialog;
        _this.toastr = toastr;
        _this.utilService = utilService;
        _this.criteriaPositionService = criteriaPositionService;
        _this.userAssessmentService = userAssessmentService;
        _this.listStatus = [];
        _this.officeTree = [];
        _this.selectedIds = [];
        _this.listMonth = [];
        _this.listYear = [];
        _this.applyForAll = true;
        _this.subscribers.getOfficePositionTree = _this.criteriaPositionService.getOfficePositionTree()
            .subscribe(function (result) {
            _this.officeTree = result.data;
        });
        _this.listStatus = [
            { id: false, name: 'Không sử dụng' },
            { id: true, name: 'Đang sử dụng' },
        ];
        _this.appService.setupPage(_this.pageId.ASSESSMENT, _this.pageId.CRITERIA_CONFIG, 'Cấu hình tiêu chí đánh giá hiệu quả công việc', 'Đánh giá hiệu quả CV');
        return _this;
    }
    CriteriaPositionComponent.prototype.ngOnInit = function () {
        // this.subscribers.afterDilogClosed = this.dialog.afterAllClosed.subscribe(() => {
        //     if (this.positionId) {
        //         this.search();
        //     }
        // });
        this.listMonth = this.utilService.renderListMonth();
        this.listYear = this.utilService.renderListYear();
    };
    CriteriaPositionComponent.prototype.onItemSelected = function (item) {
        if (item.data.isOffice) {
            this.toastr.warning('Vui lòng chọn chức vụ.');
            return;
        }
        if (this.positionId === item.id) {
            return;
        }
        this.positionId = item.id;
        this.search();
    };
    CriteriaPositionComponent.prototype.onStatusSelected = function (status) {
        if (this.isActive === status.id) {
            return;
        }
        this.isActive = status.id;
        this.search();
    };
    CriteriaPositionComponent.prototype.onChangeStatus = function (criteria) {
        var _this = this;
        criteria.isActive = !criteria.isActive;
        this.criteriaPositionService.updateActiveStatus(criteria.positionId, criteria.criteriaId, criteria.isActive)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
        });
    };
    CriteriaPositionComponent.prototype.search = function () {
        var _this = this;
        this.listItems = [];
        this.selectedIds = [];
        if (!this.positionId) {
            this.toastr.warning('Vui lòng chọn chức vụ');
            return;
        }
        this.subscribers.search = this.criteriaPositionService.search(this.positionId, this.isActive)
            .subscribe(function (result) {
            if (result.items) {
                _this.selectedIds = result.items.map(function (criteriaPosition) { return criteriaPosition.criteriaId; });
                var groups = lodash__WEBPACK_IMPORTED_MODULE_3__["groupBy"](result.items, 'groupId');
                if (groups) {
                    for (var key in groups) {
                        if (groups.hasOwnProperty(key)) {
                            _this.listItems = _this.listItems.concat([{
                                    groupId: key,
                                    groupName: groups[key][0].groupName,
                                    totalPoint: lodash__WEBPACK_IMPORTED_MODULE_3__["sumBy"](groups[key], 'point'),
                                    criterias: groups[key]
                                }]);
                        }
                    }
                }
            }
            else {
                _this.listItems = [];
            }
        });
    };
    CriteriaPositionComponent.prototype.add = function () {
        var _this = this;
        if (!this.positionId) {
            this.toastr.warning('Vui lòng chọn chức vụ.');
            return;
        }
        var criteriaSelectionDialog = this.dialog.open(_criteria_position_selection_criteria_position_selection_component__WEBPACK_IMPORTED_MODULE_7__["CriteriaPositionSelectionComponent"], {
            data: {
                positionId: this.positionId,
                selectedIds: this.selectedIds
            }
        });
        criteriaSelectionDialog.afterClosed().subscribe(function (hasChange) {
            if (hasChange && _this.positionId) {
                _this.search();
            }
        });
    };
    CriteriaPositionComponent.prototype.requestReassessment = function () {
        if (!this.positionId) {
            this.toastr.warning('Vui lòng chọn chức vụ cần đánh giá lại.');
            return;
        }
        this.requestReassessmentModal.open();
    };
    CriteriaPositionComponent.prototype.confirmRequest = function () {
        var _this = this;
        this.userAssessmentService.requestReassessment(this.positionId, this.month, this.year, this.applyForAll)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
            _this.requestReassessmentModal.dismiss();
        });
    };
    CriteriaPositionComponent.prototype.delete = function (criterias, criteria) {
        var _this = this;
        if (!criteria.criteriaId) {
            this.toastr.warning('Vui lòng chọn chức vụ.');
            return;
        }
        if (!criteria.criteriaId) {
            this.toastr.warning('Vui lòng chọn tiêu chí cần xóa.');
            return;
        }
        this.criteriaPositionService.delete(criteria.positionId, criteria.criteriaId)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
            lodash__WEBPACK_IMPORTED_MODULE_3__["remove"](criterias, criteria);
            lodash__WEBPACK_IMPORTED_MODULE_3__["remove"](_this.selectedIds, function (id) {
                return id === criteria.criteriaId;
            });
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('requestReassessmentModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_10__["NhModalComponent"])
    ], CriteriaPositionComponent.prototype, "requestReassessmentModal", void 0);
    CriteriaPositionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-criteria-position',
            template: __webpack_require__(/*! ./criteria-position.component.html */ "./src/app/modules/hr/assessment/criteria/criteria-position/criteria-position.component.html"),
            providers: [_servies_criteria_position_service__WEBPACK_IMPORTED_MODULE_5__["CriteriaPositionService"], _services_user_assessment_service__WEBPACK_IMPORTED_MODULE_8__["UserAssessmentService"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialog"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_9__["UtilService"],
            _servies_criteria_position_service__WEBPACK_IMPORTED_MODULE_5__["CriteriaPositionService"],
            _services_user_assessment_service__WEBPACK_IMPORTED_MODULE_8__["UserAssessmentService"]])
    ], CriteriaPositionComponent);
    return CriteriaPositionComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_6__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/criteria.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/criteria.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaHIvYXNzZXNzbWVudC9jcml0ZXJpYS9jcml0ZXJpYS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/criteria.component.html":
/*!************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/criteria.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@listWarehousePageTitle\">Duyệt đánh giá hiệu quả công việc</span>\r\n    <small i18n=\"@@productModuleTitle\">Đánh giá hiệu quả công việc</small>\r\n</h1>\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <form class=\"form-inline\" (ngSubmit)=\"search()\">\r\n            <div class=\"form-group cm-mgr-5\">\r\n                <ghm-select [data]=\"listGroups\"\r\n                            [elementId]=\"'listGroups'\"\r\n                            title=\"-- Tất cả nhóm tiêu chí--\"\r\n                            [(ngModel)]=\"groupId\"\r\n                            name=\"groupId\"\r\n                ></ghm-select>\r\n            </div>\r\n            <div class=\"form-group cm-mgr-5\">\r\n                <ghm-select [data]=\"listStatus\"\r\n                            [elementId]=\"'listStatus'\"\r\n                            title=\"-- Tất cả trạng thái --\"\r\n                            name=\"isActive\"\r\n                            [(ngModel)]=\"isActive\"\r\n                            (itemSelected)=\"onStatusSelected($event)\"></ghm-select>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <ghm-input [icon]=\"'fa fa-search'\" placeholder=\"Nhập từ khóa tìm kiếm\" [(ngModel)]=\"keyword\"\r\n                           (remove)=\"search(1)\"\r\n                           name=\"keyword\"></ghm-input>\r\n            </div>\r\n            <div class=\"form-group cm-mgr-5\">\r\n                <button class=\"btn blue\">\r\n                    <i class=\"fa fa-search\"></i>\r\n                </button>\r\n            </div>\r\n            <div class=\"form-group pull-right\" (click)=\"add()\">\r\n                <button type=\"button\" class=\"btn blue\">Thêm</button>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"\">\r\n    <table class=\"table table-bordered table-stripped\">\r\n        <thead>\r\n        <tr>\r\n            <th class=\"center middle w50\">STT</th>\r\n            <th class=\"center middle w250\">Tên tiêu chí</th>\r\n            <th class=\"center middle w250\">Nhóm tiêu chí</th>\r\n            <th class=\"center middle\">Điểm chuẩn</th>\r\n            <th class=\"center middle\">Sử dụng</th>\r\n            <th class=\"center middle w250\">Chỉ tiêu đánh giá tháng/tiêu chuẩn</th>\r\n            <th class=\"center middle w250\">Quy định chế tài</th>\r\n            <th class=\"center middle w50\"></th>\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n        <tr *ngFor=\"let item of listItems$ | async; let i = index\">\r\n            <td class=\"center\">{{ (currentPage - 1) * pageSize + i + 1 }}</td>\r\n            <td>{{ item.name }}</td>\r\n            <td>{{ item.groupName }}</td>\r\n            <td class=\"text-right\">{{ item.point }}</td>\r\n            <td class=\"text-center\">\r\n                <mat-slide-toggle color=\"primary\" [checked]=\"item.isActive\" [disabled]=\"true\"></mat-slide-toggle>\r\n            </td>\r\n            <td>{{ item.description }}</td>\r\n            <td>{{ item.sanction }}</td>\r\n            <td class=\"center\">\r\n                <nh-dropdown>\r\n                    <button type=\"button\" class=\"btn btn-xs btn-light btn-no-background no-border\" matTooltip=\"Menu\">\r\n                        <mat-icon>more_horiz</mat-icon>\r\n                    </button>\r\n                    <ul class=\"nh-dropdown-menu right\" role=\"menu\">\r\n                        <li>\r\n                            <a *ngIf=\"permission.view\"\r\n                               (click)=\"detail(item.id)\"\r\n                               i18n=\"@@view\">\r\n                                <mat-icon class=\"menu-icon\">info</mat-icon>\r\n                                Chi tiết\r\n                            </a>\r\n                        </li>\r\n                        <li>\r\n                            <a *ngIf=\"permission.edit\"\r\n                               (click)=\"edit(item.id)\"\r\n                               i18n=\"@@edit\">\r\n                                <mat-icon class=\"menu-icon\">edit</mat-icon>\r\n                                Sửa\r\n                            </a>\r\n                        </li>\r\n                        <li>\r\n                            <a\r\n                                [swal]=\"confirmDelete\"\r\n                                (click)=\"confirm(item.id)\" i18n=\"@@delete\">\r\n                                <mat-icon class=\"menu-icon\">delete</mat-icon>\r\n                                Xóa\r\n                            </a>\r\n                        </li>\r\n                    </ul>\r\n                </nh-dropdown>\r\n            </td>\r\n        </tr>\r\n        </tbody>\r\n    </table>\r\n</div>\r\n<ghm-paging [totalRows]=\"totalRows\"\r\n            [currentPage]=\"currentPage\"\r\n            [pageShow]=\"6\"\r\n            [isDisabled]=\"isSearching\"\r\n            [pageSize]=\"pageSize\"\r\n            (pageClick)=\"search($event)\"\r\n></ghm-paging>\r\n\r\n<swal\r\n    #confirmDelete\r\n    i18n=\"@@confirmDeleteCriteria\"\r\n    i18n-title=\"@@confirmTitleDeleteCriteria\"\r\n    i18n-text=\"@@confirmTextDeleteWarehouse\"\r\n    title=\"Bạn có chắc chắn muốn xóa tiêu chí đánh giá này không?\"\r\n    text=\"Bạn không thể lấy lại tiêu chí này sau khi xóa.\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Đồng ý\"\r\n    cancelButtonText=\"Hủy bỏ\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n"

/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/criteria.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/criteria.component.ts ***!
  \**********************************************************************/
/*! exports provided: CriteriaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CriteriaComponent", function() { return CriteriaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _servies_criteria_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./servies/criteria.service */ "./src/app/modules/hr/assessment/criteria/servies/criteria.service.ts");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _criteria_form_criteria_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./criteria-form/criteria-form.component */ "./src/app/modules/hr/assessment/criteria/criteria-form/criteria-form.component.ts");
/* harmony import */ var _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @toverux/ngx-sweetalert2 */ "./node_modules/@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");










var CriteriaComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](CriteriaComponent, _super);
    function CriteriaComponent(route, dialog, toastr, criteriaService) {
        var _this = _super.call(this) || this;
        _this.route = route;
        _this.dialog = dialog;
        _this.toastr = toastr;
        _this.criteriaService = criteriaService;
        _this.listGroups = [];
        _this.listStatus = [];
        _this.listItems$ = _this.route.data.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            var data = result.data;
            _this.totalRows = data.totalRows;
            return data.items;
        }));
        _this.listStatus = [
            { id: true, name: 'Sử dụng' },
            { id: false, name: 'Không sử dụng' },
        ];
        _this.subscribers.onDialogClosed = _this.dialog.afterAllClosed.subscribe(function () {
            _this.search();
        });
        return _this;
    }
    CriteriaComponent.prototype.ngOnInit = function () {
        this.appService.setupPage(this.pageId.ASSESSMENT, this.pageId.CRITERIA_LIBRARY, 'Danh sách tiêu chí', 'Đánh giá hiệu quả CV');
        if (this.listGroups.length === 0) {
            this.getCriteriaGroups();
        }
    };
    CriteriaComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.swalConfirmDelete.confirm.subscribe(function (id) {
            _this.delete(_this.id);
        });
    };
    CriteriaComponent.prototype.confirm = function (id) {
        this.swalConfirmDelete.show();
        this.id = id;
    };
    CriteriaComponent.prototype.onStatusSelected = function (status) {
        // this.isActive = status.id;
    };
    CriteriaComponent.prototype.add = function () {
        var _this = this;
        var criteriaGroupDialog = this.dialog.open(_criteria_form_criteria_form_component__WEBPACK_IMPORTED_MODULE_7__["CriteriaFormComponent"], { data: { groups: this.listGroups } });
        criteriaGroupDialog.afterClosed().subscribe(function (result) {
            if (result) {
                _this.getCriteriaGroups();
            }
        });
    };
    CriteriaComponent.prototype.edit = function (id) {
        this.dialog.open(_criteria_form_criteria_form_component__WEBPACK_IMPORTED_MODULE_7__["CriteriaFormComponent"], { data: { id: id, groups: this.listGroups } });
    };
    CriteriaComponent.prototype.detail = function (id) {
        this.dialog.open(_criteria_form_criteria_form_component__WEBPACK_IMPORTED_MODULE_7__["CriteriaFormComponent"], { data: { id: id, readonly: true } });
    };
    CriteriaComponent.prototype.delete = function (id) {
        var _this = this;
        this.subscribers.delete = this.criteriaService.delete(id)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
            _this.search();
        });
    };
    CriteriaComponent.prototype.search = function (currentPage) {
        var _this = this;
        if (currentPage === void 0) { currentPage = 1; }
        this.currentPage = currentPage;
        this.listItems$ = this.criteriaService.search(this.keyword, this.groupId, this.isActive, this.currentPage, this.appConfig.PAGE_SIZE)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.totalRows = result.totalRows;
            return result.items;
        }));
    };
    CriteriaComponent.prototype.getCriteriaGroups = function () {
        var _this = this;
        this.subscribers.getGroups = this.criteriaService.groupSuggestion('')
            .subscribe(function (result) {
            _this.listGroups = result.items;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('confirmDelete'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_8__["SwalComponent"])
    ], CriteriaComponent.prototype, "swalConfirmDelete", void 0);
    CriteriaComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-criteria',
            template: __webpack_require__(/*! ./criteria.component.html */ "./src/app/modules/hr/assessment/criteria/criteria.component.html"),
            styles: [__webpack_require__(/*! ./criteria.component.css */ "./src/app/modules/hr/assessment/criteria/criteria.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatDialog"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_9__["ToastrService"],
            _servies_criteria_service__WEBPACK_IMPORTED_MODULE_2__["CriteriaService"]])
    ], CriteriaComponent);
    return CriteriaComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_3__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/group/group-form/group-form.component.css":
/*!******************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/group/group-form/group-form.component.css ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaHIvYXNzZXNzbWVudC9jcml0ZXJpYS9ncm91cC9ncm91cC1mb3JtL2dyb3VwLWZvcm0uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/group/group-form/group-form.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/group/group-form/group-form.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"portlet light cm-mgb-0\" style=\"min-width: 450px;\">\r\n    <div class=\"portlet-title\">\r\n        <div class=\"caption font-green-sharp\">\r\n            <i class=\"icon-plus font-green-sharp\"></i>\r\n            <span class=\"caption-subject bold uppercase\">\r\n                <span *ngIf=\"readonly; else editTitleTemplate\">Chi tiết nhóm tiêu chí</span>\r\n                <ng-template #editTitleTemplate>\r\n                    {isUpdate, select, 0 {Thêm mới Nhóm} 1 {Cập nhật nhóm tiêu chí} other {}}\r\n                </ng-template>\r\n            </span>\r\n        </div>\r\n    </div>\r\n    <form action=\"\" class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <div class=\"portlet-body form\">\r\n            <div class=\"form-body\">\r\n                <div formArrayName=\"translations\">\r\n                    <div class=\"form-group\" *ngIf=\"languages && languages.length > 1\">\r\n                        <label i18n-ghmLabel=\"@@language\" ghmLabel=\"Language\"\r\n                               class=\"col-sm-4 control-label\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <nh-select [data]=\"languages\"\r\n                                       i18n-title=\"@@pleaseSelectLanguage\"\r\n                                       title=\"-- Chọn ngôn ngữ --\"\r\n                                       name=\"language\"\r\n                                       [(value)]=\"currentLanguage\"\r\n                                       (onSelectItem)=\"currentLanguage = $event.id\"></nh-select>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group\"\r\n                         *ngFor=\"let modelTranslation of translations.controls; index as i\"\r\n                         [hidden]=\"modelTranslation.value.languageId !== currentLanguage\"\r\n                         [formGroupName]=\"i\"\r\n                         [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.name\">\r\n                        <label for=\"\" class=\"label-control col-sm-4 control-label\" ghmLabel=\"Tên nhóm\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <input type=\"text\" class=\"form-control\" placeholder=\"Nhập tên nhóm\"\r\n                                   formControlName=\"name\">\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div formArrayName=\"translations\">\r\n                    <div class=\"form-group\"\r\n                         *ngFor=\"let modelTranslation of translations.controls; index as i\"\r\n                         [hidden]=\"modelTranslation.value.languageId !== currentLanguage\"\r\n                         [formGroupName]=\"i\"\r\n                         [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.description\">\r\n                        <label for=\"\" class=\"label-control col-sm-4 control-label\" ghmLabel=\"Mô tả\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <textarea class=\"form-control no-resizeable h100\" formControlName=\"description\"></textarea>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <label i18n-ghmLabel=\"@@language\" ghmLabel=\"Thứ tự\"\r\n                           class=\"col-sm-4 control-label\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <input type=\"text\" class=\"form-control\" formControlName=\"order\">\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <div class=\"col-sm-8 col-sm-offset-4\">\r\n                        <mat-slide-toggle\r\n                            color=\"primary\"\r\n                            formControlName=\"isActive\">Sử dụng\r\n                        </mat-slide-toggle>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"portlet-foot\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-12 text-right\">\r\n                    <mat-checkbox\r\n                        class=\"cm-mgr-5\"\r\n                        color=\"primary\"\r\n                        name=\"isCreateAnother\"\r\n                        i18n=\"@@isCreateAnother\"\r\n                        *ngIf=\"!isUpdate\"\r\n                        [(checked)]=\"isCreateAnother\"\r\n                        (change)=\"isCreateAnother = !isCreateAnother\"> Tiếp tục thêm\r\n                    </mat-checkbox>\r\n                    <button color=\"primary\" class=\"btn blue cm-mgr-5\" *ngIf=\"!readonly\">\r\n                        Lưu\r\n                    </button>\r\n                    <button type=\"button\" class=\"btn btn-default\" [mat-dialog-close]=\"isModified\">\r\n                        Hủy bỏ\r\n                    </button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </form>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/group/group-form/group-form.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/group/group-form/group-form.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: GroupFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupFormComponent", function() { return GroupFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_criteria_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../models/criteria.model */ "./src/app/modules/hr/assessment/criteria/models/criteria.model.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _servies_criteria_group_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../servies/criteria-group.service */ "./src/app/modules/hr/assessment/criteria/servies/criteria-group.service.ts");
/* harmony import */ var _models_criteria_group_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../models/criteria-group.model */ "./src/app/modules/hr/assessment/criteria/models/criteria-group.model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");










var GroupFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](GroupFormComponent, _super);
    function GroupFormComponent(data, dialog, toastr, criteriaGroupService) {
        var _this = _super.call(this) || this;
        _this.data = data;
        _this.dialog = dialog;
        _this.toastr = toastr;
        _this.criteriaGroupService = criteriaGroupService;
        _this.readonly = false;
        _this.criteriaGroup = new _models_criteria_group_model__WEBPACK_IMPORTED_MODULE_6__["CriteriaGroup"]();
        _this.criteriaGroupTranslation = new _models_criteria_group_model__WEBPACK_IMPORTED_MODULE_6__["CriteriaGroupTranslation"]();
        _this.buildFormLanguage = function (language) {
            _this.translationFormErrors[language] = _this.renderFormError(['name', 'description']);
            _this.translationValidationMessage[language] = _this.renderFormErrorMessage([
                { 'name': ['required', 'maxlength'] },
                { 'description': ['required', 'maxlength'] },
            ]);
            var translationModel = _this.formBuilder.group({
                languageId: [language],
                name: [{ value: _this.criteriaGroupTranslation.name, disabled: _this.readonly }, [
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(256)
                    ]],
                description: [{ value: _this.criteriaGroupTranslation.description, disabled: _this.readonly }, [
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(500)
                    ]]
            });
            translationModel.valueChanges.subscribe(function (data) { return _this.validateTranslation(false); });
            return translationModel;
        };
        return _this;
    }
    GroupFormComponent.prototype.ngOnInit = function () {
        this.renderForm();
        if (this.data && this.data.id) {
            this.id = this.data.id;
            this.getDetail();
        }
    };
    GroupFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.validateModel();
        var isLanguageValid = this.validateLanguage();
        if (isValid && isLanguageValid) {
            this.criteriaGroup = this.model.value;
            this.isSaving = true;
            if (this.isUpdate) {
                this.criteriaGroupService
                    .update(this.id, this.criteriaGroup)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return (_this.isSaving = false); }))
                    .subscribe(function (result) {
                    _this.toastr.success(result.message);
                    _this.dialog.getDialogById('criteriaGroupForm').close();
                });
            }
            else {
                this.criteriaGroupService
                    .insert(this.criteriaGroup)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return (_this.isSaving = false); }))
                    .subscribe(function (result) {
                    _this.toastr.success(result.message);
                    _this.isModified = true;
                    if (!_this.isCreateAnother) {
                        _this.dialog.getDialogById('criteriaGroupForm').close();
                    }
                    else {
                        _this.resetModel();
                    }
                });
            }
        }
    };
    GroupFormComponent.prototype.getDetail = function () {
        var _this = this;
        this.isUpdate = true;
        this.criteriaGroupService.getDetail(this.id)
            .subscribe(function (data) {
            _this.model.patchValue(data);
        });
    };
    GroupFormComponent.prototype.renderForm = function () {
        this.buildForm();
        this.renderTranslationArray(this.buildFormLanguage);
    };
    GroupFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.renderFormError(['isActive', 'order']);
        this.validationMessages = this.renderFormErrorMessage([
            { 'isActive': ['required'] },
            { 'order': ['required'] },
        ]);
        this.model = this.formBuilder.group({
            isActive: [{ value: this.criteriaGroup.isActive, disabled: this.readonly }, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
                ]],
            order: [{ value: this.criteriaGroup.order, disabled: this.readonly }, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
                ]],
            concurrencyStamp: [this.criteriaGroup.concurrencyStamp],
            translations: this.formBuilder.array([])
        });
        this.model.valueChanges.subscribe(function () { return _this.validateModel(false); });
    };
    GroupFormComponent.prototype.resetModel = function () {
        this.id = null;
        this.isUpdate = false;
        this.model.patchValue(new _models_criteria_model__WEBPACK_IMPORTED_MODULE_2__["Criteria"]());
        this.translations.controls.forEach(function (model) {
            model.patchValue({
                name: '',
                description: '',
                sanction: ''
            });
        });
        this.clearFormError(this.formErrors);
        this.clearFormError(this.translationFormErrors);
    };
    GroupFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-group-form',
            template: __webpack_require__(/*! ./group-form.component.html */ "./src/app/modules/hr/assessment/criteria/group/group-form/group-form.component.html"),
            providers: [_servies_criteria_group_service__WEBPACK_IMPORTED_MODULE_5__["CriteriaGroupService"]],
            styles: [__webpack_require__(/*! ./group-form.component.css */ "./src/app/modules/hr/assessment/criteria/group/group-form/group-form.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_9__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatDialog"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_8__["ToastrService"],
            _servies_criteria_group_service__WEBPACK_IMPORTED_MODULE_5__["CriteriaGroupService"]])
    ], GroupFormComponent);
    return GroupFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_4__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/group/group.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/group/group.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaHIvYXNzZXNzbWVudC9jcml0ZXJpYS9ncm91cC9ncm91cC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/group/group.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/group/group.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@listWarehousePageTitle\">Danh sách nhóm tiêu chí đánh giá thành tích</span>\r\n    <small i18n=\"@@productModuleTitle\">Đánh giá hiệu quả công việc</small>\r\n</h1>\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <form action=\"\" class=\"form-inline\" (ngSubmit)=\"search()\">\r\n            <div class=\"form-group\">\r\n                <input type=\"text\" [(ngModel)]=\"keyword\"\r\n                       class=\"form-control\"\r\n                       placeholder=\"Nhập tên nhóm cần tìm\"\r\n                       name=\"keyword\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <button class=\"btn btn-primary\" matTooltip=\"Tìm kiếm\">\r\n                    <i class=\"fa fa-search\"></i>\r\n                </button>\r\n            </div>\r\n            <div class=\"form-group pull-right\">\r\n                <button type=\"button\" class=\"btn btn-primary\" (click)=\"add()\">\r\n                    Thêm\r\n                </button>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <div class=\"table-responsive\">\r\n            <table class=\"table table-stripped table-hover\">\r\n                <thead>\r\n                <tr>\r\n                    <th class=\"center middle w50\">STT</th>\r\n                    <th class=\"center middle\">Tên nhóm</th>\r\n                    <th class=\"center middle\">Trạng thái</th>\r\n                    <th class=\"text-right\">Thư tự</th>\r\n                    <th class=\"center middle w100\"></th>\r\n                </tr>\r\n                </thead>\r\n                <tbody>\r\n                <tr *ngFor=\"let item of listItems; let i = index\">\r\n                    <td class=\"center\">{{ (i + 1) }}</td>\r\n                    <td>{{ item.name }}</td>\r\n                    <td class=\"center middle\">\r\n                        <mat-slide-toggle\r\n                            color=\"primary\"\r\n                            [checked]=\"item.isActive\"\r\n                            [disabled]=\"true\"></mat-slide-toggle>\r\n                    </td>\r\n                    <td class=\"text-right middle\">\r\n                        {{ item.order }}\r\n                    </td>\r\n                    <td class=\"text-right\">\r\n                        <button type=\"button\" class=\"btn btn-primary btn-sm\" (click)=\"edit(item.id)\">\r\n                            <i class=\"fa fa-edit\"></i>\r\n                        </button>\r\n                        <button type=\"button\" class=\"btn btn-danger btn-sm\"\r\n                                [swal]=\"confirmDelete\"\r\n                                (confirm)=\"delete(item.id)\">\r\n                            <i class=\"fa fa-trash-o\"></i>\r\n                        </button>\r\n                    </td>\r\n                </tr>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<swal\r\n    #confirmDelete\r\n    i18n=\"@@confirmDeleteCriteria\"\r\n    i18n-title=\"@@confirmTitleDeleteCriteria\"\r\n    i18n-text=\"@@confirmTextDeleteWarehouse\"\r\n    title=\"Bạn có chắc chắn muốn xóa nhóm tiêu chí này không?\"\r\n    text=\"Lưu ý: Sau khi xóa bạn không thể lấy lại được.\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"false\">\r\n</swal>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/group/group.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/group/group.component.ts ***!
  \*************************************************************************/
/*! exports provided: GroupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupComponent", function() { return GroupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _group_form_group_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./group-form/group-form.component */ "./src/app/modules/hr/assessment/criteria/group/group-form/group-form.component.ts");
/* harmony import */ var _servies_criteria_group_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../servies/criteria-group.service */ "./src/app/modules/hr/assessment/criteria/servies/criteria-group.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);








var GroupComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](GroupComponent, _super);
    function GroupComponent(dialog, toastr, criteriaGroupService) {
        var _this = _super.call(this) || this;
        _this.dialog = dialog;
        _this.toastr = toastr;
        _this.criteriaGroupService = criteriaGroupService;
        _this.subscribers.afterDialogClosed = _this.dialog.afterAllClosed.subscribe(function () {
            _this.search();
        });
        return _this;
    }
    GroupComponent.prototype.ngOnInit = function () {
        this.appService.setupPage(this.pageId.ASSESSMENT, this.pageId.CRITERIA_GROUP, 'Danh sách nhóm tiêu chí', 'Quản lý đánh giá hiệu quả công việc');
        this.search();
    };
    GroupComponent.prototype.search = function () {
        var _this = this;
        this.criteriaGroupService.search(this.keyword, this.isActive)
            .subscribe(function (result) {
            _this.totalRows = result.totalRows;
            _this.listItems = result.items;
        });
    };
    GroupComponent.prototype.add = function () {
        this.dialog.open(_group_form_group_form_component__WEBPACK_IMPORTED_MODULE_4__["GroupFormComponent"], { id: 'criteriaGroupForm', data: { id: null } });
    };
    GroupComponent.prototype.edit = function (id) {
        this.dialog.open(_group_form_group_form_component__WEBPACK_IMPORTED_MODULE_4__["GroupFormComponent"], { id: 'criteriaGroupForm', data: { id: id } });
    };
    GroupComponent.prototype.delete = function (id) {
        var _this = this;
        this.criteriaGroupService.delete(id)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
            lodash__WEBPACK_IMPORTED_MODULE_7__["remove"](_this.listItems, function (group) {
                return group.id === id;
            });
        });
    };
    GroupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-group',
            template: __webpack_require__(/*! ./group.component.html */ "./src/app/modules/hr/assessment/criteria/group/group.component.html"),
            providers: [_servies_criteria_group_service__WEBPACK_IMPORTED_MODULE_5__["CriteriaGroupService"]],
            styles: [__webpack_require__(/*! ./group.component.css */ "./src/app/modules/hr/assessment/criteria/group/group.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"],
            _servies_criteria_group_service__WEBPACK_IMPORTED_MODULE_5__["CriteriaGroupService"]])
    ], GroupComponent);
    return GroupComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/models/criteria-group.model.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/models/criteria-group.model.ts ***!
  \*******************************************************************************/
/*! exports provided: CriteriaGroup, CriteriaGroupTranslation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CriteriaGroup", function() { return CriteriaGroup; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CriteriaGroupTranslation", function() { return CriteriaGroupTranslation; });
var CriteriaGroup = /** @class */ (function () {
    function CriteriaGroup() {
        this.isActive = true;
        this.order = 0;
    }
    return CriteriaGroup;
}());

var CriteriaGroupTranslation = /** @class */ (function () {
    function CriteriaGroupTranslation() {
    }
    return CriteriaGroupTranslation;
}());



/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/models/criteria.model.ts":
/*!*************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/models/criteria.model.ts ***!
  \*************************************************************************/
/*! exports provided: Criteria, CriteriaTranslation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Criteria", function() { return Criteria; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CriteriaTranslation", function() { return CriteriaTranslation; });
var Criteria = /** @class */ (function () {
    function Criteria() {
        this.isActive = true;
    }
    return Criteria;
}());

var CriteriaTranslation = /** @class */ (function () {
    function CriteriaTranslation() {
    }
    return CriteriaTranslation;
}());



/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/servies/criteria-config.service.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/servies/criteria-config.service.ts ***!
  \***********************************************************************************/
/*! exports provided: CriteriaConfigService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CriteriaConfigService", function() { return CriteriaConfigService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../../environments/environment */ "./src/environments/environment.ts");







var CriteriaConfigService = /** @class */ (function () {
    function CriteriaConfigService(appConfig, spinnerService, http) {
        this.appConfig = appConfig;
        this.spinnerService = spinnerService;
        this.http = http;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].apiGatewayUrl + "api/v1/hr/criterias-positions";
    }
    CriteriaConfigService.prototype.getOfficePositionTree = function () {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .get(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].apiGatewayUrl + "api/v1/hr/offices-positions/tree")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    CriteriaConfigService.prototype.search = function (positionId, isActive) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .get(this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('positionId', positionId ? positionId : '')
                .set('isActive', isActive != null && isActive !== undefined ? isActive.toString() : '')
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    CriteriaConfigService.prototype.insert = function (positionId, criteriaId) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .post(this.url, {
            positionId: positionId,
            criteriaId: criteriaId
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    CriteriaConfigService.prototype.updateActiveStatus = function (positionId, criteriaId, isActive) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .post(this.url + "/" + positionId + "/" + criteriaId, isActive)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    CriteriaConfigService.prototype.delete = function (positionId, criteriaId) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .delete(this.url + "/" + positionId + "/" + criteriaId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    CriteriaConfigService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_3__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__["SpinnerService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], CriteriaConfigService);
    return CriteriaConfigService;
}());



/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/servies/criteria-group.service.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/servies/criteria-group.service.ts ***!
  \**********************************************************************************/
/*! exports provided: CriteriaGroupService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CriteriaGroupService", function() { return CriteriaGroupService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");






var CriteriaGroupService = /** @class */ (function () {
    function CriteriaGroupService(spinnerService, http) {
        this.spinnerService = spinnerService;
        this.http = http;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiGatewayUrl + "api/v1/hr/criterias/groups";
        // this.url = `${environment.apiGatewayUrl}${this.url}`;
    }
    CriteriaGroupService.prototype.search = function (keyword, isActive) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .get(this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
                .set('keyword', keyword ? keyword.toString() : '')
                .set('isActive', isActive != null && isActive !== undefined ? isActive.toString() : '')
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    CriteriaGroupService.prototype.insert = function (criteriaGroup) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .post(this.url, criteriaGroup)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    CriteriaGroupService.prototype.update = function (id, criteriaGroup) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .post(this.url + "/" + id, criteriaGroup)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    CriteriaGroupService.prototype.delete = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .delete(this.url + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    CriteriaGroupService.prototype.getDetail = function (id) {
        return this.http.get(this.url + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) { return result.data; }));
    };
    CriteriaGroupService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_2__["SpinnerService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], CriteriaGroupService);
    return CriteriaGroupService;
}());



/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/servies/criteria-position.service.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/servies/criteria-position.service.ts ***!
  \*************************************************************************************/
/*! exports provided: CriteriaPositionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CriteriaPositionService", function() { return CriteriaPositionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../../environments/environment */ "./src/environments/environment.ts");







var CriteriaPositionService = /** @class */ (function () {
    function CriteriaPositionService(appConfig, spinnerService, http) {
        this.appConfig = appConfig;
        this.spinnerService = spinnerService;
        this.http = http;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].apiGatewayUrl + "api/v1/hr/criterias-positions";
    }
    CriteriaPositionService.prototype.getOfficePositionTree = function () {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .get(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].apiGatewayUrl + "api/v1/hr/offices-positions/tree")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    CriteriaPositionService.prototype.search = function (positionId, isActive) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .get(this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('positionId', positionId ? positionId : '')
                .set('isActive', isActive != null && isActive !== undefined ? isActive.toString() : '')
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    CriteriaPositionService.prototype.insert = function (positionId, criteriaId) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .post(this.url, {
            positionId: positionId,
            criteriaId: criteriaId
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    CriteriaPositionService.prototype.updateActiveStatus = function (positionId, criteriaId, isActive) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .post(this.url + "/" + positionId + "/" + criteriaId, isActive)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    CriteriaPositionService.prototype.delete = function (positionId, criteriaId) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .delete(this.url + "/" + positionId + "/" + criteriaId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    CriteriaPositionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_3__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__["SpinnerService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], CriteriaPositionService);
    return CriteriaPositionService;
}());



/***/ }),

/***/ "./src/app/modules/hr/assessment/criteria/servies/criteria.service.ts":
/*!****************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/criteria/servies/criteria.service.ts ***!
  \****************************************************************************/
/*! exports provided: CriteriaService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CriteriaService", function() { return CriteriaService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../../environments/environment */ "./src/environments/environment.ts");







var CriteriaService = /** @class */ (function () {
    function CriteriaService(appConfig, spinceService, http) {
        this.appConfig = appConfig;
        this.spinceService = spinceService;
        this.http = http;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].apiGatewayUrl + "api/v1/hr/criterias";
    }
    CriteriaService.prototype.resolve = function (route, state) {
        var queryParams = route.queryParams;
        return this.search(queryParams.keyword, queryParams.groupId, queryParams.isActive, queryParams.page, queryParams.pageSize);
    };
    CriteriaService.prototype.search = function (keyword, groupId, isActive, page, pageSize) {
        var _this = this;
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 20; }
        this.spinceService.show();
        return this.http
            .get(this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpParams"]()
                .set('keyword', keyword ? keyword.toString() : '')
                .set('groupId', groupId ? groupId.toString() : '')
                .set('isActive', isActive !== undefined && isActive != null ? isActive.toString() : '')
                .set('page', page ? page.toString() : '')
                .set('pageSize', pageSize ? pageSize.toString() : '20')
        })
            .pipe((Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinceService.hide(); })));
    };
    CriteriaService.prototype.searchActivated = function (keyword, groupId, page, pageSize) {
        var _this = this;
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 20; }
        this.spinceService.show();
        return this.http
            .get(this.url + "/search", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpParams"]()
                .set('keyword', keyword ? keyword.toString() : '')
                .set('groupId', groupId ? groupId.toString() : '')
                .set('page', page ? page.toString() : '')
                .set('pageSize', pageSize ? pageSize.toString() : '20')
        })
            .pipe((Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinceService.hide(); })));
    };
    CriteriaService.prototype.insert = function (criteria) {
        var _this = this;
        this.spinceService.show();
        return this.http
            .post(this.url, criteria)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinceService.hide(); }));
    };
    CriteriaService.prototype.update = function (id, criteria) {
        var _this = this;
        this.spinceService.show();
        return this.http
            .post(this.url + "/" + id, criteria)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinceService.hide(); }));
    };
    CriteriaService.prototype.delete = function (id) {
        var _this = this;
        this.spinceService.show();
        return this.http
            .delete(this.url + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinceService.hide(); }));
    };
    CriteriaService.prototype.getDetail = function (id) {
        var _this = this;
        this.spinceService.show();
        return this.http
            .get(this.url + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinceService.hide(); }));
    };
    CriteriaService.prototype.searchGroup = function (keyword, isActive, page, pageSize) {
        var _this = this;
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 20; }
        this.spinceService.show();
        return this.http
            .get(this.url + "/groups", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpParams"]()
                .set('keyword', keyword ? keyword.toString() : '')
                .set('isActive', isActive !== undefined && isActive != null ? isActive.toString() : '')
                .set('page', page ? page.toString() : '')
                .set('pageSize', pageSize ? pageSize.toString() : '20')
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinceService.hide(); }));
    };
    CriteriaService.prototype.insertGroup = function (criteriaGroup) {
        var _this = this;
        this.spinceService.show();
        return this.http
            .post(this.url, criteriaGroup)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinceService.hide(); }));
    };
    CriteriaService.prototype.updateGroup = function (id, criteriaGroup) {
        var _this = this;
        this.spinceService.show();
        return this.http
            .post(this.url + "/groups/" + id, criteriaGroup)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinceService.hide(); }));
    };
    CriteriaService.prototype.deleteGroup = function (id) {
        var _this = this;
        this.spinceService.show();
        return this.http
            .delete(this.url + "/groups/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinceService.hide(); }));
    };
    CriteriaService.prototype.groupSuggestion = function (keyword, page, pageSize) {
        var _this = this;
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        this.spinceService.show();
        return this.http
            .get(this.url + "/groups/suggestions", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpParams"]()
                .set('keyword', keyword ? keyword.toString() : '')
                .set('page', page ? page.toString() : '')
                .set('pageSize', pageSize ? pageSize.toString() : '20')
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinceService.hide(); }));
    };
    CriteriaService.prototype.getGroupDetail = function (id) {
        var _this = this;
        this.spinceService.show();
        return this.http
            .get(this.url + "/groups/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinceService.hide(); }));
    };
    CriteriaService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_3__["SpinnerService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]])
    ], CriteriaService);
    return CriteriaService;
}());



/***/ }),

/***/ "./src/app/modules/hr/assessment/my-assessment/my-assessment.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/my-assessment/my-assessment.component.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaHIvYXNzZXNzbWVudC9teS1hc3Nlc3NtZW50L215LWFzc2Vzc21lbnQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/modules/hr/assessment/my-assessment/my-assessment.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/my-assessment/my-assessment.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@listWarehousePageTitle\">Cấu hình tiêu chí đánh giá</span>\r\n    <small i18n=\"@@productModuleTitle\">Đánh giá hiệu quả công việc</small>\r\n</h1>\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <form action=\"\" class=\"form-inline\">\r\n            <div class=\"form-group cm-mgr-5\">\r\n                <label for=\"\">Chọn năm</label>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <ghm-select [data]=\"listYears\"\r\n                           title=\"-- Chọn năm --\"\r\n                           [selectedItem]=\"{id: year, name: year}\"\r\n                           (itemSelected)=\"onYearSelected($event)\"></ghm-select>\r\n            </div>\r\n            <div class=\"form-group pull-right\">\r\n                <button class=\"btn blue\" (click)=\"register()\">Đăng ký</button>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</div>\r\n\r\n<app-assessment-table\r\n    type=\"0\"\r\n    [data]=\"listItems\"\r\n></app-assessment-table>\r\n\r\n<nh-modal #registerTimeModal>\r\n    <nh-modal-header>\r\n        Thời gian đăng ký\r\n    </nh-modal-header>\r\n    <form action=\"\" class=\"form-horizontal\" (ngSubmit)=\"confirmRegister()\">\r\n        <nh-modal-content>\r\n            <div class=\"form-group\">\r\n                <label for=\"\" class=\"col-sm-3 control-label\" ghmLabel=\"Tháng\"></label>\r\n                <div class=\"col-sm-9\">\r\n                    <nh-select\r\n                        name=\"month\"\r\n                        title=\"-- Chọn tháng --\"\r\n                        [liveSearch]=\"true\"\r\n                        [data]=\"listMonths\"\r\n                        [(ngModel)]=\"month\"></nh-select>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"\" class=\"col-sm-3 control-label\" ghmLabel=\"Năm\"></label>\r\n                <div class=\"col-sm-9\">\r\n                    <nh-select\r\n                        name=\"year\"\r\n                        title=\"-- Chọn năm --\"\r\n                        [liveSearch]=\"true\"\r\n                        [data]=\"listYears\"\r\n                        [(ngModel)]=\"year\"></nh-select>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <button class=\"btn blue cm-mgr-5\">Đăng ký</button>\r\n            <button type=\"button\" class=\"btn default\" nh-dismiss>Hủy bỏ</button>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/assessment/my-assessment/my-assessment.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/my-assessment/my-assessment.component.ts ***!
  \********************************************************************************/
/*! exports provided: MyAssessmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyAssessmentComponent", function() { return MyAssessmentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _services_user_assessment_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/user-assessment.service */ "./src/app/modules/hr/assessment/services/user-assessment.service.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");








var MyAssessmentComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](MyAssessmentComponent, _super);
    function MyAssessmentComponent(router, toastr, utilService, userAssessmentService) {
        var _this = _super.call(this) || this;
        _this.router = router;
        _this.toastr = toastr;
        _this.utilService = utilService;
        _this.userAssessmentService = userAssessmentService;
        _this.listMonths = [];
        _this.listYears = [];
        _this.month = new Date().getMonth() + 1;
        _this.year = new Date().getFullYear();
        _this.listMonths = _this.utilService.renderListMonth();
        _this.listYears = _this.utilService.renderListYear();
        return _this;
    }
    MyAssessmentComponent.prototype.ngOnInit = function () {
        this.appService.setupPage(this.pageId.ASSESSMENT, this.pageId.MY_ASSESSMENT, 'Kỳ đánh giá của tôi', 'Đánh giá hiệu quả công việc');
        this.search();
    };
    MyAssessmentComponent.prototype.onYearSelected = function (year) {
        this.year = year.id;
        this.search();
    };
    MyAssessmentComponent.prototype.register = function () {
        this.registerTimeModal.open();
    };
    MyAssessmentComponent.prototype.confirmRegister = function () {
        var _this = this;
        if (!this.month || !this.year) {
            this.toastr.warning('Vui lòng chọn thời gian đánh giá.');
            return;
        }
        this.subscribers.register = this.userAssessmentService.register(this.month, this.year)
            .subscribe(function (id) {
            console.log(id);
            _this.registerTimeModal.dismiss();
            setTimeout(function () {
                _this.router.navigateByUrl("/assessment/register?id=" + id);
            });
            // this.userAssessment = result;
            // const groups = _.groupBy(result.userAssessmentCriterias, 'criteriaGroupId');
            // for (const key in groups) {
            //     if (groups.hasOwnProperty(key)) {
            //         this.criteriaGroups = [...this.criteriaGroups, {
            //             groupId: key,
            //             groupName: groups[key][0].criteriaGroupName,
            //             totalPoint: _.sumBy(groups[key], 'point'),
            //             totalUserPoint: _.sumBy(groups[key], 'userPoint'),
            //             totalManagerPoint: _.sumBy(groups[key], 'managerPoint'),
            //             totalApproverPoint: _.sumBy(groups[key], 'approverPoint'),
            //             criterias: groups[key].map((criteria: UserAssessmentCriteriaViewModel) => {
            //                 criteria.userShow = this.type === this.assessmentType.userRegister
            //                     && (this.userAssessment.status === this.assessmentStatus.new
            //                         || this.userAssessment.status === this.assessmentStatus.managerDecline)
            //                     && this.userAssessment.userId === this.currentUser.id;
            //                 criteria.managerShow = this.type === this.assessmentType.managerApprove
            //                     && (this.userAssessment.status === this.assessmentStatus.waitingManagerApprove
            //                         || this.userAssessment.status === this.assessmentStatus.approverDecline)
            //                     && this.userAssessment.managerUserId === this.currentUser.id;
            //                 criteria.approverShow = this.type === this.assessmentType.approverApprove
            //                     && this.userAssessment.status === this.assessmentStatus.managerApproveWaitingApproverApprove
            //                     && this.userAssessment.approverUserId === this.currentUser.id;
            //                 return criteria;
            //             })
            //         }];
            //     }
            // }
        }, function (error) {
        });
    };
    MyAssessmentComponent.prototype.search = function () {
        var _this = this;
        this.subscribers.searchMyAssessment = this.userAssessmentService.search(this.year)
            .subscribe(function (result) {
            _this.totalRows = result.totalRows;
            _this.listItems = result.items;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('registerTimeModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_4__["NhModalComponent"])
    ], MyAssessmentComponent.prototype, "registerTimeModal", void 0);
    MyAssessmentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-my-assessment',
            template: __webpack_require__(/*! ./my-assessment.component.html */ "./src/app/modules/hr/assessment/my-assessment/my-assessment.component.html"),
            providers: [_services_user_assessment_service__WEBPACK_IMPORTED_MODULE_3__["UserAssessmentService"]],
            styles: [__webpack_require__(/*! ./my-assessment.component.css */ "./src/app/modules/hr/assessment/my-assessment/my-assessment.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_5__["UtilService"],
            _services_user_assessment_service__WEBPACK_IMPORTED_MODULE_3__["UserAssessmentService"]])
    ], MyAssessmentComponent);
    return MyAssessmentComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/assessment/my-assessment/user-assessment.viewmodel.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/my-assessment/user-assessment.viewmodel.ts ***!
  \**********************************************************************************/
/*! exports provided: AssessmentStatus */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssessmentStatus", function() { return AssessmentStatus; });
var AssessmentStatus;
(function (AssessmentStatus) {
    /// <summary>
    /// Mới.
    /// </summary>
    AssessmentStatus[AssessmentStatus["new"] = 0] = "new";
    /// <summary>
    /// Chờ QLTT duyệt.
    /// </summary>
    AssessmentStatus[AssessmentStatus["waitingManagerApprove"] = 1] = "waitingManagerApprove";
    /// <summary>
    /// QLTT không duyệt.
    /// </summary>
    AssessmentStatus[AssessmentStatus["managerDecline"] = 2] = "managerDecline";
    /// <summary>
    /// QLTT duyệt chờ QLPD duyệt.
    /// </summary>
    AssessmentStatus[AssessmentStatus["managerApproveWaitingApproverApprove"] = 3] = "managerApproveWaitingApproverApprove";
    /// <summary>
    /// QLPD duyệt.
    /// </summary>
    AssessmentStatus[AssessmentStatus["approverApprove"] = 4] = "approverApprove";
    /// <summary>
    /// QLPD không duyệt.
    /// </summary>
    AssessmentStatus[AssessmentStatus["approverDecline"] = 5] = "approverDecline";
    /// <summary>
    /// Hủy kết quả. Sử dụng trong trường hợp đánh giá lại (Bắt buộc phải nhập lý do hủy).
    /// </summary>
    AssessmentStatus[AssessmentStatus["cancel"] = 6] = "cancel";
    /// <summary>
    /// QLTT đã duyệt. Sử dụng trong trường hợp không có QLPD
    /// </summary>
    AssessmentStatus[AssessmentStatus["managerApproved"] = 7] = "managerApproved";
})(AssessmentStatus || (AssessmentStatus = {}));


/***/ }),

/***/ "./src/app/modules/hr/assessment/register-form/comment/comment.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/register-form/comment/comment.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"portlet light bordered cm-mgb-0\" style=\"min-width: 450px\">\r\n    <div class=\"portlet-title\">\r\n        <div class=\"caption\">\r\n            <i class=\"icon-bubble font-hide\"></i>\r\n            <span class=\"caption-subject font-hide bold uppercase\">Bình luận</span>\r\n        </div>\r\n    </div>\r\n    <div class=\"portlet-body\" id=\"chats\">\r\n        <ul class=\"chats\" style=\"min-height: 450px;\">\r\n            <li\r\n                [class.out]=\"currentUser?.id !== comment.userId\"\r\n                [class.in]=\"currentUser?.id === comment.userId\"\r\n                *ngFor=\"let comment of listItems\">\r\n                <img class=\"avatar\" alt=\"\" src=\"{{ comment.avatar }}\" ghmImage>\r\n                <div class=\"message\">\r\n                    <span class=\"arrow\"> </span>\r\n                    <a href=\"javascript:;\" class=\"name\"> {{ comment.fullName }} </a>\r\n                    <span class=\"datetime\"> {{ comment.createTime }} </span>\r\n                    <span class=\"body\">\r\n                        {{ comment.content }}\r\n                    </span>\r\n                    <ul class=\"actions\" *ngIf=\"comment.userId === currentUser.id\">\r\n                        <li>\r\n                            <a href=\"javascript://\" (click)=\"edit(comment)\">\r\n                                <i class=\"fa fa-pencil\"></i>\r\n                            </a>\r\n                        </li>\r\n                        <li>\r\n                            <a href=\"javascript://\" (click)=\"delete(comment.id)\">\r\n                                <i class=\"fa fa-trash-o\"></i>\r\n                            </a>\r\n                        </li>\r\n                    </ul>\r\n                </div>\r\n            </li>\r\n        </ul>\r\n        <div class=\"cm-mgt-15\">\r\n            <button class=\"btn btn-primary btn-block\"\r\n                    *ngIf=\"totalPages > 1 && currentPage < totalPages\"\r\n                    (click)=\"loadMore()\">Tải thêm\r\n            </button>\r\n        </div>\r\n        <div class=\"chat-form cm-mgt-0\">\r\n            <div class=\"input-cont\">\r\n                <input class=\"form-control\" type=\"text\" placeholder=\"Nội dung bình luận...\"\r\n                       (keyup.enter)=\"save()\"\r\n                       [(ngModel)]=\"content\" name=\"commentContent\">\r\n            </div>\r\n            <div class=\"btn-cont\" (click)=\"save()\">\r\n                <span class=\"arrow\"> </span>\r\n                <a href=\"javascript://\" class=\"btn blue icn-only\">\r\n                    <i class=\"fa fa-check icon-white\"></i>\r\n                </a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/assessment/register-form/comment/comment.component.scss":
/*!************************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/register-form/comment/comment.component.scss ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#chats ul.chats {\n  max-height: 450px;\n  overflow-y: auto; }\n  #chats ul.chats .message {\n    position: relative; }\n  #chats ul.chats .message:hover ul.actions {\n      display: block; }\n  #chats ul.chats .message ul.actions {\n      position: absolute;\n      right: 10px;\n      top: 0;\n      display: none; }\n  #chats ul.chats .message ul.actions li {\n        display: inline-block; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy9oci9hc3Nlc3NtZW50L3JlZ2lzdGVyLWZvcm0vY29tbWVudC9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXBwXFxtb2R1bGVzXFxoclxcYXNzZXNzbWVudFxccmVnaXN0ZXItZm9ybVxcY29tbWVudFxcY29tbWVudC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRLGlCQUFpQjtFQUNqQixnQkFBZ0IsRUFBQTtFQUh4QjtJQU1ZLGtCQUFrQixFQUFBO0VBTjlCO01BVW9CLGNBQWMsRUFBQTtFQVZsQztNQWVnQixrQkFBa0I7TUFDbEIsV0FBVztNQUNYLE1BQU07TUFDTixhQUFhLEVBQUE7RUFsQjdCO1FBcUJvQixxQkFBcUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaHIvYXNzZXNzbWVudC9yZWdpc3Rlci1mb3JtL2NvbW1lbnQvY29tbWVudC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNjaGF0cyB7XHJcbiAgICB1bC5jaGF0cyB7XHJcbiAgICAgICAgbWF4LWhlaWdodDogNDUwcHg7XHJcbiAgICAgICAgb3ZlcmZsb3cteTogYXV0bztcclxuXHJcbiAgICAgICAgLm1lc3NhZ2Uge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcblxyXG4gICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgIHVsLmFjdGlvbnMge1xyXG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB1bC5hY3Rpb25zIHtcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgICAgIHJpZ2h0OiAxMHB4O1xyXG4gICAgICAgICAgICAgICAgdG9wOiAwO1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogbm9uZTtcclxuXHJcbiAgICAgICAgICAgICAgICBsaSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/modules/hr/assessment/register-form/comment/comment.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/register-form/comment/comment.component.ts ***!
  \**********************************************************************************/
/*! exports provided: CommentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommentComponent", function() { return CommentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _comment_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./comment.service */ "./src/app/modules/hr/assessment/register-form/comment/comment.service.ts");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _comment_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./comment.model */ "./src/app/modules/hr/assessment/register-form/comment/comment.model.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_8__);









var CommentComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](CommentComponent, _super);
    function CommentComponent(data, toastr, commentService) {
        var _this = _super.call(this) || this;
        _this.data = data;
        _this.toastr = toastr;
        _this.commentService = commentService;
        _this.userAssessmentCriteriaId = _this.data.userAssessmentCriteriaId;
        _this.currentUser = _this.appService.currentUser;
        return _this;
    }
    CommentComponent.prototype.ngOnInit = function () {
        if (this.userAssessmentCriteriaId) {
            this.search();
        }
    };
    CommentComponent.prototype.search = function (isAppend) {
        var _this = this;
        if (isAppend === void 0) { isAppend = false; }
        this.subscribers.searchCriteriaComment = this.commentService.search(this.userAssessmentCriteriaId, this.currentPage, this.pageSize)
            .subscribe(function (result) {
            _this.totalRows = result.totalRows;
            _this.calculateTotalPages();
            if (isAppend) {
                result.items.forEach(function (comment) {
                    _this.listItems = _this.listItems.concat([comment]);
                });
            }
            else {
                _this.listItems = [];
                _this.listItems = result.items;
            }
        });
    };
    CommentComponent.prototype.edit = function (comment) {
        this.comment = comment;
        this.content = this.comment.content;
    };
    CommentComponent.prototype.save = function () {
        var _this = this;
        if (!this.content) {
            this.toastr.warning('Vui lòng nhập nội dung bình luận');
            return;
        }
        if (this.comment) {
            this.comment.content = this.content;
            this.subscribers.update = this.commentService.update(this.comment)
                .subscribe(function (result) {
                _this.toastr.success(result.message);
                _this.comment.concurrencyStamp = result.data;
                var comment = lodash__WEBPACK_IMPORTED_MODULE_8__["find"](_this.listItems, function (item) {
                    return item.id === _this.comment.id;
                });
                if (comment) {
                    comment.content = _this.comment.content;
                    comment.lastUpdate = moment__WEBPACK_IMPORTED_MODULE_7__();
                }
                _this.resetModel();
            });
        }
        else {
            this.comment = new _comment_model__WEBPACK_IMPORTED_MODULE_5__["Comment"]();
            this.comment.content = this.content;
            this.comment.userId = this.currentUser.id;
            this.comment.fullName = this.currentUser.fullName;
            this.comment.avatar = this.currentUser.avatar;
            this.comment.createTime = moment__WEBPACK_IMPORTED_MODULE_7__().fromNow();
            this.comment.userAssessmentCriteriaId = this.userAssessmentCriteriaId;
            this.listItems = this.listItems.concat([this.comment]);
            this.subscribers.update = this.commentService.insert(this.comment)
                .subscribe(function (result) {
                _this.toastr.success(result.message);
                _this.comment.concurrencyStamp = result.data;
                _this.comment.id = result.data;
                _this.resetModel();
            });
        }
    };
    CommentComponent.prototype.delete = function (id) {
        var _this = this;
        this.commentService.delete(id)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
            lodash__WEBPACK_IMPORTED_MODULE_8__["remove"](_this.listItems, function (comment) {
                return comment.id === id;
            });
        });
    };
    CommentComponent.prototype.loadMore = function () {
        this.currentPage = this.currentPage + 1;
        this.search(true);
    };
    CommentComponent.prototype.resetModel = function () {
        this.comment = null;
        this.content = null;
    };
    CommentComponent.prototype.calculateTotalPages = function () {
        this.totalPages = Math.ceil(this.totalRows / this.pageSize);
    };
    CommentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-comment',
            template: __webpack_require__(/*! ./comment.component.html */ "./src/app/modules/hr/assessment/register-form/comment/comment.component.html"),
            providers: [_comment_service__WEBPACK_IMPORTED_MODULE_3__["CommentService"]],
            styles: [__webpack_require__(/*! ./comment.component.scss */ "./src/app/modules/hr/assessment/register-form/comment/comment.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"],
            _comment_service__WEBPACK_IMPORTED_MODULE_3__["CommentService"]])
    ], CommentComponent);
    return CommentComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_4__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/assessment/register-form/comment/comment.model.ts":
/*!******************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/register-form/comment/comment.model.ts ***!
  \******************************************************************************/
/*! exports provided: Comment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Comment", function() { return Comment; });
var Comment = /** @class */ (function () {
    function Comment() {
    }
    return Comment;
}());



/***/ }),

/***/ "./src/app/modules/hr/assessment/register-form/comment/comment.service.ts":
/*!********************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/register-form/comment/comment.service.ts ***!
  \********************************************************************************/
/*! exports provided: CommentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommentService", function() { return CommentService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);








var CommentService = /** @class */ (function () {
    function CommentService(appConfig, spinceService, http) {
        this.appConfig = appConfig;
        this.spinceService = spinceService;
        this.http = http;
        this.url = 'api/v1/hr/assessments/criterias';
        this.url = "" + _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].apiGatewayUrl + this.url;
    }
    CommentService.prototype.insert = function (comment) {
        var _this = this;
        this.spinceService.show();
        return this.http.post(this.url + "/comments", comment)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinceService.hide(); }));
    };
    CommentService.prototype.update = function (comment) {
        var _this = this;
        this.spinceService.show();
        return this.http.post(this.url + "/comments/" + comment.id, comment)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinceService.hide(); }));
    };
    CommentService.prototype.delete = function (id) {
        var _this = this;
        this.spinceService.show();
        return this.http.delete(this.url + "/comments/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinceService.hide(); }));
    };
    CommentService.prototype.search = function (userAssessmentCriteriaId, page, pageSize) {
        var _this = this;
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        this.spinceService.show();
        return this.http
            .get(this.url + "/" + userAssessmentCriteriaId + "/comments/", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpParams"]()
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : '10')
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinceService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            result.items.forEach(function (comment) {
                comment.createTime = moment__WEBPACK_IMPORTED_MODULE_7__(moment__WEBPACK_IMPORTED_MODULE_7__(comment.createTime, 'DD/MM/YYYY hh:mm:ss')).fromNow();
            });
            return result;
        }));
    };
    CommentService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_3__["SpinnerService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]])
    ], CommentService);
    return CommentService;
}());



/***/ }),

/***/ "./src/app/modules/hr/assessment/register-form/register-form.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/register-form/register-form.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row cm-mgb-20\">\r\n    <div class=\"col-sm-12\">\r\n        <h4 class=\"star-title\">\r\n            Đăng ký đánh giá hiệu quả công việc tháng {{ userAssessment?.month }} năm {{ userAssessment?.year }}\r\n        </h4>\r\n        <div class=\"star-title-decoration\">\r\n            <i class=\"fa fa-star-o\"></i>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<form action=\"\" class=\"form-horizontal cm-mgb-10\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"control-label col-md-3\" ghmLabel=\"Họ và tên\"></label>\r\n                        <div class=\"col-md-9\">\r\n                            <div class=\"form-control\" readonly>{{ userAssessment?.fullName }}</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <!--/span-->\r\n                <div class=\"col-md-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"control-label col-md-3\" ghmLabel=\"Chức vụ\"></label>\r\n                        <div class=\"col-md-9\">\r\n                            <div class=\"form-control\" readonly>{{ userAssessment?.positionName }}</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <!--/span-->\r\n                <div class=\"col-md-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"control-label col-md-3\" ghmLabel=\"Bộ phần\"></label>\r\n                        <div class=\"col-md-9\">\r\n                            <div class=\"form-control\" readonly>{{ userAssessment?.officeName }}</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <!--/span-->\r\n                <div class=\"col-md-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"control-label col-md-3\" ghmLabel=\"Trạng thái\"></label>\r\n                        <div class=\"col-md-9\">\r\n                            <div class=\"form-control\" readonly>\r\n                                {{ userAssessment?.statusName }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <!--/span-->\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <div class=\"alert alert-warning\" *ngIf=\"!userAssessment?.allowAssessment\">\r\n            <b>Thông báo: </b>Kỳ đánh giá chưa được mở hoặc đã quá hạn đánh giá. Bạn sẽ không thể thay đổi điểm của kỳ đánh giá này.\r\n        </div>\r\n        <div class=\"table-responsive\">\r\n            <table class=\"table table-bordered table-stripped table-hover\">\r\n                <thead class=\"bg-warning\">\r\n                <tr>\r\n                    <th rowspan=\"2\" class=\"center middle w50\">STT</th>\r\n                    <th rowspan=\"2\" class=\"center middle w250\">Nội dung đánh giá</th>\r\n                    <th colspan=\"4\" class=\"center middle\">Điểm</th>\r\n                    <th rowspan=\"2\" class=\"center middle w250\">Chỉ tiêu đánh giá tháng/tiêu chuẩn</th>\r\n                    <th rowspan=\"2\" class=\"center middle w250\">Quy định chế tài</th>\r\n                    <th rowspan=\"2\" class=\"center middle w250\">Ghi chú</th>\r\n                    <th rowspan=\"2\" class=\"w50 center\"></th>\r\n                </tr>\r\n                <tr>\r\n                    <th class=\"w100 center\">Chuẩn</th>\r\n                    <th class=\"w100 center\">Tự đánh giá</th>\r\n                    <th class=\"w100 center\">QLTT</th>\r\n                    <th class=\"w100 center\">QLPD</th>\r\n                </tr>\r\n                </thead>\r\n                <tbody>\r\n                <ng-container *ngFor=\"let group of criteriaGroups; let i = index\">\r\n                    <tr class=\"bg-success\">\r\n                        <td class=\"center\">{{ (i + 1) }}</td>\r\n                        <td>{{ group.groupName }}</td>\r\n                        <td class=\"text-right middle\">{{ group.totalPoint }}</td>\r\n                        <td class=\"text-right middle\">{{ group.totalUserPoint }}</td>\r\n                        <td class=\"text-right middle\">{{ group.totalManagerPoint }}</td>\r\n                        <td class=\"text-right middle\">{{ group.totalApproverPoint }}</td>\r\n                        <td colspan=\"4\"></td>\r\n                    </tr>\r\n                    <tr *ngFor=\"let criteria of group.criterias; let j = index\">\r\n                        <td class=\"center middle\">{{ (j + 1) }}</td>\r\n                        <td>{{ criteria.criteriaName }}</td>\r\n                        <td class=\"text-right middle\">\r\n                            {{ criteria.point }}\r\n                        </td>\r\n                        <td class=\"text-right middle\">\r\n                            <ng-container *ngIf=\"criteria.userShow && userAssessment?.allowAssessment; else userPointReadOnlyTemplate\">\r\n                                <input type=\"text\" class=\"form-control text-right\" [(ngModel)]=\"criteria.userPoint\"\r\n                                       ghm-text-selection\r\n                                       (focus)=\"point = criteria.userPoint\"\r\n                                       (blur)=\"update(criteria)\">\r\n                            </ng-container>\r\n                            <ng-template #userPointReadOnlyTemplate>\r\n                                {{ criteria.userPoint }}\r\n                            </ng-template>\r\n                        </td>\r\n                        <td class=\"text-right middle\">\r\n                            <ng-container *ngIf=\"criteria.managerShow && userAssessment?.allowAssessment; else managerPointReadOnlyTemplate\">\r\n                                <input type=\"text\" class=\"form-control text-right\" [(ngModel)]=\"criteria.managerPoint\"\r\n                                       ghm-text-selection\r\n                                       (focus)=\"point = criteria.managerPoint\"\r\n                                       (blur)=\"update(criteria)\">\r\n                            </ng-container>\r\n                            <ng-template #managerPointReadOnlyTemplate>\r\n                                {{ criteria.managerPoint }}\r\n                            </ng-template>\r\n                        </td>\r\n                        <td class=\"text-right middle\">\r\n                            <ng-container *ngIf=\"criteria.approverShow && userAssessment?.allowAssessment; else approverPointReadOnlyTemplate\">\r\n                                <input type=\"text\" class=\"form-control text-right\" [(ngModel)]=\"criteria.approverPoint\"\r\n                                       ghm-text-selection\r\n                                       (focus)=\"point = criteria.approverPoint\"\r\n                                       (blur)=\"update(criteria)\">\r\n                            </ng-container>\r\n                            <ng-template #approverPointReadOnlyTemplate>\r\n                                {{ criteria.approverPoint }}\r\n                            </ng-template>\r\n                        </td>\r\n                        <td>{{ criteria.description }}</td>\r\n                        <td>{{ criteria.sanction }}</td>\r\n                        <td>\r\n                            <textarea [(ngModel)]=\"criteria.note\" class=\"form-control\"\r\n                                      *ngIf=\"criteria.userShow || criteria.managerShow || criteria.approverShow; else noteReadOnlyTemplate\"\r\n                                      (blur)=\"update(criteria)\"></textarea>\r\n                            <ng-template #noteReadOnlyTemplate>\r\n                                {{ criteria.note }}\r\n                            </ng-template>\r\n                        </td>\r\n                        <td class=\"center\">\r\n                            <button type=\"button\" class=\"btn blue btn-sm\" (click)=\"showComment(criteria.id)\">\r\n                                <i class=\"fa fa-commenting-o\"></i>\r\n                            </button>\r\n                        </td>\r\n                    </tr>\r\n                </ng-container>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class=\"row cm-mgt-10\">\r\n    <div class=\"col-sm-12 center\">\r\n        <button type=\"button\" class=\"btn btn-primary cm-mgr-5\"\r\n                *ngIf=\"type === assessmentType.userRegister  && (userAssessment?.status === assessmentStatus.new\r\n                || userAssessment?.status === assessmentStatus.managerDecline)\"\r\n                [swal]=\"confirmSendAssessment\"\r\n                (confirm)=\"approve()\">\r\n            Gửi\r\n        </button>\r\n        <ng-container\r\n            *ngIf=\"(type === assessmentType.managerApprove && (userAssessment?.status === assessmentStatus.waitingManagerApprove\r\n            || userAssessment?.status === assessmentStatus.approverDecline)) || (type === assessmentType.approverApprove\r\n            && (userAssessment?.status === assessmentStatus.approverDecline || userAssessment?.status === assessmentStatus.managerApproveWaitingApproverApprove))\">\r\n            <button type=\"button\" class=\"btn btn-success cm-mgr-5\"\r\n                    [swal]=\"confirmApprove\"\r\n                    (confirm)=\"approve()\">\r\n                Duyệt\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-danger cm-mgr-5\"\r\n                    [swal]=\"confirmDelince\" (confirm)=\"decline($event)\">\r\n                Không duyệt\r\n            </button>\r\n        </ng-container>\r\n        <button type=\"button\" class=\"btn btn-default\" (click)=\"back()\">\r\n            Trở về trang danh sách\r\n        </button>\r\n    </div>\r\n</div>\r\n\r\n\r\n<swal\r\n    #confirmSendAssessment\r\n    i18n=\"@@confirmSendAssessment\"\r\n    title=\"Bạn có chắc chắn muốn gửi đánh giá hiệu quả công việc?\"\r\n    text=\"Lưu ý: Sau khi gửi bạn không thể thay đổi thông tin đánh giá.\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"false\">\r\n</swal>\r\n\r\n<swal\r\n    #confirmApprove\r\n    i18n=\"@@confirmApproveAssessment\"\r\n    title=\"Bạn có chắc chắn muốn duyệt bản đánh giá hiệu quả công việc này?\"\r\n    text=\"Lưu ý: Sau khi duyệt bạn không thể thay đổi thông tin đánh giá.\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"false\">\r\n</swal>\r\n\r\n<swal\r\n    #confirmDelince\r\n    i18n=\"@@confirmDeclineAssessment\"\r\n    title=\"Bạn có chắc chắn không duyệt bản đánh giá hiệu quả công việc này?\"\r\n    text=\"Lưu ý: Sau khi không duyệt bản đánh giá sẽ phải thực hiện đánh giá lại.\"\r\n    type=\"question\"\r\n    input=\"textarea\"\r\n    [showCancelButton]=\"true\">\r\n</swal>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/modules/hr/assessment/register-form/register-form.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/register-form/register-form.component.ts ***!
  \********************************************************************************/
/*! exports provided: AssessmentType, RegisterFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssessmentType", function() { return AssessmentType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterFormComponent", function() { return RegisterFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _services_user_assessment_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/user-assessment.service */ "./src/app/modules/hr/assessment/services/user-assessment.service.ts");
/* harmony import */ var _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../my-assessment/user-assessment.viewmodel */ "./src/app/modules/hr/assessment/my-assessment/user-assessment.viewmodel.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _comment_comment_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./comment/comment.component */ "./src/app/modules/hr/assessment/register-form/comment/comment.component.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");











var AssessmentType;
(function (AssessmentType) {
    AssessmentType[AssessmentType["userRegister"] = 0] = "userRegister";
    AssessmentType[AssessmentType["managerApprove"] = 1] = "managerApprove";
    AssessmentType[AssessmentType["approverApprove"] = 2] = "approverApprove";
})(AssessmentType || (AssessmentType = {}));
var RegisterFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](RegisterFormComponent, _super);
    function RegisterFormComponent(dialog, route, router, toastr, utilService, userAssessmentService) {
        var _this = _super.call(this) || this;
        _this.dialog = dialog;
        _this.route = route;
        _this.router = router;
        _this.toastr = toastr;
        _this.utilService = utilService;
        _this.userAssessmentService = userAssessmentService;
        _this.criteriaGroups = [];
        _this.assessmentType = AssessmentType;
        _this.assessmentStatus = _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_4__["AssessmentStatus"];
        // month: number;
        // year: number;
        _this.listMonths = [];
        _this.listYears = [];
        return _this;
    }
    RegisterFormComponent.prototype.ngOnInit = function () {
        this.appService.setupPage(this.pageId.ASSESSMENT, this.pageId.MY_ASSESSMENT, 'Đánh giá hiệu quả công việc', 'Đánh giá hiệu quả công việc');
        this.listMonths = this.utilService.renderListMonth();
        this.listYears = this.utilService.renderListYear();
    };
    RegisterFormComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.subscribers.queryParams = this.route.queryParams.subscribe(function (params) {
            if (params.id) {
                _this.getDetail(params.id);
            }
            if (params.showComment && params.userAssessmentCriteriaId) {
                _this.showComment(params.userAssessmentCriteriaId);
            }
        });
    };
    RegisterFormComponent.prototype.onMonthSelected = function (month) {
        if (this.userAssessment) {
            this.userAssessment.month = month.id;
        }
    };
    RegisterFormComponent.prototype.onYearSelected = function (year) {
        if (this.userAssessment) {
            this.userAssessment.year = year.id;
        }
    };
    RegisterFormComponent.prototype.send = function () {
    };
    RegisterFormComponent.prototype.showAddGroupForm = function () {
    };
    RegisterFormComponent.prototype.approve = function () {
        var _this = this;
        var oldStatus = this.userAssessment.status;
        this.userAssessment.status = this.type === this.assessmentType.userRegister
            ? this.assessmentStatus.waitingManagerApprove
            : this.type === this.assessmentType.managerApprove ? this.assessmentStatus.managerApproveWaitingApproverApprove
                : this.type === this.assessmentType.approverApprove ? this.assessmentStatus.approverApprove
                    : null;
        this.userAssessmentService.updateStatus(this.userAssessment.id, this.userAssessment)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
            if (_this.type === _this.assessmentType.userRegister) {
                _this.router.navigateByUrl('/assessment/my-assessment');
            }
            else {
                _this.router.navigateByUrl('/assessment/approve');
            }
        }, function (error) {
            _this.userAssessment.status = oldStatus;
            _this.userAssessment.statusName = _this.userAssessmentService.getAssessmentStatusName(_this.userAssessment.status);
        });
    };
    RegisterFormComponent.prototype.decline = function (reason) {
        var _this = this;
        if (!reason) {
            this.toastr.warning('Vui lòng nhập lý do không duyệt.');
            return;
        }
        var oldStatus = this.userAssessment.status;
        this.userAssessment.status = this.type === this.assessmentType.managerApprove ? this.assessmentStatus.managerDecline
            : this.type === this.assessmentType.approverApprove ? this.assessmentStatus.approverDecline
                : null;
        this.userAssessment.reason = reason;
        this.userAssessmentService.updateStatus(this.userAssessment.id, this.userAssessment)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
            if (_this.type === _this.assessmentType.userRegister) {
                _this.router.navigateByUrl('/assessment/my-assessment');
            }
            else {
                _this.router.navigateByUrl('/assessment/approve');
            }
        }, function (error) {
            _this.userAssessment.status = oldStatus;
            _this.userAssessment.statusName = _this.userAssessmentService.getAssessmentStatusName(_this.userAssessment.status);
        });
    };
    RegisterFormComponent.prototype.back = function () {
        this.router.navigateByUrl(this.type === this.assessmentType.userRegister
            ? '/assessment/my-assessment'
            : '/assessment/approve');
    };
    RegisterFormComponent.prototype.update = function (criteria) {
        if ((this.type === this.assessmentType.userRegister && criteria.userPoint < 0)
            || (this.type === this.assessmentType.managerApprove && criteria.managerPoint < 0)
            || (this.type === this.assessmentType.approverApprove && criteria.approverPoint < 0)) {
            this.toastr.warning('Điểm số phải lớn hơn hoặc bằng 0');
            return;
        }
        if ((this.type === this.assessmentType.userRegister && this.point === criteria.userPoint)
            || (this.type === this.assessmentType.managerApprove && this.point === criteria.managerPoint)
            || (this.type === this.assessmentType.approverApprove && this.point === criteria.approverPoint)) {
            return;
        }
        if ((this.type === this.assessmentType.userRegister && criteria.point < criteria.userPoint)
            || (this.type === this.assessmentType.managerApprove && criteria.point < criteria.managerPoint)
            || (this.type === this.assessmentType.approverApprove && criteria.point < criteria.approverPoint)) {
            this.toastr.warning('Điểm đánh giá không được phép lớn hơn điểm chuẩn.');
            return;
        }
        this.userAssessmentService.update(this.userAssessment.id, this.type, criteria)
            .subscribe(function (result) {
        });
    };
    RegisterFormComponent.prototype.showComment = function (userAssessemntCriteriaId) {
        this.dialog.open(_comment_comment_component__WEBPACK_IMPORTED_MODULE_9__["CommentComponent"], { data: { userAssessmentCriteriaId: userAssessemntCriteriaId } });
    };
    RegisterFormComponent.prototype.getDetail = function (id) {
        var _this = this;
        this.criteriaGroups = [];
        this.subscribers.getDetail = this.userAssessmentService.getDetail(id)
            .subscribe(function (data) {
            if (data) {
                _this.userAssessment = data;
                _this.type = _this.userAssessment.userId === _this.currentUser.id ? _this.assessmentType.userRegister
                    : _this.userAssessment.managerUserId === _this.currentUser.id ? _this.assessmentType.managerApprove
                        : _this.userAssessment.approverUserId === _this.currentUser.id ? _this.assessmentType.approverApprove : null;
                var groups = lodash__WEBPACK_IMPORTED_MODULE_5__["groupBy"](data.userAssessmentCriterias, 'criteriaGroupId');
                for (var key in groups) {
                    if (groups.hasOwnProperty(key)) {
                        _this.criteriaGroups = _this.criteriaGroups.concat([{
                                groupId: key,
                                groupName: groups[key][0].criteriaGroupName,
                                totalPoint: lodash__WEBPACK_IMPORTED_MODULE_5__["sumBy"](groups[key], 'point'),
                                totalUserPoint: lodash__WEBPACK_IMPORTED_MODULE_5__["sumBy"](groups[key], 'userPoint'),
                                totalManagerPoint: lodash__WEBPACK_IMPORTED_MODULE_5__["sumBy"](groups[key], 'managerPoint'),
                                totalApproverPoint: lodash__WEBPACK_IMPORTED_MODULE_5__["sumBy"](groups[key], 'approverPoint'),
                                criterias: groups[key].map(function (criteria) {
                                    criteria.userShow = _this.type === _this.assessmentType.userRegister
                                        && (_this.userAssessment.status === _this.assessmentStatus.new
                                            || _this.userAssessment.status === _this.assessmentStatus.managerDecline)
                                        && _this.userAssessment.userId === _this.currentUser.id;
                                    criteria.managerShow = _this.type === _this.assessmentType.managerApprove
                                        && (_this.userAssessment.status === _this.assessmentStatus.waitingManagerApprove
                                            || _this.userAssessment.status === _this.assessmentStatus.approverDecline)
                                        && _this.userAssessment.managerUserId === _this.currentUser.id;
                                    criteria.approverShow = _this.type === _this.assessmentType.approverApprove
                                        && _this.userAssessment.status === _this.assessmentStatus.managerApproveWaitingApproverApprove
                                        && _this.userAssessment.approverUserId === _this.currentUser.id;
                                    return criteria;
                                })
                            }]);
                    }
                }
            }
        });
    };
    RegisterFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-register-form',
            template: __webpack_require__(/*! ./register-form.component.html */ "./src/app/modules/hr/assessment/register-form/register-form.component.html"),
            providers: [_services_user_assessment_service__WEBPACK_IMPORTED_MODULE_3__["UserAssessmentService"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_8__["MatDialog"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_10__["UtilService"],
            _services_user_assessment_service__WEBPACK_IMPORTED_MODULE_3__["UserAssessmentService"]])
    ], RegisterFormComponent);
    return RegisterFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/assessment/services/user-assessment.service.ts":
/*!***************************************************************************!*\
  !*** ./src/app/modules/hr/assessment/services/user-assessment.service.ts ***!
  \***************************************************************************/
/*! exports provided: UserAssessmentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserAssessmentService", function() { return UserAssessmentService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../my-assessment/user-assessment.viewmodel */ "./src/app/modules/hr/assessment/my-assessment/user-assessment.viewmodel.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../environments/environment */ "./src/environments/environment.ts");









var UserAssessmentService = /** @class */ (function () {
    function UserAssessmentService(appConfig, spinnerService, toastr, http) {
        this.appConfig = appConfig;
        this.spinnerService = spinnerService;
        this.toastr = toastr;
        this.http = http;
        this.url = 'api/v1/hr/assessments';
        this.url = "" + _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].apiGatewayUrl + this.url;
    }
    UserAssessmentService.prototype.register = function (month, year) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .get(this.url + "/register", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('month', month.toString())
                .set('year', year.toString())
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (result) { return result.data; }));
    };
    UserAssessmentService.prototype.getDetail = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (result) {
            result.data.statusName = _this.getAssessmentStatusName(result.data.status);
            return result.data;
        }));
    };
    UserAssessmentService.prototype.search = function (year) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .get(this.url + "/my-assessments", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('year', year ? year.toString() : '')
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (result) {
            if (result.items) {
                result.items.forEach(function (userAssessment) {
                    userAssessment.statusName = _this.getAssessmentStatusName(userAssessment.status);
                });
            }
            return result;
        }));
    };
    UserAssessmentService.prototype.managerSearch = function (keyword, month, year, page, pageSize) {
        var _this = this;
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 20; }
        this.spinnerService.show();
        return this.http
            .get(this.url + "/managers", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('keyword', keyword ? keyword.toString() : '')
                .set('month', month ? month.toString() : '')
                .set('year', year ? year.toString() : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : '20')
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (result) {
            if (result.items) {
                result.items.forEach(function (userAssessment) {
                    userAssessment.statusName = _this.getAssessmentStatusName(userAssessment.status);
                });
            }
            return result;
        }));
    };
    UserAssessmentService.prototype.approverSearch = function (keyword, month, year, page, pageSize) {
        var _this = this;
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 20; }
        this.spinnerService.show();
        return this.http
            .get(this.url + "/approvers", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('keyword', keyword ? keyword.toString() : '')
                .set('month', month ? month.toString() : '')
                .set('year', year ? year.toString() : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : '20')
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (result) {
            if (result.items) {
                result.items.forEach(function (userAssessment) {
                    userAssessment.statusName = _this.getAssessmentStatusName(userAssessment.status);
                });
            }
            return result;
        }));
    };
    UserAssessmentService.prototype.update = function (id, type, criteria) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .post(this.url + "/" + id + "/" + type, criteria)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    UserAssessmentService.prototype.updateStatus = function (id, userAssessment) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .post(this.url + "/" + id, userAssessment)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    UserAssessmentService.prototype.getAssessmentStatusName = function (assessmentStatus) {
        switch (assessmentStatus) {
            case _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].new:
                return 'Mới';
            case _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].approverDecline:
                return 'QLPD không duyệt';
            case _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].approverApprove:
                return 'QLPD đã duyệt';
            case _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].waitingManagerApprove:
                return 'Chờ QLTT duyệt';
            case _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].managerApproveWaitingApproverApprove:
                return 'QLTT duyệt chờ QLPD duyệt';
            case _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].managerDecline:
                return 'QLTT không duyệt';
            case _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].cancel:
                return 'Đã hủy';
            case _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].managerApproved:
                return 'QLTT đã duyệt';
            default:
                return 'Không xác định';
        }
    };
    UserAssessmentService.prototype.searchResult = function (keyword, officeId, status, month, year, page, pageSize) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .get(this.url + "/results", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('officeId', officeId ? officeId.toString() : '')
                .set('status', status ? status.toString() : '')
                .set('month', month ? month.toString() : '')
                .set('year', year ? year.toString() : '')
                .set('page', page ? page.toString() : '')
                .set('pageSize', pageSize ? pageSize.toString() : '')
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (result) {
            if (result.items) {
                result.items.forEach(function (userAssessment) {
                    userAssessment.statusName = _this.getAssessmentStatusName(userAssessment.status);
                });
            }
            return result;
        }));
    };
    UserAssessmentService.prototype.getListStatus = function () {
        return [
            { id: _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].new, name: this.getAssessmentStatusName(_my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].new) },
            { id: _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].waitingManagerApprove, name: this.getAssessmentStatusName(_my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].waitingManagerApprove) },
            { id: _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].managerApproved, name: this.getAssessmentStatusName(_my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].managerApproved) },
            { id: _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].managerDecline, name: this.getAssessmentStatusName(_my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].managerDecline) },
            {
                id: _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].managerApproveWaitingApproverApprove,
                name: this.getAssessmentStatusName(_my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].managerApproveWaitingApproverApprove)
            },
            { id: _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].approverApprove, name: this.getAssessmentStatusName(_my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].approverApprove) },
            { id: _my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].approverDecline, name: this.getAssessmentStatusName(_my_assessment_user_assessment_viewmodel__WEBPACK_IMPORTED_MODULE_6__["AssessmentStatus"].approverDecline) },
        ];
    };
    UserAssessmentService.prototype.getOfficeTree = function () {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .get(_environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].apiGatewayUrl + "api/v1/hr/offices/trees")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (result) {
            return result;
        }));
    };
    UserAssessmentService.prototype.requestReassessment = function (positionId, month, year, applyForAll) {
        return this.http.get(this.url + "/request-reassessment/" + positionId, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('month', month.toString())
                .set('year', year.toString())
                .set('applyForAll', applyForAll.toString())
        });
    };
    UserAssessmentService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_3__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__["SpinnerService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], UserAssessmentService);
    return UserAssessmentService;
}());



/***/ }),

/***/ "./src/app/shareds/components/tinymce/tinymce.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/shareds/components/tinymce/tinymce.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "div.tinymce-editor {\n  height: auto !important; }\n  div.tinymce-editor p {\n    margin: 0 0 !important; }\n  .mce-fullscreen {\n  margin-top: 50px !important;\n  border: 0;\n  padding: 0;\n  margin: 0;\n  overflow: hidden;\n  height: 100%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL3RpbnltY2UvRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFwcFxcc2hhcmVkc1xcY29tcG9uZW50c1xcdGlueW1jZVxcdGlueW1jZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHVCQUF1QixFQUFBO0VBRDNCO0lBR1Esc0JBQXNCLEVBQUE7RUFJOUI7RUFDSSwyQkFBMkI7RUFDM0IsU0FBUztFQUNULFVBQVU7RUFDVixTQUFTO0VBQ1QsZ0JBQWdCO0VBQ2hCLFlBQVksRUFBQSIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZHMvY29tcG9uZW50cy90aW55bWNlL3RpbnltY2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJkaXYudGlueW1jZS1lZGl0b3Ige1xyXG4gICAgaGVpZ2h0OiBhdXRvICFpbXBvcnRhbnQ7XHJcbiAgICBwIHtcclxuICAgICAgICBtYXJnaW46IDAgMCAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG59XHJcblxyXG4ubWNlLWZ1bGxzY3JlZW4ge1xyXG4gICAgbWFyZ2luLXRvcDogNTBweCAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyOiAwO1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/shareds/components/tinymce/tinymce.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/shareds/components/tinymce/tinymce.component.ts ***!
  \*****************************************************************/
/*! exports provided: TinymceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TinymceComponent", function() { return TinymceComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");



var TinymceComponent = /** @class */ (function () {
    function TinymceComponent() {
        this.inline = false;
        this.menu = {
            file: { title: 'File', items: 'newdocument | print' },
            edit: { title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall' },
            // insert: {title: 'Insert', items: 'link media | template hr '},
            view: { title: 'View', items: 'visualaid | preview | fullscreen ' },
            format: {
                title: 'Format',
                items: 'bold italic underline strikethrough superscript subscript | formats | removeformat '
            },
            table: { title: 'Table', items: 'inserttable tableprops deletetable | cell row column' },
            tools: { title: 'Tools', items: 'code ' }
        };
        this.onEditorKeyup = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onBlur = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.propagateChange = function () {
        };
    }
    TinymceComponent_1 = TinymceComponent;
    Object.defineProperty(TinymceComponent.prototype, "content", {
        get: function () {
            return this._content;
        },
        set: function (val) {
            this._content = val;
        },
        enumerable: true,
        configurable: true
    });
    TinymceComponent.prototype.ngAfterViewInit = function () {
        this.initEditor();
    };
    TinymceComponent.prototype.ngOnDestroy = function () {
        tinymce.remove("" + this.elementId);
    };
    TinymceComponent.prototype.initEditor = function () {
        var _this = this;
        setTimeout(function () {
            tinymce.remove("#" + _this.elementId);
            tinymce.init({
                selector: "#" + _this.elementId,
                plugins: ['fullscreen', 'link', 'autolink', 'paste', 'image', 'table', 'textcolor', 'print', 'preview', 'spellchecker',
                    'colorpicker', 'fullscreen', 'code', 'lists', 'emoticons', 'wordcount'],
                toolbar: 'insertfile undo redo | | fontselect | fontsizeselect | bold italic ' +
                    '| alignleft aligncenter alignright alignjustify ' +
                    '| bullist numlist outdent indent | link image | fullscreen',
                fontsize_formats: '8pt 9pt 10pt 11pt 12pt 13pt 14pt 18pt 24pt 36pt',
                skin_url: '/assets/skins/lightgray',
                menu: _this.menu,
                inline: _this.inline,
                setup: function (editor) {
                    _this.editor = editor;
                    editor.on('keyup', function (event) {
                        var content = editor.getContent();
                        _this.content = content;
                        _this.propagateChange(content);
                        _this.onEditorKeyup.emit({
                            text: editor.getContent({ format: 'text' }),
                            content: _this.content
                        });
                    });
                    editor.on('change', function (event) {
                        var contentChange = editor.getContent();
                        _this.content = contentChange;
                        _this.propagateChange(_this.content);
                        _this.onChange.emit({
                            text: editor.getContent({ format: 'text' }),
                            content: _this.content
                        });
                    });
                    editor.on('blur', function (event) {
                        var contentChange = editor.getContent();
                        _this.content = contentChange;
                        _this.propagateChange(_this.content);
                        _this.onBlur.emit({
                            text: editor.getContent({ format: 'text' }),
                            content: _this.content
                        });
                    });
                }
            });
        });
    };
    TinymceComponent.prototype.setContent = function (content) {
        this.content = content;
        var editor = tinymce.get(this.elementId);
        if (editor != null) {
            editor.setContent(this.content != null ? this.content : '');
        }
    };
    TinymceComponent.prototype.append = function (data, editorId) {
        var editor = !editorId ? tinymce.get(this.elementId) : tinymce.get(editorId);
        if (editor != null) {
            editor.execCommand('mceInsertContent', false, data);
        }
    };
    TinymceComponent.prototype.registerOnChange = function (fn) {
        this.propagateChange = fn;
    };
    TinymceComponent.prototype.destroy = function () {
        tinymce.remove("#" + this.elementId);
    };
    TinymceComponent.prototype.writeValue = function (value) {
        this.content = value;
        var editor = tinymce.get(this.elementId);
        this.initEditor();
    };
    TinymceComponent.prototype.registerOnTouched = function () {
    };
    var TinymceComponent_1;
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], TinymceComponent.prototype, "elementId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], TinymceComponent.prototype, "height", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TinymceComponent.prototype, "inline", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TinymceComponent.prototype, "menu", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TinymceComponent.prototype, "onEditorKeyup", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TinymceComponent.prototype, "onChange", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TinymceComponent.prototype, "onBlur", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
    ], TinymceComponent.prototype, "content", null);
    TinymceComponent = TinymceComponent_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            selector: 'tinymce',
            template: "\n        <div class=\"form-control tinymce-editor\" id=\"{{elementId}}\" *ngIf=\"inline\"\n             [ngStyle]=\"{'height': height + 'px'}\">\n            <span [innerHTML]=\"content\"></span>\n        </div>\n        <textarea *ngIf=\"!inline\" id=\"{{elementId}}\" [ngStyle]=\"{'height': height + 'px'}\"\n                  value=\"{{content}}\"></textarea>\n    ",
            providers: [
                { provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALUE_ACCESSOR"], useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () { return TinymceComponent_1; }), multi: true }
            ],
            styles: [__webpack_require__(/*! ./tinymce.component.scss */ "./src/app/shareds/components/tinymce/tinymce.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TinymceComponent);
    return TinymceComponent;
}());



/***/ }),

/***/ "./src/app/shareds/components/tinymce/tinymce.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/shareds/components/tinymce/tinymce.module.ts ***!
  \**************************************************************/
/*! exports provided: TinymceModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TinymceModule", function() { return TinymceModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _tinymce_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tinymce.component */ "./src/app/shareds/components/tinymce/tinymce.component.ts");




var TinymceModule = /** @class */ (function () {
    function TinymceModule() {
    }
    TinymceModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]],
            exports: [_tinymce_component__WEBPACK_IMPORTED_MODULE_3__["TinymceComponent"]],
            declarations: [_tinymce_component__WEBPACK_IMPORTED_MODULE_3__["TinymceComponent"]],
            providers: [],
        })
    ], TinymceModule);
    return TinymceModule;
}());



/***/ }),

/***/ "./src/app/validators/number.validator.ts":
/*!************************************************!*\
  !*** ./src/app/validators/number.validator.ts ***!
  \************************************************/
/*! exports provided: NumberValidator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NumberValidator", function() { return NumberValidator; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NumberValidator = /** @class */ (function () {
    function NumberValidator() {
    }
    NumberValidator.prototype.isValid = function (c) {
        if (c && c.value && c.value != null) {
            if (isNaN(parseFloat(c.value)) || !isFinite(c.value)) {
                return { isValid: false };
            }
        }
        return null;
    };
    NumberValidator.prototype.greaterThan = function (value) {
        return function (c) {
            if (value !== undefined && c.value) {
                if (c.value <= value) {
                    return { greaterThan: false };
                }
            }
            return null;
        };
    };
    NumberValidator.prototype.lessThan = function (value) {
        return function (c) {
            if (value && c.value) {
                if (c.value >= value) {
                    return { lessThan: false };
                }
            }
            return null;
        };
    };
    NumberValidator.prototype.range = function (value) {
        return function (c) {
            if (value && c.value) {
                if (c.value < value.fromValue || c.value > value.toValue) {
                    return { invalidRange: false };
                }
            }
            return null;
        };
    };
    NumberValidator = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NumberValidator);
    return NumberValidator;
}());



/***/ })

}]);
//# sourceMappingURL=modules-hr-assessment-assessment-module.js.map