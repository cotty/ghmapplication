(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~modules-configs-config-module~modules-surveys-survey-module~modules-task-target-target-modul~dab05ece"],{

/***/ "./src/app/modules/hr/organization/office/services/office.service.ts":
/*!***************************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/services/office.service.ts ***!
  \***************************************************************************/
/*! exports provided: OfficeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfficeService", function() { return OfficeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../shareds/components/nh-suggestion/nh-suggestion.component */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.component.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../../environments/environment */ "./src/environments/environment.ts");









var OfficeService = /** @class */ (function () {
    function OfficeService(appConfig, spinnerService, toastr, http) {
        this.appConfig = appConfig;
        this.spinnerService = spinnerService;
        this.toastr = toastr;
        this.http = http;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].apiGatewayUrl + "api/v1/hr/offices";
    }
    OfficeService.prototype.resolve = function (route, state) {
        var queryParams = route.queryParams;
        return this.search(queryParams.keyword, queryParams.isActive, queryParams.page, queryParams.pageSize);
    };
    OfficeService.prototype.search = function (keyword, isActive, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        return this.http
            .get(this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('isActive', isActive != null ? isActive.toString() : '')
                .set('page', page ? page.toString() : '0')
                .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString())
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            result.items.forEach(function (item) {
                item.activeStatus = item.isActive
                    ? 'active'
                    : 'inActive';
                var level = item.idPath.split('.');
                item.nameLevel = '';
                for (var i = 1; i < level.length; i++) {
                    item.nameLevel += '<i class="fa fa-long-arrow-right cm-mgr-5"></i>';
                }
            });
            return result;
        }));
    };
    OfficeService.prototype.searchName = function (keyword, isActive, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 20; }
        return this.http.get(this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('isActive', isActive != null ? isActive.toString() : '')
                .set('page', page ? page.toString() : '0')
                .set('pageSize', pageSize ? pageSize.toString() : '10')
        });
    };
    OfficeService.prototype.getDetail = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/" + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            var data = result.data;
            data.activeStatus = data.isActive ? 'active' : 'inActive';
            return result;
        }));
    };
    OfficeService.prototype.getEditDetail = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/edit/" + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            var data = result.data;
            data.activeStatus = data.isActive ? 'active' : 'inActive';
            return result;
        }));
    };
    OfficeService.prototype.insert = function (office) {
        var _this = this;
        return this.http.post("" + this.url, {
            isActive: office.isActive,
            code: office.code,
            officeType: office.officeType,
            parentId: office.parentId,
            officeTranslations: office.modelTranslations,
            officeContacts: office.officeContacts
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    OfficeService.prototype.update = function (id, office) {
        var _this = this;
        return this.http.post(this.url + "/" + id, {
            isActive: office.isActive,
            code: office.code,
            officeType: office.officeType,
            parentId: office.parentId,
            officeTranslations: office.modelTranslations,
            officeContacts: office.officeContacts
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    // updateIsActive(office: Office): Observable<IActionResultResponse> {
    //     return this.http.post(`${this.url}update-active-status`, '', {
    //         params: new HttpParams()
    //             .set('id', office.id.toString())
    //             .set('isActive', office.isActive.toString())
    //     }) as Observable<IActionResultResponse>;
    // }
    OfficeService.prototype.delete = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.delete(this.url + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    OfficeService.prototype.getTree = function () {
        return this.http.get(this.url + "/trees");
    };
    OfficeService.prototype.getOfficeUserTree = function () {
        return this.http.get(this.url + "/user-trees");
    };
    OfficeService.prototype.getOfficeUserTreeLazy = function (parentId) {
        return this.http.get("" + this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]().set('parentId', parentId ? parentId.toString() : '')
        });
    };
    OfficeService.prototype.searchForSuggestion = function (keyword) {
        return this.http
            .get(this.url + "/suggestions", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            return result.items.map(function (item) {
                return new _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_7__["NhSuggestion"](item.id, item.name);
            });
        }));
    };
    // Contacts.
    OfficeService.prototype.updateContact = function (officeId, id, contact) {
        var _this = this;
        return this.http
            .post(this.url + "/" + officeId + "/contacts/" + id, {
            userId: contact.userId,
            email: contact.email,
            phoneNumber: contact.phoneNumber,
            fax: contact.fax
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    OfficeService.prototype.addContact = function (officeId, contact) {
        var _this = this;
        return this.http
            .post(this.url + "/" + officeId + "/contacts", {
            userId: contact.userId,
            email: contact.email,
            phoneNumber: contact.phoneNumber,
            fax: contact.fax
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    OfficeService.prototype.deleteContact = function (officeId, contactId) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .delete(this.url + "/" + officeId + "/contacts/" + contactId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    OfficeService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_4__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_6__["SpinnerService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], OfficeService);
    return OfficeService;
}());



/***/ }),

/***/ "./src/app/shareds/components/nh-user-picker/nh-user-picker.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/shareds/components/nh-user-picker/nh-user-picker.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"nh-user-picker-container\" *ngIf=\"isShow\">\r\n    <div class=\"nh-user-picker-header\" *ngIf=\"title\">\r\n        <h4 class=\"bold\">{{ title }}</h4>\r\n    </div><!-- END: .nh-user-picker-header -->\r\n    <div class=\"nh-user-picker-body\">\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-12\">\r\n                <div class=\"alert alert-danger\" i18n=\"@@PleaseSelectAtLeastOneItem\" *ngIf=\"errorMessage\">\r\n                    {errorMessage, select, required {Vui lòng chọn ít nhất một nhân viên} other {}}\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"nh-user-picker-left\">\r\n            <h4 class=\"title\">{{allTitle}}</h4>\r\n            <form class=\"form-inline\" (ngSubmit)=\"search(1)\">\r\n                <div class=\"form-group cm-mgr-5\">\r\n                    <div class=\"input-group\">\r\n                        <input type=\"text\" class=\"form-control\"\r\n                               i18n=\"@@enterKeyword\"\r\n                               placeholder=\"Nhập từ khóa tìm kiếm\"\r\n                               [(ngModel)]=\"keyword\"\r\n                               name=\"keyword\">\r\n                        <span class=\"input-group-btn\">\r\n                            <button class=\"btn btn-primary cm-mgl-5\" type=\"submit\">\r\n                                <i class=\"fa fa-pulse fa-spinner\" *ngIf=\"isSearching\"></i>\r\n                                <i class=\"fa fa-search\" *ngIf=\"!isSearching\"></i>\r\n                            </button>\r\n                        </span>\r\n                    </div><!-- /input-group -->\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <nh-dropdown-tree\r\n                        height=\"250\"\r\n                        [data]=\"officeTree\"\r\n                        i18n-title=\"@@filterByOffice\"\r\n                        title=\"-- Tìm kiếm theo phòng ban --\"\r\n                        (nodeSelected)=\"onSelectOffice($event)\">\r\n                    </nh-dropdown-tree>\r\n                </div>\r\n            </form>\r\n            <ul class=\"nh-user-picker-items\">\r\n                <li *ngFor=\"let user of source\" (click)=\"selectItem(user)\"\r\n                    class=\"user-item\"\r\n                    [class.selected]=\"user.isSelected\">\r\n                    <div class=\"avatar-wrapper\">\r\n                        <img class=\"avatar-sm rounded-avatar\"\r\n                             ghmImage\r\n                             [src]=\"user.avatar\"\r\n                             alt=\"{{ user.fullName }}\">\r\n                    </div><!-- END: .avatar-wrapper -->\r\n                    <div class=\"user-info\">\r\n                        <span class=\"full-name\">{{ user.fullName }}</span>\r\n                        <div class=\"description\"> {{user.description }}</div>\r\n                    </div><!-- END: .info -->\r\n                </li>\r\n            </ul>\r\n        </div><!-- END: .nh-user-picker-left -->\r\n        <div class=\"nh-user-picker-right\">\r\n            <h4 class=\"title\">{{selectedTitle}}</h4>\r\n            <form class=\"form-inline\" (ngSubmit)=\"search(1)\">\r\n                <div class=\"form-group text-right\">\r\n                    <button type=\"button\" class=\"btn btn-primary\" i18n=\"@@deleteAll\" (click)=\"deleteAllSelected()\">\r\n                        Xóa tất cả\r\n                    </button>\r\n                </div>\r\n            </form>\r\n            <ul class=\"nh-user-picker-items\">\r\n                <li *ngFor=\"let user of selectedItems\"\r\n                    class=\"user-item\"\r\n                    (click)=\"removeSelectedUser(user)\">\r\n                    <div class=\"avatar-wrapper\">\r\n                        <img class=\"avatar-sm rounded-avatar\"\r\n                             ghmImage\r\n                             [src]=\"user.avatar\"\r\n                             alt=\"{{ user.fullName }}\">\r\n                    </div><!-- END: .avatar-wrapper -->\r\n                    <div class=\"user-info\">\r\n                        <svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" focusable=\"false\"\r\n                             role=\"presentation\">\r\n                            <path\r\n                                d=\"M12 10.586L6.707 5.293a1 1 0 0 0-1.414 1.414L10.586 12l-5.293 5.293a1 1 0 0 0 1.414 1.414L12 13.414l5.293 5.293a1 1 0 0 0 1.414-1.414L13.414 12l5.293-5.293a1 1 0 1 0-1.414-1.414L12 10.586z\"\r\n                                fill=\"currentColor\">\r\n                            </path>\r\n                        </svg>\r\n                        <span class=\"full-name\">{{ user.fullName }}</span>\r\n                        <div class=\"description\"> {{user.description }}</div>\r\n                    </div><!-- END: .info -->\r\n                </li>\r\n            </ul>\r\n        </div><!-- END: .nh-user-picker-right -->\r\n    </div><!-- END: .nh-user-picker-body -->\r\n    <div class=\"nh-user-picker-footer\">\r\n        <ghm-paging\r\n            [totalRows]=\"totalRows\"\r\n            [currentPage]=\"currentPage\"\r\n            [pageShow]=\"6\"\r\n            [isDisabled]=\"isSearching\"\r\n            [pageSize]=\"pageSize\"\r\n            (pageClick)=\"search($event)\"\r\n        ></ghm-paging>\r\n        <button class=\"btn btn-primary\" i18n=\"@@accept\" (click)=\"accept()\">Đồng ý</button>\r\n        <button class=\"btn btn-light\" i18n=\"@@cancel\" (click)=\"dismiss()\">Hủy</button>\r\n    </div><!-- END: .nh-user-picker-footer -->\r\n</div><!-- END: .nh-user-picker-container -->\r\n"

/***/ }),

/***/ "./src/app/shareds/components/nh-user-picker/nh-user-picker.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/shareds/components/nh-user-picker/nh-user-picker.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".rounded-avatar {\n  border-radius: 50%; }\n\n.avatar-wrapper {\n  overflow: hidden; }\n\n.avatar-xs {\n  width: 16px;\n  hight: 16px; }\n\n.avatar-sm {\n  width: 32px;\n  height: 32px; }\n\n.nh-user-picker-container {\n  border: 1px solid #ddd;\n  width: 60%;\n  margin: 0px auto;\n  position: fixed;\n  top: 20%;\n  left: 0;\n  right: 0;\n  background: white;\n  z-index: 9999999; }\n\n.nh-user-picker-container ul.nh-user-picker-items {\n    list-style: none;\n    padding-left: 0;\n    margin-top: 10px;\n    margin-bottom: 0;\n    border: 1px solid #ddd;\n    height: 250px;\n    min-height: 250px;\n    max-height: 250px;\n    overflow-y: auto; }\n\n.nh-user-picker-container ul.nh-user-picker-items li {\n      border-bottom: 1px solid #ddd;\n      padding: 3px 7px; }\n\n.nh-user-picker-container ul.nh-user-picker-items li:last-child {\n        border-bottom: none; }\n\n.nh-user-picker-container ul.nh-user-picker-items li:hover {\n        cursor: pointer;\n        background: #eaeaea; }\n\n.nh-user-picker-container ul.nh-user-picker-items li a.nh-user-picker-item-action {\n        float: right;\n        color: #D91E18; }\n\n.nh-user-picker-container .nh-user-picker-header, .nh-user-picker-container .nh-user-picker-body, .nh-user-picker-container .nh-user-picker-footer {\n    padding: 10px; }\n\n.nh-user-picker-container .nh-user-picker-header {\n    border-bottom: 1px solid #ddd; }\n\n.nh-user-picker-container .nh-user-picker-body {\n    position: relative;\n    overflow: hidden; }\n\n.nh-user-picker-container .nh-user-picker-body .nh-user-picker-left {\n      padding-right: 5px; }\n\n.nh-user-picker-container .nh-user-picker-body .nh-user-picker-right {\n      padding-left: 5px; }\n\n.nh-user-picker-container .nh-user-picker-body .nh-user-picker-left, .nh-user-picker-container .nh-user-picker-body .nh-user-picker-right {\n      width: 50%;\n      display: block;\n      float: left; }\n\n.nh-user-picker-container .nh-user-picker-body .nh-user-picker-left h4.title, .nh-user-picker-container .nh-user-picker-body .nh-user-picker-right h4.title {\n        font-size: 14px;\n        font-weight: bold; }\n\n.nh-user-picker-container .nh-user-picker-footer {\n    border-top: 1px solid #ddd;\n    text-align: right; }\n\n.nh-user-picker-container .nh-user-picker-footer button {\n      margin-left: 5px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL25oLXVzZXItcGlja2VyL0Q6XFxQcm9qZWN0XFxHaG1BcHBsaWNhdGlvblxcY2xpZW50c1xcZ2htYXBwbGljYXRpb25jbGllbnQvc3JjXFxhcHBcXHNoYXJlZHNcXGNvbXBvbmVudHNcXG5oLXVzZXItcGlja2VyXFxuaC11c2VyLXBpY2tlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHQTtFQUdJLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLFdBQVc7RUFDWCxXQUFXLEVBQUE7O0FBR2Y7RUFDSSxXQUFXO0VBQ1gsWUFBWSxFQUFBOztBQUdoQjtFQUNJLHNCQXhCZTtFQXlCZixVQUFVO0VBQ1YsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixRQUFRO0VBQ1IsT0FBTztFQUNQLFFBQVE7RUFDUixpQkFBaUI7RUFDakIsZ0JBQWdCLEVBQUE7O0FBVHBCO0lBWVEsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLHNCQXZDVztJQXdDWCxhQUFhO0lBQ2IsaUJBQWlCO0lBQ2pCLGlCQUFpQjtJQUNqQixnQkFBZ0IsRUFBQTs7QUFwQnhCO01BdUJZLDZCQTlDTztNQStDUCxnQkFBZ0IsRUFBQTs7QUF4QjVCO1FBMkJnQixtQkFBbUIsRUFBQTs7QUEzQm5DO1FBK0JnQixlQUFlO1FBQ2YsbUJBQW1CLEVBQUE7O0FBaENuQztRQW9DZ0IsWUFBWTtRQUNaLGNBQWMsRUFBQTs7QUFyQzlCO0lBMkNRLGFBQWEsRUFBQTs7QUEzQ3JCO0lBK0NRLDZCQXRFVyxFQUFBOztBQXVCbkI7SUFtRFEsa0JBQWtCO0lBQ2xCLGdCQUFnQixFQUFBOztBQXBEeEI7TUF1RFksa0JBQWtCLEVBQUE7O0FBdkQ5QjtNQTBEWSxpQkFBaUIsRUFBQTs7QUExRDdCO01BNkRZLFVBQVU7TUFDVixjQUFjO01BQ2QsV0FBVyxFQUFBOztBQS9EdkI7UUFrRWdCLGVBQWU7UUFDZixpQkFBaUIsRUFBQTs7QUFuRWpDO0lBeUVRLDBCQWhHVztJQWlHWCxpQkFBaUIsRUFBQTs7QUExRXpCO01BNkVZLGdCQUFnQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL25oLXVzZXItcGlja2VyL25oLXVzZXItcGlja2VyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiJGJvcmRlci1jb2xvcjogI2RkZDtcclxuJGhvdmVyLWJnLWNvbG9yOiAjZWFlYWVhO1xyXG5cclxuLnJvdW5kZWQtYXZhdGFyIHtcclxuICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgLW1vei1ib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbn1cclxuXHJcbi5hdmF0YXItd3JhcHBlciB7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcblxyXG4uYXZhdGFyLXhzIHtcclxuICAgIHdpZHRoOiAxNnB4O1xyXG4gICAgaGlnaHQ6IDE2cHg7XHJcbn1cclxuXHJcbi5hdmF0YXItc20ge1xyXG4gICAgd2lkdGg6IDMycHg7XHJcbiAgICBoZWlnaHQ6IDMycHg7XHJcbn1cclxuXHJcbi5uaC11c2VyLXBpY2tlci1jb250YWluZXIge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgJGJvcmRlci1jb2xvcjtcclxuICAgIHdpZHRoOiA2MCU7XHJcbiAgICBtYXJnaW46IDBweCBhdXRvO1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgdG9wOiAyMCU7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIHotaW5kZXg6IDk5OTk5OTk7XHJcblxyXG4gICAgdWwubmgtdXNlci1waWNrZXItaXRlbXMge1xyXG4gICAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAwO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAkYm9yZGVyLWNvbG9yO1xyXG4gICAgICAgIGhlaWdodDogMjUwcHg7XHJcbiAgICAgICAgbWluLWhlaWdodDogMjUwcHg7XHJcbiAgICAgICAgbWF4LWhlaWdodDogMjUwcHg7XHJcbiAgICAgICAgb3ZlcmZsb3cteTogYXV0bztcclxuXHJcbiAgICAgICAgbGkge1xyXG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJGJvcmRlci1jb2xvcjtcclxuICAgICAgICAgICAgcGFkZGluZzogM3B4IDdweDtcclxuXHJcbiAgICAgICAgICAgICY6bGFzdC1jaGlsZCB7XHJcbiAgICAgICAgICAgICAgICBib3JkZXItYm90dG9tOiBub25lO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICNlYWVhZWE7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGEubmgtdXNlci1waWNrZXItaXRlbS1hY3Rpb24ge1xyXG4gICAgICAgICAgICAgICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICNEOTFFMTg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLm5oLXVzZXItcGlja2VyLWhlYWRlciwgLm5oLXVzZXItcGlja2VyLWJvZHksIC5uaC11c2VyLXBpY2tlci1mb290ZXIge1xyXG4gICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLm5oLXVzZXItcGlja2VyLWhlYWRlciB7XHJcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICRib3JkZXItY29sb3I7XHJcbiAgICB9XHJcblxyXG4gICAgLm5oLXVzZXItcGlja2VyLWJvZHkge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG5cclxuICAgICAgICAubmgtdXNlci1waWNrZXItbGVmdCB7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLm5oLXVzZXItcGlja2VyLXJpZ2h0IHtcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5uaC11c2VyLXBpY2tlci1sZWZ0LCAubmgtdXNlci1waWNrZXItcmlnaHQge1xyXG4gICAgICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcblxyXG4gICAgICAgICAgICBoNC50aXRsZSB7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAubmgtdXNlci1waWNrZXItZm9vdGVyIHtcclxuICAgICAgICBib3JkZXItdG9wOiAxcHggc29saWQgJGJvcmRlci1jb2xvcjtcclxuICAgICAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuXHJcbiAgICAgICAgYnV0dG9uIHtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDVweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/shareds/components/nh-user-picker/nh-user-picker.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/shareds/components/nh-user-picker/nh-user-picker.component.ts ***!
  \*******************************************************************************/
/*! exports provided: NhUserPickerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NhUserPickerComponent", function() { return NhUserPickerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _nh_user_picker_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./nh-user-picker.service */ "./src/app/shareds/components/nh-user-picker/nh-user-picker.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _modules_hr_organization_office_services_office_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../modules/hr/organization/office/services/office.service */ "./src/app/modules/hr/organization/office/services/office.service.ts");






var NhUserPickerComponent = /** @class */ (function () {
    function NhUserPickerComponent(userPickerService, officeService) {
        this.userPickerService = userPickerService;
        this.officeService = officeService;
        this.isShow = false;
        this.allTitle = '';
        this.selectedTitle = '';
        this.source = [];
        this.totalRows = 0;
        this.pageSize = 0;
        this.title = '';
        this.selectedItem = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.selectedPage = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.removedItem = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.accepted = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.close = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.errorMessage = '';
        this.currentPage = 1;
        this.isSearching = false;
        this.officeTree = [];
        this._selectedItems = [];
        // this.officeService.getTree()
        //     .subscribe((result: TreeData[]) => this.officeTree = result);
    }
    Object.defineProperty(NhUserPickerComponent.prototype, "selectedItems", {
        get: function () {
            return this._selectedItems;
        },
        set: function (value) {
            this._selectedItems = lodash__WEBPACK_IMPORTED_MODULE_2__["cloneDeep"](value);
        },
        enumerable: true,
        configurable: true
    });
    NhUserPickerComponent.prototype.ngOnInit = function () {
        // this.search(1);
    };
    NhUserPickerComponent.prototype.ngOnChanges = function (changes) {
    };
    NhUserPickerComponent.prototype.onSelectOffice = function (officeTree) {
        this.officeId = officeTree ? officeTree.id : null;
        this.search(1);
    };
    NhUserPickerComponent.prototype.show = function () {
        var _this = this;
        this.officeId = null;
        this.isShow = true;
        if (!this.officeTree || this.officeTree.length === 0) {
            this.officeService.getTree()
                .subscribe(function (result) { return _this.officeTree = result; });
        }
        this.search(1);
        lodash__WEBPACK_IMPORTED_MODULE_2__["each"](this.source, function (item) {
            var selectedItemInfo = lodash__WEBPACK_IMPORTED_MODULE_2__["find"](_this.selectedItems, function (selectedItem) {
                return selectedItem.id === item.id;
            });
            item.isSelected = selectedItemInfo != null && selectedItemInfo !== undefined;
        });
    };
    NhUserPickerComponent.prototype.deleteAllSelected = function () {
        this.selectedItems = [];
        lodash__WEBPACK_IMPORTED_MODULE_2__["each"](this.source, function (item) {
            item.isSelected = false;
        });
    };
    NhUserPickerComponent.prototype.removeSelectedUser = function (user) {
        lodash__WEBPACK_IMPORTED_MODULE_2__["remove"](this.selectedItems, function (item) {
            return item.id === user.id;
        });
        var userInfo = lodash__WEBPACK_IMPORTED_MODULE_2__["find"](this.source, function (item) {
            return item.id === user.id;
        });
        if (userInfo) {
            userInfo.isSelected = false;
        }
        this.removedItem.emit(user);
    };
    NhUserPickerComponent.prototype.dismiss = function () {
        this.isShow = false;
        this.close.emit();
    };
    NhUserPickerComponent.prototype.selectItem = function (item) {
        this.errorMessage = '';
        this.selectedItem.emit(item);
        var existingItem = lodash__WEBPACK_IMPORTED_MODULE_2__["find"](this.selectedItems, function (selectedItem) {
            return selectedItem.id === item.id;
        });
        if (existingItem) {
            return;
        }
        item.isSelected = true;
        this.selectedItems.push(item);
    };
    NhUserPickerComponent.prototype.removeItem = function (id) {
        lodash__WEBPACK_IMPORTED_MODULE_2__["remove"](this.selectedItems, function (selectedItem) {
            return selectedItem.id === id;
        });
        this.removedItem.emit(id);
    };
    NhUserPickerComponent.prototype.accept = function () {
        if (!this.selectedItems || this.selectedItems.length === 0) {
            this.errorMessage = 'required';
            return;
        }
        this.accepted.emit(this.selectedItems);
        this.isShow = false;
        this.selectedItems = [];
    };
    NhUserPickerComponent.prototype.search = function (currentPage) {
        var _this = this;
        this.currentPage = currentPage;
        this.isSearching = true;
        this.userPickerService.search(this.keyword, this.officeId, this.currentPage, this.pageSize)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () { return _this.isSearching = false; }))
            .subscribe(function (result) {
            _this.totalRows = result.totalRows;
            lodash__WEBPACK_IMPORTED_MODULE_2__["each"](result.items, function (item) {
                var selectedItemInfo = lodash__WEBPACK_IMPORTED_MODULE_2__["find"](_this.selectedItems, function (selectedItem) {
                    return selectedItem.id === item.id;
                });
                item.isSelected = selectedItemInfo != null && selectedItemInfo !== undefined;
            });
            _this.source = result.items;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhUserPickerComponent.prototype, "isShow", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhUserPickerComponent.prototype, "allTitle", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhUserPickerComponent.prototype, "selectedTitle", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], NhUserPickerComponent.prototype, "source", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhUserPickerComponent.prototype, "totalRows", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhUserPickerComponent.prototype, "pageSize", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhUserPickerComponent.prototype, "title", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhUserPickerComponent.prototype, "selectedItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhUserPickerComponent.prototype, "selectedPage", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhUserPickerComponent.prototype, "removedItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhUserPickerComponent.prototype, "accepted", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhUserPickerComponent.prototype, "close", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Array])
    ], NhUserPickerComponent.prototype, "selectedItems", null);
    NhUserPickerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'nh-user-picker',
            template: __webpack_require__(/*! ./nh-user-picker.component.html */ "./src/app/shareds/components/nh-user-picker/nh-user-picker.component.html"),
            providers: [_modules_hr_organization_office_services_office_service__WEBPACK_IMPORTED_MODULE_5__["OfficeService"]],
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./nh-user-picker.component.scss */ "./src/app/shareds/components/nh-user-picker/nh-user-picker.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_nh_user_picker_service__WEBPACK_IMPORTED_MODULE_3__["NhUserPickerService"],
            _modules_hr_organization_office_services_office_service__WEBPACK_IMPORTED_MODULE_5__["OfficeService"]])
    ], NhUserPickerComponent);
    return NhUserPickerComponent;
}());



/***/ }),

/***/ "./src/app/shareds/components/nh-user-picker/nh-user-picker.model.ts":
/*!***************************************************************************!*\
  !*** ./src/app/shareds/components/nh-user-picker/nh-user-picker.model.ts ***!
  \***************************************************************************/
/*! exports provided: NhUserPicker */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NhUserPicker", function() { return NhUserPicker; });
var NhUserPicker = /** @class */ (function () {
    function NhUserPicker(id, fullName, avatar, description) {
        this.id = id;
        this.fullName = fullName;
        this.avatar = avatar;
        this.description = description;
        this.isSelected = false;
    }
    return NhUserPicker;
}());



/***/ }),

/***/ "./src/app/shareds/components/nh-user-picker/nh-user-picker.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/shareds/components/nh-user-picker/nh-user-picker.module.ts ***!
  \****************************************************************************/
/*! exports provided: NhUserPickerModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NhUserPickerModule", function() { return NhUserPickerModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _nh_user_picker_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./nh-user-picker.component */ "./src/app/shareds/components/nh-user-picker/nh-user-picker.component.ts");
/* harmony import */ var _nh_user_picker_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./nh-user-picker.service */ "./src/app/shareds/components/nh-user-picker/nh-user-picker.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _ghm_paging_ghm_paging_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../ghm-paging/ghm-paging.module */ "./src/app/shareds/components/ghm-paging/ghm-paging.module.ts");
/* harmony import */ var _nh_tree_nh_tree_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../nh-tree/nh-tree.module */ "./src/app/shareds/components/nh-tree/nh-tree.module.ts");









var NhUserPickerModule = /** @class */ (function () {
    function NhUserPickerModule() {
    }
    NhUserPickerModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _core_core_module__WEBPACK_IMPORTED_MODULE_6__["CoreModule"], _ghm_paging_ghm_paging_module__WEBPACK_IMPORTED_MODULE_7__["GhmPagingModule"], _nh_tree_nh_tree_module__WEBPACK_IMPORTED_MODULE_8__["NHTreeModule"]],
            exports: [_nh_user_picker_component__WEBPACK_IMPORTED_MODULE_3__["NhUserPickerComponent"]],
            declarations: [_nh_user_picker_component__WEBPACK_IMPORTED_MODULE_3__["NhUserPickerComponent"]],
            providers: [_nh_user_picker_service__WEBPACK_IMPORTED_MODULE_4__["NhUserPickerService"]],
        })
    ], NhUserPickerModule);
    return NhUserPickerModule;
}());



/***/ }),

/***/ "./src/app/shareds/components/nh-user-picker/nh-user-picker.service.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/shareds/components/nh-user-picker/nh-user-picker.service.ts ***!
  \*****************************************************************************/
/*! exports provided: NhUserPickerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NhUserPickerService", function() { return NhUserPickerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _nh_user_picker_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./nh-user-picker.model */ "./src/app/shareds/components/nh-user-picker/nh-user-picker.model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");






var NhUserPickerService = /** @class */ (function () {
    function NhUserPickerService(http) {
        this.http = http;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].apiGatewayUrl + "api/v1/hr/users";
    }
    NhUserPickerService.prototype.search = function (keyword, officeId, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        return this.http.get(this.url + "/suggestions", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('officeId', officeId ? officeId.toString() : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : '10')
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return {
                items: result.items.map(function (item) {
                    var description = item.officeName + " - " + item.positionName;
                    return new _nh_user_picker_model__WEBPACK_IMPORTED_MODULE_3__["NhUserPicker"](item.id, item.fullName, item.avatar, description);
                }),
                totalRows: result.totalRows
            };
        }));
    };
    NhUserPickerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], NhUserPickerService);
    return NhUserPickerService;
}());



/***/ })

}]);
//# sourceMappingURL=default~modules-configs-config-module~modules-surveys-survey-module~modules-task-target-target-modul~dab05ece.js.map