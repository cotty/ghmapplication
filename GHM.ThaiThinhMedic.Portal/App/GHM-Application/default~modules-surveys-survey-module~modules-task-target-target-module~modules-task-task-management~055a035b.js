(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~modules-surveys-survey-module~modules-task-target-target-module~modules-task-task-management~055a035b"],{

/***/ "./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-suggestion\r\n    #userSuggestion\r\n    [placeholder]=\"'Vui lòng nhập tên nhân viên cần tìm'\"\r\n    [sources]=\"listItems\"\r\n    [loading]=\"isSearching\"\r\n    [readonly]=\"readonly\"\r\n    [selectedItem]=\"selectedItem\"\r\n    [totalRows]=\"totalRows\"\r\n    (searched)=\"onSearchKeyPress($event)\"\r\n    (nextPage)=\"onNextPageClick($event)\"\r\n    (itemRemoved)=\"itemRemoved.emit($event)\"\r\n    (itemSelected)=\"onItemSelected($event)\"></nh-suggestion>\r\n"

/***/ }),

/***/ "./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.component.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.component.scss ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ghm-suggestion-user {\n  width: 100%; }\n\n.rounded-avatar {\n  border-radius: 50%; }\n\n.avatar-wrapper {\n  overflow: hidden; }\n\n.avatar-xs {\n  width: 16px;\n  hight: 16px; }\n\n.avatar-sm {\n  width: 32px;\n  height: 32px; }\n\n.hus-selected-wrapper .hus-item-selected .avatar-wrapper {\n  width: 20px;\n  height: auto;\n  padding-left: 0 !important; }\n\n.hus-selected-wrapper .hus-item-selected .avatar-wrapper img {\n    width: 100%;\n    border-radius: 50% !important; }\n\n.hus-container {\n  border: 1px solid #ddd;\n  background: white;\n  border-radius: 5px !important;\n  position: relative; }\n\n.hus-container:hover {\n    cursor: pointer; }\n\n.hus-container.active {\n    border: 1px solid #007bff;\n    background: white;\n    box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25); }\n\n.hus-container.active .hus-search-wrapper .hus-search-content .hus-search-input input {\n      background: white;\n      border: none;\n      outline: none; }\n\n.hus-container.active .hus-search-wrapper .hus-search-content .hus-search-input input:focus, .hus-container.active .hus-search-wrapper .hus-search-content .hus-search-input input:active, .hus-container.active .hus-search-wrapper .hus-search-content .hus-search-input input:visited {\n        border: none;\n        outline: none;\n        box-shadow: none !important; }\n\n.hus-container.active .hus-search-wrapper .hus-search-content .hus-item .user-info {\n      margin-bottom: 0 !important; }\n\n.hus-container ul {\n    list-style: none;\n    padding-left: 0;\n    margin-bottom: 0; }\n\n.hus-container .hus-search-wrapper {\n    align-items: center;\n    display: flex;\n    width: 100%;\n    min-height: 37px; }\n\n.hus-container .hus-search-wrapper .hus-search-content {\n      white-space: nowrap;\n      width: 100%;\n      flex: 1 1 auto;\n      margin: 3px 8px; }\n\n.hus-container .hus-search-wrapper .hus-search-content .hus-search-input {\n        white-space: nowrap;\n        width: 100%;\n        flex: 1 1 auto;\n        margin: 3px 8px; }\n\n.hus-container .hus-search-wrapper .hus-search-content .hus-search-input input {\n          border: none;\n          display: block;\n          width: 100%;\n          background: white; }\n\n.hus-container .hus-search-wrapper .hus-search-content .hus-search-input input:focus, .hus-container .hus-search-wrapper .hus-search-content .hus-search-input input:active, .hus-container .hus-search-wrapper .hus-search-content .hus-search-input input:visited {\n            border: none !important;\n            outline: none !important;\n            box-shadow: none !important; }\n\n.hus-container .hus-search-wrapper .hus-search-content ul {\n        display: flex;\n        flex-wrap: wrap;\n        justify-content: flex-start;\n        width: 100%; }\n\n.hus-container .hus-search-wrapper .hus-search-content ul li.hus-item-selected {\n          box-sizing: border-box;\n          display: inline-block;\n          background-color: #f4f5f7;\n          margin-top: 5px;\n          border-radius: 5px !important;\n          margin-left: 5px; }\n\n.hus-container .hus-search-wrapper .hus-search-content ul li.hus-item-selected div.hus-item {\n            color: #253858;\n            cursor: default;\n            display: flex;\n            height: 20px;\n            line-height: 1;\n            border-radius: 3px;\n            margin: 4px !important;\n            padding: 0px;\n            overflow: initial; }\n\n.hus-container .hus-search-wrapper .hus-search-content ul li.hus-item-selected div.hus-item .avatar-wrapper {\n              align-items: center;\n              display: flex;\n              justify-content: center; }\n\n.hus-container .hus-search-wrapper .hus-search-content ul li.hus-item-selected div.hus-item .user-info {\n              margin: 0 5px;\n              margin-bottom: 0 !important; }\n\n.hus-container .hus-search-wrapper .hus-search-content ul li.hus-item-selected div.hus-item .user-info .full-name {\n                font-size: 14px;\n                font-weight: normal;\n                line-height: 1;\n                margin-left: 4px;\n                margin-right: 4px;\n                max-width: 160px;\n                text-overflow: ellipsis;\n                white-space: nowrap;\n                padding: 2px 0px;\n                overflow: hidden; }\n\n.hus-container .hus-search-wrapper .hus-search-icon {\n      align-items: center;\n      display: flex;\n      justify-content: center;\n      flex: 0 0 24px;\n      margin: 0px 8px;\n      color: #222; }\n\n.hus-container .hus-search-wrapper .avatar-wrapper {\n      width: 30px; }\n\n.hus-container .hus-search-wrapper .avatar-wrapper img {\n        width: 100%; }\n\n.hus-container .hus-search-result-wrapper {\n    position: absolute;\n    left: 0;\n    top: 100%;\n    max-height: 250px;\n    overflow-y: auto;\n    background: white;\n    width: 100%;\n    z-index: 999999;\n    border: 1px solid #ddd;\n    border-radius: 5px !important;\n    box-shadow: rgba(9, 30, 66, 0.25) 0px 4px 8px -2px, rgba(9, 30, 66, 0.31) 0px 0px 1px; }\n\n.hus-container .hus-search-result-wrapper ul {\n      padding: 5px 0; }\n\n.hus-container .hus-search-result-wrapper ul li {\n        align-items: center;\n        box-sizing: border-box;\n        color: #172b4d;\n        cursor: pointer;\n        display: flex;\n        flex-wrap: nowrap;\n        font-size: 14px;\n        font-weight: normal;\n        padding: 0px 12px;\n        text-decoration: none; }\n\n.hus-container .hus-search-result-wrapper ul li.active, .hus-container .hus-search-result-wrapper ul li:hover {\n          cursor: pointer;\n          background-color: #f4f5f7; }\n\n.hus-container .hus-search-result-wrapper ul li.searching {\n          min-height: 34px; }\n\n.hus-container .hus-search-result-wrapper ul li.searching:hover {\n            background-color: white; }\n\n.hus-container .hus-search-result-wrapper ul li.searching div {\n            margin-left: 5px; }\n\n.hus-container .hus-search-result-wrapper ul li div.avatar-wrapper {\n          width: 32px;\n          height: 32px; }\n\n.hus-container .hus-search-result-wrapper ul li div.avatar-wrapper img {\n            width: 100%; }\n\n.hus-container .hus-item {\n    align-items: center;\n    box-sizing: border-box;\n    color: #172b4d;\n    cursor: pointer;\n    display: flex;\n    flex-wrap: nowrap;\n    font-size: 14px;\n    font-weight: normal;\n    padding: 0px 12px;\n    text-decoration: none; }\n\n.hus-container .hus-item div.avatar-wrapper {\n      background-color: white;\n      color: #091e42;\n      display: flex;\n      flex-direction: column;\n      max-height: calc(100% - 1px);\n      outline: 0;\n      align-self: flex-start;\n      border-radius: 50% !important; }\n\n.hus-container .hus-item div.user-info {\n      display: flex;\n      flex-direction: column;\n      margin: 0px 8px;\n      overflow: hidden; }\n\n.hus-container .hus-item div.user-info .full-name {\n        font-weight: bold; }\n\n.hus-container .hus-item div.user-info .description {\n        font-size: 12px;\n        color: #999; }\n\n.hus-container .hus-item .remove {\n      height: 16px;\n      width: 16px;\n      color: currentcolor;\n      display: inline-block;\n      fill: white;\n      line-height: 1; }\n\n.hus-container .hus-item .remove:hover {\n        cursor: pointer;\n        color: #bf2600; }\n\n.hus-container .hus-item .remove svg {\n        height: 16px;\n        width: 16px;\n        max-height: 100%;\n        max-width: 100%;\n        vertical-align: bottom;\n        overflow: hidden; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL2dobS1zdWdnZXN0aW9uLXVzZXIvRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFwcFxcc2hhcmVkc1xcY29tcG9uZW50c1xcZ2htLXN1Z2dlc3Rpb24tdXNlclxcZ2htLXN1Z2dlc3Rpb24tdXNlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFJQTtFQUNJLFdBQVcsRUFBQTs7QUFHZjtFQUdJLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLFdBQVc7RUFDWCxXQUFXLEVBQUE7O0FBR2Y7RUFDSSxXQUFXO0VBQ1gsWUFBWSxFQUFBOztBQUdoQjtFQUVRLFdBQVc7RUFDWCxZQUFZO0VBQ1osMEJBQTBCLEVBQUE7O0FBSmxDO0lBT1ksV0FBVztJQUNYLDZCQUE2QixFQUFBOztBQUt6QztFQUNJLHNCQXpDYztFQTBDZCxpQkEzQ1c7RUE0Q1gsNkJBQTZCO0VBQzdCLGtCQUFrQixFQUFBOztBQUp0QjtJQU9RLGVBQWUsRUFBQTs7QUFQdkI7SUFXUSx5QkFsRG1CO0lBbURuQixpQkFBaUI7SUFDakIsZ0RBQStDLEVBQUE7O0FBYnZEO01BaUJnQixpQkFBaUI7TUFDakIsWUFBWTtNQUNaLGFBQWEsRUFBQTs7QUFuQjdCO1FBc0JvQixZQUFZO1FBQ1osYUFBYTtRQUNiLDJCQUEyQixFQUFBOztBQXhCL0M7TUE2QmdCLDJCQUEyQixFQUFBOztBQTdCM0M7SUFtQ1EsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixnQkFBZ0IsRUFBQTs7QUFyQ3hCO0lBMENRLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsV0FBVztJQUNYLGdCQUFnQixFQUFBOztBQTdDeEI7TUFnRFksbUJBQW1CO01BQ25CLFdBQVc7TUFDWCxjQUFjO01BQ2QsZUFBZSxFQUFBOztBQW5EM0I7UUFzRGdCLG1CQUFtQjtRQUNuQixXQUFXO1FBQ1gsY0FBYztRQUNkLGVBQWUsRUFBQTs7QUF6RC9CO1VBNERvQixZQUFZO1VBQ1osY0FBYztVQUNkLFdBQVc7VUFDWCxpQkF4R0wsRUFBQTs7QUF5Q2Y7WUFrRXdCLHVCQUF1QjtZQUN2Qix3QkFBd0I7WUFDeEIsMkJBQTJCLEVBQUE7O0FBcEVuRDtRQTBFZ0IsYUFBYTtRQUNiLGVBQWU7UUFFZiwyQkFBMkI7UUFDM0IsV0FBVyxFQUFBOztBQTlFM0I7VUFpRm9CLHNCQUFzQjtVQUN0QixxQkFBcUI7VUFDckIseUJBQW9DO1VBQ3BDLGVBQWU7VUFDZiw2QkFBNkI7VUFDN0IsZ0JBQWdCLEVBQUE7O0FBdEZwQztZQXlGd0IsY0FBc0I7WUFDdEIsZUFBZTtZQUNmLGFBQWE7WUFDYixZQUFZO1lBQ1osY0FBYztZQUNkLGtCQUFrQjtZQUNsQixzQkFBc0I7WUFDdEIsWUFBWTtZQUNaLGlCQUFpQixFQUFBOztBQWpHekM7Y0FxRzRCLG1CQUFtQjtjQUNuQixhQUFhO2NBRWIsdUJBQXVCLEVBQUE7O0FBeEduRDtjQTRHNEIsYUFBYTtjQUNiLDJCQUEyQixFQUFBOztBQTdHdkQ7Z0JBZ0hnQyxlQUFlO2dCQUNmLG1CQUFtQjtnQkFDbkIsY0FBYztnQkFDZCxnQkFBZ0I7Z0JBQ2hCLGlCQUFpQjtnQkFDakIsZ0JBQWdCO2dCQUNoQix1QkFBdUI7Z0JBQ3ZCLG1CQUFtQjtnQkFDbkIsZ0JBQWdCO2dCQUNoQixnQkFBZ0IsRUFBQTs7QUF6SGhEO01BbUlZLG1CQUFtQjtNQUNuQixhQUFhO01BRWIsdUJBQXVCO01BQ3ZCLGNBQWM7TUFDZCxlQUFlO01BQ2YsV0FBVyxFQUFBOztBQXpJdkI7TUE2SVksV0FBVyxFQUFBOztBQTdJdkI7UUFnSmdCLFdBQVcsRUFBQTs7QUFoSjNCO0lBc0pRLGtCQUFrQjtJQUNsQixPQUFPO0lBQ1AsU0FBUztJQUNULGlCQUFpQjtJQUNqQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLFdBQVc7SUFDWCxlQUFlO0lBQ2Ysc0JBdE1VO0lBeU1WLDZCQUE2QjtJQUM3QixxRkFBcUYsRUFBQTs7QUFsSzdGO01BcUtZLGNBQWMsRUFBQTs7QUFySzFCO1FBeUtnQixtQkFBbUI7UUFDbkIsc0JBQXNCO1FBQ3RCLGNBQXNCO1FBQ3RCLGVBQWU7UUFDZixhQUFhO1FBQ2IsaUJBQWlCO1FBQ2pCLGVBQWU7UUFDZixtQkFBbUI7UUFDbkIsaUJBQWlCO1FBQ2pCLHFCQUFxQixFQUFBOztBQWxMckM7VUFxTG9CLGVBQWU7VUFDZix5QkFBb0MsRUFBQTs7QUF0THhEO1VBMExvQixnQkFBZ0IsRUFBQTs7QUExTHBDO1lBNkx3Qix1QkFBdUIsRUFBQTs7QUE3TC9DO1lBaU13QixnQkFBZ0IsRUFBQTs7QUFqTXhDO1VBc01vQixXQUFXO1VBQ1gsWUFBWSxFQUFBOztBQXZNaEM7WUEwTXdCLFdBQVcsRUFBQTs7QUExTW5DO0lBbU5RLG1CQUFtQjtJQUNuQixzQkFBc0I7SUFDdEIsY0FBc0I7SUFDdEIsZUFBZTtJQUNmLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIscUJBQXFCLEVBQUE7O0FBNU43QjtNQStOWSx1QkFBb0M7TUFDcEMsY0FBcUI7TUFDckIsYUFBYTtNQUNiLHNCQUFzQjtNQUN0Qiw0QkFBNEI7TUFDNUIsVUFBVTtNQUNWLHNCQUFzQjtNQUd0Qiw2QkFBNkIsRUFBQTs7QUF4T3pDO01BNE9ZLGFBQWE7TUFDYixzQkFBc0I7TUFDdEIsZUFBZTtNQUNmLGdCQUFnQixFQUFBOztBQS9PNUI7UUFrUGdCLGlCQUFpQixFQUFBOztBQWxQakM7UUFxUGdCLGVBQWU7UUFDZixXQUFXLEVBQUE7O0FBdFAzQjtNQTBQWSxZQUFZO01BQ1osV0FBVztNQUNYLG1CQUFtQjtNQUNuQixxQkFBcUI7TUFDckIsV0FBd0I7TUFDeEIsY0FBYyxFQUFBOztBQS9QMUI7UUFrUWdCLGVBQWU7UUFDZixjQUFzQixFQUFBOztBQW5RdEM7UUF1UWdCLFlBQVk7UUFDWixXQUFXO1FBQ1gsZ0JBQWdCO1FBQ2hCLGVBQWU7UUFDZixzQkFBc0I7UUFDdEIsZ0JBQWdCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWRzL2NvbXBvbmVudHMvZ2htLXN1Z2dlc3Rpb24tdXNlci9naG0tc3VnZ2VzdGlvbi11c2VyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiJGJnQ29sb3I6IHdoaXRlO1xyXG4kYm9yZGVyQ29sb3I6ICNkZGQ7XHJcbiRib3JkZXJBY3RpdmVDb2xvcjogIzAwN2JmZjtcclxuXHJcbmdobS1zdWdnZXN0aW9uLXVzZXIge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5yb3VuZGVkLWF2YXRhciB7XHJcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIC1tb3otYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG59XHJcblxyXG4uYXZhdGFyLXdyYXBwZXIge1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG5cclxuLmF2YXRhci14cyB7XHJcbiAgICB3aWR0aDogMTZweDtcclxuICAgIGhpZ2h0OiAxNnB4O1xyXG59XHJcblxyXG4uYXZhdGFyLXNtIHtcclxuICAgIHdpZHRoOiAzMnB4O1xyXG4gICAgaGVpZ2h0OiAzMnB4O1xyXG59XHJcblxyXG4uaHVzLXNlbGVjdGVkLXdyYXBwZXIge1xyXG4gICAgLmh1cy1pdGVtLXNlbGVjdGVkIC5hdmF0YXItd3JhcHBlciB7XHJcbiAgICAgICAgd2lkdGg6IDIwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogMCAhaW1wb3J0YW50O1xyXG5cclxuICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4uaHVzLWNvbnRhaW5lciB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAkYm9yZGVyQ29sb3I7XHJcbiAgICBiYWNrZ3JvdW5kOiAkYmdDb2xvcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweCAhaW1wb3J0YW50O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG5cclxuICAgICY6aG92ZXIge1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIH1cclxuXHJcbiAgICAmLmFjdGl2ZSB7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgJGJvcmRlckFjdGl2ZUNvbG9yO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDAgMCAwIDAuMnJlbSByZ2JhKDAsIDEyMywgMjU1LCAuMjUpO1xyXG5cclxuICAgICAgICAuaHVzLXNlYXJjaC13cmFwcGVyIC5odXMtc2VhcmNoLWNvbnRlbnQge1xyXG4gICAgICAgICAgICAuaHVzLXNlYXJjaC1pbnB1dCBpbnB1dCB7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgICAgICAgICAgIG91dGxpbmU6IG5vbmU7XHJcblxyXG4gICAgICAgICAgICAgICAgJjpmb2N1cywgJjphY3RpdmUsICY6dmlzaXRlZCB7XHJcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgICAgICAgICAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICAgICAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAuaHVzLWl0ZW0gLnVzZXItaW5mbyB7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgdWwge1xyXG4gICAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAwO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICB9XHJcblxyXG4gICAgLmh1cy1zZWFyY2gtd3JhcHBlciB7XHJcbiAgICAgICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgbWluLWhlaWdodDogMzdweDtcclxuXHJcbiAgICAgICAgLmh1cy1zZWFyY2gtY29udGVudCB7XHJcbiAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICBmbGV4OiAxIDEgYXV0bztcclxuICAgICAgICAgICAgbWFyZ2luOiAzcHggOHB4O1xyXG5cclxuICAgICAgICAgICAgLmh1cy1zZWFyY2gtaW5wdXQge1xyXG4gICAgICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgZmxleDogMSAxIGF1dG87XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDNweCA4cHg7XHJcblxyXG4gICAgICAgICAgICAgICAgaW5wdXQge1xyXG4gICAgICAgICAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAkYmdDb2xvcjtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgJjpmb2N1cywgJjphY3RpdmUsICY6dmlzaXRlZCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlcjogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvdXRsaW5lOiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHVsIHtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAgICAgICAgICAgICAtd2Via2l0LWJveC1wYWNrOiBzdGFydDtcclxuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgICAgICAgICAgICAgIGxpLmh1cy1pdGVtLXNlbGVjdGVkIHtcclxuICAgICAgICAgICAgICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjQ0LCAyNDUsIDI0Nyk7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDVweCAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGRpdi5odXMtaXRlbSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiByZ2IoMzcsIDU2LCA4OCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGN1cnNvcjogZGVmYXVsdDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDRweCAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG92ZXJmbG93OiBpbml0aWFsO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLmF2YXRhci13cmFwcGVyIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAudXNlci1pbmZvIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbjogMCA1cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLmZ1bGwtbmFtZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDRweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDRweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDE2MHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMnB4IDBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuaHVzLXNlYXJjaC1pY29uIHtcclxuICAgICAgICAgICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgZmxleDogMCAwIDI0cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbjogMHB4IDhweDtcclxuICAgICAgICAgICAgY29sb3I6ICMyMjI7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuYXZhdGFyLXdyYXBwZXIge1xyXG4gICAgICAgICAgICB3aWR0aDogMzBweDtcclxuXHJcbiAgICAgICAgICAgIGltZyB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuaHVzLXNlYXJjaC1yZXN1bHQtd3JhcHBlciB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgdG9wOiAxMDAlO1xyXG4gICAgICAgIG1heC1oZWlnaHQ6IDI1MHB4O1xyXG4gICAgICAgIG92ZXJmbG93LXk6IGF1dG87XHJcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgei1pbmRleDogOTk5OTk5O1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRib3JkZXJDb2xvcjtcclxuICAgICAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDVweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIC1tb3otYm9yZGVyLXJhZGl1czogNXB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgYm94LXNoYWRvdzogcmdiYSg5LCAzMCwgNjYsIDAuMjUpIDBweCA0cHggOHB4IC0ycHgsIHJnYmEoOSwgMzAsIDY2LCAwLjMxKSAwcHggMHB4IDFweDtcclxuXHJcbiAgICAgICAgdWwge1xyXG4gICAgICAgICAgICBwYWRkaW5nOiA1cHggMDtcclxuXHJcbiAgICAgICAgICAgIGxpIHtcclxuICAgICAgICAgICAgICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiByZ2IoMjMsIDQzLCA3Nyk7XHJcbiAgICAgICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICAgICAgZmxleC13cmFwOiBub3dyYXA7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogMHB4IDEycHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcblxyXG4gICAgICAgICAgICAgICAgJi5hY3RpdmUsICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjQ0LCAyNDUsIDI0Nyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgJi5zZWFyY2hpbmcge1xyXG4gICAgICAgICAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDM0cHg7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGRpdiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGRpdi5hdmF0YXItd3JhcHBlciB7XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDMycHg7XHJcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAzMnB4O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmh1cy1pdGVtIHtcclxuICAgICAgICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICBjb2xvcjogcmdiKDIzLCA0MywgNzcpO1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtd3JhcDogbm93cmFwO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgIHBhZGRpbmc6IDBweCAxMnB4O1xyXG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuXHJcbiAgICAgICAgZGl2LmF2YXRhci13cmFwcGVyIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDI1NSwgMjU1LCAyNTUpO1xyXG4gICAgICAgICAgICBjb2xvcjogcmdiKDksIDMwLCA2Nik7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgICAgIG1heC1oZWlnaHQ6IGNhbGMoMTAwJSAtIDFweCk7XHJcbiAgICAgICAgICAgIG91dGxpbmU6IDA7XHJcbiAgICAgICAgICAgIGFsaWduLXNlbGY6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNTAlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIC1tb3otYm9yZGVyLXJhZGl1czogNTAlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJSAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZGl2LnVzZXItaW5mbyB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgICAgIG1hcmdpbjogMHB4IDhweDtcclxuICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuXHJcbiAgICAgICAgICAgIC5mdWxsLW5hbWUge1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmRlc2NyaXB0aW9uIHtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAjOTk5O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5yZW1vdmUge1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDE2cHg7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxNnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogY3VycmVudGNvbG9yO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgIGZpbGw6IHJnYigyNTUsIDI1NSwgMjU1KTtcclxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDE7XHJcblxyXG4gICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiByZ2IoMTkxLCAzOCwgMCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHN2ZyB7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDE2cHg7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTZweDtcclxuICAgICAgICAgICAgICAgIG1heC1oZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogYm90dG9tO1xyXG4gICAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: UserSuggestion, GhmSuggestionUserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserSuggestion", function() { return UserSuggestion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmSuggestionUserComponent", function() { return GhmSuggestionUserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../nh-suggestion/nh-suggestion.component */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.component.ts");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _ghm_suggestion_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ghm-suggestion-user.service */ "./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.service.ts");






var UserSuggestion = /** @class */ (function () {
    function UserSuggestion(id, fullName, officeName, positionName, avatar, isSelected) {
        this.id = id;
        this.fullName = fullName;
        this.officeName = officeName;
        this.positionName = positionName;
        this.avatar = avatar;
        this.isSelected = isSelected;
        this.isActive = false;
    }
    return UserSuggestion;
}());

var GhmSuggestionUserComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](GhmSuggestionUserComponent, _super);
    function GhmSuggestionUserComponent(userSuggestionService) {
        var _this = _super.call(this) || this;
        _this.userSuggestionService = userSuggestionService;
        _this.multiple = false;
        _this.readonly = false;
        _this.keyPressed = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.itemSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.itemRemoved = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        return _this;
    }
    GhmSuggestionUserComponent.prototype.ngOnInit = function () {
    };
    GhmSuggestionUserComponent.prototype.onItemSelected = function (item) {
        this.itemSelected.emit(item);
    };
    GhmSuggestionUserComponent.prototype.onSearchKeyPress = function (keyword) {
        this.keyPressed.emit(keyword);
        this.keyword = keyword;
        this.search(1);
    };
    GhmSuggestionUserComponent.prototype.onNextPageClick = function (data) {
        this.keyword = data.keyword;
        this.pageSize = data.pageSize;
        this.search(data.page, true);
    };
    GhmSuggestionUserComponent.prototype.search = function (currentPage, isAppend) {
        var _this = this;
        if (isAppend === void 0) { isAppend = false; }
        this.isSearching = true;
        this.currentPage = currentPage;
        this.userSuggestionService.search(this.keyword, this.currentPage, this.appConfig.PAGE_SIZE)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () { return _this.isSearching = false; }))
            .subscribe(function (result) {
            var _a;
            _this.totalRows = result.totalRows;
            if (!isAppend) {
                _this.listItems = result.items;
            }
            else {
                (_a = _this.listItems).push.apply(_a, result.items);
                _this.nhSuggestionComponent.updateScrollPosition();
            }
        });
    };
    GhmSuggestionUserComponent.prototype.clear = function () {
        this.nhSuggestionComponent.clear();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_3__["NhSuggestionComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_3__["NhSuggestionComponent"])
    ], GhmSuggestionUserComponent.prototype, "nhSuggestionComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmSuggestionUserComponent.prototype, "multiple", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmSuggestionUserComponent.prototype, "selectedItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmSuggestionUserComponent.prototype, "selectedUser", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmSuggestionUserComponent.prototype, "readonly", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmSuggestionUserComponent.prototype, "keyPressed", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmSuggestionUserComponent.prototype, "itemSelected", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmSuggestionUserComponent.prototype, "itemRemoved", void 0);
    GhmSuggestionUserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ghm-suggestion-user',
            template: __webpack_require__(/*! ./ghm-suggestion-user.component.html */ "./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./ghm-suggestion-user.component.scss */ "./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ghm_suggestion_user_service__WEBPACK_IMPORTED_MODULE_5__["GhmSuggestionUserService"]])
    ], GhmSuggestionUserComponent);
    return GhmSuggestionUserComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_4__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.module.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.module.ts ***!
  \**************************************************************************************/
/*! exports provided: GhmSuggestionUserModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmSuggestionUserModule", function() { return GhmSuggestionUserModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _nh_suggestion_nh_suggestion_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../nh-suggestion/nh-suggestion.module */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.module.ts");
/* harmony import */ var _ghm_suggestion_user_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ghm-suggestion-user.component */ "./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.component.ts");
/* harmony import */ var _ghm_suggestion_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./ghm-suggestion-user.service */ "./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.service.ts");








var GhmSuggestionUserModule = /** @class */ (function () {
    function GhmSuggestionUserModule() {
    }
    GhmSuggestionUserModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _core_core_module__WEBPACK_IMPORTED_MODULE_4__["CoreModule"], _nh_suggestion_nh_suggestion_module__WEBPACK_IMPORTED_MODULE_5__["NhSuggestionModule"]],
            exports: [_ghm_suggestion_user_component__WEBPACK_IMPORTED_MODULE_6__["GhmSuggestionUserComponent"]],
            declarations: [_ghm_suggestion_user_component__WEBPACK_IMPORTED_MODULE_6__["GhmSuggestionUserComponent"]],
            providers: [_ghm_suggestion_user_service__WEBPACK_IMPORTED_MODULE_7__["GhmSuggestionUserService"]],
        })
    ], GhmSuggestionUserModule);
    return GhmSuggestionUserModule;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.service.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.service.ts ***!
  \***************************************************************************************/
/*! exports provided: GhmSuggestionUserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmSuggestionUserService", function() { return GhmSuggestionUserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");






var GhmSuggestionUserService = /** @class */ (function () {
    function GhmSuggestionUserService(appConfig, http) {
        this.appConfig = appConfig;
        this.http = http;
    }
    GhmSuggestionUserService.prototype.search = function (keyword, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 20; }
        var url = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].apiGatewayUrl + "api/v1/hr/users/suggestions";
        return this.http
            .get(url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('page', page ? page.toString() : '')
                .set('pageSize', pageSize ? pageSize.toString() : '')
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return {
                totalRows: result.totalRows,
                items: result.items.map(function (item) {
                    return {
                        id: item.id,
                        name: item.fullName,
                        description: item.officeName + " - " + item.positionName,
                        isSelected: false,
                        data: item,
                        image: item.avatar
                    };
                })
            };
        }));
    };
    GhmSuggestionUserService.prototype.stripToVietnameChar = function (str) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
        str = str.replace(/đ/g, 'd');
        return str;
    };
    GhmSuggestionUserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_3__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], GhmSuggestionUserService);
    return GhmSuggestionUserService;
}());



/***/ })

}]);
//# sourceMappingURL=default~modules-surveys-survey-module~modules-task-target-target-module~modules-task-task-management~055a035b.js.map