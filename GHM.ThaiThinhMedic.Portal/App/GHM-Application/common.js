(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./src/app/modules/hr/user/models/user-contact.model.ts":
/*!**************************************************************!*\
  !*** ./src/app/modules/hr/user/models/user-contact.model.ts ***!
  \**************************************************************/
/*! exports provided: ContactType, UserContact */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactType", function() { return ContactType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserContact", function() { return UserContact; });
var ContactType = {
    homePhone: 0,
    mobilePhone: 1,
    email: 2,
    fax: 3
};
var UserContact = /** @class */ (function () {
    function UserContact() {
    }
    return UserContact;
}());



/***/ }),

/***/ "./src/app/modules/hr/user/models/user.model.ts":
/*!******************************************************!*\
  !*** ./src/app/modules/hr/user/models/user.model.ts ***!
  \******************************************************/
/*! exports provided: UserStatus, UserType, AcademicRank, MarriedStatus, Gender, User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserStatus", function() { return UserStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserType", function() { return UserType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcademicRank", function() { return AcademicRank; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MarriedStatus", function() { return MarriedStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Gender", function() { return Gender; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var UserStatus = {
    // Dịch vụ
    collaborators: 0,
    // Học việc
    apprentice: 1,
    // Thử việc
    probation: 2,
    // chính thức.
    official: 3,
    // Thai sản.
    maternity: 4,
    // Thôi việc.
    discontinue: 5,
    // Nghỉ hửu
    retirement: 6
};
var UserType = {
    // Nhân viên.
    staff: 0,
    // Trưởng đơn vị.
    leader: 1,
    // Phó đơn vị.
    viceLeader: 2
};
var AcademicRank = {
    // Thạc Sỹ
    master: 0,
    // Tiến sỹ
    phD: 1,
    // Giáo sư
    professor: 2
};
var MarriedStatus = {
    // Độc thân
    single: 0,
    // Kết hôn
    married: 1,
    // Ly thân
    separated: 2,
    // Ly hôn
    divorce: 3,
};
var Gender = {
    // Nam
    male: 1,
    // Nữ.
    female: 0,
    // Khác.
    other: 2
};
var User = /** @class */ (function () {
    function User() {
        this.userType = UserType.staff;
    }
    return User;
}());



/***/ }),

/***/ "./src/app/modules/hr/user/services/national.service.ts":
/*!**************************************************************!*\
  !*** ./src/app/modules/hr/user/services/national.service.ts ***!
  \**************************************************************/
/*! exports provided: NationalService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NationalService", function() { return NationalService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../environments/environment */ "./src/environments/environment.ts");





var NationalService = /** @class */ (function () {
    function NationalService(appConfig, http) {
        this.http = http;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiGatewayUrl + "api/v1/core/nationals";
        // this.url = `${appConfig.CORE_API_URL}${this.url}`;
    }
    NationalService.prototype.getAllNational = function () {
        return this.http.get("" + this.url);
    };
    NationalService.prototype.getProvinceByNational = function (nationalId) {
        return this.http.get(this.url + "/" + nationalId + "/provinces");
    };
    NationalService.prototype.getAllProvince = function () {
        return this.http.get(this.url + "/provinces");
    };
    NationalService.prototype.getAllDistrict = function () {
        return this.http.get(this.url + "/districts");
    };
    NationalService.prototype.getDistrictByProvinceId = function (provinceId) {
        return this.http.get(this.url + "/provinces/" + provinceId + "/districts");
    };
    NationalService.prototype.searchEthnic = function () {
        return this.http.get(this.url + "/ethnic");
    };
    NationalService.prototype.getAll = function () {
        return this.http.get(this.url + "/get-all");
    };
    NationalService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_3__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], NationalService);
    return NationalService;
}());



/***/ }),

/***/ "./src/app/shareds/models/time-object.model.ts":
/*!*****************************************************!*\
  !*** ./src/app/shareds/models/time-object.model.ts ***!
  \*****************************************************/
/*! exports provided: TimeObject */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimeObject", function() { return TimeObject; });
var TimeObject = /** @class */ (function () {
    function TimeObject(hour, minute, second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }
    return TimeObject;
}());



/***/ })

}]);
//# sourceMappingURL=common.js.map