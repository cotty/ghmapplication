(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-warehouse-goods-goods-module"],{

/***/ "./node_modules/file-saver/dist/FileSaver.min.js":
/*!*******************************************************!*\
  !*** ./node_modules/file-saver/dist/FileSaver.min.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;(function(a,b){if(true)!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (b),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));else {}})(this,function(){"use strict";function b(a,b){return"undefined"==typeof b?b={autoBom:!1}:"object"!=typeof b&&(console.warn("Depricated: Expected third argument to be a object"),b={autoBom:!b}),b.autoBom&&/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(a.type)?new Blob(["\uFEFF",a],{type:a.type}):a}function c(b,c,d){var e=new XMLHttpRequest;e.open("GET",b),e.responseType="blob",e.onload=function(){a(e.response,c,d)},e.onerror=function(){console.error("could not download file")},e.send()}function d(a){var b=new XMLHttpRequest;return b.open("HEAD",a,!1),b.send(),200<=b.status&&299>=b.status}function e(a){try{a.dispatchEvent(new MouseEvent("click"))}catch(c){var b=document.createEvent("MouseEvents");b.initMouseEvent("click",!0,!0,window,0,0,0,80,20,!1,!1,!1,!1,0,null),a.dispatchEvent(b)}}var f="object"==typeof window&&window.window===window?window:"object"==typeof self&&self.self===self?self:"object"==typeof global&&global.global===global?global:void 0,a=f.saveAs||"object"!=typeof window||window!==f?function(){}:"download"in HTMLAnchorElement.prototype?function(b,g,h){var i=f.URL||f.webkitURL,j=document.createElement("a");g=g||b.name||"download",j.download=g,j.rel="noopener","string"==typeof b?(j.href=b,j.origin===location.origin?e(j):d(j.href)?c(b,g,h):e(j,j.target="_blank")):(j.href=i.createObjectURL(b),setTimeout(function(){i.revokeObjectURL(j.href)},4E4),setTimeout(function(){e(j)},0))}:"msSaveOrOpenBlob"in navigator?function(f,g,h){if(g=g||f.name||"download","string"!=typeof f)navigator.msSaveOrOpenBlob(b(f,h),g);else if(d(f))c(f,g,h);else{var i=document.createElement("a");i.href=f,i.target="_blank",setTimeout(function(){e(i)})}}:function(a,b,d,e){if(e=e||open("","_blank"),e&&(e.document.title=e.document.body.innerText="downloading..."),"string"==typeof a)return c(a,b,d);var g="application/octet-stream"===a.type,h=/constructor/i.test(f.HTMLElement)||f.safari,i=/CriOS\/[\d]+/.test(navigator.userAgent);if((i||g&&h)&&"object"==typeof FileReader){var j=new FileReader;j.onloadend=function(){var a=j.result;a=i?a:a.replace(/^data:[^;]*;/,"data:attachment/file;"),e?e.location.href=a:location=a,e=null},j.readAsDataURL(a)}else{var k=f.URL||f.webkitURL,l=k.createObjectURL(a);e?e.location=l:location.href=l,e=null,setTimeout(function(){k.revokeObjectURL(l)},4E4)}};f.saveAs=a.saveAs=a, true&&(module.exports=a)});

//# sourceMappingURL=FileSaver.min.js.map

/***/ }),

/***/ "./node_modules/jsbarcode/bin/JsBarcode.js":
/*!*************************************************!*\
  !*** ./node_modules/jsbarcode/bin/JsBarcode.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _barcodes = __webpack_require__(/*! ./barcodes/ */ "./node_modules/jsbarcode/bin/barcodes/index.js");

var _barcodes2 = _interopRequireDefault(_barcodes);

var _merge = __webpack_require__(/*! ./help/merge.js */ "./node_modules/jsbarcode/bin/help/merge.js");

var _merge2 = _interopRequireDefault(_merge);

var _linearizeEncodings = __webpack_require__(/*! ./help/linearizeEncodings.js */ "./node_modules/jsbarcode/bin/help/linearizeEncodings.js");

var _linearizeEncodings2 = _interopRequireDefault(_linearizeEncodings);

var _fixOptions = __webpack_require__(/*! ./help/fixOptions.js */ "./node_modules/jsbarcode/bin/help/fixOptions.js");

var _fixOptions2 = _interopRequireDefault(_fixOptions);

var _getRenderProperties = __webpack_require__(/*! ./help/getRenderProperties.js */ "./node_modules/jsbarcode/bin/help/getRenderProperties.js");

var _getRenderProperties2 = _interopRequireDefault(_getRenderProperties);

var _optionsFromStrings = __webpack_require__(/*! ./help/optionsFromStrings.js */ "./node_modules/jsbarcode/bin/help/optionsFromStrings.js");

var _optionsFromStrings2 = _interopRequireDefault(_optionsFromStrings);

var _ErrorHandler = __webpack_require__(/*! ./exceptions/ErrorHandler.js */ "./node_modules/jsbarcode/bin/exceptions/ErrorHandler.js");

var _ErrorHandler2 = _interopRequireDefault(_ErrorHandler);

var _exceptions = __webpack_require__(/*! ./exceptions/exceptions.js */ "./node_modules/jsbarcode/bin/exceptions/exceptions.js");

var _defaults = __webpack_require__(/*! ./options/defaults.js */ "./node_modules/jsbarcode/bin/options/defaults.js");

var _defaults2 = _interopRequireDefault(_defaults);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// The protype of the object returned from the JsBarcode() call


// Help functions
var API = function API() {};

// The first call of the library API
// Will return an object with all barcodes calls and the data that is used
// by the renderers


// Default values


// Exceptions
// Import all the barcodes
var JsBarcode = function JsBarcode(element, text, options) {
	var api = new API();

	if (typeof element === "undefined") {
		throw Error("No element to render on was provided.");
	}

	// Variables that will be pased through the API calls
	api._renderProperties = (0, _getRenderProperties2.default)(element);
	api._encodings = [];
	api._options = _defaults2.default;
	api._errorHandler = new _ErrorHandler2.default(api);

	// If text is set, use the simple syntax (render the barcode directly)
	if (typeof text !== "undefined") {
		options = options || {};

		if (!options.format) {
			options.format = autoSelectBarcode();
		}

		api.options(options)[options.format](text, options).render();
	}

	return api;
};

// To make tests work TODO: remove
JsBarcode.getModule = function (name) {
	return _barcodes2.default[name];
};

// Register all barcodes
for (var name in _barcodes2.default) {
	if (_barcodes2.default.hasOwnProperty(name)) {
		// Security check if the propery is a prototype property
		registerBarcode(_barcodes2.default, name);
	}
}
function registerBarcode(barcodes, name) {
	API.prototype[name] = API.prototype[name.toUpperCase()] = API.prototype[name.toLowerCase()] = function (text, options) {
		var api = this;
		return api._errorHandler.wrapBarcodeCall(function () {
			// Ensure text is options.text
			options.text = typeof options.text === 'undefined' ? undefined : '' + options.text;

			var newOptions = (0, _merge2.default)(api._options, options);
			newOptions = (0, _optionsFromStrings2.default)(newOptions);
			var Encoder = barcodes[name];
			var encoded = encode(text, Encoder, newOptions);
			api._encodings.push(encoded);

			return api;
		});
	};
}

// encode() handles the Encoder call and builds the binary string to be rendered
function encode(text, Encoder, options) {
	// Ensure that text is a string
	text = "" + text;

	var encoder = new Encoder(text, options);

	// If the input is not valid for the encoder, throw error.
	// If the valid callback option is set, call it instead of throwing error
	if (!encoder.valid()) {
		throw new _exceptions.InvalidInputException(encoder.constructor.name, text);
	}

	// Make a request for the binary data (and other infromation) that should be rendered
	var encoded = encoder.encode();

	// Encodings can be nestled like [[1-1, 1-2], 2, [3-1, 3-2]
	// Convert to [1-1, 1-2, 2, 3-1, 3-2]
	encoded = (0, _linearizeEncodings2.default)(encoded);

	// Merge
	for (var i = 0; i < encoded.length; i++) {
		encoded[i].options = (0, _merge2.default)(options, encoded[i].options);
	}

	return encoded;
}

function autoSelectBarcode() {
	// If CODE128 exists. Use it
	if (_barcodes2.default["CODE128"]) {
		return "CODE128";
	}

	// Else, take the first (probably only) barcode
	return Object.keys(_barcodes2.default)[0];
}

// Sets global encoder options
// Added to the api by the JsBarcode function
API.prototype.options = function (options) {
	this._options = (0, _merge2.default)(this._options, options);
	return this;
};

// Will create a blank space (usually in between barcodes)
API.prototype.blank = function (size) {
	var zeroes = new Array(size + 1).join("0");
	this._encodings.push({ data: zeroes });
	return this;
};

// Initialize JsBarcode on all HTML elements defined.
API.prototype.init = function () {
	// Should do nothing if no elements where found
	if (!this._renderProperties) {
		return;
	}

	// Make sure renderProperies is an array
	if (!Array.isArray(this._renderProperties)) {
		this._renderProperties = [this._renderProperties];
	}

	var renderProperty;
	for (var i in this._renderProperties) {
		renderProperty = this._renderProperties[i];
		var options = (0, _merge2.default)(this._options, renderProperty.options);

		if (options.format == "auto") {
			options.format = autoSelectBarcode();
		}

		this._errorHandler.wrapBarcodeCall(function () {
			var text = options.value;
			var Encoder = _barcodes2.default[options.format.toUpperCase()];
			var encoded = encode(text, Encoder, options);

			render(renderProperty, encoded, options);
		});
	}
};

// The render API call. Calls the real render function.
API.prototype.render = function () {
	if (!this._renderProperties) {
		throw new _exceptions.NoElementException();
	}

	if (Array.isArray(this._renderProperties)) {
		for (var i = 0; i < this._renderProperties.length; i++) {
			render(this._renderProperties[i], this._encodings, this._options);
		}
	} else {
		render(this._renderProperties, this._encodings, this._options);
	}

	return this;
};

API.prototype._defaults = _defaults2.default;

// Prepares the encodings and calls the renderer
function render(renderProperties, encodings, options) {
	encodings = (0, _linearizeEncodings2.default)(encodings);

	for (var i = 0; i < encodings.length; i++) {
		encodings[i].options = (0, _merge2.default)(options, encodings[i].options);
		(0, _fixOptions2.default)(encodings[i].options);
	}

	(0, _fixOptions2.default)(options);

	var Renderer = renderProperties.renderer;
	var renderer = new Renderer(renderProperties.element, encodings, options);
	renderer.render();

	if (renderProperties.afterRender) {
		renderProperties.afterRender();
	}
}

// Export to browser
if (typeof window !== "undefined") {
	window.JsBarcode = JsBarcode;
}

// Export to jQuery
/*global jQuery */
if (typeof jQuery !== 'undefined') {
	jQuery.fn.JsBarcode = function (content, options) {
		var elementArray = [];
		jQuery(this).each(function () {
			elementArray.push(this);
		});
		return JsBarcode(elementArray, content, options);
	};
}

// Export to commonJS
module.exports = JsBarcode;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/Barcode.js":
/*!********************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/Barcode.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Barcode = function Barcode(data, options) {
	_classCallCheck(this, Barcode);

	this.data = data;
	this.text = options.text || data;
	this.options = options;
};

exports.default = Barcode;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/CODE128/CODE128.js":
/*!****************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/CODE128/CODE128.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Barcode2 = __webpack_require__(/*! ../Barcode.js */ "./node_modules/jsbarcode/bin/barcodes/Barcode.js");

var _Barcode3 = _interopRequireDefault(_Barcode2);

var _constants = __webpack_require__(/*! ./constants */ "./node_modules/jsbarcode/bin/barcodes/CODE128/constants.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// This is the master class,
// it does require the start code to be included in the string
var CODE128 = function (_Barcode) {
	_inherits(CODE128, _Barcode);

	function CODE128(data, options) {
		_classCallCheck(this, CODE128);

		// Get array of ascii codes from data
		var _this = _possibleConstructorReturn(this, (CODE128.__proto__ || Object.getPrototypeOf(CODE128)).call(this, data.substring(1), options));

		_this.bytes = data.split('').map(function (char) {
			return char.charCodeAt(0);
		});
		return _this;
	}

	_createClass(CODE128, [{
		key: 'valid',
		value: function valid() {
			// ASCII value ranges 0-127, 200-211
			return (/^[\x00-\x7F\xC8-\xD3]+$/.test(this.data)
			);
		}

		// The public encoding function

	}, {
		key: 'encode',
		value: function encode() {
			var bytes = this.bytes;
			// Remove the start code from the bytes and set its index
			var startIndex = bytes.shift() - 105;
			// Get start set by index
			var startSet = _constants.SET_BY_CODE[startIndex];

			if (startSet === undefined) {
				throw new RangeError('The encoding does not start with a start character.');
			}

			if (this.shouldEncodeAsEan128() === true) {
				bytes.unshift(_constants.FNC1);
			}

			// Start encode with the right type
			var encodingResult = CODE128.next(bytes, 1, startSet);

			return {
				text: this.text === this.data ? this.text.replace(/[^\x20-\x7E]/g, '') : this.text,
				data:
				// Add the start bits
				CODE128.getBar(startIndex) +
				// Add the encoded bits
				encodingResult.result +
				// Add the checksum
				CODE128.getBar((encodingResult.checksum + startIndex) % _constants.MODULO) +
				// Add the end bits
				CODE128.getBar(_constants.STOP)
			};
		}

		// GS1-128/EAN-128

	}, {
		key: 'shouldEncodeAsEan128',
		value: function shouldEncodeAsEan128() {
			var isEAN128 = this.options.ean128 || false;
			if (typeof isEAN128 === 'string') {
				isEAN128 = isEAN128.toLowerCase() === 'true';
			}
			return isEAN128;
		}

		// Get a bar symbol by index

	}], [{
		key: 'getBar',
		value: function getBar(index) {
			return _constants.BARS[index] ? _constants.BARS[index].toString() : '';
		}

		// Correct an index by a set and shift it from the bytes array

	}, {
		key: 'correctIndex',
		value: function correctIndex(bytes, set) {
			if (set === _constants.SET_A) {
				var charCode = bytes.shift();
				return charCode < 32 ? charCode + 64 : charCode - 32;
			} else if (set === _constants.SET_B) {
				return bytes.shift() - 32;
			} else {
				return (bytes.shift() - 48) * 10 + bytes.shift() - 48;
			}
		}
	}, {
		key: 'next',
		value: function next(bytes, pos, set) {
			if (!bytes.length) {
				return { result: '', checksum: 0 };
			}

			var nextCode = void 0,
			    index = void 0;

			// Special characters
			if (bytes[0] >= 200) {
				index = bytes.shift() - 105;
				var nextSet = _constants.SWAP[index];

				// Swap to other set
				if (nextSet !== undefined) {
					nextCode = CODE128.next(bytes, pos + 1, nextSet);
				}
				// Continue on current set but encode a special character
				else {
						// Shift
						if ((set === _constants.SET_A || set === _constants.SET_B) && index === _constants.SHIFT) {
							// Convert the next character so that is encoded correctly
							bytes[0] = set === _constants.SET_A ? bytes[0] > 95 ? bytes[0] - 96 : bytes[0] : bytes[0] < 32 ? bytes[0] + 96 : bytes[0];
						}
						nextCode = CODE128.next(bytes, pos + 1, set);
					}
			}
			// Continue encoding
			else {
					index = CODE128.correctIndex(bytes, set);
					nextCode = CODE128.next(bytes, pos + 1, set);
				}

			// Get the correct binary encoding and calculate the weight
			var enc = CODE128.getBar(index);
			var weight = index * pos;

			return {
				result: enc + nextCode.result,
				checksum: weight + nextCode.checksum
			};
		}
	}]);

	return CODE128;
}(_Barcode3.default);

exports.default = CODE128;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/CODE128/CODE128A.js":
/*!*****************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/CODE128/CODE128A.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _CODE2 = __webpack_require__(/*! ./CODE128.js */ "./node_modules/jsbarcode/bin/barcodes/CODE128/CODE128.js");

var _CODE3 = _interopRequireDefault(_CODE2);

var _constants = __webpack_require__(/*! ./constants */ "./node_modules/jsbarcode/bin/barcodes/CODE128/constants.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CODE128A = function (_CODE) {
	_inherits(CODE128A, _CODE);

	function CODE128A(string, options) {
		_classCallCheck(this, CODE128A);

		return _possibleConstructorReturn(this, (CODE128A.__proto__ || Object.getPrototypeOf(CODE128A)).call(this, _constants.A_START_CHAR + string, options));
	}

	_createClass(CODE128A, [{
		key: 'valid',
		value: function valid() {
			return new RegExp('^' + _constants.A_CHARS + '+$').test(this.data);
		}
	}]);

	return CODE128A;
}(_CODE3.default);

exports.default = CODE128A;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/CODE128/CODE128B.js":
/*!*****************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/CODE128/CODE128B.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _CODE2 = __webpack_require__(/*! ./CODE128.js */ "./node_modules/jsbarcode/bin/barcodes/CODE128/CODE128.js");

var _CODE3 = _interopRequireDefault(_CODE2);

var _constants = __webpack_require__(/*! ./constants */ "./node_modules/jsbarcode/bin/barcodes/CODE128/constants.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CODE128B = function (_CODE) {
	_inherits(CODE128B, _CODE);

	function CODE128B(string, options) {
		_classCallCheck(this, CODE128B);

		return _possibleConstructorReturn(this, (CODE128B.__proto__ || Object.getPrototypeOf(CODE128B)).call(this, _constants.B_START_CHAR + string, options));
	}

	_createClass(CODE128B, [{
		key: 'valid',
		value: function valid() {
			return new RegExp('^' + _constants.B_CHARS + '+$').test(this.data);
		}
	}]);

	return CODE128B;
}(_CODE3.default);

exports.default = CODE128B;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/CODE128/CODE128C.js":
/*!*****************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/CODE128/CODE128C.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _CODE2 = __webpack_require__(/*! ./CODE128.js */ "./node_modules/jsbarcode/bin/barcodes/CODE128/CODE128.js");

var _CODE3 = _interopRequireDefault(_CODE2);

var _constants = __webpack_require__(/*! ./constants */ "./node_modules/jsbarcode/bin/barcodes/CODE128/constants.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CODE128C = function (_CODE) {
	_inherits(CODE128C, _CODE);

	function CODE128C(string, options) {
		_classCallCheck(this, CODE128C);

		return _possibleConstructorReturn(this, (CODE128C.__proto__ || Object.getPrototypeOf(CODE128C)).call(this, _constants.C_START_CHAR + string, options));
	}

	_createClass(CODE128C, [{
		key: 'valid',
		value: function valid() {
			return new RegExp('^' + _constants.C_CHARS + '+$').test(this.data);
		}
	}]);

	return CODE128C;
}(_CODE3.default);

exports.default = CODE128C;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/CODE128/CODE128_AUTO.js":
/*!*********************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/CODE128/CODE128_AUTO.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _CODE2 = __webpack_require__(/*! ./CODE128 */ "./node_modules/jsbarcode/bin/barcodes/CODE128/CODE128.js");

var _CODE3 = _interopRequireDefault(_CODE2);

var _auto = __webpack_require__(/*! ./auto */ "./node_modules/jsbarcode/bin/barcodes/CODE128/auto.js");

var _auto2 = _interopRequireDefault(_auto);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CODE128AUTO = function (_CODE) {
	_inherits(CODE128AUTO, _CODE);

	function CODE128AUTO(data, options) {
		_classCallCheck(this, CODE128AUTO);

		// ASCII value ranges 0-127, 200-211
		if (/^[\x00-\x7F\xC8-\xD3]+$/.test(data)) {
			var _this = _possibleConstructorReturn(this, (CODE128AUTO.__proto__ || Object.getPrototypeOf(CODE128AUTO)).call(this, (0, _auto2.default)(data), options));
		} else {
			var _this = _possibleConstructorReturn(this, (CODE128AUTO.__proto__ || Object.getPrototypeOf(CODE128AUTO)).call(this, data, options));
		}
		return _possibleConstructorReturn(_this);
	}

	return CODE128AUTO;
}(_CODE3.default);

exports.default = CODE128AUTO;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/CODE128/auto.js":
/*!*************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/CODE128/auto.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _constants = __webpack_require__(/*! ./constants */ "./node_modules/jsbarcode/bin/barcodes/CODE128/constants.js");

// Match Set functions
var matchSetALength = function matchSetALength(string) {
	return string.match(new RegExp('^' + _constants.A_CHARS + '*'))[0].length;
};
var matchSetBLength = function matchSetBLength(string) {
	return string.match(new RegExp('^' + _constants.B_CHARS + '*'))[0].length;
};
var matchSetC = function matchSetC(string) {
	return string.match(new RegExp('^' + _constants.C_CHARS + '*'))[0];
};

// CODE128A or CODE128B
function autoSelectFromAB(string, isA) {
	var ranges = isA ? _constants.A_CHARS : _constants.B_CHARS;
	var untilC = string.match(new RegExp('^(' + ranges + '+?)(([0-9]{2}){2,})([^0-9]|$)'));

	if (untilC) {
		return untilC[1] + String.fromCharCode(204) + autoSelectFromC(string.substring(untilC[1].length));
	}

	var chars = string.match(new RegExp('^' + ranges + '+'))[0];

	if (chars.length === string.length) {
		return string;
	}

	return chars + String.fromCharCode(isA ? 205 : 206) + autoSelectFromAB(string.substring(chars.length), !isA);
}

// CODE128C
function autoSelectFromC(string) {
	var cMatch = matchSetC(string);
	var length = cMatch.length;

	if (length === string.length) {
		return string;
	}

	string = string.substring(length);

	// Select A/B depending on the longest match
	var isA = matchSetALength(string) >= matchSetBLength(string);
	return cMatch + String.fromCharCode(isA ? 206 : 205) + autoSelectFromAB(string, isA);
}

// Detect Code Set (A, B or C) and format the string

exports.default = function (string) {
	var newString = void 0;
	var cLength = matchSetC(string).length;

	// Select 128C if the string start with enough digits
	if (cLength >= 2) {
		newString = _constants.C_START_CHAR + autoSelectFromC(string);
	} else {
		// Select A/B depending on the longest match
		var isA = matchSetALength(string) > matchSetBLength(string);
		newString = (isA ? _constants.A_START_CHAR : _constants.B_START_CHAR) + autoSelectFromAB(string, isA);
	}

	return newString.replace(/[\xCD\xCE]([^])[\xCD\xCE]/, // Any sequence between 205 and 206 characters
	function (match, char) {
		return String.fromCharCode(203) + char;
	});
};

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/CODE128/constants.js":
/*!******************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/CODE128/constants.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _SET_BY_CODE;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// constants for internal usage
var SET_A = exports.SET_A = 0;
var SET_B = exports.SET_B = 1;
var SET_C = exports.SET_C = 2;

// Special characters
var SHIFT = exports.SHIFT = 98;
var START_A = exports.START_A = 103;
var START_B = exports.START_B = 104;
var START_C = exports.START_C = 105;
var MODULO = exports.MODULO = 103;
var STOP = exports.STOP = 106;
var FNC1 = exports.FNC1 = 207;

// Get set by start code
var SET_BY_CODE = exports.SET_BY_CODE = (_SET_BY_CODE = {}, _defineProperty(_SET_BY_CODE, START_A, SET_A), _defineProperty(_SET_BY_CODE, START_B, SET_B), _defineProperty(_SET_BY_CODE, START_C, SET_C), _SET_BY_CODE);

// Get next set by code
var SWAP = exports.SWAP = {
	101: SET_A,
	100: SET_B,
	99: SET_C
};

var A_START_CHAR = exports.A_START_CHAR = String.fromCharCode(208); // START_A + 105
var B_START_CHAR = exports.B_START_CHAR = String.fromCharCode(209); // START_B + 105
var C_START_CHAR = exports.C_START_CHAR = String.fromCharCode(210); // START_C + 105

// 128A (Code Set A)
// ASCII characters 00 to 95 (0–9, A–Z and control codes), special characters, and FNC 1–4
var A_CHARS = exports.A_CHARS = "[\x00-\x5F\xC8-\xCF]";

// 128B (Code Set B)
// ASCII characters 32 to 127 (0–9, A–Z, a–z), special characters, and FNC 1–4
var B_CHARS = exports.B_CHARS = "[\x20-\x7F\xC8-\xCF]";

// 128C (Code Set C)
// 00–99 (encodes two digits with a single code point) and FNC1
var C_CHARS = exports.C_CHARS = "(\xCF*[0-9]{2}\xCF*)";

// CODE128 includes 107 symbols:
// 103 data symbols, 3 start symbols (A, B and C), and 1 stop symbol (the last one)
// Each symbol consist of three black bars (1) and three white spaces (0).
var BARS = exports.BARS = [11011001100, 11001101100, 11001100110, 10010011000, 10010001100, 10001001100, 10011001000, 10011000100, 10001100100, 11001001000, 11001000100, 11000100100, 10110011100, 10011011100, 10011001110, 10111001100, 10011101100, 10011100110, 11001110010, 11001011100, 11001001110, 11011100100, 11001110100, 11101101110, 11101001100, 11100101100, 11100100110, 11101100100, 11100110100, 11100110010, 11011011000, 11011000110, 11000110110, 10100011000, 10001011000, 10001000110, 10110001000, 10001101000, 10001100010, 11010001000, 11000101000, 11000100010, 10110111000, 10110001110, 10001101110, 10111011000, 10111000110, 10001110110, 11101110110, 11010001110, 11000101110, 11011101000, 11011100010, 11011101110, 11101011000, 11101000110, 11100010110, 11101101000, 11101100010, 11100011010, 11101111010, 11001000010, 11110001010, 10100110000, 10100001100, 10010110000, 10010000110, 10000101100, 10000100110, 10110010000, 10110000100, 10011010000, 10011000010, 10000110100, 10000110010, 11000010010, 11001010000, 11110111010, 11000010100, 10001111010, 10100111100, 10010111100, 10010011110, 10111100100, 10011110100, 10011110010, 11110100100, 11110010100, 11110010010, 11011011110, 11011110110, 11110110110, 10101111000, 10100011110, 10001011110, 10111101000, 10111100010, 11110101000, 11110100010, 10111011110, 10111101110, 11101011110, 11110101110, 11010000100, 11010010000, 11010011100, 1100011101011];

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/CODE128/index.js":
/*!**************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/CODE128/index.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CODE128C = exports.CODE128B = exports.CODE128A = exports.CODE128 = undefined;

var _CODE128_AUTO = __webpack_require__(/*! ./CODE128_AUTO.js */ "./node_modules/jsbarcode/bin/barcodes/CODE128/CODE128_AUTO.js");

var _CODE128_AUTO2 = _interopRequireDefault(_CODE128_AUTO);

var _CODE128A = __webpack_require__(/*! ./CODE128A.js */ "./node_modules/jsbarcode/bin/barcodes/CODE128/CODE128A.js");

var _CODE128A2 = _interopRequireDefault(_CODE128A);

var _CODE128B = __webpack_require__(/*! ./CODE128B.js */ "./node_modules/jsbarcode/bin/barcodes/CODE128/CODE128B.js");

var _CODE128B2 = _interopRequireDefault(_CODE128B);

var _CODE128C = __webpack_require__(/*! ./CODE128C.js */ "./node_modules/jsbarcode/bin/barcodes/CODE128/CODE128C.js");

var _CODE128C2 = _interopRequireDefault(_CODE128C);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.CODE128 = _CODE128_AUTO2.default;
exports.CODE128A = _CODE128A2.default;
exports.CODE128B = _CODE128B2.default;
exports.CODE128C = _CODE128C2.default;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/CODE39/index.js":
/*!*************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/CODE39/index.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.CODE39 = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Barcode2 = __webpack_require__(/*! ../Barcode.js */ "./node_modules/jsbarcode/bin/barcodes/Barcode.js");

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // Encoding documentation:
// https://en.wikipedia.org/wiki/Code_39#Encoding

var CODE39 = function (_Barcode) {
	_inherits(CODE39, _Barcode);

	function CODE39(data, options) {
		_classCallCheck(this, CODE39);

		data = data.toUpperCase();

		// Calculate mod43 checksum if enabled
		if (options.mod43) {
			data += getCharacter(mod43checksum(data));
		}

		return _possibleConstructorReturn(this, (CODE39.__proto__ || Object.getPrototypeOf(CODE39)).call(this, data, options));
	}

	_createClass(CODE39, [{
		key: "encode",
		value: function encode() {
			// First character is always a *
			var result = getEncoding("*");

			// Take every character and add the binary representation to the result
			for (var i = 0; i < this.data.length; i++) {
				result += getEncoding(this.data[i]) + "0";
			}

			// Last character is always a *
			result += getEncoding("*");

			return {
				data: result,
				text: this.text
			};
		}
	}, {
		key: "valid",
		value: function valid() {
			return this.data.search(/^[0-9A-Z\-\.\ \$\/\+\%]+$/) !== -1;
		}
	}]);

	return CODE39;
}(_Barcode3.default);

// All characters. The position in the array is the (checksum) value


var characters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "-", ".", " ", "$", "/", "+", "%", "*"];

// The decimal representation of the characters, is converted to the
// corresponding binary with the getEncoding function
var encodings = [20957, 29783, 23639, 30485, 20951, 29813, 23669, 20855, 29789, 23645, 29975, 23831, 30533, 22295, 30149, 24005, 21623, 29981, 23837, 22301, 30023, 23879, 30545, 22343, 30161, 24017, 21959, 30065, 23921, 22385, 29015, 18263, 29141, 17879, 29045, 18293, 17783, 29021, 18269, 17477, 17489, 17681, 20753, 35770];

// Get the binary representation of a character by converting the encodings
// from decimal to binary
function getEncoding(character) {
	return getBinary(characterValue(character));
}

function getBinary(characterValue) {
	return encodings[characterValue].toString(2);
}

function getCharacter(characterValue) {
	return characters[characterValue];
}

function characterValue(character) {
	return characters.indexOf(character);
}

function mod43checksum(data) {
	var checksum = 0;
	for (var i = 0; i < data.length; i++) {
		checksum += characterValue(data[i]);
	}

	checksum = checksum % 43;
	return checksum;
}

exports.CODE39 = CODE39;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/EAN.js":
/*!************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/EAN_UPC/EAN.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _constants = __webpack_require__(/*! ./constants */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/constants.js");

var _encoder = __webpack_require__(/*! ./encoder */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/encoder.js");

var _encoder2 = _interopRequireDefault(_encoder);

var _Barcode2 = __webpack_require__(/*! ../Barcode */ "./node_modules/jsbarcode/bin/barcodes/Barcode.js");

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// Base class for EAN8 & EAN13
var EAN = function (_Barcode) {
	_inherits(EAN, _Barcode);

	function EAN(data, options) {
		_classCallCheck(this, EAN);

		// Make sure the font is not bigger than the space between the guard bars
		var _this = _possibleConstructorReturn(this, (EAN.__proto__ || Object.getPrototypeOf(EAN)).call(this, data, options));

		_this.fontSize = !options.flat && options.fontSize > options.width * 10 ? options.width * 10 : options.fontSize;

		// Make the guard bars go down half the way of the text
		_this.guardHeight = options.height + _this.fontSize / 2 + options.textMargin;
		return _this;
	}

	_createClass(EAN, [{
		key: 'encode',
		value: function encode() {
			return this.options.flat ? this.encodeFlat() : this.encodeGuarded();
		}
	}, {
		key: 'leftText',
		value: function leftText(from, to) {
			return this.text.substr(from, to);
		}
	}, {
		key: 'leftEncode',
		value: function leftEncode(data, structure) {
			return (0, _encoder2.default)(data, structure);
		}
	}, {
		key: 'rightText',
		value: function rightText(from, to) {
			return this.text.substr(from, to);
		}
	}, {
		key: 'rightEncode',
		value: function rightEncode(data, structure) {
			return (0, _encoder2.default)(data, structure);
		}
	}, {
		key: 'encodeGuarded',
		value: function encodeGuarded() {
			var textOptions = { fontSize: this.fontSize };
			var guardOptions = { height: this.guardHeight };

			return [{ data: _constants.SIDE_BIN, options: guardOptions }, { data: this.leftEncode(), text: this.leftText(), options: textOptions }, { data: _constants.MIDDLE_BIN, options: guardOptions }, { data: this.rightEncode(), text: this.rightText(), options: textOptions }, { data: _constants.SIDE_BIN, options: guardOptions }];
		}
	}, {
		key: 'encodeFlat',
		value: function encodeFlat() {
			var data = [_constants.SIDE_BIN, this.leftEncode(), _constants.MIDDLE_BIN, this.rightEncode(), _constants.SIDE_BIN];

			return {
				data: data.join(''),
				text: this.text
			};
		}
	}]);

	return EAN;
}(_Barcode3.default);

exports.default = EAN;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/EAN13.js":
/*!**************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/EAN_UPC/EAN13.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _constants = __webpack_require__(/*! ./constants */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/constants.js");

var _EAN2 = __webpack_require__(/*! ./EAN */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/EAN.js");

var _EAN3 = _interopRequireDefault(_EAN2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // Encoding documentation:
// https://en.wikipedia.org/wiki/International_Article_Number_(EAN)#Binary_encoding_of_data_digits_into_EAN-13_barcode

// Calculate the checksum digit
// https://en.wikipedia.org/wiki/International_Article_Number_(EAN)#Calculation_of_checksum_digit
var checksum = function checksum(number) {
	var res = number.substr(0, 12).split('').map(function (n) {
		return +n;
	}).reduce(function (sum, a, idx) {
		return idx % 2 ? sum + a * 3 : sum + a;
	}, 0);

	return (10 - res % 10) % 10;
};

var EAN13 = function (_EAN) {
	_inherits(EAN13, _EAN);

	function EAN13(data, options) {
		_classCallCheck(this, EAN13);

		// Add checksum if it does not exist
		if (data.search(/^[0-9]{12}$/) !== -1) {
			data += checksum(data);
		}

		// Adds a last character to the end of the barcode
		var _this = _possibleConstructorReturn(this, (EAN13.__proto__ || Object.getPrototypeOf(EAN13)).call(this, data, options));

		_this.lastChar = options.lastChar;
		return _this;
	}

	_createClass(EAN13, [{
		key: 'valid',
		value: function valid() {
			return this.data.search(/^[0-9]{13}$/) !== -1 && +this.data[12] === checksum(this.data);
		}
	}, {
		key: 'leftText',
		value: function leftText() {
			return _get(EAN13.prototype.__proto__ || Object.getPrototypeOf(EAN13.prototype), 'leftText', this).call(this, 1, 6);
		}
	}, {
		key: 'leftEncode',
		value: function leftEncode() {
			var data = this.data.substr(1, 6);
			var structure = _constants.EAN13_STRUCTURE[this.data[0]];
			return _get(EAN13.prototype.__proto__ || Object.getPrototypeOf(EAN13.prototype), 'leftEncode', this).call(this, data, structure);
		}
	}, {
		key: 'rightText',
		value: function rightText() {
			return _get(EAN13.prototype.__proto__ || Object.getPrototypeOf(EAN13.prototype), 'rightText', this).call(this, 7, 6);
		}
	}, {
		key: 'rightEncode',
		value: function rightEncode() {
			var data = this.data.substr(7, 6);
			return _get(EAN13.prototype.__proto__ || Object.getPrototypeOf(EAN13.prototype), 'rightEncode', this).call(this, data, 'RRRRRR');
		}

		// The "standard" way of printing EAN13 barcodes with guard bars

	}, {
		key: 'encodeGuarded',
		value: function encodeGuarded() {
			var data = _get(EAN13.prototype.__proto__ || Object.getPrototypeOf(EAN13.prototype), 'encodeGuarded', this).call(this);

			// Extend data with left digit & last character
			if (this.options.displayValue) {
				data.unshift({
					data: '000000000000',
					text: this.text.substr(0, 1),
					options: { textAlign: 'left', fontSize: this.fontSize }
				});

				if (this.options.lastChar) {
					data.push({
						data: '00'
					});
					data.push({
						data: '00000',
						text: this.options.lastChar,
						options: { fontSize: this.fontSize }
					});
				}
			}

			return data;
		}
	}]);

	return EAN13;
}(_EAN3.default);

exports.default = EAN13;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/EAN2.js":
/*!*************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/EAN_UPC/EAN2.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _constants = __webpack_require__(/*! ./constants */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/constants.js");

var _encoder = __webpack_require__(/*! ./encoder */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/encoder.js");

var _encoder2 = _interopRequireDefault(_encoder);

var _Barcode2 = __webpack_require__(/*! ../Barcode */ "./node_modules/jsbarcode/bin/barcodes/Barcode.js");

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // Encoding documentation:
// https://en.wikipedia.org/wiki/EAN_2#Encoding

var EAN2 = function (_Barcode) {
	_inherits(EAN2, _Barcode);

	function EAN2(data, options) {
		_classCallCheck(this, EAN2);

		return _possibleConstructorReturn(this, (EAN2.__proto__ || Object.getPrototypeOf(EAN2)).call(this, data, options));
	}

	_createClass(EAN2, [{
		key: 'valid',
		value: function valid() {
			return this.data.search(/^[0-9]{2}$/) !== -1;
		}
	}, {
		key: 'encode',
		value: function encode() {
			// Choose the structure based on the number mod 4
			var structure = _constants.EAN2_STRUCTURE[parseInt(this.data) % 4];
			return {
				// Start bits + Encode the two digits with 01 in between
				data: '1011' + (0, _encoder2.default)(this.data, structure, '01'),
				text: this.text
			};
		}
	}]);

	return EAN2;
}(_Barcode3.default);

exports.default = EAN2;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/EAN5.js":
/*!*************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/EAN_UPC/EAN5.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _constants = __webpack_require__(/*! ./constants */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/constants.js");

var _encoder = __webpack_require__(/*! ./encoder */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/encoder.js");

var _encoder2 = _interopRequireDefault(_encoder);

var _Barcode2 = __webpack_require__(/*! ../Barcode */ "./node_modules/jsbarcode/bin/barcodes/Barcode.js");

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // Encoding documentation:
// https://en.wikipedia.org/wiki/EAN_5#Encoding

var checksum = function checksum(data) {
	var result = data.split('').map(function (n) {
		return +n;
	}).reduce(function (sum, a, idx) {
		return idx % 2 ? sum + a * 9 : sum + a * 3;
	}, 0);
	return result % 10;
};

var EAN5 = function (_Barcode) {
	_inherits(EAN5, _Barcode);

	function EAN5(data, options) {
		_classCallCheck(this, EAN5);

		return _possibleConstructorReturn(this, (EAN5.__proto__ || Object.getPrototypeOf(EAN5)).call(this, data, options));
	}

	_createClass(EAN5, [{
		key: 'valid',
		value: function valid() {
			return this.data.search(/^[0-9]{5}$/) !== -1;
		}
	}, {
		key: 'encode',
		value: function encode() {
			var structure = _constants.EAN5_STRUCTURE[checksum(this.data)];
			return {
				data: '1011' + (0, _encoder2.default)(this.data, structure, '01'),
				text: this.text
			};
		}
	}]);

	return EAN5;
}(_Barcode3.default);

exports.default = EAN5;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/EAN8.js":
/*!*************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/EAN_UPC/EAN8.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _EAN2 = __webpack_require__(/*! ./EAN */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/EAN.js");

var _EAN3 = _interopRequireDefault(_EAN2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // Encoding documentation:
// http://www.barcodeisland.com/ean8.phtml

// Calculate the checksum digit
var checksum = function checksum(number) {
	var res = number.substr(0, 7).split('').map(function (n) {
		return +n;
	}).reduce(function (sum, a, idx) {
		return idx % 2 ? sum + a : sum + a * 3;
	}, 0);

	return (10 - res % 10) % 10;
};

var EAN8 = function (_EAN) {
	_inherits(EAN8, _EAN);

	function EAN8(data, options) {
		_classCallCheck(this, EAN8);

		// Add checksum if it does not exist
		if (data.search(/^[0-9]{7}$/) !== -1) {
			data += checksum(data);
		}

		return _possibleConstructorReturn(this, (EAN8.__proto__ || Object.getPrototypeOf(EAN8)).call(this, data, options));
	}

	_createClass(EAN8, [{
		key: 'valid',
		value: function valid() {
			return this.data.search(/^[0-9]{8}$/) !== -1 && +this.data[7] === checksum(this.data);
		}
	}, {
		key: 'leftText',
		value: function leftText() {
			return _get(EAN8.prototype.__proto__ || Object.getPrototypeOf(EAN8.prototype), 'leftText', this).call(this, 0, 4);
		}
	}, {
		key: 'leftEncode',
		value: function leftEncode() {
			var data = this.data.substr(0, 4);
			return _get(EAN8.prototype.__proto__ || Object.getPrototypeOf(EAN8.prototype), 'leftEncode', this).call(this, data, 'LLLL');
		}
	}, {
		key: 'rightText',
		value: function rightText() {
			return _get(EAN8.prototype.__proto__ || Object.getPrototypeOf(EAN8.prototype), 'rightText', this).call(this, 4, 4);
		}
	}, {
		key: 'rightEncode',
		value: function rightEncode() {
			var data = this.data.substr(4, 4);
			return _get(EAN8.prototype.__proto__ || Object.getPrototypeOf(EAN8.prototype), 'rightEncode', this).call(this, data, 'RRRR');
		}
	}]);

	return EAN8;
}(_EAN3.default);

exports.default = EAN8;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/UPC.js":
/*!************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/EAN_UPC/UPC.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

exports.checksum = checksum;

var _encoder = __webpack_require__(/*! ./encoder */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/encoder.js");

var _encoder2 = _interopRequireDefault(_encoder);

var _Barcode2 = __webpack_require__(/*! ../Barcode.js */ "./node_modules/jsbarcode/bin/barcodes/Barcode.js");

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // Encoding documentation:
// https://en.wikipedia.org/wiki/Universal_Product_Code#Encoding

var UPC = function (_Barcode) {
	_inherits(UPC, _Barcode);

	function UPC(data, options) {
		_classCallCheck(this, UPC);

		// Add checksum if it does not exist
		if (data.search(/^[0-9]{11}$/) !== -1) {
			data += checksum(data);
		}

		var _this = _possibleConstructorReturn(this, (UPC.__proto__ || Object.getPrototypeOf(UPC)).call(this, data, options));

		_this.displayValue = options.displayValue;

		// Make sure the font is not bigger than the space between the guard bars
		if (options.fontSize > options.width * 10) {
			_this.fontSize = options.width * 10;
		} else {
			_this.fontSize = options.fontSize;
		}

		// Make the guard bars go down half the way of the text
		_this.guardHeight = options.height + _this.fontSize / 2 + options.textMargin;
		return _this;
	}

	_createClass(UPC, [{
		key: "valid",
		value: function valid() {
			return this.data.search(/^[0-9]{12}$/) !== -1 && this.data[11] == checksum(this.data);
		}
	}, {
		key: "encode",
		value: function encode() {
			if (this.options.flat) {
				return this.flatEncoding();
			} else {
				return this.guardedEncoding();
			}
		}
	}, {
		key: "flatEncoding",
		value: function flatEncoding() {
			var result = "";

			result += "101";
			result += (0, _encoder2.default)(this.data.substr(0, 6), "LLLLLL");
			result += "01010";
			result += (0, _encoder2.default)(this.data.substr(6, 6), "RRRRRR");
			result += "101";

			return {
				data: result,
				text: this.text
			};
		}
	}, {
		key: "guardedEncoding",
		value: function guardedEncoding() {
			var result = [];

			// Add the first digit
			if (this.displayValue) {
				result.push({
					data: "00000000",
					text: this.text.substr(0, 1),
					options: { textAlign: "left", fontSize: this.fontSize }
				});
			}

			// Add the guard bars
			result.push({
				data: "101" + (0, _encoder2.default)(this.data[0], "L"),
				options: { height: this.guardHeight }
			});

			// Add the left side
			result.push({
				data: (0, _encoder2.default)(this.data.substr(1, 5), "LLLLL"),
				text: this.text.substr(1, 5),
				options: { fontSize: this.fontSize }
			});

			// Add the middle bits
			result.push({
				data: "01010",
				options: { height: this.guardHeight }
			});

			// Add the right side
			result.push({
				data: (0, _encoder2.default)(this.data.substr(6, 5), "RRRRR"),
				text: this.text.substr(6, 5),
				options: { fontSize: this.fontSize }
			});

			// Add the end bits
			result.push({
				data: (0, _encoder2.default)(this.data[11], "R") + "101",
				options: { height: this.guardHeight }
			});

			// Add the last digit
			if (this.displayValue) {
				result.push({
					data: "00000000",
					text: this.text.substr(11, 1),
					options: { textAlign: "right", fontSize: this.fontSize }
				});
			}

			return result;
		}
	}]);

	return UPC;
}(_Barcode3.default);

// Calulate the checksum digit
// https://en.wikipedia.org/wiki/International_Article_Number_(EAN)#Calculation_of_checksum_digit


function checksum(number) {
	var result = 0;

	var i;
	for (i = 1; i < 11; i += 2) {
		result += parseInt(number[i]);
	}
	for (i = 0; i < 11; i += 2) {
		result += parseInt(number[i]) * 3;
	}

	return (10 - result % 10) % 10;
}

exports.default = UPC;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/UPCE.js":
/*!*************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/EAN_UPC/UPCE.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _encoder = __webpack_require__(/*! ./encoder */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/encoder.js");

var _encoder2 = _interopRequireDefault(_encoder);

var _Barcode2 = __webpack_require__(/*! ../Barcode.js */ "./node_modules/jsbarcode/bin/barcodes/Barcode.js");

var _Barcode3 = _interopRequireDefault(_Barcode2);

var _UPC = __webpack_require__(/*! ./UPC.js */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/UPC.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // Encoding documentation:
// https://en.wikipedia.org/wiki/Universal_Product_Code#Encoding
//
// UPC-E documentation:
// https://en.wikipedia.org/wiki/Universal_Product_Code#UPC-E

var EXPANSIONS = ["XX00000XXX", "XX10000XXX", "XX20000XXX", "XXX00000XX", "XXXX00000X", "XXXXX00005", "XXXXX00006", "XXXXX00007", "XXXXX00008", "XXXXX00009"];

var PARITIES = [["EEEOOO", "OOOEEE"], ["EEOEOO", "OOEOEE"], ["EEOOEO", "OOEEOE"], ["EEOOOE", "OOEEEO"], ["EOEEOO", "OEOOEE"], ["EOOEEO", "OEEOOE"], ["EOOOEE", "OEEEOO"], ["EOEOEO", "OEOEOE"], ["EOEOOE", "OEOEEO"], ["EOOEOE", "OEEOEO"]];

var UPCE = function (_Barcode) {
	_inherits(UPCE, _Barcode);

	function UPCE(data, options) {
		_classCallCheck(this, UPCE);

		var _this = _possibleConstructorReturn(this, (UPCE.__proto__ || Object.getPrototypeOf(UPCE)).call(this, data, options));
		// Code may be 6 or 8 digits;
		// A 7 digit code is ambiguous as to whether the extra digit
		// is a UPC-A check or number system digit.


		_this.isValid = false;
		if (data.search(/^[0-9]{6}$/) !== -1) {
			_this.middleDigits = data;
			_this.upcA = expandToUPCA(data, "0");
			_this.text = options.text || '' + _this.upcA[0] + data + _this.upcA[_this.upcA.length - 1];
			_this.isValid = true;
		} else if (data.search(/^[01][0-9]{7}$/) !== -1) {
			_this.middleDigits = data.substring(1, data.length - 1);
			_this.upcA = expandToUPCA(_this.middleDigits, data[0]);

			if (_this.upcA[_this.upcA.length - 1] === data[data.length - 1]) {
				_this.isValid = true;
			} else {
				// checksum mismatch
				return _possibleConstructorReturn(_this);
			}
		} else {
			return _possibleConstructorReturn(_this);
		}

		_this.displayValue = options.displayValue;

		// Make sure the font is not bigger than the space between the guard bars
		if (options.fontSize > options.width * 10) {
			_this.fontSize = options.width * 10;
		} else {
			_this.fontSize = options.fontSize;
		}

		// Make the guard bars go down half the way of the text
		_this.guardHeight = options.height + _this.fontSize / 2 + options.textMargin;
		return _this;
	}

	_createClass(UPCE, [{
		key: 'valid',
		value: function valid() {
			return this.isValid;
		}
	}, {
		key: 'encode',
		value: function encode() {
			if (this.options.flat) {
				return this.flatEncoding();
			} else {
				return this.guardedEncoding();
			}
		}
	}, {
		key: 'flatEncoding',
		value: function flatEncoding() {
			var result = "";

			result += "101";
			result += this.encodeMiddleDigits();
			result += "010101";

			return {
				data: result,
				text: this.text
			};
		}
	}, {
		key: 'guardedEncoding',
		value: function guardedEncoding() {
			var result = [];

			// Add the UPC-A number system digit beneath the quiet zone
			if (this.displayValue) {
				result.push({
					data: "00000000",
					text: this.text[0],
					options: { textAlign: "left", fontSize: this.fontSize }
				});
			}

			// Add the guard bars
			result.push({
				data: "101",
				options: { height: this.guardHeight }
			});

			// Add the 6 UPC-E digits
			result.push({
				data: this.encodeMiddleDigits(),
				text: this.text.substring(1, 7),
				options: { fontSize: this.fontSize }
			});

			// Add the end bits
			result.push({
				data: "010101",
				options: { height: this.guardHeight }
			});

			// Add the UPC-A check digit beneath the quiet zone
			if (this.displayValue) {
				result.push({
					data: "00000000",
					text: this.text[7],
					options: { textAlign: "right", fontSize: this.fontSize }
				});
			}

			return result;
		}
	}, {
		key: 'encodeMiddleDigits',
		value: function encodeMiddleDigits() {
			var numberSystem = this.upcA[0];
			var checkDigit = this.upcA[this.upcA.length - 1];
			var parity = PARITIES[parseInt(checkDigit)][parseInt(numberSystem)];
			return (0, _encoder2.default)(this.middleDigits, parity);
		}
	}]);

	return UPCE;
}(_Barcode3.default);

function expandToUPCA(middleDigits, numberSystem) {
	var lastUpcE = parseInt(middleDigits[middleDigits.length - 1]);
	var expansion = EXPANSIONS[lastUpcE];

	var result = "";
	var digitIndex = 0;
	for (var i = 0; i < expansion.length; i++) {
		var c = expansion[i];
		if (c === 'X') {
			result += middleDigits[digitIndex++];
		} else {
			result += c;
		}
	}

	result = '' + numberSystem + result;
	return '' + result + (0, _UPC.checksum)(result);
}

exports.default = UPCE;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/constants.js":
/*!******************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/EAN_UPC/constants.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
// Standard start end and middle bits
var SIDE_BIN = exports.SIDE_BIN = '101';
var MIDDLE_BIN = exports.MIDDLE_BIN = '01010';

var BINARIES = exports.BINARIES = {
	'L': [// The L (left) type of encoding
	'0001101', '0011001', '0010011', '0111101', '0100011', '0110001', '0101111', '0111011', '0110111', '0001011'],
	'G': [// The G type of encoding
	'0100111', '0110011', '0011011', '0100001', '0011101', '0111001', '0000101', '0010001', '0001001', '0010111'],
	'R': [// The R (right) type of encoding
	'1110010', '1100110', '1101100', '1000010', '1011100', '1001110', '1010000', '1000100', '1001000', '1110100'],
	'O': [// The O (odd) encoding for UPC-E
	'0001101', '0011001', '0010011', '0111101', '0100011', '0110001', '0101111', '0111011', '0110111', '0001011'],
	'E': [// The E (even) encoding for UPC-E
	'0100111', '0110011', '0011011', '0100001', '0011101', '0111001', '0000101', '0010001', '0001001', '0010111']
};

// Define the EAN-2 structure
var EAN2_STRUCTURE = exports.EAN2_STRUCTURE = ['LL', 'LG', 'GL', 'GG'];

// Define the EAN-5 structure
var EAN5_STRUCTURE = exports.EAN5_STRUCTURE = ['GGLLL', 'GLGLL', 'GLLGL', 'GLLLG', 'LGGLL', 'LLGGL', 'LLLGG', 'LGLGL', 'LGLLG', 'LLGLG'];

// Define the EAN-13 structure
var EAN13_STRUCTURE = exports.EAN13_STRUCTURE = ['LLLLLL', 'LLGLGG', 'LLGGLG', 'LLGGGL', 'LGLLGG', 'LGGLLG', 'LGGGLL', 'LGLGLG', 'LGLGGL', 'LGGLGL'];

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/encoder.js":
/*!****************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/EAN_UPC/encoder.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _constants = __webpack_require__(/*! ./constants */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/constants.js");

// Encode data string
var encode = function encode(data, structure, separator) {
	var encoded = data.split('').map(function (val, idx) {
		return _constants.BINARIES[structure[idx]];
	}).map(function (val, idx) {
		return val ? val[data[idx]] : '';
	});

	if (separator) {
		var last = data.length - 1;
		encoded = encoded.map(function (val, idx) {
			return idx < last ? val + separator : val;
		});
	}

	return encoded.join('');
};

exports.default = encode;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/index.js":
/*!**************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/EAN_UPC/index.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UPCE = exports.UPC = exports.EAN2 = exports.EAN5 = exports.EAN8 = exports.EAN13 = undefined;

var _EAN = __webpack_require__(/*! ./EAN13.js */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/EAN13.js");

var _EAN2 = _interopRequireDefault(_EAN);

var _EAN3 = __webpack_require__(/*! ./EAN8.js */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/EAN8.js");

var _EAN4 = _interopRequireDefault(_EAN3);

var _EAN5 = __webpack_require__(/*! ./EAN5.js */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/EAN5.js");

var _EAN6 = _interopRequireDefault(_EAN5);

var _EAN7 = __webpack_require__(/*! ./EAN2.js */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/EAN2.js");

var _EAN8 = _interopRequireDefault(_EAN7);

var _UPC = __webpack_require__(/*! ./UPC.js */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/UPC.js");

var _UPC2 = _interopRequireDefault(_UPC);

var _UPCE = __webpack_require__(/*! ./UPCE.js */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/UPCE.js");

var _UPCE2 = _interopRequireDefault(_UPCE);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.EAN13 = _EAN2.default;
exports.EAN8 = _EAN4.default;
exports.EAN5 = _EAN6.default;
exports.EAN2 = _EAN8.default;
exports.UPC = _UPC2.default;
exports.UPCE = _UPCE2.default;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/GenericBarcode/index.js":
/*!*********************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/GenericBarcode/index.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.GenericBarcode = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Barcode2 = __webpack_require__(/*! ../Barcode.js */ "./node_modules/jsbarcode/bin/barcodes/Barcode.js");

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var GenericBarcode = function (_Barcode) {
	_inherits(GenericBarcode, _Barcode);

	function GenericBarcode(data, options) {
		_classCallCheck(this, GenericBarcode);

		return _possibleConstructorReturn(this, (GenericBarcode.__proto__ || Object.getPrototypeOf(GenericBarcode)).call(this, data, options)); // Sets this.data and this.text
	}

	// Return the corresponding binary numbers for the data provided


	_createClass(GenericBarcode, [{
		key: "encode",
		value: function encode() {
			return {
				data: "10101010101010101010101010101010101010101",
				text: this.text
			};
		}

		// Resturn true/false if the string provided is valid for this encoder

	}, {
		key: "valid",
		value: function valid() {
			return true;
		}
	}]);

	return GenericBarcode;
}(_Barcode3.default);

exports.GenericBarcode = GenericBarcode;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/ITF/ITF.js":
/*!********************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/ITF/ITF.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _constants = __webpack_require__(/*! ./constants */ "./node_modules/jsbarcode/bin/barcodes/ITF/constants.js");

var _Barcode2 = __webpack_require__(/*! ../Barcode */ "./node_modules/jsbarcode/bin/barcodes/Barcode.js");

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ITF = function (_Barcode) {
	_inherits(ITF, _Barcode);

	function ITF() {
		_classCallCheck(this, ITF);

		return _possibleConstructorReturn(this, (ITF.__proto__ || Object.getPrototypeOf(ITF)).apply(this, arguments));
	}

	_createClass(ITF, [{
		key: 'valid',
		value: function valid() {
			return this.data.search(/^([0-9]{2})+$/) !== -1;
		}
	}, {
		key: 'encode',
		value: function encode() {
			var _this2 = this;

			// Calculate all the digit pairs
			var encoded = this.data.match(/.{2}/g).map(function (pair) {
				return _this2.encodePair(pair);
			}).join('');

			return {
				data: _constants.START_BIN + encoded + _constants.END_BIN,
				text: this.text
			};
		}

		// Calculate the data of a number pair

	}, {
		key: 'encodePair',
		value: function encodePair(pair) {
			var second = _constants.BINARIES[pair[1]];

			return _constants.BINARIES[pair[0]].split('').map(function (first, idx) {
				return (first === '1' ? '111' : '1') + (second[idx] === '1' ? '000' : '0');
			}).join('');
		}
	}]);

	return ITF;
}(_Barcode3.default);

exports.default = ITF;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/ITF/ITF14.js":
/*!**********************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/ITF/ITF14.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _ITF2 = __webpack_require__(/*! ./ITF */ "./node_modules/jsbarcode/bin/barcodes/ITF/ITF.js");

var _ITF3 = _interopRequireDefault(_ITF2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// Calculate the checksum digit
var checksum = function checksum(data) {
	var res = data.substr(0, 13).split('').map(function (num) {
		return parseInt(num, 10);
	}).reduce(function (sum, n, idx) {
		return sum + n * (3 - idx % 2 * 2);
	}, 0);

	return Math.ceil(res / 10) * 10 - res;
};

var ITF14 = function (_ITF) {
	_inherits(ITF14, _ITF);

	function ITF14(data, options) {
		_classCallCheck(this, ITF14);

		// Add checksum if it does not exist
		if (data.search(/^[0-9]{13}$/) !== -1) {
			data += checksum(data);
		}
		return _possibleConstructorReturn(this, (ITF14.__proto__ || Object.getPrototypeOf(ITF14)).call(this, data, options));
	}

	_createClass(ITF14, [{
		key: 'valid',
		value: function valid() {
			return this.data.search(/^[0-9]{14}$/) !== -1 && +this.data[13] === checksum(this.data);
		}
	}]);

	return ITF14;
}(_ITF3.default);

exports.default = ITF14;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/ITF/constants.js":
/*!**************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/ITF/constants.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
var START_BIN = exports.START_BIN = '1010';
var END_BIN = exports.END_BIN = '11101';

var BINARIES = exports.BINARIES = ['00110', '10001', '01001', '11000', '00101', '10100', '01100', '00011', '10010', '01010'];

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/ITF/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/ITF/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ITF14 = exports.ITF = undefined;

var _ITF = __webpack_require__(/*! ./ITF */ "./node_modules/jsbarcode/bin/barcodes/ITF/ITF.js");

var _ITF2 = _interopRequireDefault(_ITF);

var _ITF3 = __webpack_require__(/*! ./ITF14 */ "./node_modules/jsbarcode/bin/barcodes/ITF/ITF14.js");

var _ITF4 = _interopRequireDefault(_ITF3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.ITF = _ITF2.default;
exports.ITF14 = _ITF4.default;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/MSI/MSI.js":
/*!********************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/MSI/MSI.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Barcode2 = __webpack_require__(/*! ../Barcode.js */ "./node_modules/jsbarcode/bin/barcodes/Barcode.js");

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // Encoding documentation
// https://en.wikipedia.org/wiki/MSI_Barcode#Character_set_and_binary_lookup

var MSI = function (_Barcode) {
	_inherits(MSI, _Barcode);

	function MSI(data, options) {
		_classCallCheck(this, MSI);

		return _possibleConstructorReturn(this, (MSI.__proto__ || Object.getPrototypeOf(MSI)).call(this, data, options));
	}

	_createClass(MSI, [{
		key: "encode",
		value: function encode() {
			// Start bits
			var ret = "110";

			for (var i = 0; i < this.data.length; i++) {
				// Convert the character to binary (always 4 binary digits)
				var digit = parseInt(this.data[i]);
				var bin = digit.toString(2);
				bin = addZeroes(bin, 4 - bin.length);

				// Add 100 for every zero and 110 for every 1
				for (var b = 0; b < bin.length; b++) {
					ret += bin[b] == "0" ? "100" : "110";
				}
			}

			// End bits
			ret += "1001";

			return {
				data: ret,
				text: this.text
			};
		}
	}, {
		key: "valid",
		value: function valid() {
			return this.data.search(/^[0-9]+$/) !== -1;
		}
	}]);

	return MSI;
}(_Barcode3.default);

function addZeroes(number, n) {
	for (var i = 0; i < n; i++) {
		number = "0" + number;
	}
	return number;
}

exports.default = MSI;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/MSI/MSI10.js":
/*!**********************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/MSI/MSI10.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _MSI2 = __webpack_require__(/*! ./MSI.js */ "./node_modules/jsbarcode/bin/barcodes/MSI/MSI.js");

var _MSI3 = _interopRequireDefault(_MSI2);

var _checksums = __webpack_require__(/*! ./checksums.js */ "./node_modules/jsbarcode/bin/barcodes/MSI/checksums.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MSI10 = function (_MSI) {
	_inherits(MSI10, _MSI);

	function MSI10(data, options) {
		_classCallCheck(this, MSI10);

		return _possibleConstructorReturn(this, (MSI10.__proto__ || Object.getPrototypeOf(MSI10)).call(this, data + (0, _checksums.mod10)(data), options));
	}

	return MSI10;
}(_MSI3.default);

exports.default = MSI10;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/MSI/MSI1010.js":
/*!************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/MSI/MSI1010.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _MSI2 = __webpack_require__(/*! ./MSI.js */ "./node_modules/jsbarcode/bin/barcodes/MSI/MSI.js");

var _MSI3 = _interopRequireDefault(_MSI2);

var _checksums = __webpack_require__(/*! ./checksums.js */ "./node_modules/jsbarcode/bin/barcodes/MSI/checksums.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MSI1010 = function (_MSI) {
	_inherits(MSI1010, _MSI);

	function MSI1010(data, options) {
		_classCallCheck(this, MSI1010);

		data += (0, _checksums.mod10)(data);
		data += (0, _checksums.mod10)(data);
		return _possibleConstructorReturn(this, (MSI1010.__proto__ || Object.getPrototypeOf(MSI1010)).call(this, data, options));
	}

	return MSI1010;
}(_MSI3.default);

exports.default = MSI1010;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/MSI/MSI11.js":
/*!**********************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/MSI/MSI11.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _MSI2 = __webpack_require__(/*! ./MSI.js */ "./node_modules/jsbarcode/bin/barcodes/MSI/MSI.js");

var _MSI3 = _interopRequireDefault(_MSI2);

var _checksums = __webpack_require__(/*! ./checksums.js */ "./node_modules/jsbarcode/bin/barcodes/MSI/checksums.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MSI11 = function (_MSI) {
	_inherits(MSI11, _MSI);

	function MSI11(data, options) {
		_classCallCheck(this, MSI11);

		return _possibleConstructorReturn(this, (MSI11.__proto__ || Object.getPrototypeOf(MSI11)).call(this, data + (0, _checksums.mod11)(data), options));
	}

	return MSI11;
}(_MSI3.default);

exports.default = MSI11;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/MSI/MSI1110.js":
/*!************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/MSI/MSI1110.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _MSI2 = __webpack_require__(/*! ./MSI.js */ "./node_modules/jsbarcode/bin/barcodes/MSI/MSI.js");

var _MSI3 = _interopRequireDefault(_MSI2);

var _checksums = __webpack_require__(/*! ./checksums.js */ "./node_modules/jsbarcode/bin/barcodes/MSI/checksums.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MSI1110 = function (_MSI) {
	_inherits(MSI1110, _MSI);

	function MSI1110(data, options) {
		_classCallCheck(this, MSI1110);

		data += (0, _checksums.mod11)(data);
		data += (0, _checksums.mod10)(data);
		return _possibleConstructorReturn(this, (MSI1110.__proto__ || Object.getPrototypeOf(MSI1110)).call(this, data, options));
	}

	return MSI1110;
}(_MSI3.default);

exports.default = MSI1110;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/MSI/checksums.js":
/*!**************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/MSI/checksums.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.mod10 = mod10;
exports.mod11 = mod11;
function mod10(number) {
	var sum = 0;
	for (var i = 0; i < number.length; i++) {
		var n = parseInt(number[i]);
		if ((i + number.length) % 2 === 0) {
			sum += n;
		} else {
			sum += n * 2 % 10 + Math.floor(n * 2 / 10);
		}
	}
	return (10 - sum % 10) % 10;
}

function mod11(number) {
	var sum = 0;
	var weights = [2, 3, 4, 5, 6, 7];
	for (var i = 0; i < number.length; i++) {
		var n = parseInt(number[number.length - 1 - i]);
		sum += weights[i % weights.length] * n;
	}
	return (11 - sum % 11) % 11;
}

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/MSI/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/MSI/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MSI1110 = exports.MSI1010 = exports.MSI11 = exports.MSI10 = exports.MSI = undefined;

var _MSI = __webpack_require__(/*! ./MSI.js */ "./node_modules/jsbarcode/bin/barcodes/MSI/MSI.js");

var _MSI2 = _interopRequireDefault(_MSI);

var _MSI3 = __webpack_require__(/*! ./MSI10.js */ "./node_modules/jsbarcode/bin/barcodes/MSI/MSI10.js");

var _MSI4 = _interopRequireDefault(_MSI3);

var _MSI5 = __webpack_require__(/*! ./MSI11.js */ "./node_modules/jsbarcode/bin/barcodes/MSI/MSI11.js");

var _MSI6 = _interopRequireDefault(_MSI5);

var _MSI7 = __webpack_require__(/*! ./MSI1010.js */ "./node_modules/jsbarcode/bin/barcodes/MSI/MSI1010.js");

var _MSI8 = _interopRequireDefault(_MSI7);

var _MSI9 = __webpack_require__(/*! ./MSI1110.js */ "./node_modules/jsbarcode/bin/barcodes/MSI/MSI1110.js");

var _MSI10 = _interopRequireDefault(_MSI9);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.MSI = _MSI2.default;
exports.MSI10 = _MSI4.default;
exports.MSI11 = _MSI6.default;
exports.MSI1010 = _MSI8.default;
exports.MSI1110 = _MSI10.default;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/codabar/index.js":
/*!**************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/codabar/index.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.codabar = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Barcode2 = __webpack_require__(/*! ../Barcode.js */ "./node_modules/jsbarcode/bin/barcodes/Barcode.js");

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // Encoding specification:
// http://www.barcodeisland.com/codabar.phtml

var codabar = function (_Barcode) {
	_inherits(codabar, _Barcode);

	function codabar(data, options) {
		_classCallCheck(this, codabar);

		if (data.search(/^[0-9\-\$\:\.\+\/]+$/) === 0) {
			data = "A" + data + "A";
		}

		var _this = _possibleConstructorReturn(this, (codabar.__proto__ || Object.getPrototypeOf(codabar)).call(this, data.toUpperCase(), options));

		_this.text = _this.options.text || _this.text.replace(/[A-D]/g, '');
		return _this;
	}

	_createClass(codabar, [{
		key: "valid",
		value: function valid() {
			return this.data.search(/^[A-D][0-9\-\$\:\.\+\/]+[A-D]$/) !== -1;
		}
	}, {
		key: "encode",
		value: function encode() {
			var result = [];
			var encodings = this.getEncodings();
			for (var i = 0; i < this.data.length; i++) {
				result.push(encodings[this.data.charAt(i)]);
				// for all characters except the last, append a narrow-space ("0")
				if (i !== this.data.length - 1) {
					result.push("0");
				}
			}
			return {
				text: this.text,
				data: result.join('')
			};
		}
	}, {
		key: "getEncodings",
		value: function getEncodings() {
			return {
				"0": "101010011",
				"1": "101011001",
				"2": "101001011",
				"3": "110010101",
				"4": "101101001",
				"5": "110101001",
				"6": "100101011",
				"7": "100101101",
				"8": "100110101",
				"9": "110100101",
				"-": "101001101",
				"$": "101100101",
				":": "1101011011",
				"/": "1101101011",
				".": "1101101101",
				"+": "101100110011",
				"A": "1011001001",
				"B": "1001001011",
				"C": "1010010011",
				"D": "1010011001"
			};
		}
	}]);

	return codabar;
}(_Barcode3.default);

exports.codabar = codabar;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/index.js":
/*!******************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/index.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _CODE = __webpack_require__(/*! ./CODE39/ */ "./node_modules/jsbarcode/bin/barcodes/CODE39/index.js");

var _CODE2 = __webpack_require__(/*! ./CODE128/ */ "./node_modules/jsbarcode/bin/barcodes/CODE128/index.js");

var _EAN_UPC = __webpack_require__(/*! ./EAN_UPC/ */ "./node_modules/jsbarcode/bin/barcodes/EAN_UPC/index.js");

var _ITF = __webpack_require__(/*! ./ITF/ */ "./node_modules/jsbarcode/bin/barcodes/ITF/index.js");

var _MSI = __webpack_require__(/*! ./MSI/ */ "./node_modules/jsbarcode/bin/barcodes/MSI/index.js");

var _pharmacode = __webpack_require__(/*! ./pharmacode/ */ "./node_modules/jsbarcode/bin/barcodes/pharmacode/index.js");

var _codabar = __webpack_require__(/*! ./codabar */ "./node_modules/jsbarcode/bin/barcodes/codabar/index.js");

var _GenericBarcode = __webpack_require__(/*! ./GenericBarcode/ */ "./node_modules/jsbarcode/bin/barcodes/GenericBarcode/index.js");

exports.default = {
	CODE39: _CODE.CODE39,
	CODE128: _CODE2.CODE128, CODE128A: _CODE2.CODE128A, CODE128B: _CODE2.CODE128B, CODE128C: _CODE2.CODE128C,
	EAN13: _EAN_UPC.EAN13, EAN8: _EAN_UPC.EAN8, EAN5: _EAN_UPC.EAN5, EAN2: _EAN_UPC.EAN2, UPC: _EAN_UPC.UPC, UPCE: _EAN_UPC.UPCE,
	ITF14: _ITF.ITF14,
	ITF: _ITF.ITF,
	MSI: _MSI.MSI, MSI10: _MSI.MSI10, MSI11: _MSI.MSI11, MSI1010: _MSI.MSI1010, MSI1110: _MSI.MSI1110,
	pharmacode: _pharmacode.pharmacode,
	codabar: _codabar.codabar,
	GenericBarcode: _GenericBarcode.GenericBarcode
};

/***/ }),

/***/ "./node_modules/jsbarcode/bin/barcodes/pharmacode/index.js":
/*!*****************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/barcodes/pharmacode/index.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.pharmacode = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Barcode2 = __webpack_require__(/*! ../Barcode.js */ "./node_modules/jsbarcode/bin/barcodes/Barcode.js");

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // Encoding documentation
// http://www.gomaro.ch/ftproot/Laetus_PHARMA-CODE.pdf

var pharmacode = function (_Barcode) {
	_inherits(pharmacode, _Barcode);

	function pharmacode(data, options) {
		_classCallCheck(this, pharmacode);

		var _this = _possibleConstructorReturn(this, (pharmacode.__proto__ || Object.getPrototypeOf(pharmacode)).call(this, data, options));

		_this.number = parseInt(data, 10);
		return _this;
	}

	_createClass(pharmacode, [{
		key: "encode",
		value: function encode() {
			var z = this.number;
			var result = "";

			// http://i.imgur.com/RMm4UDJ.png
			// (source: http://www.gomaro.ch/ftproot/Laetus_PHARMA-CODE.pdf, page: 34)
			while (!isNaN(z) && z != 0) {
				if (z % 2 === 0) {
					// Even
					result = "11100" + result;
					z = (z - 2) / 2;
				} else {
					// Odd
					result = "100" + result;
					z = (z - 1) / 2;
				}
			}

			// Remove the two last zeroes
			result = result.slice(0, -2);

			return {
				data: result,
				text: this.text
			};
		}
	}, {
		key: "valid",
		value: function valid() {
			return this.number >= 3 && this.number <= 131070;
		}
	}]);

	return pharmacode;
}(_Barcode3.default);

exports.pharmacode = pharmacode;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/exceptions/ErrorHandler.js":
/*!***************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/exceptions/ErrorHandler.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/*eslint no-console: 0 */

var ErrorHandler = function () {
	function ErrorHandler(api) {
		_classCallCheck(this, ErrorHandler);

		this.api = api;
	}

	_createClass(ErrorHandler, [{
		key: "handleCatch",
		value: function handleCatch(e) {
			// If babel supported extending of Error in a correct way instanceof would be used here
			if (e.name === "InvalidInputException") {
				if (this.api._options.valid !== this.api._defaults.valid) {
					this.api._options.valid(false);
				} else {
					throw e.message;
				}
			} else {
				throw e;
			}

			this.api.render = function () {};
		}
	}, {
		key: "wrapBarcodeCall",
		value: function wrapBarcodeCall(func) {
			try {
				var result = func.apply(undefined, arguments);
				this.api._options.valid(true);
				return result;
			} catch (e) {
				this.handleCatch(e);

				return this.api;
			}
		}
	}]);

	return ErrorHandler;
}();

exports.default = ErrorHandler;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/exceptions/exceptions.js":
/*!*************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/exceptions/exceptions.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var InvalidInputException = function (_Error) {
	_inherits(InvalidInputException, _Error);

	function InvalidInputException(symbology, input) {
		_classCallCheck(this, InvalidInputException);

		var _this = _possibleConstructorReturn(this, (InvalidInputException.__proto__ || Object.getPrototypeOf(InvalidInputException)).call(this));

		_this.name = "InvalidInputException";

		_this.symbology = symbology;
		_this.input = input;

		_this.message = '"' + _this.input + '" is not a valid input for ' + _this.symbology;
		return _this;
	}

	return InvalidInputException;
}(Error);

var InvalidElementException = function (_Error2) {
	_inherits(InvalidElementException, _Error2);

	function InvalidElementException() {
		_classCallCheck(this, InvalidElementException);

		var _this2 = _possibleConstructorReturn(this, (InvalidElementException.__proto__ || Object.getPrototypeOf(InvalidElementException)).call(this));

		_this2.name = "InvalidElementException";
		_this2.message = "Not supported type to render on";
		return _this2;
	}

	return InvalidElementException;
}(Error);

var NoElementException = function (_Error3) {
	_inherits(NoElementException, _Error3);

	function NoElementException() {
		_classCallCheck(this, NoElementException);

		var _this3 = _possibleConstructorReturn(this, (NoElementException.__proto__ || Object.getPrototypeOf(NoElementException)).call(this));

		_this3.name = "NoElementException";
		_this3.message = "No element to render on.";
		return _this3;
	}

	return NoElementException;
}(Error);

exports.InvalidInputException = InvalidInputException;
exports.InvalidElementException = InvalidElementException;
exports.NoElementException = NoElementException;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/help/fixOptions.js":
/*!*******************************************************!*\
  !*** ./node_modules/jsbarcode/bin/help/fixOptions.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = fixOptions;


function fixOptions(options) {
	// Fix the margins
	options.marginTop = options.marginTop || options.margin;
	options.marginBottom = options.marginBottom || options.margin;
	options.marginRight = options.marginRight || options.margin;
	options.marginLeft = options.marginLeft || options.margin;

	return options;
}

/***/ }),

/***/ "./node_modules/jsbarcode/bin/help/getOptionsFromElement.js":
/*!******************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/help/getOptionsFromElement.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _optionsFromStrings = __webpack_require__(/*! ./optionsFromStrings.js */ "./node_modules/jsbarcode/bin/help/optionsFromStrings.js");

var _optionsFromStrings2 = _interopRequireDefault(_optionsFromStrings);

var _defaults = __webpack_require__(/*! ../options/defaults.js */ "./node_modules/jsbarcode/bin/options/defaults.js");

var _defaults2 = _interopRequireDefault(_defaults);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getOptionsFromElement(element) {
	var options = {};
	for (var property in _defaults2.default) {
		if (_defaults2.default.hasOwnProperty(property)) {
			// jsbarcode-*
			if (element.hasAttribute("jsbarcode-" + property.toLowerCase())) {
				options[property] = element.getAttribute("jsbarcode-" + property.toLowerCase());
			}

			// data-*
			if (element.hasAttribute("data-" + property.toLowerCase())) {
				options[property] = element.getAttribute("data-" + property.toLowerCase());
			}
		}
	}

	options["value"] = element.getAttribute("jsbarcode-value") || element.getAttribute("data-value");

	// Since all atributes are string they need to be converted to integers
	options = (0, _optionsFromStrings2.default)(options);

	return options;
}

exports.default = getOptionsFromElement;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/help/getRenderProperties.js":
/*!****************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/help/getRenderProperties.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; /* global HTMLImageElement */
/* global HTMLCanvasElement */
/* global SVGElement */

var _getOptionsFromElement = __webpack_require__(/*! ./getOptionsFromElement.js */ "./node_modules/jsbarcode/bin/help/getOptionsFromElement.js");

var _getOptionsFromElement2 = _interopRequireDefault(_getOptionsFromElement);

var _renderers = __webpack_require__(/*! ../renderers */ "./node_modules/jsbarcode/bin/renderers/index.js");

var _renderers2 = _interopRequireDefault(_renderers);

var _exceptions = __webpack_require__(/*! ../exceptions/exceptions.js */ "./node_modules/jsbarcode/bin/exceptions/exceptions.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Takes an element and returns an object with information about how
// it should be rendered
// This could also return an array with these objects
// {
//   element: The element that the renderer should draw on
//   renderer: The name of the renderer
//   afterRender (optional): If something has to done after the renderer
//     completed, calls afterRender (function)
//   options (optional): Options that can be defined in the element
// }

function getRenderProperties(element) {
	// If the element is a string, query select call again
	if (typeof element === "string") {
		return querySelectedRenderProperties(element);
	}
	// If element is array. Recursivly call with every object in the array
	else if (Array.isArray(element)) {
			var returnArray = [];
			for (var i = 0; i < element.length; i++) {
				returnArray.push(getRenderProperties(element[i]));
			}
			return returnArray;
		}
		// If element, render on canvas and set the uri as src
		else if (typeof HTMLCanvasElement !== 'undefined' && element instanceof HTMLImageElement) {
				return newCanvasRenderProperties(element);
			}
			// If SVG
			else if (element && element.nodeName === 'svg' || typeof SVGElement !== 'undefined' && element instanceof SVGElement) {
					return {
						element: element,
						options: (0, _getOptionsFromElement2.default)(element),
						renderer: _renderers2.default.SVGRenderer
					};
				}
				// If canvas (in browser)
				else if (typeof HTMLCanvasElement !== 'undefined' && element instanceof HTMLCanvasElement) {
						return {
							element: element,
							options: (0, _getOptionsFromElement2.default)(element),
							renderer: _renderers2.default.CanvasRenderer
						};
					}
					// If canvas (in node)
					else if (element && element.getContext) {
							return {
								element: element,
								renderer: _renderers2.default.CanvasRenderer
							};
						} else if (element && (typeof element === "undefined" ? "undefined" : _typeof(element)) === 'object' && !element.nodeName) {
							return {
								element: element,
								renderer: _renderers2.default.ObjectRenderer
							};
						} else {
							throw new _exceptions.InvalidElementException();
						}
}

function querySelectedRenderProperties(string) {
	var selector = document.querySelectorAll(string);
	if (selector.length === 0) {
		return undefined;
	} else {
		var returnArray = [];
		for (var i = 0; i < selector.length; i++) {
			returnArray.push(getRenderProperties(selector[i]));
		}
		return returnArray;
	}
}

function newCanvasRenderProperties(imgElement) {
	var canvas = document.createElement('canvas');
	return {
		element: canvas,
		options: (0, _getOptionsFromElement2.default)(imgElement),
		renderer: _renderers2.default.CanvasRenderer,
		afterRender: function afterRender() {
			imgElement.setAttribute("src", canvas.toDataURL());
		}
	};
}

exports.default = getRenderProperties;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/help/linearizeEncodings.js":
/*!***************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/help/linearizeEncodings.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = linearizeEncodings;

// Encodings can be nestled like [[1-1, 1-2], 2, [3-1, 3-2]
// Convert to [1-1, 1-2, 2, 3-1, 3-2]

function linearizeEncodings(encodings) {
	var linearEncodings = [];
	function nextLevel(encoded) {
		if (Array.isArray(encoded)) {
			for (var i = 0; i < encoded.length; i++) {
				nextLevel(encoded[i]);
			}
		} else {
			encoded.text = encoded.text || "";
			encoded.data = encoded.data || "";
			linearEncodings.push(encoded);
		}
	}
	nextLevel(encodings);

	return linearEncodings;
}

/***/ }),

/***/ "./node_modules/jsbarcode/bin/help/merge.js":
/*!**************************************************!*\
  !*** ./node_modules/jsbarcode/bin/help/merge.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.default = function (old, replaceObj) {
  return _extends({}, old, replaceObj);
};

/***/ }),

/***/ "./node_modules/jsbarcode/bin/help/optionsFromStrings.js":
/*!***************************************************************!*\
  !*** ./node_modules/jsbarcode/bin/help/optionsFromStrings.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = optionsFromStrings;

// Convert string to integers/booleans where it should be

function optionsFromStrings(options) {
	var intOptions = ["width", "height", "textMargin", "fontSize", "margin", "marginTop", "marginBottom", "marginLeft", "marginRight"];

	for (var intOption in intOptions) {
		if (intOptions.hasOwnProperty(intOption)) {
			intOption = intOptions[intOption];
			if (typeof options[intOption] === "string") {
				options[intOption] = parseInt(options[intOption], 10);
			}
		}
	}

	if (typeof options["displayValue"] === "string") {
		options["displayValue"] = options["displayValue"] != "false";
	}

	return options;
}

/***/ }),

/***/ "./node_modules/jsbarcode/bin/options/defaults.js":
/*!********************************************************!*\
  !*** ./node_modules/jsbarcode/bin/options/defaults.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
var defaults = {
	width: 2,
	height: 100,
	format: "auto",
	displayValue: true,
	fontOptions: "",
	font: "monospace",
	text: undefined,
	textAlign: "center",
	textPosition: "bottom",
	textMargin: 2,
	fontSize: 20,
	background: "#ffffff",
	lineColor: "#000000",
	margin: 10,
	marginTop: undefined,
	marginBottom: undefined,
	marginLeft: undefined,
	marginRight: undefined,
	valid: function valid() {}
};

exports.default = defaults;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/renderers/canvas.js":
/*!********************************************************!*\
  !*** ./node_modules/jsbarcode/bin/renderers/canvas.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _merge = __webpack_require__(/*! ../help/merge.js */ "./node_modules/jsbarcode/bin/help/merge.js");

var _merge2 = _interopRequireDefault(_merge);

var _shared = __webpack_require__(/*! ./shared.js */ "./node_modules/jsbarcode/bin/renderers/shared.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CanvasRenderer = function () {
	function CanvasRenderer(canvas, encodings, options) {
		_classCallCheck(this, CanvasRenderer);

		this.canvas = canvas;
		this.encodings = encodings;
		this.options = options;
	}

	_createClass(CanvasRenderer, [{
		key: "render",
		value: function render() {
			// Abort if the browser does not support HTML5 canvas
			if (!this.canvas.getContext) {
				throw new Error('The browser does not support canvas.');
			}

			this.prepareCanvas();
			for (var i = 0; i < this.encodings.length; i++) {
				var encodingOptions = (0, _merge2.default)(this.options, this.encodings[i].options);

				this.drawCanvasBarcode(encodingOptions, this.encodings[i]);
				this.drawCanvasText(encodingOptions, this.encodings[i]);

				this.moveCanvasDrawing(this.encodings[i]);
			}

			this.restoreCanvas();
		}
	}, {
		key: "prepareCanvas",
		value: function prepareCanvas() {
			// Get the canvas context
			var ctx = this.canvas.getContext("2d");

			ctx.save();

			(0, _shared.calculateEncodingAttributes)(this.encodings, this.options, ctx);
			var totalWidth = (0, _shared.getTotalWidthOfEncodings)(this.encodings);
			var maxHeight = (0, _shared.getMaximumHeightOfEncodings)(this.encodings);

			this.canvas.width = totalWidth + this.options.marginLeft + this.options.marginRight;

			this.canvas.height = maxHeight;

			// Paint the canvas
			ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
			if (this.options.background) {
				ctx.fillStyle = this.options.background;
				ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
			}

			ctx.translate(this.options.marginLeft, 0);
		}
	}, {
		key: "drawCanvasBarcode",
		value: function drawCanvasBarcode(options, encoding) {
			// Get the canvas context
			var ctx = this.canvas.getContext("2d");

			var binary = encoding.data;

			// Creates the barcode out of the encoded binary
			var yFrom;
			if (options.textPosition == "top") {
				yFrom = options.marginTop + options.fontSize + options.textMargin;
			} else {
				yFrom = options.marginTop;
			}

			ctx.fillStyle = options.lineColor;

			for (var b = 0; b < binary.length; b++) {
				var x = b * options.width + encoding.barcodePadding;

				if (binary[b] === "1") {
					ctx.fillRect(x, yFrom, options.width, options.height);
				} else if (binary[b]) {
					ctx.fillRect(x, yFrom, options.width, options.height * binary[b]);
				}
			}
		}
	}, {
		key: "drawCanvasText",
		value: function drawCanvasText(options, encoding) {
			// Get the canvas context
			var ctx = this.canvas.getContext("2d");

			var font = options.fontOptions + " " + options.fontSize + "px " + options.font;

			// Draw the text if displayValue is set
			if (options.displayValue) {
				var x, y;

				if (options.textPosition == "top") {
					y = options.marginTop + options.fontSize - options.textMargin;
				} else {
					y = options.height + options.textMargin + options.marginTop + options.fontSize;
				}

				ctx.font = font;

				// Draw the text in the correct X depending on the textAlign option
				if (options.textAlign == "left" || encoding.barcodePadding > 0) {
					x = 0;
					ctx.textAlign = 'left';
				} else if (options.textAlign == "right") {
					x = encoding.width - 1;
					ctx.textAlign = 'right';
				}
				// In all other cases, center the text
				else {
						x = encoding.width / 2;
						ctx.textAlign = 'center';
					}

				ctx.fillText(encoding.text, x, y);
			}
		}
	}, {
		key: "moveCanvasDrawing",
		value: function moveCanvasDrawing(encoding) {
			var ctx = this.canvas.getContext("2d");

			ctx.translate(encoding.width, 0);
		}
	}, {
		key: "restoreCanvas",
		value: function restoreCanvas() {
			// Get the canvas context
			var ctx = this.canvas.getContext("2d");

			ctx.restore();
		}
	}]);

	return CanvasRenderer;
}();

exports.default = CanvasRenderer;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/renderers/index.js":
/*!*******************************************************!*\
  !*** ./node_modules/jsbarcode/bin/renderers/index.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _canvas = __webpack_require__(/*! ./canvas.js */ "./node_modules/jsbarcode/bin/renderers/canvas.js");

var _canvas2 = _interopRequireDefault(_canvas);

var _svg = __webpack_require__(/*! ./svg.js */ "./node_modules/jsbarcode/bin/renderers/svg.js");

var _svg2 = _interopRequireDefault(_svg);

var _object = __webpack_require__(/*! ./object.js */ "./node_modules/jsbarcode/bin/renderers/object.js");

var _object2 = _interopRequireDefault(_object);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = { CanvasRenderer: _canvas2.default, SVGRenderer: _svg2.default, ObjectRenderer: _object2.default };

/***/ }),

/***/ "./node_modules/jsbarcode/bin/renderers/object.js":
/*!********************************************************!*\
  !*** ./node_modules/jsbarcode/bin/renderers/object.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ObjectRenderer = function () {
	function ObjectRenderer(object, encodings, options) {
		_classCallCheck(this, ObjectRenderer);

		this.object = object;
		this.encodings = encodings;
		this.options = options;
	}

	_createClass(ObjectRenderer, [{
		key: "render",
		value: function render() {
			this.object.encodings = this.encodings;
		}
	}]);

	return ObjectRenderer;
}();

exports.default = ObjectRenderer;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/renderers/shared.js":
/*!********************************************************!*\
  !*** ./node_modules/jsbarcode/bin/renderers/shared.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.getTotalWidthOfEncodings = exports.calculateEncodingAttributes = exports.getBarcodePadding = exports.getEncodingHeight = exports.getMaximumHeightOfEncodings = undefined;

var _merge = __webpack_require__(/*! ../help/merge.js */ "./node_modules/jsbarcode/bin/help/merge.js");

var _merge2 = _interopRequireDefault(_merge);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getEncodingHeight(encoding, options) {
	return options.height + (options.displayValue && encoding.text.length > 0 ? options.fontSize + options.textMargin : 0) + options.marginTop + options.marginBottom;
}

function getBarcodePadding(textWidth, barcodeWidth, options) {
	if (options.displayValue && barcodeWidth < textWidth) {
		if (options.textAlign == "center") {
			return Math.floor((textWidth - barcodeWidth) / 2);
		} else if (options.textAlign == "left") {
			return 0;
		} else if (options.textAlign == "right") {
			return Math.floor(textWidth - barcodeWidth);
		}
	}
	return 0;
}

function calculateEncodingAttributes(encodings, barcodeOptions, context) {
	for (var i = 0; i < encodings.length; i++) {
		var encoding = encodings[i];
		var options = (0, _merge2.default)(barcodeOptions, encoding.options);

		// Calculate the width of the encoding
		var textWidth;
		if (options.displayValue) {
			textWidth = messureText(encoding.text, options, context);
		} else {
			textWidth = 0;
		}

		var barcodeWidth = encoding.data.length * options.width;
		encoding.width = Math.ceil(Math.max(textWidth, barcodeWidth));

		encoding.height = getEncodingHeight(encoding, options);

		encoding.barcodePadding = getBarcodePadding(textWidth, barcodeWidth, options);
	}
}

function getTotalWidthOfEncodings(encodings) {
	var totalWidth = 0;
	for (var i = 0; i < encodings.length; i++) {
		totalWidth += encodings[i].width;
	}
	return totalWidth;
}

function getMaximumHeightOfEncodings(encodings) {
	var maxHeight = 0;
	for (var i = 0; i < encodings.length; i++) {
		if (encodings[i].height > maxHeight) {
			maxHeight = encodings[i].height;
		}
	}
	return maxHeight;
}

function messureText(string, options, context) {
	var ctx;

	if (context) {
		ctx = context;
	} else if (typeof document !== "undefined") {
		ctx = document.createElement("canvas").getContext("2d");
	} else {
		// If the text cannot be messured we will return 0.
		// This will make some barcode with big text render incorrectly
		return 0;
	}
	ctx.font = options.fontOptions + " " + options.fontSize + "px " + options.font;

	// Calculate the width of the encoding
	var size = ctx.measureText(string).width;

	return size;
}

exports.getMaximumHeightOfEncodings = getMaximumHeightOfEncodings;
exports.getEncodingHeight = getEncodingHeight;
exports.getBarcodePadding = getBarcodePadding;
exports.calculateEncodingAttributes = calculateEncodingAttributes;
exports.getTotalWidthOfEncodings = getTotalWidthOfEncodings;

/***/ }),

/***/ "./node_modules/jsbarcode/bin/renderers/svg.js":
/*!*****************************************************!*\
  !*** ./node_modules/jsbarcode/bin/renderers/svg.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _merge = __webpack_require__(/*! ../help/merge.js */ "./node_modules/jsbarcode/bin/help/merge.js");

var _merge2 = _interopRequireDefault(_merge);

var _shared = __webpack_require__(/*! ./shared.js */ "./node_modules/jsbarcode/bin/renderers/shared.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var svgns = "http://www.w3.org/2000/svg";

var SVGRenderer = function () {
	function SVGRenderer(svg, encodings, options) {
		_classCallCheck(this, SVGRenderer);

		this.svg = svg;
		this.encodings = encodings;
		this.options = options;
		this.document = options.xmlDocument || document;
	}

	_createClass(SVGRenderer, [{
		key: "render",
		value: function render() {
			var currentX = this.options.marginLeft;

			this.prepareSVG();
			for (var i = 0; i < this.encodings.length; i++) {
				var encoding = this.encodings[i];
				var encodingOptions = (0, _merge2.default)(this.options, encoding.options);

				var group = this.createGroup(currentX, encodingOptions.marginTop, this.svg);

				this.setGroupOptions(group, encodingOptions);

				this.drawSvgBarcode(group, encodingOptions, encoding);
				this.drawSVGText(group, encodingOptions, encoding);

				currentX += encoding.width;
			}
		}
	}, {
		key: "prepareSVG",
		value: function prepareSVG() {
			// Clear the SVG
			while (this.svg.firstChild) {
				this.svg.removeChild(this.svg.firstChild);
			}

			(0, _shared.calculateEncodingAttributes)(this.encodings, this.options);
			var totalWidth = (0, _shared.getTotalWidthOfEncodings)(this.encodings);
			var maxHeight = (0, _shared.getMaximumHeightOfEncodings)(this.encodings);

			var width = totalWidth + this.options.marginLeft + this.options.marginRight;
			this.setSvgAttributes(width, maxHeight);

			if (this.options.background) {
				this.drawRect(0, 0, width, maxHeight, this.svg).setAttribute("style", "fill:" + this.options.background + ";");
			}
		}
	}, {
		key: "drawSvgBarcode",
		value: function drawSvgBarcode(parent, options, encoding) {
			var binary = encoding.data;

			// Creates the barcode out of the encoded binary
			var yFrom;
			if (options.textPosition == "top") {
				yFrom = options.fontSize + options.textMargin;
			} else {
				yFrom = 0;
			}

			var barWidth = 0;
			var x = 0;
			for (var b = 0; b < binary.length; b++) {
				x = b * options.width + encoding.barcodePadding;

				if (binary[b] === "1") {
					barWidth++;
				} else if (barWidth > 0) {
					this.drawRect(x - options.width * barWidth, yFrom, options.width * barWidth, options.height, parent);
					barWidth = 0;
				}
			}

			// Last draw is needed since the barcode ends with 1
			if (barWidth > 0) {
				this.drawRect(x - options.width * (barWidth - 1), yFrom, options.width * barWidth, options.height, parent);
			}
		}
	}, {
		key: "drawSVGText",
		value: function drawSVGText(parent, options, encoding) {
			var textElem = this.document.createElementNS(svgns, 'text');

			// Draw the text if displayValue is set
			if (options.displayValue) {
				var x, y;

				textElem.setAttribute("style", "font:" + options.fontOptions + " " + options.fontSize + "px " + options.font);

				if (options.textPosition == "top") {
					y = options.fontSize - options.textMargin;
				} else {
					y = options.height + options.textMargin + options.fontSize;
				}

				// Draw the text in the correct X depending on the textAlign option
				if (options.textAlign == "left" || encoding.barcodePadding > 0) {
					x = 0;
					textElem.setAttribute("text-anchor", "start");
				} else if (options.textAlign == "right") {
					x = encoding.width - 1;
					textElem.setAttribute("text-anchor", "end");
				}
				// In all other cases, center the text
				else {
						x = encoding.width / 2;
						textElem.setAttribute("text-anchor", "middle");
					}

				textElem.setAttribute("x", x);
				textElem.setAttribute("y", y);

				textElem.appendChild(this.document.createTextNode(encoding.text));

				parent.appendChild(textElem);
			}
		}
	}, {
		key: "setSvgAttributes",
		value: function setSvgAttributes(width, height) {
			var svg = this.svg;
			svg.setAttribute("width", width + "px");
			svg.setAttribute("height", height + "px");
			svg.setAttribute("x", "0px");
			svg.setAttribute("y", "0px");
			svg.setAttribute("viewBox", "0 0 " + width + " " + height);

			svg.setAttribute("xmlns", svgns);
			svg.setAttribute("version", "1.1");

			svg.setAttribute("style", "transform: translate(0,0)");
		}
	}, {
		key: "createGroup",
		value: function createGroup(x, y, parent) {
			var group = this.document.createElementNS(svgns, 'g');
			group.setAttribute("transform", "translate(" + x + ", " + y + ")");

			parent.appendChild(group);

			return group;
		}
	}, {
		key: "setGroupOptions",
		value: function setGroupOptions(group, options) {
			group.setAttribute("style", "fill:" + options.lineColor + ";");
		}
	}, {
		key: "drawRect",
		value: function drawRect(x, y, width, height, parent) {
			var rect = this.document.createElementNS(svgns, 'rect');

			rect.setAttribute("x", x);
			rect.setAttribute("y", y);
			rect.setAttribute("width", width);
			rect.setAttribute("height", height);

			parent.appendChild(rect);

			return rect;
		}
	}]);

	return SVGRenderer;
}();

exports.default = SVGRenderer;

/***/ }),

/***/ "./node_modules/ngx-barcode/index.js":
/*!*******************************************!*\
  !*** ./node_modules/ngx-barcode/index.js ***!
  \*******************************************/
/*! exports provided: NgxBarcodeModule, NgxBarcodeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxBarcodeModule", function() { return NgxBarcodeModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxBarcodeComponent", function() { return NgxBarcodeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var jsbarcode = __webpack_require__(/*! jsbarcode */ "./node_modules/jsbarcode/bin/JsBarcode.js");
var NgxBarcodeComponent = (function () {
    /**
     * @param {?} renderer
     */
    function NgxBarcodeComponent(renderer) {
        this.renderer = renderer;
        this.elementType = 'svg';
        this.cssClass = 'barcode'; // this should be done more elegantly
        this.format = 'CODE128';
        this.lineColor = '#000000';
        this.width = 2;
        this.height = 100;
        this.displayValue = false;
        this.fontOptions = '';
        this.font = 'monospace';
        this.textAlign = 'center';
        this.textPosition = 'bottom';
        this.textMargin = 2;
        this.fontSize = 20;
        this.background = '#ffffff';
        this.margin = 10;
        this.marginTop = 10;
        this.marginBottom = 10;
        this.marginLeft = 10;
        this.marginRight = 10;
        this.value = '';
        this.valid = function () { return true; };
    }
    Object.defineProperty(NgxBarcodeComponent.prototype, "options", {
        /**
         * @return {?}
         */
        get: function () {
            return {
                format: this.format,
                lineColor: this.lineColor,
                width: this.width,
                height: this.height,
                displayValue: this.displayValue,
                fontOptions: this.fontOptions,
                font: this.font,
                textAlign: this.textAlign,
                textPosition: this.textPosition,
                textMargin: this.textMargin,
                fontSize: this.fontSize,
                background: this.background,
                margin: this.margin,
                marginTop: this.marginTop,
                marginBottom: this.marginBottom,
                marginLeft: this.marginLeft,
                marginRight: this.marginRight,
                valid: this.valid,
            };
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    NgxBarcodeComponent.prototype.ngOnChanges = function () {
        this.createBarcode();
    };
    /**
     * @return {?}
     */
    NgxBarcodeComponent.prototype.createBarcode = function () {
        if (!this.value) {
            return;
        }
        
        var /** @type {?} */ element;
        switch (this.elementType) {
            case 'img':
                element = this.renderer.createElement('img');
                break;
            case 'canvas':
                element = this.renderer.createElement('canvas');
                break;
            case 'svg':
            default:
                element = this.renderer.createElement('svg', 'svg');
        }
        jsbarcode(element, this.value, this.options);
        for (var _i = 0, _a = this.bcElement.nativeElement.childNodes; _i < _a.length; _i++) {
            var node = _a[_i];
            this.renderer.removeChild(this.bcElement.nativeElement, node);
        }
        this.renderer.appendChild(this.bcElement.nativeElement, element);
    };
    NgxBarcodeComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                    selector: 'ngx-barcode',
                    template: "<div #bcElement [class]=\"cssClass\"></div>",
                    styles: []
                },] },
    ];
    /**
     * @nocollapse
     */
    NgxBarcodeComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"], },
    ]; };
    NgxBarcodeComponent.propDecorators = {
        'elementType': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-element-type',] },],
        'cssClass': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-class',] },],
        'format': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-format',] },],
        'lineColor': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-line-color',] },],
        'width': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-width',] },],
        'height': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-height',] },],
        'displayValue': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-display-value',] },],
        'fontOptions': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-font-options',] },],
        'font': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-font',] },],
        'textAlign': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-text-align',] },],
        'textPosition': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-text-position',] },],
        'textMargin': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-text-margin',] },],
        'fontSize': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-font-size',] },],
        'background': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-background',] },],
        'margin': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-margin',] },],
        'marginTop': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-margin-top',] },],
        'marginBottom': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-margin-bottom',] },],
        'marginLeft': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-margin-left',] },],
        'marginRight': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-margin-right',] },],
        'value': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-value',] },],
        'bcElement': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['bcElement',] },],
        'valid': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['bc-valid',] },],
    };
    return NgxBarcodeComponent;
}());

var NgxBarcodeModule = (function () {
    function NgxBarcodeModule() {
    }
    /**
     * @return {?}
     */
    NgxBarcodeModule.forRoot = function () {
        return {
            ngModule: NgxBarcodeModule,
            providers: []
        };
    };
    NgxBarcodeModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                    imports: [],
                    declarations: [
                        NgxBarcodeComponent,
                    ],
                    exports: [
                        NgxBarcodeComponent,
                    ]
                },] },
    ];
    /**
     * @nocollapse
     */
    NgxBarcodeModule.ctorParameters = function () { return []; };
    return NgxBarcodeModule;
}());




/***/ }),

/***/ "./src/app/base.service.ts":
/*!*********************************!*\
  !*** ./src/app/base.service.ts ***!
  \*********************************/
/*! exports provided: BaseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseService", function() { return BaseService; });
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _shareds_helpers_app_injector__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./shareds/helpers/app-injector */ "./src/app/shareds/helpers/app-injector.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var BaseService = /** @class */ (function () {
    function BaseService() {
        this.appConfig = _shareds_helpers_app_injector__WEBPACK_IMPORTED_MODULE_1__["AppInjector"].get(_configs_app_config__WEBPACK_IMPORTED_MODULE_0__["APP_CONFIG"]);
        this.http = _shareds_helpers_app_injector__WEBPACK_IMPORTED_MODULE_1__["AppInjector"].get(_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]);
    }
    return BaseService;
}());



/***/ }),

/***/ "./src/app/modules/warehouse/goods/deliver-suggestion/deliver-suggestion.component.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/deliver-suggestion/deliver-suggestion.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-suggestion\r\n    [class.receipt]=\"isReceipt\"\r\n    [loading]=\"isSearching\"\r\n    [sources]=\"listItems\"\r\n    [selectedItem]=\"selectedItem\"\r\n    [totalRows]=\"totalRows\"\r\n    [pageSize]=\"10\"\r\n    [allowAdd]=\"true\"\r\n    (itemSelected)=\"onItemSelected($event)\"\r\n    (itemRemoved)=\"itemRemoved.emit($event)\"\r\n    (searched)=\"onSearchKeyPress($event)\"\r\n    (nextPage)=\"onNextPage($event)\"\r\n></nh-suggestion>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/deliver-suggestion/deliver-suggestion.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/deliver-suggestion/deliver-suggestion.component.ts ***!
  \********************************************************************************************/
/*! exports provided: DeliverSuggestionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeliverSuggestionComponent", function() { return DeliverSuggestionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../shareds/components/nh-suggestion/nh-suggestion.component */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.component.ts");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _goods_receipt_note_goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../goods-receipt-note/goods-receipt-note.service */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.service.ts");






var DeliverSuggestionComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](DeliverSuggestionComponent, _super);
    function DeliverSuggestionComponent(goodsReceiptNoteService) {
        var _this = _super.call(this) || this;
        _this.goodsReceiptNoteService = goodsReceiptNoteService;
        _this.multiple = false;
        _this.isReceipt = false;
        _this.keyPressed = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.itemSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.itemRemoved = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        return _this;
    }
    DeliverSuggestionComponent.prototype.ngOnInit = function () {
    };
    DeliverSuggestionComponent.prototype.onItemSelected = function (item) {
        this.itemSelected.emit(item);
    };
    DeliverSuggestionComponent.prototype.onSearchKeyPress = function (keyword) {
        this.keyword = keyword;
        this.keyPressed.emit(keyword);
        this.search(1);
    };
    DeliverSuggestionComponent.prototype.search = function (currentPage, isLoadMore) {
        var _this = this;
        if (isLoadMore === void 0) { isLoadMore = false; }
        this.isSearching = true;
        this.currentPage = currentPage;
        this.goodsReceiptNoteService.deliverSuggestion(this.supplierId, this.keyword, this.currentPage, this.appConfig.PAGE_SIZE)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () { return _this.isSearching = false; }))
            .subscribe(function (result) {
            _this.totalRows = result.totalRows;
            var items = result.items.map(function (item) {
                return {
                    id: item.id,
                    name: item.name,
                    description: item.phoneNumber,
                    data: item
                };
            });
            if (isLoadMore) {
                _this.listItems = _this.listItems.concat(items);
            }
            else {
                _this.listItems = items;
            }
        });
    };
    DeliverSuggestionComponent.prototype.clear = function () {
        this.nhSuggestionComponent.clear();
    };
    DeliverSuggestionComponent.prototype.onNextPage = function (event) {
        this.keyword = event.keyword;
        this.pageSize = event.pageSize;
        this.search(event.page, true);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_3__["NhSuggestionComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_3__["NhSuggestionComponent"])
    ], DeliverSuggestionComponent.prototype, "nhSuggestionComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DeliverSuggestionComponent.prototype, "multiple", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DeliverSuggestionComponent.prototype, "isReceipt", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], DeliverSuggestionComponent.prototype, "supplierId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DeliverSuggestionComponent.prototype, "selectedItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DeliverSuggestionComponent.prototype, "keyPressed", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DeliverSuggestionComponent.prototype, "itemSelected", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DeliverSuggestionComponent.prototype, "itemRemoved", void 0);
    DeliverSuggestionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-deliver-suggestion',
            template: __webpack_require__(/*! ./deliver-suggestion.component.html */ "./src/app/modules/warehouse/goods/deliver-suggestion/deliver-suggestion.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_goods_receipt_note_goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_5__["GoodsReceiptNoteService"]])
    ], DeliverSuggestionComponent);
    return DeliverSuggestionComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_4__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-detail/goods-delivery-note-detail.component.html":
/*!**********************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-detail/goods-delivery-note-detail.component.html ***!
  \**********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #formGoodsDeliveryNote [size]=\"'full'\">\r\n    <form class=\"form-horizontal\">\r\n        <nh-modal-content class=\"form-body\">\r\n            <h3 class=\"form-section bold\" i18n=\"@@goodsDeliveryNoteInfo\">Thông tin phiếu xuất</h3>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Ngày xuất\" i18n-ghmLabel=\"@@deliveryDate\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\">\r\n                                {{ goodsDeliveryNoteDetail?.deliveryDate | dateTimeFormat:'DD/MM/YYYY HH:mm' }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Hình thức xuất\"\r\n                               i18n-ghmLabel=\"@@deliveryType\" [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                                <span class=\"badge cm-mgt-10\"\r\n                                      [class.badge-success]=\"goodsDeliveryNoteDetail?.type === deliveryType.retail\"\r\n                                      [class.badge-info]=\"goodsDeliveryNoteDetail?.type === deliveryType.selfConsumer\"\r\n                                      [class.badge-warning]=\"goodsDeliveryNoteDetail?.type === deliveryType.return\"\r\n                                      [class.badge-danger]=\"goodsDeliveryNoteDetail?.type === deliveryType.voided\"\r\n                                      [class.badge-defaut]=\"goodsDeliveryNoteDetail?.type === deliveryType.transfer\"\r\n                                      [class.badge-defaut]=\"goodsDeliveryNoteDetail?.type === deliveryType.inventory\"\r\n                                      i18n=\"@@goodsDeliveryNoteTypeValue\">\r\n                {goodsDeliveryNoteDetail?.type, select, 0 {Bán} 1 {Sử dụng} 2 {Trả nhà cung cấp} 3 {Hủy} 4 {Điều chuyển} 5 {Kiểm kê} other {}}\r\n            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\" *ngIf=\"goodsDeliveryNoteDetail?.type === deliveryType.selfConsumer\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Phòng ban\"\r\n                               i18n-ghmLabel=\"@@officeName\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\">\r\n                                {{ goodsDeliveryNoteDetail?.officeName }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Người nhận\" [required]=\"true\"\r\n                               i18n-ghmLabel=\"@@receiver\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\">{{ goodsDeliveryNoteDetail?.receiverFullName }}</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\" *ngIf=\"goodsDeliveryNoteDetail?.type === deliveryType.retail\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Người nhận\"\r\n                               i18n-ghmLabel=\"@@receiverFullName\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\">\r\n                                {{ goodsDeliveryNoteDetail?.receiverFullName }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Số điện thoại\"\r\n                               i18n-ghmLabel=\"@@receiverPhoneNumber\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\">\r\n                                {{ goodsDeliveryNoteDetail?.receiverPhoneNumber }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\" [class.has-error]=\"formErrors?.warehouseId\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Xuất tại kho\"\r\n                               i18n-ghmLabel=\"@@deliveringWarehouse\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\">\r\n                                {{ goodsDeliveryNoteDetail?.warehouseName }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\"\r\n                         [class.has-error]=\"formErrors.receptionWarehouseId\"\r\n                         *ngIf=\"goodsDeliveryNoteDetail?.type === deliveryType.transfer\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Nhập tại kho\"\r\n                               i18n-ghmLabel=\"@@receptionWarehouse\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\">\r\n                                {{ goodsDeliveryNoteDetail?.receptionWarehouseName }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\" *ngIf=\"goodsDeliveryNoteDetail?.type === deliveryType.transfer\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Người nhận\"\r\n                               i18n-ghmLabel=\"@@receiver\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\">{{ goodsDeliveryNoteDetail?.receiverFullName }}</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\" *ngIf=\"goodsDeliveryNoteDetail?.type === deliveryType.return\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Nhà cung cấp\"\r\n                               i18n-ghmLabel=\"@@supplier\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\">\r\n                                {{ goodsDeliveryNoteDetail?.supplierName }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Lý do\"\r\n                               i18n-ghmLabel=\"@@reason\"\r\n                               [required]=\"goodsDeliveryNoteDetail?.type === deliveryType.voided || goodsDeliveryNoteDetail?.type === deliveryType.return\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control height-auto\">\r\n                                {{ goodsDeliveryNoteDetail?.reason }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <h3 class=\"form-section bold\" i18n=\"@@goodsDeliveryNoteDetailInfo\">Chi tiết phiếu xuất</h3>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-12\">\r\n                    <app-goods-delivery-note-product [goodsDeliveryNoteId]=\"id\"\r\n                                                     [readonly]=\"true\"\r\n                                                     [warehouseId]=\"goodsDeliveryNoteDetail?.warehouseId\"\r\n                                                     [deliveryDate]=\"goodsDeliveryNoteDetail?.deliveryDate\"\r\n                                                     [totalAmounts]=\"goodsDeliveryNoteDetail?.totalAmounts\"\r\n                                                     [listGoodsDeliveryNoteDetail]=\"listGoodsDeliveryNoteDetail\"\r\n                                                     [deliveryType]=\"goodsDeliveryNoteDetail?.type\"\r\n                    ></app-goods-delivery-note-product>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-sm-12\">\r\n                <div class=\"form-group\">\r\n                    <label class=\"col-sm-2\" ghmLabel=\"Tổng số tiền (viết bằng chữ)\"\r\n                           i18n-ghmLabel=\"@@totalAmountsWords\"></label>\r\n                    <div class=\"bold col-sm-10\">\r\n                        <div class=\"dotted-form-control\">{{ goodsDeliveryNoteDetail?.totalAmounts | ghmAmountToWord }}\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <ghm-button classes=\"btn default\"\r\n                        nh-dismiss=\"true\"\r\n                        [type]=\"'button'\"\r\n                        [loading]=\"isSaving\">\r\n                <span i18n=\"@@close\">Đóng</span>\r\n            </ghm-button>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-detail/goods-delivery-note-detail.component.ts":
/*!********************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-detail/goods-delivery-note-detail.component.ts ***!
  \********************************************************************************************************************************/
/*! exports provided: GoodsDeliveryNoteDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsDeliveryNoteDetailComponent", function() { return GoodsDeliveryNoteDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _goods_delivery_note_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../goods-delivery-note.service */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.service.ts");
/* harmony import */ var _model_goods_delivery_note_details_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../model/goods-delivery-note-details.model */ "./src/app/modules/warehouse/goods/goods-delivery-note/model/goods-delivery-note-details.model.ts");
/* harmony import */ var _shareds_constants_deliveryType_const__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../shareds/constants/deliveryType.const */ "./src/app/shareds/constants/deliveryType.const.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _goods_delivery_note_type_const__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../goods-delivery-note-type.const */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-type.const.ts");









var GoodsDeliveryNoteDetailComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](GoodsDeliveryNoteDetailComponent, _super);
    function GoodsDeliveryNoteDetailComponent(goodsDeliveryNoteService) {
        var _this = _super.call(this) || this;
        _this.goodsDeliveryNoteService = goodsDeliveryNoteService;
        _this.deliveryType = _shareds_constants_deliveryType_const__WEBPACK_IMPORTED_MODULE_6__["DeliveryType"];
        _this.goodsDeliveryNoteType = _goods_delivery_note_type_const__WEBPACK_IMPORTED_MODULE_8__["GoodsDeliveryNoteType"];
        return _this;
    }
    GoodsDeliveryNoteDetailComponent.prototype.show = function (id) {
        this.isUpdate = false;
        this.listGoodsDeliveryNoteDetail = [];
        this.getDetail(id);
    };
    GoodsDeliveryNoteDetailComponent.prototype.getDetail = function (id) {
        var _this = this;
        setTimeout(function () {
            _this.formGoodsDeliveryNote.open();
        });
        this.goodsDeliveryNoteService.getDetail(id).subscribe(function (result) {
            var data = result.data;
            if (data) {
                _this.goodsDeliveryNoteDetail = data;
                _this.listGoodsDeliveryNoteDetail = _this.goodsDeliveryNoteDetail.goodsDeliveryNoteDetails
                    .map(function (goodsDeliveryNoteDetail) {
                    return new _model_goods_delivery_note_details_model__WEBPACK_IMPORTED_MODULE_5__["GoodsDeliveryNoteDetail"](goodsDeliveryNoteDetail.id, goodsDeliveryNoteDetail.code, goodsDeliveryNoteDetail.productId, goodsDeliveryNoteDetail.productName, goodsDeliveryNoteDetail.warehouseId, goodsDeliveryNoteDetail.warehouseName, goodsDeliveryNoteDetail.unitId, goodsDeliveryNoteDetail.unitName, goodsDeliveryNoteDetail.price, goodsDeliveryNoteDetail.quantity, goodsDeliveryNoteDetail.recommendedQuantity, goodsDeliveryNoteDetail.lotId, goodsDeliveryNoteDetail.inventoryQuantity, goodsDeliveryNoteDetail.goodsReceiptNoteDetailCode, goodsDeliveryNoteDetail.concurrencyStamp, goodsDeliveryNoteDetail.units);
                });
                _this.totalQuantity = lodash__WEBPACK_IMPORTED_MODULE_7__["sumBy"](_this.listGoodsDeliveryNoteDetail, function (item) {
                    return item.quantity;
                });
                _this.totalAmount = lodash__WEBPACK_IMPORTED_MODULE_7__["sumBy"](_this.listGoodsDeliveryNoteDetail, function (item) {
                    return item.totalAmounts;
                });
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('formGoodsDeliveryNote'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__["NhModalComponent"])
    ], GoodsDeliveryNoteDetailComponent.prototype, "formGoodsDeliveryNote", void 0);
    GoodsDeliveryNoteDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-goods-delivery-note-detail',
            template: __webpack_require__(/*! ././goods-delivery-note-detail.component.html */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-detail/goods-delivery-note-detail.component.html"),
            styles: [__webpack_require__(/*! ../goods-delivery-note.scss */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_goods_delivery_note_service__WEBPACK_IMPORTED_MODULE_4__["GoodsDeliveryNoteService"]])
    ], GoodsDeliveryNoteDetailComponent);
    return GoodsDeliveryNoteDetailComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-form/goods-delivery-note-form.component.html":
/*!******************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-form/goods-delivery-note-form.component.html ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #formGoodsDeliveryNote [size]=\"'full'\"\r\n          (shown)=\"onModalShow()\"\r\n          (hidden)=\"onModalHidden()\">\r\n    <form class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content class=\"form-body\">\r\n            <h3 class=\"form-section bold\" i18n=\"@@goodsDeliveryNoteInfo\">Thông tin phiếu xuất</h3>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Ngày xuất\" i18n-ghmLabel=\"@@deliveryDate\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <nh-date\r\n                                [mask]=\"true\"\r\n                                [showTime]=\"true\"\r\n                                [format]=\"'DD/MM/YYYY HH:mm:ss'\"\r\n                                [outputFormat]=\"'YYYY/MM/DD HH:mm:ss'\"\r\n                                formControlName=\"deliveryDate\"></nh-date>\r\n                            <span class=\"help-block\" *ngIf=\"formErrors.entryDate\"\r\n                                  i18n=\"@@goodsReceiptNoteDeliveryDateErrorMessage\">\r\n                                {formErrors.deliveryDate, select, required {Vui lòng nhập ngày xuất}}\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Hình thức xuất\"\r\n                               i18n-ghmLabel=\"@@deliveryType\" [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <nh-select\r\n                                *ngIf=\"!isUpdate; else goodsDeliveryNoteTypeReadOnlyTemplate\"\r\n                                [data]=\"listDeliveryType\"\r\n                                title=\"-- Chọn hình thức xuất kho --\"\r\n                                i18n-title=\"@@deliveryTypeTitle\"\r\n                                formControlName=\"type\"\r\n                            ></nh-select>\r\n                            <ng-template #goodsDeliveryNoteTypeReadOnlyTemplate>\r\n                                <div class=\"form-control\"\r\n                                     i18n=\"@@goodsDeliveryNoteTypeValue\"\r\n                                     readonly>\r\n                                    {model.value.type, select, 0 {Bán} 1 {Sử dụng} 2 {Trả nhà cung cấp} 3 {Hủy} 4 {Điều\r\n                                    chuyển} 5 {Kiểm kê} other {}}\r\n                                </div>\r\n                            </ng-template>\r\n                            <span class=\"help-block\">\r\n                                {\r\n                                    formErrors?.type,\r\n                                    select, required {Vui lòng chọn hình thức xuất kho}\r\n                                    isValid {Hình thức xuất kho không hợp lệ}\r\n                                }\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\" *ngIf=\"model.value.type === deliveryType.selfConsumer\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Phòng ban\"\r\n                               i18n-ghmLabel=\"@@officeName\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <app-office-tree\r\n                                [selectedText]=\"model.value.officeName\"\r\n                                (officeSelected)=\"onOfficeSelected($event)\"></app-office-tree>\r\n                            <span class=\"help-block\" i18n=\"@@deliveryOfficeErrorMessage\">\r\n                                 {\r\n                                   formErrors?.officeId,\r\n                                   select, required {Vui lòng chọn phòng ban}\r\n                                 }\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Người nhận\" [required]=\"true\"\r\n                               i18n-ghmLabel=\"@@receiver\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <ghm-user-suggestion\r\n                                [selectedUser]=\"model.value.receiverFullName\r\n                                ? {id: model.value.receiverId, fullName: model.value.receiverFullName, avatar: model.value.receiverAvatar}\r\n                                : null\"\r\n                                (userSelected)=\"onUserSelected($event)\"\r\n                                (userRemoved)=\"onUserRemoved()\"></ghm-user-suggestion>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\" *ngIf=\"model.value.type === deliveryType.retail\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\"\r\n                         [class.has-error]=\"formErrors.receiverFullName\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Người nhận\"\r\n                               i18n-ghmLabel=\"@@receiverFullName\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <app-receiver-suggestion\r\n                                [selectedItem]=\"model.value.receiverFullName ? {id: model.value.receiverId, name: model.value.receiverFullName} : null\"\r\n                                (itemRemoved)=\"onReceiverRemoved()\"\r\n                                (itemSelected)=\"onReceiverSelected($event)\"></app-receiver-suggestion>\r\n                            <span class=\"help-block\" i18n=\"@@deliveryReceiverErrorMessage\">\r\n                                 {\r\n                                   formErrors?.receiverFullName,\r\n                                   select, required {Vui lòng nhập họ tên người nhận.} maxlength {Họ tên người nhận không được phép vượt quá 50 ký tự.}\r\n                                 }\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\"\r\n                         [class.has-error]=\"formErrors.receiverPhoneNumber\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Số điện thoại\"\r\n                               i18n-ghmLabel=\"@@receiverPhoneNumber\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <input type=\"text\" class=\"form-control\" formControlName=\"receiverPhoneNumber\">\r\n                            <span class=\"help-block\" i18n=\"@@deliveryReceiverPhoneNumberErrorMessage\">\r\n                                 {\r\n                                   formErrors?.warehouseId,\r\n                                   select, required {Vui lòng nhập số điện thoại người nhận} maxlength {Số điện thoại người nhận không được phép vượt quá 50 ký tự.}\r\n                                 }\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\" [class.has-error]=\"formErrors?.warehouseId\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Xuất tại kho\"\r\n                               i18n-ghmLabel=\"@@deliveringWarehouse\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <app-warehouse-suggestion\r\n                                #warehouseSuggestion\r\n                                *ngIf=\"!isUpdate; else warehouseReadOnlyTemplate\"\r\n                                (itemRemoved)=\"onRemoveWarehouse()\"\r\n                                (itemSelected)=\"onWarehouseSelected($event)\"></app-warehouse-suggestion>\r\n                            <ng-template #warehouseReadOnlyTemplate>\r\n                                <div class=\"form-control\" readonly>{{ model.value.warehouseName }}</div>\r\n                            </ng-template>\r\n                            <span class=\"help-block\" i18n=\"@@deliveryFromWarehouseErrorMessage\">\r\n                                 {\r\n                                   formErrors?.warehouseId,\r\n                                   select, required {Vui lòng chọn kho xuất}\r\n                                 }\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\"\r\n                         [class.has-error]=\"formErrors.receptionWarehouseId\"\r\n                         *ngIf=\"model.value.type === deliveryType.transfer\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Nhập tại kho\"\r\n                               i18n-ghmLabel=\"@@receptionWarehouse\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <app-warehouse-suggestion\r\n                                #warehouseReceiverSuggestion\r\n                                [selectedItem]=\"selectedReceiverWarehouse\"\r\n                                (itemRemoved)=\"onRemoveReceptionWarehouse()\"\r\n                                (itemSelected)=\"onReceptionWarehouseSelected($event)\"></app-warehouse-suggestion>\r\n                            <span class=\"help-block\" i18n=\"@@deliveryToWarehouseErrorMessage\">\r\n                               {\r\n                                  formErrors?.receiverWarehouseId,\r\n                                  select, required {Vui lòng chọn kho nhập}\r\n                               }\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\" *ngIf=\"model.value.type === deliveryType.transfer\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Người nhận\"\r\n                               i18n-ghmLabel=\"@@receiver\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <app-warehouse-manager-suggestion\r\n                                #warehouseReceiverSuggestion\r\n                                [warehouseId]=\"model.value.receptionWarehouseId\"\r\n                                [selectedItem]=\"model.value.receiverFullName ? {id: model.value.receiverId, name: model.value.receiverFullName} : null\"\r\n                                (itemRemoved)=\"onReceiverRemoved()\"\r\n                                (itemSelected)=\"onReceiverSelected($event)\"></app-warehouse-manager-suggestion>\r\n                            <span class=\"help-block\" i18n=\"@@deliveryToWarehouseErrorMessage\">\r\n                               {\r\n                                  formErrors?.receiverId,\r\n                                  select, required {Vui lòng chọn người nhận}\r\n                               }\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\" *ngIf=\"goodsDeliveryNoteDetail?.type === deliveryType.return\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Nhà cung cấp\"\r\n                               i18n-ghmLabel=\"@@supplier\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\" readonly=\"\">\r\n                                {{ model.value.supplierName }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\"\r\n                         [class.has-error]=\"formErrors?.reason\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Lý do\"\r\n                               i18n-ghmLabel=\"@@reason\"\r\n                               [required]=\"model.value.type === deliveryType.voided || model.value.type === deliveryType.return\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <textarea name=\"\" class=\"form-control\" rows=\"2\"\r\n                                      formControlName=\"reason\"></textarea>\r\n                            <span class=\"help-block\" i18n=\"@@deliveryReasonErrorMessage\">\r\n                               { formErrors?.reason,\r\n                                 select, required {Vui lòng nhập lý do} maxlength {Lý do không được phép vượt quá 500 ký tự}\r\n                               }\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <h3 class=\"form-section bold\" i18n=\"@@goodsDeliveryNoteDetailInfo\">Chi tiết phiếu xuất</h3>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-12\">\r\n                    <app-goods-delivery-note-product\r\n                        [goodsDeliveryNoteId]=\"id\"\r\n                        [warehouseId]=\"model.value.warehouseId\"\r\n                        [deliveryDate]=\"model.value.deliveryDate\"\r\n                        [totalQuantity]=\"model.value.totalQuantity\"\r\n                        [totalAmounts]=\"model.value.totalAmounts\"\r\n                        [listGoodsDeliveryNoteDetail]=\"listGoodsDeliveryNoteDetail\"\r\n                        [deliveryType]=\"model.value.type\"\r\n                        (totalAmountChanged)=\"onTotalAmountChanged($event)\"\r\n                    ></app-goods-delivery-note-product>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-sm-12\">\r\n                <div class=\"form-group\">\r\n                    <label class=\"col-sm-2\" ghmLabel=\"Tổng số tiền (viết bằng chữ)\"\r\n                           i18n-ghmLabel=\"@@totalAmountsWords\"></label>\r\n                    <div class=\"bold col-sm-10\">\r\n                        <div class=\"dotted-form-control\">{{ model.value.totalAmounts | ghmAmountToWord }}</div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <mat-checkbox\r\n                class=\"cm-mgr-5\"\r\n                color=\"primary\"\r\n                name=\"isCreateAnother\"\r\n                i18n=\"@@isCreateAnother\"\r\n                *ngIf=\"!isUpdate\"\r\n                [(checked)]=\"isCreateAnother\"\r\n                (change)=\"isCreateAnother = !isCreateAnother\"> Tiếp tục thêm\r\n            </mat-checkbox>\r\n            <ghm-button classes=\"btn blue cm-mgr-5\"\r\n                        [loading]=\"isSaving\">\r\n                <span i18n=\"@@save\">Lưu lại</span>\r\n            </ghm-button>\r\n            <ghm-button classes=\"btn default\"\r\n                        nh-dismiss=\"true\"\r\n                        [type]=\"'button'\"\r\n                        [loading]=\"isSaving\">\r\n                <span i18n=\"@@close\">Đóng lại</span>\r\n            </ghm-button>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-form/goods-delivery-note-form.component.ts":
/*!****************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-form/goods-delivery-note-form.component.ts ***!
  \****************************************************************************************************************************/
/*! exports provided: GoodsDeliveryNoteFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsDeliveryNoteFormComponent", function() { return GoodsDeliveryNoteFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _model_goods_delivery_note_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../model/goods-delivery-note.model */ "./src/app/modules/warehouse/goods/goods-delivery-note/model/goods-delivery-note.model.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _validators_number_validator__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../validators/number.validator */ "./src/app/validators/number.validator.ts");
/* harmony import */ var _shareds_constants_deliveryType_const__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../shareds/constants/deliveryType.const */ "./src/app/shareds/constants/deliveryType.const.ts");
/* harmony import */ var _validators_datetime_validator__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../validators/datetime.validator */ "./src/app/validators/datetime.validator.ts");
/* harmony import */ var _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../../shareds/components/nh-suggestion/nh-suggestion.component */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.component.ts");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _goods_delivery_note_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../goods-delivery-note.service */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.service.ts");
/* harmony import */ var _model_goods_delivery_note_details_model__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../model/goods-delivery-note-details.model */ "./src/app/modules/warehouse/goods/goods-delivery-note/model/goods-delivery-note-details.model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _shareds_components_ghm_user_suggestion_ghm_user_suggestion_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../../../../shareds/components/ghm-user-suggestion/ghm-user-suggestion.component */ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.component.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var _warehouse_warehouse_suggestion_warehouse_suggestion_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../../warehouse/warehouse-suggestion/warehouse-suggestion.component */ "./src/app/modules/warehouse/warehouse/warehouse-suggestion/warehouse-suggestion.component.ts");
/* harmony import */ var _product_supplier_supplier_suggestion_supplier_suggestion_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../../../product/supplier/supplier-suggestion/supplier-suggestion.component */ "./src/app/modules/warehouse/product/supplier/supplier-suggestion/supplier-suggestion.component.ts");
/* harmony import */ var _list_product_goods_delivery_note_product_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./list-product/goods-delivery-note-product.component */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-form/list-product/goods-delivery-note-product.component.ts");






















var GoodsDeliveryNoteFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](GoodsDeliveryNoteFormComponent, _super);
    function GoodsDeliveryNoteFormComponent(appConfig, utilService, numberValidator, dateTimeValidator, fb, toastr, goodsDeliveryNoteService) {
        var _this = _super.call(this) || this;
        _this.appConfig = appConfig;
        _this.utilService = utilService;
        _this.numberValidator = numberValidator;
        _this.dateTimeValidator = dateTimeValidator;
        _this.fb = fb;
        _this.toastr = toastr;
        _this.goodsDeliveryNoteService = goodsDeliveryNoteService;
        _this.goodsDeliveryNote = new _model_goods_delivery_note_model__WEBPACK_IMPORTED_MODULE_5__["GoodsDeliveryNote"]();
        _this.listDeliveryType = _shareds_constants_deliveryType_const__WEBPACK_IMPORTED_MODULE_8__["DeliveryTypes"];
        _this.deliveryType = _shareds_constants_deliveryType_const__WEBPACK_IMPORTED_MODULE_8__["DeliveryType"];
        _this.selectedReceiverWarehouse = null;
        _this.officeTree = [];
        _this._reasonRequired = false;
        return _this;
    }
    Object.defineProperty(GoodsDeliveryNoteFormComponent.prototype, "reasonRequired", {
        get: function () {
            return this._reasonRequired;
        },
        set: function (value) {
            this._reasonRequired = value;
            var reasonControl = this.model.get('reason');
            if (value) {
                reasonControl.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(500)]);
                reasonControl.updateValueAndValidity();
            }
            else {
                reasonControl.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(500));
                reasonControl.updateValueAndValidity();
            }
        },
        enumerable: true,
        configurable: true
    });
    GoodsDeliveryNoteFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.renderForm();
        // this.subscribers.getDay = this.model.get('day').valueChanges.subscribe((value) => {
        //     const month = this.model.value.month;
        //     const year = this.model.value.year;
        //     this.getDeliveryDate(value, month, year);
        // });
        // this.subscribers.getMonth = this.model.get('month').valueChanges.subscribe((value) => {
        //     const day = this.model.value.day;
        //     const year = this.model.value.year;
        //     this.getDeliveryDate(day, value, year);
        // });
        // this.subscribers.getYear = this.model.get('year').valueChanges.subscribe((value) => {
        //     const day = this.model.value.day;
        //     const month = this.model.value.month;
        //     this.getDeliveryDate(day, month, value);
        // });
        this.subscribers.getType = this.model.get('type').valueChanges.subscribe(function (value) {
            _this.reasonRequired = value === _this.deliveryType.voided || value === _this.deliveryType.return;
        });
    };
    GoodsDeliveryNoteFormComponent.prototype.onModalShow = function () {
    };
    GoodsDeliveryNoteFormComponent.prototype.onModalHidden = function () {
    };
    GoodsDeliveryNoteFormComponent.prototype.onOfficeSelected = function (data) {
        this.model.patchValue({
            officeId: data.id,
            officeName: data.text
        });
    };
    GoodsDeliveryNoteFormComponent.prototype.onUserSelected = function (user) {
        this.model.patchValue({
            receiverId: user.id,
            receiverFullName: user.fullName
        });
    };
    GoodsDeliveryNoteFormComponent.prototype.onUserRemoved = function () {
        this.model.patchValue({
            receiverId: null,
            receiverFullName: null
        });
    };
    GoodsDeliveryNoteFormComponent.prototype.onReceiverSelected = function (receiver) {
        this.model.patchValue({
            receiverId: receiver ? receiver.id : null,
            receiverFullName: receiver ? receiver.name : null,
            receiverPhoneNumber: receiver ? receiver.phoneNumber : null
        });
    };
    GoodsDeliveryNoteFormComponent.prototype.onReceiverRemoved = function () {
        this.model.patchValue({
            receiverId: null,
            receiverFullName: null,
            receiverPhoneNumber: null
        });
    };
    GoodsDeliveryNoteFormComponent.prototype.add = function () {
        this.resetForm();
        this.isUpdate = false;
        this.listGoodsDeliveryNoteDetail = [];
        this.formGoodsDeliveryNote.open();
    };
    GoodsDeliveryNoteFormComponent.prototype.edit = function (id) {
        this.isUpdate = true;
        this.id = id;
        this.getDetail(id);
        this.formGoodsDeliveryNote.open();
    };
    GoodsDeliveryNoteFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            this.goodsDeliveryNote = this.model.value;
            this.goodsDeliveryNote.goodsDeliveryNoteDetails = this.goodsDeliveryNoteProductComponent.listGoodsDeliveryNoteDetail;
            // Compare real quantity with inventory quantity.
            var count = lodash__WEBPACK_IMPORTED_MODULE_18__["countBy"](this.goodsDeliveryNote.goodsDeliveryNoteDetails, function (goodsDeliveryNoteDetail) {
                return goodsDeliveryNoteDetail.quantity > goodsDeliveryNoteDetail.conversionInventory;
            }).true;
            if (count && count > 0) {
                this.toastr.warning('Số lượng thực xuất phải nhỏ hơn số lượng tồn kho.');
                return;
            }
            if (!this.goodsDeliveryNote.goodsDeliveryNoteDetails || this.goodsDeliveryNote.goodsDeliveryNoteDetails.length === 0) {
                this.toastr.error('Vui lòng chọn sản phẩm.');
                return;
            }
            this.isSaving = true;
            if (this.isUpdate) {
                this.goodsDeliveryNoteService.update(this.id, this.goodsDeliveryNote)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["finalize"])(function () { return (_this.isSaving = false); }))
                    .subscribe(function () {
                    _this.isModified = true;
                    _this.saveSuccessful.emit();
                    _this.formGoodsDeliveryNote.dismiss();
                });
            }
            else {
                this.goodsDeliveryNoteService.insert(this.goodsDeliveryNote)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["finalize"])(function () { return (_this.isSaving = false); }))
                    .subscribe(function () {
                    _this.isModified = true;
                    if (_this.isCreateAnother) {
                        _this.resetForm();
                    }
                    else {
                        _this.saveSuccessful.emit();
                        _this.formGoodsDeliveryNote.dismiss();
                    }
                });
            }
        }
    };
    GoodsDeliveryNoteFormComponent.prototype.onReceptionWarehouseSelected = function (value) {
        if (value) {
            this.model.patchValue({
                receptionWarehouseId: value.id
            });
            if (this.model.value.type === _shareds_constants_deliveryType_const__WEBPACK_IMPORTED_MODULE_8__["DeliveryType"].transfer && this.model.value.receptionWarehouseId && this.model.value.warehouseId
                && this.model.value.receptionWarehouseId === this.model.value.warehouseId) {
                this.toastr.error('Kho xuất và kho nhập phải khác nhau.');
                this.model.patchValue({
                    receptionWarehouseId: ''
                });
                return;
            }
        }
    };
    GoodsDeliveryNoteFormComponent.prototype.onWarehouseSelected = function (warehouse) {
        this.model.patchValue({ warehouseId: warehouse ? warehouse.id : null });
        if (this.model.value.type === _shareds_constants_deliveryType_const__WEBPACK_IMPORTED_MODULE_8__["DeliveryType"].transfer && this.model.value.receptionWarehouseId && this.model.value.warehouseId
            && this.model.value.receptionWarehouseId === this.model.value.warehouseId) {
            this.toastr.error('Kho nhận phải khác kho xuất.');
            this.model.patchValue({
                warehouseId: ''
            });
            return;
        }
    };
    GoodsDeliveryNoteFormComponent.prototype.onRemoveWarehouse = function () {
        this.model.patchValue({
            warehouseId: null
        });
    };
    GoodsDeliveryNoteFormComponent.prototype.onRemoveReceptionWarehouse = function () {
        this.model.patchValue({
            receptionWarehouseId: null
        });
    };
    GoodsDeliveryNoteFormComponent.prototype.onSupplierSelected = function (supplier) {
        this.model.patchValue({ receiverId: supplier ? supplier.id : null });
    };
    GoodsDeliveryNoteFormComponent.prototype.selectUser = function (user) {
        if (user) {
            this.model.patchValue({ receiverId: user.id, receiverFullName: user.fullName });
        }
        else {
            this.model.patchValue({ receiverId: null, receiverFullName: '' });
        }
    };
    GoodsDeliveryNoteFormComponent.prototype.onSelectProduct = function (value) {
        if (value) {
            this.listGoodsDeliveryNoteDetail = value;
        }
    };
    GoodsDeliveryNoteFormComponent.prototype.onTotalAmountChanged = function (value) {
        if (value) {
            this.model.patchValue({ totalAmounts: value });
        }
        else {
            this.model.patchValue({ totalAmounts: 0 });
        }
    };
    GoodsDeliveryNoteFormComponent.prototype.getDetail = function (id) {
        var _this = this;
        this.goodsDeliveryNoteService.getDetail(id).subscribe(function (result) {
            var data = result.data;
            _this.goodsDeliveryNoteDetail = data;
            var totalQuantity = lodash__WEBPACK_IMPORTED_MODULE_18__["sumBy"](_this.goodsDeliveryNoteDetail.goodsDeliveryNoteDetails, function (item) {
                return item.quantity;
            });
            if (data) {
                if (data.warehouseId && data.warehouseName) {
                    _this.selectedReceiverWarehouse = new _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_10__["NhSuggestion"](data.receptionWarehouseId, data.receptionWarehouseName);
                }
                data.deliveryDate = moment__WEBPACK_IMPORTED_MODULE_17__(data.deliveryDate).format('YYYY/MM/DD HH:mm:ss');
                _this.model.patchValue({
                    warehouseId: data.warehouseId,
                    warehouseName: data.warehouseName,
                    reason: data.reason,
                    type: data.type,
                    receiverId: data.receiverId,
                    receiverFullName: data.receiverFullName,
                    receiverPhoneNumber: data.receiverPhoneNumber,
                    note: data.note,
                    officeId: data.officeId,
                    officeName: data.officeName,
                    receptionWarehouseId: data.receptionWarehouseId,
                    agencyId: data.agencyId,
                    totalAmounts: data.totalAmounts,
                    totalQuantity: totalQuantity,
                    inventoryId: data.inventoryId,
                    deliveryNo: data.deliveryNo,
                    deliveryDate: data.deliveryDate,
                    concurrencyStamp: data.concurrencyStamp,
                    supplierName: data.supplierName
                });
                _this.listGoodsDeliveryNoteDetail = _this.goodsDeliveryNoteDetail.goodsDeliveryNoteDetails
                    .map(function (goodsDeliveryNoteDetail) {
                    return new _model_goods_delivery_note_details_model__WEBPACK_IMPORTED_MODULE_13__["GoodsDeliveryNoteDetail"](goodsDeliveryNoteDetail.id, goodsDeliveryNoteDetail.code, goodsDeliveryNoteDetail.productId, goodsDeliveryNoteDetail.productName, goodsDeliveryNoteDetail.warehouseId, goodsDeliveryNoteDetail.warehouseName, goodsDeliveryNoteDetail.unitId, goodsDeliveryNoteDetail.unitName, goodsDeliveryNoteDetail.price, goodsDeliveryNoteDetail.quantity, goodsDeliveryNoteDetail.recommendedQuantity, goodsDeliveryNoteDetail.lotId, goodsDeliveryNoteDetail.inventoryQuantity, goodsDeliveryNoteDetail.goodsReceiptNoteDetailCode, goodsDeliveryNoteDetail.concurrencyStamp, goodsDeliveryNoteDetail.units);
                });
            }
        });
    };
    GoodsDeliveryNoteFormComponent.prototype.renderForm = function () {
        this.buildForm();
    };
    GoodsDeliveryNoteFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.utilService.renderFormError(['warehouseId', 'reason', 'type', 'receiverId', 'receiverFullName',
            'receiverPhoneNumber', 'note', 'receptionWarehouseId', 'agencyId', 'totalAmounts', 'inventoryId', 'deliveryNo', 'day', 'month',
            'year']);
        this.validationMessages = this.utilService.renderFormErrorMessage([
            { 'warehouseId': ['required', 'maxlength'] },
            { 'reason': ['required', 'maxlength'] },
            { 'type': ['required', 'isValid'] },
            { 'receiverId': ['maxlength'] },
            { 'receiverFullName': ['maxlength'] },
            { 'receiverPhoneNumber': ['maxlength'] },
            { 'note': ['maxlength'] },
            { 'receptionWarehouseId': ['maxLength'] },
            { 'totalAmounts': ['required', 'isValid', 'greaterThan', 'lessThan'] },
            { 'inventoryId': ['maxlength'] },
            { 'deliveryNo': ['maxlength'] },
            { 'day': ['required', 'isValid', 'lessThan', 'greaterThan'] },
            { 'month': ['required', 'isValid', 'lessThan', 'greaterThan'] },
            { 'year': ['required', 'isValid', 'greaterThan'] },
        ]);
        this.model = this.fb.group({
            warehouseId: [this.goodsDeliveryNote.warehouseId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(50)
                ]],
            warehouseName: [this.goodsDeliveryNote.warehouseName],
            reason: [this.goodsDeliveryNote.reason, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(500)
                ]],
            type: [this.goodsDeliveryNote.type, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, this.numberValidator.isValid
                ]],
            receiverId: [this.goodsDeliveryNote.receiverId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(50)
                ]],
            receiverFullName: [this.goodsDeliveryNote.receiverFullName, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(50)
                ]],
            receiverAvatar: [this.goodsDeliveryNote.receiverAvatar],
            receiverPhoneNumber: [this.goodsDeliveryNote.receiverPhoneNumber, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(50)
                ]],
            note: [this.goodsDeliveryNote.note, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(500)
                ]],
            receptionWarehouseId: [this.goodsDeliveryNote.receptionWarehouseId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(50)
                ]],
            agencyId: [this.goodsDeliveryNote.agencyId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(50)
                ]],
            totalAmounts: [this.goodsDeliveryNote.totalAmounts, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, this.numberValidator.isValid,
                    this.numberValidator.greaterThan(0), this.numberValidator.lessThan(2147483648)
                ]],
            totalQuantity: [this.goodsDeliveryNote.totalQuantity],
            inventoryId: [this.goodsDeliveryNote.inventoryId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(50)
                ]],
            deliveryNo: [{ value: this.goodsDeliveryNote.deliveryNo, disabled: true }, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(50)
                ]],
            day: [this.goodsDeliveryNote.day, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, this.numberValidator.isValid, this.numberValidator.lessThan(32), this.numberValidator.greaterThan(0)
                ]],
            month: [this.goodsDeliveryNote.month, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, this.numberValidator.isValid, this.numberValidator.lessThan(13), this.numberValidator.greaterThan(0)
                ]],
            year: [this.goodsDeliveryNote.year, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, this.numberValidator.isValid, this.numberValidator.greaterThan(new Date().getFullYear() - 1),
                    this.numberValidator.lessThan(new Date().getFullYear() + 1)
                ]],
            officeId: [this.goodsDeliveryNote.officeId],
            officeName: [this.goodsDeliveryNote.officeName],
            deliveryDate: [this.goodsDeliveryNote.deliveryDate],
            supplierName: [this.goodsDeliveryNote.supplierName],
            concurrencyStamp: [this.goodsDeliveryNote.concurrencyStamp],
        });
        this.model.valueChanges.subscribe(function (data) { return _this.validateModel(false); });
    };
    GoodsDeliveryNoteFormComponent.prototype.resetForm = function () {
        this.id = null;
        this.listGoodsDeliveryNoteDetail = [];
        this.model.patchValue(new _model_goods_delivery_note_model__WEBPACK_IMPORTED_MODULE_5__["GoodsDeliveryNote"]());
        if (this.warehouseSuggestionComponent) {
            this.warehouseSuggestionComponent.clear();
        }
        this.selectedReceiverWarehouse = null;
        if (this.supplierSuggestionComponent) {
            this.supplierSuggestionComponent.clear();
        }
        if (this.ghmUserSuggestionComponent) {
            this.ghmUserSuggestionComponent.clear();
        }
        this.clearFormError(this.formErrors);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('formGoodsDeliveryNote'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__["NhModalComponent"])
    ], GoodsDeliveryNoteFormComponent.prototype, "formGoodsDeliveryNote", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('warehouseSuggestion'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _warehouse_warehouse_suggestion_warehouse_suggestion_component__WEBPACK_IMPORTED_MODULE_19__["WarehouseSuggestionComponent"])
    ], GoodsDeliveryNoteFormComponent.prototype, "warehouseSuggestionComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_product_supplier_supplier_suggestion_supplier_suggestion_component__WEBPACK_IMPORTED_MODULE_20__["SupplierSuggestionComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _product_supplier_supplier_suggestion_supplier_suggestion_component__WEBPACK_IMPORTED_MODULE_20__["SupplierSuggestionComponent"])
    ], GoodsDeliveryNoteFormComponent.prototype, "supplierSuggestionComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_shareds_components_ghm_user_suggestion_ghm_user_suggestion_component__WEBPACK_IMPORTED_MODULE_16__["GhmUserSuggestionComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_user_suggestion_ghm_user_suggestion_component__WEBPACK_IMPORTED_MODULE_16__["GhmUserSuggestionComponent"])
    ], GoodsDeliveryNoteFormComponent.prototype, "ghmUserSuggestionComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_list_product_goods_delivery_note_product_component__WEBPACK_IMPORTED_MODULE_21__["GoodsDeliveryNoteProductComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _list_product_goods_delivery_note_product_component__WEBPACK_IMPORTED_MODULE_21__["GoodsDeliveryNoteProductComponent"])
    ], GoodsDeliveryNoteFormComponent.prototype, "goodsDeliveryNoteProductComponent", void 0);
    GoodsDeliveryNoteFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-goods-delivery-note-form',
            template: __webpack_require__(/*! ./goods-delivery-note-form.component.html */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-form/goods-delivery-note-form.component.html"),
            providers: [_validators_number_validator__WEBPACK_IMPORTED_MODULE_7__["NumberValidator"], _validators_datetime_validator__WEBPACK_IMPORTED_MODULE_9__["DateTimeValidator"], _goods_delivery_note_service__WEBPACK_IMPORTED_MODULE_12__["GoodsDeliveryNoteService"]],
            styles: [__webpack_require__(/*! ../goods-delivery-note.scss */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_11__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_6__["UtilService"],
            _validators_number_validator__WEBPACK_IMPORTED_MODULE_7__["NumberValidator"],
            _validators_datetime_validator__WEBPACK_IMPORTED_MODULE_9__["DateTimeValidator"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_15__["ToastrService"],
            _goods_delivery_note_service__WEBPACK_IMPORTED_MODULE_12__["GoodsDeliveryNoteService"]])
    ], GoodsDeliveryNoteFormComponent);
    return GoodsDeliveryNoteFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-form/list-product/goods-delivery-note-product.component.html":
/*!**********************************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-form/list-product/goods-delivery-note-product.component.html ***!
  \**********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table class=\"table table-bordered table-stripped table-hover\">\r\n    <thead>\r\n    <tr>\r\n        <th rowspan=\"2\" class=\"middle center w50\" i18n=\"@@no\">STT</th>\r\n        <th rowspan=\"2\" class=\"middle center w100\" i18n=\"@@code\">Mã</th>\r\n        <th rowspan=\"2\" class=\"middle center\" i18n=\"@@productName\">Tên sản phẩm</th>\r\n        <th rowspan=\"2\" class=\"middle center w100\" i18n=\"@@unit\">Đơn vị</th>\r\n        <th rowspan=\"2\" class=\"middle center w100\" i18n=\"@@lot\">Số lô</th>\r\n        <!--<th rowspan=\"2\" class=\"middle center w100\" i18n=\"@@expiredDate\">ExpiredDate</th>-->\r\n        <th colspan=\"3\" class=\"middle center\" i18n=\"@@quantity\">Số lượng</th>\r\n        <th rowspan=\"2\" class=\"middle center w100\" i18n=\"@@price\">Đơn giá</th>\r\n        <!--<td rowspan=\"2\" class=\"middle center w100\" i18n=\"@@discount\">Discount</td>-->\r\n        <th rowspan=\"2\" class=\"middle center w100\" i18n=\"@@totalAmount\">Thành tiền</th>\r\n        <th class=\"center middle w50\" rowspan=\"2\" *ngIf=\"!readonly\"></th>\r\n    </tr>\r\n    <tr>\r\n        <th class=\"middle center w100\" i18n=\"@@inventory\">Tồn kho</th>\r\n        <th class=\"middle center w100\" i18n=\"@@recommendedQuantity\">Yêu cầu</th>\r\n        <th class=\"middle center w100\" i18n=\"@@realQuantity\">Thực xuất</th>\r\n    </tr>\r\n    </thead>\r\n    <tbody>\r\n    <tr *ngIf=\"!readonly\">\r\n        <ng-container [formGroup]=\"model\">\r\n            <td colspan=\"2\" class=\"text-right middle\">\r\n                <span i18n=\"@@product\">Sản phẩm:</span>\r\n            </td>\r\n            <td class=\"middle center\" [class.has-error]=\"formErrors?.goodsReceiptNoteDetailCode\">\r\n                <!--<app-product-suggestion *ngIf=\"!isUpdate\"-->\r\n                <!--#productSuggestion-->\r\n                <!--(itemSelected)=\"selectProductInForm($event)\"></app-product-suggestion>-->\r\n                <input type=\"text\" class=\"form-control\" formControlName=\"goodsReceiptNoteDetailCode\"\r\n                       id=\"goodsReceiptNoteDetailCode\"\r\n                       ghm-text-selection\r\n                       (keydown.enter)=\"$event.preventDefault()\">\r\n                <span class=\"help-block\" i18n=\"@@productCodeErrorMessage\"> {formErrors?.code,\r\n                select, required {Vui lòng nhập mã hàng hóa}\r\n                maxlength {Mã hàng hóa không được phép lơn hơn 50 ký tự}}\r\n                </span>\r\n            </td>\r\n            <!--<td class=\"middle\">-->\r\n            <!--{{ model.value.productName }}-->\r\n            <!--</td>-->\r\n            <td class=\"middle center\" [class.has-error]=\"formErrors?.unitId\" [formGroup]=\"model\">\r\n                {{ model.value.unitName }}\r\n            </td>\r\n            <td class=\"middle center\">\r\n                {{ model.value.lotId }}\r\n            </td>\r\n            <td class=\"middle text-right\">\r\n                {{ model.value.inventoryQuantity }}\r\n            </td>\r\n            <!--<td class=\"middle\" [formGroup]=\"model\">-->\r\n            <!--<span>{{model.value.expiredDate | dateTimeFormat :'DD/MM/YYYY'}}</span>-->\r\n            <!--</td>-->\r\n            <td class=\"middle\" [class.has-error]=\"formErrors.quantity\" [formGroup]=\"model\">\r\n                <!--<input class=\"form-control text-right\" formControlName=\"recommendedQuantity\" id=\"recommendQuantity\">-->\r\n                <!--<span class=\"help-block\">-->\r\n                <!--{-->\r\n                <!--formErrors?.recommendedQuantity,-->\r\n                <!--select,-->\r\n                <!--isValid {Quantity must is number}-->\r\n                <!--lessThan {Quantity must less than 2147483647}-->\r\n                <!--greaterThan {Quantity must greater than 0}-->\r\n                <!--}-->\r\n                <!--</span>-->\r\n\r\n            </td>\r\n            <td class=\"middle\" [class.has-error]=\"formErrors?.recommendedQuantity\" [formGroup]=\"model\">\r\n                <!--<input class=\"form-control text-right\" formControlName=\"quantity\" id=\"quantity\">-->\r\n                <!--<span class=\"help-block\">-->\r\n                <!--{-->\r\n                <!--formErrors?.quantity,-->\r\n                <!--select, required {Please enter quantity}-->\r\n                <!--isValid {Quantity must is number}-->\r\n                <!--lessThan {Quantity must less than 2147483647}-->\r\n                <!--greaterThan {Quantity must greater than 0}-->\r\n                <!--}-->\r\n                <!--</span>-->\r\n            </td>\r\n            <td></td>\r\n            <td></td>\r\n            <td *ngIf=\"!readonly\"></td>\r\n        </ng-container>\r\n    </tr>\r\n    <tr *ngFor=\"let item of listGoodsDeliveryNoteDetail; let i = index\"\r\n        nhContextMenuTrigger\r\n        [nhContextMenuTriggerFor]=\"nhMenu\"\r\n        [nhContextMenuData]=\"item\">\r\n        <td class=\"middle center\">{{(currentPage - 1 )*pageSize + i + 1}}</td>\r\n        <td class=\"middle\">{{ item.goodsReceiptNoteDetailCode }}</td>\r\n        <td class=\"middle\"><span class=\"media-heading\">{{ item.productName }}</span></td>\r\n        <td class=\"middle center w150\">\r\n            <!--{{item.unitName}}-->\r\n            <span *ngIf=\"readonly; else unitEditableTemmplate\">{{ item.unitName }}</span>\r\n            <ng-template #unitEditableTemmplate>\r\n                <nh-select [data]=\"item.units\"\r\n                           [title]=\"'-- Chọn đơn vị --'\"\r\n                           [selectedItem]=\"item.unitId ? {id: item.unitId, name: item.unitName} : null\"\r\n                           (itemSelected)=\"onUnitSelected(item, $event)\"\r\n                ></nh-select>\r\n            </ng-template>\r\n        </td>\r\n        <td class=\"middle center w150\">{{ item.lotId }}</td>\r\n        <td class=\"middle text-right\">{{ item.conversionInventory | formatNumber:2 }}</td>\r\n        <td class=\"middle text-right\">\r\n            <input type=\"text\" class=\"form-control text-right\" [(ngModel)]=\"item.recommendedQuantity\"\r\n                   name=\"recommended-quantity-{{i}}\"\r\n                   ghm-text-selection\r\n                   (focus)=\"oldRecommendedQuantity = item.recommendedQuantity\"\r\n                   (blur)=\"updateRecommendedQuantity(item)\"\r\n                   *ngIf=\"!readonly; else recommendedQuantityReadOnlyTemplate\"/>\r\n\r\n            <ng-template #recommendedQuantityReadOnlyTemplate>\r\n                {{ item.recommendedQuantity }}\r\n            </ng-template>\r\n        </td>\r\n        <td class=\"middle text-right\">\r\n            <input type=\"text\" class=\"form-control text-right\" [(ngModel)]=\"item.quantity\"\r\n                   name=\"quantity={{i}}\"\r\n                   ghm-text-selection\r\n                   id=\"quantity-{{i}}\"\r\n                   (focus)=\"oldQuantity = item.quantity\"\r\n                   (blur)=\"updateQuantity(item)\" *ngIf=\"!readonly; else quantityReadOnlyTemplate\"/>\r\n            <ng-template #quantityReadOnlyTemplate>\r\n                {{ item.quantity }}\r\n            </ng-template>\r\n        </td>\r\n        <td class=\"middle text-right\">{{ item.conversionPrice | ghmCurrency:2 }}</td>\r\n        <td class=\"middle text-right\"><span>{{ item.totalAmounts | ghmCurrency:2 }}</span></td>\r\n        <td class=\"middle text-right\" *ngIf=\"!readonly\">\r\n            <button class=\"btn red btn-sm\" type=\"button\" (click)=\"remove(item)\">\r\n                <i class=\"fa fa-trash-o\"></i>\r\n            </button>\r\n        </td>\r\n    </tr>\r\n    </tbody>\r\n    <tfoot>\r\n    <tr>\r\n        <td colspan=\"9\" class=\"middle text-right \" i18n=\"@@total\">Tổng cộng:</td>\r\n        <td class=\"text-right\">{{ totalAmounts | ghmCurrency:2 }}</td>\r\n        <td *ngIf=\"!readonly\"></td>\r\n    </tr>\r\n    </tfoot>\r\n</table>\r\n\r\n<swal\r\n    #confirmDeleteGoodsDeliveryNotesDetail\r\n    i18n=\"@@confirmDeleteGoodsDeliveryNotesDetail\"\r\n    i18n-title=\"@@confirmTitleDeleteGoodsDeliveryNotesDetail\"\r\n    i18n-text=\"@@confirmTextDeleteGoodsDeliveryNotesDetail\"\r\n    title=\"Bạn có chắc chắn muốn xóa chi tiết phiếu xuất này không?\"\r\n    text=\"Lưu ý: bạn không thể khôi phục sau khi thực hiện xóa.\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Đồng ý\"\r\n    cancelButtonText=\"Hủy bỏ\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<nh-menu #nhMenu>\r\n    <nh-menu-item (clicked)=\"edit($event)\">\r\n        <mat-icon class=\"menu-icon\">edit</mat-icon>\r\n        <span i18n=\"@@edit\">Edit</span>\r\n    </nh-menu-item>\r\n    <nh-menu-item *ngIf=\"permission.delete\"\r\n                  (clicked)=\"confirm($event)\">\r\n        <mat-icon class=\"menu-icon\">delete</mat-icon>\r\n        <span i18n=\"@@edit\">Delete</span>\r\n    </nh-menu-item>\r\n</nh-menu>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-form/list-product/goods-delivery-note-product.component.ts":
/*!********************************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-form/list-product/goods-delivery-note-product.component.ts ***!
  \********************************************************************************************************************************************/
/*! exports provided: GoodsDeliveryNoteProductComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsDeliveryNoteProductComponent", function() { return GoodsDeliveryNoteProductComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _product_product_service_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../product/product/service/product.service */ "./src/app/modules/warehouse/product/product/service/product.service.ts");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _model_goods_delivery_note_details_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../model/goods-delivery-note-details.model */ "./src/app/modules/warehouse/goods/goods-delivery-note/model/goods-delivery-note-details.model.ts");
/* harmony import */ var _validators_number_validator__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../../validators/number.validator */ "./src/app/validators/number.validator.ts");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _goods_delivery_note_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../goods-delivery-note.service */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.service.ts");
/* harmony import */ var _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @toverux/ngx-sweetalert2 */ "./node_modules/@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _shareds_constants_deliveryType_const__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../../../../shareds/constants/deliveryType.const */ "./src/app/shareds/constants/deliveryType.const.ts");
/* harmony import */ var _product_product_product_suggestion_product_suggestion_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../../product/product/product-suggestion/product-suggestion.component */ "./src/app/modules/warehouse/product/product/product-suggestion/product-suggestion.component.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _goods_receipt_note_goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../../goods-receipt-note/goods-receipt-note.service */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.service.ts");

















var GoodsDeliveryNoteProductComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](GoodsDeliveryNoteProductComponent, _super);
    function GoodsDeliveryNoteProductComponent(appConfig, productService, numberValidator, fb, utilService, toastr, goodsDeliveryNoteService, goodsReceiptNoteService) {
        var _this = _super.call(this) || this;
        _this.appConfig = appConfig;
        _this.productService = productService;
        _this.numberValidator = numberValidator;
        _this.fb = fb;
        _this.utilService = utilService;
        _this.toastr = toastr;
        _this.goodsDeliveryNoteService = goodsDeliveryNoteService;
        _this.goodsReceiptNoteService = goodsReceiptNoteService;
        _this.readonly = false;
        _this.selectProduct = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.totalAmountChanged = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.currentPage = 1;
        _this.pageSize = 10;
        _this.goodsDeliveryNoteDetail = new _model_goods_delivery_note_details_model__WEBPACK_IMPORTED_MODULE_6__["GoodsDeliveryNoteDetail"]();
        _this.units = [];
        _this._listGoodsDeliveryNoteDetail = [];
        _this.urlSearchProduct = appConfig.API_GATEWAY_URL + "api/v1/warehouse/products";
        return _this;
    }
    Object.defineProperty(GoodsDeliveryNoteProductComponent.prototype, "listGoodsDeliveryNoteDetail", {
        get: function () {
            return this._listGoodsDeliveryNoteDetail;
        },
        set: function (value) {
            this._listGoodsDeliveryNoteDetail = value;
            this.calculateAmount();
        },
        enumerable: true,
        configurable: true
    });
    GoodsDeliveryNoteProductComponent.prototype.ngOnInit = function () {
        this.buildForm();
        this.getProduct();
    };
    GoodsDeliveryNoteProductComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.swalConfirmDelete.confirm.subscribe(function (result) {
            _this.confirm();
        });
    };
    GoodsDeliveryNoteProductComponent.prototype.updateQuantity = function (goodsDeliveryNoteDetail) {
        var _this = this;
        if (this.oldQuantity === goodsDeliveryNoteDetail.quantity) {
            return;
        }
        if (goodsDeliveryNoteDetail.quantity > goodsDeliveryNoteDetail.inventoryQuantity) {
            this.toastr.warning('Số lượng thực xuất phải nhỏ hơn số lượng tồn kho.');
            return;
        }
        if (goodsDeliveryNoteDetail.quantity.toString() === '') {
            this.toastr.error('Số lượng thực tế phải là số.');
            return;
        }
        if (goodsDeliveryNoteDetail.quantity <= 0) {
            this.toastr.error('Số lượng thức tế phải lớn hơn 0.');
            return;
        }
        if (!goodsDeliveryNoteDetail.quantity) {
            this.toastr.error('Số lượng phải lớn hơn 0');
            return;
        }
        if (this.goodsDeliveryNoteId) {
            this.subscribers.updateQuantity = this.goodsDeliveryNoteService.updateDetail(this.goodsDeliveryNoteId, goodsDeliveryNoteDetail.id, goodsDeliveryNoteDetail)
                .subscribe(function (result) {
                _this.toastr.success(result.message);
                goodsDeliveryNoteDetail.concurrencyStamp = result.data;
            }, function () {
                goodsDeliveryNoteDetail.quantity = _this.oldQuantity;
            });
        }
        goodsDeliveryNoteDetail.totalAmounts = goodsDeliveryNoteDetail.quantity * goodsDeliveryNoteDetail.conversionPrice;
        this.calculateAmount();
    };
    GoodsDeliveryNoteProductComponent.prototype.updateRecommendedQuantity = function (goodsDeliveryNoteDetail) {
        var _this = this;
        if (this.oldRecommendedQuantity === goodsDeliveryNoteDetail.recommendedQuantity) {
            return;
        }
        if (goodsDeliveryNoteDetail.recommendedQuantity.toString() === '') {
            this.toastr.error('Số lượng yêu cầu phải là số.');
            return;
        }
        if (this.goodsDeliveryNoteId) {
            this.subscribers.updateRecommendedQuantity = this.goodsDeliveryNoteService.updateRecommendedQuantity(goodsDeliveryNoteDetail.id, goodsDeliveryNoteDetail.recommendedQuantity, goodsDeliveryNoteDetail.concurrencyStamp)
                .subscribe(function (result) {
                _this.toastr.success(result.message);
                goodsDeliveryNoteDetail.concurrencyStamp = result.data;
            });
        }
    };
    GoodsDeliveryNoteProductComponent.prototype.edit = function (value) {
        this.isUpdate = true;
        this.goodsDeliveryNoteDetailId = value.id;
        this.productId = value.productId;
        this.model.patchValue(value);
        this.model.patchValue({ warehouseId: this.warehouseId, deliveryDate: this.deliveryDate });
        this.utilService.focusElement('quantityUpdate');
        this.getProductUnit(value.productId);
    };
    GoodsDeliveryNoteProductComponent.prototype.search = function (currentPage) {
        var _this = this;
        this.currentPage = currentPage;
        this.isSearching = true;
        this.goodsDeliveryNoteService.searchProduct(this.goodsDeliveryNoteId, this.keyword, this.currentPage, 1000)
            .subscribe(function (result) {
            _this.listGoodsDeliveryNoteDetail = result.items;
            _this.totalRows = result.totalRows;
            _this.calculateAmount();
        });
    };
    GoodsDeliveryNoteProductComponent.prototype.resetFormSearch = function () {
        this.keyword = '';
        this.search(1);
    };
    GoodsDeliveryNoteProductComponent.prototype.searchProduct = function (value) {
        var _this = this;
        this.productService.suggestions(value, 20).subscribe(function (result) {
            _this.listProductSuggestion = result.items;
        });
    };
    GoodsDeliveryNoteProductComponent.prototype.selectProductInForm = function (value) {
        if (value) {
            var existsProduct = lodash__WEBPACK_IMPORTED_MODULE_9__["find"](this.listGoodsDeliveryNoteDetail, function (item) {
                return item.productId === value.id;
            });
            if (existsProduct && !this.isUpdate) {
                this.toastr.error('Product already exists');
                this.model.patchValue({ productId: '' });
                return;
            }
            else {
                this.utilService.focusElement('quantity');
                this.model.patchValue({ productName: value.name, productId: value.id, totalAmounts: 0 });
                this.getProductUnit(value.id.toString());
            }
        }
    };
    GoodsDeliveryNoteProductComponent.prototype.getProductUnit = function (productId) {
        var _this = this;
        this.goodsDeliveryNoteService.getProductInfoDelivery(this.warehouseId ? this.warehouseId : '', productId)
            .subscribe(function (result) {
            var data = result.data;
            if (data) {
                _this.units = data.units;
                _this.calculatorMethod = data.calculatorMethod;
                _this.model.patchValue({ unitId: data.unitDefaultId, unitName: data.unitDefaultName });
                if (_this.deliveryType === _shareds_constants_deliveryType_const__WEBPACK_IMPORTED_MODULE_13__["DeliveryType"].retail) {
                    _this.model.patchValue({ price: data.priceRetail });
                }
                else {
                    _this.model.patchValue({ price: data.exWarehousePrice });
                }
            }
        });
    };
    GoodsDeliveryNoteProductComponent.prototype.onUnitSelected = function (goodsDeliveryNoteDetail, unit) {
        var _this = this;
        this.oldUnitId = goodsDeliveryNoteDetail.unitId;
        this.oldUnitName = goodsDeliveryNoteDetail.unitName;
        goodsDeliveryNoteDetail.unitId = unit.unitId;
        goodsDeliveryNoteDetail.unitName = unit.name;
        var conversion = unit.conversionValue;
        if (conversion > 0) {
            if (unit.isDefault) {
                goodsDeliveryNoteDetail.conversionInventory = goodsDeliveryNoteDetail.inventoryQuantity;
                goodsDeliveryNoteDetail.conversionPrice = goodsDeliveryNoteDetail.price;
                goodsDeliveryNoteDetail.totalAmounts = goodsDeliveryNoteDetail.quantity * goodsDeliveryNoteDetail.price;
            }
            else {
                goodsDeliveryNoteDetail.conversionInventory = goodsDeliveryNoteDetail.inventoryQuantity / conversion;
                goodsDeliveryNoteDetail.conversionPrice = conversion * goodsDeliveryNoteDetail.price;
                goodsDeliveryNoteDetail.totalAmounts = goodsDeliveryNoteDetail.quantity * goodsDeliveryNoteDetail.conversionPrice;
            }
            this.calculateAmount();
        }
        if (this.goodsDeliveryNoteId) {
            this.goodsDeliveryNoteService.updateDetail(this.goodsDeliveryNoteId, goodsDeliveryNoteDetail.id, goodsDeliveryNoteDetail)
                .subscribe(function (result) {
                _this.toastr.success(result.message);
                goodsDeliveryNoteDetail.concurrencyStamp = result.data;
            }, function () {
                goodsDeliveryNoteDetail.unitId = _this.oldUnitId;
                goodsDeliveryNoteDetail.unitName = _this.oldUnitName;
            });
        }
    };
    GoodsDeliveryNoteProductComponent.prototype.onLotKeyPressed = function (keyword) {
        this.model.patchValue({ lotId: keyword });
    };
    GoodsDeliveryNoteProductComponent.prototype.onLotSelected = function (lot) {
        if (lot) {
            this.model.patchValue({ lotId: lot.id });
        }
    };
    GoodsDeliveryNoteProductComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            this.goodsDeliveryNoteDetail = this.model.value;
            this.goodsDeliveryNoteDetail.totalAmounts = this.goodsDeliveryNoteDetail.quantity * this.goodsDeliveryNoteDetail.price;
            if (this.goodsDeliveryNoteDetail.inventoryQuantity < 1) {
                this.goodsDeliveryNoteDetail.quantity = this.goodsDeliveryNoteDetail.conversionInventory;
            }
            else if (this.goodsDeliveryNoteDetail.quantity > this.goodsDeliveryNoteDetail.inventoryQuantity) {
                this.toastr.warning('Số lượng thực xuất phải nhỏ hơn số lượng tồn kho.');
                return;
            }
            if (this.goodsDeliveryNoteId) {
                if (this.isUpdate) {
                    this.goodsDeliveryNoteService.updateProduct(this.goodsDeliveryNoteId, this.goodsDeliveryNoteDetailId, this.goodsDeliveryNoteDetail).subscribe(function () {
                        _this.resetForm();
                        _this.search(1);
                    });
                }
                else {
                    this.goodsDeliveryNoteService.insertProduct(this.goodsDeliveryNoteId, this.goodsDeliveryNoteDetail)
                        .subscribe(function (result) {
                        _this.resetForm();
                        _this.id = result.data;
                        _this.goodsDeliveryNoteDetail.id = result.data.id;
                        _this.goodsDeliveryNoteDetail.concurrencyStamp = result.data.concurrencyStamp;
                        _this.listGoodsDeliveryNoteDetail = _this.listGoodsDeliveryNoteDetail.concat([lodash__WEBPACK_IMPORTED_MODULE_9__["cloneDeep"](_this.goodsDeliveryNoteDetail)]);
                        _this.calculateAmount();
                    });
                }
            }
            else {
                if (this.isUpdate) {
                    var goodsDeliveryNoteDetails = lodash__WEBPACK_IMPORTED_MODULE_9__["filter"](this.listGoodsDeliveryNoteDetail, function (detail) {
                        return detail.productId === _this.productId;
                    });
                    if (goodsDeliveryNoteDetails) {
                        var value = goodsDeliveryNoteDetails[0];
                        value.unitId = this.goodsDeliveryNoteDetail.unitId;
                        value.unitName = this.goodsDeliveryNoteDetail.unitName;
                        value.price = this.goodsDeliveryNoteDetail.price;
                        value.totalAmounts = this.goodsDeliveryNoteDetail.totalAmounts;
                        value.quantity = this.goodsDeliveryNoteDetail.quantity;
                        this.calculateAmount();
                        // this.selectProduct.emit(this.listGoodsDeliveryNoteDetail);
                        this.resetForm();
                        // _.each(this.listGoodsDeliveryNoteDetail, (goodsDeliveryDetail: GoodsDeliveryNoteDetail) => {
                        //     goodsDeliveryDetail.isEdit = false;
                        // });
                    }
                }
                else {
                    this.listGoodsDeliveryNoteDetail = this.listGoodsDeliveryNoteDetail.concat([lodash__WEBPACK_IMPORTED_MODULE_9__["cloneDeep"](this.goodsDeliveryNoteDetail)]);
                    this.resetForm();
                    this.calculateAmount();
                    // this.selectProduct.emit(this.listGoodsDeliveryNoteDetail);
                }
            }
        }
    };
    GoodsDeliveryNoteProductComponent.prototype.cancel = function () {
        this.isUpdate = false;
        this.resetForm();
        // _.each(this.listGoodsDeliveryNoteDetail, (item: GoodsDeliveryNoteDetail) => {
        //     item.isEdit = false;
        // });
    };
    GoodsDeliveryNoteProductComponent.prototype.confirm = function () {
        var _this = this;
        this.goodsDeliveryNoteDetailId = this.goodsDeliveryNoteDetail.id;
        this.productId = this.goodsDeliveryNoteDetail.productId;
        this.goodsDeliveryNoteService.deleteDetail(this.goodsDeliveryNoteId, this.goodsDeliveryNoteDetail.id)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
            lodash__WEBPACK_IMPORTED_MODULE_9__["remove"](_this.listGoodsDeliveryNoteDetail, _this.goodsDeliveryNoteDetail);
        });
    };
    GoodsDeliveryNoteProductComponent.prototype.remove = function (goodsDeliveryNoteDetail) {
        this.goodsDeliveryNoteDetail = goodsDeliveryNoteDetail;
        if (this.goodsDeliveryNoteId) {
            this.swalConfirmDelete.show();
        }
        else {
            this.listGoodsDeliveryNoteDetail.splice(lodash__WEBPACK_IMPORTED_MODULE_9__["indexOf"](this.listGoodsDeliveryNoteDetail, goodsDeliveryNoteDetail), 1);
        }
    };
    GoodsDeliveryNoteProductComponent.prototype.calculateAmount = function () {
        if (this.listGoodsDeliveryNoteDetail && this.listGoodsDeliveryNoteDetail.length > 0) {
            this.calculateTotalAmounts();
            this.totalAmountChanged.emit(this.totalAmounts);
        }
        else {
            this.totalAmountChanged.emit(0);
        }
    };
    GoodsDeliveryNoteProductComponent.prototype.calculatorTotalAmount = function (quantity) {
        var price = this.model.value.price;
        this.totalAmounts = price && quantity ? price * quantity : 0;
    };
    GoodsDeliveryNoteProductComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.utilService.renderFormError(['goodsReceiptNoteDetailCode', 'productId', 'quantity', 'unitId']);
        this.validationMessages = this.utilService.renderFormErrorMessage([
            { 'goodsReceiptNoteDetailCode': ['required', 'maxlength'] },
            { 'productId': ['required'] },
            { 'quantity': ['required', 'isValid', 'lessThan', 'greaterThan'] },
            { 'discountPrice': ['isValid', 'greaterThan'] },
            { 'discountByPercent': ['isValid', 'lessThan', 'greaterThan'] },
            { 'recommendedQuantity': ['isValid', 'lessThan', 'greaterThan'] },
            { 'realQuantity': ['isValid', 'lessThan', 'greaterThan'] },
            { 'unitId': ['required'] },
        ]);
        this.model = this.fb.group({
            goodsReceiptNoteDetailCode: [this.goodsDeliveryNoteDetail.goodsReceiptNoteDetailCode, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(50)
                ]],
            goodsDeliveryNoteId: [this.goodsDeliveryNoteId],
            warehouseId: [this.warehouseId],
            deliveryDate: [this.deliveryDate],
            productId: [this.goodsDeliveryNoteDetail.productId, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
            productName: [this.goodsDeliveryNoteDetail.productName],
            quantity: [this.goodsDeliveryNoteDetail.quantity, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, this.numberValidator.isValid,
                    this.numberValidator.lessThan(2147483648), this.numberValidator.greaterThan(0)]],
            unitId: [this.goodsDeliveryNoteDetail.unitId, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
            unitName: [this.goodsDeliveryNoteDetail.unitName],
            price: [this.goodsDeliveryNoteDetail.price],
            expiredDate: [this.goodsDeliveryNoteDetail.expiredDate],
            totalAmounts: [this.goodsDeliveryNoteDetail.totalAmounts],
            discountPrice: [this.goodsDeliveryNoteDetail.discountPrice, [this.numberValidator.isValid,
                    this.numberValidator.greaterThan(0)]],
            discountByPercent: [this.goodsDeliveryNoteDetail.discountByPercent, [this.numberValidator.isValid,
                    this.numberValidator.lessThan(100), this.numberValidator.greaterThan(0)]],
            recommendedQuantity: [this.goodsDeliveryNoteDetail.recommendedQuantity, [this.numberValidator.isValid,
                    this.numberValidator.lessThan(2147483648), this.numberValidator.greaterThan(0)]],
            lotId: [this.goodsDeliveryNoteDetail.lotId],
            concurrencyStamp: [this.goodsDeliveryNoteDetail.concurrencyStamp],
            inventoryQuantity: [this.goodsDeliveryNoteDetail.inventoryQuantity],
            conversionInventory: [this.goodsDeliveryNoteDetail.conversionInventory],
            conversionPrice: [this.goodsDeliveryNoteDetail.conversionPrice],
            units: [[]]
        });
        this.model.valueChanges.subscribe(function (data) { return _this.validateModel(false); });
    };
    GoodsDeliveryNoteProductComponent.prototype.resetForm = function () {
        this.isUpdate = false;
        this.id = null;
        this.model.patchValue({
            productId: '',
            productName: '',
            quantity: '',
            unitId: '',
            unitName: '',
            price: '',
            expiredDate: '',
            lotId: '',
            recommendedQuantity: '',
            inventoryQuantity: null,
            goodsReceiptNoteDetailCode: '',
            conversionInventory: null,
            conversionPrice: null,
            units: []
        });
        if (this.productSuggestion) {
            this.productSuggestion.clear();
        }
        this.clearFormError(this.formErrors);
    };
    GoodsDeliveryNoteProductComponent.prototype.getProduct = function () {
        var _this = this;
        this.subscribers.getCode = this.model.get('goodsReceiptNoteDetailCode')
            .valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_15__["debounceTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_15__["distinctUntilChanged"])())
            .subscribe(function (value) {
            if (!value) {
                return;
            }
            if (!_this.warehouseId) {
                _this.toastr.warning('Vui lòng chọn kho xuất.');
                return;
            }
            if (_this.deliveryType == null || _this.deliveryType === undefined) {
                _this.toastr.warning('Vui lòng chọn hình thức xuất.');
                return;
            }
            _this.goodsReceiptNoteService.getProductInfoByCode(value, _this.warehouseId, _this.deliveryType, _this.deliveryDate)
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_15__["finalize"])(function () { return _this.model.patchValue({ 'goodsReceiptNoteDetailCode': '' }); }))
                .subscribe(function (result) {
                _this.goodsDeliveryNoteDetail = new _model_goods_delivery_note_details_model__WEBPACK_IMPORTED_MODULE_6__["GoodsDeliveryNoteDetail"](null, result.code, result.productId, result.productName, result.warehouseId, result.warehouseName, result.unitId, result.unitName, result.price, 1, 0, result.lotId, result.inventoryQuantity, value.toUpperCase(), '', result.units);
                // this.goodsDeliveryNoteDetail.realInventoryQuantity = result.realInventoryQuantity;
                _this.model.patchValue(_this.goodsDeliveryNoteDetail);
                _this.save();
            });
        });
    };
    GoodsDeliveryNoteProductComponent.prototype.calculateTotalAmounts = function () {
        this.totalAmounts = lodash__WEBPACK_IMPORTED_MODULE_9__["sumBy"](this.listGoodsDeliveryNoteDetail, function (item) {
            return parseFloat((item.totalAmounts ? item.totalAmounts : 0).toString());
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('confirmDeleteGoodsDeliveryNotesDetail'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_11__["SwalComponent"])
    ], GoodsDeliveryNoteProductComponent.prototype, "swalConfirmDelete", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_product_product_product_suggestion_product_suggestion_component__WEBPACK_IMPORTED_MODULE_14__["ProductSuggestionComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _product_product_product_suggestion_product_suggestion_component__WEBPACK_IMPORTED_MODULE_14__["ProductSuggestionComponent"])
    ], GoodsDeliveryNoteProductComponent.prototype, "productSuggestion", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GoodsDeliveryNoteProductComponent.prototype, "goodsDeliveryNoteId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GoodsDeliveryNoteProductComponent.prototype, "warehouseId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GoodsDeliveryNoteProductComponent.prototype, "totalAmounts", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GoodsDeliveryNoteProductComponent.prototype, "totalQuantity", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GoodsDeliveryNoteProductComponent.prototype, "deliveryType", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GoodsDeliveryNoteProductComponent.prototype, "deliveryDate", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GoodsDeliveryNoteProductComponent.prototype, "readonly", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GoodsDeliveryNoteProductComponent.prototype, "selectProduct", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GoodsDeliveryNoteProductComponent.prototype, "totalAmountChanged", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Array])
    ], GoodsDeliveryNoteProductComponent.prototype, "listGoodsDeliveryNoteDetail", null);
    GoodsDeliveryNoteProductComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-goods-delivery-note-product',
            template: __webpack_require__(/*! ./goods-delivery-note-product.component.html */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-form/list-product/goods-delivery-note-product.component.html"),
            providers: [_validators_number_validator__WEBPACK_IMPORTED_MODULE_7__["NumberValidator"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_8__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _product_product_service_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"],
            _validators_number_validator__WEBPACK_IMPORTED_MODULE_7__["NumberValidator"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_5__["UtilService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_12__["ToastrService"],
            _goods_delivery_note_service__WEBPACK_IMPORTED_MODULE_10__["GoodsDeliveryNoteService"],
            _goods_receipt_note_goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_16__["GoodsReceiptNoteService"]])
    ], GoodsDeliveryNoteProductComponent);
    return GoodsDeliveryNoteProductComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_3__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-print/goods-delivery-note-print.component.html":
/*!********************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-print/goods-delivery-note-print.component.html ***!
  \********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"display: none;\" #printArea>\r\n    <div class=\"print-page\">\r\n        <header>\r\n            <img src=\"{{imageUrl}}/assets/images/print/print-header.jpg\" alt=\"\">\r\n        </header>\r\n        <table class=\"w100pc\">\r\n            <tr style=\"margin-bottom: 20px;\">\r\n                <td colspan=\"3\">\r\n                    <h4 class=\"receipt-title\">Phiếu Xuất</h4>\r\n                    <div class=\"center\">\r\n                        Ngày {{ day < 10 ? '0' + day : day }}\r\n                        tháng {{ month < 10 ? '0' + month : month\r\n                        }} năm {{ year }}\r\n                    </div>\r\n                </td>\r\n            </tr>\r\n            <tr>\r\n                <td colspan=\"3\">\r\n                    <div class=\"control-group\">\r\n                        <label for=\"\">Lý do xuất:</label>\r\n                        <div class=\"dotted-control\">{{ goodsDeliveryNoteDetail?.reason }}</div>\r\n                    </div>\r\n                </td>\r\n            </tr>\r\n            <tr>\r\n                <td colspan=\"3\">\r\n                    <div class=\"control-group\">\r\n                        <label for=\"\">Xuất tại kho:</label>\r\n                        <div class=\"dotted-control\">{{ goodsDeliveryNoteDetail?.warehouseName }}</div>\r\n                    </div>\r\n                </td>\r\n            </tr>\r\n        </table>\r\n        <table class=\"w100pc bordered\">\r\n            <thead>\r\n            <tr>\r\n                <th rowspan=\"2\" class=\"center w50\">STT</th>\r\n                <th rowspan=\"2\" class=\"center\">Sản phẩm</th>\r\n                <th rowspan=\"2\" class=\"center w70\">Mã sản phẩm</th>\r\n                <th rowspan=\"2\" class=\"center w70\">Lô SX</th>\r\n                <th rowspan=\"2\" class=\"center w100\">Hạn SD</th>\r\n                <th rowspan=\"2\" class=\"center w70\">Đơn vị tính</th>\r\n                <th colspan=\"2\" class=\"center\">Số lượng</th>\r\n                <th rowspan=\"2\" class=\"center w70\">Đơn giá (đồng)</th>\r\n                <th rowspan=\"2\" class=\"center w70\">Tổng tiền</th>\r\n            </tr>\r\n            <tr>\r\n                <th class=\"center w70\">Đề xuất</th>\r\n                <th class=\"center w70\">Thực tế</th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            <tr *ngFor=\"let item of listGoodsDeliveryNoteDetail; let i = index\">\r\n                <td>{{ (i + 1) }}</td>\r\n                <td>{{ item.productName }}</td>\r\n                <td>{{ item.productId }}</td>\r\n                <td>{{ item.lotId }}</td>\r\n                <td>{{ item.expiryDate | dateTimeFormat:'DD/MM/YYYY' }}</td>\r\n                <td>{{ item.unitName }}</td>\r\n                <td>{{ item.realQuantity | ghmCurrency:0 }}</td>\r\n                <td>{{ item.quantity | ghmCurrency:0 }}</td>\r\n                <td>{{ item.price | ghmCurrency:0 }}</td>\r\n                <td>{{ item.totalAmounts | ghmCurrency:0 }}</td>\r\n            </tr>\r\n            <tr>\r\n                <td class=\"text-right\" colspan=\"8\" i18n=\"@@totalAmounts\">Tổng số</td>\r\n                <td class=\"text-right bold\" colspan=\"2\">{{ totalQuantity | formatNumber:0 }}</td>\r\n            </tr>\r\n            <tr>\r\n                <td class=\"text-right\" colspan=\"8\" i18n=\"@@totalAmounts\">Total amounts</td>\r\n                <td class=\"text-right bold\" colspan=\"2\">{{ goodsDeliveryNoteDetail?.totalAmounts | ghmCurrency:0 }}</td>\r\n            </tr>\r\n            </tbody>\r\n        </table>\r\n        <table class=\"w100pc\">\r\n            <tr>\r\n                <td>\r\n                    <div class=\"control-group\">\r\n                        <label for=\"\">Tổng số tiền (Viết bằng chữ):</label>\r\n                        <div class=\"dotted-control\">{{ goodsDeliveryNoteDetail?.totalAmounts | ghmAmountToWord }}</div>\r\n                    </div>\r\n                </td>\r\n            </tr>\r\n        </table>\r\n        <table class=\"w100pc\" style=\"margin-top: 10px;\">\r\n            <tr>\r\n                <td class=\"center\">Người lập bảng</td>\r\n                <td class=\"center\">Người giao hàng</td>\r\n                <td class=\"center\">Thủ kho</td>\r\n                <td class=\"center\">Kế toán trưởng</td>\r\n                <td></td>\r\n            </tr>\r\n            <tr>\r\n                <td style=\"min-height: 100px; height: 100px;\"></td>\r\n                <td style=\"min-height: 100px; height: 100px;\"></td>\r\n                <td style=\"min-height: 100px; height: 100px;\"></td>\r\n            </tr>\r\n            </tbody>\r\n            <tfoot>\r\n            <tr>\r\n                <td class=\"center\">{{ currentUser?.fullName }}</td>\r\n                <td class=\"center\">{{ goodsDeliveryNoteDetail?.receiverFullName }}</td>\r\n                <td class=\"center\"></td>\r\n                <td class=\"center\"></td>\r\n            </tr>\r\n            </tfoot>\r\n        </table>\r\n        <div class=\"page-break\"></div>\r\n        <footer>\r\n            <img src=\"{{imageUrl}}/assets/images/print/print-footer.jpg\" alt=\"\">\r\n        </footer>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-print/goods-delivery-note-print.component.ts":
/*!******************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-print/goods-delivery-note-print.component.ts ***!
  \******************************************************************************************************************************/
/*! exports provided: GoodsDeliveryNotePrintComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsDeliveryNotePrintComponent", function() { return GoodsDeliveryNotePrintComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _goods_delivery_note_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../goods-delivery-note.service */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.service.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _shareds_services_helper_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../shareds/services/helper.service */ "./src/app/shareds/services/helper.service.ts");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _shareds_services_app_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../shareds/services/app.service */ "./src/app/shareds/services/app.service.ts");







var GoodsDeliveryNotePrintComponent = /** @class */ (function () {
    function GoodsDeliveryNotePrintComponent(appConfig, helperService, appService, goodsDeliveryNoteService) {
        this.appConfig = appConfig;
        this.helperService = helperService;
        this.appService = appService;
        this.goodsDeliveryNoteService = goodsDeliveryNoteService;
        this.currentUser = appService.currentUser;
        this.imageUrl = this.appConfig.CORE_API_URL;
    }
    GoodsDeliveryNotePrintComponent.prototype.print = function (id) {
        var _this = this;
        this.goodsDeliveryNoteService.getDetail(id).subscribe(function (result) {
            var data = result.data;
            if (data) {
                _this.goodsDeliveryNoteDetail = data;
                _this.listGoodsDeliveryNoteDetail = _this.goodsDeliveryNoteDetail.goodsDeliveryNoteDetails;
                _this.totalQuantity = lodash__WEBPACK_IMPORTED_MODULE_3__["sumBy"](_this.listGoodsDeliveryNoteDetail, function (item) {
                    return item.quantity;
                });
                _this.day = new Date(_this.goodsDeliveryNoteDetail.deliveryDate).getDay();
                _this.month = new Date(_this.goodsDeliveryNoteDetail.deliveryDate).getMonth();
                _this.year = new Date(_this.goodsDeliveryNoteDetail.deliveryDate).getFullYear();
                setTimeout(function () {
                    _this.executePrint();
                }, 100);
            }
        });
    };
    GoodsDeliveryNotePrintComponent.prototype.executePrint = function () {
        var style = "\n                     h4.receipt-title {\n                            font-size: 25px;\n                            text-align: center;\n                            font-weight: bold;\n                            text-transform: uppercase;\n                            margin-top: 10px;\n                      }\n                     .amountsInWord {\n                        width: 200px;\n                     }\n                     ";
        var content = this.printArea.nativeElement.innerHTML;
        this.helperService.openPrintWindow('Phiếu xuất kho', content, style);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('printArea'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], GoodsDeliveryNotePrintComponent.prototype, "printArea", void 0);
    GoodsDeliveryNotePrintComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-goods-note-delivery-print',
            template: __webpack_require__(/*! ./goods-delivery-note-print.component.html */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-print/goods-delivery-note-print.component.html"),
            providers: [_shareds_services_helper_service__WEBPACK_IMPORTED_MODULE_4__["HelperService"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_5__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _shareds_services_helper_service__WEBPACK_IMPORTED_MODULE_4__["HelperService"],
            _shareds_services_app_service__WEBPACK_IMPORTED_MODULE_6__["AppService"],
            _goods_delivery_note_service__WEBPACK_IMPORTED_MODULE_2__["GoodsDeliveryNoteService"]])
    ], GoodsDeliveryNotePrintComponent);
    return GoodsDeliveryNotePrintComponent;
}());



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-type.const.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-type.const.ts ***!
  \***********************************************************************************************/
/*! exports provided: GoodsDeliveryNoteType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsDeliveryNoteType", function() { return GoodsDeliveryNoteType; });
var GoodsDeliveryNoteType;
(function (GoodsDeliveryNoteType) {
    GoodsDeliveryNoteType[GoodsDeliveryNoteType["retail"] = 0] = "retail";
    GoodsDeliveryNoteType[GoodsDeliveryNoteType["selfConsumer"] = 1] = "selfConsumer";
    GoodsDeliveryNoteType[GoodsDeliveryNoteType["return"] = 2] = "return";
    GoodsDeliveryNoteType[GoodsDeliveryNoteType["voided"] = 3] = "voided";
    GoodsDeliveryNoteType[GoodsDeliveryNoteType["transfer"] = 4] = "transfer";
    GoodsDeliveryNoteType[GoodsDeliveryNoteType["inventory"] = 5] = "inventory";
})(GoodsDeliveryNoteType || (GoodsDeliveryNoteType = {}));


/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.component.html":
/*!************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@listGoodsDeliveryNoteTitle\">Danh sách phiếu xuất kho</span>\r\n    <small i18n=\"@@goodsNoteModuleTitle\">Quản lý kho</small>\r\n</h1>\r\n<form class=\"form-inline cm-mgb-10\" (ngSubmit)=\"search(1)\">\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <app-warehouse-suggestion\r\n            (itemSelected)=\"selectWarehouse($event)\"\r\n            (itemRemoved)=\"onWarehouseRemoved()\"></app-warehouse-suggestion>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <nh-date placeholder=\"Từ ngày\"\r\n                 i18n-placeholder=\"@@fromDate\"\r\n                 [(ngModel)]=\"fromDate\"\r\n                 [allowRemove]=\"true\"\r\n                 [mask]=\"true\"\r\n                 [showTime]=\"true\"\r\n                 (selectedDateEvent)=\"search(1)\"\r\n                 name=\"fromDate\"></nh-date>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <nh-date placeholder=\"Đến ngày\"\r\n                 i18n-placeholder=\"@@toDate\"\r\n                 [(ngModel)]=\"toDate\"\r\n                 [allowRemove]=\"true\"\r\n                 [mask]=\"true\"\r\n                 [showTime]=\"true\"\r\n                 (selectedDateEvent)=\"search(1)\"\r\n                 name=\"toDate\"></nh-date>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <nh-select\r\n            [title]=\"'Tất cả'\"\r\n            i18n-title=\"@@allFilter\"\r\n            [data]=\"listDeliveryType\"\r\n            [value]=\"type\"\r\n            (onSelectItem)=\"selectDeliveryType($event)\"\r\n        ></nh-select>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <input type=\"text\" class=\"form-control\" i18n=\"@@keywordSearch\" i18n-placeholder\r\n               placeholder=\"Nhập từ khóa tìm kiếm.\"\r\n               name=\"searchInput\" [(ngModel)]=\"keyword\">\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <button class=\"btn blue\" type=\"submit\">\r\n            <i class=\"fa fa-search\" *ngIf=\"!isSearching\"></i>\r\n            <i class=\"fa fa-pulse fa-spinner\" *ngIf=\"isSearching\"></i>\r\n        </button>\r\n    </div>\r\n    <div class=\"form-group cm-mgl-5\">\r\n        <button class=\"btn default\" type=\"button\" (click)=\"resetFormSearch()\">\r\n            <i class=\"fa fa-refresh\"></i>\r\n        </button>\r\n    </div>\r\n    <div class=\"form-group pull-right\">\r\n        <nh-dropdown *ngIf=\"permission.add\">\r\n            <button type=\"button\" class=\"btn blue\" (click)=\"add()\">\r\n                <span i18n=\"@@add\">Thêm</span>\r\n            </button>\r\n            <ul class=\"nh-dropdown-menu right\" role=\"menu\">\r\n                <li>\r\n                    <a href=\"javascript://\">\r\n                        <span i18n=\"@@retailDelivery\">Xuất bán</span>\r\n                    </a>\r\n                </li>\r\n                <li>\r\n                    <a href=\"javascript://\">\r\n                        <span i18n=\"@@transferDelivery\">Xuất điều chuyển</span>\r\n                    </a>\r\n                </li>\r\n                <li>\r\n                    <a href=\"javascript://\">\r\n                        <span i18n=\"@@voidedDelivery\">Xuất hủy</span>\r\n                    </a>\r\n                </li>\r\n            </ul>\r\n        </nh-dropdown>\r\n    </div>\r\n</form>\r\n<table class=\"table table-striped table-hover\">\r\n    <thead>\r\n    <tr>\r\n        <th class=\"middle center w50\" i18n=\"@@no\">STT</th>\r\n        <th class=\"middle\" i18n=\"@@deliveryType\">Hình thức xuất</th>\r\n        <th class=\"middle text-right \" i18n=\"@@totalAmount\">Tổng tiền</th>\r\n        <th class=\"middle\" i18n=\"@@issuePerson\">Người xuất</th>\r\n        <th class=\"middle\" i18n=\"@@deliveryDate\">Ngày xuất</th>\r\n        <th class=\"middle\" i18n=\"@@warehouse\">Kho xuất</th>\r\n        <th class=\"middle text-right w150\" i18n=\"@@action\" *ngIf=\"permission.edit || permission.delete\">Actions</th>\r\n    </tr>\r\n    </thead>\r\n    <tbody>\r\n    <tr *ngFor=\"let item of listItems$ | async; let i = index\">\r\n        <td class=\"middle center\">{{(currentPage - 1) * pageSize + i + 1}}</td>\r\n        <td class=\"middle\">\r\n            <span class=\"badge\"\r\n                  [class.badge-success]=\"item.type === deliveryType.retail\"\r\n                  [class.badge-info]=\"item.type === deliveryType.selfConsumer\"\r\n                  [class.badge-warning]=\"item.type === deliveryType.return\"\r\n                  [class.badge-danger]=\"item.type === deliveryType.voided\"\r\n                  [class.badge-defaut]=\"item.type === deliveryType.transfer\"\r\n                  [class.badge-defaut]=\"item.type === deliveryType.inventory\"\r\n                  i18n=\"@@goodsDeliveryNoteTypeValue\">\r\n                {item.type, select, 0 {Bán} 1 {Sử dụng} 2 {Trả nhà cung cấp} 3 {Hủy} 4 {Điều chuyển} 5 {Kiểm kê} other {}}\r\n            </span>\r\n        </td>\r\n        <td class=\"middle text-right\">{{ item.totalAmounts | ghmCurrency:0 }}</td>\r\n        <td class=\"middle\">{{ item.creatorFullName }}</td>\r\n        <td class=\"middle\">{{ item.deliveryDate | dateTimeFormat : 'DD/MM/YYYY HH:mm' }}</td>\r\n        <td class=\"middle\">{{ item.warehouseName }}</td>\r\n        <td class=\"text-right middle\" *ngIf=\"permission.edit || permission.delete\">\r\n            <nh-dropdown>\r\n                <button type=\"button\" class=\"btn btn-sm btn-light btn-no-background no-border\" matTooltip=\"Menu\">\r\n                    <mat-icon>more_horiz</mat-icon>\r\n                </button>\r\n                <ul class=\"nh-dropdown-menu right\" role=\"menu\">\r\n                    <li>\r\n                        <a *ngIf=\"permission.edit\"\r\n                           (click)=\"detail(item)\"\r\n                           i18n=\"@@edit\">\r\n                            <!--<i class=\"fa fa-eye\"></i>-->\r\n                            <mat-icon class=\"menu-icon\">info</mat-icon>\r\n                            Chi tiết\r\n                        </a>\r\n                    </li>\r\n                    <li>\r\n                        <a *ngIf=\"permission.edit && item.type !== goodsDeliveryNoteType.inventory\"\r\n                           (click)=\"edit(item)\"\r\n                           i18n=\"@@edit\">\r\n                            <!--<i class=\"fa fa-edit\"></i>-->\r\n                            <mat-icon class=\"menu-icon\">edit</mat-icon>\r\n                            Chỉnh sửa\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </nh-dropdown>\r\n        </td>\r\n    </tr>\r\n    </tbody>\r\n</table>\r\n\r\n<ghm-paging\r\n    class=\"pull-right\"\r\n    [totalRows]=\"totalRows\"\r\n    [currentPage]=\"currentPage\"\r\n    [pageShow]=\"6\"\r\n    [pageSize]=\"pageSize\"\r\n    (pageClick)=\"search($event)\"\r\n    i18n=\"@@product\" i18n-pageName\r\n    [pageName]=\"'goodsDelivery'\">\r\n</ghm-paging>\r\n\r\n<swal\r\n    #confirmDelete\r\n    i18n=\"@@confirmDelete\"\r\n    i18n-title=\"@@confirmTitleDeleteGoodsDelivery\"\r\n    i18n-text=\"@@confirmTextDeleteGoodsDelivery\"\r\n    title=\"Are you sure for delete this goods delivery?\"\r\n    text=\"You can't recover this goods delivery after delete.\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Accept\"\r\n    cancelButtonText=\"Cancel\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<app-goods-delivery-note-form (saveSuccessful)=\"search(1)\"></app-goods-delivery-note-form>\r\n<app-goods-delivery-note-detail></app-goods-delivery-note-detail>\r\n<app-goods-note-delivery-print></app-goods-note-delivery-print>\r\n\r\n<nh-menu #nhMenu>\r\n    <nh-menu-item (clicked)=\"detail($event)\">\r\n        <mat-icon class=\"menu-icon\">info</mat-icon>\r\n        <span i18n=\"@@view\">Chi tiết</span>\r\n    </nh-menu-item>\r\n    <nh-menu-item (clicked)=\"edit($event)\">\r\n        <mat-icon class=\"menu-icon\">edit</mat-icon>\r\n        <span i18n=\"@@edit\">Chỉnh sửa</span>\r\n    </nh-menu-item>\r\n</nh-menu>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.component.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: GoodsDeliveryNoteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsDeliveryNoteComponent", function() { return GoodsDeliveryNoteComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _goods_delivery_note_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./goods-delivery-note.service */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.service.ts");
/* harmony import */ var _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../shareds/models/filter-link.model */ "./src/app/shareds/models/filter-link.model.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _configs_page_id_config__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../configs/page-id.config */ "./src/app/configs/page-id.config.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @toverux/ngx-sweetalert2 */ "./node_modules/@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _goods_delivery_note_form_goods_delivery_note_form_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./goods-delivery-note-form/goods-delivery-note-form.component */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-form/goods-delivery-note-form.component.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _goods_delivery_note_detail_goods_delivery_note_detail_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./goods-delivery-note-detail/goods-delivery-note-detail.component */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-detail/goods-delivery-note-detail.component.ts");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _shareds_constants_deliveryType_const__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../../../shareds/constants/deliveryType.const */ "./src/app/shareds/constants/deliveryType.const.ts");
/* harmony import */ var _goods_delivery_note_type_const__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./goods-delivery-note-type.const */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-type.const.ts");

















var GoodsDeliveryNoteComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](GoodsDeliveryNoteComponent, _super);
    function GoodsDeliveryNoteComponent(pageId, appConfig, location, route, router, goodsDeliveryNoteService, utilService) {
        var _this = _super.call(this) || this;
        _this.pageId = pageId;
        _this.appConfig = appConfig;
        _this.location = location;
        _this.route = route;
        _this.router = router;
        _this.goodsDeliveryNoteService = goodsDeliveryNoteService;
        _this.utilService = utilService;
        _this.listDeliveryType = _shareds_constants_deliveryType_const__WEBPACK_IMPORTED_MODULE_15__["DeliveryTypes"];
        _this.deliveryType = _shareds_constants_deliveryType_const__WEBPACK_IMPORTED_MODULE_15__["DeliveryType"];
        _this.goodsDeliveryNoteType = _goods_delivery_note_type_const__WEBPACK_IMPORTED_MODULE_16__["GoodsDeliveryNoteType"];
        _this.urlSearchWarehouse = appConfig.API_GATEWAY_URL + "api/v1/warehouse/warehouses/suggestions";
        _this.listItems$ = _this.route.data.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_12__["map"])(function (result) {
            var data = result.data;
            _this.totalRows = data.totalRows;
            return data.items;
        }));
        return _this;
    }
    GoodsDeliveryNoteComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.appService.setupPage(this.pageId.WAREHOUSE, this.pageId.GOODS_DELIVERY_NOTE, 'Quản lý xuất kho', 'Quản lý kho');
        this.subscribers.queryParams = this.route.queryParams.subscribe(function (params) {
            _this.keyword = params.keyword ? params.keyword : '';
            _this.fromDate = params.keyword ? params.fromDate : '';
            _this.toDate = params.toDate ? params.toDate : '';
            _this.type = params.type ? parseInt(params.type) : '';
            _this.warehouseId = params.warehouseId ? params.warehouseId : '';
            _this.currentPage = params.page ? parseInt(params.page) : 1;
            _this.pageSize = params.pageSize ? parseInt(params.pageSize) : _this.appConfig.PAGE_SIZE;
        });
    };
    GoodsDeliveryNoteComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.swalConfirmDelete.confirm.subscribe(function (result) {
            _this.delete(_this.goodsDeliveryNoteId);
        });
        // this.goodsDeliveryNoteFormComponent.add();
    };
    GoodsDeliveryNoteComponent.prototype.onWarehouseRemoved = function () {
        this.warehouseId = null;
        this.search();
    };
    GoodsDeliveryNoteComponent.prototype.add = function () {
        this.goodsDeliveryNoteFormComponent.add();
    };
    GoodsDeliveryNoteComponent.prototype.search = function (currentPage) {
        var _this = this;
        if (currentPage === void 0) { currentPage = 1; }
        this.currentPage = currentPage;
        this.isSearching = true;
        this.renderFilterLink();
        this.listItems$ = this.goodsDeliveryNoteService.search(this.keyword, this.fromDate, this.toDate, this.type, this.warehouseId, this.currentPage, this.pageSize)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_12__["finalize"])(function () {
            _this.isSearching = false;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_12__["map"])(function (result) {
            _this.totalRows = result.totalRows;
            return result.items;
        }));
    };
    GoodsDeliveryNoteComponent.prototype.onPageClick = function (page) {
        this.currentPage = page;
        this.search(1);
    };
    GoodsDeliveryNoteComponent.prototype.resetFormSearch = function () {
        this.keyword = '';
        this.search(1);
    };
    GoodsDeliveryNoteComponent.prototype.edit = function (goodsDeliveryNote) {
        this.goodsDeliveryNoteFormComponent.edit(goodsDeliveryNote.id);
    };
    GoodsDeliveryNoteComponent.prototype.delete = function (id) {
        var _this = this;
        this.goodsDeliveryNoteService.delete(id).subscribe(function () {
            // _.remove(this.listGoodsDeliveryNote, (item: GoodsDeliveryNoteSearchViewModel) => {
            //     return item.id === id;
            // });
            _this.search(1);
        });
    };
    GoodsDeliveryNoteComponent.prototype.detail = function (goodsDeliveryNote) {
        this.goodsDeliveryNoteDetailComponent.show(goodsDeliveryNote.id);
    };
    GoodsDeliveryNoteComponent.prototype.confirm = function (value) {
        this.swalConfirmDelete.show();
        this.goodsDeliveryNoteId = value.id;
    };
    GoodsDeliveryNoteComponent.prototype.selectWarehouse = function (value) {
        if (value) {
            this.warehouseId = value.id;
        }
        else {
            this.warehouseId = '';
        }
        this.search(1);
    };
    GoodsDeliveryNoteComponent.prototype.selectDeliveryType = function (value) {
        if (value) {
            this.type = value.id;
        }
        else {
            this.type = '';
        }
        this.search(1);
    };
    GoodsDeliveryNoteComponent.prototype.exportExcel = function (item) {
        this.goodsDeliveryNoteService.exportGoodsDeliveryDeltail(item.id)
            .subscribe(function (result) {
            file_saver__WEBPACK_IMPORTED_MODULE_14__["saveAs"](result, 'Phieu_xuat_kho.xlsx');
            var fileURL = URL.createObjectURL(result);
            window.open(fileURL);
        });
    };
    GoodsDeliveryNoteComponent.prototype.export = function () {
    };
    GoodsDeliveryNoteComponent.prototype.print = function (goodsDeliveryNoteDetail) {
        // console.log(goodsDeliveryNoteDetail);
    };
    GoodsDeliveryNoteComponent.prototype.renderFilterLink = function () {
        var path = '/goods/goods-delivery-notes';
        var query = this.utilService.renderLocationFilter([
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_4__["FilterLink"]('keyword', this.keyword),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_4__["FilterLink"]('fromDate', this.fromDate),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_4__["FilterLink"]('toDate', this.toDate),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_4__["FilterLink"]('type', this.type),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_4__["FilterLink"]('warehouseId', this.warehouseId),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_4__["FilterLink"]('page', this.currentPage),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_4__["FilterLink"]('pageSize', this.pageSize)
        ]);
        this.location.go(path, query);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('confirmDelete'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_9__["SwalComponent"])
    ], GoodsDeliveryNoteComponent.prototype, "swalConfirmDelete", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_goods_delivery_note_form_goods_delivery_note_form_component__WEBPACK_IMPORTED_MODULE_11__["GoodsDeliveryNoteFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _goods_delivery_note_form_goods_delivery_note_form_component__WEBPACK_IMPORTED_MODULE_11__["GoodsDeliveryNoteFormComponent"])
    ], GoodsDeliveryNoteComponent.prototype, "goodsDeliveryNoteFormComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_goods_delivery_note_detail_goods_delivery_note_detail_component__WEBPACK_IMPORTED_MODULE_13__["GoodsDeliveryNoteDetailComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _goods_delivery_note_detail_goods_delivery_note_detail_component__WEBPACK_IMPORTED_MODULE_13__["GoodsDeliveryNoteDetailComponent"])
    ], GoodsDeliveryNoteComponent.prototype, "goodsDeliveryNoteDetailComponent", void 0);
    GoodsDeliveryNoteComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-goods-delivery-note',
            template: __webpack_require__(/*! ./goods-delivery-note.component.html */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.component.html"),
            providers: [_goods_delivery_note_service__WEBPACK_IMPORTED_MODULE_3__["GoodsDeliveryNoteService"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_page_id_config__WEBPACK_IMPORTED_MODULE_7__["PAGE_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_6__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, Object, _angular_common__WEBPACK_IMPORTED_MODULE_10__["Location"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"],
            _goods_delivery_note_service__WEBPACK_IMPORTED_MODULE_3__["GoodsDeliveryNoteService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_5__["UtilService"]])
    ], GoodsDeliveryNoteComponent);
    return GoodsDeliveryNoteComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".goods-delivery-note-content .title {\n  font-size: 25px;\n  text-align: center;\n  font-weight: bold;\n  text-transform: uppercase;\n  margin-top: 10px; }\n\n.goods-delivery-note-content .delivery-date-wrapper {\n  display: flex;\n  text-align: center;\n  justify-content: center; }\n\n.goods-delivery-note-content .delivery-date-wrapper div {\n    width: 90px;\n    display: flex;\n    align-items: center; }\n\n.goods-delivery-note-content .red {\n  color: red; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy93YXJlaG91c2UvZ29vZHMvZ29vZHMtZGVsaXZlcnktbm90ZS9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXBwXFxtb2R1bGVzXFx3YXJlaG91c2VcXGdvb2RzXFxnb29kcy1kZWxpdmVyeS1ub3RlXFxnb29kcy1kZWxpdmVyeS1ub3RlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFFUSxlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQix5QkFBeUI7RUFDekIsZ0JBQWdCLEVBQUE7O0FBTnhCO0VBVVEsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQix1QkFBdUIsRUFBQTs7QUFaL0I7SUFlWSxXQUFXO0lBQ1gsYUFBYTtJQUNiLG1CQUFtQixFQUFBOztBQWpCL0I7RUFxQlEsVUFBVSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvbW9kdWxlcy93YXJlaG91c2UvZ29vZHMvZ29vZHMtZGVsaXZlcnktbm90ZS9nb29kcy1kZWxpdmVyeS1ub3RlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLmdvb2RzLWRlbGl2ZXJ5LW5vdGUtY29udGVudCB7XHJcbiAgICAudGl0bGUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5kZWxpdmVyeS1kYXRlLXdyYXBwZXIge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cclxuICAgICAgICBkaXYge1xyXG4gICAgICAgICAgICB3aWR0aDogOTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAucmVkIHtcclxuICAgICAgICBjb2xvcjogcmVkO1xyXG4gICAgfVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.service.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.service.ts ***!
  \********************************************************************************************/
/*! exports provided: GoodsDeliveryNoteService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsDeliveryNoteService", function() { return GoodsDeliveryNoteService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/internal/operators */ "./node_modules/rxjs/internal/operators/index.js");
/* harmony import */ var rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_6__);







var GoodsDeliveryNoteService = /** @class */ (function () {
    function GoodsDeliveryNoteService(appConfig, spinceService, http, spinnerService, toastr) {
        this.appConfig = appConfig;
        this.spinceService = spinceService;
        this.http = http;
        this.spinnerService = spinnerService;
        this.toastr = toastr;
        this.url = 'api/v1/warehouse/goods-delivery-notes';
        this.url = "" + appConfig.API_GATEWAY_URL + this.url;
    }
    GoodsDeliveryNoteService.prototype.resolve = function (route, state) {
        var queryParams = route.queryParams;
        return this.search(queryParams.keyword, queryParams.fromDate, queryParams.toDate, queryParams.type, queryParams.warehouseId, queryParams.page, queryParams.pageSize);
    };
    GoodsDeliveryNoteService.prototype.search = function (keyword, fromDate, todate, type, warehouseId, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = this.appConfig.PAGE_SIZE; }
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
            .set('keyword', keyword ? keyword : '')
            .set('fromDate', fromDate ? fromDate : '')
            .set('todate', todate ? todate : '')
            .set('type', type ? type.toString() : '')
            .set('warehouseId', warehouseId ? warehouseId : '')
            .set('page', page ? page.toString() : '1')
            .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString());
        return this.http.get("" + this.url, {
            params: params
        });
    };
    GoodsDeliveryNoteService.prototype.insert = function (goodsDeliveryNote) {
        var _this = this;
        return this.http.post("" + this.url, goodsDeliveryNote).pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    GoodsDeliveryNoteService.prototype.update = function (id, goodsDeliveryNote) {
        var _this = this;
        return this.http.post(this.url + "/" + id, goodsDeliveryNote).pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    GoodsDeliveryNoteService.prototype.delete = function (id) {
        var _this = this;
        return this.http.delete(this.url + "/" + id)
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    GoodsDeliveryNoteService.prototype.getDetail = function (id) {
        var _this = this;
        this.spinceService.show();
        return this.http.get(this.url + "/" + id, {})
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () {
            _this.spinceService.hide();
        }));
    };
    GoodsDeliveryNoteService.prototype.deleteDetail = function (goosdReceiptNoteId, id) {
        var _this = this;
        this.spinceService.show();
        return this.http.delete(this.url + "/" + goosdReceiptNoteId + "/details/" + id, {})
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () {
            _this.spinceService.hide();
        }));
    };
    GoodsDeliveryNoteService.prototype.searchProduct = function (goodsDeliveryNoteId, keyword, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = this.appConfig.PAGE_SIZE; }
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
            .set('keyword', keyword ? keyword : '')
            .set('page', page ? page.toString() : '1')
            .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString());
        return this.http.get(this.url + "/" + goodsDeliveryNoteId + "/details", {
            params: params
        });
    };
    GoodsDeliveryNoteService.prototype.insertProduct = function (goodsDeliveryNoteId, goodsDeliveryNoteDetail) {
        var _this = this;
        return this.http.post(this.url + "/" + goodsDeliveryNoteId + "/details", goodsDeliveryNoteDetail)
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    GoodsDeliveryNoteService.prototype.updateProduct = function (goodsDeliveryNoteId, productId, goodsDeliveryNoteDetail) {
        var _this = this;
        return this.http.post(this.url + "/" + goodsDeliveryNoteId + "/details/" + productId, goodsDeliveryNoteDetail)
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    GoodsDeliveryNoteService.prototype.deleteProduct = function (goodsDeliveryNoteId, goodsDeliveryNoteDetailId) {
        var _this = this;
        return this.http.delete(this.url + "/" + goodsDeliveryNoteId + "/details/" + goodsDeliveryNoteDetailId)
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    GoodsDeliveryNoteService.prototype.getProductInfoDelivery = function (warehouseId, productId) {
        var _this = this;
        this.spinceService.show();
        return this.http.get(this.url + "/product/" + productId + "/warehouse/" + warehouseId)
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () {
            _this.spinceService.hide();
        }));
    };
    // export
    GoodsDeliveryNoteService.prototype.exportGoodsDeliveryDeltail = function (id) {
        return this.http.get(this.url + "/export-goods-delivery-note/" + id, {
            responseType: 'blob',
        }).pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (response) {
            return new Blob([response]);
        }));
    };
    GoodsDeliveryNoteService.prototype.updateDetail = function (goodsDeliveryNoteId, id, goodsDeliveryNoteDetail) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .post(this.url + "/" + goodsDeliveryNoteId + "/details/" + id, goodsDeliveryNoteDetail)
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    GoodsDeliveryNoteService.prototype.updateRecommendedQuantity = function (id, recommendedQuantity, concurrencyStamp) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .get(this.url + "/details/" + id + "/recommended-quantity/" + recommendedQuantity, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
                .set('concurrencyStamp', concurrencyStamp)
        })
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    GoodsDeliveryNoteService.prototype.receiverSuggestion = function (keyword, page, pageSize) {
        return this.http.get(this.url + "/receivers", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString())
        });
    };
    GoodsDeliveryNoteService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__["SpinnerService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__["SpinnerService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_1__["ToastrService"]])
    ], GoodsDeliveryNoteService);
    return GoodsDeliveryNoteService;
}());



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-delivery-note/model/goods-delivery-note-details.model.ts":
/*!********************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-delivery-note/model/goods-delivery-note-details.model.ts ***!
  \********************************************************************************************************/
/*! exports provided: GoodsDeliveryNoteDetail */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsDeliveryNoteDetail", function() { return GoodsDeliveryNoteDetail; });
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);

var GoodsDeliveryNoteDetail = /** @class */ (function () {
    function GoodsDeliveryNoteDetail(id, code, productId, productName, warehouseId, warehouseName, unitId, unitName, price, quantity, recommendedQuantity, lotId, inventoryQuantity, goodsReceiptNoteDetailCode, concurrencyStamp, units) {
        var _this = this;
        this.price = 0;
        this.discountPrice = 0;
        this.discountByPercent = false;
        this.recommendedQuantity = 0;
        this.id = id;
        this.code = code;
        this.productId = productId;
        this.productName = productName;
        this.warehouseId = warehouseId;
        this.warehouseName = warehouseName;
        this.unitId = unitId;
        this.unitName = unitName;
        this.price = price;
        this.quantity = quantity;
        this.recommendedQuantity = recommendedQuantity;
        this.lotId = lotId;
        this.inventoryQuantity = inventoryQuantity;
        this.goodsReceiptNoteDetailCode = goodsReceiptNoteDetailCode;
        this.conversionPrice = price;
        this.concurrencyStamp = concurrencyStamp;
        this.units = units;
        // Get current unit.
        var conversionUnit = lodash__WEBPACK_IMPORTED_MODULE_0__["find"](units, function (unit) {
            return unit.unitId === _this.unitId;
        });
        if (conversionUnit) {
            this.conversionInventory = inventoryQuantity / conversionUnit.conversionValue;
            this.conversionPrice = price * conversionUnit.conversionValue;
        }
        this.totalAmounts = this.quantity * this.conversionPrice;
    }
    return GoodsDeliveryNoteDetail;
}());



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-delivery-note/model/goods-delivery-note.model.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-delivery-note/model/goods-delivery-note.model.ts ***!
  \************************************************************************************************/
/*! exports provided: GoodsDeliveryNote */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsDeliveryNote", function() { return GoodsDeliveryNote; });
/* harmony import */ var _shareds_constants_deliveryType_const__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../shareds/constants/deliveryType.const */ "./src/app/shareds/constants/deliveryType.const.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);


var GoodsDeliveryNote = /** @class */ (function () {
    function GoodsDeliveryNote(warehouseId, reason, type, receiverId, agencyId, totalAmount) {
        var now = moment__WEBPACK_IMPORTED_MODULE_1__();
        this.day = now.date();
        this.month = now.month() + 1;
        this.year = now.year();
        this.deliveryDate = now.format('YYYY/MM/DD HH:mm:ss');
        this.type = _shareds_constants_deliveryType_const__WEBPACK_IMPORTED_MODULE_0__["DeliveryType"].retail;
        this.goodsDeliveryNoteDetails = [];
        this.totalAmounts = 0;
        this.totalQuantity = 0;
        this.reason = '';
        this.warehouseId = null;
        this.receptionWarehouseId = null;
        this.receiverId = null;
        this.receiverFullName = null;
        this.receiverPhoneNumber = null;
        this.officeId = null;
        // this.deliveryNo = moment().format('YYMMDDHHmmssSS');
    }
    return GoodsDeliveryNote;
}());



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/follow-suggestion/follow-suggestion.component.html":
/*!***************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/follow-suggestion/follow-suggestion.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-suggestion\r\n    [class.receipt]=\"isReceipt\"\r\n    [loading]=\"isSearching\"\r\n    [sources]=\"listItems\"\r\n    [selectedItem]=\"selectedItem\"\r\n    [allowAdd]=\"true\"\r\n    (itemSelected)=\"onItemSelected($event)\"\r\n    (itemRemoved)=\"itemRemoved.emit($event)\"\r\n    (searched)=\"onSearchKeyPress($event)\"\r\n></nh-suggestion>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/follow-suggestion/follow-suggestion.component.ts":
/*!*************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/follow-suggestion/follow-suggestion.component.ts ***!
  \*************************************************************************************************************/
/*! exports provided: FollowSuggestionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FollowSuggestionComponent", function() { return FollowSuggestionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../shareds/components/nh-suggestion/nh-suggestion.component */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.component.ts");
/* harmony import */ var _goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../goods-receipt-note.service */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.service.ts");







var FollowSuggestionComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](FollowSuggestionComponent, _super);
    function FollowSuggestionComponent(goodsReceiptNoteService) {
        var _this = _super.call(this) || this;
        _this.goodsReceiptNoteService = goodsReceiptNoteService;
        _this.multiple = false;
        _this.isReceipt = false;
        _this.keyPressed = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.itemSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.itemRemoved = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.keyword$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        _this.keyword$.subscribe(function (term) {
            _this.keyword = term;
            _this.search(1);
        });
        return _this;
    }
    FollowSuggestionComponent.prototype.ngOnInit = function () {
    };
    FollowSuggestionComponent.prototype.onItemSelected = function (item) {
        this.itemSelected.emit(item);
    };
    FollowSuggestionComponent.prototype.onSearchKeyPress = function (keyword) {
        this.keyword = keyword;
        this.keyPressed.emit(keyword);
        this.keyword$.next(keyword);
    };
    FollowSuggestionComponent.prototype.search = function (currentPage) {
        var _this = this;
        this.isSearching = true;
        this.currentPage = currentPage;
        this.goodsReceiptNoteService.followSuggestion(this.keyword, this.currentPage, this.appConfig.PAGE_SIZE)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () { return _this.isSearching = false; }))
            .subscribe(function (result) {
            _this.totalRows = result.totalRows;
            _this.listItems = result.items;
        });
    };
    FollowSuggestionComponent.prototype.clear = function () {
        this.nhSuggestionComponent.clear();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_5__["NhSuggestionComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_5__["NhSuggestionComponent"])
    ], FollowSuggestionComponent.prototype, "nhSuggestionComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], FollowSuggestionComponent.prototype, "multiple", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], FollowSuggestionComponent.prototype, "isReceipt", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], FollowSuggestionComponent.prototype, "selectedItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], FollowSuggestionComponent.prototype, "keyPressed", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], FollowSuggestionComponent.prototype, "itemSelected", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], FollowSuggestionComponent.prototype, "itemRemoved", void 0);
    FollowSuggestionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-follow-suggestion',
            template: __webpack_require__(/*! ./follow-suggestion.component.html */ "./src/app/modules/warehouse/goods/goods-receipt-note/follow-suggestion/follow-suggestion.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_6__["GoodsReceiptNoteService"]])
    ], FollowSuggestionComponent);
    return FollowSuggestionComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_4__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-detail/goods-receipt-note-detail.component.html":
/*!*******************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-detail/goods-receipt-note-detail.component.html ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #goodsReceiptNoteDetailModal size=\"full\">\r\n    <form class=\"cm-pdt-20 form-horizontal\">\r\n        <nh-modal-content class=\"form-body\">\r\n            <h3 class=\"form-section bold\" i18n=\"@@goodsReceiptNoteInfo\">Thông tin phiếu nhập</h3>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label ghmLabel=\"Ngày nhập\"\r\n                               class=\"col-sm-3 control-label\"\r\n                               i18n-ghmLabel=\"@@entryDate\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\">\r\n                                {{ goodsReceiptNote?.entryDate | dateTimeFormat:'DD/MM/YYYY HH:mm' }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label ghmLabel=\"Số phiếu\"\r\n                               class=\"col-sm-3 control-label\"\r\n                               i18n-ghmLabel=\"@@receiptNo\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\">\r\n                                {{ goodsReceiptNote?.receiptNo }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Hình thức nhập\"\r\n                               i18n-ghmLabel=\"@@receiptType\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                           <span class=\"badge cm-mgt-10\"\r\n                                 [class.badge-info]=\"goodsReceiptNote?.type === goodsReceiptNoteType.newPurchase\"\r\n                                 [class.badge-danger]=\"goodsReceiptNote?.type === goodsReceiptNoteType.customerReturn\"\r\n                                 [class.badge-warning]=\"goodsReceiptNote?.type === goodsReceiptNoteType.transfer\"\r\n                                 [class.badge-default]=\"goodsReceiptNote?.type === goodsReceiptNoteType.inventory\">\r\n                    {goodsReceiptNote?.type, select, 0 {Nhập mua mới} 1 {Nhập khách trả} 2 {Nhập điều chuyển} 3 {Nhập kiểm kê} other{}}\r\n                </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Tại kho\"\r\n                               i18n-ghmLabel=\"@@warehouse\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\">{{ goodsReceiptNote?.warehouseName }}</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\" *ngIf=\"goodsReceiptNote?.type === goodsReceiptNoteType.newPurchase\">\r\n                <div class=\"col-sm-6\" *ngIf=\"goodsReceiptNote?.type === goodsReceiptNoteType.newPurchase\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Nhà cung cấp\"\r\n                               i18n-ghmLabel=\"@@supplier\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\">{{ goodsReceiptNote?.supplierName }}</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Người giao hàng\"\r\n                               i18n-ghmLabel=\"@@deliver\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\">{{ goodsReceiptNote?.deliverFullName }}</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n            <div class=\"row\" *ngIf=\"goodsReceiptNote?.type === goodsReceiptNoteType.newPurchase\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Số điện thoại\"\r\n                               i18n-ghmLabel=\"@@phoneNumber\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\">{{ goodsReceiptNote?.deliverPhoneNumber }}</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Theo\" i18n-ghmLabel=\"@@follow\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\">{{ goodsReceiptNote?.follow }}</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\" *ngIf=\"goodsReceiptNote?.type === goodsReceiptNoteType.newPurchase\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Số\" i18n-ghmLabel=\"@@invoiceNo\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\">\r\n                                {{ goodsReceiptNote?.invoiceNo }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Ngày\"\r\n                               i18n-ghmLabel=\"@@date\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control\">\r\n                                {{ goodsReceiptNote?.invoiceDate | dateTimeFormat:'DD/MM/YYYY HH:mm' }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Ghi chú\" i18n-ghmLabel=\"@@note\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"form-control height-auto\">\r\n                                {{ goodsReceiptNote?.note }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <h3 class=\"form-section bold\" i18n=\"@@goodsReceiptNoteDetailInfo\">Chi tiết phiếu nhập</h3>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-12\">\r\n                    <div class=\"table-responsive\">\r\n                        <app-goods-receipt-note-form-product\r\n                            [readOnly]=\"true\"\r\n                            [receiptId]=\"id\"\r\n                            [listItems]=\"goodsReceiptNote?.goodsReceiptNoteDetails\"\r\n                        ></app-goods-receipt-note-form-product>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-12 cm-mgt-10\">\r\n                    <div class=\"form-group\">\r\n                        <label ghmLabel=\"Thành tiền (Viết bằng chữ)\"\r\n                               i18n-ghmLabel=\"@@totalAmountsInWord\"\r\n                               class=\"col-sm-2\"\r\n                        ></label>\r\n                        <div class=\"col-sm-10\">\r\n                            <div class=\"dotted-form-control bold\">\r\n                                {{ totalAmounts | ghmAmountToWord }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <button type=\"button\" class=\"btn blue cm-mgr-5\" i18n=\"@@printCode\" (click)=\"printBarcode()\">\r\n                In mã vạch\r\n            </button>\r\n            <button type=\"button\" class=\"btn blue cm-mgr-5\" i18n=\"@@printReceipt\" (click)=\"printReceipt()\">\r\n                In phiếu\r\n            </button>\r\n            <button type=\"button\" nh-dismiss class=\"btn default\" i18n=\"@@cancel\">\r\n                Đóng\r\n            </button>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n\r\n<app-goods-receipt-note-print></app-goods-receipt-note-print>\r\n<app-goods-receipt-note-print-barcode></app-goods-receipt-note-print-barcode>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-detail/goods-receipt-note-detail.component.ts":
/*!*****************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-detail/goods-receipt-note-detail.component.ts ***!
  \*****************************************************************************************************************************/
/*! exports provided: GoodsReceiptNoteDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsReceiptNoteDetailComponent", function() { return GoodsReceiptNoteDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../goods-receipt-note.service */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.service.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _shareds_services_helper_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../shareds/services/helper.service */ "./src/app/shareds/services/helper.service.ts");
/* harmony import */ var _shareds_services_app_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../shareds/services/app.service */ "./src/app/shareds/services/app.service.ts");
/* harmony import */ var _goods_receipt_note_print_goods_receipt_note_print_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../goods-receipt-note-print/goods-receipt-note-print.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-print/goods-receipt-note-print.component.ts");
/* harmony import */ var _goods_receipt_note_print_barcode_goods_receipt_note_print_barcode_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../goods-receipt-note-print-barcode/goods-receipt-note-print-barcode.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-print-barcode/goods-receipt-note-print-barcode.component.ts");
/* harmony import */ var _goods_receipt_note_type_const__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../goods-receipt-note-type.const */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-type.const.ts");











var GoodsReceiptNoteDetailComponent = /** @class */ (function () {
    function GoodsReceiptNoteDetailComponent(appService, route, helperService, goodsReceiptNoteService) {
        this.appService = appService;
        this.route = route;
        this.helperService = helperService;
        this.goodsReceiptNoteService = goodsReceiptNoteService;
        this.goodsReceiptNoteType = _goods_receipt_note_type_const__WEBPACK_IMPORTED_MODULE_10__["GoodsReceiptNoteType"];
        this.currentUser = this.appService.currentUser;
    }
    GoodsReceiptNoteDetailComponent.prototype.ngOnInit = function () {
    };
    GoodsReceiptNoteDetailComponent.prototype.getDetail = function (id) {
        var _this = this;
        this.id = id;
        setTimeout(function () {
            _this.goodsReceiptNoteDetailModal.open();
        });
        this.goodsReceiptNoteService.getDetail(id)
            .subscribe(function (result) {
            if (result) {
                _this.goodsReceiptNote = result;
                _this.totalAmounts = lodash__WEBPACK_IMPORTED_MODULE_5__["sumBy"](result.goodsReceiptNoteDetails, function (item) {
                    return (item.quantity * item.price) + item.taxes;
                });
            }
        });
    };
    GoodsReceiptNoteDetailComponent.prototype.printBarcode = function () {
        this.goodsReceiptNotePrintBarcodeComponent.print(this.id);
    };
    GoodsReceiptNoteDetailComponent.prototype.printReceipt = function () {
        this.goodsReceiptNotePrintComponent.print(this.id);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('goodsReceiptNoteDetailModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_4__["NhModalComponent"])
    ], GoodsReceiptNoteDetailComponent.prototype, "goodsReceiptNoteDetailModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_goods_receipt_note_print_goods_receipt_note_print_component__WEBPACK_IMPORTED_MODULE_8__["GoodsReceiptNotePrintComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _goods_receipt_note_print_goods_receipt_note_print_component__WEBPACK_IMPORTED_MODULE_8__["GoodsReceiptNotePrintComponent"])
    ], GoodsReceiptNoteDetailComponent.prototype, "goodsReceiptNotePrintComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_goods_receipt_note_print_barcode_goods_receipt_note_print_barcode_component__WEBPACK_IMPORTED_MODULE_9__["GoodsReceiptNotePrintBarcodeComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _goods_receipt_note_print_barcode_goods_receipt_note_print_barcode_component__WEBPACK_IMPORTED_MODULE_9__["GoodsReceiptNotePrintBarcodeComponent"])
    ], GoodsReceiptNoteDetailComponent.prototype, "goodsReceiptNotePrintBarcodeComponent", void 0);
    GoodsReceiptNoteDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-goods-receipt-note-detail',
            template: __webpack_require__(/*! ./goods-receipt-note-detail.component.html */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-detail/goods-receipt-note-detail.component.html"),
            providers: [_shareds_services_helper_service__WEBPACK_IMPORTED_MODULE_6__["HelperService"]],
            styles: [__webpack_require__(/*! ../goods-receipt-note.component.scss */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shareds_services_app_service__WEBPACK_IMPORTED_MODULE_7__["AppService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _shareds_services_helper_service__WEBPACK_IMPORTED_MODULE_6__["HelperService"],
            _goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_3__["GoodsReceiptNoteService"]])
    ], GoodsReceiptNoteDetailComponent);
    return GoodsReceiptNoteDetailComponent;
}());



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form-product/goods-receipt-note-form-product-form/goods-receipt-note-form-product-form.component.html":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form-product/goods-receipt-note-form-product-form/goods-receipt-note-form-product-form.component.html ***!
  \*************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal size=\"md\" #goodsReipcetNoteProductModal\r\n          (hidden)=\"onModalHidden()\">\r\n    <nh-modal-header class=\"uppercase font-green-sharp bold\">\r\n        {isUpdate, select, 0 {Thêm sản phẩm} 1 {Cập nhật phiếu nhập}}\r\n    </nh-modal-header>\r\n    <form action=\"\" class=\"form-horizontal\" [formGroup]=\"model\" (ngSubmit)=\"save()\">\r\n        <nh-modal-content>\r\n            <div class=\"form-group\"\r\n                 [class.has-error]=\"formErrors.productId\">\r\n                <label class=\"col-sm-3 control-label control-label\" for=\"\" ghmLabel=\"Sản phẩm\"\r\n                       i18n-ghmLabel=\"@@product\" [required]=\"true\"></label>\r\n                <div class=\"col-sm-9\">\r\n                    <app-product-suggestion\r\n                        class=\"no-border\"\r\n                        [hasError]=\"formErrors.productId\"\r\n                        #productSuggestion\r\n                        (itemRemoved)=\"onProductRemoved($event)\"\r\n                        (itemSelected)=\"onProductSelected($event)\"></app-product-suggestion>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\"\r\n                 [class.has-error]=\"formErrors.lotId\">\r\n                <label class=\"col-sm-3 control-label\" for=\"\" ghmLabel=\"Lô sản xuất\" i18n-ghmLabel=\"@@lot\"\r\n                       [required]=\"isManageByLot\"></label>\r\n                <div class=\"col-sm-9\">\r\n                    <app-lot-suggestion\r\n                        class=\"no-border\"\r\n                        [hasError]=\"formErrors.lotId\"\r\n                        #lotSuggestion\r\n                        (keyPressed)=\"onLotKeyPressed($event)\"\r\n                        (itemSelected)=\"onLotSelected($event)\"\r\n                    ></app-lot-suggestion>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\"\r\n                 [class.has-error]=\"formErrors.unitId\">\r\n                <label class=\"col-sm-3 control-label\" for=\"\" ghmLabel=\"Ngày hết hạn\"\r\n                       i18n-ghmLabel=\"@@expiryDate\"></label>\r\n                <div class=\"col-sm-9\">\r\n                    <nh-date formControlName=\"expiryDate\"\r\n                             [mask]=\"true\"\r\n                             [type]=\"'input'\"></nh-date>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\"\r\n                 [class.has-error]=\"formErrors.expiryDate\">\r\n                <label class=\"col-sm-3 control-label\" for=\"\" ghmLabel=\"Đơn vị\" i18n-ghmLabel=\"@@unit\"\r\n                       [required]=\"true\"></label>\r\n                <div class=\"col-sm-9\">\r\n                    <nh-select\r\n                        [class.has-error]=\"formErrors.unitId\"\r\n                        [loading]=\"isGettingUnit\"\r\n                        [data]=\"units\"\r\n                        title=\"-- Chọn đơn vị --\"\r\n                        i18n-title=\"@@selectUnit\"\r\n                        (itemSelected)=\"onUnitSelected($event)\"\r\n                        formControlName=\"unitId\"\r\n                    ></nh-select>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\"\r\n                 [class.has-error]=\"formErrors.quantity || formErrors.price\">\r\n                <label class=\"col-sm-3 control-label\" for=\"\" ghmLabel=\"Số lượng\" i18n-ghmLabel=\"@@quantity\"\r\n                       [required]=\"true\"></label>\r\n                <div class=\"col-sm-3\">\r\n                    <input type=\"text\"\r\n                           ghm-text-selection\r\n                           [class.has-error]=\"formErrors.quantity\"\r\n                           class=\"form-control text-right\" formControlName=\"quantity\">\r\n                </div>\r\n                <label class=\"col-sm-3 control-label\" for=\"\" ghmLabel=\"Giá nhập\" i18n-ghmLabel=\"@@price\"\r\n                       [required]=\"true\"></label>\r\n                <div class=\"col-sm-3\">\r\n                    <input type=\"text\"\r\n                           ghm-text-selection\r\n                           [class.has-error]=\"formErrors.price\"\r\n                           class=\"form-control text-right\" formControlName=\"price\">\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\"\r\n                 [class.has-error]=\"formErrors.tax || formErrors.invoiceQuantity\">\r\n                <label class=\"col-sm-3 control-label\" for=\"\" ghmLabel=\"Thuế\" i18n-ghmLabel=\"@@tax\"></label>\r\n                <div class=\"col-sm-3\">\r\n                    <input type=\"text\"\r\n                           [class.has-error]=\"formErrors.tax\"\r\n                           ghm-text-selection\r\n                           class=\"form-control text-right\" formControlName=\"tax\">\r\n                </div>\r\n                <label class=\"col-sm-3 control-label\" for=\"\" ghmLabel=\"Số lượng hóa đơn\"\r\n                       i18n-ghmLabel=\"@@invoiceQuantity\"></label>\r\n                <div class=\"col-sm-3\">\r\n                    <input type=\"text\"\r\n                           ghm-text-selection\r\n                           [class.has-error]=\"formErrors.invoiceQuantity\"\r\n                           class=\"form-control text-right\" formControlName=\"invoiceQuantity\">\r\n                </div>\r\n            </div>\r\n            <hr>\r\n            <div class=\"form-group\">\r\n                <label class=\"col-sm-9 control-label bold\" for=\"\" ghmLabel=\"Tổng tiền trước thuế\"\r\n                       i18n-ghmLabel=\"@@totalBeforeTaxes\"></label>\r\n                <div class=\"col-sm-3 cm-pdt-10\">\r\n                    {{ model.value.totalBeforeTaxes | ghmCurrency:'2' }}\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label class=\"col-sm-9 control-label bold\" for=\"\" ghmLabel=\"Tổng tiền thuế\"\r\n                       i18n-ghmLabel=\"@@taxes\"></label>\r\n                <div class=\"col-sm-3 cm-pdt-10\">\r\n                    {{ model.value.taxes | ghmCurrency:'2' }}\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label class=\"col-sm-9 control-label bold\" for=\"\" ghmLabel=\"Thành tiền\"\r\n                       i18n-ghmLabel=\"@@totalAmounts\"></label>\r\n                <div class=\"col-sm-3 cm-pdt-10\">\r\n                    {{ model.value.totalAmounts | ghmCurrency:'2' }}\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <mat-checkbox\r\n                class=\"cm-mgr-5\"\r\n                color=\"primary\"\r\n                name=\"isCreateAnother\"\r\n                i18n=\"@@isCreateAnother\"\r\n                *ngIf=\"!isUpdate\"\r\n                [(checked)]=\"isCreateAnother\"\r\n                (change)=\"isCreateAnother = !isCreateAnother\"> Tiếp tục thêm\r\n            </mat-checkbox>\r\n            <button class=\"btn blue cm-mgr-5\" i18n=\"@@save\">\r\n                Lưu\r\n            </button>\r\n            <button type=\"button\" nh-dismiss class=\"btn default\" i18n=\"@@close\">\r\n                Đóng\r\n            </button>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form-product/goods-receipt-note-form-product-form/goods-receipt-note-form-product-form.component.ts":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form-product/goods-receipt-note-form-product-form/goods-receipt-note-form-product-form.component.ts ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: GoodsReceiptNoteFormProductFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsReceiptNoteFormProductFormComponent", function() { return GoodsReceiptNoteFormProductFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../shareds/components/nh-suggestion/nh-suggestion.component */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.component.ts");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _goods_receipt_note_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../goods-receipt-note.model */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.model.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _validators_number_validator__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../../../validators/number.validator */ "./src/app/validators/number.validator.ts");
/* harmony import */ var _product_product_service_product_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../product/product/service/product.service */ "./src/app/modules/warehouse/product/product/service/product.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../goods-receipt-note.service */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.service.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _product_product_product_suggestion_product_suggestion_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../../product/product/product-suggestion/product-suggestion.component */ "./src/app/modules/warehouse/product/product/product-suggestion/product-suggestion.component.ts");
/* harmony import */ var _lot_suggestion_lot_suggestion_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../../lot-suggestion/lot-suggestion.component */ "./src/app/modules/warehouse/goods/lot-suggestion/lot-suggestion.component.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _validators_conditional_required_validator__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../../../../../validators/conditional-required.validator */ "./src/app/validators/conditional-required.validator.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");

















var GoodsReceiptNoteFormProductFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](GoodsReceiptNoteFormProductFormComponent, _super);
    function GoodsReceiptNoteFormProductFormComponent(numberValidator, toastr, utilService, productService, goodsReceiptNoteService) {
        var _this = _super.call(this) || this;
        _this.numberValidator = numberValidator;
        _this.toastr = toastr;
        _this.utilService = utilService;
        _this.productService = productService;
        _this.goodsReceiptNoteService = goodsReceiptNoteService;
        _this.totalAmounts = 0;
        _this.goodsReceiptNoteItem = new _goods_receipt_note_model__WEBPACK_IMPORTED_MODULE_4__["GoodsReceiptNoteDetail"]();
        _this.units = [];
        _this.isGettingUnit = false;
        _this._isManageByLot = false;
        return _this;
    }
    Object.defineProperty(GoodsReceiptNoteFormProductFormComponent.prototype, "isManageByLot", {
        get: function () {
            return this._isManageByLot;
        },
        set: function (value) {
            this._isManageByLot = value;
            this.buildForm();
        },
        enumerable: true,
        configurable: true
    });
    GoodsReceiptNoteFormProductFormComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    GoodsReceiptNoteFormProductFormComponent.prototype.onLotKeyPressed = function (keyword) {
        this.model.patchValue({ lotId: keyword });
    };
    GoodsReceiptNoteFormProductFormComponent.prototype.onLotSelected = function (lot) {
        if (lot) {
            this.model.patchValue({ lotId: lot.id ? lot.id : lot.name });
        }
        else {
            this.model.patchValue({ lotId: null });
        }
    };
    GoodsReceiptNoteFormProductFormComponent.prototype.onProductSelected = function (product) {
        console.log(product);
        this.isManageByLot = product.data.isManageByLot;
        this.model.patchValue({
            productId: product ? product.id : null,
            productName: product ? product.name : null
        });
        if (product) {
            // Get product units
            this.getUnitByProductId(product.id.toString(), true);
        }
    };
    GoodsReceiptNoteFormProductFormComponent.prototype.onProductRemoved = function (event) {
        this.model.patchValue({ productId: null });
    };
    GoodsReceiptNoteFormProductFormComponent.prototype.onUnitSelected = function (unit) {
        this.model.patchValue({
            unitName: unit ? unit.name : null
        });
    };
    GoodsReceiptNoteFormProductFormComponent.prototype.onModalHidden = function () {
        this.resetModel();
    };
    GoodsReceiptNoteFormProductFormComponent.prototype.edit = function (goodsReceiptNoteItem) {
        this.id = goodsReceiptNoteItem.id;
        this.isUpdate = true;
        this.productSuggestion.selectedItem = new _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_2__["NhSuggestion"](goodsReceiptNoteItem.productId, goodsReceiptNoteItem.productName);
        this.lotSuggestion.selectedItem = goodsReceiptNoteItem.lotId
            ? new _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_2__["NhSuggestion"](goodsReceiptNoteItem.lotId, goodsReceiptNoteItem.lotId)
            : null;
        this.getUnitByProductId(goodsReceiptNoteItem.productId);
        this.model.patchValue(goodsReceiptNoteItem);
        this.goodsReipcetNoteProductModal.open();
    };
    GoodsReceiptNoteFormProductFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.validateModel();
        if (isValid) {
            this.goodsReceiptNoteItem = this.model.value;
            this.goodsReceiptNoteItem.invoiceQuantity = this.goodsReceiptNoteItem.invoiceQuantity
                ? this.goodsReceiptNoteItem.invoiceQuantity
                : this.goodsReceiptNoteItem.quantity;
            this.goodsReceiptNoteItem.id = this.id ? this.id : this.utilService.generateRandomNumber().toString();
            if (this.receiptId) {
                if (this.isUpdate) {
                    this.goodsReceiptNoteService.updateItem(this.receiptId, this.id, this.goodsReceiptNoteItem)
                        .subscribe(function (result) {
                        _this.toastr.success(result.message);
                        var data = result.data;
                        if (data) {
                            _this.goodsReceiptNoteItem.concurrencyStamp = data;
                            _this.emitValue();
                            _this.goodsReipcetNoteProductModal.dismiss();
                        }
                    });
                }
                else {
                    this.goodsReceiptNoteService.insertItem(this.receiptId, this.goodsReceiptNoteItem)
                        .subscribe(function (result) {
                        _this.toastr.success(result.message);
                        var data = result.data;
                        if (data) {
                            _this.goodsReceiptNoteItem.id = data.id;
                            _this.goodsReceiptNoteItem.concurrencyStamp = data.concurrencyStamp;
                            _this.emitValue();
                            _this.resetModel();
                            if (!_this.isCreateAnother) {
                                _this.goodsReipcetNoteProductModal.dismiss();
                            }
                        }
                    });
                }
            }
            else {
                this.emitValue();
                if (!this.isCreateAnother) {
                    this.goodsReipcetNoteProductModal.dismiss();
                }
            }
        }
    };
    GoodsReceiptNoteFormProductFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.renderFormError(['productId', 'lotId', 'unitId', 'price', 'invoiceQuantity', 'quantity', 'tax', 'note']);
        this.validationMessages = this.renderFormErrorMessage([
            { 'productId': ['required'] },
            { 'lotId': ['required', 'maxLength'] },
            { 'unitId': ['required'] },
            { 'price': ['required', 'isValid'] },
            { 'invoiceQuantity': ['isValid'] },
            { 'quantity': ['required', 'isValid'] },
            { 'tax': ['isValid'] },
            { 'note': ['maxlength'] },
        ]);
        this.model = this.formBuilder.group({
            productId: [this.goodsReceiptNoteItem.productId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required
                ]],
            productName: [this.goodsReceiptNoteItem.productName],
            lotId: [this.goodsReceiptNoteItem.lotId, [
                    Object(_validators_conditional_required_validator__WEBPACK_IMPORTED_MODULE_14__["conditionalRequiredValidator"])(this.isManageByLot)
                ]],
            unitId: [this.goodsReceiptNoteItem.unitId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required
                ]],
            unitName: [this.goodsReceiptNoteItem.unitName],
            price: [this.goodsReceiptNoteItem.price, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required,
                    this.numberValidator.isValid
                ]],
            invoiceQuantity: [this.goodsReceiptNoteItem.invoiceQuantity, [
                    this.numberValidator.isValid
                ]],
            quantity: [this.goodsReceiptNoteItem.quantity, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required,
                    this.numberValidator.isValid
                ]],
            tax: [this.goodsReceiptNoteItem.tax, [
                    this.numberValidator.isValid
                ]],
            totalBeforeTaxes: [this.goodsReceiptNoteItem.totalBeforeTaxes],
            taxes: [this.goodsReceiptNoteItem.taxes],
            totalAmounts: [this.goodsReceiptNoteItem.totalAmounts],
            manufactureDate: [this.goodsReceiptNoteItem.manufactureDate],
            expiryDate: [this.goodsReceiptNoteItem.expiryDate],
            note: [this.goodsReceiptNoteItem.note, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].maxLength(500)
                ]],
            concurrencyStamp: [this.goodsReceiptNoteItem.concurrencyStamp]
        });
        this.model.valueChanges.subscribe(function () { return _this.validateModel(false); });
        this.model.get('price').valueChanges.subscribe(function (value) {
            var _a = _this.model.value, quantity = _a.quantity, tax = _a.tax;
            _this.calculateValues(value, quantity, tax);
        });
        this.model.get('quantity').valueChanges.subscribe(function (value) {
            var _a = _this.model.value, price = _a.price, tax = _a.tax;
            _this.calculateValues(price, value, tax);
        });
        this.model.get('tax').valueChanges.subscribe(function (value) {
            var _a = _this.model.value, price = _a.price, quantity = _a.quantity;
            _this.calculateValues(price, quantity, value);
        });
    };
    GoodsReceiptNoteFormProductFormComponent.prototype.resetModel = function () {
        this.id = null;
        this.goodsReceiptNoteItem = new _goods_receipt_note_model__WEBPACK_IMPORTED_MODULE_4__["GoodsReceiptNoteDetail"]();
        this.model.reset();
        this.productSuggestion.clear();
        this.lotSuggestion.clear();
        this.totalAmounts = null;
    };
    GoodsReceiptNoteFormProductFormComponent.prototype.emitValue = function () {
        this.utilService.focusElement('productId');
        this.saveSuccessful.emit(lodash__WEBPACK_IMPORTED_MODULE_15__["cloneDeep"](this.goodsReceiptNoteItem));
        this.resetModel();
    };
    GoodsReceiptNoteFormProductFormComponent.prototype.calculateValues = function (price, quantity, tax) {
        var totalBeforeTaxes = price * quantity;
        var taxes = tax ? totalBeforeTaxes * tax / 100 : 0;
        var totalAmounts = totalBeforeTaxes + taxes;
        this.model.patchValue({
            totalBeforeTaxes: totalBeforeTaxes,
            taxes: taxes,
            totalAmounts: totalAmounts
        });
    };
    GoodsReceiptNoteFormProductFormComponent.prototype.getUnitByProductId = function (id, isSelectDefault) {
        var _this = this;
        if (isSelectDefault === void 0) { isSelectDefault = false; }
        this.isGettingUnit = true;
        this.subscribers.getUnits = this.productService.getUnit(id, 1, 10)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["finalize"])(function () { return _this.isGettingUnit = false; }))
            .subscribe(function (result) {
            _this.units = result.items;
            if (isSelectDefault) {
                var defaultUnit = lodash__WEBPACK_IMPORTED_MODULE_15__["find"](_this.units, function (unit) {
                    return unit.isDefault;
                });
                if (defaultUnit) {
                    _this.model.patchValue({
                        unitId: defaultUnit.id,
                        unitName: defaultUnit.name
                    });
                }
            }
        });
    };
    GoodsReceiptNoteFormProductFormComponent.prototype.add = function () {
        this.isUpdate = false;
        this.goodsReipcetNoteProductModal.open();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('productSuggestion'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _product_product_product_suggestion_product_suggestion_component__WEBPACK_IMPORTED_MODULE_11__["ProductSuggestionComponent"])
    ], GoodsReceiptNoteFormProductFormComponent.prototype, "productSuggestion", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('lotSuggestion'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _lot_suggestion_lot_suggestion_component__WEBPACK_IMPORTED_MODULE_12__["LotSuggestionComponent"])
    ], GoodsReceiptNoteFormProductFormComponent.prototype, "lotSuggestion", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('goodsReipcetNoteProductModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_16__["NhModalComponent"])
    ], GoodsReceiptNoteFormProductFormComponent.prototype, "goodsReipcetNoteProductModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GoodsReceiptNoteFormProductFormComponent.prototype, "receiptId", void 0);
    GoodsReceiptNoteFormProductFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-goods-receipt-note-item-form',
            template: __webpack_require__(/*! ./goods-receipt-note-form-product-form.component.html */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form-product/goods-receipt-note-form-product-form/goods-receipt-note-form-product-form.component.html"),
            providers: [_validators_number_validator__WEBPACK_IMPORTED_MODULE_6__["NumberValidator"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_validators_number_validator__WEBPACK_IMPORTED_MODULE_6__["NumberValidator"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_13__["ToastrService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_10__["UtilService"],
            _product_product_service_product_service__WEBPACK_IMPORTED_MODULE_7__["ProductService"],
            _goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_9__["GoodsReceiptNoteService"]])
    ], GoodsReceiptNoteFormProductFormComponent);
    return GoodsReceiptNoteFormProductFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_3__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form-product/goods-receipt-note-form-product.component.html":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form-product/goods-receipt-note-form-product.component.html ***!
  \*******************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row cm-mgb-10\" *ngIf=\"!readOnly\">\r\n    <div class=\"col-sm-12 text-right\">\r\n        <button type=\"button\" class=\"btn blue\" (click)=\"addDetail()\" i18n=\"@@add\">Thêm</button>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"table-responsive\">\r\n    <table class=\"table table-bordered table-stripped\">\r\n        <thead>\r\n        <tr>\r\n            <th rowspan=\"2\" class=\"center middle w50\" i18n=\"@@no\">STT</th>\r\n            <th rowspan=\"2\" class=\"center middle w100\" i18n=\"@@code\">Mã sản phẩm</th>\r\n            <th rowspan=\"2\" class=\"center middle w250\" i18n=\"@@product\">Tên sản phẩm</th>\r\n            <th rowspan=\"2\" class=\"center middle w100\" i18n=\"@@lot\">Số lô</th>\r\n            <th rowspan=\"2\" class=\"center middle w150\" i18n=\"@@expireDate\">Ngày hết hạn</th>\r\n            <th rowspan=\"2\" class=\"center middle w100\" i18n=\"@@unit\">Đơn vị</th>\r\n            <th colspan=\"2\" class=\"center middle\" i18n=\"@@quantity\">Số lượng</th>\r\n            <th rowspan=\"2\" class=\"center middle w100\" i18n=\"@@price\">Đơn giá</th>\r\n            <th rowspan=\"2\" class=\"center middle w100\" i18n=\"@@totalBeforeTaxes\">Tổng tiền trước thuế</th>\r\n            <th rowspan=\"2\" class=\"center middle w100\" i18n=\"@@taxPercent\">Thuế(%)</th>\r\n            <th rowspan=\"2\" class=\"center middle w100\" i18n=\"@@taxes\">Tổng tiền thuế</th>\r\n            <th rowspan=\"2\" class=\"center middle w100\" i18n=\"@@totalAmounts\">Thành tiền</th>\r\n            <th rowspan=\"2\" class=\"w100\" *ngIf=\"!readOnly\"></th>\r\n        </tr>\r\n        <tr>\r\n            <th class=\"center w70\" i18n=\"@@invoice\">Hóa đơn</th>\r\n            <th class=\"center w70\" i18n=\"@@real\">Thực tế</th>\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n        <tr *ngFor=\"let item of listItems; let i = index\"\r\n            nhContextMenuTrigger\r\n            [nhContextMenuTriggerFor]=\"nhMenu\"\r\n            [nhContextMenuData]=\"item\">\r\n            <td class=\"center middle\">{{ (i + 1) }}</td>\r\n            <td class=\"middle text-ellipsis w100\">\r\n                <a href=\"javascript://\" title=\"{{ item.productId }}\">{{ item.productId }}</a>\r\n            </td>\r\n            <td class=\"middle\">\r\n                <a href=\"javascript://\" title=\"{{ item.productName }}\">{{ item.productName }}</a>\r\n            </td>\r\n            <td class=\"middle\">{{ item.lotId }}</td>\r\n            <td class=\"middle\">{{ item.expiryDate | dateTimeFormat:'DD/MM/YYYY' }}</td>\r\n            <td class=\"middle\">{{ item.unitName }}</td>\r\n            <td class=\"text-right middle\">{{ item.invoiceQuantity }}</td>\r\n            <td class=\"text-right middle\">{{ item.quantity }}</td>\r\n            <td class=\"text-right middle\">{{ item.price | ghmCurrency }}</td>\r\n            <td class=\"text-right middle\">{{ item.totalBeforeTaxes | ghmCurrency }}</td>\r\n            <td class=\"text-right middle\">{{ item.tax | ghmCurrency }}</td>\r\n            <td class=\"text-right middle\">{{ item.taxes | ghmCurrency }}</td>\r\n            <td class=\"text-right middle\">{{ item.totalAmounts | ghmCurrency }}</td>\r\n            <td class=\"center middle\" *ngIf=\"!readOnly\">\r\n                <button type=\"button\" class=\"btn btn-sm blue\" (click)=\"edit(item)\">\r\n                    <i class=\"fa fa-edit\"></i>\r\n                </button>\r\n                <button type=\"button\" class=\"btn btn-sm red\"\r\n                        [swal]=\"confirmDelete\"\r\n                        (click)=\"confirm(item)\">\r\n                    <i class=\"fa fa-trash-o\"></i>\r\n                </button>\r\n            </td>\r\n        </tr>\r\n        <tr *ngIf=\"!readOnly\"\r\n            goods-receipt-note-item-form\r\n            (saveSuccessful)=\"onSaveItemSuccess($event)\"></tr>\r\n        </tbody>\r\n        <tfoot>\r\n        <tr>\r\n            <td class=\"text-right\" colspan=\"10\" i18n=\"@@total\">Tổng tiền</td>\r\n            <td class=\"text-right bold cm-pd-5\" [attr.colspan]=\"readOnly ? 3 : 4\">{{ totalBeforeTaxes | ghmCurrency }}\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"text-right\" colspan=\"10\" i18n=\"@@taxes\">Tổng tiền thuế</td>\r\n            <td class=\"text-right bold\" [attr.colspan]=\"readOnly ? 3 : 4\">{{ taxes | ghmCurrency }}</td>\r\n        </tr>\r\n        <tr>\r\n            <td class=\"text-right\" colspan=\"10\" i18n=\"@@totalAmounts\">Thành tiền</td>\r\n            <td class=\"text-right bold\" [attr.colspan]=\"readOnly ? 3 : 4\">{{ totalAmounts | ghmCurrency }}</td>\r\n        </tr>\r\n        </tfoot>\r\n    </table>\r\n</div>\r\n\r\n<swal\r\n    #confirmDelete\r\n    i18n-title=\"@@areYouSureForDeleteThisItem\"\r\n    i18n-text=\"@@cantRecoverItemAfterDeleteMessage\"\r\n    title=\"Bạn có chắc chắn muốn xóa sản phẩm này không?\"\r\n    text=\"Sau khi xóa bạn không thể lấy lại sản phẩm này.\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Đồng ý\"\r\n    cancelButtonText=\"Hủy bỏ\">\r\n</swal>\r\n\r\n<nh-menu #nhMenu>\r\n    <nh-menu-item (clicked)=\"edit($event)\">\r\n        <!--<i class=\"fa fa-edit menu-icon\"></i>-->\r\n        <mat-icon class=\"menu-icon\">edit</mat-icon>\r\n        <span i18n=\"@@edit\">Chỉnh sửa</span>\r\n    </nh-menu-item>\r\n    <nh-menu-item\r\n        (clicked)=\"confirm($event)\">\r\n        <!--<i class=\"menu-icon fa fa-trash-o\"></i>-->\r\n        <mat-icon class=\"menu-icon\">delete</mat-icon>\r\n        <span i18n=\"@@edit\">Xóa</span>\r\n    </nh-menu-item>\r\n</nh-menu>\r\n\r\n<app-goods-receipt-note-item-form\r\n    [receiptId]=\"receiptId\"\r\n    (saveSuccessful)=\"onSaveItemSuccess($event)\"\r\n></app-goods-receipt-note-item-form>\r\n\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form-product/goods-receipt-note-form-product.component.ts":
/*!*****************************************************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form-product/goods-receipt-note-form-product.component.ts ***!
  \*****************************************************************************************************************************************************************/
/*! exports provided: GoodsReceiptNoteFormProductComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsReceiptNoteFormProductComponent", function() { return GoodsReceiptNoteFormProductComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _goods_receipt_note_form_product_form_goods_receipt_note_form_product_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./goods-receipt-note-form-product-form/goods-receipt-note-form-product-form.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form-product/goods-receipt-note-form-product-form/goods-receipt-note-form-product-form.component.ts");
/* harmony import */ var _goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../goods-receipt-note.service */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.service.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @toverux/ngx-sweetalert2 */ "./node_modules/@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);








var GoodsReceiptNoteFormProductComponent = /** @class */ (function () {
    function GoodsReceiptNoteFormProductComponent(toastr, goodsReceiptNoteService) {
        this.toastr = toastr;
        this.goodsReceiptNoteService = goodsReceiptNoteService;
        this._listItems = [];
        this.readOnly = false;
        this.totalAmountUpdated = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.hasChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.totalAmounts = 0;
    }
    Object.defineProperty(GoodsReceiptNoteFormProductComponent.prototype, "listItems", {
        get: function () {
            return this._listItems;
        },
        set: function (items) {
            this._listItems = items;
            this.totalAmounts = lodash__WEBPACK_IMPORTED_MODULE_4__["sumBy"](this.listItems, function (x) { return x.totalAmounts; });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GoodsReceiptNoteFormProductComponent.prototype, "taxes", {
        get: function () {
            return lodash__WEBPACK_IMPORTED_MODULE_4__["sumBy"](this.listItems, function (x) { return x.taxes; });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GoodsReceiptNoteFormProductComponent.prototype, "totalBeforeTaxes", {
        get: function () {
            return lodash__WEBPACK_IMPORTED_MODULE_4__["sumBy"](this.listItems, function (x) { return x.totalBeforeTaxes; });
        },
        enumerable: true,
        configurable: true
    });
    GoodsReceiptNoteFormProductComponent.prototype.ngOnInit = function () {
    };
    GoodsReceiptNoteFormProductComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.swalConfirmDelete.confirm.subscribe(function (result) {
            _this.delete();
        });
    };
    GoodsReceiptNoteFormProductComponent.prototype.addDetail = function () {
        this.goodsReceiptNoteFormProductFormComponent.add();
    };
    GoodsReceiptNoteFormProductComponent.prototype.edit = function (goodsReceiptNoteItem) {
        this.goodsReceiptNoteFormProductFormComponent.edit(goodsReceiptNoteItem);
    };
    GoodsReceiptNoteFormProductComponent.prototype.confirm = function (goodsReceiptNoteItem) {
        this.selectedGoodsReceiptNoteItem = goodsReceiptNoteItem;
        this.swalConfirmDelete.show();
    };
    GoodsReceiptNoteFormProductComponent.prototype.onSaveItemSuccess = function (goodsReceiptNoteItem) {
        var itemInfo = lodash__WEBPACK_IMPORTED_MODULE_4__["find"](this.listItems, function (item) {
            return goodsReceiptNoteItem.id === item.id;
        });
        if (itemInfo) {
            itemInfo.productId = goodsReceiptNoteItem.productId;
            itemInfo.productName = goodsReceiptNoteItem.productName;
            itemInfo.lotId = goodsReceiptNoteItem.lotId;
            itemInfo.unitId = goodsReceiptNoteItem.unitId;
            itemInfo.unitName = goodsReceiptNoteItem.unitName;
            itemInfo.invoiceQuantity = goodsReceiptNoteItem.invoiceQuantity;
            itemInfo.quantity = goodsReceiptNoteItem.quantity;
            itemInfo.price = goodsReceiptNoteItem.price;
            itemInfo.totalBeforeTaxes = goodsReceiptNoteItem.totalBeforeTaxes;
            itemInfo.tax = goodsReceiptNoteItem.tax;
            itemInfo.taxes = goodsReceiptNoteItem.taxes;
            itemInfo.totalAmounts = goodsReceiptNoteItem.totalAmounts;
            itemInfo.concurrencyStamp = goodsReceiptNoteItem.concurrencyStamp;
            itemInfo.expiryDate = moment__WEBPACK_IMPORTED_MODULE_7__(goodsReceiptNoteItem.expiryDate, 'YYYY/MM/DD');
        }
        else {
            this.listItems = this.listItems.concat([goodsReceiptNoteItem]);
        }
        this.getTotalAmounts();
        this.hasChange.emit();
    };
    GoodsReceiptNoteFormProductComponent.prototype.delete = function () {
        var _this = this;
        if (this.receiptId) {
            this.goodsReceiptNoteService.deleteItem(this.receiptId, this.selectedGoodsReceiptNoteItem.id)
                .subscribe(function (result) {
                _this.toastr.success(result.message);
                lodash__WEBPACK_IMPORTED_MODULE_4__["remove"](_this.listItems, function (item) {
                    return item.id === _this.selectedGoodsReceiptNoteItem.id;
                });
                _this.getTotalAmounts();
            });
        }
        else {
            lodash__WEBPACK_IMPORTED_MODULE_4__["remove"](this.listItems, this.selectedGoodsReceiptNoteItem);
            this.getTotalAmounts();
        }
    };
    GoodsReceiptNoteFormProductComponent.prototype.getTotalAmounts = function () {
        this.totalAmounts = lodash__WEBPACK_IMPORTED_MODULE_4__["sumBy"](this.listItems, 'totalAmounts');
        this.totalAmountUpdated.emit(this.totalAmounts);
    };
    GoodsReceiptNoteFormProductComponent.prototype.reset = function () {
        this.goodsReceiptNoteFormProductFormComponent.resetModel();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_goods_receipt_note_form_product_form_goods_receipt_note_form_product_form_component__WEBPACK_IMPORTED_MODULE_2__["GoodsReceiptNoteFormProductFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _goods_receipt_note_form_product_form_goods_receipt_note_form_product_form_component__WEBPACK_IMPORTED_MODULE_2__["GoodsReceiptNoteFormProductFormComponent"])
    ], GoodsReceiptNoteFormProductComponent.prototype, "goodsReceiptNoteFormProductFormComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('confirmDelete'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_5__["SwalComponent"])
    ], GoodsReceiptNoteFormProductComponent.prototype, "swalConfirmDelete", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GoodsReceiptNoteFormProductComponent.prototype, "receiptId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Array])
    ], GoodsReceiptNoteFormProductComponent.prototype, "listItems", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GoodsReceiptNoteFormProductComponent.prototype, "readOnly", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GoodsReceiptNoteFormProductComponent.prototype, "totalAmountUpdated", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GoodsReceiptNoteFormProductComponent.prototype, "hasChange", void 0);
    GoodsReceiptNoteFormProductComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-goods-receipt-note-form-product',
            template: __webpack_require__(/*! ./goods-receipt-note-form-product.component.html */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form-product/goods-receipt-note-form-product.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"],
            _goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_3__["GoodsReceiptNoteService"]])
    ], GoodsReceiptNoteFormProductComponent);
    return GoodsReceiptNoteFormProductComponent;
}());



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form.component.html":
/*!***************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form.component.html ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #goodsReceiptNoteFormModal size=\"full\"\r\n          (hidden)=\"onModalHidden()\">\r\n    <form class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content class=\"form-body\">\r\n            <h3 class=\"form-section bold\" i18n=\"@@goodsReceiptNoteInfo\">Thông tin phiếu nhập</h3>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\"\r\n                         [class.has-error]=\"formErrors.entryDate\">\r\n                        <label ghmLabel=\"Ngày nhập\"\r\n                               class=\"col-sm-3 control-label\"\r\n                               i18n-ghmLabel=\"@@entryDate\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <nh-date\r\n                                [mask]=\"true\"\r\n                                [showTime]=\"true\"\r\n                                [format]=\"'DD/MM/YYYY HH:mm:ss'\"\r\n                                [outputFormat]=\"'YYYY/MM/DD HH:mm:ss'\"\r\n                                formControlName=\"entryDate\"></nh-date>\r\n                            <span class=\"help-block\" *ngIf=\"formErrors.entryDate\"\r\n                                  i18n=\"@@goodsReceiptNoteEntryDateErrorMessage\">\r\n                                {formErrors.entryDate, select, required {Vui lòng nhập ngày nhập}}\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div><!-- END: .col-sm-6 -->\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\"\r\n                         [class.has-error]=\"formErrors.supplierId\">\r\n                        <label class=\"col-sm-3 control-label\"\r\n                               ghmLabel=\"Nhà cung cấp\"\r\n                               i18n-ghmLabel=\"@@supplier\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <app-supplier-suggestion\r\n                                (keyPressed)=\"onSupplierKeyPressed($event)\"\r\n                                (itemSelected)=\"onSupplierSelected($event)\"></app-supplier-suggestion>\r\n                            <span class=\"help-block\" *ngIf=\"formErrors.supplierId\"\r\n                                  i18n=\"@@goodsReceiptNoteSupplierErrorMessage\">\r\n                            {formErrors.supplierId, select, required {Vui lòng chọn nhà cung cấp}}\r\n                        </span>\r\n                        </div>\r\n                    </div>\r\n                </div><!-- END: .col-sm-6 -->\r\n            </div><!-- END: .row -->\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\"\r\n                         [class.has-error]=\"formErrors.deliverFullName\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Người giao hàng\" i18n-ghmLabel=\"@@deliver\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <app-deliver-suggestion\r\n                                [supplierId]=\"model.value.supplierId\"\r\n                                (keyPressed)=\"onDeliverKeyPressed($event)\"\r\n                                (itemSelected)=\"onDeliverSelected($event)\"></app-deliver-suggestion>\r\n                            <span class=\"help-block\" *ngIf=\"formErrors.deliverFullName\"\r\n                                  i18n=\"@@goodsReceiptNoteDeliverFullNameErrorMessage\">\r\n                            {formErrors.deliverFullName, select, required {Vui lòng chọn người giao hàng.} maxlength {Tên người gia hàng không được phép vượt quá 50 ký tự.}}\r\n                        </span>\r\n                        </div>\r\n                    </div>\r\n                </div><!-- END: .col-sm-6 -->\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\"\r\n                         [class.has-error]=\"formErrors.deliverPhoneNumber\">\r\n                        <label class=\"col-sm-3 control-label\"\r\n                               ghmLabel=\"Số điện thoại\"\r\n                               i18n-ghmLabel=\"@@deliverPhoneNumber\" [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <input type=\"text\" class=\"form-control\"\r\n                                   formControlName=\"deliverPhoneNumber\">\r\n                            <span class=\"help-block\" *ngIf=\"formErrors.deliverPhoneNumber\"\r\n                                  i18n=\"@@goodsReceiptNoteDeliverPhoneNumberErrorMessage\">\r\n                                {formErrors.deliverPhoneNumber, select, required {Vui lòng nhập số điện thoại người giao hàng.} maxlength {Số điện thoại người giao hàng không được phép vượt quá 50 ký tự.}}\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div><!-- END: .col-sm-6 -->\r\n            </div><!-- END: .row -->\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\"\r\n                         [class.has-error]=\"formErrors.warehouseId\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Nhập tại kho\" i18n-ghmLabel=\"@@warehouse\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <app-warehouse-suggestion\r\n                                (itemSelected)=\"onWarehouseSelected($event)\"></app-warehouse-suggestion>\r\n                            <span class=\"help-block\" *ngIf=\"formErrors.warehouseId\"\r\n                                  i18n=\"@@goodsReceiptNoteWarehouseErrorMessage\">\r\n                                    {formErrors.warehouseId, select, required {Vui lòng chọn kho.}}\r\n                                </span>\r\n                        </div>\r\n                    </div>\r\n                </div><!-- END: .col-sm-6 -->\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\"\r\n                         [class.has-error]=\"formErrors.follow\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Theo\" i18n-ghmLabel=\"@@follow\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <app-follow-suggestion\r\n                                (itemSelected)=\"onFollowSelected($event)\"\r\n                                (keyPressed)=\"onFollowKeyPress($event)\"\r\n                            ></app-follow-suggestion>\r\n                            <span class=\"help-block\" *ngIf=\"formErrors.follow\"\r\n                                  i18n=\"@@goodsReceiptNoteFollowErrorMessage\">\r\n                            {formErrors.follow, select, required {Vui lòng chọn phiếu nhập theo} maxlength {Phiếu nhập theo không được phép vượt quá 256 ký tự.}}\r\n                        </span>\r\n                        </div>\r\n                    </div>\r\n                </div><!-- END: .col-sm-6 -->\r\n            </div><!-- END: .row -->\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\"\r\n                         [class.has-error]=\"formErrors.invoiceNo\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Số\" i18n-ghmLabel=\"@@invoiceNo\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <input type=\"text\" class=\"form-control\" formControlName=\"invoiceNo\">\r\n                            <span class=\"help-block\" *ngIf=\"formErrors.invoiceNo\"\r\n                                  i18n=\"@@goodsReceiptNoteInvoiceNoErrorMessage\">\r\n                                {formErrors.invoiceNo, select, required {Vui lòng nhập số hóa đơn.} maxlength {Số hóa đơn không được phép vượt quá 50 ký tự.}}\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div><!-- END: .col-sm-6 -->\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\"\r\n                         [class.has-error]=\"formErrors.invoiceDate\">\r\n                        <label class=\"col-sm-3 control-label\" ghmLabel=\"Ngày\" i18n-ghmLabel=\"@@invoiceDate\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <nh-date formControlName=\"invoiceDate\"\r\n                                     [showTime]=\"true\"\r\n                                     [mask]=\"true\"></nh-date>\r\n                            <span class=\"help-block\" *ngIf=\"formErrors.invoiceDate\"\r\n                                  i18n=\"@@goodsReceiptNoteInvoiceDateErrorMessage\">\r\n                            {formErrors.invoiceDate, select, required {Vui lòng chọn ngày hóa đơn}}\r\n                        </span>\r\n                        </div>\r\n                    </div>\r\n                </div><!-- END: .col-sm-6 -->\r\n            </div><!-- END: .row -->\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\"\r\n                         [class.has-error]=\"formErrors.note\">\r\n                        <label ghmLabel=\"Ghi chú\" i18n-ghmLabel=\"@@note\"\r\n                               class=\"col-sm-3 control-label\"></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <textarea class=\"form-control\" id=\"\" rows=\"3\"\r\n                                      formControlName=\"note\"></textarea>\r\n                            <span class=\"help-block\" *ngIf=\"formErrors.note\" i18n=\"@@goodsReceiptNoteNoteErrorMessage\">\r\n                            {formErrors.note, select, maxlength {Ghi chú không được phép vượt quá 500 ký tự}}\r\n                        </span>\r\n                        </div>\r\n                    </div>\r\n                </div><!-- END: .col-sm-6 -->\r\n            </div><!-- END: .row -->\r\n            <h3 class=\"form-section bold\" i18n=\"@@goodsReceiptNoteDetailInfo\">Chi tiết phiếu nhập</h3>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-12\">\r\n                    <app-goods-receipt-note-form-product\r\n                        [receiptId]=\"id\"\r\n                        [listItems]=\"goodsReceiptNoteDetails\"></app-goods-receipt-note-form-product>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-12 cm-mgt-10\">\r\n                    <div class=\"form-group\">\r\n                        <label ghmLabel=\"Thành tiền (viết bằng chữ)\"\r\n                               i18n-ghmLabel=\"@@totalAmountsInWord\"\r\n                               class=\"col-sm-3\"\r\n                        ></label>\r\n                        <div class=\"col-sm-9\">\r\n                            <div class=\"dotted-form-control\">\r\n                                {{ totalAmounts | ghmAmountToWord }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <mat-checkbox\r\n                class=\"cm-mgr-5\"\r\n                color=\"primary\"\r\n                name=\"isCreateAnother\"\r\n                i18n=\"@@isCreateAnother\"\r\n                *ngIf=\"!isUpdate\"\r\n                [(checked)]=\"isCreateAnother\"\r\n                (change)=\"isCreateAnother = !isCreateAnother\"> Tiếp tục thêm\r\n            </mat-checkbox>\r\n            <button class=\"btn blue cm-mgr-5\" i18n=\"@@save\">\r\n                Lưu\r\n            </button>\r\n            <button type=\"button\" class=\"btn blue cm-mgr-5\" i18n=\"@@saveAndPrint\" (click)=\"save(true)\">\r\n                Lưu và in\r\n            </button>\r\n            <button type=\"button\" nh-dismiss class=\"btn default\" i18n=\"@@close\">\r\n                Đóng\r\n            </button>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n\r\n<swal\r\n    #confirmDelete\r\n    i18n-title=\"@@areYouSureForDeleteThisItem\"\r\n    i18n-text=\"@@cantRecoverItemAfterDeleteMessage\"\r\n    title=\"Are you sure for delete this item?\"\r\n    text=\"You can't recover this item after delete.\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Accept\"\r\n    cancelButtonText=\"Cancel\">\r\n</swal>\r\n\r\n<nh-menu #nhMenu>\r\n    <nh-menu-item (clicked)=\"edit($event)\">\r\n        <i class=\"fa fa-edit menu-icon\"></i>\r\n        <span i18n=\"@@edit\">Chỉnh sửa</span>\r\n    </nh-menu-item>\r\n</nh-menu>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form.component.ts":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form.component.ts ***!
  \*************************************************************************************************************************/
/*! exports provided: GoodsReceiptNoteFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsReceiptNoteFormComponent", function() { return GoodsReceiptNoteFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../goods-receipt-note.service */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.service.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _goods_receipt_note_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../goods-receipt-note.model */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.model.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../shareds/components/nh-suggestion/nh-suggestion.component */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.component.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _goods_receipt_note_form_product_goods_receipt_note_form_product_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./goods-receipt-note-form-product/goods-receipt-note-form-product.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form-product/goods-receipt-note-form-product.component.ts");
/* harmony import */ var _product_supplier_supplier_suggestion_supplier_suggestion_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../product/supplier/supplier-suggestion/supplier-suggestion.component */ "./src/app/modules/warehouse/product/supplier/supplier-suggestion/supplier-suggestion.component.ts");
/* harmony import */ var _deliver_suggestion_deliver_suggestion_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../deliver-suggestion/deliver-suggestion.component */ "./src/app/modules/warehouse/goods/deliver-suggestion/deliver-suggestion.component.ts");
/* harmony import */ var _follow_suggestion_follow_suggestion_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../follow-suggestion/follow-suggestion.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/follow-suggestion/follow-suggestion.component.ts");
/* harmony import */ var _warehouse_warehouse_suggestion_warehouse_suggestion_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../warehouse/warehouse-suggestion/warehouse-suggestion.component */ "./src/app/modules/warehouse/warehouse/warehouse-suggestion/warehouse-suggestion.component.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _validators_conditional_required_validator__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../../../../validators/conditional-required.validator */ "./src/app/validators/conditional-required.validator.ts");
/* harmony import */ var _validators_number_validator__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../../../../validators/number.validator */ "./src/app/validators/number.validator.ts");
/* harmony import */ var _product_product_service_product_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../../product/product/service/product.service */ "./src/app/modules/warehouse/product/product/service/product.service.ts");



















var GoodsReceiptNoteFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](GoodsReceiptNoteFormComponent, _super);
    function GoodsReceiptNoteFormComponent(productService, numberValidator, toastr, utilService, goodsReceiptNoteService) {
        var _this = _super.call(this) || this;
        _this.productService = productService;
        _this.numberValidator = numberValidator;
        _this.toastr = toastr;
        _this.utilService = utilService;
        _this.goodsReceiptNoteService = goodsReceiptNoteService;
        _this.goodsReceiptNote = new _goods_receipt_note_model__WEBPACK_IMPORTED_MODULE_5__["GoodsReceiptNote"]();
        _this.follows = [];
        _this.isPrint = false;
        _this.detailFormErrors = {};
        _this.detailValidationMessages = {};
        _this.goodsReceiptNoteDetails = [];
        // get goodsReceiptNoteDetails(): FormArray {
        //     return this.model.get('goodsReceiptNoteDetails') as FormArray;
        // }
        _this._isManageByLot = false;
        return _this;
    }
    Object.defineProperty(GoodsReceiptNoteFormComponent.prototype, "isManageByLot", {
        get: function () {
            return this._isManageByLot;
        },
        set: function (value) {
            this._isManageByLot = value;
        },
        enumerable: true,
        configurable: true
    });
    GoodsReceiptNoteFormComponent.prototype.ngOnInit = function () {
        this.buildForm();
        // this.addDetail();
    };
    GoodsReceiptNoteFormComponent.prototype.onModalHidden = function () {
        if (this.isModified) {
            this.saveSuccessful.emit({
                isPrint: this.isPrint,
                id: this.id
            });
        }
        this.resetModel();
    };
    GoodsReceiptNoteFormComponent.prototype.onSupplierKeyPressed = function (keyword) {
        this.model.patchValue({ supplierName: keyword });
    };
    GoodsReceiptNoteFormComponent.prototype.onSupplierSelected = function (supplier) {
        this.model.patchValue({ supplierId: supplier ? supplier.id : null });
    };
    GoodsReceiptNoteFormComponent.prototype.onDeliverKeyPressed = function (keyword) {
        this.model.patchValue({ deliverFullName: keyword });
    };
    GoodsReceiptNoteFormComponent.prototype.onDeliverSelected = function (deliver) {
        this.model.patchValue({
            deliverId: deliver ? deliver.id : null,
            deliverFullName: deliver ? deliver.name : null,
            deliverPhoneNumber: deliver ? deliver.data.phoneNumber : null
        });
    };
    GoodsReceiptNoteFormComponent.prototype.onFollowSelected = function (follow) {
        this.model.patchValue({ follow: follow ? follow.name : null, followId: follow ? follow.id : null });
    };
    GoodsReceiptNoteFormComponent.prototype.onFollowKeyPress = function (keyword) {
        this.model.patchValue({ follow: keyword });
    };
    GoodsReceiptNoteFormComponent.prototype.onWarehouseSelected = function (warehouse) {
        this.model.patchValue({ warehouseId: warehouse ? warehouse.id : null });
    };
    GoodsReceiptNoteFormComponent.prototype.onProductSelected = function (product, goodsReceiptNoteDetailFormControl) {
        this.isManageByLot = product.isManageByLot;
        goodsReceiptNoteDetailFormControl.patchValue({
            productId: product ? product.id : null,
            productName: product ? product.name : null
        });
        if (!goodsReceiptNoteDetailFormControl.value.units || goodsReceiptNoteDetailFormControl.value.units.length === 0) {
            // Get product units
            this.getUnitByProductId(product.id.toString(), goodsReceiptNoteDetailFormControl);
        }
    };
    GoodsReceiptNoteFormComponent.prototype.onProductRemoved = function (event, goodsReceiptNoteDetailFormControl) {
        // goodsReceiptNoteDetailFormControl.patchValue({productId: null});
    };
    GoodsReceiptNoteFormComponent.prototype.onLotKeyPressed = function (keyword, goodsReceiptNoteDetailFormControl) {
        goodsReceiptNoteDetailFormControl.patchValue({ lotId: keyword });
    };
    GoodsReceiptNoteFormComponent.prototype.onLotSelected = function (lot, goodsReceiptNoteDetailFormControl) {
        if (lot) {
            goodsReceiptNoteDetailFormControl.patchValue({ lotId: lot.id ? lot.id : lot.name });
        }
        else {
            goodsReceiptNoteDetailFormControl.patchValue({ lotId: null });
        }
    };
    GoodsReceiptNoteFormComponent.prototype.onLotRemoved = function () {
    };
    GoodsReceiptNoteFormComponent.prototype.onUnitShown = function (goodsReceiptNoteDetailModel) {
        var units = goodsReceiptNoteDetailModel.value.units;
        if (!units || units.length === 0) {
            this.getUnitByProductId(goodsReceiptNoteDetailModel.value.productId, goodsReceiptNoteDetailModel);
        }
    };
    GoodsReceiptNoteFormComponent.prototype.onUnitSelected = function (unit) {
        this.model.patchValue({
            unitName: unit ? unit.name : null
        });
    };
    GoodsReceiptNoteFormComponent.prototype.add = function () {
        this.isUpdate = false;
        this.goodsReceiptNoteFormModal.open();
    };
    // addDetail(goodsReceiptNoteDetail?: GoodsReceiptNoteDetail) {
    //     this.goodsReceiptNoteDetails.push(this.buildDetailForm(this.goodsReceiptNoteDetails.length, goodsReceiptNoteDetail));
    // }
    GoodsReceiptNoteFormComponent.prototype.edit = function (id) {
        var _this = this;
        this.isUpdate = true;
        this.id = id;
        this.goodsReceiptNoteFormModal.open();
        this.goodsReceiptNoteService.getDetail(this.id)
            .subscribe(function (result) {
            if (result) {
                result.entryDate = moment__WEBPACK_IMPORTED_MODULE_9__(result.entryDate).format('YYYY/MM/DD HH:mm:ss');
                result.invoiceDate = moment__WEBPACK_IMPORTED_MODULE_9__(result.invoiceDate).format('YYYY/MM/DD HH:mm:ss');
                _this.model.patchValue(result);
                _this.supplierSuggestionComponent.selectedItem = new _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_8__["NhSuggestion"](result.supplierId, result.supplierName);
                _this.warehouseSuggestionComponent.selectedItem = new _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_8__["NhSuggestion"](result.warehouseId, result.warehouseName);
                _this.followSuggestionComponent.selectedItem = new _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_8__["NhSuggestion"](result.followId, result.follow);
                _this.deliverSuggestionComponent.selectedItem = new _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_8__["NhSuggestion"](result.deliverId, result.deliverFullName);
                // this.clearFormArray(this.goodsReceiptNoteDetails);
                _this.goodsReceiptNoteDetails = result.goodsReceiptNoteDetails;
                // result.goodsReceiptNoteDetails.forEach((goodsReceiptNoteDetail: GoodsReceiptNoteDetail) => {
                //     this.addDetail(goodsReceiptNoteDetail);
                // });
                _this.getStats();
            }
        });
    };
    GoodsReceiptNoteFormComponent.prototype.save = function (isPrint) {
        var _this = this;
        if (isPrint === void 0) { isPrint = false; }
        this.isPrint = isPrint;
        var isValid = this.validateModel();
        this.goodsReceiptNote = this.model.value;
        if (isValid) {
            this.goodsReceiptNote.goodsReceiptNoteDetails = this.goodsReceiptNoteFormProductComponent.listItems;
            if (this.id) {
                this.goodsReceiptNoteService.update(this.id, this.goodsReceiptNote)
                    .subscribe(function (result) {
                    _this.toastr.success(result.message);
                    _this.isModified = true;
                    _this.goodsReceiptNoteFormModal.dismiss();
                });
            }
            else {
                this.goodsReceiptNoteService.insert(this.goodsReceiptNote)
                    .subscribe(function (result) {
                    _this.toastr.success(result.message);
                    _this.isModified = true;
                    if (_this.isPrint) {
                        _this.id = result.data;
                        _this.goodsReceiptNoteFormModal.dismiss();
                    }
                    else {
                        if (_this.isCreateAnother) {
                            _this.resetModel();
                        }
                        else {
                            _this.goodsReceiptNoteFormModal.dismiss();
                        }
                    }
                });
            }
        }
    };
    GoodsReceiptNoteFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.renderFormError(['receiptNo', 'supplierId', 'deliverFullName', 'deliverPhoneNumber', 'warehouseId',
            'invoiceNo', 'invoiceDate', 'day', 'month', 'year', 'follow']);
        this.validationMessages = this.renderFormErrorMessage([
            { 'receiptNo': ['required', 'maxlength'] },
            { 'supplierId': ['required'] },
            { 'deliverFullName': ['required', 'maxlength'] },
            { 'deliverPhoneNumber': ['required', 'maxlength'] },
            { 'warehouseId': ['required'] },
            { 'invoiceNo': ['required', 'maxlength'] },
            { 'invoiceDate': ['required'] },
            { 'day': ['required'] },
            { 'month': ['required'] },
            { 'year': ['required'] },
            { 'follow': ['required', 'maxlength'] },
        ]);
        this.model = this.formBuilder.group({
            receiptNo: [this.goodsReceiptNote.receiptNo],
            supplierId: [this.goodsReceiptNote.supplierId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required
                ]],
            supplierName: [this.goodsReceiptNote.supplierName],
            deliverId: [this.goodsReceiptNote.deliverId],
            deliverFullName: [this.goodsReceiptNote.deliverFullName, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(50)
                ]],
            deliverPhoneNumber: [this.goodsReceiptNote.deliverPhoneNumber, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(20)
                ]],
            warehouseId: [this.goodsReceiptNote.warehouseId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required
                ]],
            invoiceNo: [this.goodsReceiptNote.invoiceNo, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(50)
                ]],
            invoiceDate: [this.goodsReceiptNote.invoiceDate, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required
                ]],
            entryDate: [this.goodsReceiptNote.entryDate],
            followId: [this.goodsReceiptNote.followId],
            follow: [this.goodsReceiptNote.follow, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(256)
                ]],
            day: [this.goodsReceiptNote.day, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required
                ]],
            month: [this.goodsReceiptNote.month, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required
                ]],
            year: [this.goodsReceiptNote.year, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required
                ]],
            note: [this.goodsReceiptNote.note, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(500)
                ]],
            goodsReceiptNoteDetails: this.formBuilder.array([])
        });
        this.model.valueChanges.subscribe(function () { return _this.validateModel(false); });
    };
    GoodsReceiptNoteFormComponent.prototype.buildDetailForm = function (index, goodsReceiptNoteDetail) {
        var _this = this;
        this.detailFormErrors[index] = this.renderFormError(['productId', 'lotId', 'unitId', 'invoiceQuantity', 'quantity',
            'price']);
        this.detailValidationMessages[index] = this.renderFormErrorMessage([
            { 'productId': ['required', 'maxlength'] },
            { 'lotId': ['required'] },
            { 'unitId': ['required', 'maxlength'] },
            { 'invoiceQuantity': ['required', 'isValid'] },
            { 'quantity': ['required', 'isValid'] },
            { 'price': ['required', 'isValid'] }
        ]);
        var goodsReceiptNoteDetailModel = this.formBuilder.group({
            id: [goodsReceiptNoteDetail ? goodsReceiptNoteDetail.id : ''],
            productId: [goodsReceiptNoteDetail ? goodsReceiptNoteDetail.productId : '', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(50)
                ]],
            productName: [goodsReceiptNoteDetail ? goodsReceiptNoteDetail.productName : '', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(256)
                ]],
            lotId: [goodsReceiptNoteDetail ? goodsReceiptNoteDetail.lotId : '', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(50),
                    Object(_validators_conditional_required_validator__WEBPACK_IMPORTED_MODULE_16__["conditionalRequiredValidator"])(this.isManageByLot)
                ]],
            expiryDate: [goodsReceiptNoteDetail ? goodsReceiptNoteDetail.expiryDate : ''],
            unitId: [goodsReceiptNoteDetail ? goodsReceiptNoteDetail.unitId : '', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(50)
                ]],
            unitName: [goodsReceiptNoteDetail ? goodsReceiptNoteDetail.unitName : ''],
            invoiceQuantity: [goodsReceiptNoteDetail ? goodsReceiptNoteDetail.invoiceQuantity : '', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required,
                    this.numberValidator.isValid
                ]],
            quantity: [goodsReceiptNoteDetail ? goodsReceiptNoteDetail.quantity : '', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required,
                    this.numberValidator.isValid
                ]],
            price: [goodsReceiptNoteDetail ? goodsReceiptNoteDetail.price : '', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required,
                    this.numberValidator.isValid
                ]],
            totalBeforeTaxes: [goodsReceiptNoteDetail ? goodsReceiptNoteDetail.totalBeforeTaxes : ''],
            taxes: [goodsReceiptNoteDetail ? goodsReceiptNoteDetail.taxes : ''],
            tax: [goodsReceiptNoteDetail ? goodsReceiptNoteDetail.tax : ''],
            totalAmounts: [goodsReceiptNoteDetail ? goodsReceiptNoteDetail.totalAmounts : ''],
            units: [[]]
        });
        goodsReceiptNoteDetailModel.valueChanges.subscribe(function () { return _this.validateFormGroup(goodsReceiptNoteDetailModel, _this.detailFormErrors[index], _this.detailValidationMessages[index]); });
        goodsReceiptNoteDetailModel.get('price').valueChanges.subscribe(function (value) {
            var _a = goodsReceiptNoteDetailModel.value, quantity = _a.quantity, tax = _a.tax;
            _this.calculateValues(goodsReceiptNoteDetailModel, value, quantity, tax);
        });
        goodsReceiptNoteDetailModel.get('quantity').valueChanges.subscribe(function (value) {
            var _a = goodsReceiptNoteDetailModel.value, price = _a.price, tax = _a.tax;
            _this.calculateValues(goodsReceiptNoteDetailModel, price, value, tax);
        });
        goodsReceiptNoteDetailModel.get('tax').valueChanges.subscribe(function (value) {
            var _a = goodsReceiptNoteDetailModel.value, price = _a.price, quantity = _a.quantity;
            _this.calculateValues(goodsReceiptNoteDetailModel, price, quantity, value);
        });
        return goodsReceiptNoteDetailModel;
    };
    GoodsReceiptNoteFormComponent.prototype.resetModel = function () {
        this.isUpdate = false;
        this.id = null;
        this.model.reset(new _goods_receipt_note_model__WEBPACK_IMPORTED_MODULE_5__["GoodsReceiptNote"]());
        this.supplierSuggestionComponent.clear();
        this.deliverSuggestionComponent.clear();
        this.followSuggestionComponent.clear();
        this.warehouseSuggestionComponent.clear();
        this.totalAmounts = null;
        // this.goodsReceiptNoteFormProductComponent.reset();
        this.isPrint = false;
        // this.clearFormArray(this.goodsReceiptNoteDetails);
        this.goodsReceiptNoteDetails = [];
    };
    GoodsReceiptNoteFormComponent.prototype.getStats = function () {
        var _this = this;
        this.totalAmounts = 0;
        this.totalBeforeTaxes = 0;
        this.taxes = 0;
        this.goodsReceiptNoteDetails.forEach(function (goodsReceiptNoteDetail) {
            _this.totalAmounts += goodsReceiptNoteDetail.totalAmounts;
            _this.totalBeforeTaxes += goodsReceiptNoteDetail.totalBeforeTaxes;
            _this.taxes += goodsReceiptNoteDetail.taxes;
        });
    };
    GoodsReceiptNoteFormComponent.prototype.getUnitByProductId = function (id, goodsReceiptNoteDetailFormControl) {
        this.subscribers.getUnits = this.productService.getUnit(id, 1, 10)
            .subscribe(function (result) {
            goodsReceiptNoteDetailFormControl.patchValue({
                units: result.items
            });
        });
    };
    GoodsReceiptNoteFormComponent.prototype.calculateValues = function (goodsReceiptNoteDetailModel, price, quantity, tax) {
        var _this = this;
        var totalBeforeTaxes = price * quantity;
        var taxes = tax ? totalBeforeTaxes * tax / 100 : 0;
        var totalAmounts = totalBeforeTaxes + taxes;
        goodsReceiptNoteDetailModel.patchValue({
            totalBeforeTaxes: totalBeforeTaxes,
            taxes: taxes,
            totalAmounts: totalAmounts
        });
        this.totalAmounts = 0;
        this.totalBeforeTaxes = 0;
        this.taxes = 0;
        this.goodsReceiptNoteDetails.forEach(function (goodsReceiptNoteDetail) {
            _this.totalAmounts += goodsReceiptNoteDetail.totalAmounts;
            _this.totalBeforeTaxes += goodsReceiptNoteDetail.totalBeforeTaxes;
            _this.taxes += goodsReceiptNoteDetail.taxes;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('goodsReceiptNoteFormModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_4__["NhModalComponent"])
    ], GoodsReceiptNoteFormComponent.prototype, "goodsReceiptNoteFormModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_product_supplier_supplier_suggestion_supplier_suggestion_component__WEBPACK_IMPORTED_MODULE_11__["SupplierSuggestionComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _product_supplier_supplier_suggestion_supplier_suggestion_component__WEBPACK_IMPORTED_MODULE_11__["SupplierSuggestionComponent"])
    ], GoodsReceiptNoteFormComponent.prototype, "supplierSuggestionComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_deliver_suggestion_deliver_suggestion_component__WEBPACK_IMPORTED_MODULE_12__["DeliverSuggestionComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _deliver_suggestion_deliver_suggestion_component__WEBPACK_IMPORTED_MODULE_12__["DeliverSuggestionComponent"])
    ], GoodsReceiptNoteFormComponent.prototype, "deliverSuggestionComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_follow_suggestion_follow_suggestion_component__WEBPACK_IMPORTED_MODULE_13__["FollowSuggestionComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _follow_suggestion_follow_suggestion_component__WEBPACK_IMPORTED_MODULE_13__["FollowSuggestionComponent"])
    ], GoodsReceiptNoteFormComponent.prototype, "followSuggestionComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_warehouse_warehouse_suggestion_warehouse_suggestion_component__WEBPACK_IMPORTED_MODULE_14__["WarehouseSuggestionComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _warehouse_warehouse_suggestion_warehouse_suggestion_component__WEBPACK_IMPORTED_MODULE_14__["WarehouseSuggestionComponent"])
    ], GoodsReceiptNoteFormComponent.prototype, "warehouseSuggestionComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_goods_receipt_note_form_product_goods_receipt_note_form_product_component__WEBPACK_IMPORTED_MODULE_10__["GoodsReceiptNoteFormProductComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _goods_receipt_note_form_product_goods_receipt_note_form_product_component__WEBPACK_IMPORTED_MODULE_10__["GoodsReceiptNoteFormProductComponent"])
    ], GoodsReceiptNoteFormComponent.prototype, "goodsReceiptNoteFormProductComponent", void 0);
    GoodsReceiptNoteFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-goods-receipt-note-form',
            template: __webpack_require__(/*! ./goods-receipt-note-form.component.html */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form.component.html"),
            providers: [_validators_number_validator__WEBPACK_IMPORTED_MODULE_17__["NumberValidator"]],
            styles: [__webpack_require__(/*! ../goods-receipt-note.component.scss */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_product_product_service_product_service__WEBPACK_IMPORTED_MODULE_18__["ProductService"],
            _validators_number_validator__WEBPACK_IMPORTED_MODULE_17__["NumberValidator"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_15__["ToastrService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_6__["UtilService"],
            _goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_3__["GoodsReceiptNoteService"]])
    ], GoodsReceiptNoteFormComponent);
    return GoodsReceiptNoteFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-print-barcode/goods-receipt-note-print-barcode.component.html":
/*!*********************************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-print-barcode/goods-receipt-note-print-barcode.component.html ***!
  \*********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #printPreviewModal size=\"md\">\r\n    <nh-modal-header class=\"uppercase font-green-sharp bold\" i18n=\"@@productList\">\r\n        Danh sách sản phẩm\r\n    </nh-modal-header>\r\n    <form action=\"\" class=\"form-horizontal\" (ngSubmit)=\"printBarcode()\">\r\n        <nh-modal-content>\r\n            <table class=\"table table-striped table-hover\">\r\n                <thead>\r\n                <tr>\r\n                    <th i18n=\"@@no\">STT</th>\r\n                    <th i18n=\"@@productName\">Tên sản phẩm</th>\r\n                    <th i18n=\"@@unit\">Đơn vị</th>\r\n                    <th class=\"text-right\" i18n=\"@@price\">Giá bán</th>\r\n                    <th class=\"text-right\" i18n=\"@@quantity\">Số lượng</th>\r\n                </tr>\r\n                </thead>\r\n                <tbody>\r\n                <tr *ngFor=\"let barcode of barcodes; let i = index\">\r\n                    <td class=\"middle\">{{ (i + 1) }}</td>\r\n                    <td class=\"middle\">{{ barcode.name }}</td>\r\n                    <td class=\"middle\">\r\n                        <nh-select [data]=\"barcode.units\"\r\n                                   title=\"-- Select unit --\"\r\n                                   [selectedItem]=\"{id: barcode.unitId, name: barcode.unitName}\"\r\n                                   i18n-title=\"@@selectUnit\"\r\n                                   (shown)=\"onUnitDropdownShown(barcode)\"\r\n                                   (itemSelected)=\"onUnitSelected($event, barcode)\"></nh-select>\r\n                    </td>\r\n                    <td class=\"text-right middle\">{{ barcode.price | ghmCurrency:0 }}</td>\r\n                    <td class=\"text-right w100 middle\">\r\n                        <input type=\"text\" class=\"form-control\"\r\n                               ghm-text-selection\r\n                               [(ngModel)]=\"barcode.quantity\" name=\"quantity-{{i}}\">\r\n                    </td>\r\n                </tr>\r\n                </tbody>\r\n            </table>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <button class=\"btn blue cm-mgr-5\" i18n=\"@@print\">\r\n                In\r\n            </button>\r\n            <button type=\"button\" class=\"btn default\" i18n=\"@@close\" nh-dismiss>\r\n                Đóng\r\n            </button>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n\r\n<div #barcodeArea style=\"display: none;\">\r\n    <div class=\"print-barcode-page\">\r\n        <div class=\"row\" *ngFor=\"let items of rows\">\r\n            <div class=\"barcode-item\" *ngFor=\"let barcode of items\">\r\n                <div class=\"pdl-5 pdr-5\">\r\n                    <div class=\"bold\">{{ barcode.name }}</div>\r\n                    <div class=\"bold\">Giá: {{ barcode.price | ghmCurrency:0 }}</div>\r\n                </div>\r\n                <div class=\"barcode-wrapper\">\r\n                    <ngx-barcode [bc-value]=\"barcode.id\"\r\n                                 [bc-font-size]=\"12\"\r\n                                 [bc-width]=\"1\"\r\n                                 [bc-height]=\"27\"\r\n                                 [bc-margin]=\"0\"\r\n                                 [bc-margin-top]=\"0\"\r\n                                 [bc-margin-bottom]=\"5\"\r\n                                 [bc-margin-left]=\"5\"\r\n                                 [bc-margin-right]=\"5\"\r\n                                 [bc-display-value]=\"true\"></ngx-barcode>\r\n                </div>\r\n            </div>\r\n            <div class=\"page-break\" *ngIf=\"rows.length > 2\"></div>\r\n        </div>\r\n    </div><!-- END: print-barcode-page -->\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-print-barcode/goods-receipt-note-print-barcode.component.ts":
/*!*******************************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-print-barcode/goods-receipt-note-print-barcode.component.ts ***!
  \*******************************************************************************************************************************************/
/*! exports provided: GoodsReceiptNotePrintBarcodeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsReceiptNotePrintBarcodeComponent", function() { return GoodsReceiptNotePrintBarcodeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shareds_services_helper_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../shareds/services/helper.service */ "./src/app/shareds/services/helper.service.ts");
/* harmony import */ var _goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../goods-receipt-note.service */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.service.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _product_product_service_product_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../product/product/service/product.service */ "./src/app/modules/warehouse/product/product/service/product.service.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");








var GoodsReceiptNotePrintBarcodeComponent = /** @class */ (function () {
    function GoodsReceiptNotePrintBarcodeComponent(helperService, toastr, goodsReceiptNoteService, productService) {
        this.helperService = helperService;
        this.toastr = toastr;
        this.goodsReceiptNoteService = goodsReceiptNoteService;
        this.productService = productService;
        this.barcodes = [];
        this.subscribers = {};
        this.rows = [];
    }
    GoodsReceiptNotePrintBarcodeComponent.prototype.ngOnInit = function () {
    };
    GoodsReceiptNotePrintBarcodeComponent.prototype.onUnitDropdownShown = function (barcode) {
        if (!barcode.units || barcode.units.length === 0) {
            this.getProductUnit(barcode);
        }
    };
    GoodsReceiptNotePrintBarcodeComponent.prototype.onUnitSelected = function (unit, barcode) {
        console.log(unit);
        if (!unit || unit.id == null) {
            this.toastr.warning('Vui lòng chọn đơn vị.');
            return;
        }
        barcode.price = unit.price;
    };
    GoodsReceiptNotePrintBarcodeComponent.prototype.print = function (id) {
        var _this = this;
        this.printPreviewModal.open();
        this.goodsReceiptNoteService.getBarcode(id)
            .subscribe(function (barcodes) {
            _this.barcodes = barcodes;
        });
    };
    GoodsReceiptNotePrintBarcodeComponent.prototype.printBarcode = function () {
        var _this = this;
        this.printPreviewModal.dismiss();
        var items = this.barcodes;
        this.barcodes.forEach(function (item) {
            for (var i = 0; i < item.quantity - 1; i++) {
                items = items.concat([item]);
            }
        });
        this.rows = lodash__WEBPACK_IMPORTED_MODULE_6__["chunk"](items, 2);
        setTimeout(function () {
            var content = _this.barcodeArea.nativeElement.innerHTML;
            _this.helperService.openPrintWindow('Mã phiếu nhập chi tiết', content);
        });
    };
    GoodsReceiptNotePrintBarcodeComponent.prototype.getProductUnit = function (barcode) {
        this.subscribers.getUnits = this.productService.getUnit(barcode.productId, 1, 1000000)
            .subscribe(function (result) {
            console.log(result.items);
            barcode.units = result.items;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('printPreviewModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_4__["NhModalComponent"])
    ], GoodsReceiptNotePrintBarcodeComponent.prototype, "printPreviewModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('barcodeArea'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], GoodsReceiptNotePrintBarcodeComponent.prototype, "barcodeArea", void 0);
    GoodsReceiptNotePrintBarcodeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-goods-receipt-note-print-barcode',
            template: __webpack_require__(/*! ./goods-receipt-note-print-barcode.component.html */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-print-barcode/goods-receipt-note-print-barcode.component.html"),
            providers: [_shareds_services_helper_service__WEBPACK_IMPORTED_MODULE_2__["HelperService"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shareds_services_helper_service__WEBPACK_IMPORTED_MODULE_2__["HelperService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"],
            _goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_3__["GoodsReceiptNoteService"],
            _product_product_service_product_service__WEBPACK_IMPORTED_MODULE_5__["ProductService"]])
    ], GoodsReceiptNotePrintBarcodeComponent);
    return GoodsReceiptNotePrintBarcodeComponent;
}());



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-print/goods-receipt-note-print.component.html":
/*!*****************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-print/goods-receipt-note-print.component.html ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"display: none;\" #printArea>\r\n    <div class=\"print-page\">\r\n        <header>\r\n            <img src=\"{{imageUrl}}/assets/images/print/print-header.jpg\" alt=\"\">\r\n        </header>\r\n        <table class=\"w100pc\">\r\n            <tr>\r\n                <td class=\"text-right\" colspan=\"3\">\r\n                    <div class=\"bold\">Mã số: 01-VT</div>\r\n                    <i>Ngày 14/09/2006 của Bộ trưởng BTC</i>\r\n                </td>\r\n            </tr>\r\n            <tr style=\"margin-bottom: 20px;\">\r\n                <td colspan=\"3\">\r\n                    <h4 class=\"receipt-title\">phiếu nhập</h4>\r\n                    <div class=\"center\">\r\n                        Ngày {{ goodsReceiptNote?.day < 10 ? '0' + goodsReceiptNote?.day : goodsReceiptNote?.day }}\r\n                        tháng {{ goodsReceiptNote?.month < 10 ? '0' + goodsReceiptNote?.month : goodsReceiptNote?.month\r\n                        }} năm {{ goodsReceiptNote?.year }}\r\n                    </div>\r\n                </td>\r\n            </tr>\r\n            <tr>\r\n                <td colspan=\"3\">\r\n                    <div class=\"control-group\">\r\n                        <label for=\"\">Người bán hàng:</label>\r\n                        <div class=\"dotted-control\">{{ goodsReceiptNote?.supplierName }}</div>\r\n                    </div>\r\n                </td>\r\n            </tr>\r\n            <tr>\r\n                <td colspan=\"3\">\r\n                    <div class=\"control-group\">\r\n                        <label for=\"\">Họ tên người giao hàng:</label>\r\n                        <div class=\"dotted-control\">{{ goodsReceiptNote?.deliverFullName }}</div>\r\n                    </div>\r\n                </td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <div class=\"control-group\">\r\n                        <label for=\"\">Theo:</label>\r\n                        <div class=\"dotted-control\">{{ goodsReceiptNote?.follow }}</div>\r\n                    </div>\r\n                </td>\r\n                <td>\r\n                    <div class=\"control-group\">\r\n                        <label for=\"\">Số:</label>\r\n                        <div class=\"dotted-control\">{{ goodsReceiptNote?.invoiceNo }}</div>\r\n                    </div>\r\n                </td>\r\n                <td>\r\n                    <div class=\"control-group\">\r\n                        <label for=\"\">Ngày:</label>\r\n                        <div class=\"dotted-control\">{{ goodsReceiptNote?.invoiceDate | dateTimeFormat:'DD/MM/YYYY' }}\r\n                        </div>\r\n                    </div>\r\n                </td>\r\n            </tr>\r\n            <tr>\r\n                <td colspan=\"3\">\r\n                    <div class=\"control-group\">\r\n                        <label for=\"\">Nhập tại kho:</label>\r\n                        <div class=\"dotted-control\">{{ goodsReceiptNote?.warehouseName }}</div>\r\n                    </div>\r\n                </td>\r\n            </tr>\r\n        </table>\r\n        <table class=\"w100pc bordered\">\r\n            <thead>\r\n            <tr>\r\n                <th rowspan=\"2\" class=\"center w50\">STT</th>\r\n                <th rowspan=\"2\" class=\"center\">Tên, Nhãn hiệu, quy cách, phẩm chât</th>\r\n                <th rowspan=\"2\" class=\"center w70\">Mã số</th>\r\n                <th rowspan=\"2\" class=\"center w70\">Lô SX</th>\r\n                <th rowspan=\"2\" class=\"center w100\">Hạn SD</th>\r\n                <th rowspan=\"2\" class=\"center w70\">Đơn vị tính</th>\r\n                <th colspan=\"2\" class=\"center\">Số lượng</th>\r\n                <th rowspan=\"2\" class=\"center w70\">Đơn giá (đồng)</th>\r\n                <th rowspan=\"2\" class=\"center w70\">Tổng tiền</th>\r\n            </tr>\r\n            <tr>\r\n                <th class=\"center w70\">Chứng từ</th>\r\n                <th class=\"center w70\">Thực nhập</th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            <tr *ngFor=\"let item of goodsReceiptNote?.goodsReceiptNoteDetails; let i = index\">\r\n                <td>{{ (i + 1) }}</td>\r\n                <td>{{ item.productName }}</td>\r\n                <td>{{ item.productId }}</td>\r\n                <td>{{ item.lotId }}</td>\r\n                <td>{{ item.expiryDate | dateTimeFormat:'DD/MM/YYYY' }}</td>\r\n                <td>{{ item.unitName }}</td>\r\n                <td>{{ item.invoiceQuantity | ghmCurrency:0 }}</td>\r\n                <td>{{ item.quantity | ghmCurrency:0 }}</td>\r\n                <td>{{ item.price | ghmCurrency:0 }}</td>\r\n                <td>{{ item.totalAmounts | ghmCurrency:0 }}</td>\r\n            </tr>\r\n            <tr>\r\n                <td class=\"text-right\" colspan=\"8\" i18n=\"@@totalAmounts\">Tổng tiền</td>\r\n                <td class=\"text-right bold\" colspan=\"2\">{{ totalAmounts | ghmCurrency:0 }}</td>\r\n            </tr>\r\n            </tbody>\r\n        </table>\r\n        <table class=\"w100pc\">\r\n            <tr>\r\n                <td>\r\n                    <div class=\"control-group\">\r\n                        <label for=\"\">Tổng số tiền (Viết bằng chữ):</label>\r\n                        <div class=\"dotted-control\">{{ totalAmounts | ghmAmountToWord }}</div>\r\n                    </div>\r\n                </td>\r\n            </tr>\r\n        </table>\r\n        <table class=\"w100pc\" style=\"margin-top: 10px;\">\r\n            <tr>\r\n                <td class=\"center\">Người lập bảng</td>\r\n                <td class=\"center\">Người giao hàng</td>\r\n                <td class=\"center\">Thủ kho</td>\r\n                <td class=\"center\">Kế toán trưởng</td>\r\n                <td></td>\r\n            </tr>\r\n            <tr>\r\n                <td style=\"min-height: 100px; height: 100px;\"></td>\r\n                <td style=\"min-height: 100px; height: 100px;\"></td>\r\n                <td style=\"min-height: 100px; height: 100px;\"></td>\r\n            </tr>\r\n            </tbody>\r\n            <tfoot>\r\n            <tr>\r\n                <td class=\"center\">{{ currentUser?.fullName }}</td>\r\n                <td class=\"center\">{{ goodsReceiptNote?.deliverFullName }}</td>\r\n                <td class=\"center\">{{ goodsReceiptNote?.warehouseManager }}</td>\r\n                <td class=\"center\"></td>\r\n            </tr>\r\n            </tfoot>\r\n        </table>\r\n        <div class=\"page-break\"></div>\r\n        <footer>\r\n            <img src=\"{{imageUrl}}/assets/images/print/print-footer.jpg\" alt=\"\">\r\n        </footer>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-print/goods-receipt-note-print.component.ts":
/*!***************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-print/goods-receipt-note-print.component.ts ***!
  \***************************************************************************************************************************/
/*! exports provided: GoodsReceiptNotePrintComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsReceiptNotePrintComponent", function() { return GoodsReceiptNotePrintComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shareds_services_helper_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../shareds/services/helper.service */ "./src/app/shareds/services/helper.service.ts");
/* harmony import */ var _shareds_services_app_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../shareds/services/app.service */ "./src/app/shareds/services/app.service.ts");
/* harmony import */ var _goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../goods-receipt-note.service */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.service.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../configs/app.config */ "./src/app/configs/app.config.ts");







var GoodsReceiptNotePrintComponent = /** @class */ (function () {
    function GoodsReceiptNotePrintComponent(appConfig, helperService, appService, goodsReceiptNoteService) {
        this.appConfig = appConfig;
        this.helperService = helperService;
        this.appService = appService;
        this.goodsReceiptNoteService = goodsReceiptNoteService;
        this.currentUser = appService.currentUser;
        this.imageUrl = this.appConfig.CORE_API_URL;
    }
    GoodsReceiptNotePrintComponent.prototype.ngOnInit = function () {
    };
    GoodsReceiptNotePrintComponent.prototype.print = function (id) {
        var _this = this;
        this.goodsReceiptNoteService.getDetail(id)
            .subscribe(function (result) {
            if (result) {
                _this.goodsReceiptNote = result;
                _this.totalAmounts = lodash__WEBPACK_IMPORTED_MODULE_5__["sumBy"](result.goodsReceiptNoteDetails, function (item) {
                    return (item.quantity * item.price) + item.taxes;
                });
                setTimeout(function () {
                    _this.executePrint();
                });
            }
        });
    };
    GoodsReceiptNotePrintComponent.prototype.executePrint = function () {
        var style = "\n                     h4.receipt-title {\n                            font-size: 25px;\n                            text-align: center;\n                            font-weight: bold;\n                            text-transform: uppercase;\n                            margin-top: 10px;\n                      }\n                     .amountsInWord {\n                        width: 200px;\n                     }\n                     ";
        var content = this.printArea.nativeElement.innerHTML;
        this.helperService.openPrintWindow('Phiếu nhập kho', content, style);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('printArea'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], GoodsReceiptNotePrintComponent.prototype, "printArea", void 0);
    GoodsReceiptNotePrintComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-goods-receipt-note-print',
            template: __webpack_require__(/*! ./goods-receipt-note-print.component.html */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-print/goods-receipt-note-print.component.html"),
            providers: [_shareds_services_helper_service__WEBPACK_IMPORTED_MODULE_2__["HelperService"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_6__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _shareds_services_helper_service__WEBPACK_IMPORTED_MODULE_2__["HelperService"],
            _shareds_services_app_service__WEBPACK_IMPORTED_MODULE_3__["AppService"],
            _goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_4__["GoodsReceiptNoteService"]])
    ], GoodsReceiptNotePrintComponent);
    return GoodsReceiptNotePrintComponent;
}());



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-type.const.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-type.const.ts ***!
  \*********************************************************************************************/
/*! exports provided: GoodsReceiptNoteType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsReceiptNoteType", function() { return GoodsReceiptNoteType; });
var GoodsReceiptNoteType;
(function (GoodsReceiptNoteType) {
    GoodsReceiptNoteType[GoodsReceiptNoteType["newPurchase"] = 0] = "newPurchase";
    GoodsReceiptNoteType[GoodsReceiptNoteType["customerReturn"] = 1] = "customerReturn";
    GoodsReceiptNoteType[GoodsReceiptNoteType["transfer"] = 2] = "transfer";
    GoodsReceiptNoteType[GoodsReceiptNoteType["inventory"] = 3] = "inventory";
})(GoodsReceiptNoteType || (GoodsReceiptNoteType = {}));


/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.component.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@listSupplierPageTitle\">Danh sách phiếu nhập kho</span>\r\n    <small i18n=\"@@productModuleTitle\">Quản lý kho</small>\r\n</h1>\r\n<form class=\"form-inline cm-mgb-10\" (ngSubmit)=\"search(1)\">\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <app-warehouse-suggestion\r\n            (itemSelected)=\"onWarehouseSelected($event)\"\r\n            (itemRemoved)=\"onWarehouseRemoved()\"></app-warehouse-suggestion>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <app-supplier-suggestion\r\n            (itemRemoved)=\"onSupplierRemoved()\"\r\n            (itemSelected)=\"onSupplierSelected($event)\"></app-supplier-suggestion>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <nh-date [(ngModel)]=\"fromDate\" name=\"fromDate\" [mask]=\"true\"\r\n                 (selectedDateEvent)=\"search()\"\r\n                 (removedDateEvent)=\"search()\"\r\n                 [showTime]=\"true\"\r\n                 [allowRemove]=\"true\"></nh-date>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <nh-date [(ngModel)]=\"toDate\" name=\"toDate\" [mask]=\"true\"\r\n                 (selectedDateEvent)=\"search()\"\r\n                 (removedDateEvent)=\"search()\"\r\n                 [showTime]=\"true\"\r\n                 [allowRemove]=\"true\"></nh-date>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <button class=\"btn blue\" type=\"submit\">\r\n            <i class=\"fa fa-search\" *ngIf=\"!isSearching\"></i>\r\n            <i class=\"fa fa-pulse fa-spinner\" *ngIf=\"isSearching\"></i>\r\n        </button>\r\n    </div>\r\n    <div class=\"form-group cm-mgl-5\">\r\n        <button class=\"btn default\" type=\"button\" (click)=\"resetFormSearch()\">\r\n            <i class=\"fa fa-refresh\"></i>\r\n        </button>\r\n    </div>\r\n    <div class=\"form-group pull-right\">\r\n        <a class=\"btn blue cm-mgr-5\" *ngIf=\"permission.add\" i18n=\"@@add\" (click)=\"add()\"\r\n           type=\"button\">\r\n            Thêm\r\n        </a>\r\n    </div>\r\n</form>\r\n<div class=\"table-responsive\">\r\n    <table class=\"table table-striped table-hover\">\r\n        <thead>\r\n        <tr>\r\n            <th class=\"middle center w50\" i18n=\"@@no\">STT</th>\r\n            <th class=\"middle\" i18n=\"@@receiptNo\">Số chứng từ</th>\r\n            <th class=\"middle\" i18n=\"@@invoiceNo\">Số hóa đơn</th>\r\n            <th class=\"middle\" i18n=\"@@receiptType\">Loại nhập</th>\r\n            <th class=\"middle w150\" i18n=\"@@entryDate\">Ngày nhập</th>\r\n            <th class=\"middle center\" i18n=\"@@supplier\">Nhà cung cấp</th>\r\n            <th class=\"middle text-right\" i18n=\"@@totalItems\">Tổng sản phẩm</th>\r\n            <!--<th class=\"middle\" i18n=\"@@total\">Total</th>-->\r\n            <!--<th class=\"middle\" i18n=\"@@discount\">Discount</th>-->\r\n            <th class=\"middle text-right\" i18n=\"@@totalAmounts\">Tổng tiền</th>\r\n            <th class=\"middle\" i18n=\"@@deliver\">Người giao hàng</th>\r\n            <th class=\"middle\" i18n=\"@@receiver\">Người nhận hàng</th>\r\n            <th class=\"middle text-right w50\" i18n=\"@@action\" *ngIf=\"permission.edit || permission.delete\">Hành động\r\n            </th>\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n        <tr *ngFor=\"let item of listItems$ | async; let i = index\">\r\n            <td class=\"center\">{{ (currentPage - 1) * pageSize + i + 1 }}</td>\r\n            <td>{{ item.receiptNo }}</td>\r\n            <td>{{ item.invoiceNo }}</td>\r\n            <td>\r\n                <span class=\"badge\"\r\n                      [class.badge-info]=\"item.type === goodsReceiptNoteType.newPurchase\"\r\n                      [class.badge-danger]=\"item.type === goodsReceiptNoteType.customerReturn\"\r\n                      [class.badge-warning]=\"item.type === goodsReceiptNoteType.transfer\"\r\n                      [class.badge-default]=\"item.type === goodsReceiptNoteType.inventory\">\r\n                    {item.type, select, 0 {Nhập mua mới} 1 {Nhập khách trả} 2 {Nhập điều chuyển} 3 {Nhập kiểm kê} other{}}\r\n                </span>\r\n            </td>\r\n            <td>{{ item.entryDate | dateTimeFormat:'DD/MM/YYYY HH:mm' }}</td>\r\n            <td>{{ item.supplierName }}</td>\r\n            <td class=\"text-right\">{{ item.totalItems }}</td>\r\n            <td class=\"text-right\">{{ item.totalAmounts | ghmCurrency }}</td>\r\n            <td>{{ item.deliverFullName }}</td>\r\n            <td>{{ item.creatorFullName }}</td>\r\n            <td class=\"text-right\">\r\n                <nh-dropdown>\r\n                    <button type=\"button\" class=\"btn btn-sm btn-no-background no-border\" matTooltip=\"Menu\">\r\n                        <mat-icon>more_horiz</mat-icon>\r\n                    </button>\r\n                    <ul class=\"nh-dropdown-menu right\">\r\n                        <li *ngIf=\"permission.view\" (click)=\"detail(item.id)\">\r\n                            <a href=\"javascript://\">\r\n                                <!--<i class=\"fa fa-eye\"></i>-->\r\n                                <mat-icon class=\"menu-icon\">info</mat-icon>\r\n                                <span i18n=\"@@detail\">Chi tiết</span>\r\n                            </a>\r\n                        </li>\r\n                        <li *ngIf=\"permission.edit && item.type === goodsReceiptNoteType.newPurchase\"\r\n                            (click)=\"edit(item.id)\">\r\n                            <a href=\"javascript://\">\r\n                                <!--<i class=\"fa fa-edit\"></i>-->\r\n                                <mat-icon class=\"menu-icon\">edit</mat-icon>\r\n                                <span i18n=\"@@edit\">Chỉnh sửa</span>\r\n                            </a>\r\n                        </li>\r\n                        <li *ngIf=\"permission.print\" (click)=\"print(item.id)\">\r\n                            <a href=\"javascript://\">\r\n                                <!--<i class=\"fa fa-print menu-icon\"></i>-->\r\n                                <mat-icon class=\"menu-icon\">print</mat-icon>\r\n                                <span i18n=\"@@printReceipt\">In hóa đơn</span>\r\n                            </a>\r\n                        </li>\r\n                        <li *ngIf=\"permission.print\" (click)=\"printBarcode(item.id)\">\r\n                            <a href=\"javascript://\">\r\n                                <!--<i class=\"fa fa-print menu-icon\"></i>-->\r\n                                <mat-icon class=\"menu-icon\">print</mat-icon>\r\n                                <span i18n=\"@@printBarcode\">In mã vạch</span>\r\n                            </a>\r\n                        </li>\r\n                    </ul><!-- END: nh-dropdown-menu -->\r\n                </nh-dropdown>\r\n            </td>\r\n        </tr>\r\n        </tbody>\r\n    </table>\r\n</div>\r\n\r\n<ghm-paging\r\n    class=\"pull-right\"\r\n    [totalRows]=\"totalRows\"\r\n    [currentPage]=\"currentPage\"\r\n    [pageShow]=\"6\"\r\n    [pageSize]=\"pageSize\"\r\n    (pageClick)=\"search($event)\"\r\n    i18n=\"@@product\" i18n-pageName\r\n    [pageName]=\"'product'\">\r\n</ghm-paging>\r\n\r\n<swal\r\n    #confirmDeleteProduct\r\n    i18n=\"@@confirmDeleteProduct\"\r\n    i18n-title=\"@@confirmTitleDeleteProduct\"\r\n    i18n-text=\"@@confirmTextDeleteProduct\"\r\n    title=\"Are you sure for delete this product?\"\r\n    text=\"You can't recover this product after delete.\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Accept\"\r\n    cancelButtonText=\"Cancel\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<div dynamic-component-host></div>\r\n\r\n<app-goods-receipt-note-form\r\n    (saveSuccessful)=\"onSaveSuccess($event)\"></app-goods-receipt-note-form>\r\n\r\n<nh-menu #nhMenu>\r\n    <nh-menu-item (clicked)=\"detail($event.id)\" *ngIf=\"permission.view\">\r\n        <!--<i class=\"fa fa-eye menu-icon\"></i>-->\r\n        <mat-icon class=\"menu-icon\">info</mat-icon>\r\n        <span i18n=\"@@detail\">Chi tiết</span>\r\n    </nh-menu-item>\r\n    <nh-menu-item (clicked)=\"edit($event.id)\"\r\n                  *ngIf=\"permission.edit\">\r\n        <!--<i class=\"fa fa-edit menu-icon\"></i>-->\r\n        <mat-icon class=\"menu-icon\">edit</mat-icon>\r\n        <span i18n=\"@@edit\">Chỉnh sửa</span>\r\n    </nh-menu-item>\r\n    <nh-menu-item (clicked)=\"print($event.id)\" *ngIf=\"permission.print\">\r\n        <!--<i class=\"fa fa-print menu-icon\"></i>-->\r\n        <mat-icon class=\"menu-icon\">print</mat-icon>\r\n        <span i18n=\"@@printReceipt\">In hóa đơn</span>\r\n    </nh-menu-item>\r\n    <nh-menu-item (clicked)=\"printBarcode($event.id)\" *ngIf=\"permission.print\">\r\n        <!--<i class=\"fa fa-print menu-icon\"></i>-->\r\n        <mat-icon class=\"menu-icon\">print</mat-icon>\r\n        <span i18n=\"@@printBarcode\">In mã vạch</span>\r\n    </nh-menu-item>\r\n</nh-menu>\r\n\r\n<app-goods-receipt-note-print></app-goods-receipt-note-print>\r\n<app-goods-receipt-note-print-barcode></app-goods-receipt-note-print-barcode>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.component.scss":
/*!**********************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.component.scss ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".goods-receipt-note-container .title {\n  font-size: 25px;\n  text-align: center;\n  font-weight: bold;\n  text-transform: uppercase;\n  margin-top: 10px; }\n\n.goods-receipt-note-container .form-group div {\n  display: flex;\n  align-items: center; }\n\n.goods-receipt-note-container .form-group div app-follow-suggestion, .goods-receipt-note-container .form-group div app-deliver-suggestion, .goods-receipt-note-container .form-group div app-warehouse-suggestion {\n    width: 100%; }\n\n.goods-receipt-note-container .form-group label {\n  color: #666; }\n\n.goods-receipt-note-container .entry-date-wrapper {\n  text-align: center;\n  display: flex;\n  width: 100%;\n  align-items: center;\n  justify-content: center;\n  margin-bottom: 20px; }\n\n.goods-receipt-note-container .entry-date-wrapper div {\n    width: 90px;\n    display: flex;\n    align-items: center; }\n\n.goods-receipt-note-container .amountsInWord {\n  width: 200px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy93YXJlaG91c2UvZ29vZHMvZ29vZHMtcmVjZWlwdC1ub3RlL0Q6XFxQcm9qZWN0XFxHaG1BcHBsaWNhdGlvblxcY2xpZW50c1xcZ2htYXBwbGljYXRpb25jbGllbnQvc3JjXFxhcHBcXG1vZHVsZXNcXHdhcmVob3VzZVxcZ29vZHNcXGdvb2RzLXJlY2VpcHQtbm90ZVxcZ29vZHMtcmVjZWlwdC1ub3RlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRVEsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIseUJBQXlCO0VBQ3pCLGdCQUFnQixFQUFBOztBQU54QjtFQVdZLGFBQWE7RUFDYixtQkFBbUIsRUFBQTs7QUFaL0I7SUFlZ0IsV0FBVyxFQUFBOztBQWYzQjtFQW9CWSxXQUFXLEVBQUE7O0FBcEJ2QjtFQXlCUSxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLFdBQVc7RUFDWCxtQkFBbUI7RUFDbkIsdUJBQXVCO0VBQ3ZCLG1CQUFtQixFQUFBOztBQTlCM0I7SUFpQ1ksV0FBVztJQUNYLGFBQWE7SUFDYixtQkFBbUIsRUFBQTs7QUFuQy9CO0VBd0NRLFlBQVksRUFBQSIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvd2FyZWhvdXNlL2dvb2RzL2dvb2RzLXJlY2VpcHQtbm90ZS9nb29kcy1yZWNlaXB0LW5vdGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZ29vZHMtcmVjZWlwdC1ub3RlLWNvbnRhaW5lciB7XHJcbiAgICAudGl0bGUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5mb3JtLWdyb3VwIHtcclxuICAgICAgICBkaXYge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cclxuICAgICAgICAgICAgYXBwLWZvbGxvdy1zdWdnZXN0aW9uLCBhcHAtZGVsaXZlci1zdWdnZXN0aW9uLCBhcHAtd2FyZWhvdXNlLXN1Z2dlc3Rpb24ge1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxhYmVsIHtcclxuICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5lbnRyeS1kYXRlLXdyYXBwZXIge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuXHJcbiAgICAgICAgZGl2IHtcclxuICAgICAgICAgICAgd2lkdGg6IDkwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5hbW91bnRzSW5Xb3JkIHtcclxuICAgICAgICB3aWR0aDogMjAwcHg7XHJcbiAgICB9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.component.ts ***!
  \********************************************************************************************/
/*! exports provided: GoodsReceiptNoteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsReceiptNoteComponent", function() { return GoodsReceiptNoteComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./goods-receipt-note.service */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.service.ts");
/* harmony import */ var _goods_receipt_note_form_goods_receipt_note_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./goods-receipt-note-form/goods-receipt-note-form.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _goods_receipt_note_detail_goods_receipt_note_detail_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./goods-receipt-note-detail/goods-receipt-note-detail.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-detail/goods-receipt-note-detail.component.ts");
/* harmony import */ var _core_directives_dynamic_component_host_directive__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../core/directives/dynamic-component-host.directive */ "./src/app/core/directives/dynamic-component-host.directive.ts");
/* harmony import */ var _goods_receipt_note_print_goods_receipt_note_print_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./goods-receipt-note-print/goods-receipt-note-print.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-print/goods-receipt-note-print.component.ts");
/* harmony import */ var _goods_receipt_note_print_barcode_goods_receipt_note_print_barcode_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./goods-receipt-note-print-barcode/goods-receipt-note-print-barcode.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-print-barcode/goods-receipt-note-print-barcode.component.ts");
/* harmony import */ var _goods_receipt_note_type_const__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./goods-receipt-note-type.const */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-type.const.ts");












var GoodsReceiptNoteComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](GoodsReceiptNoteComponent, _super);
    function GoodsReceiptNoteComponent(componentFactoryResolver, route, goodsReceiptNoteService) {
        var _this = _super.call(this) || this;
        _this.componentFactoryResolver = componentFactoryResolver;
        _this.route = route;
        _this.goodsReceiptNoteService = goodsReceiptNoteService;
        _this.goodsReceiptNoteType = _goods_receipt_note_type_const__WEBPACK_IMPORTED_MODULE_11__["GoodsReceiptNoteType"];
        _this.warehouses = [];
        _this.suppliers = [];
        _this.listItems$ = _this.route.data.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            var data = result.data;
            _this.totalRows = data.totalRows;
            return data.items;
        }));
        return _this;
    }
    GoodsReceiptNoteComponent.prototype.ngOnInit = function () {
        this.appService.setupPage(this.pageId.WAREHOUSE, this.pageId.GOODS_RECEIPT_NOTE, 'Quản lý hàng hóa', 'Danh sách phiếu nhập');
    };
    GoodsReceiptNoteComponent.prototype.ngAfterViewInit = function () {
        // this.goodsReceiptNoteFormComponent.add();
    };
    GoodsReceiptNoteComponent.prototype.onWarehouseSelected = function (warehouse) {
        this.warehouseId = warehouse.id;
        this.search();
    };
    GoodsReceiptNoteComponent.prototype.onWarehouseRemoved = function () {
        this.warehouseId = null;
        this.search();
    };
    GoodsReceiptNoteComponent.prototype.onSupplierSelected = function (supplier) {
        this.supplierId = supplier.id;
        this.search();
    };
    GoodsReceiptNoteComponent.prototype.onSupplierRemoved = function () {
        this.supplierId = null;
        this.search();
    };
    GoodsReceiptNoteComponent.prototype.onSaveSuccess = function (event) {
        if (event.isPrint) {
            this.goodsReceiptNotePrintComponent.print(event.id);
        }
        this.search(1);
    };
    GoodsReceiptNoteComponent.prototype.resetFormSearch = function () {
        this.keyword = '';
        this.fromDate = '';
        this.toDate = '';
        this.supplierId = '';
        this.warehouseId = '';
        this.deliverId = '';
        this.search(1);
    };
    GoodsReceiptNoteComponent.prototype.search = function (currentPage) {
        var _this = this;
        if (currentPage === void 0) { currentPage = 1; }
        this.currentPage = currentPage;
        this.isSearching = true;
        this.listItems$ = this.goodsReceiptNoteService
            .search(this.keyword, this.supplierId, this.warehouseId, this.deliverId, this.fromDate, this.toDate, this.currentPage)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.isSearching = false; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.totalRows = result.totalRows;
            return result.items;
        }));
    };
    GoodsReceiptNoteComponent.prototype.add = function () {
        this.goodsReceiptNoteFormComponent.add();
    };
    GoodsReceiptNoteComponent.prototype.edit = function (id) {
        this.goodsReceiptNoteFormComponent.edit(id);
    };
    GoodsReceiptNoteComponent.prototype.confirm = function () {
    };
    GoodsReceiptNoteComponent.prototype.detail = function (id) {
        this.loadDetailComponent(id);
    };
    GoodsReceiptNoteComponent.prototype.print = function (id) {
        this.goodsReceiptNotePrintComponent.print(id);
    };
    GoodsReceiptNoteComponent.prototype.printBarcode = function (id) {
        this.goodsReceiptNotePrintBarcodeComponent.print(id);
    };
    GoodsReceiptNoteComponent.prototype.loadDetailComponent = function (id) {
        var componentFactory = this.componentFactoryResolver.resolveComponentFactory(_goods_receipt_note_detail_goods_receipt_note_detail_component__WEBPACK_IMPORTED_MODULE_7__["GoodsReceiptNoteDetailComponent"]);
        var viewContainerRef = this.dynamicComponentHostDirective.viewContainerRef;
        viewContainerRef.clear();
        var componentRef = viewContainerRef.createComponent(componentFactory);
        componentRef.instance.getDetail(id);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_goods_receipt_note_form_goods_receipt_note_form_component__WEBPACK_IMPORTED_MODULE_4__["GoodsReceiptNoteFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _goods_receipt_note_form_goods_receipt_note_form_component__WEBPACK_IMPORTED_MODULE_4__["GoodsReceiptNoteFormComponent"])
    ], GoodsReceiptNoteComponent.prototype, "goodsReceiptNoteFormComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_core_directives_dynamic_component_host_directive__WEBPACK_IMPORTED_MODULE_8__["DynamicComponentHostDirective"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _core_directives_dynamic_component_host_directive__WEBPACK_IMPORTED_MODULE_8__["DynamicComponentHostDirective"])
    ], GoodsReceiptNoteComponent.prototype, "dynamicComponentHostDirective", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_goods_receipt_note_print_goods_receipt_note_print_component__WEBPACK_IMPORTED_MODULE_9__["GoodsReceiptNotePrintComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _goods_receipt_note_print_goods_receipt_note_print_component__WEBPACK_IMPORTED_MODULE_9__["GoodsReceiptNotePrintComponent"])
    ], GoodsReceiptNoteComponent.prototype, "goodsReceiptNotePrintComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_goods_receipt_note_print_barcode_goods_receipt_note_print_barcode_component__WEBPACK_IMPORTED_MODULE_10__["GoodsReceiptNotePrintBarcodeComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _goods_receipt_note_print_barcode_goods_receipt_note_print_barcode_component__WEBPACK_IMPORTED_MODULE_10__["GoodsReceiptNotePrintBarcodeComponent"])
    ], GoodsReceiptNoteComponent.prototype, "goodsReceiptNotePrintBarcodeComponent", void 0);
    GoodsReceiptNoteComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-goods-receipt-note',
            template: __webpack_require__(/*! ./goods-receipt-note.component.html */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_3__["GoodsReceiptNoteService"]])
    ], GoodsReceiptNoteComponent);
    return GoodsReceiptNoteComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.model.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.model.ts ***!
  \****************************************************************************************/
/*! exports provided: GoodsReceiptNote, GoodsReceiptNoteDetail */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsReceiptNote", function() { return GoodsReceiptNote; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsReceiptNoteDetail", function() { return GoodsReceiptNoteDetail; });
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);

var GoodsReceiptNote = /** @class */ (function () {
    function GoodsReceiptNote() {
        var now = moment__WEBPACK_IMPORTED_MODULE_0__();
        this.day = now.date();
        this.month = now.month() + 1;
        this.year = now.year();
        this.entryDate = now.format('YYYY/MM/DD HH:mm:ss');
        this.invoiceDate = now.format('YYYY/MM/DD HH:mm:ss');
    }
    return GoodsReceiptNote;
}());

var GoodsReceiptNoteDetail = /** @class */ (function () {
    function GoodsReceiptNoteDetail() {
    }
    return GoodsReceiptNoteDetail;
}());



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.service.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.service.ts ***!
  \******************************************************************************************/
/*! exports provided: GoodsReceiptNoteService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsReceiptNoteService", function() { return GoodsReceiptNoteService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");







var GoodsReceiptNoteService = /** @class */ (function () {
    function GoodsReceiptNoteService(appConfig, http, spinnerService, toastr) {
        this.appConfig = appConfig;
        this.http = http;
        this.spinnerService = spinnerService;
        this.toastr = toastr;
        this.url = 'api/v1/warehouse/goods-receipt-notes';
        this.url = "" + appConfig.API_GATEWAY_URL + this.url;
    }
    GoodsReceiptNoteService.prototype.resolve = function (route, state) {
        var params = route.queryParams;
        return this.search(params.keyword, params.supplierId, params.warehouseId, params.deliverId, params.fromDate, params.toDate, params.page, params.pageSize);
    };
    GoodsReceiptNoteService.prototype.search = function (keyword, supplierId, warehouseId, deliverId, fromDate, toDate, page, pageSize) {
        if (pageSize === void 0) { pageSize = 10; }
        return this.http.get(this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('supplierId', supplierId ? supplierId : '')
                .set('warehouseId', warehouseId ? warehouseId : '')
                .set('fromDate', fromDate ? fromDate : '')
                .set('toDate', toDate ? toDate : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString())
        });
    };
    GoodsReceiptNoteService.prototype.getDetail = function (receiptId) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/" + receiptId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            return result.data;
        }));
    };
    GoodsReceiptNoteService.prototype.insert = function (goodsReceiptNote) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url, goodsReceiptNote)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    GoodsReceiptNoteService.prototype.update = function (id, goodsReceiptNote) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/" + id, goodsReceiptNote)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    GoodsReceiptNoteService.prototype.insertItem = function (receiptId, goodsReceiptNoteItem) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/" + receiptId + "/details", goodsReceiptNoteItem)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    GoodsReceiptNoteService.prototype.updateItem = function (receiptId, itemId, goodsReceiptNoteItem) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/" + receiptId + "/details/" + itemId, goodsReceiptNoteItem)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    GoodsReceiptNoteService.prototype.deleteItem = function (receiptId, itemId) {
        var _this = this;
        this.spinnerService.show();
        return this.http.delete(this.url + "/" + receiptId + "/details/" + itemId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    GoodsReceiptNoteService.prototype.lotSuggestion = function (keyword, page, pageSize) {
        return this.http.get(this.url + "/lots", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString())
        });
    };
    GoodsReceiptNoteService.prototype.deliverSuggestion = function (supplierId, keyword, page, pageSize) {
        return this.http.get(this.url + "/delivers", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('supplierId', supplierId ? supplierId : '')
                .set('keyword', keyword ? keyword : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString())
        });
    };
    GoodsReceiptNoteService.prototype.followSuggestion = function (keyword, page, pageSize) {
        return this.http.get(this.url + "/follows", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString())
        });
    };
    GoodsReceiptNoteService.prototype.getBarcode = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/" + id + "/barcodes")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) { return result.items; }));
    };
    GoodsReceiptNoteService.prototype.getProductInfoByCode = function (code, warehouseId, type, deliveryDate) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .get(this.url + "/details/" + code, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('warehouseId', warehouseId)
                .set('code', code)
                .set('type', type.toString())
                .set('deliveryDate', deliveryDate)
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) { return result.data; }));
    };
    GoodsReceiptNoteService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_5__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_3__["SpinnerService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]])
    ], GoodsReceiptNoteService);
    return GoodsReceiptNoteService;
}());



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: goodsRoutes, GoodsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "goodsRoutes", function() { return goodsRoutes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsRoutingModule", function() { return GoodsRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _goods_receipt_note_goods_receipt_note_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./goods-receipt-note/goods-receipt-note.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.component.ts");
/* harmony import */ var _goods_delivery_note_goods_delivery_note_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./goods-delivery-note/goods-delivery-note.component */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.component.ts");
/* harmony import */ var _goods_delivery_note_goods_delivery_note_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./goods-delivery-note/goods-delivery-note.service */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.service.ts");
/* harmony import */ var _goods_receipt_note_goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./goods-receipt-note/goods-receipt-note.service */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.service.ts");
/* harmony import */ var _inventory_inventory_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./inventory/inventory.component */ "./src/app/modules/warehouse/goods/inventory/inventory.component.ts");
/* harmony import */ var _inventory_service_inventory_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./inventory/service/inventory.service */ "./src/app/modules/warehouse/goods/inventory/service/inventory.service.ts");
/* harmony import */ var _inventory_report_inventory_report_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./inventory-report/inventory-report.component */ "./src/app/modules/warehouse/goods/inventory-report/inventory-report.component.ts");
/* harmony import */ var _inventory_report_inventory_report_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./inventory-report/inventory-report.service */ "./src/app/modules/warehouse/goods/inventory-report/inventory-report.service.ts");
/* harmony import */ var _inventory_inventory_form_inventory_form_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./inventory/inventory-form/inventory-form.component */ "./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-form.component.ts");
/* harmony import */ var _inventory_inventory_detail_inventory_detail_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./inventory/inventory-detail/inventory-detail.component */ "./src/app/modules/warehouse/goods/inventory/inventory-detail/inventory-detail.component.ts");













var goodsRoutes = [
    {
        path: '',
        component: _goods_receipt_note_goods_receipt_note_component__WEBPACK_IMPORTED_MODULE_3__["GoodsReceiptNoteComponent"],
        resolve: {
            data: _goods_receipt_note_goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_6__["GoodsReceiptNoteService"]
        }
    },
    {
        path: 'goods-receipt-notes',
        component: _goods_receipt_note_goods_receipt_note_component__WEBPACK_IMPORTED_MODULE_3__["GoodsReceiptNoteComponent"],
        resolve: {
            data: _goods_receipt_note_goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_6__["GoodsReceiptNoteService"]
        }
    },
    {
        path: 'goods-delivery-notes',
        component: _goods_delivery_note_goods_delivery_note_component__WEBPACK_IMPORTED_MODULE_4__["GoodsDeliveryNoteComponent"],
        resolve: {
            data: _goods_delivery_note_goods_delivery_note_service__WEBPACK_IMPORTED_MODULE_5__["GoodsDeliveryNoteService"]
        }
    },
    {
        path: 'inventories',
        component: _inventory_inventory_component__WEBPACK_IMPORTED_MODULE_7__["InventoryComponent"],
        resolve: {
            data: _inventory_service_inventory_service__WEBPACK_IMPORTED_MODULE_8__["InventoryService"]
        }
    },
    {
        path: 'inventories/add',
        component: _inventory_inventory_form_inventory_form_component__WEBPACK_IMPORTED_MODULE_11__["InventoryFormComponent"],
    },
    {
        path: 'inventories/edit/:id',
        component: _inventory_inventory_form_inventory_form_component__WEBPACK_IMPORTED_MODULE_11__["InventoryFormComponent"],
    },
    {
        path: 'inventories/:id',
        component: _inventory_inventory_detail_inventory_detail_component__WEBPACK_IMPORTED_MODULE_12__["InventoryDetailComponent"],
    },
    {
        path: 'inventory-report',
        component: _inventory_report_inventory_report_component__WEBPACK_IMPORTED_MODULE_9__["InventoryReportComponent"],
        resolve: {
            data: _inventory_report_inventory_report_service__WEBPACK_IMPORTED_MODULE_10__["InventoryReportService"]
        }
    }
];
var GoodsRoutingModule = /** @class */ (function () {
    function GoodsRoutingModule() {
    }
    GoodsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(goodsRoutes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
            providers: [_goods_receipt_note_goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_6__["GoodsReceiptNoteService"], _goods_delivery_note_goods_delivery_note_service__WEBPACK_IMPORTED_MODULE_5__["GoodsDeliveryNoteService"], _inventory_service_inventory_service__WEBPACK_IMPORTED_MODULE_8__["InventoryService"], _inventory_report_inventory_report_service__WEBPACK_IMPORTED_MODULE_10__["InventoryReportService"]]
        })
    ], GoodsRoutingModule);
    return GoodsRoutingModule;
}());



/***/ }),

/***/ "./src/app/modules/warehouse/goods/goods.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/modules/warehouse/goods/goods.module.ts ***!
  \*********************************************************/
/*! exports provided: GoodsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoodsModule", function() { return GoodsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _goods_receipt_note_goods_receipt_note_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./goods-receipt-note/goods-receipt-note.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.component.ts");
/* harmony import */ var _goods_delivery_note_goods_delivery_note_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./goods-delivery-note/goods-delivery-note.component */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.component.ts");
/* harmony import */ var _goods_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./goods-routing.module */ "./src/app/modules/warehouse/goods/goods-routing.module.ts");
/* harmony import */ var _goods_receipt_note_goods_receipt_note_form_goods_receipt_note_form_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form.component.ts");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _shareds_components_nh_suggestion_nh_suggestion_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../shareds/components/nh-suggestion/nh-suggestion.module */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.module.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../shareds/components/nh-modal/nh-modal.module */ "./src/app/shareds/components/nh-modal/nh-modal.module.ts");
/* harmony import */ var _shareds_components_ghm_paging_ghm_paging_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../shareds/components/ghm-paging/ghm-paging.module */ "./src/app/shareds/components/ghm-paging/ghm-paging.module.ts");
/* harmony import */ var _shareds_components_nh_image_viewer_nh_image_viewer_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../shareds/components/nh-image-viewer/nh-image-viewer.module */ "./src/app/shareds/components/nh-image-viewer/nh-image-viewer.module.ts");
/* harmony import */ var _shareds_components_nh_wizard_nh_wizard_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../shareds/components/nh-wizard/nh-wizard.module */ "./src/app/shareds/components/nh-wizard/nh-wizard.module.ts");
/* harmony import */ var _shareds_components_nh_tab_nh_tab_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../../shareds/components/nh-tab/nh-tab.module */ "./src/app/shareds/components/nh-tab/nh-tab.module.ts");
/* harmony import */ var _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @toverux/ngx-sweetalert2 */ "./node_modules/@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
/* harmony import */ var _shareds_components_nh_dropdown_nh_dropdown_module__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../../shareds/components/nh-dropdown/nh-dropdown.module */ "./src/app/shareds/components/nh-dropdown/nh-dropdown.module.ts");
/* harmony import */ var _shareds_pipe_datetime_format_datetime_format_module__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../../shareds/pipe/datetime-format/datetime-format.module */ "./src/app/shareds/pipe/datetime-format/datetime-format.module.ts");
/* harmony import */ var _shareds_components_nh_context_menu_nh_context_menu_module__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../../shareds/components/nh-context-menu/nh-context-menu.module */ "./src/app/shareds/components/nh-context-menu/nh-context-menu.module.ts");
/* harmony import */ var _shareds_components_nh_tree_nh_tree_module__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../../../shareds/components/nh-tree/nh-tree.module */ "./src/app/shareds/components/nh-tree/nh-tree.module.ts");
/* harmony import */ var _shareds_components_ghm_file_explorer_ghm_file_explorer_module__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../../../shareds/components/ghm-file-explorer/ghm-file-explorer.module */ "./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.module.ts");
/* harmony import */ var _shareds_components_nh_select_nh_select_module__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../../../shareds/components/nh-select/nh-select.module */ "./src/app/shareds/components/nh-select/nh-select.module.ts");
/* harmony import */ var _product_product_module__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../product/product.module */ "./src/app/modules/warehouse/product/product.module.ts");
/* harmony import */ var _goods_receipt_note_goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./goods-receipt-note/goods-receipt-note.service */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.service.ts");
/* harmony import */ var _shareds_components_nh_datetime_picker_nh_date_module__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ../../../shareds/components/nh-datetime-picker/nh-date.module */ "./src/app/shareds/components/nh-datetime-picker/nh-date.module.ts");
/* harmony import */ var _goods_delivery_note_goods_delivery_note_form_goods_delivery_note_form_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./goods-delivery-note/goods-delivery-note-form/goods-delivery-note-form.component */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-form/goods-delivery-note-form.component.ts");
/* harmony import */ var _goods_delivery_note_goods_delivery_note_form_list_product_goods_delivery_note_product_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./goods-delivery-note/goods-delivery-note-form/list-product/goods-delivery-note-product.component */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-form/list-product/goods-delivery-note-product.component.ts");
/* harmony import */ var _shareds_components_ghm_user_suggestion_ghm_user_suggestion_module__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ../../../shareds/components/ghm-user-suggestion/ghm-user-suggestion.module */ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.module.ts");
/* harmony import */ var _warehouse_module__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ../warehouse.module */ "./src/app/modules/warehouse/warehouse.module.ts");
/* harmony import */ var _goods_receipt_note_goods_receipt_note_form_goods_receipt_note_form_product_goods_receipt_note_form_product_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form-product/goods-receipt-note-form-product.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form-product/goods-receipt-note-form-product.component.ts");
/* harmony import */ var _goods_receipt_note_goods_receipt_note_form_goods_receipt_note_form_product_goods_receipt_note_form_product_form_goods_receipt_note_form_product_form_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form-product/goods-receipt-note-form-product-form/goods-receipt-note-form-product-form.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-form/goods-receipt-note-form-product/goods-receipt-note-form-product-form/goods-receipt-note-form-product-form.component.ts");
/* harmony import */ var _lot_suggestion_lot_suggestion_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./lot-suggestion/lot-suggestion.component */ "./src/app/modules/warehouse/goods/lot-suggestion/lot-suggestion.component.ts");
/* harmony import */ var _deliver_suggestion_deliver_suggestion_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./deliver-suggestion/deliver-suggestion.component */ "./src/app/modules/warehouse/goods/deliver-suggestion/deliver-suggestion.component.ts");
/* harmony import */ var _goods_receipt_note_follow_suggestion_follow_suggestion_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./goods-receipt-note/follow-suggestion/follow-suggestion.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/follow-suggestion/follow-suggestion.component.ts");
/* harmony import */ var _goods_receipt_note_goods_receipt_note_detail_goods_receipt_note_detail_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./goods-receipt-note/goods-receipt-note-detail/goods-receipt-note-detail.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-detail/goods-receipt-note-detail.component.ts");
/* harmony import */ var _shareds_pipe_format_number_format_number_module__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ../../../shareds/pipe/format-number/format-number.module */ "./src/app/shareds/pipe/format-number/format-number.module.ts");
/* harmony import */ var _goods_delivery_note_goods_delivery_note_detail_goods_delivery_note_detail_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./goods-delivery-note/goods-delivery-note-detail/goods-delivery-note-detail.component */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-detail/goods-delivery-note-detail.component.ts");
/* harmony import */ var _shareds_pipe_format_money_string_format_money_string_module__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ../../../shareds/pipe/format-money-string/format-money-string.module */ "./src/app/shareds/pipe/format-money-string/format-money-string.module.ts");
/* harmony import */ var _inventory_inventory_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./inventory/inventory.component */ "./src/app/modules/warehouse/goods/inventory/inventory.component.ts");
/* harmony import */ var _inventory_inventory_form_inventory_form_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./inventory/inventory-form/inventory-form.component */ "./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-form.component.ts");
/* harmony import */ var _inventory_inventory_form_inventory_product_inventory_product_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./inventory/inventory-form/inventory-product/inventory-product.component */ "./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-product/inventory-product.component.ts");
/* harmony import */ var _inventory_inventory_form_inventory_member_inventory_member_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./inventory/inventory-form/inventory-member/inventory-member.component */ "./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-member/inventory-member.component.ts");
/* harmony import */ var _shareds_pipe_ghm_amount_to_word_ghm_amount_to_word_module__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ../../../shareds/pipe/ghm-amount-to-word/ghm-amount-to-word.module */ "./src/app/shareds/pipe/ghm-amount-to-word/ghm-amount-to-word.module.ts");
/* harmony import */ var _shareds_components_ghm_mask_ghm_mask_module__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ../../../shareds/components/ghm-mask/ghm-mask.module */ "./src/app/shareds/components/ghm-mask/ghm-mask.module.ts");
/* harmony import */ var _inventory_report_inventory_report_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./inventory-report/inventory-report.component */ "./src/app/modules/warehouse/goods/inventory-report/inventory-report.component.ts");
/* harmony import */ var _inventory_report_inventory_report_detail_inventory_report_detail_component__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./inventory-report/inventory-report-detail/inventory-report-detail.component */ "./src/app/modules/warehouse/goods/inventory-report/inventory-report-detail/inventory-report-detail.component.ts");
/* harmony import */ var _inventory_report_warehouse_card_warehouse_card_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./inventory-report/warehouse-card/warehouse-card.component */ "./src/app/modules/warehouse/goods/inventory-report/warehouse-card/warehouse-card.component.ts");
/* harmony import */ var _goods_receipt_note_goods_receipt_note_print_goods_receipt_note_print_component__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./goods-receipt-note/goods-receipt-note-print/goods-receipt-note-print.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-print/goods-receipt-note-print.component.ts");
/* harmony import */ var _goods_receipt_note_goods_receipt_note_print_barcode_goods_receipt_note_print_barcode_component__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./goods-receipt-note/goods-receipt-note-print-barcode/goods-receipt-note-print-barcode.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-print-barcode/goods-receipt-note-print-barcode.component.ts");
/* harmony import */ var ngx_barcode__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ngx-barcode */ "./node_modules/ngx-barcode/index.js");
/* harmony import */ var _inventory_report_warehouse_card_detail_warehouse_card_detail_component__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./inventory-report/warehouse-card-detail/warehouse-card-detail.component */ "./src/app/modules/warehouse/goods/inventory-report/warehouse-card-detail/warehouse-card-detail.component.ts");
/* harmony import */ var _goods_delivery_note_goods_delivery_note_print_goods_delivery_note_print_component__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./goods-delivery-note/goods-delivery-note-print/goods-delivery-note-print.component */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-print/goods-delivery-note-print.component.ts");
/* harmony import */ var _hr_organization_organization_module__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ../../hr/organization/organization.module */ "./src/app/modules/hr/organization/organization.module.ts");
/* harmony import */ var _receiver_suggestion_receiver_suggestion_component__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ./receiver-suggestion/receiver-suggestion.component */ "./src/app/modules/warehouse/goods/receiver-suggestion/receiver-suggestion.component.ts");
/* harmony import */ var _warehouse_manager_suggestion_warehouse_manager_suggestion_component__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! ./warehouse-manager-suggestion/warehouse-manager-suggestion.component */ "./src/app/modules/warehouse/goods/warehouse-manager-suggestion/warehouse-manager-suggestion.component.ts");
/* harmony import */ var _inventory_inventory_detail_inventory_detail_component__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! ./inventory/inventory-detail/inventory-detail.component */ "./src/app/modules/warehouse/goods/inventory/inventory-detail/inventory-detail.component.ts");

























































var GoodsModule = /** @class */ (function () {
    function GoodsModule() {
    }
    GoodsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _goods_routing_module__WEBPACK_IMPORTED_MODULE_5__["GoodsRoutingModule"], _product_product_module__WEBPACK_IMPORTED_MODULE_23__["ProductModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["ReactiveFormsModule"], _core_core_module__WEBPACK_IMPORTED_MODULE_7__["CoreModule"], _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatCheckboxModule"], _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatTooltipModule"], _shareds_components_nh_datetime_picker_nh_date_module__WEBPACK_IMPORTED_MODULE_25__["NhDateModule"], _shareds_components_ghm_user_suggestion_ghm_user_suggestion_module__WEBPACK_IMPORTED_MODULE_28__["GhmUserSuggestionModule"],
                _shareds_components_nh_tree_nh_tree_module__WEBPACK_IMPORTED_MODULE_20__["NHTreeModule"], _shareds_components_nh_select_nh_select_module__WEBPACK_IMPORTED_MODULE_22__["NhSelectModule"], _shareds_components_nh_dropdown_nh_dropdown_module__WEBPACK_IMPORTED_MODULE_17__["NhDropdownModule"], _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatIconModule"], _shareds_components_nh_modal_nh_modal_module__WEBPACK_IMPORTED_MODULE_11__["NhModalModule"], _shareds_components_ghm_paging_ghm_paging_module__WEBPACK_IMPORTED_MODULE_12__["GhmPagingModule"], _shareds_components_nh_dropdown_nh_dropdown_module__WEBPACK_IMPORTED_MODULE_17__["NhDropdownModule"],
                _shareds_pipe_datetime_format_datetime_format_module__WEBPACK_IMPORTED_MODULE_18__["DatetimeFormatModule"], _shareds_components_nh_wizard_nh_wizard_module__WEBPACK_IMPORTED_MODULE_14__["NhWizardModule"], _shareds_components_nh_tab_nh_tab_module__WEBPACK_IMPORTED_MODULE_15__["NhTabModule"], _shareds_components_nh_suggestion_nh_suggestion_module__WEBPACK_IMPORTED_MODULE_8__["NhSuggestionModule"], _shareds_components_ghm_file_explorer_ghm_file_explorer_module__WEBPACK_IMPORTED_MODULE_21__["GhmFileExplorerModule"], _shareds_components_nh_context_menu_nh_context_menu_module__WEBPACK_IMPORTED_MODULE_19__["NhContextMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatRadioModule"], _warehouse_module__WEBPACK_IMPORTED_MODULE_29__["WarehouseModule"], _shareds_pipe_format_number_format_number_module__WEBPACK_IMPORTED_MODULE_36__["FormatNumberModule"], _shareds_pipe_datetime_format_datetime_format_module__WEBPACK_IMPORTED_MODULE_18__["DatetimeFormatModule"], _shareds_pipe_format_money_string_format_money_string_module__WEBPACK_IMPORTED_MODULE_38__["FormatMoneyStringModule"], _hr_organization_organization_module__WEBPACK_IMPORTED_MODULE_53__["OrganizationModule"],
                _shareds_components_nh_image_viewer_nh_image_viewer_module__WEBPACK_IMPORTED_MODULE_13__["NhImageViewerModule"], _shareds_pipe_ghm_amount_to_word_ghm_amount_to_word_module__WEBPACK_IMPORTED_MODULE_43__["GhmAmountToWordModule"], _shareds_components_ghm_mask_ghm_mask_module__WEBPACK_IMPORTED_MODULE_44__["GhmMaskModule"], ngx_barcode__WEBPACK_IMPORTED_MODULE_50__["NgxBarcodeModule"], _shareds_components_nh_tab_nh_tab_module__WEBPACK_IMPORTED_MODULE_15__["NhTabModule"], _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatExpansionModule"], _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatButtonModule"],
                _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_16__["SweetAlert2Module"].forRoot({
                    buttonsStyling: false,
                    customClass: 'modal-content',
                    confirmButtonClass: 'btn blue cm-mgr-5',
                    cancelButtonClass: 'btn',
                    showCancelButton: true,
                })
            ],
            declarations: [_goods_receipt_note_goods_receipt_note_component__WEBPACK_IMPORTED_MODULE_3__["GoodsReceiptNoteComponent"], _goods_delivery_note_goods_delivery_note_component__WEBPACK_IMPORTED_MODULE_4__["GoodsDeliveryNoteComponent"], _goods_receipt_note_goods_receipt_note_form_goods_receipt_note_form_component__WEBPACK_IMPORTED_MODULE_6__["GoodsReceiptNoteFormComponent"],
                _goods_delivery_note_goods_delivery_note_form_goods_delivery_note_form_component__WEBPACK_IMPORTED_MODULE_26__["GoodsDeliveryNoteFormComponent"], _goods_delivery_note_goods_delivery_note_form_list_product_goods_delivery_note_product_component__WEBPACK_IMPORTED_MODULE_27__["GoodsDeliveryNoteProductComponent"], _goods_delivery_note_goods_delivery_note_detail_goods_delivery_note_detail_component__WEBPACK_IMPORTED_MODULE_37__["GoodsDeliveryNoteDetailComponent"],
                _goods_delivery_note_goods_delivery_note_form_goods_delivery_note_form_component__WEBPACK_IMPORTED_MODULE_26__["GoodsDeliveryNoteFormComponent"], _goods_delivery_note_goods_delivery_note_form_list_product_goods_delivery_note_product_component__WEBPACK_IMPORTED_MODULE_27__["GoodsDeliveryNoteProductComponent"], _goods_receipt_note_goods_receipt_note_form_goods_receipt_note_form_product_goods_receipt_note_form_product_component__WEBPACK_IMPORTED_MODULE_30__["GoodsReceiptNoteFormProductComponent"], _lot_suggestion_lot_suggestion_component__WEBPACK_IMPORTED_MODULE_32__["LotSuggestionComponent"],
                _goods_receipt_note_goods_receipt_note_form_goods_receipt_note_form_product_goods_receipt_note_form_product_form_goods_receipt_note_form_product_form_component__WEBPACK_IMPORTED_MODULE_31__["GoodsReceiptNoteFormProductFormComponent"], _goods_receipt_note_follow_suggestion_follow_suggestion_component__WEBPACK_IMPORTED_MODULE_34__["FollowSuggestionComponent"],
                _deliver_suggestion_deliver_suggestion_component__WEBPACK_IMPORTED_MODULE_33__["DeliverSuggestionComponent"], _inventory_inventory_component__WEBPACK_IMPORTED_MODULE_39__["InventoryComponent"], _inventory_inventory_form_inventory_form_component__WEBPACK_IMPORTED_MODULE_40__["InventoryFormComponent"], _inventory_inventory_form_inventory_product_inventory_product_component__WEBPACK_IMPORTED_MODULE_41__["InventoryProductComponent"], _inventory_inventory_form_inventory_member_inventory_member_component__WEBPACK_IMPORTED_MODULE_42__["InventoryMemberComponent"],
                _goods_receipt_note_goods_receipt_note_detail_goods_receipt_note_detail_component__WEBPACK_IMPORTED_MODULE_35__["GoodsReceiptNoteDetailComponent"], _inventory_report_inventory_report_component__WEBPACK_IMPORTED_MODULE_45__["InventoryReportComponent"], _inventory_report_inventory_report_detail_inventory_report_detail_component__WEBPACK_IMPORTED_MODULE_46__["InventoryReportDetailComponent"], _inventory_report_warehouse_card_warehouse_card_component__WEBPACK_IMPORTED_MODULE_47__["WarehouseCardComponent"],
                _goods_receipt_note_goods_receipt_note_print_goods_receipt_note_print_component__WEBPACK_IMPORTED_MODULE_48__["GoodsReceiptNotePrintComponent"], _goods_receipt_note_goods_receipt_note_print_barcode_goods_receipt_note_print_barcode_component__WEBPACK_IMPORTED_MODULE_49__["GoodsReceiptNotePrintBarcodeComponent"], _inventory_report_warehouse_card_detail_warehouse_card_detail_component__WEBPACK_IMPORTED_MODULE_51__["WarehouseCardDetailComponent"],
                _goods_delivery_note_goods_delivery_note_print_goods_delivery_note_print_component__WEBPACK_IMPORTED_MODULE_52__["GoodsDeliveryNotePrintComponent"], _receiver_suggestion_receiver_suggestion_component__WEBPACK_IMPORTED_MODULE_54__["ReceiverSuggestionComponent"], _warehouse_manager_suggestion_warehouse_manager_suggestion_component__WEBPACK_IMPORTED_MODULE_55__["WarehouseManagerSuggestionComponent"], _inventory_inventory_detail_inventory_detail_component__WEBPACK_IMPORTED_MODULE_56__["InventoryDetailComponent"]],
            entryComponents: [_goods_receipt_note_goods_receipt_note_detail_goods_receipt_note_detail_component__WEBPACK_IMPORTED_MODULE_35__["GoodsReceiptNoteDetailComponent"], _goods_delivery_note_goods_delivery_note_detail_goods_delivery_note_detail_component__WEBPACK_IMPORTED_MODULE_37__["GoodsDeliveryNoteDetailComponent"]],
            exports: [_lot_suggestion_lot_suggestion_component__WEBPACK_IMPORTED_MODULE_32__["LotSuggestionComponent"]],
            providers: [_goods_receipt_note_goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_24__["GoodsReceiptNoteService"], _goods_receipt_note_goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_24__["GoodsReceiptNoteService"]]
        })
    ], GoodsModule);
    return GoodsModule;
}());



/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory-report/inventory-report-detail/inventory-report-detail.component.html":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory-report/inventory-report-detail/inventory-report-detail.component.html ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal size=\"full\" #inventoryDetailModal\r\n          [backdropStatic]=\"false\"\r\n          (hidden)=\"onModalHidden()\">\r\n    <nh-modal-content class=\"cm-pd-10\">\r\n        <nh-tab #inventoryDailyReportDetailTab>\r\n            <nh-tab-pane\r\n                id=\"warehouseCard\"\r\n                [active]=\"true\"\r\n                title=\"Thẻ kho\"\r\n                i18n-title=\"@@warehouseCard\"\r\n            >\r\n                <app-warehouse-card></app-warehouse-card>\r\n            </nh-tab-pane><!-- END: warehouse card tab pane -->\r\n            <nh-tab-pane\r\n                id=\"warehouseCardDetail\"\r\n                title=\"Chi tiết sổ kho\"\r\n                i18n-title=\"@@warehouseDetailReport\"\r\n                (tabSelected)=\"onWarehouseDetailSelected()\"\r\n            >\r\n                <app-warehouse-card-detail></app-warehouse-card-detail>\r\n            </nh-tab-pane><!-- END: warehouse detail tab pane -->\r\n        </nh-tab>\r\n    </nh-modal-content>\r\n    <nh-modal-footer>\r\n        <button type=\"button\" class=\"btn default\" nh-dismiss=\"true\" i18n=\"@@close\">Đóng</button>\r\n    </nh-modal-footer>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory-report/inventory-report-detail/inventory-report-detail.component.ts":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory-report/inventory-report-detail/inventory-report-detail.component.ts ***!
  \***********************************************************************************************************************/
/*! exports provided: InventoryReportDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryReportDetailComponent", function() { return InventoryReportDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _warehouse_card_warehouse_card_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../warehouse-card/warehouse-card.component */ "./src/app/modules/warehouse/goods/inventory-report/warehouse-card/warehouse-card.component.ts");
/* harmony import */ var _warehouse_card_detail_warehouse_card_detail_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../warehouse-card-detail/warehouse-card-detail.component */ "./src/app/modules/warehouse/goods/inventory-report/warehouse-card-detail/warehouse-card-detail.component.ts");
/* harmony import */ var _shareds_components_nh_tab_nh_tab_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../shareds/components/nh-tab/nh-tab.component */ "./src/app/shareds/components/nh-tab/nh-tab.component.ts");







var InventoryReportDetailComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](InventoryReportDetailComponent, _super);
    function InventoryReportDetailComponent() {
        return _super.call(this) || this;
    }
    InventoryReportDetailComponent.prototype.ngOnInit = function () {
    };
    InventoryReportDetailComponent.prototype.onModalHidden = function () {
        this.productId = null;
        this.warehouseId = null;
        this.fromDate = null;
        this.toDate = null;
        this.inventoryDailyReportDetailTab.setTabActiveById('warehouseCard');
    };
    InventoryReportDetailComponent.prototype.onWarehouseDetailSelected = function () {
        this.warehouseCardDetailComponent.productId = this.productId;
        this.warehouseCardDetailComponent.warehouseId = this.warehouseId;
        this.warehouseCardDetailComponent.fromDate = this.fromDate;
        this.warehouseCardDetailComponent.toDate = this.toDate;
        this.warehouseCardDetailComponent.search();
    };
    InventoryReportDetailComponent.prototype.show = function (productId, warehouseId, fromDate, toDate) {
        this.productId = productId;
        this.warehouseId = warehouseId;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.inventoryDetailModal.open();
        this.warehouseCardComponent.productId = productId;
        this.warehouseCardComponent.warehouseId = warehouseId;
        this.warehouseCardComponent.fromDate = fromDate;
        this.warehouseCardComponent.toDate = toDate;
        this.warehouseCardComponent.search(1);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('inventoryDetailModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_2__["NhModalComponent"])
    ], InventoryReportDetailComponent.prototype, "inventoryDetailModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_warehouse_card_warehouse_card_component__WEBPACK_IMPORTED_MODULE_4__["WarehouseCardComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _warehouse_card_warehouse_card_component__WEBPACK_IMPORTED_MODULE_4__["WarehouseCardComponent"])
    ], InventoryReportDetailComponent.prototype, "warehouseCardComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_warehouse_card_detail_warehouse_card_detail_component__WEBPACK_IMPORTED_MODULE_5__["WarehouseCardDetailComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _warehouse_card_detail_warehouse_card_detail_component__WEBPACK_IMPORTED_MODULE_5__["WarehouseCardDetailComponent"])
    ], InventoryReportDetailComponent.prototype, "warehouseCardDetailComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('inventoryDailyReportDetailTab'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_tab_nh_tab_component__WEBPACK_IMPORTED_MODULE_6__["NhTabComponent"])
    ], InventoryReportDetailComponent.prototype, "inventoryDailyReportDetailTab", void 0);
    InventoryReportDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-inventory-report-detail',
            template: __webpack_require__(/*! ./inventory-report-detail.component.html */ "./src/app/modules/warehouse/goods/inventory-report/inventory-report-detail/inventory-report-detail.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], InventoryReportDetailComponent);
    return InventoryReportDetailComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_3__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory-report/inventory-report.component.html":
/*!******************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory-report/inventory-report.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@listSupplierPageTitle\">Báo cáo tồn kho</span>\r\n    <small i18n=\"@@productModuleTitle\">Quản lý sản phẩm</small>\r\n</h1>\r\n<form class=\"form-inline cm-mgb-10\" (ngSubmit)=\"search(1)\">\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <app-warehouse-suggestion\r\n            (itemSelected)=\"onWarehouseSelected($event)\"></app-warehouse-suggestion>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <nh-date name=\"fromDate\" [(ngModel)]=\"fromDate\"\r\n                 [showTime]=\"true\"\r\n                 [mask]=\"true\"\r\n                 [allowRemove]=\"true\"></nh-date>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <nh-date name=\"toDate\" [(ngModel)]=\"toDate\" [mask]=\"true\"\r\n                 [showTime]=\"true\"\r\n                 [allowRemove]=\"true\"></nh-date>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <input type=\"text\" class=\"form-control\" i18n=\"@@keywordSearch\" i18n-placeholder\r\n               placeholder=\"Enter keyword for search please.\"\r\n               name=\"searchInput\" [(ngModel)]=\"keyword\">\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <button class=\"btn blue\" type=\"submit\">\r\n            <i class=\"fa fa-search\" *ngIf=\"!isSearching\"></i>\r\n            <i class=\"fa fa-pulse fa-spinner\" *ngIf=\"isSearching\"></i>\r\n        </button>\r\n    </div>\r\n    <!--<div class=\"form-group cm-mgl-5\">-->\r\n        <!--<button class=\"btn default\" type=\"button\" (click)=\"resetFormSearch()\">-->\r\n            <!--<i class=\"fa fa-refresh\"></i>-->\r\n        <!--</button>-->\r\n    <!--</div>-->\r\n</form>\r\n<div class=\"table-responsive\">\r\n    <table class=\"table table-bordered table-striped table-hover\">\r\n        <thead>\r\n        <tr>\r\n            <th class=\"middle center w50\" rowspan=\"2\" i18n=\"@@no\">STT</th>\r\n            <th class=\"middle center w250\" rowspan=\"2\" i18n=\"@@receiptNo\">Tên sản phẩm</th>\r\n            <th class=\"middle center w250\" rowspan=\"2\" i18n=\"@@unit\">Đơn vị tính</th>\r\n            <th class=\"middle center\" i18n=\"@@invoiceNo\" colspan=\"2\">Tồn đầu</th>\r\n            <th class=\"middle center\" i18n=\"@@entryDate\" colspan=\"2\">Nhập</th>\r\n            <th class=\"middle center\" i18n=\"@@supplier\" colspan=\"2\">Xuất</th>\r\n            <th class=\"middle center text-right\" i18n=\"@@totalItems\" colspan=\"2\">Tồn cuối</th>\r\n        </tr>\r\n        <tr>\r\n            <th class=\"center w100\">Số tiền</th>\r\n            <th class=\"center w100\">Số lượng</th>\r\n            <th class=\"center w100\">Số tiền</th>\r\n            <th class=\"center w100\">Số lượng</th>\r\n            <th class=\"center w100\">Số tiền</th>\r\n            <th class=\"center w100\">Số lượng</th>\r\n            <th class=\"center w100\">Số tiền</th>\r\n            <th class=\"center w100\">Số lượng</th>\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n        <tr *ngFor=\"let item of listItems$ | async; let i = index\" (click)=\"detail(item.productId)\"\r\n            class=\"cursor-pointer\">\r\n            <td class=\"center\">{{ (currentPage - 1) * pageSize + i + 1 }}</td>\r\n            <td><a href=\"javascript://\">{{ item.productName }}</a></td>\r\n            <td>{{ item.unitName }}</td>\r\n            <td class=\"text-right\">{{ item.openingStockValue | ghmCurrency }}</td>\r\n            <td class=\"text-right\">{{ item.openingStockQuantity | ghmCurrency }}</td>\r\n            <td class=\"text-right\">{{ item.receivingValue | ghmCurrency }}</td>\r\n            <td class=\"text-right\">{{ item.receivingQuantity | ghmCurrency }}</td>\r\n            <td class=\"text-right\"\r\n                [class.color-red]=\"item.deliveringValue < 0\">{{ item.deliveringValue | ghmCurrency }}\r\n            </td>\r\n            <td class=\"text-right\">{{ item.deliveringQuantity | ghmCurrency}}</td>\r\n            <td class=\"text-right\" [class.color-red]=\"item.closingStockValue < 0\">\r\n                {{ item.closingStockValue | ghmCurrency }}\r\n            </td>\r\n            <td class=\"text-right\"\r\n                [class.color-red]=\"item.closingStockQuantity < 0\">\r\n                {{ item.closingStockQuantity | ghmCurrency }}\r\n            </td>\r\n        </tr>\r\n        </tbody>\r\n        <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\" i18n=\"@@total\" class=\"text-right bold\">Total</td>\r\n            <td class=\"text-right bold\">{{ totalOpeningStockValue | ghmCurrency }}</td>\r\n            <td class=\"text-right bold\">{{ totalOpeningStockQuantity | ghmCurrency }}</td>\r\n            <td class=\"text-right bold\">{{ totalReceivingValue | ghmCurrency }}</td>\r\n            <td class=\"text-right bold\">{{ totalReceivingQuantity | ghmCurrency }}</td>\r\n            <td class=\"text-right bold\"\r\n                [class.color-red]=\"totalDeliveringValue < 0\">{{ totalDeliveringValue | ghmCurrency }}\r\n            </td>\r\n            <td class=\"text-right bold\">{{ totalDeliveringQuantity | ghmCurrency }}\r\n            </td>\r\n            <td class=\"text-right bold\">{{ totalClosingStockValue | ghmCurrency }}</td>\r\n            <td class=\"text-right bold\"\r\n                [class.color-red]=\"totalClosingStockQuantity < 0\">{{ totalClosingStockQuantity | ghmCurrency }}\r\n            </td>\r\n        </tr>\r\n        </tfoot>\r\n    </table>\r\n</div><!-- END: .table-responsive -->\r\n\r\n<ghm-paging\r\n    class=\"pull-right\"\r\n    [totalRows]=\"totalRows\"\r\n    [currentPage]=\"currentPage\"\r\n    [pageShow]=\"6\"\r\n    [pageSize]=\"pageSize\"\r\n    (pageClick)=\"search($event)\"\r\n    i18n=\"@@product\" i18n-pageName\r\n    [pageName]=\"'product'\">\r\n</ghm-paging>\r\n\r\n<app-goods-receipt-note-form\r\n    (saveSuccessful)=\"search(1)\"></app-goods-receipt-note-form>\r\n\r\n<app-inventory-report-detail></app-inventory-report-detail>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory-report/inventory-report.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory-report/inventory-report.component.ts ***!
  \****************************************************************************************/
/*! exports provided: InventoryReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryReportComponent", function() { return InventoryReportComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _inventory_report_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./inventory-report.service */ "./src/app/modules/warehouse/goods/inventory-report/inventory-report.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _inventory_report_detail_inventory_report_detail_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./inventory-report-detail/inventory-report-detail.component */ "./src/app/modules/warehouse/goods/inventory-report/inventory-report-detail/inventory-report-detail.component.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);








var InventoryReportComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](InventoryReportComponent, _super);
    function InventoryReportComponent(inventoryReportService) {
        var _this = _super.call(this) || this;
        _this.inventoryReportService = inventoryReportService;
        _this.fromDate = moment__WEBPACK_IMPORTED_MODULE_7__().hours(0).minutes(0).seconds(0).format('YYYY/MM/DD HH:mm');
        _this.toDate = moment__WEBPACK_IMPORTED_MODULE_7__().format('YYYY/MM/DD HH:mm');
        _this.totalOpeningStockQuantity = 0;
        _this.totalOpeningStockValue = 0;
        _this.totalReceivingValue = 0;
        _this.totalReceivingQuantity = 0;
        _this.totalDeliveringValue = 0;
        _this.totalDeliveringQuantity = 0;
        _this.totalClosingStockValue = 0;
        _this.totalClosingStockQuantity = 0;
        _this.pageSize = 20;
        return _this;
    }
    InventoryReportComponent.prototype.ngOnInit = function () {
        this.appService.setupPage(this.pageId.WAREHOUSE, this.pageId.INVENTORY_REPORT, 'Báo cáo nhập xuất tồn', 'Quản lý kho');
    };
    // onFromDateSelected(date: any) {
    //     this.fromDate = date.originalDate;
    // }
    //
    // onToDateSelected(date: any) {
    //     this.toDate = date.originalDate;
    // }
    InventoryReportComponent.prototype.onWarehouseSelected = function (warehouse) {
        this.warehouseId = warehouse.id;
        // this.search();
    };
    InventoryReportComponent.prototype.search = function (currentPage) {
        var _this = this;
        if (currentPage === void 0) { currentPage = 1; }
        this.currentPage = currentPage;
        this.listItems$ = this.inventoryReportService.search(this.keyword, this.warehouseId, this.fromDate, this.toDate, this.currentPage, this.pageSize)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            _this.totalRows = result.totalRows;
            result.items = result.items.map(function (item) {
                item.closingStockQuantity = item.openingStockQuantity + item.receivingQuantity - item.deliveringQuantity;
                item.closingStockValue = item.openingStockValue + item.receivingValue - item.deliveringValue;
                return item;
            });
            if (result.items) {
                _this.totalOpeningStockQuantity = lodash__WEBPACK_IMPORTED_MODULE_5__["sumBy"](result.items, 'openingStockQuantity');
                _this.totalOpeningStockValue = lodash__WEBPACK_IMPORTED_MODULE_5__["sumBy"](result.items, 'openingStockValue');
                _this.totalReceivingQuantity = lodash__WEBPACK_IMPORTED_MODULE_5__["sumBy"](result.items, 'receivingQuantity');
                _this.totalReceivingValue = lodash__WEBPACK_IMPORTED_MODULE_5__["sumBy"](result.items, 'receivingValue');
                _this.totalDeliveringQuantity = lodash__WEBPACK_IMPORTED_MODULE_5__["sumBy"](result.items, 'deliveringQuantity');
                _this.totalDeliveringValue = lodash__WEBPACK_IMPORTED_MODULE_5__["sumBy"](result.items, 'deliveringValue');
                _this.totalClosingStockQuantity = lodash__WEBPACK_IMPORTED_MODULE_5__["sumBy"](result.items, 'closingStockQuantity');
                _this.totalClosingStockValue = lodash__WEBPACK_IMPORTED_MODULE_5__["sumBy"](result.items, 'closingStockValue');
            }
            else {
                _this.totalOpeningStockQuantity = 0;
                _this.totalOpeningStockValue = 0;
                _this.totalReceivingValue = 0;
                _this.totalReceivingQuantity = 0;
                _this.totalDeliveringValue = 0;
                _this.totalDeliveringQuantity = 0;
                _this.totalClosingStockValue = 0;
                _this.totalClosingStockQuantity = 0;
            }
            return result.items;
        }));
    };
    // resetFormSearch() {
    //     this.fromDate = moment().hours(0).minutes(0).seconds(0).format('YYYY/MM/DD HH:mm');
    //     this.toDate = moment().hours(0).minutes(0).seconds(0).format('YYYY/MM/DD HH:mm');
    //     this.warehouseId = null;
    //     this.search(1);
    // }
    InventoryReportComponent.prototype.detail = function (id) {
        this.inventoryReportDetailComponent.show(id, this.warehouseId, this.fromDate, this.toDate);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_inventory_report_detail_inventory_report_detail_component__WEBPACK_IMPORTED_MODULE_6__["InventoryReportDetailComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _inventory_report_detail_inventory_report_detail_component__WEBPACK_IMPORTED_MODULE_6__["InventoryReportDetailComponent"])
    ], InventoryReportComponent.prototype, "inventoryReportDetailComponent", void 0);
    InventoryReportComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-inventory-report',
            template: __webpack_require__(/*! ./inventory-report.component.html */ "./src/app/modules/warehouse/goods/inventory-report/inventory-report.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_inventory_report_service__WEBPACK_IMPORTED_MODULE_3__["InventoryReportService"]])
    ], InventoryReportComponent);
    return InventoryReportComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory-report/inventory-report.service.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory-report/inventory-report.service.ts ***!
  \**************************************************************************************/
/*! exports provided: InventoryReportService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryReportService", function() { return InventoryReportService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _base_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../base.service */ "./src/app/base.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");






var InventoryReportService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](InventoryReportService, _super);
    function InventoryReportService(spinnerService) {
        var _this = _super.call(this) || this;
        _this.spinnerService = spinnerService;
        _this.url = 'api/v1/warehouse/inventory-reports';
        _this.url = "" + _this.appConfig.API_GATEWAY_URL + _this.url;
        return _this;
    }
    InventoryReportService.prototype.resolve = function (route, state) {
        var queryParams = route.queryParams;
        // return this.search(queryParams.keyword, queryParams.warehouseId, queryParams.fromDate, queryParams.toDate, queryParams.page,
        //     queryParams.pageSize);
        return null;
    };
    InventoryReportService.prototype.search = function (keyword, warehouseId, fromDate, toDate, page, pageSize) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpParams"]()
                .set('keyword', keyword ? keyword.toString() : '')
                .set('warehouseId', warehouseId ? warehouseId.toString() : '')
                .set('fromDate', fromDate ? fromDate : '')
                .set('toDate', toDate ? toDate : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : '20')
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    // searchWarehouseCard(keyword: string, warehouseId: string, fromDate: string, toDate: string, page: number, pageSize?: number) {
    //     return this.http.get(`${this.url}/warehouse-cards`, {
    //         params: new HttpParams()
    //             .set('keyword', keyword ? keyword.toString() : '')
    //             .set('warehouseId', warehouseId ? warehouseId.toString() : '')
    //             .set('fromDate', fromDate ? fromDate : '')
    //             .set('toDate', toDate ? toDate : '')
    //             .set('page', page ? page.toString() : '1')
    //             .set('pageSize', pageSize ? pageSize.toString() : '20')
    //     });
    // }
    InventoryReportService.prototype.searchWarehouseCard = function (warehouseId, productId, fromDate, toDate, page, pageSize) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/warehouse-cards", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpParams"]()
                .set('warehouseId', warehouseId ? warehouseId : '')
                .set('productId', productId ? productId : '')
                .set('fromDate', fromDate ? fromDate : '')
                .set('toDate', toDate ? toDate : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : '100')
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            return result.data;
        }));
    };
    InventoryReportService.prototype.searchWarehouseCardDetail = function (warehouseId, productId, fromDate, toDate, page, pageSize) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/warehouse-card-details", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpParams"]()
                .set('warehouseId', warehouseId ? warehouseId : '')
                .set('productId', productId ? productId : '')
                .set('fromDate', fromDate ? fromDate : '')
                .set('toDate', toDate ? toDate : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : '100')
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            return result.data;
        }));
    };
    InventoryReportService.prototype.getProduct = function (warehouseId, productId, date) {
        return this.http.get(this.url + "/products/" + productId, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpParams"]()
                .set('warehouseId', warehouseId)
                .set('date', date)
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            return result.data;
        }));
    };
    InventoryReportService.prototype.getAllProductToTakeInventory = function (warehouseId, date) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/products", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpParams"]()
                .set('warehouseId', warehouseId)
                .set('date', date)
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            return result.items;
        }));
    };
    InventoryReportService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_2__["SpinnerService"]])
    ], InventoryReportService);
    return InventoryReportService;
}(_base_service__WEBPACK_IMPORTED_MODULE_3__["BaseService"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory-report/warehouse-card-detail/warehouse-card-detail.component.html":
/*!*********************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory-report/warehouse-card-detail/warehouse-card-detail.component.html ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <h4 class=\"title bold center uppercase\" i18n=\"@@warehouseCardDetail\">Chi tiết sổ kho</h4>\r\n        <div class=\"form-horizontal\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label ghmLabel=\"Tên kho\" i18n-ghmLabel=\"@@warehouseName\" class=\"col-sm-3\"></label>\r\n                        <div class=\"col-sm-9 bold\">\r\n                            {{ warehouseCard?.warehouseName }}\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label ghmLabel=\"Từ ngày\" i18n-ghmLabel=\"@@fromDate\" class=\"col-sm-3\"></label>\r\n                        <div class=\"col-sm-9 bold\">\r\n                            {{ fromDate | dateTimeFormat:'DD/MM/YYYY' }}\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label ghmLabel=\"Đến ngày\" i18n-ghmLabel=\"@@toDate\" class=\"col-sm-3\"></label>\r\n                        <div class=\"col-sm-9 bold\">\r\n                            {{ toDate | dateTimeFormat:'DD/MM/YYYY' }}\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label ghmLabel=\"Tên sản phẩm\" i18n-ghmLabel=\"@@productName\" class=\"col-sm-3\"></label>\r\n                        <div class=\"col-sm-9 bold\">\r\n                            {{ warehouseCard?.productName }}\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label ghmLabel=\"Đơn vị tính\" i18n-ghmLabel=\"@@unitName\" class=\"col-sm-3\"></label>\r\n                        <div class=\"col-sm-9 bold\">\r\n                            {{ warehouseCard?.unitName }}\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div><!-- END: product info -->\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-12\">\r\n                <div class=\"table-responsive\">\r\n                    <table class=\"table table-bordered table-striped table-hover\">\r\n                        <thead>\r\n                        <tr>\r\n                            <th class=\"center middle\" colspan=\"2\" i18n=\"@@receipt\">Chứng từ</th>\r\n                            <th class=\"center middle\" rowspan=\"2\" i18n=\"@@note\">Ghi chú</th>\r\n                            <th class=\"center middle w100\" rowspan=\"2\" i18n=\"@@lotId\">Số lô</th>\r\n                            <th class=\"center middle w100\" rowspan=\"2\" i18n=\"@@price\">Đơn giá</th>\r\n                            <th class=\"center middle\" colspan=\"2\" i18n=\"@@receiving\">Nhập</th>\r\n                            <th class=\"center middle\" colspan=\"2\" i18n=\"@@delivering\">Xuất</th>\r\n                            <th class=\"center middle\" colspan=\"2\" i18n=\"@@inventory\">Tồn</th>\r\n                        </tr>\r\n                        <tr>\r\n                            <th class=\"center middle w100\">Số</th>\r\n                            <th class=\"center middle w150\">Ngày</th>\r\n                            <th class=\"center middle w100\" i18n=\"@@quantity\">Số lượng</th>\r\n                            <th class=\"center middle w100\" i18n=\"@@amount\">Thành tiền</th>\r\n                            <th class=\"center middle w100\" i18n=\"@@quantity\">Số lượng</th>\r\n                            <th class=\"center middle w100\" i18n=\"@@amount\">Thành tiền</th>\r\n                            <th class=\"center middle w100\" i18n=\"@@quantity\">Số lượng</th>\r\n                            <th class=\"center middle w100\" i18n=\"@@amount\">Thành tiền</th>\r\n                        </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                        <tr>\r\n                            <td colspan=\"3\" class=\"text-right bold\" i18n=\"@@openingStockQuantity\">Tồn đầu kỳ</td>\r\n                            <td></td>\r\n                            <td></td>\r\n                            <td></td>\r\n                            <td></td>\r\n                            <td></td>\r\n                            <td></td>\r\n                            <td class=\"text-right bold\">{{ warehouseCard?.openingStockQuantity | ghmCurrency:2 }}</td>\r\n                            <td class=\"text-right bold\">{{ warehouseCard?.openingStockValue | ghmCurrency:2 }}</td>\r\n                        </tr>\r\n                        <tr *ngFor=\"let item of warehouseCard?.warehouseCardItems; let i = index\"\r\n                            class=\"cursor-pointer\"\r\n                            (click)=\"detail(item.id, item.isReceiving)\">\r\n                            <td>{{ item.receiptNo }}</td>\r\n                            <td>{{ item.invoiceDate | dateTimeFormat:'DD/MM/YYYY HH:mm:ss' }}</td>\r\n                            <td>{{ item.note }}</td>\r\n                            <td>{{ item.lotId }}</td>\r\n                            <td class=\"text-right\">{{ item.price | ghmCurrency:2 }}</td>\r\n                            <td class=\"text-right\" [class.bold]=\"item.isReceiving\">\r\n                                {{ (item.isReceiving ? item.quantity : '') | ghmCurrency:2 }}\r\n                            </td>\r\n                            <td class=\"text-right\" [class.bold]=\"item.isReceiving\">\r\n                                {{ (item.isReceiving ? item.value : '') | ghmCurrency:2 }}\r\n                            </td>\r\n                            <td class=\"text-right\" [class.bold]=\"!item.isReceiving\">\r\n                                {{ (!item.isReceiving ? item.quantity : '') | ghmCurrency:2 }}\r\n                            </td>\r\n                            <td class=\"text-right\" [class.bold]=\"!item.isReceiving\">\r\n                                {{ (!item.isReceiving ? item.value : '') | ghmCurrency:2 }}\r\n                            </td>\r\n                            <td class=\"text-right\"\r\n                                [class.color-red]=\"item.inventory < 0\">{{ item.inventory | ghmCurrency:2 }}\r\n                            </td>\r\n                            <td class=\"text-right\"\r\n                                [class.color-red]=\"item.inventoryValue < 0\">{{ item.inventoryValue | ghmCurrency:2 }}\r\n                            </td>\r\n                        </tr>\r\n                        </tbody>\r\n                    </table>\r\n                </div><!-- END: .table-responsive -->\r\n                <ghm-paging [totalRows]=\"totalRows\"\r\n                            [currentPage]=\"currentPage\"\r\n                            [pageShow]=\"6\"\r\n                            [isDisabled]=\"isSearching\"\r\n                            [pageSize]=\"pageSize\"\r\n                            (pageClick)=\"search($event)\"\r\n                ></ghm-paging>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div dynamic-component-host></div>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory-report/warehouse-card-detail/warehouse-card-detail.component.ts":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory-report/warehouse-card-detail/warehouse-card-detail.component.ts ***!
  \*******************************************************************************************************************/
/*! exports provided: WarehouseCardDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WarehouseCardDetailComponent", function() { return WarehouseCardDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _core_directives_dynamic_component_host_directive__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../core/directives/dynamic-component-host.directive */ "./src/app/core/directives/dynamic-component-host.directive.ts");
/* harmony import */ var _goods_receipt_note_goods_receipt_note_detail_goods_receipt_note_detail_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../goods-receipt-note/goods-receipt-note-detail/goods-receipt-note-detail.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-detail/goods-receipt-note-detail.component.ts");
/* harmony import */ var _inventory_report_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../inventory-report.service */ "./src/app/modules/warehouse/goods/inventory-report/inventory-report.service.ts");
/* harmony import */ var _goods_delivery_note_goods_delivery_note_detail_goods_delivery_note_detail_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../goods-delivery-note/goods-delivery-note-detail/goods-delivery-note-detail.component */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-detail/goods-delivery-note-detail.component.ts");








var WarehouseCardDetailComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](WarehouseCardDetailComponent, _super);
    function WarehouseCardDetailComponent(componentFactoryResolver, inventoryReportService) {
        var _this = _super.call(this) || this;
        _this.componentFactoryResolver = componentFactoryResolver;
        _this.inventoryReportService = inventoryReportService;
        return _this;
    }
    WarehouseCardDetailComponent.prototype.ngOnInit = function () {
    };
    WarehouseCardDetailComponent.prototype.show = function (id, warehouseId, fromDate, toDate) {
        this.productId = id;
        this.warehouseId = warehouseId;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.inventoryDetailModal.open();
        this.search(1);
    };
    WarehouseCardDetailComponent.prototype.search = function (currentPage) {
        var _this = this;
        if (currentPage === void 0) { currentPage = 1; }
        this.currentPage = currentPage;
        this.subscribers.getWarehousecard = this.inventoryReportService
            .searchWarehouseCardDetail(this.warehouseId, this.productId, this.fromDate, this.toDate, this.currentPage)
            .subscribe(function (warehouseCardDetailViewModel) {
            if (warehouseCardDetailViewModel) {
                _this.totalRows = warehouseCardDetailViewModel.totalItems;
                _this.warehouseCard = warehouseCardDetailViewModel;
                // let openingStockQuantity = this.warehouseCard.openingStockQuantity;
                // let openingStockValue = this.warehouseCard.openingStockQuantity;
                // if (warehouseCardDetailViewModel.warehouseCardItems) {
                //     warehouseCardDetailViewModel.warehouseCardItems.map((warehouseCardItem: WarehouseCardDetailItemViewModel) => {
                //         openingStockQuantity = warehouseCardItem.isReceiving
                //             ? openingStockQuantity + warehouseCardItem.quantity
                //             : openingStockQuantity - warehouseCardItem.quantity;
                //         openingStockValue = warehouseCardItem.isReceiving
                //             ? openingStockValue + warehouseCardItem.value
                //             : openingStockValue - warehouseCardItem.value;
                //         warehouseCardItem.inventory = openingStockQuantity;
                //         warehouseCardItem.inventoryValue = openingStockValue;
                //     });
                // }
            }
        });
    };
    // View goods receipt/delivery note.
    WarehouseCardDetailComponent.prototype.detail = function (id, isReceiving) {
        if (isReceiving) {
            this.loadDetailComponent(id);
        }
        else {
            this.loadGoodsDeliveryNoteDetailComponent(id);
        }
    };
    WarehouseCardDetailComponent.prototype.loadDetailComponent = function (id) {
        var componentFactory = this.componentFactoryResolver.resolveComponentFactory(_goods_receipt_note_goods_receipt_note_detail_goods_receipt_note_detail_component__WEBPACK_IMPORTED_MODULE_5__["GoodsReceiptNoteDetailComponent"]);
        var viewContainerRef = this.dynamicComponentHostDirective.viewContainerRef;
        viewContainerRef.clear();
        var componentRef = viewContainerRef.createComponent(componentFactory);
        componentRef.instance.getDetail(id);
    };
    WarehouseCardDetailComponent.prototype.loadGoodsDeliveryNoteDetailComponent = function (id) {
        var componentFactory = this.componentFactoryResolver.resolveComponentFactory(_goods_delivery_note_goods_delivery_note_detail_goods_delivery_note_detail_component__WEBPACK_IMPORTED_MODULE_7__["GoodsDeliveryNoteDetailComponent"]);
        var viewContainerRef = this.dynamicComponentHostDirective.viewContainerRef;
        viewContainerRef.clear();
        var componentRef = viewContainerRef.createComponent(componentFactory);
        componentRef.instance.getDetail(id);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('inventoryDetailModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_2__["NhModalComponent"])
    ], WarehouseCardDetailComponent.prototype, "inventoryDetailModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_core_directives_dynamic_component_host_directive__WEBPACK_IMPORTED_MODULE_4__["DynamicComponentHostDirective"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _core_directives_dynamic_component_host_directive__WEBPACK_IMPORTED_MODULE_4__["DynamicComponentHostDirective"])
    ], WarehouseCardDetailComponent.prototype, "dynamicComponentHostDirective", void 0);
    WarehouseCardDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-warehouse-card-detail',
            template: __webpack_require__(/*! ./warehouse-card-detail.component.html */ "./src/app/modules/warehouse/goods/inventory-report/warehouse-card-detail/warehouse-card-detail.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"],
            _inventory_report_service__WEBPACK_IMPORTED_MODULE_6__["InventoryReportService"]])
    ], WarehouseCardDetailComponent);
    return WarehouseCardDetailComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_3__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory-report/warehouse-card/warehouse-card.component.html":
/*!*******************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory-report/warehouse-card/warehouse-card.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <h4 class=\"title bold center uppercase\" i18n=\"@@warehouseCard\">Thẻ kho</h4>\r\n        <div class=\"form-horizontal\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label ghmLabel=\"Tên kho\" i18n-ghmLabel=\"@@warehouseName\" class=\"col-sm-3\"></label>\r\n                        <div class=\"col-sm-9 bold\">\r\n                            {{ warehouseCard?.warehouseName }}\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label ghmLabel=\"Từ ngày\" i18n-ghmLabel=\"@@fromDate\" class=\"col-sm-3\"></label>\r\n                        <div class=\"col-sm-9 bold\">\r\n                            {{ fromDate | dateTimeFormat:'DD/MM/YYYY HH:mm:ss' }}\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label ghmLabel=\"Đến ngày\" i18n-ghmLabel=\"@@toDate\" class=\"col-sm-3\"></label>\r\n                        <div class=\"col-sm-9 bold\">\r\n                            {{ toDate | dateTimeFormat:'DD/MM/YYYY HH:mm:ss' }}\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label ghmLabel=\"Tên sản phẩm\" i18n-ghmLabel=\"@@productName\" class=\"col-sm-3\"></label>\r\n                        <div class=\"col-sm-9 bold\">\r\n                            {{ warehouseCard?.productName }}\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <div class=\"form-group\">\r\n                        <label ghmLabel=\"Đơn vị tính\" i18n-ghmLabel=\"@@unitName\" class=\"col-sm-3\"></label>\r\n                        <div class=\"col-sm-9 bold\">\r\n                            {{ warehouseCard?.unitName }}\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div><!-- END: product info -->\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-12\">\r\n                <div class=\"table-responsive\">\r\n                    <table class=\"table table-bordered table-striped table-hover\">\r\n                        <thead>\r\n                        <tr>\r\n                            <th class=\"center middle\" rowspan=\"2\" i18n=\"@@no\">STT</th>\r\n                            <th class=\"center middle\" rowspan=\"2\" i18n=\"@@receiptDate\">Ngày chứng từ</th>\r\n                            <th class=\"center middle\" colspan=\"2\" i18n=\"@@receiptNo\">Số chứng từ</th>\r\n                            <th class=\"center middle\" rowspan=\"2\" i18n=\"@@note\">Ghi chú</th>\r\n                            <th class=\"center middle\" rowspan=\"2\" i18n=\"@@receivingDeliveringDate\">Ngày nhập/xuất</th>\r\n                            <th class=\"center\" colspan=\"3\" i18n=\"@@quantity\">Số lượng</th>\r\n                        </tr>\r\n                        <tr>\r\n                            <th class=\"center w100\" i18n=\"@@receiving\">Nhập</th>\r\n                            <th class=\"center w100\" i18n=\"@@delivering\">Xuất</th>\r\n                            <th class=\"center w100\" i18n=\"@@receiving\">Nhập</th>\r\n                            <th class=\"center w100\" i18n=\"@@delivering\">Xuất</th>\r\n                            <th class=\"center w100\" i18n=\"@@delivering\">Tồn</th>\r\n                        </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                        <tr>\r\n                            <td colspan=\"5\" class=\"text-right bold\" i18n=\"@@openingStockQuantity\">Tồn đầu kỳ</td>\r\n                            <td></td>\r\n                            <td></td>\r\n                            <td></td>\r\n                            <td class=\"text-right bold\">{{ warehouseCard?.openingStockQuantity | ghmCurrency:2 }}</td>\r\n                        </tr>\r\n                        <tr *ngFor=\"let item of warehouseCard?.warehouseCardItems; let i = index\"\r\n                            class=\"cursor-pointer\"\r\n                            (click)=\"detail(item.id, item.isReceiving)\">\r\n                            <td class=\"center\">{{ (currentPage - 1) * pageSize + i + 1 }}</td>\r\n                            <td>{{ item.invoiceDate | dateTimeFormat:'DD/MM/YYYY HH:mm:ss' }}</td>\r\n                            <td class=\"text-right\">{{ item.isReceiving ? item.receiptNo : '' }}</td>\r\n                            <td class=\"text-right\">{{ !item.isReceiving ? item.receiptNo : '' }}</td>\r\n                            <td>{{ item.note }}</td>\r\n                            <td>{{ item.date | dateTimeFormat:'DD/MM/YYYY HH:mm:ss' }}</td>\r\n                            <td class=\"text-right\">{{ item.isReceiving ? item.quantity : '' }}</td>\r\n                            <td class=\"text-right\">{{ !item.isReceiving ? item.quantity : '' }}</td>\r\n                            <td class=\"text-right\" [class.color-red]=\"item.inventory < 0\">\r\n                                {{ item.inventory }}\r\n                            </td>\r\n                        </tr>\r\n                        </tbody>\r\n                    </table>\r\n                </div><!-- END: .table-responsive -->\r\n                <ghm-paging [totalRows]=\"totalRows\"\r\n                            [currentPage]=\"currentPage\"\r\n                            [pageShow]=\"6\"\r\n                            [isDisabled]=\"isSearching\"\r\n                            [pageSize]=\"pageSize\"\r\n                            (pageClick)=\"search($event)\"\r\n                ></ghm-paging>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div dynamic-component-host></div>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory-report/warehouse-card/warehouse-card.component.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory-report/warehouse-card/warehouse-card.component.ts ***!
  \*****************************************************************************************************/
/*! exports provided: WarehouseCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WarehouseCardComponent", function() { return WarehouseCardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _core_directives_dynamic_component_host_directive__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../core/directives/dynamic-component-host.directive */ "./src/app/core/directives/dynamic-component-host.directive.ts");
/* harmony import */ var _goods_receipt_note_goods_receipt_note_detail_goods_receipt_note_detail_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../goods-receipt-note/goods-receipt-note-detail/goods-receipt-note-detail.component */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note-detail/goods-receipt-note-detail.component.ts");
/* harmony import */ var _inventory_report_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../inventory-report.service */ "./src/app/modules/warehouse/goods/inventory-report/inventory-report.service.ts");
/* harmony import */ var _goods_delivery_note_goods_delivery_note_detail_goods_delivery_note_detail_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../goods-delivery-note/goods-delivery-note-detail/goods-delivery-note-detail.component */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note-detail/goods-delivery-note-detail.component.ts");








var WarehouseCardComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](WarehouseCardComponent, _super);
    function WarehouseCardComponent(componentFactoryResolver, inventoryReportService) {
        var _this = _super.call(this) || this;
        _this.componentFactoryResolver = componentFactoryResolver;
        _this.inventoryReportService = inventoryReportService;
        return _this;
    }
    WarehouseCardComponent.prototype.ngOnInit = function () {
    };
    WarehouseCardComponent.prototype.show = function (id, warehouseId, fromDate, toDate) {
        this.productId = id;
        this.warehouseId = warehouseId;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.inventoryDetailModal.open();
        this.search(1);
    };
    WarehouseCardComponent.prototype.search = function (currentPage) {
        var _this = this;
        this.currentPage = currentPage;
        this.subscribers.getWarehousecard = this.inventoryReportService
            .searchWarehouseCard(this.warehouseId, this.productId, this.fromDate, this.toDate, this.currentPage)
            .subscribe(function (warehouseCardViewModel) {
            if (warehouseCardViewModel) {
                _this.totalRows = warehouseCardViewModel.totalItems;
                _this.warehouseCard = warehouseCardViewModel;
            }
        });
    };
    // View goods receipt/delivery note.
    WarehouseCardComponent.prototype.detail = function (id, isReceiving) {
        if (isReceiving) {
            this.loadDetailComponent(id);
        }
        else {
            this.loadGoodsDeliveryNoteDetailComponent(id);
        }
    };
    WarehouseCardComponent.prototype.loadDetailComponent = function (id) {
        var componentFactory = this.componentFactoryResolver.resolveComponentFactory(_goods_receipt_note_goods_receipt_note_detail_goods_receipt_note_detail_component__WEBPACK_IMPORTED_MODULE_5__["GoodsReceiptNoteDetailComponent"]);
        var viewContainerRef = this.dynamicComponentHostDirective.viewContainerRef;
        viewContainerRef.clear();
        var componentRef = viewContainerRef.createComponent(componentFactory);
        componentRef.instance.getDetail(id);
    };
    WarehouseCardComponent.prototype.loadGoodsDeliveryNoteDetailComponent = function (id) {
        var componentFactory = this.componentFactoryResolver.resolveComponentFactory(_goods_delivery_note_goods_delivery_note_detail_goods_delivery_note_detail_component__WEBPACK_IMPORTED_MODULE_7__["GoodsDeliveryNoteDetailComponent"]);
        var viewContainerRef = this.dynamicComponentHostDirective.viewContainerRef;
        viewContainerRef.clear();
        var componentRef = viewContainerRef.createComponent(componentFactory);
        componentRef.instance.getDetail(id);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('inventoryDetailModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_2__["NhModalComponent"])
    ], WarehouseCardComponent.prototype, "inventoryDetailModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_core_directives_dynamic_component_host_directive__WEBPACK_IMPORTED_MODULE_4__["DynamicComponentHostDirective"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _core_directives_dynamic_component_host_directive__WEBPACK_IMPORTED_MODULE_4__["DynamicComponentHostDirective"])
    ], WarehouseCardComponent.prototype, "dynamicComponentHostDirective", void 0);
    WarehouseCardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-warehouse-card',
            template: __webpack_require__(/*! ./warehouse-card.component.html */ "./src/app/modules/warehouse/goods/inventory-report/warehouse-card/warehouse-card.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"],
            _inventory_report_service__WEBPACK_IMPORTED_MODULE_6__["InventoryReportService"]])
    ], WarehouseCardComponent);
    return WarehouseCardComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_3__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory/inventory-detail/inventory-detail.component.html":
/*!****************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory/inventory-detail/inventory-detail.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form class=\"horizontal-form\" (ngSubmit)=\"save(false)\" [formGroup]=\"model\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-8\">\r\n            <app-inventory-product [inventoryId]=\"id\"\r\n                                   [warehouseId]=\"model.value.warehouseId\"\r\n                                   [totalRows]=\"inventoryDetail?.totalProduct\"\r\n                                   [status]=\"inventoryDetail?.status\"\r\n                                   [details]=\"listInventoryDetail\"\r\n                                   [date]=\"date\"\r\n                                   [isUpdate]=\"isUpdate\"\r\n                                   [readonly]=\"true\"\r\n                                   (onBalanced)=\"balanced()\"\r\n                                   (warehouseSelected)=\"onWarehouseSelected($event)\"\r\n            ></app-inventory-product>\r\n        </div>\r\n        <div class=\"col-sm-4\">\r\n            <div class=\"portlet light\">\r\n                <div class=\"portlet-title\">\r\n                    <div class=\"caption font-green-sharp\">\r\n                        <i class=\"icon-share font-green-sharp\"></i>\r\n                        <span class=\"caption-subject bold uppercase\"\r\n                              i18n=\"@@inventoryInfo\"> Thông tin phiếu kiểm kê</span>\r\n                        <!--<span class=\"caption-helper\">monthly stats...</span>-->\r\n                    </div>\r\n                </div>\r\n                <div class=\"portlet-body\">\r\n                    <div class=\"form-body\">\r\n                        <div class=\"row\">\r\n                            <div class=\"col-sm-12\">\r\n                                <div class=\"form-group\">\r\n                                    <label class=\"\" ghmLabel=\"Ngày kiểm kê\"\r\n                                           i18n-ghmLabel=\"@@inventoryDate\"></label>\r\n                                    <div class=\"form-control\">\r\n                                        {{ model.value.inventoryDate | dateTimeFormat:'DD/MM/YYYY HH:mm' }}\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"col-sm-12\">\r\n                                <div class=\"form-group\" [class.has-error]=\"formErrors?.status\">\r\n                                    <label class=\"\" ghmLabel=\"Trạng thái\"\r\n                                           i18n-ghmLabel=\"@@status\"></label>\r\n                                    <div>\r\n                                        <div class=\"form-control\" i18n=\"@@inventoryStatus\">\r\n                                            {model.value.status, select, 0 {Tạm thời} 1 {Đã cân bằng} other {}}\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"col-sm-12\">\r\n                                <div class=\"form-group\">\r\n                                    <label class=\"\" ghmLabel=\"Kiểm kê tại kho\"\r\n                                           i18n-ghmLabel=\"@@inventoryWarehouse\"></label>\r\n                                    <div class=\"form-control\">\r\n                                        {{ model.value.warehouseName }}\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"col-sm-12\">\r\n                                <div class=\"form-group\">\r\n                                    <label class=\"\" ghmLabel=\"Thành viên tham gia\"\r\n                                           i18n-ghmLabel=\"@@inventoryMember\"></label>\r\n                                    <ul class=\"media-list\">\r\n                                        <li class=\"media\" *ngFor=\"let user of selectedMembers\">\r\n                                            <div class=\"media-left\">\r\n                                                <a href=\"#\" class=\"w50\">\r\n                                                    <img class=\"media-object\" src=\"{{ user.avatar }}\"\r\n                                                         alt=\"{{ user.fullName }}\">\r\n                                                </a>\r\n                                            </div>\r\n                                            <div class=\"media-body\">\r\n                                                <h4 class=\"media-heading\">{{ user.fullName }}</h4>\r\n                                                {{ user.positionName }} - {{ user.officeName }}\r\n                                            </div>\r\n                                        </li>\r\n                                    </ul>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"col-sm-12\">\r\n                                <div class=\"form-group\" [class.has-error]=\"formErrors?.note\">\r\n                                    <label class=\"\" ghmLabel=\"Ghi chú\" i18n-ghmLabel=\"@@description\"></label>\r\n                                    <div class=\"form-control height-auto\">\r\n                                        {{ model.value.description }}\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"row cm-mgt-10\">\r\n                        <div class=\"col-sm-12\">\r\n                            <div class=\"form-group\">\r\n                                <a class=\"btn default\"\r\n                                   [routerLink]=\"['/goods/inventories']\"\r\n                                   [queryParams]=\"{warehouseId: model.value.warehouseId, warehouseName: model.value.warehouseName}\">\r\n                                    <span i18n=\"@@back\">Trở về</span>\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory/inventory-detail/inventory-detail.component.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory/inventory-detail/inventory-detail.component.ts ***!
  \**************************************************************************************************/
/*! exports provided: InventoryDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryDetailComponent", function() { return InventoryDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _service_inventory_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/inventory.service */ "./src/app/modules/warehouse/goods/inventory/service/inventory.service.ts");
/* harmony import */ var _model_inventory_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../model/inventory.model */ "./src/app/modules/warehouse/goods/inventory/model/inventory.model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _warehouse_warehouse_suggestion_warehouse_suggestion_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../warehouse/warehouse-suggestion/warehouse-suggestion.component */ "./src/app/modules/warehouse/warehouse/warehouse-suggestion/warehouse-suggestion.component.ts");
/* harmony import */ var _validators_datetime_validator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../../validators/datetime.validator */ "./src/app/validators/datetime.validator.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _inventory_form_inventory_product_inventory_product_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../inventory-form/inventory-product/inventory-product.component */ "./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-product/inventory-product.component.ts");














var InventoryDetailComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](InventoryDetailComponent, _super);
    function InventoryDetailComponent(inventoryService, fb, toastr, route, router, datetimeValidator, utilService) {
        var _this = _super.call(this) || this;
        _this.inventoryService = inventoryService;
        _this.fb = fb;
        _this.toastr = toastr;
        _this.route = route;
        _this.router = router;
        _this.datetimeValidator = datetimeValidator;
        _this.utilService = utilService;
        _this.inventory = new _model_inventory_model__WEBPACK_IMPORTED_MODULE_4__["Inventory"]();
        _this.selectedMembers = [];
        _this.listInventoryDetail = [];
        _this.inventoryStatus = _model_inventory_model__WEBPACK_IMPORTED_MODULE_4__["InventoryStatus"];
        _this.currentPage = 1;
        _this.pageSize = 20;
        _this.date = _this.inventory.inventoryDate;
        _this.subscribers.queryParams = _this.route.params.subscribe(function (params) {
            if (params.id) {
                _this.isUpdate = true;
                _this.id = params.id;
                _this.getDetail(params.id);
            }
        });
        return _this;
    }
    InventoryDetailComponent.prototype.ngOnInit = function () {
        this.appService.setupPage(this.pageId.WAREHOUSE, this.pageId.INVENTORY, 'Thêm mới', 'Quản lý kho');
        this.renderForm();
    };
    InventoryDetailComponent.prototype.onModalShow = function () {
    };
    InventoryDetailComponent.prototype.onModalHidden = function () {
    };
    InventoryDetailComponent.prototype.onUserSelected = function (users) {
        this.selectedMembers = users;
    };
    InventoryDetailComponent.prototype.onDateSelected = function (date) {
        this.date = date;
    };
    InventoryDetailComponent.prototype.add = function () {
        this.resetForm();
        if (this.warehouseSuggestionComponent) {
            this.warehouseSuggestionComponent.clear();
        }
        this.isUpdate = false;
        // this.formInventory.open();
    };
    InventoryDetailComponent.prototype.edit = function (id) {
        this.isUpdate = true;
        this.id = id;
        this.getDetail(id);
        // this.formInventory.open();
    };
    InventoryDetailComponent.prototype.save = function (isBalance) {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (!this.inventoryProductComponent.details || this.inventoryProductComponent.details.length === 0) {
            this.toastr.error('Vui lòng chọn ít nhất một sản phẩm.');
            return;
        }
        var inValidDetail = lodash__WEBPACK_IMPORTED_MODULE_11__["countBy"](this.inventoryProductComponent.details, function (inventoryDetail) {
            return !inventoryDetail.realQuantity && inventoryDetail.realQuantity !== 0;
        }).true;
        if (inValidDetail && inValidDetail > 0) {
            this.toastr.error('Vui lòng nhập số lượng thực tế.');
            return;
        }
        if (isValid) {
            this.inventory = this.model.value;
            this.inventory.members = this.selectedMembers.map(function (user) {
                return {
                    userId: user.id
                };
            });
            this.inventory.details = this.inventoryProductComponent.details;
            if (this.isUpdate) {
                this.inventoryService.update(this.id, this.inventory, isBalance)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return (_this.isSaving = false); }))
                    .subscribe(function () {
                    _this.router.navigateByUrl("/goods/inventories?warehouseId=" + _this.inventory.warehouseId.trim() + "&warehouseName=" + _this.inventory.warehouseName.trim());
                });
            }
            else {
                this.inventoryService.insert(this.inventory, isBalance)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return (_this.isSaving = false); }))
                    .subscribe(function () {
                    // this.isModified = true;
                    // if (this.isCreateAnother) {
                    //     this.resetForm();
                    // } else {
                    //     // this.saveSuccessful.emit();
                    //     // this.formInventory.dismiss();
                    // }
                    _this.router.navigateByUrl("/goods/inventories?warehouseId=" + _this.inventory.warehouseId + "&warehouseName=" + _this.inventory.warehouseName.trim());
                });
            }
        }
    };
    InventoryDetailComponent.prototype.onWarehouseSelected = function (warehouse) {
        this.model.patchValue({ warehouseId: warehouse.id, warehouseName: warehouse.name });
        // if (warehouseId) {
        // this.inventoryService.getProductByWarehouseId('', warehouseId, this.model.value.inventoryDate, this.currentPage,
        // this.pageSize)
        //     .subscribe((result: SearchResultViewModel<InventoryDetail>) => {
        //         console.log(result.items);
        //         this.listInventoryDetail = result.items;
        //     });
        // }
    };
    InventoryDetailComponent.prototype.balanced = function () {
        this.inventoryDetail.status = _model_inventory_model__WEBPACK_IMPORTED_MODULE_4__["InventoryStatus"].balanced;
    };
    InventoryDetailComponent.prototype.getDetail = function (id) {
        var _this = this;
        this.inventoryService.getDetail(id).subscribe(function (result) {
            var data = result.data;
            console.log(data);
            _this.inventoryDetail = data;
            if (data) {
                _this.model.patchValue({
                    id: data.id,
                    warehouseId: data.warehouseId,
                    warehouseName: data.warehouseName,
                    note: data.note,
                    inventoryDate: data.inventoryDate,
                    concurrencyStamp: data.concurrencyStamp,
                    status: data.status
                });
                _this.inventoryProductComponent.warehouseId = data.warehouseId;
                // this.inventoryProductComponent.warehouseName = data.warehouseName;
                // this.listInventoryDetail = this.inventoryDetail.inventoryDetails;
                _this.inventoryProductComponent.details = data.details
                    .map(function (inventoryDetail) {
                    inventoryDetail.difference = inventoryDetail.realQuantity - inventoryDetail.inventoryQuantity;
                    inventoryDetail.amount = inventoryDetail.realQuantity * inventoryDetail.price;
                    inventoryDetail.accountingAmount = inventoryDetail.inventoryQuantity * inventoryDetail.price;
                    inventoryDetail.differenceAmount = inventoryDetail.amount - inventoryDetail.accountingAmount;
                    return inventoryDetail;
                });
                _this.selectedMembers = _this.inventoryDetail.members
                    .map(function (member) {
                    return {
                        id: member.userId,
                        fullName: member.fullName,
                        avatar: member.avatar,
                        positionName: member.positionName,
                        officeName: member.officeName
                    };
                });
                // if (this.warehouseSuggestionComponent) {
                //     this.warehouseSuggestionComponent.selectedItem = new NhSuggestion(this.inventoryDetail.warehouseId,
                //         this.inventoryDetail.warehouseName);
                // }
            }
        });
    };
    InventoryDetailComponent.prototype.selectAmount = function (value) {
        if (value) {
            this.model.patchValue({ totalAmounts: value.totalAmounts, totalQuantity: value.totalQuantity });
        }
        else {
            this.model.patchValue({ totalAmounts: 0, totalQuantity: 0 });
        }
    };
    InventoryDetailComponent.prototype.renderForm = function () {
        this.buildForm();
    };
    InventoryDetailComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.utilService.renderFormError(['warehouseId', 'note', 'inventoryDate']);
        this.validationMessages = this.utilService.renderFormErrorMessage([
            { 'warehouseId': ['required', 'maxLength'] },
            { 'note': ['maxLength'] },
            { 'inventoryDate': ['required', 'isValid'] },
        ]);
        this.model = this.fb.group({
            id: [this.inventory.id],
            warehouseId: [this.inventory.warehouseId, [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].maxLength(50)]],
            warehouseName: [this.inventory.warehouseName],
            note: [this.inventory.note, [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].maxLength(500)]],
            inventoryDate: [this.inventory.inventoryDate, [this.datetimeValidator.isValid]],
            concurrencyStamp: [this.inventory.concurrencyStamp],
            status: [this.inventory.status],
        });
        this.model.valueChanges.subscribe(function (data) { return _this.validateModel(false); });
    };
    InventoryDetailComponent.prototype.resetForm = function () {
        this.id = null;
        this.model.patchValue(new _model_inventory_model__WEBPACK_IMPORTED_MODULE_4__["Inventory"]());
        if (this.warehouseSuggestionComponent) {
            this.warehouseSuggestionComponent.clear();
        }
        this.listInventoryDetail = [];
        this.selectedMembers = [];
        this.clearFormError(this.formErrors);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_warehouse_warehouse_suggestion_warehouse_suggestion_component__WEBPACK_IMPORTED_MODULE_9__["WarehouseSuggestionComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _warehouse_warehouse_suggestion_warehouse_suggestion_component__WEBPACK_IMPORTED_MODULE_9__["WarehouseSuggestionComponent"])
    ], InventoryDetailComponent.prototype, "warehouseSuggestionComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_inventory_form_inventory_product_inventory_product_component__WEBPACK_IMPORTED_MODULE_13__["InventoryProductComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _inventory_form_inventory_product_inventory_product_component__WEBPACK_IMPORTED_MODULE_13__["InventoryProductComponent"])
    ], InventoryDetailComponent.prototype, "inventoryProductComponent", void 0);
    InventoryDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-inventory-detail',
            template: __webpack_require__(/*! ./inventory-detail.component.html */ "./src/app/modules/warehouse/goods/inventory/inventory-detail/inventory-detail.component.html"),
            providers: [_validators_datetime_validator__WEBPACK_IMPORTED_MODULE_10__["DateTimeValidator"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_inventory_service__WEBPACK_IMPORTED_MODULE_3__["InventoryService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_8__["ToastrService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_12__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_12__["Router"],
            _validators_datetime_validator__WEBPACK_IMPORTED_MODULE_10__["DateTimeValidator"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_7__["UtilService"]])
    ], InventoryDetailComponent);
    return InventoryDetailComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-form.component.html":
/*!************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-form.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form class=\"horizontal-form\" (ngSubmit)=\"save(false)\" [formGroup]=\"model\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-8\">\r\n            <app-inventory-product [inventoryId]=\"id\"\r\n                                   [warehouseId]=\"model.value.warehouseId\"\r\n                                   [totalRows]=\"inventoryDetail?.totalProduct\"\r\n                                   [status]=\"inventoryDetail?.status\"\r\n                                   [details]=\"listInventoryDetail\"\r\n                                   [date]=\"date\"\r\n                                   [readonly]=\"model.value.status === inventoryStatus.balanced\"\r\n                                   [isUpdate]=\"isUpdate\"\r\n                                   (onBalanced)=\"balanced()\"\r\n                                   (warehouseSelected)=\"onWarehouseSelected($event)\"\r\n            ></app-inventory-product>\r\n        </div>\r\n        <div class=\"col-sm-4\">\r\n            <div class=\"portlet light\">\r\n                <div class=\"portlet-title\">\r\n                    <div class=\"caption font-green-sharp\">\r\n                        <i class=\"icon-share font-green-sharp\"></i>\r\n                        <span class=\"caption-subject bold uppercase\"\r\n                              i18n=\"@@inventoryInfo\"> Thông tin phiếu kiểm kê</span>\r\n                        <!--<span class=\"caption-helper\">monthly stats...</span>-->\r\n                    </div>\r\n                </div>\r\n                <div class=\"portlet-body\">\r\n\r\n                    <div class=\"form-body\">\r\n                        <div class=\"row\">\r\n                            <div class=\"col-sm-12\">\r\n                                <div class=\"form-group\" [class.has-error]=\"formErrors?.inventoryDate\">\r\n                                    <label class=\"\" ghmLabel=\"Ngày kiểm kê\"\r\n                                           i18n-ghmLabel=\"@@inventoryDate\"\r\n                                           [required]=\"true\"></label>\r\n                                    <nh-date\r\n                                        [mask]=\"true\"\r\n                                        [showTime]=\"true\"\r\n                                        [format]=\"'DD/MM/YYYY HH:mm:ss'\"\r\n                                        [outputFormat]=\"'YYYY/MM/DD HH:mm:ss'\"\r\n                                        (selectedDateEvent)=\"onDateSelected($event)\"\r\n                                        formControlName=\"inventoryDate\"></nh-date>\r\n                                    <span class=\"help-block\">\r\n                                       {\r\n                                          formErrors?.inventoryDate,\r\n                                           select, required {Please select Inventory Date} isValid {Inventory Date is invalid}\r\n                                       }\r\n                                    </span>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"col-sm-12\">\r\n                                <div class=\"form-group\" [class.has-error]=\"formErrors?.status\">\r\n                                    <label class=\"\" ghmLabel=\"Trạng thái\"\r\n                                           i18n-ghmLabel=\"@@status\"></label>\r\n                                    <div>\r\n                                        <div class=\"form-control\" i18n=\"@@inventoryStatus\">\r\n                                            {model.value.status, select, 0 {Tạm thời} 1 {Đã cân bằng} other {}}\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"col-sm-12\">\r\n                                <div class=\"form-group\">\r\n                                    <label class=\"\" ghmLabel=\"Kiểm kê tại kho\"\r\n                                           i18n-ghmLabel=\"@@inventoryWarehouse\"></label>\r\n                                    <app-warehouse-suggestion\r\n                                        *ngIf=\"!isUpdate; else warehouseReadOnlyTemplate\"\r\n                                        [selectedItem]=\"model.value.warehouseId && model.value.warehouseName\r\n                                        ? {id: model.value.warehouseId, name: model.value.warehouseName} : null\"\r\n                                        (itemSelected)=\"onWarehouseSelected($event)\"></app-warehouse-suggestion>\r\n                                    <ng-template #warehouseReadOnlyTemplate>\r\n                                        <div class=\"form-control\">\r\n                                            {{ model.value.warehouseName }}\r\n                                        </div>\r\n                                    </ng-template>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"col-sm-12\">\r\n                                <div class=\"form-group\">\r\n                                    <label class=\"\" ghmLabel=\"Thành viên tham gia\"\r\n                                           i18n-ghmLabel=\"@@inventoryMember\"></label>\r\n                                    <ghm-user-suggestion\r\n                                        [multiple]=\"true\"\r\n                                        [selectedUser]=\"selectedMembers\"\r\n                                        (userSelected)=\"onUserSelected($event)\"></ghm-user-suggestion>\r\n                                    <!--<app-user-suggestion-->\r\n                                    <!--[selectedItem]=\"selectedMembers\"-->\r\n                                    <!--(itemSelected)=\"onUserSelected($event)\"></app-user-suggestion>-->\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"col-sm-12\">\r\n                                <div class=\"form-group\" [class.has-error]=\"formErrors?.note\">\r\n                                    <label class=\"\" ghmLabel=\"Ghi chú\" i18n-ghmLabel=\"@@description\"></label>\r\n                                    <textarea name=\"\" class=\"form-control\" rows=\"2\"\r\n                                              formControlName=\"note\"></textarea>\r\n                                    <span class=\"help-block\">\r\n                                       { formErrors?.note,\r\n                                         select, maxlength {Description can not exceed 500 characters}\r\n                                       }\r\n                                </span>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"row cm-mgt-10 cm-mgb-5\">\r\n                        <div class=\"col-sm-12\">\r\n                            <div class=\"form-group\">\r\n                                <button class=\"btn blue cm-mgr-5\"\r\n                                        *ngIf=\"!isUpdate || inventoryDetail?.status === inventoryStatus.temporary\"\r\n                                        [disabled]=\"isSaving\">\r\n                                    <span i18n=\"@@save\">Lưu tạm</span>\r\n                                </button>\r\n                                <button type=\"button\" class=\"btn btn-success cm-mgr-5\"\r\n                                        *ngIf=\"!isUpdate || inventoryDetail?.status === inventoryStatus.temporary\"\r\n                                        [disabled]=\"isSaving\"\r\n                                        (click)=\"save(true)\">\r\n                                    <span i18n=\"@@save\">Hoàn thành</span>\r\n                                </button>\r\n                                <!--<button type=\"button\" class=\"btn blue cm-mgr-5\"-->\r\n                                        <!--*ngIf=\"permission.print\"-->\r\n                                        <!--[disabled]=\"isSaving\"-->\r\n                                        <!--(click)=\"print()\">-->\r\n                                    <!--<span i18n=\"@@print\">In</span>-->\r\n                                <!--</button>-->\r\n                                <button type=\"button\" class=\"btn blue cm-mgr-5\"\r\n                                        *ngIf=\"permission.export\"\r\n                                        [disabled]=\"isSaving\"\r\n                                        (click)=\"export()\">\r\n                                    <span i18n=\"@@export\">Xuất file</span>\r\n                                </button>\r\n                                <a class=\"btn default\"\r\n                                   [routerLink]=\"['/goods/inventories']\"\r\n                                   [queryParams]=\"{warehouseId: model.value.warehouseId, warehouseName: model.value.warehouseName}\"\r\n                                >\r\n                                    <span i18n=\"@@back\">Trở về</span>\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-form.component.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-form.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: InventoryFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryFormComponent", function() { return InventoryFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _service_inventory_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/inventory.service */ "./src/app/modules/warehouse/goods/inventory/service/inventory.service.ts");
/* harmony import */ var _model_inventory_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../model/inventory.model */ "./src/app/modules/warehouse/goods/inventory/model/inventory.model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _warehouse_warehouse_suggestion_warehouse_suggestion_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../warehouse/warehouse-suggestion/warehouse-suggestion.component */ "./src/app/modules/warehouse/warehouse/warehouse-suggestion/warehouse-suggestion.component.ts");
/* harmony import */ var _validators_datetime_validator__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../../validators/datetime.validator */ "./src/app/validators/datetime.validator.ts");
/* harmony import */ var _inventory_product_inventory_product_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./inventory-product/inventory-product.component */ "./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-product/inventory-product.component.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");















var InventoryFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](InventoryFormComponent, _super);
    function InventoryFormComponent(inventoryService, fb, toastr, route, router, datetimeValidator, utilService) {
        var _this = _super.call(this) || this;
        _this.inventoryService = inventoryService;
        _this.fb = fb;
        _this.toastr = toastr;
        _this.route = route;
        _this.router = router;
        _this.datetimeValidator = datetimeValidator;
        _this.utilService = utilService;
        _this.inventory = new _model_inventory_model__WEBPACK_IMPORTED_MODULE_4__["Inventory"]();
        _this.selectedMembers = [];
        _this.listInventoryDetail = [];
        _this.inventoryStatus = _model_inventory_model__WEBPACK_IMPORTED_MODULE_4__["InventoryStatus"];
        _this.currentPage = 1;
        _this.pageSize = 20;
        _this.date = _this.inventory.inventoryDate;
        _this.subscribers.queryParams = _this.route.params.subscribe(function (params) {
            if (params.id) {
                _this.isUpdate = true;
                _this.id = params.id;
                _this.getDetail(params.id);
            }
        });
        return _this;
    }
    InventoryFormComponent.prototype.ngOnInit = function () {
        this.appService.setupPage(this.pageId.WAREHOUSE, this.pageId.INVENTORY, 'Thêm mới', 'Quản lý kho');
        this.renderForm();
    };
    InventoryFormComponent.prototype.onModalShow = function () {
    };
    InventoryFormComponent.prototype.onModalHidden = function () {
    };
    InventoryFormComponent.prototype.onUserSelected = function (users) {
        this.selectedMembers = users;
    };
    InventoryFormComponent.prototype.onDateSelected = function (date) {
        this.date = date;
    };
    InventoryFormComponent.prototype.add = function () {
        this.resetForm();
        if (this.warehouseSuggestionComponent) {
            this.warehouseSuggestionComponent.clear();
        }
        this.isUpdate = false;
        // this.formInventory.open();
    };
    InventoryFormComponent.prototype.edit = function (id) {
        this.isUpdate = true;
        this.id = id;
        this.getDetail(id);
        // this.formInventory.open();
    };
    InventoryFormComponent.prototype.save = function (isBalance) {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (!this.inventoryProductComponent.details || this.inventoryProductComponent.details.length === 0) {
            this.toastr.error('Vui lòng chọn ít nhất một sản phẩm.');
            return;
        }
        if (isBalance) {
            var inValidDetail = lodash__WEBPACK_IMPORTED_MODULE_13__["countBy"](this.inventoryProductComponent.details, function (inventoryDetail) {
                return !inventoryDetail.realQuantity && inventoryDetail.realQuantity !== 0;
            }).true;
            if (inValidDetail && inValidDetail > 0) {
                this.toastr.error('Vui lòng nhập số lượng thực tế.');
                return;
            }
        }
        if (isValid) {
            this.inventory = this.model.value;
            this.inventory.members = this.selectedMembers.map(function (user) {
                return {
                    userId: user.id
                };
            });
            this.inventory.details = this.inventoryProductComponent.details;
            if (this.isUpdate) {
                this.inventoryService.update(this.id, this.inventory, isBalance)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return (_this.isSaving = false); }))
                    .subscribe(function () {
                    _this.router.navigateByUrl("/goods/inventories?warehouseId=" + _this.inventory.warehouseId.trim() + "&warehouseName=" + _this.inventory.warehouseName.trim());
                });
            }
            else {
                this.inventoryService.insert(this.inventory, isBalance)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return (_this.isSaving = false); }))
                    .subscribe(function () {
                    // this.isModified = true;
                    // if (this.isCreateAnother) {
                    //     this.resetForm();
                    // } else {
                    //     // this.saveSuccessful.emit();
                    //     // this.formInventory.dismiss();
                    // }
                    _this.router.navigateByUrl("/goods/inventories?warehouseId=" + _this.inventory.warehouseId + "&warehouseName=" + _this.inventory.warehouseName.trim());
                });
            }
        }
    };
    InventoryFormComponent.prototype.onWarehouseSelected = function (warehouse) {
        this.model.patchValue({ warehouseId: warehouse.id, warehouseName: warehouse.name });
        // if (warehouseId) {
        // this.inventoryService.getProductByWarehouseId('', warehouseId, this.model.value.inventoryDate, this.currentPage,
        // this.pageSize)
        //     .subscribe((result: SearchResultViewModel<InventoryDetail>) => {
        //         console.log(result.items);
        //         this.listInventoryDetail = result.items;
        //     });
        // }
    };
    InventoryFormComponent.prototype.balanced = function () {
        this.inventoryDetail.status = _model_inventory_model__WEBPACK_IMPORTED_MODULE_4__["InventoryStatus"].balanced;
    };
    InventoryFormComponent.prototype.print = function () {
    };
    InventoryFormComponent.prototype.export = function () {
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (!this.inventoryProductComponent.details || this.inventoryProductComponent.details.length === 0) {
            this.toastr.error('Vui lòng chọn ít nhất một sản phẩm.');
            return;
        }
        this.inventory = this.model.value;
        var isBalance = this.inventory.status === this.inventoryStatus.balanced;
        if (isBalance) {
            var inValidDetail = lodash__WEBPACK_IMPORTED_MODULE_13__["countBy"](this.inventoryProductComponent.details, function (inventoryDetail) {
                return !inventoryDetail.realQuantity && inventoryDetail.realQuantity !== 0;
            }).true;
            if (inValidDetail && inValidDetail > 0) {
                this.toastr.error('Vui lòng nhập số lượng thực tế.');
                return;
            }
        }
        if (isValid) {
            this.inventory.members = this.selectedMembers.map(function (user) {
                return {
                    userId: user.id
                };
            });
            this.inventory.details = this.inventoryProductComponent.details;
            this.subscribers.export = this.inventoryService.export(this.id, this.inventory, isBalance)
                .subscribe(function (result) {
                file_saver__WEBPACK_IMPORTED_MODULE_6__["saveAs"](result, 'DANH_SACH_KIEM_KE.xlsx');
                var fileURL = URL.createObjectURL(result);
                window.open(fileURL);
            });
        }
    };
    InventoryFormComponent.prototype.getDetail = function (id) {
        var _this = this;
        this.inventoryService.getDetailForEdit(id).subscribe(function (result) {
            var data = result.data;
            _this.inventoryDetail = data;
            if (data) {
                _this.model.patchValue({
                    id: data.id,
                    warehouseId: data.warehouseId,
                    warehouseName: data.warehouseName,
                    note: data.note,
                    inventoryDate: data.inventoryDate,
                    concurrencyStamp: data.concurrencyStamp,
                    status: data.status
                });
                _this.inventoryProductComponent.warehouseId = data.warehouseId;
                // this.inventoryProductComponent.warehouseName = data.warehouseName;
                // this.listInventoryDetail = this.inventoryDetail.inventoryDetails;
                _this.inventoryProductComponent.details = data.details
                    .map(function (inventoryDetail) {
                    inventoryDetail.difference = inventoryDetail.realQuantity - inventoryDetail.inventoryQuantity;
                    return inventoryDetail;
                });
                _this.selectedMembers = _this.inventoryDetail.members
                    .map(function (member) {
                    return {
                        id: member.userId,
                        fullName: member.fullName,
                        avatar: member.avatar,
                        positionName: member.positionName,
                        officeName: member.officeName
                    };
                });
                // if (this.warehouseSuggestionComponent) {
                //     this.warehouseSuggestionComponent.selectedItem = new NhSuggestion(this.inventoryDetail.warehouseId,
                //         this.inventoryDetail.warehouseName);
                // }
            }
        });
    };
    InventoryFormComponent.prototype.selectAmount = function (value) {
        if (value) {
            this.model.patchValue({ totalAmounts: value.totalAmounts, totalQuantity: value.totalQuantity });
        }
        else {
            this.model.patchValue({ totalAmounts: 0, totalQuantity: 0 });
        }
    };
    InventoryFormComponent.prototype.renderForm = function () {
        this.buildForm();
    };
    InventoryFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.utilService.renderFormError(['warehouseId', 'note', 'inventoryDate']);
        this.validationMessages = this.utilService.renderFormErrorMessage([
            { 'warehouseId': ['required', 'maxLength'] },
            { 'note': ['maxLength'] },
            { 'inventoryDate': ['required', 'isValid'] },
        ]);
        this.model = this.fb.group({
            id: [this.inventory.id],
            warehouseId: [this.inventory.warehouseId, [_angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(50)]],
            warehouseName: [this.inventory.warehouseName],
            note: [this.inventory.note, [_angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(500)]],
            inventoryDate: [this.inventory.inventoryDate, [this.datetimeValidator.isValid]],
            concurrencyStamp: [this.inventory.concurrencyStamp],
            status: [this.inventory.status],
        });
        this.model.valueChanges.subscribe(function (data) { return _this.validateModel(false); });
    };
    InventoryFormComponent.prototype.resetForm = function () {
        this.id = null;
        this.model.patchValue(new _model_inventory_model__WEBPACK_IMPORTED_MODULE_4__["Inventory"]());
        if (this.warehouseSuggestionComponent) {
            this.warehouseSuggestionComponent.clear();
        }
        this.listInventoryDetail = [];
        this.selectedMembers = [];
        this.clearFormError(this.formErrors);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_warehouse_warehouse_suggestion_warehouse_suggestion_component__WEBPACK_IMPORTED_MODULE_10__["WarehouseSuggestionComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _warehouse_warehouse_suggestion_warehouse_suggestion_component__WEBPACK_IMPORTED_MODULE_10__["WarehouseSuggestionComponent"])
    ], InventoryFormComponent.prototype, "warehouseSuggestionComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_inventory_product_inventory_product_component__WEBPACK_IMPORTED_MODULE_12__["InventoryProductComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _inventory_product_inventory_product_component__WEBPACK_IMPORTED_MODULE_12__["InventoryProductComponent"])
    ], InventoryFormComponent.prototype, "inventoryProductComponent", void 0);
    InventoryFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-inventory-form',
            template: __webpack_require__(/*! ./inventory-form.component.html */ "./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-form.component.html"),
            providers: [_validators_datetime_validator__WEBPACK_IMPORTED_MODULE_11__["DateTimeValidator"]],
            styles: [__webpack_require__(/*! ../inventory.scss */ "./src/app/modules/warehouse/goods/inventory/inventory.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_inventory_service__WEBPACK_IMPORTED_MODULE_3__["InventoryService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_9__["ToastrService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_14__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_14__["Router"],
            _validators_datetime_validator__WEBPACK_IMPORTED_MODULE_11__["DateTimeValidator"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_8__["UtilService"]])
    ], InventoryFormComponent);
    return InventoryFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-member/inventory-member.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-member/inventory-member.component.html ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table class=\"table table-striped table-hover\">\r\n    <thead>\r\n    <tr>\r\n        <th class=\"middle center w50\" i18n=\"@@no\">STT</th>\r\n        <th class=\"middle\" i18n=\"@@fullName\">Họ Tên</th>\r\n        <th class=\"middle\" i18n=\"@@address\">Vị trí</th>\r\n        <th class=\"middle\" i18n=\"@@description\">Ghi chú</th>\r\n        <th class=\"middle text-right w50\" i18n=\"@@action\" *ngIf=\"(permission.delete)\">\r\n        </th>\r\n    </tr>\r\n    </thead>\r\n    <tbody>\r\n    <tr *ngFor=\"let item of listInventoryMember; let i = index\"\r\n        nhContextMenuTrigger\r\n        [nhContextMenuTriggerFor]=\"nhMenu\"\r\n        [nhContextMenuData]=\"item\">\r\n        <td class=\"center middle\">{{i + 1}}</td>\r\n        <td class=\"middle\">{{item.fullName}}</td>\r\n        <td class=\"middle\">{{item.position}}</td>\r\n        <td class=\"middle\">\r\n            <input class=\"form-control\"\r\n                      [(ngModel)]=\"item.description\"\r\n                      (blur)=\"updateDescription(item)\"\r\n                       >\r\n        </td>\r\n        <td class=\"text-right middle\" *ngIf=\"(permission.delete) \">\r\n            <button class=\"btn red\" (click)=\"confirm(item)\" type=\"button\">\r\n                <i class=\"fa fa-times\"></i>\r\n            </button>\r\n        </td>\r\n    </tr>\r\n    </tbody>\r\n    <tfoot>\r\n    <tr>\r\n        <ng-container [formGroup]=\"model\">\r\n            <td></td>\r\n            <td class=\"middle\" [formGroup]=\"model\"\r\n                [class.has-error]=\"formErrors?.fullName\">\r\n                <ghm-user-suggestion (userSelected)=\"selectUser($event)\"></ghm-user-suggestion>\r\n                <span class=\"help-block\">\r\n                         {\r\n                                formErrors?.fullName,\r\n                                select, required {Please enter fullName}\r\n                                maxlength {FullName can not exceed 50 characters}\r\n                         }\r\n                </span>\r\n            </td>\r\n            <td class=\"middle\">\r\n                <span [formGroup]=\"model\">{{model.value.position}}</span>\r\n            </td>\r\n            <td class=\"middle\" [formGroup]=\"model\"\r\n                [class.has-error]=\"formErrors?.description\">\r\n                <input *ngIf=\"model.value.userId\"\r\n                          id=\"description\"\r\n                          rows=\"1\"\r\n                          class=\"form-control\" formControlName=\"description\">\r\n                <span class=\"help-block\">\r\n                 {\r\n                                formErrors?.description,\r\n                                select,\r\n                                maxlength {Description can not exceed 500 characters}\r\n                 }\r\n                </span>\r\n            </td>\r\n            <td class=\"middle center\">\r\n                <button class=\"btn blue\" (click)=\"save()\" type=\"button\">\r\n                    <i class=\"fa fa-save\"></i>\r\n                </button>\r\n            </td>\r\n        </ng-container>\r\n    </tr>\r\n    </tfoot>\r\n</table>\r\n\r\n<swal\r\n    #confirmDelete\r\n    i18n=\"@@confirmDelete\"\r\n    i18n-title=\"@@confirmTitleDeleteInventoryMember\"\r\n    i18n-text=\"@@confirmTextDeleteInventoryMember\"\r\n    title=\"Bạn có muốn xóa thành viên kiểm kho này?\"\r\n    text=\"Bạn không thể lấy lại dữ liệu sau khi xóa.\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Accept\"\r\n    cancelButtonText=\"Cancel\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<nh-menu #nhMenu>\r\n    <nh-menu-item *ngIf=\"permission.delete\"\r\n                  (clicked)=\"confirm($event)\">\r\n        <i class=\"fa fa-trash\"></i>\r\n        <span i18n=\"@@edit\">Delete</span>\r\n    </nh-menu-item>\r\n</nh-menu>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-member/inventory-member.component.ts":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-member/inventory-member.component.ts ***!
  \*****************************************************************************************************************/
/*! exports provided: InventoryMemberComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryMemberComponent", function() { return InventoryMemberComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _model_inventory_member_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../model/inventory-member.model */ "./src/app/modules/warehouse/goods/inventory/model/inventory-member.model.ts");
/* harmony import */ var _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @toverux/ngx-sweetalert2 */ "./node_modules/@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
/* harmony import */ var _service_inventory_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../service/inventory.service */ "./src/app/modules/warehouse/goods/inventory/service/inventory.service.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _shareds_components_ghm_user_suggestion_ghm_user_suggestion_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../../../shareds/components/ghm-user-suggestion/ghm-user-suggestion.component */ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.component.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");












var InventoryMemberComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](InventoryMemberComponent, _super);
    function InventoryMemberComponent(utilService, toastr, fb, inventoryService) {
        var _this = _super.call(this) || this;
        _this.utilService = utilService;
        _this.toastr = toastr;
        _this.fb = fb;
        _this.inventoryService = inventoryService;
        _this.listInventoryMember = [];
        _this.selectInventoryMember = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.inventoryMember = new _model_inventory_member_model__WEBPACK_IMPORTED_MODULE_2__["InventoryMember"]();
        return _this;
    }
    InventoryMemberComponent.prototype.ngOnInit = function () {
        this.renderForm();
    };
    InventoryMemberComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.swalConfirmDelete.confirm.subscribe(function () {
            _this.deleteInventoryMember(_this.inventoryMemberId);
        });
    };
    InventoryMemberComponent.prototype.confirm = function (inventoryMember) {
        this.swalConfirmDelete.show();
        this.inventoryMemberId = inventoryMember.id;
        this.userIdSelect = inventoryMember.userId;
    };
    InventoryMemberComponent.prototype.deleteInventoryMember = function (id) {
        var _this = this;
        if (this.inventoryId) {
            this.inventoryService.deleteInventoryMember(this.inventoryId, this.inventoryMemberId).subscribe(function () {
                lodash__WEBPACK_IMPORTED_MODULE_5__["remove"](_this.listInventoryMember, function (item) {
                    return item.userId === _this.userIdSelect;
                });
            });
        }
        else {
            lodash__WEBPACK_IMPORTED_MODULE_5__["remove"](this.listInventoryMember, function (item) {
                return item.userId === _this.userIdSelect;
            });
        }
    };
    InventoryMemberComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            this.inventoryMember = this.model.value;
            if (this.inventoryId) {
                this.isSaving = true;
                this.inventoryService.insertInventoryMember(this.inventoryId, this.inventoryMember)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["finalize"])(function () { return (_this.isSaving = false); }))
                    .subscribe(function () {
                    _this.resetForm();
                    _this.listInventoryMember.push(_this.inventoryMember);
                    _this.selectInventoryMember.emit(_this.listInventoryMember);
                });
            }
            else {
                this.resetForm();
                this.listInventoryMember.push(this.inventoryMember);
                this.selectInventoryMember.emit(this.listInventoryMember);
            }
        }
    };
    InventoryMemberComponent.prototype.updateDescription = function (item) {
        var _this = this;
        if (!item.description) {
            this.toastr.error('');
            return;
        }
        if (this.inventoryId && item.id) {
            this.inventoryService.updateInventoryMember(this.inventoryId, item.id, item)
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["finalize"])(function () { return (_this.isSaving = false); }))
                .subscribe(function () {
            });
        }
    };
    InventoryMemberComponent.prototype.selectUser = function (value) {
        var existsUser = lodash__WEBPACK_IMPORTED_MODULE_5__["find"](this.listInventoryMember, function (inventoryMember) {
            return inventoryMember.userId === value.id;
        });
        if (existsUser) {
            this.toastr.error('User already exists');
            return;
        }
        else {
            this.model.patchValue({
                userId: value.id,
                fullName: value.fullName,
                position: value.positionName,
            });
            this.utilService.focusElement('description');
        }
    };
    InventoryMemberComponent.prototype.renderForm = function () {
        this.buildForm();
    };
    InventoryMemberComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.utilService.renderFormError(['userId', 'fullName', 'positionName', 'description']);
        this.validationMessages = this.utilService.renderFormErrorMessage([
            { 'userId': ['required', 'maxLength'] },
            { 'fullName': ['required', 'maxLength'] },
            { 'positionName': ['required', 'maxLength'] },
            { 'description': ['maxLength'] }
        ]);
        this.model = this.fb.group({
            id: [this.inventoryMember.id],
            userId: [this.inventoryMember.userId, [_angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(50)]],
            fullName: [this.inventoryMember.fullName, [_angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(50)]],
            positionName: [this.inventoryMember.positionName, [_angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(100)]],
            description: [this.inventoryMember.description, [_angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(500)]]
        });
        this.model.valueChanges.subscribe(function (data) { return _this.validateModel(false); });
    };
    InventoryMemberComponent.prototype.resetForm = function () {
        this.id = null;
        this.model.patchValue(new _model_inventory_member_model__WEBPACK_IMPORTED_MODULE_2__["InventoryMember"]());
        this.ghmUserSuggestionComponent.clear();
        this.clearFormError(this.formErrors);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('confirmDelete'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_3__["SwalComponent"])
    ], InventoryMemberComponent.prototype, "swalConfirmDelete", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_shareds_components_ghm_user_suggestion_ghm_user_suggestion_component__WEBPACK_IMPORTED_MODULE_10__["GhmUserSuggestionComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_user_suggestion_ghm_user_suggestion_component__WEBPACK_IMPORTED_MODULE_10__["GhmUserSuggestionComponent"])
    ], InventoryMemberComponent.prototype, "ghmUserSuggestionComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], InventoryMemberComponent.prototype, "inventoryId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], InventoryMemberComponent.prototype, "listInventoryMember", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], InventoryMemberComponent.prototype, "selectInventoryMember", void 0);
    InventoryMemberComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-inventory-member',
            template: __webpack_require__(/*! ./inventory-member.component.html */ "./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-member/inventory-member.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shareds_services_util_service__WEBPACK_IMPORTED_MODULE_8__["UtilService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_11__["ToastrService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"],
            _service_inventory_service__WEBPACK_IMPORTED_MODULE_4__["InventoryService"]])
    ], InventoryMemberComponent);
    return InventoryMemberComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_6__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-product/inventory-product.component.html":
/*!*********************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-product/inventory-product.component.html ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form class=\"form-inline cm-mgb-10\" (ngSubmit)=\"search(1)\">\r\n    <!--<div class=\"form-group cm-mgr-5\">-->\r\n    <!--<app-warehouse-suggestion-->\r\n    <!--*ngIf=\"!isUpdate; else warehouseReadOnlyTemplate\"-->\r\n    <!--[selectedItem]=\"warehouseId && warehouseName ? {id: warehouseId, name: warehouseName} : null\"-->\r\n    <!--(itemSelected)=\"onWarehouseSelected($event)\"></app-warehouse-suggestion>-->\r\n    <!--<ng-template #warehouseReadOnlyTemplate>-->\r\n    <!--<div class=\"form-control\">-->\r\n    <!--{{ warehouseName }}-->\r\n    <!--</div>-->\r\n    <!--</ng-template>-->\r\n    <!--</div>-->\r\n    <div class=\"form-group pull-right\" *ngIf=\"!readonly\">\r\n        <button type=\"button\" class=\"btn blue\" i18n=\"@@addAllProduct\"\r\n                [swal]=\"confirmSelectAllInventoryProduct\" (confirm)=\"addAllProduct()\">\r\n            Thêm toàn bộ sản phẩm\r\n        </button>\r\n    </div>\r\n</form><!-- END search form -->\r\n<div class=\"clear\"></div>\r\n<div class=\"table-responsive\" style=\"min-height: 450px;\">\r\n    <table class=\"table table-striped table-hover\">\r\n        <thead>\r\n        <tr>\r\n            <th class=\"middle center w50\" i18n=\"@@no\">STT</th>\r\n            <th class=\"middle\" i18n=\"@@product\">Sản phẩm</th>\r\n            <th class=\"middle\" i18n=\"@@lotNumber\">Số lô</th>\r\n            <th class=\"middle w100\" i18n=\"@@unit\">Đơn vị</th>\r\n            <th class=\"text-right middle w100\" i18n=\"@@inventory\">Tồn kho</th>\r\n            <th class=\"text-right middle w100\" i18n=\"@@actual\">Thực tế</th>\r\n            <th class=\"middle text-right w100\" i18n=\"@@different\">Chênh lệch</th>\r\n            <th class=\"text-right middle w100\" i18n=\"@@price\" *ngIf=\"readonly\">Đơn giá</th>\r\n            <th class=\"text-right middle w100\" i18n=\"@@amount\" *ngIf=\"readonly\">Số tiền</th>\r\n            <th class=\"text-right middle w100\" i18n=\"@@accountantAmount\" *ngIf=\"readonly\">Số tiền kế toán</th>\r\n            <th class=\"text-right middle w100\" i18n=\"@@differenceAmount\" *ngIf=\"readonly\">Số tiền Chênh lệch</th>\r\n            <th class=\"middle center w50\" i18n=\"@@action\"\r\n                *ngIf=\"permission.delete && status !== inventoryStatus.balanced\"></th>\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n        <tr *ngFor=\"let item of details; let i = index\"\r\n            [class.color-red]=\"item.difference < 0\"\r\n            [class.color-green]=\"item.difference > 0\">\r\n            <td class=\"middle center\">{{(currentPage - 1 )*pageSize + i + 1}}</td>\r\n            <td class=\"middle w250\">{{ item.productName }}</td>\r\n            <td class=\"middle\">{{ item.lotId }}</td>\r\n            <td class=\"middle\"> {{item.unitName}}</td>\r\n            <td class=\"middle text-right\">{{item.inventoryQuantity}}</td>\r\n            <td class=\"middle text-right\">\r\n                <input *ngIf=\"status !== 1; else spanRealQuantity\" class=\"form-control text-right\"\r\n                       [(ngModel)]=\"item.realQuantity\"\r\n                       ghm-text-selection\r\n                       name=\"realQuantity - {{item.productId}}\"\r\n                       (blur)=\"onRealQuantityBlur(item)\">\r\n                <ng-template #spanRealQuantity>\r\n                    {{item.realQuantity | formatNumber}}\r\n                </ng-template>\r\n            </td>\r\n            <td class=\"text-right middle\">{{ item.difference }}</td>\r\n            <td class=\"text-right middle\" *ngIf=\"readonly\">{{ item.price | ghmCurrency:'2'}}</td>\r\n            <td class=\"text-right middle\" *ngIf=\"readonly\">{{ item.amount | ghmCurrency:'2' }}</td>\r\n            <td class=\"text-right middle\" *ngIf=\"readonly\">{{ item.accountingAmount | ghmCurrency:'2'}}</td>\r\n            <td class=\"text-right middle\" *ngIf=\"readonly\">{{ item.differenceAmount | ghmCurrency:'2'}}</td>\r\n            <td class=\"middle center\" *ngIf=\"permission.delete && status !== inventoryStatus.balanced\">\r\n                <button type=\"button\" mat-icon-button\r\n                        color=\"warn\"\r\n                        [disableRipple]=\"true\"\r\n                        (click)=\"confirm(item)\"\r\n                        i18n-matTooltip=\"@@delete\"\r\n                        matTooltip=\"Xóa\">\r\n                    <mat-icon>delete</mat-icon>\r\n                </button>\r\n            </td>\r\n        </tr>\r\n        </tbody>\r\n        <tfoot>\r\n        <tr *ngIf=\"!readonly\">\r\n            <td colspan=\"9\">\r\n                <app-product-suggestion\r\n                    #productSuggestion\r\n                    [warehouseId]=\"warehouseId\"\r\n                    (itemSelected)=\"onProductSelected($event)\"></app-product-suggestion>\r\n            </td>\r\n        </tr>\r\n        </tfoot>\r\n    </table>\r\n</div>\r\n<ghm-paging\r\n    class=\"pull-right\"\r\n    [totalRows]=\"totalRows\"\r\n    [currentPage]=\"currentPage\"\r\n    [pageShow]=\"6\"\r\n    [pageSize]=\"pageSize\"\r\n    (pageClick)=\"search($event)\"\r\n    i18n=\"@@inventoryProduct\" i18n-pageName\r\n    [pageName]=\"'Inventory Product'\">\r\n</ghm-paging>\r\n<swal\r\n    #confirmSelectAllInventoryProduct\r\n    i18n-title=\"@@confirmSelectAllInventoryProductTitle\"\r\n    title=\"Bạn có chắc chắn chọn tất cả sản phẩm của kho này\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Đồng ý\"\r\n    cancelButtonText=\"Hủy bỏ\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n<swal\r\n    #confirmDeleteProduct\r\n    i18n=\"@@confirmDeleteInventoryProduct\"\r\n    i18n-title=\"@@confirmDeleteInventoryProductTitle\"\r\n    i18n-text=\"@@confirmDeleteInventoryProductText\"\r\n    title=\"Bạn có chắc chắn muốn xóa sản phẩm này không?\"\r\n    text=\"Lưu ý: sau khi xóa bạn không thể khôi phục lại sản phẩm này.\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Đồng ý\"\r\n    cancelButtonText=\"Hủy bỏ\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n<nh-menu #nhMenu>\r\n    <nh-menu-item *ngIf=\"permission.delete\"\r\n                  (clicked)=\"confirm($event)\">\r\n        <mat-icon class=\"menu-icon\">delete</mat-icon>\r\n        <span i18n=\"@@edit\">Xóa</span>\r\n    </nh-menu-item>\r\n</nh-menu>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-product/inventory-product.component.ts":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-product/inventory-product.component.ts ***!
  \*******************************************************************************************************************/
/*! exports provided: InventoryProductComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryProductComponent", function() { return InventoryProductComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _model_inventory_detail_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../model/inventory-detail.model */ "./src/app/modules/warehouse/goods/inventory/model/inventory-detail.model.ts");
/* harmony import */ var _service_inventory_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../service/inventory.service */ "./src/app/modules/warehouse/goods/inventory/service/inventory.service.ts");
/* harmony import */ var rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/internal/operators */ "./node_modules/rxjs/internal/operators/index.js");
/* harmony import */ var rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _validators_number_validator__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../../validators/number.validator */ "./src/app/validators/number.validator.ts");
/* harmony import */ var _model_inventory_model__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../model/inventory.model */ "./src/app/modules/warehouse/goods/inventory/model/inventory.model.ts");
/* harmony import */ var _inventory_report_inventory_report_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../inventory-report/inventory-report.service */ "./src/app/modules/warehouse/goods/inventory-report/inventory-report.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @toverux/ngx-sweetalert2 */ "./node_modules/@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
/* harmony import */ var _product_product_product_suggestion_product_suggestion_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../../product/product/product-suggestion/product-suggestion.component */ "./src/app/modules/warehouse/product/product/product-suggestion/product-suggestion.component.ts");















var InventoryProductComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](InventoryProductComponent, _super);
    function InventoryProductComponent(utilService, numberValidator, fb, toastr, inventoryReportService, inventoryService) {
        var _this = _super.call(this) || this;
        _this.utilService = utilService;
        _this.numberValidator = numberValidator;
        _this.fb = fb;
        _this.toastr = toastr;
        _this.inventoryReportService = inventoryReportService;
        _this.inventoryService = inventoryService;
        _this.isUpdate = false;
        _this.onBalanced = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.warehouseSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.currentPage = 1;
        _this.pageSize = 1000;
        _this.inventoryDetail = new _model_inventory_detail_model__WEBPACK_IMPORTED_MODULE_2__["InventoryDetail"]();
        _this.inventoryStatus = _model_inventory_model__WEBPACK_IMPORTED_MODULE_10__["InventoryStatus"];
        return _this;
    }
    InventoryProductComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.renderForm();
        this.model.get('realQuantity').valueChanges.subscribe(function (value) {
            var quantity = _this.model.value.quantity;
        });
    };
    InventoryProductComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.swalConfirmDelete.confirm.subscribe(function (result) {
            _this.delete();
        });
    };
    // onWarehouseSelected(warehouse: any) {
    //     this.warehouseId = warehouse.id;
    //     this.warehouseSelected.emit(warehouse.id);
    // }
    InventoryProductComponent.prototype.onProductSelected = function (product) {
        var _this = this;
        if (!this.warehouseId) {
            this.toastr.warning(' Vui lòng chọn kho.');
            this.productSuggestion.clear();
            return;
        }
        var existingInventoryDetail = lodash__WEBPACK_IMPORTED_MODULE_5__["find"](this.details, function (detail) {
            return detail.productId === product.id && detail.lotId === product.description;
        });
        if (existingInventoryDetail) {
            this.toastr.warning('Sản phẩm đã tồn tại trong danh sách kiểm kê. Vui lòng kiểm tra lại.');
            this.productSuggestion.clear();
            return;
        }
        this.subscribers.getProduct = this.inventoryReportService.getProduct(this.warehouseId, product.id, this.date)
            .subscribe(function (result) {
            var inventoryDetail = {
                inventoryId: _this.inventoryId,
                productId: product.id,
                productName: product.name,
                lotId: result.lotId,
                unitId: result.unitId,
                unitName: result.unitName,
                inventoryQuantity: result.inventoryQuantity,
                reason: '',
            };
            _this.details = _this.details.concat([inventoryDetail]);
            _this.productSuggestion.clear();
            // if (this.isUpdate && this.inventoryId) {
            //     this.subscribers.insertDetail = this.inventoryService.insertProduct(this.inventoryId, inventoryDetail)
            //         .subscribe((resultInsert: ActionResultViewModel) => {
            //             inventoryDetail.concurrencyStamp = resultInsert.data;
            //         });
            // }
        });
    };
    InventoryProductComponent.prototype.onRealQuantityBlur = function (inventoryDetail) {
        if (inventoryDetail.realQuantity) {
            inventoryDetail.difference = inventoryDetail.realQuantity - inventoryDetail.inventoryQuantity;
        }
    };
    InventoryProductComponent.prototype.addAllProduct = function () {
        var _this = this;
        if (!this.warehouseId) {
            this.toastr.warning('Vui lòng chọn kho.');
            return;
        }
        if (!this.date) {
            this.toastr.warning('Vui lòng chọn thời ngày kiểm kê.');
            return;
        }
        this.subscribers.inventoryGetAllProducts = this.inventoryReportService
            .getAllProductToTakeInventory(this.warehouseId, this.date)
            .subscribe(function (result) {
            _this.details = result;
        });
    };
    InventoryProductComponent.prototype.search = function (currentPage) {
        var _this = this;
        this.currentPage = currentPage;
        this.isSearching = true;
        this.inventoryService.searchProduct(this.inventoryId, this.keyword, this.currentPage, this.pageSize)
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () {
            _this.isSearching = false;
        })).subscribe(function (result) {
            _this.details = result.items;
            _this.totalRows = result.totalRows;
        });
    };
    InventoryProductComponent.prototype.confirm = function (inventoryDetail) {
        this.selectedInventoryDetail = inventoryDetail;
        this.swalConfirmDelete.show();
    };
    InventoryProductComponent.prototype.resetFormSearch = function () {
        this.keyword = '';
        this.search(1);
    };
    InventoryProductComponent.prototype.selectInventory = function (inventoryDetail) {
        this.selectedInventoryDetail = lodash__WEBPACK_IMPORTED_MODULE_5__["cloneDeep"](inventoryDetail);
    };
    InventoryProductComponent.prototype.onSaveSuccess = function (inventoryDetail) {
        this.isUpdate = false;
        if (!inventoryDetail) {
            this.search(1);
        }
        else {
            var inventoryDetailInfo = lodash__WEBPACK_IMPORTED_MODULE_5__["find"](this.details, function (item) {
                return item.productId === inventoryDetail.productId && item.unitId === inventoryDetail.unitId;
            });
            if (inventoryDetailInfo) {
                inventoryDetailInfo.realQuantity = inventoryDetail.realQuantity;
                inventoryDetailInfo.reason = inventoryDetail.reason;
            }
        }
    };
    InventoryProductComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            this.inventoryDetail = this.model.value;
            if (this.inventoryId) {
                if (!this.isUpdate) {
                    this.inventoryService.insertProduct(this.inventoryId, this.inventoryDetail).subscribe(function () {
                        _this.resetForm();
                        _this.search(1);
                    });
                }
                else {
                    this.inventoryService.updateProduct(this.inventoryDetail).subscribe(function () {
                        _this.resetForm();
                        _this.search(1);
                    });
                }
            }
            else {
                var inventoryDetailInfo = this.details[this.index];
                if (inventoryDetailInfo) {
                    inventoryDetailInfo.realQuantity = this.inventoryDetail.realQuantity;
                    inventoryDetailInfo.reason = this.inventoryDetail.reason;
                }
            }
            lodash__WEBPACK_IMPORTED_MODULE_5__["each"](this.details, function (item) {
                item.isEdit = false;
            });
        }
    };
    InventoryProductComponent.prototype.update = function (inventoryDetail) {
        var _this = this;
        this.subscribers.updateProduct = this.inventoryService.updateProduct(inventoryDetail)
            .subscribe(function (result) {
            _this.toastr.success(result.message);
            inventoryDetail.concurrencyStamp = result.data;
        });
    };
    InventoryProductComponent.prototype.edit = function (inventoryDetail, index) {
        lodash__WEBPACK_IMPORTED_MODULE_5__["each"](this.details, function (item) {
            item.isEdit = false;
        });
        this.index = index;
        inventoryDetail.isEdit = true;
        this.isUpdate = true;
        this.productId = inventoryDetail.productId;
        this.model.patchValue({
            inventoryId: inventoryDetail.productId,
            productId: inventoryDetail.productId,
            productName: inventoryDetail.productName,
            unitId: inventoryDetail.unitId,
            unitName: inventoryDetail.unitName,
            inventoryQuantity: inventoryDetail.inventoryQuantity,
            realQuantity: inventoryDetail.realQuantity ? this.inventoryDetail.realQuantity : 0,
            reason: inventoryDetail.reason,
        });
    };
    InventoryProductComponent.prototype.editRealQuantity = function (inventoryDetail, index) {
        this.edit(inventoryDetail, index);
        this.utilService.focusElement('realQuantity');
    };
    InventoryProductComponent.prototype.editReason = function (inventoryDetail, index) {
        this.edit(inventoryDetail, index);
        this.utilService.focusElement('reason');
    };
    InventoryProductComponent.prototype.delete = function () {
        var _this = this;
        // if (this.inventoryId) {
        //     this.subscribers.deleteProduct = this.inventoryService.deleteProduct(this.selectedInventoryDetail.inventoryId,
        //         this.selectedInventoryDetail.productId)
        //         .subscribe((result: ActionResultViewModel) => {
        //             _.remove(this.details, (detail: InventoryDetail) => {
        //                 return detail.productId === this.selectedInventoryDetail.productId
        //                     && detail.productId === this.selectedInventoryDetail.productId;
        //             });
        //         });
        // } else {
        lodash__WEBPACK_IMPORTED_MODULE_5__["remove"](this.details, function (detail) {
            return detail.productId === _this.selectedInventoryDetail.productId
                && detail.productId === _this.selectedInventoryDetail.productId;
        });
        // }
    };
    InventoryProductComponent.prototype.balance = function () {
        var _this = this;
        this.inventoryService.balancedWarehouse(this.inventoryId).subscribe(function () {
            _this.search(1);
            _this.status = _model_inventory_model__WEBPACK_IMPORTED_MODULE_10__["InventoryStatus"].balanced;
            _this.onBalanced.emit();
        });
    };
    InventoryProductComponent.prototype.renderForm = function () {
        var _this = this;
        this.formErrors = this.utilService.renderFormError(['productId', 'unitId', 'closingStockQuantity', 'realQuantity',
            'reason', 'difference']);
        this.validationMessages = this.utilService.renderFormErrorMessage([
            { 'productId': ['required'] },
            { 'unitId': ['required'] },
            { 'closingStockQuantity': ['isValid', 'required', 'lessThan', 'greaterThan'] },
            { 'realQuantity': ['isValid', 'required', 'lessThan', 'greaterThan'] },
            { 'reason': ['maxLength'] },
            { 'difference': ['isValid', 'required', 'lessThan'] }
        ]);
        this.model = this.fb.group({
            inventoryId: [this.inventoryId],
            productId: [this.inventoryDetail.productId, [_angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required]],
            productName: [this.inventoryDetail.productName],
            unitId: [this.inventoryDetail.unitId, [_angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required]],
            unitName: [this.inventoryDetail.unitName],
            inventoryQuantity: [this.inventoryDetail.inventoryQuantity, [this.numberValidator.isValid,
                    this.numberValidator.lessThan(2147483648), this.numberValidator.greaterThan(0)]],
            realQuantity: [this.inventoryDetail.realQuantity, [_angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required, this.numberValidator.isValid,
                    this.numberValidator.greaterThan(0), this.numberValidator.lessThan(2147483648)]],
            reason: [this.inventoryDetail.reason, [_angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(500)]],
            difference: [this.inventoryDetail.difference, [this.numberValidator.isValid, this.numberValidator.lessThan(2147483648)]]
        });
        this.model.valueChanges.subscribe(function (data) { return _this.validateModel(false); });
    };
    InventoryProductComponent.prototype.resetForm = function () {
        this.model.patchValue(new _model_inventory_detail_model__WEBPACK_IMPORTED_MODULE_2__["InventoryDetail"]());
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('confirmDeleteProduct'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_13__["SwalComponent"])
    ], InventoryProductComponent.prototype, "swalConfirmDelete", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('productSuggestion'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _product_product_product_suggestion_product_suggestion_component__WEBPACK_IMPORTED_MODULE_14__["ProductSuggestionComponent"])
    ], InventoryProductComponent.prototype, "productSuggestion", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], InventoryProductComponent.prototype, "details", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], InventoryProductComponent.prototype, "inventoryId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], InventoryProductComponent.prototype, "status", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], InventoryProductComponent.prototype, "totalRows", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], InventoryProductComponent.prototype, "date", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], InventoryProductComponent.prototype, "isUpdate", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], InventoryProductComponent.prototype, "onBalanced", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], InventoryProductComponent.prototype, "warehouseSelected", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], InventoryProductComponent.prototype, "warehouseId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], InventoryProductComponent.prototype, "readonly", void 0);
    InventoryProductComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-inventory-product',
            template: __webpack_require__(/*! ./inventory-product.component.html */ "./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-product/inventory-product.component.html"),
            providers: [_validators_number_validator__WEBPACK_IMPORTED_MODULE_9__["NumberValidator"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shareds_services_util_service__WEBPACK_IMPORTED_MODULE_8__["UtilService"],
            _validators_number_validator__WEBPACK_IMPORTED_MODULE_9__["NumberValidator"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_12__["ToastrService"],
            _inventory_report_inventory_report_service__WEBPACK_IMPORTED_MODULE_11__["InventoryReportService"],
            _service_inventory_service__WEBPACK_IMPORTED_MODULE_3__["InventoryService"]])
    ], InventoryProductComponent);
    return InventoryProductComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_6__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory/inventory.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory/inventory.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@inventoryPageTitle\">Kiêm kê</span>\r\n    <small i18n=\"@@warehouseModuleTitle\">Quản lý kho</small>\r\n</h1>\r\n<form class=\"form-inline cm-mgb-10\" (ngSubmit)=\"search(1)\">\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <app-warehouse-suggestion\r\n            [selectedItem]=\"warehouseId && warehouseName ? {id: warehouseId, name: warehouseName} : null\"\r\n            (itemSelected)=\"selectWarehouse($event)\"></app-warehouse-suggestion>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <nh-date placeholder=\"Từ ngày\"\r\n                 i18n-placeholder=\"@@fromDate\"\r\n                 [(ngModel)]=\"fromDate\"\r\n                 [allowRemove]=\"true\"\r\n                 [mask]=\"true\"\r\n                 [showTime]=\"true\"\r\n                 (selectedDateEvent)=\"search(1)\"\r\n                 name=\"fromDate\"></nh-date>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <nh-date placeholder=\"Đến ngày\"\r\n                 i18n-placeholder=\"@@toDate\"\r\n                 [(ngModel)]=\"toDate\"\r\n                 [allowRemove]=\"true\"\r\n                 [mask]=\"true\"\r\n                 [showTime]=\"true\"\r\n                 (selectedDateEvent)=\"search(1)\"\r\n                 name=\"toDate\"></nh-date>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <nh-select\r\n            [title]=\"'-- Tất cả --'\"\r\n            i18n-title=\"@@allTitle\"\r\n            [data]=\"[{id: 1, name: 'Đã xử lý'}, {id: 0, name: 'Chưa xử lý'}]\"\r\n            [value]=\"isResolve\"\r\n            (onSelectItem)=\"selectResolve($event)\"\r\n        ></nh-select>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <input type=\"text\" class=\"form-control\" i18n=\"@@keywordSearch\" i18n-placeholder\r\n               placeholder=\"Nhập từ khóa tìm kiếm.\"\r\n               name=\"searchInput\" [(ngModel)]=\"keyword\">\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <button class=\"btn blue\" type=\"submit\">\r\n            <i class=\"fa fa-search\" *ngIf=\"!isSearching\"></i>\r\n            <i class=\"fa fa-pulse fa-spinner\" *ngIf=\"isSearching\"></i>\r\n        </button>\r\n    </div>\r\n    <div class=\"form-group cm-mgl-5\">\r\n        <button class=\"btn default\" type=\"button\" (click)=\"resetFormSearch()\">\r\n            <i class=\"fa fa-refresh\"></i>\r\n        </button>\r\n    </div>\r\n    <div class=\"form-group pull-right\">\r\n        <a class=\"btn blue cm-mgr-5\" *ngIf=\"permission.add\" i18n=\"@@add\" routerLink=\"/goods/inventories/add\"\r\n           type=\"button\" i18n=\"@@add\">\r\n            Thêm\r\n        </a>\r\n    </div>\r\n</form>\r\n<table class=\"table table-striped table-hover\">\r\n    <thead>\r\n    <tr>\r\n        <th class=\"middle center w50\" i18n=\"@@no\">STT</th>\r\n        <th class=\"middle w200\" i18n=\"@@warehouse\">Kho kiểm kê</th>\r\n        <th class=\"middle \" i18n=\"@@creator\">Người tạo</th>\r\n        <th class=\"middle w150\" i18n=\"@@inventoryDate\">Ngày kiểm kê</th>\r\n        <th class=\"middle w100\" i18n=\"@@issuePerson\">Trạng thái</th>\r\n        <th class=\"middle text-right\" i18n=\"@@description\">Ghi chú</th>\r\n        <th class=\"middle text-right w150\" i18n=\"@@action\" *ngIf=\"permission.edit || permission.delete\"></th>\r\n    </tr>\r\n    </thead>\r\n    <tbody>\r\n    <tr *ngFor=\"let item of listItems$ | async; let i = index\">\r\n        <td class=\"middle center\">{{(currentPage - 1) * pageSize + i + 1}}</td>\r\n        <td class=\"middle\">{{ item.warehouseName }}</td>\r\n        <td class=\"middle\">{{ item.creatorFullName }}</td>\r\n        <td class=\"middle\">{{ item.date | dateTimeFormat:'DD/MM/YYYY HH:mm' }}</td>\r\n        <td class=\"middle\">\r\n         <span class=\"badge\"\r\n               [class.badge-success]=\"item.status === inventoryStatus.balanced\"\r\n               [class.badge-danger]=\"item.status === inventoryStatus.temporary\">{item.status, select, 0 {Chưa xử lý} 1 {Đã xử lý} Khác {}}</span>\r\n        </td>\r\n        <td class=\"middle text-right\">{{item.note}}</td>\r\n        <td class=\"text-right middle\" *ngIf=\"permission.edit || permission.delete\">\r\n            <nh-dropdown>\r\n                <button type=\"button\" class=\"btn btn-sm btn-light btn-no-background no-border\" matTooltip=\"Menu\">\r\n                    <mat-icon>more_horiz</mat-icon>\r\n                </button>\r\n                <ul class=\"nh-dropdown-menu right\" role=\"menu\">\r\n                    <li>\r\n                        <a *ngIf=\"permission.view\"\r\n                           routerLink=\"/goods/inventories/{{ item.id }}\"\r\n                           i18n=\"@@edit\">\r\n                            <!--<i class=\"fa fa-eye\"></i>-->\r\n                            <mat-icon class=\"menu-icon\">info</mat-icon>\r\n                            Xem\r\n                        </a>\r\n                    </li>\r\n                    <li>\r\n                        <a *ngIf=\"permission.edit && item.status !== inventoryStatus.balanced\"\r\n                           routerLink=\"/goods/inventories/edit/{{ item.id }}\"\r\n                           i18n=\"@@edit\">\r\n                            <mat-icon class=\"menu-icon\">edit</mat-icon>\r\n                            Sửa\r\n                        </a>\r\n                    </li>\r\n                    <li>\r\n                        <a *ngIf=\"permission.delete && item.status !== inventoryStatus.balanced\"\r\n                           (click)=\"confirm(item)\" i18n=\"@@delete\">\r\n                            <mat-icon class=\"menu-icon\">delete</mat-icon>\r\n                            Xóa\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </nh-dropdown>\r\n        </td>\r\n    </tr>\r\n    </tbody>\r\n</table>\r\n\r\n<!--<app-inventory-form (saveSuccessful)=\"search(1)\"></app-inventory-form>-->\r\n\r\n<ghm-paging\r\n    class=\"pull-right\"\r\n    [totalRows]=\"totalRows\"\r\n    [currentPage]=\"currentPage\"\r\n    [pageShow]=\"6\"\r\n    [pageSize]=\"pageSize\"\r\n    (pageClick)=\"search($event)\"\r\n    i18n=\"@@inventory\" i18n-pageName\r\n    [pageName]=\"'Inventory'\">\r\n</ghm-paging>\r\n\r\n<swal\r\n    #confirmDelete\r\n    i18n=\"@@confirmDelete\"\r\n    i18n-title=\"@@confirmTitleDeleteInventory\"\r\n    i18n-text=\"@@confirmTextDeleteInventory\"\r\n    title=\"Are you sure for delete this inventory?\"\r\n    text=\"You can't recover this inventory after delete.\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Accept\"\r\n    cancelButtonText=\"Cancel\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<nh-menu #nhMenu>\r\n    <nh-menu-item (clicked)=\"detail($event)\">\r\n        <!--<i class=\"fa fa-eye\"></i>-->\r\n        <mat-icon class=\"menu-icon\">info</mat-icon>\r\n        <span i18n=\"@@view\">Chi tiết</span>\r\n    </nh-menu-item>\r\n    <nh-menu-item (clicked)=\"edit($event)\">\r\n        <mat-icon class=\"menu-icon\">edit</mat-icon>\r\n        <span i18n=\"@@edit\">Sửa</span>\r\n    </nh-menu-item>\r\n    <nh-menu-item\r\n        (clicked)=\"confirm($event)\">\r\n        <mat-icon class=\"menu-icon\">delete</mat-icon>\r\n        <span i18n=\"@@edit\">Xóa</span>\r\n    </nh-menu-item>\r\n</nh-menu>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory/inventory.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory/inventory.component.ts ***!
  \**************************************************************************/
/*! exports provided: InventoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryComponent", function() { return InventoryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _configs_page_id_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../configs/page-id.config */ "./src/app/configs/page-id.config.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../shareds/models/filter-link.model */ "./src/app/shareds/models/filter-link.model.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @toverux/ngx-sweetalert2 */ "./node_modules/@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _service_inventory_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./service/inventory.service */ "./src/app/modules/warehouse/goods/inventory/service/inventory.service.ts");
/* harmony import */ var _inventory_form_inventory_form_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./inventory-form/inventory-form.component */ "./src/app/modules/warehouse/goods/inventory/inventory-form/inventory-form.component.ts");
/* harmony import */ var _warehouse_warehouse_suggestion_warehouse_suggestion_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../warehouse/warehouse-suggestion/warehouse-suggestion.component */ "./src/app/modules/warehouse/warehouse/warehouse-suggestion/warehouse-suggestion.component.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _model_inventory_model__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./model/inventory.model */ "./src/app/modules/warehouse/goods/inventory/model/inventory.model.ts");
















var InventoryComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](InventoryComponent, _super);
    function InventoryComponent(pageId, appConfig, location, route, router, toastr, invertoryService, utilService) {
        var _this = _super.call(this) || this;
        _this.pageId = pageId;
        _this.appConfig = appConfig;
        _this.location = location;
        _this.route = route;
        _this.router = router;
        _this.toastr = toastr;
        _this.invertoryService = invertoryService;
        _this.utilService = utilService;
        _this.inventoryStatus = _model_inventory_model__WEBPACK_IMPORTED_MODULE_15__["InventoryStatus"];
        return _this;
    }
    InventoryComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.appService.setupPage(this.pageId.WAREHOUSE, this.pageId.INVENTORY, 'Kiểm kê', 'Quản lý kho');
        // this.subscribers.data = this.route.data.subscribe((result: { data: SearchResultViewModel<InventorySearchViewModel> }) => {
        //     const data = result.data;
        //     this.listInventory = this.rendResult( data.items);
        //     this.totalRows = data.totalRows;
        // });
        this.subscribers.queryParams = this.route.queryParams.subscribe(function (params) {
            _this.keyword = params.keyword ? params.keyword : '';
            _this.fromDate = params.keyword ? params.fromDate : '';
            _this.toDate = params.toDate ? params.toDate : '';
            _this.isResolve = params.isResolve !== null && params.isResolve !== '' && params.isResolve !== undefined
                ? parseInt(params.isResolve) : null;
            _this.warehouseId = params.warehouseId ? params.warehouseId : null;
            _this.warehouseName = params.warehouseName ? params.warehouseName : null;
            _this.currentPage = params.page ? parseInt(params.page) : 1;
            _this.pageSize = params.pageSize ? parseInt(params.pageSize) : _this.appConfig.PAGE_SIZE;
        });
        this.listItems$ = this.route.data.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            var data = result.data;
            _this.totalRows = data.totalRows;
            return data.items;
        }));
    };
    InventoryComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.swalConfirmDelete.confirm.subscribe(function (result) {
            _this.delete(_this.inventoryId);
        });
    };
    InventoryComponent.prototype.add = function () {
        this.inventoryForm.add();
    };
    InventoryComponent.prototype.search = function (currentPage) {
        var _this = this;
        if (!this.warehouseId) {
            this.toastr.error('Please select warehouse');
            return;
        }
        this.currentPage = currentPage;
        this.isSearching = true;
        this.renderFilterLink();
        this.listItems$ = this.invertoryService.search(this.keyword, this.fromDate, this.toDate, this.isResolve, this.warehouseId, this.currentPage, this.pageSize)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () {
            _this.isSearching = false;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.totalRows = result.totalRows;
            return result.items;
        }));
        //     .subscribe(() => {
        //     // this.listInventory = this.rendResult(result.items);
        //     // this.totalRows = result.totalRows;
        // });
    };
    InventoryComponent.prototype.onPageClick = function (page) {
        this.currentPage = page;
        this.search(1);
    };
    InventoryComponent.prototype.resetFormSearch = function () {
        this.keyword = '';
        this.fromDate = '';
        this.toDate = '';
        this.warehouseSuggestionComponent.clear();
        this.search(1);
    };
    InventoryComponent.prototype.edit = function (inventory) {
        // this.inventoryForm.edit(inventory.id);
        this.router.navigateByUrl("/goods/inventories/edit/" + inventory.id);
    };
    InventoryComponent.prototype.delete = function (id) {
        var _this = this;
        this.invertoryService.delete(id).subscribe(function () {
            _this.search(1);
            // _.remove(this.listInventory, (item: InventorySearchViewModel) => {
            //     return item.id === id;
            // });
        });
    };
    InventoryComponent.prototype.detail = function (inventory) {
        this.router.navigateByUrl("/goods/inventories/" + inventory.id);
        // this.goodsDeliveryNoteDetailComponent.show(goodsDeliveryNote.id);
    };
    InventoryComponent.prototype.confirm = function (value) {
        this.swalConfirmDelete.show();
        this.inventoryId = value.id;
    };
    InventoryComponent.prototype.selectWarehouse = function (warehouse) {
        if (warehouse) {
            this.warehouseId = warehouse.id;
            this.warehouseName = warehouse.name;
        }
        else {
            this.warehouseId = null;
            this.warehouseName = '';
        }
        this.search(1);
    };
    InventoryComponent.prototype.selectResolve = function (value) {
        this.isResolve = value ? value.id : '';
    };
    // private rendResult(list: InventorySearchViewModel[]) {
    //     if (list != null && list.length > 0) {
    //         _.each(list, (item: InventorySearchViewModel) => {
    //             if (item.inventoryMemberFullNames && item.inventoryMemberFullNames.length > 0) {
    //                 item.members = _.join(item.inventoryMemberFullNames, ', ');
    //             }
    //         });
    //
    //         return list;
    //     }
    // }
    InventoryComponent.prototype.renderFilterLink = function () {
        var path = '/goods/inventories';
        var query = this.utilService.renderLocationFilter([
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_6__["FilterLink"]('keyword', this.keyword),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_6__["FilterLink"]('fromDate', this.fromDate),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_6__["FilterLink"]('toDate', this.toDate),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_6__["FilterLink"]('isResolve', this.isResolve),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_6__["FilterLink"]('warehouseId', this.warehouseId),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_6__["FilterLink"]('warehouseName', this.warehouseName),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_6__["FilterLink"]('page', this.currentPage),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_6__["FilterLink"]('pageSize', this.pageSize)
        ]);
        this.location.go(path, query);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('confirmDelete'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_8__["SwalComponent"])
    ], InventoryComponent.prototype, "swalConfirmDelete", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_warehouse_warehouse_suggestion_warehouse_suggestion_component__WEBPACK_IMPORTED_MODULE_13__["WarehouseSuggestionComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _warehouse_warehouse_suggestion_warehouse_suggestion_component__WEBPACK_IMPORTED_MODULE_13__["WarehouseSuggestionComponent"])
    ], InventoryComponent.prototype, "warehouseSuggestionComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_inventory_form_inventory_form_component__WEBPACK_IMPORTED_MODULE_12__["InventoryFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _inventory_form_inventory_form_component__WEBPACK_IMPORTED_MODULE_12__["InventoryFormComponent"])
    ], InventoryComponent.prototype, "inventoryForm", void 0);
    InventoryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-inventory',
            template: __webpack_require__(/*! ./inventory.component.html */ "./src/app/modules/warehouse/goods/inventory/inventory.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_page_id_config__WEBPACK_IMPORTED_MODULE_2__["PAGE_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_9__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, Object, _angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_14__["ToastrService"],
            _service_inventory_service__WEBPACK_IMPORTED_MODULE_11__["InventoryService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_3__["UtilService"]])
    ], InventoryComponent);
    return InventoryComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_10__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory/inventory.scss":
/*!******************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory/inventory.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvd2FyZWhvdXNlL2dvb2RzL2ludmVudG9yeS9pbnZlbnRvcnkuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory/model/inventory-detail.model.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory/model/inventory-detail.model.ts ***!
  \***********************************************************************************/
/*! exports provided: InventoryDetail */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryDetail", function() { return InventoryDetail; });
var InventoryDetail = /** @class */ (function () {
    function InventoryDetail(productId, lotId, productName, unitId, unitName, inventoryQuantity, realQuantity, reason, difference) {
        this.productId = productId;
        this.lotId = lotId;
        this.productName = productName;
        this.unitId = unitId;
        this.unitName = unitName;
        this.inventoryQuantity = inventoryQuantity ? inventoryQuantity : 0;
        this.realQuantity = realQuantity ? realQuantity : 0;
        this.reason = reason ? reason : '';
        this.difference = difference;
    }
    return InventoryDetail;
}());



/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory/model/inventory-member.model.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory/model/inventory-member.model.ts ***!
  \***********************************************************************************/
/*! exports provided: InventoryMember */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryMember", function() { return InventoryMember; });
var InventoryMember = /** @class */ (function () {
    function InventoryMember(userId, fullName, position, description) {
        this.userId = userId;
        this.fullName = fullName;
        this.positionName = position;
        this.description = description;
    }
    return InventoryMember;
}());



/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory/model/inventory.model.ts":
/*!****************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory/model/inventory.model.ts ***!
  \****************************************************************************/
/*! exports provided: InventoryStatus, Inventory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryStatus", function() { return InventoryStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Inventory", function() { return Inventory; });
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);

var InventoryStatus;
(function (InventoryStatus) {
    InventoryStatus[InventoryStatus["temporary"] = 0] = "temporary";
    InventoryStatus[InventoryStatus["balanced"] = 1] = "balanced";
})(InventoryStatus || (InventoryStatus = {}));
var Inventory = /** @class */ (function () {
    function Inventory(warehouseId, inventoryDate, note, status) {
        this.warehouseId = warehouseId;
        this.warehouseName = '';
        // this.inventoryDate = inventoryDate ? inventoryDate : new Date();
        this.note = note;
        this.status = InventoryStatus.temporary;
        this.details = [];
        this.members = [];
        this.concurrencyStamp = '';
        this.inventoryDate = moment__WEBPACK_IMPORTED_MODULE_0__().format('YYYY/MM/DD HH:mm:ss');
    }
    return Inventory;
}());



/***/ }),

/***/ "./src/app/modules/warehouse/goods/inventory/service/inventory.service.ts":
/*!********************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/inventory/service/inventory.service.ts ***!
  \********************************************************************************/
/*! exports provided: InventoryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryService", function() { return InventoryService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/internal/operators */ "./node_modules/rxjs/internal/operators/index.js");
/* harmony import */ var rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");







var InventoryService = /** @class */ (function () {
    function InventoryService(appConfig, spinceService, http, spinnerService, toastr) {
        this.appConfig = appConfig;
        this.spinceService = spinceService;
        this.http = http;
        this.spinnerService = spinnerService;
        this.toastr = toastr;
        this.url = 'api/v1/warehouse/inventories';
        this.url = "" + appConfig.API_GATEWAY_URL + this.url;
    }
    InventoryService.prototype.resolve = function (route, state) {
        var queryParams = route.queryParams;
        return this.search(queryParams.keyword, queryParams.fromDate, queryParams.toDate, queryParams.isResolve, queryParams.warehouseId, queryParams.page, queryParams.pageSize);
    };
    InventoryService.prototype.search = function (keyword, fromDate, toDate, isResolve, warehouseId, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = this.appConfig.PAGE_SIZE; }
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpParams"]()
            .set('keyword', keyword ? keyword : '')
            .set('fromDate', fromDate ? fromDate : '')
            .set('toDate', toDate ? toDate : '')
            .set('isResolve', isResolve !== null && isResolve !== undefined ? isResolve.toString() : '')
            .set('warehouseId', warehouseId ? warehouseId : '')
            .set('page', page ? page.toString() : '1')
            .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString());
        return this.http.get("" + this.url, {
            params: params
        });
    };
    InventoryService.prototype.insert = function (inventory, isBalanced) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/balanced/" + isBalanced, inventory).pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        })).pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    InventoryService.prototype.update = function (id, inventory, isBalanced) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/" + id + "/balanced/" + isBalanced, inventory).pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        })).pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    InventoryService.prototype.delete = function (id) {
        var _this = this;
        return this.http.delete(this.url + "/" + id)
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    InventoryService.prototype.getDetailForEdit = function (id) {
        var _this = this;
        this.spinceService.show();
        return this.http.get(this.url + "/edit/" + id, {})
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () {
            _this.spinceService.hide();
        }));
    };
    InventoryService.prototype.getDetail = function (id) {
        var _this = this;
        this.spinceService.show();
        return this.http.get(this.url + "/" + id, {})
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () {
            _this.spinceService.hide();
        }));
    };
    InventoryService.prototype.searchProduct = function (inventoryId, keyword, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = this.appConfig.PAGE_SIZE; }
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpParams"]()
            .set('keyword', keyword ? keyword : '')
            .set('page', page ? page.toString() : '1')
            .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString());
        return this.http.get(this.url + "/" + inventoryId + "/detail", {
            params: params
        });
    };
    InventoryService.prototype.insertProduct = function (inventoryId, inventoryDetail) {
        var _this = this;
        return this.http.post(this.url + "/" + inventoryId + "/detail", inventoryDetail)
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    InventoryService.prototype.updateProduct = function (inventoryDetail) {
        return this.http
            .post(this.url + "/" + inventoryDetail.inventoryId + "/detail/" + inventoryDetail.productId, inventoryDetail);
    };
    InventoryService.prototype.deleteProduct = function (inventoryId, productId) {
        var _this = this;
        return this.http.delete(this.url + "/" + inventoryId + "/products/" + productId)
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    InventoryService.prototype.insertInventoryMember = function (inventoryId, inventoryMember) {
        var _this = this;
        return this.http.post(this.url + "/" + inventoryId + "/member", inventoryMember)
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    InventoryService.prototype.updateInventoryMember = function (inventoryId, inventoryMemberId, inventoryMember) {
        var _this = this;
        return this.http.post(this.url + "/" + inventoryId + "/member/" + inventoryMemberId, inventoryMember)
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    InventoryService.prototype.deleteInventoryMember = function (inventoryId, inventoryMemberId) {
        var _this = this;
        return this.http.delete(this.url + "/" + inventoryId + "/member/" + inventoryMemberId)
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    InventoryService.prototype.getProductByWarehouseId = function (keyword, warehouseId, date, page, pageSize) {
        var _this = this;
        this.spinceService.show();
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpParams"]()
            .set('keyword', keyword ? keyword : '')
            .set('date', date ? date : '')
            .set('page', page ? page.toString() : '1')
            .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString());
        return this.http.get(this.url + "/products/" + warehouseId, {
            params: params
        }).pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () {
            _this.spinceService.hide();
        }));
    };
    InventoryService.prototype.balancedWarehouse = function (inventoryId) {
        var _this = this;
        return this.http.post(this.url + "/balanced-warehouse/" + inventoryId, {})
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    InventoryService.prototype.export = function (inventoryId, inventory, isBalance) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/" + inventoryId + "/exports/" + isBalance, inventory, { responseType: 'blob' })
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (response) {
            return new Blob([response]);
        }));
    };
    InventoryService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_6__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_1__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_3__["SpinnerService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_3__["SpinnerService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"]])
    ], InventoryService);
    return InventoryService;
}());



/***/ }),

/***/ "./src/app/modules/warehouse/goods/lot-suggestion/lot-suggestion.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/lot-suggestion/lot-suggestion.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-suggestion\r\n    [class.has-error]=\"hasError\"\r\n    [class.receipt]=\"isReceipt\"\r\n    [loading]=\"isSearching\"\r\n    [sources]=\"listItems\"\r\n    [selectedItem]=\"selectedItem\"\r\n    [allowAdd]=\"true\"\r\n    (itemRemoved)=\"itemRemoved.emit($event)\"\r\n    (itemSelected)=\"onItemSelected($event)\"\r\n    (searched)=\"onSearchKeyPress($event)\"\r\n></nh-suggestion>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/lot-suggestion/lot-suggestion.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/lot-suggestion/lot-suggestion.component.ts ***!
  \************************************************************************************/
/*! exports provided: LotSuggestionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LotSuggestionComponent", function() { return LotSuggestionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../shareds/components/nh-suggestion/nh-suggestion.component */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.component.ts");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _goods_receipt_note_goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../goods-receipt-note/goods-receipt-note.service */ "./src/app/modules/warehouse/goods/goods-receipt-note/goods-receipt-note.service.ts");






var LotSuggestionComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](LotSuggestionComponent, _super);
    function LotSuggestionComponent(goodsReceiptNoteService) {
        var _this = _super.call(this) || this;
        _this.goodsReceiptNoteService = goodsReceiptNoteService;
        _this.multiple = false;
        _this.isReceipt = false;
        _this.hasError = false;
        _this.keyPressed = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.itemSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.itemRemoved = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        return _this;
    }
    LotSuggestionComponent.prototype.ngOnInit = function () {
    };
    LotSuggestionComponent.prototype.onItemSelected = function (item) {
        this.itemSelected.emit(item);
    };
    LotSuggestionComponent.prototype.onSearchKeyPress = function (keyword) {
        this.keyPressed.emit(keyword);
        this.keyword = keyword;
        this.search(1);
    };
    LotSuggestionComponent.prototype.search = function (currentPage) {
        var _this = this;
        this.isSearching = true;
        this.currentPage = currentPage;
        this.goodsReceiptNoteService.lotSuggestion(this.keyword, this.currentPage, this.appConfig.PAGE_SIZE)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () { return _this.isSearching = false; }))
            .subscribe(function (result) {
            _this.totalRows = result.totalRows;
            _this.listItems = result.items;
        });
    };
    LotSuggestionComponent.prototype.clear = function () {
        this.nhSuggestionComponent.clear();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_3__["NhSuggestionComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_3__["NhSuggestionComponent"])
    ], LotSuggestionComponent.prototype, "nhSuggestionComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], LotSuggestionComponent.prototype, "multiple", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], LotSuggestionComponent.prototype, "isReceipt", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], LotSuggestionComponent.prototype, "selectedItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], LotSuggestionComponent.prototype, "hasError", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], LotSuggestionComponent.prototype, "keyPressed", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], LotSuggestionComponent.prototype, "itemSelected", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], LotSuggestionComponent.prototype, "itemRemoved", void 0);
    LotSuggestionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-lot-suggestion',
            template: __webpack_require__(/*! ./lot-suggestion.component.html */ "./src/app/modules/warehouse/goods/lot-suggestion/lot-suggestion.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_goods_receipt_note_goods_receipt_note_service__WEBPACK_IMPORTED_MODULE_5__["GoodsReceiptNoteService"]])
    ], LotSuggestionComponent);
    return LotSuggestionComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_4__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/receiver-suggestion/receiver-suggestion.component.html":
/*!************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/receiver-suggestion/receiver-suggestion.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-suggestion\r\n    [class.receipt]=\"isReceipt\"\r\n    [loading]=\"isSearching\"\r\n    [sources]=\"listItems\"\r\n    [selectedItem]=\"selectedItem\"\r\n    [totalRows]=\"totalRows\"\r\n    [pageSize]=\"10\"\r\n    [allowAdd]=\"true\"\r\n    (itemSelected)=\"onItemSelected($event)\"\r\n    (itemRemoved)=\"itemRemoved.emit($event)\"\r\n    (searched)=\"onSearchKeyPress($event)\"\r\n    (nextPage)=\"onNextPage($event)\"\r\n></nh-suggestion>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/receiver-suggestion/receiver-suggestion.component.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/receiver-suggestion/receiver-suggestion.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: ReceiverSuggestionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReceiverSuggestionComponent", function() { return ReceiverSuggestionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../shareds/components/nh-suggestion/nh-suggestion.component */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.component.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _goods_delivery_note_goods_delivery_note_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../goods-delivery-note/goods-delivery-note.service */ "./src/app/modules/warehouse/goods/goods-delivery-note/goods-delivery-note.service.ts");






var ReceiverSuggestionComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](ReceiverSuggestionComponent, _super);
    function ReceiverSuggestionComponent(goodsDeliveryNoteService) {
        var _this = _super.call(this) || this;
        _this.goodsDeliveryNoteService = goodsDeliveryNoteService;
        _this.multiple = false;
        _this.isReceipt = false;
        _this.keyPressed = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.itemSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.itemRemoved = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        return _this;
    }
    ReceiverSuggestionComponent.prototype.ngOnInit = function () {
    };
    ReceiverSuggestionComponent.prototype.onItemSelected = function (item) {
        this.itemSelected.emit(item);
    };
    ReceiverSuggestionComponent.prototype.onSearchKeyPress = function (keyword) {
        this.keyword = keyword;
        this.keyPressed.emit(keyword);
        this.search(1);
    };
    ReceiverSuggestionComponent.prototype.search = function (currentPage, isLoadMore) {
        var _this = this;
        if (isLoadMore === void 0) { isLoadMore = false; }
        this.isSearching = true;
        this.currentPage = currentPage;
        this.goodsDeliveryNoteService.receiverSuggestion(this.keyword, this.currentPage, this.appConfig.PAGE_SIZE)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(function () { return _this.isSearching = false; }))
            .subscribe(function (result) {
            _this.totalRows = result.totalRows;
            if (isLoadMore) {
                _this.listItems = _this.listItems.concat(result.items);
            }
            else {
                _this.listItems = result.items;
            }
        });
    };
    ReceiverSuggestionComponent.prototype.clear = function () {
        this.nhSuggestionComponent.clear();
    };
    ReceiverSuggestionComponent.prototype.onNextPage = function (event) {
        this.keyword = event.keyword;
        this.pageSize = event.pageSize;
        this.search(event.page, true);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_2__["NhSuggestionComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_2__["NhSuggestionComponent"])
    ], ReceiverSuggestionComponent.prototype, "nhSuggestionComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ReceiverSuggestionComponent.prototype, "multiple", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ReceiverSuggestionComponent.prototype, "isReceipt", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ReceiverSuggestionComponent.prototype, "selectedItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ReceiverSuggestionComponent.prototype, "keyPressed", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ReceiverSuggestionComponent.prototype, "itemSelected", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ReceiverSuggestionComponent.prototype, "itemRemoved", void 0);
    ReceiverSuggestionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-receiver-suggestion',
            template: __webpack_require__(/*! ./receiver-suggestion.component.html */ "./src/app/modules/warehouse/goods/receiver-suggestion/receiver-suggestion.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_goods_delivery_note_goods_delivery_note_service__WEBPACK_IMPORTED_MODULE_5__["GoodsDeliveryNoteService"]])
    ], ReceiverSuggestionComponent);
    return ReceiverSuggestionComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_4__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/warehouse/goods/warehouse-manager-suggestion/warehouse-manager-suggestion.component.html":
/*!******************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/warehouse-manager-suggestion/warehouse-manager-suggestion.component.html ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-suggestion\r\n    [class.receipt]=\"isReceipt\"\r\n    [loading]=\"isSearching\"\r\n    [sources]=\"listItems\"\r\n    [selectedItem]=\"selectedItem\"\r\n    [totalRows]=\"totalRows\"\r\n    [pageSize]=\"10\"\r\n    [allowAdd]=\"false\"\r\n    (itemSelected)=\"onItemSelected($event)\"\r\n    (itemRemoved)=\"itemRemoved.emit($event)\"\r\n    (searched)=\"onSearchKeyPress($event)\"\r\n    (nextPage)=\"onNextPage($event)\"\r\n></nh-suggestion>\r\n"

/***/ }),

/***/ "./src/app/modules/warehouse/goods/warehouse-manager-suggestion/warehouse-manager-suggestion.component.ts":
/*!****************************************************************************************************************!*\
  !*** ./src/app/modules/warehouse/goods/warehouse-manager-suggestion/warehouse-manager-suggestion.component.ts ***!
  \****************************************************************************************************************/
/*! exports provided: WarehouseManagerSuggestionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WarehouseManagerSuggestionComponent", function() { return WarehouseManagerSuggestionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../shareds/components/nh-suggestion/nh-suggestion.component */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.component.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _warehouse_service_warehouse_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../warehouse/service/warehouse.service */ "./src/app/modules/warehouse/warehouse/service/warehouse.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");







var WarehouseManagerSuggestionComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](WarehouseManagerSuggestionComponent, _super);
    function WarehouseManagerSuggestionComponent(toastr, warehouseService) {
        var _this = _super.call(this) || this;
        _this.toastr = toastr;
        _this.warehouseService = warehouseService;
        _this.multiple = false;
        _this.isReceipt = false;
        _this.keyPressed = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.itemSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.itemRemoved = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        return _this;
    }
    WarehouseManagerSuggestionComponent.prototype.ngOnInit = function () {
    };
    WarehouseManagerSuggestionComponent.prototype.onItemSelected = function (item) {
        this.itemSelected.emit(item);
    };
    WarehouseManagerSuggestionComponent.prototype.onSearchKeyPress = function (keyword) {
        this.keyword = keyword;
        this.keyPressed.emit(keyword);
        this.search(1);
    };
    WarehouseManagerSuggestionComponent.prototype.search = function (currentPage, isLoadMore) {
        var _this = this;
        if (isLoadMore === void 0) { isLoadMore = false; }
        if (!this.warehouseId) {
            this.toastr.warning('Vui lòng chọn kho nhập.');
            return;
        }
        this.isSearching = true;
        this.currentPage = currentPage;
        this.warehouseService.managerSuggestion(this.warehouseId, this.keyword, this.currentPage, this.appConfig.PAGE_SIZE)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () { return _this.isSearching = false; }))
            .subscribe(function (result) {
            _this.totalRows = result.totalRows;
            if (isLoadMore) {
                _this.listItems = _this.listItems.concat(result.items);
            }
            else {
                _this.listItems = result.items;
            }
        });
    };
    WarehouseManagerSuggestionComponent.prototype.clear = function () {
        this.nhSuggestionComponent.clear();
    };
    WarehouseManagerSuggestionComponent.prototype.onNextPage = function (event) {
        this.keyword = event.keyword;
        this.pageSize = event.pageSize;
        this.search(event.page, true);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_3__["NhSuggestionComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_3__["NhSuggestionComponent"])
    ], WarehouseManagerSuggestionComponent.prototype, "nhSuggestionComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], WarehouseManagerSuggestionComponent.prototype, "multiple", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], WarehouseManagerSuggestionComponent.prototype, "isReceipt", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], WarehouseManagerSuggestionComponent.prototype, "warehouseId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], WarehouseManagerSuggestionComponent.prototype, "selectedItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], WarehouseManagerSuggestionComponent.prototype, "keyPressed", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], WarehouseManagerSuggestionComponent.prototype, "itemSelected", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], WarehouseManagerSuggestionComponent.prototype, "itemRemoved", void 0);
    WarehouseManagerSuggestionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-warehouse-manager-suggestion',
            template: __webpack_require__(/*! ./warehouse-manager-suggestion.component.html */ "./src/app/modules/warehouse/goods/warehouse-manager-suggestion/warehouse-manager-suggestion.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"],
            _warehouse_service_warehouse_service__WEBPACK_IMPORTED_MODULE_5__["WarehouseService"]])
    ], WarehouseManagerSuggestionComponent);
    return WarehouseManagerSuggestionComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/shareds/constants/deliveryType.const.ts":
/*!*********************************************************!*\
  !*** ./src/app/shareds/constants/deliveryType.const.ts ***!
  \*********************************************************/
/*! exports provided: DeliveryType, DeliveryTypes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeliveryType", function() { return DeliveryType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeliveryTypes", function() { return DeliveryTypes; });
var DeliveryType = {
    // Xuất bán lẻ.
    retail: 0,
    // Xuất sử dụng.
    selfConsumer: 1,
    // Xuất trả nhà cung cấp.
    return: 2,
    // Xuất hủy.
    voided: 3,
    // Xuất điều chuyển.
    transfer: 4,
    // Xuất kiểm kê.
    inventory: 5,
};
var DeliveryTypes = [{
        id: DeliveryType.retail,
        name: 'Xuất bán'
    }, {
        id: DeliveryType.selfConsumer,
        name: 'Xuất sử dụng'
    }, {
        id: DeliveryType.return,
        name: 'Xuất trả nhà cung cấp'
    }, {
        id: DeliveryType.voided,
        name: 'Xuất hủy'
    }, {
        id: DeliveryType.transfer,
        name: 'Xuất điều chuyển'
    }];


/***/ }),

/***/ "./src/app/shareds/pipe/format-money-string/format-money-string.module.ts":
/*!********************************************************************************!*\
  !*** ./src/app/shareds/pipe/format-money-string/format-money-string.module.ts ***!
  \********************************************************************************/
/*! exports provided: FormatMoneyStringModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormatMoneyStringModule", function() { return FormatMoneyStringModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _format_money_string_pipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./format-money-string.pipe */ "./src/app/shareds/pipe/format-money-string/format-money-string.pipe.ts");




var FormatMoneyStringModule = /** @class */ (function () {
    function FormatMoneyStringModule() {
    }
    FormatMoneyStringModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]],
            declarations: [_format_money_string_pipe__WEBPACK_IMPORTED_MODULE_3__["FormatMoneyStringPipe"]],
            exports: [_format_money_string_pipe__WEBPACK_IMPORTED_MODULE_3__["FormatMoneyStringPipe"]],
        })
    ], FormatMoneyStringModule);
    return FormatMoneyStringModule;
}());



/***/ }),

/***/ "./src/app/shareds/pipe/format-money-string/format-money-string.pipe.ts":
/*!******************************************************************************!*\
  !*** ./src/app/shareds/pipe/format-money-string/format-money-string.pipe.ts ***!
  \******************************************************************************/
/*! exports provided: FormatMoneyStringPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormatMoneyStringPipe", function() { return FormatMoneyStringPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FormatMoneyStringPipe = /** @class */ (function () {
    function FormatMoneyStringPipe() {
    }
    FormatMoneyStringPipe.prototype.transform = function (value, exponent) {
        return this.convertMoneyToString(value, '');
    };
    FormatMoneyStringPipe.prototype.convertMoneyToString = function (money, exponent) {
        var moneyString = this.formatMoney(money, exponent, ',', ',');
        var groups = moneyString.split(',');
        var groupLength = groups.length;
        var str = '';
        for (var i = groupLength; i > 0; i--) {
            var kq = this.groupThousand(groups[groupLength - i], groupLength === i, i === 1);
            str += str === '' ? kq + (kq === '' || kq === ' ' ? '' : (i === 6 ? ' triệu ' : i === 5 ? ' nghìn ' : i === 4 ?
                ' tỷ ' : i === 3 ? ' triệu ' : i === 2 ? ' nghìn ' : '')) :
                ' ' + kq + (kq === '' || kq === ' ' ? '' : (i === 6 ? ' triệu ' : i === 5 ? ' nghìn ' : i === 4 ?
                    ' tỷ ' : i === 3 ? ' triệu ' : i === 2 ? ' nghìn ' : ''));
        }
        return str + (moneyString.length >= 17 && groups[2] === '000' && groups[3] === '000' && groups[4] === '000' ? ' tỷ' : '');
    };
    FormatMoneyStringPipe.prototype.groupThousand = function (value, isFirst, isLast) {
        value = parseInt(value);
        // console.log(value);
        var tram = Math.floor(value / 100);
        var chuc = Math.floor((value - tram * 100) / 10);
        var donvi = (value - tram * 100 - chuc * 10);
        return (tram === 0 && isFirst || tram === 0 && chuc === 0 && donvi === 0 ? '' : this.numericToVietnamese(tram) + ' trăm ') +
            (chuc === 0 && donvi === 0 ? '' : (chuc === 0 && !isFirst || tram > 0 && chuc === 0 && isFirst ?
                'linh' : tram === 0 && chuc === 1 && donvi === 0 && !isFirst ? 'linh mười' : tram === 0 && chuc === 0 ? ''
                : chuc === 1 ? 'mười' : this.numericToVietnamese(chuc) + ' mươi') +
                (donvi === 0 ? '' : donvi === 1 && !isLast && !isFirst ? ' mốt' : ' ' + this.numericToVietnamese(donvi)));
    };
    FormatMoneyStringPipe.prototype.numericToVietnamese = function (number) {
        if (number === 0) {
            return 'không';
        }
        else if (number === 1) {
            return 'một';
        }
        else if (number === 2) {
            return 'hai';
        }
        else if (number === 3) {
            return 'ba';
        }
        else if (number === 4) {
            return 'bốn';
        }
        else if (number === 5) {
            return 'năm';
        }
        else if (number === 6) {
            return 'sáu';
        }
        else if (number === 7) {
            return 'bảy';
        }
        else if (number === 8) {
            return 'tám';
        }
        else if (number === 9) {
            return 'chín';
        }
    };
    FormatMoneyStringPipe.prototype.formatMoney = function (n, c, d, t) {
        c = isNaN(c = Math.abs(c)) ? 2 : c;
        d = d === undefined ? '.' : d;
        t = t === undefined ? ',' : t;
        var s = n < 0 ? '-' : '', i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c)));
        this.j = (this.j = i.length) > 3 ? this.j % 3 : 0;
        return s + (this.j ? i.substr(0, this.j) + t : '') + i.substr(this.j).replace(/(\d{3})(?=\d)/g, '$1' + t)
            + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
    };
    FormatMoneyStringPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({ name: 'formatMoneyString' })
    ], FormatMoneyStringPipe);
    return FormatMoneyStringPipe;
}());



/***/ }),

/***/ "./src/app/shareds/pipe/ghm-amount-to-word/ghm-amount-to-word.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/shareds/pipe/ghm-amount-to-word/ghm-amount-to-word.module.ts ***!
  \******************************************************************************/
/*! exports provided: GhmAmountToWordModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmAmountToWordModule", function() { return GhmAmountToWordModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ghm_amount_to_word_pipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ghm-amount-to-word.pipe */ "./src/app/shareds/pipe/ghm-amount-to-word/ghm-amount-to-word.pipe.ts");




var GhmAmountToWordModule = /** @class */ (function () {
    function GhmAmountToWordModule() {
    }
    GhmAmountToWordModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
            ],
            declarations: [_ghm_amount_to_word_pipe__WEBPACK_IMPORTED_MODULE_3__["GhmAmountToWordPipe"]],
            exports: [_ghm_amount_to_word_pipe__WEBPACK_IMPORTED_MODULE_3__["GhmAmountToWordPipe"]]
        })
    ], GhmAmountToWordModule);
    return GhmAmountToWordModule;
}());



/***/ }),

/***/ "./src/app/shareds/pipe/ghm-amount-to-word/ghm-amount-to-word.pipe.ts":
/*!****************************************************************************!*\
  !*** ./src/app/shareds/pipe/ghm-amount-to-word/ghm-amount-to-word.pipe.ts ***!
  \****************************************************************************/
/*! exports provided: GhmAmountToWordPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmAmountToWordPipe", function() { return GhmAmountToWordPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var GhmAmountToWordPipe = /** @class */ (function () {
    function GhmAmountToWordPipe() {
    }
    GhmAmountToWordPipe.prototype.transform = function (value, args) {
        if (!this.isInfinite(value)) {
            return null;
        }
        else {
            return this.generateWords(value);
        }
    };
    GhmAmountToWordPipe.prototype.isInfinite = function (value) {
        return !(typeof value !== 'number' || value !== value || value === Infinity || value === -Infinity);
    };
    GhmAmountToWordPipe.prototype.generateWords = function (number, isNegative) {
        if (isNegative === void 0) { isNegative = false; }
        // 1. Khởi tạo tham số
        var numberConvert = number.toString();
        var numbers = ['không', 'một', 'hai', 'ba', 'bốn', 'năm', 'sáu', 'bảy', 'tám', 'chín'];
        var currencyTypes = ['', 'nghìn', 'triệu', 'tỷ'];
        var i, j, unit, tens, hundred;
        var result = '';
        // 2. Xử lý
        i = numberConvert.length;
        if (i === 0) {
            result = numbers[0] + result;
        }
        else {
            j = 0;
            while (i > 0) {
                unit = +numberConvert.substring(i - 1, i);
                i--;
                if (i > 0) {
                    tens = +numberConvert.substring(i - 1, i);
                }
                else {
                    tens = -1;
                }
                i--;
                if (i > 0) {
                    hundred = +numberConvert.substring(i - 1, i);
                }
                else {
                    hundred = -1;
                }
                i--;
                if ((unit > 0) || (tens > 0) || (hundred > 0) || (j === 3)) {
                    result = currencyTypes[j] + result;
                }
                j++;
                if (j > 3) {
                    j = 1;
                }
                if ((unit === 1) && (tens === 1)) {
                    result = 'một ' + result;
                }
                else {
                    if (tens > 0) {
                        result = (unit === 1 ? 'mốt '
                            : unit === 5 ? 'lăm '
                                : unit > 0 ? numbers[unit] + ' ' : '') + result;
                    }
                    else {
                        result = (unit > 0 ? numbers[unit] + ' ' : ' ') + result;
                    }
                }
                if (tens >= 0) {
                    if ((tens === 0) && (unit > 0)) {
                        result = 'linh ' + result;
                    }
                    else if (tens === 1) {
                        result = 'mười ' + result;
                    }
                    else if (tens > 1) {
                        result = numbers[tens] + ' mươi ' + result;
                    }
                }
                if (hundred >= 0) {
                    if ((hundred > 0) || (tens > 0) || (unit > 0)) {
                        result = numbers[hundred] + ' trăm ' + result;
                    }
                }
                result = ' ' + result;
            }
        }
        // 3. Kiểm tra
        result = result.trim();
        if (isNegative) {
            result = 'Âm ' + result;
        }
        else if (result.length > 0) {
            var firstCharacter = result.substring(0, 1);
            var restCharacters = result.substring(1, result.length);
            result = firstCharacter.trim().toUpperCase() + restCharacters;
        }
        // 4. Trả lại chuỗi
        return result + ' đồng.';
    };
    GhmAmountToWordPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'ghmAmountToWord'
        })
    ], GhmAmountToWordPipe);
    return GhmAmountToWordPipe;
}());



/***/ }),

/***/ "./src/app/validators/conditional-required.validator.ts":
/*!**************************************************************!*\
  !*** ./src/app/validators/conditional-required.validator.ts ***!
  \**************************************************************/
/*! exports provided: conditionalRequiredValidator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "conditionalRequiredValidator", function() { return conditionalRequiredValidator; });
function conditionalRequiredValidator(condition) {
    return function (control) {
        return condition && !control.value ? { required: true } : null;
    };
}


/***/ })

}]);
//# sourceMappingURL=modules-warehouse-goods-goods-module.js.map