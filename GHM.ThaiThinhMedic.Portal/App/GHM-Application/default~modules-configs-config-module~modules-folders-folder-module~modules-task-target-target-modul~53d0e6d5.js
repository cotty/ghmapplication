(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~modules-configs-config-module~modules-folders-folder-module~modules-task-target-target-modul~53d0e6d5"],{

/***/ "./src/app/shareds/components/ghm-file-explorer/explorer-item.model.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-file-explorer/explorer-item.model.ts ***!
  \*****************************************************************************/
/*! exports provided: ExplorerItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExplorerItem", function() { return ExplorerItem; });
var ExplorerItem = /** @class */ (function () {
    function ExplorerItem(id, name, type, createTime, size, creatorId, creatorFullName, creatorAvatar, extension, url, absoluteUrl) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.size = size;
        this.sizeString = this.bytesToSize(size);
        this.createTime = createTime;
        this.creatorId = creatorId;
        this.creatorFullName = creatorFullName;
        this.creatorAvatar = creatorAvatar;
        this.extension = extension;
        this.isSelected = false;
        this.url = url;
        this.absoluteUrl = absoluteUrl;
        this.isImage = this.checkIsImage(extension);
    }
    ExplorerItem.prototype.bytesToSize = function (bytes) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes === 0) {
            return "0 " + sizes[0];
        }
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)).toString(), 10);
        if (i === 0) {
            return bytes + " " + sizes[i] + ")";
        }
        return (bytes / (Math.pow(1024, i))).toFixed(1) + " " + sizes[i];
    };
    ExplorerItem.prototype.checkIsImage = function (extension) {
        return ['png', 'jpg', 'jpeg', 'gif'].indexOf(extension) > -1;
    };
    return ExplorerItem;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<button type=\"button\" *ngIf=\"type === 'button'\"\r\n        class=\"btn btn-blue {{ buttonClass }}\"\r\n        [class.disabled]=\"isEnable\"\r\n        (click)=\"showExplorer()\">\r\n    {{ buttonText }}\r\n</button>\r\n<a class=\"text-decoration-none color-gray\" [class.disabled]=\"isEnable\" *ngIf=\"type === 'label'\" (click)=\"showExplorer()\">\r\n    {{ buttonText }}\r\n</a>\r\n\r\n<ng-template #ghmExplorerTemplate>\r\n    <div class=\"ghm-file-explorer-container\">\r\n        <div class=\"header\">\r\n            <h4 class=\"header-title uppercase gray bold\">{{ headerTitle }}</h4>\r\n            <svg\r\n                *ngIf=\"showCloseButton\"\r\n                (click)=\"closeModal()\"\r\n\r\n                width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" focusable=\"false\"\r\n                role=\"presentation\"\r\n                class=\"btn-close\">\r\n                <path\r\n                    d=\"M12 10.586L6.707 5.293a1 1 0 0 0-1.414 1.414L10.586 12l-5.293 5.293a1 1 0 0 0 1.414 1.414L12 13.414l5.293 5.293a1 1 0 0 0 1.414-1.414L13.414 12l5.293-5.293a1 1 0 1 0-1.414-1.414L12 10.586z\"\r\n                    fill=\"currentColor\">\r\n                </path>\r\n            </svg>\r\n        </div><!-- END: .header -->\r\n        <div class=\"actions\">\r\n            <ul class=\"breadcrumb\">\r\n                <li>\r\n                    <a href=\"javascript://\" (click)=\"showDirectory()\" i18n=\"@@myDriver\">My driver</a>\r\n                </li>\r\n                <li *ngFor=\"let item of breadcrumbs\">\r\n                    <a href=\"javascript://\" (click)=\"showDirectory(item)\">{{ item.name }}</a>\r\n                </li>\r\n            </ul><!-- END: .breadcrumb -->\r\n            <ghm-file-upload\r\n                [folderId]=\"currentFolderId\"\r\n            ></ghm-file-upload>\r\n            <div>\r\n                <button type=\"button\" class=\"btn btn-light\" i18n=\"@@newFolder\" (click)=\"createNewFolder()\">\r\n                    <i class=\"fa fa-folder cm-mgr-5\"></i>\r\n                    <span i81n=\"@@newFolder\">New folder</span>\r\n                </button>\r\n                <button type=\"button\" class=\"btn btn-light\" (click)=\"isGridView = !isGridView\">\r\n                    <i class=\"fa fa-list\" *ngIf=\"isGridView\"></i>\r\n                    <i class=\"fa fa-th-large\" *ngIf=\"!isGridView\"></i>\r\n                </button>\r\n            </div>\r\n        </div><!-- END: .actions -->\r\n        <div class=\"content\">\r\n            <div class=\"list-ghm-fe\"\r\n                 [class.list]=\"!isGridView\"\r\n                 [class.grid]=\"isGridView\">\r\n                <div *ngIf=\"explorerItems.length === 0; else listItemTemplate\" i18n=\"@@folderIsEmpty\">\r\n                    Folder is empty.\r\n                </div>\r\n                <ng-template #listItemTemplate>\r\n                    <div class=\"ghm-fe-item\" *ngFor=\"let item of explorerItems\"\r\n                         (click)=\"selectItem(item)\"\r\n                         (dblclick)=\"openItem(item)\">\r\n                        <div class=\"ghm-fe-item-prop ghm-fe-item-select\" *ngIf=\"multiple && !isGridView\">\r\n                            <mat-checkbox color=\"primary\" [checked]=\"item.isSelected\"\r\n                                          (change)=\"changeSelectedStatus(item)\"></mat-checkbox>\r\n                        </div>\r\n                        <div class=\"ghm-fe-item-prop\">\r\n                            <div class=\"ghm-fe-item-content\"\r\n                                 [class.selected]=\"item.isSelected\">\r\n                                <div class=\"ghm-fe-item-icon\">\r\n                                    <img src=\"{{ item.absoluteUrl }}\" *ngIf=\"item.isImage; else fileTemplate\">\r\n                                    <ng-template #fileTemplate>\r\n                                        <i class=\"icon icon-{{ item.extension }}\"></i>\r\n                                    </ng-template>\r\n                                </div><!-- END: .ghm-fe-item-icon -->\r\n                                <div class=\"ghm-fe-name\">\r\n                                    {{ item.name}}\r\n                                </div><!-- END: .ghm-fe-item-name -->\r\n                            </div><!-- END: .ghm-fe-item-content -->\r\n                        </div><!-- END: .ghm-fe-item-prop -->\r\n                        <div class=\"ghm-fe-item-prop ghm-fe-owner\">\r\n                            {{ item.creatorFullName }}\r\n                        </div><!-- END: .ghm-fe-owner -->\r\n                        <div class=\"ghm-fe-item-prop ghm-fe-lu\">\r\n                            <div class=\"ghm-fe-lu-item\">\r\n                                <div class=\"ghm-fe-lu-date\">{{ item.createTime | dateTimeFormat:'DD/MM/YYYY' }}</div>\r\n                                <div class=\"ghm-fe-lu-u\">Tôi</div>\r\n                            </div>\r\n                        </div><!-- END: .ghm-fe-last-update -->\r\n                        <div class=\"ghm-fe-item-prop ghm-fe-size\">\r\n                            {{ item.sizeString }}\r\n                        </div>\r\n                    </div><!-- END: .ghm-file-explorer-item -->\r\n                </ng-template>\r\n            </div><!-- END: .wrapper-list-items -->\r\n        </div><!-- END: .content -->\r\n        <div class=\"footer {{ footerClass }}\">\r\n            <button type=\"button\" class=\"btn btn-blue primary\" (click)=\"confirmSelect()\"\r\n                    *ngIf=\"multiple\" [class.disabled]=\"!isMultipleSelected\">\r\n                {{ confirmText }}\r\n            </button>\r\n\r\n            <button type=\"button\" class=\"btn btn-light btn-close\" (click)=\"closeModal()\">\r\n                {{ closeText }}\r\n            </button>\r\n        </div><!-- END: .footer -->\r\n    </div><!-- END: .ghm-file-explorer-container -->\r\n</ng-template>\r\n\r\n<ghm-new-folder\r\n    [parentId]=\"currentFolderId\"\r\n    (saveSuccessful)=\"onSaveFolderSuccessful($event)\"></ghm-new-folder>\r\n"

/***/ }),

/***/ "./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn {\n  display: inline-block;\n  font-weight: 400;\n  text-align: center;\n  white-space: nowrap;\n  vertical-align: middle;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  border: 1px solid transparent;\n  padding: 6px 12px;\n  font-size: 14px;\n  line-height: 1.5;\n  transition: color .15s ease-in-out,\r background-color .15s ease-in-out,\r border-color .15s ease-in-out,\r box-shadow .15s ease-in-out;\n  text-rendering: auto;\n  color: initial;\n  letter-spacing: normal;\n  word-spacing: normal;\n  text-transform: none;\n  text-indent: 0px;\n  text-shadow: none;\n  display: inline-block;\n  text-align: start;\n  margin: 0em;\n  color: #fff; }\n  .btn.btn-blue {\n    background: #007bff;\n    border-color: #007bff; }\n  .btn.btn-blue:active, .btn.btn-blue:focus {\n      box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.5);\n      border-color: #005cbf;\n      background-color: #0062cc; }\n  .btn.btn-blue:hover {\n      background-color: #0069d9;\n      border-color: #0062cc; }\n  .btn.btn-light {\n    color: #212529;\n    background-color: #f8f9fa;\n    border-color: #f8f9fa; }\n  .btn.btn-light:hover {\n      color: #212529;\n      background-color: #e2e6ea;\n      border-color: #dae0e5; }\n  .ghm-file-explorer-container {\n  background: #fff;\n  display: flex;\n  flex-flow: row wrap;\n  border-radius: 5px !important;\n  border: 1px solid rgba(0, 0, 0, 0.2);\n  outline: none; }\n  .ghm-file-explorer-container > * {\n    flex: 1 100%; }\n  .ghm-file-explorer-container .header {\n    order: 1;\n    display: flex;\n    align-self: center;\n    align-items: center; }\n  .ghm-file-explorer-container .header .header-title {\n      flex: 50 auto; }\n  .ghm-file-explorer-container .header .header-title .uppercase {\n        text-transform: uppercase; }\n  .ghm-file-explorer-container .header .header-title .bold {\n        font-weight: bold; }\n  .ghm-file-explorer-container .header .header-title .gray {\n        color: #6c757d; }\n  .ghm-file-explorer-container .header svg.btn-close {\n      flex: 1 auto;\n      text-align: right; }\n  .ghm-file-explorer-container .actions {\n    border-bottom: 1px solid #ddd;\n    order: 2;\n    display: flex;\n    justify-content: flex-end; }\n  .ghm-file-explorer-container .actions button {\n      margin-left: 5px; }\n  .ghm-file-explorer-container .header, .ghm-file-explorer-container .footer, .ghm-file-explorer-container .actions {\n    padding: 7px 15px; }\n  .ghm-file-explorer-container .sidebar, .ghm-file-explorer-container .content {\n    height: 450px;\n    overflow-y: auto; }\n  .ghm-file-explorer-container .sidebar {\n    order: 3; }\n  .ghm-file-explorer-container .content {\n    order: 4;\n    padding: 10px;\n    overflow: auto; }\n  .ghm-file-explorer-container .content .list-ghm-fe {\n      /* --- List view ---*/\n      /* --- END: list view ---*/\n      /* --- Grid view --- */\n      /* --- END: Grid view --- */ }\n  .ghm-file-explorer-container .content .list-ghm-fe.list {\n        display: table;\n        width: 100%;\n        -webkit-user-select: none;\n        -moz-user-select: none;\n        -ms-user-select: none;\n        user-select: none; }\n  .ghm-file-explorer-container .content .list-ghm-fe.list .ghm-fe-item {\n          display: table-row;\n          height: 40px;\n          margin-left: 16px;\n          margin-right: 16px;\n          padding: 0;\n          position: relative;\n          width: 24px;\n          -webkit-user-select: none;\n          -moz-user-select: none;\n          -ms-user-select: none;\n          user-select: none; }\n  .ghm-file-explorer-container .content .list-ghm-fe.list .ghm-fe-item:hover, .ghm-file-explorer-container .content .list-ghm-fe.list .ghm-fe-item.selected {\n            color: #007bff;\n            background: #f8f9fa;\n            cursor: pointer; }\n  .ghm-file-explorer-container .content .list-ghm-fe.list .ghm-fe-item .ghm-fe-item-content {\n            display: flex; }\n  .ghm-file-explorer-container .content .list-ghm-fe.list .ghm-fe-item .ghm-fe-item-content > div {\n              flex: 1; }\n  .ghm-file-explorer-container .content .list-ghm-fe.list .ghm-fe-item .ghm-fe-item-content > div.ghm-fe-name {\n                flex: 3; }\n  .ghm-file-explorer-container .content .list-ghm-fe.list .ghm-fe-item .ghm-fe-item-prop {\n            display: table-cell;\n            vertical-align: middle;\n            border-bottom: 1px solid #ddd;\n            -webkit-user-select: none;\n            -moz-user-select: none;\n            -ms-user-select: none;\n            user-select: none; }\n  .ghm-file-explorer-container .content .list-ghm-fe.list .ghm-fe-item .ghm-fe-item-prop .ghm-fe-item-icon {\n              padding-top: 2px;\n              width: 40px; }\n  .ghm-file-explorer-container .content .list-ghm-fe.list .ghm-fe-item .ghm-fe-item-prop .ghm-fe-item-icon img {\n                width: 30px !important;\n                margin-right: 5px; }\n  .ghm-file-explorer-container .content .list-ghm-fe.list .ghm-fe-item .ghm-fe-item-prop .ghm-fe-lu-item {\n              display: flex; }\n  .ghm-file-explorer-container .content .list-ghm-fe.grid {\n        display: flex;\n        flex-wrap: wrap; }\n  .ghm-file-explorer-container .content .list-ghm-fe.grid .ghm-fe-owner, .ghm-file-explorer-container .content .list-ghm-fe.grid .ghm-fe-lu, .ghm-file-explorer-container .content .list-ghm-fe.grid .ghm-fe-size {\n          display: none; }\n  .ghm-file-explorer-container .content .list-ghm-fe.grid .ghm-fe-item {\n          white-space: nowrap;\n          overflow: hidden;\n          text-overflow: ellipsis; }\n  .ghm-file-explorer-container .content .list-ghm-fe.grid .ghm-fe-item:hover {\n            cursor: pointer; }\n  .ghm-file-explorer-container .content .list-ghm-fe.grid .ghm-fe-item .ghm-fe-item-content {\n            display: flex;\n            flex-wrap: wrap;\n            border: 1px solid #ddd;\n            margin: 10px 10px 0 0;\n            justify-items: center;\n            align-items: center; }\n  .ghm-file-explorer-container .content .list-ghm-fe.grid .ghm-fe-item .ghm-fe-item-content.selected {\n              border: 1px solid #007bff; }\n  .ghm-file-explorer-container .content .list-ghm-fe.grid .ghm-fe-item .ghm-fe-item-content > div {\n              flex: 1 1 auto; }\n  .ghm-file-explorer-container .content .list-ghm-fe.grid .ghm-fe-item .ghm-fe-item-content > div.ghm-fe-name {\n                border-top: 1px solid #ddd;\n                padding: 7px 15px; }\n  .ghm-file-explorer-container .content .list-ghm-fe.grid .ghm-fe-item .ghm-fe-item-icon {\n            display: flex;\n            width: 100%;\n            text-align: center;\n            align-items: center;\n            height: 188px;\n            overflow: hidden; }\n  .ghm-file-explorer-container .content .list-ghm-fe.grid .ghm-fe-item .ghm-fe-item-icon i {\n              font-size: 80px;\n              margin: 0 auto; }\n  .ghm-file-explorer-container .content .list-ghm-fe.grid .ghm-fe-item .ghm-fe-item-icon img {\n              width: 100%; }\n  .ghm-file-explorer-container .content .list-ghm-fe.grid .ghm-fe-item .ghm-fe-item-icon div.ghm-fe-name {\n              text-align: left;\n              background: #6c757d;\n              overflow: hidden;\n              text-overflow: ellipsis;\n              white-space: nowrap; }\n  .ghm-file-explorer-container .footer {\n    order: 5;\n    display: flex;\n    flex-wrap: wrap;\n    justify-content: flex-end;\n    align-items: center; }\n  .ghm-file-explorer-container .footer button {\n      margin-left: 5px; }\n  i.icon {\n  display: inline-block;\n  line-height: 14px;\n  font-family: \"FontAwesome\";\n  font-style: normal;\n  font-size: inherit;\n  text-rendering: auto;\n  -webkit-font-smoothing: antialiased; }\n  i.icon.icon-folder:before {\n    content: '\\f07b';\n    color: #6c757d; }\n  i.icon.icon-xls, i.icon.icon-xlsx {\n    color: forestgreen; }\n  i.icon.icon-xls::before, i.icon.icon-xlsx::before {\n      content: \"\\f1c3\"; }\n  i.icon.icon-doc, i.icon.icon-docx {\n    color: cornflowerblue; }\n  i.icon.icon-doc:before, i.icon.icon-docx:before {\n      content: \"\\f1c2\"; }\n  i.icon.icon-txt:before {\n    content: \"\\f0f6\"; }\n  i.icon.icon-pptx {\n    color: #e74c3c; }\n  i.icon.icon-pptx:before {\n      content: \"\\f1c4\"; }\n  i.icon.icon-pdf {\n    color: #c0392b; }\n  i.icon.icon-pdf::before {\n      content: \"\\f1c1\"; }\n  @media all and (max-width: 480px) {\n  .ghm-file-explorer-container .header {\n    order: 1; }\n  .ghm-file-explorer-container .actions {\n    order: 2; }\n  .ghm-file-explorer-container .content {\n    order: 3; }\n  .ghm-file-explorer-container .footer {\n    order: 4; }\n  .ghm-file-explorer-container .content .list-ghm-fe {\n    justify-content: flex-start; }\n    .ghm-file-explorer-container .content .list-ghm-fe.grid .ghm-fe-item {\n      flex-basis: 50%; }\n    .ghm-file-explorer-container .content .list-ghm-fe.list .ghm-fe-lu, .ghm-file-explorer-container .content .list-ghm-fe.list .ghm-fe-size {\n      display: none !important; } }\n  @media all and (min-width: 480px) and (max-width: 768px) {\n  .ghm-file-explorer-container .content .list-ghm-fe {\n    justify-content: flex-start; }\n    .ghm-file-explorer-container .content .list-ghm-fe.grid .ghm-fe-item {\n      flex-basis: 33%; } }\n  @media all and (min-width: 768px) {\n  .ghm-file-explorer-container {\n    width: 888px; }\n    .ghm-file-explorer-container .header {\n      border-bottom: 1px solid #ddd; }\n    .ghm-file-explorer-container .sidebar {\n      flex: 1 auto;\n      border-right: 1px solid #ddd;\n      order: 3; }\n    .ghm-file-explorer-container .content {\n      order: 3;\n      flex: 4 auto; }\n      .ghm-file-explorer-container .content .list-ghm-fe.grid .ghm-fe-item {\n        flex-basis: 25%; }\n    .ghm-file-explorer-container .footer {\n      order: 5;\n      border-top: 1px solid #ddd; } }\n  /* END: .ghm-file-explorer-container*/\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL2dobS1maWxlLWV4cGxvcmVyL0Q6XFxQcm9qZWN0XFxHaG1BcHBsaWNhdGlvblxcY2xpZW50c1xcZ2htYXBwbGljYXRpb25jbGllbnQvc3JjXFxhcHBcXHNoYXJlZHNcXGNvbXBvbmVudHNcXGdobS1maWxlLWV4cGxvcmVyXFxnaG0tZmlsZS1leHBsb3Jlci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL2dobS1maWxlLWV4cGxvcmVyL2dobS1maWxlLWV4cGxvcmVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQXlCQTtFQUNJLHFCQUFxQjtFQUNyQixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixzQkFBc0I7RUFDdEIseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0QixxQkFBcUI7RUFDckIsaUJBQWlCO0VBQ2pCLDZCQUE2QjtFQUM3QixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixvSUFNc0I7RUFDdEIsb0JBQW9CO0VBQ3BCLGNBQWM7RUFDZCxzQkFBZ0I7RUFDaEIsb0JBQWlCO0VBQ2pCLG9CQUFTO0VBQ1QsZ0JBQVk7RUFDWixpQkFBVztFQUNYLHFCQTZCSDtFQXpERCxpQkE4QmU7RUM1QmIsV0Q2Qk07RUM1Qk4sV0Q2Qk0sRUFBQTtFQzVCTjtJQUNFLG1CRDhCeUI7SUM3QnpCLHFCRDdCb0IsRUFBTztFQzhCM0I7TURQSiwrQ0F3Q2dCO01BQ0oscUJBQWtCO01BQ2xCLHlCQUNILEVBQUE7RUEzQ1Q7TUErQ1EseUJBQWM7TUFDZCxxQkFuREEsRUFBTztFQ2lCYjtJRGRGLGNBOENlO0lDOUJYLHlCRG9Dc0I7SUNuQ3RCLHFCRG9DMEIsRUFBQTtFQ25DMUI7TUFDRSxjQUFjO01Ed0NwQix5QkFBNkI7TUFDekIscUJBQWdCLEVBQUE7RUNyQ3BCO0VEd0NJLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFlO0VBTm5CLDZCQUE0QjtFQzdCMUIsb0NEeUNHO0VBWkwsYUFBQSxFQUFBO0VDM0JFO0lEMkNNLFlBQVMsRUFBSTtFQ3pDbkI7SUQyQ00sUUFBQTtJQWxCUixhQUFBO0lDdEJJLGtCRHdESztJQ3ZETCxtQkRxQkosRUFBQTtFQ3BCSTtNRG9CSixhQUFBLEVBQUE7RUNsQk07UURrQk4seUJBQUEsRUFjSTtFQzlCRTtRRGdCTixpQkFBQSxFQUFBO0VBcUNZO1FBQ0EsY0FBWSxFQUFLO0VBdEM3QjtNQTJDUSxZQUFhO01BQ2IsaUJBQVEsRUFBQTtFQ3JEZDtJRHVETSw2QkFLSDtJQW5ETCxRQUFBO0lDTkksYUR1RG1CO0lBakR2Qix5QkFBQSxFQXFESTtFQUNJO01BdERSLGdCQUFBLEVBQUE7RUNGRTtJRDZETSxpQkFBZ0IsRUFDbkI7RUE1REw7SUErRFEsYUFFSDtJQWpFTCxnQkFBQSxFQUFBO0VDR0U7SURrRU0sUUFBTyxFQUFFO0VDaEVmO0lETEYsUUFBQTtJQ09JLGFEa0VRO0lDakVSLGNEa0lRLEVBQUE7RUNqSVI7TURzTVEscUJBQUE7TUEvTVosMEJBQUE7TUNZTSxzQkQrRHdCO01DOUR4QiwyQkQrRHFCLEVBQUE7RUM5RHJCO1FEZ0VVLGNBQUE7UUFDQSxXQUFBO1FBQ0EseUJBeURIO1FBekliLHNCQUFBO1FDbUJRLHFCRGdFOEI7UUMvRDlCLGlCRGdFd0IsRUFBQTtFQy9EeEI7VURpRVksa0JBQWtCO1VBQ2xCLFlBQVU7VUFDVixpQkFBVTtVQUNWLGtCQUFXO1VBQ1gsVUFBQTtVQUNBLGtCQUFrQjtVQUNsQixXQUFBO1VBQ0EseUJBMkNIO1VBeElqQixzQkFBQTtVQytCVSxxQkRuR087VUNvR1AsaUJEOUZGLEVBQUE7RUMrRkU7WURqQ1YsY0FBQTtZQXNHd0IsbUJBU0g7WUEvR3JCLGVBQUEsRUFBQTtFQ3FDVTtZQUNFLGFEdENaLEVBQUE7RUN1Q1k7Y0R2Q1osT0FBQSxFQUFBO0VBa0h3QjtnQkFDQSxPQUFBLEVBQUE7RUN4RWQ7WUQwRWMsbUJBQW1CO1lBQ25CLHNCQUFzQjtZQUN0Qiw2QkFBcUI7WUFDckIseUJBZUg7WUF2SXJCLHNCQUFBO1lDaURZLHFCRDBFZ0M7WUN6RWhDLGlCRGdGYSxFQUFBO0VDL0ViO2NBQ0UsZ0JEMkV5QjtjQzFFekIsV0QyRWtCLEVBQUE7RUFoSWhDO2dCQXFJNEIsc0JBQ0g7Z0JBdEl6QixpQkFBQSxFQW1FSTtFQTJFWTtjQUNBLGFBK0RILEVBQUE7RUNuSlA7UUFDRSxhRHNGcUI7UUFsSjdCLGVBQUEsRUFBQTtFQzhEUTtVRHlGWSxhQUFVLEVBQUE7RUN2RnRCO1VEaEVSLG1CQUFBO1VDa0VVLGdCRHlGc0I7VUEzSmhDLHVCQUFBLEVBQUE7RUNvRVU7WUQ0RmMsZUFBZSxFQUFBO0VDMUY3QjtZRDRGYyxhQUFRO1lBQ1IsZUFBZTtZQUNmLHNCQWNIO1lBbExyQixxQkFBQTtZQzJFWSxxQkQ5Skw7WUFtRlAsbUJBQUEsRUFBQTtFQzZFWTtjRDdFWix5QkFBQSxFQUFBO0VDK0VZO2NBQ0UsY0QrRjJCLEVBQUE7RUEvS3pDO2dCQXFMd0IsMEJBQWE7Z0JBQ2IsaUJBQVcsRUFBQTtFQ2xHekI7WURvR2MsYUFBYTtZQUNiLFdBQVE7WUFDUixrQkFBZ0I7WUExTHhDLG1CQUFBO1lDeUZZLGFEb0cyQjtZQ25HM0IsZ0JEb0c4QixFQUFBO0VBOUwxQztjQWtNNEIsZUFDSDtjQW5NekIsY0FBQSxFQUFBO0VDOEZZO2NEeUdnQixXQUFVLEVBN1EvQjtFQ3NLSztjRHlHZ0IsZ0JBQWU7Y0FDZixtQkFBbUI7Y0ExTS9DLGdCQUE0QjtjQW9OWix1QkFBQTtjQUNDLG1CQUFJLEVBQUE7RUMvR25CO0lEaUhNLFFBQUE7SUFDQSxhQUFhO0lBeE5yQixlQUFBO0lDMEdJLHlCRGtISztJQ2pITCxtQkFBbUIsRUFBRTtFRHFIeEI7TUFDRyxnQkFBUyxFQUFZO0VDbEh6QjtFRHFISSxxQkFBa0I7RUFDbEIsaUJBQVc7RUFDWCwwQkFBb0I7RUFDcEIsa0JBQUE7RUFQSixrQkFTaUI7RUNwSGYsb0JEcUhzQjtFQ3BIdEIsbUNEc0hHLEVBQUE7RUFaTDtJQWtCUSxnQkFBTztJQWxCZixjQUFNLEVBY1E7RUNwSFo7SURzR0Qsa0JBQUEsRUFBQTtFQXdCTztNQXhCUCxnQkFvQmEsRUFBQTtFQ3RIWjtJRGtHRCxxQkEwQnFCLEVBQUE7RUFDZDtNQTNCUCxnQkE2QmUsRUFBQTtFQzNIZDtJRDhGRixnQkE2QmUsRUFBQTtFQ3pIYjtJRDRGRCxjQW1DYyxFQUFBO0VBSVA7TUF2Q1AsZ0JBbUNhLEVBQUE7RUMzSFo7SUFDRSxjQUFjLEVBQUU7RURrSXBCO01BQ0ksZ0JBQUEsRUFBQTtFQy9ISjtFQUNFO0lEOEhFLFFBQUEsRUFBQTtFQzVIRjtJRDRIRSxRQUFBLEVBQUE7RUMxSEY7SUQwSEUsUUFBQSxFQUFBO0VDeEhGO0lEd0hFLFFBQUEsRUFBQTtFQ3RIRjtJRHNIRSwyQkFBQSxFQWNJO0lDbElKO01BQ0UsZUFBZSxFQUFFO0lEc0p2QjtNQUNJLHdCQUE0QixFQUN4QixFQUFBO0VDcEpSO0VBQ0U7SUFDRSwyQkFBMkIsRUFBRTtJRGdLakM7TUFDSSxlQUFBLEVBQUEsRUFBQTtFQzdKSjtFQUNFO0lENEpFLFlBQUEsRUFBQTtJQzFKQTtNRG1LUSw2QkE3Wk0sRUFBQTtJQzRQZDtNRHdKQSxZQUFBO01BY1EsNEJBQVE7TUFDUixRQUFNLEVBQUE7SUNuS2Q7TUFDRSxRRHVLa0I7TUFwQnBCLFlBQUEsRUFBQTtNQTJCUTtRQUNBLGVBQWdCLEVBQUE7SUMzS3hCO01EZ0xKLFFBQUE7TUM5S00sMEJBQTBCLEVBQUUsRUFBRTtFQUVwQyxxQ0FBcUMiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWRzL2NvbXBvbmVudHMvZ2htLWZpbGUtZXhwbG9yZXIvZ2htLWZpbGUtZXhwbG9yZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIkYm9yZGVyQ29sb3I6ICNkZGQ7XHJcbiRibHVlOiAjMDA3YmZmO1xyXG4kYmx1ZUFjdGl2ZUJvcmRlckNvbG9yOiAjMDA1Y2JmO1xyXG4kYmx1ZUFjdGl2ZUJhY2tncm91bmRDb2xvcjogIzAwNjJjYztcclxuJGluZGlnbzogIzY2MTBmMjtcclxuJHB1cnBsZTogIzZmNDJjMTtcclxuJHBpbms6ICNlODNlOGM7XHJcbiRyZWQ6ICNkYzM1NDU7XHJcbiRvcmFuZ2U6ICNmZDdlMTQ7XHJcbiR5ZWxsb3c6ICNmZmMxMDc7XHJcbiRncmVlbjogIzI4YTc0NTtcclxuJHRlYWw6ICMyMGM5OTc7XHJcbiRjeWFuOiAjMTdhMmI4O1xyXG4kd2hpdGU6ICNmZmY7XHJcbiRncmF5OiAjNmM3NTdkO1xyXG4kZ3JheS1kYXJrOiAjMzQzYTQwO1xyXG4kcHJpbWFyeTogIzAwN2JmZjtcclxuJHNlY29uZGFyeTogIzZjNzU3ZDtcclxuJHN1Y2Nlc3M6ICMyOGE3NDU7XHJcbiRpbmZvOiAjMTdhMmI4O1xyXG4kd2FybmluZzogI2ZmYzEwNztcclxuJGRhbmdlcjogI2RjMzU0NTtcclxuJGxpZ2h0OiAjZjhmOWZhO1xyXG4kZGFyazogIzM0M2E0MDtcclxuXHJcbi5idG4ge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gICAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcclxuICAgIC1tb3otdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICAtbXMtdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICB1c2VyLXNlbGVjdDogbm9uZTtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHRyYW5zcGFyZW50O1xyXG4gICAgcGFkZGluZzogNnB4IDEycHg7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBsaW5lLWhlaWdodDogMS41O1xyXG4gICAgdHJhbnNpdGlvbjogY29sb3IgLjE1cyBlYXNlLWluLW91dCxcclxuICAgIGJhY2tncm91bmQtY29sb3IgLjE1cyBlYXNlLWluLW91dCxcclxuICAgIGJvcmRlci1jb2xvciAuMTVzIGVhc2UtaW4tb3V0LFxyXG4gICAgYm94LXNoYWRvdyAuMTVzIGVhc2UtaW4tb3V0O1xyXG4gICAgdGV4dC1yZW5kZXJpbmc6IGF1dG87XHJcbiAgICBjb2xvcjogaW5pdGlhbDtcclxuICAgIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XHJcbiAgICB3b3JkLXNwYWNpbmc6IG5vcm1hbDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gICAgdGV4dC1pbmRlbnQ6IDBweDtcclxuICAgIHRleHQtc2hhZG93OiBub25lO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgdGV4dC1hbGlnbjogc3RhcnQ7XHJcbiAgICBtYXJnaW46IDBlbTtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG5cclxuICAgICYuYnRuLWJsdWUge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICRibHVlO1xyXG4gICAgICAgIGJvcmRlci1jb2xvcjogJGJsdWU7XHJcblxyXG4gICAgICAgICY6YWN0aXZlLCAmOmZvY3VzIHtcclxuICAgICAgICAgICAgYm94LXNoYWRvdzogMCAwIDAgMC4ycmVtIHJnYmEoMCwgMTIzLCAyNTUsIC41KTtcclxuICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiAkYmx1ZUFjdGl2ZUJvcmRlckNvbG9yO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkYmx1ZUFjdGl2ZUJhY2tncm91bmRDb2xvcjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDA2OWQ5O1xyXG4gICAgICAgICAgICBib3JkZXItY29sb3I6ICRibHVlQWN0aXZlQmFja2dyb3VuZENvbG9yO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAmLmJ0bi1saWdodCB7XHJcbiAgICAgICAgY29sb3I6ICMyMTI1Mjk7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGxpZ2h0O1xyXG4gICAgICAgIGJvcmRlci1jb2xvcjogJGxpZ2h0O1xyXG5cclxuICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgY29sb3I6ICMyMTI1Mjk7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlMmU2ZWE7XHJcbiAgICAgICAgICAgIGJvcmRlci1jb2xvcjogI2RhZTBlNTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIge1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWZsb3c6IHJvdyB3cmFwO1xyXG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA1cHggIWltcG9ydGFudDtcclxuICAgIC1tb3otYm9yZGVyLXJhZGl1czogNXB4ICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHggIWltcG9ydGFudDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgLjIpO1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuXHJcbiAgICAmID4gKiB7XHJcbiAgICAgICAgZmxleDogMSAxMDAlO1xyXG4gICAgfVxyXG5cclxuICAgIC5oZWFkZXIge1xyXG4gICAgICAgIG9yZGVyOiAxO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblxyXG4gICAgICAgIC5oZWFkZXItdGl0bGUge1xyXG4gICAgICAgICAgICBmbGV4OiA1MCBhdXRvO1xyXG5cclxuICAgICAgICAgICAgLnVwcGVyY2FzZSB7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAuYm9sZCB7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLmdyYXkge1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICRncmF5O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBzdmcuYnRuLWNsb3NlIHtcclxuICAgICAgICAgICAgZmxleDogMSBhdXRvO1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmFjdGlvbnMge1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAkYm9yZGVyQ29sb3I7XHJcbiAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG5cclxuICAgICAgICBidXR0b24ge1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuaGVhZGVyLCAuZm9vdGVyLCAuYWN0aW9ucyB7XHJcbiAgICAgICAgcGFkZGluZzogN3B4IDE1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLnNpZGViYXIsIC5jb250ZW50IHtcclxuICAgICAgICBoZWlnaHQ6IDQ1MHB4O1xyXG4gICAgICAgIG92ZXJmbG93LXk6IGF1dG87XHJcbiAgICB9XHJcblxyXG4gICAgLnNpZGViYXIge1xyXG4gICAgICAgIG9yZGVyOiAzO1xyXG4gICAgICAgIC8vb3ZlcmZsb3cteDogYXV0bztcclxuICAgIH1cclxuXHJcbiAgICAuY29udGVudCB7XHJcbiAgICAgICAgb3JkZXI6IDQ7XHJcbiAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICBvdmVyZmxvdzogYXV0bztcclxuXHJcbiAgICAgICAgLmxpc3QtZ2htLWZlIHtcclxuICAgICAgICAgICAgLyogLS0tIExpc3QgdmlldyAtLS0qL1xyXG4gICAgICAgICAgICAmLmxpc3Qge1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogdGFibGU7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICAtbW96LXVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgICAgICAgICAgICAgLW1zLXVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgICAgICAgICAgICAgdXNlci1zZWxlY3Q6IG5vbmU7XHJcblxyXG4gICAgICAgICAgICAgICAgJiAuZ2htLWZlLWl0ZW0ge1xyXG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IHRhYmxlLXJvdztcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDE2cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxNnB4O1xyXG4gICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAyNHB4O1xyXG4gICAgICAgICAgICAgICAgICAgIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICAgICAgLW1vei11c2VyLXNlbGVjdDogbm9uZTtcclxuICAgICAgICAgICAgICAgICAgICAtbXMtdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICAgICAgdXNlci1zZWxlY3Q6IG5vbmU7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICY6aG92ZXIsICYuc2VsZWN0ZWQge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogJHByaW1hcnk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICRsaWdodDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLmdobS1mZS1pdGVtLWNvbnRlbnQge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgJiA+IGRpdiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmbGV4OiAxO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICYuZ2htLWZlLW5hbWUge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZsZXg6IDM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC5naG0tZmUtaXRlbS1wcm9wIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogdGFibGUtY2VsbDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICRib3JkZXJDb2xvcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLW1vei11c2VyLXNlbGVjdDogbm9uZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLW1zLXVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1c2VyLXNlbGVjdDogbm9uZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5naG0tZmUtaXRlbS1pY29uIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmctdG9wOiAycHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogNDBweDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAzMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5naG0tZmUtbHUtaXRlbSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8qIC0tLSBFTkQ6IGxpc3QgdmlldyAtLS0qL1xyXG5cclxuICAgICAgICAgICAgLyogLS0tIEdyaWQgdmlldyAtLS0gKi9cclxuICAgICAgICAgICAgJi5ncmlkIHtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XHJcblxyXG4gICAgICAgICAgICAgICAgLmdobS1mZS1vd25lciwgLmdobS1mZS1sdSwgLmdobS1mZS1zaXplIHtcclxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC5naG0tZmUtaXRlbSB7XHJcbiAgICAgICAgICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgICAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLmdobS1mZS1pdGVtLWNvbnRlbnQge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRib3JkZXJDb2xvcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luOiAxMHB4IDEwcHggMCAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmLnNlbGVjdGVkIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRibHVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmID4gZGl2IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZsZXg6IDEgMSBhdXRvO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICYuZ2htLWZlLW5hbWUge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAkYm9yZGVyQ29sb3I7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogN3B4IDE1cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC5naG0tZmUtaXRlbS1pY29uIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxODhweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiA4MHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGltZyB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgZGl2LmdobS1mZS1uYW1lIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAkZ3JheTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLyogLS0tIEVORDogR3JpZCB2aWV3IC0tLSAqL1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuZm9vdGVyIHtcclxuICAgICAgICBvcmRlcjogNTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblxyXG4gICAgICAgIGJ1dHRvbiB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5pLmljb24ge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgbGluZS1oZWlnaHQ6IDE0cHg7XHJcbiAgICBmb250LWZhbWlseTogXCJGb250QXdlc29tZVwiO1xyXG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gICAgZm9udC1zaXplOiBpbmhlcml0O1xyXG4gICAgdGV4dC1yZW5kZXJpbmc6IGF1dG87XHJcbiAgICAtd2Via2l0LWZvbnQtc21vb3RoaW5nOiBhbnRpYWxpYXNlZDtcclxuXHJcbiAgICAmLmljb24tZm9sZGVyOmJlZm9yZSB7XHJcbiAgICAgICAgY29udGVudDogJ1xcZjA3Yic7XHJcbiAgICAgICAgY29sb3I6ICRncmF5O1xyXG4gICAgfVxyXG5cclxuICAgICYuaWNvbi14bHMsICYuaWNvbi14bHN4IHtcclxuICAgICAgICAmOjpiZWZvcmUge1xyXG4gICAgICAgICAgICBjb250ZW50OiBcIlxcZjFjM1wiO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb2xvcjogZm9yZXN0Z3JlZW47XHJcbiAgICB9XHJcbiAgICAmLmljb24tZG9jLCAmLmljb24tZG9jeCB7XHJcbiAgICAgICAgJjpiZWZvcmUge1xyXG4gICAgICAgICAgICBjb250ZW50OiBcIlxcZjFjMlwiO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb2xvcjogY29ybmZsb3dlcmJsdWU7XHJcbiAgICB9XHJcbiAgICAmLmljb24tdHh0OmJlZm9yZSB7XHJcbiAgICAgICAgY29udGVudDogXCJcXGYwZjZcIjtcclxuICAgIH1cclxuICAgICYuaWNvbi1wcHR4IHtcclxuICAgICAgICAmOmJlZm9yZSB7XHJcbiAgICAgICAgICAgIGNvbnRlbnQ6IFwiXFxmMWM0XCI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbG9yOiAjZTc0YzNjO1xyXG4gICAgfVxyXG4gICAgJi5pY29uLXBkZiB7XHJcbiAgICAgICAgJjo6YmVmb3JlIHtcclxuICAgICAgICAgICAgY29udGVudDogXCJcXGYxYzFcIjtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29sb3I6ICNjMDM5MmI7XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6IDQ4MHB4KSB7XHJcbiAgICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIHtcclxuICAgICAgICAuaGVhZGVyIHtcclxuICAgICAgICAgICAgb3JkZXI6IDE7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5hY3Rpb25zIHtcclxuICAgICAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jb250ZW50IHtcclxuICAgICAgICAgICAgb3JkZXI6IDM7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5mb290ZXIge1xyXG4gICAgICAgICAgICBvcmRlcjogNDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5jb250ZW50IHtcclxuXHJcbiAgICAgICAgICAgIC5saXN0LWdobS1mZSB7XHJcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcblxyXG4gICAgICAgICAgICAgICAgJi5ncmlkIHtcclxuICAgICAgICAgICAgICAgICAgICAuZ2htLWZlLWl0ZW0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmbGV4LWJhc2lzOiA1MCU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICYubGlzdCB7XHJcbiAgICAgICAgICAgICAgICAgICAgLmdobS1mZS1sdSwgLmdobS1mZS1zaXplIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogNDgwcHgpIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xyXG4gICAgLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciB7XHJcbiAgICAgICAgLmNvbnRlbnQge1xyXG4gICAgICAgICAgICAubGlzdC1naG0tZmUge1xyXG4gICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG5cclxuICAgICAgICAgICAgICAgICYuZ3JpZCB7XHJcbiAgICAgICAgICAgICAgICAgICAgLmdobS1mZS1pdGVtIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmxleC1iYXNpczogMzMlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogNzY4cHgpIHtcclxuICAgIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIge1xyXG4gICAgICAgIHdpZHRoOiA4ODhweDtcclxuXHJcbiAgICAgICAgLmhlYWRlciB7XHJcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAkYm9yZGVyQ29sb3I7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuc2lkZWJhciB7XHJcbiAgICAgICAgICAgIGZsZXg6IDEgYXV0bztcclxuICAgICAgICAgICAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgJGJvcmRlckNvbG9yO1xyXG4gICAgICAgICAgICBvcmRlcjogMztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5jb250ZW50IHtcclxuICAgICAgICAgICAgb3JkZXI6IDM7XHJcbiAgICAgICAgICAgIGZsZXg6IDQgYXV0bztcclxuXHJcbiAgICAgICAgICAgIC5saXN0LWdobS1mZSB7XHJcbiAgICAgICAgICAgICAgICAmLmdyaWQge1xyXG4gICAgICAgICAgICAgICAgICAgIC5naG0tZmUtaXRlbSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZsZXgtYmFzaXM6IDI1JTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5mb290ZXIge1xyXG4gICAgICAgICAgICBvcmRlcjogNTtcclxuICAgICAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICRib3JkZXJDb2xvcjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qIEVORDogLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciovXHJcbiIsIi5idG4ge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcbiAgLW1vei11c2VyLXNlbGVjdDogbm9uZTtcbiAgLW1zLXVzZXItc2VsZWN0OiBub25lO1xuICB1c2VyLXNlbGVjdDogbm9uZTtcbiAgYm9yZGVyOiAxcHggc29saWQgdHJhbnNwYXJlbnQ7XG4gIHBhZGRpbmc6IDZweCAxMnB4O1xuICBmb250LXNpemU6IDE0cHg7XG4gIGxpbmUtaGVpZ2h0OiAxLjU7XG4gIHRyYW5zaXRpb246IGNvbG9yIC4xNXMgZWFzZS1pbi1vdXQsXHIgYmFja2dyb3VuZC1jb2xvciAuMTVzIGVhc2UtaW4tb3V0LFxyIGJvcmRlci1jb2xvciAuMTVzIGVhc2UtaW4tb3V0LFxyIGJveC1zaGFkb3cgLjE1cyBlYXNlLWluLW91dDtcbiAgdGV4dC1yZW5kZXJpbmc6IGF1dG87XG4gIGNvbG9yOiBpbml0aWFsO1xuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICB3b3JkLXNwYWNpbmc6IG5vcm1hbDtcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XG4gIHRleHQtaW5kZW50OiAwcHg7XG4gIHRleHQtc2hhZG93OiBub25lO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHRleHQtYWxpZ246IHN0YXJ0O1xuICBtYXJnaW46IDBlbTtcbiAgY29sb3I6ICNmZmY7IH1cbiAgLmJ0bi5idG4tYmx1ZSB7XG4gICAgYmFja2dyb3VuZDogIzAwN2JmZjtcbiAgICBib3JkZXItY29sb3I6ICMwMDdiZmY7IH1cbiAgICAuYnRuLmJ0bi1ibHVlOmFjdGl2ZSwgLmJ0bi5idG4tYmx1ZTpmb2N1cyB7XG4gICAgICBib3gtc2hhZG93OiAwIDAgMCAwLjJyZW0gcmdiYSgwLCAxMjMsIDI1NSwgMC41KTtcbiAgICAgIGJvcmRlci1jb2xvcjogIzAwNWNiZjtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDYyY2M7IH1cbiAgICAuYnRuLmJ0bi1ibHVlOmhvdmVyIHtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDY5ZDk7XG4gICAgICBib3JkZXItY29sb3I6ICMwMDYyY2M7IH1cbiAgLmJ0bi5idG4tbGlnaHQge1xuICAgIGNvbG9yOiAjMjEyNTI5O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOGY5ZmE7XG4gICAgYm9yZGVyLWNvbG9yOiAjZjhmOWZhOyB9XG4gICAgLmJ0bi5idG4tbGlnaHQ6aG92ZXIge1xuICAgICAgY29sb3I6ICMyMTI1Mjk7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTJlNmVhO1xuICAgICAgYm9yZGVyLWNvbG9yOiAjZGFlMGU1OyB9XG5cbi5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIge1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdyB3cmFwO1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDVweCAhaW1wb3J0YW50O1xuICAtbW96LWJvcmRlci1yYWRpdXM6IDVweCAhaW1wb3J0YW50O1xuICBib3JkZXItcmFkaXVzOiA1cHggIWltcG9ydGFudDtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjIpO1xuICBvdXRsaW5lOiBub25lOyB9XG4gIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIgPiAqIHtcbiAgICBmbGV4OiAxIDEwMCU7IH1cbiAgLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciAuaGVhZGVyIHtcbiAgICBvcmRlcjogMTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyOyB9XG4gICAgLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciAuaGVhZGVyIC5oZWFkZXItdGl0bGUge1xuICAgICAgZmxleDogNTAgYXV0bzsgfVxuICAgICAgLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciAuaGVhZGVyIC5oZWFkZXItdGl0bGUgLnVwcGVyY2FzZSB7XG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7IH1cbiAgICAgIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIgLmhlYWRlciAuaGVhZGVyLXRpdGxlIC5ib2xkIHtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7IH1cbiAgICAgIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIgLmhlYWRlciAuaGVhZGVyLXRpdGxlIC5ncmF5IHtcbiAgICAgICAgY29sb3I6ICM2Yzc1N2Q7IH1cbiAgICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5oZWFkZXIgc3ZnLmJ0bi1jbG9zZSB7XG4gICAgICBmbGV4OiAxIGF1dG87XG4gICAgICB0ZXh0LWFsaWduOiByaWdodDsgfVxuICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5hY3Rpb25zIHtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RkZDtcbiAgICBvcmRlcjogMjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7IH1cbiAgICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5hY3Rpb25zIGJ1dHRvbiB7XG4gICAgICBtYXJnaW4tbGVmdDogNXB4OyB9XG4gIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIgLmhlYWRlciwgLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciAuZm9vdGVyLCAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5hY3Rpb25zIHtcbiAgICBwYWRkaW5nOiA3cHggMTVweDsgfVxuICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5zaWRlYmFyLCAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5jb250ZW50IHtcbiAgICBoZWlnaHQ6IDQ1MHB4O1xuICAgIG92ZXJmbG93LXk6IGF1dG87IH1cbiAgLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciAuc2lkZWJhciB7XG4gICAgb3JkZXI6IDM7IH1cbiAgLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciAuY29udGVudCB7XG4gICAgb3JkZXI6IDQ7XG4gICAgcGFkZGluZzogMTBweDtcbiAgICBvdmVyZmxvdzogYXV0bzsgfVxuICAgIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIgLmNvbnRlbnQgLmxpc3QtZ2htLWZlIHtcbiAgICAgIC8qIC0tLSBMaXN0IHZpZXcgLS0tKi9cbiAgICAgIC8qIC0tLSBFTkQ6IGxpc3QgdmlldyAtLS0qL1xuICAgICAgLyogLS0tIEdyaWQgdmlldyAtLS0gKi9cbiAgICAgIC8qIC0tLSBFTkQ6IEdyaWQgdmlldyAtLS0gKi8gfVxuICAgICAgLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciAuY29udGVudCAubGlzdC1naG0tZmUubGlzdCB7XG4gICAgICAgIGRpc3BsYXk6IHRhYmxlO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcbiAgICAgICAgLW1vei11c2VyLXNlbGVjdDogbm9uZTtcbiAgICAgICAgLW1zLXVzZXItc2VsZWN0OiBub25lO1xuICAgICAgICB1c2VyLXNlbGVjdDogbm9uZTsgfVxuICAgICAgICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5jb250ZW50IC5saXN0LWdobS1mZS5saXN0IC5naG0tZmUtaXRlbSB7XG4gICAgICAgICAgZGlzcGxheTogdGFibGUtcm93O1xuICAgICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgICBtYXJnaW4tbGVmdDogMTZweDtcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDE2cHg7XG4gICAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgICAgd2lkdGg6IDI0cHg7XG4gICAgICAgICAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcbiAgICAgICAgICAtbW96LXVzZXItc2VsZWN0OiBub25lO1xuICAgICAgICAgIC1tcy11c2VyLXNlbGVjdDogbm9uZTtcbiAgICAgICAgICB1c2VyLXNlbGVjdDogbm9uZTsgfVxuICAgICAgICAgIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIgLmNvbnRlbnQgLmxpc3QtZ2htLWZlLmxpc3QgLmdobS1mZS1pdGVtOmhvdmVyLCAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5jb250ZW50IC5saXN0LWdobS1mZS5saXN0IC5naG0tZmUtaXRlbS5zZWxlY3RlZCB7XG4gICAgICAgICAgICBjb2xvcjogIzAwN2JmZjtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmOGY5ZmE7XG4gICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7IH1cbiAgICAgICAgICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5jb250ZW50IC5saXN0LWdobS1mZS5saXN0IC5naG0tZmUtaXRlbSAuZ2htLWZlLWl0ZW0tY29udGVudCB7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4OyB9XG4gICAgICAgICAgICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5jb250ZW50IC5saXN0LWdobS1mZS5saXN0IC5naG0tZmUtaXRlbSAuZ2htLWZlLWl0ZW0tY29udGVudCA+IGRpdiB7XG4gICAgICAgICAgICAgIGZsZXg6IDE7IH1cbiAgICAgICAgICAgICAgLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciAuY29udGVudCAubGlzdC1naG0tZmUubGlzdCAuZ2htLWZlLWl0ZW0gLmdobS1mZS1pdGVtLWNvbnRlbnQgPiBkaXYuZ2htLWZlLW5hbWUge1xuICAgICAgICAgICAgICAgIGZsZXg6IDM7IH1cbiAgICAgICAgICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5jb250ZW50IC5saXN0LWdobS1mZS5saXN0IC5naG0tZmUtaXRlbSAuZ2htLWZlLWl0ZW0tcHJvcCB7XG4gICAgICAgICAgICBkaXNwbGF5OiB0YWJsZS1jZWxsO1xuICAgICAgICAgICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZGRkO1xuICAgICAgICAgICAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcbiAgICAgICAgICAgIC1tb3otdXNlci1zZWxlY3Q6IG5vbmU7XG4gICAgICAgICAgICAtbXMtdXNlci1zZWxlY3Q6IG5vbmU7XG4gICAgICAgICAgICB1c2VyLXNlbGVjdDogbm9uZTsgfVxuICAgICAgICAgICAgLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciAuY29udGVudCAubGlzdC1naG0tZmUubGlzdCAuZ2htLWZlLWl0ZW0gLmdobS1mZS1pdGVtLXByb3AgLmdobS1mZS1pdGVtLWljb24ge1xuICAgICAgICAgICAgICBwYWRkaW5nLXRvcDogMnB4O1xuICAgICAgICAgICAgICB3aWR0aDogNDBweDsgfVxuICAgICAgICAgICAgICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5jb250ZW50IC5saXN0LWdobS1mZS5saXN0IC5naG0tZmUtaXRlbSAuZ2htLWZlLWl0ZW0tcHJvcCAuZ2htLWZlLWl0ZW0taWNvbiBpbWcge1xuICAgICAgICAgICAgICAgIHdpZHRoOiAzMHB4ICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7IH1cbiAgICAgICAgICAgIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIgLmNvbnRlbnQgLmxpc3QtZ2htLWZlLmxpc3QgLmdobS1mZS1pdGVtIC5naG0tZmUtaXRlbS1wcm9wIC5naG0tZmUtbHUtaXRlbSB7XG4gICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7IH1cbiAgICAgIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIgLmNvbnRlbnQgLmxpc3QtZ2htLWZlLmdyaWQge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LXdyYXA6IHdyYXA7IH1cbiAgICAgICAgLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciAuY29udGVudCAubGlzdC1naG0tZmUuZ3JpZCAuZ2htLWZlLW93bmVyLCAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5jb250ZW50IC5saXN0LWdobS1mZS5ncmlkIC5naG0tZmUtbHUsIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIgLmNvbnRlbnQgLmxpc3QtZ2htLWZlLmdyaWQgLmdobS1mZS1zaXplIHtcbiAgICAgICAgICBkaXNwbGF5OiBub25lOyB9XG4gICAgICAgIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIgLmNvbnRlbnQgLmxpc3QtZ2htLWZlLmdyaWQgLmdobS1mZS1pdGVtIHtcbiAgICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7IH1cbiAgICAgICAgICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5jb250ZW50IC5saXN0LWdobS1mZS5ncmlkIC5naG0tZmUtaXRlbTpob3ZlciB7XG4gICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7IH1cbiAgICAgICAgICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5jb250ZW50IC5saXN0LWdobS1mZS5ncmlkIC5naG0tZmUtaXRlbSAuZ2htLWZlLWl0ZW0tY29udGVudCB7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgZmxleC13cmFwOiB3cmFwO1xuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2RkZDtcbiAgICAgICAgICAgIG1hcmdpbjogMTBweCAxMHB4IDAgMDtcbiAgICAgICAgICAgIGp1c3RpZnktaXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IH1cbiAgICAgICAgICAgIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIgLmNvbnRlbnQgLmxpc3QtZ2htLWZlLmdyaWQgLmdobS1mZS1pdGVtIC5naG0tZmUtaXRlbS1jb250ZW50LnNlbGVjdGVkIHtcbiAgICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgIzAwN2JmZjsgfVxuICAgICAgICAgICAgLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciAuY29udGVudCAubGlzdC1naG0tZmUuZ3JpZCAuZ2htLWZlLWl0ZW0gLmdobS1mZS1pdGVtLWNvbnRlbnQgPiBkaXYge1xuICAgICAgICAgICAgICBmbGV4OiAxIDEgYXV0bzsgfVxuICAgICAgICAgICAgICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5jb250ZW50IC5saXN0LWdobS1mZS5ncmlkIC5naG0tZmUtaXRlbSAuZ2htLWZlLWl0ZW0tY29udGVudCA+IGRpdi5naG0tZmUtbmFtZSB7XG4gICAgICAgICAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkZGQ7XG4gICAgICAgICAgICAgICAgcGFkZGluZzogN3B4IDE1cHg7IH1cbiAgICAgICAgICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5jb250ZW50IC5saXN0LWdobS1mZS5ncmlkIC5naG0tZmUtaXRlbSAuZ2htLWZlLWl0ZW0taWNvbiB7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgaGVpZ2h0OiAxODhweDtcbiAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47IH1cbiAgICAgICAgICAgIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIgLmNvbnRlbnQgLmxpc3QtZ2htLWZlLmdyaWQgLmdobS1mZS1pdGVtIC5naG0tZmUtaXRlbS1pY29uIGkge1xuICAgICAgICAgICAgICBmb250LXNpemU6IDgwcHg7XG4gICAgICAgICAgICAgIG1hcmdpbjogMCBhdXRvOyB9XG4gICAgICAgICAgICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5jb250ZW50IC5saXN0LWdobS1mZS5ncmlkIC5naG0tZmUtaXRlbSAuZ2htLWZlLWl0ZW0taWNvbiBpbWcge1xuICAgICAgICAgICAgICB3aWR0aDogMTAwJTsgfVxuICAgICAgICAgICAgLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciAuY29udGVudCAubGlzdC1naG0tZmUuZ3JpZCAuZ2htLWZlLWl0ZW0gLmdobS1mZS1pdGVtLWljb24gZGl2LmdobS1mZS1uYW1lIHtcbiAgICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgICAgICAgYmFja2dyb3VuZDogIzZjNzU3ZDtcbiAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgICAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gICAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7IH1cbiAgLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciAuZm9vdGVyIHtcbiAgICBvcmRlcjogNTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IH1cbiAgICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5mb290ZXIgYnV0dG9uIHtcbiAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7IH1cblxuaS5pY29uIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBsaW5lLWhlaWdodDogMTRweDtcbiAgZm9udC1mYW1pbHk6IFwiRm9udEF3ZXNvbWVcIjtcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xuICBmb250LXNpemU6IGluaGVyaXQ7XG4gIHRleHQtcmVuZGVyaW5nOiBhdXRvO1xuICAtd2Via2l0LWZvbnQtc21vb3RoaW5nOiBhbnRpYWxpYXNlZDsgfVxuICBpLmljb24uaWNvbi1mb2xkZXI6YmVmb3JlIHtcbiAgICBjb250ZW50OiAnXFxmMDdiJztcbiAgICBjb2xvcjogIzZjNzU3ZDsgfVxuICBpLmljb24uaWNvbi14bHMsIGkuaWNvbi5pY29uLXhsc3gge1xuICAgIGNvbG9yOiBmb3Jlc3RncmVlbjsgfVxuICAgIGkuaWNvbi5pY29uLXhsczo6YmVmb3JlLCBpLmljb24uaWNvbi14bHN4OjpiZWZvcmUge1xuICAgICAgY29udGVudDogXCJcXGYxYzNcIjsgfVxuICBpLmljb24uaWNvbi1kb2MsIGkuaWNvbi5pY29uLWRvY3gge1xuICAgIGNvbG9yOiBjb3JuZmxvd2VyYmx1ZTsgfVxuICAgIGkuaWNvbi5pY29uLWRvYzpiZWZvcmUsIGkuaWNvbi5pY29uLWRvY3g6YmVmb3JlIHtcbiAgICAgIGNvbnRlbnQ6IFwiXFxmMWMyXCI7IH1cbiAgaS5pY29uLmljb24tdHh0OmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcXGYwZjZcIjsgfVxuICBpLmljb24uaWNvbi1wcHR4IHtcbiAgICBjb2xvcjogI2U3NGMzYzsgfVxuICAgIGkuaWNvbi5pY29uLXBwdHg6YmVmb3JlIHtcbiAgICAgIGNvbnRlbnQ6IFwiXFxmMWM0XCI7IH1cbiAgaS5pY29uLmljb24tcGRmIHtcbiAgICBjb2xvcjogI2MwMzkyYjsgfVxuICAgIGkuaWNvbi5pY29uLXBkZjo6YmVmb3JlIHtcbiAgICAgIGNvbnRlbnQ6IFwiXFxmMWMxXCI7IH1cblxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDogNDgwcHgpIHtcbiAgLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciAuaGVhZGVyIHtcbiAgICBvcmRlcjogMTsgfVxuICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5hY3Rpb25zIHtcbiAgICBvcmRlcjogMjsgfVxuICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5jb250ZW50IHtcbiAgICBvcmRlcjogMzsgfVxuICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5mb290ZXIge1xuICAgIG9yZGVyOiA0OyB9XG4gIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIgLmNvbnRlbnQgLmxpc3QtZ2htLWZlIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7IH1cbiAgICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5jb250ZW50IC5saXN0LWdobS1mZS5ncmlkIC5naG0tZmUtaXRlbSB7XG4gICAgICBmbGV4LWJhc2lzOiA1MCU7IH1cbiAgICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5jb250ZW50IC5saXN0LWdobS1mZS5saXN0IC5naG0tZmUtbHUsIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIgLmNvbnRlbnQgLmxpc3QtZ2htLWZlLmxpc3QgLmdobS1mZS1zaXplIHtcbiAgICAgIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDsgfSB9XG5cbkBtZWRpYSBhbGwgYW5kIChtaW4td2lkdGg6IDQ4MHB4KSBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcbiAgLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciAuY29udGVudCAubGlzdC1naG0tZmUge1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDsgfVxuICAgIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIgLmNvbnRlbnQgLmxpc3QtZ2htLWZlLmdyaWQgLmdobS1mZS1pdGVtIHtcbiAgICAgIGZsZXgtYmFzaXM6IDMzJTsgfSB9XG5cbkBtZWRpYSBhbGwgYW5kIChtaW4td2lkdGg6IDc2OHB4KSB7XG4gIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIge1xuICAgIHdpZHRoOiA4ODhweDsgfVxuICAgIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIgLmhlYWRlciB7XG4gICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RkZDsgfVxuICAgIC5naG0tZmlsZS1leHBsb3Jlci1jb250YWluZXIgLnNpZGViYXIge1xuICAgICAgZmxleDogMSBhdXRvO1xuICAgICAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2RkZDtcbiAgICAgIG9yZGVyOiAzOyB9XG4gICAgLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciAuY29udGVudCB7XG4gICAgICBvcmRlcjogMztcbiAgICAgIGZsZXg6IDQgYXV0bzsgfVxuICAgICAgLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciAuY29udGVudCAubGlzdC1naG0tZmUuZ3JpZCAuZ2htLWZlLWl0ZW0ge1xuICAgICAgICBmbGV4LWJhc2lzOiAyNSU7IH1cbiAgICAuZ2htLWZpbGUtZXhwbG9yZXItY29udGFpbmVyIC5mb290ZXIge1xuICAgICAgb3JkZXI6IDU7XG4gICAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2RkZDsgfSB9XG5cbi8qIEVORDogLmdobS1maWxlLWV4cGxvcmVyLWNvbnRhaW5lciovXG4iXX0= */"

/***/ }),

/***/ "./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.component.ts ***!
  \*************************************************************************************/
/*! exports provided: GhmFileExplorerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmFileExplorerComponent", function() { return GhmFileExplorerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/overlay */ "./node_modules/@angular/cdk/esm5/overlay.es5.js");
/* harmony import */ var _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/portal */ "./node_modules/@angular/cdk/esm5/portal.es5.js");
/* harmony import */ var _explorer_item_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./explorer-item.model */ "./src/app/shareds/components/ghm-file-explorer/explorer-item.model.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_util_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _ghm_new_folder_ghm_new_folder_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./ghm-new-folder/ghm-new-folder.component */ "./src/app/shareds/components/ghm-file-explorer/ghm-new-folder/ghm-new-folder.component.ts");
/* harmony import */ var _ghm_file_upload_ghm_file_upload_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./ghm-file-upload/ghm-file-upload.service */ "./src/app/shareds/components/ghm-file-explorer/ghm-file-upload/ghm-file-upload.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _ghm_file_explorer_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./ghm-file-explorer.service */ "./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.service.ts");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _models_breadcrumb_model__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./models/breadcrumb.model */ "./src/app/shareds/components/ghm-file-explorer/models/breadcrumb.model.ts");















var GhmFileExplorerComponent = /** @class */ (function () {
    function GhmFileExplorerComponent(appConfig, utilService, toastr, viewContainerRef, ghmFileUploadService, ghmFileExplorerService, overlay) {
        var _this = this;
        this.appConfig = appConfig;
        this.utilService = utilService;
        this.toastr = toastr;
        this.viewContainerRef = viewContainerRef;
        this.ghmFileUploadService = ghmFileUploadService;
        this.ghmFileExplorerService = ghmFileExplorerService;
        this.overlay = overlay;
        this.multiple = false;
        this.confirmText = 'Confirm';
        this.closeText = 'Close';
        this.isEnable = false;
        this.buttonClass = 'blue';
        this.footerClass = 'blue';
        this.headerTitle = 'GHMSoft file explorer';
        this.showCloseButton = true;
        this.type = 'button'; // button, label
        this.itemSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.acceptSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.selectItem$ = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
        this.openItem$ = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
        this.isMultipleSelected = false;
        // 0: List 1: Grid
        this.isGridView = true;
        this.explorerItems = [];
        this.breadcrumbs = [];
        this.positionStrategy = new _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_2__["GlobalPositionStrategy"]();
        this.selectItem$
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["delay"])(200), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["takeUntil"])(this.openItem$), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["repeat"])())
            .subscribe(function (explorerItem) {
            if (_this.multiple) {
                // Set item is selected to true.
                _this.changeSelectedStatus(explorerItem);
            }
            else {
                // Emit selected item.
                _this.itemSelected.emit(explorerItem);
                _this.dismissModal();
            }
        });
        this.openItem$.subscribe(function (explorerItem) {
            if (_this.multiple) {
                explorerItem.isSelected = !explorerItem.isSelected;
            }
            if (explorerItem.type === 'folder') {
                // Set id to current folder id then get all item inside folder.
                _this.currentFolderId = explorerItem.id;
                _this.getCurrentDirectory();
                _this.createBreadcrumb(explorerItem);
            }
            else {
                _this.itemSelected.emit(explorerItem);
                _this.dismissModal();
            }
        });
        this.ghmFileUploadService.complete$
            .subscribe(function (result) {
            if (result.code <= 0) {
                _this.toastr.error(result.message);
                return;
            }
            else {
                var files = result.data;
                var explorerItems = files.map(function (file) {
                    return new _explorer_item_model__WEBPACK_IMPORTED_MODULE_4__["ExplorerItem"](file.id, file.name, file.type, file.createTime, file.size, file.creatorId, file.creatorFullName, file.creatorAvatar, file.extension, file.url, _this.renderFileUrl(file.url));
                });
                explorerItems.forEach(function (explorerItem) {
                    _this.explorerItems.push(explorerItem);
                });
            }
        });
    }
    GhmFileExplorerComponent.prototype.windowResize = function (e) {
        if (this.overlayRef.hasAttached()) {
            this.calculatePositionStrategy();
        }
    };
    GhmFileExplorerComponent.prototype.ngOnInit = function () {
        this.overlayRef = this.overlay.create({
            positionStrategy: this.positionStrategy,
            hasBackdrop: true
        });
    };
    GhmFileExplorerComponent.prototype.ngOnDestroy = function () {
        this.overlayRef.dispose();
    };
    GhmFileExplorerComponent.prototype.onSaveFolderSuccessful = function (folder) {
        var explorerItemInfo = lodash__WEBPACK_IMPORTED_MODULE_8__["find"](this.explorerItems, function (explorerItem) {
            return folder.id === explorerItem.id;
        });
        if (explorerItemInfo) {
            explorerItemInfo.name = folder.name;
        }
        else {
            this.explorerItems.push(new _explorer_item_model__WEBPACK_IMPORTED_MODULE_4__["ExplorerItem"](folder.id, folder.name, 'folder', folder.createTime, null, folder.creatorId, folder.creatorFullName, folder.creatorAvatar, 'folder'));
        }
    };
    GhmFileExplorerComponent.prototype.changeSelectedStatus = function (explorerItem) {
        explorerItem.isSelected = !explorerItem.isSelected;
        this.setMultipleCheck();
    };
    GhmFileExplorerComponent.prototype.showExplorer = function () {
        if (this.overlayRef && !this.overlayRef.hasAttached()) {
            this.overlayRef.attach(new _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_3__["TemplatePortal"](this.templateRef, this.viewContainerRef));
            this.calculatePositionStrategy();
            // Get all file and folder.
            this.getCurrentDirectory();
        }
    };
    GhmFileExplorerComponent.prototype.showDirectory = function (breadcrumb) {
        this.currentFolderId = !breadcrumb ? null : breadcrumb.id;
        this.reRenderBreadcrumb(breadcrumb);
        this.getCurrentDirectory();
    };
    GhmFileExplorerComponent.prototype.createNewFolder = function () {
        this.ghmNewFolderComponent.add(this.currentFolderId);
    };
    GhmFileExplorerComponent.prototype.closeModal = function () {
        this.overlayRef.detach();
    };
    GhmFileExplorerComponent.prototype.selectItem = function (explorerItem) {
        this.selectItem$.next(explorerItem);
    };
    GhmFileExplorerComponent.prototype.openItem = function (explorerItem) {
        this.openItem$.next(explorerItem);
    };
    GhmFileExplorerComponent.prototype.confirmSelect = function () {
        var selectedItems = lodash__WEBPACK_IMPORTED_MODULE_8__["filter"](this.explorerItems, function (explorerItem) {
            return explorerItem.isSelected;
        });
        this.acceptSelected.emit(selectedItems);
        this.dismissModal();
    };
    GhmFileExplorerComponent.prototype.calculatePositionStrategy = function () {
        var windowHeight = window.innerHeight;
        var windowWidth = window.innerWidth;
        var elementRect = this.overlayRef.overlayElement.getBoundingClientRect();
        if (elementRect) {
            var elementWidth = elementRect.width;
            var elementHeight = elementRect.height;
            var left = (windowWidth - elementWidth) / 2;
            var top_1 = (windowHeight - elementHeight) / 2;
            this.positionStrategy.top(top_1 + 'px');
            this.positionStrategy.left(left + 'px');
            this.positionStrategy.apply();
        }
    };
    GhmFileExplorerComponent.prototype.setMultipleCheck = function () {
        var countSelectedItems = lodash__WEBPACK_IMPORTED_MODULE_8__["countBy"](this.explorerItems, function (explorerItem) {
            return explorerItem.isSelected;
        });
        this.isMultipleSelected = countSelectedItems.true > 0;
    };
    GhmFileExplorerComponent.prototype.renderFileUrl = function (url) {
        return "" + this.appConfig.FILE_URL + url;
    };
    GhmFileExplorerComponent.prototype.getCurrentDirectory = function () {
        var _this = this;
        this.explorerItems = [];
        this.ghmFileExplorerService.getCurrentDirectory(this.currentFolderId)
            .subscribe(function (result) {
            var explorerItems = [];
            if (result.files) {
                var files = result.files.map(function (file) {
                    return new _explorer_item_model__WEBPACK_IMPORTED_MODULE_4__["ExplorerItem"](file.id, file.name, file.type, file.createTime, file.size, file.creatorId, file.creatorFullName, file.creatorAvatar, file.extension, file.url, _this.renderFileUrl(file.url));
                });
                files.forEach(function (file) {
                    explorerItems.push(file);
                });
            }
            if (result.folders) {
                var folders = result.folders.map(function (folder) {
                    return new _explorer_item_model__WEBPACK_IMPORTED_MODULE_4__["ExplorerItem"](folder.id, folder.name, 'folder', folder.createTime, 0, folder.creatorId, folder.creatorFullName, folder.creatorAvatar, 'folder');
                });
                folders.forEach(function (folder) {
                    explorerItems.push(folder);
                });
            }
            _this.explorerItems = explorerItems;
        });
    };
    GhmFileExplorerComponent.prototype.createBreadcrumb = function (explorerItem) {
        var existingBreadcrumb = lodash__WEBPACK_IMPORTED_MODULE_8__["find"](this.breadcrumbs, function (breadcrumb) {
            return breadcrumb.id === explorerItem.id;
        });
        if (!existingBreadcrumb) {
            this.breadcrumbs.push(new _models_breadcrumb_model__WEBPACK_IMPORTED_MODULE_14__["Breadcrumb"](explorerItem.id, explorerItem.name));
        }
    };
    GhmFileExplorerComponent.prototype.reRenderBreadcrumb = function (breadcrumb) {
        var index = this.breadcrumbs.indexOf(breadcrumb);
        this.breadcrumbs = this.breadcrumbs.slice(0, index + 1);
    };
    GhmFileExplorerComponent.prototype.dismissModal = function () {
        this.overlayRef.detach();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('ghmExplorerTemplate'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"])
    ], GhmFileExplorerComponent.prototype, "templateRef", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ghm_new_folder_ghm_new_folder_component__WEBPACK_IMPORTED_MODULE_9__["GhmNewFolderComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ghm_new_folder_ghm_new_folder_component__WEBPACK_IMPORTED_MODULE_9__["GhmNewFolderComponent"])
    ], GhmFileExplorerComponent.prototype, "ghmNewFolderComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GhmFileExplorerComponent.prototype, "buttonText", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmFileExplorerComponent.prototype, "multiple", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmFileExplorerComponent.prototype, "confirmText", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmFileExplorerComponent.prototype, "closeText", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmFileExplorerComponent.prototype, "isEnable", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmFileExplorerComponent.prototype, "buttonClass", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmFileExplorerComponent.prototype, "footerClass", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmFileExplorerComponent.prototype, "headerTitle", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmFileExplorerComponent.prototype, "showCloseButton", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmFileExplorerComponent.prototype, "type", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmFileExplorerComponent.prototype, "itemSelected", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmFileExplorerComponent.prototype, "acceptSelected", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:resize', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], GhmFileExplorerComponent.prototype, "windowResize", null);
    GhmFileExplorerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ghm-file-explorer',
            template: __webpack_require__(/*! ./ghm-file-explorer.component.html */ "./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./ghm-file-explorer.component.scss */ "./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_13__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _services_util_service__WEBPACK_IMPORTED_MODULE_7__["UtilService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_11__["ToastrService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"],
            _ghm_file_upload_ghm_file_upload_service__WEBPACK_IMPORTED_MODULE_10__["GhmFileUploadService"],
            _ghm_file_explorer_service__WEBPACK_IMPORTED_MODULE_12__["GhmFileExplorerService"],
            _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_2__["Overlay"]])
    ], GhmFileExplorerComponent);
    return GhmFileExplorerComponent;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.module.ts ***!
  \**********************************************************************************/
/*! exports provided: GhmFileExplorerModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmFileExplorerModule", function() { return GhmFileExplorerModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ghm_file_explorer_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ghm-file-explorer.component */ "./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.component.ts");
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/cdk/overlay */ "./node_modules/@angular/cdk/esm5/overlay.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _ghm_new_folder_ghm_new_folder_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ghm-new-folder/ghm-new-folder.component */ "./src/app/shareds/components/ghm-file-explorer/ghm-new-folder/ghm-new-folder.component.ts");
/* harmony import */ var _nh_modal_nh_modal_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../nh-modal/nh-modal.module */ "./src/app/shareds/components/nh-modal/nh-modal.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ghm_file_upload_ghm_file_upload_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./ghm-file-upload/ghm-file-upload.component */ "./src/app/shareds/components/ghm-file-explorer/ghm-file-upload/ghm-file-upload.component.ts");
/* harmony import */ var _ghm_file_explorer_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./ghm-file-explorer.service */ "./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.service.ts");
/* harmony import */ var _ghm_file_upload_ghm_file_upload_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./ghm-file-upload/ghm-file-upload.service */ "./src/app/shareds/components/ghm-file-explorer/ghm-file-upload/ghm-file-upload.service.ts");
/* harmony import */ var _pipe_datetime_format_datetime_format_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../pipe/datetime-format/datetime-format.module */ "./src/app/shareds/pipe/datetime-format/datetime-format.module.ts");













var GhmFileExplorerModule = /** @class */ (function () {
    function GhmFileExplorerModule() {
    }
    GhmFileExplorerModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_4__["OverlayModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatCheckboxModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTooltipModule"], _nh_modal_nh_modal_module__WEBPACK_IMPORTED_MODULE_7__["NhModalModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"], _pipe_datetime_format_datetime_format_module__WEBPACK_IMPORTED_MODULE_12__["DatetimeFormatModule"]
            ],
            declarations: [_ghm_file_explorer_component__WEBPACK_IMPORTED_MODULE_3__["GhmFileExplorerComponent"], _ghm_new_folder_ghm_new_folder_component__WEBPACK_IMPORTED_MODULE_6__["GhmNewFolderComponent"], _ghm_file_upload_ghm_file_upload_component__WEBPACK_IMPORTED_MODULE_9__["GhmFileUploadComponent"]],
            entryComponents: [],
            exports: [_ghm_file_explorer_component__WEBPACK_IMPORTED_MODULE_3__["GhmFileExplorerComponent"], _ghm_file_upload_ghm_file_upload_component__WEBPACK_IMPORTED_MODULE_9__["GhmFileUploadComponent"]],
            providers: [_ghm_file_explorer_service__WEBPACK_IMPORTED_MODULE_10__["GhmFileExplorerService"], _ghm_file_upload_ghm_file_upload_service__WEBPACK_IMPORTED_MODULE_11__["GhmFileUploadService"]]
        })
    ], GhmFileExplorerModule);
    return GhmFileExplorerModule;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.service.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.service.ts ***!
  \***********************************************************************************/
/*! exports provided: GhmFileExplorerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmFileExplorerService", function() { return GhmFileExplorerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");








var GhmFileExplorerService = /** @class */ (function () {
    function GhmFileExplorerService(appConfig, toastr, spinnerService, http) {
        this.appConfig = appConfig;
        this.toastr = toastr;
        this.spinnerService = spinnerService;
        this.http = http;
        this.url = 'files';
        this.folderUrl = 'folders';
        this.url = "" + _environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].apiGatewayUrl + this.url;
        this.folderUrl = "" + appConfig.FILE_MANAGEMENT + this.folderUrl;
    }
    GhmFileExplorerService.prototype.createFolder = function (folder) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post("" + this.folderUrl, {
            name: folder.name,
            parentId: folder.parentId,
            concurrencyStamp: folder.concurrencyStamp,
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    GhmFileExplorerService.prototype.updateFolderName = function (id, folder) {
        var _this = this;
        return this.http.post(this.folderUrl + "/" + id, {
            name: folder.name,
            parentId: folder.parentId,
            concurrencyStamp: folder.concurrencyStamp,
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    GhmFileExplorerService.prototype.getCurrentDirectory = function (folderId) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get("" + this.folderUrl, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpParams"]()
                .set('folderId', folderId ? folderId.toString() : '')
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    GhmFileExplorerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__["SpinnerService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]])
    ], GhmFileExplorerService);
    return GhmFileExplorerService;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-file-explorer/ghm-file-upload/ghm-file-upload.component.html":
/*!*****************************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-file-explorer/ghm-file-upload/ghm-file-upload.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<input\r\n    #fileInput\r\n    class=\"hidden\"\r\n    type=\"file\"\r\n    [attr.multiple]=\"multiple ? true : null\"\r\n    (change)=\"onFileChange($event)\">\r\n\r\n<button type=\"button\" class=\"btn btn-blue\" (click)=\"fileInput.click()\">\r\n    <i class=\"fa fa-cloud-upload cm-mgr-5\"></i>\r\n    <span i18n=\"@@chooseFile\">{{ text }}</span>\r\n</button>\r\n"

/***/ }),

/***/ "./src/app/shareds/components/ghm-file-explorer/ghm-file-upload/ghm-file-upload.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-file-explorer/ghm-file-upload/ghm-file-upload.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: GhmFileUploadComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmFileUploadComponent", function() { return GhmFileUploadComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ghm_file_upload_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ghm-file-upload.service */ "./src/app/shareds/components/ghm-file-explorer/ghm-file-upload/ghm-file-upload.service.ts");



var GhmFileUploadComponent = /** @class */ (function () {
    function GhmFileUploadComponent(ghmFileUploadService) {
        this.ghmFileUploadService = ghmFileUploadService;
        this.multiple = true;
        this.text = 'Choose file';
        this.uploaded = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.ghmFileUploadService.sent$.subscribe(function (result) { return console.log('Start upload'); });
        this.ghmFileUploadService.progress$.subscribe(function (result) {
            return console.log(result);
        });
        this.ghmFileUploadService.complete$.subscribe(function (result) {
            console.log('complete');
        });
    }
    GhmFileUploadComponent.prototype.ngOnInit = function () {
    };
    GhmFileUploadComponent.prototype.onFileChange = function (event) {
        var _this = this;
        var files = event.target.files;
        this.ghmFileUploadService.upload(files, { folderId: this.folderId })
            .subscribe(function (response) {
            if (response.code > 0) {
                _this.uploaded.next(response.data);
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], GhmFileUploadComponent.prototype, "folderId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmFileUploadComponent.prototype, "multiple", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmFileUploadComponent.prototype, "text", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmFileUploadComponent.prototype, "uploaded", void 0);
    GhmFileUploadComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ghm-file-upload',
            template: __webpack_require__(/*! ./ghm-file-upload.component.html */ "./src/app/shareds/components/ghm-file-explorer/ghm-file-upload/ghm-file-upload.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ../ghm-file-explorer.component.scss */ "./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ghm_file_upload_service__WEBPACK_IMPORTED_MODULE_2__["GhmFileUploadService"]])
    ], GhmFileUploadComponent);
    return GhmFileUploadComponent;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-file-explorer/ghm-file-upload/ghm-file-upload.service.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-file-explorer/ghm-file-upload/ghm-file-upload.service.ts ***!
  \*************************************************************************************************/
/*! exports provided: GhmFileUploadService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmFileUploadService", function() { return GhmFileUploadService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");






var GhmFileUploadService = /** @class */ (function () {
    function GhmFileUploadService(appConfig, http) {
        this.appConfig = appConfig;
        this.http = http;
        this.url = 'files/';
        this.sent$ = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
        this.progress$ = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
        this.complete$ = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
        this.url = "" + appConfig.FILE_MANAGEMENT + this.url;
    }
    GhmFileUploadService.prototype.upload = function (fileList, extractsData) {
        var _this = this;
        var files = new FormData();
        for (var key in extractsData) {
            if (extractsData.hasOwnProperty(key)) {
                var value = extractsData[key];
                files.set(key, value ? value : '');
            }
        }
        for (var i = 0; i < fileList.length; i++) {
            files.append('formFileCollection', fileList[i]);
        }
        return this.http.post(this.url + "uploads", files, {
            reportProgress: true,
            observe: 'events',
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]()
                .set('Content-Type', 'clear')
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (event) { return _this.getEventMessage(event); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (message) { return _this.showProgress(message); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["last"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError()));
    };
    GhmFileUploadService.prototype.getEventMessage = function (event) {
        switch (event.type) {
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpEventType"].Sent:
                this.sent$.next();
                return;
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpEventType"].UploadProgress:
                // Compute and show the % done:
                var percentDone = Math.round(100 * event.loaded / event.total);
                this.progress$.next(percentDone);
                return;
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpEventType"].Response:
                var body = event.body;
                this.complete$.next(body);
                return body;
            default:
                return "File surprising upload event: " + event.type + ".";
        }
    };
    GhmFileUploadService.prototype.showProgress = function (message) {
    };
    GhmFileUploadService.prototype.handleError = function () {
        return function (p1, p2) {
            return undefined;
        };
    };
    GhmFileUploadService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], GhmFileUploadService);
    return GhmFileUploadService;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-file-explorer/ghm-new-folder/folder.model.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-file-explorer/ghm-new-folder/folder.model.ts ***!
  \*************************************************************************************/
/*! exports provided: Folder */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Folder", function() { return Folder; });
var Folder = /** @class */ (function () {
    function Folder(name, parentId, concurrencyStamp) {
        this.name = name ? name : '';
        this.parentId = parentId;
        this.concurrencyStamp = concurrencyStamp ? concurrencyStamp : '';
    }
    return Folder;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-file-explorer/ghm-new-folder/ghm-new-folder.component.css":
/*!**************************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-file-explorer/ghm-new-folder/ghm-new-folder.component.css ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZHMvY29tcG9uZW50cy9naG0tZmlsZS1leHBsb3Jlci9naG0tbmV3LWZvbGRlci9naG0tbmV3LWZvbGRlci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/shareds/components/ghm-file-explorer/ghm-new-folder/ghm-new-folder.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-file-explorer/ghm-new-folder/ghm-new-folder.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #folderFormModal size=\"sm\" (onShown)=\"onModalShown()\">\r\n    <nh-modal-header [showCloseButton]=\"true\">\r\n        <span i18n=\"@@addNewFolder\">Add new folder</span>\r\n    </nh-modal-header>\r\n    <form (ngSubmit)=\"save()\">\r\n        <nh-modal-content>\r\n            <div class=\"form-group\">\r\n                <input type=\"text\" class=\"form-control\"\r\n                       i18n-placeholder=\"@@enterFolderName\"\r\n                       placeholder=\"Enter folder name\"\r\n                       id=\"folderName\"\r\n                       name=\"folderName\"\r\n                       [(ngModel)]=\"name\">\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer class=\"text-right\">\r\n            <button type=\"submit\" class=\"btn blue cm-mgr-5\" [disabled]=\"!name\">\r\n                <span i18n=\"@@create\">Create</span>\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-light\" i18n=\"@@cancel\" nh-dismiss>\r\n                Cancel\r\n            </button>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/shareds/components/ghm-file-explorer/ghm-new-folder/ghm-new-folder.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-file-explorer/ghm-new-folder/ghm-new-folder.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: GhmNewFolderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmNewFolderComponent", function() { return GhmNewFolderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _services_util_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _ghm_file_explorer_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../ghm-file-explorer.service */ "./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.service.ts");
/* harmony import */ var _folder_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./folder.model */ "./src/app/shareds/components/ghm-file-explorer/ghm-new-folder/folder.model.ts");







var GhmNewFolderComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](GhmNewFolderComponent, _super);
    function GhmNewFolderComponent(utilService, ghmFileExplorerService) {
        var _this = _super.call(this) || this;
        _this.utilService = utilService;
        _this.ghmFileExplorerService = ghmFileExplorerService;
        return _this;
    }
    GhmNewFolderComponent.prototype.ngOnInit = function () {
    };
    GhmNewFolderComponent.prototype.onModalShown = function () {
        this.utilService.focusElement('folderName');
    };
    GhmNewFolderComponent.prototype.add = function (parentId) {
        this.parentId = parentId;
        this.folderFormModal.open();
    };
    GhmNewFolderComponent.prototype.save = function () {
        var _this = this;
        if (this.name) {
            if (this.isUpdate) {
            }
            else {
                this.ghmFileExplorerService.createFolder(new _folder_model__WEBPACK_IMPORTED_MODULE_6__["Folder"](this.name, this.parentId))
                    .subscribe(function (result) {
                    _this.saveSuccessful.emit(result.data);
                    _this.name = null;
                    _this.folderFormModal.dismiss();
                });
            }
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('folderFormModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_2__["NhModalComponent"])
    ], GhmNewFolderComponent.prototype, "folderFormModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], GhmNewFolderComponent.prototype, "parentId", void 0);
    GhmNewFolderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ghm-new-folder',
            template: __webpack_require__(/*! ./ghm-new-folder.component.html */ "./src/app/shareds/components/ghm-file-explorer/ghm-new-folder/ghm-new-folder.component.html"),
            styles: [__webpack_require__(/*! ./ghm-new-folder.component.css */ "./src/app/shareds/components/ghm-file-explorer/ghm-new-folder/ghm-new-folder.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_util_service__WEBPACK_IMPORTED_MODULE_3__["UtilService"],
            _ghm_file_explorer_service__WEBPACK_IMPORTED_MODULE_5__["GhmFileExplorerService"]])
    ], GhmNewFolderComponent);
    return GhmNewFolderComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_4__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/shareds/components/ghm-file-explorer/models/breadcrumb.model.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-file-explorer/models/breadcrumb.model.ts ***!
  \*********************************************************************************/
/*! exports provided: Breadcrumb */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Breadcrumb", function() { return Breadcrumb; });
var Breadcrumb = /** @class */ (function () {
    function Breadcrumb(id, name) {
        this.id = id;
        this.name = name;
    }
    return Breadcrumb;
}());



/***/ }),

/***/ "./src/app/shareds/pipe/datetime-format/datetime-format.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/shareds/pipe/datetime-format/datetime-format.module.ts ***!
  \************************************************************************/
/*! exports provided: DatetimeFormatModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatetimeFormatModule", function() { return DatetimeFormatModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _datetime_format_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./datetime-format.pipe */ "./src/app/shareds/pipe/datetime-format/datetime-format.pipe.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");




var DatetimeFormatModule = /** @class */ (function () {
    function DatetimeFormatModule() {
    }
    DatetimeFormatModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"]],
            exports: [_datetime_format_pipe__WEBPACK_IMPORTED_MODULE_2__["DateTimeFormatPipe"]],
            declarations: [_datetime_format_pipe__WEBPACK_IMPORTED_MODULE_2__["DateTimeFormatPipe"]],
            providers: [],
        })
    ], DatetimeFormatModule);
    return DatetimeFormatModule;
}());



/***/ }),

/***/ "./src/app/shareds/pipe/datetime-format/datetime-format.pipe.ts":
/*!**********************************************************************!*\
  !*** ./src/app/shareds/pipe/datetime-format/datetime-format.pipe.ts ***!
  \**********************************************************************/
/*! exports provided: DateTimeFormatPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DateTimeFormatPipe", function() { return DateTimeFormatPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);



var DateTimeFormatPipe = /** @class */ (function () {
    function DateTimeFormatPipe() {
        this._inputDateTimeAllowedFormat = [
            'DD/MM/YYYY',
            'DD/MM/YYYY HH:mm',
            'DD/MM/YYYY HH:mm:ss',
            'DD/MM/YYYY HH:mm Z',
            'DD-MM-YYYY',
            'DD-MM-YYYY HH:mm',
            'DD-MM-YYYY HH:mm:ss',
            'DD-MM-YYYY HH:mm Z',
            // --------------------
            'MM/DD/YYYY',
            'MM/DD/YYYY HH:mm',
            'MM/DD/YYYY HH:mm:ss',
            'MM/DD/YYYY HH:mm Z',
            'MM-DD-YYYY',
            'MM-DD-YYYY HH:mm',
            'MM-DD-YYYY HH:mm:ss',
            'MM-DD-YYYY HH:mm Z',
            // --------------------
            'YYYY/MM/DD',
            'YYYY/MM/DD HH:mm',
            'YYYY/MM/DD HH:mm:ss',
            'YYYY/MM/DD HH:mm Z',
            'YYYY-MM-DD',
            'YYYY-MM-DD HH:mm',
            'YYYY-MM-DD HH:mm:ss',
            'YYYY-MM-DD HH:mm Z',
            // --------------------
            'YYYY/DD/MM',
            'YYYY/DD/MM HH:mm',
            'YYYY/DD/MM HH:mm:ss',
            'YYYY/DD/MM HH:mm Z',
            'YYYY-DD-MM',
            'YYYY-DD-MM HH:mm',
            'YYYY-DD-MM HH:mm:ss',
            'YYYY-DD-MM HH:mm Z',
        ];
    }
    DateTimeFormatPipe.prototype.transform = function (value, exponent, isUtc) {
        if (isUtc === void 0) { isUtc = false; }
        return this.formatDate(value, exponent, isUtc);
    };
    DateTimeFormatPipe.prototype.formatDate = function (value, format, isUtc) {
        if (isUtc === void 0) { isUtc = false; }
        if (!moment__WEBPACK_IMPORTED_MODULE_2__(value, this._inputDateTimeAllowedFormat).isValid()) {
            return '';
        }
        return isUtc ? moment__WEBPACK_IMPORTED_MODULE_2__["utc"](value, this._inputDateTimeAllowedFormat).format(format)
            : moment__WEBPACK_IMPORTED_MODULE_2__(value, this._inputDateTimeAllowedFormat).format(format);
        // return value;
    };
    DateTimeFormatPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({ name: 'dateTimeFormat' })
    ], DateTimeFormatPipe);
    return DateTimeFormatPipe;
}());



/***/ })

}]);
//# sourceMappingURL=default~modules-configs-config-module~modules-folders-folder-module~modules-task-target-target-modul~53d0e6d5.js.map