(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~modules-hr-organization-organization-module~modules-hr-user-user-module~modules-warehouse-go~a430b70d"],{

/***/ "./src/app/base.component.ts":
/*!***********************************!*\
  !*** ./src/app/base.component.ts ***!
  \***********************************/
/*! exports provided: String, BaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "String", function() { return String; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseComponent", function() { return BaseComponent; });
var String;
(function (String) {
})(String || (String = {}));
var BaseComponent = /** @class */ (function () {
    function BaseComponent() {
        this.isSaving = false;
        this.isUpdate = false;
        this.isShowForm = false;
        this.isLoading = false;
        this.isSearching = false;
        this.totalRows = 0;
        this.currentPage = 1;
        this.pageSize = 20;
        this.isSubmitted = false;
        this.keyword = '';
        this.isActiveSearch = null;
        this.pageTitle = '';
        this.formErrors = {};
        this.validationMessages = {};
        this.isHasInsertPermission = false;
        this.isHasUpdatePermission = false;
        this.isHasDeletePermission = false;
        this.isHasPrintPermission = false;
        this.isHasApprovePermission = false;
        this.isHasExportPermission = false;
        this.isHasViewPermission = false;
        this.isHasReportPermission = false;
        this.subscribers = {};
        this.downloading = false;
        this.dateTimeValidFormat = [
            'DD/MM/YYYY',
            'DD/MM/YYYY HH:mm',
            'DD/MM/YYYY HH:mm:ss',
            'DD/MM/YYYY HH:mm Z',
            'DD-MM-YYYY',
            'DD-MM-YYYY HH:mm',
            'DD-MM-YYYY HH:mm:ss',
            'DD-MM-YYYY HH:mm Z',
            // --------------------
            'MM/DD/YYYY',
            'MM/DD/YYYY HH:mm',
            'MM/DD/YYYY HH:mm:ss',
            'MM/DD/YYYY HH:mm Z',
            'MM-DD-YYYY',
            'MM-DD-YYYY HH:mm',
            'MM-DD-YYYY HH:mm:ss',
            'MM-DD-YYYY HH:mm Z',
            // --------------------
            'YYYY/MM/DD',
            'YYYY/MM/DD HH:mm',
            'YYYY/MM/DD HH:mm:ss',
            'YYYY/MM/DD HH:mm Z',
            'YYYY-MM-DD',
            'YYYY-MM-DD HH:mm',
            'YYYY-MM-DD HH:mm:ss',
            'YYYY-MM-DD HH:mm Z',
            // --------------------
            'YYYY/DD/MM',
            'YYYY/DD/MM HH:mm',
            'YYYY/DD/MM HH:mm:ss',
            'YYYY/DD/MM HH:mm Z',
            'YYYY-DD-MM',
            'YYYY-DD-MM HH:mm',
            'YYYY-DD-MM HH:mm:ss',
            'YYYY-DD-MM HH:mm Z',
        ];
    }
    BaseComponent.prototype.resetAfterSave = function () {
        this.isSaving = false;
        this.isSubmitted = false;
    };
    BaseComponent.prototype.formatString = function (message) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        args.forEach(function (value, index) {
            var pattern = new RegExp("\\{" + index + "\\}", 'g');
            message = message.replace(pattern, value);
        });
        return message;
    };
    // showWarningBox(title: string, message: string) {
    //     this.showAlertBox(title, message, 'warning');
    // }
    //
    // showSuccessBox(title: string, message: string) {
    //     this.showAlertBox(title, message, 'success');
    // }
    //
    // showDangerBox(title: string, message: string) {
    //     this.showAlertBox(title, message, 'error');
    // }
    //
    // showInfoBox(title: string, message: string) {
    //     this.showAlertBox(title, message, 'info');
    // }
    // showAlertBox(title: string, message: string, type: any = 'success') {
    //     setTimeout(() => {
    //         swal({
    //             title: title,
    //             text: message,
    //             type: type,
    //             timer: 1500,
    //             showConfirmButton: false
    //         }).then(() => {
    //         }, () => {
    //         });
    //     });
    // }
    BaseComponent.prototype.getListOrderNumber = function (currentPage, pageSize, index) {
        return (currentPage - 1) * pageSize + index + 1;
    };
    return BaseComponent;
}());



/***/ }),

/***/ "./src/app/modules/hr/organization/office/services/office-position.service.ts":
/*!************************************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/services/office-position.service.ts ***!
  \************************************************************************************/
/*! exports provided: OfficePositionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfficePositionService", function() { return OfficePositionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../../environments/environment */ "./src/environments/environment.ts");








var OfficePositionService = /** @class */ (function () {
    function OfficePositionService(appConfig, spinnerService, toastr, http) {
        this.appConfig = appConfig;
        this.spinnerService = spinnerService;
        this.toastr = toastr;
        this.http = http;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].apiGatewayUrl + "api/v1/hr/offices-positions";
    }
    OfficePositionService.prototype.insert = function (officeId, positionIds) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .post(this.url + "/" + officeId, positionIds)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    OfficePositionService.prototype.delete = function (positionId, officeId) {
        var _this = this;
        this.spinnerService.show();
        return this.http.delete(this.url + "/" + officeId + "/" + positionId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    OfficePositionService.prototype.search = function (keyword, officeId, page, pageSize) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('officeId', officeId.toString())
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString())
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    OfficePositionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_3__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_5__["SpinnerService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], OfficePositionService);
    return OfficePositionService;
}());



/***/ }),

/***/ "./src/app/modules/hr/organization/office/services/office.service.ts":
/*!***************************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/services/office.service.ts ***!
  \***************************************************************************/
/*! exports provided: OfficeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfficeService", function() { return OfficeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../shareds/components/nh-suggestion/nh-suggestion.component */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.component.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../../environments/environment */ "./src/environments/environment.ts");









var OfficeService = /** @class */ (function () {
    function OfficeService(appConfig, spinnerService, toastr, http) {
        this.appConfig = appConfig;
        this.spinnerService = spinnerService;
        this.toastr = toastr;
        this.http = http;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].apiGatewayUrl + "api/v1/hr/offices";
    }
    OfficeService.prototype.resolve = function (route, state) {
        var queryParams = route.queryParams;
        return this.search(queryParams.keyword, queryParams.isActive, queryParams.page, queryParams.pageSize);
    };
    OfficeService.prototype.search = function (keyword, isActive, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        return this.http
            .get(this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('isActive', isActive != null ? isActive.toString() : '')
                .set('page', page ? page.toString() : '0')
                .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString())
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            result.items.forEach(function (item) {
                item.activeStatus = item.isActive
                    ? 'active'
                    : 'inActive';
                var level = item.idPath.split('.');
                item.nameLevel = '';
                for (var i = 1; i < level.length; i++) {
                    item.nameLevel += '<i class="fa fa-long-arrow-right cm-mgr-5"></i>';
                }
            });
            return result;
        }));
    };
    OfficeService.prototype.searchName = function (keyword, isActive, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 20; }
        return this.http.get(this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('isActive', isActive != null ? isActive.toString() : '')
                .set('page', page ? page.toString() : '0')
                .set('pageSize', pageSize ? pageSize.toString() : '10')
        });
    };
    OfficeService.prototype.getDetail = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/" + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            var data = result.data;
            data.activeStatus = data.isActive ? 'active' : 'inActive';
            return result;
        }));
    };
    OfficeService.prototype.getEditDetail = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/edit/" + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            var data = result.data;
            data.activeStatus = data.isActive ? 'active' : 'inActive';
            return result;
        }));
    };
    OfficeService.prototype.insert = function (office) {
        var _this = this;
        return this.http.post("" + this.url, {
            isActive: office.isActive,
            code: office.code,
            officeType: office.officeType,
            parentId: office.parentId,
            officeTranslations: office.modelTranslations,
            officeContacts: office.officeContacts
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    OfficeService.prototype.update = function (id, office) {
        var _this = this;
        return this.http.post(this.url + "/" + id, {
            isActive: office.isActive,
            code: office.code,
            officeType: office.officeType,
            parentId: office.parentId,
            officeTranslations: office.modelTranslations,
            officeContacts: office.officeContacts
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    // updateIsActive(office: Office): Observable<IActionResultResponse> {
    //     return this.http.post(`${this.url}update-active-status`, '', {
    //         params: new HttpParams()
    //             .set('id', office.id.toString())
    //             .set('isActive', office.isActive.toString())
    //     }) as Observable<IActionResultResponse>;
    // }
    OfficeService.prototype.delete = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.delete(this.url + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    OfficeService.prototype.getTree = function () {
        return this.http.get(this.url + "/trees");
    };
    OfficeService.prototype.getOfficeUserTree = function () {
        return this.http.get(this.url + "/user-trees");
    };
    OfficeService.prototype.getOfficeUserTreeLazy = function (parentId) {
        return this.http.get("" + this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]().set('parentId', parentId ? parentId.toString() : '')
        });
    };
    OfficeService.prototype.searchForSuggestion = function (keyword) {
        return this.http
            .get(this.url + "/suggestions", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            return result.items.map(function (item) {
                return new _shareds_components_nh_suggestion_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_7__["NhSuggestion"](item.id, item.name);
            });
        }));
    };
    // Contacts.
    OfficeService.prototype.updateContact = function (officeId, id, contact) {
        var _this = this;
        return this.http
            .post(this.url + "/" + officeId + "/contacts/" + id, {
            userId: contact.userId,
            email: contact.email,
            phoneNumber: contact.phoneNumber,
            fax: contact.fax
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    OfficeService.prototype.addContact = function (officeId, contact) {
        var _this = this;
        return this.http
            .post(this.url + "/" + officeId + "/contacts", {
            userId: contact.userId,
            email: contact.email,
            phoneNumber: contact.phoneNumber,
            fax: contact.fax
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    OfficeService.prototype.deleteContact = function (officeId, contactId) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .delete(this.url + "/" + officeId + "/contacts/" + contactId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    OfficeService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_4__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_6__["SpinnerService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], OfficeService);
    return OfficeService;
}());



/***/ }),

/***/ "./src/app/modules/hr/organization/position/position.service.ts":
/*!**********************************************************************!*\
  !*** ./src/app/modules/hr/organization/position/position.service.ts ***!
  \**********************************************************************/
/*! exports provided: PositionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PositionService", function() { return PositionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/internal/operators */ "./node_modules/rxjs/internal/operators/index.js");
/* harmony import */ var rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../environments/environment */ "./src/environments/environment.ts");









var PositionService = /** @class */ (function () {
    function PositionService(appConfig, toastr, spinnerService, http) {
        this.appConfig = appConfig;
        this.toastr = toastr;
        this.spinnerService = spinnerService;
        this.http = http;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].apiGatewayUrl + "api/v1/hr/positions";
    }
    PositionService.prototype.resolve = function (route, state) {
        var queryParams = route.queryParams;
        return this.search(queryParams.keyword, queryParams.isManager, queryParams.isMultiple, queryParams.isActive, queryParams.page, queryParams.pageSize);
    };
    PositionService.prototype.search = function (keyword, isManager, isMultiple, isActive, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        return this.http.get(this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('isManager', isManager !== undefined && isManager != null ? isManager.toString() : '')
                .set('isMultiple', isMultiple !== undefined && isMultiple != null ? isMultiple.toString() : '')
                .set('isActive', isActive !== undefined && isActive != null ? isActive.toString() : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString())
        });
    };
    PositionService.prototype.searchForSuggestion = function (keyword) {
        return this.http.get(this.url + "/suggestions", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
        });
    };
    PositionService.prototype.getAll = function () {
        return this.http.get("" + this.url);
    };
    PositionService.prototype.getDetail = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/" + id)
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    PositionService.prototype.insert = function (position) {
        var _this = this;
        return this.http.post("" + this.url, position)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    PositionService.prototype.update = function (position) {
        var _this = this;
        return this.http.post(this.url + "/" + position.id, position).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    PositionService.prototype.delete = function (id) {
        var _this = this;
        return this.http.delete("" + this.url + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    PositionService.prototype.getTitleByPositionId = function (id) {
        return this.http.get(this.url + "/titles/" + id);
    };
    PositionService.prototype.getAllActivated = function () {
        return this.http.get(this.url + "/activated");
    };
    PositionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_4__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_6__["SpinnerService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], PositionService);
    return PositionService;
}());



/***/ }),

/***/ "./src/app/modules/hr/organization/title/title.service.ts":
/*!****************************************************************!*\
  !*** ./src/app/modules/hr/organization/title/title.service.ts ***!
  \****************************************************************/
/*! exports provided: TitleService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TitleService", function() { return TitleService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../environments/environment */ "./src/environments/environment.ts");








var TitleService = /** @class */ (function () {
    function TitleService(appConfig, http, spinnerService, toastr) {
        this.appConfig = appConfig;
        this.http = http;
        this.spinnerService = spinnerService;
        this.toastr = toastr;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].apiGatewayUrl + "api/v1/hr/titles";
    }
    TitleService.prototype.resolve = function (route, state) {
        var queryParams = route.queryParams;
        return this.search(queryParams.keyword, queryParams.isActive, queryParams.page, queryParams.pageSize);
    };
    TitleService.prototype.search = function (keyword, isActive, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 20; }
        return this.http.get("" + this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('isActive', isActive != null ? isActive.toString() : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString())
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) {
            result.items.forEach(function (item) {
                item.activeStatus = item.isActive ? 'active' : 'inActive';
            });
            return result;
        }));
    };
    TitleService.prototype.getAllActivated = function () {
        return this.http.get(this.url + "/activated");
    };
    TitleService.prototype.getDetail = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    TitleService.prototype.insert = function (title) {
        var _this = this;
        return this.http.post(this.url, title).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    TitleService.prototype.update = function (id, title) {
        var _this = this;
        return this.http.post(this.url + "/" + id, title).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    TitleService.prototype.delete = function (id) {
        var _this = this;
        return this.http.delete(this.url + "/" + id, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('id', id.toString())
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    TitleService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_4__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_6__["SpinnerService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"]])
    ], TitleService);
    return TitleService;
}());



/***/ }),

/***/ "./src/app/shareds/components/tinymce/tinymce.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/shareds/components/tinymce/tinymce.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "div.tinymce-editor {\n  height: auto !important; }\n  div.tinymce-editor p {\n    margin: 0 0 !important; }\n  .mce-fullscreen {\n  margin-top: 50px !important;\n  border: 0;\n  padding: 0;\n  margin: 0;\n  overflow: hidden;\n  height: 100%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL3RpbnltY2UvRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFwcFxcc2hhcmVkc1xcY29tcG9uZW50c1xcdGlueW1jZVxcdGlueW1jZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHVCQUF1QixFQUFBO0VBRDNCO0lBR1Esc0JBQXNCLEVBQUE7RUFJOUI7RUFDSSwyQkFBMkI7RUFDM0IsU0FBUztFQUNULFVBQVU7RUFDVixTQUFTO0VBQ1QsZ0JBQWdCO0VBQ2hCLFlBQVksRUFBQSIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZHMvY29tcG9uZW50cy90aW55bWNlL3RpbnltY2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJkaXYudGlueW1jZS1lZGl0b3Ige1xyXG4gICAgaGVpZ2h0OiBhdXRvICFpbXBvcnRhbnQ7XHJcbiAgICBwIHtcclxuICAgICAgICBtYXJnaW46IDAgMCAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG59XHJcblxyXG4ubWNlLWZ1bGxzY3JlZW4ge1xyXG4gICAgbWFyZ2luLXRvcDogNTBweCAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyOiAwO1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/shareds/components/tinymce/tinymce.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/shareds/components/tinymce/tinymce.component.ts ***!
  \*****************************************************************/
/*! exports provided: TinymceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TinymceComponent", function() { return TinymceComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");



var TinymceComponent = /** @class */ (function () {
    function TinymceComponent() {
        this.inline = false;
        this.menu = {
            file: { title: 'File', items: 'newdocument | print' },
            edit: { title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall' },
            // insert: {title: 'Insert', items: 'link media | template hr '},
            view: { title: 'View', items: 'visualaid | preview | fullscreen ' },
            format: {
                title: 'Format',
                items: 'bold italic underline strikethrough superscript subscript | formats | removeformat '
            },
            table: { title: 'Table', items: 'inserttable tableprops deletetable | cell row column' },
            tools: { title: 'Tools', items: 'code ' }
        };
        this.onEditorKeyup = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onBlur = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.propagateChange = function () {
        };
    }
    TinymceComponent_1 = TinymceComponent;
    Object.defineProperty(TinymceComponent.prototype, "content", {
        get: function () {
            return this._content;
        },
        set: function (val) {
            this._content = val;
        },
        enumerable: true,
        configurable: true
    });
    TinymceComponent.prototype.ngAfterViewInit = function () {
        this.initEditor();
    };
    TinymceComponent.prototype.ngOnDestroy = function () {
        tinymce.remove("" + this.elementId);
    };
    TinymceComponent.prototype.initEditor = function () {
        var _this = this;
        setTimeout(function () {
            tinymce.remove("#" + _this.elementId);
            tinymce.init({
                selector: "#" + _this.elementId,
                plugins: ['fullscreen', 'link', 'autolink', 'paste', 'image', 'table', 'textcolor', 'print', 'preview', 'spellchecker',
                    'colorpicker', 'fullscreen', 'code', 'lists', 'emoticons', 'wordcount'],
                toolbar: 'insertfile undo redo | | fontselect | fontsizeselect | bold italic ' +
                    '| alignleft aligncenter alignright alignjustify ' +
                    '| bullist numlist outdent indent | link image | fullscreen',
                fontsize_formats: '8pt 9pt 10pt 11pt 12pt 13pt 14pt 18pt 24pt 36pt',
                skin_url: '/assets/skins/lightgray',
                menu: _this.menu,
                inline: _this.inline,
                setup: function (editor) {
                    _this.editor = editor;
                    editor.on('keyup', function (event) {
                        var content = editor.getContent();
                        _this.content = content;
                        _this.propagateChange(content);
                        _this.onEditorKeyup.emit({
                            text: editor.getContent({ format: 'text' }),
                            content: _this.content
                        });
                    });
                    editor.on('change', function (event) {
                        var contentChange = editor.getContent();
                        _this.content = contentChange;
                        _this.propagateChange(_this.content);
                        _this.onChange.emit({
                            text: editor.getContent({ format: 'text' }),
                            content: _this.content
                        });
                    });
                    editor.on('blur', function (event) {
                        var contentChange = editor.getContent();
                        _this.content = contentChange;
                        _this.propagateChange(_this.content);
                        _this.onBlur.emit({
                            text: editor.getContent({ format: 'text' }),
                            content: _this.content
                        });
                    });
                }
            });
        });
    };
    TinymceComponent.prototype.setContent = function (content) {
        this.content = content;
        var editor = tinymce.get(this.elementId);
        if (editor != null) {
            editor.setContent(this.content != null ? this.content : '');
        }
    };
    TinymceComponent.prototype.append = function (data, editorId) {
        var editor = !editorId ? tinymce.get(this.elementId) : tinymce.get(editorId);
        if (editor != null) {
            editor.execCommand('mceInsertContent', false, data);
        }
    };
    TinymceComponent.prototype.registerOnChange = function (fn) {
        this.propagateChange = fn;
    };
    TinymceComponent.prototype.destroy = function () {
        tinymce.remove("#" + this.elementId);
    };
    TinymceComponent.prototype.writeValue = function (value) {
        this.content = value;
        var editor = tinymce.get(this.elementId);
        this.initEditor();
    };
    TinymceComponent.prototype.registerOnTouched = function () {
    };
    var TinymceComponent_1;
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], TinymceComponent.prototype, "elementId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], TinymceComponent.prototype, "height", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TinymceComponent.prototype, "inline", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TinymceComponent.prototype, "menu", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TinymceComponent.prototype, "onEditorKeyup", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TinymceComponent.prototype, "onChange", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TinymceComponent.prototype, "onBlur", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
    ], TinymceComponent.prototype, "content", null);
    TinymceComponent = TinymceComponent_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            selector: 'tinymce',
            template: "\n        <div class=\"form-control tinymce-editor\" id=\"{{elementId}}\" *ngIf=\"inline\"\n             [ngStyle]=\"{'height': height + 'px'}\">\n            <span [innerHTML]=\"content\"></span>\n        </div>\n        <textarea *ngIf=\"!inline\" id=\"{{elementId}}\" [ngStyle]=\"{'height': height + 'px'}\"\n                  value=\"{{content}}\"></textarea>\n    ",
            providers: [
                { provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALUE_ACCESSOR"], useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () { return TinymceComponent_1; }), multi: true }
            ],
            styles: [__webpack_require__(/*! ./tinymce.component.scss */ "./src/app/shareds/components/tinymce/tinymce.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TinymceComponent);
    return TinymceComponent;
}());



/***/ }),

/***/ "./src/app/shareds/components/tinymce/tinymce.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/shareds/components/tinymce/tinymce.module.ts ***!
  \**************************************************************/
/*! exports provided: TinymceModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TinymceModule", function() { return TinymceModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _tinymce_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tinymce.component */ "./src/app/shareds/components/tinymce/tinymce.component.ts");




var TinymceModule = /** @class */ (function () {
    function TinymceModule() {
    }
    TinymceModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]],
            exports: [_tinymce_component__WEBPACK_IMPORTED_MODULE_3__["TinymceComponent"]],
            declarations: [_tinymce_component__WEBPACK_IMPORTED_MODULE_3__["TinymceComponent"]],
            providers: [],
        })
    ], TinymceModule);
    return TinymceModule;
}());



/***/ })

}]);
//# sourceMappingURL=default~modules-hr-organization-organization-module~modules-hr-user-user-module~modules-warehouse-go~a430b70d.js.map