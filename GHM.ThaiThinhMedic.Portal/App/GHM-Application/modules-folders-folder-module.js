(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-folders-folder-module"],{

/***/ "./src/app/modules/folders/file-form/file-form.component.html":
/*!********************************************************************!*\
  !*** ./src/app/modules/folders/file-form/file-form.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #fileFormModal size=\"lg\"\r\n          (show)=\"onFormModalShown()\"\r\n          (hidden)=\"onFormModalHidden()\">\r\n    <nh-modal-header class=\"uppercase\">\r\n        {isUpdate, select, 0 {Add new file} 1 {Update file} other {}}\r\n    </nh-modal-header>\r\n    <form class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content>\r\n            <div class=\"col-sm-12\">\r\n                <div class=\"form-group\">\r\n                    <label i18n-ghmLabel=\"@@fileName\" ghmLabel=\"File Name\" class=\"control-label\"\r\n                           [required]=\"true\"></label>\r\n                    <div [class.has-error]=\"formErrors?.name\">\r\n                        <input type=\"text\" class=\"form-control\" i18n-placeholder=\"@@fileNamePlaceHolder\"\r\n                               placeholder=\"Please enter file name\"\r\n                               formControlName=\"name\"/>\r\n                        <span class=\"help-block\">{ formErrors?.name, select, required {Name is required} maxlength {Name not allowed\r\n                                                    over 256 characters} }</span>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\" [formGroup]=\"model\">\r\n                    <label i18n-ghmLabel=\"@@folder\" class=\"control-label\" ghmLabel=\"Folder\"\r\n                           [required]=\"true\"></label>\r\n                    <div [class.has-error]=\"formErrors?.folderId\">\r\n                        <nh-dropdown-tree\r\n                            [width]=\"350\"\r\n                            [data]=\"folderTree\"\r\n                            i18n-title=\"@@folderTitle\"\r\n                            title=\"-- Please select folder --\"\r\n                            formControlName=\"folderId\">\r\n                        </nh-dropdown-tree>\r\n                        <span class=\"help-block\">{formErrors?.folderId, select, required {Folder is required}}\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <div class=\"pull-right cm-mgb-10\">\r\n                <mat-checkbox [checked]=\"isCreateAnother\" (change)=\"isCreateAnother = !isCreateAnother\"\r\n                              *ngIf=\"!isUpdate\"\r\n                              i18n=\"@@isCreateAnother\"\r\n                              class=\"cm-mgr-5\"\r\n                              color=\"primary\">\r\n                    Create another\r\n                </mat-checkbox>\r\n                <ghm-button classes=\"btn blue cm-mgr-5\"\r\n                            [loading]=\"isSaving\">\r\n                    <span i18n=\"@@Save\">Save</span>\r\n                </ghm-button>\r\n                <ghm-button classes=\"btn default\"\r\n                            nh-dismiss=\"true\"\r\n                            [type]=\"'button'\"\r\n                            [loading]=\"isSaving\">\r\n                    <span i18n=\"@@cancel\">Cancel</span>\r\n                </ghm-button>\r\n            </div>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/folders/file-form/file-form.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/modules/folders/file-form/file-form.component.ts ***!
  \******************************************************************/
/*! exports provided: FileFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileFormComponent", function() { return FileFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _model_file_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../model/file.model */ "./src/app/modules/folders/model/file.model.ts");
/* harmony import */ var _service_file_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../service/file.service */ "./src/app/modules/folders/service/file.service.ts");
/* harmony import */ var _service_folder_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../service/folder.service */ "./src/app/modules/folders/service/folder.service.ts");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../configs/app.config */ "./src/app/configs/app.config.ts");











var FileFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](FileFormComponent, _super);
    function FileFormComponent(appConfig, fb, utilService, folderService, fileService) {
        var _this = _super.call(this) || this;
        _this.appConfig = appConfig;
        _this.fb = fb;
        _this.utilService = utilService;
        _this.folderService = folderService;
        _this.fileService = fileService;
        _this.onSaveSuccess = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.file = new _model_file_model__WEBPACK_IMPORTED_MODULE_7__["Files"]();
        _this.urlUpload = _this.appConfig.FILE_MANAGEMENT + "uploads";
        return _this;
    }
    FileFormComponent.prototype.ngOnInit = function () {
        this.renderForm();
    };
    FileFormComponent.prototype.onFormModalShown = function () {
        // this.getFolderTree();
        this.isModified = false;
        this.renderForm();
    };
    FileFormComponent.prototype.onFormModalHidden = function () {
        this.isUpdate = false;
        this.resetForm();
        if (this.isModified) {
            this.onSaveSuccess.emit();
        }
    };
    FileFormComponent.prototype.add = function () {
        this.renderForm();
        this.isUpdate = false;
        this.fileFormModal.open();
    };
    FileFormComponent.prototype.edit = function (id) {
        this.isUpdate = true;
        this.id = id;
        this.getDetail(id);
        this.fileFormModal.open();
    };
    FileFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        var isLanguageValid = this.checkLanguageValid();
        if (isValid && isLanguageValid) {
            this.file = this.model.value;
            this.isSaving = true;
            if (this.isUpdate) {
                this.fileService.update(this.id, this.file)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.isModified = true;
                    _this.fileFormModal.dismiss();
                });
            }
            else {
                this.fileService.insert(this.file)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.isModified = true;
                    if (_this.isCreateAnother) {
                        _this.resetForm();
                    }
                    else {
                        _this.fileFormModal.dismiss();
                    }
                });
            }
        }
    };
    FileFormComponent.prototype.getDetail = function (id) {
        var _this = this;
        this.fileService
            .getDetail(id)
            .subscribe(function (result) {
            var fileDetail = result;
            if (fileDetail) {
                _this.model.patchValue({
                    name: fileDetail.name,
                    folderId: fileDetail.folderId,
                    concurrencyStamp: fileDetail.concurrencyStamp,
                });
            }
        });
    };
    FileFormComponent.prototype.closeModal = function () {
        this.fileFormModal.dismiss();
    };
    FileFormComponent.prototype.renderForm = function () {
        this.buildForm();
    };
    FileFormComponent.prototype.getFolderTree = function () {
        var _this = this;
        this.folderService.getTree().subscribe(function (result) {
            _this.folderTree = result;
        });
    };
    FileFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.utilService.renderFormError(['folderId', 'name']);
        this.validationMessages = this.utilService.renderFormErrorMessage([
            {
                'folderId': ['required'],
                'name': ['required', 'maxLength']
            },
        ]);
        this.model = this.fb.group({
            name: [this.file.name, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength]],
            folderId: [this.file.folderId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required
                ]],
            concurrencyStamp: [this.file.concurrencyStamp],
        });
        this.model.valueChanges.subscribe(function () { return _this.utilService.onValueChanged(_this.model, _this.formErrors, _this.validationMessages); });
    };
    FileFormComponent.prototype.resetForm = function () {
        this.id = null;
        this.model.patchValue({
            name: '',
            folderId: -1,
            concurrencyStamp: '',
        });
        this.clearFormError(this.formErrors);
        this.clearFormError(this.translationFormErrors);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileFormModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__["NhModalComponent"])
    ], FileFormComponent.prototype, "fileFormModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], FileFormComponent.prototype, "onSaveSuccess", void 0);
    FileFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-file-form',
            template: __webpack_require__(/*! ./file-form.component.html */ "./src/app/modules/folders/file-form/file-form.component.html"),
            providers: [_service_file_service__WEBPACK_IMPORTED_MODULE_8__["FileService"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_10__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_5__["UtilService"],
            _service_folder_service__WEBPACK_IMPORTED_MODULE_9__["FolderService"],
            _service_file_service__WEBPACK_IMPORTED_MODULE_8__["FileService"]])
    ], FileFormComponent);
    return FileFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/folders/folder-form/folder-form.component.html":
/*!************************************************************************!*\
  !*** ./src/app/modules/folders/folder-form/folder-form.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #folderFormModal size=\"lg\"\r\n          (shown)=\"onModalShow()\"\r\n          (hidden)=\"onModalHidden()\">\r\n    <nh-modal-header class=\"uppercase\">\r\n        {isUpdate, select, 0 {Add new folder} 1 {Update folder} other {}}\r\n    </nh-modal-header>\r\n    <form class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content>\r\n            <div class=\"col-sm-12\" [formGroup]=\"model\">\r\n                <div class=\"form-group cm-mgb-10\">\r\n                    <label i18n-ghmLabel=\"@@folderName\" ghmLabel=\"Folder Name\" *ngIf=\"isUpdate\"\r\n                           class=\"control-label\" [required]=\"true\"></label>\r\n                    <div class=\"\" [class.has-error]=\"formErrors?.name\">\r\n                        <input type=\"text\" class=\"form-control\"\r\n                               id=\"name\"\r\n                               i18n-placeholder=\"@@enterFolderNamePlaceHolder\"\r\n                               placeholder=\"Enter folder name.\"\r\n                               formControlName=\"name\">\r\n                        <span class=\"help-block\">\r\n                               {formErrors?.name, select, required {Folder name is required} maxLength {Folder name\r\n                                  name not allowed over 300 characters}}\r\n                             </span>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group cm-mgb-10\" *ngIf=\"isUpdate\">\r\n                    <label i18n-ghmLabel=\"@@folder\" ghmLabel=\"Folder\"\r\n                           class=\"control-label\"></label>\r\n                    <div class=\"\" [formGroup]=\"model\">\r\n                        <nh-dropdown-tree\r\n                            [width]=\"500\"\r\n                            [data]=\"folderTree\" i18n-title=\"@@selectFolderGroup\"\r\n                            title=\"-- Select Folder --\"\r\n                            formControlName=\"parentId\">\r\n                        </nh-dropdown-tree>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <div class=\"pull-right cm-mgb-10\">\r\n                <ghm-button classes=\"btn blue cm-mgr-5\"\r\n                            [loading]=\"isSaving\">\r\n                    <span i18n=\"@@Save\">Save</span>\r\n                </ghm-button>\r\n                <ghm-button classes=\"btn default\"\r\n                            nh-dismiss=\"true\"\r\n                            [type]=\"'button'\"\r\n                            [loading]=\"isSaving\">\r\n                    <span i18n=\"@@cancel\">Cancel</span>\r\n                </ghm-button>\r\n            </div>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/folders/folder-form/folder-form.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/modules/folders/folder-form/folder-form.component.ts ***!
  \**********************************************************************/
/*! exports provided: FolderFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FolderFormComponent", function() { return FolderFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _configs_page_id_config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../configs/page-id.config */ "./src/app/configs/page-id.config.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _service_folder_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../service/folder.service */ "./src/app/modules/folders/service/folder.service.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _model_folder_model__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../model/folder.model */ "./src/app/modules/folders/model/folder.model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");










var FolderFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](FolderFormComponent, _super);
    function FolderFormComponent(pageId, fb, folderService, utilService) {
        var _this = _super.call(this) || this;
        _this.fb = fb;
        _this.folderService = folderService;
        _this.utilService = utilService;
        _this.onEditorKeyup = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.onCloseForm = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.folder = new _model_folder_model__WEBPACK_IMPORTED_MODULE_8__["Folder"]();
        _this.isGettingTree = false;
        return _this;
    }
    FolderFormComponent.prototype.ngOnInit = function () {
        this.renderForm();
    };
    FolderFormComponent.prototype.onModalShow = function () {
        // this.getFolderTree();
        this.isModified = false;
    };
    FolderFormComponent.prototype.onModalHidden = function () {
        this.isUpdate = false;
        this.resetForm();
    };
    FolderFormComponent.prototype.add = function () {
        this.utilService.focusElement('name');
        this.renderForm();
        this.model.patchValue({
            parentId: this.folderId,
            name: ''
        });
        this.folderFormModal.open();
    };
    FolderFormComponent.prototype.edit = function (id) {
        this.utilService.focusElement('name');
        this.isUpdate = true;
        this.id = id;
        this.getDetail(id);
        this.folderFormModal.open();
    };
    FolderFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            this.folder = this.model.value;
            this.isSaving = true;
            if (this.isUpdate) {
                this.folderService.update(this.id, this.folder)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["finalize"])(function () {
                    return _this.isSaving = false;
                })).subscribe(function () {
                    _this.isModified = true;
                    _this.folder.id = _this.id;
                    _this.saveSuccessful.emit(_this.folder);
                    _this.folderFormModal.dismiss();
                });
            }
            else {
                this.folder.parentId = this.folderId;
                this.folderService.insert(this.folder)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["finalize"])(function () {
                    return _this.isSaving = false;
                })).subscribe(function () {
                    _this.isModified = true;
                    if (_this.isCreateAnother) {
                        _this.resetForm();
                        _this.saveSuccessful.emit(_this.folder);
                        _this.utilService.focusElement('name');
                    }
                    else {
                        _this.saveSuccessful.emit(_this.folder);
                        _this.resetForm();
                        _this.folderFormModal.dismiss();
                    }
                });
            }
        }
    };
    FolderFormComponent.prototype.closeForm = function () {
        this.onCloseForm.emit();
    };
    FolderFormComponent.prototype.onParentSelect = function (folder) {
        this.model.patchValue({ parentId: folder ? folder.id : null });
    };
    FolderFormComponent.prototype.getDetail = function (id) {
        var _this = this;
        this.folderService.getDetail(id).subscribe(function (result) {
            var folderDetail = result.data;
            if (folderDetail) {
                _this.model.patchValue({
                    name: folderDetail.name,
                    parentId: folderDetail.parentId,
                    concurrencyStamp: folderDetail.concurrencyStamp,
                });
            }
        });
    };
    FolderFormComponent.prototype.getFolderTree = function () {
        var _this = this;
        this.isGettingTree = true;
        this.folderService.getTree().subscribe(function (result) {
            _this.isGettingTree = false;
            _this.folderTree = result;
        });
    };
    FolderFormComponent.prototype.renderForm = function () {
        this.buildForm();
    };
    FolderFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.utilService.renderFormError([
            'name',
            'description',
        ]);
        this.validationMessages = this.utilService.renderFormErrorMessage([
            { 'name': ['required', 'maxLength'] },
            { 'description': ['maxLength'] },
        ]);
        this.model = this.fb.group({
            parentId: [this.folder.parentId],
            name: [this.folder.name, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].maxLength(300)
                ]],
            concurrencyStamp: [this.folder.concurrencyStamp]
        });
        this.model.valueChanges.subscribe(function (data) { return _this.validateModel(false); });
    };
    FolderFormComponent.prototype.resetForm = function () {
        this.id = null;
        this.model.patchValue({
            parentId: null,
            name: '',
            description: ''
        });
        this.clearFormError(this.formErrors);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('folderFormModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__["NhModalComponent"])
    ], FolderFormComponent.prototype, "folderFormModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], FolderFormComponent.prototype, "elementId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], FolderFormComponent.prototype, "onEditorKeyup", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], FolderFormComponent.prototype, "onCloseForm", void 0);
    FolderFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-folder-form',
            template: __webpack_require__(/*! ./folder-form.component.html */ "./src/app/modules/folders/folder-form/folder-form.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_page_id_config__WEBPACK_IMPORTED_MODULE_4__["PAGE_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
            _service_folder_service__WEBPACK_IMPORTED_MODULE_6__["FolderService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_7__["UtilService"]])
    ], FolderFormComponent);
    return FolderFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/folders/folder-routing.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/modules/folders/folder-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: FolderRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FolderRoutingModule", function() { return FolderRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _folder_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./folder.component */ "./src/app/modules/folders/folder.component.ts");
/* harmony import */ var _service_folder_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./service/folder.service */ "./src/app/modules/folders/service/folder.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");





var routes = [
    {
        path: '',
        component: _folder_component__WEBPACK_IMPORTED_MODULE_2__["FolderComponent"],
        resolve: {
            data: _service_folder_service__WEBPACK_IMPORTED_MODULE_3__["FolderService"]
        }
    }
];
var FolderRoutingModule = /** @class */ (function () {
    function FolderRoutingModule() {
    }
    FolderRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
            providers: [_service_folder_service__WEBPACK_IMPORTED_MODULE_3__["FolderService"]]
        })
    ], FolderRoutingModule);
    return FolderRoutingModule;
}());



/***/ }),

/***/ "./src/app/modules/folders/folder-tree/folder-tree.component.html":
/*!************************************************************************!*\
  !*** ./src/app/modules/folders/folder-tree/folder-tree.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ul class=\"folder-tree\">\r\n    <li *ngFor=\"let item of listData; \">\r\n        <i class=\"fa fa\"></i>\r\n    </li>\r\n</ul>"

/***/ }),

/***/ "./src/app/modules/folders/folder-tree/folder-tree.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/modules/folders/folder-tree/folder-tree.component.ts ***!
  \**********************************************************************/
/*! exports provided: FolderTreeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FolderTreeComponent", function() { return FolderTreeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FolderTreeComponent = /** @class */ (function () {
    function FolderTreeComponent() {
    }
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], FolderTreeComponent.prototype, "listData", void 0);
    FolderTreeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-folder-tree',
            template: __webpack_require__(/*! ./folder-tree.component.html */ "./src/app/modules/folders/folder-tree/folder-tree.component.html"),
            styles: [__webpack_require__(/*! ../folder.scss */ "./src/app/modules/folders/folder.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FolderTreeComponent);
    return FolderTreeComponent;
}());



/***/ }),

/***/ "./src/app/modules/folders/folder.component.html":
/*!*******************************************************!*\
  !*** ./src/app/modules/folders/folder.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <small i18n=\"@@folderManager\">Quản lý văn bản</small>\r\n</h1>\r\n<div class=\"inbox\">\r\n    <div class=\"row\">\r\n        <div class=\"col-lg-2 col-sm-3\">\r\n            <div class=\"inbox-sidebar\">\r\n                <div class=\"home-dictionary\">\r\n                    <a href=\"javascript://\" (click)=\"showFolderInHome()\"><i class=\"fa fa-home blue\"></i> Home</a>\r\n                </div>\r\n                <nh-tree\r\n                    [height]=\"height + 70\"\r\n                    [data]=\"folderTree\"\r\n                    (nodeExpanded)=\"expandedFolder($event)\"\r\n                    (nodeSelected)=\"onSelectNode($event)\">\r\n                </nh-tree>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-lg-10 col-sm-9 cm-pdl-0\" [ngStyle]=\"{'height': height + 100 + 'px'}\">\r\n            <div class=\"inbox-body\">\r\n                <div class=\"form-inline\">\r\n                    <div class=\"form-group cm-mgr-5\">\r\n                        <button class=\"btn default\" i18n=\"@@addFolder\" (click)=\"addFolder()\" type=\"button\">\r\n                            <i class=\"fa fa-folder\"></i> Add Folder\r\n                        </button>\r\n                    </div>\r\n                    <div class=\"form-group cm-mgr-5 cm-mgb-0\">\r\n                        <ghm-file-upload [multiple]=\"true\" [folderId]=\"folderId\"\r\n                        ></ghm-file-upload>\r\n                    </div>\r\n                    <div class=\"form-group pull-right cm-mgr-5\">\r\n                        <button class=\"btn default\" (click)=\"onViewType()\" type=\"button\"\r\n                                [matTooltip]=\"'View type'\"\r\n                                [matTooltipPosition]=\"'below'\">\r\n                            <i class=\"fa fa-th-list\" aria-hidden=\"true\" *ngIf=\"type === viewType.list\"></i>\r\n                            <i class=\"fa fa-th-large\" aria-hidden=\"true\" *ngIf=\"type === viewType.gird\"></i>\r\n                        </button>\r\n                    </div>\r\n                </div>\r\n                <div class=\"page-bar\">\r\n                    <form class=\"form-inline\" (ngSubmit)=\"searchByName(1)\">\r\n                        <ul class=\"page-breadcrumb cm-pdl-10\">\r\n                            <li>\r\n                                <a href=\"javascript://\" (click)=\"showFolderInHome()\">Home</a>\r\n                                <i class=\"fa fa-angle-right\" *ngIf=\"listBreadcrumb && listBreadcrumb.length >0\"></i>\r\n                            </li>\r\n                            <li *ngFor=\"let breadcrumb of listBreadcrumb; let i = index\">\r\n                                <a href=\"javascript://\" (click)=\"showByBreadcrumb(breadcrumb.id)\"\r\n                                   *ngIf=\"i < listBreadcrumb?.length - 1; else spanBreadcrumb\">\r\n                                    {{breadcrumb.name}}\r\n                                </a>\r\n                                <i class=\"fa fa-angle-right\" *ngIf=\"i < listBreadcrumb?.length - 1;\"></i>\r\n                                <ng-template #spanBreadcrumb>\r\n                                    <span>{{breadcrumb.name}}</span>\r\n                                </ng-template>\r\n                            </li>\r\n                        </ul>\r\n                        <div class=\"page-toolbar\">\r\n                            <div class=\"input-group input-medium\">\r\n                                <input type=\"text\" class=\"form-control\" i18n-placeholder=\"@@fileSearch\"\r\n                                       placeholder=\"Enter file name.\"\r\n                                       name=\"searchInput\" [(ngModel)]=\"keyword\">\r\n                                <span class=\"input-group-btn\">\r\n                             <button type=\"submit\" class=\"btn blue\">\r\n                                 <i class=\"fa fa-search\"></i>\r\n                             </button>\r\n                         </span>\r\n                            </div>\r\n                        </div>\r\n                    </form>\r\n                </div>\r\n                <div *ngIf=\"type === viewType.list; else viewGird\">\r\n                    <table class=\"table table-striped table-hover\">\r\n                        <thead>\r\n                        <tr>\r\n                            <th class=\"middle\" i18n=\"@@name\">Name</th>\r\n                            <th class=\"middle w150 visible-lg visible-md\" i18n=\"@@createDate\">Create Date</th>\r\n                            <th class=\"middle center w150\" i18n=\"@@size\">Size</th>\r\n                            <th class=\"middle w150\" i18n=\"@@author\">Author</th>\r\n                        </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                        <tr *ngFor=\"let item of listFolder; let i = index\"\r\n                            nhContextMenuTrigger\r\n                            [nhContextMenuTriggerFor]=\"nhMenuFolder\"\r\n                            [nhContextMenuData]=\"item\">\r\n                            <td class=\"middle\">\r\n                                <a *ngIf=\"!item.isEditName; else inputFolder\" (click)=\"showFolderDetail(item)\">\r\n                                    <i class=\"fa fa-folder color-folder\"></i>\r\n                                    {{item.name}}\r\n                                </a>\r\n                                <ng-template #inputFolder>\r\n                                    <input class=\"form-control\" [(ngModel)]=\"item.name\" (blur)=\"updateFolderName(item)\">\r\n                                </ng-template>\r\n                            </td>\r\n                            <td class=\"middle visible-lg visible-md\">{{item.createTime | dateTimeFormat :\r\n                                'DD/MM/YYYY hh:mm'}}\r\n                            </td>\r\n                            <td class=\"middle center\"><span></span></td>\r\n                            <td class=\"middle\">\r\n                                <img ghmImage=\"\" src=\"{{item.creatorAvatar}}\"\r\n                                     class=\"avatar-sm visible-sm visible-xs\">\r\n                                <span class=\"visible-lg visible-md\">\r\n                                    {{item.creatorFullName}}\r\n                                </span>\r\n                            </td>\r\n                        </tr>\r\n                        </tbody>\r\n                        <tbody>\r\n                        <tr *ngFor=\"let item of listFile; let i = index\"\r\n                            nhContextMenuTrigger\r\n                            [nhContextMenuTriggerFor]=\"nhMenuFile\"\r\n                            [nhContextMenuData]=\"item\">\r\n                            <td class=\"middle\">\r\n                                <a *ngIf=\"!item.isEditName; else inputFolder \">\r\n                                    <img *ngIf=\"item.isImage\" ghmImage [errorImageUrl]=\"'/assets/images/no-image.png'\"\r\n                                         [nhImageViewer]=\"item.absoluteUrl\"\r\n                                         class=\"w50\" src=\"{{item.url}}\"/>\r\n                                    <i [class]=\"'at-icon at-icon-' + item.extension.replace('.', '')\"\r\n                                       *ngIf=\"!item.isImage\"></i>\r\n                                    {{item.name}}\r\n                                </a>\r\n                                <ng-template #inputFolder>\r\n                                    <input class=\"form-control\" [(ngModel)]=\"item.name\" (blur)=\"updateFileName(item)\">\r\n                                </ng-template>\r\n                            </td>\r\n                            <td class=\"middle visible-lg visible-md\">{{item.createTime | dateTimeFormat : 'DD/MM/YYYY\r\n                                hh:mm'}}\r\n                            </td>\r\n                            <td class=\"middle center\"><span>{{item.sizeString}}</span></td>\r\n                            <td class=\"middle\">\r\n                                <img ghmImage=\"\" src=\"{{item.creatorAvatar}}\"\r\n                                     class=\"avatar-sm visible-sm visible-xs\">\r\n                                <span class=\"visible-lg visible-md\">\r\n                                    {{item.creatorFullName}}\r\n                                </span>\r\n                            </td>\r\n                        </tr>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n                <ghm-paging *ngIf=\"isSearchName\" [totalRows]=\"totalRows\" [pageSize]=\"pageSize\"\r\n                            [currentPage]=\"currentPage\" [pageShow]=\"6\" (pageClick)=\"searchByName($event)\"\r\n                            [isDisabled]=\"isSearching\" i18n-pageName=\"@@news\" pageName=\"News\"></ghm-paging>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<ng-template #viewGird>\r\n    <div class=\"title\" i18n=\"folder\" *ngIf=\"listFolder && listFolder.length > 0 && !isSearchName\">Folder</div>\r\n    <div class=\"folder-gird-container\" *ngIf=\"listFolder && listFolder.length > 0\">\r\n        <div class=\"folder-gird-item\" *ngFor=\"let item of listFolder; let i = index\"\r\n             title=\"{{item.name}}\"\r\n             nhContextMenuTrigger\r\n             [nhContextMenuTriggerFor]=\"nhMenuFolder\"\r\n             [nhContextMenuData]=\"item\">\r\n            <div class=\"media\" (click)=\"clickFolder(item)\">\r\n                <div class=\"media-left\">\r\n                    <i class=\"fa fa-folder\"></i>\r\n                </div>\r\n                <div class=\"media-body\">\r\n                    <div class=\"name\">\r\n                        <span *ngIf=\"!item.isEditName; else inputFolder\">{{item.name}}</span>\r\n                        <ng-template #inputFolder>\r\n                            <input class=\"form-control\" [(ngModel)]=\"item.name\" (click)=\"clickInputFolder(item)\"\r\n                                   (blur)=\"updateFolderName(item)\">\r\n                        </ng-template>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"title\" i18n=\"@@file\" *ngIf=\"listFile && listFile.length > 0 && !isSearchName\">\r\n        File\r\n    </div>\r\n    <div class=\"file-grid-container\" *ngIf=\"listFile && listFile.length > 0\"\r\n         [class.no-item]=\"listFile?.length === 0\">\r\n        <div class=\"file-grid-item\"\r\n             *ngFor=\"let item of listFile; let i = index\"\r\n             title=\"{{item.name}}\"\r\n             nhContextMenuTrigger\r\n             [nhContextMenuTriggerFor]=\"nhMenuFile\"\r\n             [nhContextMenuData]=\"item\">\r\n            <div class=\"icon\">\r\n                <i [class]=\"'at-icon at-icon-' + item.extension.replace('.', '')\" *ngIf=\"!item.isImage\"></i>\r\n                <img ghmImage [errorImageUrl]=\"'/assets/images/no-image.png'\" src=\"{{item.url}}\"\r\n                     [nhImageViewer]=\"item.absoluteUrl\"\r\n                     *ngIf=\"item.isImage\"/>\r\n            </div>\r\n            <div class=\"name\">\r\n                <span *ngIf=\"!item.isEditName; else inputFolder\">{{item.name}}</span>\r\n                <ng-template #inputFolder>\r\n                    <input class=\"form-control\" [(ngModel)]=\"item.name\" (blur)=\"updateFileName(item)\">\r\n                </ng-template>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div *ngIf=\"listFile?.length === 0 && listFolder?.length === 0\" class=\"alert alert-warning center font-size-14\"\r\n         i18n=\"@@noFileOrFolder\">No file or folder.\r\n        <a href=\"javascript://\" (click)=\"addFolder()\" i18n=\"@@addFolder\">Add folder</a></div>\r\n</ng-template>\r\n\r\n<app-folder-form (saveSuccessful)=\"saveSuccessFolder($event)\"></app-folder-form>\r\n<app-file-form (saveSuccessful)=\"search()\"></app-file-form>\r\n<app-slider-image></app-slider-image>\r\n\r\n<swal\r\n    #confirmDeleteFolder\r\n    i18n=\"@@confirmDeleteFolder\"\r\n    i18n-title=\"@@confirmTitleDeleteFolder\"\r\n    i18n-text=\"@@confirmTextDeleteFolder\"\r\n    title=\"Are you sure for delete this folder?\"\r\n    text=\"You can't recover this folder after delete.\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Accept\"\r\n    cancelButtonText=\"Cancel\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<swal\r\n    #confirmDeleteFile\r\n    i18n=\"@@confirmDeleteFile\"\r\n    i18n-title=\"@@confirmTitleDeleteFile\"\r\n    i18n-text=\"@@confirmTextDeleteFile\"\r\n    title=\"Are you sure for delete this file?\"\r\n    text=\"You can't recover this file after delete.\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Accept\"\r\n    cancelButtonText=\"Cancel\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<nh-menu #nhMenuFolder>\r\n    <nh-menu-item (clicked)=\"showFolderDetail($event)\">\r\n        <mat-icon class=\"menu-icon\">visibility</mat-icon>\r\n        <span i18n=\"@@view\">View</span>\r\n    </nh-menu-item>\r\n    <nh-menu-item (clicked)=\"edit($event, true)\">\r\n        <mat-icon class=\"menu-icon\">edit</mat-icon>\r\n        <span i18n=\"@@edit\">Edit</span>\r\n    </nh-menu-item>\r\n    <nh-menu-item (clicked)=\"$event.isEditName = true\">\r\n        <mat-icon class=\"menu-icon\">edit</mat-icon>\r\n        <span i18n=\"@@rename\">Rename</span>\r\n    </nh-menu-item>\r\n    <nh-menu-item *ngIf=\"permission.delete\"\r\n                  (clicked)=\"delete($event.id, true)\">\r\n        <mat-icon class=\"menu-icon\">delete</mat-icon>\r\n        <span i18n=\"@@edit\">Delete</span>\r\n    </nh-menu-item>\r\n</nh-menu>\r\n\r\n<nh-menu #nhMenuFile>\r\n    <!--<nh-menu-item (clicked)=\"showFileDetail($event)\">-->\r\n        <!--<mat-icon class=\"menu-icon\">visibility</mat-icon>-->\r\n        <!--<span i18n=\"@@view\">View</span>-->\r\n    <!--</nh-menu-item>-->\r\n    <nh-menu-item (clicked)=\"edit($event, false)\">\r\n        <mat-icon class=\"menu-icon\">edit</mat-icon>\r\n        <span i18n=\"@@edit\">Edit</span>\r\n    </nh-menu-item>\r\n    <nh-menu-item (clicked)=\"$event.isEditName = true\">\r\n        <mat-icon class=\"menu-icon\">edit</mat-icon>\r\n        <span i18n=\"@@rename\">Rename</span>\r\n    </nh-menu-item>\r\n    <nh-menu-item *ngIf=\"permission.delete\"\r\n                  (clicked)=\"delete($event.id, false)\">\r\n        <mat-icon class=\"menu-icon\">delete</mat-icon>\r\n        <span i18n=\"@@edit\">Delete</span>\r\n    </nh-menu-item>\r\n</nh-menu>\r\n"

/***/ }),

/***/ "./src/app/modules/folders/folder.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/modules/folders/folder.component.ts ***!
  \*****************************************************/
/*! exports provided: FolderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FolderComponent", function() { return FolderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _configs_page_id_config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../configs/page-id.config */ "./src/app/configs/page-id.config.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../shareds/models/filter-link.model */ "./src/app/shareds/models/filter-link.model.ts");
/* harmony import */ var _shareds_services_helper_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../shareds/services/helper.service */ "./src/app/shareds/services/helper.service.ts");
/* harmony import */ var _service_folder_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./service/folder.service */ "./src/app/modules/folders/service/folder.service.ts");
/* harmony import */ var _folder_form_folder_form_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./folder-form/folder-form.component */ "./src/app/modules/folders/folder-form/folder-form.component.ts");
/* harmony import */ var _file_form_file_form_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./file-form/file-form.component */ "./src/app/modules/folders/file-form/file-form.component.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _service_file_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./service/file.service */ "./src/app/modules/folders/service/file.service.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _viewmodels_folder_search_viewmodel__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./viewmodels/folder-search.viewmodel */ "./src/app/modules/folders/viewmodels/folder-search.viewmodel.ts");
/* harmony import */ var _shareds_components_ghm_file_explorer_ghm_file_upload_ghm_file_upload_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../shareds/components/ghm-file-explorer/ghm-file-upload/ghm-file-upload.service */ "./src/app/shareds/components/ghm-file-explorer/ghm-file-upload/ghm-file-upload.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _model_file_model__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./model/file.model */ "./src/app/modules/folders/model/file.model.ts");
/* harmony import */ var _slider_image_slider_image_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./slider-image/slider-image.component */ "./src/app/modules/folders/slider-image/slider-image.component.ts");
/* harmony import */ var _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @toverux/ngx-sweetalert2 */ "./node_modules/@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
/* harmony import */ var _shareds_constants_view_type_const__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../../shareds/constants/view-type.const */ "./src/app/shareds/constants/view-type.const.ts");























var FolderComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](FolderComponent, _super);
    function FolderComponent(appConfig, pageId, route, utilService, location, ghmFileUploadService, toastr, cdr, fileService, folderService) {
        var _this = _super.call(this) || this;
        _this.appConfig = appConfig;
        _this.pageId = pageId;
        _this.route = route;
        _this.utilService = utilService;
        _this.location = location;
        _this.ghmFileUploadService = ghmFileUploadService;
        _this.toastr = toastr;
        _this.cdr = cdr;
        _this.fileService = fileService;
        _this.folderService = folderService;
        _this.viewType = _shareds_constants_view_type_const__WEBPACK_IMPORTED_MODULE_22__["ViewType"];
        _this.listBreadcrumb = [];
        return _this;
    }
    FolderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.appService.setupPage(this.pageId.WEBSITE, this.pageId.FOLDER, 'Quản lý folder', 'Danh sách folder');
        this.subscribers.data = this.route.data.subscribe(function (data) {
            if (data.data) {
                _this.isSearching = false;
                _this.listFolder = data.data.folders;
                _this.listFile = data.data.files;
                _this.listBreadcrumb = data.data.breadcrumbs;
                _this.isSearchName = false;
            }
        });
        this.subscribers.queryParams = this.route.queryParams.subscribe(function (params) {
            _this.folderId = params.folderId ? parseInt(params.folderId) : '';
            _this.type = params.type ? parseInt(params.type) : _shareds_constants_view_type_const__WEBPACK_IMPORTED_MODULE_22__["ViewType"].list;
        });
        if (!this.folderId) {
            this.listFolderRoot = this.listFolder;
            this.folderTree = this.renderFolderTree(this.listFolderRoot);
        }
        this.urlLoadFolder = this.appConfig.FILE_MANAGEMENT + "folders/children/";
        this.ghmFileUploadService.complete$
            .subscribe(function (result) {
            if (result.code <= 0) {
                _this.toastr.error(result.message);
                return;
            }
            else {
                _this.search();
            }
        });
    };
    FolderComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.height = window.innerHeight - 300;
        if (this.folderId && this.folderId > 0) {
            this.getFolderFoot();
        }
        this.cdr.detectChanges();
        this.swalConfirmDelete.confirm.subscribe(function (result) {
            _this.delete(_this.folderSelectId, true);
        });
        this.swalConfirmDeleteFile.confirm.subscribe(function (result) {
            _this.delete(_this.fileSelectId, false);
        });
    };
    FolderComponent.prototype.onResize = function (event) {
        this.height = window.innerHeight - 300;
    };
    FolderComponent.prototype.addFolder = function () {
        this.folderFormComponent.folderId = this.folderId;
        this.folderFormComponent.add();
    };
    FolderComponent.prototype.searchByName = function (currentPage) {
        var _this = this;
        this.currentPage = currentPage;
        this.isSearching = true;
        this.folderService.searchByName(this.keyword, this.currentPage, this.pageSize)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_13__["finalize"])(function () { return _this.isSearching = false; })).subscribe(function (result) {
            _this.listFolder = result.folders;
            _this.listFile = result.files;
            _this.totalRows = result.totalFolder > result.totalFiles ? result.totalFolder : result.totalFiles;
            _this.listBreadcrumb = [];
            _this.isSearchName = true;
        });
    };
    FolderComponent.prototype.search = function () {
        var _this = this;
        this.renderLink();
        this.folderService.search(this.folderId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_13__["finalize"])(function () { return _this.isSearching = false; })).subscribe(function (result) {
            _this.listFolder = result.folders;
            _this.listFile = result.files;
            _this.isSearchName = false;
            _this.listBreadcrumb = result.breadcrumbs;
        });
    };
    FolderComponent.prototype.checkAll = function () {
        // if (this.listFileAndFolder) {
        //     _.each(this.listFileAndFolder, (item: FileSearchViewmodel) => {
        //         item.isCheck = this.isSelectAll;
        //     });
        //     this.getFolderIdSelect();
        // }
    };
    FolderComponent.prototype.checkFolder = function (folder) {
        // this.getFolderIdSelect();
        // if (this.listFolderSelect && this.listFileAndFolder && this.listFileAndFolder.length === this.listFolderSelect.length) {
        //     this.isSelectAll = true;
        // } else {
        //     this.isSelectAll = false;
        // }
    };
    FolderComponent.prototype.clickFolder = function (item) {
        this.showFolderDetail(item);
    };
    FolderComponent.prototype.clickInputFolder = function (item) {
        this.isClickInputFolder = true;
    };
    FolderComponent.prototype.edit = function (item, isFolder) {
        if (isFolder) {
            this.folderFormComponent.edit(item.id);
        }
        else {
            this.fileFormComponent.edit(item.id);
        }
    };
    FolderComponent.prototype.delete = function (id, isFolder) {
        var _this = this;
        if (isFolder) {
            this.folderService.delete(id).subscribe(function () {
                lodash__WEBPACK_IMPORTED_MODULE_15__["remove"](_this.listFolderRoot, function (item) {
                    return item.id === id;
                });
                lodash__WEBPACK_IMPORTED_MODULE_15__["remove"](_this.listFolder, function (item) {
                    return item.id === id;
                });
                _this.folderTree = _this.renderFolderTree(_this.listFolderRoot);
            });
        }
        else {
            this.fileService.delete(id).subscribe(function () {
                lodash__WEBPACK_IMPORTED_MODULE_15__["remove"](_this.listFile, function (item) {
                    return item.id === id;
                });
            });
        }
    };
    FolderComponent.prototype.onSelectNode = function (value) {
        if (value) {
            this.folderId = value.id;
            this.search();
        }
    };
    FolderComponent.prototype.showFolderInHome = function () {
        this.folderId = null;
        this.listBreadcrumb = [];
        this.search();
    };
    FolderComponent.prototype.showFolderDetail = function (item) {
        if (this.isClickInputFolder) {
            this.isClickInputFolder = false;
        }
        else {
            this.folderId = item.id;
            this.search();
        }
    };
    FolderComponent.prototype.showFileDetail = function (item) {
        this.sliderImageComponent.imageSelect = item;
        this.sliderImageComponent.listImage = lodash__WEBPACK_IMPORTED_MODULE_15__["filter"](this.listFile, function (file) {
            return file.isImage;
        });
        this.sliderImageComponent.show();
    };
    FolderComponent.prototype.updateFolderName = function (item) {
        var _this = this;
        if (!item.name || !item.name.trim()) {
            this.toastr.error('Please enter folder name');
            return;
        }
        this.folderService.updateName(item.id, item.concurrencyStamp, item.name).subscribe(function () {
            item.isEditName = false;
            var folderInTree = lodash__WEBPACK_IMPORTED_MODULE_15__["find"](_this.listFolderRoot, function (folder) {
                return folder.id === item.id;
            });
            if (folderInTree) {
                folderInTree.name = item.name;
                _this.folderTree = _this.renderFolderTree(_this.listFolderRoot);
            }
            var folderInList = lodash__WEBPACK_IMPORTED_MODULE_15__["find"](_this.listFolder, function (folder) {
                return folder.id === item.id;
            });
            if (folderInList) {
                folderInList.name = item.name;
            }
        });
    };
    FolderComponent.prototype.saveSuccessFolder = function (value) {
        if (value) {
            if (value.id) {
                var folderInTree = lodash__WEBPACK_IMPORTED_MODULE_15__["find"](this.listFolderRoot, function (folder) {
                    return folder.id === value.id;
                });
                if (folderInTree) {
                    folderInTree.name = value.name;
                }
                var folderInList = lodash__WEBPACK_IMPORTED_MODULE_15__["find"](this.listFolder, function (folder) {
                    return folder.id === value.id;
                });
                if (folderInList) {
                    folderInList.name = value.name;
                }
            }
            else {
                var folderInsert = new _viewmodels_folder_search_viewmodel__WEBPACK_IMPORTED_MODULE_16__["FolderSearchViewModel"](value.id, value.name, value.parentId, true, false);
                this.listFolderRoot.push(folderInsert);
            }
            this.folderTree = this.renderFolderTree(this.listFolderRoot);
            this.search();
        }
    };
    FolderComponent.prototype.updateFileName = function (item) {
        if (!item.name || !item.name.trim()) {
            this.toastr.error('Please enter file name');
            return;
        }
        var file = new _model_file_model__WEBPACK_IMPORTED_MODULE_19__["Files"](item.name, item.folderId, item.concurrencyStamp);
        this.fileService.update(item.id, file).subscribe(function () {
            item.isEditName = false;
        });
    };
    FolderComponent.prototype.onViewType = function () {
        this.type = this.type === _shareds_constants_view_type_const__WEBPACK_IMPORTED_MODULE_22__["ViewType"].gird ? _shareds_constants_view_type_const__WEBPACK_IMPORTED_MODULE_22__["ViewType"].list : _shareds_constants_view_type_const__WEBPACK_IMPORTED_MODULE_22__["ViewType"].gird;
        this.renderLink();
    };
    FolderComponent.prototype.showByBreadcrumb = function (folderId) {
        this.folderId = folderId;
        this.search();
    };
    FolderComponent.prototype.expandedFolder = function (value) {
        var _this = this;
        if (value) {
            this.folderService.getChildren(value.id).subscribe(function (result) {
                var children = _this.renderFolderTree(result, value.id);
                value.children = children;
            });
        }
    };
    FolderComponent.prototype.confirm = function (value, isFolder) {
        if (isFolder) {
            this.folderSelectId = value.id;
        }
        else {
            this.fileSelectId = value.id;
        }
    };
    FolderComponent.prototype.renderBreadcrumb = function (idPath) {
        var _this = this;
        this.listBreadcrumb = [];
        if (idPath) {
            if (idPath.indexOf('.') > -1) {
                var list = idPath.split('.');
                if (list && list.length > 0) {
                    lodash__WEBPACK_IMPORTED_MODULE_15__["each"](list, function (item) {
                        var breadCrumbItem = {
                            id: item,
                            name: item,
                        };
                        _this.listBreadcrumb.push(breadCrumbItem);
                    });
                }
            }
            else {
                var breadcrumb = {
                    id: idPath,
                    name: idPath,
                };
                this.listBreadcrumb.push(breadcrumb);
            }
        }
        else {
            this.listBreadcrumb = [];
        }
    };
    FolderComponent.prototype.getFolderFoot = function () {
        var _this = this;
        this.folderService.search(null).subscribe(function (result) {
            var listFolder = result.folders;
            _this.listFolderRoot = result.folders;
            _this.folderTree = _this.renderFolderTree(_this.listFolderRoot);
        });
    };
    FolderComponent.prototype.renderFolderTree = function (listData, id) {
        if (listData && listData.length > 0) {
            var folderTree_1 = [];
            lodash__WEBPACK_IMPORTED_MODULE_15__["each"](listData, function (folder) {
                var tree = {
                    id: folder.id,
                    text: folder.name,
                    parentId: folder.parentId,
                    idPath: folder.idPath,
                    data: folder,
                    childCount: folder.childCount,
                    icon: null,
                    isSelected: false,
                    open: true,
                    isLoading: false,
                    state: {
                        opened: false,
                        selected: false,
                        disabled: false,
                    },
                    children: []
                };
                folderTree_1.push(tree);
            });
            return folderTree_1;
        }
    };
    FolderComponent.prototype.renderLink = function () {
        var path = '/folders';
        var query = this.utilService.renderLocationFilter([
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_8__["FilterLink"]('type', this.type),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_8__["FilterLink"]('folderId', this.folderId),
        ]);
        this.location.go(path, query);
    };
    FolderComponent.prototype.getFolderIdSelect = function () {
        // this.listFolderSelect = _.map(_.filter(this.listFileAndFolder, (item: FileSearchViewmodel) => {
        //     return item.isCheck;
        // }), (folderSelect => {
        //     return folderSelect.id && folderSelect.isFolder;
        // }));
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_folder_form_folder_form_component__WEBPACK_IMPORTED_MODULE_11__["FolderFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _folder_form_folder_form_component__WEBPACK_IMPORTED_MODULE_11__["FolderFormComponent"])
    ], FolderComponent.prototype, "folderFormComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_file_form_file_form_component__WEBPACK_IMPORTED_MODULE_12__["FileFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _file_form_file_form_component__WEBPACK_IMPORTED_MODULE_12__["FileFormComponent"])
    ], FolderComponent.prototype, "fileFormComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_slider_image_slider_image_component__WEBPACK_IMPORTED_MODULE_20__["SliderImageComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _slider_image_slider_image_component__WEBPACK_IMPORTED_MODULE_20__["SliderImageComponent"])
    ], FolderComponent.prototype, "sliderImageComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('confirmDeleteFolder'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_21__["SwalComponent"])
    ], FolderComponent.prototype, "swalConfirmDelete", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('confirmDeleteFile'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_21__["SwalComponent"])
    ], FolderComponent.prototype, "swalConfirmDeleteFile", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:resize', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], FolderComponent.prototype, "onResize", null);
    FolderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-folder',
            template: __webpack_require__(/*! ./folder.component.html */ "./src/app/modules/folders/folder.component.html"),
            providers: [
                _angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"], { provide: _angular_common__WEBPACK_IMPORTED_MODULE_7__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_7__["PathLocationStrategy"] },
                _shareds_services_helper_service__WEBPACK_IMPORTED_MODULE_9__["HelperService"], _service_folder_service__WEBPACK_IMPORTED_MODULE_10__["FolderService"], _service_file_service__WEBPACK_IMPORTED_MODULE_14__["FileService"]
            ],
            styles: [__webpack_require__(/*! ./folder.scss */ "./src/app/modules/folders/folder.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_3__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_page_id_config__WEBPACK_IMPORTED_MODULE_4__["PAGE_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, Object, _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_6__["UtilService"],
            _angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"],
            _shareds_components_ghm_file_explorer_ghm_file_upload_ghm_file_upload_service__WEBPACK_IMPORTED_MODULE_17__["GhmFileUploadService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_18__["ToastrService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _service_file_service__WEBPACK_IMPORTED_MODULE_14__["FileService"],
            _service_folder_service__WEBPACK_IMPORTED_MODULE_10__["FolderService"]])
    ], FolderComponent);
    return FolderComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/folders/folder.module.ts":
/*!**************************************************!*\
  !*** ./src/app/modules/folders/folder.module.ts ***!
  \**************************************************/
/*! exports provided: FolderModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FolderModule", function() { return FolderModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shareds_layouts_layout_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shareds/layouts/layout.module */ "./src/app/shareds/layouts/layout.module.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _folder_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./folder-routing.module */ "./src/app/modules/folders/folder-routing.module.ts");
/* harmony import */ var _folder_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./folder.component */ "./src/app/modules/folders/folder.component.ts");
/* harmony import */ var _shareds_components_nh_select_nh_select_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shareds/components/nh-select/nh-select.module */ "./src/app/shareds/components/nh-select/nh-select.module.ts");
/* harmony import */ var _shareds_pipe_datetime_format_datetime_format_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../shareds/pipe/datetime-format/datetime-format.module */ "./src/app/shareds/pipe/datetime-format/datetime-format.module.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shareds_components_nh_datetime_picker_nh_date_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../shareds/components/nh-datetime-picker/nh-date.module */ "./src/app/shareds/components/nh-datetime-picker/nh-date.module.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../shareds/components/nh-modal/nh-modal.module */ "./src/app/shareds/components/nh-modal/nh-modal.module.ts");
/* harmony import */ var _shareds_components_nh_tree_nh_tree_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../shareds/components/nh-tree/nh-tree.module */ "./src/app/shareds/components/nh-tree/nh-tree.module.ts");
/* harmony import */ var _shareds_components_ghm_user_suggestion_ghm_user_suggestion_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../shareds/components/ghm-user-suggestion/ghm-user-suggestion.module */ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.module.ts");
/* harmony import */ var _shareds_components_ghm_select_picker_ghm_select_picker_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../shareds/components/ghm-select-picker/ghm-select-picker.module */ "./src/app/shareds/components/ghm-select-picker/ghm-select-picker.module.ts");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _shareds_components_ghm_paging_ghm_paging_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../shareds/components/ghm-paging/ghm-paging.module */ "./src/app/shareds/components/ghm-paging/ghm-paging.module.ts");
/* harmony import */ var _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @toverux/ngx-sweetalert2 */ "./node_modules/@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
/* harmony import */ var _folder_form_folder_form_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./folder-form/folder-form.component */ "./src/app/modules/folders/folder-form/folder-form.component.ts");
/* harmony import */ var _file_form_file_form_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./file-form/file-form.component */ "./src/app/modules/folders/file-form/file-form.component.ts");
/* harmony import */ var _shareds_components_nh_upload_nh_upload_module__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../../shareds/components/nh-upload/nh-upload.module */ "./src/app/shareds/components/nh-upload/nh-upload.module.ts");
/* harmony import */ var _shareds_components_ghm_file_explorer_ghm_file_explorer_module__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../../shareds/components/ghm-file-explorer/ghm-file-explorer.module */ "./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.module.ts");
/* harmony import */ var _shareds_directives_nh_dropdown_nh_dropdown_module__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../../shareds/directives/nh-dropdown/nh-dropdown.module */ "./src/app/shareds/directives/nh-dropdown/nh-dropdown.module.ts");
/* harmony import */ var _slider_image_slider_image_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./slider-image/slider-image.component */ "./src/app/modules/folders/slider-image/slider-image.component.ts");
/* harmony import */ var _folder_tree_folder_tree_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./folder-tree/folder-tree.component */ "./src/app/modules/folders/folder-tree/folder-tree.component.ts");
/* harmony import */ var _shareds_components_nh_context_menu_nh_context_menu_module__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ../../shareds/components/nh-context-menu/nh-context-menu.module */ "./src/app/shareds/components/nh-context-menu/nh-context-menu.module.ts");
/* harmony import */ var _shareds_components_nh_image_viewer_nh_image_viewer_module__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ../../shareds/components/nh-image-viewer/nh-image-viewer.module */ "./src/app/shareds/components/nh-image-viewer/nh-image-viewer.module.ts");



























var FolderModule = /** @class */ (function () {
    function FolderModule() {
    }
    FolderModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _folder_routing_module__WEBPACK_IMPORTED_MODULE_4__["FolderRoutingModule"], _shareds_layouts_layout_module__WEBPACK_IMPORTED_MODULE_2__["LayoutModule"], _shareds_components_nh_select_nh_select_module__WEBPACK_IMPORTED_MODULE_6__["NhSelectModule"], _shareds_pipe_datetime_format_datetime_format_module__WEBPACK_IMPORTED_MODULE_7__["DatetimeFormatModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatCheckboxModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"], _shareds_components_nh_datetime_picker_nh_date_module__WEBPACK_IMPORTED_MODULE_10__["NhDateModule"], _shareds_components_nh_upload_nh_upload_module__WEBPACK_IMPORTED_MODULE_20__["NhUploadModule"], _shareds_components_ghm_file_explorer_ghm_file_explorer_module__WEBPACK_IMPORTED_MODULE_21__["GhmFileExplorerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"],
                _shareds_directives_nh_dropdown_nh_dropdown_module__WEBPACK_IMPORTED_MODULE_22__["NhDropdownModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatMenuModule"], _shareds_components_nh_context_menu_nh_context_menu_module__WEBPACK_IMPORTED_MODULE_25__["NhContextMenuModule"], _shareds_components_nh_image_viewer_nh_image_viewer_module__WEBPACK_IMPORTED_MODULE_26__["NhImageViewerModule"],
                _shareds_components_nh_modal_nh_modal_module__WEBPACK_IMPORTED_MODULE_11__["NhModalModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTooltipModule"], _shareds_components_nh_tree_nh_tree_module__WEBPACK_IMPORTED_MODULE_12__["NHTreeModule"], _shareds_components_ghm_user_suggestion_ghm_user_suggestion_module__WEBPACK_IMPORTED_MODULE_13__["GhmUserSuggestionModule"],
                _shareds_components_ghm_select_picker_ghm_select_picker_module__WEBPACK_IMPORTED_MODULE_14__["GhmSelectPickerModule"], _core_core_module__WEBPACK_IMPORTED_MODULE_15__["CoreModule"], _shareds_components_ghm_paging_ghm_paging_module__WEBPACK_IMPORTED_MODULE_16__["GhmPagingModule"], _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_17__["SweetAlert2Module"].forRoot({
                    buttonsStyling: false,
                    customClass: 'modal-content',
                    confirmButtonClass: 'btn blue cm-mgr-5',
                    cancelButtonClass: 'btn',
                    // confirmButtonText: 'Accept',
                    showCancelButton: true,
                })],
            exports: [_folder_component__WEBPACK_IMPORTED_MODULE_5__["FolderComponent"]],
            declarations: [_folder_component__WEBPACK_IMPORTED_MODULE_5__["FolderComponent"], _folder_form_folder_form_component__WEBPACK_IMPORTED_MODULE_18__["FolderFormComponent"], _file_form_file_form_component__WEBPACK_IMPORTED_MODULE_19__["FileFormComponent"], _slider_image_slider_image_component__WEBPACK_IMPORTED_MODULE_23__["SliderImageComponent"], _folder_tree_folder_tree_component__WEBPACK_IMPORTED_MODULE_24__["FolderTreeComponent"]],
            providers: []
        })
    ], FolderModule);
    return FolderModule;
}());



/***/ }),

/***/ "./src/app/modules/folders/folder.scss":
/*!*********************************************!*\
  !*** ./src/app/modules/folders/folder.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".inbox {\n  margin-top: 0px; }\n  .inbox .compose-btn {\n    padding: 8px 14px; }\n  .inbox .inbox-content {\n    min-height: 400px; }\n  .inbox .inbox-sidebar {\n    padding: 5px;\n    border-radius: 4px; }\n  .inbox .inbox-sidebar .home-dictionary a {\n      margin-left: 30px;\n      font-size: 20px;\n      color: #777;\n      font-weight: 600; }\n  .inbox .inbox-sidebar .home-dictionary i {\n      color: #007bff; }\n  .inbox .inbox-body {\n    padding: 5px;\n    border-radius: 4px; }\n  .inbox .inbox-body .color-folder {\n      color: #F7CA18; }\n  .inbox .inbox-body .page-bar {\n      background-color: transparent !important;\n      padding: 0px;\n      margin: 0px 0px 10px; }\n  .inbox .inbox-body .title {\n      color: #777;\n      font-weight: 600;\n      font-size: 16px; }\n  .inbox .inbox-body .folder-gird-container {\n      display: flex;\n      flex-wrap: wrap;\n      margin: 1em 0;\n      padding: 0;\n      -webkit-column-gap: 1em;\n      column-gap: 1em;\n      width: 100%; }\n  .inbox .inbox-body .folder-gird-container .folder-gird-item {\n        display: flex;\n        flex-basis: 14%;\n        overflow: hidden;\n        align-items: center;\n        justify-content: left;\n        padding: 1em;\n        margin: 0 0 1em;\n        border-radius: 4px !important;\n        position: relative;\n        text-align: center;\n        height: auto; }\n  @media screen and (max-width: 768px) {\n          .inbox .inbox-body .folder-gird-container .folder-gird-item {\n            flex-basis: 48%; } }\n  @media screen and (min-width: 768px) and (max-width: 992px) {\n          .inbox .inbox-body .folder-gird-container .folder-gird-item {\n            flex-basis: 31%; } }\n  @media screen and (min-width: 992px) and (max-width: 1280px) {\n          .inbox .inbox-body .folder-gird-container .folder-gird-item {\n            flex-basis: 18%; } }\n  .inbox .inbox-body .folder-gird-container .folder-gird-item:hover {\n          box-shadow: 3px 3px 1px 0 #ccc;\n          background-color: #f1f4f7; }\n  .inbox .inbox-body .folder-gird-container .folder-gird-item:hover .btn {\n            display: block; }\n  .inbox .inbox-body .folder-gird-container .folder-gird-item .btn {\n          display: none;\n          position: absolute;\n          right: 0;\n          top: 0;\n          float: right !important;\n          margin: 0 auto;\n          z-index: 9999; }\n  .inbox .inbox-body .folder-gird-container .folder-gird-item .dropdown-menu {\n          top: 20px; }\n  .inbox .inbox-body .folder-gird-container .folder-gird-item .media {\n          margin-top: 0px;\n          cursor: pointer; }\n  .inbox .inbox-body .folder-gird-container .folder-gird-item .media .media-left {\n            padding-top: 10px; }\n  .inbox .inbox-body .folder-gird-container .folder-gird-item .media .media-left i {\n              color: #F7CA18;\n              font-size: 30px; }\n  .inbox .inbox-body .folder-gird-container .folder-gird-item .media .media-body {\n            width: initial; }\n  .inbox .inbox-body .folder-gird-container .folder-gird-item .media .media-body .name {\n              width: 100%;\n              padding-top: 5px;\n              bottom: 0;\n              font-size: 14px;\n              cursor: pointer; }\n  .inbox .inbox-body .file-grid-container {\n      display: flex;\n      flex-wrap: wrap;\n      margin: 1em 0;\n      padding: 0;\n      -webkit-column-gap: 1em;\n      column-gap: 1em;\n      width: 100%; }\n  .inbox .inbox-body .file-grid-container.no-item {\n        -webkit-column-count: 1;\n                column-count: 1; }\n  .inbox .inbox-body .file-grid-container .file-grid-item {\n        display: flex;\n        flex-basis: 14%;\n        padding: 1em;\n        box-sizing: border-box;\n        border-radius: 4px !important;\n        position: relative;\n        text-align: center;\n        padding: 5px;\n        overflow: hidden;\n        align-items: center;\n        justify-content: center;\n        background-color: #fff;\n        margin: 5px; }\n  @media screen and (max-width: 768px) {\n          .inbox .inbox-body .file-grid-container .file-grid-item {\n            flex-basis: 48%; } }\n  @media screen and (min-width: 768px) and (max-width: 992px) {\n          .inbox .inbox-body .file-grid-container .file-grid-item {\n            flex-basis: 31%; } }\n  @media screen and (min-width: 992px) and (max-width: 1280px) {\n          .inbox .inbox-body .file-grid-container .file-grid-item {\n            flex-basis: 18%; } }\n  .inbox .inbox-body .file-grid-container .file-grid-item .icon {\n          display: flex;\n          align-items: center;\n          justify-content: center;\n          margin-bottom: 30px;\n          min-height: 188px; }\n  .inbox .inbox-body .file-grid-container .file-grid-item img {\n          width: 100%; }\n  .inbox .inbox-body .file-grid-container .file-grid-item:hover {\n          opacity: 0.8; }\n  .inbox .inbox-body .file-grid-container .file-grid-item:hover .btn {\n            display: block; }\n  .inbox .inbox-body .file-grid-container .file-grid-item .btn {\n          display: none;\n          position: absolute;\n          right: 0;\n          top: 0;\n          float: right !important;\n          margin: 0 auto;\n          z-index: 9999; }\n  .inbox .inbox-body .file-grid-container .file-grid-item .dropdown-menu {\n          top: 20px; }\n  .inbox .inbox-body .file-grid-container .file-grid-item i.at-icon {\n          font-size: 50px;\n          position: absolute;\n          left: 0;\n          right: 0;\n          margin: 0 auto;\n          top: 30%; }\n  .inbox .inbox-body .file-grid-container .file-grid-item div.name {\n          display: block;\n          width: 100%;\n          padding: 5px 10px;\n          overflow: hidden;\n          text-overflow: ellipsis;\n          white-space: nowrap;\n          position: absolute;\n          bottom: 0;\n          cursor: pointer; }\n  .inbox .inbox-body .file-grid-container .file-grid-item img {\n          width: 100%; }\n  .inbox .inbox {\n    margin-bottom: 0; }\n  .inbox .tab-content {\n    overflow: inherit; }\n  .inbox .inbox-loading {\n    display: none;\n    font-size: 22px;\n    font-weight: 300; }\n  .inbox .inbox-header {\n    overflow: hidden; }\n  .inbox .inbox-header h1 {\n      color: #666;\n      margin: 0 0 20px; }\n  .inbox .pagination-control {\n    text-align: right; }\n  .inbox .pagination-control .pagination-info {\n      display: inline-block;\n      padding-right: 10px;\n      font-size: 14px;\n      line-height: 14px; }\n  .inbox tr {\n    color: #777;\n    font-size: 13px; }\n  .inbox tr label {\n      display: inline-block; }\n  .inbox tr i.icon-star {\n      cursor: pointer;\n      color: #eee; }\n  .inbox tr i.icon-star:hover {\n        color: #fd7b12; }\n  .inbox tr i.icon-trash {\n      cursor: pointer; }\n  .inbox tr i.inbox-started {\n      color: #fd7b12; }\n  .inbox tr.unread td {\n      font-weight: 600; }\n  .inbox td.text-right {\n    text-align: right; }\n  .inbox td.inbox-small-cells {\n    width: 10px; }\n  .inbox td i.icon-paper-clip {\n    top: 2px;\n    color: #d8e0e5;\n    font-size: 17px;\n    position: relative; }\n  .inbox .table th {\n    border: none;\n    background: #f1f4f7;\n    border-bottom: solid 5px #ffffff; }\n  .inbox .table td {\n    border: none; }\n  .inbox th.text-right {\n    text-align: right; }\n  .inbox th label.inbox-select-all {\n    color: #828f97;\n    font-size: 13px;\n    padding: 1px 4px 0; }\n  .inbox tbody {\n    border-top: none !important; }\n  .inbox .inbox-drafts {\n    padding: 8px 0;\n    text-align: center;\n    border-top: solid 1px #eee;\n    border-bottom: solid 1px #eee; }\n  .inbox .input-actions .btn {\n    margin-left: 10px; }\n  .inbox .table-hover tbody tr:hover > td {\n    background: #f8fbfd;\n    cursor: pointer;\n    background: #f1f4f7; }\n  .inbox .table-hover tbody tr:hover > th {\n    background: #f8fbfd;\n    cursor: pointer;\n    background: #f1f4f7; }\n  .inbox .table-striped tbody > tr:nth-child(odd) > td {\n    background: #f8fbfd;\n    cursor: pointer; }\n  .inbox .table-striped tbody > tr:nth-child(odd) > th {\n    background: #f8fbfd;\n    cursor: pointer; }\n  .at-icon {\n  font-family: \"FontAwesome\";\n  font-style: normal; }\n  .at-icon.at-icon-xls, .at-icon.at-icon-xlsx {\n    color: forestgreen; }\n  .at-icon.at-icon-xls:before, .at-icon.at-icon-xlsx:before {\n      content: \"\\f1c3\"; }\n  .at-icon.at-icon-doc, .at-icon.at-icon-docx {\n    color: cornflowerblue; }\n  .at-icon.at-icon-doc:before, .at-icon.at-icon-docx:before {\n      content: \"\\f1c2\"; }\n  .at-icon.at-icon-txt:before {\n    content: \"\\f0f6\"; }\n  .at-icon.at-icon-pptx {\n    color: #e74c3c; }\n  .at-icon.at-icon-pptx:before {\n      content: \"\\f1c4\"; }\n  .at-icon.at-icon-pdf {\n    color: #c0392b; }\n  .at-icon.at-icon-pdf:before {\n      content: \"\\f1c1\"; }\n  .img-slide {\n  display: flex;\n  align-items: center; }\n  @media only screen and (min-width: 400px) {\n  .file-grid-container, .folder-gird-container {\n    -webkit-column-count: 2;\n    column-count: 2; } }\n  @media only screen and (min-width: 700px) {\n  .file-grid-container, .folder-gird-container {\n    -webkit-column-count: 3;\n    column-count: 3; } }\n  @media only screen and (min-width: 900px) {\n  .file-grid-container, .folder-gird-container {\n    -webkit-column-count: 4;\n    column-count: 4; } }\n  @media only screen and (min-width: 1100px) {\n  .file-grid-container, .folder-gird-container {\n    -webkit-column-count: 5;\n    column-count: 5; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy9mb2xkZXJzL0Q6XFxQcm9qZWN0XFxHaG1BcHBsaWNhdGlvblxcY2xpZW50c1xcZ2htYXBwbGljYXRpb25jbGllbnQvc3JjXFxhcHBcXG1vZHVsZXNcXGZvbGRlcnNcXGZvbGRlci5zY3NzIiwic3JjL2FwcC9tb2R1bGVzL2ZvbGRlcnMvRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFzc2V0c1xcc3R5bGVzXFxfY29uZmlnLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBbUNBO0VBQ0ksZUFBZSxFQUFBO0VBRG5CO0lBR1EsaUJBQWlCLEVBQUE7RUFIekI7SUFNUSxpQkFBaUIsRUFBQTtFQU56QjtJQVVRLFlBQVk7SUFFWixrQkFBa0IsRUFBQTtFQVoxQjtNQWVnQixpQkFBaUI7TUFDakIsZUFBZTtNQUNmLFdBN0JFO01BOEJGLGdCQUFnQixFQUFBO0VBbEJoQztNQXNCZ0IsY0NsREYsRUFBQTtFRDRCZDtJQTJCUSxZQUFZO0lBR1osa0JBQWtCLEVBQUE7RUE5QjFCO01BaUNZLGNBNUNVLEVBQUE7RUFXdEI7TUFvQ1ksd0NBQXdDO01BQ3hDLFlBQVk7TUFDWixvQkFBb0IsRUFBQTtFQXRDaEM7TUF5Q1ksV0FyRE07TUFzRE4sZ0JBQWdCO01BQ2hCLGVBQWUsRUFBQTtFQTNDM0I7TUErQ1ksYUFBYTtNQUNiLGVBQWU7TUFDZixhQUFhO01BQ2IsVUFBVTtNQUVWLHVCQUF1QjtNQUN2QixlQUFlO01BQ2YsV0FBVyxFQUFBO0VBdER2QjtRQXdEZ0IsYUFBYTtRQUNiLGVBQWU7UUFDZixnQkFBZ0I7UUFDaEIsbUJBQW1CO1FBQ25CLHFCQUFxQjtRQUNyQixZQUFZO1FBQ1osZUFBZTtRQUNmLDZCQUE2QjtRQUM3QixrQkFBa0I7UUFDbEIsa0JBQWtCO1FBQ2xCLFlBQVksRUFBQTtFQUVaO1VBcEVoQjtZQXFFb0IsZUFBZSxFQUFBLEVBdUR0QjtFQXBERztVQXhFaEI7WUF5RW9CLGVBQWUsRUFBQSxFQW1EdEI7RUFoREc7VUE1RWhCO1lBNkVvQixlQUFlLEVBQUEsRUErQ3RCO0VBNUhiO1VBaUZvQiw4QkFBOEI7VUFDOUIseUJBbEhpQixFQUFBO0VBZ0NyQztZQW9Gd0IsY0FBYyxFQUFBO0VBcEZ0QztVQXlGb0IsYUFBYTtVQUNiLGtCQUFrQjtVQUNsQixRQUFRO1VBQ1IsTUFBTTtVQUNOLHVCQUF1QjtVQUN2QixjQUFjO1VBQ2QsYUFBYSxFQUFBO0VBL0ZqQztVQW1Hb0IsU0FBUyxFQUFBO0VBbkc3QjtVQXVHb0IsZUFBZTtVQUNmLGVBQWUsRUFBQTtFQXhHbkM7WUEwR3dCLGlCQUFpQixFQUFBO0VBMUd6QztjQTRHNEIsY0F2SE47Y0F3SE0sZUFBZSxFQUFBO0VBN0czQztZQWtId0IsY0FBYyxFQUFBO0VBbEh0QztjQW9INEIsV0FBVztjQUNYLGdCQUFnQjtjQUNoQixTQUFTO2NBQ1QsZUFBZTtjQUNmLGVBQWUsRUFBQTtFQXhIM0M7TUFnSVksYUFBYTtNQUNiLGVBQWU7TUFDZixhQUFhO01BQ2IsVUFBVTtNQUVWLHVCQUF1QjtNQUN2QixlQUFlO01BQ2YsV0FBVyxFQUFBO0VBdkl2QjtRQTBJZ0IsdUJBQWU7Z0JBQWYsZUFBZSxFQUFBO0VBMUkvQjtRQThJZ0IsYUFBYTtRQUNiLGVBQWU7UUFDZixZQUFZO1FBQ1osc0JBQXNCO1FBQ3RCLDZCQUE2QjtRQUM3QixrQkFBa0I7UUFDbEIsa0JBQWtCO1FBQ2xCLFlBQVk7UUFDWixnQkFBZ0I7UUFDaEIsbUJBQW1CO1FBQ25CLHVCQUF1QjtRQUN2QixzQkFBc0I7UUFDdEIsV0FBVyxFQUFBO0VBRVg7VUE1SmhCO1lBNkpvQixlQUFlLEVBQUEsRUFvRXRCO0VBakVHO1VBaEtoQjtZQWlLb0IsZUFBZSxFQUFBLEVBZ0V0QjtFQTdERztVQXBLaEI7WUFxS29CLGVBQWUsRUFBQSxFQTREdEI7RUFqT2I7VUF5S29CLGFBQWE7VUFDYixtQkFBbUI7VUFDbkIsdUJBQXVCO1VBQ3ZCLG1CQUFtQjtVQUNuQixpQkFBaUIsRUFBQTtFQTdLckM7VUFnTG9CLFdBQVcsRUFBQTtFQWhML0I7VUFxTG9CLFlBQVksRUFBQTtFQXJMaEM7WUF1THdCLGNBQWMsRUFBQTtFQXZMdEM7VUE0TG9CLGFBQWE7VUFDYixrQkFBa0I7VUFDbEIsUUFBUTtVQUNSLE1BQU07VUFDTix1QkFBdUI7VUFDdkIsY0FBYztVQUNkLGFBQWEsRUFBQTtFQWxNakM7VUFzTW9CLFNBQVMsRUFBQTtFQXRNN0I7VUEwTW9CLGVBQWU7VUFDZixrQkFBa0I7VUFDbEIsT0FBTztVQUNQLFFBQVE7VUFDUixjQUFjO1VBQ2QsUUFBUSxFQUFBO0VBL001QjtVQW1Ob0IsY0FBYztVQUNkLFdBQVc7VUFDWCxpQkFBaUI7VUFDakIsZ0JBQWdCO1VBQ2hCLHVCQUF1QjtVQUN2QixtQkFBbUI7VUFDbkIsa0JBQWtCO1VBQ2xCLFNBQVM7VUFDVCxlQUFlLEVBQUE7RUEzTm5DO1VBK05vQixXQUFXLEVBQUE7RUEvTi9CO0lBcU9RLGdCQUFnQixFQUFBO0VBck94QjtJQXdPUSxpQkFBaUIsRUFBQTtFQXhPekI7SUEyT1EsYUFBYTtJQUNiLGVBQWU7SUFDZixnQkFBZ0IsRUFBQTtFQTdPeEI7SUFnUFEsZ0JBQWdCLEVBQUE7RUFoUHhCO01Ba1BZLFdBOVFrQjtNQStRbEIsZ0JBQWdCLEVBQUE7RUFuUDVCO0lBdVBRLGlCQUFpQixFQUFBO0VBdlB6QjtNQXlQWSxxQkFBcUI7TUFDckIsbUJBQW1CO01BQ25CLGVBQWU7TUFDZixpQkFBaUIsRUFBQTtFQTVQN0I7SUFnUVEsV0F4UmdCO0lBeVJoQixlQUFlLEVBQUE7RUFqUXZCO01BbVFZLHFCQUFxQixFQUFBO0VBblFqQztNQXVRZ0IsZUFBZTtNQUNmLFdBblNXLEVBQUE7RUEyQjNCO1FBMFFvQixjQWhTVSxFQUFBO0VBc0I5QjtNQThRZ0IsZUFBZSxFQUFBO0VBOVEvQjtNQWlSZ0IsY0F2U2MsRUFBQTtFQXNCOUI7TUFxUlksZ0JBQWdCLEVBQUE7RUFyUjVCO0lBMFJZLGlCQUFpQixFQUFBO0VBMVI3QjtJQTZSWSxXQUFXLEVBQUE7RUE3UnZCO0lBZ1NZLFFBQVE7SUFDUixjQXhUaUI7SUF5VGpCLGVBQWU7SUFDZixrQkFBa0IsRUFBQTtFQW5TOUI7SUF3U1ksWUFBWTtJQUNaLG1CQXpVeUI7SUEwVXpCLGdDQW5VRyxFQUFBO0VBeUJmO0lBNlNZLFlBQVksRUFBQTtFQTdTeEI7SUFrVFksaUJBQWlCLEVBQUE7RUFsVDdCO0lBcVRZLGNBMVVvQjtJQTJVcEIsZUFBZTtJQUNmLGtCQUFrQixFQUFBO0VBdlQ5QjtJQTRUUSwyQkFBMkIsRUFBQTtFQTVUbkM7SUErVFEsY0FBYztJQUNkLGtCQUFrQjtJQUNsQiwwQkE1Vm1CO0lBNlZuQiw2QkE3Vm1CLEVBQUE7RUEyQjNCO0lBcVVRLGlCQUFpQixFQUFBO0VBclV6QjtJQXlVWSxtQkE3Vm9CO0lBOFZwQixlQUFlO0lBQ2YsbUJBM1d5QixFQUFBO0VBZ0NyQztJQThVWSxtQkFsV29CO0lBbVdwQixlQUFlO0lBQ2YsbUJBaFh5QixFQUFBO0VBZ0NyQztJQXFWWSxtQkF6V29CO0lBMFdwQixlQUFlLEVBQUE7RUF0VjNCO0lBeVZZLG1CQTdXb0I7SUE4V3BCLGVBQWUsRUFBQTtFQU8zQjtFQUNJLDBCQUEwQjtFQUMxQixrQkFBa0IsRUFBQTtFQUZ0QjtJQVFRLGtCQUFrQixFQUFBO0VBUjFCO01BTVksZ0JBQWdCLEVBQUE7RUFONUI7SUFjUSxxQkFBcUIsRUFBQTtFQWQ3QjtNQVlZLGdCQUFnQixFQUFBO0VBWjVCO0lBaUJRLGdCQUFnQixFQUFBO0VBakJ4QjtJQXVCUSxjQUFjLEVBQUE7RUF2QnRCO01BcUJZLGdCQUFnQixFQUFBO0VBckI1QjtJQTZCUSxjQUFjLEVBQUE7RUE3QnRCO01BMkJZLGdCQUFnQixFQUFBO0VBTTVCO0VBQ0ksYUFBYTtFQUNiLG1CQUFtQixFQUFBO0VBR3ZCO0VBQ0k7SUFFSSx1QkFBdUI7SUFDdkIsZUFBZSxFQUFBLEVBQ2xCO0VBR0w7RUFDSTtJQUVJLHVCQUF1QjtJQUN2QixlQUFlLEVBQUEsRUFDbEI7RUFHTDtFQUNJO0lBRUksdUJBQXVCO0lBQ3ZCLGVBQWUsRUFBQSxFQUNsQjtFQUdMO0VBQ0k7SUFFSSx1QkFBdUI7SUFDdkIsZUFBZSxFQUFBLEVBQ2xCIiwiZmlsZSI6InNyYy9hcHAvbW9kdWxlcy9mb2xkZXJzL2ZvbGRlci5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uLy4uLy4uL2Fzc2V0cy9zdHlsZXMvY29uZmlnXCI7XHJcblxyXG4vL2NvbG9yc1xyXG4kY29sb3JfY2F0c2tpbGxfd2hpdGVfYXBwcm94OiAjZjFmNGY3O1xyXG4kY29sb3JfZnJvbHlfYXBwcm94OiAjZWQ2Yjc1O1xyXG4kY29sb3JfYXRoZW5zX2dyYXlfYXBwcm94OiAjZWVmMWY1O1xyXG4kY29sb3JfY2FwZV9jb2RfYXBwcm94OiAjM2Y0NDRhO1xyXG4kY29sb3Jfc3Rvcm1fZHVzdF9hcHByb3g6ICM2NjY7XHJcbiRjb2xvcl9nYWxsZXJ5X2FwcHJveDogI2VlZTtcclxuJGNvbG9yX215c3RpY19hcHByb3g6ICNlN2VjZjE7XHJcbiR3aGl0ZTogI2ZmZmZmZjtcclxuJGNvbG9yX3RhcGFfYXBwcm94OiAjNzc3O1xyXG4kY29sb3JfZ2V5c2VyX2FwcHJveDogI2Q4ZTBlNTtcclxuJGNvbG9yX2Vjc3Rhc3lfYXBwcm94OiAjZmQ3YjEyO1xyXG4kY29sb3Jfb3Nsb19ncmF5X2FwcHJveDogIzgyOGY5NztcclxuJGNvbG9yX2FsYWJhc3Rlcl9hcHByb3g6ICNmOGZiZmQ7XHJcbiRjb2xvcl90b3dlcl9ncmF5X2FwcHJveDogI2IwYmNjNDtcclxuJGNvbG9yX2JvbWJheV9hcHByb3g6ICNhYWI1YmM7XHJcbiRjb2xvcl9wZXd0ZXJfYXBwcm94OiAjOTRhNGFiO1xyXG4kY29sb3JfZG9kZ2VyX2JsdWVfYXBwcm94OiAjMTY5Y2U5O1xyXG4kY29sb3JfZnVzY291c19ncmF5X2FwcHJveDogIzU1NTtcclxuJGNvbG9yX21vdW50YWluX21pc3RfYXBwcm94OiAjOTc5Nzk3O1xyXG4kc2Vhc2hlbGw6ICNmMWYxZjE7XHJcbiRjb2xvci10aXRsZTogIzc3NztcclxuJGNvbG9yLWZvbGRlcjogI0Y3Q0ExODtcclxuXHJcbi8vQGV4dGVuZC1lbGVtZW50c1xyXG4vL29yaWdpbmFsIHNlbGVjdG9yc1xyXG4vLy5pbmJveC1jb21wb3NlIC53eXNpaHRtbDUtdG9vbGJhciA+bGkgPmE6aG92ZXIsIC5pbmJveC1jb21wb3NlIC53eXNpaHRtbDUtdG9vbGJhciA+bGkgPmRpdiA+YTpob3ZlclxyXG4lZXh0ZW5kXzEge1xyXG4gICAgYmFja2dyb3VuZDogJHNlYXNoZWxsICFpbXBvcnRhbnQ7XHJcbiAgICAvL2JvcmRlci1jb2xvcjogJHNlYXNoZWxsICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwO1xyXG59XHJcblxyXG4uaW5ib3gge1xyXG4gICAgbWFyZ2luLXRvcDogMHB4O1xyXG4gICAgLmNvbXBvc2UtYnRuIHtcclxuICAgICAgICBwYWRkaW5nOiA4cHggMTRweDtcclxuICAgIH1cclxuICAgIC5pbmJveC1jb250ZW50IHtcclxuICAgICAgICBtaW4taGVpZ2h0OiA0MDBweDtcclxuICAgIH1cclxuXHJcbiAgICAuaW5ib3gtc2lkZWJhciB7XHJcbiAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgIC8vYm9yZGVyOiAxcHggc29saWQgJGNvbG9yX215c3RpY19hcHByb3g7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gICAgICAgIC5ob21lLWRpY3Rpb25hcnkge1xyXG4gICAgICAgICAgICBhIHtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAzMHB4O1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICRjb2xvci10aXRsZTtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGkge1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICRibHVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmluYm94LWJvZHkge1xyXG4gICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICAvL2JvcmRlcjogMXB4IHNvbGlkICRjb2xvcl9teXN0aWNfYXBwcm94O1xyXG4gICAgICAgIC8vSW5zdGVhZCBvZiB0aGUgbGluZSBiZWxvdyB5b3UgY291bGQgdXNlIEBpbmNsdWRlIGJvcmRlci1yYWRpdXMoJHJhZGl1cywgJHZlcnRpY2FsLXJhZGl1cylcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcblxyXG4gICAgICAgIC5jb2xvci1mb2xkZXIge1xyXG4gICAgICAgICAgICBjb2xvcjogJGNvbG9yLWZvbGRlcjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnBhZ2UtYmFyIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4O1xyXG4gICAgICAgICAgICBtYXJnaW46IDBweCAwcHggMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnRpdGxlIHtcclxuICAgICAgICAgICAgY29sb3I6ICRjb2xvci10aXRsZTtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmZvbGRlci1naXJkLWNvbnRhaW5lciB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgICAgICAgICAgbWFyZ2luOiAxZW0gMDtcclxuICAgICAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICAgICAgLW1vei1jb2x1bW4tZ2FwOiAxZW07XHJcbiAgICAgICAgICAgIC13ZWJraXQtY29sdW1uLWdhcDogMWVtO1xyXG4gICAgICAgICAgICBjb2x1bW4tZ2FwOiAxZW07XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAuZm9sZGVyLWdpcmQtaXRlbSB7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICAgICAgZmxleC1iYXNpczogMTQlO1xyXG4gICAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGxlZnQ7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAxZW07XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDAgMCAxZW07XHJcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA0cHggIWltcG9ydGFudDtcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogYXV0bztcclxuXHJcbiAgICAgICAgICAgICAgICBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAkbWF4LXdpZHRoLW1vYmlsZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGZsZXgtYmFzaXM6IDQ4JTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBAbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAkbWF4LXdpZHRoLW1vYmlsZSkgYW5kIChtYXgtd2lkdGg6ICRtYXgtd2lkdGgtdGFibGV0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZmxleC1iYXNpczogMzElO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIEBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6ICRtYXgtd2lkdGgtdGFibGV0KSBhbmQgKG1heC13aWR0aDogJG1heC13aWR0aC1kZXNrdG9wKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZmxleC1iYXNpczogMTglO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgICAgIGJveC1zaGFkb3c6IDNweCAzcHggMXB4IDAgI2NjYztcclxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3JfY2F0c2tpbGxfd2hpdGVfYXBwcm94O1xyXG4gICAgICAgICAgICAgICAgICAgIC5idG4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLmJ0biB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogbm9uZTtcclxuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgICAgICAgICAgcmlnaHQ6IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgdG9wOiAwO1xyXG4gICAgICAgICAgICAgICAgICAgIGZsb2F0OiByaWdodCAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgICAgICAgICAgICAgIHotaW5kZXg6IDk5OTk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLmRyb3Bkb3duLW1lbnUge1xyXG4gICAgICAgICAgICAgICAgICAgIHRvcDogMjBweDtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAubWVkaWEge1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgICAgICAgICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgLm1lZGlhLWxlZnQge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogJGNvbG9yLWZvbGRlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLm1lZGlhLWJvZHkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogaW5pdGlhbDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLm5hbWUge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nLXRvcDogNXB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYm90dG9tOiAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuZmlsZS1ncmlkLWNvbnRhaW5lciB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgICAgICAgICAgbWFyZ2luOiAxZW0gMDtcclxuICAgICAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICAgICAgLW1vei1jb2x1bW4tZ2FwOiAxZW07XHJcbiAgICAgICAgICAgIC13ZWJraXQtY29sdW1uLWdhcDogMWVtO1xyXG4gICAgICAgICAgICBjb2x1bW4tZ2FwOiAxZW07XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgICAgICAgICAgJi5uby1pdGVtIHtcclxuICAgICAgICAgICAgICAgIGNvbHVtbi1jb3VudDogMTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLmZpbGUtZ3JpZC1pdGVtIHtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICBmbGV4LWJhc2lzOiAxNCU7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAxZW07XHJcbiAgICAgICAgICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogNXB4O1xyXG5cclxuICAgICAgICAgICAgICAgIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6ICRtYXgtd2lkdGgtbW9iaWxlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZmxleC1iYXNpczogNDglO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIEBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6ICRtYXgtd2lkdGgtbW9iaWxlKSBhbmQgKG1heC13aWR0aDogJG1heC13aWR0aC10YWJsZXQpIHtcclxuICAgICAgICAgICAgICAgICAgICBmbGV4LWJhc2lzOiAzMSU7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDogJG1heC13aWR0aC10YWJsZXQpIGFuZCAobWF4LXdpZHRoOiAkbWF4LXdpZHRoLWRlc2t0b3ApIHtcclxuICAgICAgICAgICAgICAgICAgICBmbGV4LWJhc2lzOiAxOCU7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLmljb257XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDMwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgbWluLWhlaWdodDogMTg4cHg7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vYm94LXNoYWRvdzogM3B4IDNweCAxcHggMCAjY2NjO1xyXG4gICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDAuODtcclxuICAgICAgICAgICAgICAgICAgICAuYnRuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC5idG4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICAgICAgICAgIHJpZ2h0OiAwO1xyXG4gICAgICAgICAgICAgICAgICAgIHRvcDogMDtcclxuICAgICAgICAgICAgICAgICAgICBmbG9hdDogcmlnaHQgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgICAgICAgICAgICAgICB6LWluZGV4OiA5OTk5O1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC5kcm9wZG93bi1tZW51IHtcclxuICAgICAgICAgICAgICAgICAgICB0b3A6IDIwcHg7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaS5hdC1pY29uIHtcclxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICAgICAgICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgcmlnaHQ6IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgICAgICAgICAgICAgICAgdG9wOiAzMCU7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgZGl2Lm5hbWUge1xyXG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDVweCAxMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgICAgICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgICAgICAgICAgYm90dG9tOiAwO1xyXG4gICAgICAgICAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmluYm94IHtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgfVxyXG4gICAgLnRhYi1jb250ZW50IHtcclxuICAgICAgICBvdmVyZmxvdzogaW5oZXJpdDtcclxuICAgIH1cclxuICAgIC5pbmJveC1sb2FkaW5nIHtcclxuICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjJweDtcclxuICAgICAgICBmb250LXdlaWdodDogMzAwO1xyXG4gICAgfVxyXG4gICAgLmluYm94LWhlYWRlciB7XHJcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICBoMSB7XHJcbiAgICAgICAgICAgIGNvbG9yOiAkY29sb3Jfc3Rvcm1fZHVzdF9hcHByb3g7XHJcbiAgICAgICAgICAgIG1hcmdpbjogMCAwIDIwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLnBhZ2luYXRpb24tY29udHJvbCB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICAgICAgLnBhZ2luYXRpb24taW5mbyB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMTRweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICB0ciB7XHJcbiAgICAgICAgY29sb3I6ICRjb2xvcl90YXBhX2FwcHJveDtcclxuICAgICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgICAgbGFiZWwge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGkge1xyXG4gICAgICAgICAgICAmLmljb24tc3RhciB7XHJcbiAgICAgICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogJGNvbG9yX2dhbGxlcnlfYXBwcm94O1xyXG4gICAgICAgICAgICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICRjb2xvcl9lY3N0YXN5X2FwcHJveDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAmLmljb24tdHJhc2gge1xyXG4gICAgICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICYuaW5ib3gtc3RhcnRlZCB7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogJGNvbG9yX2Vjc3Rhc3lfYXBwcm94O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgICYudW5yZWFkIHRkIHtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICB0ZCB7XHJcbiAgICAgICAgJi50ZXh0LXJpZ2h0IHtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgICYuaW5ib3gtc21hbGwtY2VsbHMge1xyXG4gICAgICAgICAgICB3aWR0aDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgaS5pY29uLXBhcGVyLWNsaXAge1xyXG4gICAgICAgICAgICB0b3A6IDJweDtcclxuICAgICAgICAgICAgY29sb3I6ICRjb2xvcl9nZXlzZXJfYXBwcm94O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDE3cHg7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAudGFibGUge1xyXG4gICAgICAgIHRoIHtcclxuICAgICAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAkY29sb3JfY2F0c2tpbGxfd2hpdGVfYXBwcm94O1xyXG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiBzb2xpZCA1cHggJHdoaXRlO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0ZCB7XHJcbiAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICB0aCB7XHJcbiAgICAgICAgJi50ZXh0LXJpZ2h0IHtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVsLmluYm94LXNlbGVjdC1hbGwge1xyXG4gICAgICAgICAgICBjb2xvcjogJGNvbG9yX29zbG9fZ3JheV9hcHByb3g7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgICAgICAgICAgcGFkZGluZzogMXB4IDRweCAwO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB0Ym9keSB7XHJcbiAgICAgICAgYm9yZGVyLXRvcDogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gICAgLmluYm94LWRyYWZ0cyB7XHJcbiAgICAgICAgcGFkZGluZzogOHB4IDA7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGJvcmRlci10b3A6IHNvbGlkIDFweCAkY29sb3JfZ2FsbGVyeV9hcHByb3g7XHJcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogc29saWQgMXB4ICRjb2xvcl9nYWxsZXJ5X2FwcHJveDtcclxuICAgIH1cclxuICAgIC5pbnB1dC1hY3Rpb25zIC5idG4ge1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgfVxyXG4gICAgLnRhYmxlLWhvdmVyIHRib2R5IHRyOmhvdmVyIHtcclxuICAgICAgICA+IHRkIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogJGNvbG9yX2FsYWJhc3Rlcl9hcHByb3g7XHJcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogJGNvbG9yX2NhdHNraWxsX3doaXRlX2FwcHJveDtcclxuICAgICAgICB9XHJcbiAgICAgICAgPiB0aCB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICRjb2xvcl9hbGFiYXN0ZXJfYXBwcm94O1xyXG4gICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICRjb2xvcl9jYXRza2lsbF93aGl0ZV9hcHByb3g7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLnRhYmxlLXN0cmlwZWQgdGJvZHkgPiB0cjpudGgtY2hpbGQob2RkKSB7XHJcbiAgICAgICAgPiB0ZCB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICRjb2xvcl9hbGFiYXN0ZXJfYXBwcm94O1xyXG4gICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgID4gdGgge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAkY29sb3JfYWxhYmFzdGVyX2FwcHJveDtcclxuICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuJGJvcmRlci1jb2xvcjogI2RkZDtcclxuXHJcbi5hdC1pY29uIHtcclxuICAgIGZvbnQtZmFtaWx5OiBcIkZvbnRBd2Vzb21lXCI7XHJcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XHJcblxyXG4gICAgJi5hdC1pY29uLXhscywgJi5hdC1pY29uLXhsc3gge1xyXG4gICAgICAgICY6YmVmb3JlIHtcclxuICAgICAgICAgICAgY29udGVudDogXCJcXGYxYzNcIjtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29sb3I6IGZvcmVzdGdyZWVuO1xyXG4gICAgfVxyXG4gICAgJi5hdC1pY29uLWRvYywgJi5hdC1pY29uLWRvY3gge1xyXG4gICAgICAgICY6YmVmb3JlIHtcclxuICAgICAgICAgICAgY29udGVudDogXCJcXGYxYzJcIjtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29sb3I6IGNvcm5mbG93ZXJibHVlO1xyXG4gICAgfVxyXG4gICAgJi5hdC1pY29uLXR4dDpiZWZvcmUge1xyXG4gICAgICAgIGNvbnRlbnQ6IFwiXFxmMGY2XCI7XHJcbiAgICB9XHJcbiAgICAmLmF0LWljb24tcHB0eCB7XHJcbiAgICAgICAgJjpiZWZvcmUge1xyXG4gICAgICAgICAgICBjb250ZW50OiBcIlxcZjFjNFwiO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb2xvcjogI2U3NGMzYztcclxuICAgIH1cclxuICAgICYuYXQtaWNvbi1wZGYge1xyXG4gICAgICAgICY6YmVmb3JlIHtcclxuICAgICAgICAgICAgY29udGVudDogXCJcXGYxYzFcIjtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29sb3I6ICNjMDM5MmI7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5pbWctc2xpZGUge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogNDAwcHgpIHtcclxuICAgIC5maWxlLWdyaWQtY29udGFpbmVyLCAuZm9sZGVyLWdpcmQtY29udGFpbmVyIHtcclxuICAgICAgICAtbW96LWNvbHVtbi1jb3VudDogMjtcclxuICAgICAgICAtd2Via2l0LWNvbHVtbi1jb3VudDogMjtcclxuICAgICAgICBjb2x1bW4tY291bnQ6IDI7XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogNzAwcHgpIHtcclxuICAgIC5maWxlLWdyaWQtY29udGFpbmVyLCAuZm9sZGVyLWdpcmQtY29udGFpbmVyIHtcclxuICAgICAgICAtbW96LWNvbHVtbi1jb3VudDogMztcclxuICAgICAgICAtd2Via2l0LWNvbHVtbi1jb3VudDogMztcclxuICAgICAgICBjb2x1bW4tY291bnQ6IDM7XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogOTAwcHgpIHtcclxuICAgIC5maWxlLWdyaWQtY29udGFpbmVyLCAuZm9sZGVyLWdpcmQtY29udGFpbmVyIHtcclxuICAgICAgICAtbW96LWNvbHVtbi1jb3VudDogNDtcclxuICAgICAgICAtd2Via2l0LWNvbHVtbi1jb3VudDogNDtcclxuICAgICAgICBjb2x1bW4tY291bnQ6IDQ7XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogMTEwMHB4KSB7XHJcbiAgICAuZmlsZS1ncmlkLWNvbnRhaW5lciwgLmZvbGRlci1naXJkLWNvbnRhaW5lciB7XHJcbiAgICAgICAgLW1vei1jb2x1bW4tY291bnQ6IDU7XHJcbiAgICAgICAgLXdlYmtpdC1jb2x1bW4tY291bnQ6IDU7XHJcbiAgICAgICAgY29sdW1uLWNvdW50OiA1O1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuXHJcbiIsIiRkZWZhdWx0LWNvbG9yOiAjMjIyO1xyXG4kZm9udC1mYW1pbHk6IFwiQXJpYWxcIiwgdGFob21hLCBIZWx2ZXRpY2EgTmV1ZTtcclxuJGNvbG9yLWJsdWU6ICMzNTk4ZGM7XHJcbiRtYWluLWNvbG9yOiAjMDA3NDU1O1xyXG4kYm9yZGVyQ29sb3I6ICNkZGQ7XHJcbiRzZWNvbmQtY29sb3I6ICNiMDFhMWY7XHJcbiR0YWJsZS1iYWNrZ3JvdW5kLWNvbG9yOiAjMDA5Njg4O1xyXG4kYmx1ZTogIzAwN2JmZjtcclxuJGRhcmstYmx1ZTogIzAwNzJCQztcclxuJGJyaWdodC1ibHVlOiAjZGZmMGZkO1xyXG4kaW5kaWdvOiAjNjYxMGYyO1xyXG4kcHVycGxlOiAjNmY0MmMxO1xyXG4kcGluazogI2U4M2U4YztcclxuJHJlZDogI2RjMzU0NTtcclxuJG9yYW5nZTogI2ZkN2UxNDtcclxuJHllbGxvdzogI2ZmYzEwNztcclxuJGdyZWVuOiAjMjhhNzQ1O1xyXG4kdGVhbDogIzIwYzk5NztcclxuJGN5YW46ICMxN2EyYjg7XHJcbiR3aGl0ZTogI2ZmZjtcclxuJGdyYXk6ICM4NjhlOTY7XHJcbiRncmF5LWRhcms6ICMzNDNhNDA7XHJcbiRwcmltYXJ5OiAjMDA3YmZmO1xyXG4kc2Vjb25kYXJ5OiAjNmM3NTdkO1xyXG4kc3VjY2VzczogIzI4YTc0NTtcclxuJGluZm86ICMxN2EyYjg7XHJcbiR3YXJuaW5nOiAjZmZjMTA3O1xyXG4kZGFuZ2VyOiAjZGMzNTQ1O1xyXG4kbGlnaHQ6ICNmOGY5ZmE7XHJcbiRkYXJrOiAjMzQzYTQwO1xyXG4kbGFiZWwtY29sb3I6ICM2NjY7XHJcbiRiYWNrZ3JvdW5kLWNvbG9yOiAjRUNGMEYxO1xyXG4kYm9yZGVyQWN0aXZlQ29sb3I6ICM4MGJkZmY7XHJcbiRib3JkZXJSYWRpdXM6IDA7XHJcbiRkYXJrQmx1ZTogIzQ1QTJEMjtcclxuJGxpZ2h0R3JlZW46ICMyN2FlNjA7XHJcbiRsaWdodC1ibHVlOiAjZjVmN2Y3O1xyXG4kYnJpZ2h0R3JheTogIzc1NzU3NTtcclxuJG1heC13aWR0aC1tb2JpbGU6IDc2OHB4O1xyXG4kbWF4LXdpZHRoLXRhYmxldDogOTkycHg7XHJcbiRtYXgtd2lkdGgtZGVza3RvcDogMTI4MHB4O1xyXG5cclxuLy8gQkVHSU46IE1hcmdpblxyXG5AbWl4aW4gbmgtbWcoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW46ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWd0KCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLXRvcDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ2IoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1nbCgkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1ncigkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1yaWdodDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogTWFyZ2luXHJcblxyXG4vLyBCRUdJTjogUGFkZGluZ1xyXG5AbWl4aW4gbmgtcGQoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkdCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctdG9wOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkYigkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctYm90dG9tOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkbCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctbGVmdDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZHIoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBQYWRkaW5nXHJcblxyXG4vLyBCRUdJTjogV2lkdGhcclxuQG1peGluIG5oLXdpZHRoKCR3aWR0aCkge1xyXG4gICAgd2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG4gICAgbWluLXdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxuICAgIG1heC13aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogV2lkdGhcclxuXHJcbi8vIEJFR0lOOiBJY29uIFNpemVcclxuQG1peGluIG5oLXNpemUtaWNvbigkc2l6ZSkge1xyXG4gICAgd2lkdGg6ICRzaXplO1xyXG4gICAgaGVpZ2h0OiAkc2l6ZTtcclxuICAgIGZvbnQtc2l6ZTogJHNpemU7XHJcbn1cclxuXHJcbi8vIEVORDogSWNvbiBTaXplXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/modules/folders/model/file.model.ts":
/*!*****************************************************!*\
  !*** ./src/app/modules/folders/model/file.model.ts ***!
  \*****************************************************/
/*! exports provided: Files */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Files", function() { return Files; });
var Files = /** @class */ (function () {
    function Files(name, folderId, concurrencyStamp) {
        this.name = name ? name : '';
        this.folderId = folderId;
        this.concurrencyStamp = concurrencyStamp ? concurrencyStamp : '';
    }
    return Files;
}());



/***/ }),

/***/ "./src/app/modules/folders/model/folder.model.ts":
/*!*******************************************************!*\
  !*** ./src/app/modules/folders/model/folder.model.ts ***!
  \*******************************************************/
/*! exports provided: Folder */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Folder", function() { return Folder; });
var Folder = /** @class */ (function () {
    function Folder(name, parentId, concurrencyStamp) {
        this.name = name ? name : '';
        this.parentId = parentId;
        this.concurrencyStamp = concurrencyStamp ? concurrencyStamp : '';
    }
    return Folder;
}());



/***/ }),

/***/ "./src/app/modules/folders/service/file.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/modules/folders/service/file.service.ts ***!
  \*********************************************************/
/*! exports provided: FileService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileService", function() { return FileService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");







var FileService = /** @class */ (function () {
    function FileService(appConfig, toastr, spinnerService, http) {
        this.appConfig = appConfig;
        this.toastr = toastr;
        this.spinnerService = spinnerService;
        this.http = http;
        this.url = 'files/';
        this.url = "" + appConfig.FILE_MANAGEMENT + this.url;
    }
    FileService.prototype.getDetail = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get("" + this.url + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    FileService.prototype.insert = function (file) {
        var _this = this;
        return this.http.post("" + this.url, {
            name: file.name,
            folderId: file.folderId,
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    FileService.prototype.update = function (id, file) {
        var _this = this;
        return this.http.post("" + this.url + id, {
            name: file.name,
            folderId: file.folderId,
            concurrencyStamp: file.concurrencyStamp,
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    FileService.prototype.delete = function (id) {
        var _this = this;
        return this.http.delete("" + this.url + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    FileService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__["SpinnerService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]])
    ], FileService);
    return FileService;
}());



/***/ }),

/***/ "./src/app/modules/folders/service/folder.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/modules/folders/service/folder.service.ts ***!
  \***********************************************************/
/*! exports provided: FolderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FolderService", function() { return FolderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");







var FolderService = /** @class */ (function () {
    function FolderService(appConfig, toastr, spinnerService, http) {
        this.appConfig = appConfig;
        this.toastr = toastr;
        this.spinnerService = spinnerService;
        this.http = http;
        this.url = 'folders/';
        this.imageArray = ['.jpg', '.jpeg', '.gif', '.png', '.bmp'];
        this.url = "" + appConfig.FILE_MANAGEMENT + this.url;
    }
    FolderService.prototype.resolve = function (route, state) {
        var queryParams = route.queryParams;
        var folderId = queryParams.folderId;
        return this.search(folderId);
    };
    FolderService.prototype.searchByName = function (keyword, page, pageSize) {
        var _this = this;
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = this.appConfig.PAGE_SIZE; }
        this.spinnerService.show();
        return this.http.get(this.url + "names", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString())
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            if (result.files && result.files.length > 0) {
                result.files.forEach(function (item) {
                    item.absoluteUrl = "" + _this.appConfig.FILE_URL + item.url;
                    item.sizeString = _this.bytesToSize(item.size);
                    item.isImage = _this.checkIsImage(item.extension);
                });
            }
            return result;
        }));
    };
    FolderService.prototype.search = function (folderId) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get("" + this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpParams"]()
                .set('folderId', folderId ? folderId.toString() : '')
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            if (result.files && result.files.length > 0) {
                result.files.forEach(function (item) {
                    item.absoluteUrl = "" + _this.appConfig.FILE_URL + item.url;
                    item.sizeString = _this.bytesToSize(item.size);
                    item.isImage = _this.checkIsImage(item.extension);
                });
            }
            return result;
        }));
    };
    FolderService.prototype.getChildren = function (folderId) {
        return this.http.get(this.url + "/children/" + folderId, {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            return result;
        }));
    };
    FolderService.prototype.getTree = function () {
        return this.http.get(this.url + "trees");
    };
    FolderService.prototype.insert = function (folder) {
        var _this = this;
        return this.http.post("" + this.url, {
            name: folder.name,
            parentId: folder.parentId,
            concurrencyStamp: folder.concurrencyStamp,
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    FolderService.prototype.getDetail = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get("" + this.url + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    FolderService.prototype.update = function (id, folder) {
        var _this = this;
        return this.http.post("" + this.url + id, {
            name: folder.name,
            parentId: folder.parentId,
            concurrencyStamp: folder.concurrencyStamp,
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    FolderService.prototype.updateName = function (id, concurrencyStamp, name) {
        var _this = this;
        return this.http.post("" + this.url + id + "/name", '', {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpParams"]()
                .set('concurrencyStamp', concurrencyStamp ? concurrencyStamp : '')
                .set('name', name ? name : '')
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    FolderService.prototype.delete = function (id) {
        var _this = this;
        return this.http.delete("" + this.url + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    FolderService.prototype.bytesToSize = function (bytes) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes === 0) {
            return "0 " + sizes[0];
        }
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)).toString(), 10);
        if (i === 0) {
            return bytes + " " + sizes[i] + ")";
        }
        return (bytes / (Math.pow(1024, i))).toFixed(1) + " " + sizes[i];
    };
    FolderService.prototype.checkIsImage = function (extension) {
        return ['png', 'jpg', 'jpeg', 'gif'].indexOf(extension) > -1;
    };
    FolderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__["SpinnerService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]])
    ], FolderService);
    return FolderService;
}());



/***/ }),

/***/ "./src/app/modules/folders/slider-image/slider-image.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/modules/folders/slider-image/slider-image.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #sliderImageModal [size]=\"'lg'\">\r\n    <nh-modal-header class=\"uppercase\">\r\n        <i class=\"fa fa-picture-o\" aria-hidden=\"true\"></i>\r\n        <span>{{imageSelect?.name}}</span>\r\n    </nh-modal-header>\r\n    <nh-modal-content class=\"center\">\r\n        <div class=\"w100pc middle img-slide\" [ngStyle]=\"{'height': height+ 'px'}\">\r\n            <img ghmImage width=\"100%\" class=\"img-reponsive\" [src]=\"imageSelect?.url\"/>\r\n        </div>\r\n    </nh-modal-content>\r\n    <nh-modal-footer>\r\n        <div class=\"center cm-mgb-10\">\r\n            <button [disabled]=\"!isEnablePrevious\" clas type=\"button\"\r\n                    class=\"btn blue cm-mgr-5\"\r\n                    title=\"Previous\" (click)=\"previousImage()\">\r\n                <i class=\"fa fa-step-backward\" aria-hidden=\"true\"></i>\r\n            </button>\r\n            <button class=\"cm-mgl-5\" [disabled]=\"!isEnableNex\" type=\"button\"\r\n                    class=\"btn blue\"\r\n                    title=\"Next\" (click)=\"nextImage()\">\r\n                <i class=\"fa fa-step-forward\" aria-hidden=\"true\"></i>\r\n            </button>\r\n            <!--<ghm-button classes=\"btn default\"-->\r\n                        <!--nh-dismiss=\"true\"-->\r\n                        <!--[type]=\"'button'\">-->\r\n                <!--<span i18n=\"@@closr\">Close</span>-->\r\n            <!--</ghm-button>-->\r\n        </div>\r\n    </nh-modal-footer>\r\n</nh-modal>"

/***/ }),

/***/ "./src/app/modules/folders/slider-image/slider-image.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/modules/folders/slider-image/slider-image.component.ts ***!
  \************************************************************************/
/*! exports provided: SliderImageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SliderImageComponent", function() { return SliderImageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_3__);




var SliderImageComponent = /** @class */ (function () {
    function SliderImageComponent(cdr) {
        this.cdr = cdr;
    }
    SliderImageComponent.prototype.ngAfterViewInit = function () {
        this.height = window.innerHeight - 220;
        this.cdr.detectChanges();
    };
    SliderImageComponent.prototype.onResize = function (event) {
        this.height = window.innerHeight - 220;
    };
    SliderImageComponent.prototype.show = function () {
        this.sliderImageModal.open();
        this.checkIndexImage();
    };
    SliderImageComponent.prototype.showImage = function (item, isShowModal) {
        if (!item.isClick) {
            item.isClick = true;
            this.imageSelect = item;
            this.checkIndexImage();
            item.isClick = false;
        }
    };
    SliderImageComponent.prototype.checkIndexImage = function () {
        var _this = this;
        this.indexFile = lodash__WEBPACK_IMPORTED_MODULE_3__["findIndex"](this.listImage, function (item) {
            return item.id === _this.imageSelect.id;
        });
        if (this.indexFile === 0 && this.listImage.length > 1) {
            this.isEnablePrevious = false;
            this.isEnableNex = true;
        }
        else if (this.indexFile === this.listImage.length - 1 && this.listImage.length > 1) {
            this.isEnableNex = false;
            this.isEnablePrevious = true;
        }
        else if (this.listImage.length === 1) {
            this.isEnablePrevious = false;
            this.isEnableNex = false;
        }
        else {
            this.isEnablePrevious = true;
            this.isEnableNex = true;
        }
    };
    SliderImageComponent.prototype.nextImage = function () {
        this.imageSelect = this.listImage[this.indexFile + 1];
        this.showImage(this.imageSelect, false);
    };
    SliderImageComponent.prototype.previousImage = function () {
        this.imageSelect = this.listImage[this.indexFile - 1];
        this.showImage(this.imageSelect, false);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('sliderImageModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_2__["NhModalComponent"])
    ], SliderImageComponent.prototype, "sliderImageModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:resize', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], SliderImageComponent.prototype, "onResize", null);
    SliderImageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-slider-image',
            template: __webpack_require__(/*! ./slider-image.component.html */ "./src/app/modules/folders/slider-image/slider-image.component.html"),
            styles: [__webpack_require__(/*! ../folder.scss */ "./src/app/modules/folders/folder.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
    ], SliderImageComponent);
    return SliderImageComponent;
}());



/***/ }),

/***/ "./src/app/modules/folders/viewmodels/folder-search.viewmodel.ts":
/*!***********************************************************************!*\
  !*** ./src/app/modules/folders/viewmodels/folder-search.viewmodel.ts ***!
  \***********************************************************************/
/*! exports provided: FolderSearchViewModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FolderSearchViewModel", function() { return FolderSearchViewModel; });
var FolderSearchViewModel = /** @class */ (function () {
    function FolderSearchViewModel(id, name, parentId, isFolder, isEditName, concurrencyStamp) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
        this.isFolder = true;
    }
    return FolderSearchViewModel;
}());



/***/ }),

/***/ "./src/app/shareds/constants/view-type.const.ts":
/*!******************************************************!*\
  !*** ./src/app/shareds/constants/view-type.const.ts ***!
  \******************************************************/
/*! exports provided: ViewType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewType", function() { return ViewType; });
var ViewType = {
    list: 0,
    gird: 1,
};


/***/ }),

/***/ "./src/app/shareds/directives/nh-dropdown/nh-dropdown.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/shareds/directives/nh-dropdown/nh-dropdown.module.ts ***!
  \**********************************************************************/
/*! exports provided: NhDropdownModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NhDropdownModule", function() { return NhDropdownModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _nh_dropdown_nh_dropdown_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./nh-dropdown/nh-dropdown.component */ "./src/app/shareds/directives/nh-dropdown/nh-dropdown/nh-dropdown.component.ts");




var NhDropdownModule = /** @class */ (function () {
    function NhDropdownModule() {
    }
    NhDropdownModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
            ],
            declarations: [_nh_dropdown_nh_dropdown_component__WEBPACK_IMPORTED_MODULE_3__["NhDropdownComponent"]],
            exports: [_nh_dropdown_nh_dropdown_component__WEBPACK_IMPORTED_MODULE_3__["NhDropdownComponent"]]
        })
    ], NhDropdownModule);
    return NhDropdownModule;
}());



/***/ }),

/***/ "./src/app/shareds/directives/nh-dropdown/nh-dropdown/nh-dropdown.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/shareds/directives/nh-dropdown/nh-dropdown/nh-dropdown.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ng-content></ng-content>\r\n\r\n"

/***/ }),

/***/ "./src/app/shareds/directives/nh-dropdown/nh-dropdown/nh-dropdown.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/shareds/directives/nh-dropdown/nh-dropdown/nh-dropdown.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nh-dropdown {\n  display: inline-block;\n  position: relative; }\n  nh-dropdown.nh-dropdown-open .nh-dropdown-menu {\n    display: block;\n    top: 100%;\n    left: 0;\n    background: white;\n    border: 1px solid #ddd; }\n  nh-dropdown .nh-dropdown-menu {\n    position: absolute;\n    padding-left: 0;\n    margin-bottom: 0;\n    list-style: none;\n    display: none;\n    min-width: 175px;\n    z-index: 1000;\n    margin-top: 10px;\n    text-align: left; }\n  nh-dropdown .nh-dropdown-menu:before {\n      position: absolute;\n      top: -8px;\n      left: 9px;\n      right: auto;\n      display: inline-block !important;\n      border-right: 8px solid transparent;\n      border-bottom: 8px solid #ddd;\n      border-left: 8px solid transparent;\n      content: ''; }\n  nh-dropdown .nh-dropdown-menu.right {\n      right: 0 !important;\n      left: auto; }\n  nh-dropdown .nh-dropdown-menu.right:before {\n        left: auto;\n        right: 18px; }\n  nh-dropdown .nh-dropdown-menu li a {\n      padding: 8px 16px;\n      color: #6f6f6f;\n      text-decoration: none;\n      clear: both;\n      font-weight: 300;\n      line-height: 18px;\n      white-space: nowrap;\n      display: flex;\n      align-items: center; }\n  nh-dropdown .nh-dropdown-menu li a:hover {\n        background: #eee; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkcy9kaXJlY3RpdmVzL25oLWRyb3Bkb3duL25oLWRyb3Bkb3duL0Q6XFxQcm9qZWN0XFxHaG1BcHBsaWNhdGlvblxcY2xpZW50c1xcZ2htYXBwbGljYXRpb25jbGllbnQvc3JjXFxhcHBcXHNoYXJlZHNcXGRpcmVjdGl2ZXNcXG5oLWRyb3Bkb3duXFxuaC1kcm9wZG93blxcbmgtZHJvcGRvd24uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBR0E7RUFDSSxxQkFBcUI7RUFDckIsa0JBQWtCLEVBQUE7RUFGdEI7SUFNWSxjQUFjO0lBQ2QsU0FBUztJQUNULE9BQU87SUFDUCxpQkFBaUI7SUFDakIsc0JBYk0sRUFBQTtFQUdsQjtJQWVRLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLGFBQWE7SUFDYixnQkFBZ0I7SUFDaEIsZ0JBQWdCLEVBQUE7RUF2QnhCO01BMEJZLGtCQUFrQjtNQUNsQixTQUFTO01BQ1QsU0FBUztNQUNULFdBQVc7TUFDWCxnQ0FBZ0M7TUFDaEMsbUNBQW1DO01BQ25DLDZCQW5DTTtNQW9DTixrQ0FBa0M7TUFDbEMsV0FBVyxFQUFBO0VBbEN2QjtNQXNDWSxtQkFBbUI7TUFDbkIsVUFBVSxFQUFBO0VBdkN0QjtRQTBDZ0IsVUFBVTtRQUNWLFdBQVcsRUFBQTtFQTNDM0I7TUFnRFksaUJBQWlCO01BQ2pCLGNBQWM7TUFDZCxxQkFBcUI7TUFDckIsV0FBVztNQUNYLGdCQUFnQjtNQUNoQixpQkFBaUI7TUFDakIsbUJBQW1CO01BQ25CLGFBQWE7TUFDYixtQkFBbUIsRUFBQTtFQXhEL0I7UUEyRGdCLGdCQTdEVyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkcy9kaXJlY3RpdmVzL25oLWRyb3Bkb3duL25oLWRyb3Bkb3duL25oLWRyb3Bkb3duLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiJGJvcmRlckNvbG9yOiAjZGRkO1xyXG4kaG92ZXJCYWNrZ3JvdW5kQ29sb3I6ICNlZWU7XHJcblxyXG5uaC1kcm9wZG93biB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcblxyXG4gICAgJi5uaC1kcm9wZG93bi1vcGVuIHtcclxuICAgICAgICAubmgtZHJvcGRvd24tbWVudSB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICB0b3A6IDEwMCU7XHJcbiAgICAgICAgICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAkYm9yZGVyQ29sb3I7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5uaC1kcm9wZG93bi1tZW51IHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAwO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICAgICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICAgIG1pbi13aWR0aDogMTc1cHg7XHJcbiAgICAgICAgei1pbmRleDogMTAwMDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcblxyXG4gICAgICAgICY6YmVmb3JlIHtcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IC04cHg7XHJcbiAgICAgICAgICAgIGxlZnQ6IDlweDtcclxuICAgICAgICAgICAgcmlnaHQ6IGF1dG87XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jayAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICBib3JkZXItcmlnaHQ6IDhweCBzb2xpZCB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogOHB4IHNvbGlkICRib3JkZXJDb2xvcjtcclxuICAgICAgICAgICAgYm9yZGVyLWxlZnQ6IDhweCBzb2xpZCB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgY29udGVudDogJyc7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAmLnJpZ2h0IHtcclxuICAgICAgICAgICAgcmlnaHQ6IDAgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgbGVmdDogYXV0bztcclxuXHJcbiAgICAgICAgICAgICY6YmVmb3JlIHtcclxuICAgICAgICAgICAgICAgIGxlZnQ6IGF1dG87XHJcbiAgICAgICAgICAgICAgICByaWdodDogMThweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGkgYSB7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDhweCAxNnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzZmNmY2ZjtcclxuICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgICAgICBjbGVhcjogYm90aDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDMwMDtcclxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDE4cHg7XHJcbiAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblxyXG4gICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICRob3ZlckJhY2tncm91bmRDb2xvcjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/shareds/directives/nh-dropdown/nh-dropdown/nh-dropdown.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/shareds/directives/nh-dropdown/nh-dropdown/nh-dropdown.component.ts ***!
  \*************************************************************************************/
/*! exports provided: NhDropdownComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NhDropdownComponent", function() { return NhDropdownComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NhDropdownComponent = /** @class */ (function () {
    function NhDropdownComponent(el, renderer) {
        this.el = el;
        this.renderer = renderer;
        this.shown = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.hidden = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.isOpen = false;
    }
    NhDropdownComponent.prototype.documentClick = function (event) {
        if (!this.el.nativeElement.contains(event.target)) {
            this.closeDropdown();
        }
    };
    NhDropdownComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.renderer.listen(this.el.nativeElement, 'click', function (event) {
            _this.toggleDropdown(event);
        });
    };
    NhDropdownComponent.prototype.toggleDropdown = function (event) {
        this.isOpen = !this.isOpen;
        if (this.isOpen) {
            this.renderer.addClass(this.el.nativeElement, 'nh-dropdown-open');
            this.shown.emit();
        }
        else {
            this.closeDropdown();
        }
    };
    NhDropdownComponent.prototype.closeDropdown = function () {
        this.isOpen = false;
        this.renderer.removeClass(this.el.nativeElement, 'nh-dropdown-open');
        this.hidden.emit();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhDropdownComponent.prototype, "shown", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhDropdownComponent.prototype, "hidden", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('document:click', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], NhDropdownComponent.prototype, "documentClick", null);
    NhDropdownComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'nh-dropdown',
            template: __webpack_require__(/*! ./nh-dropdown.component.html */ "./src/app/shareds/directives/nh-dropdown/nh-dropdown/nh-dropdown.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./nh-dropdown.component.scss */ "./src/app/shareds/directives/nh-dropdown/nh-dropdown/nh-dropdown.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]])
    ], NhDropdownComponent);
    return NhDropdownComponent;
}());



/***/ }),

/***/ "./src/app/shareds/models/filter-link.model.ts":
/*!*****************************************************!*\
  !*** ./src/app/shareds/models/filter-link.model.ts ***!
  \*****************************************************/
/*! exports provided: FilterLink */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterLink", function() { return FilterLink; });
var FilterLink = /** @class */ (function () {
    function FilterLink(key, value) {
        this.key = key;
        this.value = value;
    }
    return FilterLink;
}());



/***/ }),

/***/ "./src/app/shareds/services/helper.service.ts":
/*!****************************************************!*\
  !*** ./src/app/shareds/services/helper.service.ts ***!
  \****************************************************/
/*! exports provided: HelperService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelperService", function() { return HelperService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

/**
 * Created by HoangNH on 3/22/2017.
 */

var HelperService = /** @class */ (function () {
    function HelperService(_componentFactoryResolver) {
        this._componentFactoryResolver = _componentFactoryResolver;
    }
    HelperService.prototype.createComponent = function (viewContainerRef, component) {
        var componentFactory = this._componentFactoryResolver.resolveComponentFactory(component);
        var componentRef = viewContainerRef.createComponent(componentFactory);
        return componentRef.instance;
    };
    HelperService.prototype.openPrintWindow = function (title, content, style) {
        var htmlContent = " <!DOCTYPE html>\n                    <html>\n                    <head>\n                        <title>" + title + "</title>\n                        <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\" rel=\"stylesheet\" integrity=\"sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN\"\n                            crossorigin=\"anonymous\">\n                        <style>\n                        body{\n                        color: white;\n                        }\n                            @page {\n        size: auto;\n        margin: 0mm 0;\n    }\n    @media print {\n        * {\n            margin: 0;\n            padding: 0;\n            font-size: 11px;\n            box-sizing: border-box;\n        }\n        html,\n        body {\n            color: black;\n            width: 100%;\n            height: 100%;\n            padding: 0;\n            margin: 0;\n        }\n        header {\n            padding-top: 10px;\n        }\n        header,\n        footer {\n            text-align: center;\n        }\n        header img {\n            width: 70%;\n        }\n        footer img {\n            width: 100%;\n        }\n        .print-barcode-page{\n        margin-top: 0; margin-bottom: 0;\n        padding-top: -5;\n        padding-bottom: 0;\n        margin-left: 0px !important;\n        display: block;\n        width: 100%;\n        }        \n        .print-barcode-page .barcode-item{\n            padding-left: 0px !important;\n            padding-right: 0 !important;\n            padding-top: 0 !important;\n            padding-bottom: 5px !important;\n            overflow: hidden;\n            width: 50%;\n            display: inline-block;\n            float: left;\n        }\n        .print-barcode-page .barcode-item .barcode-wrapper{\n        display: block;\n        width: 100%;\n        text-align: center !important;\n        }\n        .print-barcode-page .barcode-item img{\n        display: block;\n        width: 100%;\n        }\n        .print-page {\n            width: 100%;\n            height: 100%;\n            position: relative;\n            padding: 20px 20px;\n        }\n        .print-page footer {\n            position: absolute;\n            bottom: 0;\n            left: 0;\n            right: 0;\n        }\n        div.wrapper-table {\n            padding: 0 30px;\n            text-align: center;\n        }\n        table.bordered {\n            border: 1px solid black;\n            width: 100%;\n            max-width: 100%;\n            margin-bottom: 1rem;\n            border-collapse: collapse;\n            background-color: transparent;\n            margin-top: 20px;\n        }\n        table.bordered thead tr th,\n        table.bordered tbody tr td {\n            border: 1px solid black;\n            font-size: 12px !important;\n            text-align: center;\n            padding: 3px;\n        }\n        table.bordered tbody tr td a {\n            text-decoration: none;\n            text-align: left;\n            font-size: 14px;\n        }\n        .middle {\n            vertical-align: middle;\n        }\n        .pr-w-30 {\n            width: 30px !important;\n        }\n        .pr-w-27 {\n            width: 27px !important;\n        }\n        .pr-w-70 {\n            width: 70px !important;\n            min-width: 70px !important;\n            max-width: 70px !important;\n        }\n        .pr-w-100 {\n            width: 100px !important;\n        }        \n        .pr-w-55 {\n            width: 55px !important;\n            min-width: 55px !important;\n            max-width: 55px !important;\n        }\n        .pd-5{\n        padding: 5px !important;        \n        }\n        .pdr-5{\n        padding-right: 5px !important;        \n        }\n        .pdl-5{\n        padding-left: 5px !important;        \n        }\n        .pd-10{\n        padding: 10px !important;        \n        }\n        .w70 {\n        width: 70px !important;\n        }\n        .w50{\n        wdith: 50px !important;\n        }\n        .w150 {\n            width: 150px !important;\n        }\n        .w250{\n        width: 250px !important;\n        }\n        .center {\n            text-align: center;\n        }\n        .pr-va-top {\n            vertical-align: top !important;\n        }\n        .page-break {\n            page-break-after: always;\n            border-top: 1px solid transparent;\n            margin: 1px;\n        }\n        .visible-print {\n            display: block;\n        }\n        .hidden-print {\n            display: none;\n        }\n        .text-left {\n            text-align: left !important;\n        }\n        .text-right {\n            text-align: right !important;\n        }\n        .w100pc {\n            width: 100%;\n        }\n        .uppercase {\n            text-transform: uppercase;\n        }\n        table .dotted-control {\n            border-bottom: 1px dotted black;\n            text-align: left;\n        }\n\n        table td {\n            padding-top: 3px;\n            padding-bottom: 3px;\n        }\n\n        table td div.control-group {\n            display: table;\n            width: 100%;\n        }\n\n        table td div.control-group label {\n            width: 1%;\n            white-space: nowrap;\n        }\n\n        table td div.control-group label,\n        table td div.control-group div {\n            display: table-cell;\n        }\n        " + style + "\n    }\n                        </style>\n                     </head>\n                     <body onload=\"window.print();window.close()\">\n                     " + content + "\n                     </body>\n                     </html>\n        ";
        var popupWin;
        var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : 0;
        var dualScreenTop = window.screenTop !== undefined ? window.screenTop : 0;
        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
        var w = window.outerWidth;
        var h = window.outerHeight;
        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;
        popupWin = window.open('', '_blank', 'width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
        popupWin.document.open();
        popupWin.document.write(htmlContent);
        popupWin.document.close();
    };
    HelperService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"]])
    ], HelperService);
    return HelperService;
}());



/***/ })

}]);
//# sourceMappingURL=modules-folders-folder-module.js.map