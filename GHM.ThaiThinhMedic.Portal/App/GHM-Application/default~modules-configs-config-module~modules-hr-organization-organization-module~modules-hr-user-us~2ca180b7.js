(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~modules-configs-config-module~modules-hr-organization-organization-module~modules-hr-user-us~2ca180b7"],{

/***/ "./src/app/shareds/components/nh-suggestion/nh-suggestion.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/shareds/components/nh-suggestion/nh-suggestion.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"nhs-container\" [class.disabled]=\"readonly\"\r\n     [class.active]=\"isActive\">\r\n    <div class=\"nhs-search-wrapper\"\r\n         (click)=\"activeSuggestion($event)\">\r\n        <div class=\"nhs-search-content\">\r\n            <ng-container *ngIf=\"multiple; else singleTemplate\">\r\n                <ul class=\"nhs-selected-wrapper\" *ngIf=\"selectedItems?.length > 0\">\r\n                    <ng-container *ngFor=\"let item of selectedItems\">\r\n                        <li class=\"nhs-item-selected\" *ngIf=\"item.id && item.name\">\r\n                            <div class=\"nhs-item\">\r\n                                {{ item.image }}\r\n                                <div class=\"nhs-item-image\" *ngIf=\"item.image\">\r\n                                    <img src=\"{{ item.image }}\" alt=\"{{ item.name }}\">\r\n                                </div><!-- END: .nhs-item-image -->\r\n                                <div class=\"nhs-item-info\">\r\n                                    <i class=\"{{ item.icon }}\" *ngIf=\"item.icon\"></i>\r\n                                    <span class=\"name\">{{ item.name }}</span>\r\n                                </div>\r\n                                <span class=\"remove\" (click)=\"removeSelectedItem(item)\">\r\n                                <svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" focusable=\"false\"\r\n                                     role=\"presentation\">\r\n                                    <path\r\n                                        d=\"M12 10.586L6.707 5.293a1 1 0 0 0-1.414 1.414L10.586 12l-5.293 5.293a1 1 0 0 0 1.414 1.414L12 13.414l5.293 5.293a1 1 0 0 0 1.414-1.414L13.414 12l5.293-5.293a1 1 0 1 0-1.414-1.414L12 10.586z\"\r\n                                        fill=\"currentColor\">\r\n                                    </path>\r\n                                </svg>\r\n                            </span><!-- END: .remove -->\r\n                            </div><!-- END: .nhs-item -->\r\n                        </li>\r\n                    </ng-container>\r\n                </ul><!-- END: .nhs-selected-wrapper -->\r\n            </ng-container><!-- END: display selected users -->\r\n            <div class=\"nhs-search-input\"\r\n                 *ngIf=\"isShowSearchBox\">\r\n                <input\r\n                    [attr.id]=\"id\"\r\n                    type=\"text\"\r\n                    [(ngModel)]=\"keyword\"\r\n                    name=\"searchSuggestion\"\r\n                    autocomplete=\"off\"\r\n                    placeholder=\"{{placeholder}}\"\r\n                    (keydown.enter)=\"$event.preventDefault()\"\r\n                    (keyup)=\"inputKeyUp($event)\">\r\n            </div><!-- END: .nhs-search-input -->\r\n        </div><!-- END: .nhs-search-content -->\r\n        <div class=\"nhs-search-icon\">\r\n            <svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" focusable=\"false\" role=\"presentation\">\r\n                <path\r\n                    d=\"M16.436 15.085l3.94 4.01a1 1 0 0 1-1.425 1.402l-3.938-4.006a7.5 7.5 0 1 1 1.423-1.406zM10.5 16a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11z\"\r\n                    fill=\"currentColor\" fill-rule=\"evenodd\"></path>\r\n            </svg>\r\n        </div><!-- END: .nhs-search-icon -->\r\n    </div><!-- END: .nhs-search-wrapper -->\r\n    <div class=\"nhs-search-result-wrapper\"\r\n         #searchResultContainer\r\n         *ngIf=\"isActive\">\r\n        <ul>\r\n            <li class=\"searching\" *ngIf=\"loading; else loadDoneTemplate\">\r\n                <div class=\"m-loader m-loader--brand\"></div>\r\n            </li>\r\n            <li i18n=\"@@cantFindPerson\" *ngIf=\"keyword && sources.length === 0 && !isLoading\">\r\n                <ng-container *ngIf=\"allowAdd; else notFoundMessage\">\r\n                    <a href=\"javascript://\" (click)=\"add()\"><span i18n=\"@@add\">Add</span>: {{ keyword }}</a>\r\n                </ng-container>\r\n                <ng-template #notFoundMessage>\r\n                    Không tìm thấy kết quả với từ khóa: \"{{ keyword }}\"\r\n                </ng-template>\r\n            </li>\r\n            <li i18n=\"@@loadMore\" *ngIf=\"currentPage < totalPages\" (click)=\"loadMore($event)\" class=\"load-more\">\r\n                <a href=\"javascript://\">Tải thêm</a>\r\n            </li>\r\n        </ul>\r\n    </div><!-- END: .nhs-search-result-wrapper -->\r\n</div><!-- END: .nhs-container -->\r\n\r\n<ng-template #loadDoneTemplate>\r\n    <li class=\"nhs-item\"\r\n        *ngFor=\"let item of sources\"\r\n        [class.active]=\"item.isActive\"\r\n        [class.selected]=\"item.isSelected\"\r\n        (click)=\"selectItem(item)\">\r\n        <div class=\"nhs-item-image\" *ngIf=\"item.image\">\r\n            <img src=\"{{ item.image }}\" alt=\"{{ item.name }}\">\r\n        </div><!-- END: .nhs-item-image -->\r\n        <div class=\"nhs-item-info\">\r\n            <i class=\"{{ item.icon }}\" *ngIf=\"item.icon\"></i>\r\n            <h2 class=\"nh-suggestion-name\">{{ item.name }}</h2>\r\n            <div class=\"nh-suggestion-des\" *ngIf=\"item.description\">{{ item.description }}</div>\r\n        </div><!-- END: .nhs-item-info -->\r\n    </li><!-- END: .nhs-item -->\r\n</ng-template><!-- END: search result template -->\r\n\r\n<ng-template #singleTemplate>\r\n    <div class=\"nhs-item\" *ngIf=\"selectedItem && !isShowSearchBox\">\r\n        <div class=\"nhs-item-info\" (click)=\"showSearchBox($event)\">\r\n            <i class=\"{{ selectedItem?.icon }}\" *ngIf=\"selectedItem?.icon\"></i>\r\n            <span>{{ selectedItem?.name }}</span>\r\n        </div>\r\n        <span class=\"remove\" (click)=\"removeSelectedItem(selectedItem)\">\r\n            <svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" focusable=\"false\"\r\n                 role=\"presentation\">\r\n                <path\r\n                    d=\"M12 10.586L6.707 5.293a1 1 0 0 0-1.414 1.414L10.586 12l-5.293 5.293a1 1 0 0 0 1.414 1.414L12 13.414l5.293 5.293a1 1 0 0 0 1.414-1.414L13.414 12l5.293-5.293a1 1 0 1 0-1.414-1.414L12 10.586z\"\r\n                    fill=\"currentColor\">\r\n                </path>\r\n            </svg>\r\n        </span>\r\n    </div><!-- END: .nhs-item -->\r\n</ng-template><!-- END: single selected template -->\r\n"

/***/ }),

/***/ "./src/app/shareds/components/nh-suggestion/nh-suggestion.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/shareds/components/nh-suggestion/nh-suggestion.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nh-suggestion.has-error .nhs-container {\n  border: 1px solid #dc3545 !important; }\n\n.rounded-avatar {\n  border-radius: 50%; }\n\n.avatar-wrapper {\n  overflow: hidden; }\n\n.avatar-xs {\n  width: 16px;\n  hight: 16px; }\n\n.avatar-sm {\n  width: 32px;\n  height: 32px; }\n\n.nhs-container {\n  border: 1px solid #ddd;\n  background: #fff;\n  border-radius: 5px !important;\n  position: relative;\n  z-index: 1000; }\n\n.nhs-container:hover {\n    cursor: pointer; }\n\n.nhs-container.active {\n    border: 1px solid #80bdff;\n    background: white;\n    box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25); }\n\n.nhs-container.active .nhs-search-wrapper .nhs-search-content .nhs-search-input input {\n      background: white;\n      border: none;\n      outline: none; }\n\n.nhs-container.active .nhs-search-wrapper .nhs-search-content .nhs-item {\n      margin-bottom: 0 !important; }\n\n.nhs-container ul {\n    list-style: none;\n    padding-left: 0;\n    margin-bottom: 0; }\n\n.nhs-container .nhs-search-wrapper {\n    align-items: center;\n    display: flex;\n    width: 100%;\n    min-height: 33px; }\n\n.nhs-container .nhs-search-wrapper .nhs-search-content {\n      white-space: nowrap;\n      width: 100%;\n      flex: 1 1 auto;\n      margin: 3px 8px; }\n\n.nhs-container .nhs-search-wrapper .nhs-search-content .nhs-search-input {\n        white-space: nowrap;\n        width: 100%;\n        flex: 1 1 auto;\n        margin: 3px 8px; }\n\n.nhs-container .nhs-search-wrapper .nhs-search-content .nhs-search-input input {\n          border: none;\n          display: block;\n          width: 100%;\n          background: #fff; }\n\n.nhs-container .nhs-search-wrapper .nhs-search-content .nhs-search-input input:focus, .nhs-container .nhs-search-wrapper .nhs-search-content .nhs-search-input input:visited, .nhs-container .nhs-search-wrapper .nhs-search-content .nhs-search-input input:active {\n            outline: none !important;\n            border: none !important;\n            box-shadow: none !important; }\n\n.nhs-container .nhs-search-wrapper .nhs-search-content ul {\n        display: flex;\n        flex-wrap: wrap;\n        justify-content: flex-start;\n        width: 100%; }\n\n.nhs-container .nhs-search-wrapper .nhs-search-content ul li.nhs-item-selected {\n          box-sizing: border-box;\n          display: inline-block; }\n\n.nhs-container .nhs-search-wrapper .nhs-search-content ul li.nhs-item-selected div.nhs-item {\n            background-color: #f4f5f7;\n            color: #253858;\n            cursor: default;\n            display: flex;\n            height: 20px;\n            line-height: 1;\n            border-radius: 3px;\n            margin: 4px !important;\n            padding: 0px;\n            overflow: initial;\n            -webkit-user-select: none;\n               -moz-user-select: none;\n                -ms-user-select: none;\n                    user-select: none; }\n\n.nhs-container .nhs-search-wrapper .nhs-search-content ul li.nhs-item-selected div.nhs-item .avatar-wrapper {\n              align-items: center;\n              display: flex;\n              justify-content: center;\n              padding-left: 4px; }\n\n.nhs-container .nhs-search-wrapper .nhs-search-content ul li.nhs-item-selected div.nhs-item .user-info {\n              margin: 0 5px;\n              margin-bottom: 0 !important; }\n\n.nhs-container .nhs-search-wrapper .nhs-search-content ul li.nhs-item-selected div.nhs-item .user-info .full-name {\n                font-size: 14px;\n                font-weight: normal;\n                line-height: 1;\n                margin-left: 4px;\n                margin-right: 4px;\n                max-width: 160px;\n                text-overflow: ellipsis;\n                white-space: nowrap;\n                padding: 2px 0px;\n                overflow: hidden; }\n\n.nhs-container .nhs-search-wrapper .nhs-search-icon {\n      align-items: center;\n      display: flex;\n      justify-content: center;\n      flex: 0 0 24px;\n      margin: 0px 8px;\n      color: #222; }\n\n.nhs-container .nhs-search-result-wrapper {\n    position: absolute;\n    left: 0;\n    top: 100%;\n    max-height: 250px;\n    overflow-y: auto;\n    background: white;\n    width: 100%;\n    z-index: 999999;\n    border: 1px solid #ddd;\n    border-radius: 5px !important;\n    box-shadow: rgba(9, 30, 66, 0.25) 0px 4px 8px -2px, rgba(9, 30, 66, 0.31) 0px 0px 1px; }\n\n.nhs-container .nhs-search-result-wrapper.hidden {\n      display: none; }\n\n.nhs-container .nhs-search-result-wrapper ul {\n      padding: 5px 0; }\n\n.nhs-container .nhs-search-result-wrapper ul li {\n        -webkit-box-align: center;\n        box-sizing: border-box;\n        color: #172b4d;\n        cursor: pointer;\n        display: flex;\n        flex-wrap: nowrap;\n        font-size: 14px;\n        font-weight: normal;\n        padding: 4px 12px;\n        text-decoration: none;\n        align-items: flex-start;\n        width: 100%;\n        border-bottom: 1px dashed #ddd; }\n\n.nhs-container .nhs-search-result-wrapper ul li.active, .nhs-container .nhs-search-result-wrapper ul li:hover, .nhs-container .nhs-search-result-wrapper ul li.selected {\n          cursor: pointer;\n          background-color: #007bff;\n          color: #fff; }\n\n.nhs-container .nhs-search-result-wrapper ul li.active .nhs-item-info .nh-suggestion-name, .nhs-container .nhs-search-result-wrapper ul li.active .nhs-item-info .nh-suggestion-des, .nhs-container .nhs-search-result-wrapper ul li:hover .nhs-item-info .nh-suggestion-name, .nhs-container .nhs-search-result-wrapper ul li:hover .nhs-item-info .nh-suggestion-des, .nhs-container .nhs-search-result-wrapper ul li.selected .nhs-item-info .nh-suggestion-name, .nhs-container .nhs-search-result-wrapper ul li.selected .nhs-item-info .nh-suggestion-des {\n            color: white; }\n\n.nhs-container .nhs-search-result-wrapper ul li.searching {\n          min-height: 34px; }\n\n.nhs-container .nhs-search-result-wrapper ul li.searching:hover {\n            background-color: white; }\n\n.nhs-container .nhs-search-result-wrapper ul li.searching div {\n            margin-left: 10px;\n            margin-top: 10px; }\n\n.nhs-container .nhs-search-result-wrapper ul li:last-child {\n          border-bottom: none !important; }\n\n.nhs-container .nhs-search-result-wrapper ul li:last-child.load-more {\n            text-align: center; }\n\n.nhs-container .nhs-search-result-wrapper ul li:last-child.load-more a {\n              display: block;\n              width: 100%;\n              text-decoration: none; }\n\n.nhs-container .nhs-search-result-wrapper ul li:last-child.load-more a:hover {\n                color: white;\n                text-decoration: none; }\n\n.nhs-container .nhs-search-result-wrapper ul li:last-child.load-more a:visited, .nhs-container .nhs-search-result-wrapper ul li:last-child.load-more a:active, .nhs-container .nhs-search-result-wrapper ul li:last-child.load-more a:focus {\n                text-decoration: none; }\n\n.nhs-container .nhs-search-result-wrapper ul li .nhs-item-image {\n          margin-right: 5px;\n          width: 30px;\n          height: 30px;\n          overflow: hidden; }\n\n.nhs-container .nhs-search-result-wrapper ul li .nhs-item-image img {\n            width: 100%; }\n\n.nhs-container .nhs-search-result-wrapper ul li .nhs-item-info {\n          flex: 1;\n          overflow: hidden; }\n\n.nhs-container .nhs-search-result-wrapper ul li .nhs-item-info .nh-suggestion-name {\n            color: #333;\n            width: 100%;\n            font-size: 1rem;\n            margin-top: 0;\n            margin-bottom: 3px;\n            white-space: nowrap;\n            overflow: hidden;\n            text-overflow: ellipsis; }\n\n.nhs-container .nhs-search-result-wrapper ul li .nhs-item-info .nh-suggestion-des {\n            color: #999; }\n\n.nhs-container .nhs-item {\n    align-items: center;\n    box-sizing: border-box;\n    color: #172b4d;\n    cursor: pointer;\n    display: flex;\n    flex-wrap: nowrap;\n    font-size: 14px;\n    font-weight: normal;\n    padding: 0px 12px;\n    text-decoration: none;\n    -webkit-user-select: none;\n       -moz-user-select: none;\n        -ms-user-select: none;\n            user-select: none; }\n\n.nhs-container .nhs-item div.avatar-wrapper {\n      background-color: white;\n      color: #091e42;\n      display: flex;\n      flex-direction: column;\n      height: auto;\n      max-height: calc(100% - 1px);\n      outline: 0px;\n      align-self: flex-start;\n      border-radius: 50% !important; }\n\n.nhs-container .nhs-item div.user-info {\n      display: flex;\n      flex-direction: column;\n      margin: 0px 8px;\n      overflow: hidden; }\n\n.nhs-container .nhs-item div.user-info .full-name {\n        font-weight: bold; }\n\n.nhs-container .nhs-item div.user-info .description {\n        font-size: 12px;\n        color: #999; }\n\n.nhs-container .nhs-item .remove {\n      height: 16px;\n      width: 16px;\n      color: currentcolor;\n      display: inline-block;\n      fill: white;\n      line-height: 1; }\n\n.nhs-container .nhs-item .remove:hover {\n        cursor: pointer;\n        color: #bf2600; }\n\n.nhs-container .nhs-item .remove svg {\n        height: 16px;\n        width: 16px;\n        max-height: 100%;\n        max-width: 100%;\n        vertical-align: bottom;\n        overflow: hidden; }\n\nnh-suggestion.receipt .nhs-container {\n  border: none !important;\n  background: transparent !important;\n  border-bottom: 1px dotted #333 !important;\n  border-radius: 0px !important; }\n\nnh-suggestion.receipt .nhs-container .nhs-search-input {\n    margin: 0 !important; }\n\nnh-suggestion.receipt .nhs-container .nhs-search-input input {\n      background: transparent !important; }\n\nnh-suggestion.receipt .nhs-container .nhs-search-input input:focus, nh-suggestion.receipt .nhs-container .nhs-search-input input:active, nh-suggestion.receipt .nhs-container .nhs-search-input input:visited, nh-suggestion.receipt .nhs-container .nhs-search-input input:hover {\n        border: none !important;\n        box-shadow: none !important; }\n\nnh-suggestion.receipt .nhs-search-icon {\n  display: none !important; }\n\nnh-suggestion.no-border .nhs-container {\n  border-radius: 0; }\n\nnh-suggestion.no-border .nhs-container .active {\n    border: none !important; }\n\nnh-suggestion.has-error .nhs-container {\n  border: 1px solid #dc3545; }\n\n.disabled {\n  cursor: no-drop; }\n\n.disabled input {\n    background-color: #eef1f5 !important; }\n\n.disabled .icon-prepend {\n    color: #686868; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL25oLXN1Z2dlc3Rpb24vRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFwcFxcc2hhcmVkc1xcY29tcG9uZW50c1xcbmgtc3VnZ2VzdGlvblxcbmgtc3VnZ2VzdGlvbi5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL25oLXN1Z2dlc3Rpb24vRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFzc2V0c1xcc3R5bGVzXFxfY29uZmlnLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFFUSxvQ0FBb0MsRUFBQTs7QUFJNUM7RUFHSSxrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSSxXQUFXO0VBQ1gsV0FBVyxFQUFBOztBQUdmO0VBQ0ksV0FBVztFQUNYLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxzQkN6QmM7RUQwQmQsZ0JDWFE7RURZUiw2QkFBNkI7RUFDN0Isa0JBQWtCO0VBQ2xCLGFBQWEsRUFBQTs7QUFMakI7SUFRUSxlQUFlLEVBQUE7O0FBUnZCO0lBWVEseUJDUm1CO0lEU25CLGlCQUFpQjtJQUNqQixnREFBK0MsRUFBQTs7QUFkdkQ7TUFrQmdCLGlCQUFpQjtNQUNqQixZQUFZO01BQ1osYUFBYSxFQUFBOztBQXBCN0I7TUF3QmdCLDJCQUEyQixFQUFBOztBQXhCM0M7SUE4QlEsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixnQkFBZ0IsRUFBQTs7QUFoQ3hCO0lBcUNRLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsV0FBVztJQUNYLGdCQUFnQixFQUFBOztBQXhDeEI7TUEyQ1ksbUJBQW1CO01BQ25CLFdBQVc7TUFDWCxjQUFjO01BQ2QsZUFBZSxFQUFBOztBQTlDM0I7UUFpRGdCLG1CQUFtQjtRQUNuQixXQUFXO1FBQ1gsY0FBYztRQUNkLGVBQWUsRUFBQTs7QUFwRC9CO1VBdURvQixZQUFZO1VBQ1osY0FBYztVQUNkLFdBQVc7VUFDWCxnQkNuRVIsRUFBQTs7QURTWjtZQTZEd0Isd0JBQXdCO1lBQ3hCLHVCQUF1QjtZQUN2QiwyQkFBMkIsRUFBQTs7QUEvRG5EO1FBcUVnQixhQUFhO1FBQ2IsZUFBZTtRQUVmLDJCQUEyQjtRQUMzQixXQUFXLEVBQUE7O0FBekUzQjtVQTRFb0Isc0JBQXNCO1VBQ3RCLHFCQUFxQixFQUFBOztBQTdFekM7WUFnRndCLHlCQUFvQztZQUNwQyxjQUFzQjtZQUN0QixlQUFlO1lBQ2YsYUFBYTtZQUNiLFlBQVk7WUFDWixjQUFjO1lBQ2Qsa0JBQWtCO1lBQ2xCLHNCQUFzQjtZQUN0QixZQUFZO1lBQ1osaUJBQWlCO1lBQ2pCLHlCQUFpQjtlQUFqQixzQkFBaUI7Z0JBQWpCLHFCQUFpQjtvQkFBakIsaUJBQWlCLEVBQUE7O0FBMUZ6QztjQThGNEIsbUJBQW1CO2NBQ25CLGFBQWE7Y0FFYix1QkFBdUI7Y0FDdkIsaUJBQWlCLEVBQUE7O0FBbEc3QztjQXNHNEIsYUFBYTtjQUNiLDJCQUEyQixFQUFBOztBQXZHdkQ7Z0JBMEdnQyxlQUFlO2dCQUNmLG1CQUFtQjtnQkFDbkIsY0FBYztnQkFDZCxnQkFBZ0I7Z0JBQ2hCLGlCQUFpQjtnQkFDakIsZ0JBQWdCO2dCQUNoQix1QkFBdUI7Z0JBQ3ZCLG1CQUFtQjtnQkFDbkIsZ0JBQWdCO2dCQUNoQixnQkFBZ0IsRUFBQTs7QUFuSGhEO01BNkhZLG1CQUFtQjtNQUNuQixhQUFhO01BRWIsdUJBQXVCO01BQ3ZCLGNBQWM7TUFDZCxlQUFlO01BQ2YsV0FBVyxFQUFBOztBQW5JdkI7SUF3SVEsa0JBQWtCO0lBQ2xCLE9BQU87SUFDUCxTQUFTO0lBQ1QsaUJBQWlCO0lBQ2pCLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsV0FBVztJQUNYLGVBQWU7SUFDZixzQkN4S1U7SUQyS1YsNkJBQTZCO0lBQzdCLHFGQUFxRixFQUFBOztBQXBKN0Y7TUF1SlksYUFBYSxFQUFBOztBQXZKekI7TUEySlksY0FBYyxFQUFBOztBQTNKMUI7UUE4SmdCLHlCQUF5QjtRQUN6QixzQkFBc0I7UUFDdEIsY0FBc0I7UUFDdEIsZUFBZTtRQUNmLGFBQWE7UUFDYixpQkFBaUI7UUFDakIsZUFBZTtRQUNmLG1CQUFtQjtRQUNuQixpQkFBaUI7UUFDakIscUJBQXFCO1FBQ3JCLHVCQUF1QjtRQUN2QixXQUFXO1FBQ1gsOEJDbE1FLEVBQUE7O0FEd0JsQjtVQTZLb0IsZUFBZTtVQUNmLHlCQ3BMSDtVRHFMRyxXQ3hMUixFQUFBOztBRFNaO1lBbUw0QixZQUFZLEVBQUE7O0FBbkx4QztVQXlMb0IsZ0JBQWdCLEVBQUE7O0FBekxwQztZQTRMd0IsdUJBQXVCLEVBQUE7O0FBNUwvQztZQWdNd0IsaUJBQWlCO1lBQ2pCLGdCQUFnQixFQUFBOztBQWpNeEM7VUFzTW9CLDhCQUE4QixFQUFBOztBQXRNbEQ7WUF5TXdCLGtCQUFrQixFQUFBOztBQXpNMUM7Y0E0TTRCLGNBQWM7Y0FDZCxXQUFXO2NBQ1gscUJBQXFCLEVBQUE7O0FBOU1qRDtnQkFpTmdDLFlBQVk7Z0JBQ1oscUJBQXFCLEVBQUE7O0FBbE5yRDtnQkFzTmdDLHFCQUFxQixFQUFBOztBQXROckQ7VUE2Tm9CLGlCQUFpQjtVQUNqQixXQUFXO1VBQ1gsWUFBWTtVQUNaLGdCQUFnQixFQUFBOztBQWhPcEM7WUFtT3dCLFdBQVcsRUFBQTs7QUFuT25DO1VBd09vQixPQUFPO1VBQ1AsZ0JBQWdCLEVBQUE7O0FBek9wQztZQTRPd0IsV0FBVztZQUNYLFdBQVc7WUFDWCxlQUFlO1lBQ2YsYUFBYTtZQUNiLGtCQUFrQjtZQUNsQixtQkFBbUI7WUFDbkIsZ0JBQWdCO1lBQ2hCLHVCQUF1QixFQUFBOztBQW5QL0M7WUF1UHdCLFdBQVcsRUFBQTs7QUF2UG5DO0lBZ1FRLG1CQUFtQjtJQUNuQixzQkFBc0I7SUFDdEIsY0FBc0I7SUFDdEIsZUFBZTtJQUNmLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIscUJBQXFCO0lBQ3JCLHlCQUFpQjtPQUFqQixzQkFBaUI7UUFBakIscUJBQWlCO1lBQWpCLGlCQUFpQixFQUFBOztBQTFRekI7TUE2UVksdUJBQW9DO01BQ3BDLGNBQXFCO01BQ3JCLGFBQWE7TUFDYixzQkFBc0I7TUFDdEIsWUFBWTtNQUNaLDRCQUE0QjtNQUM1QixZQUFZO01BQ1osc0JBQXNCO01BR3RCLDZCQUE2QixFQUFBOztBQXZSekM7TUEyUlksYUFBYTtNQUNiLHNCQUFzQjtNQUN0QixlQUFlO01BQ2YsZ0JBQWdCLEVBQUE7O0FBOVI1QjtRQWlTZ0IsaUJBQWlCLEVBQUE7O0FBalNqQztRQW9TZ0IsZUFBZTtRQUNmLFdBQVcsRUFBQTs7QUFyUzNCO01BeVNZLFlBQVk7TUFDWixXQUFXO01BQ1gsbUJBQW1CO01BQ25CLHFCQUFxQjtNQUNyQixXQUF3QjtNQUN4QixjQUFjLEVBQUE7O0FBOVMxQjtRQWlUZ0IsZUFBZTtRQUNmLGNBQXNCLEVBQUE7O0FBbFR0QztRQXNUZ0IsWUFBWTtRQUNaLFdBQVc7UUFDWCxnQkFBZ0I7UUFDaEIsZUFBZTtRQUNmLHNCQUFzQjtRQUN0QixnQkFBZ0IsRUFBQTs7QUFNaEM7RUFJWSx1QkFBdUI7RUFDdkIsa0NBQWtDO0VBQ2xDLHlDQUF5QztFQUN6Qyw2QkFBNkIsRUFBQTs7QUFQekM7SUFVZ0Isb0JBQW9CLEVBQUE7O0FBVnBDO01BYW9CLGtDQUFrQyxFQUFBOztBQWJ0RDtRQWdCd0IsdUJBQXVCO1FBQ3ZCLDJCQUEyQixFQUFBOztBQWpCbkQ7RUF3Qlksd0JBQXdCLEVBQUE7O0FBeEJwQztFQWdDWSxnQkFBZ0IsRUFBQTs7QUFoQzVCO0lBbUNnQix1QkFBdUIsRUFBQTs7QUFuQ3ZDO0VBMkNZLHlCQzdXSSxFQUFBOztBRGtYaEI7RUFDSSxlQUFlLEVBQUE7O0FBRG5CO0lBR1Esb0NBQW9DLEVBQUE7O0FBSDVDO0lBT1EsY0FBYyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL25oLXN1Z2dlc3Rpb24vbmgtc3VnZ2VzdGlvbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi8uLi8uLi9hc3NldHMvc3R5bGVzL2NvbmZpZ1wiO1xyXG5cclxubmgtc3VnZ2VzdGlvbi5oYXMtZXJyb3Ige1xyXG4gICAgLm5ocy1jb250YWluZXIge1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRkYW5nZXIgIWltcG9ydGFudDtcclxuICAgIH1cclxufVxyXG5cclxuLnJvdW5kZWQtYXZhdGFyIHtcclxuICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgLW1vei1ib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbn1cclxuXHJcbi5hdmF0YXItd3JhcHBlciB7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcblxyXG4uYXZhdGFyLXhzIHtcclxuICAgIHdpZHRoOiAxNnB4O1xyXG4gICAgaGlnaHQ6IDE2cHg7XHJcbn1cclxuXHJcbi5hdmF0YXItc20ge1xyXG4gICAgd2lkdGg6IDMycHg7XHJcbiAgICBoZWlnaHQ6IDMycHg7XHJcbn1cclxuXHJcbi5uaHMtY29udGFpbmVyIHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICRib3JkZXJDb2xvcjtcclxuICAgIGJhY2tncm91bmQ6ICR3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweCAhaW1wb3J0YW50O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgei1pbmRleDogMTAwMDtcclxuXHJcbiAgICAmOmhvdmVyIHtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgJi5hY3RpdmUge1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRib3JkZXJBY3RpdmVDb2xvcjtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICBib3gtc2hhZG93OiAwIDAgMCAwLjJyZW0gcmdiYSgwLCAxMjMsIDI1NSwgLjI1KTtcclxuXHJcbiAgICAgICAgLm5ocy1zZWFyY2gtd3JhcHBlciAubmhzLXNlYXJjaC1jb250ZW50IHtcclxuICAgICAgICAgICAgLm5ocy1zZWFyY2gtaW5wdXQgaW5wdXQge1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAubmhzLWl0ZW0ge1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMCAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHVsIHtcclxuICAgICAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogMDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgfVxyXG5cclxuICAgIC5uaHMtc2VhcmNoLXdyYXBwZXIge1xyXG4gICAgICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIG1pbi1oZWlnaHQ6IDMzcHg7XHJcblxyXG4gICAgICAgIC5uaHMtc2VhcmNoLWNvbnRlbnQge1xyXG4gICAgICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgZmxleDogMSAxIGF1dG87XHJcbiAgICAgICAgICAgIG1hcmdpbjogM3B4IDhweDtcclxuXHJcbiAgICAgICAgICAgIC5uaHMtc2VhcmNoLWlucHV0IHtcclxuICAgICAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIGZsZXg6IDEgMSBhdXRvO1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAzcHggOHB4O1xyXG5cclxuICAgICAgICAgICAgICAgIGlucHV0IHtcclxuICAgICAgICAgICAgICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogJHdoaXRlO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAmOmZvY3VzLCAmOnZpc2l0ZWQsICY6YWN0aXZlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3V0bGluZTogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdWwge1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgICAgICAgICAgICAgIC13ZWJraXQtYm94LXBhY2s6IHN0YXJ0O1xyXG4gICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgICAgICAgICAgICAgbGkubmhzLWl0ZW0tc2VsZWN0ZWQge1xyXG4gICAgICAgICAgICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBkaXYubmhzLWl0ZW0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjQ0LCAyNDUsIDI0Nyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiByZ2IoMzcsIDU2LCA4OCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGN1cnNvcjogZGVmYXVsdDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDRweCAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG92ZXJmbG93OiBpbml0aWFsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1c2VyLXNlbGVjdDogbm9uZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hdmF0YXItd3JhcHBlciB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmctbGVmdDogNHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAudXNlci1pbmZvIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbjogMCA1cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLmZ1bGwtbmFtZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDRweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDRweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDE2MHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMnB4IDBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAubmhzLXNlYXJjaC1pY29uIHtcclxuICAgICAgICAgICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgZmxleDogMCAwIDI0cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbjogMHB4IDhweDtcclxuICAgICAgICAgICAgY29sb3I6ICMyMjI7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5uaHMtc2VhcmNoLXJlc3VsdC13cmFwcGVyIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgbGVmdDogMDtcclxuICAgICAgICB0b3A6IDEwMCU7XHJcbiAgICAgICAgbWF4LWhlaWdodDogMjUwcHg7XHJcbiAgICAgICAgb3ZlcmZsb3cteTogYXV0bztcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICB6LWluZGV4OiA5OTk5OTk7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgJGJvcmRlckNvbG9yO1xyXG4gICAgICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNXB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgLW1vei1ib3JkZXItcmFkaXVzOiA1cHggIWltcG9ydGFudDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1cHggIWltcG9ydGFudDtcclxuICAgICAgICBib3gtc2hhZG93OiByZ2JhKDksIDMwLCA2NiwgMC4yNSkgMHB4IDRweCA4cHggLTJweCwgcmdiYSg5LCAzMCwgNjYsIDAuMzEpIDBweCAwcHggMXB4O1xyXG5cclxuICAgICAgICAmLmhpZGRlbiB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB1bCB7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDVweCAwO1xyXG5cclxuICAgICAgICAgICAgbGkge1xyXG4gICAgICAgICAgICAgICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogcmdiKDIzLCA0MywgNzcpO1xyXG4gICAgICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgIGZsZXgtd3JhcDogbm93cmFwO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDRweCAxMnB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBkYXNoZWQgJGJvcmRlckNvbG9yO1xyXG5cclxuICAgICAgICAgICAgICAgICYuYWN0aXZlLCAmOmhvdmVyLCAmLnNlbGVjdGVkIHtcclxuICAgICAgICAgICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICR3aGl0ZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLm5ocy1pdGVtLWluZm8ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAubmgtc3VnZ2VzdGlvbi1uYW1lLCAubmgtc3VnZ2VzdGlvbi1kZXMge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICYuc2VhcmNoaW5nIHtcclxuICAgICAgICAgICAgICAgICAgICBtaW4taGVpZ2h0OiAzNHB4O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBkaXYge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgJjpsYXN0LWNoaWxkIHtcclxuICAgICAgICAgICAgICAgICAgICBib3JkZXItYm90dG9tOiBub25lICFpbXBvcnRhbnQ7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICYubG9hZC1tb3JlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgYSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJjp2aXNpdGVkLCAmOmFjdGl2ZSwgJjpmb2N1c3tcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLm5ocy1pdGVtLWltYWdlIHtcclxuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMzBweDtcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaW1nIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC5uaHMtaXRlbS1pbmZvIHtcclxuICAgICAgICAgICAgICAgICAgICBmbGV4OiAxO1xyXG4gICAgICAgICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC5uaC1zdWdnZXN0aW9uLW5hbWUge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzMzMztcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogM3B4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC5uaC1zdWdnZXN0aW9uLWRlcyB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjOTk5O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAubmhzLWl0ZW0ge1xyXG4gICAgICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgICAgIGNvbG9yOiByZ2IoMjMsIDQzLCA3Nyk7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC13cmFwOiBub3dyYXA7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgcGFkZGluZzogMHB4IDEycHg7XHJcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgIHVzZXItc2VsZWN0OiBub25lO1xyXG5cclxuICAgICAgICBkaXYuYXZhdGFyLXdyYXBwZXIge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjU1LCAyNTUsIDI1NSk7XHJcbiAgICAgICAgICAgIGNvbG9yOiByZ2IoOSwgMzAsIDY2KTtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgICAgICBtYXgtaGVpZ2h0OiBjYWxjKDEwMCUgLSAxcHgpO1xyXG4gICAgICAgICAgICBvdXRsaW5lOiAwcHg7XHJcbiAgICAgICAgICAgIGFsaWduLXNlbGY6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNTAlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIC1tb3otYm9yZGVyLXJhZGl1czogNTAlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJSAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZGl2LnVzZXItaW5mbyB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgICAgIG1hcmdpbjogMHB4IDhweDtcclxuICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuXHJcbiAgICAgICAgICAgIC5mdWxsLW5hbWUge1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmRlc2NyaXB0aW9uIHtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAjOTk5O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5yZW1vdmUge1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDE2cHg7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxNnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogY3VycmVudGNvbG9yO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgIGZpbGw6IHJnYigyNTUsIDI1NSwgMjU1KTtcclxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDE7XHJcblxyXG4gICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiByZ2IoMTkxLCAzOCwgMCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHN2ZyB7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDE2cHg7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTZweDtcclxuICAgICAgICAgICAgICAgIG1heC1oZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogYm90dG9tO1xyXG4gICAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxubmgtc3VnZ2VzdGlvbiB7XHJcbiAgICAvLyBCRUdJTjogcmVjZWlwdCBzdHlsZVxyXG4gICAgJi5yZWNlaXB0IHtcclxuICAgICAgICAubmhzLWNvbnRhaW5lciB7XHJcbiAgICAgICAgICAgIGJvcmRlcjogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggZG90dGVkICMzMzMgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMHB4ICFpbXBvcnRhbnQ7XHJcblxyXG4gICAgICAgICAgICAubmhzLXNlYXJjaC1pbnB1dCB7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDAgIWltcG9ydGFudDtcclxuXHJcbiAgICAgICAgICAgICAgICBpbnB1dCB7XHJcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgJjpmb2N1cywgJjphY3RpdmUsICY6dmlzaXRlZCwgJjpob3ZlciB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlcjogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAubmhzLXNlYXJjaC1pY29uIHtcclxuICAgICAgICAgICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC8vIEVORDogcmVjZWlwdCBzdHlsZVxyXG5cclxuICAgIC8vIEJFR0lOOiBubyBib3JkZXJcclxuICAgICYubm8tYm9yZGVyIHtcclxuICAgICAgICAubmhzLWNvbnRhaW5lciB7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcblxyXG4gICAgICAgICAgICAuYWN0aXZlIHtcclxuICAgICAgICAgICAgICAgIGJvcmRlcjogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLy8gRU5EOiBubyBib3JkZXJcclxuXHJcbiAgICAmLmhhcy1lcnJvciB7XHJcbiAgICAgICAgLm5ocy1jb250YWluZXIge1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAkZGFuZ2VyO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLmRpc2FibGVkIHtcclxuICAgIGN1cnNvcjogbm8tZHJvcDtcclxuICAgIGlucHV0IHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVmMWY1ICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgLmljb24tcHJlcGVuZCB7XHJcbiAgICAgICAgY29sb3I6ICM2ODY4Njg7XHJcbiAgICB9XHJcbn1cclxuIiwiJGRlZmF1bHQtY29sb3I6ICMyMjI7XHJcbiRmb250LWZhbWlseTogXCJBcmlhbFwiLCB0YWhvbWEsIEhlbHZldGljYSBOZXVlO1xyXG4kY29sb3ItYmx1ZTogIzM1OThkYztcclxuJG1haW4tY29sb3I6ICMwMDc0NTU7XHJcbiRib3JkZXJDb2xvcjogI2RkZDtcclxuJHNlY29uZC1jb2xvcjogI2IwMWExZjtcclxuJHRhYmxlLWJhY2tncm91bmQtY29sb3I6ICMwMDk2ODg7XHJcbiRibHVlOiAjMDA3YmZmO1xyXG4kZGFyay1ibHVlOiAjMDA3MkJDO1xyXG4kYnJpZ2h0LWJsdWU6ICNkZmYwZmQ7XHJcbiRpbmRpZ286ICM2NjEwZjI7XHJcbiRwdXJwbGU6ICM2ZjQyYzE7XHJcbiRwaW5rOiAjZTgzZThjO1xyXG4kcmVkOiAjZGMzNTQ1O1xyXG4kb3JhbmdlOiAjZmQ3ZTE0O1xyXG4keWVsbG93OiAjZmZjMTA3O1xyXG4kZ3JlZW46ICMyOGE3NDU7XHJcbiR0ZWFsOiAjMjBjOTk3O1xyXG4kY3lhbjogIzE3YTJiODtcclxuJHdoaXRlOiAjZmZmO1xyXG4kZ3JheTogIzg2OGU5NjtcclxuJGdyYXktZGFyazogIzM0M2E0MDtcclxuJHByaW1hcnk6ICMwMDdiZmY7XHJcbiRzZWNvbmRhcnk6ICM2Yzc1N2Q7XHJcbiRzdWNjZXNzOiAjMjhhNzQ1O1xyXG4kaW5mbzogIzE3YTJiODtcclxuJHdhcm5pbmc6ICNmZmMxMDc7XHJcbiRkYW5nZXI6ICNkYzM1NDU7XHJcbiRsaWdodDogI2Y4ZjlmYTtcclxuJGRhcms6ICMzNDNhNDA7XHJcbiRsYWJlbC1jb2xvcjogIzY2NjtcclxuJGJhY2tncm91bmQtY29sb3I6ICNFQ0YwRjE7XHJcbiRib3JkZXJBY3RpdmVDb2xvcjogIzgwYmRmZjtcclxuJGJvcmRlclJhZGl1czogMDtcclxuJGRhcmtCbHVlOiAjNDVBMkQyO1xyXG4kbGlnaHRHcmVlbjogIzI3YWU2MDtcclxuJGxpZ2h0LWJsdWU6ICNmNWY3Zjc7XHJcbiRicmlnaHRHcmF5OiAjNzU3NTc1O1xyXG4kbWF4LXdpZHRoLW1vYmlsZTogNzY4cHg7XHJcbiRtYXgtd2lkdGgtdGFibGV0OiA5OTJweDtcclxuJG1heC13aWR0aC1kZXNrdG9wOiAxMjgwcHg7XHJcblxyXG4vLyBCRUdJTjogTWFyZ2luXHJcbkBtaXhpbiBuaC1tZygkcGl4ZWwpIHtcclxuICAgIG1hcmdpbjogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ3QoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tdG9wOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1nYigkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1ib3R0b206ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWdsKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLWxlZnQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWdyKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBNYXJnaW5cclxuXHJcbi8vIEJFR0lOOiBQYWRkaW5nXHJcbkBtaXhpbiBuaC1wZCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmc6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGR0KCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy10b3A6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGRiKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy1ib3R0b206ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGRsKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy1sZWZ0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkcigkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctcmlnaHQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vLyBFTkQ6IFBhZGRpbmdcclxuXHJcbi8vIEJFR0lOOiBXaWR0aFxyXG5AbWl4aW4gbmgtd2lkdGgoJHdpZHRoKSB7XHJcbiAgICB3aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbiAgICBtaW4td2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG4gICAgbWF4LXdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBXaWR0aFxyXG5cclxuLy8gQkVHSU46IEljb24gU2l6ZVxyXG5AbWl4aW4gbmgtc2l6ZS1pY29uKCRzaXplKSB7XHJcbiAgICB3aWR0aDogJHNpemU7XHJcbiAgICBoZWlnaHQ6ICRzaXplO1xyXG4gICAgZm9udC1zaXplOiAkc2l6ZTtcclxufVxyXG5cclxuLy8gRU5EOiBJY29uIFNpemVcclxuIl19 */"

/***/ }),

/***/ "./src/app/shareds/components/nh-suggestion/nh-suggestion.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/shareds/components/nh-suggestion/nh-suggestion.component.ts ***!
  \*****************************************************************************/
/*! exports provided: NhSuggestion, NhSuggestionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NhSuggestion", function() { return NhSuggestion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NhSuggestionComponent", function() { return NhSuggestionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _nh_suggestion_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./nh-suggestion.service */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");







var NhSuggestion = /** @class */ (function () {
    function NhSuggestion(id, name, icon, isSelected, isActive, data) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.isSelected = isSelected !== undefined && isSelected != null ? isSelected : false;
        this.isActive = isActive !== undefined && isActive != null ? isActive : false;
        this.data = data;
        this.description = '';
        this.image = null;
    }
    return NhSuggestion;
}());

var NhSuggestionComponent = /** @class */ (function () {
    function NhSuggestionComponent(el, ref, nhSuggestionService) {
        var _this = this;
        this.el = el;
        this.ref = ref;
        this.nhSuggestionService = nhSuggestionService;
        this.multiple = false;
        this.isShowSelected = true;
        this.placeholder = '';
        this.loading = false;
        this.allowAdd = false;
        this.readonly = false;
        this.itemSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.itemRemoved = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.keyUpPressed = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.searched = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.nextPage = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this._subscribers = {};
        this._selectedItems = [];
        this._isShowSearchBox = true;
        this._selectedItem = null;
        this.isLoading = false;
        this.isActive = false;
        this.showLoadMore = false;
        this.currentPage = 1;
        this.totalPages = 0;
        this.searchTerm$ = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
        this.propagateChange = function () {
        };
        this.id = Math.floor(Math.random() * 1000).toString();
        this.searchTerm$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["debounceTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["distinctUntilChanged"])()).subscribe(function (term) {
            _this.searched.emit(term);
        });
    }
    NhSuggestionComponent_1 = NhSuggestionComponent;
    Object.defineProperty(NhSuggestionComponent.prototype, "totalRows", {
        get: function () {
            return this._totalRows;
        },
        set: function (value) {
            this._totalRows = value;
            this.renderPaging();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NhSuggestionComponent.prototype, "sources", {
        get: function () {
            return this._sources ? this._sources : [];
        },
        set: function (values) {
            this._sources = values;
            this.updateSelectedStatus();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NhSuggestionComponent.prototype, "value", {
        get: function () {
            return this._value;
        },
        set: function (value) {
            this.value = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NhSuggestionComponent.prototype, "selectedItems", {
        get: function () {
            return this._selectedItems;
        },
        set: function (values) {
            this._selectedItems = values ? values : [];
            this.updateSelectedStatus();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NhSuggestionComponent.prototype, "selectedItem", {
        get: function () {
            return this._selectedItem;
        },
        set: function (value) {
            if (value instanceof Array) {
                this._selectedItems = value;
                this.isShowSearchBox = !value || value.length === 0;
            }
            else {
                this._selectedItem = value;
                this.isShowSearchBox = value ? false : true;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NhSuggestionComponent.prototype, "isShowSearchBox", {
        get: function () {
            return this._isShowSearchBox;
        },
        set: function (value) {
            this._isShowSearchBox = value;
            if (value) {
                this.focusSearchInputElement();
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NhSuggestionComponent.prototype, "isShowListSuggestion", {
        get: function () {
            return this._isShowSuggestionList;
        },
        enumerable: true,
        configurable: true
    });
    NhSuggestionComponent.prototype.ngOnInit = function () {
        this.nhSuggestionService.add(this);
    };
    NhSuggestionComponent.prototype.ngOnDestroy = function () {
        // this._subscribers.searchTermChange.unsubscribe();
        this.selectedItems = [];
        this.sources = [];
    };
    NhSuggestionComponent.prototype.registerOnChange = function (fn) {
        this.propagateChange = fn;
    };
    NhSuggestionComponent.prototype.registerOnTouched = function (fn) {
    };
    NhSuggestionComponent.prototype.onKeyUp = function (event) {
        if (event.code === 'Tab' || event.code === 'tab') {
            this.isActive = false;
        }
    };
    NhSuggestionComponent.prototype.onDocumentClick = function (targetElement) {
        if (this.el.nativeElement && !this.el.nativeElement.contains(targetElement.target)) {
            // this.nhSuggestionService.setActive(this, false);
            this.isActive = false;
            if (this.selectedItem || (this.selectedItems && this.selectedItems.length > 0)) {
                this.isShowSearchBox = false;
            }
        }
    };
    NhSuggestionComponent.prototype.documentRightClick = function (event) {
        this.isActive = false;
    };
    NhSuggestionComponent.prototype.showSearchBox = function (e) {
        e.preventDefault();
        e.stopPropagation();
        this.isShowSearchBox = true;
        this.nhSuggestionService.setActive(this, true);
    };
    NhSuggestionComponent.prototype.activeSuggestion = function (e) {
        e.stopPropagation();
        e.preventDefault();
        this.nhSuggestionService.setActive(this, true);
        this.isActive = true;
        this.searched.emit(this.keyword);
        this.isShowSearchBox = true;
    };
    NhSuggestionComponent.prototype.inputKeyUp = function (event) {
        var isNavigation = this.navigate(event);
        if (!isNavigation) {
            // this.searchTerms$.next(this.keyword);
            this.searchTerm$.next(this.keyword);
            this.keyUpPressed.emit({
                keyword: this.keyword,
                events: event
            });
        }
    };
    NhSuggestionComponent.prototype.add = function () {
        this.selectedItem = new NhSuggestion(null, lodash__WEBPACK_IMPORTED_MODULE_2__["cloneDeep"](this.keyword));
        this.itemSelected.emit(this.selectedItem);
        this.isActive = false;
        this.isShowSearchBox = false;
    };
    NhSuggestionComponent.prototype.selectItem = function (item) {
        if (!this.multiple) {
            this.isShowSearchBox = false;
            this.isActive = false;
            this.keyword = '';
            this.selectedItem = item;
            this.propagateChange(item.id);
            this.itemSelected.emit(item);
        }
        else {
            item.isSelected = !item.isSelected;
            // const listSelectedItems = _.filter(this.sources, (sourceItem: NhSuggestion) => sourceItem.isSelected);
            // this.selectedItems = listSelectedItems;
            if (item.isSelected) {
                var existingItem = lodash__WEBPACK_IMPORTED_MODULE_2__["find"](this.selectedItems, function (selectedItem) {
                    return selectedItem.id === item.id;
                });
                if (!existingItem) {
                    this.selectedItems.push(item);
                    this.itemSelected.emit(this.selectedItems);
                    this.keyword = '';
                    // this.itemSelected.emit(this.selectedItems);
                }
                else if (existingItem && !item.isSelected) {
                    this.removeSelectedItem(item);
                    this.itemSelected.emit(this.selectedItems);
                    // this.removeSelectedItem(item);
                }
            }
            else {
                this.removeSelectedItem(item);
            }
        }
    };
    NhSuggestionComponent.prototype.removeSelectedItem = function (item) {
        this.isShowSearchBox = true;
        if (item) {
            if (this.multiple && this.selectedItems instanceof Array) {
                lodash__WEBPACK_IMPORTED_MODULE_2__["remove"](this.selectedItems, function (selectedItem) { return selectedItem.id === item.id; });
            }
            else {
                this.selectedItems = null;
                this.selectedItem = null;
            }
            this.resetActiveStatus();
            this.itemRemoved.emit(item);
        }
        else {
            this.selectedItem = new NhSuggestion();
            this.propagateChange(null);
            this.itemSelected.emit(null);
        }
    };
    NhSuggestionComponent.prototype.writeValue = function (value) {
        this.value = value;
    };
    NhSuggestionComponent.prototype.clear = function () {
        this.keyword = '';
        this.selectedItem = null;
        this.selectedItems = [];
    };
    NhSuggestionComponent.prototype.loadMore = function (event) {
        event.preventDefault();
        event.stopPropagation();
        if (this.searchResultContainer) {
            this.lastScrollHeight = this.searchResultContainer.nativeElement.scrollTop;
        }
        this.currentPage += 1;
        this.nextPage.emit({
            keyword: this.keyword,
            page: this.currentPage,
            pageSize: this.pageSize
        });
    };
    NhSuggestionComponent.prototype.updateScrollPosition = function () {
        var _this = this;
        if (this.searchResultContainer) {
            setTimeout(function () {
                _this.searchResultContainer.nativeElement.scrollTop = _this.lastScrollHeight;
            });
        }
    };
    NhSuggestionComponent.prototype.navigate = function (key) {
        var keyCode = key.keyCode;
        // Escape
        if (keyCode === 27) {
            this.isActive = false;
            return true;
        }
        if (keyCode === 27 || keyCode === 17 || key.ctrlKey) {
            return true;
        }
        // 37: Left arrow
        // 38: Up arrow
        // 39: Right arrow
        // 40: Down arrow
        // 13: Enter
        if (keyCode === 37 || keyCode === 38 || keyCode === 39 || keyCode === 40 || keyCode === 13) {
            switch (keyCode) {
                case 37:
                case 38:
                    this.back();
                    break;
                case 39:
                case 40:
                    this.next();
                    break;
                case 13:
                    var selectedItems = this.sources.find(function (item) {
                        return item.isActive;
                    });
                    this.selectItem(selectedItems);
                    break;
            }
            key.preventDefault();
            return true;
        }
        return false;
    };
    NhSuggestionComponent.prototype.next = function () {
        var index = this.getActiveItemIndex();
        if (index === -1) {
            var firstItem = this.sources[0];
            if (firstItem) {
                firstItem.isActive = true;
            }
        }
        else {
            index = index < this.sources.length - 1 ? index + 1 : 0;
            this.setItemActiveStatus(index);
        }
    };
    NhSuggestionComponent.prototype.back = function () {
        var index = this.getActiveItemIndex();
        if (index === -1) {
            var lastItem = this.sources[this.sources.length - 1];
            if (lastItem) {
                lastItem.isActive = true;
            }
        }
        else {
            index = index > 0 ? index - 1 : this.sources.length - 1;
            this.setItemActiveStatus(index);
        }
    };
    NhSuggestionComponent.prototype.resetActiveStatus = function () {
        this.sources.forEach(function (item) { return item.isActive = false; });
    };
    NhSuggestionComponent.prototype.getActiveItemIndex = function () {
        return lodash__WEBPACK_IMPORTED_MODULE_2__["findIndex"](this.sources, function (item) {
            return item.isActive;
        });
    };
    NhSuggestionComponent.prototype.setItemActiveStatus = function (index) {
        this.sources.forEach(function (item) { return item.isActive = false; });
        var sourceItem = this.sources[index];
        if (sourceItem) {
            sourceItem.isActive = true;
        }
    };
    NhSuggestionComponent.prototype.updateSelectedStatus = function () {
        if (this.sources && this.selectedItems) {
            var intersections = lodash__WEBPACK_IMPORTED_MODULE_2__["intersectionBy"](this.sources, this.selectedItems, 'id');
            var differences = lodash__WEBPACK_IMPORTED_MODULE_2__["differenceBy"](this.sources, this.selectedItems, 'id');
            if (intersections && intersections.length > 0) {
                lodash__WEBPACK_IMPORTED_MODULE_2__["each"](intersections, function (item) {
                    item.isSelected = true;
                });
            }
            if (differences && differences.length > 0) {
                lodash__WEBPACK_IMPORTED_MODULE_2__["each"](differences, function (item) {
                    item.isSelected = false;
                });
            }
        }
    };
    NhSuggestionComponent.prototype.focusSearchInputElement = function () {
        var _this = this;
        setTimeout(function () {
            var element = document.getElementById(_this.id);
            if (element) {
                element.focus();
            }
        }, 100);
    };
    NhSuggestionComponent.prototype.renderPaging = function () {
        this.totalPages = Math.ceil(this.totalRows / (this.pageSize ? this.pageSize : 10));
        this.showLoadMore = this.totalPages > 0 && this.currentPage < this.totalPages;
    };
    var NhSuggestionComponent_1;
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('searchResultContainer'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NhSuggestionComponent.prototype, "searchResultContainer", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSuggestionComponent.prototype, "multiple", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSuggestionComponent.prototype, "isShowSelected", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSuggestionComponent.prototype, "placeholder", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSuggestionComponent.prototype, "loading", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], NhSuggestionComponent.prototype, "pageSize", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSuggestionComponent.prototype, "allowAdd", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSuggestionComponent.prototype, "readonly", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Number])
    ], NhSuggestionComponent.prototype, "totalRows", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Array])
    ], NhSuggestionComponent.prototype, "sources", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
    ], NhSuggestionComponent.prototype, "value", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Array])
    ], NhSuggestionComponent.prototype, "selectedItems", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSuggestionComponent.prototype, "itemSelected", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSuggestionComponent.prototype, "itemRemoved", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSuggestionComponent.prototype, "keyUpPressed", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSuggestionComponent.prototype, "searched", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSuggestionComponent.prototype, "nextPage", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
    ], NhSuggestionComponent.prototype, "selectedItem", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:keyup', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [KeyboardEvent]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], NhSuggestionComponent.prototype, "onKeyUp", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('document:click', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], NhSuggestionComponent.prototype, "onDocumentClick", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('document:contextmenu', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], NhSuggestionComponent.prototype, "documentRightClick", null);
    NhSuggestionComponent = NhSuggestionComponent_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'nh-suggestion',
            template: __webpack_require__(/*! ./nh-suggestion.component.html */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            providers: [
                {
                    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NG_VALUE_ACCESSOR"],
                    useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () { return NhSuggestionComponent_1; }),
                    multi: true
                }
            ],
            styles: [__webpack_require__(/*! ./nh-suggestion.component.scss */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _nh_suggestion_service__WEBPACK_IMPORTED_MODULE_5__["NhSuggestionService"]])
    ], NhSuggestionComponent);
    return NhSuggestionComponent;
}());



/***/ }),

/***/ "./src/app/shareds/components/nh-suggestion/nh-suggestion.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/shareds/components/nh-suggestion/nh-suggestion.module.ts ***!
  \**************************************************************************/
/*! exports provided: NhSuggestionModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NhSuggestionModule", function() { return NhSuggestionModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _nh_suggestion_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./nh-suggestion.service */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.service.ts");
/* harmony import */ var _nh_suggestion_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./nh-suggestion.component */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.component.ts");






var NhSuggestionModule = /** @class */ (function () {
    function NhSuggestionModule() {
    }
    NhSuggestionModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]],
            declarations: [_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_5__["NhSuggestionComponent"]],
            exports: [_nh_suggestion_component__WEBPACK_IMPORTED_MODULE_5__["NhSuggestionComponent"]],
            providers: [_nh_suggestion_service__WEBPACK_IMPORTED_MODULE_4__["NhSuggestionService"]],
        })
    ], NhSuggestionModule);
    return NhSuggestionModule;
}());



/***/ }),

/***/ "./src/app/shareds/components/nh-suggestion/nh-suggestion.service.ts":
/*!***************************************************************************!*\
  !*** ./src/app/shareds/components/nh-suggestion/nh-suggestion.service.ts ***!
  \***************************************************************************/
/*! exports provided: NhSuggestionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NhSuggestionService", function() { return NhSuggestionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_3__);




var NhSuggestionService = /** @class */ (function () {
    function NhSuggestionService(http) {
        this.http = http;
        this.suggestions = [];
    }
    NhSuggestionService.prototype.search = function (url, keyword) {
        return this.http.get(url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]().set('keyword', keyword)
        });
    };
    NhSuggestionService.prototype.add = function (suggestion) {
        var count = lodash__WEBPACK_IMPORTED_MODULE_3__["countBy"](this.suggestions, function (suggestionItem) {
            return suggestionItem.id === suggestion.id;
        }).true;
        if (!count || count === 0) {
            this.suggestions = this.suggestions.concat([suggestion]);
        }
    };
    NhSuggestionService.prototype.setActive = function (suggestion, isActive) {
        this.suggestions.forEach(function (suggestionItem) {
            if (suggestion.id === suggestionItem.id) {
                suggestionItem.isActive = isActive;
            }
        });
    };
    NhSuggestionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], NhSuggestionService);
    return NhSuggestionService;
}());



/***/ })

}]);
//# sourceMappingURL=default~modules-configs-config-module~modules-hr-organization-organization-module~modules-hr-user-us~2ca180b7.js.map