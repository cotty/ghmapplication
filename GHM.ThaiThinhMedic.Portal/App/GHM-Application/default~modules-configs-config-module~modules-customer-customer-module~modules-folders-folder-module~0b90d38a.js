(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~modules-configs-config-module~modules-customer-customer-module~modules-folders-folder-module~0b90d38a"],{

/***/ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"hus-container\"\r\n     [class.active]=\"isActive\">\r\n    <div class=\"hus-search-wrapper\"\r\n         (click)=\"activeSuggestion()\">\r\n        <div class=\"hus-search-content\">\r\n            <ng-container *ngIf=\"multiple; else singleTemplate\">\r\n                <ul class=\"hus-selected-wrapper\" *ngIf=\"selectedUsers.length > 0\">\r\n                    <li class=\"hus-item-selected\" *ngFor=\"let user of selectedUsers\">\r\n                        <div class=\"hus-item\">\r\n                            <div class=\"avatar-wrapper\">\r\n                                <img class=\"rounded-avatar\" ghmImage\r\n                                     [src]=\" user.avatar\"\r\n                                     [errorImageUrl]=\"'/assets/images/no-image.png'\"\r\n                                     alt=\"{{ user.fullName }}\">\r\n                            </div><!-- END: .avatar-wrapper -->\r\n                            <div class=\"user-info\">\r\n                                <span class=\"full-name\">{{ user.fullName }}</span>\r\n                            </div><!-- END: .info -->\r\n                            <span class=\"remove\" (click)=\"removeSelectedUser(user)\">\r\n                                <svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" focusable=\"false\"\r\n                                     role=\"presentation\">\r\n                                    <path\r\n                                        d=\"M12 10.586L6.707 5.293a1 1 0 0 0-1.414 1.414L10.586 12l-5.293 5.293a1 1 0 0 0 1.414 1.414L12 13.414l5.293 5.293a1 1 0 0 0 1.414-1.414L13.414 12l5.293-5.293a1 1 0 1 0-1.414-1.414L12 10.586z\"\r\n                                        fill=\"currentColor\">\r\n                                    </path>\r\n                                </svg>\r\n                            </span><!-- END: .remove -->\r\n                        </div><!-- END: .hus-item -->\r\n                    </li>\r\n                </ul><!-- END: .hus-selected-wrapper -->\r\n            </ng-container><!-- END: display selected users -->\r\n            <div class=\"hus-search-input\">\r\n                <input type=\"text\"\r\n                       [(ngModel)]=\"keyword\"\r\n                       name=\"searchUserSuggestion\"\r\n                       autocomplete=\"off\"\r\n                       placeholder=\"{{placeholder}}\"\r\n                       (keydown.enter)=\"$event.preventDefault()\"\r\n                       (keyup)=\"inputKeyUp($event)\">\r\n            </div><!-- END: .hus-search-input -->\r\n        </div><!-- END: .hus-search-content -->\r\n        <div class=\"hus-search-icon\">\r\n            <svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" focusable=\"false\" role=\"presentation\">\r\n                <path\r\n                    d=\"M16.436 15.085l3.94 4.01a1 1 0 0 1-1.425 1.402l-3.938-4.006a7.5 7.5 0 1 1 1.423-1.406zM10.5 16a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11z\"\r\n                    fill=\"currentColor\" fill-rule=\"evenodd\"></path>\r\n            </svg>\r\n        </div><!-- END: .hus-search-icon -->\r\n    </div><!-- END: .hus-search-wrapper -->\r\n    <div class=\"hus-search-result-wrapper\" *ngIf=\"isActive\">\r\n        <ul>\r\n            <li class=\"searching\" *ngIf=\"isLoading; else loadDoneTemplate\">\r\n                <div class=\"m-loader m-loader--brand\"></div>\r\n            </li>\r\n            <li i18n=\"@@cantFindPerson\" *ngIf=\"keyword && listUsers.length === 0 && !isLoading\">\r\n                We can't find that person. Enter their email to find them.\r\n            </li>\r\n        </ul>\r\n    </div><!-- END: .hus-search-result-wrapper -->\r\n</div><!-- END: .hus-container -->\r\n\r\n<ng-template #loadDoneTemplate>\r\n    <li class=\"hus-item\" *ngFor=\"let user of listUsers\"\r\n        [class.active]=\"user.isActive\"\r\n        (click)=\"selectUser(user)\">\r\n        <div class=\"avatar-wrapper\">\r\n            <img class=\"rounded-avatar\"\r\n                 src=\"{{ user.avatar }}\"\r\n                 alt=\"{{ user.fullName }}\"\r\n                 (error)=\"onImageError(user)\">\r\n        </div><!-- END: .avatar-wrapper -->\r\n        <div class=\"user-info\">\r\n            <div class=\"full-name\">{{ user.fullName }}</div>\r\n            <div class=\"description\" *ngIf=\"user.officeName && user.positionName\">\r\n                {{ user.officeName }} - {{ user.positionName }}\r\n            </div>\r\n        </div><!-- END: .info -->\r\n    </li><!-- END: .hus-item -->\r\n</ng-template><!-- END: search result template -->\r\n\r\n<ng-template #singleTemplate>\r\n    <div class=\"hus-item\" *ngIf=\"selectedUser\">\r\n        <div class=\"avatar-wrapper\" *ngIf=\"isShowImage\">\r\n            <img class=\"rounded-avatar\"\r\n                 src=\"{{ selectedUser?.avatar }}\"\r\n                 alt=\"{{ selectedUser?.fullName }}\"\r\n                 (error)=\"onImageError(selectedUser)\">\r\n        </div><!-- END: .avatar-wrapper -->\r\n        <div class=\"user-info\">\r\n            <span class=\"full-name\">{{ selectedUser?.fullName }}</span>\r\n            <span class=\"description\" *ngIf=\"selectedUser.officeName && selectedUser.positionName\">{{ selectedUser?.officeName }} - {{ selectedUser?.positionName }}</span>\r\n        </div><!-- END: .info -->\r\n    </div><!-- END: .hus-item -->\r\n</ng-template><!-- END: single selected template -->\r\n"

/***/ }),

/***/ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.component.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.component.scss ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".rounded-avatar {\n  border-radius: 50%; }\n\n.avatar-wrapper {\n  overflow: hidden; }\n\n.avatar-xs {\n  width: 16px;\n  hight: 16px; }\n\n.avatar-sm {\n  width: 32px;\n  height: 32px; }\n\n.hus-selected-wrapper .hus-item-selected .avatar-wrapper {\n  width: 20px;\n  height: auto;\n  padding-left: 0 !important; }\n\n.hus-selected-wrapper .hus-item-selected .avatar-wrapper img {\n    width: 100%;\n    border-radius: 50% !important; }\n\n.hus-container {\n  border: 1px solid #ddd;\n  background: white;\n  border-radius: 5px !important;\n  position: relative; }\n\n.hus-container:hover {\n    cursor: pointer; }\n\n.hus-container.active {\n    border: 1px solid #007bff;\n    background: white;\n    box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25); }\n\n.hus-container.active .hus-search-wrapper .hus-search-content .hus-search-input input {\n      background: white;\n      border: none;\n      outline: none; }\n\n.hus-container.active .hus-search-wrapper .hus-search-content .hus-search-input input:focus, .hus-container.active .hus-search-wrapper .hus-search-content .hus-search-input input:active, .hus-container.active .hus-search-wrapper .hus-search-content .hus-search-input input:visited {\n        border: none;\n        outline: none;\n        box-shadow: none !important; }\n\n.hus-container.active .hus-search-wrapper .hus-search-content .hus-item .user-info {\n      margin-bottom: 0 !important; }\n\n.hus-container ul {\n    list-style: none;\n    padding-left: 0;\n    margin-bottom: 0; }\n\n.hus-container .hus-search-wrapper {\n    align-items: center;\n    display: flex;\n    width: 100%;\n    min-height: 37px; }\n\n.hus-container .hus-search-wrapper .hus-search-content {\n      white-space: nowrap;\n      width: 100%;\n      flex: 1 1 auto;\n      margin: 3px 8px; }\n\n.hus-container .hus-search-wrapper .hus-search-content .hus-search-input {\n        white-space: nowrap;\n        width: 100%;\n        flex: 1 1 auto;\n        margin: 3px 8px; }\n\n.hus-container .hus-search-wrapper .hus-search-content .hus-search-input input {\n          border: none;\n          display: block;\n          width: 100%;\n          background: white; }\n\n.hus-container .hus-search-wrapper .hus-search-content .hus-search-input input:focus, .hus-container .hus-search-wrapper .hus-search-content .hus-search-input input:active, .hus-container .hus-search-wrapper .hus-search-content .hus-search-input input:visited {\n            border: none !important;\n            outline: none !important;\n            box-shadow: none !important; }\n\n.hus-container .hus-search-wrapper .hus-search-content ul {\n        display: flex;\n        flex-wrap: wrap;\n        justify-content: flex-start;\n        width: 100%; }\n\n.hus-container .hus-search-wrapper .hus-search-content ul li.hus-item-selected {\n          box-sizing: border-box;\n          display: inline-block;\n          background-color: #f4f5f7;\n          margin-top: 5px;\n          border-radius: 5px !important;\n          margin-left: 5px; }\n\n.hus-container .hus-search-wrapper .hus-search-content ul li.hus-item-selected div.hus-item {\n            color: #253858;\n            cursor: default;\n            display: flex;\n            height: 20px;\n            line-height: 1;\n            border-radius: 3px;\n            margin: 4px !important;\n            padding: 0px;\n            overflow: initial; }\n\n.hus-container .hus-search-wrapper .hus-search-content ul li.hus-item-selected div.hus-item .avatar-wrapper {\n              align-items: center;\n              display: flex;\n              justify-content: center; }\n\n.hus-container .hus-search-wrapper .hus-search-content ul li.hus-item-selected div.hus-item .user-info {\n              margin: 0 5px;\n              margin-bottom: 0 !important; }\n\n.hus-container .hus-search-wrapper .hus-search-content ul li.hus-item-selected div.hus-item .user-info .full-name {\n                font-size: 14px;\n                font-weight: normal;\n                line-height: 1;\n                margin-left: 4px;\n                margin-right: 4px;\n                max-width: 160px;\n                text-overflow: ellipsis;\n                white-space: nowrap;\n                padding: 2px 0px;\n                overflow: hidden; }\n\n.hus-container .hus-search-wrapper .hus-search-icon {\n      align-items: center;\n      display: flex;\n      justify-content: center;\n      flex: 0 0 24px;\n      margin: 0px 8px;\n      color: #222; }\n\n.hus-container .hus-search-wrapper .avatar-wrapper {\n      width: 30px; }\n\n.hus-container .hus-search-wrapper .avatar-wrapper img {\n        width: 100%; }\n\n.hus-container .hus-search-result-wrapper {\n    position: absolute;\n    left: 0;\n    top: 100%;\n    max-height: 250px;\n    overflow-y: auto;\n    background: white;\n    width: 100%;\n    z-index: 999999;\n    border: 1px solid #ddd;\n    border-radius: 5px !important;\n    box-shadow: rgba(9, 30, 66, 0.25) 0px 4px 8px -2px, rgba(9, 30, 66, 0.31) 0px 0px 1px; }\n\n.hus-container .hus-search-result-wrapper ul {\n      padding: 5px 0; }\n\n.hus-container .hus-search-result-wrapper ul li {\n        align-items: center;\n        box-sizing: border-box;\n        color: #172b4d;\n        cursor: pointer;\n        display: flex;\n        flex-wrap: nowrap;\n        font-size: 14px;\n        font-weight: normal;\n        padding: 0px 12px;\n        text-decoration: none; }\n\n.hus-container .hus-search-result-wrapper ul li.active, .hus-container .hus-search-result-wrapper ul li:hover {\n          cursor: pointer;\n          background-color: #f4f5f7; }\n\n.hus-container .hus-search-result-wrapper ul li.searching {\n          min-height: 34px; }\n\n.hus-container .hus-search-result-wrapper ul li.searching:hover {\n            background-color: white; }\n\n.hus-container .hus-search-result-wrapper ul li.searching div {\n            margin-left: 5px; }\n\n.hus-container .hus-search-result-wrapper ul li div.avatar-wrapper {\n          width: 32px;\n          height: 32px; }\n\n.hus-container .hus-search-result-wrapper ul li div.avatar-wrapper img {\n            width: 100%; }\n\n.hus-container .hus-item {\n    align-items: center;\n    box-sizing: border-box;\n    color: #172b4d;\n    cursor: pointer;\n    display: flex;\n    flex-wrap: nowrap;\n    font-size: 14px;\n    font-weight: normal;\n    padding: 0px 12px;\n    text-decoration: none; }\n\n.hus-container .hus-item div.avatar-wrapper {\n      background-color: white;\n      color: #091e42;\n      display: flex;\n      flex-direction: column;\n      max-height: calc(100% - 1px);\n      outline: 0;\n      align-self: flex-start;\n      border-radius: 50% !important; }\n\n.hus-container .hus-item div.user-info {\n      display: flex;\n      flex-direction: column;\n      margin: 0px 8px;\n      overflow: hidden; }\n\n.hus-container .hus-item div.user-info .full-name {\n        font-weight: bold; }\n\n.hus-container .hus-item div.user-info .description {\n        font-size: 12px;\n        color: #999; }\n\n.hus-container .hus-item .remove {\n      height: 16px;\n      width: 16px;\n      color: currentcolor;\n      display: inline-block;\n      fill: white;\n      line-height: 1; }\n\n.hus-container .hus-item .remove:hover {\n        cursor: pointer;\n        color: #bf2600; }\n\n.hus-container .hus-item .remove svg {\n        height: 16px;\n        width: 16px;\n        max-height: 100%;\n        max-width: 100%;\n        vertical-align: bottom;\n        overflow: hidden; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL2dobS11c2VyLXN1Z2dlc3Rpb24vRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFwcFxcc2hhcmVkc1xcY29tcG9uZW50c1xcZ2htLXVzZXItc3VnZ2VzdGlvblxcZ2htLXVzZXItc3VnZ2VzdGlvbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFJQTtFQUdJLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLFdBQVc7RUFDWCxXQUFXLEVBQUE7O0FBR2Y7RUFDSSxXQUFXO0VBQ1gsWUFBWSxFQUFBOztBQUdoQjtFQUVRLFdBQVc7RUFDWCxZQUFZO0VBQ1osMEJBQTBCLEVBQUE7O0FBSmxDO0lBT1ksV0FBVztJQUNYLDZCQUE2QixFQUFBOztBQUt6QztFQUNJLHNCQXJDYztFQXNDZCxpQkF2Q1c7RUF3Q1gsNkJBQTZCO0VBQzdCLGtCQUFrQixFQUFBOztBQUp0QjtJQU9RLGVBQWUsRUFBQTs7QUFQdkI7SUFXUSx5QkE5Q21CO0lBK0NuQixpQkFBaUI7SUFDakIsZ0RBQStDLEVBQUE7O0FBYnZEO01BaUJnQixpQkFBaUI7TUFDakIsWUFBWTtNQUNaLGFBQWEsRUFBQTs7QUFuQjdCO1FBc0JvQixZQUFZO1FBQ1osYUFBYTtRQUNiLDJCQUEyQixFQUFBOztBQXhCL0M7TUE2QmdCLDJCQUEyQixFQUFBOztBQTdCM0M7SUFtQ1EsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixnQkFBZ0IsRUFBQTs7QUFyQ3hCO0lBMENRLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsV0FBVztJQUNYLGdCQUFnQixFQUFBOztBQTdDeEI7TUFnRFksbUJBQW1CO01BQ25CLFdBQVc7TUFDWCxjQUFjO01BQ2QsZUFBZSxFQUFBOztBQW5EM0I7UUFzRGdCLG1CQUFtQjtRQUNuQixXQUFXO1FBQ1gsY0FBYztRQUNkLGVBQWUsRUFBQTs7QUF6RC9CO1VBNERvQixZQUFZO1VBQ1osY0FBYztVQUNkLFdBQVc7VUFDWCxpQkFwR0wsRUFBQTs7QUFxQ2Y7WUFrRXdCLHVCQUF1QjtZQUN2Qix3QkFBd0I7WUFDeEIsMkJBQTJCLEVBQUE7O0FBcEVuRDtRQTBFZ0IsYUFBYTtRQUNiLGVBQWU7UUFFZiwyQkFBMkI7UUFDM0IsV0FBVyxFQUFBOztBQTlFM0I7VUFpRm9CLHNCQUFzQjtVQUN0QixxQkFBcUI7VUFDckIseUJBQW9DO1VBQ3BDLGVBQWU7VUFDZiw2QkFBNkI7VUFDN0IsZ0JBQWdCLEVBQUE7O0FBdEZwQztZQXlGd0IsY0FBc0I7WUFDdEIsZUFBZTtZQUNmLGFBQWE7WUFDYixZQUFZO1lBQ1osY0FBYztZQUNkLGtCQUFrQjtZQUNsQixzQkFBc0I7WUFDdEIsWUFBWTtZQUNaLGlCQUFpQixFQUFBOztBQWpHekM7Y0FxRzRCLG1CQUFtQjtjQUNuQixhQUFhO2NBRWIsdUJBQXVCLEVBQUE7O0FBeEduRDtjQTRHNEIsYUFBYTtjQUNiLDJCQUEyQixFQUFBOztBQTdHdkQ7Z0JBZ0hnQyxlQUFlO2dCQUNmLG1CQUFtQjtnQkFDbkIsY0FBYztnQkFDZCxnQkFBZ0I7Z0JBQ2hCLGlCQUFpQjtnQkFDakIsZ0JBQWdCO2dCQUNoQix1QkFBdUI7Z0JBQ3ZCLG1CQUFtQjtnQkFDbkIsZ0JBQWdCO2dCQUNoQixnQkFBZ0IsRUFBQTs7QUF6SGhEO01BbUlZLG1CQUFtQjtNQUNuQixhQUFhO01BRWIsdUJBQXVCO01BQ3ZCLGNBQWM7TUFDZCxlQUFlO01BQ2YsV0FBVyxFQUFBOztBQXpJdkI7TUE2SVksV0FBVyxFQUFBOztBQTdJdkI7UUFnSmdCLFdBQVcsRUFBQTs7QUFoSjNCO0lBc0pRLGtCQUFrQjtJQUNsQixPQUFPO0lBQ1AsU0FBUztJQUNULGlCQUFpQjtJQUNqQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLFdBQVc7SUFDWCxlQUFlO0lBQ2Ysc0JBbE1VO0lBcU1WLDZCQUE2QjtJQUM3QixxRkFBcUYsRUFBQTs7QUFsSzdGO01BcUtZLGNBQWMsRUFBQTs7QUFySzFCO1FBeUtnQixtQkFBbUI7UUFDbkIsc0JBQXNCO1FBQ3RCLGNBQXNCO1FBQ3RCLGVBQWU7UUFDZixhQUFhO1FBQ2IsaUJBQWlCO1FBQ2pCLGVBQWU7UUFDZixtQkFBbUI7UUFDbkIsaUJBQWlCO1FBQ2pCLHFCQUFxQixFQUFBOztBQWxMckM7VUFxTG9CLGVBQWU7VUFDZix5QkFBb0MsRUFBQTs7QUF0THhEO1VBMExvQixnQkFBZ0IsRUFBQTs7QUExTHBDO1lBNkx3Qix1QkFBdUIsRUFBQTs7QUE3TC9DO1lBaU13QixnQkFBZ0IsRUFBQTs7QUFqTXhDO1VBc01vQixXQUFXO1VBQ1gsWUFBWSxFQUFBOztBQXZNaEM7WUEwTXdCLFdBQVcsRUFBQTs7QUExTW5DO0lBbU5RLG1CQUFtQjtJQUNuQixzQkFBc0I7SUFDdEIsY0FBc0I7SUFDdEIsZUFBZTtJQUNmLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIscUJBQXFCLEVBQUE7O0FBNU43QjtNQStOWSx1QkFBb0M7TUFDcEMsY0FBcUI7TUFDckIsYUFBYTtNQUNiLHNCQUFzQjtNQUN0Qiw0QkFBNEI7TUFDNUIsVUFBVTtNQUNWLHNCQUFzQjtNQUd0Qiw2QkFBNkIsRUFBQTs7QUF4T3pDO01BNE9ZLGFBQWE7TUFDYixzQkFBc0I7TUFDdEIsZUFBZTtNQUNmLGdCQUFnQixFQUFBOztBQS9PNUI7UUFrUGdCLGlCQUFpQixFQUFBOztBQWxQakM7UUFxUGdCLGVBQWU7UUFDZixXQUFXLEVBQUE7O0FBdFAzQjtNQTBQWSxZQUFZO01BQ1osV0FBVztNQUNYLG1CQUFtQjtNQUNuQixxQkFBcUI7TUFDckIsV0FBd0I7TUFDeEIsY0FBYyxFQUFBOztBQS9QMUI7UUFrUWdCLGVBQWU7UUFDZixjQUFzQixFQUFBOztBQW5RdEM7UUF1UWdCLFlBQVk7UUFDWixXQUFXO1FBQ1gsZ0JBQWdCO1FBQ2hCLGVBQWU7UUFDZixzQkFBc0I7UUFDdEIsZ0JBQWdCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWRzL2NvbXBvbmVudHMvZ2htLXVzZXItc3VnZ2VzdGlvbi9naG0tdXNlci1zdWdnZXN0aW9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiJGJnQ29sb3I6IHdoaXRlO1xyXG4kYm9yZGVyQ29sb3I6ICNkZGQ7XHJcbiRib3JkZXJBY3RpdmVDb2xvcjogIzAwN2JmZjtcclxuXHJcbi5yb3VuZGVkLWF2YXRhciB7XHJcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIC1tb3otYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG59XHJcblxyXG4uYXZhdGFyLXdyYXBwZXIge1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG5cclxuLmF2YXRhci14cyB7XHJcbiAgICB3aWR0aDogMTZweDtcclxuICAgIGhpZ2h0OiAxNnB4O1xyXG59XHJcblxyXG4uYXZhdGFyLXNtIHtcclxuICAgIHdpZHRoOiAzMnB4O1xyXG4gICAgaGVpZ2h0OiAzMnB4O1xyXG59XHJcblxyXG4uaHVzLXNlbGVjdGVkLXdyYXBwZXIge1xyXG4gICAgLmh1cy1pdGVtLXNlbGVjdGVkIC5hdmF0YXItd3JhcHBlciB7XHJcbiAgICAgICAgd2lkdGg6IDIwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogMCAhaW1wb3J0YW50O1xyXG5cclxuICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4uaHVzLWNvbnRhaW5lciB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAkYm9yZGVyQ29sb3I7XHJcbiAgICBiYWNrZ3JvdW5kOiAkYmdDb2xvcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweCAhaW1wb3J0YW50O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG5cclxuICAgICY6aG92ZXIge1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIH1cclxuXHJcbiAgICAmLmFjdGl2ZSB7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgJGJvcmRlckFjdGl2ZUNvbG9yO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDAgMCAwIDAuMnJlbSByZ2JhKDAsIDEyMywgMjU1LCAuMjUpO1xyXG5cclxuICAgICAgICAuaHVzLXNlYXJjaC13cmFwcGVyIC5odXMtc2VhcmNoLWNvbnRlbnQge1xyXG4gICAgICAgICAgICAuaHVzLXNlYXJjaC1pbnB1dCBpbnB1dCB7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgICAgICAgICAgIG91dGxpbmU6IG5vbmU7XHJcblxyXG4gICAgICAgICAgICAgICAgJjpmb2N1cywgJjphY3RpdmUsICY6dmlzaXRlZCB7XHJcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgICAgICAgICAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICAgICAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAuaHVzLWl0ZW0gLnVzZXItaW5mbyB7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgdWwge1xyXG4gICAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAwO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICB9XHJcblxyXG4gICAgLmh1cy1zZWFyY2gtd3JhcHBlciB7XHJcbiAgICAgICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgbWluLWhlaWdodDogMzdweDtcclxuXHJcbiAgICAgICAgLmh1cy1zZWFyY2gtY29udGVudCB7XHJcbiAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICBmbGV4OiAxIDEgYXV0bztcclxuICAgICAgICAgICAgbWFyZ2luOiAzcHggOHB4O1xyXG5cclxuICAgICAgICAgICAgLmh1cy1zZWFyY2gtaW5wdXQge1xyXG4gICAgICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgZmxleDogMSAxIGF1dG87XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDNweCA4cHg7XHJcblxyXG4gICAgICAgICAgICAgICAgaW5wdXQge1xyXG4gICAgICAgICAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAkYmdDb2xvcjtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgJjpmb2N1cywgJjphY3RpdmUsICY6dmlzaXRlZCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlcjogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvdXRsaW5lOiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHVsIHtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAgICAgICAgICAgICAtd2Via2l0LWJveC1wYWNrOiBzdGFydDtcclxuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgICAgICAgICAgICAgIGxpLmh1cy1pdGVtLXNlbGVjdGVkIHtcclxuICAgICAgICAgICAgICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjQ0LCAyNDUsIDI0Nyk7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDVweCAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGRpdi5odXMtaXRlbSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiByZ2IoMzcsIDU2LCA4OCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGN1cnNvcjogZGVmYXVsdDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDRweCAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG92ZXJmbG93OiBpbml0aWFsO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLmF2YXRhci13cmFwcGVyIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAudXNlci1pbmZvIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbjogMCA1cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLmZ1bGwtbmFtZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDRweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDRweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDE2MHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMnB4IDBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuaHVzLXNlYXJjaC1pY29uIHtcclxuICAgICAgICAgICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgZmxleDogMCAwIDI0cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbjogMHB4IDhweDtcclxuICAgICAgICAgICAgY29sb3I6ICMyMjI7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuYXZhdGFyLXdyYXBwZXIge1xyXG4gICAgICAgICAgICB3aWR0aDogMzBweDtcclxuXHJcbiAgICAgICAgICAgIGltZyB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuaHVzLXNlYXJjaC1yZXN1bHQtd3JhcHBlciB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgdG9wOiAxMDAlO1xyXG4gICAgICAgIG1heC1oZWlnaHQ6IDI1MHB4O1xyXG4gICAgICAgIG92ZXJmbG93LXk6IGF1dG87XHJcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgei1pbmRleDogOTk5OTk5O1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRib3JkZXJDb2xvcjtcclxuICAgICAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDVweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIC1tb3otYm9yZGVyLXJhZGl1czogNXB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgYm94LXNoYWRvdzogcmdiYSg5LCAzMCwgNjYsIDAuMjUpIDBweCA0cHggOHB4IC0ycHgsIHJnYmEoOSwgMzAsIDY2LCAwLjMxKSAwcHggMHB4IDFweDtcclxuXHJcbiAgICAgICAgdWwge1xyXG4gICAgICAgICAgICBwYWRkaW5nOiA1cHggMDtcclxuXHJcbiAgICAgICAgICAgIGxpIHtcclxuICAgICAgICAgICAgICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiByZ2IoMjMsIDQzLCA3Nyk7XHJcbiAgICAgICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICAgICAgZmxleC13cmFwOiBub3dyYXA7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogMHB4IDEycHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcblxyXG4gICAgICAgICAgICAgICAgJi5hY3RpdmUsICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjQ0LCAyNDUsIDI0Nyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgJi5zZWFyY2hpbmcge1xyXG4gICAgICAgICAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDM0cHg7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGRpdiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGRpdi5hdmF0YXItd3JhcHBlciB7XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDMycHg7XHJcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAzMnB4O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmh1cy1pdGVtIHtcclxuICAgICAgICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICBjb2xvcjogcmdiKDIzLCA0MywgNzcpO1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtd3JhcDogbm93cmFwO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgIHBhZGRpbmc6IDBweCAxMnB4O1xyXG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuXHJcbiAgICAgICAgZGl2LmF2YXRhci13cmFwcGVyIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDI1NSwgMjU1LCAyNTUpO1xyXG4gICAgICAgICAgICBjb2xvcjogcmdiKDksIDMwLCA2Nik7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgICAgIG1heC1oZWlnaHQ6IGNhbGMoMTAwJSAtIDFweCk7XHJcbiAgICAgICAgICAgIG91dGxpbmU6IDA7XHJcbiAgICAgICAgICAgIGFsaWduLXNlbGY6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNTAlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIC1tb3otYm9yZGVyLXJhZGl1czogNTAlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJSAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZGl2LnVzZXItaW5mbyB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgICAgIG1hcmdpbjogMHB4IDhweDtcclxuICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuXHJcbiAgICAgICAgICAgIC5mdWxsLW5hbWUge1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmRlc2NyaXB0aW9uIHtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAjOTk5O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5yZW1vdmUge1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDE2cHg7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxNnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogY3VycmVudGNvbG9yO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgIGZpbGw6IHJnYigyNTUsIDI1NSwgMjU1KTtcclxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDE7XHJcblxyXG4gICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiByZ2IoMTkxLCAzOCwgMCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHN2ZyB7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDE2cHg7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTZweDtcclxuICAgICAgICAgICAgICAgIG1heC1oZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogYm90dG9tO1xyXG4gICAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: UserSuggestion, GhmUserSuggestionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserSuggestion", function() { return UserSuggestion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmUserSuggestionComponent", function() { return GhmUserSuggestionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ghm_user_suggestion_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ghm-user-suggestion.service */ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");







var UserSuggestion = /** @class */ (function () {
    function UserSuggestion(id, fullName, officeName, positionName, avatar, isSelected) {
        this.id = id;
        this.fullName = fullName;
        this.officeName = officeName;
        this.positionName = positionName;
        this.avatar = avatar;
        this.isSelected = isSelected;
        this.isActive = false;
    }
    return UserSuggestion;
}());

var GhmUserSuggestionComponent = /** @class */ (function () {
    function GhmUserSuggestionComponent(el, renderer, userSuggestionService) {
        this.el = el;
        this.renderer = renderer;
        this.userSuggestionService = userSuggestionService;
        this.type = 'input';
        this.multiple = false;
        this.isShowSelected = true;
        this.placeholder = 'Vui lòng nhập tên nhân viên cần tìm';
        this.noImageFallback = '/assets/images/noavatar.png';
        this.isReceipt = false;
        this.isShowImage = true;
        this.userSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.userRemoved = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.keyUpPressed = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this._subscribers = {};
        this._selectedUser = null;
        this.isLoading = false;
        this.isActive = false;
        this.selectedUsers = [];
        this.searchTerms = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
        this.listUsers = [];
        this.propagateChange = function () {
        };
    }
    GhmUserSuggestionComponent_1 = GhmUserSuggestionComponent;
    Object.defineProperty(GhmUserSuggestionComponent.prototype, "selectedUser", {
        get: function () {
            return this._selectedUser;
        },
        set: function (value) {
            if (value instanceof Array) {
                this.selectedUsers = value;
            }
            else {
                this._selectedUser = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GhmUserSuggestionComponent.prototype, "sources", {
        get: function () {
            return this._sources ? this._sources : [];
        },
        set: function (values) {
            this._sources = values;
            this.listUsers = values;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GhmUserSuggestionComponent.prototype, "userId", {
        get: function () {
            return this._userId;
        },
        set: function (userId) {
            this._userId = userId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GhmUserSuggestionComponent.prototype, "isShowListSuggestion", {
        get: function () {
            return this._isShowSuggestionList;
        },
        enumerable: true,
        configurable: true
    });
    GhmUserSuggestionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._subscribers.getUsers = this.searchTerms.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["debounceTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["distinctUntilChanged"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["tap"])(function () { return _this.isLoading = true; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(function (term) { return _this.userSuggestionService.search(term)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.isLoading = false; })); })).subscribe(function (result) {
            _this.listUsers = result.map(function (user) {
                user.isSelected = lodash__WEBPACK_IMPORTED_MODULE_2__["countBy"](_this.selectedUsers, function (selectedUser) {
                    return selectedUser.id === user.id;
                }).true;
                return user;
            });
        });
    };
    GhmUserSuggestionComponent.prototype.ngOnDestroy = function () {
        this.selectedUser = null;
        this._subscribers.getUsers.unsubscribe();
    };
    GhmUserSuggestionComponent.prototype.registerOnChange = function (fn) {
        this.propagateChange = fn;
    };
    GhmUserSuggestionComponent.prototype.registerOnTouched = function (fn) {
    };
    GhmUserSuggestionComponent.prototype.onDocumentClick = function (targetElement) {
        if (this.el.nativeElement && !this.el.nativeElement.contains(targetElement.target)) {
            this.isActive = false;
        }
    };
    GhmUserSuggestionComponent.prototype.activeSuggestion = function () {
        var _this = this;
        if (this.isActive) {
            return;
        }
        this.isActive = true;
        setTimeout(function () {
            _this.searchTerms.next(_this.keyword);
        }, 0);
    };
    GhmUserSuggestionComponent.prototype.inputKeyUp = function (event) {
        var isNavigation = this.navigate(event);
        if (!isNavigation) {
            this.searchTerms.next(this.keyword);
            this.keyUpPressed.emit(event);
        }
    };
    GhmUserSuggestionComponent.prototype.onImageError = function (user) {
        if (user) {
            user.avatar = this.noImageFallback;
        }
    };
    GhmUserSuggestionComponent.prototype.selectUser = function (user) {
        if (!this.multiple) {
            this.isActive = false;
            this.keyword = '';
            this.selectedUser = user;
            this.propagateChange(user.id);
            this.userSelected.emit(user);
        }
        else {
            user.isSelected = !user.isSelected;
            var selectedUsers = lodash__WEBPACK_IMPORTED_MODULE_2__["filter"](this.listUsers, function (userItem) { return userItem.isSelected; });
            this.selectedUsers = selectedUsers;
            this.keyword = '';
            this.userSelected.emit(selectedUsers);
        }
    };
    GhmUserSuggestionComponent.prototype.removeSelectedUser = function (user) {
        if (this.multiple && this.selectedUsers instanceof Array) {
            lodash__WEBPACK_IMPORTED_MODULE_2__["remove"](this.selectedUsers, function (userItem) { return userItem.id === user.id; });
        }
        else {
            this.selectedUsers = null;
        }
        this.resetActiveStatus();
        this.userRemoved.emit(user);
    };
    GhmUserSuggestionComponent.prototype.writeValue = function (value) {
        this.userId = value;
    };
    GhmUserSuggestionComponent.prototype.clear = function () {
        this.selectedUser = null;
    };
    GhmUserSuggestionComponent.prototype.navigate = function (key) {
        var keyCode = key.keyCode;
        // Escape
        if (keyCode === 27) {
            this.isActive = false;
            return true;
        }
        if (keyCode === 27 || keyCode === 17 || key.ctrlKey) {
            return true;
        }
        // 37: Left arrow
        // 38: Up arrow
        // 39: Right arrow
        // 40: Down arrow
        // 13: Enter
        if (keyCode === 37 || keyCode === 38 || keyCode === 39 || keyCode === 40 || keyCode === 13) {
            switch (keyCode) {
                case 37:
                case 38:
                    this.back();
                    break;
                case 39:
                case 40:
                    this.next();
                    break;
                case 13:
                    var selectedItems = this.listUsers.find(function (user) {
                        return user.isActive;
                    });
                    this.selectUser(selectedItems);
                    break;
            }
            key.preventDefault();
            return true;
        }
        return false;
    };
    GhmUserSuggestionComponent.prototype.next = function () {
        var index = this.getActiveUserIndex();
        if (index === -1) {
            var firstUser = this.listUsers[0];
            if (firstUser) {
                firstUser.isActive = true;
            }
        }
        else {
            index = index < this.listUsers.length - 1 ? index + 1 : 0;
            this.setUserActiveStatus(index);
        }
    };
    GhmUserSuggestionComponent.prototype.back = function () {
        var index = this.getActiveUserIndex();
        if (index === -1) {
            var lastUser = this.listUsers[this.listUsers.length - 1];
            if (lastUser) {
                lastUser.isActive = true;
            }
        }
        else {
            index = index > 0 ? index - 1 : this.listUsers.length - 1;
            this.setUserActiveStatus(index);
        }
    };
    GhmUserSuggestionComponent.prototype.resetActiveStatus = function () {
        this.listUsers.forEach(function (user) { return user.isActive = false; });
    };
    GhmUserSuggestionComponent.prototype.getActiveUserIndex = function () {
        return lodash__WEBPACK_IMPORTED_MODULE_2__["findIndex"](this.listUsers, function (userItem) {
            return userItem.isActive;
        });
    };
    GhmUserSuggestionComponent.prototype.setUserActiveStatus = function (index) {
        this.listUsers.forEach(function (userItem) { return userItem.isActive = false; });
        var user = this.listUsers[index];
        if (user) {
            user.isActive = true;
        }
    };
    GhmUserSuggestionComponent.prototype.resetSelectedStatus = function () {
        if (this.selectedUsers instanceof Array) {
            lodash__WEBPACK_IMPORTED_MODULE_2__["each"](this.selectedUsers, function (userItem) {
                userItem.isSelected = false;
            });
        }
    };
    var GhmUserSuggestionComponent_1;
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmUserSuggestionComponent.prototype, "type", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmUserSuggestionComponent.prototype, "multiple", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmUserSuggestionComponent.prototype, "isShowSelected", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmUserSuggestionComponent.prototype, "placeholder", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmUserSuggestionComponent.prototype, "noImageFallback", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmUserSuggestionComponent.prototype, "isReceipt", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmUserSuggestionComponent.prototype, "isShowImage", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
    ], GhmUserSuggestionComponent.prototype, "selectedUser", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Array])
    ], GhmUserSuggestionComponent.prototype, "sources", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [String])
    ], GhmUserSuggestionComponent.prototype, "userId", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmUserSuggestionComponent.prototype, "userSelected", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmUserSuggestionComponent.prototype, "userRemoved", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmUserSuggestionComponent.prototype, "keyUpPressed", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('document:click', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], GhmUserSuggestionComponent.prototype, "onDocumentClick", null);
    GhmUserSuggestionComponent = GhmUserSuggestionComponent_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ghm-user-suggestion',
            template: __webpack_require__(/*! ./ghm-user-suggestion.component.html */ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.component.html"),
            providers: [
                {
                    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NG_VALUE_ACCESSOR"],
                    useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () { return GhmUserSuggestionComponent_1; }),
                    multi: true
                },
                _ghm_user_suggestion_service__WEBPACK_IMPORTED_MODULE_4__["GhmUserSuggestionService"]
            ],
            styles: [__webpack_require__(/*! ./ghm-user-suggestion.component.scss */ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"],
            _ghm_user_suggestion_service__WEBPACK_IMPORTED_MODULE_4__["GhmUserSuggestionService"]])
    ], GhmUserSuggestionComponent);
    return GhmUserSuggestionComponent;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.module.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.module.ts ***!
  \**************************************************************************************/
/*! exports provided: GhmUserSuggestionModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmUserSuggestionModule", function() { return GhmUserSuggestionModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ghm_user_suggestion_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ghm-user-suggestion.component */ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.component.ts");
/* harmony import */ var _ghm_user_suggestion_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ghm-user-suggestion.service */ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../core/core.module */ "./src/app/core/core.module.ts");







var GhmUserSuggestionModule = /** @class */ (function () {
    function GhmUserSuggestionModule() {
    }
    GhmUserSuggestionModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _core_core_module__WEBPACK_IMPORTED_MODULE_6__["CoreModule"]],
            exports: [_ghm_user_suggestion_component__WEBPACK_IMPORTED_MODULE_3__["GhmUserSuggestionComponent"]],
            declarations: [_ghm_user_suggestion_component__WEBPACK_IMPORTED_MODULE_3__["GhmUserSuggestionComponent"]],
            providers: [_ghm_user_suggestion_service__WEBPACK_IMPORTED_MODULE_4__["GhmUserSuggestionService"]],
        })
    ], GhmUserSuggestionModule);
    return GhmUserSuggestionModule;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.service.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.service.ts ***!
  \***************************************************************************************/
/*! exports provided: GhmUserSuggestionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmUserSuggestionService", function() { return GhmUserSuggestionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");






var GhmUserSuggestionService = /** @class */ (function () {
    function GhmUserSuggestionService(appConfig, http) {
        this.appConfig = appConfig;
        this.http = http;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].apiGatewayUrl + "api/v1/hr/users/suggestions";
    }
    GhmUserSuggestionService.prototype.search = function (keyword) {
        // const url = `${this.appConfig.HR_API_URL}users/suggestions`;
        return this.http.get(this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]().set('keyword', keyword ? keyword : '')
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return result.items;
        }));
    };
    GhmUserSuggestionService.prototype.stripToVietnameChar = function (str) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
        str = str.replace(/đ/g, 'd');
        return str;
    };
    GhmUserSuggestionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_3__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], GhmUserSuggestionService);
    return GhmUserSuggestionService;
}());



/***/ })

}]);
//# sourceMappingURL=default~modules-configs-config-module~modules-customer-customer-module~modules-folders-folder-module~0b90d38a.js.map