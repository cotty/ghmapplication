(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-task-task-management-task-module"],{

/***/ "./src/app/modules/task/task-management/models/task-check-permission.model.ts":
/*!************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/models/task-check-permission.model.ts ***!
  \************************************************************************************/
/*! exports provided: TaskCheckPermission */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskCheckPermission", function() { return TaskCheckPermission; });
var TaskCheckPermission = /** @class */ (function () {
    function TaskCheckPermission() {
        this.isNotification = false;
        this.isComment = false;
        this.isReport = false;
        this.isWrite = false;
        this.isFull = false;
    }
    return TaskCheckPermission;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/models/task-participant.model.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/modules/task/task-management/models/task-participant.model.ts ***!
  \*******************************************************************************/
/*! exports provided: TaskParticipant */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskParticipant", function() { return TaskParticipant; });
var TaskParticipant = /** @class */ (function () {
    function TaskParticipant(taskId, userId, fullName) {
        this.id = Math.random().toString(36).substring(7);
        this.taskId = taskId;
        this.userId = userId;
        this.fullName = fullName;
        this.roleIds = [];
        this.roles = [];
    }
    return TaskParticipant;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/services/task.service.ts":
/*!***********************************************************************!*\
  !*** ./src/app/modules/task/task-management/services/task.service.ts ***!
  \***********************************************************************/
/*! exports provided: TaskService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskService", function() { return TaskService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _consts_tasks_const__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../consts/tasks.const */ "./src/app/modules/task/task-management/consts/tasks.const.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../environments/environment */ "./src/environments/environment.ts");










var TaskService = /** @class */ (function () {
    function TaskService(appConfig, spinnerService, http) {
        this.spinnerService = spinnerService;
        this.http = http;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].apiGatewayUrl + "api/v1/task/tasks";
        this.triggerSearch$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
    }
    TaskService.prototype.resolve = function (route, state) {
        var path = route.routeConfig.path;
        return this.search(path === 'list' ? 1 : 0, '', '', '', '', null, 0, 0);
    };
    TaskService.prototype.insert = function (task) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url, task)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    TaskService.prototype.update = function (id, task) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/" + id, task)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    TaskService.prototype.delete = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.delete(this.url + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    TaskService.prototype.search = function (type, userId, keyword, fromDate, toDate, targetId, workingType, workingConfig, page, pageSize) {
        var _this = this;
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 1000; }
        this.spinnerService.show();
        var taskFilter = {
            type: type !== null && type !== undefined ? type.toString() : '1',
            currentUserId: userId ? userId : '',
            keyword: keyword ? keyword : '',
            targetId: targetId !== null && targetId !== undefined ? targetId.toString() : '',
            workingType: workingType != null && workingType !== undefined ? workingType.toString() : '3',
            workingConfig: workingConfig != null && workingConfig !== undefined ? workingConfig.toString() : '0',
            startDate: fromDate ? fromDate : '',
            endDate: toDate ? toDate : '',
            page: page ? page.toString() : '1',
            pageSize: pageSize ? pageSize.toString() : '1000'
        };
        return this.http
            .post(this.url + "/filter", taskFilter)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            var listInProgress = lodash__WEBPACK_IMPORTED_MODULE_8__["filter"](result.items, function (task) {
                return task.status === _consts_tasks_const__WEBPACK_IMPORTED_MODULE_7__["TaskStatus"].inProgress || task.status === _consts_tasks_const__WEBPACK_IMPORTED_MODULE_7__["TaskStatus"].declineFinish;
            });
            var listNotStartYet = lodash__WEBPACK_IMPORTED_MODULE_8__["filter"](result.items, function (task) {
                return task.status === _consts_tasks_const__WEBPACK_IMPORTED_MODULE_7__["TaskStatus"].notStartYet;
            });
            var listCompleted = lodash__WEBPACK_IMPORTED_MODULE_8__["filter"](result.items, function (task) {
                return task.status === _consts_tasks_const__WEBPACK_IMPORTED_MODULE_7__["TaskStatus"].completed || task.status === _consts_tasks_const__WEBPACK_IMPORTED_MODULE_7__["TaskStatus"].pendingToFinish;
            });
            return {
                listInProgress: listInProgress,
                listNotStartYet: listNotStartYet,
                listCompleted: listCompleted
            };
        }));
    };
    TaskService.prototype.getDetail = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            var taskDetail = result.data;
            if (result.data && result.data.attachments) {
                result.data.attachments.forEach(function (attachment) {
                    attachment.isImage = _this.checkIsImage(attachment.extension);
                });
            }
            return taskDetail;
        }));
    };
    TaskService.prototype.getListTaskStatus = function () {
        return [
            { id: _consts_tasks_const__WEBPACK_IMPORTED_MODULE_7__["TaskStatus"].notStartYet, name: 'Chưa bắt đầu' },
            { id: _consts_tasks_const__WEBPACK_IMPORTED_MODULE_7__["TaskStatus"].inProgress, name: 'Đang tiến hành' },
            { id: _consts_tasks_const__WEBPACK_IMPORTED_MODULE_7__["TaskStatus"].pendingToFinish, name: 'Hoàn thành' },
        ];
    };
    TaskService.prototype.updateStatus = function (id, concurrencyStamp, status, explanation) {
        return this.http.post(this.url + "/" + id + "/status/" + status, {
            taskId: id,
            concurrencyStamp: concurrencyStamp,
            status: status,
            explanation: explanation
        });
    };
    TaskService.prototype.insertChecklist = function (taskId, checkList) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .post(this.url + "/" + taskId + "/checklists", checkList)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            return result;
        }));
    };
    TaskService.prototype.updateChecklist = function (taskId, checklist) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .post(this.url + "/" + taskId + "/checklists/" + checklist.id, checklist)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            return result;
        }));
    };
    TaskService.prototype.deleteChecklist = function (taskId, checklistId) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .delete(this.url + "/" + taskId + "/checklists/" + checklistId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    TaskService.prototype.updateChecklistStatus = function (taskId, checklistId, isCompleted) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .get(this.url + "/" + taskId + "/checklists/" + checklistId, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpParams"]()
                .set('isCompleted', isCompleted != null && isCompleted !== undefined ? isCompleted.toString() : '')
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    TaskService.prototype.insertParticipants = function (taskId, participant) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/" + taskId + "/participants", participant)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    TaskService.prototype.updateParticipant = function (taskId, participantId, participant) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/" + taskId + "/participants/" + participantId, participant)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    TaskService.prototype.removeParticipant = function (taskId, participantId) {
        var _this = this;
        this.spinnerService.show();
        return this.http.delete(this.url + "/" + taskId + "/participants/" + participantId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    TaskService.prototype.getParticipants = function (taskId) {
        return this.http.get(this.url + "/" + taskId + "/participants", {});
    };
    // Attachment
    TaskService.prototype.insertAttachment = function (taskId, attachment) {
        return this.http.post(this.url + "/" + taskId + "/attachment", attachment);
    };
    TaskService.prototype.insertListAttachment = function (taskId, attachments) {
        return this.http.post(this.url + "/" + taskId + "/attachments", attachments);
    };
    TaskService.prototype.deleteAttachment = function (taskId, attachmentId) {
        return this.http.delete(this.url + "/" + taskId + "/attachment/" + attachmentId);
    };
    TaskService.prototype.checkIsImage = function (extension) {
        return ['png', 'jpg', 'jpeg', 'gif'].indexOf(extension) > -1;
    };
    TaskService.prototype.getTaskChild = function (taskId) {
        return this.http.get(this.url + "/" + taskId + "/child");
    };
    TaskService.prototype.searchForReport = function (taskReportType, infoSearch) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/searchForReport", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpParams"]()
                .set('taskReportType', taskReportType.toString())
                .set('typeSearchTime', infoSearch.typeSearchTime.toString())
                .set('startDate', infoSearch.startDate)
                .set('endDate', infoSearch.endDate)
                .set('officeId', infoSearch.officeId ? infoSearch.officeId.toString() : '')
                .set('targetId', infoSearch.targetId ? infoSearch.targetId.toString() : '')
                .set('userId', infoSearch.userId ? infoSearch.userId : '')
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    TaskService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_6__["SpinnerService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]])
    ], TaskService);
    return TaskService;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/shared-components/task-user-suggestion/task-user-suggestion.component.css":
/*!************************************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/shared-components/task-user-suggestion/task-user-suggestion.component.css ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvdGFzay90YXNrLW1hbmFnZW1lbnQvc2hhcmVkLWNvbXBvbmVudHMvdGFzay11c2VyLXN1Z2dlc3Rpb24vdGFzay11c2VyLXN1Z2dlc3Rpb24uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/modules/task/task-management/shared-components/task-user-suggestion/task-user-suggestion.component.html":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/shared-components/task-user-suggestion/task-user-suggestion.component.html ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app-suggest\">\r\n    <div class=\"header\">\r\n        <span>Bạn đang xem mục tiêu của</span>\r\n    </div>\r\n    <div class=\"media\">\r\n        <div class=\"media-left\">\r\n            <img ghmImage\r\n                 [src]=\"selectedUser?.avatar\"\r\n                 [errorImageUrl]=\"'/assets/images/noavatar.png'\"\r\n                 class=\"avatar-sm\"/>\r\n        </div>\r\n        <div class=\"media-body\">\r\n            <h4 class=\"media-heading\">{{ selectedUser?.fullName }}</h4>\r\n            <i>{{ selectedUser?.description }}</i>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class=\"app-suggest cm-mgt-15\">\r\n    <div class=\"header\">\r\n        <span class=\"cm-mgb-10\"> Bạn muốn xem công việc của người khác</span>\r\n        <ghm-suggestion-user (itemSelected)=\"selectUser($event)\">\r\n        </ghm-suggestion-user>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/task/task-management/shared-components/task-user-suggestion/task-user-suggestion.component.ts":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/shared-components/task-user-suggestion/task-user-suggestion.component.ts ***!
  \***********************************************************************************************************************/
/*! exports provided: TaskUserSuggestionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskUserSuggestionComponent", function() { return TaskUserSuggestionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shareds_services_app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../shareds/services/app.service */ "./src/app/shareds/services/app.service.ts");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/task.service */ "./src/app/modules/task/task-management/services/task.service.ts");




var TaskUserSuggestionComponent = /** @class */ (function () {
    function TaskUserSuggestionComponent(appService, taskService) {
        this.appService = appService;
        this.taskService = taskService;
        this.selectedUser = this.appService.currentUser;
    }
    TaskUserSuggestionComponent.prototype.ngOnInit = function () {
    };
    TaskUserSuggestionComponent.prototype.selectUser = function (user) {
        this.selectedUser = {
            id: user.data.id,
            fullName: user.data.fullName,
            avatar: user.data.avatar,
            description: user.data.officeName + " - " + user.data.positionName
        };
        this.taskService.triggerSearch$.next(this.selectedUser);
    };
    TaskUserSuggestionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-user-suggestion',
            template: __webpack_require__(/*! ./task-user-suggestion.component.html */ "./src/app/modules/task/task-management/shared-components/task-user-suggestion/task-user-suggestion.component.html"),
            styles: [__webpack_require__(/*! ./task-user-suggestion.component.css */ "./src/app/modules/task/task-management/shared-components/task-user-suggestion/task-user-suggestion.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shareds_services_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"],
            _services_task_service__WEBPACK_IMPORTED_MODULE_3__["TaskService"]])
    ], TaskUserSuggestionComponent);
    return TaskUserSuggestionComponent;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-detail/task-detail.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-detail/task-detail.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"task-detail-container\">\r\n    <div class=\"portlet light bordered cm-mgb-0\">\r\n        <div class=\"portlet-title\">\r\n            <ul>\r\n                <li class=\"icon\">\r\n                    <a imgsrc=\"dlg-working\"></a>\r\n                </li>\r\n                <li>\r\n                    <h4 class=\"bold\">\r\n                        Thông tin công việc\r\n                    </h4>\r\n                </li>\r\n                <li class=\"icon pull-right\" (click)=\"closeModal()\">\r\n                    <a href=\"javascript://\" class=\"btn-close\" imgsrc=\"dlg-close\">\r\n                    </a>\r\n                </li>\r\n                <li class=\"icon pull-right\">\r\n                    <a href=\"javascript://\" imgsrc=\"dlg-action\" ghmDialogTrigger=\"\"\r\n                       [ghmDialogTriggerFor]=\"taskDetailAction\"></a>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n        <form action=\"\" class=\"form-horizontal\">\r\n            <div class=\"portlet-body\">\r\n                <div class=\"col-sm-12 cm-pdr-0\">\r\n                    <div class=\"col-sm-8 col-md-9\">\r\n                        <div class=\"row task-name cm-mgb-10\">\r\n                            <h4 #taskName\r\n                                (click)=\"isShowUpdateName = this.taskCheckPermission.isWrite\"\r\n                                [attr.contenteditable]=\"isShowUpdateName && this.taskCheckPermission.isWrite\"\r\n                                (keydown.enter)=\"saveName($event)\">\r\n                                {{ taskDetail?.name}}\r\n                            </h4>\r\n                            <div class=\"row\" *ngIf=\"isShowUpdateName\">\r\n                                <div class=\"col-sm-12 cm-mgt-5\">\r\n                                    <button type=\"button\" class=\"btn blue cm-mgr-5\"\r\n                                            (click)=\"saveName($event)\">\r\n                                        Cập nhật\r\n                                    </button>\r\n                                    <button type=\"button\" class=\"btn btn-light\"\r\n                                            (click)=\"saveName($event, false)\">\r\n                                        Đóng\r\n                                    </button>\r\n                                </div>\r\n                            </div>\r\n                        </div><!-- END: .task-name -->\r\n                        <div class=\"row task-actions\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-sm-6\">\r\n                                    <button type=\"button\" class=\"btn\"\r\n                                            [class.btn-success]=\"(taskDetail?.status === taskStatus.inProgress || taskDetail?.status === taskStatus.declineFinish) && taskDetail?.overdueDate <= 0\"\r\n                                            [class.default]=\"taskDetail?.status === taskStatus.notStartYet && taskDetail?.overdueDate <= 0\"\r\n                                            [class.btn-danger]=\"(taskDetail?.status === taskStatus.notStartYet || taskDetail?.status === taskStatus.inProgress\r\n                                                         || taskDetail?.status === taskStatus.declineFinish) && taskDetail?.overdueDate > 0\"\r\n                                            [class.btn-warning]=\"(taskDetail?.status === taskStatus.completed || taskDetail?.status === taskStatus.pendingToFinish) && taskDetail?.overdueDate > 0\"\r\n                                            [class.btn-primary]=\"(taskDetail?.status === taskStatus.completed || taskDetail?.status === taskStatus.pendingToFinish) && taskDetail?.overdueDate <= 0\"\r\n                                            ghmDialogTrigger=\"\" [ghmDialogTriggerFor]=\"taskDateDialog\"\r\n                                            [class.disabled]=\"!this.taskCheckPermission.isWrite\">\r\n                                        <i class=\"fa fa-calendar\"></i>\r\n                                        Thời hạn: <span> {{ taskDetail?.estimateStartDate | dateTimeFormat:'DD/MM/YYYY HH:mm' }} - {{ taskDetail?.estimateEndDate | dateTimeFormat:'DD/MM/YYYY HH:mm' }} </span>\r\n                                        <i class=\"fa fa-caret-down\"></i>\r\n                                    </button>\r\n                                </div>\r\n                                <div class=\"col-sm-6 text-right\">\r\n                                    <nh-select title=\"-- Chọn trạng thái --\"\r\n                                               *ngIf=\"taskDetail?.status !== taskStatus.completed; else taskStatusNameComplete\"\r\n                                               name=\"status\"\r\n                                               [isEnable]=\"this.taskCheckPermission.isWrite && taskDetail?.status !== taskStatus.completed\"\r\n                                               [btnClasses]=\"(taskDetail?.status === taskStatus.inProgress || taskDetail?.status === taskStatus.declineFinish) && taskDetail?.overdueDate <=0 ? 'btn-success'\r\n                                                                : taskDetail?.status === taskStatus.notStartYet && taskDetail?.overdueDate <= 0 ? 'default'\r\n                                                                : (taskDetail?.status === taskStatus.notStartYet || taskDetail?.status === taskStatus.inProgress\r\n                                                                 || taskDetail?.status === taskStatus.declineFinish) && taskDetail?.overdueDate > 0 ? 'btn-danger'\r\n                                                                : (taskDetail?.status === taskStatus.completed || taskDetail?.status === taskStatus.pendingToFinish) && taskDetail?.overdueDate > 0 ? 'btn-warning'\r\n                                                                : (taskDetail?.status === taskStatus.completed || taskDetail?.status === taskStatus.pendingToFinish) && taskDetail?.overdueDate <= 0 ? 'btn-primary' : ''\"\r\n                                               [data]=\"listStatus\"\r\n                                               [(ngModel)]=\"taskDetail.status\"\r\n                                               (itemSelected)=\"onStatusSelected($event)\"></nh-select>\r\n                                    <ng-template #taskStatusNameComplete>\r\n                                        <button type=\"button\" class=\"btn\" [disabled]=\"true\"\r\n                                                [class.btn-warning]=\"taskDetail?.overdueDate > 0\"\r\n                                                [class.btn-primary]=\"taskDetail?.overdueDate <= 0\">\r\n                                            Hoàn thành\r\n                                        </button>\r\n                                    </ng-template>\r\n                                </div>\r\n                            </div>\r\n                        </div><!-- END: .task-actions -->\r\n                        <div class=\"row task-block description\">\r\n                            <div class=\"task-block-content\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"col-sm-12\">\r\n                                        <p #taskDescription class=\"cm-mg-0 cm-mgb-5\"\r\n                                           (click)=\"isShowUpdateDescription = this.taskCheckPermission.isWrite\"\r\n                                           [attr.contenteditable]=\"isShowUpdateDescription && this.taskCheckPermission.isWrite\"\r\n                                           [innerHTML]=\"taskDetail?.description\"></p>\r\n                                        <ng-container\r\n                                            *ngIf=\"isShowUpdateDescription && this.taskCheckPermission.isWrite; else updateDescriptionPlaceholder\">\r\n                                            <button type=\"button\" class=\"btn blue cm-mgr-5\"\r\n                                                    (click)=\"saveDescription()\">\r\n                                                Cập nhật\r\n                                            </button>\r\n                                            <button type=\"button\" class=\"btn btn-light\"\r\n                                                    (click)=\"saveDescription(false)\">\r\n                                                Đóng\r\n                                            </button>\r\n                                        </ng-container>\r\n                                    </div>\r\n                                </div>\r\n                                <ng-template #updateDescriptionPlaceholder>\r\n                                    <div class=\"help-text\" (click)=\"isShowUpdateDescription = true\"\r\n                                         *ngIf=\"(!taskDetail.description || !taskDetail.description.trim())\">\r\n                                        Click để nhập mô tả\r\n                                    </div>\r\n                                </ng-template>\r\n                            </div><!-- END: .task-block-content -->\r\n                        </div><!-- END: .task-block -->\r\n                        <div class=\"row task-block center\"\r\n                             *ngIf=\"taskDetail?.status === taskStatus.pendingToFinish && taskDetail.isAllowApprove\">\r\n                            <a href=\"javascript://\"\r\n                               class=\"btn btn-success cm-mgr-5\"\r\n                               [swal]=\"confirmAppApproveTask\"\r\n                               (confirm)=\"approveTask(taskStatus.completed)\">Duyệt</a>\r\n                            <a href=\"javascript://\"\r\n                               class=\"btn btn-danger\"\r\n                               [swal]=\"confirmAppDeclineTask\"\r\n                               (confirm)=\"approveTask(taskStatus.declineFinish)\">Không duyệt</a>\r\n                        </div>\r\n                        <div class=\"row task-block center\" *ngIf=\"taskDetail?.status === taskStatus.completed\">\r\n                            <div class=\"alert alert-success\">Công việc đã hoàn thành.\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"row task-block center\"\r\n                             *ngIf=\"taskDetail?.status === taskStatus.declineFinish\">\r\n                            <div class=\"alert alert-danger\">Công việc bị từ chối hoàn thành.\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"row task-block progress-block\">\r\n                            <h4 class=\"title\">\r\n                                <i class=\"fa fa-list-ul\"></i>\r\n                                Tiến độ thực hiện\r\n                                <span class=\"link-text-muted\" ghmDialogTrigger=\"\"\r\n                                      *ngIf=\"this.taskCheckPermission.isWrite\"\r\n                                      [ghmDialogTriggerFor]=\"taskMethodCalculatorDialog\">\r\n                                                Thiết lập cách tính kết quả\r\n                                </span>\r\n                                <div class=\"progress-update\"\r\n                                     [class.text-decoration-underline]=\"taskDetail?.methodCalculatorResult === methodCalculatorResult.manually\"\r\n                                     [class.cursor-pointer]=\"taskDetail?.methodCalculatorResult === methodCalculatorResult.manually\">\r\n                                                <span class=\"link-textmuted\"\r\n                                                      *ngIf=\"taskDetail?.methodCalculatorResult === methodCalculatorResult.manually && this.taskCheckPermission.isWrite\"\r\n                                                      ghmDialogTrigger=\"\" [ghmDialogTriggerFor]=\"taskUpdateProgress\">\r\n                                                    Cập nhật |\r\n                                                </span>\r\n                                    <span>{{ taskDetail.percentCompleted ? taskDetail.percentCompleted : 0}}%</span>\r\n                                </div>\r\n                            </h4>\r\n                            <div class=\"progress\">\r\n                                <div class=\"progress-bar\" role=\"progressbar\"\r\n                                     [class.progress-bar-success]=\"(taskDetail?.status === taskStatus.inProgress || taskDetail?.status === taskStatus.declineFinish) && taskDetail?.overdueDate <= 0\"\r\n                                     [class.progress-bar-default]=\"taskDetail?.status === taskStatus.notStartYet && taskDetail?.overdueDate <= 0\"\r\n                                     [class.progress-bar-danger]=\"(taskDetail.status === taskStatus.notStartYet || taskDetail.status === taskStatus.inProgress\r\n                                                  || taskDetail?.status === taskStatus.declineFinish) && taskDetail?.overdueDate > 0\"\r\n                                     [class.progress-bar-warning]=\"(taskDetail?.status === taskStatus.pendingToFinish || taskDetail?.status === taskStatus.completed) && taskDetail?.overdueDate > 0\"\r\n                                     [attr.aria-valuenow]=\"taskDetail?.percentCompleted\"\r\n                                     aria-valuemin=\"0\"\r\n                                     aria-valuemax=\"100\"\r\n                                     [ngStyle]=\"{'width': taskDetail?.percentCompleted + '%'}\">\r\n                                                <span\r\n                                                    class=\"sr-only\">{{ taskDetail?.percentCompleted }}% Complete</span>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"task-block-content cm-mgb-20\">\r\n                                <app-ghm-checklist\r\n                                    [allowAdd]=\"taskCheckPermission.isWrite\"\r\n                                    [allowComplete]=\"taskCheckPermission.isWrite\"\r\n                                    [allowEdit]=\"taskCheckPermission.isWrite\"\r\n                                    [allowDelete]=\"taskCheckPermission.isWrite\"\r\n                                    #taskChecklist\r\n                                    [source]=\"taskDetail?.checklists\"\r\n                                    (completeChange)=\"onChecklistCompleteChange($event)\"\r\n                                    (saved)=\"onChecklistSaved($event)\"\r\n                                    (removed)=\"onChecklistRemoved($event)\"></app-ghm-checklist>\r\n                            </div>\r\n                        </div>\r\n                        <!--Công việc con-->\r\n                        <div class=\"row task-block\" *ngIf=\"taskDetail?.type === taskType.combination\">\r\n                            <task-group-tree [listData]=\"taskDetail?.taskChild\"></task-group-tree>\r\n                            <div class=\"drag-drop-box center\" *ngIf=\"taskCheckPermission.isWrite\">\r\n                                <a class=\"color-gray\" (click)=\"addTaskChild()\"><i class=\"fa fa-plus\">\r\n                                    Thêm công việc con</i>\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"row task-block\">\r\n                            <ghm-attachments [listFileAttachment]=\"taskDetail?.attachments\"\r\n                                             [objectId]=\"taskDetail?.id\"\r\n                                             [objectType]=\"objectType\"\r\n                                             [isEnable]=\"this.taskCheckPermission.isWrite\"\r\n                                             (removeFile)=\"removeAttachment($event)\"\r\n                                             (selectFile)=\"selectFileAttachment($event)\"\r\n                            ></ghm-attachments>\r\n                        </div><!-- END: .task-block-content -->\r\n                        <div class=\"row task-block\">\r\n                            <ghm-comment #comment\r\n                                         [objectType]=\"objectType\"\r\n                                         [isEnable]=\"this.taskCheckPermission.isComment\"\r\n                                         [objectId]=\"taskDetail?.id\"\r\n                                         (onSendCommentComplete)=\"this.isModified = true\"\r\n                                         (onRemoveComment)=\"this.isModified = true\"></ghm-comment>\r\n                        </div>\r\n                    </div><!-- END: left column -->\r\n                    <div class=\"col-sm-4 col-md-3 cm-pdr-0\">\r\n                        <div class=\"app-suggest\">\r\n                            <div class=\"header\">Bạn đang xem công việc của</div>\r\n                            <div class=\"media\">\r\n                                <div class=\"media-left\">\r\n                                    <img ghmImage\r\n                                         [src]=\"taskDetail?.responsibleAvatar\"\r\n                                         [errorImageUrl]=\"'/assets/images/noavatar.png'\"\r\n                                         class=\"avatar-sm\"/>\r\n                                </div>\r\n                                <div class=\"media-body\">\r\n                                    <h4 class=\"media-heading\">{{taskDetail?.responsibleFullName}}</h4>\r\n                                    <i>{{taskDetail?.responsiblePositionName}}</i>\r\n                                </div>\r\n                            </div>\r\n                        </div><!-- END: .app-suggest -->\r\n                        <hr>\r\n                        <div class=\"app-suggest participant-box\">\r\n                            <div class=\"header\">{taskDetail?.type, select, 0{Danh sách người tham gia}\r\n                                1{Danh\r\n                                sách người phối hợp}}\r\n                            </div>\r\n                            <ul class=\"media-list list-participants cm-mgb-0\">\r\n                                <li class=\"media participant\" *ngFor=\"let participant of listParticipants\"\r\n                                    ghmDialogTrigger=\"\"\r\n                                    [ghmDialogData]=\"participant\"\r\n                                    [ghmDialogTriggerFor]=\"taskParticipantDialog\">\r\n                                    <div class=\"media-left\">\r\n                                        <a href=\"javascript://\">\r\n                                            <img class=\"media-object\"\r\n                                                 ghmImage\r\n                                                 [src]=\"participant?.avatar\"\r\n                                                 [errorImageUrl]=\"'/assets/images/noavatar.png'\"\r\n                                                 alt=\"{{ participant.fullName }}\">\r\n                                        </a>\r\n                                    </div>\r\n                                    <div class=\"media-body\">\r\n                                        <h4 class=\"media-heading\">{{ participant?.fullName }}</h4>\r\n                                        <a href=\"javascript://\" class=\"participant-config\">\r\n                                            <i class=\"fa fa-gear fa-lg\"></i>\r\n                                        </a>\r\n                                    </div>\r\n                                </li>\r\n                            </ul>\r\n                            <a href=\"javascript://\" *ngIf=\"taskCheckPermission.isWrite\"\r\n                               class=\"add-participant\"\r\n                               ghmDialogTrigger=\"\"\r\n                               [ghmDialogTriggerFor]=\"taskAddParticipantDialog\">\r\n                                {taskDetail?.type, select , 0{ Bạn có muốn thêm người tham gia?} 1{ Bạn có\r\n                                muốn thêm người phối hợp?}}\r\n                            </a>\r\n                        </div><!-- END: .app-suggest -->\r\n                        <!--<hr>-->\r\n                        <!--<div class=\"app-suggest\">-->\r\n                        <!--<div class=\"header\">Nhãn công việc</div>-->\r\n                        <!--<div class=\"help-text\">Click để gán nhãn</div>-->\r\n                        <!--</div>&lt;!&ndash; END: .app-suggest &ndash;&gt;-->\r\n                        <hr>\r\n                        <div class=\"app-suggest\">\r\n                            <div class=\"header\">Thông tin công việc</div>\r\n                            <div class=\"row\"\r\n                                 *ngIf=\"taskDetail?.status === taskStatus.completed || taskDetail?.status === taskStatus.pendingToFinish\">\r\n                                <div class=\"col-sm-5\">Ngày hoàn thành</div>\r\n                                <div class=\"col-sm-7\">\r\n                                    <a href=\"javascript://\"\r\n                                       *ngIf=\"taskCheckPermission.isWrite; else spanCompleteDate\"\r\n                                       class=\"text-decoration-none\" ghmDialogTrigger=\"\"\r\n                                       [ghmDialogTriggerFor]=\"dialogUpdateCompleteDate\">\r\n                                        {{ taskDetail?.endDate | dateTimeFormat : 'DD/MM/YYYY hh:mm' }}\r\n                                    </a>\r\n                                    <ng-template #spanCompleteDate>{{ taskDetail?.endDate | dateTimeFormat :\r\n                                        'DD/MM/YYYY hh:mm' }}\r\n                                    </ng-template>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-sm-5\">Mục tiêu</div>\r\n                                <div class=\"col-sm-7\">\r\n                                    <a href=\"javascript://\" class=\"text-decoration-none\">\r\n                                        {{ taskDetail?.targetName }}\r\n                                    </a>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-sm-5\">Người tạo việc</div>\r\n                                <div class=\"col-sm-7\">\r\n                                    <a href=\"javascript://\" class=\"color-bright-gray text-decoration-none\">{{\r\n                                        taskDetail?.creatorFullName }}</a>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-sm-5\">Ngày tạo việc</div>\r\n                                <div class=\"col-sm-7\">\r\n                                    <a href=\"javascript://\" class=\"color-bright-gray text-decoration-none\">\r\n                                        {{ taskDetail?.createTime | dateTimeFormat:'DD/MM/YYYY' }}\r\n                                    </a>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div><!-- END: right column -->\r\n                </div>\r\n            </div>\r\n            <div class=\"portlet-foot center\">\r\n                <button type=\"button\" class=\"btn btn-light\" (click)=\"closeModal()\">Đóng</button>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</div><!-- END: .task-detail-container -->\r\n\r\n<app-log></app-log>\r\n\r\n<swal\r\n    #confirmCancel\r\n    title=\"Bạn có chắc chắn muốn hủy công việc này không?\"\r\n    text=\"Lưu ý: sau khi hủy bạn không thể lấy lại công việc này.\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<!-- BEGIN: Add participants -->\r\n<ghm-dialog #taskAddParticipantDialog [size]=\"'sm'\"\r\n            [backdropStatic]=\"true\"\r\n            class=\"act-item-text\">\r\n    <ghm-dialog-header>\r\n        <span i18n=\"@@filterTarget\" class=\"cm-pdl-10\">\r\n            {taskDetail?.type, select, 0{Thêm người tham gia} 1 {Thêm người phối hợp}}\r\n        </span>\r\n    </ghm-dialog-header>\r\n    <ghm-dialog-content>\r\n        <div class=\"cm-pd-10 cm-pdt-0\">\r\n            <ghm-select-user-picker #participantSelectPicker [selectUsers]=\"listSelectedUser\"\r\n                                    (itemSelectUser)=\"onParticipantSelected($event)\"\r\n                                    (acceptSelectUsers)=\"onSelectParticipantAccepted($event)\"></ghm-select-user-picker>\r\n        </div>\r\n    </ghm-dialog-content>\r\n</ghm-dialog>\r\n<!-- End: Add participants -->\r\n\r\n<!-- BEGIN: task update progress -->\r\n<ghm-dialog #taskUpdateProgress [size]=\"'sm'\"\r\n            [backdropStatic]=\"true\"\r\n            position=\"center\"\r\n            class=\"act-item-text\"\r\n            (show)=\"onTaskUpdateProgressShow()\">\r\n    <ghm-dialog-header>\r\n        <i class=\"fa fa-filter color-dark-blue\"> </i>\r\n        <span i18n=\"@@filterTarget\"\r\n              class=\"cm-pdl-10\">Cập nhật tiến độ</span>\r\n    </ghm-dialog-header>\r\n    <ghm-dialog-content>\r\n        <div class=\"cm-pd-10\">\r\n            <ghm-input icon=\"fa fa-pencil\"\r\n                       [elementId]=\"'percentCompleted'\"\r\n                       name=\"percentCompleted\"\r\n                       [(ngModel)]=\"completedPercent\"\r\n                       (keyUp.enter)=\"saveProgress()\"\r\n            ></ghm-input>\r\n        </div>\r\n    </ghm-dialog-content>\r\n    <ghm-dialog-footer>\r\n        <div class=\"center\">\r\n            <button class=\"btn btn-sm blue cm-mgr-5\" i18n=\"@@update\"\r\n                    (click)=\"saveProgress()\">\r\n                Cập nhật\r\n            </button>\r\n            <button class=\"btn btn-sm btn-light\" i18n=\"@@close\"\r\n                    ghm-dismiss=\"true\"\r\n                    type=\"button\">\r\n                Đóng\r\n            </button>\r\n        </div>\r\n    </ghm-dialog-footer>\r\n    <!--</form>-->\r\n</ghm-dialog>\r\n<!-- BEGIN: task update progress -->\r\n\r\n<!-- BEGIN: task date -->\r\n<ghm-dialog #taskDateDialog [size]=\"'sm'\"\r\n            [backdropStatic]=\"true\"\r\n            position=\"center\"\r\n            class=\"act-item-text\">\r\n    <ghm-dialog-header>\r\n        <span i18n=\"@@filterTarget\">Cập nhật thời hạn\r\n        </span>\r\n    </ghm-dialog-header>\r\n    <ghm-dialog-content>\r\n        <div class=\"cm-pd-10\">\r\n            <div class=\"form-group\">\r\n                <label ghmLabel=\"Ngày bắt đầu\"></label>\r\n                <div>\r\n                    <nh-date\r\n                        name=\"startDate\"\r\n                        [showTime]=\"true\"\r\n                        [(ngModel)]=\"taskDetail.estimateStartDate\"></nh-date>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label ghmLabel=\"Ngày kết thúc\"></label>\r\n                <div>\r\n                    <nh-date\r\n                        name=\"endDate\"\r\n                        [showTime]=\"true\"\r\n                        [(ngModel)]=\"taskDetail.estimateEndDate\"></nh-date>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </ghm-dialog-content>\r\n    <ghm-dialog-footer>\r\n        <div class=\"center\">\r\n            <button class=\"btn btn-sm blue cm-mgr-5\" i18n=\"@@search\"\r\n                    (click)=\"saveDeadline()\">\r\n                Cập nhật\r\n            </button>\r\n            <button class=\"btn btn-sm btn-light\" i18n=\"@@close\"\r\n                    ghm-dismiss=\"true\"\r\n                    type=\"button\">\r\n                Đóng\r\n            </button>\r\n        </div>\r\n    </ghm-dialog-footer>\r\n    <!--</form>-->\r\n</ghm-dialog>\r\n<!-- BEGIN: end task date -->\r\n\r\n<!-- BEGIN: task actions -->\r\n<ghm-dialog #taskDetailAction [size]=\"'sm'\"\r\n            position=\"center\"\r\n            class=\"act-item-text\">\r\n    <ghm-dialog-content class=\"task-detail-action-container\">\r\n        <ul class=\"task-detail-action\">\r\n            <li (click)=\"configReminder()\" *ngIf=\"taskCheckPermission.isWrite\">\r\n                <div class=\"wrap-img\"><i class=\"fa fa-gear\"></i></div>\r\n                Thiết lập cấu hình nhắc việc\r\n            </li>\r\n            <!--<li (click)=\"sendTaskReminder()\">-->\r\n            <!--<div class=\"wrap-img\"><i class=\"fa fa-envelope\"></i></div>-->\r\n            <!--Gửi lịch công việc-->\r\n            <!--</li>-->\r\n            <!--<li (click)=\"moveTask()\">-->\r\n            <!--<div class=\"wrap-img\"><i class=\"fa fa-scissors\"></i></div>-->\r\n            <!--Chuyển công việc-->\r\n            <!--</li>-->\r\n            <li (click)=\"copy()\">\r\n                <div class=\"wrap-img\"><i class=\"fa fa-files-o\"></i></div>\r\n                Sao chép công việc\r\n            </li>\r\n            <li (click)=\"showHistory()\">\r\n                <div class=\"wrap-img\"><i class=\"fa fa-history\"></i></div>\r\n                Lịch sử công việc\r\n            </li>\r\n            <li [swal]=\"confirmCancel\" *ngIf=\"taskCheckPermission.isWrite\"\r\n                (confirm)=\"cancel()\">\r\n                <div class=\"wrap-img\"><i class=\"fa fa-trash\"></i></div>\r\n                Hủy công việc\r\n            </li>\r\n        </ul>\r\n    </ghm-dialog-content>\r\n</ghm-dialog>\r\n<!-- END: task actions -->\r\n\r\n<!-- BEGIN: task participant actions -->\r\n<ghm-dialog #taskParticipantDialog [size]=\"'sm'\"\r\n            position=\"center\"\r\n            class=\"act-item-text\"\r\n            (show)=\"onConfigRoleDialogShow($event)\">\r\n    <ghm-dialog-header *ngIf=\"taskDetail?.type === taskType.personal\">\r\n        Thiết lập vai trò\r\n    </ghm-dialog-header>\r\n    <ghm-dialog-content *ngIf=\"taskDetail?.type === taskType.personal\">\r\n        <ul class=\"list-task-config-role\">\r\n            <ng-container *ngFor=\"let role of listRoleGroup\">\r\n                <li *ngIf=\"role.id !== 'NGV' && role.id !== 'NNV' && role.id !== 'CVN'\">\r\n                    <mat-checkbox\r\n                        color=\"primary\"\r\n                        name=\"role\"\r\n                        [disabled]=\"!taskCheckPermission.isWrite\"\r\n                        [(ngModel)]=\"role.isSelected\"\r\n                        [checked]=\"role.isSelected\"> {{ role.name }}\r\n                    </mat-checkbox>\r\n                </li>\r\n            </ng-container>\r\n        </ul>\r\n        <div class=\"cm-mgt-10 center cm-mgb-10\">\r\n            <button type=\"button\" class=\"btn blue cm-mgr-5\"\r\n                    [disabled]=\"!taskCheckPermission.isWrite\"\r\n                    (click)=\"saveParticipantRole()\">Cập nhật\r\n            </button>\r\n            <button type=\"button\" (click)=\"dismissTaskParticipantDiaLog()\" class=\"btn btn-light\">Đóng</button>\r\n        </div>\r\n    </ghm-dialog-content>\r\n    <ghm-dialog-footer class=\"center\">\r\n        <div>Loại bỏ nhân viên khỏi danh sách?</div>\r\n        <button class=\"btn red\" (click)=\"removeParticipant()\" [disabled]=\"!taskCheckPermission.isWrite\">Loại bỏ</button>\r\n    </ghm-dialog-footer>\r\n</ghm-dialog>\r\n<!-- END: task participant actions -->\r\n\r\n<!-- BEGIN: task participant actions -->\r\n<ghm-dialog #taskMethodCalculatorDialog [size]=\"'sm'\"\r\n            position=\"center\"\r\n            class=\"act-item-text\"\r\n            (show)=\"onMethodCalculatorDialogShow()\">\r\n    <ghm-dialog-header>\r\n        Thiết lập cách tính kết quả công việc\r\n    </ghm-dialog-header>\r\n    <ghm-dialog-content class=\"task-detail-action-container\">\r\n        <div class=\"list-method-calculator\">\r\n            <mat-radio-group class=\"example-radio-group\">\r\n                <mat-radio-button class=\"example-radio-button\" value=\"{{method.id}}\"\r\n                                  *ngFor=\"let method of listMethodCalculatorResult\"\r\n                                  [checked]=\"method.id === methodCalculator\"\r\n                                  (change)=\"methodCalculator = method.id\"\r\n                                  color=\"primary\">\r\n                    <span>{{method.name}}</span>\r\n                </mat-radio-button>\r\n            </mat-radio-group>\r\n        </div>\r\n    </ghm-dialog-content>\r\n    <ghm-dialog-footer class=\"center\">\r\n        <button type=\"button\" class=\"btn blue cm-mgr-5\" (click)=\"acceptChangeMethodCalculator()\">Cập nhật</button>\r\n        <button type=\"button\" class=\"btn btn-light\" ghm-dismiss=\"\">Đóng</button>\r\n    </ghm-dialog-footer>\r\n</ghm-dialog>\r\n<!-- END: task participant actions -->\r\n<ghm-setting-notification (saveSuccessful)=\"saveSettingNotification($event)\"></ghm-setting-notification>\r\n\r\n<ghm-dialog #dialogUpdateCompleteDate (show)=\"openDialogCompleteDate()\"\r\n            [backdropStatic]=\"true\"\r\n            [position]=\"'center'\">\r\n    <ghm-dialog-header><span i18n=\"@@updateTime\" class=\"cm-pdl-10\">Cập nhập thời gian hoàn thành</span>\r\n    </ghm-dialog-header>\r\n    <ghm-dialog-content>\r\n        <div class=\"form-group\">\r\n            <nh-date [format]=\"'DD/MM/YYYY'\"\r\n                     [(ngModel)]=\"completeDate\"></nh-date>\r\n        </div>\r\n    </ghm-dialog-content>\r\n    <ghm-dialog-footer>\r\n        <div class=\"center\">\r\n            <button class=\"btn btn-sm blue cm-mgr-5\"\r\n                    (click)=\"updateCompleteDate()\"\r\n                    i18n=\"@@update\">\r\n                Cập nhập\r\n            </button>\r\n            <button class=\"btn btn-sm btn-light\" i18n=\"@@close\"\r\n                    ghm-dismiss=\"true\"\r\n                    type=\"button\">\r\n                Đóng\r\n            </button>\r\n        </div>\r\n    </ghm-dialog-footer>\r\n</ghm-dialog>\r\n\r\n<swal\r\n    #confirmAppApproveTask\r\n    i18n=\"@@confirmApproveTask\"\r\n    i18n-title\r\n    i18n-text\r\n    title=\"Bạn có muốn duyệt công việc này?\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<swal\r\n    #confirmAppDeclineTask\r\n    i18n=\"@@confirmDeclineTask\"\r\n    i18n-title\r\n    i18n-text\r\n    title=\"Bạn có chắc chắn không muốn duyệt công việc này hoàn thành?\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<swal #confirmChangeCompleteStatusCheckList\r\n      title=\"Bạn có chắc chắn công việc đã hoàn thành?\"\r\n      text=\"Công việc sẽ được chuyển sang trạng thái chờ duyệt và bạn không thể thay đổi được thông tin trong công việc này nữa.\"\r\n      type=\"question\"\r\n      [showCancelButton]=\"true\"\r\n      [focusCancel]=\"true\">\r\n</swal>\r\n"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-detail/task-detail.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-detail/task-detail.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ghm-dialog-content {\n  padding: 0; }\n  ghm-dialog-content.task-detail-action-container {\n    padding: 0 !important; }\n  ghm-dialog-content ul.task-detail-action {\n    list-style: none;\n    padding-left: 0;\n    margin-bottom: 0; }\n  ghm-dialog-content ul.task-detail-action li {\n      padding: 12px 12px 12px 48px;\n      position: relative;\n      float: left;\n      width: 100%;\n      min-height: 48px;\n      cursor: pointer;\n      color: #0072bc;\n      border-bottom: 1px solid #ddd; }\n  ghm-dialog-content ul.task-detail-action li:hover {\n        background-color: #dff0fd; }\n  ghm-dialog-content ul.task-detail-action li div.wrap-img {\n        position: absolute;\n        top: 0;\n        left: 0;\n        width: 48px;\n        height: 48px; }\n  ghm-dialog-content ul.task-detail-action li div.wrap-img i {\n          font-size: 24px;\n          padding: 15px;\n          display: block; }\n  ghm-dialog-content .list-method-calculator {\n    padding: 15px; }\n  ghm-dialog-content .list-method-calculator mat-radio-group {\n      display: block;\n      width: 100%; }\n  ghm-dialog-content .list-method-calculator mat-radio-group mat-radio-button {\n        display: block;\n        width: 100%;\n        margin-bottom: 5px; }\n  ghm-dialog-content .list-method-calculator mat-radio-group mat-radio-button:last-child {\n          margin-bottom: 0; }\n  ghm-dialog-content .list-task-config-role {\n    list-style: none;\n    padding: 10px;\n    width: 100%; }\n  ghm-dialog-content .list-task-config-role li {\n      display: block;\n      width: 100%; }\n  .task-detail-container .portlet-title {\n  padding: 0 !important;\n  margin-bottom: 0px; }\n  .task-detail-container .portlet-title ul {\n    list-style: none;\n    margin-bottom: 0;\n    padding: 0;\n    display: block;\n    width: 100%; }\n  .task-detail-container .portlet-title ul li {\n      display: inline-block;\n      float: left; }\n  .task-detail-container .portlet-title ul li.icon a {\n        display: block;\n        width: 48px;\n        height: 48px; }\n  .task-detail-container .portlet-title ul li.icon a.btn-close {\n          border-left: 1px solid #ddd; }\n  .task-detail-container .portlet-title ul li h4 {\n        margin-top: 15px; }\n  .task-detail-container .portlet-body {\n  padding-top: 0px; }\n  .task-detail-container .task-name h4, .task-detail-container .task-name textarea {\n  font-size: 23px !important;\n  font-weight: bold; }\n  .task-detail-container [contenteditable=\"true\"] {\n  border: 1px solid #ddd;\n  padding: 10px 0; }\n  .task-detail-container .description {\n  padding: 10px 0; }\n  .task-detail-container .help-text {\n  border: 1px dashed #ddd;\n  text-align: center;\n  padding: 5px 10px;\n  display: block;\n  width: 100%;\n  color: #757575;\n  margin-top: 10px; }\n  .task-detail-container .help-text:hover {\n    cursor: pointer;\n    background: #f5f7f7; }\n  .task-detail-container .app-suggest .help-text:hover {\n  background: white; }\n  .task-detail-container .task-block.progress-block h4.title {\n  font-weight: bold; }\n  .task-detail-container .task-block.progress-block h4.title a {\n    font-weight: normal; }\n  .task-detail-container .checklist {\n  list-style: none;\n  padding-left: 0;\n  display: block;\n  width: 100%; }\n  .task-detail-container .checklist li {\n    border-bottom: 1px solid #ddd;\n    display: block;\n    width: 100%;\n    padding: 5px 0; }\n  .task-detail-container .checklist li:last-child {\n      border-bottom: none !important; }\n  .task-detail-container .checklist li:hover {\n      cursor: pointer;\n      background-color: #dff0fd;\n      color: #0072bc; }\n  .task-detail-container .progress-update {\n  float: right;\n  font-size: 14px; }\n  .task-detail-container .progress {\n  height: 10px;\n  margin-bottom: 5px; }\n  .task-detail-container .list-participants .participant {\n  position: relative;\n  border-bottom: 1px dashed #ddd; }\n  .task-detail-container .list-participants .participant:hover {\n    background: #dff0fd; }\n  .task-detail-container .list-participants .participant:hover .participant-config {\n      opacity: 1; }\n  .task-detail-container .list-participants .participant:last-child {\n    border-bottom: none; }\n  .task-detail-container .list-participants .participant .participant-config {\n    position: absolute;\n    top: 0;\n    right: 0;\n    color: #0072bc;\n    opacity: .8; }\n  .task-detail-container *[imgsrc=\"dlg-working\"] {\n  background: url('working.png') no-repeat center; }\n  .task-detail-container *[imgsrc=\"dlg-close\"] {\n  background: url('dlg-close.png') no-repeat center; }\n  .task-detail-container *[imgsrc=\"dlg-action\"] {\n  background: url('dlg-action.png') no-repeat center; }\n  .task-detail-container .app-suggest.participant-box {\n  padding: 0 !important; }\n  .task-detail-container .app-suggest.participant-box .header {\n    position: relative;\n    float: left;\n    width: 100%;\n    padding: 12px 12px 6px 12px;\n    font-size: 15px;\n    font-weight: 500;\n    color: #27ae60;\n    line-height: 21px;\n    margin-bottom: 0 !important; }\n  .task-detail-container .app-suggest.participant-box ul.list-participants {\n    display: block;\n    width: 100%; }\n  .task-detail-container .app-suggest.participant-box ul.list-participants li {\n      padding: 4px 28px 4px 12px; }\n  .task-detail-container .app-suggest.participant-box ul.list-participants li:hover {\n        background-color: #dff0fd;\n        cursor: pointer; }\n  .task-detail-container .app-suggest.participant-box ul.list-participants li:hover .participant-config {\n          opacity: 1; }\n  .task-detail-container .app-suggest.participant-box ul.list-participants li .participant-config {\n        position: absolute;\n        top: 7px;\n        right: 12px;\n        width: 16px;\n        height: 16px;\n        opacity: .6; }\n  .task-detail-container .app-suggest.participant-box ul.list-participants li .media-left img {\n        display: block;\n        width: 24px;\n        height: 24px; }\n  .task-detail-container .app-suggest.participant-box ul.list-participants li .media-body h4.media-heading {\n        margin-bottom: 0;\n        margin-top: 4px !important; }\n  .task-detail-container .app-suggest.participant-box .add-participant {\n    display: block;\n    width: 100%;\n    padding: 4px 12px;\n    margin-bottom: 10px; }\n  .task-detail-container .app-suggest.participant-box .add-participant:hover {\n      text-decoration: none; }\n  .task-detail-container hr {\n  margin: 0px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC90YXNrLWRldGFpbC9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXBwXFxtb2R1bGVzXFx0YXNrXFx0YXNrLW1hbmFnZW1lbnRcXHRhc2stZGV0YWlsXFx0YXNrLWRldGFpbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHQTtFQUNJLFVBQVUsRUFBQTtFQURkO0lBSVEscUJBQXFCLEVBQUE7RUFKN0I7SUFRUSxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGdCQUFnQixFQUFBO0VBVnhCO01BYVksNEJBQTRCO01BQzVCLGtCQUFrQjtNQUNsQixXQUFXO01BQ1gsV0FBVztNQUNYLGdCQUFnQjtNQUNoQixlQUFlO01BQ2YsY0FBYztNQUNkLDZCQXZCTSxFQUFBO0VBR2xCO1FBdUJnQix5QkFDSixFQUFBO0VBeEJaO1FBMkJnQixrQkFBa0I7UUFDbEIsTUFBTTtRQUNOLE9BQU87UUFDUCxXQUFXO1FBQ1gsWUFBWSxFQUFBO0VBL0I1QjtVQWtDb0IsZUFBZTtVQUNmLGFBQWE7VUFDYixjQUFjLEVBQUE7RUFwQ2xDO0lBMkNRLGFBQWEsRUFBQTtFQTNDckI7TUE4Q1ksY0FBYztNQUNkLFdBQVcsRUFBQTtFQS9DdkI7UUFrRGdCLGNBQWM7UUFDZCxXQUFXO1FBQ1gsa0JBQWtCLEVBQUE7RUFwRGxDO1VBdURvQixnQkFBZ0IsRUFBQTtFQXZEcEM7SUE4RFEsZ0JBQWdCO0lBQ2hCLGFBQWE7SUFDYixXQUFXLEVBQUE7RUFoRW5CO01BbUVZLGNBQWM7TUFDZCxXQUFXLEVBQUE7RUFLdkI7RUFFUSxxQkFBcUI7RUFDckIsa0JBQWtCLEVBQUE7RUFIMUI7SUFLWSxnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLFVBQVU7SUFDVixjQUFjO0lBQ2QsV0FBVyxFQUFBO0VBVHZCO01BWWdCLHFCQUFxQjtNQUNyQixXQUFXLEVBQUE7RUFiM0I7UUFpQndCLGNBQWM7UUFDZCxXQUFXO1FBQ1gsWUFBWSxFQUFBO0VBbkJwQztVQXFCNEIsMkJBakdWLEVBQUE7RUE0RWxCO1FBMkJvQixnQkFBZ0IsRUFBQTtFQTNCcEM7RUFrQ1EsZ0JBQWdCLEVBQUE7RUFsQ3hCO0VBcUNRLDBCQUEwQjtFQUMxQixpQkFBaUIsRUFBQTtFQXRDekI7RUEwQ1Esc0JBdEhVO0VBdUhWLGVBQWUsRUFBQTtFQTNDdkI7RUFnRFEsZUFBZSxFQUFBO0VBaER2QjtFQW9EUSx1QkFoSVU7RUFpSVYsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixjQUFjO0VBQ2QsV0FBVztFQUNYLGNBQWM7RUFDZCxnQkFBZ0IsRUFBQTtFQTFEeEI7SUE2RFksZUFBZTtJQUNmLG1CQUFtQixFQUFBO0VBOUQvQjtFQW9FWSxpQkFBaUIsRUFBQTtFQXBFN0I7RUEyRWdCLGlCQUFpQixFQUFBO0VBM0VqQztJQTZFb0IsbUJBQW1CLEVBQUE7RUE3RXZDO0VBb0ZRLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsY0FBYztFQUNkLFdBQVcsRUFBQTtFQXZGbkI7SUEwRlksNkJBdEtNO0lBdUtOLGNBQWM7SUFDZCxXQUFXO0lBQ1gsY0FBYyxFQUFBO0VBN0YxQjtNQWdHZ0IsOEJBQThCLEVBQUE7RUFoRzlDO01Bb0dnQixlQUFlO01BQ2YseUJBaExPO01BaUxQLGNBQWMsRUFBQTtFQXRHOUI7RUE0R1EsWUFBWTtFQUNaLGVBQWUsRUFBQTtFQTdHdkI7RUFpSFEsWUFBWTtFQUNaLGtCQUFrQixFQUFBO0VBbEgxQjtFQXVIWSxrQkFBa0I7RUFDbEIsOEJBcE1NLEVBQUE7RUE0RWxCO0lBMkhnQixtQkFBbUIsRUFBQTtFQTNIbkM7TUE4SG9CLFVBQVUsRUFBQTtFQTlIOUI7SUFtSWdCLG1CQUFtQixFQUFBO0VBbkluQztJQXVJZ0Isa0JBQWtCO0lBQ2xCLE1BQU07SUFDTixRQUFRO0lBQ1IsY0FBYztJQUNkLFdBQVcsRUFBQTtFQTNJM0I7RUFrSlEsK0NBQTZFLEVBQUE7RUFsSnJGO0VBc0pRLGlEQUErRSxFQUFBO0VBdEp2RjtFQTBKUSxrREFBZ0YsRUFBQTtFQTFKeEY7RUE4SlEscUJBQXFCLEVBQUE7RUE5SjdCO0lBaUtZLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsV0FBVztJQUNYLDJCQUEyQjtJQUMzQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGNBQWM7SUFDZCxpQkFBaUI7SUFDakIsMkJBQTJCLEVBQUE7RUF6S3ZDO0lBNktZLGNBQWM7SUFDZCxXQUFXLEVBQUE7RUE5S3ZCO01BaUxnQiwwQkFBMEIsRUFBQTtFQWpMMUM7UUFvTG9CLHlCQUF5QjtRQUN6QixlQUFlLEVBQUE7RUFyTG5DO1VBd0x3QixVQUFVLEVBQUE7RUF4TGxDO1FBNkxvQixrQkFBa0I7UUFDbEIsUUFBUTtRQUNSLFdBQVc7UUFDWCxXQUFXO1FBQ1gsWUFBWTtRQUNaLFdBQVcsRUFBQTtFQWxNL0I7UUF1TXdCLGNBQWM7UUFDZCxXQUFXO1FBQ1gsWUFBWSxFQUFBO0VBek1wQztRQStNd0IsZ0JBQWdCO1FBQ2hCLDBCQUEwQixFQUFBO0VBaE5sRDtJQXVOWSxjQUFjO0lBQ2QsV0FBVztJQUNYLGlCQUFpQjtJQUNqQixtQkFBbUIsRUFBQTtFQTFOL0I7TUE2TmdCLHFCQUFxQixFQUFBO0VBN05yQztFQW1PUSxzQkFBc0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvdGFzay90YXNrLW1hbmFnZW1lbnQvdGFzay1kZXRhaWwvdGFzay1kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIkYm9yZGVyQ29sb3I6ICNkZGQ7XHJcbiRhY3RpdmVCZ0NvbG9yOiAjZGZmMGZkO1xyXG5cclxuZ2htLWRpYWxvZy1jb250ZW50IHtcclxuICAgIHBhZGRpbmc6IDA7XHJcblxyXG4gICAgJi50YXNrLWRldGFpbC1hY3Rpb24tY29udGFpbmVyIHtcclxuICAgICAgICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgdWwudGFzay1kZXRhaWwtYWN0aW9uIHtcclxuICAgICAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogMDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG5cclxuICAgICAgICBsaSB7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEycHggMTJweCAxMnB4IDQ4cHg7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiA0OHB4O1xyXG4gICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjMDA3MmJjO1xyXG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJGJvcmRlckNvbG9yO1xyXG5cclxuICAgICAgICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGZmMGZkXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGRpdi53cmFwLWltZyB7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgICAgICB0b3A6IDA7XHJcbiAgICAgICAgICAgICAgICBsZWZ0OiAwO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDQ4cHg7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDQ4cHg7XHJcblxyXG4gICAgICAgICAgICAgICAgaSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAyNHB4O1xyXG4gICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmxpc3QtbWV0aG9kLWNhbGN1bGF0b3Ige1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcblxyXG4gICAgICAgIG1hdC1yYWRpby1ncm91cCB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAgICAgICAgIG1hdC1yYWRpby1idXR0b24ge1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuXHJcbiAgICAgICAgICAgICAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmxpc3QtdGFzay1jb25maWctcm9sZSB7XHJcbiAgICAgICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgICAgICBsaSB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi50YXNrLWRldGFpbC1jb250YWluZXIge1xyXG4gICAgLnBvcnRsZXQtdGl0bGUge1xyXG4gICAgICAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgdWwge1xyXG4gICAgICAgICAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgICAgICAgICBsaSB7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuXHJcbiAgICAgICAgICAgICAgICAmLmljb24ge1xyXG4gICAgICAgICAgICAgICAgICAgIGEge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDQ4cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogNDhweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJi5idG4tY2xvc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAkYm9yZGVyQ29sb3I7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaDQge1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLnBvcnRsZXQtYm9keSB7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDBweDtcclxuICAgIH1cclxuICAgIC50YXNrLW5hbWUgaDQsIC50YXNrLW5hbWUgdGV4dGFyZWEge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjNweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgfVxyXG5cclxuICAgIFtjb250ZW50ZWRpdGFibGU9XCJ0cnVlXCJdIHtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAkYm9yZGVyQ29sb3I7XHJcbiAgICAgICAgcGFkZGluZzogMTBweCAwO1xyXG4gICAgfVxyXG5cclxuICAgIC5kZXNjcmlwdGlvbiB7XHJcbiAgICAgICAgLy8gbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgICAgICBwYWRkaW5nOiAxMHB4IDA7XHJcbiAgICB9XHJcblxyXG4gICAgLmhlbHAtdGV4dCB7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggZGFzaGVkICRib3JkZXJDb2xvcjtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgcGFkZGluZzogNXB4IDEwcHg7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgY29sb3I6ICM3NTc1NzU7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuXHJcbiAgICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2Y1ZjdmNztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmFwcC1zdWdnZXN0IHtcclxuICAgICAgICAuaGVscC10ZXh0OmhvdmVyIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC50YXNrLWJsb2NrIHtcclxuICAgICAgICAmLnByb2dyZXNzLWJsb2NrIHtcclxuICAgICAgICAgICAgaDQudGl0bGUge1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgICAgICBhIHtcclxuICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5jaGVja2xpc3Qge1xyXG4gICAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAwO1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgICAgICBsaSB7XHJcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAkYm9yZGVyQ29sb3I7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgcGFkZGluZzogNXB4IDA7XHJcblxyXG4gICAgICAgICAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRhY3RpdmVCZ0NvbG9yO1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICMwMDcyYmM7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLnByb2dyZXNzLXVwZGF0ZSB7XHJcbiAgICAgICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIH1cclxuXHJcbiAgICAucHJvZ3Jlc3Mge1xyXG4gICAgICAgIGhlaWdodDogMTBweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmxpc3QtcGFydGljaXBhbnRzIHtcclxuICAgICAgICAucGFydGljaXBhbnQge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBkYXNoZWQgJGJvcmRlckNvbG9yO1xyXG5cclxuICAgICAgICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZGZmMGZkO1xyXG5cclxuICAgICAgICAgICAgICAgIC5wYXJ0aWNpcGFudC1jb25maWcge1xyXG4gICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDE7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICY6bGFzdC1jaGlsZCB7XHJcbiAgICAgICAgICAgICAgICBib3JkZXItYm90dG9tOiBub25lO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAucGFydGljaXBhbnQtY29uZmlnIHtcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgICAgIHRvcDogMDtcclxuICAgICAgICAgICAgICAgIHJpZ2h0OiAwO1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICMwMDcyYmM7XHJcbiAgICAgICAgICAgICAgICBvcGFjaXR5OiAuODtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAqW2ltZ3NyYz1cImRsZy13b3JraW5nXCJdIHtcclxuICAgICAgICAvL2JhY2tncm91bmQtaW1hZ2U6IHVybCgnc3JjL2Fzc2V0cy9pbWFnZXMvYmFja2dyb3VuZHMvZGVsZXRlLnBuZycpIGNlbnRlciBjZW50ZXIgbm8tcmVwZWF0O1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2Fzc2V0cy9pbWFnZXMvYmFja2dyb3VuZHMvd29ya2luZy5wbmcnKSBuby1yZXBlYXQgY2VudGVyO1xyXG4gICAgfVxyXG5cclxuICAgICpbaW1nc3JjPVwiZGxnLWNsb3NlXCJdIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hc3NldHMvaW1hZ2VzL2JhY2tncm91bmRzL2RsZy1jbG9zZS5wbmcnKSBuby1yZXBlYXQgY2VudGVyO1xyXG4gICAgfVxyXG5cclxuICAgICpbaW1nc3JjPVwiZGxnLWFjdGlvblwiXSB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXNzZXRzL2ltYWdlcy9iYWNrZ3JvdW5kcy9kbGctYWN0aW9uLnBuZycpIG5vLXJlcGVhdCBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLmFwcC1zdWdnZXN0LnBhcnRpY2lwYW50LWJveCB7XHJcbiAgICAgICAgcGFkZGluZzogMCAhaW1wb3J0YW50O1xyXG5cclxuICAgICAgICAuaGVhZGVyIHtcclxuICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEycHggMTJweCA2cHggMTJweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICAgICAgICBjb2xvcjogIzI3YWU2MDtcclxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDIxcHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDAgIWltcG9ydGFudDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHVsLmxpc3QtcGFydGljaXBhbnRzIHtcclxuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgICAgICAgICAgbGkge1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogNHB4IDI4cHggNHB4IDEycHg7XHJcblxyXG4gICAgICAgICAgICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2RmZjBmZDtcclxuICAgICAgICAgICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC5wYXJ0aWNpcGFudC1jb25maWcge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAxO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAucGFydGljaXBhbnQtY29uZmlnIHtcclxuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgICAgICAgICAgdG9wOiA3cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgcmlnaHQ6IDEycHg7XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDE2cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxNnB4O1xyXG4gICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IC42O1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC5tZWRpYS1sZWZ0IHtcclxuICAgICAgICAgICAgICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDI0cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMjRweDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLm1lZGlhLWJvZHkge1xyXG4gICAgICAgICAgICAgICAgICAgIGg0Lm1lZGlhLWhlYWRpbmcge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiA0cHggIWltcG9ydGFudDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5hZGQtcGFydGljaXBhbnQge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDRweCAxMnB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG5cclxuICAgICAgICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaHJ7XHJcbiAgICAgICAgbWFyZ2luOiAwcHggIWltcG9ydGFudDtcclxuICAgIH1cclxufVxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-detail/task-detail.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-detail/task-detail.component.ts ***!
  \***********************************************************************************/
/*! exports provided: TaskDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskDetailComponent", function() { return TaskDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/task.service */ "./src/app/modules/task/task-management/services/task.service.ts");
/* harmony import */ var _shareds_components_ghm_select_user_picker_ghm_select_user_picker_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../shareds/components/ghm-select-user-picker/ghm-select-user-picker.component */ "./src/app/shareds/components/ghm-select-user-picker/ghm-select-user-picker.component.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _task_library_services_task_check_list_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../task-library/services/task-check-list.service */ "./src/app/modules/task/task-management/task-library/services/task-check-list.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _shareds_components_ghm_checklist_ghm_checklist_ghm_checklist_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../shareds/components/ghm-checklist/ghm-checklist/ghm-checklist.component */ "./src/app/shareds/components/ghm-checklist/ghm-checklist/ghm-checklist.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../shareds/components/ghm-dialog/ghm-dialog.component */ "./src/app/shareds/components/ghm-dialog/ghm-dialog.component.ts");
/* harmony import */ var _log_log_log_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../log/log/log.component */ "./src/app/modules/task/log/log/log.component.ts");
/* harmony import */ var _consts_tasks_const__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../consts/tasks.const */ "./src/app/modules/task/task-management/consts/tasks.const.ts");
/* harmony import */ var _task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../task-role-group/permission-task-role-group.const */ "./src/app/modules/task/task-management/task-role-group/permission-task-role-group.const.ts");
/* harmony import */ var _log_log_logs_const__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../log/log/logs.const */ "./src/app/modules/task/log/log/logs.const.ts");
/* harmony import */ var _models_task_check_permission_model__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../models/task-check-permission.model */ "./src/app/modules/task/task-management/models/task-check-permission.model.ts");
/* harmony import */ var _shareds_components_ghm_comment_ghm_comment_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../../../shareds/components/ghm-comment/ghm-comment.component */ "./src/app/shareds/components/ghm-comment/ghm-comment.component.ts");
/* harmony import */ var _shareds_components_nh_user_picker_nh_user_picker_model__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../../../shareds/components/nh-user-picker/nh-user-picker.model */ "./src/app/shareds/components/nh-user-picker/nh-user-picker.model.ts");
/* harmony import */ var _shareds_components_ghm_setting_notification_ghm_setting_notification_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../../../shareds/components/ghm-setting-notification/ghm-setting-notification.component */ "./src/app/shareds/components/ghm-setting-notification/ghm-setting-notification.component.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _task_form_task_form_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../task-form/task-form.component */ "./src/app/modules/task/task-management/task-form/task-form.component.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_21__);
/* harmony import */ var _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @toverux/ngx-sweetalert2 */ "./node_modules/@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");























var TaskDetailComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TaskDetailComponent, _super);
    function TaskDetailComponent(data, dialog, toastr, taskService, utilService) {
        var _this = _super.call(this) || this;
        _this.data = data;
        _this.dialog = dialog;
        _this.toastr = toastr;
        _this.taskService = taskService;
        _this.utilService = utilService;
        _this.taskDetail = {};
        _this.listParticipants = [];
        _this.listStatus = [];
        _this.listRoleGroup = [];
        _this.methodCalculatorResult = _consts_tasks_const__WEBPACK_IMPORTED_MODULE_12__["MethodCalculatorResult"];
        _this.taskStatus = _consts_tasks_const__WEBPACK_IMPORTED_MODULE_12__["TaskStatus"];
        _this.taskType = _consts_tasks_const__WEBPACK_IMPORTED_MODULE_12__["TaskType"];
        _this.objectType = _log_log_logs_const__WEBPACK_IMPORTED_MODULE_14__["ObjectType"].task;
        _this._isShowUpdateName = false;
        _this._isShowUpdateDescription = false;
        _this.taskCheckPermission = new _models_task_check_permission_model__WEBPACK_IMPORTED_MODULE_15__["TaskCheckPermission"]();
        _this.listRoleParticipantsCurrentUser = [];
        _this.listSelectedUser = [];
        _this.listMethodCalculatorResult = [
            { id: _consts_tasks_const__WEBPACK_IMPORTED_MODULE_12__["MethodCalculatorResult"].checkList, name: 'Theo checklist' },
            { id: _consts_tasks_const__WEBPACK_IMPORTED_MODULE_12__["MethodCalculatorResult"].manually, name: 'Tự nhập' },
            { id: _consts_tasks_const__WEBPACK_IMPORTED_MODULE_12__["MethodCalculatorResult"].byChild, name: 'Theo mục tiêu con' }
        ];
        _this.listStatus = _this.taskService.getListTaskStatus();
        return _this;
    }
    TaskDetailComponent_1 = TaskDetailComponent;
    Object.defineProperty(TaskDetailComponent.prototype, "isShowUpdateName", {
        get: function () {
            return this._isShowUpdateName;
        },
        set: function (value) {
            var _this = this;
            this._isShowUpdateName = value;
            setTimeout(function () {
                _this.taskNameElement.nativeElement.focus();
            });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TaskDetailComponent.prototype, "isShowUpdateDescription", {
        get: function () {
            return this._isShowUpdateDescription;
        },
        set: function (value) {
            var _this = this;
            this._isShowUpdateDescription = value;
            setTimeout(function () {
                _this.taskDescriptionElement.nativeElement.focus();
            });
        },
        enumerable: true,
        configurable: true
    });
    TaskDetailComponent.prototype.ngOnInit = function () {
        // this.appService.setupPage(this.pageId.TASK, this.pageId.TASK, 'Quản lý công việc', 'Chi tiết công việc');
        if (this.data && this.data.id) {
            this.getDetail(this.data.id);
        }
    };
    TaskDetailComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.confirmChangeCompleteStatusCheckList.confirm.subscribe(function () {
            _this.changeStatusAfterUpdateCheckList(true);
        });
        this.confirmChangeCompleteStatusCheckList.cancel.subscribe(function () {
            _this.changeStatusAfterUpdateCheckList(false);
        });
    };
    TaskDetailComponent.prototype.onMethodCalculatorDialogShow = function () {
        this.methodCalculator = this.taskDetail.methodCalculatorResult;
        console.log(this.methodCalculator);
    };
    TaskDetailComponent.prototype.onTaskUpdateProgressShow = function () {
        this.utilService.focusElement('percentCompleted');
        this.completedPercent = this.taskDetail.percentCompleted;
    };
    TaskDetailComponent.prototype.onCloseUpdateName = function () {
        this.isShowUpdateName = false;
    };
    TaskDetailComponent.prototype.onParticipantSelected = function (user) {
        var _this = this;
        var participantsExist = lodash__WEBPACK_IMPORTED_MODULE_5__["find"](this.listParticipants, function (participant) {
            return participant.userId === user.id;
        });
        if (participantsExist == null && user.id !== this.taskDetail.creatorId && user.id !== this.taskDetail.responsibleId) {
            var participants_1 = [{
                    userId: user.id,
                    fullName: user.fullName,
                    officeName: user.officeName,
                    positionName: user.positionName,
                    roleGroupIds: lodash__WEBPACK_IMPORTED_MODULE_5__["filter"](this.listRoleGroup, function (roleGroup) {
                        return (roleGroup.role & _task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_13__["PermissionTaskRoleGroup"].default) === _task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_13__["PermissionTaskRoleGroup"].default;
                    }).map(function (roleGroup) {
                        return {
                            id: roleGroup.id,
                            role: roleGroup.role,
                            name: roleGroup.name
                        };
                    })
                }];
            this.taskService.insertParticipants(this.taskDetail.id, participants_1)
                .subscribe(function (result) {
                var _a;
                (_a = _this.taskDetail.participants).push.apply(_a, participants_1);
                _this.taskAddParticipantDialog.dismiss();
                _this.convertUserPicker();
            });
        }
    };
    // Thêm người hàm xóa người phối hợp
    TaskDetailComponent.prototype.onSelectParticipantAccepted = function (data) {
        var _this = this;
        if (data && data.listUserInsert) {
            var listUserInserted = lodash__WEBPACK_IMPORTED_MODULE_5__["filter"](data.listUserInsert, function (user) {
                return user.userId !== _this.taskDetail.creatorId && user.userId !== _this.taskDetail.responsibleId;
            });
            var listUserRemoved = data.listUserRemove;
            // Thêm người phối hợp
            var listNewParticipants_1 = listUserInserted.map(function (user) {
                return {
                    userId: user.id,
                    fullName: user.fullName,
                    officeName: user.description,
                    roleGroupIds: lodash__WEBPACK_IMPORTED_MODULE_5__["filter"](_this.listRoleGroup, function (roleGroup) {
                        return (roleGroup.role & _task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_13__["PermissionTaskRoleGroup"].default) === _task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_13__["PermissionTaskRoleGroup"].default;
                    }).map(function (roleGroup) {
                        return {
                            id: roleGroup.id,
                            role: roleGroup.role,
                            name: roleGroup.name
                        };
                    })
                };
            });
            this.taskService.insertParticipants(this.taskDetail.id, listNewParticipants_1)
                .subscribe(function (result) {
                var _a;
                (_a = _this.taskDetail.participants).push.apply(_a, listNewParticipants_1);
                _this.taskAddParticipantDialog.dismiss();
                _this.convertUserPicker();
            });
        }
    };
    TaskDetailComponent.prototype.onChecklistCompleteChange = function (checklist) {
        var _this = this;
        this.subscribers.updateChecklistStatus = this.taskService.updateChecklistStatus(this.taskDetail.id, checklist.id, checklist.isComplete).subscribe(function (result) {
            if (_this.taskDetail.methodCalculatorResult === _this.methodCalculatorResult.checkList) {
                _this.updateCompletedPercentage();
            }
        });
    };
    TaskDetailComponent.prototype.onChecklistSaved = function (checklist) {
        var _this = this;
        if (checklist.id) {
            this.taskService.updateChecklist(this.taskDetail.id, checklist)
                .subscribe(function (result) {
                _this.toastr.success(result.message);
                _this.isModified = true;
                var checklistInfo = lodash__WEBPACK_IMPORTED_MODULE_5__["find"](_this.taskDetail.checklists, function (cl) {
                    return cl.id === checklist.id;
                });
                if (checklistInfo) {
                    checklistInfo.concurrencyStamp = result.data;
                }
            });
        }
        else {
            this.taskService.insertChecklist(this.taskDetail.id, checklist)
                .subscribe(function (result) {
                _this.toastr.success(result.message);
                var data = result.data;
                checklist.concurrencyStamp = data.concurrencyStamp;
                checklist.id = data.id;
                _this.taskDetail.checklists = _this.taskDetail.checklists.concat([checklist]);
                _this.isModified = true;
                _this.updateCompletedPercentage();
            });
        }
    };
    TaskDetailComponent.prototype.onChecklistRemoved = function (checklistId) {
        var _this = this;
        this.subscribers.deleteChecklist = this.taskService.deleteChecklist(this.taskDetail.id, checklistId)
            .subscribe(function (result) {
            _this.isModified = true;
            _this.updateCompletedPercentage();
        });
    };
    TaskDetailComponent.prototype.onStatusSelected = function (status) {
        this.taskDetail.endDate = this.taskDetail.status === _consts_tasks_const__WEBPACK_IMPORTED_MODULE_12__["TaskStatus"].pendingToFinish ? moment__WEBPACK_IMPORTED_MODULE_21__().format('YYYY/MM/DD HH:mm').toString() : '';
        this.save();
    };
    TaskDetailComponent.prototype.onConfigRoleDialogShow = function (participant) {
        var data = this.taskParticipantDialog.ghmDialogData;
        this.participant = participant;
        lodash__WEBPACK_IMPORTED_MODULE_5__["each"](this.listRoleGroup, function (roleGroup) {
            roleGroup.isSelected = participant.roleGroupIds.map(function (value) { return value.id; }).indexOf(roleGroup.id) > -1;
        });
    };
    TaskDetailComponent.prototype.dismissTaskParticipantDiaLog = function () {
        this.taskParticipantDialog.dismiss();
    };
    TaskDetailComponent.prototype.saveParticipantRole = function () {
        var _this = this;
        var roleGroupIds = lodash__WEBPACK_IMPORTED_MODULE_5__["filter"](this.listRoleGroup, function (roleGroup) {
            return roleGroup.isSelected;
        }).map(function (roleGroup) {
            return {
                id: roleGroup.id,
                name: roleGroup.name,
                role: roleGroup.role
            };
        });
        this.participant.roleGroupIds = roleGroupIds;
        this.taskService.updateParticipant(this.taskDetail.id, this.participant.userId, this.participant)
            .subscribe(function (result) {
            _this.taskParticipantDialog.dismiss();
        });
    };
    TaskDetailComponent.prototype.saveProgress = function () {
        if (this.completedPercent > 100) {
            this.toastr.warning('Tiến độ không được phép lớn hơn 100%');
            this.utilService.focusElement('percentCompleted');
            return;
        }
        this.taskDetail.percentCompleted = +this.completedPercent;
        this.taskDetail.status = this.taskDetail.percentCompleted >= 100 ? _consts_tasks_const__WEBPACK_IMPORTED_MODULE_12__["TaskStatus"].completed : _consts_tasks_const__WEBPACK_IMPORTED_MODULE_12__["TaskStatus"].inProgress;
        this.save();
        this.taskUpdateProgress.dismiss();
    };
    TaskDetailComponent.prototype.saveDescription = function (isSave) {
        if (isSave === void 0) { isSave = true; }
        this.isShowUpdateDescription = false;
        if (isSave && (!this.taskDescriptionElement.nativeElement.innerText
            || !this.taskDescriptionElement.nativeElement.innerText.trim())) {
            this.isShowUpdateDescription = true;
            return;
        }
        if (isSave && this.taskDescriptionElement.nativeElement.innerText === this.taskDetail.description) {
            return;
        }
        if (!isSave) {
            this.taskDescriptionElement.nativeElement.innerHTML = this.taskDetail.description;
        }
        else {
            this.taskDetail.description = this.taskDescriptionElement.nativeElement.innerText.trim();
            this.save();
        }
    };
    TaskDetailComponent.prototype.saveName = function (e, isSave) {
        if (isSave === void 0) { isSave = true; }
        e.preventDefault();
        e.stopPropagation();
        this.isShowUpdateName = false;
        if (isSave && (!this.taskNameElement.nativeElement.innerText || !this.taskNameElement.nativeElement.innerText.trim())) {
            this.isShowUpdateName = true;
            return;
        }
        if (isSave && this.taskNameElement.nativeElement.innerText === this.taskDetail.name) {
            this.isShowUpdateName = true;
            return;
        }
        if (!isSave) {
            this.taskNameElement.nativeElement.innerText = this.taskDetail.name;
        }
        else {
            this.taskDetail.name = this.taskNameElement.nativeElement.innerText.trim();
            this.save();
        }
    };
    TaskDetailComponent.prototype.acceptChangeMethodCalculator = function () {
        this.taskDetail.methodCalculatorResult = this.methodCalculator;
        if (this.taskDetail.methodCalculatorResult === _consts_tasks_const__WEBPACK_IMPORTED_MODULE_12__["MethodCalculatorResult"].checkList) {
            this.updateCompletedPercentage();
        }
        this.save();
        this.taskMethodCalculatorDialog.dismiss();
    };
    TaskDetailComponent.prototype.configReminder = function () {
        this.ghmSettingNotification.taskId = this.taskDetail.id;
        this.ghmSettingNotification.selectedNotification = this.taskDetail.taskNotification;
        this.ghmSettingNotification.url = this.url;
        this.ghmSettingNotification.open();
    };
    TaskDetailComponent.prototype.sendTaskReminder = function () {
        console.log('send reminder');
    };
    TaskDetailComponent.prototype.moveTask = function () {
        console.log('move task');
    };
    TaskDetailComponent.prototype.copy = function () {
        var _this = this;
        var taskFormDialog = this.dialog.open(_task_form_task_form_component__WEBPACK_IMPORTED_MODULE_20__["TaskFormComponent"], {
            id: 'taskFormDialog',
            disableClose: true,
            data: {
                taskDetail: this.taskDetail,
                type: this.taskDetail.type
            }
        });
        this.subscribers.afterTaskFormDialogClosed = taskFormDialog.afterClosed().subscribe(function (result) {
            if (result && result.taskId) {
                _this.dialog.open(TaskDetailComponent_1, {
                    data: { id: result.taskId },
                    id: "taskDetailDialog-" + result.taskId,
                    disableClose: true
                });
            }
        });
    };
    TaskDetailComponent.prototype.showHistory = function () {
        this.logComponent.open(this.taskDetail.id, 1);
    };
    TaskDetailComponent.prototype.cancel = function () {
        var _this = this;
        this.taskService.delete(this.taskDetail.id)
            .subscribe(function (result) {
            _this.isModified = true;
            _this.dialog.getDialogById("taskDetailDialog-" + _this.taskDetail.id).close({ isModified: _this.isModified });
            _this.toastr.success(result.message);
        });
    };
    TaskDetailComponent.prototype.removeParticipant = function () {
        var _this = this;
        this.taskService.removeParticipant(this.taskDetail.id, this.participant.userId)
            .subscribe(function (result) {
            lodash__WEBPACK_IMPORTED_MODULE_5__["remove"](_this.taskDetail.participants, function (participantItem) {
                return _this.participant.userId === participantItem.userId;
            });
            _this.convertUserPicker();
        });
    };
    TaskDetailComponent.prototype.saveDeadline = function () {
        this.taskDateDialog.dismiss();
        this.save();
    };
    TaskDetailComponent.prototype.closeModal = function () {
        this.dialog.getDialogById("taskDetailDialog-" + this.taskDetail.id).close({ isUpdate: this.isModified });
    };
    TaskDetailComponent.prototype.removeAttachment = function (value) {
        var _this = this;
        if (value) {
            this.taskService.deleteAttachment(this.taskDetail.id, value.id).subscribe(function (result) {
                lodash__WEBPACK_IMPORTED_MODULE_5__["remove"](_this.taskDetail.attachments, function (attachment) {
                    return attachment.id === value.id;
                });
            });
        }
    };
    TaskDetailComponent.prototype.selectFileAttachment = function (value) {
        var _this = this;
        if (value) {
            if (value.fileInsert) {
                this.taskService.insertAttachment(this.taskDetail.id, value.fileInsert).subscribe(function (result) {
                    value.fileInsert.id = result.data;
                    _this.taskDetail.attachments.push(value.fileInsert);
                });
            }
            if (value.listFileInsert) {
                this.taskService.insertListAttachment(this.taskDetail.id, value.listFileInsert)
                    .subscribe(function (result) {
                    if (result.data) {
                        lodash__WEBPACK_IMPORTED_MODULE_5__["each"](result.data, function (item) {
                            item.isImage = _this.taskService.checkIsImage(item.extension);
                            _this.taskDetail.attachments.push(item);
                        });
                    }
                });
            }
        }
    };
    TaskDetailComponent.prototype.openDialogCompleteDate = function () {
        this.completeDate = this.taskDetail.endDate;
    };
    TaskDetailComponent.prototype.updateCompleteDate = function () {
        if (!this.completeDate) {
            this.toastr.error('Ngày hoàn thành không được để trống');
            return;
        }
        this.taskDetail.endDate = this.completeDate;
        this.dialogUpdateCompleteDate.dismiss();
        this.save();
    };
    TaskDetailComponent.prototype.addTaskChild = function () {
        var _this = this;
        var taskChildFormDialog = this.dialog.open(_task_form_task_form_component__WEBPACK_IMPORTED_MODULE_20__["TaskFormComponent"], { id: 'taskFormDialog', disableClose: true, data: { taskDetail: this.taskDetail, isChild: true, type: this.taskDetail.type } });
        taskChildFormDialog.afterClosed().subscribe(function () {
            _this.taskService.getTaskChild(_this.taskDetail.id).subscribe(function (result) {
                _this.taskDetail.taskChild = result.items;
            });
        });
    };
    TaskDetailComponent.prototype.getListRoleParticipantsCurrentUser = function () {
        var _this = this;
        var currentUserInParticipant = lodash__WEBPACK_IMPORTED_MODULE_5__["filter"](this.taskDetail.participants, function (item) {
            return item.userId === _this.currentUser.id;
        });
        if (!currentUserInParticipant) {
            return false;
        }
        else {
            lodash__WEBPACK_IMPORTED_MODULE_5__["each"](currentUserInParticipant, function (currentUser) {
                lodash__WEBPACK_IMPORTED_MODULE_5__["each"](currentUser.roleGroupIds, function (role) {
                    _this.listRoleParticipantsCurrentUser.push(role);
                });
            });
        }
    };
    TaskDetailComponent.prototype.checkPermissionRoleParticipants = function (permission) {
        var isHavePermission = false;
        lodash__WEBPACK_IMPORTED_MODULE_5__["each"](this.listRoleParticipantsCurrentUser, function (roleParticipants) {
            if (permission & roleParticipants.role) {
                isHavePermission = true;
            }
        });
        return isHavePermission;
    };
    TaskDetailComponent.prototype.convertUserPicker = function () {
        var _this = this;
        this.listSelectedUser = [];
        lodash__WEBPACK_IMPORTED_MODULE_5__["each"](this.taskDetail.participants, function (item) {
            var userPicker = new _shareds_components_nh_user_picker_nh_user_picker_model__WEBPACK_IMPORTED_MODULE_17__["NhUserPicker"](item.userId, item.fullName, item.avatar, item.officeName + " - " + item.positionName);
            userPicker.isSelected = true;
            if (userPicker.id !== _this.taskDetail.creatorId && userPicker.id !== _this.taskDetail.responsibleId) {
                _this.listSelectedUser.push(userPicker);
            }
        });
        var ownUser = new _shareds_components_nh_user_picker_nh_user_picker_model__WEBPACK_IMPORTED_MODULE_17__["NhUserPicker"](this.taskDetail.creatorId, this.taskDetail.creatorFullName);
        this.listSelectedUser.push(ownUser);
    };
    TaskDetailComponent.prototype.saveSettingNotification = function (event) {
        this.taskDetail.taskNotification = event;
    };
    TaskDetailComponent.prototype.approveTask = function (status) {
        var _this = this;
        this.taskService.updateStatus(this.taskDetail.id, this.taskDetail.concurrencyStamp, status)
            .subscribe(function (result) {
            _this.taskDetail.concurrencyStamp = result.data;
            _this.taskDetail.status = status;
        });
    };
    TaskDetailComponent.prototype.save = function () {
        var _this = this;
        this.taskService
            .update(this.taskDetail.id, {
            name: this.taskDetail.name,
            description: this.taskDetail.description,
            concurrencyStamp: this.taskDetail.concurrencyStamp,
            estimateStartDate: this.taskDetail.estimateStartDate,
            estimateEndDate: this.taskDetail.estimateEndDate,
            endDate: this.taskDetail.endDate,
            status: this.taskDetail.status,
            methodCalculatorResult: this.taskDetail.methodCalculatorResult,
            percentCompleted: this.taskDetail.percentCompleted,
            responsibleId: this.taskDetail.responsibleId
        }).subscribe(function (result) {
            _this.taskDetail.concurrencyStamp = result.data;
            _this.taskDetail.overdueDate = _this.taskDetail.status === _this.taskStatus.notStartYet
                || _this.taskDetail.status === _this.taskStatus.declineFinish
                || _this.taskDetail.status === _this.taskStatus.inProgress ?
                moment__WEBPACK_IMPORTED_MODULE_21__(new Date()).diff(moment__WEBPACK_IMPORTED_MODULE_21__(_this.taskDetail.estimateEndDate), 'days')
                : moment__WEBPACK_IMPORTED_MODULE_21__(_this.taskDetail.endDate).diff(moment__WEBPACK_IMPORTED_MODULE_21__(_this.taskDetail.estimateEndDate), 'days');
            _this.isModified = true;
        });
    };
    TaskDetailComponent.prototype.getDetail = function (id) {
        var _this = this;
        this.subscribers.getDetail = this.taskService.getDetail(id)
            .subscribe(function (taskDetail) {
            _this.taskDetail = taskDetail;
            _this.listRoleGroup = taskDetail.roleGroups;
            _this.getListRoleParticipantsCurrentUser();
            _this.taskCheckPermission.isNotification = _this.checkPermissionRoleParticipants(_task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_13__["PermissionTaskRoleGroup"].notification);
            _this.taskCheckPermission.isComment = _this.checkPermissionRoleParticipants(_task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_13__["PermissionTaskRoleGroup"].disputation);
            _this.taskCheckPermission.isReport = _this.checkPermissionRoleParticipants(_task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_13__["PermissionTaskRoleGroup"].report);
            // Trang thai chờ duyệt và hoàn thành không được sửa công việc
            _this.taskCheckPermission.isWrite = _this.checkPermissionRoleParticipants(_task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_13__["PermissionTaskRoleGroup"].write)
                && taskDetail.status !== _this.taskStatus.completed && taskDetail.status !== _this.taskStatus.pendingToFinish;
            _this.taskCheckPermission.isFull = _this.checkPermissionRoleParticipants(_task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_13__["PermissionTaskRoleGroup"].full);
            _this.listParticipants = taskDetail.participants;
            lodash__WEBPACK_IMPORTED_MODULE_5__["remove"](_this.listParticipants, function (participants) {
                return participants.userId === taskDetail.creatorId || participants.userId === taskDetail.responsibleId;
            });
            _this.convertUserPicker();
            if (_this.taskDetail) {
                setTimeout(function () {
                    _this.commentForm.searchByObjectId(_this.objectType, _this.taskDetail.id);
                    _this.url = 'api/v1/task/tasks/' + _this.taskDetail.id + '/notification';
                }, 200);
            }
        });
    };
    TaskDetailComponent.prototype.updateCompletedPercentage = function () {
        if (this.taskDetail.methodCalculatorResult === _consts_tasks_const__WEBPACK_IMPORTED_MODULE_12__["MethodCalculatorResult"].checkList) {
            var totalCompleted = lodash__WEBPACK_IMPORTED_MODULE_5__["filter"](this.taskDetail.checklists, function (checklist) {
                return checklist.isComplete;
            }).length;
            var totalChecklist = this.taskDetail.checklists.length;
            this.taskDetail.percentCompleted = totalChecklist > 0 ? Math.round(totalCompleted / totalChecklist * 100) : 0;
            if (this.taskDetail.percentCompleted === 100) {
                this.confirmChangeCompleteStatusCheckList.show();
            }
            else {
                this.taskDetail.status = _consts_tasks_const__WEBPACK_IMPORTED_MODULE_12__["TaskStatus"].inProgress;
                this.save();
            }
        }
    };
    TaskDetailComponent.prototype.changeStatusAfterUpdateCheckList = function (isUpdateStatus) {
        if (isUpdateStatus) {
            this.taskDetail.status = this.taskDetail.percentCompleted === 100 ? _consts_tasks_const__WEBPACK_IMPORTED_MODULE_12__["TaskStatus"].pendingToFinish : _consts_tasks_const__WEBPACK_IMPORTED_MODULE_12__["TaskStatus"].inProgress;
            this.taskDetail.endDate = this.taskDetail.percentCompleted === 100 ? moment__WEBPACK_IMPORTED_MODULE_21__().format('YYYY/MM/DD HH:mm') : null;
            this.save();
        }
        else {
            if (this.taskDetail.status !== _consts_tasks_const__WEBPACK_IMPORTED_MODULE_12__["TaskStatus"].inProgress) {
                this.taskDetail.status = _consts_tasks_const__WEBPACK_IMPORTED_MODULE_12__["TaskStatus"].inProgress;
                this.save();
            }
        }
    };
    var TaskDetailComponent_1;
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('confirmChangeCompleteStatusCheckList'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_22__["SwalComponent"])
    ], TaskDetailComponent.prototype, "confirmChangeCompleteStatusCheckList", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('comment'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_comment_ghm_comment_component__WEBPACK_IMPORTED_MODULE_16__["GhmCommentComponent"])
    ], TaskDetailComponent.prototype, "commentForm", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('participantSelectPicker'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_select_user_picker_ghm_select_user_picker_component__WEBPACK_IMPORTED_MODULE_4__["GhmSelectUserPickerComponent"])
    ], TaskDetailComponent.prototype, "participantSelectPicker", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('taskChecklist'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_checklist_ghm_checklist_ghm_checklist_component__WEBPACK_IMPORTED_MODULE_8__["GhmChecklistComponent"])
    ], TaskDetailComponent.prototype, "taskChecklist", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('taskName'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], TaskDetailComponent.prototype, "taskNameElement", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('taskDescription'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], TaskDetailComponent.prototype, "taskDescriptionElement", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('taskDateDialog'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_10__["GhmDialogComponent"])
    ], TaskDetailComponent.prototype, "taskDateDialog", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_log_log_log_component__WEBPACK_IMPORTED_MODULE_11__["LogComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _log_log_log_component__WEBPACK_IMPORTED_MODULE_11__["LogComponent"])
    ], TaskDetailComponent.prototype, "logComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('taskMethodCalculatorDialog'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_10__["GhmDialogComponent"])
    ], TaskDetailComponent.prototype, "taskMethodCalculatorDialog", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('taskUpdateProgress'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_10__["GhmDialogComponent"])
    ], TaskDetailComponent.prototype, "taskUpdateProgress", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('taskParticipantDialog'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_10__["GhmDialogComponent"])
    ], TaskDetailComponent.prototype, "taskParticipantDialog", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('taskAddParticipantDialog'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_10__["GhmDialogComponent"])
    ], TaskDetailComponent.prototype, "taskAddParticipantDialog", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_shareds_components_ghm_setting_notification_ghm_setting_notification_component__WEBPACK_IMPORTED_MODULE_18__["GhmSettingNotificationComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_setting_notification_ghm_setting_notification_component__WEBPACK_IMPORTED_MODULE_18__["GhmSettingNotificationComponent"])
    ], TaskDetailComponent.prototype, "ghmSettingNotification", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('dialogUpdateCompleteDate'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_10__["GhmDialogComponent"])
    ], TaskDetailComponent.prototype, "dialogUpdateCompleteDate", void 0);
    TaskDetailComponent = TaskDetailComponent_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-detail',
            template: __webpack_require__(/*! ./task-detail.component.html */ "./src/app/modules/task/task-management/task-detail/task-detail.component.html"),
            providers: [_task_library_services_task_check_list_service__WEBPACK_IMPORTED_MODULE_6__["TaskCheckListService"]],
            styles: [__webpack_require__(/*! ./task-detail.component.scss */ "./src/app/modules/task/task-management/task-detail/task-detail.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_9__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatDialog"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"],
            _services_task_service__WEBPACK_IMPORTED_MODULE_3__["TaskService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_19__["UtilService"]])
    ], TaskDetailComponent);
    return TaskDetailComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/task/task-management/task-form/task-form.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-form/task-form.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"task-form\" style=\"width: 600px\">\r\n    <div class=\"portlet light bordered cm-mgb-0\">\r\n        <div class=\"portlet-title cm-pd-0\" style=\"padding: 0px !important;\">\r\n            <ul class=\"\">\r\n                <li class=\"icon\">\r\n                    <a imgsrc=\"dlg-working\"></a>\r\n                </li>\r\n                <li>\r\n                    <h4 class=\"bold\">\r\n                        Tạo công việc\r\n                    </h4>\r\n                </li>\r\n                <li class=\"icon pull-right\" (click)=\"closeModal()\">\r\n                    <a href=\"javascript://\" class=\"btn-close\" imgsrc=\"dlg-close\">\r\n                    </a>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n        <ng-container *ngIf=\"insertSuccess; else taskFormTemplate\">\r\n            <div class=\"portlet-body\">\r\n                <div class=\"col-sm-12\">\r\n                    <div class=\"alert alert-success\">\r\n                        <i class=\"fa fa-check-circle\"></i>\r\n                        Chúc mừng bạn đã tạo công việc thành công!\r\n                    </div>\r\n                    <div class=\"alert alert-warning cm-mgt-10\">\r\n                        Bấm vào tên công việc <a href=\"javascript://\" (click)=\"showDetail()\">{{ task.name }}</a> để xem\r\n                        chi tiết công việc vừa\r\n                        tạo.\r\n                        Bấm vào nút Tạo công việc khác bên dưới để tiếp tục tạo công việc.\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"portlet-foot center\">\r\n                <button class=\"btn blue cm-mgr-5\" (click)=\"createTask()\">Tạo công việc khác</button>\r\n                <button class=\"btn btn-light\" [mat-dialog-close]=\"isModified\">Đóng</button>\r\n            </div>\r\n        </ng-container>\r\n        <ng-template #taskFormTemplate>\r\n            <form action=\"\" class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n                <div class=\"portlet-body\">\r\n                    <div class=\"col-sm-12\">\r\n                        <div class=\"form-group\">\r\n                            <div class=\"col-sm-8 col-md-9 col-sm-offset-4 col-md-offset-3\">\r\n                                <mat-radio-group formControlName=\"type\" (change)=\"onTaskTypeChange($event)\">\r\n                                    <mat-radio-button [checked]=\"model.value.type === taskType.personal\"\r\n                                                      class=\"cm-mgr-10\" value=\"1\" color=\"primary\">Công việc cá nhân\r\n                                    </mat-radio-button>\r\n                                    <mat-radio-button [checked]=\"model.value.type === taskType.combination\" value=\"0\"\r\n                                                      color=\"primary\">Công việc nhóm\r\n                                    </mat-radio-button>\r\n                                </mat-radio-group>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group\"\r\n                             [class.has-error]=\"formErrors.responsibleId\">\r\n                            <label ghmLabel=\"Nhân viên\"\r\n                                   class=\"col-sm-4 col-md-3 ghm-label\"\r\n                                   [required]=\"true\"></label>\r\n                            <div class=\"col-sm-8 col-md-9\">\r\n                                <ghm-suggestion-user\r\n                                    [readonly]=\"model.value.type == taskType.personal\"\r\n                                    [selectedItem]=\"model.value.responsibleId ? {id: model.value.responsibleId, name: model.value.responsibleFullName} : null\"\r\n                                    (itemSelected)=\"onResponsibleSelected($event)\"></ghm-suggestion-user>\r\n                                <span class=\"help-block\">\r\n                                {formErrors.responsibleId, select, required {Vui lòng chọn người dùng}}\r\n                            </span>\r\n                                <div class=\"cm-mgt-10 cm-mgb-5\">\r\n                                    <mat-radio-group formControlName=\"targetType\" (change)=\"onTargetTypeChange($event)\">\r\n                                        <mat-radio-button [checked]=\"model.value.targetType === 1\" class=\"cm-mgr-10\"\r\n                                                          value=\"1\" color=\"primary\">Mục tiêu nhân viên\r\n                                        </mat-radio-button>\r\n                                        <mat-radio-button [checked]=\"model.value.targetType === 0\" value=\"0\"\r\n                                                          color=\"primary\">Mục tiêu đơn vị\r\n                                        </mat-radio-button>\r\n                                    </mat-radio-group>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group\"\r\n                             [class.has-error]=\"formErrors.targetId\">\r\n                            <label ghmLabel=\"Mục tiêu\"\r\n                                   class=\"col-sm-4 col-md-3 ghm-label\"\r\n                                   [required]=\"true\"></label>\r\n                            <div class=\"col-sm-8 col-md-9\">\r\n                                <target-suggestion-filter\r\n                                    [type]=\"'button'\"\r\n                                    [label]=\"model.value.targetName\"\r\n                                    [readonly]=\"isAddTaskChild\"\r\n                                    [isGetFinish]=\"false\"\r\n                                    [typeTarget]=\"model.value.targetType\"\r\n                                    (itemSelect)=\"onTargetSelected($event)\"\r\n                                ></target-suggestion-filter>\r\n                                <div class=\"help-block\">\r\n                                    {formErrors.targetId, select, required {Vui lòng chọn mục tiêu}}\r\n                                </div>\r\n                                <!--<div class=\"text-right cm-mgt-5\">-->\r\n                                <!--<a href=\"javascript://\" class=\"text-decoration-none\" (click)=\"addTarget()\">-->\r\n                                <!--<i class=\"fa fa-plus\"></i>-->\r\n                                <!--Tạo mục tiêu-->\r\n                                <!--</a>-->\r\n                                <!--</div>-->\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group\">\r\n                            <div class=\"col-sm-12\">\r\n                                <hr>\r\n                            </div>\r\n                            <div class=\"col-sm-12\">\r\n                                <a class=\"pull-right text-decoration-none\" (click)=\"showTaskLibrary()\">\r\n                                    <i class=\"fa fa-tasks\" aria-hidden=\"true\"></i>\r\n                                    Tạo từ thư viện công việc\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group\"\r\n                             [class.has-error]=\"formErrors.name\">\r\n                            <label ghmLabel=\"Công việc\"\r\n                                   class=\"col-sm-4 col-md-3 ghm-label\"\r\n                                   [required]=\"true\"></label>\r\n                            <div class=\"col-sm-8 col-md-9\">\r\n                                <ghm-input\r\n                                    [icon]=\"'fa fa-pencil'\"\r\n                                    [elementId]=\"'taskName'\"\r\n                                    formControlName=\"name\"></ghm-input>\r\n                                <div class=\"help-block\">\r\n                                    {formErrors.name, select, required {Vui lòng nhập tên công việc} maxlength {Tên công\r\n                                    việc không được phép vượt quá 256 ký tự}}\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group\"\r\n                             [class.has-error]=\"formErrors.startDate\">\r\n                            <label ghmLabel=\"Ngày bắt đầu\"\r\n                                   class=\"col-sm-4 col-md-3 ghm-label\"\r\n                                   [required]=\"true\"></label>\r\n                            <div class=\"col-sm-8 col-md-9\">\r\n                                <nh-date formControlName=\"estimateStartDate\"\r\n                                         [showTime]=\"true\"\r\n                                         [mask]=\"true\"></nh-date>\r\n                                <div class=\"help-block\">\r\n                                    {formErrors.startDate, select, required {Vui lòng chọn ngày bắt đầu}}\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group\"\r\n                             [class.has-error]=\"formErrors.endDate\">\r\n                            <label ghmLabel=\"Hạn hoàn thành\"\r\n                                   class=\"col-sm-4 col-md-3 ghm-label\"\r\n                                   [required]=\"true\"></label>\r\n                            <div class=\"col-sm-8 col-md-9\">\r\n                                <nh-date formControlName=\"estimateEndDate\"\r\n                                         [showTime]=\"true\"\r\n                                         [mask]=\"true\"></nh-date>\r\n                                <div class=\"help-block\">\r\n                                    {formErrors.endDate, select, required {Vui lòng chọn ngày kết thúc}}\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <ng-container *ngIf=\"isExpand\">\r\n                            <div class=\"form-group\"\r\n                                 [class.has-error]=\"formErrors.description\">\r\n                                <label ghmLabel=\"Mô tả\"\r\n                                       class=\"col-sm-4 col-md-3 ghm-label\"></label>\r\n                                <div class=\"col-sm-8 col-md-9\">\r\n                                <textarea class=\"form-control no-resizeable\" rows=\"3\"\r\n                                          formControlName=\"description\"></textarea>\r\n                                    <div class=\"help-block\">\r\n                                        {formErrors.description, select, maxlength {Mô tả công việc không được phép vượt\r\n                                        quá\r\n                                        500 ký tự}}\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"form-group\"\r\n                                 [class.has-error]=\"formErrors.status\">\r\n                                <label ghmLabel=\"Trạng thái\"\r\n                                       class=\"col-sm-4 col-md-3 ghm-label\"></label>\r\n                                <div class=\"col-sm-8 col-md-9\">\r\n                                    <ghm-select\r\n                                        icon=\"fa fa-list\"\r\n                                        [data]=\"listStatus\"\r\n                                        formControlName=\"status\"></ghm-select>\r\n                                    <div class=\"help-block\">\r\n                                        {formErrors.status, select, requried {Vui lòng chọn trạng thái công việc}}\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <mat-expansion-panel *ngIf=\"model.value.type === taskType.personal\">\r\n                                <mat-expansion-panel-header\r\n                                    [collapsedHeight]=\"'40px'\"\r\n                                    [expandedHeight]=\"'40px'\">\r\n                                    <span class=\"title\">{model.value.type, select, 0 {Người tham gia công viêc nhóm} 1 {Người phối hợp}}</span>\r\n                                </mat-expansion-panel-header>\r\n                                <mat-panel-description>\r\n                                    <div>\r\n                                        <div class=\"cm-mgt-10\">\r\n                                            <ghm-select-user-picker #selectUserPicker\r\n                                                                    [selectUsers]=\"listSelectedUsers\"\r\n                                                                    (itemSelectUser)=\"onParticipantSelected($event)\"\r\n                                                                    (acceptSelectUsers)=\"onAcceptSelectedParticipants($event)\"></ghm-select-user-picker>\r\n                                        </div>\r\n                                        <ul class=\"list-participants\">\r\n                                            <li class=\"participant\"\r\n                                                *ngFor=\"let participant of listParticipants; index as i\">\r\n                                                <a href=\"#\" class=\"participant-name\">{{ participant.fullName }}</a>\r\n                                                <a data-icon=\"delete-16\" class=\"pull-right btn-participant-remove\"\r\n                                                   (click)=\"removeParticipants(participant.userId)\"></a>\r\n                                                <ul class=\"list-participant-roles\"\r\n                                                    *ngIf=\"model.value.type === taskType.personal\">\r\n                                                    <li *ngFor=\"let role of participant.roles\">\r\n                                                        <mat-checkbox color=\"primary\" [checked]=\"role.isSelected\"\r\n                                                                      (change)=\"onParticipantRoleChange(participant, role)\">\r\n                                                            {{ role.name }}\r\n                                                        </mat-checkbox>\r\n                                                    </li>\r\n                                                </ul>\r\n                                            </li>\r\n                                        </ul>\r\n                                    </div>\r\n                                </mat-panel-description>\r\n                            </mat-expansion-panel>\r\n                            <div class=\"form-group cm-mgb-10\">\r\n                                <div class=\"col-sm-12 cm-mgb-10\">\r\n                                    <ghm-attachments\r\n                                        [listFileAttachment]=\"listAttachments\"\r\n                                        [objectType]=\"1\"\r\n                                        (selectFile)=\"selectFileAttachment($event)\"></ghm-attachments>\r\n                                </div>\r\n                            </div>\r\n                        </ng-container>\r\n                        <div class=\"form-group cm-mgt-10\">\r\n                            <div class=\"col-sm-8 col-md-9 col-sm-offset-4 col-md-offset-3 \">\r\n                                <a href=\"javascript://\" class=\"text-decoration-none\"\r\n                                   (click)=\"isExpand = !isExpand\">\r\n                                    <ng-container *ngIf=\"!isExpand; else expanedText\">\r\n                                        Thông tin mở rộng&nbsp;<i class=\"fa fa-angle-double-down\"></i>\r\n                                    </ng-container>\r\n                                    <ng-template #expanedText>\r\n                                        Thu gọn&nbsp;<i class=\"fa fa-angle-double-up\"></i>\r\n                                    </ng-template>\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"portlet-foot center\">\r\n                    <button class=\"btn blue cm-mgr-5\">Thực hiện</button>\r\n                    <button type=\"button\" class=\"btn btn-light\" [mat-dialog-close]=\"isModified\">Đóng</button>\r\n                </div>\r\n            </form>\r\n        </ng-template><!-- END: taskFormTemplate -->\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-form/task-form.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-form/task-form.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".portlet .portlet-title {\n  padding: 0 !important;\n  margin-bottom: 0px; }\n  .portlet .portlet-title ul {\n    list-style: none;\n    margin-bottom: 0;\n    padding: 0;\n    display: block;\n    width: 100%; }\n  .portlet .portlet-title ul li {\n      display: inline-block;\n      float: left; }\n  .portlet .portlet-title ul li.icon a {\n        display: block;\n        width: 48px;\n        height: 48px; }\n  .portlet .portlet-title ul li.icon a.btn-close {\n          border-left: 1px solid #ddd; }\n  .portlet .portlet-title ul li h4 {\n        margin-top: 15px; }\n  .task-form mat-panel-description {\n  margin-right: 0; }\n  .task-form mat-panel-description > div {\n    flex-shrink: 0;\n    flex-basis: 100%; }\n  .task-form *[imgsrc=\"dlg-working\"] {\n  background: url('working.png') no-repeat center; }\n  .task-form *[imgsrc=\"dlg-close\"] {\n  background: url('dlg-close.png') no-repeat center; }\n  .task-form ul {\n  list-style: none;\n  padding-left: 0;\n  margin-bottom: 0;\n  display: block;\n  width: 100%; }\n  .task-form ul li.participant {\n    position: relative;\n    background: #dff0fd;\n    padding-left: 12px;\n    padding-right: 12px;\n    margin-bottom: 10px;\n    display: block;\n    width: 100%; }\n  .task-form ul li.participant a.participant-name {\n      font-weight: bold;\n      padding-top: 10px;\n      padding-bottom: 10px;\n      display: block; }\n  .task-form ul li.participant a.participant-name:hover {\n        text-decoration: none; }\n  .task-form ul li.participant ul.list-participants li {\n      display: block;\n      margin-bottom: 10px; }\n  .task-form ul li.participant .btn-participant-remove {\n      position: absolute;\n      top: 10px;\n      right: 10px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC90YXNrLWZvcm0vRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFwcFxcbW9kdWxlc1xcdGFza1xcdGFzay1tYW5hZ2VtZW50XFx0YXNrLWZvcm1cXHRhc2stZm9ybS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUVRLHFCQUFxQjtFQUNyQixrQkFBa0IsRUFBQTtFQUgxQjtJQUtZLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsVUFBVTtJQUNWLGNBQWM7SUFDZCxXQUFXLEVBQUE7RUFUdkI7TUFZZ0IscUJBQXFCO01BQ3JCLFdBQVcsRUFBQTtFQWIzQjtRQWlCd0IsY0FBYztRQUNkLFdBQVc7UUFDWCxZQUFZLEVBQUE7RUFuQnBDO1VBcUI0QiwyQkF2QlYsRUFBQTtFQUVsQjtRQTJCb0IsZ0JBQWdCLEVBQUE7RUFPcEM7RUFFUSxlQUFlLEVBQUE7RUFGdkI7SUFLWSxjQUFjO0lBQ2QsZ0JBQWdCLEVBQUE7RUFONUI7RUFZUSwrQ0FBNkUsRUFBQTtFQVpyRjtFQWdCUSxpREFBK0UsRUFBQTtFQWhCdkY7RUFvQlEsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLFdBQVcsRUFBQTtFQXhCbkI7SUEyQlksa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixjQUFjO0lBQ2QsV0FBVyxFQUFBO0VBakN2QjtNQW9DZ0IsaUJBQWlCO01BQ2pCLGlCQUFpQjtNQUNqQixvQkFBb0I7TUFDcEIsY0FBYyxFQUFBO0VBdkM5QjtRQTBDb0IscUJBQXFCLEVBQUE7RUExQ3pDO01BZ0RvQixjQUFjO01BQ2QsbUJBQW1CLEVBQUE7RUFqRHZDO01Bc0RnQixrQkFBa0I7TUFDbEIsU0FBUztNQUNULFdBQVcsRUFBQSIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvdGFzay90YXNrLW1hbmFnZW1lbnQvdGFzay1mb3JtL3Rhc2stZm9ybS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiRib3JkZXJDb2xvcjogI2RkZDtcclxuJGFjdGl2ZUJnQ29sb3I6ICNkZmYwZmQ7XHJcbi5wb3J0bGV0IHtcclxuICAgIC5wb3J0bGV0LXRpdGxlIHtcclxuICAgICAgICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIHVsIHtcclxuICAgICAgICAgICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgICAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgICAgICAgICAgbGkge1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcblxyXG4gICAgICAgICAgICAgICAgJi5pY29uIHtcclxuICAgICAgICAgICAgICAgICAgICBhIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiA0OHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDQ4cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICYuYnRuLWNsb3NlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgJGJvcmRlckNvbG9yO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGg0IHtcclxuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4udGFzay1mb3JtIHtcclxuICAgIG1hdC1wYW5lbC1kZXNjcmlwdGlvbiB7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAwO1xyXG5cclxuICAgICAgICAmID4gZGl2IHtcclxuICAgICAgICAgICAgZmxleC1zaHJpbms6IDA7XHJcbiAgICAgICAgICAgIGZsZXgtYmFzaXM6IDEwMCU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgICpbaW1nc3JjPVwiZGxnLXdvcmtpbmdcIl0ge1xyXG4gICAgICAgIC8vYmFja2dyb3VuZC1pbWFnZTogdXJsKCdzcmMvYXNzZXRzL2ltYWdlcy9iYWNrZ3JvdW5kcy9kZWxldGUucG5nJykgY2VudGVyIGNlbnRlciBuby1yZXBlYXQ7XHJcbiAgICAgICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXNzZXRzL2ltYWdlcy9iYWNrZ3JvdW5kcy93b3JraW5nLnBuZycpIG5vLXJlcGVhdCBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgKltpbWdzcmM9XCJkbGctY2xvc2VcIl0ge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2Fzc2V0cy9pbWFnZXMvYmFja2dyb3VuZHMvZGxnLWNsb3NlLnBuZycpIG5vLXJlcGVhdCBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgdWwge1xyXG4gICAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAwO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgICAgIGxpLnBhcnRpY2lwYW50IHtcclxuICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZGZmMGZkO1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDEycHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDEycHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAgICAgICAgIGEucGFydGljaXBhbnQtbmFtZSB7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuXHJcbiAgICAgICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHVsLmxpc3QtcGFydGljaXBhbnRzIHtcclxuICAgICAgICAgICAgICAgIGxpIHtcclxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAuYnRuLXBhcnRpY2lwYW50LXJlbW92ZSB7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgICAgICB0b3A6IDEwcHg7XHJcbiAgICAgICAgICAgICAgICByaWdodDogMTBweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-form/task-form.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-form/task-form.component.ts ***!
  \*******************************************************************************/
/*! exports provided: TaskFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskFormComponent", function() { return TaskFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/task.service */ "./src/app/modules/task/task-management/services/task.service.ts");
/* harmony import */ var _task_role_group_models_task_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../task-role-group/models/task.model */ "./src/app/modules/task/task-management/task-role-group/models/task.model.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _models_task_participant_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../models/task-participant.model */ "./src/app/modules/task/task-management/models/task-participant.model.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _task_role_group_task_role_group_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../task-role-group/task-role-group.service */ "./src/app/modules/task/task-management/task-role-group/task-role-group.service.ts");
/* harmony import */ var _task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../task-role-group/permission-task-role-group.const */ "./src/app/modules/task/task-management/task-role-group/permission-task-role-group.const.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _task_library_task_library_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../task-library/task-library.component */ "./src/app/modules/task/task-management/task-library/task-library.component.ts");
/* harmony import */ var _shareds_components_nh_user_picker_nh_user_picker_model__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../../shareds/components/nh-user-picker/nh-user-picker.model */ "./src/app/shareds/components/nh-user-picker/nh-user-picker.model.ts");
/* harmony import */ var _consts_tasks_const__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../consts/tasks.const */ "./src/app/modules/task/task-management/consts/tasks.const.ts");
















var TaskFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TaskFormComponent, _super);
    function TaskFormComponent(data, dialog, utilService, toastr, taskRoleGroupService, taskService) {
        var _this = _super.call(this) || this;
        _this.data = data;
        _this.dialog = dialog;
        _this.utilService = utilService;
        _this.toastr = toastr;
        _this.taskRoleGroupService = taskRoleGroupService;
        _this.taskService = taskService;
        _this.task = new _task_role_group_models_task_model__WEBPACK_IMPORTED_MODULE_4__["Task"]();
        _this.isExpand = false;
        _this.listStatus = [];
        _this.listRoleGroup = [];
        _this.listParticipants = [];
        _this.listAttachments = [];
        _this.insertSuccess = false;
        _this.listSelectedUsers = [];
        _this.taskType = _consts_tasks_const__WEBPACK_IMPORTED_MODULE_15__["TaskType"];
        _this.listStatus = _this.taskService.getListTaskStatus();
        return _this;
    }
    Object.defineProperty(TaskFormComponent.prototype, "participants", {
        get: function () {
            return this.model.get('participants');
        },
        enumerable: true,
        configurable: true
    });
    TaskFormComponent.prototype.ngOnInit = function () {
        this.buildForm();
        if (this.data) {
            this.model.patchValue({ type: this.data.type });
            if (this.data.taskDetail) {
                if (this.data.isChild) {
                    this.isAddTaskChild = true;
                    this.model.patchValue({
                        parentId: this.data.taskDetail.id,
                        targetId: this.data.taskDetail.targetId,
                        targetName: this.data.taskDetail.targetName
                    });
                }
                else {
                    this.model.patchValue(this.data.taskDetail);
                }
            }
        }
        this.utilService.focusElement('taskName');
        this.getListRoleGroup();
    };
    TaskFormComponent.prototype.onParticipantSelected = function (user) {
        if (user.id === this.currentUser.id || user.id === this.model.value.responsibleId) {
            this.toastr.warning('Bạn không thể chọn người tạo công việc hoặc người phụ trách');
            return;
        }
        var participant = lodash__WEBPACK_IMPORTED_MODULE_7__["find"](this.listParticipants, function (item) {
            return user.id === item.userId;
        });
        if (participant) {
            this.toastr.warning('Người phối hợp đã được chọn.');
            return;
        }
        this.listParticipants = this.listParticipants.concat([{
                userId: user.id,
                fullName: user.fullName,
                roles: this.listRoleGroup.map(function (roleGroup) {
                    return {
                        id: roleGroup.id,
                        name: roleGroup.name,
                        isSelected: (_task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_10__["PermissionTaskRoleGroup"].default & roleGroup.role) === _task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_10__["PermissionTaskRoleGroup"].default,
                    };
                }),
                roleGroupIds: this.listRoleGroup.map(function (item) {
                    if ((item.role & _task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_10__["PermissionTaskRoleGroup"].default) === _task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_10__["PermissionTaskRoleGroup"].default) {
                        return { id: item.id, name: item.name, role: item.role };
                    }
                })
            }]);
        var userPicker = new _shareds_components_nh_user_picker_nh_user_picker_model__WEBPACK_IMPORTED_MODULE_14__["NhUserPicker"](user.id, user.fullName, user.avatar, user.officeName + " - " + user.positionName);
        this.insertUserSelect(userPicker);
    };
    TaskFormComponent.prototype.onAcceptSelectedParticipants = function (value) {
        var _this = this;
        if (!this.listSelectedUsers || this.listSelectedUsers.length === 0) {
            this.listSelectedUsers = value.listUser;
            this.convertListUserPickerToUserParticipants();
        }
        else {
            if (value.listUserInsert && value.listUserInsert.length > 0) {
                lodash__WEBPACK_IMPORTED_MODULE_7__["each"](value.listUserInsert, function (userInsert) {
                    var userOwnerId = _this.model.value.userId;
                    if (userInsert.id !== userOwnerId && userInsert.id !== _this.currentUser.id) {
                        _this.insertParticipants(userInsert);
                        _this.insertUserSelect(userInsert);
                    }
                });
            }
            if (value.listUserRemove && value.listUserRemove.length > 0) {
                lodash__WEBPACK_IMPORTED_MODULE_7__["each"](value.listUserRemove, function (userRemove) {
                    _this.removeParticipants(userRemove.id.toString());
                });
            }
        }
    };
    TaskFormComponent.prototype.removeParticipants = function (userId) {
        lodash__WEBPACK_IMPORTED_MODULE_7__["remove"](this.listParticipants, function (item) {
            return item.userId === userId;
        });
        lodash__WEBPACK_IMPORTED_MODULE_7__["remove"](this.listSelectedUsers, function (userSelect) {
            return userSelect.id === userId;
        });
    };
    TaskFormComponent.prototype.onResponsibleSelected = function (user) {
        if (user) {
            this.model.patchValue({
                responsibleId: user.id,
                responsibleFullName: user.name
            });
        }
        else {
            this.model.patchValue({
                responsibleId: null,
                responsibleFullName: null
            });
        }
        this.searchTargetByUser();
    };
    TaskFormComponent.prototype.onTargetTypeChange = function (data) {
        this.model.patchValue({
            targetId: null,
            targetName: null
        });
    };
    TaskFormComponent.prototype.onTaskTypeChange = function (data) {
        if (data) {
            this.model.patchValue({ type: data.value });
            if (data.value === this.taskType.personal.toString()) {
                this.model.patchValue({
                    responsibleId: this.currentUser.id,
                    responsibleFullName: this.currentUser.fullName
                });
            }
        }
    };
    TaskFormComponent.prototype.onTargetSelected = function (data) {
        if (data && data.id) {
            this.model.patchValue({
                targetId: data.id,
                targetName: data.name
            });
        }
        else {
            this.model.patchValue({
                targetId: null,
                targetName: null
            });
        }
    };
    TaskFormComponent.prototype.onParticipantRoleChange = function (participant, role) {
        role.isSelected = !role.isSelected;
        participant.roleGroupIds = participant.roles.map(function (roleGroup) {
            if (roleGroup.isSelected) {
                return { id: roleGroup.id, name: roleGroup.name, role: roleGroup.name };
            }
        }).filter(function (roleGroup) {
            return roleGroup.id;
        });
    };
    TaskFormComponent.prototype.createTask = function () {
        this.insertSuccess = false;
        this.listParticipants = [];
        this.utilService.focusElement('taskName');
        this.model.patchValue({
            targetId: null,
            targetNumber: null,
            name: '',
            description: '',
        });
    };
    TaskFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.validateModel();
        if (isValid) {
            this.task = this.model.value;
            this.task.participants = this.listParticipants;
            this.task.attachments = this.listAttachments;
            this.taskService.insert(this.task)
                .subscribe(function (result) {
                _this.insertSuccess = true;
                _this.taskId = result.data;
                _this.isModified = true;
                if (_this.isAddTaskChild) {
                    _this.closeModal();
                }
            });
        }
    };
    TaskFormComponent.prototype.showDetail = function () {
        this.dialog.getDialogById('taskFormDialog').close({ taskId: this.taskId });
    };
    TaskFormComponent.prototype.closeModal = function () {
        this.dialog.getDialogById('taskFormDialog').close();
    };
    TaskFormComponent.prototype.showTaskLibrary = function () {
        var _this = this;
        var taskLibraryDialog = this.dialog.open(_task_library_task_library_component__WEBPACK_IMPORTED_MODULE_13__["TaskLibraryComponent"], {
            id: "taskLibrary",
            data: { isSelect: true }
        });
        taskLibraryDialog.afterClosed()
            .subscribe(function (data) {
            if (data && data.taskLibrary) {
                _this.utilService.focusElement('taskName');
                var taskLibrary = data.taskLibrary;
                _this.model.patchValue({ name: taskLibrary.name, description: taskLibrary.description });
            }
        });
    };
    TaskFormComponent.prototype.selectFileAttachment = function (value) {
        var _this = this;
        if (value) {
            if (value.listFileInsert) {
                lodash__WEBPACK_IMPORTED_MODULE_7__["each"](value.listFileInsert, function (file) {
                    _this.listAttachments.push(file);
                });
            }
            if (value.fileInsert) {
                this.listAttachments.push(value.fileInsert);
            }
        }
    };
    TaskFormComponent.prototype.convertListUserPickerToUserParticipants = function () {
        var _this = this;
        this.listParticipants = [];
        lodash__WEBPACK_IMPORTED_MODULE_7__["each"](this.listSelectedUsers, function (userPicker) {
            _this.insertParticipants(userPicker);
        });
    };
    TaskFormComponent.prototype.insertParticipants = function (user) {
        var userParticipantInfo = lodash__WEBPACK_IMPORTED_MODULE_7__["find"](this.listParticipants, function (participant) {
            return participant.userId === user.id;
        });
        if (!userParticipantInfo) {
            var participants = new _models_task_participant_model__WEBPACK_IMPORTED_MODULE_6__["TaskParticipant"]();
            participants.userId = user.id ? user.id.toString() : '';
            participants.fullName = user.fullName;
            participants.roles = this.listRoleGroup.map(function (roleGroup) {
                return {
                    id: roleGroup.id,
                    name: roleGroup.name,
                    isSelected: (_task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_10__["PermissionTaskRoleGroup"].default & roleGroup.role) === _task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_10__["PermissionTaskRoleGroup"].default,
                };
            });
            participants.roleGroupIds = this.listRoleGroup.map(function (item) {
                if ((item.role & _task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_10__["PermissionTaskRoleGroup"].default) === _task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_10__["PermissionTaskRoleGroup"].default) {
                    return { id: item.id, name: item.name, role: item.role };
                }
            });
            this.listParticipants.push(participants);
        }
    };
    TaskFormComponent.prototype.insertUserSelect = function (userPicker) {
        var userInfo = lodash__WEBPACK_IMPORTED_MODULE_7__["find"](this.listSelectedUsers, function (item) {
            return item.id === userPicker.id;
        });
        if (!userInfo) {
            this.listSelectedUsers.push(userPicker);
        }
    };
    TaskFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.renderFormError(['responsibleId', 'targetId', 'name', 'estimateStartDate',
            'estimateEndDate', 'description', 'status', 'type']);
        this.validationMessages = this.renderFormErrorMessage([
            { 'responsibleId': ['required'] },
            { 'targetId': ['required'] },
            { 'name': ['required', 'maxlength'] },
            { 'estimateStartDate': ['required'] },
            { 'estimateEndDate': ['required'] },
            { 'description': ['maxlength'] },
            { 'status': ['required'] },
            { 'type': ['required'] }
        ]);
        this.model = this.formBuilder.group({
            responsibleId: [this.currentUser.id, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required
                ]],
            responsibleFullName: [this.currentUser.fullName],
            targetId: [this.task.targetId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required
                ]],
            targetType: [this.task.targetType],
            targetName: [this.task.targetName],
            name: [this.task.name, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].maxLength(256)
                ]],
            estimateStartDate: [this.task.estimateStartDate, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required
                ]],
            estimateEndDate: [this.task.estimateEndDate, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required
                ]],
            description: [this.task.description, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].maxLength(500)
                ]],
            status: [this.task.status, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required
                ]],
            type: [this.task.type, [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]],
            parentId: [this.task.parentId]
        });
        this.model.valueChanges.subscribe(function () { return _this.validateModel(false); });
    };
    TaskFormComponent.prototype.searchTargetByUser = function () {
    };
    TaskFormComponent.prototype.getListRoleGroup = function () {
        var _this = this;
        this.taskRoleGroupService.searchForSelect()
            .subscribe(function (result) {
            _this.listRoleGroup = result;
        });
    };
    TaskFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-form',
            template: __webpack_require__(/*! ./task-form.component.html */ "./src/app/modules/task/task-management/task-form/task-form.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./task-form.component.scss */ "./src/app/modules/task/task-management/task-form/task-form.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_11__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatDialog"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_12__["UtilService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_8__["ToastrService"],
            _task_role_group_task_role_group_service__WEBPACK_IMPORTED_MODULE_9__["TaskRoleGroupService"],
            _services_task_service__WEBPACK_IMPORTED_MODULE_3__["TaskService"]])
    ], TaskFormComponent);
    return TaskFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/task/task-management/task-library/models/check-list.model.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-library/models/check-list.model.ts ***!
  \**************************************************************************************/
/*! exports provided: CheckList */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckList", function() { return CheckList; });
var CheckList = /** @class */ (function () {
    function CheckList(id, taskId, taskName, name, type, status, concurrencyStamp) {
        this.id = id;
        this.taskId = taskId;
        this.taskName = taskName;
        this.name = name;
        this.type = type;
        this.status = status;
        this.concurrencyStamp = concurrencyStamp;
    }
    return CheckList;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-library/models/check-list.viewmodel.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-library/models/check-list.viewmodel.ts ***!
  \******************************************************************************************/
/*! exports provided: CheckListViewModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckListViewModel", function() { return CheckListViewModel; });
var CheckListViewModel = /** @class */ (function () {
    function CheckListViewModel(id, taskId, taskName, name, type, status, concurrencyStamp) {
        this.id = id;
        this.taskId = taskId;
        this.taskName = taskName;
        this.name = name;
        this.type = type;
        this.status = status;
        this.concurrencyStamp = concurrencyStamp;
    }
    return CheckListViewModel;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-library/models/task-group-library.model.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-library/models/task-group-library.model.ts ***!
  \**********************************************************************************************/
/*! exports provided: TaskGroupLibrary */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskGroupLibrary", function() { return TaskGroupLibrary; });
var TaskGroupLibrary = /** @class */ (function () {
    function TaskGroupLibrary(id, name, description, concurrencyStamp) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.concurrencyStamp = concurrencyStamp;
    }
    return TaskGroupLibrary;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-library/models/task-library.model.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-library/models/task-library.model.ts ***!
  \****************************************************************************************/
/*! exports provided: TaskLibrary */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskLibrary", function() { return TaskLibrary; });
var TaskLibrary = /** @class */ (function () {
    function TaskLibrary(id, name, description, concurrencyStamp, methodCalculateResult, taskGroupLibraryId, taskGroupLibraryName) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.methodCalculateResult = methodCalculateResult;
        this.taskGroupLibraryId = taskGroupLibraryId;
        this.concurrencyStamp = concurrencyStamp;
        this.taskGroupLibraryName = taskGroupLibraryName;
    }
    return TaskLibrary;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-library/models/task-library.viewmodel.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-library/models/task-library.viewmodel.ts ***!
  \********************************************************************************************/
/*! exports provided: TaskLibraryViewModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskLibraryViewModel", function() { return TaskLibraryViewModel; });
var TaskLibraryViewModel = /** @class */ (function () {
    function TaskLibraryViewModel(id, name, description, concurrencyStamp, methodCalculateResult, taskGroupLibraryId, taskGroupLibraryName) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.methodCalculateResult = methodCalculateResult;
        this.taskGroupLibraryId = taskGroupLibraryId;
        this.concurrencyStamp = concurrencyStamp;
        this.taskGroupLibraryName = taskGroupLibraryName;
        this.isShowCheckList = false;
        this.checkLists = [];
    }
    return TaskLibraryViewModel;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-library/services/task-check-list.service.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-library/services/task-check-list.service.ts ***!
  \***********************************************************************************************/
/*! exports provided: TaskCheckListService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskCheckListService", function() { return TaskCheckListService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../../environments/environment */ "./src/environments/environment.ts");








var TaskCheckListService = /** @class */ (function () {
    function TaskCheckListService(appConfig, http, spinnerService, toastrService) {
        this.http = http;
        this.spinnerService = spinnerService;
        this.toastrService = toastrService;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].apiGatewayUrl + "api/v1/task/checklists";
    }
    TaskCheckListService.prototype.insert = function (checkList) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post("" + this.url, checkList).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastrService.success(result.message);
            return result;
        }));
    };
    TaskCheckListService.prototype.update = function (id, checkList) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/" + id, checkList).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastrService.success(result.message);
            return result;
        }));
    };
    TaskCheckListService.prototype.complete = function (id, isComplete) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/" + id + "/complete/" + isComplete, {})
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastrService.success(result.message);
            return result;
        }));
    };
    TaskCheckListService.prototype.delete = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.delete(this.url + "/" + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastrService.success(result.message);
            return result;
        }));
    };
    TaskCheckListService.prototype.searchByTaskType = function (taskTypeId, type) {
        // this.spinnerService.show();
        return this.http.get(this.url + "/" + taskTypeId + "/" + type)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () {
            // this.spinnerService.hide();
        }));
    };
    TaskCheckListService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__["SpinnerService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"]])
    ], TaskCheckListService);
    return TaskCheckListService;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-library/services/task-library.service.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-library/services/task-library.service.ts ***!
  \********************************************************************************************/
/*! exports provided: TaskLibraryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskLibraryService", function() { return TaskLibraryService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../../environments/environment */ "./src/environments/environment.ts");








var TaskLibraryService = /** @class */ (function () {
    function TaskLibraryService(appConfig, http, spinnerService, toastrService) {
        this.http = http;
        this.spinnerService = spinnerService;
        this.toastrService = toastrService;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].apiGatewayUrl + "api/v1/task/tasks-libraries";
    }
    TaskLibraryService.prototype.resolve = function (route, state) {
        var queryParams = route.params;
        return this.search();
    };
    TaskLibraryService.prototype.search = function () {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/group").pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            result.items.forEach(function (item) {
                item.isShowTask = true;
            });
            return result;
        }));
    };
    TaskLibraryService.prototype.getDetail = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/group/" + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    TaskLibraryService.prototype.insert = function (taskLibrary) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url, taskLibrary).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastrService.success(result.message);
            return result;
        }));
    };
    TaskLibraryService.prototype.update = function (id, taskLibrary) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/" + id, taskLibrary).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastrService.success(result.message);
            return result;
        }));
    };
    TaskLibraryService.prototype.delete = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.delete(this.url + "/" + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastrService.success(result.message);
            return result;
        }));
    };
    TaskLibraryService.prototype.insertGroup = function (taskGroupLibrary) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/group", taskGroupLibrary).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastrService.success(result.message);
            return result;
        }));
    };
    TaskLibraryService.prototype.updateGroup = function (id, taskGroupLibrary) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/group/" + id, taskGroupLibrary).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastrService.success(result.message);
            return result;
        }));
    };
    TaskLibraryService.prototype.deleteGroup = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.delete(this.url + "/group/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastrService.success(result.message);
            return result;
        }));
    };
    TaskLibraryService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__["SpinnerService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"]])
    ], TaskLibraryService);
    return TaskLibraryService;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-library/task-check-list-form/task-check-list-form.component.html":
/*!********************************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-library/task-check-list-form/task-check-list-form.component.html ***!
  \********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #taskCheckListModal size=\"md\" (hidden)=\"onModalHidden()\">\r\n    <nh-modal-header>\r\n        {{ isUpdate ? 'Cập nhật checklist' : 'Thêm checklist'}}\r\n    </nh-modal-header>\r\n    <form class=\"form-horizontal\" class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content>\r\n            <div class=\"form-body\">\r\n                <div class=\"form-group\" [class.has-error]=\"formErrors?.taskId\">\r\n                    <label i18n-ghmLabel=\"@@taskGroupLibraryName\" ghmLabel=\"Tên công việc mẫu\" class=\"col-sm-4 ghm-label\"\r\n                           [required]=\"true\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <ghm-input\r\n                            formControlName=\"taskName\"\r\n                            [isDisabled]=\"true\"></ghm-input>\r\n                        <span class=\"help-block\">\r\n                        {formErrors?.taskId, select, required {Tên công việc không được để trống }}\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\" [class.has-error]=\"formErrors?.name\">\r\n                    <label ghmLabel=\"Tên checklist\" class=\"col-sm-4 ghm-label\" [required]=\"true\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <ghm-input\r\n                            [elementId]=\"'name'\"\r\n                            formControlName=\"name\">\r\n                        </ghm-input>\r\n                        <span class=\"help-block\">\r\n                        {formErrors?.name, select, required {Tên checklist không được để trống}  maxlength{Tên check lisk không được vượt quá 256 ký tự}}\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <div class=\"center\">\r\n                <ghm-button classes=\"btn blue cm-mgr-5\" [loading]=\"isSaving\">\r\n                    Thực hiện\r\n                </ghm-button>\r\n                <ghm-button type=\"button\" classes=\"btn btn-light\" nh-dismiss=\"true\">\r\n                    Đóng\r\n                </ghm-button>\r\n            </div>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-library/task-check-list-form/task-check-list-form.component.ts":
/*!******************************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-library/task-check-list-form/task-check-list-form.component.ts ***!
  \******************************************************************************************************************/
/*! exports provided: TaskCheckListFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskCheckListFormComponent", function() { return TaskCheckListFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _services_task_check_list_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/task-check-list.service */ "./src/app/modules/task/task-management/task-library/services/task-check-list.service.ts");
/* harmony import */ var _models_check_list_model__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../models/check-list.model */ "./src/app/modules/task/task-management/task-library/models/check-list.model.ts");
/* harmony import */ var _shareds_constants_task_type_const__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../shareds/constants/task-type.const */ "./src/app/shareds/constants/task-type.const.ts");










var TaskCheckListFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TaskCheckListFormComponent, _super);
    function TaskCheckListFormComponent(fb, utilService, taskCheckListService) {
        var _this = _super.call(this) || this;
        _this.fb = fb;
        _this.utilService = utilService;
        _this.taskCheckListService = taskCheckListService;
        _this.checkList = new _models_check_list_model__WEBPACK_IMPORTED_MODULE_8__["CheckList"]();
        return _this;
    }
    TaskCheckListFormComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    TaskCheckListFormComponent.prototype.onModalHidden = function () {
        this.isUpdate = false;
        this.model.reset(new _models_check_list_model__WEBPACK_IMPORTED_MODULE_8__["CheckList"]());
        this.checkList = new _models_check_list_model__WEBPACK_IMPORTED_MODULE_8__["CheckList"]();
        this.clearFormError(this.formErrors);
    };
    TaskCheckListFormComponent.prototype.update = function (item, taskGroupId) {
        this.taskGroupLibraryId = taskGroupId;
        this.id = item.id;
        this.isUpdate = true;
        this.utilService.focusElement('name');
        this.model.patchValue(item);
        this.taskCheckListModal.open();
    };
    TaskCheckListFormComponent.prototype.add = function (taskLibrary, taskGroupId) {
        this.taskGroupLibraryId = taskGroupId;
        this.model.patchValue({ taskId: taskLibrary.id, taskName: taskLibrary.name });
        this.checkList.taskId = taskLibrary.id;
        this.isUpdate = false;
        this.utilService.focusElement('name');
        this.taskCheckListModal.open();
    };
    TaskCheckListFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            this.checkList = this.model.value;
            this.checkList.type = _shareds_constants_task_type_const__WEBPACK_IMPORTED_MODULE_9__["TaskType"].taskLibrary;
            if (this.isUpdate) {
                this.isSaving = true;
                this.taskCheckListService.update(this.id, this.checkList).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.isModified = true;
                    _this.checkList.id = _this.id;
                    _this.checkList.concurrencyStamp = result.data;
                    _this.saveSuccessful.emit({ taskGroupLibraryId: _this.taskGroupLibraryId, checkList: _this.checkList });
                    _this.taskCheckListModal.dismiss();
                });
            }
            else {
                this.isSaving = true;
                this.taskCheckListService.insert(this.checkList).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.isModified = true;
                    _this.checkList.concurrencyStamp = result.data;
                    _this.checkList.id = result.data;
                    _this.saveSuccessful.emit({ taskGroupLibraryId: _this.taskGroupLibraryId, checkList: _this.checkList });
                    _this.taskCheckListModal.dismiss();
                });
            }
        }
    };
    TaskCheckListFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.renderFormError(['name', 'taskId']);
        this.validationMessages = this.renderFormErrorMessage([
            { 'name': ['required', 'maxlength'] },
            { 'taskId': ['required'] }
        ]);
        this.model = this.fb.group({
            name: [this.checkList.name, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(500)
                ]],
            concurrencyStamp: [this.checkList.concurrencyStamp],
            taskId: [this.checkList.taskId],
            taskName: [this.checkList.taskName]
        });
        this.subscribers.modelValueChanges = this.model.valueChanges.subscribe(function () { return _this.validateModel(false); });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('taskCheckListModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_6__["NhModalComponent"])
    ], TaskCheckListFormComponent.prototype, "taskCheckListModal", void 0);
    TaskCheckListFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-check-list-form',
            template: __webpack_require__(/*! ./task-check-list-form.component.html */ "./src/app/modules/task/task-management/task-library/task-check-list-form/task-check-list-form.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_4__["UtilService"],
            _services_task_check_list_service__WEBPACK_IMPORTED_MODULE_7__["TaskCheckListService"]])
    ], TaskCheckListFormComponent);
    return TaskCheckListFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/task/task-management/task-library/task-group-library-form/task-group-library-form.component.html":
/*!**************************************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-library/task-group-library-form/task-group-library-form.component.html ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #taskGroupLibraryModal size=\"md\" (hidden)=\"onModalHidden()\">\r\n    <nh-modal-header>\r\n        {{ isUpdate ? 'Cập nhật nhóm công việc mẫu' : 'Tạo nhóm công việc mẫu'}}\r\n    </nh-modal-header>\r\n    <form class=\"form-horizontal\" class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content>\r\n            <div class=\"form-body\">\r\n                <div class=\"form-group\" [class.has-error]=\"formErrors?.name\">\r\n                    <label ghmLabel=\"Tên nhóm\" class=\"col-sm-4 ghm-label\" [required]=\"true\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <ghm-input\r\n                            [elementId]=\"'name'\"\r\n                            formControlName=\"name\">\r\n                        </ghm-input>\r\n                        <span class=\"help-block\">\r\n                        {formErrors?.name, select, required {Tên nhóm không để trống}\r\n                            maxlength {Tên nhóm không được quá 256 ký tự}}\r\n                </span>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\" [class.has-error]=\"formErrors?.description\">\r\n                    <label i18n-ghmLabel=\"@@description\" ghmLabel=\"Mô tả\" class=\"col-sm-4 ghm-label\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <textarea formControlName=\"description\"\r\n                                  class=\"form-control\"\r\n                                  rows=\"3\"></textarea>\r\n                        <span class=\"help-block\">\r\n                           {formErrors?.description, select, maxlength {Mô tả không được vượt quá 4000 ký tự}}\r\n                       </span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <div class=\"center\">\r\n                <ghm-button classes=\"btn blue cm-mgr-5\" [loading]=\"isSaving\" i18n=\"@@save\">\r\n                    Cập nhật\r\n                </ghm-button>\r\n                <ghm-button type=\"button\" classes=\"btn btn-light\" nh-dismiss=\"true\" i18n=\"@@close\">\r\n                    Đóng\r\n                </ghm-button>\r\n            </div>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-library/task-group-library-form/task-group-library-form.component.ts":
/*!************************************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-library/task-group-library-form/task-group-library-form.component.ts ***!
  \************************************************************************************************************************/
/*! exports provided: TaskGroupLibraryFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskGroupLibraryFormComponent", function() { return TaskGroupLibraryFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _models_task_group_library_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../models/task-group-library.model */ "./src/app/modules/task/task-management/task-library/models/task-group-library.model.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _services_task_library_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/task-library.service */ "./src/app/modules/task/task-management/task-library/services/task-library.service.ts");









var TaskGroupLibraryFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TaskGroupLibraryFormComponent, _super);
    function TaskGroupLibraryFormComponent(fb, utilService, taskLibraryService) {
        var _this = _super.call(this) || this;
        _this.fb = fb;
        _this.utilService = utilService;
        _this.taskLibraryService = taskLibraryService;
        _this.taskGroupLibrary = new _models_task_group_library_model__WEBPACK_IMPORTED_MODULE_6__["TaskGroupLibrary"]();
        return _this;
    }
    TaskGroupLibraryFormComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    TaskGroupLibraryFormComponent.prototype.onModalHidden = function () {
        this.isUpdate = false;
        this.model.reset(new _models_task_group_library_model__WEBPACK_IMPORTED_MODULE_6__["TaskGroupLibrary"]());
        this.taskGroupLibrary = new _models_task_group_library_model__WEBPACK_IMPORTED_MODULE_6__["TaskGroupLibrary"]();
        this.clearFormError(this.formErrors);
    };
    TaskGroupLibraryFormComponent.prototype.update = function (id) {
        this.id = id;
        this.getDetail(id);
        this.isUpdate = true;
        this.utilService.focusElement('name');
        this.taskGroupLibraryModal.open();
    };
    TaskGroupLibraryFormComponent.prototype.add = function () {
        this.isUpdate = false;
        this.utilService.focusElement('name');
        this.taskGroupLibraryModal.open();
    };
    TaskGroupLibraryFormComponent.prototype.getDetail = function (id) {
        var _this = this;
        this.taskLibraryService.getDetail(id).subscribe(function (result) {
            _this.taskGroupLibraryDetail = result.data;
            _this.model.patchValue(_this.taskGroupLibraryDetail);
        });
    };
    TaskGroupLibraryFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            this.taskGroupLibrary = this.model.value;
            if (this.isUpdate) {
                this.isSaving = true;
                this.taskLibraryService.updateGroup(this.id, this.taskGroupLibrary).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.taskGroupLibrary.id = _this.id;
                    _this.taskGroupLibrary.concurrencyStamp = result.data;
                    _this.saveSuccessful.emit(_this.taskGroupLibrary);
                    _this.taskGroupLibraryModal.dismiss();
                });
            }
            else {
                this.isSaving = true;
                this.taskLibraryService.insertGroup(this.taskGroupLibrary).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.taskGroupLibrary.concurrencyStamp = result.data;
                    _this.taskGroupLibrary.id = result.data;
                    _this.saveSuccessful.emit(_this.taskGroupLibrary);
                    _this.taskGroupLibraryModal.dismiss();
                });
            }
        }
    };
    TaskGroupLibraryFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.renderFormError(['name', 'description']);
        this.validationMessages = this.renderFormErrorMessage([
            { 'name': ['required', 'maxlength'] },
            { 'description': ['maxlength'] }
        ]);
        this.model = this.fb.group({
            name: [this.taskGroupLibrary.name, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(256)
                ]],
            description: [this.taskGroupLibrary.description],
            concurrencyStamp: [this.taskGroupLibrary.concurrencyStamp]
        });
        this.subscribers.modelValueChanges = this.model.valueChanges.subscribe(function () { return _this.validateModel(false); });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('taskGroupLibraryModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_5__["NhModalComponent"])
    ], TaskGroupLibraryFormComponent.prototype, "taskGroupLibraryModal", void 0);
    TaskGroupLibraryFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-group-library-form',
            template: __webpack_require__(/*! ./task-group-library-form.component.html */ "./src/app/modules/task/task-management/task-library/task-group-library-form/task-group-library-form.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_7__["UtilService"],
            _services_task_library_service__WEBPACK_IMPORTED_MODULE_8__["TaskLibraryService"]])
    ], TaskGroupLibraryFormComponent);
    return TaskGroupLibraryFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/task/task-management/task-library/task-library-form/task-library-form.component.html":
/*!**************************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-library/task-library-form/task-library-form.component.html ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #taskLibraryModal size=\"md\" (hidden)=\"onModalHidden()\">\r\n    <nh-modal-header>\r\n        {{ isUpdate ? 'Cập nhật nhóm công việc mẫu' : 'Thêm công việc mẫu'}}\r\n\r\n    </nh-modal-header>\r\n    <form class=\"form-horizontal\" class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content>\r\n            <div class=\"form-body\">\r\n                <div class=\"form-group\" [class.has-error]=\"formErrors?.taskGroupLibraryId\">\r\n                    <label i18n-ghmLabel=\"@@taskGroupLibraryName\" ghmLabel=\"Tên nhóm\" class=\"col-sm-4 ghm-label\"\r\n                           [required]=\"true\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <ghm-select\r\n                            formControlName=\"taskGroupLibraryId\"\r\n                            [elementId]=\"'selectTargetTask'\"\r\n                            [data]=\"listGroup\"\r\n                            i18n=\"@@listTargetGroup\"\r\n                            [selectedItem]=\"taskLibrary.taskGroupLibraryId\"\r\n                            i18n-title=\"@@selectTargetTask\"\r\n                            [title]=\"'-- Chọn nhóm công việc --'\"></ghm-select>\r\n                        <span class=\"help-block\">\r\n                        {formErrors?.taskGroupLibraryId, select, required {Tên nhóm không được để trống }}\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\" [class.has-error]=\"formErrors?.name\">\r\n                    <label i18n-ghmLabel=\"@@taskLibraryName\" ghmLabel=\"Tên công việc mẫu\" class=\"col-sm-4 ghm-label\"\r\n                           [required]=\"true\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <ghm-input\r\n                            [elementId]=\"'name'\"\r\n                            [placeholder]=\"'Nhập tên công việc mẫu'\"\r\n                            i18n-placeholder=\"@@enterTaskLibraryName\"\r\n                            formControlName=\"name\"></ghm-input>\r\n                        <span class=\"help-block\">\r\n                        {formErrors?.name, select, required {Tên công việc không được để trống}\r\n                            maxlength {Tên công việc không được quá 256 ký tự}}\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\" [class.has-error]=\"formErrors?.description\">\r\n                    <label i18n-ghmLabel=\"@@description\" ghmLabel=\"Mô tả\" class=\"col-sm-4 ghm-label\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <textarea formControlName=\"description\"\r\n                                  class=\"form-control\"\r\n                                  rows=\"3\"></textarea>\r\n                        <span class=\"help-block\">\r\n                           {formErrors?.description, select, maxlength {Mô tả không được vượt quá 4000 ký tự}}\r\n                       </span>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\" [class.has-error]=\"formErrors?.methodCalculateResult\">\r\n                    <label i18n-ghmLabel=\"@@MeasurementMethod\" ghmLabel=\"Cách tính kết quả\"\r\n                           class=\"col-sm-4 ghm-label\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <mat-radio-group formControlName=\"methodCalculateResult\">\r\n                            <mat-radio-button color=\"primary\"\r\n                                              [value]=\"0\">\r\n                                <p class=\"cm-mgt-0 cm-mgb-0\">Theo checklist</p>\r\n                            </mat-radio-button>\r\n                            <br/>\r\n                            <mat-radio-button color=\"primary\"\r\n                                              [value]=\"1\">\r\n                                <p class=\"cm-mgt-0 cm-mgb-0\">Tự nhập</p>\r\n                            </mat-radio-button>\r\n                            <br>\r\n                            <mat-radio-button color=\"primary\"\r\n                                              [value]=\"2\">\r\n                                <p class=\"cm-mgt-0 cm-mgb-0\">Theo công việc con</p>\r\n                            </mat-radio-button>\r\n                        </mat-radio-group>\r\n                        <span class=\"help-block\">\r\n                            {formErrors?.methodCalculateResult, select, required {Bạn phải chọn phương pháp đo}}\r\n                         </span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <div class=\"center\">\r\n                <ghm-button classes=\"btn blue cm-mgr-5\" [loading]=\"isSaving\" i18n=\"@@update\">\r\n                    Cập nhật\r\n                </ghm-button>\r\n                <ghm-button type=\"button\" classes=\"btn btn-light\" nh-dismiss=\"true\" i18n=\"close\">\r\n                    Đóng\r\n                </ghm-button>\r\n            </div>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-library/task-library-form/task-library-form.component.scss":
/*!**************************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-library/task-library-form/task-library-form.component.scss ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "::ng-deep .mat-radio-button.mat-primary .mat-radio-ripple .mat-ripple-element {\n  background: none !important; }\n\n::ng-deep .mat-radio-button.mat-primary .mat-radio-inner-circle {\n  background-color: #3598dc !important;\n  /*inner circle color change*/ }\n\n::ng-deep.mat-radio-button.mat-primary.mat-radio-checked .mat-radio-outer-circle {\n  border-color: #3598dc !important;\n  /*outer ring color change*/ }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC90YXNrLWxpYnJhcnkvdGFzay1saWJyYXJ5LWZvcm0vRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFwcFxcbW9kdWxlc1xcdGFza1xcdGFzay1tYW5hZ2VtZW50XFx0YXNrLWxpYnJhcnlcXHRhc2stbGlicmFyeS1mb3JtXFx0YXNrLWxpYnJhcnktZm9ybS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLDJCQUEyQixFQUFBOztBQUcvQjtFQUNJLG9DQUF3QztFQUFFLDRCQUFBLEVBQTZCOztBQUczRTtFQUNJLGdDQUFvQztFQUFFLDBCQUFBLEVBQTJCIiwiZmlsZSI6InNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC90YXNrLWxpYnJhcnkvdGFzay1saWJyYXJ5LWZvcm0vdGFzay1saWJyYXJ5LWZvcm0uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IFwiLi4vLi4vLi4vLi4vLi4vLi4vYXNzZXRzL3N0eWxlcy9jb25maWdcIjtcclxuOjpuZy1kZWVwIC5tYXQtcmFkaW8tYnV0dG9uLm1hdC1wcmltYXJ5IC5tYXQtcmFkaW8tcmlwcGxlIC5tYXQtcmlwcGxlLWVsZW1lbnQge1xyXG4gICAgYmFja2dyb3VuZDogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC1yYWRpby1idXR0b24ubWF0LXByaW1hcnkgLm1hdC1yYWRpby1pbm5lci1jaXJjbGUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWJsdWUgIWltcG9ydGFudDsgLyppbm5lciBjaXJjbGUgY29sb3IgY2hhbmdlKi9cclxufVxyXG5cclxuOjpuZy1kZWVwLm1hdC1yYWRpby1idXR0b24ubWF0LXByaW1hcnkubWF0LXJhZGlvLWNoZWNrZWQgLm1hdC1yYWRpby1vdXRlci1jaXJjbGUge1xyXG4gICAgYm9yZGVyLWNvbG9yOiAkY29sb3ItYmx1ZSAhaW1wb3J0YW50OyAvKm91dGVyIHJpbmcgY29sb3IgY2hhbmdlKi9cclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-library/task-library-form/task-library-form.component.ts":
/*!************************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-library/task-library-form/task-library-form.component.ts ***!
  \************************************************************************************************************/
/*! exports provided: TaskLibraryFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskLibraryFormComponent", function() { return TaskLibraryFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _models_task_library_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../models/task-library.model */ "./src/app/modules/task/task-management/task-library/models/task-library.model.ts");
/* harmony import */ var _services_task_library_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/task-library.service */ "./src/app/modules/task/task-management/task-library/services/task-library.service.ts");









var TaskLibraryFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TaskLibraryFormComponent, _super);
    function TaskLibraryFormComponent(fb, utilService, taskLibraryService) {
        var _this = _super.call(this) || this;
        _this.fb = fb;
        _this.utilService = utilService;
        _this.taskLibraryService = taskLibraryService;
        _this.taskLibrary = new _models_task_library_model__WEBPACK_IMPORTED_MODULE_7__["TaskLibrary"]();
        _this.onUpdateSuccess = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        return _this;
    }
    TaskLibraryFormComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    TaskLibraryFormComponent.prototype.onModalHidden = function () {
        this.isUpdate = false;
        this.taskLibrary = new _models_task_library_model__WEBPACK_IMPORTED_MODULE_7__["TaskLibrary"]();
        this.model.reset(new _models_task_library_model__WEBPACK_IMPORTED_MODULE_7__["TaskLibrary"]());
        if (this.isModified) {
            this.saveSuccessful.emit();
        }
        this.clearFormError(this.formErrors);
    };
    TaskLibraryFormComponent.prototype.update = function (item, listGroup) {
        this.id = item.id;
        this.isUpdate = true;
        this.utilService.focusElement('name');
        this.model.patchValue(item);
        this.listGroup = listGroup;
        this.taskLibraryModal.open();
    };
    TaskLibraryFormComponent.prototype.add = function (taskGroupLibrary, listGroup) {
        console.log(listGroup);
        this.isUpdate = false;
        this.utilService.focusElement('name');
        this.listGroup = listGroup;
        this.model.patchValue({
            taskGroupLibraryId: taskGroupLibrary.id,
            taskGroupLibraryName: taskGroupLibrary.name, methodCalculateResult: 0
        });
        this.taskLibraryModal.open();
    };
    TaskLibraryFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            this.taskLibrary = this.model.value;
            if (this.isUpdate) {
                this.isSaving = true;
                this.taskLibraryService.update(this.id, this.taskLibrary).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.taskLibrary.id = _this.id;
                    _this.taskLibrary.concurrencyStamp = result.data;
                    _this.saveSuccessful.emit(_this.taskLibrary);
                    _this.taskLibraryModal.dismiss();
                });
            }
            else {
                this.isSaving = true;
                this.taskLibraryService.insert(this.taskLibrary).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.taskLibrary = result.data;
                    _this.saveSuccessful.emit(_this.taskLibrary);
                    _this.taskLibraryModal.dismiss();
                });
            }
        }
    };
    TaskLibraryFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.renderFormError(['name', 'description', 'methodCalculateResult',
            'taskGroupLibraryId', 'taskGroupLibraryName']);
        this.validationMessages = this.renderFormErrorMessage([
            { 'name': ['required', 'maxlength'] },
            { 'description': ['maxlength'] },
            { 'methodCalculateResult': ['required'] },
            { 'taskGroupLibraryId': ['required'] },
            { 'taskGroupLibraryName': ['required'] }
        ]);
        this.model = this.fb.group({
            name: [this.taskLibrary.name, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(256)
                ]],
            description: [this.taskLibrary.description, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(4000)
                ]],
            methodCalculateResult: [this.taskLibrary.methodCalculateResult, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                ]],
            taskGroupLibraryId: [this.taskLibrary.taskGroupLibraryId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                ]],
            taskGroupLibraryName: [this.taskLibrary.taskGroupLibraryName, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                ]],
            concurrencyStamp: [this.taskLibrary.concurrencyStamp]
        });
        this.subscribers.modelValueChanges = this.model.valueChanges.subscribe(function () { return _this.validateModel(false); });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('taskLibraryModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_2__["NhModalComponent"])
    ], TaskLibraryFormComponent.prototype, "taskLibraryModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TaskLibraryFormComponent.prototype, "onUpdateSuccess", void 0);
    TaskLibraryFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-library-form',
            template: __webpack_require__(/*! ./task-library-form.component.html */ "./src/app/modules/task/task-management/task-library/task-library-form/task-library-form.component.html"),
            styles: [__webpack_require__(/*! ./task-library-form.component.scss */ "./src/app/modules/task/task-management/task-library/task-library-form/task-library-form.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_4__["UtilService"],
            _services_task_library_service__WEBPACK_IMPORTED_MODULE_8__["TaskLibraryService"]])
    ], TaskLibraryFormComponent);
    return TaskLibraryFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_6__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/task/task-management/task-library/task-library.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-library/task-library.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\" [class.cm-mg-10]=\"isAllowSelect\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@taskLibrary\">Thư viện công việc mẫu</span>\r\n</h1>\r\n<div class=\"cm-mgb-10\" *ngIf=\"!isAllowSelect\">\r\n    <button class=\"btn blue cm-mgr-10\" type=\"button\" (click)=\"addGroup()\" i18n=\"@@add\" *ngIf=\"permission.add\">\r\n        Thêm nhóm công việc mẫu\r\n    </button>\r\n    <!--<button class=\"btn blue\">-->\r\n    <!--<i class=\"fa fa-sort-amount-asc\"></i>-->\r\n    <!--</button>-->\r\n</div>\r\n\r\n<div class=\"tasks-library\" *ngIf=\"listItems && listItems.length > 0; else notFound\" [ngStyle]=\"{'width': isAllowSelect ? 1000 + 'px': ''}\">\r\n    <div class=\"table-tree\" *ngFor=\"let item of listItems; let i = index\">\r\n        <div class=\"tt-header\">\r\n            <div class=\"cell cell-group middle w100pc cm-pd-12-6\" (click)=\"item.isShowTask = !item.isShowTask\">\r\n                <a class=\"icon cm-mgl-5\">\r\n                    <i class=\"fa fa-caret-down\" aria-hidden=\"true\" *ngIf=\"item.isShowTask\"></i>\r\n                    <i class=\"fa fa-caret-right\" aria-hidden=\"true\" *ngIf=\"!item.isShowTask\"></i>\r\n                    {{item.name}}\r\n                </a>\r\n            </div>\r\n            <div class=\"cell cell-progress-method w150 middle center hidden-xs\">\r\n                <span i18n=\"@@methodResultTask\">Cách tính kết quả công việc</span>\r\n            </div>\r\n            <div class=\"cell cell-action-priority w100 middle center\"\r\n                 *ngIf=\"(permission.add || permission.edit || permission.delete) && !isAllowSelect\">\r\n                <a *ngIf=\"permission.add\" (click)=\"addTaskLibrary(item)\" i18n-title=\"@@addTaskLibrary\"\r\n                   title=\"Thêm mới công việc mẫu\"><i\r\n                    class=\"fa fa-plus\"></i></a>\r\n                <a *ngIf=\"permission.edit\" (click)=\"updateGroup(item)\" i18n-title=\"@@updateTaskGroupLibrary\"\r\n                   title=\"Cập nhật nhóm công việc mẫu\"><i class=\"fa fa-pencil\"></i></a>\r\n                <a *ngIf=\"permission.delete\" [swal]=\"confirmDeleteTaskGroupLibrary\"\r\n                   i18n-title=\"@@deleteTaskGroupLibrary\"\r\n                   (confirm)=\"deleteGroup(item.id)\" title=\"Xóa nhóm công việc mẫu\"><i class=\"fa fa-times\"></i></a>\r\n            </div>\r\n        </div>\r\n        <div class=\"tt-body cm-pdb-0\" *ngIf=\"item.tasksLibraries && item.tasksLibraries.length > 0 && item.isShowTask\">\r\n            <div class=\"parent-tree\">\r\n                <div class=\"children\">\r\n                    <div class=\"tree-container\" *ngFor=\"let task of item.tasksLibraries; let i = index\">\r\n                        <div class=\"goal-line row-tr bg-white\"\r\n                             [class.last-goal-line]=\"i + 1 === item.tasksLibraries.length\">\r\n                            <div class=\"goal-tree-caret\" (click)=\"showCheckList(task)\">\r\n                                <i class=\"fa fa-fw fa-chevron-down\" *ngIf=\"task.isShowCheckList\"></i>\r\n                                <i class=\"fa fa-chevron-right\" aria-hidden=\"true\" *ngIf=\"!task.isShowCheckList\"></i>\r\n                            </div>\r\n                            <div class=\"cell gw-cell-item w100pc middle\">\r\n                                <div class=\"gw-item \" [class.cursor-pointer]=\"isAllowSelect\"\r\n                                     (click)=\"isAllowSelect ? selectTaskLibrary(task) : ''\">\r\n                                    <i class=\"fa fa-circle-thin font-size-18 cm-pdr-5 color-light-brown cm-pdt-5\"\r\n                                       *ngIf=\"isAllowSelect\"></i>\r\n                                    {{task.name}}\r\n                                    <div class=\"gw-description cm-pdt-5\" *ngIf=\"task.description\">\r\n                                        {{task.description}}\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"cell gw-cell-progress-method middle center w150 hidden-xs\">\r\n                                <span>{task.methodCalculateResult, select, 0 {Theo checklist} 1 {Tự nhập} 2 {Theo công việc con}}</span>\r\n                            </div>\r\n                            <div class=\"cell gw-cell-action-priority middle center w100\"\r\n                                 *ngIf=\"(permission.add || permission.edit || permission.delete) && !isAllowSelect\">\r\n                                <a *ngIf=\"permission.add\" i18n-title=\"@@addCheckList\" title=\"Thêm mới check list\"\r\n                                   (click)=\"addCheckList(task, item.id)\">\r\n                                    <i class=\"fa fa-plus\"></i>\r\n                                </a>\r\n                                <a *ngIf=\"permission.edit\" i18n-title=\"@@updateTaskLibrary\"\r\n                                   (click)=\"updateTaskLibrary(task)\"\r\n                                   title=\"Cập nhật công việc mẫu\"><i class=\"fa fa-pencil\"></i></a>\r\n                                <a *ngIf=\"permission.delete\" i18n-title=\"@@deleteTaskLibrary\"\r\n                                   [swal]=\"confirmDeleteTaskLibrary\"\r\n                                   (confirm)=deleteTaskLibrary(task)\r\n                                   title=\"Xóa công việc mẫu\"><i class=\"fa fa-times\"></i></a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"parent-tree\"\r\n                             *ngIf=\"task.checkLists && task.checkLists.length > 0 && task.isShowCheckList\"\r\n                             [class.last-parent-tree]=\"i + 1 === item.tasksLibraries.length\">\r\n                            <div class=\"children\">\r\n                                <div class=\"tree-container\" *ngFor=\"let checkList of task.checkLists; let i = index\">\r\n                                    <div class=\"goal-line row-tr bg-white border-top-style-dashed\"\r\n                                         [class.last-goal-line]=\"i + 1 === task.checkLists.length\">\r\n                                        <div class=\"cell gw-cell-item w100pc middle\">\r\n                                            <div class=\"gw-item\">\r\n                                                - {{checkList.name}}\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"cell gw-cell-progress-method middle center w150 hidden-xs\">\r\n                                        </div>\r\n                                        <div class=\"cell gw-cell-action-priority middle center w100\"\r\n                                             *ngIf=\"(permission.add || permission.edit || permission.delete) && !isAllowSelect\">\r\n                                            <a *ngIf=\"permission.edit\" i18n-title=\"@@editCheckList\"\r\n                                               title=\"Cập nhật check list\"\r\n                                               (click)=\"updateCheckList(checkList, item.id)\">\r\n                                                <i class=\"fa fa-pencil\"></i>\r\n                                            </a>\r\n                                            <a *ngIf=\"permission.delete\" i18n-title=\"@@deleteCheckList\"\r\n                                               title=\"Xóa check list\"\r\n                                               [swal]=\"confirmDeleteTaskChekList\"\r\n                                               (confirm)=\"deleteCheckList(checkList, task)\">\r\n                                                <i class=\"fa fa-times\"></i>\r\n                                            </a>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"box-confirm bg-info-blue\" *ngIf=\"!item.tasksLibraries || item.tasksLibraries?.length === 0\">\r\n            <i class=\"fa fa-info-circle color-dark-blue font-size-18\"\r\n               aria-hidden=\"true\"></i><span> Không có dữ liệu</span>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"center cm-mgb-10\" *ngIf=\"isAllowSelect\">\r\n    <button class=\"btn btn-light\" (click)=\"closeDialog()\">Đóng</button>\r\n</div>\r\n\r\n<ng-template #notFound>\r\n    <div class=\"box-confirm bg-info-blue\">\r\n        <i class=\"fa fa-info-circle color-dark-blue font-size-18\"\r\n           aria-hidden=\"true\"></i><span> Không có dữ liệu</span>\r\n    </div>\r\n</ng-template>\r\n\r\n<app-task-library-form (saveSuccessful)=\"onSaveSuccessTaskLibrary($event)\"></app-task-library-form>\r\n<app-task-group-library-form (saveSuccessful)=\"onSaveSuccessTaskGroupLibrary($event)\"></app-task-group-library-form>\r\n<app-task-check-list-form (saveSuccessful)=\"onSaveSuccessTaskCheckList($event)\"></app-task-check-list-form>\r\n\r\n<swal\r\n    #confirmDeleteTaskGroupLibrary\r\n    i18n=\"@@confirmDeleteTaskGroupLibrary\"\r\n    i18n-title=\"@@confirmDeleteTaskGroupLibrary\"\r\n    i18n-text=\"@@confirmDeleteTaskGroupLibrary\"\r\n    title=\"Xác nhận\"\r\n    text=\"Bạn có thực sự muốn xóa nhóm công việc mẫu này ?\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Đồng ý\"\r\n    cancelButtonText=\"Không\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<swal\r\n    #confirmDeleteTaskLibrary\r\n    i18n=\"@@confirmDeleteTaskLibrary\"\r\n    i18n-title=\"@@confirmDeleteTaskLibrary\"\r\n    i18n-text=\"@@confirmDeleteTaskLibrary\"\r\n    title=\"Xác nhận\"\r\n    text=\"Bạn có thực sự muốn xóa công việc mẫu này ?\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Đồng ý\"\r\n    cancelButtonText=\"Không\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<swal\r\n    #confirmDeleteTaskChekList\r\n    i18n=\"@@confirmDeleteTaskCheckList\"\r\n    i18n-title=\"@@confirmDeleteTaskCheckList\"\r\n    i18n-text=\"@@confirmDeleteTaskCheckList\"\r\n    title=\"Xác nhận\"\r\n    text=\"Bạn có thực sự muốn xóa checklist này ?\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Đồng ý\"\r\n    cancelButtonText=\"Không\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-library/task-library.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-library/task-library.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".tasks-library {\n  position: relative;\n  float: left;\n  width: 100%;\n  border-radius: 4px !important;\n  -moz-border-radius: 4px;\n  -webkit-border-radius: 4px;\n  -ms-border-radius: 4px;\n  -o-border-radius: 4px;\n  min-height: 32px;\n  background-color: #f5f7f7;\n  padding: 12px;\n  margin-bottom: 12px; }\n  .tasks-library i {\n    font-weight: normal !important; }\n  .tasks-library .table-tree {\n    position: relative;\n    float: left;\n    width: 100%;\n    margin-bottom: 12px; }\n  .tasks-library .table-tree .cell {\n      position: relative;\n      display: table-cell; }\n  .tasks-library .table-tree .cell-group {\n      min-width: 200px; }\n  .tasks-library .table-tree .row-tr {\n      border-top: 1px solid #ddd;\n      overflow: visible; }\n  .tasks-library .table-tree .tt-header {\n      padding: 0;\n      font-weight: normal;\n      line-height: 20px;\n      position: relative;\n      float: left;\n      width: 100%;\n      background-color: #45A2D2;\n      cursor: pointer;\n      font-size: 14px;\n      color: #fff;\n      border-radius: 4px !important; }\n  .tasks-library .table-tree .tt-header .icon {\n        color: #fff;\n        font-size: 16px; }\n  .tasks-library .table-tree .tt-header a:hover {\n        text-decoration: none; }\n  .tasks-library .table-tree .tt-header .cell-progress-method {\n        padding: 5px;\n        text-align: center;\n        border-left: 1px dashed #ddd; }\n  .tasks-library .table-tree .tt-header .cell-action-priority {\n        padding: 5px;\n        border-left: 1px dashed #ddd; }\n  .tasks-library .table-tree .tt-header .cell-action-priority a {\n          color: #fff;\n          padding: 2px; }\n  .tasks-library .table-tree .tt-header .cell-action-priority a:hover {\n            color: #fd7e14 !important; }\n  .tasks-library .table-tree .tt-header .cell-action-priority a .fa {\n            margin-right: 3px; }\n  .tasks-library .table-tree .tt-body {\n      position: relative;\n      float: left;\n      width: 100%;\n      background-color: #f5f7f7; }\n  .tasks-library .table-tree .parent-tree {\n      position: relative; }\n  .tasks-library .table-tree .parent-tree:before {\n        content: \"\";\n        display: block;\n        width: 1px;\n        position: absolute;\n        top: 0;\n        bottom: 0;\n        z-index: 1;\n        left: 14px;\n        background: #bbb; }\n  .tasks-library .table-tree .parent-tree .children {\n        position: relative;\n        padding-left: 28px; }\n  .tasks-library .table-tree .parent-tree .children .tree-container {\n          position: relative;\n          width: 100%; }\n  .tasks-library .table-tree .parent-tree .children .goal-line {\n          position: relative; }\n  .tasks-library .table-tree .parent-tree .children .goal-line:hover {\n            background-color: #dff0fd !important; }\n  .tasks-library .table-tree .parent-tree .children .goal-line::before {\n            content: \"\";\n            display: block;\n            width: 14px;\n            position: absolute;\n            bottom: 0;\n            top: 50%;\n            margin-top: -1px;\n            z-index: 1;\n            background: #bbb;\n            left: -14px;\n            height: 1px; }\n  .tasks-library .table-tree .parent-tree .children .goal-line .goal-tree-caret {\n            cursor: pointer;\n            color: #8c8c8c;\n            top: 0;\n            bottom: 0;\n            width: 27px;\n            left: -28px;\n            position: absolute;\n            z-index: 2; }\n  .tasks-library .table-tree .parent-tree .children .goal-line .goal-tree-caret i {\n              font-size: .6em;\n              top: 50%;\n              margin-top: -8px;\n              background: #fff;\n              border-radius: 100%;\n              width: 2em;\n              line-height: 2em;\n              position: absolute;\n              z-index: 2;\n              left: 6px;\n              text-align: center; }\n  .tasks-library .table-tree .parent-tree .children .goal-line .gw-cell-item {\n            padding: 6px 0 6px 12px; }\n  .tasks-library .table-tree .parent-tree .children .goal-line .gw-cell-item .gw-item {\n              padding: 0px 6px; }\n  .tasks-library .table-tree .parent-tree .children .goal-line .gw-cell-item .gw-item .gw-description {\n                color: #757575;\n                font-style: italic; }\n  .tasks-library .table-tree .parent-tree .children .goal-line .gw-cell-progress-method {\n            border-left: 1px dashed #ddd; }\n  .tasks-library .table-tree .parent-tree .children .goal-line .gw-cell-action-priority {\n            border-left: 1px dashed #ddd; }\n  .tasks-library .table-tree .parent-tree .children .goal-line .gw-cell-action-priority a {\n              padding: 2px !important;\n              color: #0072bc; }\n  .tasks-library .table-tree .parent-tree .children .goal-line .gw-cell-action-priority a i {\n                margin-right: 3px; }\n  .tasks-library .table-tree .parent-tree .children .goal-line .gw-cell-action-priority a:hover {\n                color: #fd7e14; }\n  .tasks-library .table-tree .parent-tree .children .last-goal-line::after {\n          content: \"\";\n          display: block;\n          width: 1px;\n          position: absolute;\n          top: 50%;\n          z-index: 1;\n          right: 0;\n          left: -14px;\n          bottom: 0;\n          background: #f5f7f7; }\n  .tasks-library .table-tree .last-parent-tree::after {\n      content: \"\";\n      display: block;\n      width: 1px;\n      position: absolute;\n      top: 0%;\n      z-index: 1;\n      right: 0;\n      left: -14px;\n      bottom: 0;\n      background: #f5f7f7; }\n  .tasks-library .table-tree:first-child .tree_container .goal-line {\n      border-top: none; }\n  .tasks-library .table-tree .bg-info-blue {\n      border-radius: 4px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC90YXNrLWxpYnJhcnkvRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFwcFxcbW9kdWxlc1xcdGFza1xcdGFzay1tYW5hZ2VtZW50XFx0YXNrLWxpYnJhcnlcXHRhc2stbGlicmFyeS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC90YXNrLWxpYnJhcnkvRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFzc2V0c1xcc3R5bGVzXFxfY29uZmlnLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFdBQVc7RUFDWCw2QkFBNkI7RUFDN0IsdUJBQXVCO0VBQ3ZCLDBCQUEwQjtFQUMxQixzQkFBc0I7RUFDdEIscUJBQXFCO0VBQ3JCLGdCQUFnQjtFQUNoQix5QkN3QmdCO0VEdkJoQixhQUFhO0VBQ2IsbUJBQW1CLEVBQUE7RUFadkI7SUFlUSw4QkFBOEIsRUFBQTtFQWZ0QztJQW1CUSxrQkFBa0I7SUFDbEIsV0FBVztJQUNYLFdBQVc7SUFDWCxtQkFBbUIsRUFBQTtFQXRCM0I7TUF5Qlksa0JBQWtCO01BQ2xCLG1CQUFtQixFQUFBO0VBMUIvQjtNQThCWSxnQkFBZ0IsRUFBQTtFQTlCNUI7TUFrQ1ksMEJBQTBCO01BQzFCLGlCQUFpQixFQUFBO0VBbkM3QjtNQXVDWSxVQUFVO01BQ1YsbUJBQW1CO01BQ25CLGlCQUFpQjtNQUNqQixrQkFBa0I7TUFDbEIsV0FBVztNQUNYLFdBQVc7TUFDWCx5QkNiTTtNRGNOLGVBQWU7TUFDZixlQUFlO01BQ2YsV0MvQkE7TURnQ0EsNkJBQTZCLEVBQUE7RUFqRHpDO1FBb0RnQixXQ25DSjtRRG9DSSxlQUFlLEVBQUE7RUFyRC9CO1FBMERvQixxQkFBcUIsRUFBQTtFQTFEekM7UUErRGdCLFlBQVk7UUFDWixrQkFBa0I7UUFDbEIsNEJBQTRCLEVBQUE7RUFqRTVDO1FBcUVnQixZQUFZO1FBQ1osNEJBQTRCLEVBQUE7RUF0RTVDO1VBd0VvQixXQ3ZEUjtVRHdEUSxZQUFZLEVBQUE7RUF6RWhDO1lBMkV3Qix5QkFBeUIsRUFBQTtFQTNFakQ7WUErRXdCLGlCQUFpQixFQUFBO0VBL0V6QztNQXFGWSxrQkFBa0I7TUFDbEIsV0FBVztNQUNYLFdBQVc7TUFDWCx5QkN0RFEsRUFBQTtFRGxDcEI7TUE0Rlksa0JBQWtCLEVBQUE7RUE1RjlCO1FBOEZnQixXQUFXO1FBQ1gsY0FBYztRQUNkLFVBQVU7UUFDVixrQkFBa0I7UUFDbEIsTUFBTTtRQUNOLFNBQVM7UUFDVCxVQUFVO1FBQ1YsVUFBVTtRQUNWLGdCQUFnQixFQUFBO0VBdEdoQztRQTBHZ0Isa0JBQWtCO1FBQ2xCLGtCQUFrQixFQUFBO0VBM0dsQztVQThHb0Isa0JBQWtCO1VBQ2xCLFdBQVcsRUFBQTtFQS9HL0I7VUFrSG9CLGtCQUFrQixFQUFBO0VBbEh0QztZQW9Id0Isb0NBQ0osRUFBQTtFQXJIcEI7WUF3SHdCLFdBQVc7WUFDWCxjQUFjO1lBQ2QsV0FBVztZQUNYLGtCQUFrQjtZQUNsQixTQUFTO1lBQ1QsUUFBUTtZQUNSLGdCQUFnQjtZQUNoQixVQUFVO1lBQ1YsZ0JBQWdCO1lBQ2hCLFdBQVc7WUFDWCxXQUFXLEVBQUE7RUFsSW5DO1lBc0l3QixlQUFlO1lBQ2YsY0FBYztZQUNkLE1BQU07WUFDTixTQUFTO1lBQ1QsV0FBVztZQUNYLFdBQVc7WUFDWCxrQkFBa0I7WUFDbEIsVUFBVSxFQUFBO0VBN0lsQztjQWdKNEIsZUFBZTtjQUNmLFFBQVE7Y0FDUixnQkFBZ0I7Y0FDaEIsZ0JBQWdCO2NBQ2hCLG1CQUFtQjtjQUNuQixVQUFVO2NBQ1YsZ0JBQWdCO2NBQ2hCLGtCQUFrQjtjQUNsQixVQUFVO2NBQ1YsU0FBUztjQUNULGtCQUFrQixFQUFBO0VBMUo5QztZQStKd0IsdUJBQXVCLEVBQUE7RUEvSi9DO2NBaUs0QixnQkFBZ0IsRUFBQTtFQWpLNUM7Z0JBb0tnQyxjQ2pJWjtnQkRrSVksa0JBQWtCLEVBQUE7RUFyS2xEO1lBMkt3Qiw0QkFBNEIsRUFBQTtFQTNLcEQ7WUErS3dCLDRCQUE0QixFQUFBO0VBL0twRDtjQWlMNEIsdUJBQXVCO2NBQ3ZCLGNBQWMsRUFBQTtFQWxMMUM7Z0JBb0xnQyxpQkFBaUIsRUFBQTtFQXBMakQ7Z0JBd0xnQyxjQzVLaEIsRUFBQTtFRFpoQjtVQWdNd0IsV0FBVztVQUNYLGNBQWM7VUFDZCxVQUFVO1VBQ1Ysa0JBQWtCO1VBQ2xCLFFBQVE7VUFDUixVQUFVO1VBQ1YsUUFBUTtVQUNSLFdBQVc7VUFDWCxTQUFTO1VBQ1QsbUJDdktKLEVBQUE7RURsQ3BCO01BaU5nQixXQUFXO01BQ1gsY0FBYztNQUNkLFVBQVU7TUFDVixrQkFBa0I7TUFDbEIsT0FBTztNQUNQLFVBQVU7TUFDVixRQUFRO01BQ1IsV0FBVztNQUNYLFNBQVM7TUFDVCxtQkN4TEksRUFBQTtFRGxDcEI7TUFpT29CLGdCQUFnQixFQUFBO0VBak9wQztNQXVPWSw2QkFBNkIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvdGFzay90YXNrLW1hbmFnZW1lbnQvdGFzay1saWJyYXJ5L3Rhc2stbGlicmFyeS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi8uLi8uLi8uLi9hc3NldHMvc3R5bGVzL2NvbmZpZ1wiO1xyXG5cclxuLnRhc2tzLWxpYnJhcnkge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDRweCAhaW1wb3J0YW50O1xyXG4gICAgLW1vei1ib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgIC1tcy1ib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICAtby1ib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICBtaW4taGVpZ2h0OiAzMnB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGxpZ2h0LWJsdWU7XHJcbiAgICBwYWRkaW5nOiAxMnB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTJweDtcclxuXHJcbiAgICBpIHtcclxuICAgICAgICBmb250LXdlaWdodDogbm9ybWFsICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgLnRhYmxlLXRyZWUge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMnB4O1xyXG5cclxuICAgICAgICAuY2VsbCB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICAgZGlzcGxheTogdGFibGUtY2VsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5jZWxsLWdyb3VwIHtcclxuICAgICAgICAgICAgbWluLXdpZHRoOiAyMDBweDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5yb3ctdHIge1xyXG4gICAgICAgICAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2RkZDtcclxuICAgICAgICAgICAgb3ZlcmZsb3c6IHZpc2libGU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAudHQtaGVhZGVyIHtcclxuICAgICAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkZGFya0JsdWU7XHJcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgICBjb2xvcjogJHdoaXRlO1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA0cHggIWltcG9ydGFudDtcclxuXHJcbiAgICAgICAgICAgIC5pY29uIHtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAkd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGEge1xyXG4gICAgICAgICAgICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAuY2VsbC1wcm9ncmVzcy1tZXRob2Qge1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyLWxlZnQ6IDFweCBkYXNoZWQgI2RkZDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLmNlbGwtYWN0aW9uLXByaW9yaXR5IHtcclxuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICAgICAgICAgIGJvcmRlci1sZWZ0OiAxcHggZGFzaGVkICNkZGQ7XHJcbiAgICAgICAgICAgICAgICBhIHtcclxuICAgICAgICAgICAgICAgICAgICBjb2xvcjogJHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDJweDtcclxuICAgICAgICAgICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICRvcmFuZ2UgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC5mYSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogM3B4O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAudHQtYm9keSB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkbGlnaHQtYmx1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5wYXJlbnQtdHJlZSB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICAgJjpiZWZvcmUge1xyXG4gICAgICAgICAgICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDFweDtcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgICAgIHRvcDogMDtcclxuICAgICAgICAgICAgICAgIGJvdHRvbTogMDtcclxuICAgICAgICAgICAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgICAgICAgICAgICBsZWZ0OiAxNHB4O1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogI2JiYjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLmNoaWxkcmVuIHtcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMjhweDtcclxuXHJcbiAgICAgICAgICAgICAgICAudHJlZS1jb250YWluZXIge1xyXG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC5nb2FsLWxpbmUge1xyXG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2RmZjBmZCAhaW1wb3J0YW50XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAmOjpiZWZvcmUge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDE0cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYm90dG9tOiAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogLTFweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgei1pbmRleDogMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogI2JiYjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGVmdDogLTE0cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMXB4O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLmdvYWwtdHJlZS1jYXJldCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM4YzhjOGM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvcDogMDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYm90dG9tOiAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMjdweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGVmdDogLTI4cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgei1pbmRleDogMjtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAuNmVtO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9wOiA1MCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAtOHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMmVtO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDJlbTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHotaW5kZXg6IDI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZWZ0OiA2cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC5ndy1jZWxsLWl0ZW0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiA2cHggMCA2cHggMTJweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmd3LWl0ZW0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMHB4IDZweDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZ3ctZGVzY3JpcHRpb24ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAkYnJpZ2h0R3JheTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb250LXN0eWxlOiBpdGFsaWM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC5ndy1jZWxsLXByb2dyZXNzLW1ldGhvZCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1sZWZ0OiAxcHggZGFzaGVkICNkZGQ7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAuZ3ctY2VsbC1hY3Rpb24tcHJpb3JpdHkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItbGVmdDogMXB4IGRhc2hlZCAjZGRkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDJweCAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICMwMDcyYmM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDNweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogJG9yYW5nZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAubGFzdC1nb2FsLWxpbmV7XHJcbiAgICAgICAgICAgICAgICAgICAgJjo6YWZ0ZXIge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDFweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgei1pbmRleDogMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmlnaHQ6IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxlZnQ6IC0xNHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBib3R0b206IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICRsaWdodC1ibHVlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmxhc3QtcGFyZW50LXRyZWV7XHJcbiAgICAgICAgICAgICY6OmFmdGVyIHtcclxuICAgICAgICAgICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxcHg7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgICAgICB0b3A6IDAlO1xyXG4gICAgICAgICAgICAgICAgei1pbmRleDogMTtcclxuICAgICAgICAgICAgICAgIHJpZ2h0OiAwO1xyXG4gICAgICAgICAgICAgICAgbGVmdDogLTE0cHg7XHJcbiAgICAgICAgICAgICAgICBib3R0b206IDA7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAkbGlnaHQtYmx1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgJjpmaXJzdC1jaGlsZCB7XHJcbiAgICAgICAgICAgIC50cmVlX2NvbnRhaW5lciB7XHJcbiAgICAgICAgICAgICAgICAuZ29hbC1saW5lIHtcclxuICAgICAgICAgICAgICAgICAgICBib3JkZXItdG9wOiBub25lO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuYmctaW5mby1ibHVlIHtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4iLCIkZGVmYXVsdC1jb2xvcjogIzIyMjtcclxuJGZvbnQtZmFtaWx5OiBcIkFyaWFsXCIsIHRhaG9tYSwgSGVsdmV0aWNhIE5ldWU7XHJcbiRjb2xvci1ibHVlOiAjMzU5OGRjO1xyXG4kbWFpbi1jb2xvcjogIzAwNzQ1NTtcclxuJGJvcmRlckNvbG9yOiAjZGRkO1xyXG4kc2Vjb25kLWNvbG9yOiAjYjAxYTFmO1xyXG4kdGFibGUtYmFja2dyb3VuZC1jb2xvcjogIzAwOTY4ODtcclxuJGJsdWU6ICMwMDdiZmY7XHJcbiRkYXJrLWJsdWU6ICMwMDcyQkM7XHJcbiRicmlnaHQtYmx1ZTogI2RmZjBmZDtcclxuJGluZGlnbzogIzY2MTBmMjtcclxuJHB1cnBsZTogIzZmNDJjMTtcclxuJHBpbms6ICNlODNlOGM7XHJcbiRyZWQ6ICNkYzM1NDU7XHJcbiRvcmFuZ2U6ICNmZDdlMTQ7XHJcbiR5ZWxsb3c6ICNmZmMxMDc7XHJcbiRncmVlbjogIzI4YTc0NTtcclxuJHRlYWw6ICMyMGM5OTc7XHJcbiRjeWFuOiAjMTdhMmI4O1xyXG4kd2hpdGU6ICNmZmY7XHJcbiRncmF5OiAjODY4ZTk2O1xyXG4kZ3JheS1kYXJrOiAjMzQzYTQwO1xyXG4kcHJpbWFyeTogIzAwN2JmZjtcclxuJHNlY29uZGFyeTogIzZjNzU3ZDtcclxuJHN1Y2Nlc3M6ICMyOGE3NDU7XHJcbiRpbmZvOiAjMTdhMmI4O1xyXG4kd2FybmluZzogI2ZmYzEwNztcclxuJGRhbmdlcjogI2RjMzU0NTtcclxuJGxpZ2h0OiAjZjhmOWZhO1xyXG4kZGFyazogIzM0M2E0MDtcclxuJGxhYmVsLWNvbG9yOiAjNjY2O1xyXG4kYmFja2dyb3VuZC1jb2xvcjogI0VDRjBGMTtcclxuJGJvcmRlckFjdGl2ZUNvbG9yOiAjODBiZGZmO1xyXG4kYm9yZGVyUmFkaXVzOiAwO1xyXG4kZGFya0JsdWU6ICM0NUEyRDI7XHJcbiRsaWdodEdyZWVuOiAjMjdhZTYwO1xyXG4kbGlnaHQtYmx1ZTogI2Y1ZjdmNztcclxuJGJyaWdodEdyYXk6ICM3NTc1NzU7XHJcbiRtYXgtd2lkdGgtbW9iaWxlOiA3NjhweDtcclxuJG1heC13aWR0aC10YWJsZXQ6IDk5MnB4O1xyXG4kbWF4LXdpZHRoLWRlc2t0b3A6IDEyODBweDtcclxuXHJcbi8vIEJFR0lOOiBNYXJnaW5cclxuQG1peGluIG5oLW1nKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1ndCgkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi10b3A6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWdiKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ2woJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tbGVmdDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ3IoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vLyBFTkQ6IE1hcmdpblxyXG5cclxuLy8gQkVHSU46IFBhZGRpbmdcclxuQG1peGluIG5oLXBkKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZzogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZHQoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLXRvcDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZGIoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZGwoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGRyKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy1yaWdodDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogUGFkZGluZ1xyXG5cclxuLy8gQkVHSU46IFdpZHRoXHJcbkBtaXhpbiBuaC13aWR0aCgkd2lkdGgpIHtcclxuICAgIHdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxuICAgIG1pbi13aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbiAgICBtYXgtd2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vLyBFTkQ6IFdpZHRoXHJcblxyXG4vLyBCRUdJTjogSWNvbiBTaXplXHJcbkBtaXhpbiBuaC1zaXplLWljb24oJHNpemUpIHtcclxuICAgIHdpZHRoOiAkc2l6ZTtcclxuICAgIGhlaWdodDogJHNpemU7XHJcbiAgICBmb250LXNpemU6ICRzaXplO1xyXG59XHJcblxyXG4vLyBFTkQ6IEljb24gU2l6ZVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-library/task-library.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-library/task-library.component.ts ***!
  \*************************************************************************************/
/*! exports provided: TaskLibraryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskLibraryComponent", function() { return TaskLibraryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _task_group_library_form_task_group_library_form_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./task-group-library-form/task-group-library-form.component */ "./src/app/modules/task/task-management/task-library/task-group-library-form/task-group-library-form.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _task_library_form_task_library_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./task-library-form/task-library-form.component */ "./src/app/modules/task/task-management/task-library/task-library-form/task-library-form.component.ts");
/* harmony import */ var _services_task_library_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/task-library.service */ "./src/app/modules/task/task-management/task-library/services/task-library.service.ts");
/* harmony import */ var _task_check_list_form_task_check_list_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./task-check-list-form/task-check-list-form.component */ "./src/app/modules/task/task-management/task-library/task-check-list-form/task-check-list-form.component.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _models_task_library_viewmodel__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./models/task-library.viewmodel */ "./src/app/modules/task/task-management/task-library/models/task-library.viewmodel.ts");
/* harmony import */ var _viewmodel_task_group_library_viewmodel__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./viewmodel/task-group-library.viewmodel */ "./src/app/modules/task/task-management/task-library/viewmodel/task-group-library.viewmodel.ts");
/* harmony import */ var _services_task_check_list_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./services/task-check-list.service */ "./src/app/modules/task/task-management/task-library/services/task-check-list.service.ts");
/* harmony import */ var _shareds_constants_task_type_const__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../../shareds/constants/task-type.const */ "./src/app/shareds/constants/task-type.const.ts");
/* harmony import */ var _models_check_list_viewmodel__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./models/check-list.viewmodel */ "./src/app/modules/task/task-management/task-library/models/check-list.viewmodel.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
















var TaskLibraryComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TaskLibraryComponent, _super);
    function TaskLibraryComponent(data, dialog, route, utilService, taskLibraryService, taskCheckListService) {
        var _this = _super.call(this) || this;
        _this.data = data;
        _this.dialog = dialog;
        _this.route = route;
        _this.utilService = utilService;
        _this.taskLibraryService = taskLibraryService;
        _this.taskCheckListService = taskCheckListService;
        return _this;
    }
    TaskLibraryComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.data && this.data.isSelect) {
            this.isAllowSelect = this.data.isSelect;
            this.search();
        }
        else {
            this.appService.setupPage(this.pageId.TASK, this.pageId.TASK_LIBRARY, 'Danh sách công việc mẫu', 'Quản lý công việc');
            this.subscribers.data = this.route.data.subscribe(function (result) {
                if (result.data) {
                    _this.listItems = result.data.items;
                }
            });
        }
    };
    TaskLibraryComponent.prototype.addGroup = function () {
        this.taskGroupLibraryFormComponent.add();
    };
    TaskLibraryComponent.prototype.search = function () {
        var _this = this;
        this.subscribers.data = this.taskLibraryService.search().subscribe(function (result) {
            _this.listItems = result.items;
        });
    };
    TaskLibraryComponent.prototype.updateGroup = function (taskGroup) {
        this.taskGroupLibraryFormComponent.update(taskGroup.id);
    };
    TaskLibraryComponent.prototype.deleteGroup = function (id) {
        var _this = this;
        this.taskLibraryService.deleteGroup(id).subscribe(function (result) {
            lodash__WEBPACK_IMPORTED_MODULE_8__["remove"](_this.listItems, function (item) {
                return item.id === id;
            });
        });
    };
    TaskLibraryComponent.prototype.addTaskLibrary = function (taskGroup) {
        var listGroup = lodash__WEBPACK_IMPORTED_MODULE_8__["map"](this.listItems, function (item) {
            return { id: item.id, name: item.name };
        });
        this.taskLibraryFormComponent.add(taskGroup, listGroup);
    };
    TaskLibraryComponent.prototype.updateTaskLibrary = function (taskLibrary) {
        var listGroup = lodash__WEBPACK_IMPORTED_MODULE_8__["map"](this.listItems, function (item) {
            return { id: item.id, name: item.name };
        });
        this.taskLibraryFormComponent.update(taskLibrary, listGroup);
    };
    TaskLibraryComponent.prototype.deleteTaskLibrary = function (taskLibrary) {
        var _this = this;
        this.taskLibraryService.delete(taskLibrary.id).subscribe(function () {
            var taskGroupInfo = lodash__WEBPACK_IMPORTED_MODULE_8__["find"](_this.listItems, function (item) {
                return item.id === taskLibrary.taskGroupLibraryId;
            });
            if (taskGroupInfo) {
                lodash__WEBPACK_IMPORTED_MODULE_8__["remove"](taskGroupInfo.tasksLibraries, function (task) {
                    return task.id === taskLibrary.id;
                });
            }
        });
    };
    // add  checkList
    TaskLibraryComponent.prototype.addCheckList = function (task, taskGroupId) {
        this.taskCheckListComponent.add(task, taskGroupId);
    };
    TaskLibraryComponent.prototype.showCheckList = function (taskLibrary) {
        taskLibrary.isShowCheckList = !taskLibrary.isShowCheckList;
        if ((!taskLibrary.checkLists || taskLibrary.checkLists.length === 0) && taskLibrary.isShowCheckList) {
            this.taskCheckListService.searchByTaskType(taskLibrary.id, _shareds_constants_task_type_const__WEBPACK_IMPORTED_MODULE_12__["TaskType"].taskLibrary)
                .subscribe(function (result) {
                taskLibrary.checkLists = result.items;
            });
        }
    };
    TaskLibraryComponent.prototype.deleteCheckList = function (checkList, taskLibrary) {
        this.taskCheckListService.delete(checkList.id).subscribe(function () {
            lodash__WEBPACK_IMPORTED_MODULE_8__["remove"](taskLibrary.checkLists, function (item) {
                return item.id === checkList.id;
            });
        });
    };
    TaskLibraryComponent.prototype.updateCheckList = function (checkList, taskGroupId) {
        this.taskCheckListComponent.update(checkList, taskGroupId);
    };
    TaskLibraryComponent.prototype.onSaveSuccessTaskGroupLibrary = function (value) {
        var taskGroupInfo = lodash__WEBPACK_IMPORTED_MODULE_8__["find"](this.listItems, function (item) {
            return item.id === value.id;
        });
        if (taskGroupInfo) {
            taskGroupInfo.concurrencyStamp = value.concurrencyStamp;
            taskGroupInfo.name = value.name;
            taskGroupInfo.description = value.description;
            if (taskGroupInfo.tasksLibraries) {
                lodash__WEBPACK_IMPORTED_MODULE_8__["each"](taskGroupInfo.tasksLibraries, function (task) {
                    task.taskGroupLibraryName = value.name;
                });
            }
        }
        else {
            this.listItems.push(new _viewmodel_task_group_library_viewmodel__WEBPACK_IMPORTED_MODULE_10__["TaskGroupLibraryViewModel"](value.id, value.name, true, value.concurrencyStamp, []));
        }
    };
    TaskLibraryComponent.prototype.onSaveSuccessTaskLibrary = function (value) {
        var taskInfo;
        lodash__WEBPACK_IMPORTED_MODULE_8__["each"](this.listItems, function (item) {
            var info = lodash__WEBPACK_IMPORTED_MODULE_8__["find"](item.tasksLibraries, function (task) {
                return task.id === value.id;
            });
            if (info) {
                taskInfo = info;
            }
        });
        if (!taskInfo) {
            var taskGroupInfo = lodash__WEBPACK_IMPORTED_MODULE_8__["find"](this.listItems, function (item) {
                return item.id === value.taskGroupLibraryId;
            });
            if (!taskGroupInfo.tasksLibraries) {
                taskGroupInfo.tasksLibraries = [];
            }
            taskGroupInfo.tasksLibraries.push(new _models_task_library_viewmodel__WEBPACK_IMPORTED_MODULE_9__["TaskLibraryViewModel"](value.id, value.name, value.description, value.concurrencyStamp, value.methodCalculateResult, taskGroupInfo.id, taskGroupInfo.name));
            taskGroupInfo.isShowTask = true;
        }
        else {
            lodash__WEBPACK_IMPORTED_MODULE_8__["each"](this.listItems, function (item) {
                lodash__WEBPACK_IMPORTED_MODULE_8__["remove"](item.tasksLibraries, function (items) {
                    return items.id === value.id;
                });
            });
            taskInfo.name = value.name;
            taskInfo.description = value.description;
            taskInfo.concurrencyStamp = value.concurrencyStamp;
            taskInfo.methodCalculateResult = value.methodCalculateResult;
            taskInfo.taskGroupLibraryName = value.taskGroupLibraryName;
            if (taskInfo.checkLists) {
                lodash__WEBPACK_IMPORTED_MODULE_8__["each"](taskInfo.checkLists, function (checkList) {
                    checkList.taskName = value.name;
                });
            }
            var taskGroupInfo = lodash__WEBPACK_IMPORTED_MODULE_8__["find"](this.listItems, function (item) {
                return item.id === value.taskGroupLibraryId;
            });
            taskGroupInfo.tasksLibraries.push(new _models_task_library_viewmodel__WEBPACK_IMPORTED_MODULE_9__["TaskLibraryViewModel"](value.id, value.name, value.description, value.concurrencyStamp, value.methodCalculateResult, taskGroupInfo.id, taskGroupInfo.name));
            taskGroupInfo.isShowTask = true;
        }
    };
    TaskLibraryComponent.prototype.onSaveSuccessTaskCheckList = function (value) {
        var taskGroupInfo = lodash__WEBPACK_IMPORTED_MODULE_8__["find"](this.listItems, function (item) {
            return item.id === value.taskGroupLibraryId;
        });
        if (taskGroupInfo) {
            var taskInfo_1 = lodash__WEBPACK_IMPORTED_MODULE_8__["find"](taskGroupInfo.tasksLibraries, function (task) {
                return task.id === value.checkList.taskId;
            });
            if (taskInfo_1) {
                if (!taskInfo_1.isShowCheckList) {
                    this.taskCheckListService.searchByTaskType(taskInfo_1.id, _shareds_constants_task_type_const__WEBPACK_IMPORTED_MODULE_12__["TaskType"].taskLibrary)
                        .subscribe(function (result) {
                        taskInfo_1.checkLists = result.items;
                        taskInfo_1.isShowCheckList = true;
                    });
                }
                var checkListInfo = lodash__WEBPACK_IMPORTED_MODULE_8__["find"](taskInfo_1.checkLists, function (checkList) {
                    return checkList.id === value.checkList.id;
                });
                var checkListEmit = value.checkList;
                if (!checkListInfo) {
                    if (!taskInfo_1.checkLists) {
                        taskInfo_1.checkLists = [];
                    }
                    if (checkListEmit) {
                        taskInfo_1.checkLists.push(new _models_check_list_viewmodel__WEBPACK_IMPORTED_MODULE_13__["CheckListViewModel"](checkListEmit.id, checkListEmit.taskId, checkListEmit.taskName, checkListEmit.name, checkListEmit.type, checkListEmit.status, checkListEmit.concurrencyStamp));
                    }
                }
                else {
                    checkListInfo.name = checkListEmit.name;
                    checkListInfo.concurrencyStamp = checkListEmit.concurrencyStamp;
                }
            }
        }
    };
    TaskLibraryComponent.prototype.closeDialog = function () {
        var dialogTaskLibrary = this.dialog.getDialogById('taskLibrary');
        if (dialogTaskLibrary) {
            dialogTaskLibrary.close({ taskLibrary: null });
        }
    };
    TaskLibraryComponent.prototype.selectTaskLibrary = function (task) {
        var dialogTaskLibrary = this.dialog.getDialogById('taskLibrary');
        if (dialogTaskLibrary) {
            dialogTaskLibrary.close({ taskLibrary: task });
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_task_group_library_form_task_group_library_form_component__WEBPACK_IMPORTED_MODULE_3__["TaskGroupLibraryFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _task_group_library_form_task_group_library_form_component__WEBPACK_IMPORTED_MODULE_3__["TaskGroupLibraryFormComponent"])
    ], TaskLibraryComponent.prototype, "taskGroupLibraryFormComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_task_library_form_task_library_form_component__WEBPACK_IMPORTED_MODULE_5__["TaskLibraryFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _task_library_form_task_library_form_component__WEBPACK_IMPORTED_MODULE_5__["TaskLibraryFormComponent"])
    ], TaskLibraryComponent.prototype, "taskLibraryFormComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_task_check_list_form_task_check_list_form_component__WEBPACK_IMPORTED_MODULE_7__["TaskCheckListFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _task_check_list_form_task_check_list_form_component__WEBPACK_IMPORTED_MODULE_7__["TaskCheckListFormComponent"])
    ], TaskLibraryComponent.prototype, "taskCheckListComponent", void 0);
    TaskLibraryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-group-library',
            template: __webpack_require__(/*! ./task-library.component.html */ "./src/app/modules/task/task-management/task-library/task-library.component.html"),
            styles: [__webpack_require__(/*! ./task-library.component.scss */ "./src/app/modules/task/task-management/task-library/task-library.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Optional"])()), tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_14__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatDialog"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_15__["UtilService"],
            _services_task_library_service__WEBPACK_IMPORTED_MODULE_6__["TaskLibraryService"],
            _services_task_check_list_service__WEBPACK_IMPORTED_MODULE_11__["TaskCheckListService"]])
    ], TaskLibraryComponent);
    return TaskLibraryComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/task/task-management/task-library/viewmodel/task-group-library.viewmodel.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-library/viewmodel/task-group-library.viewmodel.ts ***!
  \*****************************************************************************************************/
/*! exports provided: TaskGroupLibraryViewModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskGroupLibraryViewModel", function() { return TaskGroupLibraryViewModel; });
var TaskGroupLibraryViewModel = /** @class */ (function () {
    function TaskGroupLibraryViewModel(id, name, isShowTask, concurrencyStamp, tasksLibraries) {
        this.id = id;
        this.name = name;
        this.isShowTask = isShowTask;
        this.concurrencyStamp = concurrencyStamp;
        this.tasksLibraries = tasksLibraries;
    }
    return TaskGroupLibraryViewModel;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-list/task-group-tree/task-group-tree.component.html":
/*!*******************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-list/task-group-tree/task-group-tree.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"table-tree\">\n    <div class=\"tt-header\"\n         (click)=\"isShowTaskChild = !isShowTaskChild\">\n        <div class=\"cell cell-group middle w100pc cm-pd-12-6\">\n            <a class=\"icon cm-mgl-5\">\n                <i class=\"fa fa-caret-down\" aria-hidden=\"true\" *ngIf=\"isShowTaskChild\"></i>\n                <i class=\"fa fa-caret-right\" aria-hidden=\"true\" *ngIf=\"!isShowTaskChild\"></i>\n               Công việc con\n            </a>\n        </div>\n        <div class=\"cell w150 middle center hidden-xs\">\n            <span i18n=\"@@result\">Ngày bắt đầu</span>\n        </div>\n        <div class=\"cell w150 middle center\">\n            <span i18n=\"@@status\">Ngày kết thúc</span>\n        </div>\n        <div class=\"cell w120 middle center\">\n            <span i18n=\"@@status\">Trạng thái</span>\n        </div>\n    </div>\n    <div class=\"tt-body cm-pdb-0\" *ngIf=\"listData && listData.length > 0 && isShowTaskChild\">\n        <task-tree [data]=\"listData\"></task-tree>\n    </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-list/task-group-tree/task-group-tree.component.scss":
/*!*******************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-list/task-group-tree/task-group-tree.component.scss ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".table-tree {\n  position: relative;\n  float: left;\n  width: 100%;\n  margin-bottom: 12px; }\n  .table-tree .cell {\n    position: relative;\n    display: table-cell; }\n  .table-tree .cell-group {\n    min-width: 200px; }\n  .table-tree .row-tr {\n    border-top: 1px solid #ddd;\n    overflow: visible; }\n  .table-tree .tt-header {\n    padding: 0;\n    font-weight: normal;\n    line-height: 20px;\n    position: relative;\n    float: left;\n    width: 100%;\n    background-color: #45A2D2;\n    cursor: pointer;\n    font-size: 14px;\n    color: #fff; }\n  .table-tree .tt-header .icon {\n      color: #fff;\n      font-size: 16px; }\n  .table-tree .tt-header a {\n      color: white; }\n  .table-tree .tt-header a:hover {\n        text-decoration: none; }\n  .table-tree .tt-body {\n    position: relative;\n    float: left;\n    width: 100%;\n    background-color: #f5f7f7; }\n  .table-tree:first-child .tree_container .goal-line {\n    border-top: none; }\n  .table-tree .bg-info-blue {\n    border-radius: 4px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC90YXNrLWxpc3QvdGFzay1ncm91cC10cmVlL0Q6XFxQcm9qZWN0XFxHaG1BcHBsaWNhdGlvblxcY2xpZW50c1xcZ2htYXBwbGljYXRpb25jbGllbnQvc3JjXFxhcHBcXG1vZHVsZXNcXHRhc2tcXHRhc2stbWFuYWdlbWVudFxcdGFzay1saXN0XFx0YXNrLWdyb3VwLXRyZWVcXHRhc2stZ3JvdXAtdHJlZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC90YXNrLWxpc3QvdGFzay1ncm91cC10cmVlL0Q6XFxQcm9qZWN0XFxHaG1BcHBsaWNhdGlvblxcY2xpZW50c1xcZ2htYXBwbGljYXRpb25jbGllbnQvc3JjXFxhc3NldHNcXHN0eWxlc1xcX2NvbmZpZy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxXQUFXO0VBQ1gsbUJBQW1CLEVBQUE7RUFKdkI7SUFPUSxrQkFBa0I7SUFDbEIsbUJBQW1CLEVBQUE7RUFSM0I7SUFZUSxnQkFBZ0IsRUFBQTtFQVp4QjtJQWdCUSwwQkFBMEI7SUFDMUIsaUJBQWlCLEVBQUE7RUFqQnpCO0lBcUJRLFVBQVU7SUFDVixtQkFBbUI7SUFDbkIsaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsV0FBVztJQUNYLHlCQ0tVO0lESlYsZUFBZTtJQUNmLGVBQWU7SUFDZixXQ2JJLEVBQUE7RURqQlo7TUFpQ1ksV0NoQkE7TURpQkEsZUFBZSxFQUFBO0VBbEMzQjtNQTBDWSxZQUFZLEVBQUE7RUExQ3hCO1FBdUNnQixxQkFBcUIsRUFBQTtFQXZDckM7SUE4Q1Esa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxXQUFXO0lBQ1gseUJDZlksRUFBQTtFRGxDcEI7SUF1RGdCLGdCQUFnQixFQUFBO0VBdkRoQztJQTZEUSw2QkFBNkIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvdGFzay90YXNrLW1hbmFnZW1lbnQvdGFzay1saXN0L3Rhc2stZ3JvdXAtdHJlZS90YXNrLWdyb3VwLXRyZWUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0ICcuLi8uLi8uLi8uLi8uLi8uLi9hc3NldHMvc3R5bGVzL2NvbmZpZyc7XHJcblxyXG4udGFibGUtdHJlZSB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTJweDtcclxuXHJcbiAgICAuY2VsbCB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIGRpc3BsYXk6IHRhYmxlLWNlbGw7XHJcbiAgICB9XHJcblxyXG4gICAgLmNlbGwtZ3JvdXAge1xyXG4gICAgICAgIG1pbi13aWR0aDogMjAwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLnJvdy10ciB7XHJcbiAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkZGQ7XHJcbiAgICAgICAgb3ZlcmZsb3c6IHZpc2libGU7XHJcbiAgICB9XHJcblxyXG4gICAgLnR0LWhlYWRlciB7XHJcbiAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkZGFya0JsdWU7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICBjb2xvcjogJHdoaXRlO1xyXG5cclxuICAgICAgICAuaWNvbiB7XHJcbiAgICAgICAgICAgIGNvbG9yOiAkd2hpdGU7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGEge1xyXG4gICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC50dC1ib2R5IHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGxpZ2h0LWJsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgJjpmaXJzdC1jaGlsZCB7XHJcbiAgICAgICAgLnRyZWVfY29udGFpbmVyIHtcclxuICAgICAgICAgICAgLmdvYWwtbGluZSB7XHJcbiAgICAgICAgICAgICAgICBib3JkZXItdG9wOiBub25lO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5iZy1pbmZvLWJsdWUge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDRweCAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG59XHJcbiIsIiRkZWZhdWx0LWNvbG9yOiAjMjIyO1xyXG4kZm9udC1mYW1pbHk6IFwiQXJpYWxcIiwgdGFob21hLCBIZWx2ZXRpY2EgTmV1ZTtcclxuJGNvbG9yLWJsdWU6ICMzNTk4ZGM7XHJcbiRtYWluLWNvbG9yOiAjMDA3NDU1O1xyXG4kYm9yZGVyQ29sb3I6ICNkZGQ7XHJcbiRzZWNvbmQtY29sb3I6ICNiMDFhMWY7XHJcbiR0YWJsZS1iYWNrZ3JvdW5kLWNvbG9yOiAjMDA5Njg4O1xyXG4kYmx1ZTogIzAwN2JmZjtcclxuJGRhcmstYmx1ZTogIzAwNzJCQztcclxuJGJyaWdodC1ibHVlOiAjZGZmMGZkO1xyXG4kaW5kaWdvOiAjNjYxMGYyO1xyXG4kcHVycGxlOiAjNmY0MmMxO1xyXG4kcGluazogI2U4M2U4YztcclxuJHJlZDogI2RjMzU0NTtcclxuJG9yYW5nZTogI2ZkN2UxNDtcclxuJHllbGxvdzogI2ZmYzEwNztcclxuJGdyZWVuOiAjMjhhNzQ1O1xyXG4kdGVhbDogIzIwYzk5NztcclxuJGN5YW46ICMxN2EyYjg7XHJcbiR3aGl0ZTogI2ZmZjtcclxuJGdyYXk6ICM4NjhlOTY7XHJcbiRncmF5LWRhcms6ICMzNDNhNDA7XHJcbiRwcmltYXJ5OiAjMDA3YmZmO1xyXG4kc2Vjb25kYXJ5OiAjNmM3NTdkO1xyXG4kc3VjY2VzczogIzI4YTc0NTtcclxuJGluZm86ICMxN2EyYjg7XHJcbiR3YXJuaW5nOiAjZmZjMTA3O1xyXG4kZGFuZ2VyOiAjZGMzNTQ1O1xyXG4kbGlnaHQ6ICNmOGY5ZmE7XHJcbiRkYXJrOiAjMzQzYTQwO1xyXG4kbGFiZWwtY29sb3I6ICM2NjY7XHJcbiRiYWNrZ3JvdW5kLWNvbG9yOiAjRUNGMEYxO1xyXG4kYm9yZGVyQWN0aXZlQ29sb3I6ICM4MGJkZmY7XHJcbiRib3JkZXJSYWRpdXM6IDA7XHJcbiRkYXJrQmx1ZTogIzQ1QTJEMjtcclxuJGxpZ2h0R3JlZW46ICMyN2FlNjA7XHJcbiRsaWdodC1ibHVlOiAjZjVmN2Y3O1xyXG4kYnJpZ2h0R3JheTogIzc1NzU3NTtcclxuJG1heC13aWR0aC1tb2JpbGU6IDc2OHB4O1xyXG4kbWF4LXdpZHRoLXRhYmxldDogOTkycHg7XHJcbiRtYXgtd2lkdGgtZGVza3RvcDogMTI4MHB4O1xyXG5cclxuLy8gQkVHSU46IE1hcmdpblxyXG5AbWl4aW4gbmgtbWcoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW46ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWd0KCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLXRvcDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ2IoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1nbCgkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1ncigkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1yaWdodDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogTWFyZ2luXHJcblxyXG4vLyBCRUdJTjogUGFkZGluZ1xyXG5AbWl4aW4gbmgtcGQoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkdCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctdG9wOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkYigkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctYm90dG9tOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkbCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctbGVmdDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZHIoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBQYWRkaW5nXHJcblxyXG4vLyBCRUdJTjogV2lkdGhcclxuQG1peGluIG5oLXdpZHRoKCR3aWR0aCkge1xyXG4gICAgd2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG4gICAgbWluLXdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxuICAgIG1heC13aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogV2lkdGhcclxuXHJcbi8vIEJFR0lOOiBJY29uIFNpemVcclxuQG1peGluIG5oLXNpemUtaWNvbigkc2l6ZSkge1xyXG4gICAgd2lkdGg6ICRzaXplO1xyXG4gICAgaGVpZ2h0OiAkc2l6ZTtcclxuICAgIGZvbnQtc2l6ZTogJHNpemU7XHJcbn1cclxuXHJcbi8vIEVORDogSWNvbiBTaXplXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-list/task-group-tree/task-group-tree.component.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-list/task-group-tree/task-group-tree.component.ts ***!
  \*****************************************************************************************************/
/*! exports provided: TaskGroupTreeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskGroupTreeComponent", function() { return TaskGroupTreeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TaskGroupTreeComponent = /** @class */ (function () {
    function TaskGroupTreeComponent() {
        this.isShowTaskChild = true;
    }
    TaskGroupTreeComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], TaskGroupTreeComponent.prototype, "listData", void 0);
    TaskGroupTreeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'task-group-tree',
            template: __webpack_require__(/*! ./task-group-tree.component.html */ "./src/app/modules/task/task-management/task-list/task-group-tree/task-group-tree.component.html"),
            styles: [__webpack_require__(/*! ./task-group-tree.component.scss */ "./src/app/modules/task/task-management/task-list/task-group-tree/task-group-tree.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TaskGroupTreeComponent);
    return TaskGroupTreeComponent;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-list/task-group-tree/task-tree/task-tree.component.html":
/*!***********************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-list/task-group-tree/task-tree/task-tree.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"parent-tree\">\n    <div class=\"children\">\n        <div class=\"tree-container\">\n            <ng-template #recursiveTree let-data>\n                <ng-container *ngFor=\"let task of data; let indexTask = index\">\n                    <div class=\"goal-line row-tr bg-white\"\n                         [class.last-goal-line]=\"indexTask + 1 === data.length\">\n                        <div class=\"goal-tree-caret\" *ngIf=\"task.childCount > 0\"\n                             (click)=\"task.state.opened = !task.state.opened\">\n                            <i class=\"fa fa-fw fa-chevron-down\" *ngIf=\"task.state.opened\"></i>\n                            <i class=\"fa fa-fw fa-chevron-right\" *ngIf=\"!task.state.opened\"></i>\n                        </div>\n                        <div class=\"cell gw-cell-item w100pc middle\">\n                            <div class=\"gw-item\">\n                                <app-task-list-item\n                                    [task]=\"task.data\"></app-task-list-item>\n                            </div>\n                        </div>\n                        <div class=\"cell border-left-dashed middle center w150 hidden-xs\">\n                            <i class=\"fa fa-calendar\"> </i> {{task?.data?.estimateStartDate|\n                            dateTimeFormat: 'DD/MM/YYYY'}}\n                        </div>\n                        <div class=\"cell border-left-dashed middle center w150\">\n                            <i class=\"fa fa-calendar\"> </i> {{task?.data?.estimateEndDate|\n                            dateTimeFormat: 'DD/MM/YYYY'}}\n                        </div>\n                        <div class=\"cell border-left-dashed middle center w120\">\n                            <span>{task.data.status, select, 0{Chưa bắt đầu} 1{Đang tiến hành} 2 {Hoàn thành} 3{Hủy} 4 {Chờ duyệt} 5 {Từ chối}}</span>\n                        </div>\n                    </div>\n                    <div class=\"parent-tree\" *ngIf=\"task.childCount > 0 && task.state.opened\"\n                         [class.last-parent-tree]=\"indexTask + 1 === data.length\">\n                        <div class=\"children\">\n                            <div class=\"tree-container\">\n                                <ng-container\n                                    *ngTemplateOutlet=\"recursiveTree; context:{ $implicit: task.children }\"></ng-container>\n                            </div>\n                        </div>\n                    </div>\n                </ng-container>\n            </ng-template>\n            <ng-container *ngTemplateOutlet=\"recursiveTree; context:{ $implicit: data }\"></ng-container>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-list/task-group-tree/task-tree/task-tree.component.scss":
/*!***********************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-list/task-group-tree/task-tree/task-tree.component.scss ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".cell {\n  position: relative;\n  display: table-cell; }\n\n.cell-group {\n  min-width: 200px; }\n\n.row-tr {\n  border-top: 1px solid #ddd;\n  overflow: visible; }\n\n.parent-tree {\n  position: relative; }\n\n.parent-tree:before {\n    content: \"\";\n    display: block;\n    width: 1px;\n    position: absolute;\n    top: 0;\n    bottom: 0;\n    z-index: 1;\n    left: 14px;\n    background: #bbb; }\n\n.parent-tree .children {\n    position: relative;\n    padding-left: 28px; }\n\n.parent-tree .children .tree-container {\n      position: relative;\n      width: 100%; }\n\n.parent-tree .children .goal-line {\n      position: relative; }\n\n.parent-tree .children .goal-line:hover {\n        background-color: #dff0fd !important; }\n\n.parent-tree .children .goal-line::before {\n        content: \"\";\n        display: block;\n        width: 14px;\n        position: absolute;\n        bottom: 0;\n        top: 50%;\n        margin-top: -1px;\n        z-index: 1;\n        background: #bbb;\n        left: -14px;\n        height: 1px; }\n\n.parent-tree .children .goal-line .goal-tree-caret {\n        cursor: pointer;\n        color: #8c8c8c;\n        top: 0;\n        bottom: 0;\n        width: 27px;\n        left: -28px;\n        position: absolute;\n        z-index: 2; }\n\n.parent-tree .children .goal-line .goal-tree-caret i {\n          font-size: .6em;\n          top: 50%;\n          margin-top: -8px;\n          background: #fff;\n          border-radius: 100%;\n          width: 2em;\n          line-height: 2em;\n          position: absolute;\n          z-index: 2;\n          left: 6px;\n          text-align: center; }\n\n.parent-tree .children .goal-line .gw-cell-item {\n        padding: 6px 0 6px 12px; }\n\n.parent-tree .children .goal-line .gw-cell-item .gw-item {\n          padding: 0px 6px; }\n\n.parent-tree .children .goal-line .gw-cell-item .gw-item:hover .btn-update {\n            display: block; }\n\n.parent-tree .children .goal-line .gw-cell-item .gw-item .gw-description {\n            color: #757575;\n            font-style: italic; }\n\n.parent-tree .children .goal-line .gw-cell-item .active .btn-update {\n          display: block; }\n\n.parent-tree .children .last-goal-line::after {\n      content: \"\";\n      display: block;\n      width: 1px;\n      position: absolute;\n      top: 50%;\n      z-index: 1;\n      right: 0;\n      left: -14px;\n      bottom: 0;\n      background: #f5f7f7; }\n\n.last-parent-tree::after {\n  content: \"\";\n  display: block;\n  width: 1px;\n  position: absolute;\n  top: 0%;\n  z-index: 1;\n  right: 0;\n  left: -14px;\n  bottom: 0;\n  background: #f5f7f7; }\n\n.bg-info-blue {\n  border-radius: 4px !important; }\n\n.progress {\n  height: 10px; }\n\nul, .list-inline {\n  margin-bottom: 0px;\n  margin-top: 10px; }\n\nul li, .list-inline li {\n    padding: 0px 2px; }\n\nul li a, .list-inline li a {\n      color: #757575; }\n\ni {\n  font-weight: normal !important; }\n\n.view-detail {\n  text-decoration: underline;\n  color: #757575;\n  padding-left: 5px;\n  margin-left: 5px;\n  border-left: 1px solid #757575;\n  font-size: 14px; }\n\n.view-detail:hover {\n    color: #0072BC; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC90YXNrLWxpc3QvdGFzay1ncm91cC10cmVlL3Rhc2stdHJlZS9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXBwXFxtb2R1bGVzXFx0YXNrXFx0YXNrLW1hbmFnZW1lbnRcXHRhc2stbGlzdFxcdGFzay1ncm91cC10cmVlXFx0YXNrLXRyZWVcXHRhc2stdHJlZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC90YXNrLWxpc3QvdGFzay1ncm91cC10cmVlL3Rhc2stdHJlZS9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXNzZXRzXFxzdHlsZXNcXF9jb25maWcuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNJLGtCQUFrQjtFQUNsQixtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSSwwQkFBMEI7RUFDMUIsaUJBQWlCLEVBQUE7O0FBR3JCO0VBQ0ksa0JBQWtCLEVBQUE7O0FBRHRCO0lBR1EsV0FBVztJQUNYLGNBQWM7SUFDZCxVQUFVO0lBQ1Ysa0JBQWtCO0lBQ2xCLE1BQU07SUFDTixTQUFTO0lBQ1QsVUFBVTtJQUNWLFVBQVU7SUFDVixnQkFBZ0IsRUFBQTs7QUFYeEI7SUFlUSxrQkFBa0I7SUFDbEIsa0JBQWtCLEVBQUE7O0FBaEIxQjtNQW1CWSxrQkFBa0I7TUFDbEIsV0FBVyxFQUFBOztBQXBCdkI7TUF1Qlksa0JBQWtCLEVBQUE7O0FBdkI5QjtRQXlCZ0Isb0NBQ0osRUFBQTs7QUExQlo7UUE2QmdCLFdBQVc7UUFDWCxjQUFjO1FBQ2QsV0FBVztRQUNYLGtCQUFrQjtRQUNsQixTQUFTO1FBQ1QsUUFBUTtRQUNSLGdCQUFnQjtRQUNoQixVQUFVO1FBQ1YsZ0JBQWdCO1FBQ2hCLFdBQVc7UUFDWCxXQUFXLEVBQUE7O0FBdkMzQjtRQTJDZ0IsZUFBZTtRQUNmLGNBQWM7UUFDZCxNQUFNO1FBQ04sU0FBUztRQUNULFdBQVc7UUFDWCxXQUFXO1FBQ1gsa0JBQWtCO1FBQ2xCLFVBQVUsRUFBQTs7QUFsRDFCO1VBcURvQixlQUFlO1VBQ2YsUUFBUTtVQUNSLGdCQUFnQjtVQUNoQixnQkFBZ0I7VUFDaEIsbUJBQW1CO1VBQ25CLFVBQVU7VUFDVixnQkFBZ0I7VUFDaEIsa0JBQWtCO1VBQ2xCLFVBQVU7VUFDVixTQUFTO1VBQ1Qsa0JBQWtCLEVBQUE7O0FBL0R0QztRQW9FZ0IsdUJBQXVCLEVBQUE7O0FBcEV2QztVQXNFb0IsZ0JBQWdCLEVBQUE7O0FBdEVwQztZQXlFNEIsY0FBYyxFQUFBOztBQXpFMUM7WUE2RXdCLGNDeERKO1lEeURJLGtCQUFrQixFQUFBOztBQTlFMUM7VUFvRndCLGNBQWMsRUFBQTs7QUFwRnRDO01BNkZnQixXQUFXO01BQ1gsY0FBYztNQUNkLFVBQVU7TUFDVixrQkFBa0I7TUFDbEIsUUFBUTtNQUNSLFVBQVU7TUFDVixRQUFRO01BQ1IsV0FBVztNQUNYLFNBQVM7TUFDVCxtQkNsRkksRUFBQTs7QUR3RnBCO0VBRVEsV0FBVztFQUNYLGNBQWM7RUFDZCxVQUFVO0VBQ1Ysa0JBQWtCO0VBQ2xCLE9BQU87RUFDUCxVQUFVO0VBQ1YsUUFBUTtFQUNSLFdBQVc7RUFDWCxTQUFTO0VBQ1QsbUJDbkdZLEVBQUE7O0FEdUdwQjtFQUNJLDZCQUE2QixFQUFBOztBQUdqQztFQUNJLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxrQkFBa0I7RUFDbEIsZ0JBQWdCLEVBQUE7O0FBRnBCO0lBT1EsZ0JBQWdCLEVBQUE7O0FBUHhCO01BS1ksY0NuSFEsRUFBQTs7QUR5SHBCO0VBQ0ksOEJBQThCLEVBQUE7O0FBSWxDO0VBQ0ksMEJBQTBCO0VBQzFCLGNDaElnQjtFRGlJaEIsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQiw4QkNuSWdCO0VEb0loQixlQUFlLEVBQUE7O0FBTm5CO0lBUVEsY0NuS1csRUFBQSIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvdGFzay90YXNrLW1hbmFnZW1lbnQvdGFzay1saXN0L3Rhc2stZ3JvdXAtdHJlZS90YXNrLXRyZWUvdGFzay10cmVlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCAnLi4vLi4vLi4vLi4vLi4vLi4vLi4vYXNzZXRzL3N0eWxlcy9jb25maWcnO1xyXG5cclxuLmNlbGwge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogdGFibGUtY2VsbDtcclxufVxyXG5cclxuLmNlbGwtZ3JvdXAge1xyXG4gICAgbWluLXdpZHRoOiAyMDBweDtcclxufVxyXG5cclxuLnJvdy10ciB7XHJcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2RkZDtcclxuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xyXG59XHJcblxyXG4ucGFyZW50LXRyZWUge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgJjpiZWZvcmUge1xyXG4gICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgd2lkdGg6IDFweDtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgdG9wOiAwO1xyXG4gICAgICAgIGJvdHRvbTogMDtcclxuICAgICAgICB6LWluZGV4OiAxO1xyXG4gICAgICAgIGxlZnQ6IDE0cHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2JiYjtcclxuICAgIH1cclxuXHJcbiAgICAuY2hpbGRyZW4ge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDI4cHg7XHJcblxyXG4gICAgICAgIC50cmVlLWNvbnRhaW5lciB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5nb2FsLWxpbmUge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2RmZjBmZCAhaW1wb3J0YW50XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICY6OmJlZm9yZSB7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTRweDtcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgICAgIGJvdHRvbTogMDtcclxuICAgICAgICAgICAgICAgIHRvcDogNTAlO1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogLTFweDtcclxuICAgICAgICAgICAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjYmJiO1xyXG4gICAgICAgICAgICAgICAgbGVmdDogLTE0cHg7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDFweDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLmdvYWwtdHJlZS1jYXJldCB7XHJcbiAgICAgICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogIzhjOGM4YztcclxuICAgICAgICAgICAgICAgIHRvcDogMDtcclxuICAgICAgICAgICAgICAgIGJvdHRvbTogMDtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAyN3B4O1xyXG4gICAgICAgICAgICAgICAgbGVmdDogLTI4cHg7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgICAgICB6LWluZGV4OiAyO1xyXG5cclxuICAgICAgICAgICAgICAgIGkge1xyXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogLjZlbTtcclxuICAgICAgICAgICAgICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAtOHB4O1xyXG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMmVtO1xyXG4gICAgICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyZW07XHJcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICAgICAgICAgIHotaW5kZXg6IDI7XHJcbiAgICAgICAgICAgICAgICAgICAgbGVmdDogNnB4O1xyXG4gICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLmd3LWNlbGwtaXRlbSB7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiA2cHggMCA2cHggMTJweDtcclxuICAgICAgICAgICAgICAgIC5ndy1pdGVtIHtcclxuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAwcHggNnB4O1xyXG4gICAgICAgICAgICAgICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAuYnRuLXVwZGF0ZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAuZ3ctZGVzY3JpcHRpb24ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogJGJyaWdodEdyYXk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc3R5bGU6IGl0YWxpYztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLmFjdGl2ZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLmJ0bi11cGRhdGUge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5sYXN0LWdvYWwtbGluZSB7XHJcbiAgICAgICAgICAgIC8vbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgICAgICAgICAgJjo6YWZ0ZXIge1xyXG4gICAgICAgICAgICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDFweDtcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgICAgIHRvcDogNTAlO1xyXG4gICAgICAgICAgICAgICAgei1pbmRleDogMTtcclxuICAgICAgICAgICAgICAgIHJpZ2h0OiAwO1xyXG4gICAgICAgICAgICAgICAgbGVmdDogLTE0cHg7XHJcbiAgICAgICAgICAgICAgICBib3R0b206IDA7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAkbGlnaHQtYmx1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLmxhc3QtcGFyZW50LXRyZWUge1xyXG4gICAgJjo6YWZ0ZXIge1xyXG4gICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgd2lkdGg6IDFweDtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgdG9wOiAwJTtcclxuICAgICAgICB6LWluZGV4OiAxO1xyXG4gICAgICAgIHJpZ2h0OiAwO1xyXG4gICAgICAgIGxlZnQ6IC0xNHB4O1xyXG4gICAgICAgIGJvdHRvbTogMDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAkbGlnaHQtYmx1ZTtcclxuICAgIH1cclxufVxyXG5cclxuLmJnLWluZm8tYmx1ZSB7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLnByb2dyZXNzIHtcclxuICAgIGhlaWdodDogMTBweDtcclxufVxyXG5cclxudWwsIC5saXN0LWlubGluZSB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgbGkge1xyXG4gICAgICAgIGEge1xyXG4gICAgICAgICAgICBjb2xvcjogJGJyaWdodEdyYXk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHBhZGRpbmc6IDBweCAycHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbmkge1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5cclxuLnZpZXctZGV0YWlsIHtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gICAgY29sb3I6ICRicmlnaHRHcmF5O1xyXG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogNXB4O1xyXG4gICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAkYnJpZ2h0R3JheTtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICY6aG92ZXIge1xyXG4gICAgICAgIGNvbG9yOiAkZGFyay1ibHVlO1xyXG4gICAgfVxyXG59XHJcbiIsIiRkZWZhdWx0LWNvbG9yOiAjMjIyO1xyXG4kZm9udC1mYW1pbHk6IFwiQXJpYWxcIiwgdGFob21hLCBIZWx2ZXRpY2EgTmV1ZTtcclxuJGNvbG9yLWJsdWU6ICMzNTk4ZGM7XHJcbiRtYWluLWNvbG9yOiAjMDA3NDU1O1xyXG4kYm9yZGVyQ29sb3I6ICNkZGQ7XHJcbiRzZWNvbmQtY29sb3I6ICNiMDFhMWY7XHJcbiR0YWJsZS1iYWNrZ3JvdW5kLWNvbG9yOiAjMDA5Njg4O1xyXG4kYmx1ZTogIzAwN2JmZjtcclxuJGRhcmstYmx1ZTogIzAwNzJCQztcclxuJGJyaWdodC1ibHVlOiAjZGZmMGZkO1xyXG4kaW5kaWdvOiAjNjYxMGYyO1xyXG4kcHVycGxlOiAjNmY0MmMxO1xyXG4kcGluazogI2U4M2U4YztcclxuJHJlZDogI2RjMzU0NTtcclxuJG9yYW5nZTogI2ZkN2UxNDtcclxuJHllbGxvdzogI2ZmYzEwNztcclxuJGdyZWVuOiAjMjhhNzQ1O1xyXG4kdGVhbDogIzIwYzk5NztcclxuJGN5YW46ICMxN2EyYjg7XHJcbiR3aGl0ZTogI2ZmZjtcclxuJGdyYXk6ICM4NjhlOTY7XHJcbiRncmF5LWRhcms6ICMzNDNhNDA7XHJcbiRwcmltYXJ5OiAjMDA3YmZmO1xyXG4kc2Vjb25kYXJ5OiAjNmM3NTdkO1xyXG4kc3VjY2VzczogIzI4YTc0NTtcclxuJGluZm86ICMxN2EyYjg7XHJcbiR3YXJuaW5nOiAjZmZjMTA3O1xyXG4kZGFuZ2VyOiAjZGMzNTQ1O1xyXG4kbGlnaHQ6ICNmOGY5ZmE7XHJcbiRkYXJrOiAjMzQzYTQwO1xyXG4kbGFiZWwtY29sb3I6ICM2NjY7XHJcbiRiYWNrZ3JvdW5kLWNvbG9yOiAjRUNGMEYxO1xyXG4kYm9yZGVyQWN0aXZlQ29sb3I6ICM4MGJkZmY7XHJcbiRib3JkZXJSYWRpdXM6IDA7XHJcbiRkYXJrQmx1ZTogIzQ1QTJEMjtcclxuJGxpZ2h0R3JlZW46ICMyN2FlNjA7XHJcbiRsaWdodC1ibHVlOiAjZjVmN2Y3O1xyXG4kYnJpZ2h0R3JheTogIzc1NzU3NTtcclxuJG1heC13aWR0aC1tb2JpbGU6IDc2OHB4O1xyXG4kbWF4LXdpZHRoLXRhYmxldDogOTkycHg7XHJcbiRtYXgtd2lkdGgtZGVza3RvcDogMTI4MHB4O1xyXG5cclxuLy8gQkVHSU46IE1hcmdpblxyXG5AbWl4aW4gbmgtbWcoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW46ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWd0KCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLXRvcDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ2IoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1nbCgkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1ncigkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1yaWdodDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogTWFyZ2luXHJcblxyXG4vLyBCRUdJTjogUGFkZGluZ1xyXG5AbWl4aW4gbmgtcGQoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkdCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctdG9wOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkYigkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctYm90dG9tOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkbCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctbGVmdDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZHIoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBQYWRkaW5nXHJcblxyXG4vLyBCRUdJTjogV2lkdGhcclxuQG1peGluIG5oLXdpZHRoKCR3aWR0aCkge1xyXG4gICAgd2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG4gICAgbWluLXdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxuICAgIG1heC13aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogV2lkdGhcclxuXHJcbi8vIEJFR0lOOiBJY29uIFNpemVcclxuQG1peGluIG5oLXNpemUtaWNvbigkc2l6ZSkge1xyXG4gICAgd2lkdGg6ICRzaXplO1xyXG4gICAgaGVpZ2h0OiAkc2l6ZTtcclxuICAgIGZvbnQtc2l6ZTogJHNpemU7XHJcbn1cclxuXHJcbi8vIEVORDogSWNvbiBTaXplXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-list/task-group-tree/task-tree/task-tree.component.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-list/task-group-tree/task-tree/task-tree.component.ts ***!
  \*********************************************************************************************************/
/*! exports provided: TaskTreeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskTreeComponent", function() { return TaskTreeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TaskTreeComponent = /** @class */ (function () {
    function TaskTreeComponent() {
    }
    TaskTreeComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], TaskTreeComponent.prototype, "data", void 0);
    TaskTreeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'task-tree',
            template: __webpack_require__(/*! ./task-tree.component.html */ "./src/app/modules/task/task-management/task-list/task-group-tree/task-tree/task-tree.component.html"),
            styles: [__webpack_require__(/*! ./task-tree.component.scss */ "./src/app/modules/task/task-management/task-list/task-group-tree/task-tree/task-tree.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TaskTreeComponent);
    return TaskTreeComponent;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-list/task-list-group/task-list-group.component.html":
/*!*******************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-list/task-list-group/task-list-group.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"task-list-group\">\r\n    <table [ngClass]=\"classes\">\r\n        <thead>\r\n        <tr>\r\n            <th (click)=\"showed = !showed\" class=\"cursor-pointer\">\r\n                <i class=\"fa cm-mgr-10\"\r\n                   [class.fa-caret-right]=\"!showed\"\r\n                   [class.fa-caret-down]=\"showed\"></i>{{ title }}\r\n            </th>\r\n            <th class=\"w150 item-sorter cursor-pointer hidden-xs\"\r\n                [class.sort-desc]=\"startDateSort === 1\"\r\n                [class.sort-asc]=\"startDateSort === 2\"\r\n                (click)=\"sortStartDate()\">\r\n                <span>Ngày bắt đầu</span>\r\n            </th>\r\n            <th class=\"w150 item-sorter cursor-pointer hidden-xs\"\r\n                [class.sort-desc]=\"endDateSort === 1\"\r\n                [class.sort-asc]=\"endDateSort === 2\"\r\n                (click)=\"sortEndDate()\">\r\n                <span>Ngày kết thúc</span>\r\n            </th>\r\n            <th class=\"w120 center middle hidden-xs\">\r\n                <span>Trạng thái</span>\r\n            </th>\r\n        </tr>\r\n        </thead>\r\n        <tbody [class.hide]=\"!showed\" *ngIf=\"source && source.length > 0; else noTaskTemplate\">\r\n        <tr *ngFor=\"let task of source\">\r\n            <td>\r\n                <app-task-list-item\r\n                    [task]=\"task\"\r\n                    (remove)=\"removeTaskInGroup($event)\"></app-task-list-item>\r\n            </td>\r\n            <td class=\"center middle hidden-xs\">\r\n                <span matTooltip=\"Ngày bắt đầu\">\r\n                    <i class=\"fa fa-calendar\"></i>\r\n                    {{ task.estimateStartDate | dateTimeFormat:'DD/MM/YYYY' }}\r\n                </span>\r\n            </td>\r\n            <td class=\"center middle hidden-xs\">\r\n                <span matTooltip=\"Ngày kết thúc\">\r\n                    <i class=\"fa fa-calendar\"></i>\r\n                    {{ task.estimateEndDate | dateTimeFormat:'DD/MM/YYYY' }}\r\n                </span>\r\n            </td>\r\n            <td class=\"center middle hidden-xs\">\r\n                <span>{task.status, select, 0{Chưa bắt đầu} 1{Đang tiến hành} 2 {Hoàn thành} 3{Hủy} 4 {Chờ duyệt} 5 {Từ chối}}</span>\r\n            </td>\r\n        </tr>\r\n        </tbody>\r\n        <ng-template #noTaskTemplate>\r\n            <tfoot>\r\n            <tr>\r\n                <td colspan=\"4\">\r\n                    <div class=\"alert alert-info\">\r\n                        <i class=\"fa fa-info-circle cm-mgr-5\"></i>\r\n                        <span>Chưa có công việc</span>\r\n                    </div>\r\n                </td>\r\n            </tr>\r\n            </tfoot>\r\n        </ng-template>\r\n    </table>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-list/task-list-group/task-list-group.component.scss":
/*!*******************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-list/task-list-group/task-list-group.component.scss ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".task-list-group table {\n  width: 100%; }\n  .task-list-group table thead tr th.item-sorter {\n    background-image: url('xarrow-up-down.png');\n    background-repeat: no-repeat;\n    background-position: right 6px center; }\n  .task-list-group table thead tr th.item-sorter.sort-desc {\n      background-image: url('xarrow-down.png'); }\n  .task-list-group table thead tr th.item-sorter.sort-asc {\n      background-image: url('xarrow-up.png'); }\n  .task-list-group table thead tr th {\n    padding: 10px 20px;\n    font-weight: normal !important; }\n  .task-list-group table.success thead tr {\n    background: #27ae60;\n    color: white; }\n  .task-list-group table.gray thead tr {\n    background-color: #abc8dc;\n    color: #2c4a5e; }\n  .task-list-group table.orange thead tr {\n    background-color: #f7921e;\n    color: white; }\n  .task-list-group table.red thead tr {\n    background: red;\n    color: white; }\n  .task-list-group table.info thead tr {\n    background-color: #0072bc;\n    color: white; }\n  .task-list-group table div.alert {\n    margin-top: 0 !important;\n    margin-bottom: 0 !important; }\n  .task-list-group table tbody tr {\n    background-color: white; }\n  .task-list-group table tbody tr:first-child td {\n      border-top: none !important; }\n  .task-list-group table tbody tr:nth-child(even) {\n      background: #f3fefc; }\n  .task-list-group table tbody tr:hover {\n      background-color: #f3fefc; }\n  .task-list-group table tbody tr td {\n      border: 1px dashed #ddd;\n      padding: 6px 12px; }\n  .task-list-group table tbody tr td:first-child {\n        border-left: none !important; }\n  .task-list-group table tbody tr td:last-child {\n        border-right: none !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC90YXNrLWxpc3QvdGFzay1saXN0LWdyb3VwL0Q6XFxQcm9qZWN0XFxHaG1BcHBsaWNhdGlvblxcY2xpZW50c1xcZ2htYXBwbGljYXRpb25jbGllbnQvc3JjXFxhcHBcXG1vZHVsZXNcXHRhc2tcXHRhc2stbWFuYWdlbWVudFxcdGFzay1saXN0XFx0YXNrLWxpc3QtZ3JvdXBcXHRhc2stbGlzdC1ncm91cC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRLFdBQVcsRUFBQTtFQUZuQjtJQU1nQiwyQ0FBeUU7SUFDekUsNEJBQTRCO0lBQzVCLHFDQUFxQyxFQUFBO0VBUnJEO01BV29CLHdDQUFzRSxFQUFBO0VBWDFGO01BZW9CLHNDQUFvRSxFQUFBO0VBZnhGO0lBb0JnQixrQkFBa0I7SUFDbEIsOEJBQThCLEVBQUE7RUFyQjlDO0lBMkJnQixtQkFBbUI7SUFDbkIsWUFBWSxFQUFBO0VBNUI1QjtJQWtDZ0IseUJBQXlCO0lBQ3pCLGNBQWMsRUFBQTtFQW5DOUI7SUF3Q2dCLHlCQUF5QjtJQUN6QixZQUFZLEVBQUE7RUF6QzVCO0lBOENnQixlQUFlO0lBQ2YsWUFBWSxFQUFBO0VBL0M1QjtJQW9EZ0IseUJBQXlCO0lBQ3pCLFlBQVksRUFBQTtFQXJENUI7SUEwRFksd0JBQXdCO0lBQ3hCLDJCQUEyQixFQUFBO0VBM0R2QztJQStEWSx1QkFBdUIsRUFBQTtFQS9EbkM7TUFtRW9CLDJCQUEyQixFQUFBO0VBbkUvQztNQXdFZ0IsbUJBQW1CLEVBQUE7RUF4RW5DO01BNEVnQix5QkFBeUIsRUFBQTtFQTVFekM7TUFnRmdCLHVCQUF1QjtNQUN2QixpQkFBaUIsRUFBQTtFQWpGakM7UUFvRm9CLDRCQUE0QixFQUFBO0VBcEZoRDtRQXdGb0IsNkJBQTZCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9tb2R1bGVzL3Rhc2svdGFzay1tYW5hZ2VtZW50L3Rhc2stbGlzdC90YXNrLWxpc3QtZ3JvdXAvdGFzay1saXN0LWdyb3VwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRhc2stbGlzdC1ncm91cCB7XHJcbiAgICB0YWJsZSB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgICAgIHRoZWFkIHRyIHtcclxuICAgICAgICAgICAgdGguaXRlbS1zb3J0ZXIge1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCdzcmMvYXNzZXRzL2ltYWdlcy9iYWNrZ3JvdW5kcy94YXJyb3ctdXAtZG93bi5wbmcnKTtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiByaWdodCA2cHggY2VudGVyO1xyXG5cclxuICAgICAgICAgICAgICAgICYuc29ydC1kZXNjIHtcclxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJ3NyYy9hc3NldHMvaW1hZ2VzL2JhY2tncm91bmRzL3hhcnJvdy1kb3duLnBuZycpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICYuc29ydC1hc2Mge1xyXG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnc3JjL2Fzc2V0cy9pbWFnZXMvYmFja2dyb3VuZHMveGFycm93LXVwLnBuZycpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aCB7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAxMHB4IDIwcHg7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogbm9ybWFsICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICYuc3VjY2VzcyB7XHJcbiAgICAgICAgICAgIHRoZWFkIHRyIHtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICMyN2FlNjA7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICYuZ3JheSB7XHJcbiAgICAgICAgICAgIHRoZWFkIHRyIHtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNhYmM4ZGM7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogIzJjNGE1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAmLm9yYW5nZSB7XHJcbiAgICAgICAgICAgIHRoZWFkIHRyIHtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmNzkyMWU7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgJi5yZWQge1xyXG4gICAgICAgICAgICB0aGVhZCB0ciB7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiByZWQ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgJi5pbmZvIHtcclxuICAgICAgICAgICAgdGhlYWQgdHIge1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzAwNzJiYztcclxuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZGl2LmFsZXJ0IHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMCAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0Ym9keSB0ciB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG5cclxuICAgICAgICAgICAgJjpmaXJzdC1jaGlsZCB7XHJcbiAgICAgICAgICAgICAgICB0ZCB7XHJcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXRvcDogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAmOm50aC1jaGlsZChldmVuKSB7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjNmZWZjO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmM2ZlZmM7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRkIHtcclxuICAgICAgICAgICAgICAgIGJvcmRlcjogMXB4IGRhc2hlZCAjZGRkO1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogNnB4IDEycHg7XHJcblxyXG4gICAgICAgICAgICAgICAgJjpmaXJzdC1jaGlsZCB7XHJcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLWxlZnQ6IG5vbmUgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1yaWdodDogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-list/task-list-group/task-list-group.component.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-list/task-list-group/task-list-group.component.ts ***!
  \*****************************************************************************************************/
/*! exports provided: TaskListGroupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskListGroupComponent", function() { return TaskListGroupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);



var TaskListGroupComponent = /** @class */ (function () {
    function TaskListGroupComponent() {
        this.title = 'Đang tiến hành';
        this.classes = '';
        this.showed = true;
        this.startDateSort = 0;
        this.endDateSort = 0;
    }
    TaskListGroupComponent.prototype.ngOnInit = function () {
    };
    TaskListGroupComponent.prototype.sortStartDate = function () {
        this.startDateSort = this.startDateSort === 0 ? 1
            : this.startDateSort === 1 ? 2
                : 0;
    };
    TaskListGroupComponent.prototype.sortEndDate = function () {
        this.endDateSort = this.endDateSort === 0 ? 1
            : this.endDateSort === 1 ? 2
                : 0;
    };
    TaskListGroupComponent.prototype.removeTaskInGroup = function (value) {
        lodash__WEBPACK_IMPORTED_MODULE_2__["remove"](this.source, function (task) {
            return task.id === value;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TaskListGroupComponent.prototype, "title", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TaskListGroupComponent.prototype, "classes", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], TaskListGroupComponent.prototype, "source", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TaskListGroupComponent.prototype, "showed", void 0);
    TaskListGroupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-list-group',
            template: __webpack_require__(/*! ./task-list-group.component.html */ "./src/app/modules/task/task-management/task-list/task-list-group/task-list-group.component.html"),
            styles: [__webpack_require__(/*! ./task-list-group.component.scss */ "./src/app/modules/task/task-management/task-list/task-list-group/task-list-group.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TaskListGroupComponent);
    return TaskListGroupComponent;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-list/task-list-item/task-list-item.component.html":
/*!*****************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-list/task-list-item/task-list-item.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"task-list-item\">\r\n    <div class=\"task-item\" [class.active]=\"isActive\">\r\n        <div class=\"task-progress\">\r\n            <div style=\"display: inline-block\">\r\n                <ngx-charts-pie-grid\r\n                    [view]=\"view\"\r\n                    [results]=\"progressData\"\r\n                    [label]=\"'Tiến độ'\"\r\n                    [designatedTotal]=\"100\"\r\n                    [minWidth]=\"50\"\r\n                    [scheme]=\"colorScheme\"\r\n                    [tooltipDisabled]=\"true\"\r\n                    [tooltipText]=\"''\"\r\n                    (select)=\"onSelect($event)\">\r\n                </ngx-charts-pie-grid>\r\n            </div>\r\n        </div><!-- END: .progress -->\r\n        <div class=\"task-info\">\r\n            <a href=\"javascript://\" class=\"task-name\" (click)=\"detail()\">{{ task?.name }}</a>\r\n            <ul class=\"task-info-items\">\r\n                <li>\r\n                    <a href=\"javascript://\" title=\"Check list: Hoàn thành/ Tổng số\">\r\n                        <i class=\"fa fa-list\"></i>\r\n                        {{ task?.completedChecklistCount }}/{{ task?.checklistCount }}\r\n                    </a>\r\n                </li>\r\n                <li>\r\n                    <a href=\"javascript://\" title=\"Tổng số phản hồi\">\r\n                        <i class=\"fa fa-comments-o\"></i>\r\n                        {{ task?.commentCount }}\r\n                    </a>\r\n                </li>\r\n                <li>\r\n                    <a href=\"javascript://\" title=\"Người phụ trách\"><i class=\"fa fa-user\"></i> {{ task?.responsibleFullName }} </a>\r\n                </li>\r\n                <li>\r\n                    <a href=\"javascript://\" class=\"task-target\" *ngIf=\"task?.targetName\" title=\"{{task?.targetName}}\">\r\n                        <i class=\"fa fa-bullseye\"></i>\r\n                        {{ task?.targetName }}\r\n                    </a>\r\n                </li>\r\n            </ul>\r\n            <div class=\"btn-edit\" *ngIf=\"task?.status !== taskStatus.completed\" ghmDialogTrigger=\"\" [ghmDialogTriggerFor]=\"taskUpdateQuick\" [ghmDialogEvent]=\"'mouseover'\"\r\n                 [ghmDialogData]=\"task\">\r\n                <a href=\"javascript://\" title=\"Chỉnh sửa\"\r\n                   data-icon=\"edit-16\"></a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div><!-- END: task-list-item -->\r\n\r\n<ng-template #customTooltip></ng-template>\r\n\r\n<ghm-dialog #taskUpdateQuick [size]=\"'md'\" [backdropStatic]=\"true\" position=\"center\"\r\n            (show)=\"showDialog($event)\"\r\n            (hide)=\"isActive = false\">\r\n    <ghm-dialog-header>\r\n        <span>Cập nhật công việc | </span>\r\n        <a class=\"view-detail\" i18n=\"@@viewDetail\" (click)=\"detail()\">\r\n            Xem chi tiết\r\n        </a>\r\n    </ghm-dialog-header>\r\n    <ghm-dialog-content>\r\n        <task-update-quick (saveSuccess)=\"onChangeTaskUpdateQuick($event)\"\r\n                           (remove)=\"removeTask($event)\"\r\n        ></task-update-quick>\r\n    </ghm-dialog-content>\r\n</ghm-dialog>\r\n"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-list/task-list-item/task-list-item.component.scss":
/*!*****************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-list/task-list-item/task-list-item.component.scss ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".task-list-item .task-item {\n  display: flex;\n  position: relative; }\n  .task-list-item .task-item:hover .btn-edit {\n    display: block; }\n  .task-list-item .task-item .task-progress {\n    flex-grow: 0;\n    flex-shrink: 0;\n    flex-basis: 60px;\n    overflow: hidden; }\n  .task-list-item .task-item .task-progress svg {\n      width: 60px;\n      height: 60px; }\n  .task-list-item .task-item .task-progress svg g.pie-grid.chart {\n        -webkit-transform: translate(-10px, 0) !important;\n                transform: translate(-10px, 0) !important; }\n  .task-list-item .task-item .task-progress svg text.label.percent-label {\n        margin-top: 10px;\n        -webkit-transform: translate(0, 10px) !important;\n                transform: translate(0, 10px) !important; }\n  .task-list-item .task-item .task-progress svg text.label:not(.percent-label) {\n        display: none; }\n  .task-list-item .task-item .task-info {\n    flex-basis: auto; }\n  .task-list-item .task-item .task-info a.task-name {\n      font-size: 14px;\n      line-height: 20px;\n      width: 100%;\n      display: block;\n      font-weight: bold;\n      padding-bottom: 6px;\n      text-align: left;\n      cursor: pointer;\n      word-break: break-word;\n      color: #0072bc;\n      margin-bottom: 5px;\n      padding-right: 30px; }\n  .task-list-item .task-item .task-info a.task-name:hover {\n        text-decoration: none; }\n  .task-list-item .task-item .task-info ul.task-info-items {\n      display: block;\n      width: 100%;\n      overflow: hidden;\n      padding-left: 0;\n      margin-bottom: 0; }\n  .task-list-item .task-item .task-info ul.task-info-items li {\n        display: inline-block;\n        float: left;\n        margin-right: 10px; }\n  .task-list-item .task-item .task-info ul.task-info-items li a {\n          color: #757575; }\n  .task-list-item .task-item .task-info ul.task-info-items li a:hover {\n            color: #0072bc;\n            text-decoration: none; }\n  .task-list-item .task-item .task-info a.task-target {\n      color: #757575; }\n  .task-list-item .task-item .task-info a.task-target:hover {\n        color: #0072bc;\n        text-decoration: none; }\n  .task-list-item .task-item .btn-edit {\n    position: absolute;\n    background-color: #d9d9d9;\n    top: 5px;\n    right: 5px;\n    display: none;\n    border: none !important;\n    box-shadow: none;\n    padding: 6px 6px 0px;\n    color: #0a8cf0;\n    border-radius: 4px !important; }\n  .task-list-item .active .btn-edit {\n  display: block; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC90YXNrLWxpc3QvdGFzay1saXN0LWl0ZW0vRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFwcFxcbW9kdWxlc1xcdGFza1xcdGFzay1tYW5hZ2VtZW50XFx0YXNrLWxpc3RcXHRhc2stbGlzdC1pdGVtXFx0YXNrLWxpc3QtaXRlbS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRLGFBQWE7RUFDYixrQkFBa0IsRUFBQTtFQUgxQjtJQU9nQixjQUFjLEVBQUE7RUFQOUI7SUFZWSxZQUFZO0lBQ1osY0FBYztJQUNkLGdCQUFnQjtJQUNoQixnQkFBZ0IsRUFBQTtFQWY1QjtNQWtCZ0IsV0FBVztNQUNYLFlBQVksRUFBQTtFQW5CNUI7UUFxQm9CLGlEQUF5QztnQkFBekMseUNBQXlDLEVBQUE7RUFyQjdEO1FBeUJvQixnQkFBZ0I7UUFDaEIsZ0RBQXdDO2dCQUF4Qyx3Q0FBd0MsRUFBQTtFQTFCNUQ7UUE2Qm9CLGFBQWEsRUFBQTtFQTdCakM7SUFtQ1ksZ0JBQWdCLEVBQUE7RUFuQzVCO01Bc0NnQixlQUFlO01BQ2YsaUJBQWlCO01BQ2pCLFdBQVc7TUFDWCxjQUFjO01BQ2QsaUJBQWlCO01BQ2pCLG1CQUFtQjtNQUNuQixnQkFBZ0I7TUFDaEIsZUFBZTtNQUNmLHNCQUFzQjtNQUN0QixjQUFjO01BQ2Qsa0JBQWtCO01BQ2xCLG1CQUFtQixFQUFBO0VBakRuQztRQW1Eb0IscUJBQXFCLEVBQUE7RUFuRHpDO01Bd0RnQixjQUFjO01BQ2QsV0FBVztNQUNYLGdCQUFnQjtNQUNoQixlQUFlO01BQ2YsZ0JBQWdCLEVBQUE7RUE1RGhDO1FBK0RvQixxQkFBcUI7UUFDckIsV0FBVztRQUNYLGtCQUFrQixFQUFBO0VBakV0QztVQW9Fd0IsY0FBYyxFQUFBO0VBcEV0QztZQXVFNEIsY0FBYztZQUNkLHFCQUFxQixFQUFBO0VBeEVqRDtNQStFZ0IsY0FBYyxFQUFBO0VBL0U5QjtRQWtGb0IsY0FBYztRQUNkLHFCQUFxQixFQUFBO0VBbkZ6QztJQXlGWSxrQkFBa0I7SUFDbEIseUJBQXlCO0lBQ3pCLFFBQVE7SUFDUixVQUFVO0lBQ1YsYUFBYTtJQUNiLHVCQUF1QjtJQUN2QixnQkFBZ0I7SUFDaEIsb0JBQW9CO0lBQ3BCLGNBQWM7SUFDZCw2QkFBNkIsRUFBQTtFQWxHekM7RUF3R1ksY0FBYyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC90YXNrLWxpc3QvdGFzay1saXN0LWl0ZW0vdGFzay1saXN0LWl0ZW0uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudGFzay1saXN0LWl0ZW0ge1xyXG4gICAgLnRhc2staXRlbSB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcblxyXG4gICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICAuYnRuLWVkaXQge1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC50YXNrLXByb2dyZXNzIHtcclxuICAgICAgICAgICAgZmxleC1ncm93OiAwO1xyXG4gICAgICAgICAgICBmbGV4LXNocmluazogMDtcclxuICAgICAgICAgICAgZmxleC1iYXNpczogNjBweDtcclxuICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuXHJcbiAgICAgICAgICAgIHN2ZyB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogNjBweDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogNjBweDtcclxuICAgICAgICAgICAgICAgIGcucGllLWdyaWQuY2hhcnQge1xyXG4gICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC0xMHB4LCAwKSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHRleHQubGFiZWwucGVyY2VudC1sYWJlbCB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwLCAxMHB4KSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGV4dC5sYWJlbDpub3QoLnBlcmNlbnQtbGFiZWwpIHtcclxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAudGFzay1pbmZvIHtcclxuICAgICAgICAgICAgZmxleC1iYXNpczogYXV0bztcclxuXHJcbiAgICAgICAgICAgIGEudGFzay1uYW1lIHtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDZweDtcclxuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgICAgICAgICB3b3JkLWJyZWFrOiBicmVhay13b3JkO1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICMwMDcyYmM7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB1bC50YXNrLWluZm8taXRlbXMge1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG5cclxuICAgICAgICAgICAgICAgIGxpIHtcclxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBhIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM3NTc1NzU7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjMDA3MmJjO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBhLnRhc2stdGFyZ2V0IHtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAjNzU3NTc1O1xyXG5cclxuICAgICAgICAgICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjMDA3MmJjO1xyXG4gICAgICAgICAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmJ0bi1lZGl0IHtcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDlkOWQ5O1xyXG4gICAgICAgICAgICB0b3A6IDVweDtcclxuICAgICAgICAgICAgcmlnaHQ6IDVweDtcclxuICAgICAgICAgICAgZGlzcGxheTogbm9uZTtcclxuICAgICAgICAgICAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDZweCA2cHggMHB4OztcclxuICAgICAgICAgICAgY29sb3I6ICMwYThjZjA7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDRweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuYWN0aXZlIHtcclxuICAgICAgICAuYnRuLWVkaXQge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-list/task-list-item/task-list-item.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-list/task-list-item/task-list-item.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: TaskListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskListItemComponent", function() { return TaskListItemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../task-detail/task-detail.component */ "./src/app/modules/task/task-management/task-detail/task-detail.component.ts");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/task.service */ "./src/app/modules/task/task-management/services/task.service.ts");
/* harmony import */ var _consts_tasks_const__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../consts/tasks.const */ "./src/app/modules/task/task-management/consts/tasks.const.ts");
/* harmony import */ var _task_quick_update_task_quick_update_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../task-quick-update/task-quick-update.component */ "./src/app/modules/task/task-management/task-quick-update/task-quick-update.component.ts");
/* harmony import */ var _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../shareds/components/ghm-dialog/ghm-dialog.component */ "./src/app/shareds/components/ghm-dialog/ghm-dialog.component.ts");








var TaskListItemComponent = /** @class */ (function () {
    function TaskListItemComponent(dialog, taskService) {
        this.dialog = dialog;
        this.taskService = taskService;
        this.remove = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.saveSuccess = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.taskStatus = _consts_tasks_const__WEBPACK_IMPORTED_MODULE_5__["TaskStatus"];
        this.view = [120, 120];
        this.progressData = [
            {
                'name': '',
                'value': 20
            },
        ];
        this.colorScheme = {
            domain: ['#e74c3c']
        };
    }
    Object.defineProperty(TaskListItemComponent.prototype, "task", {
        get: function () {
            return this._task;
        },
        set: function (value) {
            if (value) {
                this.progressData = [
                    {
                        'name': '',
                        'value': value.percentCompleted
                    },
                ];
                if ((value.status === _consts_tasks_const__WEBPACK_IMPORTED_MODULE_5__["TaskStatus"].inProgress || value.status === _consts_tasks_const__WEBPACK_IMPORTED_MODULE_5__["TaskStatus"].declineFinish) && value.overdueDate <= 0) {
                    this.colorScheme = { domain: ['#449D44'] };
                }
                if (value.status === _consts_tasks_const__WEBPACK_IMPORTED_MODULE_5__["TaskStatus"].notStartYet && value.overdueDate <= 0) {
                    this.colorScheme = { domain: ['#C2CAD8'] };
                }
                if ((value.status === _consts_tasks_const__WEBPACK_IMPORTED_MODULE_5__["TaskStatus"].notStartYet || value.status === _consts_tasks_const__WEBPACK_IMPORTED_MODULE_5__["TaskStatus"].inProgress
                    || value.status === _consts_tasks_const__WEBPACK_IMPORTED_MODULE_5__["TaskStatus"].declineFinish) && value.overdueDate > 0) {
                    this.colorScheme = { domain: ['#C9302C'] };
                }
                if ((value.status === _consts_tasks_const__WEBPACK_IMPORTED_MODULE_5__["TaskStatus"].completed || value.status === _consts_tasks_const__WEBPACK_IMPORTED_MODULE_5__["TaskStatus"].pendingToFinish) && value.overdueDate > 0) {
                    this.colorScheme = { domain: ['#EC971F'] };
                }
                if ((value.status === _consts_tasks_const__WEBPACK_IMPORTED_MODULE_5__["TaskStatus"].completed || value.status === _consts_tasks_const__WEBPACK_IMPORTED_MODULE_5__["TaskStatus"].pendingToFinish) && value.overdueDate <= 0) {
                    this.colorScheme = { domain: ['#286090'] };
                }
            }
            this._task = value;
        },
        enumerable: true,
        configurable: true
    });
    TaskListItemComponent.prototype.ngOnInit = function () {
    };
    TaskListItemComponent.prototype.detail = function () {
        var _this = this;
        var taskDetailDialog = this.dialog.open(_task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_3__["TaskDetailComponent"], {
            id: "taskDetailDialog-" + this.task.id,
            data: { id: this.task.id },
            disableClose: true
        });
        taskDetailDialog.afterClosed().subscribe(function (isUpdate) {
            if (isUpdate) {
                _this.taskService.triggerSearch$.next();
            }
        });
    };
    TaskListItemComponent.prototype.onSelect = function (value) {
    };
    TaskListItemComponent.prototype.showDialog = function (task) {
        this.isActive = true;
        this.taskDetail = task;
        this.getDetail(task);
    };
    TaskListItemComponent.prototype.onChangeTaskUpdateQuick = function (value) {
        this.taskUpdateQuickDialog.dismiss();
        this.taskService.triggerSearch$.next();
    };
    TaskListItemComponent.prototype.removeTask = function (value) {
        this.taskUpdateQuickDialog.dismiss();
        this.remove.emit(value);
    };
    TaskListItemComponent.prototype.getDetail = function (task) {
        var _this = this;
        this.taskService.getParticipants(task.id)
            .subscribe(function (taskParticipants) {
            _this.taskDetail.participants = taskParticipants;
            _this.taskQuickUpdateComponent.getDetail(_this.taskDetail);
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_task_quick_update_task_quick_update_component__WEBPACK_IMPORTED_MODULE_6__["TaskQuickUpdateComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _task_quick_update_task_quick_update_component__WEBPACK_IMPORTED_MODULE_6__["TaskQuickUpdateComponent"])
    ], TaskListItemComponent.prototype, "taskQuickUpdateComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('taskUpdateQuick'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_7__["GhmDialogComponent"])
    ], TaskListItemComponent.prototype, "taskUpdateQuickDialog", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TaskListItemComponent.prototype, "remove", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TaskListItemComponent.prototype, "saveSuccess", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
    ], TaskListItemComponent.prototype, "task", null);
    TaskListItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-list-item',
            template: __webpack_require__(/*! ./task-list-item.component.html */ "./src/app/modules/task/task-management/task-list/task-list-item/task-list-item.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./task-list-item.component.scss */ "./src/app/modules/task/task-management/task-list/task-list-item/task-list-item.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"],
            _services_task_service__WEBPACK_IMPORTED_MODULE_4__["TaskService"]])
    ], TaskListItemComponent);
    return TaskListItemComponent;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-list/task-list.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-list/task-list.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span i18n=\"@@listTask\">\r\n       Công việc {type, select, 0{nhóm} 1 {cá nhân}}\r\n    </span>\r\n</h1>\r\n<div class=\"row tool-bar\">\r\n    <div class=\"bar-act\">\r\n        <div class=\"bar-act-left\">\r\n            <a class=\"act-item\" (click)=\"addTask()\" *ngIf=\"permission.add\">\r\n                <i class=\"fa fa-plus  cm-pdr-5\"></i>\r\n                <span class=\"act-item-text\" i18n=\"@@createTarget\">Tạo công việc</span>\r\n            </a>\r\n        </div>\r\n        <div class=\"bar-act-center\">\r\n            <target-suggestion-filter [title]=\"'Lọc công việc'\"\r\n                                      (itemSelect)=\"selectTarget($event)\"></target-suggestion-filter>\r\n        </div>\r\n        <div class=\"bar-act-right\">\r\n            <a class=\"act-item\" ghmDialogTrigger=\"\" [ghmDialogTriggerFor]=\"taskFilterDialog\">\r\n                <i class=\"fa fa-filter cm-pdr-5\" aria-hidden=\"true\"></i>\r\n                <span class=\"act-item-text\">Lọc công việc</span>\r\n            </a>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"task-list-container\">\r\n    <div class=\"task-content-container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-9 col-sm-8\">\r\n                <app-task-list-group\r\n                    title=\"Đang tiến hành\"\r\n                    [classes]=\"'success cm-mgb-10'\"\r\n                    [source]=\"listInProgress\"></app-task-list-group>\r\n                <app-task-list-group\r\n                    title=\"Chưa bắt đầu\"\r\n                    [classes]=\"'gray cm-mgb-10'\"\r\n                    [source]=\"listNotStartYet\"></app-task-list-group>\r\n                <app-task-list-group\r\n                    title=\"Hoàn thành\"\r\n                    [classes]=\"'info cm-mgb-10'\"\r\n                    [source]=\"listCompleted\"></app-task-list-group>\r\n            </div><!-- END: left conten -->\r\n            <div class=\"col-md-3 col-sm-4 cm-pdl-0\">\r\n                <app-task-user-suggestion></app-task-user-suggestion>\r\n            </div><!-- END: right content -->\r\n        </div><!-- END: .row -->\r\n    </div><!-- END: .task-content-container -->\r\n</div><!-- END: .task-list-container -->\r\n\r\n<!-- BEGIN: Add participants -->\r\n<ghm-dialog #taskFilterDialog [size]=\"'sm'\"\r\n            [backdropStatic]=\"true\"\r\n            class=\"act-item-text\">\r\n    <ghm-dialog-header>\r\n        <i class=\"fa fa-filter\"></i>\r\n        <span class=\"cm-pdl-5\">\r\n            Lọc công việc\r\n        </span>\r\n    </ghm-dialog-header>\r\n    <form action=\"\" (ngSubmit)=\"search()\">\r\n        <ghm-dialog-content>\r\n            <div class=\"row\">\r\n                <div class=\"form-group\">\r\n                    <div class=\"col-sm-12\">\r\n                        <label ghmLabel=\"Loại công việc\"></label>\r\n                        <ul class=\"list-inline cm-pdl-0\">\r\n                            <li>\r\n                                <mat-checkbox color=\"primary\" [checked]=\"isPersonal\">Cá nhân</mat-checkbox>\r\n                            </li>\r\n                            <li>\r\n                                <mat-checkbox color=\"primary\" [checked]=\"isCombination\">Phối hợp</mat-checkbox>\r\n                            </li>\r\n                        </ul>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <div class=\"col-sm-12\">\r\n                        <label ghmLabel=\"Thời gian\"></label>\r\n                        <ghm-select-datetime\r\n                            [dateRange]=\"dateRange\"\r\n                            (selectDate)=\"selectDate($event)\"\r\n                        ></ghm-select-datetime>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </ghm-dialog-content>\r\n        <ghm-dialog-footer>\r\n            <div class=\"center\">\r\n                <button class=\"btn blue cm-mgr-5\" i18n=\"@@search\">Tìm kiếm</button>\r\n                <button class=\"btn btn-light\" i18n=\"@@close\" ghm-dismiss=\"true\" type=\"button\">\r\n                    Đóng\r\n                </button>\r\n            </div>\r\n        </ghm-dialog-footer>\r\n    </form>\r\n    <!--</form>-->\r\n</ghm-dialog>\r\n<!-- End: Add participants -->\r\n"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-list/task-list.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-list/task-list.component.ts ***!
  \*******************************************************************************/
/*! exports provided: TaskListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskListComponent", function() { return TaskListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _task_form_task_form_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../task-form/task-form.component */ "./src/app/modules/task/task-management/task-form/task-form.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/task.service */ "./src/app/modules/task/task-management/services/task.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../task-detail/task-detail.component */ "./src/app/modules/task/task-management/task-detail/task-detail.component.ts");
/* harmony import */ var _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../shareds/components/ghm-dialog/ghm-dialog.component */ "./src/app/shareds/components/ghm-dialog/ghm-dialog.component.ts");









var TaskListComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TaskListComponent, _super);
    function TaskListComponent(route, dialog, taskService) {
        var _this = _super.call(this) || this;
        _this.route = route;
        _this.dialog = dialog;
        _this.taskService = taskService;
        _this.listInProgress = [];
        _this.listNotStartYet = [];
        _this.listCompleted = [];
        _this.isPersonal = true;
        _this.isCombination = true;
        var path = route.routeConfig.path;
        _this.type = path === 'group' ? 0 : 1;
        _this.subscribers.routeData = _this.route.data.subscribe(function (result) {
            var data = result.data;
            if (data) {
                _this.listInProgress = data.listInProgress;
                _this.listNotStartYet = data.listNotStartYet;
                _this.listCompleted = data.listCompleted;
            }
        });
        _this.subscribers.triggerSearch = _this.taskService.triggerSearch$.subscribe(function (user) {
            if (user) {
                _this.userId = user.id;
            }
            _this.search();
        });
        return _this;
    }
    TaskListComponent.prototype.ngOnInit = function () {
        this.appService.setupPage(this.pageId.TASK, this.pageId.TASK_LIST, 'Quản lý công việc', 'Danh sách công việc.');
    };
    TaskListComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        var taskDetailDialog = this.dialog.getDialogById('taskDetailDialog');
        if (taskDetailDialog) {
            taskDetailDialog.afterClosed().subscribe(function (isModified) {
                if (isModified) {
                    _this.search();
                }
            });
        }
    };
    TaskListComponent.prototype.addTask = function () {
        var _this = this;
        var taskFormDialog = this.dialog.open(_task_form_task_form_component__WEBPACK_IMPORTED_MODULE_3__["TaskFormComponent"], {
            id: 'taskFormDialog',
            disableClose: true,
            data: { type: this.type }
        });
        this.subscribers.afterTaskFormDialogClosed = taskFormDialog.afterClosed().subscribe(function (result) {
            if (result && result.taskId) {
                _this.dialog.open(_task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_7__["TaskDetailComponent"], { data: { id: result.taskId }, id: "taskDetailDialog-" + result.taskId, disableClose: true });
            }
            _this.search();
        });
    };
    TaskListComponent.prototype.search = function (currentPage) {
        var _this = this;
        if (currentPage === void 0) { currentPage = 1; }
        this.currentPage = currentPage;
        this.taskService.search(this.type, this.userId, this.keyword, this.fromDate, this.toDate, this.targetId, this.workingType, this.workingConfig, this.currentPage)
            .subscribe(function (result) {
            _this.listInProgress = result.listInProgress;
            _this.listNotStartYet = result.listNotStartYet;
            _this.listCompleted = result.listCompleted;
            _this.taskFilterDialog.dismiss();
        });
    };
    TaskListComponent.prototype.detail = function (task) {
        var _this = this;
        var taskDetailDialog = this.dialog.open(_task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_7__["TaskDetailComponent"], {
            data: { id: task.id },
            id: "taskDetailDialog-" + task.id,
            disableClose: true
        });
        taskDetailDialog.afterClosed().subscribe(function (data) {
            if (data && data.isModified) {
                _this.search();
            }
        });
    };
    TaskListComponent.prototype.selectDate = function (value) {
        if (value) {
            this.fromDate = value.startDate;
            this.toDate = value.endDate;
        }
    };
    TaskListComponent.prototype.selectTarget = function (value) {
        if (value) {
            this.targetId = value.id;
        }
        else {
            this.targetId = null;
        }
        this.search();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('taskFilterDialog'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_8__["GhmDialogComponent"])
    ], TaskListComponent.prototype, "taskFilterDialog", void 0);
    TaskListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-list',
            template: __webpack_require__(/*! ./task-list.component.html */ "./src/app/modules/task/task-management/task-list/task-list.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ../task.component.scss */ "./src/app/modules/task/task-management/task.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialog"],
            _services_task_service__WEBPACK_IMPORTED_MODULE_5__["TaskService"]])
    ], TaskListComponent);
    return TaskListComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/task/task-management/task-quick-update/task-quick-update.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-quick-update/task-quick-update.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row task-update-quick\">\r\n    <div class=\"col-sm-8\">\r\n        <textarea #namInput class=\"form-control w100pc\" [value]=\"taskDetail?.name\" name=\"name\" rows=\"2\"></textarea>\r\n        <span *ngIf=\"errorName\" class=\"help-error\">{{errorName}}</span>\r\n        <div class=\"cm-mgt-5\">\r\n            <button (click)=\"updateName(namInput.value)\"\r\n                    class=\"btn blue cm-mgr-5\" i18n=\"@@update\"\r\n                    [disabled]=\"!taskCheckPermission.isWrite\">Cập nhập\r\n            </button>\r\n            <button class=\"btn btn-light\" i18n=\"@@close\" ghm-dismiss=\"true\" type=\"button\">\r\n                Đóng\r\n            </button>\r\n        </div>\r\n    </div>\r\n    <div class=\"col-sm-4 sidebar\">\r\n        <ul class=\"list-style-none cm-pdl-0\">\r\n            <li class=\"cursor-pointer\">\r\n                <a  ghmDialogTrigger=\"\"\r\n                    [ghmDialogTriggerFor]=\"diaLogUpdateStatus\" [class.disabled]=\"!taskCheckPermission.isWrite\">Cập nhập trạng thái</a>\r\n            </li>\r\n            <li class=\"cursor-pointer\" (click)=\"copy()\">\r\n                <a href=\"javascript://\"  [class.disabled]=\"!taskCheckPermission.isWrite\">Sao chép công việc</a>\r\n            </li>\r\n            <li class=\"cursor-pointer\">\r\n                <a [swal]=\"confirmDeleteTarget\"\r\n                   (confirm)=\"deleteTarget()\" [class.disabled]=\"!taskCheckPermission.isWrite\">Hủy công việc</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</div>\r\n\r\n<ghm-dialog #diaLogUpdateStatus [backdropStatic]=\"true\" position=\"center\">\r\n    <ghm-dialog-header [showCloseButton]=\"false\">\r\n        <span i18n=\"@@updateStatus\" class=\"cm-pdl-10\">Cập nhập trạng thái</span>\r\n    </ghm-dialog-header>\r\n    <ghm-dialog-content>\r\n        <div class=\"form-group\">\r\n            <ghm-select\r\n                [value]=\"taskDetail?.status\"\r\n                [icon]=\"''\"\r\n                [readonly]=\"!taskCheckPermission.isWrite\"\r\n                [elementId]=\"'selectTaskStatus'\"\r\n                [data]=\"listStatus\"\r\n                (itemSelected)=\"taskDetail.status = $event.id\">\r\n            </ghm-select>\r\n            <span *ngIf=\"errorStatus\" class=\"help-error\">{{errorStatus}}</span>\r\n        </div>\r\n    </ghm-dialog-content>\r\n    <ghm-dialog-footer>\r\n        <div class=\"center cm-mgl-20\">\r\n            <button [disabled]=\"!taskCheckPermission.isWrite\" (click)=\"updateStatus()\" class=\"btn blue cm-mgr-5\" i18n=\"@@update\">Cập nhập</button>\r\n            <button ghm-dismiss=\"\" class=\"btn btn-light\" i18n=\"@@close\" type=\"button\">\r\n                Đóng\r\n            </button>\r\n        </div>\r\n    </ghm-dialog-footer>\r\n</ghm-dialog>\r\n\r\n<swal\r\n    #confirmDeleteTarget\r\n    i18n=\"@@confirmDeleteTarget\"\r\n    i18n-title\r\n    i18n-text\r\n    title=\"Bạn có muốn xóa muốn xóa mục tiêu này ?\"\r\n    text=\"Bạn không thể lấy lại mục tiêu này sau khi xóa.\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-quick-update/task-quick-update.component.scss":
/*!*************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-quick-update/task-quick-update.component.scss ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".task-update-quick {\n  margin-right: -10px; }\n  .task-update-quick .sidebar {\n    padding-left: 0px;\n    padding-right: 0px;\n    margin-top: -7px;\n    background-color: #f5f7f7;\n    margin-bottom: -7px; }\n  .task-update-quick .sidebar ul {\n      margin-bottom: 0px; }\n  .task-update-quick .sidebar ul li {\n        padding: 8px;\n        border-bottom: 1px solid #eaeaea; }\n  .task-update-quick .sidebar ul li a {\n          color: #0072BC;\n          text-decoration: none; }\n  .task-update-quick .sidebar ul li:hover {\n          background-color: #dff0fd; }\n  .task-update-quick .sidebar ul li:last-child {\n          border-bottom: none; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC90YXNrLXF1aWNrLXVwZGF0ZS9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXBwXFxtb2R1bGVzXFx0YXNrXFx0YXNrLW1hbmFnZW1lbnRcXHRhc2stcXVpY2stdXBkYXRlXFx0YXNrLXF1aWNrLXVwZGF0ZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC90YXNrLXF1aWNrLXVwZGF0ZS9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXNzZXRzXFxzdHlsZXNcXF9jb25maWcuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNJLG1CQUFtQixFQUFBO0VBRHZCO0lBR1EsaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIseUJDNEJZO0lEM0JaLG1CQUFtQixFQUFBO0VBUDNCO01BeUJZLGtCQUFrQixFQUFBO0VBekI5QjtRQVVnQixZQUFZO1FBVVosZ0NBQWdDLEVBQUE7RUFwQmhEO1VBWW9CLGNDTkQ7VURPQyxxQkFBcUIsRUFBQTtFQWJ6QztVQWlCb0IseUJBQXlCLEVBQUE7RUFqQjdDO1VBc0JvQixtQkFBbUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvdGFzay90YXNrLW1hbmFnZW1lbnQvdGFzay1xdWljay11cGRhdGUvdGFzay1xdWljay11cGRhdGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0ICcuLi8uLi8uLi8uLi8uLi9hc3NldHMvc3R5bGVzL2NvbmZpZyc7XHJcblxyXG4udGFzay11cGRhdGUtcXVpY2sge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAtMTBweDtcclxuICAgIC5zaWRlYmFyIHtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogLTdweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkbGlnaHQtYmx1ZTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAtN3B4O1xyXG4gICAgICAgIHVsIHtcclxuICAgICAgICAgICAgbGkge1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogOHB4O1xyXG4gICAgICAgICAgICAgICAgYSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICRkYXJrLWJsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNkZmYwZmQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlYWVhZWE7XHJcbiAgICAgICAgICAgICAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b206IG5vbmU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iLCIkZGVmYXVsdC1jb2xvcjogIzIyMjtcclxuJGZvbnQtZmFtaWx5OiBcIkFyaWFsXCIsIHRhaG9tYSwgSGVsdmV0aWNhIE5ldWU7XHJcbiRjb2xvci1ibHVlOiAjMzU5OGRjO1xyXG4kbWFpbi1jb2xvcjogIzAwNzQ1NTtcclxuJGJvcmRlckNvbG9yOiAjZGRkO1xyXG4kc2Vjb25kLWNvbG9yOiAjYjAxYTFmO1xyXG4kdGFibGUtYmFja2dyb3VuZC1jb2xvcjogIzAwOTY4ODtcclxuJGJsdWU6ICMwMDdiZmY7XHJcbiRkYXJrLWJsdWU6ICMwMDcyQkM7XHJcbiRicmlnaHQtYmx1ZTogI2RmZjBmZDtcclxuJGluZGlnbzogIzY2MTBmMjtcclxuJHB1cnBsZTogIzZmNDJjMTtcclxuJHBpbms6ICNlODNlOGM7XHJcbiRyZWQ6ICNkYzM1NDU7XHJcbiRvcmFuZ2U6ICNmZDdlMTQ7XHJcbiR5ZWxsb3c6ICNmZmMxMDc7XHJcbiRncmVlbjogIzI4YTc0NTtcclxuJHRlYWw6ICMyMGM5OTc7XHJcbiRjeWFuOiAjMTdhMmI4O1xyXG4kd2hpdGU6ICNmZmY7XHJcbiRncmF5OiAjODY4ZTk2O1xyXG4kZ3JheS1kYXJrOiAjMzQzYTQwO1xyXG4kcHJpbWFyeTogIzAwN2JmZjtcclxuJHNlY29uZGFyeTogIzZjNzU3ZDtcclxuJHN1Y2Nlc3M6ICMyOGE3NDU7XHJcbiRpbmZvOiAjMTdhMmI4O1xyXG4kd2FybmluZzogI2ZmYzEwNztcclxuJGRhbmdlcjogI2RjMzU0NTtcclxuJGxpZ2h0OiAjZjhmOWZhO1xyXG4kZGFyazogIzM0M2E0MDtcclxuJGxhYmVsLWNvbG9yOiAjNjY2O1xyXG4kYmFja2dyb3VuZC1jb2xvcjogI0VDRjBGMTtcclxuJGJvcmRlckFjdGl2ZUNvbG9yOiAjODBiZGZmO1xyXG4kYm9yZGVyUmFkaXVzOiAwO1xyXG4kZGFya0JsdWU6ICM0NUEyRDI7XHJcbiRsaWdodEdyZWVuOiAjMjdhZTYwO1xyXG4kbGlnaHQtYmx1ZTogI2Y1ZjdmNztcclxuJGJyaWdodEdyYXk6ICM3NTc1NzU7XHJcbiRtYXgtd2lkdGgtbW9iaWxlOiA3NjhweDtcclxuJG1heC13aWR0aC10YWJsZXQ6IDk5MnB4O1xyXG4kbWF4LXdpZHRoLWRlc2t0b3A6IDEyODBweDtcclxuXHJcbi8vIEJFR0lOOiBNYXJnaW5cclxuQG1peGluIG5oLW1nKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1ndCgkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi10b3A6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWdiKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ2woJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tbGVmdDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ3IoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vLyBFTkQ6IE1hcmdpblxyXG5cclxuLy8gQkVHSU46IFBhZGRpbmdcclxuQG1peGluIG5oLXBkKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZzogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZHQoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLXRvcDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZGIoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZGwoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGRyKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy1yaWdodDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogUGFkZGluZ1xyXG5cclxuLy8gQkVHSU46IFdpZHRoXHJcbkBtaXhpbiBuaC13aWR0aCgkd2lkdGgpIHtcclxuICAgIHdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxuICAgIG1pbi13aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbiAgICBtYXgtd2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vLyBFTkQ6IFdpZHRoXHJcblxyXG4vLyBCRUdJTjogSWNvbiBTaXplXHJcbkBtaXhpbiBuaC1zaXplLWljb24oJHNpemUpIHtcclxuICAgIHdpZHRoOiAkc2l6ZTtcclxuICAgIGhlaWdodDogJHNpemU7XHJcbiAgICBmb250LXNpemU6ICRzaXplO1xyXG59XHJcblxyXG4vLyBFTkQ6IEljb24gU2l6ZVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-quick-update/task-quick-update.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-quick-update/task-quick-update.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: TaskQuickUpdateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskQuickUpdateComponent", function() { return TaskQuickUpdateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/task.service */ "./src/app/modules/task/task-management/services/task.service.ts");
/* harmony import */ var _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../shareds/components/ghm-dialog/ghm-dialog.component */ "./src/app/shareds/components/ghm-dialog/ghm-dialog.component.ts");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../task-detail/task-detail.component */ "./src/app/modules/task/task-management/task-detail/task-detail.component.ts");
/* harmony import */ var _task_form_task_form_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../task-form/task-form.component */ "./src/app/modules/task/task-management/task-form/task-form.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../task-role-group/permission-task-role-group.const */ "./src/app/modules/task/task-management/task-role-group/permission-task-role-group.const.ts");
/* harmony import */ var _models_task_check_permission_model__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../models/task-check-permission.model */ "./src/app/modules/task/task-management/models/task-check-permission.model.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_10__);











var TaskQuickUpdateComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TaskQuickUpdateComponent, _super);
    function TaskQuickUpdateComponent(dialog, taskService) {
        var _this = _super.call(this) || this;
        _this.dialog = dialog;
        _this.taskService = taskService;
        _this.saveSuccess = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.remove = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.taskCheckPermission = new _models_task_check_permission_model__WEBPACK_IMPORTED_MODULE_9__["TaskCheckPermission"]();
        _this.listRoleParticipantsCurrentUser = [];
        _this.listStatus = _this.taskService.getListTaskStatus();
        return _this;
    }
    TaskQuickUpdateComponent.prototype.ngOnInit = function () {
    };
    TaskQuickUpdateComponent.prototype.updateName = function (value) {
        if (!value || !value.trim()) {
            this.errorName = 'Tên công việc không được để trông';
            return;
        }
        if (value.length > 256) {
            this.errorName = 'Tên công việc không được lớn hơn 256 ký tự';
            return;
        }
        if (this.taskDetail.name === value) {
            this.errorName = 'Tên công việc chưa thay đổi';
            return;
        }
        this.taskDetail.name = value.trim();
        this.save();
    };
    TaskQuickUpdateComponent.prototype.updateStatus = function () {
        if (this.taskDetail.status < 0 || !this.taskDetail) {
            this.errorStatus = 'Trạng thái không được để trống';
            return;
        }
        this.errorStatus = null;
        this.save();
        this.diaLogUpdateStatus.dismiss();
    };
    TaskQuickUpdateComponent.prototype.deleteTarget = function () {
        var _this = this;
        this.taskService.delete(this.taskDetail.id).subscribe(function () {
            _this.remove.emit(_this.taskDetail.id);
        });
    };
    TaskQuickUpdateComponent.prototype.copy = function () {
        var _this = this;
        var taskFormDialog = this.dialog.open(_task_form_task_form_component__WEBPACK_IMPORTED_MODULE_6__["TaskFormComponent"], {
            id: 'taskFormDialog',
            disableClose: true,
            data: {
                taskDetail: this.taskDetail,
                type: this.taskDetail.type
            }
        });
        this.subscribers.afterTaskFormDialogClosed = taskFormDialog.afterClosed().subscribe(function (result) {
            if (result && result.taskId) {
                _this.dialog.open(_task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_5__["TaskDetailComponent"], { data: { id: result.taskId }, id: "taskDetailDialog-" + result.taskId, disableClose: true });
            }
        });
    };
    TaskQuickUpdateComponent.prototype.save = function () {
        var _this = this;
        this.taskService
            .update(this.taskDetail.id, {
            name: this.taskDetail.name,
            description: this.taskDetail.description,
            concurrencyStamp: this.taskDetail.concurrencyStamp,
            estimateStartDate: this.taskDetail.estimateStartDate,
            estimateEndDate: this.taskDetail.estimateEndDate,
            status: this.taskDetail.status,
            methodCalculatorResult: this.taskDetail.methodCalculatorResult,
            percentCompleted: this.taskDetail.percentCompleted,
            responsibleId: this.taskDetail.responsibleId
        })
            .subscribe(function (result) {
            _this.taskDetail.concurrencyStamp = result.data;
            _this.saveSuccess.emit(_this.taskDetail);
        });
    };
    TaskQuickUpdateComponent.prototype.getDetail = function (taskDetail) {
        this.taskDetail = taskDetail;
        this.getListRoleParticipantsCurrentUser();
        this.taskCheckPermission.isNotification = this.checkPermissionRoleParticipants(_task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_8__["PermissionTaskRoleGroup"].notification);
        this.taskCheckPermission.isComment = this.checkPermissionRoleParticipants(_task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_8__["PermissionTaskRoleGroup"].disputation);
        this.taskCheckPermission.isReport = this.checkPermissionRoleParticipants(_task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_8__["PermissionTaskRoleGroup"].report);
        this.taskCheckPermission.isWrite = this.checkPermissionRoleParticipants(_task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_8__["PermissionTaskRoleGroup"].write);
        this.taskCheckPermission.isFull = this.checkPermissionRoleParticipants(_task_role_group_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_8__["PermissionTaskRoleGroup"].full);
    };
    TaskQuickUpdateComponent.prototype.checkPermissionRoleParticipants = function (permission) {
        var isHavePermission = false;
        lodash__WEBPACK_IMPORTED_MODULE_10__["each"](this.listRoleParticipantsCurrentUser, function (roleParticipants) {
            if (permission & roleParticipants.role) {
                isHavePermission = true;
            }
        });
        return isHavePermission;
    };
    TaskQuickUpdateComponent.prototype.getListRoleParticipantsCurrentUser = function () {
        var _this = this;
        var currentUserInParticipant = lodash__WEBPACK_IMPORTED_MODULE_10__["filter"](this.taskDetail.participants, function (item) {
            return item.userId === _this.currentUser.id;
        });
        if (!currentUserInParticipant) {
            return false;
        }
        else {
            lodash__WEBPACK_IMPORTED_MODULE_10__["each"](currentUserInParticipant, function (currentUser) {
                lodash__WEBPACK_IMPORTED_MODULE_10__["each"](currentUser.roleGroupIds, function (role) {
                    _this.listRoleParticipantsCurrentUser.push(role);
                });
            });
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('diaLogUpdateStatus'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_3__["GhmDialogComponent"])
    ], TaskQuickUpdateComponent.prototype, "diaLogUpdateStatus", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TaskQuickUpdateComponent.prototype, "saveSuccess", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TaskQuickUpdateComponent.prototype, "remove", void 0);
    TaskQuickUpdateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'task-update-quick',
            template: __webpack_require__(/*! ./task-quick-update.component.html */ "./src/app/modules/task/task-management/task-quick-update/task-quick-update.component.html"),
            styles: [__webpack_require__(/*! ./task-quick-update.component.scss */ "./src/app/modules/task/task-management/task-quick-update/task-quick-update.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_7__["MatDialog"],
            _services_task_service__WEBPACK_IMPORTED_MODULE_2__["TaskService"]])
    ], TaskQuickUpdateComponent);
    return TaskQuickUpdateComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_4__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/task/task-management/task-report/const/task-report.const.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-report/const/task-report.const.ts ***!
  \*************************************************************************************/
/*! exports provided: TaskReportType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskReportType", function() { return TaskReportType; });
var TaskReportType = {
    statisticsByUnits: 0,
    statisticsByPersonal: 1,
    statisticsByTarget: 2,
    listByPersonal: 3,
    listByTarget: 4
};


/***/ }),

/***/ "./src/app/modules/task/task-management/task-report/const/task-status.const.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-report/const/task-status.const.ts ***!
  \*************************************************************************************/
/*! exports provided: TasksStatus */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TasksStatus", function() { return TasksStatus; });
var TasksStatus = {
    notStartYet: 0,
    inProgress: 1,
    completed: 2,
    canceled: 3
};


/***/ }),

/***/ "./src/app/modules/task/task-management/task-report/model/task-info-search.viewmodel.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-report/model/task-info-search.viewmodel.ts ***!
  \**********************************************************************************************/
/*! exports provided: TaskInfoSearchViewModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskInfoSearchViewModel", function() { return TaskInfoSearchViewModel; });
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);

var TaskInfoSearchViewModel = /** @class */ (function () {
    function TaskInfoSearchViewModel() {
        this.startDate = moment__WEBPACK_IMPORTED_MODULE_0__().subtract(1, 'months').format('YYYY-MM-DD');
        this.endDate = moment__WEBPACK_IMPORTED_MODULE_0__().format('YYYY-MM-DD');
        this.typeSearchTime = 0;
    }
    return TaskInfoSearchViewModel;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-report/task-report-list/task-report-list.component.html":
/*!***********************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-report/task-report-list/task-report-list.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"target-report-modal-detail\">\r\n    <div class=\"portlet light bordered cm-mgb-0\">\r\n        <div class=\"portlet-title\">\r\n            <ul>\r\n                <li class=\"icon\">\r\n                    <a imgsrc=\"dlg-goal\"></a>\r\n                </li>\r\n                <li>\r\n                    <h4 class=\"bold\">\r\n                        Thông tin công việc\r\n                    </h4>\r\n                </li>\r\n                <li class=\"icon pull-right\">\r\n                    <a href=\"javascript://\" class=\"btn-close\" imgsrc=\"dlg-close\"\r\n                       (click)=\"dialogRef.close()\">\r\n                    </a>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n        <div class=\"portlet-content cm-mg-20\" *ngIf=\"isShowTree\">\r\n            <div>\r\n                <app-task-list-group *ngIf=\"listProgressNotComplete.length > 0\"\r\n                    title=\"Đúng tiến độ\"\r\n                    [classes]=\"'success cm-mgb-10'\"\r\n                                     [showed]=\"isShowProgressNotComplete\"\r\n                    [source]=\"listProgressNotComplete\"\r\n                ></app-task-list-group>\r\n                <app-task-list-group *ngIf=\"listOverDueNotComplete.length > 0\"\r\n                                     title=\"Quá hạn\"\r\n                                     [classes]=\"'red cm-mgb-10'\"\r\n                                     [showed]=\"isShowOverDueNotComplete\"\r\n                                     [source]=\"listOverDueNotComplete\"\r\n                ></app-task-list-group>\r\n                <app-task-list-group *ngIf=\"listProgressCompleted.length > 0\"\r\n                                     title=\"Hoàn thành\"\r\n                                     [classes]=\"'info cm-mgb-10'\"\r\n                                     [showed]=\"isShowProgressComplete\"\r\n                                     [source]=\"listProgressCompleted\"\r\n                ></app-task-list-group>\r\n                <app-task-list-group *ngIf=\"listOverDueCompleted.length > 0\"\r\n                                     title=\"chậm tiến độ\"\r\n                                     [classes]=\"'orange cm-mgb-10'\"\r\n                                     [showed]=\"isShowoverDueComplete\"\r\n                                     [source]=\"listOverDueCompleted\"\r\n                ></app-task-list-group>\r\n                <app-task-list-group *ngIf=\"listDeleted.length > 0\"\r\n                                     title=\"Đã xóa\"\r\n                                     [showed]=\"isShowDelete\"\r\n                                     [classes]=\"'gray cm-mgb-10'\"\r\n                                     [source]=\"listDeleted\"\r\n                ></app-task-list-group>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-report/task-report-list/task-report-list.component.scss":
/*!***********************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-report/task-report-list/task-report-list.component.scss ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".target-report-modal-detail .portlet-title {\n  padding: 0 !important; }\n  .target-report-modal-detail .portlet-title ul {\n    list-style: none;\n    margin-bottom: 0;\n    padding: 0;\n    display: block;\n    width: 100%; }\n  .target-report-modal-detail .portlet-title ul li {\n      display: inline-block;\n      float: left; }\n  .target-report-modal-detail .portlet-title ul li.icon a {\n        display: block;\n        width: 48px;\n        height: 48px; }\n  .target-report-modal-detail .portlet-title ul li.icon a.btn-close {\n          border-left: 1px solid #ddd; }\n  .target-report-modal-detail .portlet-title ul li h4 {\n        margin-top: 15px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC90YXNrLXJlcG9ydC90YXNrLXJlcG9ydC1saXN0L0Q6XFxQcm9qZWN0XFxHaG1BcHBsaWNhdGlvblxcY2xpZW50c1xcZ2htYXBwbGljYXRpb25jbGllbnQvc3JjXFxhcHBcXG1vZHVsZXNcXHRhc2tcXHRhc2stbWFuYWdlbWVudFxcdGFzay1yZXBvcnRcXHRhc2stcmVwb3J0LWxpc3RcXHRhc2stcmVwb3J0LWxpc3QuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21vZHVsZXMvdGFzay90YXNrLW1hbmFnZW1lbnQvdGFzay1yZXBvcnQvdGFzay1yZXBvcnQtbGlzdC9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXNzZXRzXFxzdHlsZXNcXF9jb25maWcuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUVRLHFCQUFxQixFQUFBO0VBRjdCO0lBS1ksZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixVQUFVO0lBQ1YsY0FBYztJQUNkLFdBQVcsRUFBQTtFQVR2QjtNQVlnQixxQkFBcUI7TUFDckIsV0FBVyxFQUFBO0VBYjNCO1FBaUJ3QixjQUFjO1FBQ2QsV0FBVztRQUNYLFlBQVksRUFBQTtFQW5CcEM7VUFzQjRCLDJCQ25CVixFQUFBO0VESGxCO1FBNEJvQixnQkFBZ0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvdGFzay90YXNrLW1hbmFnZW1lbnQvdGFzay1yZXBvcnQvdGFzay1yZXBvcnQtbGlzdC90YXNrLXJlcG9ydC1saXN0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCAnLi4vLi4vLi4vLi4vLi4vLi4vYXNzZXRzL3N0eWxlcy9jb25maWcnO1xyXG4udGFyZ2V0LXJlcG9ydC1tb2RhbC1kZXRhaWwge1xyXG4gICAgLnBvcnRsZXQtdGl0bGUge1xyXG4gICAgICAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcclxuXHJcbiAgICAgICAgdWwge1xyXG4gICAgICAgICAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgICAgICAgICBsaSB7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuXHJcbiAgICAgICAgICAgICAgICAmLmljb24ge1xyXG4gICAgICAgICAgICAgICAgICAgIGEge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDQ4cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogNDhweDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICYuYnRuLWNsb3NlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgJGJvcmRlckNvbG9yO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGg0IHtcclxuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiIsIiRkZWZhdWx0LWNvbG9yOiAjMjIyO1xyXG4kZm9udC1mYW1pbHk6IFwiQXJpYWxcIiwgdGFob21hLCBIZWx2ZXRpY2EgTmV1ZTtcclxuJGNvbG9yLWJsdWU6ICMzNTk4ZGM7XHJcbiRtYWluLWNvbG9yOiAjMDA3NDU1O1xyXG4kYm9yZGVyQ29sb3I6ICNkZGQ7XHJcbiRzZWNvbmQtY29sb3I6ICNiMDFhMWY7XHJcbiR0YWJsZS1iYWNrZ3JvdW5kLWNvbG9yOiAjMDA5Njg4O1xyXG4kYmx1ZTogIzAwN2JmZjtcclxuJGRhcmstYmx1ZTogIzAwNzJCQztcclxuJGJyaWdodC1ibHVlOiAjZGZmMGZkO1xyXG4kaW5kaWdvOiAjNjYxMGYyO1xyXG4kcHVycGxlOiAjNmY0MmMxO1xyXG4kcGluazogI2U4M2U4YztcclxuJHJlZDogI2RjMzU0NTtcclxuJG9yYW5nZTogI2ZkN2UxNDtcclxuJHllbGxvdzogI2ZmYzEwNztcclxuJGdyZWVuOiAjMjhhNzQ1O1xyXG4kdGVhbDogIzIwYzk5NztcclxuJGN5YW46ICMxN2EyYjg7XHJcbiR3aGl0ZTogI2ZmZjtcclxuJGdyYXk6ICM4NjhlOTY7XHJcbiRncmF5LWRhcms6ICMzNDNhNDA7XHJcbiRwcmltYXJ5OiAjMDA3YmZmO1xyXG4kc2Vjb25kYXJ5OiAjNmM3NTdkO1xyXG4kc3VjY2VzczogIzI4YTc0NTtcclxuJGluZm86ICMxN2EyYjg7XHJcbiR3YXJuaW5nOiAjZmZjMTA3O1xyXG4kZGFuZ2VyOiAjZGMzNTQ1O1xyXG4kbGlnaHQ6ICNmOGY5ZmE7XHJcbiRkYXJrOiAjMzQzYTQwO1xyXG4kbGFiZWwtY29sb3I6ICM2NjY7XHJcbiRiYWNrZ3JvdW5kLWNvbG9yOiAjRUNGMEYxO1xyXG4kYm9yZGVyQWN0aXZlQ29sb3I6ICM4MGJkZmY7XHJcbiRib3JkZXJSYWRpdXM6IDA7XHJcbiRkYXJrQmx1ZTogIzQ1QTJEMjtcclxuJGxpZ2h0R3JlZW46ICMyN2FlNjA7XHJcbiRsaWdodC1ibHVlOiAjZjVmN2Y3O1xyXG4kYnJpZ2h0R3JheTogIzc1NzU3NTtcclxuJG1heC13aWR0aC1tb2JpbGU6IDc2OHB4O1xyXG4kbWF4LXdpZHRoLXRhYmxldDogOTkycHg7XHJcbiRtYXgtd2lkdGgtZGVza3RvcDogMTI4MHB4O1xyXG5cclxuLy8gQkVHSU46IE1hcmdpblxyXG5AbWl4aW4gbmgtbWcoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW46ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWd0KCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLXRvcDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ2IoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1nbCgkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1ncigkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1yaWdodDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogTWFyZ2luXHJcblxyXG4vLyBCRUdJTjogUGFkZGluZ1xyXG5AbWl4aW4gbmgtcGQoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkdCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctdG9wOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkYigkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctYm90dG9tOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkbCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctbGVmdDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZHIoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBQYWRkaW5nXHJcblxyXG4vLyBCRUdJTjogV2lkdGhcclxuQG1peGluIG5oLXdpZHRoKCR3aWR0aCkge1xyXG4gICAgd2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG4gICAgbWluLXdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxuICAgIG1heC13aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogV2lkdGhcclxuXHJcbi8vIEJFR0lOOiBJY29uIFNpemVcclxuQG1peGluIG5oLXNpemUtaWNvbigkc2l6ZSkge1xyXG4gICAgd2lkdGg6ICRzaXplO1xyXG4gICAgaGVpZ2h0OiAkc2l6ZTtcclxuICAgIGZvbnQtc2l6ZTogJHNpemU7XHJcbn1cclxuXHJcbi8vIEVORDogSWNvbiBTaXplXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-report/task-report-list/task-report-list.component.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-report/task-report-list/task-report-list.component.ts ***!
  \*********************************************************************************************************/
/*! exports provided: TaskReportListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskReportListComponent", function() { return TaskReportListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _const_task_status_const__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../const/task-status.const */ "./src/app/modules/task/task-management/task-report/const/task-status.const.ts");
/* harmony import */ var _target_targets_target_report_const_type_status_report_const__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../target/targets/target-report/const/type-status-report.const */ "./src/app/modules/task/target/targets/target-report/const/type-status-report.const.ts");







var TaskReportListComponent = /** @class */ (function () {
    function TaskReportListComponent(data, dialogRef, dialog) {
        this.data = data;
        this.dialogRef = dialogRef;
        this.dialog = dialog;
        this.tasksStatus = _const_task_status_const__WEBPACK_IMPORTED_MODULE_5__["TasksStatus"];
        this.isShowTree = false;
        this.listProgressNotComplete = [];
        this.listOverDueNotComplete = [];
        this.listProgressCompleted = [];
        this.listOverDueCompleted = [];
        this.listDeleted = [];
        this.typeStatusReport = _target_targets_target_report_const_type_status_report_const__WEBPACK_IMPORTED_MODULE_6__["TypeStatusReport"];
        this.isShowProgressNotComplete = false;
        this.isShowOverDueNotComplete = false;
        this.isShowProgressComplete = false;
        this.isShowoverDueComplete = false;
        this.isShowDelete = false;
    }
    TaskReportListComponent.prototype.ngOnInit = function () {
        if (this.data.listTask) {
            this.convertData(this.data.listTask, this.data.type);
            this.setTypeStatusReport(this.data.type);
        }
    };
    TaskReportListComponent.prototype.convertData = function (listTask, typeStatus) {
        var _this = this;
        var dateTimeNow = new Date(moment__WEBPACK_IMPORTED_MODULE_3__().format('YYYY-MM-DD'));
        this.listProgressNotComplete = lodash__WEBPACK_IMPORTED_MODULE_4__["filter"](listTask, function (item) {
            return !item.isDelete && (item.status === _this.tasksStatus.inProgress ||
                item.status === _this.tasksStatus.notStartYet) && new Date(item.estimateEndDate) >= dateTimeNow;
        });
        this.listOverDueNotComplete = lodash__WEBPACK_IMPORTED_MODULE_4__["filter"](listTask, function (item) {
            return !item.isDelete && (item.status === _this.tasksStatus.inProgress ||
                item.status === _this.tasksStatus.notStartYet) && new Date(item.estimateEndDate) < dateTimeNow;
        });
        this.listProgressCompleted = lodash__WEBPACK_IMPORTED_MODULE_4__["filter"](listTask, function (item) {
            return !item.isDelete && item.status === _this.tasksStatus.completed && item.estimateEndDate >= item.endDate;
        });
        this.listOverDueCompleted = lodash__WEBPACK_IMPORTED_MODULE_4__["filter"](listTask, function (item) {
            return !item.isDelete && item.status === _this.tasksStatus.completed && item.estimateEndDate < item.endDate;
        });
        this.listDeleted = lodash__WEBPACK_IMPORTED_MODULE_4__["filter"](listTask, function (item) {
            return item.isDelete;
        });
        this.isShowTree = true;
    };
    TaskReportListComponent.prototype.setTypeStatusReport = function (type) {
        this.isShowProgressNotComplete = false;
        this.isShowOverDueNotComplete = false;
        this.isShowProgressComplete = false;
        this.isShowoverDueComplete = false;
        if (type === this.typeStatusReport.progressComplete) {
            this.isShowProgressComplete = true;
        }
        else if (type === this.typeStatusReport.overDueComplete) {
            this.isShowoverDueComplete = true;
        }
        else if (type === this.typeStatusReport.progressNotComplete) {
            this.isShowProgressNotComplete = true;
        }
        else if (type === this.typeStatusReport.overDueNotComplete) {
            this.isShowOverDueNotComplete = true;
        }
        else {
            this.isShowDelete = true;
        }
    };
    TaskReportListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-report-list',
            template: __webpack_require__(/*! ./task-report-list.component.html */ "./src/app/modules/task/task-management/task-report/task-report-list/task-report-list.component.html"),
            styles: [__webpack_require__(/*! ./task-report-list.component.scss */ "./src/app/modules/task/task-management/task-report/task-report-list/task-report-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"]])
    ], TaskReportListComponent);
    return TaskReportListComponent;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-report/task-report.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-report/task-report.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span i18n=\"@@targetReport\">\r\n        Báo cáo mục tiêu theo đơn vị\r\n    </span>\r\n</h1>\r\n\r\n<div class=\"cm-mgb-10\">\r\n    <mat-expansion-panel [expanded]=\"true\">\r\n        <mat-expansion-panel-header class=\"panel-header\">\r\n            <mat-panel-title class=\"title\">\r\n                Loại báo cáo\r\n            </mat-panel-title>\r\n        </mat-expansion-panel-header>\r\n        <div class=\"item cm-pdt-15 cm-pdb-15\">\r\n            <label class=\"col-sm-5 cm-pdt-5\">Loại báo cáo</label>\r\n            <ghm-select [(ngModel)]=\"typeReportSelected\"\r\n                        [multiple]=\"false\"\r\n                        (itemSelected)=\"search()\"\r\n                        [data]=\"listTypeSelect\">\r\n            </ghm-select>\r\n        </div>\r\n    </mat-expansion-panel>\r\n</div>\r\n<div class=\"cm-mgb-10\"\r\n     *ngIf=\"typeReportSelected === taskReportType.statisticsByUnits || typeReportSelected === taskReportType.statisticsByPersonal || typeReportSelected === taskReportType.statisticsByTarget\">\r\n    <mat-expansion-panel [expanded]=\"true\">\r\n        <mat-expansion-panel-header class=\"panel-header\">\r\n            <mat-panel-title class=\"title\">\r\n                Thông tin tìm kiếm\r\n            </mat-panel-title>\r\n        </mat-expansion-panel-header>\r\n        <div class=\"wrap-search cm-pdt-15 cm-pdb-15\">\r\n            <div class=\"item\">\r\n                <label class=\"col-sm-5 cm-pdt-5\">Chọn thời gian</label>\r\n                <ghm-select-datetime [setStartDate]=\"infoSearch.startDate\" [startDate]=\"infoSearch.startDate\"\r\n                                     [endDate]=\"infoSearch.endDate\"\r\n                                     (selectDate)=\"selectDate($event)\"></ghm-select-datetime>\r\n            </div>\r\n            <div class=\"item cm-pdt-10\">\r\n                <label class=\"col-sm-5 cm-pdt-5\">Tìm kiếm thời gian theo</label>\r\n                <ghm-select [(ngModel)]=\"infoSearch.typeSearchTime\" [selectedItem]=\"\"\r\n                            [data]=\"listTypeSearchTime\"></ghm-select>\r\n            </div>\r\n            <div class=\"item cm-pdt-10\" *ngIf=\"typeReportSelected === taskReportType.statisticsByPersonal\">\r\n                <label class=\"col-sm-3 cm-pdt-5\">Đơn vị</label>\r\n                <nh-tree [data]=\"listOffice\" (nodeSelected)=\"selectedOfficeId($event)\"></nh-tree>\r\n\r\n            </div>\r\n            <div class=\"center\" *ngIf=\"typeReportSelected === taskReportType.statisticsByUnits || typeReportSelected === taskReportType.statisticsByPersonal\">\r\n                <ghm-button classes=\"btn blue cm-mgt-10\" (click)=\"search()\">\r\n                    Tìm kiếm\r\n                </ghm-button>\r\n            </div>\r\n            <div class=\"wrap\" *ngIf=\"typeReportSelected === taskReportType.statisticsByTarget\">\r\n                <div class=\"item cm-pdt-10\">\r\n                    <label class=\"col-sm-4 cm-pdt-5\"></label>\r\n                    <mat-radio-group [(ngModel)]=\"typeTarget\">\r\n                        <mat-radio-button color=\"primary\" checked value=\"0\">Mục tiêu nhân viên\r\n                        </mat-radio-button>\r\n                        <mat-radio-button color=\"primary\" class=\"cm-mgl-10\" value=\"1\">Mục tiêu đơn vị</mat-radio-button>\r\n                    </mat-radio-group>\r\n                </div>\r\n                <div class=\"item cm-pdt-10\" *ngIf=\"typeTarget == 0\">\r\n                    <label class=\"col-sm-5 cm-pdt-5\" i18n=\"@@user\" ghmLabel=\"Nhân viên\" [required]=\"true\"\r\n                           class=\"col-sm-5 cm-pdt-5\"></label>\r\n                    <ghm-select [(ngModel)]=\"infoSearch.userId\"\r\n                                (itemSelected)=\"selectedUser($event)\"\r\n                                [title]=\"'Chọn nhân viên'\"\r\n                                [data]=\"listUserPicker\"></ghm-select>\r\n                </div>\r\n                <div class=\"item cm-pdt-10\" *ngIf=\"typeTarget == 1\">\r\n                    <label class=\"col-sm-5 cm-pdt-5\" i18n=\"@@office\" ghmLabel=\"Phòng ban\" [required]=\"true\"\r\n                           class=\"col-sm-5 cm-pdt-5\"></label>\r\n                    <nh-tree [data]=\"listOffice\" (nodeSelected)=\"selectedOffice($event)\"></nh-tree>\r\n                </div>\r\n                <div class=\"item cm-pdt-10\" *ngIf=\"isShowTarget\">\r\n                    <label class=\"col-sm-5 cm-pdt-5\" i18n=\"@@target\" ghmLabel=\"Mục tiêu\" [required]=\"true\"\r\n                           class=\"col-sm-5 cm-pdt-5\"></label>\r\n                    <ghm-select [(ngModel)]=\"infoSearch.targetId\"\r\n                                [title]=\"'Chọn mục tiêu'\"\r\n                                (itemSelected)=\"selectTarget()\"\r\n                                [data]=\"listTargetPicker\"></ghm-select>\r\n\r\n                </div>\r\n                <div class=\"item\" *ngIf=\"isShowErrorTarget\">\r\n                    <label class=\"col-sm-5\"></label>\r\n                    <span class=\"color-red\">Bạn chưa chọn mục tiêu</span>\r\n                </div>\r\n                <!--<div class=\"item cm-pdt-10\">-->\r\n                <!--<label class=\"col-sm-5 cm-pdt-5\"></label>-->\r\n                <!--<mat-checkbox [(ngModel)]=\"infoSearch.showOfficeHaveTarget\" [color]=\"'primary'\">-->\r\n                <!--Chỉ hiện thị các phòng ban có mục tiêu-->\r\n                <!--</mat-checkbox>-->\r\n                <!--</div>-->\r\n                <div class=\"center\">\r\n                    <ghm-button classes=\"btn blue cm-mgt-10\" [class.disabled]=\"isShowErrorTarget\" (click)=\"searchStatisticsByTarget()\">\r\n                        Tìm kiếm\r\n                    </ghm-button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </mat-expansion-panel>\r\n</div>\r\n<div class=\"cm-mgb-10\"\r\n     *ngIf=\"typeReportSelected === taskReportType.statisticsByUnits || typeReportSelected === taskReportType.statisticsByPersonal || isShowTargetReport\">\r\n    <mat-expansion-panel [expanded]=\"true\">\r\n        <mat-expansion-panel-header class=\"panel-header\">\r\n            <mat-panel-title class=\"title\">\r\n                Biểu đồ báo cáo\r\n            </mat-panel-title>\r\n        </mat-expansion-panel-header>\r\n        <div class=\"chart-report col-sm-6\">\r\n            <div style=\"...\">\r\n                <ngx-charts-pie-chart *ngIf=\"isDataLoaded\"\r\n                                      [results]=\"progressData\"\r\n                                      [view]=\"view\"\r\n                                      [scheme]=\"colorScheme\"\r\n                                      [tooltipDisabled]=\"true\"\r\n                                      [tooltipText]=\"''\"\r\n                                      [animations]=\"true\"\r\n                >\r\n                </ngx-charts-pie-chart>\r\n            </div>\r\n        </div>\r\n        <div class=\"sub-chart-report col-sm-6 cm-mgt-10\">\r\n            <!--<ghm-select></ghm-select>-->\r\n            <h4><b>Mô tả màu sắc</b></h4>\r\n            <div class=\"item\">\r\n                <div class=\"notComplete cm-bg-green cm-pd-10 cm-mgr-10 cm-mgb-10\">\r\n                </div>\r\n                <span>\r\n                    Chưa hoàn thành\r\n                </span>\r\n            </div>\r\n            <div class=\"item\">\r\n                <div class=\"overDue cm-bg-red cm-pd-10 cm-mgr-10 cm-mgb-10\">\r\n\r\n                </div>\r\n                <span>\r\n                    Quá hạn\r\n                </span>\r\n            </div>\r\n            <div class=\"item\">\r\n                <div class=\"completed cm-bg-blue cm-pd-10 cm-mgr-10 cm-mgb-10\">\r\n\r\n                </div>\r\n                <span>\r\n                    Hoàn thành\r\n                </span>\r\n            </div>\r\n            <div class=\"item\">\r\n                <div class=\"completedOverDue cm-bg-orange cm-pd-10 cm-mgr-10 cm-mgb-10\">\r\n\r\n                </div>\r\n                <span>\r\n                    Hoàn thành chậm tiến độ\r\n                </span>\r\n            </div>\r\n            <div class=\"item\">\r\n                <div class=\"dontHaveData cm-bg-white cm-pd-10 cm-mgr-10 cm-mgb-10\">\r\n\r\n                </div>\r\n                <span>\r\n                    Không có dữ liệu\r\n                </span>\r\n            </div>\r\n        </div>\r\n    </mat-expansion-panel>\r\n</div>\r\n<div class=\"cm-mgb-10\"\r\n     *ngIf=\"typeReportSelected === taskReportType.statisticsByUnits || typeReportSelected === taskReportType.statisticsByPersonal || isShowTargetReport\">\r\n    <mat-expansion-panel [expanded]=\"true\">\r\n        <mat-expansion-panel-header class=\"panel-header\">\r\n            <mat-panel-title class=\"title\">\r\n                Số liêu báo cáo\r\n            </mat-panel-title>\r\n        </mat-expansion-panel-header>\r\n        <div class=\"wrap-result-report cm-mgt-15\">\r\n            <table class=\"table table-bordered\">\r\n                <thead class=\"cm-bg-gray\">\r\n                <tr>\r\n                    <th class=\"index center middle\" rowspan=\"2\">STT</th>\r\n                    <th *ngIf=\"typeReportSelected === taskReportType.statisticsByUnits\" class=\"center middle\"\r\n                        rowspan=\"2\">Đơn vị\r\n                    </th>\r\n                    <th *ngIf=\"typeReportSelected === taskReportType.statisticsByPersonal || typeReportSelected === taskReportType.statisticsByTarget\" class=\"center middle\"\r\n                        rowspan=\"2\">Nhân viên\r\n                    </th>\r\n                    <th class=\"center middle\" rowspan=\"2\">Tổng công việc hoạt động</th>\r\n                    <th class=\"center\" colspan=\"2\">Chưa hoàn thành</th>\r\n                    <th class=\"center\" colspan=\"2\">Hoàn thành</th>\r\n                    <th class=\"center middle\" rowspan=\"2\">Hủy</th>\r\n                </tr>\r\n                <tr>\r\n                    <th class=\"center color-green\">Đúng tiến độ</th>\r\n                    <th class=\"center color-red\">Quá hạn</th>\r\n                    <th class=\"center text-color-blue\">Đúng tiến độ</th>\r\n                    <th class=\"center color-orange\">Chậm tiến độ</th>\r\n                </tr>\r\n                </thead>\r\n                <tbody>\r\n                <tr *ngFor=\"let item of listTaskReport; let i = index\">\r\n                    <td class=\"center\">{{i + 1}}</td>\r\n                    <td *ngIf=\"typeReportSelected === taskReportType.statisticsByUnits\">{{item.officeName}}</td>\r\n                    <td *ngIf=\"typeReportSelected === taskReportType.statisticsByPersonal || typeReportSelected === taskReportType.statisticsByTarget\">\r\n                        <div>{{item.fullName}}</div>\r\n                        <div><i>{{item.officeName}}</i></div>\r\n                    </td>\r\n                    <td class=\"center\"><a\r\n                        (click)=\"showListTarget(item.officeId, item.userId, typeStatusReport.all)\">{{item.totalTask}}</a></td>\r\n                    <ng-container *ngIf=\"item.progressNotComplete > 0; else progressNotComplete\">\r\n                        <td class=\"center\"><a\r\n                            (click)=\"showListTarget(item.officeId, item.userId, typeStatusReport.progressNotComplete)\"\r\n                            class=\"color-green\"\r\n                            [class.text-decoration-underline]=\"item.progressNotComplete > 0\">{{item.progressNotComplete}}</a>\r\n                        </td>\r\n                    </ng-container>\r\n                    <ng-template #progressNotComplete>\r\n                        <td class=\"center color-green\">0</td>\r\n                    </ng-template>\r\n\r\n                    <ng-container *ngIf=\"item.overDueNotComplete > 0; else overDueNotComplete\">\r\n                        <td class=\"center\"><a\r\n                            (click)=\"showListTarget(item.officeId, item.userId, typeStatusReport.overDueNotComplete)\"\r\n                            class=\"color-red\"\r\n                            [class.text-decoration-underline]=\"item.overDueNotComplete > 0\">{{item.overDueNotComplete}}</a>\r\n                        </td>\r\n                    </ng-container>\r\n                    <ng-template #overDueNotComplete>\r\n                        <td class=\"center color-red\">0</td>\r\n                    </ng-template>\r\n\r\n                    <ng-container *ngIf=\"item.progressComplete > 0; else progressComplete\">\r\n                        <td class=\"center\"><a (click)=\"showListTarget(item.officeId, item.userId, typeStatusReport.progressComplete)\"\r\n                                              class=\"text-color-blue\"\r\n                                              [class.text-decoration-underline]=\"item.progressComplete > 0\">{{item.progressComplete}}</a>\r\n                        </td>\r\n                    </ng-container>\r\n                    <ng-template #progressComplete>\r\n                        <td class=\"center text-color-blue\">0</td>\r\n                    </ng-template>\r\n\r\n                    <ng-container *ngIf=\"item.overDueComplete > 0; else overDueComplete\">\r\n                        <td class=\"center\"><a (click)=\"showListTarget(item.officeId, item.userId, typeStatusReport.overDueComplete)\"\r\n                                              class=\"color-orange\"\r\n                                              [class.text-decoration-underline]=\"item.overDueComplete > 0\">{{item.overDueComplete}}</a>\r\n                        </td>\r\n                    </ng-container>\r\n                    <ng-template #overDueComplete>\r\n                        <td class=\"center color-orange\">0</td>\r\n                    </ng-template>\r\n\r\n                    <ng-container *ngIf=\"item.totalDestroy > 0; else totalDestroy\">\r\n                        <td class=\"center\" (click)=\"showListTarget(item.officeId, item.userId, typeStatusReport.deleted)\">\r\n                            <a>{{item.totalDestroy}}</a></td>\r\n                    </ng-container>\r\n                    <ng-template #totalDestroy>\r\n                        <td class=\"center\">0</td>\r\n                    </ng-template>\r\n                </tr>\r\n                <tr class=\"cm-bg-gray\">\r\n                    <td rowspan=\"2\" colspan=\"2\" class=\"center middle\">Tổng</td>\r\n                    <td rowspan=\"2\" class=\"center middle\">{{ totalTaskActive }}</td>\r\n                    <td class=\"center\">{{ totalProgressNotComplete }}</td>\r\n                    <td class=\"center\">{{ totalOverDueNotComplete }}</td>\r\n                    <td class=\"center\">{{ totalProgressComplete }}</td>\r\n                    <td class=\"center\">{{ totalOverDueComplete }}</td>\r\n                    <td class=\"center middle\" rowspan=\"2\">{{ totalDelete }}</td>\r\n                </tr>\r\n                <tr class=\"cm-bg-gray\">\r\n                    <td class=\"center\" colspan=\"2\">{{totalNotComplete}}</td>\r\n                    <td class=\"center\" colspan=\"2\">{{totalComplete}}</td>\r\n                </tr>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n    </mat-expansion-panel>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-report/task-report.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-report/task-report.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".item {\n  display: flex;\n  width: 40%;\n  margin-left: 30%; }\n  .item mat-checkbox {\n    width: 100%; }\n  .sub-chart-report .item {\n  margin-left: 0px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC90YXNrLXJlcG9ydC9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXBwXFxtb2R1bGVzXFx0YXNrXFx0YXNrLW1hbmFnZW1lbnRcXHRhc2stcmVwb3J0XFx0YXNrLXJlcG9ydC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLGFBQWE7RUFDYixVQUFTO0VBQ1QsZ0JBQWUsRUFBQTtFQUhuQjtJQUtRLFdBQVcsRUFBQTtFQUduQjtFQUVRLDJCQUEyQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC90YXNrLXJlcG9ydC90YXNrLXJlcG9ydC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi8uLi8uLi8uLi8uLi9zcmMvYXNzZXRzL3N0eWxlcy9jb25maWdcIjtcclxuLml0ZW0ge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIHdpZHRoOjQwJTtcclxuICAgIG1hcmdpbi1sZWZ0OjMwJTtcclxuICAgIG1hdC1jaGVja2JveCB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbn1cclxuLnN1Yi1jaGFydC1yZXBvcnQge1xyXG4gICAgLml0ZW0ge1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwcHggIWltcG9ydGFudDs7XHJcbiAgICB9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-report/task-report.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-report/task-report.component.ts ***!
  \***********************************************************************************/
/*! exports provided: TaskReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskReportComponent", function() { return TaskReportComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _shareds_components_nh_user_picker_nh_user_picker_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../shareds/components/nh-user-picker/nh-user-picker.service */ "./src/app/shareds/components/nh-user-picker/nh-user-picker.service.ts");
/* harmony import */ var _hr_organization_office_services_office_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../hr/organization/office/services/office.service */ "./src/app/modules/hr/organization/office/services/office.service.ts");
/* harmony import */ var _target_targets_target_report_const_target_type_search_time_const__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../target/targets/target-report/const/target-type-search-time.const */ "./src/app/modules/task/target/targets/target-report/const/target-type-search-time.const.ts");
/* harmony import */ var _target_targets_target_report_const_type_status_report_const__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../target/targets/target-report/const/type-status-report.const */ "./src/app/modules/task/target/targets/target-report/const/type-status-report.const.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _const_task_report_const__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./const/task-report.const */ "./src/app/modules/task/task-management/task-report/const/task-report.const.ts");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _model_task_info_search_viewmodel__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./model/task-info-search.viewmodel */ "./src/app/modules/task/task-management/task-report/model/task-info-search.viewmodel.ts");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../services/task.service */ "./src/app/modules/task/task-management/services/task.service.ts");
/* harmony import */ var _task_report_list_task_report_list_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./task-report-list/task-report-list.component */ "./src/app/modules/task/task-management/task-report/task-report-list/task-report-list.component.ts");
/* harmony import */ var _target_targets_services_target_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../target/targets/services/target.service */ "./src/app/modules/task/target/targets/services/target.service.ts");
















var TaskReportComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TaskReportComponent, _super);
    function TaskReportComponent(diaLog, userPickerService, officeService, taskService, targetService) {
        var _this = _super.call(this) || this;
        _this.diaLog = diaLog;
        _this.userPickerService = userPickerService;
        _this.officeService = officeService;
        _this.taskService = taskService;
        _this.targetService = targetService;
        _this.isDataLoaded = false;
        _this.progressData = [];
        _this.typeTarget = '0';
        _this.view = [700, 400];
        _this.colorScheme = {
            domain: []
        };
        _this.listTreeSearchTarget = [];
        _this.taskReportType = _const_task_report_const__WEBPACK_IMPORTED_MODULE_10__["TaskReportType"];
        _this.typeReportSelected = _this.taskReportType.statisticsByUnits;
        _this.listOffice = [];
        _this.infoSearch = new _model_task_info_search_viewmodel__WEBPACK_IMPORTED_MODULE_12__["TaskInfoSearchViewModel"]();
        _this.targetTypeSearchTimeConst = _target_targets_target_report_const_target_type_search_time_const__WEBPACK_IMPORTED_MODULE_5__["TargetTypeSearchTimeConst"];
        _this.totalRows = 0;
        _this.isShowResultSearch = false;
        _this.isShowTable = false;
        _this.listTypeSelect = [
            { id: _this.taskReportType.statisticsByUnits, name: 'Thống kê công việc theo đơn vị' },
            { id: _this.taskReportType.statisticsByPersonal, name: 'Thống kê công việc theo nhân viên' },
            { id: _this.taskReportType.statisticsByTarget, name: 'Thống kê công việc của mục tiêu' },
            { id: _this.taskReportType.listByPersonal, name: 'Danh sách công việc của nhân viên' },
            { id: _this.taskReportType.listByTarget, name: 'Danh sách công việc của mục tiêu' }
        ];
        _this.listTypeSearchTime = [
            { id: _this.targetTypeSearchTimeConst.starteDateToCompleteDate, name: 'Khoảng ngày: Ngày bắt đầu - Hạn hoàn thành' },
            { id: _this.targetTypeSearchTimeConst.startDate, name: 'Ngày bắt đầu' },
            { id: _this.targetTypeSearchTimeConst.createDate, name: 'Ngày tạo' },
            { id: _this.targetTypeSearchTimeConst.endDate, name: 'Hạn hoàn thành' },
            { id: _this.targetTypeSearchTimeConst.completeDate, name: 'Ngày hoàn thành' }
        ];
        _this.selectedItem = { id: _this.taskReportType.statisticsByUnits, name: 'Thống kê mục tiêu theo đơn vị' };
        _this.totalTaskActive = 0;
        _this.totalProgressNotComplete = 0;
        _this.totalOverDueNotComplete = 0;
        _this.totalProgressComplete = 0;
        _this.totalOverDueComplete = 0;
        _this.totalNotComplete = 0;
        _this.totalComplete = 0;
        _this.totalDelete = 0;
        _this.isShowTarget = false;
        _this.typeStatusReport = _target_targets_target_report_const_type_status_report_const__WEBPACK_IMPORTED_MODULE_6__["TypeStatusReport"];
        _this.listTargetPicker = [];
        _this.listTaskReport = [];
        _this.isShowErrorTarget = false;
        _this.isShowTargetReport = false;
        return _this;
    }
    TaskReportComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.urlOfficeSuggestion = _environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].apiGatewayUrl + "api/v1/hr/offices/suggestions";
        this.appService.setupPage(this.pageId.TASK, this.pageId.TASK_REPORT, 'Quản lý công việc', 'Báo cáo công việc');
        this.officeService.getTree().subscribe(function (result) {
            _this.listOffice = result;
        });
        this.search();
        this.getListUser();
    };
    TaskReportComponent.prototype.searchStatisticsByTarget = function () {
        var _this = this;
        this.totalTaskActive = 0;
        this.totalProgressNotComplete = 0;
        this.totalOverDueNotComplete = 0;
        this.totalProgressComplete = 0;
        this.totalOverDueComplete = 0;
        this.totalNotComplete = 0;
        this.totalComplete = 0;
        this.totalDelete = 0;
        this.progressData = [];
        this.isDataLoaded = false;
        if (this.typeReportSelected === this.taskReportType.statisticsByTarget) {
            if (!this.infoSearch.targetId || this.infoSearch.targetId < 0) {
                this.isShowErrorTarget = true;
                this.isShowTargetReport = false;
            }
            else {
                this.isShowErrorTarget = false;
                this.taskService.searchForReport(this.typeReportSelected, this.infoSearch)
                    .subscribe(function (result) {
                    _this.listTaskReport = result.items;
                    lodash__WEBPACK_IMPORTED_MODULE_9__["each"](_this.listTaskReport, function (item) {
                        _this.totalTaskActive += item.totalTask;
                        _this.totalProgressNotComplete += item.progressNotComplete;
                        _this.totalOverDueNotComplete += item.overDueNotComplete;
                        _this.totalProgressComplete += item.progressComplete;
                        _this.totalOverDueComplete += item.overDueComplete;
                        _this.totalNotComplete += item.progressNotComplete;
                        _this.totalNotComplete += item.overDueNotComplete;
                        _this.totalComplete += item.overDueComplete;
                        _this.totalComplete += item.progressComplete;
                        _this.totalDelete += item.totalDestroy;
                    });
                    _this.handleValuePieChart();
                    _this.isShowTargetReport = true;
                });
                //this.infoSearch = new TaskInfoSearchViewModel();
            }
        }
    };
    TaskReportComponent.prototype.search = function () {
        var _this = this;
        if (this.typeReportSelected !== this.taskReportType.statisticsByTarget) {
            this.totalTaskActive = 0;
            this.totalProgressNotComplete = 0;
            this.totalOverDueNotComplete = 0;
            this.totalProgressComplete = 0;
            this.totalOverDueComplete = 0;
            this.totalNotComplete = 0;
            this.totalComplete = 0;
            this.totalDelete = 0;
            this.progressData = [];
            this.isDataLoaded = false;
            this.isShowErrorTarget = false;
            this.taskService.searchForReport(this.typeReportSelected, this.infoSearch)
                .subscribe(function (result) {
                _this.listTaskReport = result.items;
                lodash__WEBPACK_IMPORTED_MODULE_9__["each"](_this.listTaskReport, function (item) {
                    _this.totalTaskActive += item.totalTask;
                    _this.totalProgressNotComplete += item.progressNotComplete;
                    _this.totalOverDueNotComplete += item.overDueNotComplete;
                    _this.totalProgressComplete += item.progressComplete;
                    _this.totalOverDueComplete += item.overDueComplete;
                    _this.totalNotComplete += item.progressNotComplete;
                    _this.totalNotComplete += item.overDueNotComplete;
                    _this.totalComplete += item.overDueComplete;
                    _this.totalComplete += item.progressComplete;
                    _this.totalDelete += item.totalDestroy;
                });
                _this.handleValuePieChart();
            });
            this.infoSearch = new _model_task_info_search_viewmodel__WEBPACK_IMPORTED_MODULE_12__["TaskInfoSearchViewModel"]();
        }
    };
    TaskReportComponent.prototype.selectDate = function (value) {
        this.infoSearch.startDate = moment__WEBPACK_IMPORTED_MODULE_8__(value.startDate).format('YYYY-MM-DD').toString();
        this.infoSearch.endDate = moment__WEBPACK_IMPORTED_MODULE_8__(value.endDate).format('YYYY-MM-DD').toString();
    };
    TaskReportComponent.prototype.handleValuePieChart = function () {
        this.progressData.push({ 'name': 'progressNotComplete', 'value': (this.totalProgressNotComplete / this.totalTaskActive) * 100 });
        this.progressData.push({ 'name': 'overDueNotComplete', 'value': (this.totalOverDueNotComplete / this.totalTaskActive) * 100 });
        this.progressData.push({ 'name': 'progressComplete', 'value': (this.totalProgressComplete / this.totalTaskActive) * 100 });
        this.progressData.push({ 'name': 'overDueComplete', 'value': (this.totalOverDueComplete / this.totalTaskActive) * 100 });
        if (this.totalTaskActive === 0) {
            this.colorScheme.domain = ['white'];
            this.progressData.push({ 'name': 'dontHaveValue', 'value': 1 });
        }
        else {
            this.colorScheme.domain = ['forestgreen', 'red', 'dodgerblue', '#f39c12'];
        }
        this.isDataLoaded = true;
    };
    //
    TaskReportComponent.prototype.showListTarget = function (officeId, userId, typeStatus) {
        var data;
        if (this.typeReportSelected === this.taskReportType.statisticsByUnits) {
            data = lodash__WEBPACK_IMPORTED_MODULE_9__["find"](this.listTaskReport, function (item) {
                return item.officeId === officeId;
            });
        }
        else {
            data = lodash__WEBPACK_IMPORTED_MODULE_9__["find"](this.listTaskReport, function (item) {
                return item.userId === userId;
            });
        }
        var listTargetDiaLog = this.diaLog.open(_task_report_list_task_report_list_component__WEBPACK_IMPORTED_MODULE_14__["TaskReportListComponent"], {
            id: "task-list-group-" + typeStatus,
            data: { listTask: data.task, type: typeStatus },
            disableClose: true,
            width: '60%',
        });
    };
    TaskReportComponent.prototype.getListUser = function () {
        var _this = this;
        this.userPickerService.search('', null, 1, 100)
            .subscribe(function (result) {
            var list = [];
            lodash__WEBPACK_IMPORTED_MODULE_9__["each"](result.items, function (item) {
                var user = { id: item.id, name: item.fullName };
                list.push(user);
            });
            _this.listUserPicker = list;
        });
    };
    TaskReportComponent.prototype.selectedUser = function () {
        var _this = this;
        this.listTargetPicker = [];
        this.isShowTarget = false;
        this.targetService.searchPersonalTarget(this.infoSearch.userId, null, null, null, null, null, [1]).subscribe(function (result) {
            console.log(result);
            lodash__WEBPACK_IMPORTED_MODULE_9__["each"](result.items, function (item) {
                var target = { id: item.data.id, name: item.data.name };
                _this.listTargetPicker.push(target);
            });
            console.log(_this.listTargetPicker);
            _this.isShowTarget = true;
        });
    };
    TaskReportComponent.prototype.selectedOffice = function (value) {
        var _this = this;
        this.listTargetPicker = [];
        this.infoSearch.officeId = value.id;
        this.targetService.searchOfficeTarget(this.infoSearch.officeId, null, null, null, null, null).subscribe(function (result) {
            lodash__WEBPACK_IMPORTED_MODULE_9__["each"](result.data, function (item) {
                var target = { id: item.data.id, name: item.data.name };
                _this.listTargetPicker.push(target);
                _this.isShowTarget = true;
            });
        });
    };
    TaskReportComponent.prototype.selectedOfficeId = function (value) {
        this.infoSearch.officeId = value.id;
    };
    TaskReportComponent.prototype.selectTarget = function () {
        this.isShowErrorTarget = false;
    };
    TaskReportComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-report',
            template: __webpack_require__(/*! ./task-report.component.html */ "./src/app/modules/task/task-management/task-report/task-report.component.html"),
            styles: [__webpack_require__(/*! ./task-report.component.scss */ "./src/app/modules/task/task-management/task-report/task-report.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"], _shareds_components_nh_user_picker_nh_user_picker_service__WEBPACK_IMPORTED_MODULE_3__["NhUserPickerService"],
            _hr_organization_office_services_office_service__WEBPACK_IMPORTED_MODULE_4__["OfficeService"], _services_task_service__WEBPACK_IMPORTED_MODULE_13__["TaskService"], _target_targets_services_target_service__WEBPACK_IMPORTED_MODULE_15__["TargetService"]])
    ], TaskReportComponent);
    return TaskReportComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_11__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/task/task-management/task-role-group/models/role-group.viewmodel.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-role-group/models/role-group.viewmodel.ts ***!
  \*********************************************************************************************/
/*! exports provided: RoleGroupViewModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleGroupViewModel", function() { return RoleGroupViewModel; });
var RoleGroupViewModel = /** @class */ (function () {
    function RoleGroupViewModel(id, name, description, concurrencyStamp, creatorId, createFullName, role, read, notification, disputation, report, write, defaultc, isRemove) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.concurrencyStamp = concurrencyStamp;
        this.creatorId = creatorId;
        this.creatorFullName = createFullName;
        this.role = role;
        this.read = true;
        this.notification = notification;
        this.disputation = disputation;
        this.report = report;
        this.write = write;
        this.default = true;
        this.isRemove = isRemove;
    }
    return RoleGroupViewModel;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-role-group/models/task-role-group.model.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-role-group/models/task-role-group.model.ts ***!
  \**********************************************************************************************/
/*! exports provided: TaskRoleGroupModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskRoleGroupModel", function() { return TaskRoleGroupModel; });
var TaskRoleGroupModel = /** @class */ (function () {
    function TaskRoleGroupModel() {
    }
    return TaskRoleGroupModel;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-role-group/models/task.model.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-role-group/models/task.model.ts ***!
  \***********************************************************************************/
/*! exports provided: Task */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Task", function() { return Task; });
/* harmony import */ var _consts_tasks_const__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../consts/tasks.const */ "./src/app/modules/task/task-management/consts/tasks.const.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);


var Task = /** @class */ (function () {
    function Task() {
        this.status = _consts_tasks_const__WEBPACK_IMPORTED_MODULE_0__["TaskStatus"].notStartYet;
        this.estimateStartDate = moment__WEBPACK_IMPORTED_MODULE_1__().format('YYYY/MM/DD HH:mm');
        this.estimateEndDate = moment__WEBPACK_IMPORTED_MODULE_1__().format('YYYY/MM/DD HH:mm');
        this.targetType = 1;
    }
    return Task;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-role-group/permission-task-role-group.const.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-role-group/permission-task-role-group.const.ts ***!
  \**************************************************************************************************/
/*! exports provided: PermissionTaskRoleGroup */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PermissionTaskRoleGroup", function() { return PermissionTaskRoleGroup; });
var PermissionTaskRoleGroup = {
    full: 63,
    read: 1,
    notification: 2,
    disputation: 4,
    report: 8,
    write: 16,
    default: 32,
};


/***/ }),

/***/ "./src/app/modules/task/task-management/task-role-group/role-group-form/role-group-form.component.html":
/*!*************************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-role-group/role-group-form/role-group-form.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #roleGroupFormModal size=\"md\" (hidden)=\"onModalHidden()\">\r\n    <nh-modal-header>\r\n        {{ isUpdate ? 'Cập nhật nhóm vai trò' : 'Tạo nhóm vai trò'}}\r\n    </nh-modal-header>\r\n    <form class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content>\r\n            <div class=\"form-body\">\r\n                <div class=\"form-group\" [class.has-error]=\"formErrors?.name\">\r\n                    <label class=\"col-sm-4 ghm-label\" ghmLabel=\"Vai Trò\" i18n-ghmLabel=\"roleName\" [required]=\"true\"></label>\r\n                    <div class=\"col-md-8\">\r\n                        <ghm-input [elementId]=\"'name'\"\r\n                                   [placeholder]=\"'Nhập tên vài trò'\"\r\n                                   [isDisabled]=\"id === 'NNV' || id === 'NGV' || id === 'CVN'\"\r\n                                   i18n-placeholder=\"@@enterTaskRoleGroupHolder\"\r\n                                   formControlName=\"name\"></ghm-input>\r\n                        <span class=\"help-block\" *ngIf=\"formErrors.name\">\r\n                        {formErrors?.name,\r\n                            select,\r\n                            required {Vui lòng nhập tên vai trò}\r\n                            maxlength {Tên vai trò không được vượt quá 256 ký tự }\r\n                            }\r\n                    </span>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <label class=\"col-sm-4 ghm-label\" ghmLabel=\"Quyền\" i18n=\"role\"></label>\r\n                    <div class=\"col-md-8\">\r\n                        <div class=\"role-row\">\r\n                            <mat-checkbox\r\n                                [(checked)]=\"listRoleGroup.read\"\r\n                                [disabled]=\"true\"\r\n                                (change)=\"onchangeRole(permissionTaskConst.read)\"\r\n                                color=\"primary\">\r\n                                <span i18n=\"roleRead\">Đọc</span>\r\n                            </mat-checkbox>\r\n                        </div>\r\n                        <div class=\"role-row\">\r\n                            <mat-checkbox\r\n                                [(checked)]=\"listRoleGroup.notification\"\r\n                                (change)=\"onchangeRole(permissionTaskConst.notification)\"\r\n                                color=\"primary\">\r\n                                <span i18n=\"roleNotification\">Thông báo</span>\r\n                            </mat-checkbox>\r\n                        </div>\r\n                        <div class=\"role-row\">\r\n                            <mat-checkbox\r\n                                [(checked)]=\"listRoleGroup.disputation\"\r\n                                (change)=\"onchangeRole(permissionTaskConst.disputation)\"\r\n                                color=\"primary\">\r\n                                <span i18n=\"roleDisputation\">Thảo luận</span>\r\n                            </mat-checkbox>\r\n                        </div>\r\n                        <div class=\"role-row\">\r\n                            <mat-checkbox\r\n                                [(checked)]=\"listRoleGroup.report\"\r\n                                (change)=\"onchangeRole(permissionTaskConst.report)\"\r\n                                color=\"primary\">\r\n                                <span i18n=\"roleReport\">Báo cáo</span>\r\n                            </mat-checkbox>\r\n                        </div>\r\n                        <div class=\"role-row\">\r\n                            <mat-checkbox\r\n                                [(checked)]=\"listRoleGroup.write\"\r\n                                (change)=\"onchangeRole(permissionTaskConst.write)\"\r\n                                color=\"primary\">\r\n                                <span i18n=\"@@write\"> Ghi</span>\r\n                            </mat-checkbox>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <label class=\"col-sm-4 ghm-label\" ghmLabel=\"Mô tả\" i18n-ghmLabel=\"description\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <textarea class=\"form-control rounded-0\" [class.has-error]=\"formErrors.description\"\r\n                                  formControlName=\"description\"\r\n                                  id=\"exampleFormControlTextarea2\" rows=\"3\"></textarea>\r\n                        <span class=\"text-danger\" *ngIf=\"formErrors.description\">\r\n                        {formErrors?.description,\r\n                            select,\r\n                            required {Vui lòng nhập mô tả.}\r\n                            maxlength {Mô tả không được vượt quá 4000 ký tự }\r\n                            }\r\n                    </span>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <label class=\"col-sm-4 ghm-label\" ghmLabel=\"Mặc định\" i18n=\"roleDefault\"></label>\r\n                    <div class=\"col-sm-8 role-row-default\">\r\n                        <mat-checkbox\r\n                            [(checked)]=\"listRoleGroup.default\"\r\n                            (change)=\"onchangeRole(permissionTaskConst.default)\"\r\n                            color=\"primary\">\r\n                            <span\r\n                                i18n=\"@@default\">{listRoleGroup.default , select, 0 {Không mặc định} 1 {Mặc định}}</span>\r\n                        </mat-checkbox>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <div class=\"center\">\r\n                <ghm-button classes=\"btn blue cm-mgr-5\" [loading]=\"isSaving\" i18n=\"@@update\">\r\n                    Cập nhật\r\n                </ghm-button>\r\n                <ghm-button type=\"button\" classes=\"btn btn-light\" nh-dismiss=\"true\">\r\n                    Đóng\r\n                </ghm-button>\r\n            </div>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-role-group/role-group-form/role-group-form.component.ts":
/*!***********************************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-role-group/role-group-form/role-group-form.component.ts ***!
  \***********************************************************************************************************/
/*! exports provided: RoleGroupFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleGroupFormComponent", function() { return RoleGroupFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../permission-task-role-group.const */ "./src/app/modules/task/task-management/task-role-group/permission-task-role-group.const.ts");
/* harmony import */ var _models_role_group_viewmodel__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../models/role-group.viewmodel */ "./src/app/modules/task/task-management/task-role-group/models/role-group.viewmodel.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _task_role_group_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../task-role-group.service */ "./src/app/modules/task/task-management/task-role-group/task-role-group.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _models_task_role_group_model__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../models/task-role-group.model */ "./src/app/modules/task/task-management/task-role-group/models/task-role-group.model.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");












var RoleGroupFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](RoleGroupFormComponent, _super);
    function RoleGroupFormComponent(fb, utilService, taskRoleGroupService, toastrService) {
        var _this = _super.call(this) || this;
        _this.fb = fb;
        _this.utilService = utilService;
        _this.taskRoleGroupService = taskRoleGroupService;
        _this.toastrService = toastrService;
        _this.permissionTaskConst = _permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_4__["PermissionTaskRoleGroup"];
        _this.listRoleGroup = new _models_role_group_viewmodel__WEBPACK_IMPORTED_MODULE_5__["RoleGroupViewModel"]();
        _this.taskRoleGroupModel = new _models_task_role_group_model__WEBPACK_IMPORTED_MODULE_10__["TaskRoleGroupModel"]();
        return _this;
    }
    RoleGroupFormComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    RoleGroupFormComponent.prototype.add = function () {
        this.isUpdate = false;
        this.utilService.focusElement('name');
        this.roleGroupFormModal.open();
    };
    RoleGroupFormComponent.prototype.update = function (item) {
        this.listRoleGroup = item;
        if (this.id !== 'NNV' && this.id !== 'NGV') {
            this.utilService.focusElement('name');
        }
        this.id = item.id;
        this.model.patchValue(item);
        this.isUpdate = true;
        this.roleGroupFormModal.open();
    };
    RoleGroupFormComponent.prototype.onModalHidden = function () {
        this.model.reset(new _models_role_group_viewmodel__WEBPACK_IMPORTED_MODULE_5__["RoleGroupViewModel"]());
        this.listRoleGroup = new _models_role_group_viewmodel__WEBPACK_IMPORTED_MODULE_5__["RoleGroupViewModel"]();
        this.isUpdate = false;
        if (this.isModified) {
            this.saveSuccessful.emit();
            this.clearFormError(this.formErrors);
        }
    };
    RoleGroupFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.validateModel();
        if (isValid) {
            this.taskRoleGroupModel = this.model.value;
            this.taskRoleGroupModel.role = this.calculatePermissions(this.listRoleGroup);
            if (this.isUpdate) {
                this.isSaving = true;
                this.taskRoleGroupService.update(this.id, this.taskRoleGroupModel)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.toastrService.success(result.message);
                    _this.isModified = true;
                    _this.roleGroupFormModal.dismiss();
                });
            }
            else {
                this.isSaving = true;
                this.taskRoleGroupService.insert(this.taskRoleGroupModel)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.toastrService.success(result.message);
                    _this.isModified = true;
                    _this.roleGroupFormModal.dismiss();
                });
            }
        }
    };
    RoleGroupFormComponent.prototype.onchangeRole = function (permission) {
        switch (permission) {
            case this.permissionTaskConst.read:
                this.listRoleGroup.read = !this.listRoleGroup.read;
                break;
            case this.permissionTaskConst.write:
                this.listRoleGroup.write = !this.listRoleGroup.write;
                break;
            case this.permissionTaskConst.report:
                this.listRoleGroup.report = !this.listRoleGroup.report;
                break;
            case this.permissionTaskConst.notification:
                this.listRoleGroup.notification = !this.listRoleGroup.notification;
                break;
            case this.permissionTaskConst.default:
                this.listRoleGroup.default = !this.listRoleGroup.default;
                break;
            case this.permissionTaskConst.disputation:
                this.listRoleGroup.disputation = !this.listRoleGroup.disputation;
                break;
        }
    };
    RoleGroupFormComponent.prototype.calculatePermissions = function (role) {
        var permissions = 0;
        if (role.read) {
            permissions += _permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_4__["PermissionTaskRoleGroup"].read;
        }
        if (role.write) {
            permissions += _permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_4__["PermissionTaskRoleGroup"].write;
        }
        if (role.disputation) {
            permissions += _permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_4__["PermissionTaskRoleGroup"].disputation;
        }
        if (role.default) {
            permissions += _permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_4__["PermissionTaskRoleGroup"].default;
        }
        if (role.notification) {
            permissions += _permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_4__["PermissionTaskRoleGroup"].notification;
        }
        if (role.report) {
            permissions += _permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_4__["PermissionTaskRoleGroup"].report;
        }
        return permissions;
    };
    RoleGroupFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.renderFormError(['name', 'description']);
        this.validationMessages = this.renderFormErrorMessage([
            { 'name': ['required', 'maxLength'] },
            { 'description': ['maxLength'] }
        ]);
        this.model = this.fb.group({
            name: [this.listRoleGroup.name, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required
                ]],
            description: [this.listRoleGroup.description, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].maxLength(4000)
                ]],
            concurrencyStamp: [this.listRoleGroup.concurrencyStamp]
        });
        this.subscribers.modelValueChanges = this.model.valueChanges.subscribe(function () { return _this.validateModel(false); });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('roleGroupFormModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__["NhModalComponent"])
    ], RoleGroupFormComponent.prototype, "roleGroupFormModal", void 0);
    RoleGroupFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-role-group-form',
            template: __webpack_require__(/*! ./role-group-form.component.html */ "./src/app/modules/task/task-management/task-role-group/role-group-form/role-group-form.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_11__["UtilService"],
            _task_role_group_service__WEBPACK_IMPORTED_MODULE_7__["TaskRoleGroupService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_9__["ToastrService"]])
    ], RoleGroupFormComponent);
    return RoleGroupFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/task/task-management/task-role-group/task-role-group.component.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-role-group/task-role-group.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@roleGroupTitle\">Vai trò công việc</span>\r\n</h1>\r\n<div class=\" cm-mgb-10\">\r\n    <button class=\"btn blue\" type=\"button\" (click)=\"add()\" i18n=\"@@add\" *ngIf=\"permission.add\">\r\n        Thêm mới\r\n    </button>\r\n</div>\r\n<div class=\"table-responsive\">\r\n    <table class=\"table table-striped table-bordered table-hover\">\r\n        <thead>\r\n        <tr>\r\n            <th class=\"w50 center middle\">STT</th>\r\n            <th class=\"middle\" i18n=\"@@roleName\">Vai trò</th>\r\n            <th class=\"w120 center middle\" i18n=\"@@roleRead\">Đọc\r\n            </th>\r\n            <th class=\"w120 center middle\" i18n=\"@@roleNotification\">Thông báo\r\n            </th>\r\n            <th class=\"w120 center middle\" i18n=\"@@roleDisputation\">Thảo luận\r\n            </th>\r\n            <th class=\"w120 center middle\" i18n=\"@@roleReport\">Báo cáo</th>\r\n            <th class=\"w120 center middle\" i18n=\"@@roleWrite\">Ghi</th>\r\n            <th class=\"w120 center middle\" i18n=\"@@roleDefault\">Mặc định</th>\r\n            <th class=\"w120 center middle\" i18n=\"@@roleOptions\" *ngIf=\"permission.edit || permission.delete\">Chức năng</th>\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n        <tr *ngFor=\"let item of listItems; let i = index\">\r\n            <td class=\"center middle\">{{ (currentPage - 1) * pageSize + i + 1 }}</td>\r\n            <td class=\"middle\">{{ item.name}}</td>\r\n            <td class=\"center middle\">\r\n                <mat-checkbox\r\n                    [(checked)]=\"item.read\"\r\n                    [disabled]=\"true\"\r\n                    color=\"primary\"></mat-checkbox>\r\n            </td>\r\n            <td class=\"center middle\">\r\n                <mat-checkbox\r\n                    [disabled]=\"true\"\r\n                    [(checked)]=\"item.notification\"\r\n                    color=\"primary\"></mat-checkbox>\r\n            </td>\r\n            <td class=\"center middle\">\r\n                <mat-checkbox\r\n                    [disabled]=\"true\"\r\n                    [(checked)]=\"item.disputation\"\r\n                    color=\"primary\"></mat-checkbox>\r\n            </td>\r\n            <td class=\"center middle\">\r\n                <mat-checkbox\r\n                    [disabled]=\"true\"\r\n                    [(checked)]=\"item.report\"\r\n                    color=\"primary\"></mat-checkbox>\r\n            </td>\r\n            <td class=\"center middle\">\r\n                <mat-checkbox\r\n                    [disabled]=\"true\"\r\n                    [(checked)]=\"item.write\"\r\n                    color=\"primary\"></mat-checkbox>\r\n            </td>\r\n            <td class=\"center middle\">\r\n                <mat-checkbox\r\n                    *ngIf=\"!item.isRemove\"\r\n                    [disabled]=\"true\"\r\n                    [(checked)]=\"item.default\"\r\n                    color=\"primary\"></mat-checkbox>\r\n                <p *ngIf=\"item.isRemove\">---</p>\r\n            </td>\r\n            <td class=\"center middle\" *ngIf=\"permission.edit || permission.delete\">\r\n                <a (click)=\"update(item)\" class=\"cm-mgr-5\" *ngIf=\"permission.edit\" data-icon=\"update-16\">\r\n                </a>\r\n                <a data-icon=\"delete-16\" (confirm)=\"delete(item.id)\" [swal]=\"deleteRole\" *ngIf=\"permission.delete && (item.id !== 'NNV' && item.id !== 'NGV' && item.id !== 'CVN')\">\r\n                </a>\r\n            </td>\r\n        </tr>\r\n        </tbody>\r\n    </table>\r\n</div>\r\n<app-role-group-form (saveSuccessful)=\"search()\"></app-role-group-form>\r\n<swal\r\n    i18n=\"@@confirmDeleteRole\"\r\n    i18n-title\r\n    i18n-text\r\n    #deleteRole\r\n    title=\"Bạn có muốn xóa vai trò này?\"\r\n    text=\"Bạn không thể khôi phục lại vai trò này sau khi xóa.\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n"

/***/ }),

/***/ "./src/app/modules/task/task-management/task-role-group/task-role-group.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-role-group/task-role-group.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: TaskRoleGroupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskRoleGroupComponent", function() { return TaskRoleGroupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _configs_role_role_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../configs/role/role.service */ "./src/app/modules/configs/role/role.service.ts");
/* harmony import */ var _task_role_group_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./task-role-group.service */ "./src/app/modules/task/task-management/task-role-group/task-role-group.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _role_group_form_role_group_form_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./role-group-form/role-group-form.component */ "./src/app/modules/task/task-management/task-role-group/role-group-form/role-group-form.component.ts");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_8__);









var TaskRoleGroupComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TaskRoleGroupComponent, _super);
    function TaskRoleGroupComponent(taskRoleGroupService, route, spinnerService) {
        var _this = _super.call(this) || this;
        _this.taskRoleGroupService = taskRoleGroupService;
        _this.route = route;
        _this.spinnerService = spinnerService;
        return _this;
    }
    TaskRoleGroupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.appService.setupPage(this.pageId.TASK, this.pageId.TASK_ROLE_GROUP, 'Danh sách vai trò công việc', 'Quản lý công việc');
        this.subscribers.data = this.route.data.subscribe(function (result) {
            _this.listItems = result.data;
        });
    };
    TaskRoleGroupComponent.prototype.search = function () {
        var _this = this;
        this.taskRoleGroupService.search('', 1, 20).subscribe(function (result) {
            _this.listItems = result;
        });
    };
    TaskRoleGroupComponent.prototype.add = function () {
        this.roleGroupFormComponent.add();
    };
    TaskRoleGroupComponent.prototype.update = function (item) {
        this.roleGroupFormComponent.update(item);
    };
    TaskRoleGroupComponent.prototype.delete = function (id) {
        var _this = this;
        this.spinnerService.show('Đang xóa vui lòng đợi ...');
        this.taskRoleGroupService.delete(id).subscribe(function () {
            lodash__WEBPACK_IMPORTED_MODULE_8__["remove"](_this.listItems, function (item) {
                return item.id === id;
            });
        });
    };
    TaskRoleGroupComponent.prototype.onchangeRole = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_role_group_form_role_group_form_component__WEBPACK_IMPORTED_MODULE_6__["RoleGroupFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _role_group_form_role_group_form_component__WEBPACK_IMPORTED_MODULE_6__["RoleGroupFormComponent"])
    ], TaskRoleGroupComponent.prototype, "roleGroupFormComponent", void 0);
    TaskRoleGroupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-role',
            template: __webpack_require__(/*! ./task-role-group.component.html */ "./src/app/modules/task/task-management/task-role-group/task-role-group.component.html"),
            providers: [_configs_role_role_service__WEBPACK_IMPORTED_MODULE_3__["RoleService"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_task_role_group_service__WEBPACK_IMPORTED_MODULE_4__["TaskRoleGroupService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_7__["SpinnerService"]])
    ], TaskRoleGroupComponent);
    return TaskRoleGroupComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/task/task-management/task-role-group/task-role-group.service.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-role-group/task-role-group.service.ts ***!
  \*****************************************************************************************/
/*! exports provided: TaskRoleGroupService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskRoleGroupService", function() { return TaskRoleGroupService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./permission-task-role-group.const */ "./src/app/modules/task/task-management/task-role-group/permission-task-role-group.const.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../environments/environment */ "./src/environments/environment.ts");









var TaskRoleGroupService = /** @class */ (function () {
    function TaskRoleGroupService(http, appConfig, spinnerService, toastrService) {
        this.http = http;
        this.appConfig = appConfig;
        this.spinnerService = spinnerService;
        this.toastrService = toastrService;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].apiGatewayUrl + "api/v1/task/role-groups";
    }
    TaskRoleGroupService.prototype.resolve = function (route, state) {
        var queryParams = route.params;
        return this.search(queryParams.keyword, queryParams.page, queryParams.pageSize);
    };
    TaskRoleGroupService.prototype.search = function (keyword, page, pageSize) {
        var _this = this;
        return this.http.get(this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString())
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            var rolePages = [];
            result.items.forEach(function (item) {
                rolePages.push({
                    id: item.id,
                    name: item.name,
                    description: item.description,
                    concurrencyStamp: item.concurrencyStamp,
                    role: item.role,
                    read: _this.checkPermissionRoleGroup(_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_6__["PermissionTaskRoleGroup"].read, item.role),
                    notification: _this.checkPermissionRoleGroup(_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_6__["PermissionTaskRoleGroup"].notification, item.role),
                    disputation: _this.checkPermissionRoleGroup(_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_6__["PermissionTaskRoleGroup"].disputation, item.role),
                    report: _this.checkPermissionRoleGroup(_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_6__["PermissionTaskRoleGroup"].report, item.role),
                    write: _this.checkPermissionRoleGroup(_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_6__["PermissionTaskRoleGroup"].write, item.role),
                    default: _this.checkPermissionRoleGroup(_permission_task_role_group_const__WEBPACK_IMPORTED_MODULE_6__["PermissionTaskRoleGroup"].default, item.role)
                });
            });
            return rolePages;
        }));
    };
    TaskRoleGroupService.prototype.insert = function (taskRoleGroupModel) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url, taskRoleGroupModel)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    TaskRoleGroupService.prototype.update = function (id, taskRoleGroupModel) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/" + id, taskRoleGroupModel)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    TaskRoleGroupService.prototype.delete = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.delete(this.url + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastrService.success(result.message);
            return result;
        }));
    };
    TaskRoleGroupService.prototype.checkPermissionRoleGroup = function (permission, permissions) {
        return (permission & permissions) === permission;
        // if (permission & permissions) {
        //     return true;
        // } else {
        //     return false;
        // }
    };
    TaskRoleGroupService.prototype.searchForSelect = function () {
        return this.http.get(this.url + "/select");
    };
    TaskRoleGroupService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_3__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], Object, _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__["SpinnerService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"]])
    ], TaskRoleGroupService);
    return TaskRoleGroupService;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/modules/task/task-management/task-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: userRoutes, TaskRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userRoutes", function() { return userRoutes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskRoutingModule", function() { return TaskRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _task_role_group_task_role_group_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./task-role-group/task-role-group.component */ "./src/app/modules/task/task-management/task-role-group/task-role-group.component.ts");
/* harmony import */ var _task_role_group_task_role_group_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./task-role-group/task-role-group.service */ "./src/app/modules/task/task-management/task-role-group/task-role-group.service.ts");
/* harmony import */ var _task_library_task_library_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./task-library/task-library.component */ "./src/app/modules/task/task-management/task-library/task-library.component.ts");
/* harmony import */ var _task_library_services_task_library_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./task-library/services/task-library.service */ "./src/app/modules/task/task-management/task-library/services/task-library.service.ts");
/* harmony import */ var _task_list_task_list_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./task-list/task-list.component */ "./src/app/modules/task/task-management/task-list/task-list.component.ts");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./services/task.service */ "./src/app/modules/task/task-management/services/task.service.ts");
/* harmony import */ var _task_report_task_report_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./task-report/task-report.component */ "./src/app/modules/task/task-management/task-report/task-report.component.ts");










var userRoutes = [
    {
        path: 'roles',
        resolve: {
            data: _task_role_group_task_role_group_service__WEBPACK_IMPORTED_MODULE_4__["TaskRoleGroupService"]
        },
        component: _task_role_group_task_role_group_component__WEBPACK_IMPORTED_MODULE_3__["TaskRoleGroupComponent"]
    },
    {
        path: 'task-library',
        resolve: {
            data: _task_library_services_task_library_service__WEBPACK_IMPORTED_MODULE_6__["TaskLibraryService"]
        },
        component: _task_library_task_library_component__WEBPACK_IMPORTED_MODULE_5__["TaskLibraryComponent"]
    },
    {
        path: 'list',
        resolve: {
            data: _services_task_service__WEBPACK_IMPORTED_MODULE_8__["TaskService"]
        },
        component: _task_list_task_list_component__WEBPACK_IMPORTED_MODULE_7__["TaskListComponent"]
    },
    {
        path: 'group',
        component: _task_list_task_list_component__WEBPACK_IMPORTED_MODULE_7__["TaskListComponent"],
        resolve: {
            data: _services_task_service__WEBPACK_IMPORTED_MODULE_8__["TaskService"]
        },
    },
    {
        path: 'reports',
        component: _task_report_task_report_component__WEBPACK_IMPORTED_MODULE_9__["TaskReportComponent"]
    }
];
var TaskRoutingModule = /** @class */ (function () {
    function TaskRoutingModule() {
    }
    TaskRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(userRoutes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
            providers: [_task_role_group_task_role_group_service__WEBPACK_IMPORTED_MODULE_4__["TaskRoleGroupService"], _task_library_services_task_library_service__WEBPACK_IMPORTED_MODULE_6__["TaskLibraryService"], _services_task_service__WEBPACK_IMPORTED_MODULE_8__["TaskService"]]
        })
    ], TaskRoutingModule);
    return TaskRoutingModule;
}());



/***/ }),

/***/ "./src/app/modules/task/task-management/task.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/modules/task/task-management/task.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".media-task-info a.task-name {\n  color: #007bff;\n  font-weight: bold;\n  font-size: 14px; }\n\n.media-task-info ul.task-metas {\n  list-style: none;\n  padding-left: 0;\n  margin-bottom: 0; }\n\n.media-task-info ul.task-metas li {\n    display: inline-block;\n    margin-right: 5px; }\n\n.media-task-info ul.task-metas li a {\n      color: #868e96;\n      text-decoration: none; }\n\n.media-task-info ul.task-metas li a:hover {\n        color: #ccc; }\n\n.task-main-nav {\n  list-style: none;\n  margin: 0;\n  padding: 0;\n  background: #007bff;\n  text-algin: left;\n  border: 1px solid #007bff; }\n\n.task-main-nav li {\n    display: inline-block; }\n\n.task-main-nav li a {\n      color: white; }\n\n.task-main-nav li a.active {\n        background: white;\n        color: #007bff; }\n\n.task-main-nav li a:hover {\n        background: white;\n        color: #007bff; }\n\nul.task-actions {\n  padding-left: 0;\n  list-style: none;\n  text-align: center;\n  display: block;\n  width: 100%;\n  border-top: 1px solid #ddd;\n  padding-top: 10px;\n  margin-top: 20px;\n  margin-bottom: 0; }\n\nul.task-actions li {\n    display: inline-block; }\n\n.task-list-container {\n  margin: -52px -15px -15px -15px !important; }\n\n.task-list-container .task-content-container {\n    padding: 15px; }\n\n.link-text-muted {\n  color: #757575;\n  text-decoration: underline;\n  cursor: pointer;\n  font-size: 14px; }\n\n.link-text-muted:hover {\n    color: #0072bc; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3Rhc2stbWFuYWdlbWVudC9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXBwXFxtb2R1bGVzXFx0YXNrXFx0YXNrLW1hbmFnZW1lbnRcXHRhc2suY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21vZHVsZXMvdGFzay90YXNrLW1hbmFnZW1lbnQvRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFzc2V0c1xcc3R5bGVzXFxfY29uZmlnLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFFUSxjQ0dNO0VERk4saUJBQWlCO0VBQ2pCLGVBQWUsRUFBQTs7QUFKdkI7RUFRUSxnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQVZ4QjtJQWFZLHFCQUFxQjtJQUNyQixpQkFBaUIsRUFBQTs7QUFkN0I7TUFpQmdCLGNDQ0Y7TURBRSxxQkFBcUIsRUFBQTs7QUFsQnJDO1FBcUJvQixXQUFXLEVBQUE7O0FBTy9CO0VBQ0ksZ0JBQWdCO0VBQ2hCLFNBQVM7RUFDVCxVQUFVO0VBQ1YsbUJDWmE7RURhYixnQkFBZ0I7RUFDaEIseUJDZGEsRUFBQTs7QURRakI7SUFTUSxxQkFBcUIsRUFBQTs7QUFUN0I7TUFXWSxZQUFZLEVBQUE7O0FBWHhCO1FBYWdCLGlCQUFpQjtRQUNqQixjQ3RCQyxFQUFBOztBRFFqQjtRQWtCZ0IsaUJBQWlCO1FBQ2pCLGNDM0JDLEVBQUE7O0FEaUNqQjtFQUNJLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxXQUFXO0VBQ1gsMEJDekRjO0VEMERkLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsZ0JBQWdCLEVBQUE7O0FBVHBCO0lBWVEscUJBQXFCLEVBQUE7O0FBSzdCO0VBQ0ksMENBQTBDLEVBQUE7O0FBRDlDO0lBSVEsYUFBYSxFQUFBOztBQUlyQjtFQUNJLGNBQWM7RUFDZCwwQkFBMEI7RUFDMUIsZUFBZTtFQUNmLGVBQWUsRUFBQTs7QUFKbkI7SUFPUSxjQUFjLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9tb2R1bGVzL3Rhc2svdGFzay1tYW5hZ2VtZW50L3Rhc2suY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0ICcuLi8uLi8uLi8uLi9hc3NldHMvc3R5bGVzL2NvbmZpZyc7XHJcblxyXG4ubWVkaWEtdGFzay1pbmZvIHtcclxuICAgIGEudGFzay1uYW1lIHtcclxuICAgICAgICBjb2xvcjogJGJsdWU7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgfVxyXG5cclxuICAgIHVsLnRhc2stbWV0YXMge1xyXG4gICAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAwO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcblxyXG4gICAgICAgIGxpIHtcclxuICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcclxuXHJcbiAgICAgICAgICAgIGEge1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICRncmF5O1xyXG4gICAgICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG5cclxuICAgICAgICAgICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjY2NjO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4udGFzay1tYWluLW5hdiB7XHJcbiAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGJhY2tncm91bmQ6ICRwcmltYXJ5O1xyXG4gICAgdGV4dC1hbGdpbjogbGVmdDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICRwcmltYXJ5O1xyXG5cclxuICAgIGxpIHtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgYSB7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgJi5hY3RpdmUge1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogJHByaW1hcnk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogJHByaW1hcnk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbnVsLnRhc2stYWN0aW9ucyB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAkYm9yZGVyQ29sb3I7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG5cclxuICAgIGxpIHtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8vIEJFR0lOOiBUYXNrIGxpc3RcclxuLnRhc2stbGlzdC1jb250YWluZXIge1xyXG4gICAgbWFyZ2luOiAtNTJweCAtMTVweCAtMTVweCAtMTVweCAhaW1wb3J0YW50O1xyXG5cclxuICAgIC50YXNrLWNvbnRlbnQtY29udGFpbmVyIHtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4ubGluay10ZXh0LW11dGVkIHtcclxuICAgIGNvbG9yOiAjNzU3NTc1O1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcblxyXG4gICAgJjpob3ZlciB7XHJcbiAgICAgICAgY29sb3I6ICMwMDcyYmM7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8vIEVORDogVGFzayBsaXN0XHJcbiIsIiRkZWZhdWx0LWNvbG9yOiAjMjIyO1xyXG4kZm9udC1mYW1pbHk6IFwiQXJpYWxcIiwgdGFob21hLCBIZWx2ZXRpY2EgTmV1ZTtcclxuJGNvbG9yLWJsdWU6ICMzNTk4ZGM7XHJcbiRtYWluLWNvbG9yOiAjMDA3NDU1O1xyXG4kYm9yZGVyQ29sb3I6ICNkZGQ7XHJcbiRzZWNvbmQtY29sb3I6ICNiMDFhMWY7XHJcbiR0YWJsZS1iYWNrZ3JvdW5kLWNvbG9yOiAjMDA5Njg4O1xyXG4kYmx1ZTogIzAwN2JmZjtcclxuJGRhcmstYmx1ZTogIzAwNzJCQztcclxuJGJyaWdodC1ibHVlOiAjZGZmMGZkO1xyXG4kaW5kaWdvOiAjNjYxMGYyO1xyXG4kcHVycGxlOiAjNmY0MmMxO1xyXG4kcGluazogI2U4M2U4YztcclxuJHJlZDogI2RjMzU0NTtcclxuJG9yYW5nZTogI2ZkN2UxNDtcclxuJHllbGxvdzogI2ZmYzEwNztcclxuJGdyZWVuOiAjMjhhNzQ1O1xyXG4kdGVhbDogIzIwYzk5NztcclxuJGN5YW46ICMxN2EyYjg7XHJcbiR3aGl0ZTogI2ZmZjtcclxuJGdyYXk6ICM4NjhlOTY7XHJcbiRncmF5LWRhcms6ICMzNDNhNDA7XHJcbiRwcmltYXJ5OiAjMDA3YmZmO1xyXG4kc2Vjb25kYXJ5OiAjNmM3NTdkO1xyXG4kc3VjY2VzczogIzI4YTc0NTtcclxuJGluZm86ICMxN2EyYjg7XHJcbiR3YXJuaW5nOiAjZmZjMTA3O1xyXG4kZGFuZ2VyOiAjZGMzNTQ1O1xyXG4kbGlnaHQ6ICNmOGY5ZmE7XHJcbiRkYXJrOiAjMzQzYTQwO1xyXG4kbGFiZWwtY29sb3I6ICM2NjY7XHJcbiRiYWNrZ3JvdW5kLWNvbG9yOiAjRUNGMEYxO1xyXG4kYm9yZGVyQWN0aXZlQ29sb3I6ICM4MGJkZmY7XHJcbiRib3JkZXJSYWRpdXM6IDA7XHJcbiRkYXJrQmx1ZTogIzQ1QTJEMjtcclxuJGxpZ2h0R3JlZW46ICMyN2FlNjA7XHJcbiRsaWdodC1ibHVlOiAjZjVmN2Y3O1xyXG4kYnJpZ2h0R3JheTogIzc1NzU3NTtcclxuJG1heC13aWR0aC1tb2JpbGU6IDc2OHB4O1xyXG4kbWF4LXdpZHRoLXRhYmxldDogOTkycHg7XHJcbiRtYXgtd2lkdGgtZGVza3RvcDogMTI4MHB4O1xyXG5cclxuLy8gQkVHSU46IE1hcmdpblxyXG5AbWl4aW4gbmgtbWcoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW46ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWd0KCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLXRvcDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ2IoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1nbCgkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1ncigkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1yaWdodDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogTWFyZ2luXHJcblxyXG4vLyBCRUdJTjogUGFkZGluZ1xyXG5AbWl4aW4gbmgtcGQoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkdCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctdG9wOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkYigkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctYm90dG9tOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkbCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctbGVmdDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZHIoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBQYWRkaW5nXHJcblxyXG4vLyBCRUdJTjogV2lkdGhcclxuQG1peGluIG5oLXdpZHRoKCR3aWR0aCkge1xyXG4gICAgd2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG4gICAgbWluLXdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxuICAgIG1heC13aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogV2lkdGhcclxuXHJcbi8vIEJFR0lOOiBJY29uIFNpemVcclxuQG1peGluIG5oLXNpemUtaWNvbigkc2l6ZSkge1xyXG4gICAgd2lkdGg6ICRzaXplO1xyXG4gICAgaGVpZ2h0OiAkc2l6ZTtcclxuICAgIGZvbnQtc2l6ZTogJHNpemU7XHJcbn1cclxuXHJcbi8vIEVORDogSWNvbiBTaXplXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/modules/task/task-management/task.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/modules/task/task-management/task.module.ts ***!
  \*************************************************************/
/*! exports provided: TaskModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskModule", function() { return TaskModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _task_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./task-routing.module */ "./src/app/modules/task/task-management/task-routing.module.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _task_role_group_task_role_group_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./task-role-group/task-role-group.component */ "./src/app/modules/task/task-management/task-role-group/task-role-group.component.ts");
/* harmony import */ var _shareds_layouts_layout_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../shareds/layouts/layout.module */ "./src/app/shareds/layouts/layout.module.ts");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../shareds/components/nh-modal/nh-modal.module */ "./src/app/shareds/components/nh-modal/nh-modal.module.ts");
/* harmony import */ var _task_role_group_role_group_form_role_group_form_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./task-role-group/role-group-form/role-group-form.component */ "./src/app/modules/task/task-management/task-role-group/role-group-form/role-group-form.component.ts");
/* harmony import */ var _task_role_group_task_role_group_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./task-role-group/task-role-group.service */ "./src/app/modules/task/task-management/task-role-group/task-role-group.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @toverux/ngx-sweetalert2 */ "./node_modules/@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
/* harmony import */ var _task_library_task_library_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./task-library/task-library.component */ "./src/app/modules/task/task-management/task-library/task-library.component.ts");
/* harmony import */ var primeng_treetable__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! primeng/treetable */ "./node_modules/primeng/treetable.js");
/* harmony import */ var primeng_treetable__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(primeng_treetable__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _task_library_services_task_library_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./task-library/services/task-library.service */ "./src/app/modules/task/task-management/task-library/services/task-library.service.ts");
/* harmony import */ var _task_library_task_group_library_form_task_group_library_form_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./task-library/task-group-library-form/task-group-library-form.component */ "./src/app/modules/task/task-management/task-library/task-group-library-form/task-group-library-form.component.ts");
/* harmony import */ var _shareds_components_ghm_input_ghm_input_module__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../../shareds/components/ghm-input/ghm-input.module */ "./src/app/shareds/components/ghm-input/ghm-input.module.ts");
/* harmony import */ var _task_library_task_library_form_task_library_form_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./task-library/task-library-form/task-library-form.component */ "./src/app/modules/task/task-management/task-library/task-library-form/task-library-form.component.ts");
/* harmony import */ var _task_library_task_check_list_form_task_check_list_form_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./task-library/task-check-list-form/task-check-list-form.component */ "./src/app/modules/task/task-management/task-library/task-check-list-form/task-check-list-form.component.ts");
/* harmony import */ var _task_library_services_task_check_list_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./task-library/services/task-check-list.service */ "./src/app/modules/task/task-management/task-library/services/task-check-list.service.ts");
/* harmony import */ var _shareds_components_nh_select_nh_select_module__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../../../shareds/components/nh-select/nh-select.module */ "./src/app/shareds/components/nh-select/nh-select.module.ts");
/* harmony import */ var _shareds_components_ghm_paging_ghm_paging_module__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../../../shareds/components/ghm-paging/ghm-paging.module */ "./src/app/shareds/components/ghm-paging/ghm-paging.module.ts");
/* harmony import */ var _task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./task-detail/task-detail.component */ "./src/app/modules/task/task-management/task-detail/task-detail.component.ts");
/* harmony import */ var _task_list_task_list_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./task-list/task-list.component */ "./src/app/modules/task/task-management/task-list/task-list.component.ts");
/* harmony import */ var _task_form_task_form_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./task-form/task-form.component */ "./src/app/modules/task/task-management/task-form/task-form.component.ts");
/* harmony import */ var _shared_components_task_user_suggestion_task_user_suggestion_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./shared-components/task-user-suggestion/task-user-suggestion.component */ "./src/app/modules/task/task-management/shared-components/task-user-suggestion/task-user-suggestion.component.ts");
/* harmony import */ var _task_list_task_list_item_task_list_item_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./task-list/task-list-item/task-list-item.component */ "./src/app/modules/task/task-management/task-list/task-list-item/task-list-item.component.ts");
/* harmony import */ var _task_list_task_list_group_task_list_group_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./task-list/task-list-group/task-list-group.component */ "./src/app/modules/task/task-management/task-list/task-list-group/task-list-group.component.ts");
/* harmony import */ var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @swimlane/ngx-charts */ "./node_modules/@swimlane/ngx-charts/release/esm.js");
/* harmony import */ var _shareds_components_ghm_select_ghm_select_module__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ../../../shareds/components/ghm-select/ghm-select.module */ "./src/app/shareds/components/ghm-select/ghm-select.module.ts");
/* harmony import */ var _shareds_components_ghm_user_suggestion_ghm_user_suggestion_module__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ../../../shareds/components/ghm-user-suggestion/ghm-user-suggestion.module */ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.module.ts");
/* harmony import */ var _shareds_components_ghm_comment_ghm_comment_module__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ../../../shareds/components/ghm-comment/ghm-comment.module */ "./src/app/shareds/components/ghm-comment/ghm-comment.module.ts");
/* harmony import */ var _shareds_components_ghm_attachments_ghm_attachments_module__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ../../../shareds/components/ghm-attachments/ghm-attachments.module */ "./src/app/shareds/components/ghm-attachments/ghm-attachments.module.ts");
/* harmony import */ var _shareds_components_ghm_dialog_ghm_dialog_module__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ../../../shareds/components/ghm-dialog/ghm-dialog.module */ "./src/app/shareds/components/ghm-dialog/ghm-dialog.module.ts");
/* harmony import */ var _shareds_components_nh_datetime_picker_nh_date_module__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ../../../shareds/components/nh-datetime-picker/nh-date.module */ "./src/app/shareds/components/nh-datetime-picker/nh-date.module.ts");
/* harmony import */ var _shareds_pipe_datetime_format_datetime_format_module__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ../../../shareds/pipe/datetime-format/datetime-format.module */ "./src/app/shareds/pipe/datetime-format/datetime-format.module.ts");
/* harmony import */ var _shareds_components_ghm_select_user_picker_ghm_select_user_picker_module__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ../../../shareds/components/ghm-select-user-picker/ghm-select-user-picker.module */ "./src/app/shareds/components/ghm-select-user-picker/ghm-select-user-picker.module.ts");
/* harmony import */ var _shareds_components_ghm_checklist_ghm_checklist_module__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ../../../shareds/components/ghm-checklist/ghm-checklist.module */ "./src/app/shareds/components/ghm-checklist/ghm-checklist.module.ts");
/* harmony import */ var _shareds_components_ghm_setting_notification_ghm_setting_notification_module__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ../../../shareds/components/ghm-setting-notification/ghm-setting-notification.module */ "./src/app/shareds/components/ghm-setting-notification/ghm-setting-notification.module.ts");
/* harmony import */ var _log_log_module__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ../log/log.module */ "./src/app/modules/task/log/log.module.ts");
/* harmony import */ var _shareds_components_nh_user_picker_nh_user_picker_module__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ../../../shareds/components/nh-user-picker/nh-user-picker.module */ "./src/app/shareds/components/nh-user-picker/nh-user-picker.module.ts");
/* harmony import */ var _shareds_components_ghm_suggestion_user_ghm_suggestion_user_module__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ../../../shareds/components/ghm-suggestion-user/ghm-suggestion-user.module */ "./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.module.ts");
/* harmony import */ var _shareds_components_ghm_select_datetime_ghm_select_datetime_module__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ../../../shareds/components/ghm-select-datetime/ghm-select-datetime.module */ "./src/app/shareds/components/ghm-select-datetime/ghm-select-datetime.module.ts");
/* harmony import */ var _task_quick_update_task_quick_update_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./task-quick-update/task-quick-update.component */ "./src/app/modules/task/task-management/task-quick-update/task-quick-update.component.ts");
/* harmony import */ var _task_list_task_group_tree_task_group_tree_component__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./task-list/task-group-tree/task-group-tree.component */ "./src/app/modules/task/task-management/task-list/task-group-tree/task-group-tree.component.ts");
/* harmony import */ var _task_list_task_group_tree_task_tree_task_tree_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./task-list/task-group-tree/task-tree/task-tree.component */ "./src/app/modules/task/task-management/task-list/task-group-tree/task-tree/task-tree.component.ts");
/* harmony import */ var _task_report_task_report_component__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./task-report/task-report.component */ "./src/app/modules/task/task-management/task-report/task-report.component.ts");
/* harmony import */ var _hr_organization_office_services_office_service__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ../../hr/organization/office/services/office.service */ "./src/app/modules/hr/organization/office/services/office.service.ts");
/* harmony import */ var _task_report_task_report_list_task_report_list_component__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./task-report/task-report-list/task-report-list.component */ "./src/app/modules/task/task-management/task-report/task-report-list/task-report-list.component.ts");
/* harmony import */ var _target_targets_services_target_service__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ../target/targets/services/target.service */ "./src/app/modules/task/target/targets/services/target.service.ts");
/* harmony import */ var _shareds_components_nh_tree_nh_tree_module__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ../../../shareds/components/nh-tree/nh-tree.module */ "./src/app/shareds/components/nh-tree/nh-tree.module.ts");
/* harmony import */ var _target_target_share_component_target_share_component_module__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ../target/target-share-component/target-share-component.module */ "./src/app/modules/task/target/target-share-component/target-share-component.module.ts");






















































var TaskModule = /** @class */ (function () {
    function TaskModule() {
    }
    TaskModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _task_routing_module__WEBPACK_IMPORTED_MODULE_3__["TaskRoutingModule"], _shareds_layouts_layout_module__WEBPACK_IMPORTED_MODULE_6__["LayoutModule"], _shareds_components_nh_modal_nh_modal_module__WEBPACK_IMPORTED_MODULE_8__["NhModalModule"], primeng_treetable__WEBPACK_IMPORTED_MODULE_15__["TreeTableModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"], _shareds_components_ghm_input_ghm_input_module__WEBPACK_IMPORTED_MODULE_18__["GhmInputModule"],
                _shareds_components_nh_select_nh_select_module__WEBPACK_IMPORTED_MODULE_22__["NhSelectModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCheckboxModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_11__["ReactiveFormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormsModule"], ngx_toastr__WEBPACK_IMPORTED_MODULE_12__["ToastrModule"].forRoot(), _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatExpansionModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTreeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatRadioModule"], _shareds_components_ghm_paging_ghm_paging_module__WEBPACK_IMPORTED_MODULE_23__["GhmPagingModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTooltipModule"], _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_30__["NgxChartsModule"], _shareds_components_ghm_select_ghm_select_module__WEBPACK_IMPORTED_MODULE_31__["GhmSelectModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"], _shareds_components_ghm_input_ghm_input_module__WEBPACK_IMPORTED_MODULE_18__["GhmInputModule"],
                _shareds_components_ghm_user_suggestion_ghm_user_suggestion_module__WEBPACK_IMPORTED_MODULE_32__["GhmUserSuggestionModule"], _shareds_components_ghm_suggestion_user_ghm_suggestion_user_module__WEBPACK_IMPORTED_MODULE_43__["GhmSuggestionUserModule"], _shareds_components_ghm_comment_ghm_comment_module__WEBPACK_IMPORTED_MODULE_33__["GhmCommentModule"], _shareds_components_ghm_attachments_ghm_attachments_module__WEBPACK_IMPORTED_MODULE_34__["GhmAttachmentsModule"], _shareds_components_ghm_dialog_ghm_dialog_module__WEBPACK_IMPORTED_MODULE_35__["GhmDialogModule"], _shareds_components_nh_datetime_picker_nh_date_module__WEBPACK_IMPORTED_MODULE_36__["NhDateModule"],
                _shareds_pipe_datetime_format_datetime_format_module__WEBPACK_IMPORTED_MODULE_37__["DatetimeFormatModule"], _shareds_components_ghm_select_datetime_ghm_select_datetime_module__WEBPACK_IMPORTED_MODULE_44__["GhmSelectDatetimeModule"], _shareds_components_ghm_setting_notification_ghm_setting_notification_module__WEBPACK_IMPORTED_MODULE_40__["GhmSettingNotificationModule"], _shareds_components_nh_tree_nh_tree_module__WEBPACK_IMPORTED_MODULE_52__["NHTreeModule"],
                _shareds_components_ghm_select_user_picker_ghm_select_user_picker_module__WEBPACK_IMPORTED_MODULE_38__["GhmSelectUserPickerModule"], _shareds_components_ghm_checklist_ghm_checklist_module__WEBPACK_IMPORTED_MODULE_39__["GhmChecklistModule"], _target_target_share_component_target_share_component_module__WEBPACK_IMPORTED_MODULE_53__["TargetShareComponentModule"],
                _log_log_module__WEBPACK_IMPORTED_MODULE_41__["LogModule"], _shareds_components_nh_user_picker_nh_user_picker_module__WEBPACK_IMPORTED_MODULE_42__["NhUserPickerModule"],
                _core_core_module__WEBPACK_IMPORTED_MODULE_7__["CoreModule"], _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_13__["SweetAlert2Module"].forRoot({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary cm-mgr-5',
                    cancelButtonClass: 'btn',
                    confirmButtonText: 'Đồng ý',
                    showCancelButton: true,
                    cancelButtonText: 'Hủy bỏ'
                })],
            exports: [],
            declarations: [_task_role_group_task_role_group_component__WEBPACK_IMPORTED_MODULE_5__["TaskRoleGroupComponent"], _task_role_group_role_group_form_role_group_form_component__WEBPACK_IMPORTED_MODULE_9__["RoleGroupFormComponent"],
                _task_library_task_library_component__WEBPACK_IMPORTED_MODULE_14__["TaskLibraryComponent"], _task_library_task_group_library_form_task_group_library_form_component__WEBPACK_IMPORTED_MODULE_17__["TaskGroupLibraryFormComponent"], _task_library_task_library_form_task_library_form_component__WEBPACK_IMPORTED_MODULE_19__["TaskLibraryFormComponent"], _task_library_task_check_list_form_task_check_list_form_component__WEBPACK_IMPORTED_MODULE_20__["TaskCheckListFormComponent"],
                _task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_24__["TaskDetailComponent"], _task_list_task_list_component__WEBPACK_IMPORTED_MODULE_25__["TaskListComponent"], _task_form_task_form_component__WEBPACK_IMPORTED_MODULE_26__["TaskFormComponent"], _shared_components_task_user_suggestion_task_user_suggestion_component__WEBPACK_IMPORTED_MODULE_27__["TaskUserSuggestionComponent"], _task_list_task_list_item_task_list_item_component__WEBPACK_IMPORTED_MODULE_28__["TaskListItemComponent"],
                _task_report_task_report_component__WEBPACK_IMPORTED_MODULE_48__["TaskReportComponent"],
                _task_list_task_list_group_task_list_group_component__WEBPACK_IMPORTED_MODULE_29__["TaskListGroupComponent"], _task_quick_update_task_quick_update_component__WEBPACK_IMPORTED_MODULE_45__["TaskQuickUpdateComponent"], _task_list_task_group_tree_task_group_tree_component__WEBPACK_IMPORTED_MODULE_46__["TaskGroupTreeComponent"], _task_list_task_group_tree_task_tree_task_tree_component__WEBPACK_IMPORTED_MODULE_47__["TaskTreeComponent"], _task_report_task_report_list_task_report_list_component__WEBPACK_IMPORTED_MODULE_50__["TaskReportListComponent"]
            ],
            entryComponents: [_task_form_task_form_component__WEBPACK_IMPORTED_MODULE_26__["TaskFormComponent"], _task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_24__["TaskDetailComponent"], _task_library_task_library_component__WEBPACK_IMPORTED_MODULE_14__["TaskLibraryComponent"], _task_report_task_report_list_task_report_list_component__WEBPACK_IMPORTED_MODULE_50__["TaskReportListComponent"]],
            providers: [_task_role_group_task_role_group_service__WEBPACK_IMPORTED_MODULE_10__["TaskRoleGroupService"], _task_library_services_task_library_service__WEBPACK_IMPORTED_MODULE_16__["TaskLibraryService"], _task_library_services_task_check_list_service__WEBPACK_IMPORTED_MODULE_21__["TaskCheckListService"], _task_library_services_task_check_list_service__WEBPACK_IMPORTED_MODULE_21__["TaskCheckListService"], _hr_organization_office_services_office_service__WEBPACK_IMPORTED_MODULE_49__["OfficeService"], _target_targets_services_target_service__WEBPACK_IMPORTED_MODULE_51__["TargetService"]],
        })
    ], TaskModule);
    return TaskModule;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-checklist/ghm-checklist.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-checklist/ghm-checklist.module.ts ***!
  \**************************************************************************/
/*! exports provided: GhmChecklistModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmChecklistModule", function() { return GhmChecklistModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ghm_checklist_ghm_checklist_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ghm-checklist/ghm-checklist.component */ "./src/app/shareds/components/ghm-checklist/ghm-checklist/ghm-checklist.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ghm_input_ghm_input_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../ghm-input/ghm-input.module */ "./src/app/shareds/components/ghm-input/ghm-input.module.ts");







var GhmChecklistModule = /** @class */ (function () {
    function GhmChecklistModule() {
    }
    GhmChecklistModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_ghm_checklist_ghm_checklist_component__WEBPACK_IMPORTED_MODULE_3__["GhmChecklistComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCheckboxModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _ghm_input_ghm_input_module__WEBPACK_IMPORTED_MODULE_6__["GhmInputModule"]
            ],
            exports: [_ghm_checklist_ghm_checklist_component__WEBPACK_IMPORTED_MODULE_3__["GhmChecklistComponent"]]
        })
    ], GhmChecklistModule);
    return GhmChecklistModule;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-checklist/ghm-checklist/ghm-checklist.component.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-checklist/ghm-checklist/ghm-checklist.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ul class=\"ghm-checklist-container\">\r\n    <li *ngFor=\"let checklist of source\" class=\"ghm-checklist\"\r\n        [class.complete]=\"checklist.isComplete\">\r\n        <div class=\"checklist-item\" *ngIf=\"!checklist.isEdit\">\r\n            <mat-checkbox\r\n                color=\"primary\"\r\n                [disabled]=\"!allowComplete\"\r\n                [checked]=\"checklist.isComplete\"\r\n                (change)=\"onCompleteChange(checklist)\"></mat-checkbox>\r\n            <div class=\"ghm-checklist-content\" (click)=\"edit(checklist)\">\r\n                {{ checklist.name }}\r\n            </div>\r\n            <span class=\"btn-remove-checklist\"\r\n                  data-icon=\"delete-16\"\r\n                  (click)=\"allowDelete ? remove(checklist.id) :''\"></span>\r\n        </div>\r\n        <form action=\"\" class=\"checklist-form\" (ngSubmit)=\"save()\" *ngIf=\"checklist.isEdit\">\r\n            <ghm-input\r\n                #ghmInput\r\n                icon=\"fa fa-pencil\"\r\n                [(ngModel)]=\"checklist.name\"\r\n                [placeholder]=\"placeholder\"\r\n                name=\"checklistName\"></ghm-input>\r\n            <div class=\"button-wrapper\">\r\n                <button class=\"btn blue\">Cập nhật</button>\r\n                <button type=\"button\" class=\"btn btn-light\" (click)=\"closeForm(checklist)\">Đóng</button>\r\n            </div>\r\n        </form>\r\n    </li>\r\n</ul><!--END: .ghm-checklist -->\r\n<div class=\"ghm-checklist-error-message\" *ngIf=\"errorMessage\">\r\n    {{ errorMessage }}\r\n</div>\r\n<form class=\"checklist-form\" (ngSubmit)=\"save()\" *ngIf=\"allowAdd && isShowForm\">\r\n    <ghm-input\r\n        #ghmInput\r\n        icon=\"fa fa-pencil\"\r\n        [(ngModel)]=\"checklist.name\"\r\n        [placeholder]=\"placeholder\"\r\n        name=\"checklistName\"></ghm-input>\r\n    <div class=\"button-wrapper\">\r\n        <button class=\"btn blue\">Cập nhật</button>\r\n        <button type=\"button\" class=\"btn btn-light\" (click)=\"closeForm()\">Đóng</button>\r\n    </div>\r\n</form>\r\n<button class=\"add-checklist\" (click)=\"isShowForm=true\" *ngIf=\"isShowForm == false && allowAdd\">\r\n    {{ buttonText }}\r\n</button>\r\n"

/***/ }),

/***/ "./src/app/shareds/components/ghm-checklist/ghm-checklist/ghm-checklist.component.scss":
/*!*********************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-checklist/ghm-checklist/ghm-checklist.component.scss ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ul.ghm-checklist-container {\n  list-style: none;\n  padding-left: 0;\n  margin-bottom: 0;\n  display: block;\n  width: 100%; }\n  ul.ghm-checklist-container .ghm-checklist {\n    display: block;\n    width: 100%;\n    position: relative;\n    border-bottom: 1px solid #ddd;\n    padding: 6px 20px; }\n  ul.ghm-checklist-container .ghm-checklist:hover {\n      background-color: #dff0fd;\n      cursor: pointer; }\n  ul.ghm-checklist-container .ghm-checklist mat-checkbox {\n      position: absolute;\n      top: 5px;\n      left: 0; }\n  ul.ghm-checklist-container .ghm-checklist.complete {\n      text-decoration: line-through; }\n  ul.ghm-checklist-container .ghm-checklist .btn-remove-checklist {\n      position: absolute;\n      top: 5px;\n      right: 5px;\n      text-decoration: none;\n      opacity: 1; }\n  ul.ghm-checklist-container .ghm-checklist .btn-remove-checklist:hover {\n        opacity: .8;\n        cursor: pointer; }\n  ul.ghm-checklist-container *[data-icon=\"delete-16\"] {\n    background: url('delete.png') center no-repeat;\n    display: inline-block;\n    zoom: 1;\n    width: 16px;\n    height: 16px;\n    letter-spacing: 0;\n    word-spacing: 0; }\n  ul.ghm-checklist-container *[data-icon=\"delete-16\"]:before {\n      content: ''; }\n  form.checklist-form .button-wrapper {\n  margin-top: 5px; }\n  form.checklist-form .button-wrapper button {\n    margin-right: 5px; }\n  button.add-checklist {\n  background: transparent;\n  border: 1px dashed #ddd;\n  color: #757575;\n  display: block;\n  width: 100%;\n  padding: 6px;\n  text-align: center; }\n  button.add-checklist:hover {\n    background-color: #f5f7f7; }\n  .ghm-checklist-error-message {\n  margin-top: 5px;\n  margin-bottom: 5px;\n  padding: 5px 10px;\n  border: 1px solid #d63031;\n  background-color: #d63031;\n  color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL2dobS1jaGVja2xpc3QvZ2htLWNoZWNrbGlzdC9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXBwXFxzaGFyZWRzXFxjb21wb25lbnRzXFxnaG0tY2hlY2tsaXN0XFxnaG0tY2hlY2tsaXN0XFxnaG0tY2hlY2tsaXN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBO0VBQ0ksZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLFdBQVcsRUFBQTtFQUxmO0lBT1EsY0FBYztJQUNkLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsNkJBYlU7SUFjVixpQkFBaUIsRUFBQTtFQVh6QjtNQWNZLHlCQUF5QjtNQUN6QixlQUFlLEVBQUE7RUFmM0I7TUFtQlksa0JBQWtCO01BQ2xCLFFBQVE7TUFDUixPQUFPLEVBQUE7RUFyQm5CO01BeUJZLDZCQUE2QixFQUFBO0VBekJ6QztNQTZCWSxrQkFBa0I7TUFDbEIsUUFBUTtNQUNSLFVBQVU7TUFDVixxQkFBcUI7TUFDckIsVUFBVSxFQUFBO0VBakN0QjtRQW9DZ0IsV0FBVztRQUNYLGVBQWUsRUFBQTtFQXJDL0I7SUEyQ1EsOENBQW1EO0lBQ25ELHFCQUFxQjtJQUNyQixPQUFPO0lBQ1AsV0FBVztJQUNYLFlBQVk7SUFDWixpQkFBaUI7SUFDakIsZUFBZSxFQUFBO0VBakR2QjtNQW9EWSxXQUNKLEVBQUE7RUFJUjtFQUVRLGVBQWUsRUFBQTtFQUZ2QjtJQUtZLGlCQUFpQixFQUFBO0VBSzdCO0VBQ0ksdUJBQXVCO0VBQ3ZCLHVCQXhFYztFQXlFZCxjQUFjO0VBQ2QsY0FBYztFQUNkLFdBQVc7RUFDWCxZQUFZO0VBQ1osa0JBQWtCLEVBQUE7RUFQdEI7SUFVUSx5QkFBeUIsRUFBQTtFQUlqQztFQUNJLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLHlCQXZGaUI7RUF3RmpCLHlCQXhGaUI7RUF5RmpCLFlBQVksRUFBQSIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZHMvY29tcG9uZW50cy9naG0tY2hlY2tsaXN0L2dobS1jaGVja2xpc3QvZ2htLWNoZWNrbGlzdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiRib3JkZXJDb2xvcjogI2RkZDtcclxuJGRhbmdlckNvbG9yOiAjZDYzMDMxO1xyXG5cclxudWwuZ2htLWNoZWNrbGlzdC1jb250YWluZXIge1xyXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgIHBhZGRpbmctbGVmdDogMDtcclxuICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgLmdobS1jaGVja2xpc3Qge1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJGJvcmRlckNvbG9yO1xyXG4gICAgICAgIHBhZGRpbmc6IDZweCAyMHB4O1xyXG5cclxuICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2RmZjBmZDtcclxuICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbWF0LWNoZWNrYm94IHtcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDVweDtcclxuICAgICAgICAgICAgbGVmdDogMDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICYuY29tcGxldGUge1xyXG4gICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IGxpbmUtdGhyb3VnaDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5idG4tcmVtb3ZlLWNoZWNrbGlzdCB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiA1cHg7XHJcbiAgICAgICAgICAgIHJpZ2h0OiA1cHg7XHJcbiAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgICAgICAgb3BhY2l0eTogMTtcclxuXHJcbiAgICAgICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgb3BhY2l0eTogLjg7XHJcbiAgICAgICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgKltkYXRhLWljb249XCJkZWxldGUtMTZcIl0ge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHVybChpbWFnZXMvZGVsZXRlLnBuZykgY2VudGVyIG5vLXJlcGVhdDtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgem9vbTogMTtcclxuICAgICAgICB3aWR0aDogMTZweDtcclxuICAgICAgICBoZWlnaHQ6IDE2cHg7XHJcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDA7XHJcbiAgICAgICAgd29yZC1zcGFjaW5nOiAwO1xyXG5cclxuICAgICAgICAmOmJlZm9yZSB7XHJcbiAgICAgICAgICAgIGNvbnRlbnQ6ICcnXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5mb3JtLmNoZWNrbGlzdC1mb3JtIHtcclxuICAgIC5idXR0b24td3JhcHBlciB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG5cclxuICAgICAgICBidXR0b24ge1xyXG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbmJ1dHRvbi5hZGQtY2hlY2tsaXN0IHtcclxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgYm9yZGVyOiAxcHggZGFzaGVkICRib3JkZXJDb2xvcjtcclxuICAgIGNvbG9yOiAjNzU3NTc1O1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHBhZGRpbmc6IDZweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbiAgICAmOmhvdmVyIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjVmN2Y3O1xyXG4gICAgfVxyXG59XHJcblxyXG4uZ2htLWNoZWNrbGlzdC1lcnJvci1tZXNzYWdlIHtcclxuICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgIHBhZGRpbmc6IDVweCAxMHB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgJGRhbmdlckNvbG9yO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGRhbmdlckNvbG9yO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/shareds/components/ghm-checklist/ghm-checklist/ghm-checklist.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-checklist/ghm-checklist/ghm-checklist.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: GhmChecklistComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmChecklistComponent", function() { return GhmChecklistComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _ghm_checklist_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ghm-checklist.model */ "./src/app/shareds/components/ghm-checklist/ghm-checklist/ghm-checklist.model.ts");
/* harmony import */ var _ghm_input_ghm_input_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../ghm-input/ghm-input.component */ "./src/app/shareds/components/ghm-input/ghm-input.component.ts");





var GhmChecklistComponent = /** @class */ (function () {
    function GhmChecklistComponent() {
        this.source = [];
        this.allowAdd = true;
        this.allowComplete = true;
        this.allowEdit = true;
        this.allowDelete = true;
        this.buttonText = 'Thêm checklist';
        this.removed = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.saved = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.completeChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.errorMessage = '';
        this._isShowForm = false;
    }
    Object.defineProperty(GhmChecklistComponent.prototype, "isShowForm", {
        get: function () {
            return this._isShowForm;
        },
        set: function (value) {
            var _this = this;
            this._isShowForm = value;
            if (value) {
                this.checklist = new _ghm_checklist_model__WEBPACK_IMPORTED_MODULE_3__["Checklist"]();
            }
            setTimeout(function () {
                _this.ghmInputElement.focus();
            });
        },
        enumerable: true,
        configurable: true
    });
    GhmChecklistComponent.prototype.ngOnInit = function () {
    };
    GhmChecklistComponent.prototype.onCompleteChange = function (checklist) {
        checklist.isComplete = !checklist.isComplete;
        this.completeChange.emit(checklist);
    };
    GhmChecklistComponent.prototype.edit = function (checklist) {
        if (!this.allowEdit) {
            return;
        }
        this.checkListName = checklist.name;
        lodash__WEBPACK_IMPORTED_MODULE_2__["each"](this.source, function (item) {
            item.isEdit = false;
        });
        this.isShowForm = false;
        checklist.isEdit = true;
        this.checklist = checklist;
    };
    GhmChecklistComponent.prototype.remove = function (id) {
        lodash__WEBPACK_IMPORTED_MODULE_2__["remove"](this.source, function (checklist) {
            return checklist.id === id;
        });
        this.removed.emit(id);
    };
    GhmChecklistComponent.prototype.save = function () {
        var _this = this;
        if (!this.checklist.name) {
            this.errorMessage = 'Vui lòng nhập checklist';
            return;
        }
        var isExists = lodash__WEBPACK_IMPORTED_MODULE_2__["find"](this.source, function (checklist) {
            return checklist.name === _this.checklist.name && checklist.id !== _this.checklist.id;
        });
        if (isExists) {
            this.errorMessage = 'Checklist đã tồn tại';
            return;
        }
        this.saved.emit(this.checklist);
        if (this.checklist.id) {
            var checklistInfo = lodash__WEBPACK_IMPORTED_MODULE_2__["find"](this.source, function (checklist) {
                return checklist.id === _this.checklist.id;
            });
            if (checklistInfo) {
                checklistInfo.name = this.checklist.name;
                checklistInfo.isEdit = false;
            }
        }
        this.resetModel();
    };
    GhmChecklistComponent.prototype.closeForm = function (checkList) {
        if (checkList) {
            checkList.isEdit = false;
            checkList.name = this.checkListName;
        }
        else {
            this.isShowForm = false;
        }
        this.errorMessage = '';
    };
    GhmChecklistComponent.prototype.resetModel = function () {
        this.checklist = new _ghm_checklist_model__WEBPACK_IMPORTED_MODULE_3__["Checklist"]();
        this.errorMessage = '';
        this.isShowForm = false;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('ghmInput'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ghm_input_ghm_input_component__WEBPACK_IMPORTED_MODULE_4__["GhmInputComponent"])
    ], GhmChecklistComponent.prototype, "ghmInputElement", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmChecklistComponent.prototype, "source", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmChecklistComponent.prototype, "allowAdd", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmChecklistComponent.prototype, "allowComplete", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmChecklistComponent.prototype, "allowEdit", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmChecklistComponent.prototype, "allowDelete", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmChecklistComponent.prototype, "buttonText", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmChecklistComponent.prototype, "removed", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmChecklistComponent.prototype, "saved", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmChecklistComponent.prototype, "completeChange", void 0);
    GhmChecklistComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ghm-checklist',
            template: __webpack_require__(/*! ./ghm-checklist.component.html */ "./src/app/shareds/components/ghm-checklist/ghm-checklist/ghm-checklist.component.html"),
            styles: [__webpack_require__(/*! ./ghm-checklist.component.scss */ "./src/app/shareds/components/ghm-checklist/ghm-checklist/ghm-checklist.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], GhmChecklistComponent);
    return GhmChecklistComponent;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-checklist/ghm-checklist/ghm-checklist.model.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-checklist/ghm-checklist/ghm-checklist.model.ts ***!
  \***************************************************************************************/
/*! exports provided: Checklist */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Checklist", function() { return Checklist; });
var Checklist = /** @class */ (function () {
    function Checklist() {
        this.isComplete = false;
        this.isEdit = false;
        this.name = '';
        this.id = null;
        this.concurrencyStamp = null;
    }
    return Checklist;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-setting-notification/ghm-setting-notification.component.html":
/*!*****************************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-setting-notification/ghm-setting-notification.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #settingNotificationModal [size]=\"'md'\" (hidden)=\"onModalHidden()\">\r\n    <nh-modal-header>\r\n        Thông tin cấu hình thông báo\r\n    </nh-modal-header>\r\n    <nh-modal-content>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-4\"></label>\r\n            <div class=\"col-sm-8\">\r\n                <mat-radio-group [(ngModel)]=\"isNotification\" (change)=\"setBuildForm()\">\r\n                    <mat-radio-button color=\"primary\" value=\"0\">Không thông báo\r\n                    </mat-radio-button>\r\n                    <mat-radio-button color=\"primary\" class=\"cm-mgl-10\" value=\"1\">Tự cấu hình</mat-radio-button>\r\n                </mat-radio-group>\r\n            </div>\r\n            <div class=\"center\" *ngIf=\"isNotification == 0\">\r\n                <ghm-button classes=\"btn blue cm-mgr-5\" [type]=\"'button'\" (clicked)=\"updateIsNotification()\"\r\n                            i18n=\"@@update\">\r\n                    Cập nhật\r\n                </ghm-button>\r\n                <ghm-button type=\"button\" classes=\"btn btn-light\" nh-dismiss=\"true\">\r\n                    Đóng\r\n                </ghm-button>\r\n            </div>\r\n        </div>\r\n        <form class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n            <div class=\"form-body\" *ngIf=\"isNotification == 1\">\r\n                <div class=\"wrap-config\">\r\n                    <div class=\"form-group\" [class.has-error]=\"formErrors.dates\">\r\n                        <label class=\"col-sm-4 ghm-label\" i18n=\"@@dayNotification\" ghmLabel=\"Ngày thông báo\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <nh-date format=\"DD/MM/YYYY\" id=\"dates\" [type]=\"'inputButton'\"\r\n                                     formControlName=\"dates\"></nh-date>\r\n                            <span class=\"help-block center\">\r\n                            {formErrors.dates, select, required {please chose date }}\r\n                        </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group\" [class.has-error]=\"formErrors.hours || formErrors.minutes\">\r\n                        <label i18n=\"@@time\" ghmLabel=\"Thời gian\" class=\"col-sm-4 ghm-label\"></label>\r\n                        <div class=\"col-sm-4\">\r\n                            <ghm-select\r\n                                formControlName=\"hours\"\r\n                                [elementId]=\"'hourNotification'\"\r\n                                [data]=\"listHour\"\r\n                                i18n-title=\"@@hour\"\r\n                                [title]=\"'Chọn giờ'\"></ghm-select>\r\n                            <span class=\"help-block col-sm-6 center\">\r\n                            {formErrors.hours, select, required {please chose hours }}\r\n                        </span>\r\n                        </div>\r\n                        <div class=\"col-sm-4\">\r\n                            <ghm-select\r\n                                formControlName=\"minutes\"\r\n                                [elementId]=\"'minuteNotification'\"\r\n                                [data]=\"listMinute\"\r\n                                i18n-title=\"@@minute\"\r\n                                [title]=\"'Chọn phút'\"></ghm-select>\r\n                            <span class=\"help-block col-sm-6 center\">\r\n                            {formErrors.minutes, select, required {please chose minute }}\r\n                        </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label class=\"col-sm-4 ghm-label\" ghmLabel=\"Loại hình thông báo\"> </label>\r\n                        <div class=\"col-sm-8\">\r\n                            <div>\r\n                                <mat-checkbox formControlName=\"isSendNotificationEmail\" color=\"primary\"\r\n                                              i18n=\"@@notificationEmail\">\r\n                                    Thông báo qua email\r\n                                </mat-checkbox>\r\n                            </div>\r\n                            <div>\r\n                                <mat-checkbox formControlName=\"isSendNotification\" color=\"primary\"\r\n                                              i18n=\"@@notification\">\r\n                                    Thông báo qua notify\r\n                                </mat-checkbox>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <hr>\r\n                <div class=\"center\">\r\n                    <ghm-button classes=\"btn blue cm-mgr-5\" [loading]=\"isSaving\" i18n=\"@@update\">\r\n                        Cập nhật\r\n                    </ghm-button>\r\n                    <ghm-button type=\"button\" classes=\"btn btn-light\" nh-dismiss=\"true\">\r\n                        Đóng\r\n                    </ghm-button>\r\n                </div>\r\n            </div>\r\n        </form>\r\n    </nh-modal-content>\r\n\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/shareds/components/ghm-setting-notification/ghm-setting-notification.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-setting-notification/ghm-setting-notification.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: GhmSettingNotificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmSettingNotificationComponent", function() { return GhmSettingNotificationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _setting_notification_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./setting-notification.model */ "./src/app/shareds/components/ghm-setting-notification/setting-notification.model.ts");
/* harmony import */ var _nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _ghm_setting_notification_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ghm-setting-notification.service */ "./src/app/shareds/components/ghm-setting-notification/ghm-setting-notification.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");








var GhmSettingNotificationComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](GhmSettingNotificationComponent, _super);
    function GhmSettingNotificationComponent(fb, settingNotificationService) {
        var _this = _super.call(this) || this;
        _this.fb = fb;
        _this.settingNotificationService = settingNotificationService;
        _this.listHour = [];
        _this.listMinute = [];
        _this.settingNotificationModel = new _setting_notification_model__WEBPACK_IMPORTED_MODULE_4__["SettingNotification"]();
        return _this;
    }
    Object.defineProperty(GhmSettingNotificationComponent.prototype, "selectedNotification", {
        set: function (value) {
            if (value != null) {
                this.settingNotificationModel = value;
                if (value.isNotification === true) {
                    this.isNotification = '1';
                }
                this.buildForm();
            }
        },
        enumerable: true,
        configurable: true
    });
    GhmSettingNotificationComponent.prototype.ngOnInit = function () {
        this.isNotification = '0';
        this.renderTime();
        this.buildForm();
    };
    GhmSettingNotificationComponent.prototype.onModalHidden = function () {
        this.model.reset(new _setting_notification_model__WEBPACK_IMPORTED_MODULE_4__["SettingNotification"]());
        if (this.isModified) {
            this.saveSuccessful.emit();
        }
    };
    GhmSettingNotificationComponent.prototype.open = function () {
        this.settingNotificationModal.open();
    };
    GhmSettingNotificationComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.validateModel();
        if (isValid) {
            this.isSaving = true;
            this.settingNotificationService.update(this.url, this.model.value)
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.isSaving = false; })).subscribe(function (result) {
                _this.settingNotificationModel.concurrencyStamp = result.data;
                _this.model.value.concurrencyStamp = result.data;
                console.log(_this.model.value);
                _this.saveSuccessful.emit(_this.model.value);
                _this.settingNotificationModal.dismiss();
            });
        }
    };
    GhmSettingNotificationComponent.prototype.updateIsNotification = function () {
        var _this = this;
        this.isSaving = true;
        var settingNotification = new _setting_notification_model__WEBPACK_IMPORTED_MODULE_4__["SettingNotification"]();
        settingNotification.isNotification = false;
        settingNotification.concurrencyStamp = this.settingNotificationModel.concurrencyStamp;
        this.settingNotificationService.update(this.url, settingNotification)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.isSaving = false; })).subscribe(function (result) {
            _this.settingNotificationModel.concurrencyStamp = result.data;
            settingNotification.concurrencyStamp = result.data;
            _this.saveSuccessful.emit(settingNotification);
            _this.settingNotificationModal.dismiss();
        });
    };
    GhmSettingNotificationComponent.prototype.setBuildForm = function () {
        if (this.isNotification) {
            this.buildForm();
        }
    };
    GhmSettingNotificationComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.renderFormError(['dates', 'hours', 'minutes']);
        this.validationMessages = this.renderFormErrorMessage([
            { 'dates': ['required'] },
            { 'hours': ['required'] },
            { 'minutes': ['required'] }
        ]);
        this.model = this.fb.group({
            isNotification: [true, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
                ]],
            dates: [this.settingNotificationModel.dates, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
                ]],
            hours: [this.settingNotificationModel.hours, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
                ]],
            minutes: [this.settingNotificationModel.minutes, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
                ]],
            isSendNotificationEmail: [this.settingNotificationModel.isSendNotificationEmail],
            isSendNotification: [this.settingNotificationModel.isSendNotification],
            taskId: [this.taskId],
            concurrencyStamp: [this.settingNotificationModel.concurrencyStamp],
            taskName: [this.settingNotificationModel.taskName]
        });
        this.subscribers.modelValueChanges = this.model.valueChanges.subscribe(function () { return _this.validateModel(false); });
    };
    GhmSettingNotificationComponent.prototype.renderTime = function () {
        for (var i = 0; i <= 24; i++) {
            var item = { id: i, name: i.toString() + ' Giờ' };
            this.listHour.push(item);
        }
        for (var i = 0; i < 60; i++) {
            var item = { id: i, name: i.toString() + ' Phút' };
            this.listMinute.push(item);
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('settingNotificationModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_5__["NhModalComponent"])
    ], GhmSettingNotificationComponent.prototype, "settingNotificationModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmSettingNotificationComponent.prototype, "taskId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GhmSettingNotificationComponent.prototype, "url", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
    ], GhmSettingNotificationComponent.prototype, "selectedNotification", null);
    GhmSettingNotificationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ghm-setting-notification',
            template: __webpack_require__(/*! ./ghm-setting-notification.component.html */ "./src/app/shareds/components/ghm-setting-notification/ghm-setting-notification.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _ghm_setting_notification_service__WEBPACK_IMPORTED_MODULE_6__["GhmSettingNotificationService"]])
    ], GhmSettingNotificationComponent);
    return GhmSettingNotificationComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_3__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/shareds/components/ghm-setting-notification/ghm-setting-notification.module.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-setting-notification/ghm-setting-notification.module.ts ***!
  \************************************************************************************************/
/*! exports provided: GhmSettingNotificationModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmSettingNotificationModule", function() { return GhmSettingNotificationModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _nh_modal_nh_modal_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../nh-modal/nh-modal.module */ "./src/app/shareds/components/nh-modal/nh-modal.module.ts");
/* harmony import */ var _ghm_setting_notification_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ghm-setting-notification.component */ "./src/app/shareds/components/ghm-setting-notification/ghm-setting-notification.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ghm_select_ghm_select_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../ghm-select/ghm-select.module */ "./src/app/shareds/components/ghm-select/ghm-select.module.ts");
/* harmony import */ var _ghm_setting_notification_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./ghm-setting-notification.service */ "./src/app/shareds/components/ghm-setting-notification/ghm-setting-notification.service.ts");
/* harmony import */ var _nh_datetime_picker_nh_date_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../nh-datetime-picker/nh-date.module */ "./src/app/shareds/components/nh-datetime-picker/nh-date.module.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");











var GhmSettingNotificationModule = /** @class */ (function () {
    function GhmSettingNotificationModule() {
    }
    GhmSettingNotificationModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _core_core_module__WEBPACK_IMPORTED_MODULE_3__["CoreModule"], _nh_modal_nh_modal_module__WEBPACK_IMPORTED_MODULE_4__["NhModalModule"], _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatRadioModule"], _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatCheckboxModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"], _ghm_select_ghm_select_module__WEBPACK_IMPORTED_MODULE_7__["GhmSelectModule"], _nh_datetime_picker_nh_date_module__WEBPACK_IMPORTED_MODULE_9__["NhDateModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"]
            ],
            declarations: [_ghm_setting_notification_component__WEBPACK_IMPORTED_MODULE_5__["GhmSettingNotificationComponent"]],
            exports: [_ghm_setting_notification_component__WEBPACK_IMPORTED_MODULE_5__["GhmSettingNotificationComponent"]],
            providers: [_ghm_setting_notification_service__WEBPACK_IMPORTED_MODULE_8__["GhmSettingNotificationService"]]
        })
    ], GhmSettingNotificationModule);
    return GhmSettingNotificationModule;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-setting-notification/ghm-setting-notification.service.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-setting-notification/ghm-setting-notification.service.ts ***!
  \*************************************************************************************************/
/*! exports provided: GhmSettingNotificationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmSettingNotificationService", function() { return GhmSettingNotificationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");







var GhmSettingNotificationService = /** @class */ (function () {
    function GhmSettingNotificationService(http, spinnerService, toastrService) {
        this.http = http;
        this.spinnerService = spinnerService;
        this.toastrService = toastrService;
    }
    GhmSettingNotificationService.prototype.update = function (urlApi, settingNotification) {
        var _this = this;
        var url = "" + _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiGatewayUrl + urlApi;
        this.spinnerService.show();
        return this.http.post(url, settingNotification)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastrService.success(result.message);
            return result;
        }));
    };
    GhmSettingNotificationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__["SpinnerService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"]])
    ], GhmSettingNotificationService);
    return GhmSettingNotificationService;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-setting-notification/setting-notification.model.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-setting-notification/setting-notification.model.ts ***!
  \*******************************************************************************************/
/*! exports provided: SettingNotification */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingNotification", function() { return SettingNotification; });
var SettingNotification = /** @class */ (function () {
    function SettingNotification() {
    }
    return SettingNotification;
}());



/***/ }),

/***/ "./src/app/shareds/constants/task-type.const.ts":
/*!******************************************************!*\
  !*** ./src/app/shareds/constants/task-type.const.ts ***!
  \******************************************************/
/*! exports provided: TaskType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskType", function() { return TaskType; });
var TaskType = {
    taskLibrary: 0,
    task: 1
};


/***/ })

}]);
//# sourceMappingURL=modules-task-task-management-task-module.js.map