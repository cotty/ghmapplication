(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-task-target-target-module"],{

/***/ "./src/app/modules/task/target/target-libraries/model/target-group-library.model.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-libraries/model/target-group-library.model.ts ***!
  \******************************************************************************************/
/*! exports provided: TargetGroupLibrary */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetGroupLibrary", function() { return TargetGroupLibrary; });
var TargetGroupLibrary = /** @class */ (function () {
    function TargetGroupLibrary() {
    }
    return TargetGroupLibrary;
}());



/***/ }),

/***/ "./src/app/modules/task/target/target-libraries/model/target-library.model.ts":
/*!************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-libraries/model/target-library.model.ts ***!
  \************************************************************************************/
/*! exports provided: TargetLibrary */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetLibrary", function() { return TargetLibrary; });
var TargetLibrary = /** @class */ (function () {
    function TargetLibrary() {
    }
    return TargetLibrary;
}());



/***/ }),

/***/ "./src/app/modules/task/target/target-libraries/services/target-group-library.service.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-libraries/services/target-group-library.service.ts ***!
  \***********************************************************************************************/
/*! exports provided: TargetGroupLibraryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetGroupLibraryService", function() { return TargetGroupLibraryService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../../environments/environment */ "./src/environments/environment.ts");








var TargetGroupLibraryService = /** @class */ (function () {
    function TargetGroupLibraryService(appConfig, http, spinnerService, toastrService) {
        this.http = http;
        this.spinnerService = spinnerService;
        this.toastrService = toastrService;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].apiGatewayUrl + "api/v1/task/target-libraries";
    }
    TargetGroupLibraryService.prototype.resolve = function (route, state) {
        var queryParams = route.params;
        return this.search();
    };
    TargetGroupLibraryService.prototype.insert = function (targetGroupLibrary) {
        var _this = this;
        return this.http.post(this.url + "/group", targetGroupLibrary).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastrService.success(result.message);
            return result;
        }));
    };
    TargetGroupLibraryService.prototype.update = function (id, targetGroupLibrary) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/group/" + id, targetGroupLibrary).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastrService.success(result.message);
            return result;
        }));
    };
    TargetGroupLibraryService.prototype.search = function () {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/group").pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    TargetGroupLibraryService.prototype.delete = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.delete(this.url + "/group/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastrService.success(result.message);
            return result;
        }));
    };
    TargetGroupLibraryService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__["SpinnerService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"]])
    ], TargetGroupLibraryService);
    return TargetGroupLibraryService;
}());



/***/ }),

/***/ "./src/app/modules/task/target/target-libraries/services/target-library.service.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-libraries/services/target-library.service.ts ***!
  \*****************************************************************************************/
/*! exports provided: TargetLibraryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetLibraryService", function() { return TargetLibraryService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../../environments/environment */ "./src/environments/environment.ts");








var TargetLibraryService = /** @class */ (function () {
    function TargetLibraryService(appConfig, http, spinnerService, toastrService) {
        this.http = http;
        this.spinnerService = spinnerService;
        this.toastrService = toastrService;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].apiGatewayUrl + "api/v1/task/target-libraries";
    }
    TargetLibraryService.prototype.insert = function (targetLibrary) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url, targetLibrary).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastrService.success(result.message);
            return result;
        }));
    };
    TargetLibraryService.prototype.update = function (id, targetLibrary) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/" + id, targetLibrary).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastrService.success(result.message);
            return result;
        }));
    };
    TargetLibraryService.prototype.delete = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.delete(this.url + "/" + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.toastrService.success(result.message);
            return result;
        }));
    };
    TargetLibraryService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_4__["SpinnerService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"]])
    ], TargetLibraryService);
    return TargetLibraryService;
}());



/***/ }),

/***/ "./src/app/modules/task/target/target-libraries/target-library-form/target-library-form.component.html":
/*!*************************************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-libraries/target-library-form/target-library-form.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #targetLibraryModal size=\"md\" (hidden)=\"onModalHidden()\">\r\n    <nh-modal-header>\r\n        {{ isUpdate ? 'Cập nhật mục tiêu mẫu' : 'Thêm mục tiêu mẫu'}}\r\n    </nh-modal-header>\r\n    <form class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content>\r\n            <div class=\"form-body\">\r\n                <div class=\"form-group\" [class.has-error]=\"formErrors?.targetGroupLibraryId\">\r\n                    <label class=\"col-sm-4 ghm-label\" i18n-ghmLabel=\"@@targetGroupLibraryName\" ghmLabel=\"Tên nhóm\"\r\n                           [required]=\"true\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <ghm-input\r\n                            formControlName=\"targetGroupLibraryName\"\r\n                            [isDisabled]=\"true\"></ghm-input>\r\n                        <span class=\"help-block\">\r\n                        {formErrors?.targetGroupLibraryId, select,\r\n                            required {Tên nhóm không được để trống }}\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\" [class.has-error]=\"formErrors?.name\">\r\n                    <label class=\"col-sm-4 ghm-label\" i18n-ghmLabel=\"@@targetLibraryName\" ghmLabel=\"Tên mục tiêu\"\r\n                           [required]=\"true\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <ghm-input\r\n                            [elementId]=\"'name'\"\r\n                            [placeholder]=\"'Nhập tên mục tiêu'\"\r\n                            i18n-placeholder=\"@@enterTargetName\"\r\n                            formControlName=\"name\"></ghm-input>\r\n                        <span class=\"help-block\">\r\n                        {formErrors?.name, select, required {Tên mục tiêu không được để trống }\r\n                            maxlength {Tên nhóm không được vượt quá 256 ký tự}}\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\" [class.has-error]=\"formErrors?.description\">\r\n                    <label class=\"col-sm-4 ghm-label\" i18n-ghmLabel=\"@@description\" ghmLabel=\"Mô tả\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <textarea formControlName=\"description\"\r\n                                  class=\"form-control\"\r\n                                  rows=\"3\"></textarea>\r\n                        <span class=\"help-block\">\r\n                           {formErrors?.description, select, maxlength {Mô tả không được vượt quá 4000 ký tự}}\r\n                       </span>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\" [class.has-error]=\"formErrors?.measurementMethod\">\r\n                    <label class=\"col-sm-4 ghm-label\" i18n-ghmLabel=\"@@MeasurementMethod\" ghmLabel=\"Phương pháp đo\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <textarea formControlName=\"measurementMethod\"\r\n                                  class=\"form-control\" type=\"text\"\r\n                                  rows=\"3\"></textarea>\r\n                        <span class=\"help-block\">\r\n                            {formErrors?.measurementMethod, select, maxlength {Phương pháp đo không được vượt quá 256 ký tự}}\r\n                         </span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <div class=\"center\">\r\n                <ghm-button classes=\"btn blue cm-mgr-5\" [loading]=\"isSaving\" i18n=\"@@update\">\r\n                    Cập nhật\r\n                </ghm-button>\r\n                <ghm-button type=\"button\" classes=\"btn btn-light\" nh-dismiss=\"true\" i18n=\"close\">\r\n                    Đóng\r\n                </ghm-button>\r\n            </div>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/task/target/target-libraries/target-library-form/target-library-form.component.ts":
/*!***********************************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-libraries/target-library-form/target-library-form.component.ts ***!
  \***********************************************************************************************************/
/*! exports provided: TargetLibraryFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetLibraryFormComponent", function() { return TargetLibraryFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _model_target_library_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../model/target-library.model */ "./src/app/modules/task/target/target-libraries/model/target-library.model.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_target_library_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/target-library.service */ "./src/app/modules/task/target/target-libraries/services/target-library.service.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");









var TargetLibraryFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TargetLibraryFormComponent, _super);
    function TargetLibraryFormComponent(fb, utilService, targetLibraryService) {
        var _this = _super.call(this) || this;
        _this.fb = fb;
        _this.utilService = utilService;
        _this.targetLibraryService = targetLibraryService;
        _this.targetLibrary = new _model_target_library_model__WEBPACK_IMPORTED_MODULE_4__["TargetLibrary"]();
        return _this;
    }
    TargetLibraryFormComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    TargetLibraryFormComponent.prototype.onModalHidden = function () {
        this.isUpdate = false;
        this.targetLibrary = new _model_target_library_model__WEBPACK_IMPORTED_MODULE_4__["TargetLibrary"]();
        this.model.reset(new _model_target_library_model__WEBPACK_IMPORTED_MODULE_4__["TargetLibrary"]());
        this.clearFormError(this.formErrors);
    };
    TargetLibraryFormComponent.prototype.update = function (item, targetGroup) {
        this.id = item.id;
        this.isUpdate = true;
        this.utilService.focusElement('name');
        this.model.patchValue(item);
        this.model.patchValue({ targetGroupLibraryId: targetGroup.id, targetGroupLibraryName: targetGroup.name });
        this.targetLibraryModal.open();
    };
    TargetLibraryFormComponent.prototype.add = function (targetGroup) {
        this.isUpdate = false;
        this.utilService.focusElement('name');
        this.model.patchValue({ targetGroupLibraryId: targetGroup.id, targetGroupLibraryName: targetGroup.name });
        this.targetLibraryModal.open();
    };
    TargetLibraryFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            this.targetLibrary = this.model.value;
            if (this.isUpdate) {
                this.isSaving = true;
                this.targetLibraryService.update(this.id, this.targetLibrary).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.isModified = true;
                    _this.targetLibrary.id = _this.id;
                    _this.targetLibrary.concurrencyStamp = result.data;
                    _this.saveSuccessful.emit(_this.targetLibrary);
                    _this.targetLibraryModal.dismiss();
                });
            }
            else {
                this.isSaving = true;
                this.targetLibraryService.insert(this.targetLibrary).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.isModified = true;
                    _this.targetLibrary.id = result.data;
                    _this.targetLibrary.concurrencyStamp = result.data;
                    _this.saveSuccessful.emit(_this.targetLibrary);
                    _this.targetLibraryModal.dismiss();
                });
            }
        }
    };
    TargetLibraryFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.renderFormError(['name']);
        this.validationMessages = this.renderFormErrorMessage([
            { 'name': ['required', 'maxlength'] },
            { 'description': ['maxlength'] },
            { 'measurementMethod': ['maxlength'] },
            { 'targetGroupLibraryId': ['required', 'maxlength'] }
        ]);
        this.model = this.fb.group({
            name: [this.targetLibrary.name, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].maxLength(500)
                ]],
            description: [this.targetLibrary.description, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].maxLength(500)
                ]],
            measurementMethod: [this.targetLibrary.measurementMethod, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].maxLength(500)
                ]],
            targetGroupLibraryId: [this.targetLibrary.targetGroupLibraryId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required,
                ]],
            targetGroupLibraryName: [this.targetLibrary.targetGroupLibraryName],
            concurrencyStamp: [this.targetLibrary.concurrencyStamp]
        });
        this.subscribers.modelValueChanges = this.model.valueChanges.subscribe(function () { return _this.validateModel(false); });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('targetLibraryModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__["NhModalComponent"])
    ], TargetLibraryFormComponent.prototype, "targetLibraryModal", void 0);
    TargetLibraryFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-target-library-form',
            template: __webpack_require__(/*! ./target-library-form.component.html */ "./src/app/modules/task/target/target-libraries/target-library-form/target-library-form.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_8__["UtilService"],
            _services_target_library_service__WEBPACK_IMPORTED_MODULE_7__["TargetLibraryService"]])
    ], TargetLibraryFormComponent);
    return TargetLibraryFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/task/target/target-libraries/target-library-group-form/target-library-group-form.component.html":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-libraries/target-library-group-form/target-library-group-form.component.html ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #libraryGroupFormModal size=\"md\" (hidden)=\"onModalHidden()\">\r\n    <nh-modal-header>\r\n        {{ isUpdate ? 'Cập nhật nhóm' : 'Tạo nhóm'}}\r\n    </nh-modal-header>\r\n    <form class=\"form-horizontal\" class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content>\r\n            <div class=\"form-body\">\r\n                <div class=\"form-group\" [class.has-error]=\"formErrors?.name\">\r\n                    <label i18n-ghmLabel=\"@@groupName\" ghmLabel=\"Tên nhóm\" class=\"col-sm-4 ghm-label\" [required]=\"true\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <ghm-input formControlName=\"name\"\r\n                                   [elementId]=\"'name'\"\r\n                                   [placeholder]=\"'Nhập tên nhóm mục tiêu'\"\r\n                                   i18n-placeholder=\"@@enterTargetGroupName\">\r\n                        </ghm-input>\r\n                        <span class=\"help-block\">\r\n                          {formErrors?.name, select,\r\n                            required {Vui lòng nhập tên nhóm}\r\n                            maxlength{ Tên nhóm không được lớn hơn 256 ký tự}}\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <div class=\"center\">\r\n                <ghm-button classes=\"btn blue cm-mgr-5\" [loading]=\"isSaving\" i18n=\"@@update\">\r\n                    Cập nhật\r\n                </ghm-button>\r\n                <ghm-button type=\"button\" classes=\"btn btn-light\" nh-dismiss=\"true\">\r\n                    Đóng\r\n                </ghm-button>\r\n            </div>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/task/target/target-libraries/target-library-group-form/target-library-group-form.component.ts":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-libraries/target-library-group-form/target-library-group-form.component.ts ***!
  \***********************************************************************************************************************/
/*! exports provided: TargetLibraryGroupFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetLibraryGroupFormComponent", function() { return TargetLibraryGroupFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _model_target_group_library_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../model/target-group-library.model */ "./src/app/modules/task/target/target-libraries/model/target-group-library.model.ts");
/* harmony import */ var _services_target_group_library_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/target-group-library.service */ "./src/app/modules/task/target/target-libraries/services/target-group-library.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");









var TargetLibraryGroupFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TargetLibraryGroupFormComponent, _super);
    function TargetLibraryGroupFormComponent(fb, targetGroupLibraryService, utilService) {
        var _this = _super.call(this) || this;
        _this.fb = fb;
        _this.targetGroupLibraryService = targetGroupLibraryService;
        _this.utilService = utilService;
        _this.targetGroupLibrary = new _model_target_group_library_model__WEBPACK_IMPORTED_MODULE_5__["TargetGroupLibrary"]();
        return _this;
    }
    TargetLibraryGroupFormComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    TargetLibraryGroupFormComponent.prototype.onModalHidden = function () {
        this.isUpdate = false;
        this.model.reset(new _model_target_group_library_model__WEBPACK_IMPORTED_MODULE_5__["TargetGroupLibrary"]());
        this.targetGroupLibrary = new _model_target_group_library_model__WEBPACK_IMPORTED_MODULE_5__["TargetGroupLibrary"]();
        if (this.isModified) {
            this.saveSuccessful.emit();
        }
        this.clearFormError(this.formErrors);
    };
    TargetLibraryGroupFormComponent.prototype.update = function (item) {
        this.id = item.id;
        this.isUpdate = true;
        this.utilService.focusElement('name');
        this.model.patchValue(item);
        this.libraryGroupFormModal.open();
    };
    TargetLibraryGroupFormComponent.prototype.add = function () {
        this.isUpdate = false;
        this.utilService.focusElement('name');
        this.libraryGroupFormModal.open();
    };
    TargetLibraryGroupFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            this.targetGroupLibrary = this.model.value;
            if (this.isUpdate) {
                this.isSaving = true;
                this.targetGroupLibraryService.update(this.id, this.targetGroupLibrary).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.isModified = true;
                    _this.libraryGroupFormModal.dismiss();
                });
            }
            else {
                this.isSaving = true;
                this.targetGroupLibraryService.insert(this.targetGroupLibrary).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.isModified = true;
                    _this.libraryGroupFormModal.dismiss();
                });
            }
        }
    };
    TargetLibraryGroupFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.renderFormError(['name']);
        this.validationMessages = this.renderFormErrorMessage([
            { 'name': ['required', 'maxlength'] },
        ]);
        this.model = this.fb.group({
            name: [this.targetGroupLibrary.name, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(256)
                ]],
            concurrencyStamp: [this.targetGroupLibrary.concurrencyStamp]
        });
        this.subscribers.modelValueChanges = this.model.valueChanges.subscribe(function () { return _this.validateModel(false); });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('libraryGroupFormModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__["NhModalComponent"])
    ], TargetLibraryGroupFormComponent.prototype, "libraryGroupFormModal", void 0);
    TargetLibraryGroupFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-target-group-library-form',
            template: __webpack_require__(/*! ./target-library-group-form.component.html */ "./src/app/modules/task/target/target-libraries/target-library-group-form/target-library-group-form.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _services_target_group_library_service__WEBPACK_IMPORTED_MODULE_6__["TargetGroupLibraryService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_8__["UtilService"]])
    ], TargetLibraryGroupFormComponent);
    return TargetLibraryGroupFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/task/target/target-libraries/target-library.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-libraries/target-library.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\" [class.cm-mg-10]=\"isAllowSelect\">\r\n    <span i18n=\"@@listTargetTable\">Thư viện mục tiêu mẫu</span>\r\n</h1>\r\n<div class=\"tooltip-helper\" *ngIf=\"!isAllowSelect\">\r\n    <div class=\"header\" (click)=\"isShowHelper = !isShowHelper\">\r\n        <div class=\"icon\">\r\n            <i class=\"fa fa-caret-down\" aria-hidden=\"true\" *ngIf=\"isShowHelper\"></i>\r\n            <i class=\"fa fa-caret-right\" aria-hidden=\"true\" *ngIf=\"!isShowHelper\"></i>\r\n        </div>\r\n        <div>\r\n            <span>Thư viện mục tiêu lưu trữ mục tiêu mẫu cho phép nhân viên sử dụng khi tạo mục tiêu</span>\r\n        </div>\r\n    </div>\r\n    <div class=\"body\" *ngIf=\"isShowHelper\">\r\n            <span>Thư viện mục tiêu được phân nhóm theo các cấp:<br/>\r\n                1. Cấp đầu tiên là Nhóm mục tiêu mẫu<br/>\r\n                2. Cấp thứ hai là mục tiêu mẫu</span>\r\n    </div>\r\n</div>\r\n<div class=\"pull-left cm-mgb-5\" *ngIf=\"!isAllowSelect\">\r\n    <button (click)=\"addGroup()\" class=\"btn blue\" type=\"button\" *ngIf=\"permission.add\">Thêm Nhóm</button>\r\n</div>\r\n<div class=\"target-libraries\" *ngIf=\"listItems && listItems.length > 0; else notFound\" [ngStyle]=\"{'width': isAllowSelect ? 1000 + 'px': ''}\">\r\n    <mat-expansion-panel *ngFor=\"let item of listItems; let i = index\" class=\"lisTargetLibraries cm-mgb-15\">\r\n        <mat-expansion-panel-header class=\"header\">\r\n            <mat-panel-title class=\"title\">\r\n                {{ item.name }}\r\n            </mat-panel-title>\r\n        </mat-expansion-panel-header>\r\n        <div class=\"\">\r\n            <div class=\"cm-mgt-10 cm-mgb-10\" *ngIf=\"!isAllowSelect\">\r\n                <button type=\"button\" class=\"btn blue cm-mgr-5\" i18n=\"@@addTargetLibrary\"\r\n                        (click)=\"addTargetLibrary(item)\" *ngIf=\"permission.add\">Thêm mục tiêu mẫu\r\n                </button>\r\n                <button type=\"button\" class=\"btn blue cm-mgr-5\" *ngIf=\"permission.edit\"\r\n                        i18n=\"@@updateTargetLibraryGroup\" (click)=\"updateLibraryGroup(item)\">Sửa nhóm\r\n                </button>\r\n                <button type=\"button\" class=\"btn blue cm-mgr-5\" *ngIf=\"permission.delete\"\r\n                        i18n=\"@deleteTargetLibraryGroup\" (confirm)=\"deleteLibraryGroup(item.id)\"\r\n                        [swal]=\"deleteTargetGroupLibrary\">\r\n                    Xóa nhóm\r\n                </button>\r\n            </div>\r\n            <div class=\"cm-pdt-10\" *ngIf=\"item.targetLibrary && item.targetLibrary.length > 0\">\r\n                <table class=\"table table-striped table-bordered table-hover cm-mgb-10\">\r\n                    <thead>\r\n                    <tr>\r\n                        <th class=\" middle\" i18n=\"@@targetSample\">Mục tiêu mẫu</th>\r\n                        <th class=\" middle\" i18n=\"@@description\">Mô tả</th>\r\n                        <th class=\"middle\" i18n=\"@@roleNotification\">Phương pháp đo\r\n                        <th class=\"w100 center middle\" i18n=\"@@function\" *ngIf=\"(permission.edit || permission.delete) && !isAllowSelect\">\r\n                            Chức năng\r\n                        </th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                    <tr *ngFor=\"let item2 of item.targetLibrary; let i2 = index\">\r\n                        <td class=\"middle\"[class.cursor-pointer]=\"isAllowSelect\"\r\n                            (click)=\"isAllowSelect ? selectTargetLibrary(item2) : ''\">\r\n                            <i class=\"fa fa-circle-thin font-size-18 cm-pdr-5 color-light-brown cm-pdt-5\"\r\n                               *ngIf=\"isAllowSelect\"></i>\r\n                            <img src=\"../../../../../assets/images/icon/icon-target.png\" height=\"16\" width=\"16\"/>\r\n                            {{item2.name}}\r\n                        </td>\r\n                        <td class=\"middle\">{{item2.description}}</td>\r\n                        <td class=\"middle\">\r\n                            <img src=\"../../../../../assets/images/icon/icon-chart.gif\" height=\"16\" width=\"16\"\r\n                                 *ngIf=\"item2.measurementMethod\"/>\r\n                            {{item2.measurementMethod}}\r\n                        </td>\r\n                        <td class=\"center middle\" *ngIf=\"(permission.edit || permission.delete) && !isAllowSelect\">\r\n                            <a (click)=\"updateTargetLibrary(item2, item)\" i18n=\"@@updateTargetLibrary\"\r\n                               *ngIf=\"permission.edit\" data-icon=\"update-16\"\r\n                               class=\"cm-mgr-5\">\r\n                            </a>\r\n                            <a i18n=\"@@deleteTargetLibrary\"\r\n                               *ngIf=\"permission.delete\"\r\n                               data-icon=\"delete-16\"\r\n                               (confirm)=\"deleteTargetLibrary(item2.id, item)\" [swal]=\"swalDeleteTargetLibrary\">\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                    </tbody>\r\n                </table>\r\n            </div>\r\n        </div>\r\n    </mat-expansion-panel>\r\n</div>\r\n\r\n<div class=\"center cm-mgb-10\" *ngIf=\"isAllowSelect\">\r\n    <button class=\"btn btn-light\" (click)=\"closeModal()\">Đóng</button>\r\n</div>\r\n\r\n<ng-template #notFound>\r\n    <div class=\"box-confirm bg-info-blue\">\r\n        <i class=\"fa fa-info-circle color-dark-blue font-size-18\"\r\n           aria-hidden=\"true\"></i><span> Không có dữ liệu</span>\r\n    </div>\r\n</ng-template>\r\n\r\n<app-target-library-form (saveSuccessful)=\"onSaveSuccess($event)\"></app-target-library-form>\r\n<app-target-group-library-form (saveSuccessful)=\"search()\"></app-target-group-library-form>\r\n\r\n<swal\r\n    i18n=\"@@confirmDeleteRole\"\r\n    i18n-title\r\n    i18n-text\r\n    #swalDeleteTargetLibrary\r\n    title=\"Bạn có muốn xóa mục tiêu mẫu này này?\"\r\n    text=\"Bạn không thể không phục mục tiêu này sau khi xóa.\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n<swal\r\n    i18n=\"@@confirmDeleteRole\"\r\n    i18n-title\r\n    i18n-text\r\n    #deleteTargetGroupLibrary\r\n    title=\"Bạn có muốn xóa nhóm mục tiêu mẫu này?\"\r\n    text=\"Bạn không thể khôi phục nhóm mục tiêu này sau khi xóa.\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n"

/***/ }),

/***/ "./src/app/modules/task/target/target-libraries/target-library.component.scss":
/*!************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-libraries/target-library.component.scss ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "::ng-deep .mat-expansion-indicator:after {\n  color: #fff !important; }\n\n.target-libraries {\n  min-height: 32px;\n  background-color: #f5f7f7;\n  padding: 12px;\n  margin-bottom: 12px;\n  position: relative;\n  float: left;\n  width: 100%;\n  border-radius: 4px !important; }\n\n.target-libraries .mat-expansion-panel .mat-expansion-panel-header {\n    padding: 0px 15px 0px 7px !important;\n    border-radius: 0px !important;\n    background: #45A2D2; }\n\n.target-libraries .mat-expansion-panel .mat-expansion-panel-header:hover {\n      background: #45A2D2; }\n\n.target-libraries .mat-expansion-panel .mat-expansion-panel-header .title {\n      font-size: 16px;\n      color: white;\n      font-weight: 500; }\n\n.target-libraries .mat-expansion-panel .mat-expansion-panel-body {\n    padding: 0 10px;\n    background-color: #f5f7f7 !important; }\n\n.target-libraries .mat-expansion-panel .mat-expansion-body {\n    margin-left: -15px;\n    margin-right: -15px;\n    margin-bottom: -16px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3RhcmdldC90YXJnZXQtbGlicmFyaWVzL0Q6XFxQcm9qZWN0XFxHaG1BcHBsaWNhdGlvblxcY2xpZW50c1xcZ2htYXBwbGljYXRpb25jbGllbnQvc3JjXFxhcHBcXG1vZHVsZXNcXHRhc2tcXHRhcmdldFxcdGFyZ2V0LWxpYnJhcmllc1xcdGFyZ2V0LWxpYnJhcnkuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21vZHVsZXMvdGFzay90YXJnZXQvdGFyZ2V0LWxpYnJhcmllcy9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXNzZXRzXFxzdHlsZXNcXF9jb25maWcuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUVRLHNCQUF3QixFQUFBOztBQUloQztFQUNJLGdCQUFnQjtFQUNoQix5QkMwQmdCO0VEekJoQixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsV0FBVztFQUNYLDZCQUE2QixFQUFBOztBQVJqQztJQVlZLG9DQUFvQztJQUNwQyw2QkFBNkI7SUFDN0IsbUJDWU0sRUFBQTs7QUQxQmxCO01BZ0JnQixtQkNVRSxFQUFBOztBRDFCbEI7TUFvQmdCLGVBQWU7TUFDZixZQUFZO01BQ1osZ0JBQWdCLEVBQUE7O0FBdEJoQztJQTJCWSxlQUFlO0lBQ2Ysb0NBQW9DLEVBQUE7O0FBNUJoRDtJQStCWSxrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLG9CQUFvQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvbW9kdWxlcy90YXNrL3RhcmdldC90YXJnZXQtbGlicmFyaWVzL3RhcmdldC1saWJyYXJ5LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uLy4uLy4uLy4uLy4uL2Fzc2V0cy9zdHlsZXMvY29uZmlnXCI7XHJcblxyXG46Om5nLWRlZXAgLm1hdC1leHBhbnNpb24taW5kaWNhdG9yIHtcclxuICAgICY6YWZ0ZXIge1xyXG4gICAgICAgIGNvbG9yOiAkd2hpdGUgIWltcG9ydGFudDtcclxuICAgIH1cclxufVxyXG5cclxuLnRhcmdldC1saWJyYXJpZXMge1xyXG4gICAgbWluLWhlaWdodDogMzJweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRsaWdodC1ibHVlO1xyXG4gICAgcGFkZGluZzogMTJweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDEycHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4ICFpbXBvcnRhbnQ7XHJcblxyXG4gICAgLm1hdC1leHBhbnNpb24tcGFuZWwge1xyXG4gICAgICAgIC5tYXQtZXhwYW5zaW9uLXBhbmVsLWhlYWRlciB7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxNXB4IDBweCA3cHggIWltcG9ydGFudDtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICRkYXJrQmx1ZTtcclxuICAgICAgICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAkZGFya0JsdWU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC50aXRsZSB7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAubWF0LWV4cGFuc2lvbi1wYW5lbC1ib2R5IHtcclxuICAgICAgICAgICAgcGFkZGluZzogMCAxMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjVmN2Y3ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5tYXQtZXhwYW5zaW9uLWJvZHkge1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLTE1cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogLTE1cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IC0xNnB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuXHJcbiIsIiRkZWZhdWx0LWNvbG9yOiAjMjIyO1xyXG4kZm9udC1mYW1pbHk6IFwiQXJpYWxcIiwgdGFob21hLCBIZWx2ZXRpY2EgTmV1ZTtcclxuJGNvbG9yLWJsdWU6ICMzNTk4ZGM7XHJcbiRtYWluLWNvbG9yOiAjMDA3NDU1O1xyXG4kYm9yZGVyQ29sb3I6ICNkZGQ7XHJcbiRzZWNvbmQtY29sb3I6ICNiMDFhMWY7XHJcbiR0YWJsZS1iYWNrZ3JvdW5kLWNvbG9yOiAjMDA5Njg4O1xyXG4kYmx1ZTogIzAwN2JmZjtcclxuJGRhcmstYmx1ZTogIzAwNzJCQztcclxuJGJyaWdodC1ibHVlOiAjZGZmMGZkO1xyXG4kaW5kaWdvOiAjNjYxMGYyO1xyXG4kcHVycGxlOiAjNmY0MmMxO1xyXG4kcGluazogI2U4M2U4YztcclxuJHJlZDogI2RjMzU0NTtcclxuJG9yYW5nZTogI2ZkN2UxNDtcclxuJHllbGxvdzogI2ZmYzEwNztcclxuJGdyZWVuOiAjMjhhNzQ1O1xyXG4kdGVhbDogIzIwYzk5NztcclxuJGN5YW46ICMxN2EyYjg7XHJcbiR3aGl0ZTogI2ZmZjtcclxuJGdyYXk6ICM4NjhlOTY7XHJcbiRncmF5LWRhcms6ICMzNDNhNDA7XHJcbiRwcmltYXJ5OiAjMDA3YmZmO1xyXG4kc2Vjb25kYXJ5OiAjNmM3NTdkO1xyXG4kc3VjY2VzczogIzI4YTc0NTtcclxuJGluZm86ICMxN2EyYjg7XHJcbiR3YXJuaW5nOiAjZmZjMTA3O1xyXG4kZGFuZ2VyOiAjZGMzNTQ1O1xyXG4kbGlnaHQ6ICNmOGY5ZmE7XHJcbiRkYXJrOiAjMzQzYTQwO1xyXG4kbGFiZWwtY29sb3I6ICM2NjY7XHJcbiRiYWNrZ3JvdW5kLWNvbG9yOiAjRUNGMEYxO1xyXG4kYm9yZGVyQWN0aXZlQ29sb3I6ICM4MGJkZmY7XHJcbiRib3JkZXJSYWRpdXM6IDA7XHJcbiRkYXJrQmx1ZTogIzQ1QTJEMjtcclxuJGxpZ2h0R3JlZW46ICMyN2FlNjA7XHJcbiRsaWdodC1ibHVlOiAjZjVmN2Y3O1xyXG4kYnJpZ2h0R3JheTogIzc1NzU3NTtcclxuJG1heC13aWR0aC1tb2JpbGU6IDc2OHB4O1xyXG4kbWF4LXdpZHRoLXRhYmxldDogOTkycHg7XHJcbiRtYXgtd2lkdGgtZGVza3RvcDogMTI4MHB4O1xyXG5cclxuLy8gQkVHSU46IE1hcmdpblxyXG5AbWl4aW4gbmgtbWcoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW46ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWd0KCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLXRvcDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ2IoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1nbCgkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1ncigkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1yaWdodDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogTWFyZ2luXHJcblxyXG4vLyBCRUdJTjogUGFkZGluZ1xyXG5AbWl4aW4gbmgtcGQoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkdCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctdG9wOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkYigkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctYm90dG9tOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkbCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctbGVmdDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZHIoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBQYWRkaW5nXHJcblxyXG4vLyBCRUdJTjogV2lkdGhcclxuQG1peGluIG5oLXdpZHRoKCR3aWR0aCkge1xyXG4gICAgd2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG4gICAgbWluLXdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxuICAgIG1heC13aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogV2lkdGhcclxuXHJcbi8vIEJFR0lOOiBJY29uIFNpemVcclxuQG1peGluIG5oLXNpemUtaWNvbigkc2l6ZSkge1xyXG4gICAgd2lkdGg6ICRzaXplO1xyXG4gICAgaGVpZ2h0OiAkc2l6ZTtcclxuICAgIGZvbnQtc2l6ZTogJHNpemU7XHJcbn1cclxuXHJcbi8vIEVORDogSWNvbiBTaXplXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/modules/task/target/target-libraries/target-library.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/modules/task/target/target-libraries/target-library.component.ts ***!
  \**********************************************************************************/
/*! exports provided: TargetLibraryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetLibraryComponent", function() { return TargetLibraryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _target_library_group_form_target_library_group_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./target-library-group-form/target-library-group-form.component */ "./src/app/modules/task/target/target-libraries/target-library-group-form/target-library-group-form.component.ts");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_target_group_library_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./services/target-group-library.service */ "./src/app/modules/task/target/target-libraries/services/target-group-library.service.ts");
/* harmony import */ var _services_target_library_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/target-library.service */ "./src/app/modules/task/target/target-libraries/services/target-library.service.ts");
/* harmony import */ var _target_library_form_target_library_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./target-library-form/target-library-form.component */ "./src/app/modules/task/target/target-libraries/target-library-form/target-library-form.component.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");










var TargetLibraryComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TargetLibraryComponent, _super);
    function TargetLibraryComponent(data, dialog, route, targetGroupLibraryService, targetLibraryService) {
        var _this = _super.call(this) || this;
        _this.data = data;
        _this.dialog = dialog;
        _this.route = route;
        _this.targetGroupLibraryService = targetGroupLibraryService;
        _this.targetLibraryService = targetLibraryService;
        _this.isShowHelper = false;
        return _this;
    }
    TargetLibraryComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.data && this.data.isSelect) {
            this.isAllowSelect = this.data.isSelect;
            this.search();
        }
        else {
            this.appService.setupPage(this.pageId.TARGET, this.pageId.TARGET_LIBRARY, 'Danh sách mục tiêu', 'Quản lý mục tiêu');
            this.subscribers.data = this.route.data.subscribe(function (result) {
                _this.listItems = result.data.items;
            });
        }
    };
    TargetLibraryComponent.prototype.search = function () {
        var _this = this;
        this.targetGroupLibraryService.search().subscribe(function (result) {
            _this.listItems = result.items;
        });
    };
    TargetLibraryComponent.prototype.addGroup = function () {
        this.libraryGroupFormComponent.add();
    };
    TargetLibraryComponent.prototype.updateLibraryGroup = function (item) {
        this.libraryGroupFormComponent.update(item);
    };
    TargetLibraryComponent.prototype.deleteLibraryGroup = function (id) {
        var _this = this;
        this.targetGroupLibraryService.delete(id).subscribe(function (result) {
            lodash__WEBPACK_IMPORTED_MODULE_8__["remove"](_this.listItems, function (item) {
                return item.id === id;
            });
        });
    };
    TargetLibraryComponent.prototype.addTargetLibrary = function (targetGroup) {
        this.targetLibraryFormComponent.add(targetGroup);
    };
    TargetLibraryComponent.prototype.updateTargetLibrary = function (targetLibrary, targetGroup) {
        this.targetLibraryFormComponent.update(targetLibrary, targetGroup);
    };
    TargetLibraryComponent.prototype.deleteTargetLibrary = function (id, targetGroupLibrary) {
        this.targetLibraryService.delete(id).subscribe(function (result) {
            lodash__WEBPACK_IMPORTED_MODULE_8__["remove"](targetGroupLibrary.targetLibrary, function (target) {
                return target.id === id;
            });
        });
    };
    TargetLibraryComponent.prototype.onSaveSuccess = function (value) {
        if (value) {
            var targetGroup = lodash__WEBPACK_IMPORTED_MODULE_8__["find"](this.listItems, function (item) {
                return item.id === value.targetGroupLibraryId;
            });
            if (targetGroup != null) {
                var targetLibraryInfo = lodash__WEBPACK_IMPORTED_MODULE_8__["find"](targetGroup.targetLibrary, function (targetLibrary) {
                    return targetLibrary.id === value.id;
                });
                if (!targetLibraryInfo) {
                    if (targetGroup.targetLibrary) {
                        targetGroup.targetLibrary.push(value);
                    }
                    else {
                        targetGroup.targetLibrary = [];
                        targetGroup.targetLibrary.push(value);
                    }
                }
                else {
                    targetLibraryInfo.name = value.name;
                    targetLibraryInfo.description = value.description;
                    targetLibraryInfo.concurrencyStamp = value.concurrencyStamp;
                    targetLibraryInfo.measurementMethod = value.measurementMethod;
                }
            }
        }
    };
    TargetLibraryComponent.prototype.closeModal = function () {
        var dialogTargetLibrary = this.dialog.getDialogById('targetLibrary');
        if (dialogTargetLibrary) {
            dialogTargetLibrary.close({ targetLibrary: null });
        }
    };
    TargetLibraryComponent.prototype.selectTargetLibrary = function (target) {
        var dialogTaskLibrary = this.dialog.getDialogById('targetLibrary');
        if (dialogTaskLibrary) {
            dialogTaskLibrary.close({ targetLibrary: target });
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_target_library_group_form_target_library_group_form_component__WEBPACK_IMPORTED_MODULE_2__["TargetLibraryGroupFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _target_library_group_form_target_library_group_form_component__WEBPACK_IMPORTED_MODULE_2__["TargetLibraryGroupFormComponent"])
    ], TargetLibraryComponent.prototype, "libraryGroupFormComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_target_library_form_target_library_form_component__WEBPACK_IMPORTED_MODULE_7__["TargetLibraryFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _target_library_form_target_library_form_component__WEBPACK_IMPORTED_MODULE_7__["TargetLibraryFormComponent"])
    ], TargetLibraryComponent.prototype, "targetLibraryFormComponent", void 0);
    TargetLibraryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-target-library',
            template: __webpack_require__(/*! ./target-library.component.html */ "./src/app/modules/task/target/target-libraries/target-library.component.html"),
            styles: [__webpack_require__(/*! ./target-library.component.scss */ "./src/app/modules/task/target/target-libraries/target-library.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Optional"])()), tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_9__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatDialog"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _services_target_group_library_service__WEBPACK_IMPORTED_MODULE_5__["TargetGroupLibraryService"],
            _services_target_library_service__WEBPACK_IMPORTED_MODULE_6__["TargetLibraryService"]])
    ], TargetLibraryComponent);
    return TargetLibraryComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_3__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/task/target/target-routing.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/modules/task/target/target-routing.module.ts ***!
  \**************************************************************/
/*! exports provided: userRoutes, TargetRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userRoutes", function() { return userRoutes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetRoutingModule", function() { return TargetRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _target_table_target_table_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./target-table/target-table.component */ "./src/app/modules/task/target/target-table/target-table.component.ts");
/* harmony import */ var _target_table_service_target_table_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./target-table/service/target-table.service */ "./src/app/modules/task/target/target-table/service/target-table.service.ts");
/* harmony import */ var _target_libraries_target_library_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./target-libraries/target-library.component */ "./src/app/modules/task/target/target-libraries/target-library.component.ts");
/* harmony import */ var _target_libraries_services_target_group_library_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./target-libraries/services/target-group-library.service */ "./src/app/modules/task/target/target-libraries/services/target-group-library.service.ts");
/* harmony import */ var _target_table_target_table_detail_target_table_detail_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./target-table/target-table-detail/target-table-detail.component */ "./src/app/modules/task/target/target-table/target-table-detail/target-table-detail.component.ts");
/* harmony import */ var _target_table_target_table_detail_target_table_detail_resolve__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./target-table/target-table-detail/target-table-detail.resolve */ "./src/app/modules/task/target/target-table/target-table-detail/target-table-detail.resolve.ts");
/* harmony import */ var _targets_targets_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./targets/targets.component */ "./src/app/modules/task/target/targets/targets.component.ts");
/* harmony import */ var _targets_services_target_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./targets/services/target.service */ "./src/app/modules/task/target/targets/services/target.service.ts");
/* harmony import */ var _targets_target_report_target_report_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./targets/target-report/target-report.component */ "./src/app/modules/task/target/targets/target-report/target-report.component.ts");












var userRoutes = [
    {
        path: 'target-table',
        component: _target_table_target_table_component__WEBPACK_IMPORTED_MODULE_3__["TargetTableComponent"],
        resolve: {
            data: _target_table_service_target_table_service__WEBPACK_IMPORTED_MODULE_4__["TargetTableService"]
        },
    },
    {
        path: 'target-table/:id',
        component: _target_table_target_table_detail_target_table_detail_component__WEBPACK_IMPORTED_MODULE_7__["TargetTableDetailComponent"],
        resolve: {
            data: _target_table_target_table_detail_target_table_detail_resolve__WEBPACK_IMPORTED_MODULE_8__["TargetTableDetailResolve"]
        }
    },
    {
        path: 'target-library',
        component: _target_libraries_target_library_component__WEBPACK_IMPORTED_MODULE_5__["TargetLibraryComponent"],
        resolve: {
            data: _target_libraries_services_target_group_library_service__WEBPACK_IMPORTED_MODULE_6__["TargetGroupLibraryService"]
        }
    },
    {
        path: 'offices',
        component: _targets_targets_component__WEBPACK_IMPORTED_MODULE_9__["TargetsComponent"],
        resolve: {
            data: _targets_services_target_service__WEBPACK_IMPORTED_MODULE_10__["TargetService"]
        }
    },
    {
        path: 'personals',
        component: _targets_targets_component__WEBPACK_IMPORTED_MODULE_9__["TargetsComponent"],
        resolve: {
            data: _targets_services_target_service__WEBPACK_IMPORTED_MODULE_10__["TargetService"]
        }
    },
    {
        path: 'reports',
        component: _targets_target_report_target_report_component__WEBPACK_IMPORTED_MODULE_11__["TargetReportComponent"],
        resolve: {
            data: _targets_services_target_service__WEBPACK_IMPORTED_MODULE_10__["TargetService"]
        }
    }
];
var TargetRoutingModule = /** @class */ (function () {
    function TargetRoutingModule() {
    }
    TargetRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(userRoutes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
            providers: [_target_table_service_target_table_service__WEBPACK_IMPORTED_MODULE_4__["TargetTableService"], _target_libraries_services_target_group_library_service__WEBPACK_IMPORTED_MODULE_6__["TargetGroupLibraryService"], _target_table_target_table_detail_target_table_detail_resolve__WEBPACK_IMPORTED_MODULE_8__["TargetTableDetailResolve"]]
        })
    ], TargetRoutingModule);
    return TargetRoutingModule;
}());



/***/ }),

/***/ "./src/app/modules/task/target/target-table/model/target-group.model.ts":
/*!******************************************************************************!*\
  !*** ./src/app/modules/task/target/target-table/model/target-group.model.ts ***!
  \******************************************************************************/
/*! exports provided: TargetGroup */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetGroup", function() { return TargetGroup; });
var TargetGroup = /** @class */ (function () {
    function TargetGroup(name, color, description, concurrencyStamp) {
        this.name = name;
        this.color = color;
        this.description = description;
        this.concurrencyStamp = concurrencyStamp;
    }
    return TargetGroup;
}());



/***/ }),

/***/ "./src/app/modules/task/target/target-table/model/target-table.model.ts":
/*!******************************************************************************!*\
  !*** ./src/app/modules/task/target/target-table/model/target-table.model.ts ***!
  \******************************************************************************/
/*! exports provided: TargetTable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetTable", function() { return TargetTable; });
/* harmony import */ var _target_table_const__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../target-table.const */ "./src/app/modules/task/target/target-table/target-table.const.ts");

var TargetTable = /** @class */ (function () {
    function TargetTable(name, description, startDate, endDate, type, concurrencyStamp) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.type = _target_table_const__WEBPACK_IMPORTED_MODULE_0__["TargetTableType"].bsc;
        this.concurrencyStamp = concurrencyStamp;
    }
    return TargetTable;
}());



/***/ }),

/***/ "./src/app/modules/task/target/target-table/service/target-table.service.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/modules/task/target/target-table/service/target-table.service.ts ***!
  \**********************************************************************************/
/*! exports provided: TargetTableService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetTableService", function() { return TargetTableService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../../environments/environment */ "./src/environments/environment.ts");








var TargetTableService = /** @class */ (function () {
    function TargetTableService(appConfig, spinceService, http, spinnerService, toastr) {
        this.appConfig = appConfig;
        this.spinceService = spinceService;
        this.http = http;
        this.spinnerService = spinnerService;
        this.toastr = toastr;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].apiGatewayUrl + "api/v1/task/target-tables";
    }
    TargetTableService.prototype.resolve = function (route, state) {
        return this.search();
    };
    TargetTableService.prototype.search = function () {
        return this.http.get("" + this.url, {});
    };
    TargetTableService.prototype.insert = function (targetTable) {
        var _this = this;
        return this.http.post("" + this.url, targetTable).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    TargetTableService.prototype.update = function (id, targetTable) {
        var _this = this;
        return this.http.post(this.url + "/" + id, targetTable).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    TargetTableService.prototype.delete = function (id) {
        var _this = this;
        return this.http.delete(this.url + "/" + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    TargetTableService.prototype.updateStatus = function (id, status) {
        var _this = this;
        return this.http.post(this.url + "/" + id + "/status/" + status, {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    TargetTableService.prototype.updateOrder = function (id, order) {
        var _this = this;
        return this.http.post(this.url + "/" + id + "/order/" + order, {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    TargetTableService.prototype.getDetail = function (id) {
        return this.http.get(this.url + "/" + id + "/detail", {})
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            return result;
        }));
    };
    TargetTableService.prototype.insertTargetGroup = function (targetTableId, targetGroup) {
        var _this = this;
        return this.http.post(this.url + "/" + targetTableId + "/groups", targetGroup).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    TargetTableService.prototype.updateTargetGroup = function (targetTableId, targetGroupId, targetGroup) {
        var _this = this;
        return this.http.post(this.url + "/" + targetTableId + "/groups/" + targetGroupId, targetGroup)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    TargetTableService.prototype.deleteTargetGroup = function (targetTableId, targetGroupId) {
        var _this = this;
        return this.http.delete(this.url + "/" + targetTableId + "/groups/" + targetGroupId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    TargetTableService.prototype.updateTargetGroupOrder = function (targetTableId, targetGroupId, order) {
        var _this = this;
        return this.http.post(this.url + "/" + targetTableId + "/groups/" + targetGroupId + "/order/" + order, {})
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    TargetTableService.prototype.updateTargetGroupColor = function (targetTableId, targetGroupId, color) {
        var _this = this;
        return this.http.post(this.url + "/" + targetTableId + "/groups/" + targetGroupId + "/color", {}, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]()
                .set('color', color)
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    TargetTableService.prototype.searchForSuggestion = function () {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/search-for-suggestion")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            return result.items;
        }));
    };
    TargetTableService.prototype.searchTargetGroupByTargetTableId = function (targetTableId) {
        return this.http.get(this.url + "/" + targetTableId + "/target-groups").pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            return result.items;
        }));
    };
    TargetTableService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_6__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_3__["SpinnerService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_3__["SpinnerService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]])
    ], TargetTableService);
    return TargetTableService;
}());



/***/ }),

/***/ "./src/app/modules/task/target/target-table/target-group-form/target-group-form.component.html":
/*!*****************************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-table/target-group-form/target-group-form.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #targetGroupFormModal size=\"md\" (hidden)=\"onModalHidden()\">\r\n    <nh-modal-header>\r\n        {{ isUpdate ? 'Cập nhật nhóm mục tiêu' : 'Tạo nhóm mục tiêu'}}\r\n    </nh-modal-header>\r\n    <form class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content>\r\n            <div class=\"form-body\">\r\n                <div class=\"form-group\" [class.has-error]=\"formErrors?.name\">\r\n                    <label i18n-ghmLabel=\"@@groupName\" ghmLabel=\"Tên nhóm mục tiêu\" class=\"col-sm-4 ghm-label\"\r\n                           [required]=\"true\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <ghm-input formControlName=\"name\"\r\n                                   [elementId]=\"'name'\"\r\n                                   [placeholder]=\"'Nhập tên nhóm mục tiêu'\"\r\n                                   i18n-placeholder=\"@@enterTargetGroupName\">\r\n                        </ghm-input>\r\n                        <span class=\"help-block\">\r\n                          {formErrors?.name, select,\r\n                            required {Vui lòng nhập tên nhóm mục tiêu}\r\n                            maxlength{ Tên nhóm mục tiêu không được lớn hơn 256 ký tự}}\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <div class=\"center\">\r\n                <ghm-button classes=\"btn blue cm-mgr-5\" [loading]=\"isSaving\" i18n=\"@@update\">\r\n                    Cập nhật\r\n                </ghm-button>\r\n                <ghm-button type=\"button\" classes=\"btn btn-light\" nh-dismiss=\"true\">\r\n                    Đóng\r\n                </ghm-button>\r\n            </div>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/task/target/target-table/target-group-form/target-group-form.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-table/target-group-form/target-group-form.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: TargetGroupFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetGroupFormComponent", function() { return TargetGroupFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _service_target_table_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/target-table.service */ "./src/app/modules/task/target/target-table/service/target-table.service.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _model_target_group_model__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../model/target-group.model */ "./src/app/modules/task/target/target-table/model/target-group.model.ts");
/* harmony import */ var _target_table_const__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../target-table.const */ "./src/app/modules/task/target/target-table/target-table.const.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_10__);











var TargetGroupFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TargetGroupFormComponent, _super);
    function TargetGroupFormComponent(utilService, fb, targetTableService) {
        var _this = _super.call(this) || this;
        _this.utilService = utilService;
        _this.fb = fb;
        _this.targetTableService = targetTableService;
        _this.listTargetGroup = [];
        _this.targetGroup = new _model_target_group_model__WEBPACK_IMPORTED_MODULE_8__["TargetGroup"]();
        _this.listColorTargetGroup = _target_table_const__WEBPACK_IMPORTED_MODULE_9__["ListColorTargetGroup"];
        return _this;
    }
    TargetGroupFormComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    TargetGroupFormComponent.prototype.onModalHidden = function () {
        this.isUpdate = false;
        this.model.reset(new _model_target_group_model__WEBPACK_IMPORTED_MODULE_8__["TargetGroup"]());
        this.targetGroup = new _model_target_group_model__WEBPACK_IMPORTED_MODULE_8__["TargetGroup"]();
        this.clearFormError(this.formErrors);
    };
    TargetGroupFormComponent.prototype.update = function (item) {
        this.id = item.id;
        this.isUpdate = true;
        this.utilService.focusElement('name');
        this.model.patchValue(item);
        this.targetGroupFormModal.open();
    };
    TargetGroupFormComponent.prototype.add = function () {
        this.isUpdate = false;
        this.utilService.focusElement('name');
        this.targetGroupFormModal.open();
    };
    TargetGroupFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            this.targetGroup = this.model.value;
            if (this.isUpdate) {
                this.isSaving = true;
                this.targetTableService.updateTargetGroup(this.targetTableId, this.id, this.targetGroup)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.isModified = true;
                    _this.targetGroup.concurrencyStamp = result.data;
                    _this.targetGroup.id = _this.id;
                    _this.saveSuccessful.emit(_this.targetGroup);
                    _this.targetGroupFormModal.dismiss();
                });
            }
            else {
                this.isSaving = true;
                var colorOrder_1 = (this.listTargetGroup.length) % 8;
                var colorTargetGroup = lodash__WEBPACK_IMPORTED_MODULE_10__["find"](this.listColorTargetGroup, function (color) {
                    return color.id === colorOrder_1;
                });
                if (colorTargetGroup) {
                    this.model.patchValue({ color: colorTargetGroup.color });
                    this.targetGroup.color = colorTargetGroup.color;
                }
                this.targetTableService.insertTargetGroup(this.targetTableId, this.targetGroup).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.isModified = true;
                    _this.targetGroup.id = result.data;
                    _this.targetGroup.concurrencyStamp = result.data;
                    _this.saveSuccessful.emit(_this.targetGroup);
                    _this.targetGroupFormModal.dismiss();
                });
            }
        }
    };
    TargetGroupFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.renderFormError(['name']);
        this.validationMessages = this.renderFormErrorMessage([
            { 'name': ['required', 'maxlength'] },
        ]);
        this.model = this.fb.group({
            name: [this.targetGroup.name, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].maxLength(256)
                ]],
            color: [this.targetGroup.color],
            concurrencyStamp: [this.targetGroup.concurrencyStamp]
        });
        this.model.valueChanges.subscribe(function (data) { return _this.validateModel(false); });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('targetGroupFormModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_2__["NhModalComponent"])
    ], TargetGroupFormComponent.prototype, "targetGroupFormModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], TargetGroupFormComponent.prototype, "targetTableId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], TargetGroupFormComponent.prototype, "listTargetGroup", void 0);
    TargetGroupFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-target-group-form',
            template: __webpack_require__(/*! ./target-group-form.component.html */ "./src/app/modules/task/target/target-table/target-group-form/target-group-form.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shareds_services_util_service__WEBPACK_IMPORTED_MODULE_5__["UtilService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
            _service_target_table_service__WEBPACK_IMPORTED_MODULE_4__["TargetTableService"]])
    ], TargetGroupFormComponent);
    return TargetGroupFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_3__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/task/target/target-table/target-table-detail/target-table-detail.component.html":
/*!*********************************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-table/target-table-detail/target-table-detail.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span i18n=\"@@targetTableDetail\">Chi tiết bảng mục tiêu</span>\r\n</h1>\r\n<div class=\"box-confirm bg-warning\" *ngIf=\"targetTableDetail?.status !== targetTableStatus.using\">\r\n    <p>\r\n        <i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> Bảng mục tiêu đang ở trạng thái\r\n        <b>{{targetTableDetail?.status === targetTableStatus.stopUsing ? 'Không sử dụng' : 'Đang xây dựng'}}</b>\r\n    </p>\r\n</div>\r\n<div class=\"pull-left cm-mgb-10\">\r\n    <a href=\"javascript://\" routerLink=\"/targets/target-table\" class=\"btn blue\" i18n=\"@@listTargetTable\">\r\n        Danh sách bảng mục tiêu\r\n    </a>\r\n</div>\r\n<div class=\"box table-list\">\r\n    <div class=\"box-header\">\r\n        <div class=\"box-status\">\r\n            <div class=\"status\">\r\n                <b i18n=\"@@informationTargetTable\">Thông tin bảng mục tiêu</b>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"box-content\">\r\n        <div class=\"cm-mgb-5\"><b>Tên bảng mục tiêu</b>: {{targetTableDetail?.name}}</div>\r\n        <div class=\"cm-mgb-5\"><b>Mô tả</b>: {{targetTableDetail?.description }}</div>\r\n        <div class=\"cm-mgb-5\"><b>Ngày bắt đầu</b>: {{targetTableDetail?.startDate | dateTimeFormat: 'DD/MM/YYYY'}}</div>\r\n        <div class=\"cm-mgb-5\"><b>Ngày kết thúc</b>: {{targetTableDetail?.endDate | dateTimeFormat: 'DD/MM/YYYY'}}</div>\r\n        <div class=\"cm-mgb-5\"><b>Trạng thái</b>:\r\n            <a [class.disabled]=\"!permission.edit\" (click)=\"confirmStatus(item)\"\r\n               *ngIf=\"targetTableDetail?.status !== targetTableStatus.underConstruction\">\r\n                <i class=\"fa fa-check-circle color-bright-green\" aria-hidden=\"true\"\r\n                   *ngIf=\"targetTableDetail?.status === targetTableStatus.using\">\r\n                </i>\r\n                <i class=\"fa fa-minus-circle color-red\" aria-hidden=\"true\"\r\n                   *ngIf=\"targetTableDetail?.status === targetTableStatus.stopUsing\">\r\n                </i>\r\n                <span i18n=\"@@using\" class=\"color-black\"> {{targetTableDetail?.status === targetTableStatus.using ? 'Đang sử dụng' : 'Ngừng sử dụng'}}</span>\r\n            </a>\r\n            <a class=\"btn-circle-edit cm-mgt-5\" [class.disabled]=\"!permission.edit\"\r\n               *ngIf=\"targetTableDetail?.status === targetTableStatus.underConstruction\"\r\n               (click)=\"confirmStatus(item)\"></a>\r\n            <span *ngIf=\"targetTableDetail?.status === targetTableStatus.underConstruction\" i18n=\"@@pending\"> Đang xây dựng</span>\r\n        </div>\r\n        <div class=\"cm-mgb-5 cm-mgt-5 pull-left\" *ngIf=\"permission.edit\">\r\n            <button class=\"btn blue cm-mgr-5\"\r\n                    [disabled]=\"targetTableDetail?.status === targetTableStatus.stopUsing\" i18n=\"@@update\"\r\n                    (click)=\"update()\">Cập nhật\r\n            </button>\r\n            <button class=\"btn blue\" (click)=\"confirmStatus()\">{{targetTableDetail?.status === targetTableStatus.using ?\r\n                'Ngừng sử dụng'\r\n                : 'Đưa vào sửa dụng'}}\r\n            </button>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"box\">\r\n    <div class=\"box-header\">\r\n        <div class=\"box-status\">\r\n            <div class=\"status\">\r\n                <i class=\"fa fa-folder-open-o color-dark-blue\"></i><b> Nhóm mục tiêu</b>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"box-content\">\r\n        <div class=\"pull-left cm-mgb-10\">\r\n            <button class=\"btn blue\" i18n=\"@@addTargetGroup\" (click)=\"addTargetGroup()\" *ngIf=\"permission.add\">Thêm nhóm\r\n                mục tiêu\r\n            </button>\r\n        </div>\r\n        <table id=\"hyperTable\" class=\"table table-striped table-bordered table-hover table-sorter table-toggle\"\r\n               cellpadding=\"0\" cellspacing=\"0\">\r\n            <thead>\r\n            <tr>\r\n                <th class=\"center middle\" i18n=\"@@no\">STT</th>\r\n                <th class=\"middle\" i18n=\"@@targetGroupName\">Tên nhóm mục tiêu</th>\r\n                <th class=\"middle center hidden-xs w150\">Màu sắc hiển thị</th>\r\n                <th class=\"middle center hidden-xs w150\" i18n=\"@@order\">Thứ tự hiển thị</th>\r\n                <th class=\"center w100\" i18n=\"@@function\" *ngIf=\"permission.edit || permission.delete\">Chức năng</th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            <tr *ngFor=\"let item of listTargetGroup; let i = index\">\r\n                <td class=\"middle center w50\">{{i+ 1}}</td>\r\n                <td class=\"middle\">\r\n                    {{item.name}}\r\n                </td>\r\n                <td class=\"middle center hidden-xs w150\">\r\n                    <div class=\"dp-block-inline\" *ngIf=\"permission.edit; else spanBlockColor\">\r\n                        <span ghmDialogTrigger=\"\" [ghmDialogTriggerFor]=\"targetTableLabelColor\"\r\n                              [ghmDialogData]=\"item\"\r\n                              class=\"block-size-20 cursor-pointer\" [ngStyle]=\"{'background-color': item.color}\"></span>\r\n                    </div>\r\n                    <ng-template #spanBlockColor>\r\n                        <div class=\"dp-block-inline\">\r\n                            <span class=\"block-size-20\" [ngStyle]=\"{'background-color': item.color}\"></span>\r\n                        </div>\r\n                    </ng-template>\r\n                </td>\r\n                <td class=\"center middle hidden-xs\">\r\n                    <ng-container *ngIf=\"permission.edit; else spanOrder\">\r\n                        <input class=\"form-control text-right\" [ngModel]=\"item.order\" #Order [id]=\"item.id\"\r\n                               (blur)=\"updateOrder(item, Order.value)\">\r\n                    </ng-container>\r\n                    <ng-template #spanOrder>\r\n                        {{item.order}}\r\n                    </ng-template>\r\n                </td>\r\n                <td class=\"center middle\" *ngIf=\"permission.edit || permission.delete\">\r\n                    <a [class.disabled]=\"targetTableDetail?.status === targetTableStatus.stopUsing\"\r\n                       *ngIf=\"permission.edit\" data-icon=\"update-16\"\r\n                       (click)=\"updateTargetGroup(item)\" class=\"cm-mgr-5\">\r\n                    </a>\r\n                    <a data-icon=\"delete-16\" (click)=\"confirmDelete(item)\" *ngIf=\"permission.delete\">\r\n                    </a>\r\n                </td>\r\n            </tr>\r\n            </tbody>\r\n        </table>\r\n    </div>\r\n</div>\r\n\r\n<swal\r\n    #confirmUpdateStatus\r\n    i18n=\"@@confirmUpdateStatusTargetTable\"\r\n    i18n-title=\"@@confirmTitleUpdateStatusTargetTable\"\r\n    i18n-text=\"@@confirmTextUpdateStatusTargetTable\"\r\n    title=\"Xác nhận\"\r\n    text=\"{{confirmTitleUpdateStatus}}\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Đồng ý\"\r\n    cancelButtonText=\"Không\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<swal\r\n    #confirmDeleteTargetGroup\r\n    i18n=\"@@confirmDeleteTargetGroup\"\r\n    i18n-title=\"@@confirmTitleDeleteTargetGroup\"\r\n    i18n-text=\"@@confirmTextDeleteTargetGroup\"\r\n    title=\"Xác nhận\"\r\n    text=\"Bạn có muốn xóa nhóm mục tiêu này không\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Đồng ý\"\r\n    cancelButtonText=\"Không\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<app-target-table-form (saveSuccessful)=\"onSaveSuccessful($event)\"></app-target-table-form>\r\n<app-target-group-form\r\n    [targetTableId]=\"targetTableDetail?.id\"\r\n    [listTargetGroup]=\"listTargetGroup\"\r\n    (saveSuccessful)=\"onSaveSuccessfulTargetGroup($event)\">\r\n</app-target-group-form>\r\n\r\n<ghm-dialog #targetTableLabelColor [backdropStatic]=\"false\" [size]=\"'sm'\"\r\n            (show)=\"showDialogColor()\"\r\n            position=\"center\">\r\n    <ghm-dialog-header>\r\n        Cập nhật mã màu\r\n    </ghm-dialog-header>\r\n    <ghm-dialog-content>\r\n        <ghm-color-picker [colorId]=\"targetGroup?.color\"\r\n                          (selectColor)=\"selectColor($event)\"></ghm-color-picker>\r\n    </ghm-dialog-content>\r\n    <ghm-dialog-footer>\r\n        <div class=\"center\">\r\n            <button class=\"btn btn-sm blue cm-mgr-5\" (click)=\"updateColor()\" i18n=\"@@update\">\r\n                Cập nhập\r\n            </button>\r\n            <button class=\"btn btn-sm btn-light\" i18n=\"@@close\" ghm-dismiss=\"true\"\r\n                    type=\"button\">\r\n                Đóng\r\n            </button>\r\n        </div>\r\n    </ghm-dialog-footer>\r\n</ghm-dialog>\r\n"

/***/ }),

/***/ "./src/app/modules/task/target/target-table/target-table-detail/target-table-detail.component.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-table/target-table-detail/target-table-detail.component.ts ***!
  \*******************************************************************************************************/
/*! exports provided: TargetTableDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetTableDetailComponent", function() { return TargetTableDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _target_table_form_target_table_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../target-table-form/target-table-form.component */ "./src/app/modules/task/target/target-table/target-table-form/target-table-form.component.ts");
/* harmony import */ var _target_table_const__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../target-table.const */ "./src/app/modules/task/target/target-table/target-table.const.ts");
/* harmony import */ var _service_target_table_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../service/target-table.service */ "./src/app/modules/task/target/target-table/service/target-table.service.ts");
/* harmony import */ var _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @toverux/ngx-sweetalert2 */ "./node_modules/@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
/* harmony import */ var _viewmodel_target_group_search_viewmodel__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../viewmodel/target-group-search.viewmodel */ "./src/app/modules/task/target/target-table/viewmodel/target-group-search.viewmodel.ts");
/* harmony import */ var _target_group_form_target_group_form_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../target-group-form/target-group-form.component */ "./src/app/modules/task/target/target-table/target-group-form/target-group-form.component.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../../../shareds/components/ghm-dialog/ghm-dialog.component */ "./src/app/shareds/components/ghm-dialog/ghm-dialog.component.ts");














var TargetTableDetailComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TargetTableDetailComponent, _super);
    function TargetTableDetailComponent(route, toastr, utilService, targetTableService) {
        var _this = _super.call(this) || this;
        _this.route = route;
        _this.toastr = toastr;
        _this.utilService = utilService;
        _this.targetTableService = targetTableService;
        _this.targetTableStatus = _target_table_const__WEBPACK_IMPORTED_MODULE_5__["TargetTableStatus"];
        _this.listTargetGroup = [];
        return _this;
    }
    TargetTableDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.appService.setupPage(this.pageId.TARGET, this.pageId.TARGET_TABLE, 'Danh sách mục tiêu', 'Quản lý mục tiêu');
        this.subscribers.routeData = this.route.data.subscribe(function (result) {
            _this.targetTableDetail = result.data.data;
            if (_this.targetTableDetail) {
                _this.listTargetGroup = _this.targetTableDetail.targetGroups;
            }
        });
    };
    TargetTableDetailComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.swaConfirmUpdateStatus.confirm.subscribe(function () {
            _this.updateStatus();
        });
        this.swaConfirmDelete.confirm.subscribe(function () {
            _this.delete(_this.targetGroup);
        });
    };
    TargetTableDetailComponent.prototype.onSaveSuccessful = function (targetTable) {
        if (targetTable) {
            this.targetTableDetail.concurrencyStamp = targetTable.concurrencyStamp;
            this.targetTableDetail.name = targetTable.name;
            this.targetTableDetail.endDate = targetTable.endDate;
            this.targetTableDetail.startDate = targetTable.startDate;
        }
    };
    TargetTableDetailComponent.prototype.update = function () {
        this.targetTableForm.update(this.targetTableDetail);
    };
    TargetTableDetailComponent.prototype.updateColor = function () {
        var _this = this;
        this.targetTableService.updateTargetGroupColor(this.targetGroup.targetTableId, this.targetGroup.id, this.colorId).subscribe(function (result) {
            var targetGroupInfo = lodash__WEBPACK_IMPORTED_MODULE_10__["find"](_this.listTargetGroup, function (targetGroup) {
                return targetGroup.id === _this.targetGroup.id;
            });
            if (targetGroupInfo) {
                targetGroupInfo.color = _this.colorId;
            }
            _this.targetTableLabelColor.dismiss();
        });
    };
    TargetTableDetailComponent.prototype.confirmStatus = function () {
        var _this = this;
        this.confirmTitleUpdateStatus = !(this.targetTableDetail.status === _target_table_const__WEBPACK_IMPORTED_MODULE_5__["TargetTableStatus"].using) ?
            'Bảng mục tiêu khi đưa vào sử dụng sẽ không được chỉnh sửa cấu trúc của bảng mục tiêu nữa.' +
                ' Bạn có chắc chắn thực hiện tác nghiệp này?'
            : 'Bảng mục tiêu sẽ không được sử dụng nữa sau khi thực hiện thành công tác nghiệp này.' +
                ' Bạn có chắc chắn thực hiện tác nghiệp này?';
        setTimeout(function () {
            _this.swaConfirmUpdateStatus.show();
        });
    };
    TargetTableDetailComponent.prototype.updateStatus = function () {
        var _this = this;
        var status = this.targetTableDetail.status === _target_table_const__WEBPACK_IMPORTED_MODULE_5__["TargetTableStatus"].using ? _target_table_const__WEBPACK_IMPORTED_MODULE_5__["TargetTableStatus"].stopUsing : _target_table_const__WEBPACK_IMPORTED_MODULE_5__["TargetTableStatus"].using;
        this.targetTableService.updateStatus(this.targetTableDetail.id, status).subscribe(function () {
            _this.targetTableDetail.status = status;
        });
    };
    TargetTableDetailComponent.prototype.updateOrder = function (value, order) {
        if (isNaN(order)) {
            this.toastr.error('Bạn phải nhập sô');
            this.utilService.setValueElement(value.id, true, value.order);
            this.utilService.focusElement(value.id);
            return;
        }
        if (value.order === parseInt(order)) {
            return;
        }
        this.targetTableService.updateTargetGroupOrder(this.targetTableDetail.id, value.id, order).subscribe(function () {
            value.order = order;
        });
    };
    TargetTableDetailComponent.prototype.delete = function (targetGroup) {
        var _this = this;
        this.targetTableService.deleteTargetGroup(this.targetTableDetail.id, targetGroup.id).subscribe(function () {
            lodash__WEBPACK_IMPORTED_MODULE_10__["remove"](_this.listTargetGroup, function (item) {
                return item.id === targetGroup.id;
            });
        });
    };
    TargetTableDetailComponent.prototype.confirmDelete = function (value) {
        this.targetGroup = value;
        this.swaConfirmDelete.show();
    };
    TargetTableDetailComponent.prototype.addTargetGroup = function () {
        this.targetGroupFormComponent.add();
    };
    TargetTableDetailComponent.prototype.updateTargetGroup = function (targetGroup) {
        this.targetGroupFormComponent.update(targetGroup);
    };
    TargetTableDetailComponent.prototype.onSaveSuccessfulTargetGroup = function (value) {
        if (value) {
            var targetGroup = lodash__WEBPACK_IMPORTED_MODULE_10__["find"](this.listTargetGroup, function (item) {
                return item.id === value.id;
            });
            if (!targetGroup) {
                this.listTargetGroup.push(new _viewmodel_target_group_search_viewmodel__WEBPACK_IMPORTED_MODULE_8__["TargetGroupSearchViewModel"](value.id, this.targetTableDetail.id, value.name, value.description, this.listTargetGroup.length + 1, value.color, value.concurrencyStamp));
            }
            else {
                targetGroup.name = value.name;
                targetGroup.description = value.description;
                targetGroup.concurrencyStamp = value.concurrencyStamp;
                targetGroup.color = value.color;
            }
        }
    };
    TargetTableDetailComponent.prototype.selectColor = function (value) {
        this.colorId = value;
    };
    TargetTableDetailComponent.prototype.showDialogColor = function () {
        this.targetGroup = this.targetTableLabelColor.ghmDialogData;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_target_table_form_target_table_form_component__WEBPACK_IMPORTED_MODULE_4__["TargetTableFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _target_table_form_target_table_form_component__WEBPACK_IMPORTED_MODULE_4__["TargetTableFormComponent"])
    ], TargetTableDetailComponent.prototype, "targetTableForm", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_target_group_form_target_group_form_component__WEBPACK_IMPORTED_MODULE_9__["TargetGroupFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _target_group_form_target_group_form_component__WEBPACK_IMPORTED_MODULE_9__["TargetGroupFormComponent"])
    ], TargetTableDetailComponent.prototype, "targetGroupFormComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('confirmUpdateStatus'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_7__["SwalComponent"])
    ], TargetTableDetailComponent.prototype, "swaConfirmUpdateStatus", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('confirmDeleteTargetGroup'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_7__["SwalComponent"])
    ], TargetTableDetailComponent.prototype, "swaConfirmDelete", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('targetTableLabelColor'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_13__["GhmDialogComponent"])
    ], TargetTableDetailComponent.prototype, "targetTableLabelColor", void 0);
    TargetTableDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-target-table-detail',
            template: __webpack_require__(/*! ./target-table-detail.component.html */ "./src/app/modules/task/target/target-table/target-table-detail/target-table-detail.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_11__["ToastrService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_12__["UtilService"],
            _service_target_table_service__WEBPACK_IMPORTED_MODULE_6__["TargetTableService"]])
    ], TargetTableDetailComponent);
    return TargetTableDetailComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_3__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/task/target/target-table/target-table-detail/target-table-detail.resolve.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-table/target-table-detail/target-table-detail.resolve.ts ***!
  \*****************************************************************************************************/
/*! exports provided: TargetTableDetailResolve */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetTableDetailResolve", function() { return TargetTableDetailResolve; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_target_table_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/target-table.service */ "./src/app/modules/task/target/target-table/service/target-table.service.ts");



var TargetTableDetailResolve = /** @class */ (function () {
    function TargetTableDetailResolve(targetTableService) {
        this.targetTableService = targetTableService;
    }
    TargetTableDetailResolve.prototype.resolve = function (route, state) {
        var id = route.params['id'];
        if (id) {
            return this.targetTableService.getDetail(id);
        }
    };
    TargetTableDetailResolve = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_target_table_service__WEBPACK_IMPORTED_MODULE_2__["TargetTableService"]])
    ], TargetTableDetailResolve);
    return TargetTableDetailResolve;
}());



/***/ }),

/***/ "./src/app/modules/task/target/target-table/target-table-form/target-table-form.component.html":
/*!*****************************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-table/target-table-form/target-table-form.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #targetTableFormModal size=\"md\" [backdropStatic]=\"true\">\r\n    <nh-modal-header>\r\n        <ng-container i18n=\"@@targetTableInformation\">Thông tin bảng mục tiêu</ng-container>\r\n    </nh-modal-header>\r\n    <form class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-12\">\r\n                    <div class=\"form\">\r\n                        <div class=\"form-group cm-mgb-10\" [class.has-error]=\"formErrors?.name\">\r\n                            <label i18n-ghmLabel=\"@@targetTableName\" ghmLabel=\"Tên bảng mục tiêu\"\r\n                                   class=\"col-sm-4 ghm-label\" [required]=\"true\"></label>\r\n                            <div class=\"col-sm-8\">\r\n                                <ghm-input [elementId]=\"'name'\"\r\n                                           [placeholder]=\"'Nhập tên bảng mục tiêu'\"\r\n                                           i18n-placeholder=\"@@enterTargetTableNameHolder\"\r\n                                           formControlName=\"name\">\r\n                                </ghm-input>\r\n                                <span class=\"help-block\">{ formErrors?.name, select,\r\n                                                            required {Tên bảng mục tiêu không được để trống}\r\n                                                            maxlength {Tên bảng mục tiêu không được vượt quá 256 ký tự}\r\n                                                            pattern {Tên bảng mục tiêu phải chứa ký tự}}</span>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group cm-mgb-10\" [class.has-error]=\"formErrors?.description\">\r\n                            <label i18n-ghmLabel=\"@@description\" ghmLabel=\"Mô tả\"\r\n                                   class=\"col-sm-4 ghm-label\"></label>\r\n                            <div class=\"col-sm-8\">\r\n                                <textarea class=\"form-control\" formControlName=\"description\"\r\n                                          i18n-placeholder=\"@@enterDescriptionHolder\"\r\n                                          rows=\"5\"\r\n                                          placeholder=\"Nhập mô tả\"></textarea>\r\n                                <span class=\"help-block\">{ formErrors?.description, select,\r\n                                                            maxlength {Mô tả không được vượt quá không được vượt quá 4000 ký tự}}\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group cm-mgb-10\" [class.has-error]=\"formErrors?.startDate\">\r\n                            <label i18n-ghmLabel=\"@@startDate\" ghmLabel=\"Ngày bắt đầu\"\r\n                                   class=\"col-sm-4 ghm-label\"></label>\r\n                            <div class=\"col-sm-6\">\r\n                                <nh-date [format]=\"'DD/MM/YYYY'\"\r\n                                         formControlName=\"startDate\"></nh-date>\r\n                                <span class=\"help-block\">{ formErrors?.startDate, select,\r\n                                                           isValid {Ngày bắt đầu không đúng định dạng}\r\n                                                           notBefore {Ngày bắt đầu không được sau ngày kết thúc}}\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group cm-mgb-10\" [class.has-error]=\"formErrors?.endDate\">\r\n                            <label i18n-ghmLabel=\"@@startDate\" ghmLabel=\"Ngày kết thúc\"\r\n                                   class=\"col-sm-4 ghm-label\"></label>\r\n                            <div class=\"col-sm-6\">\r\n                                <nh-date [format]=\"'DD/MM/YYYY'\"\r\n                                         formControlName=\"endDate\"></nh-date>\r\n                                <span class=\"help-block\">{ formErrors?.endDate, select,\r\n                                                           isValid {Ngày kết thúc không đúng định dạng}\r\n                                                           notBefore {Ngày kết thúc không được trước ngày bắt đầu}}\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <div class=\"center\">\r\n                <ghm-button classes=\"btn blue cm-mgr-5\" type=\"button\"\r\n                            [loading]=\"isSaving\"\r\n                            (clicked)=\"save()\">\r\n                    <span i18n=\"@@update\">Cập nhật</span>\r\n                </ghm-button>\r\n                <ghm-button classes=\"btn btn-light\"\r\n                            nh-dismiss=\"true\"\r\n                            [type]=\"'button'\"\r\n                            [loading]=\"isSaving\">\r\n                    <span i18n=\"@@close\">Đóng</span>\r\n                </ghm-button>\r\n            </div>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/task/target/target-table/target-table-form/target-table-form.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-table/target-table-form/target-table-form.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: TargetTableFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetTableFormComponent", function() { return TargetTableFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _model_target_table_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../model/target-table.model */ "./src/app/modules/task/target/target-table/model/target-table.model.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _service_target_table_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../service/target-table.service */ "./src/app/modules/task/target/target-table/service/target-table.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _shareds_constants_pattern_const__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../shareds/constants/pattern.const */ "./src/app/shareds/constants/pattern.const.ts");
/* harmony import */ var _validators_datetime_validator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../../validators/datetime.validator */ "./src/app/validators/datetime.validator.ts");
/* harmony import */ var _target_table_const__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../target-table.const */ "./src/app/modules/task/target/target-table/target-table.const.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_12__);













var TargetTableFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TargetTableFormComponent, _super);
    function TargetTableFormComponent(fb, utilService, dateTimeValidator, targetTableService) {
        var _this = _super.call(this) || this;
        _this.fb = fb;
        _this.utilService = utilService;
        _this.dateTimeValidator = dateTimeValidator;
        _this.targetTableService = targetTableService;
        _this.targetTable = new _model_target_table_model__WEBPACK_IMPORTED_MODULE_4__["TargetTable"]();
        _this.listTargetTableType = [
            {
                id: _target_table_const__WEBPACK_IMPORTED_MODULE_11__["TargetTableType"].bsc,
                name: 'Bảng mục tiêu theo BSC',
                description: 'Mục tiêu được nhóm vào một trong bốn nhóm Tài chính, Khách hàng, Quy trình nội bộ, Đào tạo và phát triển.' +
                    ' Bao gồm các thông tin: Phương pháp đo, Trọng số, Ngày bắt đầu, Hạn kết thúc, % hoàn thành, Trạng thái.'
            },
            {
                id: _target_table_const__WEBPACK_IMPORTED_MODULE_11__["TargetTableType"].basic,
                name: 'Bảng mục tiêu cơ bản',
                description: 'Mục tiêu bao gồm các thông tin: Phương pháp đo, Trọng số, Ngày bắt đầu, Hạn kết thúc, % hoàn thành, Trạng thái.'
            }
        ];
        return _this;
    }
    TargetTableFormComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    TargetTableFormComponent.prototype.add = function (targetTableType) {
        this.isUpdate = false;
        this.utilService.focusElement('name');
        var targetTableTypeDetail = lodash__WEBPACK_IMPORTED_MODULE_12__["find"](this.listTargetTableType, function (item) {
            return item.id === targetTableType;
        });
        this.model.patchValue({
            type: targetTableType, name: targetTableTypeDetail ? targetTableTypeDetail.name : '',
            description: targetTableTypeDetail ? targetTableTypeDetail.description : ''
        });
        this.targetTableModal.open();
    };
    TargetTableFormComponent.prototype.update = function (item) {
        this.isUpdate = true;
        this.id = item.id;
        this.model.patchValue(item);
        this.utilService.focusElement('name');
        this.targetTableModal.open();
    };
    TargetTableFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            this.targetTable = this.model.value;
            if (this.isUpdate) {
                this.targetTableService.update(this.id, this.targetTable)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["finalize"])(function () { return (_this.isSaving = false); }))
                    .subscribe(function (result) {
                    _this.targetTable.concurrencyStamp = result.data;
                    _this.targetTable.id = _this.id;
                    _this.saveSuccessful.emit(_this.targetTable);
                    _this.targetTableModal.dismiss();
                });
            }
            else {
                this.targetTableService.insert(this.targetTable)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["finalize"])(function () { return (_this.isSaving = false); }))
                    .subscribe(function (result) {
                    _this.targetTable.concurrencyStamp = result.data;
                    _this.targetTable.id = result.data;
                    _this.saveSuccessful.emit(_this.targetTable);
                    _this.targetTableModal.dismiss();
                });
            }
        }
    };
    TargetTableFormComponent.prototype.renderForm = function () {
        this.buildForm();
    };
    TargetTableFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.utilService.renderFormError([
            'name',
            'description',
            'startDate',
            'endDate'
        ]);
        this.validationMessages = this.utilService.renderFormErrorMessage([
            { 'name': ['required', 'maxlength', 'pattern'] },
            { 'description': ['maxlength'] },
            { 'startDate': ['isValid', 'notAfter'] },
            { 'endDate': ['isValid', 'notBefore'] }
        ]);
        this.model = this.fb.group({
            name: [this.targetTable.name, [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].maxLength(256), _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].pattern(_shareds_constants_pattern_const__WEBPACK_IMPORTED_MODULE_9__["Pattern"].whiteSpace)]],
            description: [this.targetTable.description, [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].maxLength(4000)]],
            startDate: [this.targetTable.startDate, [this.dateTimeValidator.isValid, this.dateTimeValidator.notAfter('endDate')]],
            endDate: [this.targetTable.endDate, [this.dateTimeValidator.isValid, this.dateTimeValidator.notBefore('startDate')]],
            type: [this.targetTable.type],
            concurrencyStamp: [this.targetTable.concurrencyStamp],
        });
        this.model.valueChanges.subscribe(function (data) { return _this.validateModel(false); });
    };
    TargetTableFormComponent.prototype.resetForm = function () {
        this.model.patchValue(new _model_target_table_model__WEBPACK_IMPORTED_MODULE_4__["TargetTable"]());
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('targetTableFormModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__["NhModalComponent"])
    ], TargetTableFormComponent.prototype, "targetTableModal", void 0);
    TargetTableFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-target-table-form',
            template: __webpack_require__(/*! ./target-table-form.component.html */ "./src/app/modules/task/target/target-table/target-table-form/target-table-form.component.html"),
            providers: [_validators_datetime_validator__WEBPACK_IMPORTED_MODULE_10__["DateTimeValidator"]],
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_5__["UtilService"],
            _validators_datetime_validator__WEBPACK_IMPORTED_MODULE_10__["DateTimeValidator"],
            _service_target_table_service__WEBPACK_IMPORTED_MODULE_7__["TargetTableService"]])
    ], TargetTableFormComponent);
    return TargetTableFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/task/target/target-table/target-table.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/modules/task/target/target-table/target-table.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span i18n=\"@@listTargetTable\">Danh sách bảng mục tiêu</span>\r\n</h1>\r\n<div class=\"pull-left cm-mgb-10\">\r\n    <a class=\"btn blue\" i18n=\"@@add\" (click)=\"add()\" *ngIf=\"permission.add\">Thêm mới</a>\r\n</div>\r\n<div class=\"box table-list\">\r\n    <div class=\"box-header\">\r\n        <div class=\"box-status\">\r\n            <div class=\"status\">\r\n                <mat-checkbox color=\"primary\" [checked]=\"isShowActive\" (change)=\"showActive()\" i18n=\"@@showTableUsing\">\r\n                    Chỉ hiển thị bảng mục tiêu đang sử dụng\r\n                </mat-checkbox>\r\n            </div>\r\n            <div class=\"statistic\">\r\n                <span>{{numberActive}}/{{listItems?.length}} </span>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"box-content\" *ngIf=\"listItems && listItems.length > 0\">\r\n        <table class=\"table table-striped table-bordered table-hover table-toggle\">\r\n            <thead>\r\n            <tr>\r\n                <th class=\"center middle\" i18n=\"@@no\">STT</th>\r\n                <th class=\"middle\" i18n=\"@@targetTableName\">Tên bảng mục tiêu</th>\r\n                <th class=\"middle visible-lg\">Mô tả</th>\r\n                <th class=\"center middle hidden-xs hidden-sm\" i18n=\"@@durationTime\">Khoảng thời gian</th>\r\n                <th class=\"middle center hidden-xs w100\" i18n=\"@@priority\">Độ ưu tiên</th>\r\n                <th class=\"middle center hidden-xs w100\" i18n=\"@@status\">Trạng thái</th>\r\n                <th class=\"center middle w100\" i18n=\"@@function\" *ngIf=\"permission.delete\">Chức năng</th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            <tr *ngFor=\"let item of listTargetTable; let i = index\">\r\n                <td class=\"middle center w50\">{{i+ 1}}</td>\r\n                <td class=\"middle\">\r\n                    <a href=\"javascript://\" routerLink=\"/targets/target-table/{{item.id}}\">{{item.name}}</a>\r\n                </td>\r\n                <td class=\"middle visible-lg\">{{item.description}}</td>\r\n                <td class=\"center hidden-xs hidden-sm\">\r\n                    <span *ngIf=\"item.startDate\"> {{item.startDate | dateTimeFormat : 'DD/MM/YYYY'}} - {{item.endDate | dateTimeFormat : 'DD/MM/YYYY'}}</span>\r\n                </td>\r\n                <td class=\"center middle hidden-xs\">\r\n                    <ng-container *ngIf=\"permission.edit; else spanOrder\">\r\n                        <input class=\"form-control text-right\" [ngModel]=\"item.order\" #Order [id]=\"item.id\"\r\n                               (blur)=\"updateOrder(item, Order.value)\" *ngIf=\"item.status === targetTableStatus.using\">\r\n                    </ng-container>\r\n                    <ng-template #spanOrder>\r\n                        {{item.order}}\r\n                    </ng-template>\r\n                    <span *ngIf=\"item.status !== targetTableStatus.using\">-</span>\r\n                </td>\r\n                <td class=\"center middle\">\r\n                    <a class=\"font-size-18\" [class.disabled]=\"!permission.edit\" (click)=\"confirmStatus(item)\"\r\n                       *ngIf=\"item.status !== targetTableStatus.underConstruction\">\r\n                        <i class=\"fa fa-check-circle color-bright-green\" aria-hidden=\"true\"\r\n                           *ngIf=\"item.status === targetTableStatus.using\"></i>\r\n                        <i class=\"fa fa-minus-circle color-red\" aria-hidden=\"true\"\r\n                           *ngIf=\"item.status === targetTableStatus.stopUsing\"></i>\r\n                    </a>\r\n                    <a class=\"btn-circle-edit cm-mgt-5\" [class.disabled]=\"!permission.edit\" *ngIf=\"item.status === targetTableStatus.underConstruction\"\r\n                       (click)=\"confirmStatus(item)\"></a>\r\n                </td>\r\n                <td class=\"center middle hidden-xs\" *ngIf=\"permission.delete\">\r\n                    <a data-icon=\"delete-16\" (click)=\"confirmDelete(item)\">\r\n                    </a>\r\n                </td>\r\n            </tr>\r\n            </tbody>\r\n        </table>\r\n    </div>\r\n</div>\r\n\r\n<swal\r\n    #confirmUpdateStatus\r\n    i18n=\"@@confirmUpdateStatusTargetTable\"\r\n    i18n-title=\"@@confirmTitleUpdateStatusTargetTable\"\r\n    i18n-text=\"@@confirmTextUpdateStatusTargetTable\"\r\n    title=\"Xác nhận\"\r\n    text=\"{{confirmTitleUpdateStatus}}\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Đồng ý\"\r\n    cancelButtonText=\"Không\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<swal\r\n    #confirmDeleteTargetTable\r\n    i18n=\"@@confirmDeleteTargetTable\"\r\n    i18n-title=\"@@confirmTitleDeleteTargetTable\"\r\n    i18n-text=\"@@confirmTextDeleteTargetTable\"\r\n    title=\"Xác nhận\"\r\n    text=\"Bạn có muốn xóa bảng mục tiêu này không\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Đồng ý\"\r\n    cancelButtonText=\"Không\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<app-target-table-form (saveSuccessful)=\"onSaveSuccessful($event)\"></app-target-table-form>\r\n\r\n<nh-modal #modalTargetTableType [size]=\"'md'\" [backdropStatic]=\"true\">\r\n    <nh-modal-header>\r\n        <span i18n=\"@@selectTargetTableType\">Chọn mẫu bảng mục tiêu</span>\r\n    </nh-modal-header>\r\n    <nh-modal-content>\r\n        <div class=\"w100pc\">\r\n            <mat-radio-group color=\"primary\" [(ngModel)]=\"targetTableType\">\r\n                <mat-radio-button color=\"primary\" *ngFor=\"let targetTableType of listTargetTableType\"\r\n                                  [value]=\"targetTableType.id\">\r\n                    <b class=\"cm-mgt-15\">{{targetTableType.name}}</b>\r\n                    <p class=\"cm-mgt-0 cm-mgb-0\">{{targetTableType.description}}</p>\r\n                </mat-radio-button>\r\n            </mat-radio-group>\r\n        </div>\r\n    </nh-modal-content>\r\n    <nh-modal-footer>\r\n        <div class=\"center\">\r\n            <button class=\"btn blue cm-mgr-5\" i18n=\"@@select\" (click)=\"addTargetTable()\">\r\n                Chọn\r\n            </button>\r\n            <button class=\"btn btn-light\" i18n=\"@@close\" nh-dismiss=\"true\">Đóng</button>\r\n        </div>\r\n    </nh-modal-footer>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/task/target/target-table/target-table.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/modules/task/target/target-table/target-table.component.ts ***!
  \****************************************************************************/
/*! exports provided: TargetTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetTableComponent", function() { return TargetTableComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _viewmodel_target_table_search_viewmodel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./viewmodel/target-table-search.viewmodel */ "./src/app/modules/task/target/target-table/viewmodel/target-table-search.viewmodel.ts");
/* harmony import */ var _configs_page_id_config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../configs/page-id.config */ "./src/app/configs/page-id.config.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var _shareds_services_helper_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../shareds/services/helper.service */ "./src/app/shareds/services/helper.service.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _service_target_table_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./service/target-table.service */ "./src/app/modules/task/target/target-table/service/target-table.service.ts");
/* harmony import */ var _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @toverux/ngx-sweetalert2 */ "./node_modules/@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _target_table_const__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./target-table.const */ "./src/app/modules/task/target/target-table/target-table.const.ts");
/* harmony import */ var _target_table_form_target_table_form_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./target-table-form/target-table-form.component */ "./src/app/modules/task/target/target-table/target-table-form/target-table-form.component.ts");
















var TargetTableComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TargetTableComponent, _super);
    function TargetTableComponent(pageId, appConfig, route, router, targetTableService, helperService, utilService, toastr) {
        var _this = _super.call(this) || this;
        _this.pageId = pageId;
        _this.appConfig = appConfig;
        _this.route = route;
        _this.router = router;
        _this.targetTableService = targetTableService;
        _this.helperService = helperService;
        _this.utilService = utilService;
        _this.toastr = toastr;
        _this.isShowActive = true;
        _this.targetTableType = _target_table_const__WEBPACK_IMPORTED_MODULE_14__["TargetTableType"].bsc;
        _this.targetTableStatus = _target_table_const__WEBPACK_IMPORTED_MODULE_14__["TargetTableStatus"];
        _this.listTargetTableType = [
            {
                id: _target_table_const__WEBPACK_IMPORTED_MODULE_14__["TargetTableType"].bsc,
                name: 'Bảng mục tiêu theo BSC',
                description: 'Mục tiêu được nhóm vào một trong bốn nhóm Tài chính, Khách hàng, Quy trình nội bộ, Đào tạo và phát triển.' +
                    ' Bao gồm các thông tin: Phương pháp đo, Trọng số, Ngày bắt đầu, Hạn kết thúc, % hoàn thành, Trạng thái.'
            },
            {
                id: _target_table_const__WEBPACK_IMPORTED_MODULE_14__["TargetTableType"].basic,
                name: 'Bảng mục tiêu cơ bản',
                description: 'Mục tiêu bao gồm các thông tin: Phương pháp đo, Trọng số, Ngày bắt đầu, Hạn kết thúc, % hoàn thành, Trạng thái.'
            }
        ];
        return _this;
    }
    TargetTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.appService.setupPage(this.pageId.TARGET, this.pageId.TARGET_TABLE, 'Danh sách mục tiêu', 'Quản lý mục tiêu');
        this.subscribers.data = this.route.data.subscribe(function (result) {
            var data = result.data;
            _this.listItems = data.items;
            _this.renderResult();
        });
    };
    TargetTableComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.swaConfirmUpdateStatus.confirm.subscribe(function () {
            _this.updateStatus(_this.targetTable);
        });
        this.swaConfirmDelete.confirm.subscribe(function () {
            _this.delete(_this.targetTable);
        });
    };
    TargetTableComponent.prototype.add = function () {
        this.modalTargetTableType.open();
    };
    TargetTableComponent.prototype.addTargetTable = function () {
        this.modalTargetTableType.dismiss();
        this.targetTableForm.add(this.targetTableType);
    };
    TargetTableComponent.prototype.showActive = function () {
        this.isShowActive = !this.isShowActive;
        this.renderResult();
    };
    TargetTableComponent.prototype.confirmStatus = function (value) {
        var _this = this;
        this.confirmTitleUpdateStatus = !(value.status === _target_table_const__WEBPACK_IMPORTED_MODULE_14__["TargetTableStatus"].using) ?
            'Bảng mục tiêu khi đưa vào sử dụng sẽ không được chỉnh sửa cấu trúc của bảng mục tiêu nữa.' +
                ' Bạn có chắc chắn thực hiện tác nghiệp này?'
            : 'Bảng mục tiêu sẽ không được sử dụng nữa sau khi thực hiện thành công tác nghiệp này.' +
                ' Bạn có chắc chắn thực hiện tác nghiệp này?';
        this.targetTable = value;
        setTimeout(function () {
            _this.swaConfirmUpdateStatus.show();
        });
    };
    TargetTableComponent.prototype.confirmDelete = function (value) {
        this.targetTable = value;
        this.swaConfirmDelete.show();
    };
    TargetTableComponent.prototype.updateStatus = function (value) {
        var _this = this;
        var status = value.status === _target_table_const__WEBPACK_IMPORTED_MODULE_14__["TargetTableStatus"].using ? _target_table_const__WEBPACK_IMPORTED_MODULE_14__["TargetTableStatus"].stopUsing : _target_table_const__WEBPACK_IMPORTED_MODULE_14__["TargetTableStatus"].using;
        this.targetTableService.updateStatus(value.id, status).subscribe(function () {
            value.status = status;
            _this.renderResult();
            if (status === _target_table_const__WEBPACK_IMPORTED_MODULE_14__["TargetTableStatus"].using) {
                value.order = _this.numberActive;
            }
        });
    };
    TargetTableComponent.prototype.updateOrder = function (value, order) {
        if (isNaN(order)) {
            this.toastr.error('Bạn phải nhập sô');
            this.utilService.setValueElement(value.id, true, value.order);
            this.utilService.focusElement(value.id);
            return;
        }
        if (value.order === parseInt(order)) {
            // this.utilService.focusElement(value.id);
            return;
        }
        this.targetTableService.updateOrder(value.id, order).subscribe(function () {
            value.order = order;
        });
    };
    TargetTableComponent.prototype.renderResult = function () {
        var listTargetTableActive = lodash__WEBPACK_IMPORTED_MODULE_11__["filter"](this.listItems, function (item) {
            return item.status !== _target_table_const__WEBPACK_IMPORTED_MODULE_14__["TargetTableStatus"].stopUsing;
        });
        if (this.isShowActive) {
            this.listTargetTable = listTargetTableActive;
        }
        else {
            this.listTargetTable = this.listItems;
        }
        this.numberActive = lodash__WEBPACK_IMPORTED_MODULE_11__["countBy"](this.listItems, function (targetTable) {
            return targetTable.status === _target_table_const__WEBPACK_IMPORTED_MODULE_14__["TargetTableStatus"].using;
        }).true;
    };
    TargetTableComponent.prototype.delete = function (value) {
        var _this = this;
        if (value.status === _target_table_const__WEBPACK_IMPORTED_MODULE_14__["TargetTableStatus"].using) {
            this.toastr.error('Bảng mục tiêu này đang được sử dụng. Bạn không thể xóa');
            return;
        }
        this.targetTableService.delete(value.id).subscribe(function () {
            lodash__WEBPACK_IMPORTED_MODULE_11__["remove"](_this.listItems, function (item) {
                return item.id === value.id;
            });
            _this.renderResult();
        });
    };
    TargetTableComponent.prototype.onSaveSuccessful = function (targetTable) {
        if (targetTable) {
            var targetTableInfo = new _viewmodel_target_table_search_viewmodel__WEBPACK_IMPORTED_MODULE_3__["TargetTableSearchViewModel"]();
            targetTableInfo.id = targetTable.id;
            targetTableInfo.status = _target_table_const__WEBPACK_IMPORTED_MODULE_14__["TargetTableStatus"].underConstruction;
            targetTableInfo.name = targetTable.name;
            targetTableInfo.description = targetTable.description;
            targetTable.startDate = targetTable.startDate;
            targetTable.endDate = targetTable.endDate;
            this.listItems.push(targetTableInfo);
            this.renderResult();
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('confirmUpdateStatus'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_10__["SwalComponent"])
    ], TargetTableComponent.prototype, "swaConfirmUpdateStatus", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('confirmDeleteTargetTable'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_10__["SwalComponent"])
    ], TargetTableComponent.prototype, "swaConfirmDelete", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('modalTargetTableType'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_13__["NhModalComponent"])
    ], TargetTableComponent.prototype, "modalTargetTableType", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_target_table_form_target_table_form_component__WEBPACK_IMPORTED_MODULE_15__["TargetTableFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _target_table_form_target_table_form_component__WEBPACK_IMPORTED_MODULE_15__["TargetTableFormComponent"])
    ], TargetTableComponent.prototype, "targetTableForm", void 0);
    TargetTableComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-target-table',
            template: __webpack_require__(/*! ./target-table.component.html */ "./src/app/modules/task/target/target-table/target-table.component.html"),
            providers: [_shareds_services_helper_service__WEBPACK_IMPORTED_MODULE_7__["HelperService"], _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_8__["UtilService"]],
            styles: [__webpack_require__(/*! ./target-table.scss */ "./src/app/modules/task/target/target-table/target-table.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_page_id_config__WEBPACK_IMPORTED_MODULE_4__["PAGE_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_6__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, Object, _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _service_target_table_service__WEBPACK_IMPORTED_MODULE_9__["TargetTableService"],
            _shareds_services_helper_service__WEBPACK_IMPORTED_MODULE_7__["HelperService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_8__["UtilService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_12__["ToastrService"]])
    ], TargetTableComponent);
    return TargetTableComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/task/target/target-table/target-table.const.ts":
/*!************************************************************************!*\
  !*** ./src/app/modules/task/target/target-table/target-table.const.ts ***!
  \************************************************************************/
/*! exports provided: TargetTableType, TargetTableStatus, ListColorTargetGroup */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetTableType", function() { return TargetTableType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetTableStatus", function() { return TargetTableStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListColorTargetGroup", function() { return ListColorTargetGroup; });
var TargetTableType = {
    bsc: 0,
    basic: 1
};
var TargetTableStatus = {
    underConstruction: 0,
    using: 1,
    stopUsing: 2 // Dừng sử dụng
};
var ListColorTargetGroup = [
    { id: 0, color: '#9BBB59' },
    { id: 1, color: '#5CB37C' },
    { id: 2, color: '#29AAE3' },
    { id: 3, color: '#8064A2' },
    { id: 4, color: '#FBD109' },
    { id: 5, color: '#0C7424' },
    { id: 6, color: '#FC0204' },
    { id: 7, color: '#FACA9A' }
];


/***/ }),

/***/ "./src/app/modules/task/target/target-table/target-table.scss":
/*!********************************************************************!*\
  !*** ./src/app/modules/task/target/target-table/target-table.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "::ng-deep .mat-radio-button.mat-primary .mat-radio-ripple .mat-ripple-element {\n  background: none !important; }\n\n::ng-deep .mat-radio-button.mat-primary .mat-radio-inner-circle {\n  background-color: #3598dc !important;\n  /*inner circle color change*/ }\n\n::ng-deep.mat-radio-button.mat-primary.mat-radio-checked .mat-radio-outer-circle {\n  border-color: #3598dc !important;\n  /*outer ring color change*/ }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3RhcmdldC90YXJnZXQtdGFibGUvRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFwcFxcbW9kdWxlc1xcdGFza1xcdGFyZ2V0XFx0YXJnZXQtdGFibGVcXHRhcmdldC10YWJsZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0ksMkJBQTJCLEVBQUE7O0FBRy9CO0VBQ0ksb0NBQXdDO0VBQUUsNEJBQUEsRUFBNkI7O0FBRzNFO0VBQ0ksZ0NBQW9DO0VBQUUsMEJBQUEsRUFBMkIiLCJmaWxlIjoic3JjL2FwcC9tb2R1bGVzL3Rhc2svdGFyZ2V0L3RhcmdldC10YWJsZS90YXJnZXQtdGFibGUuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi8uLi8uLi8uLi9hc3NldHMvc3R5bGVzL2NvbmZpZ1wiO1xyXG5cclxuOjpuZy1kZWVwIC5tYXQtcmFkaW8tYnV0dG9uLm1hdC1wcmltYXJ5IC5tYXQtcmFkaW8tcmlwcGxlIC5tYXQtcmlwcGxlLWVsZW1lbnQge1xyXG4gICAgYmFja2dyb3VuZDogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC1yYWRpby1idXR0b24ubWF0LXByaW1hcnkgLm1hdC1yYWRpby1pbm5lci1jaXJjbGUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWJsdWUgIWltcG9ydGFudDsgLyppbm5lciBjaXJjbGUgY29sb3IgY2hhbmdlKi9cclxufVxyXG5cclxuOjpuZy1kZWVwLm1hdC1yYWRpby1idXR0b24ubWF0LXByaW1hcnkubWF0LXJhZGlvLWNoZWNrZWQgLm1hdC1yYWRpby1vdXRlci1jaXJjbGUge1xyXG4gICAgYm9yZGVyLWNvbG9yOiAkY29sb3ItYmx1ZSAhaW1wb3J0YW50OyAvKm91dGVyIHJpbmcgY29sb3IgY2hhbmdlKi9cclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/modules/task/target/target-table/viewmodel/target-group-search.viewmodel.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-table/viewmodel/target-group-search.viewmodel.ts ***!
  \*********************************************************************************************/
/*! exports provided: TargetGroupSearchViewModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetGroupSearchViewModel", function() { return TargetGroupSearchViewModel; });
var TargetGroupSearchViewModel = /** @class */ (function () {
    function TargetGroupSearchViewModel(id, targetTableId, name, description, order, color, concurrencyStamp) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.order = order;
        this.color = color;
        this.concurrencyStamp = concurrencyStamp;
    }
    return TargetGroupSearchViewModel;
}());



/***/ }),

/***/ "./src/app/modules/task/target/target-table/viewmodel/target-table-search.viewmodel.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/modules/task/target/target-table/viewmodel/target-table-search.viewmodel.ts ***!
  \*********************************************************************************************/
/*! exports provided: TargetTableSearchViewModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetTableSearchViewModel", function() { return TargetTableSearchViewModel; });
var TargetTableSearchViewModel = /** @class */ (function () {
    function TargetTableSearchViewModel() {
    }
    return TargetTableSearchViewModel;
}());



/***/ }),

/***/ "./src/app/modules/task/target/target.module.ts":
/*!******************************************************!*\
  !*** ./src/app/modules/task/target/target.module.ts ***!
  \******************************************************/
/*! exports provided: TargetModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetModule", function() { return TargetModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _shareds_layouts_layout_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../shareds/layouts/layout.module */ "./src/app/shareds/layouts/layout.module.ts");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../shareds/components/nh-modal/nh-modal.module */ "./src/app/shareds/components/nh-modal/nh-modal.module.ts");
/* harmony import */ var _target_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./target-routing.module */ "./src/app/modules/task/target/target-routing.module.ts");
/* harmony import */ var _target_table_target_table_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./target-table/target-table.component */ "./src/app/modules/task/target/target-table/target-table.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _shareds_pipe_datetime_format_datetime_format_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../shareds/pipe/datetime-format/datetime-format.module */ "./src/app/shareds/pipe/datetime-format/datetime-format.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @toverux/ngx-sweetalert2 */ "./node_modules/@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
/* harmony import */ var _target_table_target_table_form_target_table_form_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./target-table/target-table-form/target-table-form.component */ "./src/app/modules/task/target/target-table/target-table-form/target-table-form.component.ts");
/* harmony import */ var _target_libraries_target_library_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./target-libraries/target-library.component */ "./src/app/modules/task/target/target-libraries/target-library.component.ts");
/* harmony import */ var _target_libraries_target_library_group_form_target_library_group_form_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./target-libraries/target-library-group-form/target-library-group-form.component */ "./src/app/modules/task/target/target-libraries/target-library-group-form/target-library-group-form.component.ts");
/* harmony import */ var _shareds_components_nh_datetime_picker_nh_date_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../../shareds/components/nh-datetime-picker/nh-date.module */ "./src/app/shareds/components/nh-datetime-picker/nh-date.module.ts");
/* harmony import */ var _shareds_components_ghm_input_ghm_input_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../../shareds/components/ghm-input/ghm-input.module */ "./src/app/shareds/components/ghm-input/ghm-input.module.ts");
/* harmony import */ var _target_libraries_target_library_form_target_library_form_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./target-libraries/target-library-form/target-library-form.component */ "./src/app/modules/task/target/target-libraries/target-library-form/target-library-form.component.ts");
/* harmony import */ var _target_libraries_services_target_library_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./target-libraries/services/target-library.service */ "./src/app/modules/task/target/target-libraries/services/target-library.service.ts");
/* harmony import */ var _target_libraries_services_target_group_library_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./target-libraries/services/target-group-library.service */ "./src/app/modules/task/target/target-libraries/services/target-group-library.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _target_table_target_table_detail_target_table_detail_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./target-table/target-table-detail/target-table-detail.component */ "./src/app/modules/task/target/target-table/target-table-detail/target-table-detail.component.ts");
/* harmony import */ var _target_table_target_group_form_target_group_form_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./target-table/target-group-form/target-group-form.component */ "./src/app/modules/task/target/target-table/target-group-form/target-group-form.component.ts");
/* harmony import */ var _targets_targets_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./targets/targets.component */ "./src/app/modules/task/target/targets/targets.component.ts");
/* harmony import */ var _targets_targets_form_targets_form_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./targets/targets-form/targets-form.component */ "./src/app/modules/task/target/targets/targets-form/targets-form.component.ts");
/* harmony import */ var _shareds_components_ghm_user_suggestion_ghm_user_suggestion_module__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ../../../shareds/components/ghm-user-suggestion/ghm-user-suggestion.module */ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.module.ts");
/* harmony import */ var _hr_organization_office_services_office_service__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ../../hr/organization/office/services/office.service */ "./src/app/modules/hr/organization/office/services/office.service.ts");
/* harmony import */ var _targets_services_target_service__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./targets/services/target.service */ "./src/app/modules/task/target/targets/services/target.service.ts");
/* harmony import */ var _shareds_components_nh_tree_nh_tree_module__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ../../../shareds/components/nh-tree/nh-tree.module */ "./src/app/shareds/components/nh-tree/nh-tree.module.ts");
/* harmony import */ var _shareds_components_ghm_dialog_ghm_dialog_module__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ../../../shareds/components/ghm-dialog/ghm-dialog.module */ "./src/app/shareds/components/ghm-dialog/ghm-dialog.module.ts");
/* harmony import */ var _shareds_components_nh_user_picker_nh_user_picker_module__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ../../../shareds/components/nh-user-picker/nh-user-picker.module */ "./src/app/shareds/components/nh-user-picker/nh-user-picker.module.ts");
/* harmony import */ var _targets_targets_form_detail_targets_form_detail_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./targets/targets-form-detail/targets-form-detail.component */ "./src/app/modules/task/target/targets/targets-form-detail/targets-form-detail.component.ts");
/* harmony import */ var _shareds_components_ghm_file_explorer_ghm_file_explorer_module__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ../../../shareds/components/ghm-file-explorer/ghm-file-explorer.module */ "./src/app/shareds/components/ghm-file-explorer/ghm-file-explorer.module.ts");
/* harmony import */ var _shareds_components_ghm_select_ghm_select_module__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ../../../shareds/components/ghm-select/ghm-select.module */ "./src/app/shareds/components/ghm-select/ghm-select.module.ts");
/* harmony import */ var _shareds_components_ghm_attachments_ghm_attachments_module__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ../../../shareds/components/ghm-attachments/ghm-attachments.module */ "./src/app/shareds/components/ghm-attachments/ghm-attachments.module.ts");
/* harmony import */ var _shareds_components_ghm_comment_ghm_comment_module__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ../../../shareds/components/ghm-comment/ghm-comment.module */ "./src/app/shareds/components/ghm-comment/ghm-comment.module.ts");
/* harmony import */ var _validators_number_validator__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ../../../validators/number.validator */ "./src/app/validators/number.validator.ts");
/* harmony import */ var _validators_datetime_validator__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ../../../validators/datetime.validator */ "./src/app/validators/datetime.validator.ts");
/* harmony import */ var _shareds_components_ghm_select_user_picker_ghm_select_user_picker_module__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ../../../shareds/components/ghm-select-user-picker/ghm-select-user-picker.module */ "./src/app/shareds/components/ghm-select-user-picker/ghm-select-user-picker.module.ts");
/* harmony import */ var _shareds_components_ghm_app_suggestion_ghm_app_suggestion_module__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ../../../shareds/components/ghm-app-suggestion/ghm-app-suggestion.module */ "./src/app/shareds/components/ghm-app-suggestion/ghm-app-suggestion.module.ts");
/* harmony import */ var _targets_target_update_quick_target_update_quick_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./targets/target-update-quick/target-update-quick.component */ "./src/app/modules/task/target/targets/target-update-quick/target-update-quick.component.ts");
/* harmony import */ var _shareds_components_ghm_color_picker_ghm_color_picker_module__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ../../../shareds/components/ghm-color-picker/ghm-color-picker.module */ "./src/app/shareds/components/ghm-color-picker/ghm-color-picker.module.ts");
/* harmony import */ var _shareds_pipe_format_number_format_number_module__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ../../../shareds/pipe/format-number/format-number.module */ "./src/app/shareds/pipe/format-number/format-number.module.ts");
/* harmony import */ var _targets_target_update_method_calculate_result_target_update_method_calculate_result_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./targets/target-update-method-calculate-result/target-update-method-calculate-result.component */ "./src/app/modules/task/target/targets/target-update-method-calculate-result/target-update-method-calculate-result.component.ts");
/* harmony import */ var _shareds_components_ghm_select_datetime_ghm_select_datetime_module__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ../../../shareds/components/ghm-select-datetime/ghm-select-datetime.module */ "./src/app/shareds/components/ghm-select-datetime/ghm-select-datetime.module.ts");
/* harmony import */ var _shareds_components_ghm_suggestion_user_ghm_suggestion_user_module__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ../../../shareds/components/ghm-suggestion-user/ghm-suggestion-user.module */ "./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.module.ts");
/* harmony import */ var _log_log_module__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ../log/log.module */ "./src/app/modules/task/log/log.module.ts");
/* harmony import */ var _targets_target_report_target_report_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./targets/target-report/target-report.component */ "./src/app/modules/task/target/targets/target-report/target-report.component.ts");
/* harmony import */ var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! @swimlane/ngx-charts */ "./node_modules/@swimlane/ngx-charts/release/esm.js");
/* harmony import */ var _targets_target_report_target_report_list_target_report_list_component__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./targets/target-report/target-report-list/target-report-list.component */ "./src/app/modules/task/target/targets/target-report/target-report-list/target-report-list.component.ts");
/* harmony import */ var _shareds_components_nh_user_picker_nh_user_picker_service__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ../../../shareds/components/nh-user-picker/nh-user-picker.service */ "./src/app/shareds/components/nh-user-picker/nh-user-picker.service.ts");
/* harmony import */ var _target_share_component_target_share_component_module__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./target-share-component/target-share-component.module */ "./src/app/modules/task/target/target-share-component/target-share-component.module.ts");
/* harmony import */ var _targets_target_group_tree_target_group_tree_component__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./targets/target-group-tree/target-group-tree.component */ "./src/app/modules/task/target/targets/target-group-tree/target-group-tree.component.ts");
/* harmony import */ var _targets_target_group_tree_target_tree_target_tree_component__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ./targets/target-group-tree/target-tree/target-tree.component */ "./src/app/modules/task/target/targets/target-group-tree/target-tree/target-tree.component.ts");






















































var TargetModule = /** @class */ (function () {
    function TargetModule() {
    }
    TargetModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["ReactiveFormsModule"], _target_routing_module__WEBPACK_IMPORTED_MODULE_6__["TargetRoutingModule"], _shareds_layouts_layout_module__WEBPACK_IMPORTED_MODULE_3__["LayoutModule"], _shareds_components_nh_modal_nh_modal_module__WEBPACK_IMPORTED_MODULE_5__["NhModalModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatCheckboxModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatRadioModule"], _shareds_pipe_datetime_format_datetime_format_module__WEBPACK_IMPORTED_MODULE_9__["DatetimeFormatModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatExpansionModule"], _shareds_components_nh_datetime_picker_nh_date_module__WEBPACK_IMPORTED_MODULE_15__["NhDateModule"], _shareds_components_ghm_select_ghm_select_module__WEBPACK_IMPORTED_MODULE_33__["GhmSelectModule"], _shareds_components_ghm_attachments_ghm_attachments_module__WEBPACK_IMPORTED_MODULE_34__["GhmAttachmentsModule"],
                _core_core_module__WEBPACK_IMPORTED_MODULE_4__["CoreModule"], ngx_toastr__WEBPACK_IMPORTED_MODULE_20__["ToastrModule"].forRoot(), _shareds_components_ghm_user_suggestion_ghm_user_suggestion_module__WEBPACK_IMPORTED_MODULE_25__["GhmUserSuggestionModule"], _shareds_components_nh_tree_nh_tree_module__WEBPACK_IMPORTED_MODULE_28__["NHTreeModule"], _shareds_components_ghm_dialog_ghm_dialog_module__WEBPACK_IMPORTED_MODULE_29__["GhmDialogModule"], _shareds_components_nh_user_picker_nh_user_picker_module__WEBPACK_IMPORTED_MODULE_30__["NhUserPickerModule"], _shareds_components_ghm_comment_ghm_comment_module__WEBPACK_IMPORTED_MODULE_35__["GhmCommentModule"],
                _shareds_components_ghm_input_ghm_input_module__WEBPACK_IMPORTED_MODULE_16__["GhmInputModule"], _shareds_components_ghm_file_explorer_ghm_file_explorer_module__WEBPACK_IMPORTED_MODULE_32__["GhmFileExplorerModule"], _shareds_components_ghm_select_user_picker_ghm_select_user_picker_module__WEBPACK_IMPORTED_MODULE_38__["GhmSelectUserPickerModule"], _shareds_components_ghm_app_suggestion_ghm_app_suggestion_module__WEBPACK_IMPORTED_MODULE_39__["GhmAppSuggestionModule"],
                _shareds_pipe_format_number_format_number_module__WEBPACK_IMPORTED_MODULE_42__["FormatNumberModule"], _shareds_components_ghm_select_datetime_ghm_select_datetime_module__WEBPACK_IMPORTED_MODULE_44__["GhmSelectDatetimeModule"], _shareds_components_ghm_suggestion_user_ghm_suggestion_user_module__WEBPACK_IMPORTED_MODULE_45__["GhmSuggestionUserModule"], _log_log_module__WEBPACK_IMPORTED_MODULE_46__["LogModule"], _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_48__["NgxChartsModule"], _target_share_component_target_share_component_module__WEBPACK_IMPORTED_MODULE_51__["TargetShareComponentModule"],
                _shareds_components_ghm_color_picker_ghm_color_picker_module__WEBPACK_IMPORTED_MODULE_41__["GhmColorPickerModule"], _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_11__["SweetAlert2Module"].forRoot({
                    buttonsStyling: false,
                    customClass: 'modal-content',
                    confirmButtonClass: 'btn blue cm-mgr-5',
                    cancelButtonClass: 'btn',
                    showCancelButton: true,
                })],
            exports: [],
            declarations: [_target_table_target_table_component__WEBPACK_IMPORTED_MODULE_7__["TargetTableComponent"], _target_table_target_table_form_target_table_form_component__WEBPACK_IMPORTED_MODULE_12__["TargetTableFormComponent"], _target_libraries_target_library_component__WEBPACK_IMPORTED_MODULE_13__["TargetLibraryComponent"],
                _target_libraries_target_library_group_form_target_library_group_form_component__WEBPACK_IMPORTED_MODULE_14__["TargetLibraryGroupFormComponent"], _target_libraries_target_library_form_target_library_form_component__WEBPACK_IMPORTED_MODULE_17__["TargetLibraryFormComponent"], _target_table_target_table_detail_target_table_detail_component__WEBPACK_IMPORTED_MODULE_21__["TargetTableDetailComponent"],
                _target_table_target_group_form_target_group_form_component__WEBPACK_IMPORTED_MODULE_22__["TargetGroupFormComponent"], _targets_targets_component__WEBPACK_IMPORTED_MODULE_23__["TargetsComponent"], _targets_targets_form_targets_form_component__WEBPACK_IMPORTED_MODULE_24__["TargetsFormComponent"], _targets_targets_form_detail_targets_form_detail_component__WEBPACK_IMPORTED_MODULE_31__["TargetsFormDetailComponent"],
                _targets_target_update_quick_target_update_quick_component__WEBPACK_IMPORTED_MODULE_40__["TargetUpdateQuickComponent"], _targets_target_group_tree_target_group_tree_component__WEBPACK_IMPORTED_MODULE_52__["TargetGroupTreeComponent"], _targets_target_group_tree_target_tree_target_tree_component__WEBPACK_IMPORTED_MODULE_53__["TargetTreeComponent"],
                _targets_target_update_method_calculate_result_target_update_method_calculate_result_component__WEBPACK_IMPORTED_MODULE_43__["TargetUpdateMethodCalculateResultComponent"], _targets_target_report_target_report_component__WEBPACK_IMPORTED_MODULE_47__["TargetReportComponent"], _targets_target_report_target_report_list_target_report_list_component__WEBPACK_IMPORTED_MODULE_49__["TargetReportListComponent"]],
            entryComponents: [_targets_targets_form_detail_targets_form_detail_component__WEBPACK_IMPORTED_MODULE_31__["TargetsFormDetailComponent"], _targets_targets_form_targets_form_component__WEBPACK_IMPORTED_MODULE_24__["TargetsFormComponent"], _targets_target_update_method_calculate_result_target_update_method_calculate_result_component__WEBPACK_IMPORTED_MODULE_43__["TargetUpdateMethodCalculateResultComponent"],
                _targets_target_report_target_report_list_target_report_list_component__WEBPACK_IMPORTED_MODULE_49__["TargetReportListComponent"]],
            providers: [_target_libraries_services_target_library_service__WEBPACK_IMPORTED_MODULE_18__["TargetLibraryService"], _target_libraries_services_target_group_library_service__WEBPACK_IMPORTED_MODULE_19__["TargetGroupLibraryService"], _hr_organization_office_services_office_service__WEBPACK_IMPORTED_MODULE_26__["OfficeService"], _targets_services_target_service__WEBPACK_IMPORTED_MODULE_27__["TargetService"], _validators_number_validator__WEBPACK_IMPORTED_MODULE_36__["NumberValidator"],
                _validators_datetime_validator__WEBPACK_IMPORTED_MODULE_37__["DateTimeValidator"], _shareds_components_nh_user_picker_nh_user_picker_service__WEBPACK_IMPORTED_MODULE_50__["NhUserPickerService"]],
        })
    ], TargetModule);
    return TargetModule;
}());



/***/ }),

/***/ "./src/app/modules/task/target/targets/constant/target-status.const.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/constant/target-status.const.ts ***!
  \*****************************************************************************/
/*! exports provided: TargetStatus */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetStatus", function() { return TargetStatus; });
var TargetStatus = {
    new: 0,
    finish: 1,
    all: 2
};


/***/ }),

/***/ "./src/app/modules/task/target/targets/models/target-participants.model.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/models/target-participants.model.ts ***!
  \*********************************************************************************/
/*! exports provided: TargetParticipants */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetParticipants", function() { return TargetParticipants; });
var TargetParticipants = /** @class */ (function () {
    function TargetParticipants(id, targetId, userId, fullName, image, role, isRead, isNotification, isDisputation, isReport, isWriteTask, isWriteTarget, isWriteTargetChild) {
        this.isRead = isRead ? isRead : true;
        this.isNotification = isNotification ? isNotification : true;
        this.isDisputation = isDisputation ? isDisputation : true;
        this.isReport = isReport ? isReport : true;
        this.isWriteTarget = isWriteTarget ? isWriteTarget : false;
        this.isWriteTask = isWriteTask ? isWriteTask : true;
        this.isWriteTargetChild = isWriteTargetChild ? isWriteTargetChild : false;
    }
    return TargetParticipants;
}());



/***/ }),

/***/ "./src/app/modules/task/target/targets/models/target.model.ts":
/*!********************************************************************!*\
  !*** ./src/app/modules/task/target/targets/models/target.model.ts ***!
  \********************************************************************/
/*! exports provided: Target */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Target", function() { return Target; });
/* harmony import */ var _constant_target_status_const__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../constant/target-status.const */ "./src/app/modules/task/target/targets/constant/target-status.const.ts");

var Target = /** @class */ (function () {
    function Target(targetGroupId, targetTableId, parentId, name, type, description, levelOfSharing, userId, fullName, officeId, officeName, weight, startDate, endDate, measurementMethod, methodCalculateResult, percentCompleted, status, concurrencyStamp) {
        this.targetGroupId = targetGroupId;
        this.targetTableId = targetTableId;
        this.parentId = parentId;
        this.name = name;
        this.type = type;
        this.description = description;
        this.levelOfSharing = levelOfSharing !== undefined ? levelOfSharing : 1;
        this.userId = userId;
        this.fullName = fullName;
        this.officeId = officeId;
        this.officeName = officeName;
        this.weight = weight;
        this.startDate = startDate;
        this.endDate = endDate;
        this.measurementMethod = measurementMethod;
        this.methodCalculateResult = 0;
        this.percentCompleted = 0;
        this.status = _constant_target_status_const__WEBPACK_IMPORTED_MODULE_0__["TargetStatus"].new;
        this.concurrencyStamp = concurrencyStamp;
    }
    return Target;
}());



/***/ }),

/***/ "./src/app/modules/task/target/targets/target-group-tree/target-group-tree.component.html":
/*!************************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-group-tree/target-group-tree.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"table-tree\">\r\n    <div class=\"tt-header\" [ngStyle]=\"{'background-color': data.targetGroupColor}\"\r\n         (click)=\"data.isShowTarget = !data.isShowTarget\">\r\n        <div class=\"cell cell-group middle w100pc cm-pd-12-6\">\r\n            <a class=\"icon cm-mgl-5\">\r\n                <i class=\"fa fa-caret-down\" aria-hidden=\"true\" *ngIf=\"data.isShowTarget\"></i>\r\n                <i class=\"fa fa-caret-right\" aria-hidden=\"true\" *ngIf=\"!data.isShowTarget\"></i>\r\n                {{data.targetGroupName}} ({{data.targets?.length}})\r\n            </a>\r\n        </div>\r\n        <div class=\"cell w100 middle center hidden-xs\">\r\n            <span i18n=\"@@result\">Kết quả</span>\r\n        </div>\r\n        <div class=\"cell w100 middle center hidden-xs\">\r\n            <span i18n=\"@@weight\">Trọng số </span>\r\n            <!--<div>-->\r\n                <!--<a title=\"Cân bằng trọng số\" i18n-title=\"@@equallyWeight\" [swal]=\"confirmEquallyWeight\"-->\r\n                   <!--class=\"text-decoration-none\"-->\r\n                   <!--(confirm)=\"equallyWeight(data)\">-->\r\n                    <!--<i class=\"fa fa-balance-scale\" aria-hidden=\"true\"></i>-->\r\n                <!--</a>-->\r\n            <!--</div>-->\r\n        </div>\r\n        <div class=\"cell w150 middle center hidden-xs\" *ngIf=\"!isShortInfo\">\r\n            <span i18n=\"@@deadline\">Thời hạn</span>\r\n        </div>\r\n        <div class=\"cell w100 middle center\">\r\n            <span i18n=\"@@status\">Trạng thái</span>\r\n        </div>\r\n    </div>\r\n    <div class=\"tt-body cm-pdb-0\" *ngIf=\"data.targets && data.targets?.length > 0 && data.isShowTarget\">\r\n        <target-tree [data]=\"data.targets\"\r\n                     [isShortInfo]=\"isShortInfo\"\r\n                     [isShowDescription]=\"isShowDescription\"\r\n                     (remove)=\"onRemove($event)\"\r\n                     (update)=\"edit($event)\"\r\n                     (saveSuccess)=\"saveSuccess($event)\"></target-tree>\r\n    </div>\r\n</div>\r\n\r\n<swal\r\n    #confirmEquallyWeight\r\n    i18n-title=\"@@confirmEquallyWeightTitle\"\r\n    i18n-text=\"@@confirmEquallyWeightText\"\r\n    title=\"Bạn có muốn cân bằng trọng số?\"\r\n    text=\"Tất cả trọng số của mục tiêu hoặc mục tiêu con bằng nhau.\"\r\n    type=\"warning\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Đồng Ý\"\r\n    cancelButtonText=\"Hủy\">\r\n</swal>\r\n"

/***/ }),

/***/ "./src/app/modules/task/target/targets/target-group-tree/target-group-tree.component.scss":
/*!************************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-group-tree/target-group-tree.component.scss ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".table-tree {\n  position: relative;\n  float: left;\n  width: 100%;\n  margin-bottom: 12px; }\n  .table-tree .cell {\n    position: relative;\n    display: table-cell; }\n  .table-tree .cell-group {\n    min-width: 200px; }\n  .table-tree .row-tr {\n    border-top: 1px solid #ddd;\n    overflow: visible; }\n  .table-tree .tt-header {\n    padding: 0;\n    font-weight: normal;\n    line-height: 20px;\n    position: relative;\n    float: left;\n    width: 100%;\n    background-color: #45A2D2;\n    cursor: pointer;\n    font-size: 14px;\n    color: #fff; }\n  .table-tree .tt-header .icon {\n      color: #fff;\n      font-size: 16px; }\n  .table-tree .tt-header a {\n      color: white; }\n  .table-tree .tt-header a:hover {\n        text-decoration: none; }\n  .table-tree .tt-body {\n    position: relative;\n    float: left;\n    width: 100%;\n    background-color: #f5f7f7; }\n  .table-tree:first-child .tree_container .goal-line {\n    border-top: none; }\n  .table-tree .bg-info-blue {\n    border-radius: 4px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3RhcmdldC90YXJnZXRzL3RhcmdldC1ncm91cC10cmVlL0Q6XFxQcm9qZWN0XFxHaG1BcHBsaWNhdGlvblxcY2xpZW50c1xcZ2htYXBwbGljYXRpb25jbGllbnQvc3JjXFxhcHBcXG1vZHVsZXNcXHRhc2tcXHRhcmdldFxcdGFyZ2V0c1xcdGFyZ2V0LWdyb3VwLXRyZWVcXHRhcmdldC1ncm91cC10cmVlLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9tb2R1bGVzL3Rhc2svdGFyZ2V0L3RhcmdldHMvdGFyZ2V0LWdyb3VwLXRyZWUvRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFzc2V0c1xcc3R5bGVzXFxfY29uZmlnLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFdBQVc7RUFDWCxtQkFBbUIsRUFBQTtFQUp2QjtJQU9RLGtCQUFrQjtJQUNsQixtQkFBbUIsRUFBQTtFQVIzQjtJQVlRLGdCQUFnQixFQUFBO0VBWnhCO0lBZ0JRLDBCQUEwQjtJQUMxQixpQkFBaUIsRUFBQTtFQWpCekI7SUFxQlEsVUFBVTtJQUNWLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxXQUFXO0lBQ1gseUJDS1U7SURKVixlQUFlO0lBQ2YsZUFBZTtJQUNmLFdDYkksRUFBQTtFRGpCWjtNQWlDWSxXQ2hCQTtNRGlCQSxlQUFlLEVBQUE7RUFsQzNCO01BMENZLFlBQVksRUFBQTtFQTFDeEI7UUF1Q2dCLHFCQUFxQixFQUFBO0VBdkNyQztJQThDUSxrQkFBa0I7SUFDbEIsV0FBVztJQUNYLFdBQVc7SUFDWCx5QkNmWSxFQUFBO0VEbENwQjtJQXVEZ0IsZ0JBQWdCLEVBQUE7RUF2RGhDO0lBNkRRLDZCQUE2QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvbW9kdWxlcy90YXNrL3RhcmdldC90YXJnZXRzL3RhcmdldC1ncm91cC10cmVlL3RhcmdldC1ncm91cC10cmVlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCAnLi4vLi4vLi4vLi4vLi4vLi4vYXNzZXRzL3N0eWxlcy9jb25maWcnO1xyXG5cclxuLnRhYmxlLXRyZWUge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1hcmdpbi1ib3R0b206IDEycHg7XHJcblxyXG4gICAgLmNlbGwge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBkaXNwbGF5OiB0YWJsZS1jZWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC5jZWxsLWdyb3VwIHtcclxuICAgICAgICBtaW4td2lkdGg6IDIwMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5yb3ctdHIge1xyXG4gICAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZGRkO1xyXG4gICAgICAgIG92ZXJmbG93OiB2aXNpYmxlO1xyXG4gICAgfVxyXG5cclxuICAgIC50dC1oZWFkZXIge1xyXG4gICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICBsaW5lLWhlaWdodDogMjBweDtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGRhcmtCbHVlO1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgY29sb3I6ICR3aGl0ZTtcclxuXHJcbiAgICAgICAgLmljb24ge1xyXG4gICAgICAgICAgICBjb2xvcjogJHdoaXRlO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBhIHtcclxuICAgICAgICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAudHQtYm9keSB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRsaWdodC1ibHVlO1xyXG4gICAgfVxyXG5cclxuICAgICY6Zmlyc3QtY2hpbGQge1xyXG4gICAgICAgIC50cmVlX2NvbnRhaW5lciB7XHJcbiAgICAgICAgICAgIC5nb2FsLWxpbmUge1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyLXRvcDogbm9uZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuYmctaW5mby1ibHVlIHtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA0cHggIWltcG9ydGFudDtcclxuICAgIH1cclxufVxyXG4iLCIkZGVmYXVsdC1jb2xvcjogIzIyMjtcclxuJGZvbnQtZmFtaWx5OiBcIkFyaWFsXCIsIHRhaG9tYSwgSGVsdmV0aWNhIE5ldWU7XHJcbiRjb2xvci1ibHVlOiAjMzU5OGRjO1xyXG4kbWFpbi1jb2xvcjogIzAwNzQ1NTtcclxuJGJvcmRlckNvbG9yOiAjZGRkO1xyXG4kc2Vjb25kLWNvbG9yOiAjYjAxYTFmO1xyXG4kdGFibGUtYmFja2dyb3VuZC1jb2xvcjogIzAwOTY4ODtcclxuJGJsdWU6ICMwMDdiZmY7XHJcbiRkYXJrLWJsdWU6ICMwMDcyQkM7XHJcbiRicmlnaHQtYmx1ZTogI2RmZjBmZDtcclxuJGluZGlnbzogIzY2MTBmMjtcclxuJHB1cnBsZTogIzZmNDJjMTtcclxuJHBpbms6ICNlODNlOGM7XHJcbiRyZWQ6ICNkYzM1NDU7XHJcbiRvcmFuZ2U6ICNmZDdlMTQ7XHJcbiR5ZWxsb3c6ICNmZmMxMDc7XHJcbiRncmVlbjogIzI4YTc0NTtcclxuJHRlYWw6ICMyMGM5OTc7XHJcbiRjeWFuOiAjMTdhMmI4O1xyXG4kd2hpdGU6ICNmZmY7XHJcbiRncmF5OiAjODY4ZTk2O1xyXG4kZ3JheS1kYXJrOiAjMzQzYTQwO1xyXG4kcHJpbWFyeTogIzAwN2JmZjtcclxuJHNlY29uZGFyeTogIzZjNzU3ZDtcclxuJHN1Y2Nlc3M6ICMyOGE3NDU7XHJcbiRpbmZvOiAjMTdhMmI4O1xyXG4kd2FybmluZzogI2ZmYzEwNztcclxuJGRhbmdlcjogI2RjMzU0NTtcclxuJGxpZ2h0OiAjZjhmOWZhO1xyXG4kZGFyazogIzM0M2E0MDtcclxuJGxhYmVsLWNvbG9yOiAjNjY2O1xyXG4kYmFja2dyb3VuZC1jb2xvcjogI0VDRjBGMTtcclxuJGJvcmRlckFjdGl2ZUNvbG9yOiAjODBiZGZmO1xyXG4kYm9yZGVyUmFkaXVzOiAwO1xyXG4kZGFya0JsdWU6ICM0NUEyRDI7XHJcbiRsaWdodEdyZWVuOiAjMjdhZTYwO1xyXG4kbGlnaHQtYmx1ZTogI2Y1ZjdmNztcclxuJGJyaWdodEdyYXk6ICM3NTc1NzU7XHJcbiRtYXgtd2lkdGgtbW9iaWxlOiA3NjhweDtcclxuJG1heC13aWR0aC10YWJsZXQ6IDk5MnB4O1xyXG4kbWF4LXdpZHRoLWRlc2t0b3A6IDEyODBweDtcclxuXHJcbi8vIEJFR0lOOiBNYXJnaW5cclxuQG1peGluIG5oLW1nKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1ndCgkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi10b3A6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWdiKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ2woJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tbGVmdDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ3IoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vLyBFTkQ6IE1hcmdpblxyXG5cclxuLy8gQkVHSU46IFBhZGRpbmdcclxuQG1peGluIG5oLXBkKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZzogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZHQoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLXRvcDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZGIoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZGwoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGRyKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy1yaWdodDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogUGFkZGluZ1xyXG5cclxuLy8gQkVHSU46IFdpZHRoXHJcbkBtaXhpbiBuaC13aWR0aCgkd2lkdGgpIHtcclxuICAgIHdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxuICAgIG1pbi13aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbiAgICBtYXgtd2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vLyBFTkQ6IFdpZHRoXHJcblxyXG4vLyBCRUdJTjogSWNvbiBTaXplXHJcbkBtaXhpbiBuaC1zaXplLWljb24oJHNpemUpIHtcclxuICAgIHdpZHRoOiAkc2l6ZTtcclxuICAgIGhlaWdodDogJHNpemU7XHJcbiAgICBmb250LXNpemU6ICRzaXplO1xyXG59XHJcblxyXG4vLyBFTkQ6IEljb24gU2l6ZVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/modules/task/target/targets/target-group-tree/target-group-tree.component.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-group-tree/target-group-tree.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: TargetGroupTreeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetGroupTreeComponent", function() { return TargetGroupTreeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _targets_viewmodel_target_render_result_viewmodel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../targets/viewmodel/target-render-result.viewmodel */ "./src/app/modules/task/target/targets/viewmodel/target-render-result.viewmodel.ts");
/* harmony import */ var _targets_constant_type_target_const__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../targets/constant/type-target.const */ "./src/app/modules/task/target/targets/constant/type-target.const.ts");
/* harmony import */ var _targets_services_target_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../targets/services/target.service */ "./src/app/modules/task/target/targets/services/target.service.ts");





var TargetGroupTreeComponent = /** @class */ (function () {
    function TargetGroupTreeComponent(targetService) {
        this.targetService = targetService;
        this.isShortInfo = false;
        this.editData = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onSaveSuccess = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.remove = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.targetType = _targets_constant_type_target_const__WEBPACK_IMPORTED_MODULE_3__["TypeTarget"];
    }
    TargetGroupTreeComponent.prototype.ngOnInit = function () {
    };
    TargetGroupTreeComponent.prototype.edit = function (target) {
        this.editData.emit(target);
    };
    TargetGroupTreeComponent.prototype.equallyWeight = function (target) {
        var _this = this;
        this.targetService.equallyWeight(this.type, target.targetGroupId, this.targetId).subscribe(function () {
            _this.onSaveSuccess.emit();
        });
    };
    TargetGroupTreeComponent.prototype.saveSuccess = function (value) {
        this.onSaveSuccess.emit(value);
    };
    TargetGroupTreeComponent.prototype.onRemove = function (value) {
        this.remove.emit(value);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _targets_viewmodel_target_render_result_viewmodel__WEBPACK_IMPORTED_MODULE_2__["TargetGroupResultViewModel"])
    ], TargetGroupTreeComponent.prototype, "data", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TargetGroupTreeComponent.prototype, "isShortInfo", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TargetGroupTreeComponent.prototype, "targetId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TargetGroupTreeComponent.prototype, "isShowDescription", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TargetGroupTreeComponent.prototype, "type", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TargetGroupTreeComponent.prototype, "editData", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TargetGroupTreeComponent.prototype, "onSaveSuccess", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TargetGroupTreeComponent.prototype, "remove", void 0);
    TargetGroupTreeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'target-group-tree',
            template: __webpack_require__(/*! ./target-group-tree.component.html */ "./src/app/modules/task/target/targets/target-group-tree/target-group-tree.component.html"),
            styles: [__webpack_require__(/*! ./target-group-tree.component.scss */ "./src/app/modules/task/target/targets/target-group-tree/target-group-tree.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_targets_services_target_service__WEBPACK_IMPORTED_MODULE_4__["TargetService"]])
    ], TargetGroupTreeComponent);
    return TargetGroupTreeComponent;
}());



/***/ }),

/***/ "./src/app/modules/task/target/targets/target-group-tree/target-tree/target-tree.component.html":
/*!******************************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-group-tree/target-tree/target-tree.component.html ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"parent-tree\">\r\n    <div class=\"children\">\r\n        <div class=\"tree-container\">\r\n            <ng-template #recursiveTree let-data>\r\n                <ng-container *ngFor=\"let target of data; let indexTarget = index\">\r\n                    <div class=\"goal-line row-tr bg-white\"\r\n                         [class.last-goal-line]=\"indexTarget + 1 === data.length\">\r\n                        <div class=\"goal-tree-caret\" *ngIf=\"target.childCount > 0\"\r\n                             (click)=\"target.state.opened = !target.state.opened\">\r\n                            <i class=\"fa fa-fw fa-chevron-down\" *ngIf=\"target.state.opened\"></i>\r\n                            <i class=\"fa fa-fw fa-chevron-right\" *ngIf=\"!target.state.opened\"></i>\r\n                        </div>\r\n                        <div class=\"cell gw-cell-item w100pc middle\">\r\n                            <div class=\"gw-item\" [class.active]=\"targetInfo?.id === target.id\" (click)=\"onSelectTarget(target.data)\"\r\n                                 [class.cursor-pointer]=\"isSelectTarget\">\r\n                                <a class=\"text-decoration-none bold\" (click)=\"edit(target.data)\">{{target.name}}</a>\r\n                                <div *ngIf=\"isShowDescription\" class=\"color-bright-gray\">\r\n                                    {{target.data.description}}\r\n                                </div>\r\n                                <ul class=\"list-inline\">\r\n                                    <li class=\"\">\r\n                                        <a href=\"javascripts://\" title=\"Mục tiêu con: Hoàn thành/Tổng số\" class=\"text-decoration-none\">\r\n                                            <i class=\"fa fa-indent\"></i>\r\n                                            {{target?.data?.childCountComplete}}/{{target.childCount}}\r\n                                        </a>\r\n                                    </li>\r\n                                    <li>\r\n                                        <a href=\"javascripts://\" title=\"Công việc: Hoàn thành/Tổng số\" class=\"text-decoration-none\" routerLink=\"/tasks/list\">\r\n                                            <i class=\"fa fa-check-square-o\">\r\n                                            </i>\r\n                                            {{target.data.totalTaskComplete}}/{{target.data.totalTask}}\r\n                                        </a>\r\n                                    </li>\r\n                                    <li>\r\n                                        <a href=\"javascripts://\" class=\"text-decoration-none\" title=\"Tổng số comment\">\r\n                                            <i class=\"fa fa-comments-o\"></i>\r\n                                            {{target?.data?.totalComment}}\r\n                                        </a>\r\n                                    </li>\r\n                                    <li *ngIf=\"isShortInfo\">\r\n                                        <a class=\"text-decoration-none\">\r\n                                            <i class=\"fa fa-calendar\"> </i> {{target?.data?.endDate |\r\n                                            dateTimeFormat: 'DD/MM/YYYY'}}\r\n                                        </a>\r\n                                    </li>\r\n                                    <li *ngIf=\"!isShortInfo\">\r\n                                        <a class=\"text-decoration-none\">\r\n                                            <ng-container *ngIf=\"target.data.type === targetType.office\">\r\n                                                <i class=\"fa fa fa-sitemap\">\r\n                                                </i>\r\n                                                {{target.data.officeName}}\r\n                                            </ng-container>\r\n                                            <ng-container *ngIf=\"target.data.type === targetType.personal\">\r\n                                                <i class=\"fa fa fa-user\">\r\n                                                </i>\r\n                                                {{target.data.fullName}}\r\n                                            </ng-container>\r\n                                        </a>\r\n                                    </li>\r\n                                </ul>\r\n                                <div class=\"btn-update\"\r\n                                     *ngIf=\"target?.data.isWireTarget\"\r\n                                     ghmDialogTrigger=\"\" [ghmDialogTriggerFor]=\"targetUpdateQuick\"\r\n                                     [ghmDialogData]=\"target?.data\"\r\n                                     ghmDialogEvent=\"mouseover\">\r\n                                    <a data-icon=\"edit-16\"></a>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"cell border-left-dashed middle center w100 hidden-xs cm-pd-5\">\r\n                            <div class=\"pull-right\"> {{target?.data?.percentCompleted ? target?.data.percentCompleted :\r\n                                0}} %\r\n                            </div>\r\n                            <div class=\"progress w100pc cm-mgb-5\">\r\n                                <div class=\"progress-bar\" role=\"progressbar\"\r\n                                     [class.active]=\"target?.data?.status === targetStatus.finish && target?.data?.overdueDate <= 0\"\r\n                                     [class.progress-bar-warning]=\"target?.data?.status === targetStatus.finish && target?.data?.overdueDate > 0\"\r\n                                     [class.progress-bar-danger]=\"target?.data?.status === targetStatus.new && target?.data?.overdueDate > 0\"\r\n                                     [class.progress-bar-success]=\"target?.data?.status === targetStatus.new && target?.data?.overdueDate <= 0\"\r\n                                     [attr.aria-valuenow]=\"target?.data?.percentCompleted\"\r\n                                     aria-valuemin=\"0\"\r\n                                     aria-valuemax=\"100\"\r\n                                     [ngStyle]=\"{'width': target?.data?.percentCompleted + '%'}\">\r\n                                    <span class=\"sr-only\">{{ target?.data?.percentCompleted }}%</span>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"cell border-left-dashed middle center w100 hidden-xs\">\r\n                            {{target?.data?.weight ? target?.data?.weight : 0}} %\r\n                        </div>\r\n                        <div class=\"cell border-left-dashed middle center w150 hidden-xs\"\r\n                             *ngIf=\"!isShortInfo\">\r\n                            <i class=\"fa fa-calendar\"> </i> {{target?.data?.endDate |\r\n                            dateTimeFormat: 'DD/MM/YYYY'}}\r\n                        </div>\r\n                        <div class=\"cell border-left-dashed middle center w100\">\r\n                            <i class=\"fa fa-check-circle color-light-gray font-size-24 font-weight-normal\"\r\n                               [class.color-dark-blue]=\"target?.data?.status !== targetStatus.new\"\r\n                               aria-hidden=\"true\"></i>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"parent-tree\" *ngIf=\"target.childCount > 0 && target.state.opened\"\r\n                         [class.last-parent-tree]=\"indexTarget + 1 === data.length\">\r\n                        <div class=\"children\">\r\n                            <div class=\"tree-container\">\r\n                                <ng-container\r\n                                    *ngTemplateOutlet=\"recursiveTree; context:{ $implicit: target.children }\"></ng-container>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </ng-container>\r\n            </ng-template>\r\n            <ng-container *ngTemplateOutlet=\"recursiveTree; context:{ $implicit: data }\"></ng-container>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<ghm-dialog #targetUpdateQuick [size]=\"'md'\" [backdropStatic]=\"true\" position=\"center\"\r\n            (hide)=\"closeDialog()\"\r\n            (show)=\"showDialog()\">\r\n    <ghm-dialog-header>\r\n        <span>Cập nhật mục tiêu</span>\r\n        <a class=\"view-detail\" i18n=\"@@viewDetail\" (click)=\"edit(targetInfo)\">\r\n            Xem chi tiết\r\n        </a>\r\n    </ghm-dialog-header>\r\n    <ghm-dialog-content>\r\n        <target-update-quick [targetDetail]=\"targetInfo\"\r\n                             (remove)=\"removeTarget($event)\"\r\n                             (saveSuccessful)=\"saveSuccessful($event)\"\r\n        ></target-update-quick>\r\n    </ghm-dialog-content>\r\n</ghm-dialog>\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/modules/task/target/targets/target-group-tree/target-tree/target-tree.component.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-group-tree/target-tree/target-tree.component.ts ***!
  \****************************************************************************************************/
/*! exports provided: TargetTreeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetTreeComponent", function() { return TargetTreeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../shareds/components/ghm-dialog/ghm-dialog.component */ "./src/app/shareds/components/ghm-dialog/ghm-dialog.component.ts");
/* harmony import */ var _targets_viewmodel_target_tree_viewmodel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../targets/viewmodel/target-tree.viewmodel */ "./src/app/modules/task/target/targets/viewmodel/target-tree.viewmodel.ts");
/* harmony import */ var _targets_constant_type_target_const__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../targets/constant/type-target.const */ "./src/app/modules/task/target/targets/constant/type-target.const.ts");
/* harmony import */ var _targets_constant_target_status_const__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../targets/constant/target-status.const */ "./src/app/modules/task/target/targets/constant/target-status.const.ts");






var TargetTreeComponent = /** @class */ (function () {
    function TargetTreeComponent() {
        this.isShortInfo = false;
        this.isShowDescription = false;
        this.isSelectTarget = false;
        this.update = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.saveSuccess = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.remove = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.selectTarget = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.targetType = _targets_constant_type_target_const__WEBPACK_IMPORTED_MODULE_4__["TypeTarget"];
        this.targetStatus = _targets_constant_target_status_const__WEBPACK_IMPORTED_MODULE_5__["TargetStatus"];
    }
    TargetTreeComponent.prototype.edit = function (value) {
        if (this.targetInfo) {
            this.targetQuickUpdate.dismiss();
        }
        this.update.emit(value);
    };
    TargetTreeComponent.prototype.closeDialog = function () {
        this.targetInfo = null;
    };
    TargetTreeComponent.prototype.showDialog = function () {
        this.targetInfo = this.targetQuickUpdate.ghmDialogData;
    };
    TargetTreeComponent.prototype.removeTarget = function (value) {
        this.targetQuickUpdate.dismiss();
        this.remove.emit(value);
    };
    TargetTreeComponent.prototype.saveSuccessful = function (value) {
        this.targetInfo = value;
        this.saveSuccess.emit(value);
        this.targetQuickUpdate.dismiss();
    };
    TargetTreeComponent.prototype.onSelectTarget = function (value) {
        if (this.isSelectTarget) {
            this.selectTarget.emit(value);
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('targetUpdateQuick'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_2__["GhmDialogComponent"])
    ], TargetTreeComponent.prototype, "targetQuickUpdate", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _targets_viewmodel_target_tree_viewmodel__WEBPACK_IMPORTED_MODULE_3__["TargetTreeViewModel"])
    ], TargetTreeComponent.prototype, "data", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TargetTreeComponent.prototype, "isShortInfo", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TargetTreeComponent.prototype, "isShowDescription", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TargetTreeComponent.prototype, "isSelectTarget", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TargetTreeComponent.prototype, "update", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TargetTreeComponent.prototype, "saveSuccess", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TargetTreeComponent.prototype, "remove", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TargetTreeComponent.prototype, "selectTarget", void 0);
    TargetTreeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'target-tree',
            template: __webpack_require__(/*! ./target-tree.component.html */ "./src/app/modules/task/target/targets/target-group-tree/target-tree/target-tree.component.html"),
            styles: [__webpack_require__(/*! ./target-tree.scss */ "./src/app/modules/task/target/targets/target-group-tree/target-tree/target-tree.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TargetTreeComponent);
    return TargetTreeComponent;
}());



/***/ }),

/***/ "./src/app/modules/task/target/targets/target-group-tree/target-tree/target-tree.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-group-tree/target-tree/target-tree.scss ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".cell {\n  position: relative;\n  display: table-cell; }\n\n.cell-group {\n  min-width: 200px; }\n\n.row-tr {\n  border-top: 1px solid #ddd;\n  overflow: visible; }\n\n.parent-tree {\n  position: relative; }\n\n.parent-tree:before {\n    content: \"\";\n    display: block;\n    width: 1px;\n    position: absolute;\n    top: 0;\n    bottom: 0;\n    z-index: 1;\n    left: 14px;\n    background: #bbb; }\n\n.parent-tree .children {\n    position: relative;\n    padding-left: 28px; }\n\n.parent-tree .children .tree-container {\n      position: relative;\n      width: 100%; }\n\n.parent-tree .children .goal-line {\n      position: relative; }\n\n.parent-tree .children .goal-line:hover {\n        background-color: #dff0fd !important; }\n\n.parent-tree .children .goal-line::before {\n        content: \"\";\n        display: block;\n        width: 14px;\n        position: absolute;\n        bottom: 0;\n        top: 50%;\n        margin-top: -1px;\n        z-index: 1;\n        background: #bbb;\n        left: -14px;\n        height: 1px; }\n\n.parent-tree .children .goal-line .goal-tree-caret {\n        cursor: pointer;\n        color: #8c8c8c;\n        top: 0;\n        bottom: 0;\n        width: 27px;\n        left: -28px;\n        position: absolute;\n        z-index: 2; }\n\n.parent-tree .children .goal-line .goal-tree-caret i {\n          font-size: .6em;\n          top: 50%;\n          margin-top: -8px;\n          background: #fff;\n          border-radius: 100%;\n          width: 2em;\n          line-height: 2em;\n          position: absolute;\n          z-index: 2;\n          left: 6px;\n          text-align: center; }\n\n.parent-tree .children .goal-line .gw-cell-item {\n        padding: 6px 0 6px 12px; }\n\n.parent-tree .children .goal-line .gw-cell-item .gw-item {\n          padding: 0px 6px; }\n\n.parent-tree .children .goal-line .gw-cell-item .gw-item:hover .btn-update {\n            display: block; }\n\n.parent-tree .children .goal-line .gw-cell-item .gw-item .gw-description {\n            color: #757575;\n            font-style: italic; }\n\n.parent-tree .children .goal-line .gw-cell-item .active .btn-update {\n          display: block; }\n\n.parent-tree .children .last-goal-line::after {\n      content: \"\";\n      display: block;\n      width: 1px;\n      position: absolute;\n      top: 50%;\n      z-index: 1;\n      right: 0;\n      left: -14px;\n      bottom: 0;\n      background: #f5f7f7; }\n\n.last-parent-tree::after {\n  content: \"\";\n  display: block;\n  width: 1px;\n  position: absolute;\n  top: 0%;\n  z-index: 1;\n  right: 0;\n  left: -14px;\n  bottom: 0;\n  background: #f5f7f7; }\n\n.bg-info-blue {\n  border-radius: 4px !important; }\n\n.progress {\n  height: 10px; }\n\nul, .list-inline {\n  margin-bottom: 0px;\n  margin-top: 10px; }\n\nul li, .list-inline li {\n    padding: 0px 2px; }\n\nul li a, .list-inline li a {\n      color: #757575; }\n\ni {\n  font-weight: normal !important; }\n\n.view-detail {\n  text-decoration: underline;\n  color: #757575;\n  padding-left: 5px;\n  margin-left: 5px;\n  border-left: 1px solid #757575;\n  font-size: 14px; }\n\n.view-detail:hover {\n    color: #0072BC; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3RhcmdldC90YXJnZXRzL3RhcmdldC1ncm91cC10cmVlL3RhcmdldC10cmVlL0Q6XFxQcm9qZWN0XFxHaG1BcHBsaWNhdGlvblxcY2xpZW50c1xcZ2htYXBwbGljYXRpb25jbGllbnQvc3JjXFxhcHBcXG1vZHVsZXNcXHRhc2tcXHRhcmdldFxcdGFyZ2V0c1xcdGFyZ2V0LWdyb3VwLXRyZWVcXHRhcmdldC10cmVlXFx0YXJnZXQtdHJlZS5zY3NzIiwic3JjL2FwcC9tb2R1bGVzL3Rhc2svdGFyZ2V0L3RhcmdldHMvdGFyZ2V0LWdyb3VwLXRyZWUvdGFyZ2V0LXRyZWUvRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFzc2V0c1xcc3R5bGVzXFxfY29uZmlnLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDSSxrQkFBa0I7RUFDbEIsbUJBQW1CLEVBQUE7O0FBR3ZCO0VBQ0ksZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0ksMEJBQTBCO0VBQzFCLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLGtCQUFrQixFQUFBOztBQUR0QjtJQUdRLFdBQVc7SUFDWCxjQUFjO0lBQ2QsVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixNQUFNO0lBQ04sU0FBUztJQUNULFVBQVU7SUFDVixVQUFVO0lBQ1YsZ0JBQWdCLEVBQUE7O0FBWHhCO0lBZVEsa0JBQWtCO0lBQ2xCLGtCQUFrQixFQUFBOztBQWhCMUI7TUFtQlksa0JBQWtCO01BQ2xCLFdBQVcsRUFBQTs7QUFwQnZCO01BdUJZLGtCQUFrQixFQUFBOztBQXZCOUI7UUF5QmdCLG9DQUNKLEVBQUE7O0FBMUJaO1FBNkJnQixXQUFXO1FBQ1gsY0FBYztRQUNkLFdBQVc7UUFDWCxrQkFBa0I7UUFDbEIsU0FBUztRQUNULFFBQVE7UUFDUixnQkFBZ0I7UUFDaEIsVUFBVTtRQUNWLGdCQUFnQjtRQUNoQixXQUFXO1FBQ1gsV0FBVyxFQUFBOztBQXZDM0I7UUEyQ2dCLGVBQWU7UUFDZixjQUFjO1FBQ2QsTUFBTTtRQUNOLFNBQVM7UUFDVCxXQUFXO1FBQ1gsV0FBVztRQUNYLGtCQUFrQjtRQUNsQixVQUFVLEVBQUE7O0FBbEQxQjtVQXFEb0IsZUFBZTtVQUNmLFFBQVE7VUFDUixnQkFBZ0I7VUFDaEIsZ0JBQWdCO1VBQ2hCLG1CQUFtQjtVQUNuQixVQUFVO1VBQ1YsZ0JBQWdCO1VBQ2hCLGtCQUFrQjtVQUNsQixVQUFVO1VBQ1YsU0FBUztVQUNULGtCQUFrQixFQUFBOztBQS9EdEM7UUFvRWdCLHVCQUF1QixFQUFBOztBQXBFdkM7VUFzRW9CLGdCQUFnQixFQUFBOztBQXRFcEM7WUF5RTRCLGNBQWMsRUFBQTs7QUF6RTFDO1lBNkV3QixjQ3hESjtZRHlESSxrQkFBa0IsRUFBQTs7QUE5RTFDO1VBb0Z3QixjQUFjLEVBQUE7O0FBcEZ0QztNQTZGZ0IsV0FBVztNQUNYLGNBQWM7TUFDZCxVQUFVO01BQ1Ysa0JBQWtCO01BQ2xCLFFBQVE7TUFDUixVQUFVO01BQ1YsUUFBUTtNQUNSLFdBQVc7TUFDWCxTQUFTO01BQ1QsbUJDbEZJLEVBQUE7O0FEd0ZwQjtFQUVRLFdBQVc7RUFDWCxjQUFjO0VBQ2QsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixPQUFPO0VBQ1AsVUFBVTtFQUNWLFFBQVE7RUFDUixXQUFXO0VBQ1gsU0FBUztFQUNULG1CQ25HWSxFQUFBOztBRHVHcEI7RUFDSSw2QkFBNkIsRUFBQTs7QUFHakM7RUFDSSxZQUFZLEVBQUE7O0FBR2hCO0VBQ0ksa0JBQWtCO0VBQ2xCLGdCQUFnQixFQUFBOztBQUZwQjtJQU9RLGdCQUFnQixFQUFBOztBQVB4QjtNQUtZLGNDbkhRLEVBQUE7O0FEeUhwQjtFQUNJLDhCQUE4QixFQUFBOztBQUdsQztFQUNJLDBCQUEwQjtFQUMxQixjQy9IZ0I7RURnSWhCLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsOEJDbElnQjtFRG1JaEIsZUFBZSxFQUFBOztBQU5uQjtJQVFRLGNDbEtXLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9tb2R1bGVzL3Rhc2svdGFyZ2V0L3RhcmdldHMvdGFyZ2V0LWdyb3VwLXRyZWUvdGFyZ2V0LXRyZWUvdGFyZ2V0LXRyZWUuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgJy4uLy4uLy4uLy4uLy4uLy4uLy4uL2Fzc2V0cy9zdHlsZXMvY29uZmlnJztcclxuXHJcbi5jZWxsIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IHRhYmxlLWNlbGw7XHJcbn1cclxuXHJcbi5jZWxsLWdyb3VwIHtcclxuICAgIG1pbi13aWR0aDogMjAwcHg7XHJcbn1cclxuXHJcbi5yb3ctdHIge1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkZGQ7XHJcbiAgICBvdmVyZmxvdzogdmlzaWJsZTtcclxufVxyXG5cclxuLnBhcmVudC10cmVlIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICY6YmVmb3JlIHtcclxuICAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIHdpZHRoOiAxcHg7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHRvcDogMDtcclxuICAgICAgICBib3R0b206IDA7XHJcbiAgICAgICAgei1pbmRleDogMTtcclxuICAgICAgICBsZWZ0OiAxNHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNiYmI7XHJcbiAgICB9XHJcblxyXG4gICAgLmNoaWxkcmVuIHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAyOHB4O1xyXG5cclxuICAgICAgICAudHJlZS1jb250YWluZXIge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuZ29hbC1saW5lIHtcclxuICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNkZmYwZmQgIWltcG9ydGFudFxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAmOjpiZWZvcmUge1xyXG4gICAgICAgICAgICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDE0cHg7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgICAgICBib3R0b206IDA7XHJcbiAgICAgICAgICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IC0xcHg7XHJcbiAgICAgICAgICAgICAgICB6LWluZGV4OiAxO1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogI2JiYjtcclxuICAgICAgICAgICAgICAgIGxlZnQ6IC0xNHB4O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAxcHg7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC5nb2FsLXRyZWUtY2FyZXQge1xyXG4gICAgICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICM4YzhjOGM7XHJcbiAgICAgICAgICAgICAgICB0b3A6IDA7XHJcbiAgICAgICAgICAgICAgICBib3R0b206IDA7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMjdweDtcclxuICAgICAgICAgICAgICAgIGxlZnQ6IC0yOHB4O1xyXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICAgICAgei1pbmRleDogMjtcclxuXHJcbiAgICAgICAgICAgICAgICBpIHtcclxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IC42ZW07XHJcbiAgICAgICAgICAgICAgICAgICAgdG9wOiA1MCU7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogLThweDtcclxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDJlbTtcclxuICAgICAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMmVtO1xyXG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgICAgICAgICB6LWluZGV4OiAyO1xyXG4gICAgICAgICAgICAgICAgICAgIGxlZnQ6IDZweDtcclxuICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC5ndy1jZWxsLWl0ZW0ge1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogNnB4IDAgNnB4IDEycHg7XHJcbiAgICAgICAgICAgICAgICAuZ3ctaXRlbSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMHB4IDZweDtcclxuICAgICAgICAgICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmJ0bi11cGRhdGUge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgLmd3LWRlc2NyaXB0aW9uIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICRicmlnaHRHcmF5O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXN0eWxlOiBpdGFsaWM7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC5hY3RpdmUge1xyXG4gICAgICAgICAgICAgICAgICAgIC5idG4tdXBkYXRlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAubGFzdC1nb2FsLWxpbmUge1xyXG4gICAgICAgICAgICAvL21hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICAgICAgICAgICY6OmFmdGVyIHtcclxuICAgICAgICAgICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxcHg7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICAgICAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgICAgICAgICAgICByaWdodDogMDtcclxuICAgICAgICAgICAgICAgIGxlZnQ6IC0xNHB4O1xyXG4gICAgICAgICAgICAgICAgYm90dG9tOiAwO1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogJGxpZ2h0LWJsdWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5sYXN0LXBhcmVudC10cmVlIHtcclxuICAgICY6OmFmdGVyIHtcclxuICAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIHdpZHRoOiAxcHg7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHRvcDogMCU7XHJcbiAgICAgICAgei1pbmRleDogMTtcclxuICAgICAgICByaWdodDogMDtcclxuICAgICAgICBsZWZ0OiAtMTRweDtcclxuICAgICAgICBib3R0b206IDA7XHJcbiAgICAgICAgYmFja2dyb3VuZDogJGxpZ2h0LWJsdWU7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5iZy1pbmZvLWJsdWUge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5wcm9ncmVzcyB7XHJcbiAgICBoZWlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcbnVsLCAubGlzdC1pbmxpbmUge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIGxpIHtcclxuICAgICAgICBhIHtcclxuICAgICAgICAgICAgY29sb3I6ICRicmlnaHRHcmF5O1xyXG4gICAgICAgIH1cclxuICAgICAgICBwYWRkaW5nOiAwcHggMnB4O1xyXG4gICAgfVxyXG59XHJcblxyXG5pIHtcclxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnZpZXctZGV0YWlsIHtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gICAgY29sb3I6ICRicmlnaHRHcmF5O1xyXG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogNXB4O1xyXG4gICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAkYnJpZ2h0R3JheTtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICY6aG92ZXIge1xyXG4gICAgICAgIGNvbG9yOiAkZGFyay1ibHVlO1xyXG4gICAgfVxyXG59XHJcbiIsIiRkZWZhdWx0LWNvbG9yOiAjMjIyO1xyXG4kZm9udC1mYW1pbHk6IFwiQXJpYWxcIiwgdGFob21hLCBIZWx2ZXRpY2EgTmV1ZTtcclxuJGNvbG9yLWJsdWU6ICMzNTk4ZGM7XHJcbiRtYWluLWNvbG9yOiAjMDA3NDU1O1xyXG4kYm9yZGVyQ29sb3I6ICNkZGQ7XHJcbiRzZWNvbmQtY29sb3I6ICNiMDFhMWY7XHJcbiR0YWJsZS1iYWNrZ3JvdW5kLWNvbG9yOiAjMDA5Njg4O1xyXG4kYmx1ZTogIzAwN2JmZjtcclxuJGRhcmstYmx1ZTogIzAwNzJCQztcclxuJGJyaWdodC1ibHVlOiAjZGZmMGZkO1xyXG4kaW5kaWdvOiAjNjYxMGYyO1xyXG4kcHVycGxlOiAjNmY0MmMxO1xyXG4kcGluazogI2U4M2U4YztcclxuJHJlZDogI2RjMzU0NTtcclxuJG9yYW5nZTogI2ZkN2UxNDtcclxuJHllbGxvdzogI2ZmYzEwNztcclxuJGdyZWVuOiAjMjhhNzQ1O1xyXG4kdGVhbDogIzIwYzk5NztcclxuJGN5YW46ICMxN2EyYjg7XHJcbiR3aGl0ZTogI2ZmZjtcclxuJGdyYXk6ICM4NjhlOTY7XHJcbiRncmF5LWRhcms6ICMzNDNhNDA7XHJcbiRwcmltYXJ5OiAjMDA3YmZmO1xyXG4kc2Vjb25kYXJ5OiAjNmM3NTdkO1xyXG4kc3VjY2VzczogIzI4YTc0NTtcclxuJGluZm86ICMxN2EyYjg7XHJcbiR3YXJuaW5nOiAjZmZjMTA3O1xyXG4kZGFuZ2VyOiAjZGMzNTQ1O1xyXG4kbGlnaHQ6ICNmOGY5ZmE7XHJcbiRkYXJrOiAjMzQzYTQwO1xyXG4kbGFiZWwtY29sb3I6ICM2NjY7XHJcbiRiYWNrZ3JvdW5kLWNvbG9yOiAjRUNGMEYxO1xyXG4kYm9yZGVyQWN0aXZlQ29sb3I6ICM4MGJkZmY7XHJcbiRib3JkZXJSYWRpdXM6IDA7XHJcbiRkYXJrQmx1ZTogIzQ1QTJEMjtcclxuJGxpZ2h0R3JlZW46ICMyN2FlNjA7XHJcbiRsaWdodC1ibHVlOiAjZjVmN2Y3O1xyXG4kYnJpZ2h0R3JheTogIzc1NzU3NTtcclxuJG1heC13aWR0aC1tb2JpbGU6IDc2OHB4O1xyXG4kbWF4LXdpZHRoLXRhYmxldDogOTkycHg7XHJcbiRtYXgtd2lkdGgtZGVza3RvcDogMTI4MHB4O1xyXG5cclxuLy8gQkVHSU46IE1hcmdpblxyXG5AbWl4aW4gbmgtbWcoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW46ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWd0KCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLXRvcDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ2IoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1nbCgkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1ncigkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1yaWdodDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogTWFyZ2luXHJcblxyXG4vLyBCRUdJTjogUGFkZGluZ1xyXG5AbWl4aW4gbmgtcGQoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkdCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctdG9wOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkYigkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctYm90dG9tOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkbCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctbGVmdDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZHIoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBQYWRkaW5nXHJcblxyXG4vLyBCRUdJTjogV2lkdGhcclxuQG1peGluIG5oLXdpZHRoKCR3aWR0aCkge1xyXG4gICAgd2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG4gICAgbWluLXdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxuICAgIG1heC13aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogV2lkdGhcclxuXHJcbi8vIEJFR0lOOiBJY29uIFNpemVcclxuQG1peGluIG5oLXNpemUtaWNvbigkc2l6ZSkge1xyXG4gICAgd2lkdGg6ICRzaXplO1xyXG4gICAgaGVpZ2h0OiAkc2l6ZTtcclxuICAgIGZvbnQtc2l6ZTogJHNpemU7XHJcbn1cclxuXHJcbi8vIEVORDogSWNvbiBTaXplXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/modules/task/target/targets/target-report/const/target-report-type.const.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-report/const/target-report-type.const.ts ***!
  \*********************************************************************************************/
/*! exports provided: TargetReportTypeConst */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetReportTypeConst", function() { return TargetReportTypeConst; });
var TargetReportTypeConst = {
    statisticsByUnits: 0,
    statisticsByPersonal: 1,
    listTargetEmployee: 2,
    generalTargetAndTask: 3,
};


/***/ }),

/***/ "./src/app/modules/task/target/targets/target-report/model/info-search.viewmodel.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-report/model/info-search.viewmodel.ts ***!
  \******************************************************************************************/
/*! exports provided: InfoSearchViewModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoSearchViewModel", function() { return InfoSearchViewModel; });
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);

var InfoSearchViewModel = /** @class */ (function () {
    function InfoSearchViewModel() {
        this.startDate = moment__WEBPACK_IMPORTED_MODULE_0__().subtract(1, 'months').format('YYYY-MM-DD');
        this.endDate = moment__WEBPACK_IMPORTED_MODULE_0__().format('YYYY-MM-DD');
        this.typeSearchTime = 0;
        this.tableTargetId = '0';
        this.targetType = 3;
        this.showOfficeHaveTarget = true;
        this.status = 2;
    }
    return InfoSearchViewModel;
}());



/***/ }),

/***/ "./src/app/modules/task/target/targets/target-report/target-report-list/target-report-list.component.html":
/*!****************************************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-report/target-report-list/target-report-list.component.html ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"target-report-modal-detail\">\r\n    <div class=\"portlet light bordered cm-mgb-0\">\r\n        <div class=\"portlet-title\">\r\n            <ul>\r\n                <li class=\"icon\">\r\n                    <a imgsrc=\"dlg-goal\"></a>\r\n                </li>\r\n                <li>\r\n                    <h4 class=\"bold\">\r\n                        Thông tin mục tiêu\r\n                    </h4>\r\n                </li>\r\n                <li class=\"icon pull-right\">\r\n                    <a href=\"javascript://\" class=\"btn-close\" imgsrc=\"dlg-close\"\r\n                       (click)=\"dialogRef.close()\">\r\n                    </a>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n        <div class=\"portlet-content cm-mg-20\" *ngIf=\"isShowTree\">\r\n            <div>\r\n                <ng-container *ngIf=\"listData && listData.length >0; else onlyData\">\r\n                    <div *ngFor=\"let data of listData; let i = index\">\r\n                          <ng-container *ngIf=\"data.targetGroups && data.targetGroups.length > 0\">\r\n                              <div *ngFor=\"let targetGroup of data.targetGroups\">\r\n                                  <target-group-tree (editData)=\"update($event)\" [data]=\"targetGroup\"></target-group-tree>\r\n                              </div>\r\n                          </ng-container>\r\n                    </div>\r\n                </ng-container>\r\n                <ng-template #onlyData>\r\n                    <target-group-tree (editData)=\"update($event)\" [data]=\"listTree\">\r\n                    </target-group-tree>\r\n                </ng-template>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/task/target/targets/target-report/target-report-list/target-report-list.component.scss":
/*!****************************************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-report/target-report-list/target-report-list.component.scss ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".target-report-modal-detail .portlet-title {\n  padding: 0 !important; }\n  .target-report-modal-detail .portlet-title ul {\n    list-style: none;\n    margin-bottom: 0;\n    padding: 0;\n    display: block;\n    width: 100%; }\n  .target-report-modal-detail .portlet-title ul li {\n      display: inline-block;\n      float: left; }\n  .target-report-modal-detail .portlet-title ul li.icon a {\n        display: block;\n        width: 48px;\n        height: 48px; }\n  .target-report-modal-detail .portlet-title ul li.icon a.btn-close {\n          border-left: 1px solid #ddd; }\n  .target-report-modal-detail .portlet-title ul li h4 {\n        margin-top: 15px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3RhcmdldC90YXJnZXRzL3RhcmdldC1yZXBvcnQvdGFyZ2V0LXJlcG9ydC1saXN0L0Q6XFxQcm9qZWN0XFxHaG1BcHBsaWNhdGlvblxcY2xpZW50c1xcZ2htYXBwbGljYXRpb25jbGllbnQvc3JjXFxhcHBcXG1vZHVsZXNcXHRhc2tcXHRhcmdldFxcdGFyZ2V0c1xcdGFyZ2V0LXJlcG9ydFxcdGFyZ2V0LXJlcG9ydC1saXN0XFx0YXJnZXQtcmVwb3J0LWxpc3QuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21vZHVsZXMvdGFzay90YXJnZXQvdGFyZ2V0cy90YXJnZXQtcmVwb3J0L3RhcmdldC1yZXBvcnQtbGlzdC9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXNzZXRzXFxzdHlsZXNcXF9jb25maWcuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUVRLHFCQUFxQixFQUFBO0VBRjdCO0lBS1ksZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixVQUFVO0lBQ1YsY0FBYztJQUNkLFdBQVcsRUFBQTtFQVR2QjtNQVlnQixxQkFBcUI7TUFDckIsV0FBVyxFQUFBO0VBYjNCO1FBaUJ3QixjQUFjO1FBQ2QsV0FBVztRQUNYLFlBQVksRUFBQTtFQW5CcEM7VUFzQjRCLDJCQ25CVixFQUFBO0VESGxCO1FBNEJvQixnQkFBZ0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvdGFzay90YXJnZXQvdGFyZ2V0cy90YXJnZXQtcmVwb3J0L3RhcmdldC1yZXBvcnQtbGlzdC90YXJnZXQtcmVwb3J0LWxpc3QuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0ICcuLi8uLi8uLi8uLi8uLi8uLi8uLi9hc3NldHMvc3R5bGVzL2NvbmZpZyc7XHJcbi50YXJnZXQtcmVwb3J0LW1vZGFsLWRldGFpbCB7XHJcbiAgICAucG9ydGxldC10aXRsZSB7XHJcbiAgICAgICAgcGFkZGluZzogMCAhaW1wb3J0YW50O1xyXG5cclxuICAgICAgICB1bCB7XHJcbiAgICAgICAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAgICAgICAgIGxpIHtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgICAgIGZsb2F0OiBsZWZ0O1xyXG5cclxuICAgICAgICAgICAgICAgICYuaWNvbiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogNDhweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA0OHB4O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgJi5idG4tY2xvc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAkYm9yZGVyQ29sb3I7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaDQge1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIiwiJGRlZmF1bHQtY29sb3I6ICMyMjI7XHJcbiRmb250LWZhbWlseTogXCJBcmlhbFwiLCB0YWhvbWEsIEhlbHZldGljYSBOZXVlO1xyXG4kY29sb3ItYmx1ZTogIzM1OThkYztcclxuJG1haW4tY29sb3I6ICMwMDc0NTU7XHJcbiRib3JkZXJDb2xvcjogI2RkZDtcclxuJHNlY29uZC1jb2xvcjogI2IwMWExZjtcclxuJHRhYmxlLWJhY2tncm91bmQtY29sb3I6ICMwMDk2ODg7XHJcbiRibHVlOiAjMDA3YmZmO1xyXG4kZGFyay1ibHVlOiAjMDA3MkJDO1xyXG4kYnJpZ2h0LWJsdWU6ICNkZmYwZmQ7XHJcbiRpbmRpZ286ICM2NjEwZjI7XHJcbiRwdXJwbGU6ICM2ZjQyYzE7XHJcbiRwaW5rOiAjZTgzZThjO1xyXG4kcmVkOiAjZGMzNTQ1O1xyXG4kb3JhbmdlOiAjZmQ3ZTE0O1xyXG4keWVsbG93OiAjZmZjMTA3O1xyXG4kZ3JlZW46ICMyOGE3NDU7XHJcbiR0ZWFsOiAjMjBjOTk3O1xyXG4kY3lhbjogIzE3YTJiODtcclxuJHdoaXRlOiAjZmZmO1xyXG4kZ3JheTogIzg2OGU5NjtcclxuJGdyYXktZGFyazogIzM0M2E0MDtcclxuJHByaW1hcnk6ICMwMDdiZmY7XHJcbiRzZWNvbmRhcnk6ICM2Yzc1N2Q7XHJcbiRzdWNjZXNzOiAjMjhhNzQ1O1xyXG4kaW5mbzogIzE3YTJiODtcclxuJHdhcm5pbmc6ICNmZmMxMDc7XHJcbiRkYW5nZXI6ICNkYzM1NDU7XHJcbiRsaWdodDogI2Y4ZjlmYTtcclxuJGRhcms6ICMzNDNhNDA7XHJcbiRsYWJlbC1jb2xvcjogIzY2NjtcclxuJGJhY2tncm91bmQtY29sb3I6ICNFQ0YwRjE7XHJcbiRib3JkZXJBY3RpdmVDb2xvcjogIzgwYmRmZjtcclxuJGJvcmRlclJhZGl1czogMDtcclxuJGRhcmtCbHVlOiAjNDVBMkQyO1xyXG4kbGlnaHRHcmVlbjogIzI3YWU2MDtcclxuJGxpZ2h0LWJsdWU6ICNmNWY3Zjc7XHJcbiRicmlnaHRHcmF5OiAjNzU3NTc1O1xyXG4kbWF4LXdpZHRoLW1vYmlsZTogNzY4cHg7XHJcbiRtYXgtd2lkdGgtdGFibGV0OiA5OTJweDtcclxuJG1heC13aWR0aC1kZXNrdG9wOiAxMjgwcHg7XHJcblxyXG4vLyBCRUdJTjogTWFyZ2luXHJcbkBtaXhpbiBuaC1tZygkcGl4ZWwpIHtcclxuICAgIG1hcmdpbjogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ3QoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tdG9wOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1nYigkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1ib3R0b206ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWdsKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLWxlZnQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWdyKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBNYXJnaW5cclxuXHJcbi8vIEJFR0lOOiBQYWRkaW5nXHJcbkBtaXhpbiBuaC1wZCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmc6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGR0KCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy10b3A6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGRiKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy1ib3R0b206ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGRsKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy1sZWZ0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkcigkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctcmlnaHQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vLyBFTkQ6IFBhZGRpbmdcclxuXHJcbi8vIEJFR0lOOiBXaWR0aFxyXG5AbWl4aW4gbmgtd2lkdGgoJHdpZHRoKSB7XHJcbiAgICB3aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbiAgICBtaW4td2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG4gICAgbWF4LXdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBXaWR0aFxyXG5cclxuLy8gQkVHSU46IEljb24gU2l6ZVxyXG5AbWl4aW4gbmgtc2l6ZS1pY29uKCRzaXplKSB7XHJcbiAgICB3aWR0aDogJHNpemU7XHJcbiAgICBoZWlnaHQ6ICRzaXplO1xyXG4gICAgZm9udC1zaXplOiAkc2l6ZTtcclxufVxyXG5cclxuLy8gRU5EOiBJY29uIFNpemVcclxuIl19 */"

/***/ }),

/***/ "./src/app/modules/task/target/targets/target-report/target-report-list/target-report-list.component.ts":
/*!**************************************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-report/target-report-list/target-report-list.component.ts ***!
  \**************************************************************************************************************/
/*! exports provided: TargetReportListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetReportListComponent", function() { return TargetReportListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _const_type_status_report_const__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../const/type-status-report.const */ "./src/app/modules/task/target/targets/target-report/const/type-status-report.const.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _viewmodel_target_tree_viewmodel__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../viewmodel/target-tree.viewmodel */ "./src/app/modules/task/target/targets/viewmodel/target-tree.viewmodel.ts");
/* harmony import */ var _viewmodel_target_viewmodel__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../viewmodel/target.viewmodel */ "./src/app/modules/task/target/targets/viewmodel/target.viewmodel.ts");
/* harmony import */ var _viewmodel_target_render_result_viewmodel__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../viewmodel/target-render-result.viewmodel */ "./src/app/modules/task/target/targets/viewmodel/target-render-result.viewmodel.ts");
/* harmony import */ var _targets_form_detail_targets_form_detail_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../targets-form-detail/targets-form-detail.component */ "./src/app/modules/task/target/targets/targets-form-detail/targets-form-detail.component.ts");
/* harmony import */ var _target_group_tree_target_group_tree_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../target-group-tree/target-group-tree.component */ "./src/app/modules/task/target/targets/target-group-tree/target-group-tree.component.ts");











var TargetReportListComponent = /** @class */ (function () {
    function TargetReportListComponent(data, dialogRef, dialog) {
        this.data = data;
        this.dialogRef = dialogRef;
        this.dialog = dialog;
        this.isShowTree = false;
        this.listItems = [];
        this.listData = [];
        this.listTargetGroupResult = [];
    }
    TargetReportListComponent.prototype.ngOnInit = function () {
        if (this.data.listTarget) {
            // _.each(this.data.listTarget, (item: TargetDetailViewModel) => {
            //     target = new TargetTreeViewModel();
            // });
            // this.listItems = this.data.listTarget;
            // this.typeStatus = this.data.type;
            this.convertData(this.data.listTarget, this.data.type);
        }
    };
    TargetReportListComponent.prototype.update = function (target) {
        var dialogRef = this.dialog.open(_targets_form_detail_targets_form_detail_component__WEBPACK_IMPORTED_MODULE_9__["TargetsFormDetailComponent"], {
            id: 'targetDetailDialog',
            data: { id: target.id },
            disableClose: true
        });
    };
    TargetReportListComponent.prototype.convertData = function (listTarget, typeStatus) {
        console.log(listTarget);
        var dateTimeNow = new Date(moment__WEBPACK_IMPORTED_MODULE_5__().format('YYYY-MM-DD'));
        if (typeStatus === _const_type_status_report_const__WEBPACK_IMPORTED_MODULE_3__["TypeStatusReport"].progressNotComplete) {
            var listItems = lodash__WEBPACK_IMPORTED_MODULE_4__["filter"](listTarget, function (item) {
                return !item.isDelete && item.status === 0 && new Date(item.endDate) >= dateTimeNow;
            });
            this.renderTree(listItems, 'Đúng tiến độ', 'green');
        }
        else if (typeStatus === _const_type_status_report_const__WEBPACK_IMPORTED_MODULE_3__["TypeStatusReport"].overDueNotComplete) {
            var listItems = lodash__WEBPACK_IMPORTED_MODULE_4__["filter"](listTarget, function (item) {
                return !item.isDelete && item.status === 0 && new Date(item.endDate) < dateTimeNow;
            });
            this.renderTree(listItems, 'Quá hạn', 'red');
        }
        else if (typeStatus === _const_type_status_report_const__WEBPACK_IMPORTED_MODULE_3__["TypeStatusReport"].progressComplete) {
            var listItems = lodash__WEBPACK_IMPORTED_MODULE_4__["filter"](listTarget, function (item) {
                return !item.isDelete && item.status === 1 && item.endDate >= item.completedDate;
            });
            this.renderTree(listItems, 'Đúng tiến độ', '#45A2D2');
        }
        else if (typeStatus === _const_type_status_report_const__WEBPACK_IMPORTED_MODULE_3__["TypeStatusReport"].overDueComplete) {
            var listItems = lodash__WEBPACK_IMPORTED_MODULE_4__["filter"](listTarget, function (item) {
                return !item.isDelete && item.status === 1 && item.endDate < item.completedDate;
            });
            this.renderTree(listItems, 'Chậm tiến độ', 'orange');
        }
        else if (typeStatus === _const_type_status_report_const__WEBPACK_IMPORTED_MODULE_3__["TypeStatusReport"].deleted) {
            var listItems = lodash__WEBPACK_IMPORTED_MODULE_4__["filter"](listTarget, function (item) {
                return item.isDelete;
            });
            this.renderTree(listItems, 'Đã hủy', 'lightGrey');
        }
        else {
            this.listData = [];
            this.listTargetGroupResult = [];
            var listItemsProgressNotComplete = lodash__WEBPACK_IMPORTED_MODULE_4__["filter"](listTarget, function (item) {
                return !item.isDelete && item.status === 0 && new Date(item.endDate) >= dateTimeNow;
            });
            if (listItemsProgressNotComplete && listItemsProgressNotComplete.length > 0) {
                this.renderTree(listItemsProgressNotComplete, 'Đúng tiến độ', 'green');
            }
            var listItemsOverDueNotCompelte = lodash__WEBPACK_IMPORTED_MODULE_4__["filter"](listTarget, function (item) {
                return !item.isDelete && item.status === 0 && new Date(item.endDate) < dateTimeNow;
            });
            if (listItemsOverDueNotCompelte && listItemsOverDueNotCompelte.length > 0) {
                this.renderTree(listItemsOverDueNotCompelte, 'Quá hạn', 'red');
            }
            var listItemsProgressComplete = lodash__WEBPACK_IMPORTED_MODULE_4__["filter"](listTarget, function (item) {
                return !item.isDelete && item.status === 1 && item.endDate >= item.completedDate;
            });
            if (listItemsProgressComplete && listItemsProgressComplete.length > 0) {
                this.renderTree(listItemsProgressComplete, 'Đúng tiến độ', '#45A2D2');
            }
            var listItemsOverDueComplete = lodash__WEBPACK_IMPORTED_MODULE_4__["filter"](listTarget, function (item) {
                return !item.isDelete && item.status === 1 && item.endDate < item.completedDate;
            });
            if (listItemsOverDueComplete && listItemsOverDueComplete.length > 0) {
                this.renderTree(listItemsOverDueComplete, 'Chậm tiến độ', 'orange');
            }
            var listItemsDeleted = lodash__WEBPACK_IMPORTED_MODULE_4__["filter"](listTarget, function (item) {
                return item.isDelete;
            });
            if (listItemsDeleted && listItemsDeleted.length > 0) {
                this.renderTree(listItemsDeleted, 'Đã xóa', 'lightGrey');
            }
            this.listData.push(new _viewmodel_target_render_result_viewmodel__WEBPACK_IMPORTED_MODULE_8__["TargetTableResultViewModel"]('', '', 0, this.listTargetGroupResult));
        }
    };
    TargetReportListComponent.prototype.renderTree = function (listItems, groupName, color) {
        var targetGroupResult = new _viewmodel_target_render_result_viewmodel__WEBPACK_IMPORTED_MODULE_8__["TargetGroupResultViewModel"]();
        targetGroupResult.targetGroupName = groupName;
        targetGroupResult.targetGroupColor = color;
        var targetTree = [];
        lodash__WEBPACK_IMPORTED_MODULE_4__["each"](listItems, function (item) {
            var target = new _viewmodel_target_tree_viewmodel__WEBPACK_IMPORTED_MODULE_6__["TargetTreeViewModel"]();
            target.id = item.id;
            target.name = item.name;
            var data = new _viewmodel_target_viewmodel__WEBPACK_IMPORTED_MODULE_7__["TargetViewModel"]();
            data.id = item.id;
            data.name = item.name;
            data.officeId = item.officeId;
            data.officeName = item.officeName;
            data.totalTask = item.totalTask;
            data.totalComment = item.totalComment;
            data.endDate = item.endDate;
            data.percentCompleted = item.percentCompleted;
            data.weight = item.weight;
            data.status = item.status;
            target.data = data;
            targetTree.push(target);
        });
        targetGroupResult.targets = targetTree;
        this.listTree = targetGroupResult;
        this.isShowTree = true;
        this.listTargetGroupResult.push(targetGroupResult);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_target_group_tree_target_group_tree_component__WEBPACK_IMPORTED_MODULE_10__["TargetGroupTreeComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _target_group_tree_target_group_tree_component__WEBPACK_IMPORTED_MODULE_10__["TargetGroupTreeComponent"])
    ], TargetReportListComponent.prototype, "targetGroupTreeComponent", void 0);
    TargetReportListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-target-report-list',
            template: __webpack_require__(/*! ./target-report-list.component.html */ "./src/app/modules/task/target/targets/target-report/target-report-list/target-report-list.component.html"),
            styles: [__webpack_require__(/*! ./target-report-list.component.scss */ "./src/app/modules/task/target/targets/target-report/target-report-list/target-report-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"]])
    ], TargetReportListComponent);
    return TargetReportListComponent;
}());



/***/ }),

/***/ "./src/app/modules/task/target/targets/target-report/target-report.component.html":
/*!****************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-report/target-report.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span i18n=\"@@targetReport\">\r\n        Báo cáo mục tiêu theo đơn vị\r\n    </span>\r\n</h1>\r\n\r\n<div class=\"cm-mgb-10\">\r\n    <mat-expansion-panel [expanded]=\"true\">\r\n        <mat-expansion-panel-header class=\"panel-header\">\r\n            <mat-panel-title class=\"title\">\r\n                Loại báo cáo\r\n            </mat-panel-title>\r\n        </mat-expansion-panel-header>\r\n        <div class=\"item cm-pdt-15 cm-pdb-15\">\r\n            <label class=\"col-sm-5 cm-pdt-5\">Loại báo cáo</label>\r\n            <ghm-select [(ngModel)]=\"typeReportSelected\"\r\n                        [multiple]=\"false\"\r\n                        (itemSelected)=\"search()\"\r\n                        [data]=\"listTypeSelect\">\r\n            </ghm-select>\r\n        </div>\r\n    </mat-expansion-panel>\r\n</div>\r\n<div class=\"cm-mgb-10\"\r\n     *ngIf=\"typeReportSelected === targetReportType.statisticsByUnits || typeReportSelected === targetReportType.statisticsByPersonal\">\r\n    <mat-expansion-panel [expanded]=\"true\">\r\n        <mat-expansion-panel-header class=\"panel-header\">\r\n            <mat-panel-title class=\"title\">\r\n                Thông tin tìm kiếm\r\n            </mat-panel-title>\r\n        </mat-expansion-panel-header>\r\n        <div class=\"wrap-search cm-pdt-15 cm-pdb-15\">\r\n            <div class=\"item\">\r\n                <label class=\"col-sm-5 cm-pdt-5\">Chọn thời gian</label>\r\n                <ghm-select-datetime [setStartDate]=\"infoSearch.startDate\" [startDate]=\"infoSearch.startDate\"\r\n                                     [endDate]=\"infoSearch.endDate\"\r\n                                     (selectDate)=\"selectDate($event)\"></ghm-select-datetime>\r\n            </div>\r\n            <div class=\"item cm-pdt-10\">\r\n                <label class=\"col-sm-5 cm-pdt-5\">Tìm kiếm thời gian theo</label>\r\n                <ghm-select [(ngModel)]=\"infoSearch.typeSearchTime\" [selectedItem]=\"\"\r\n                            [data]=\"listTypeSearchTime\"></ghm-select>\r\n            </div>\r\n            <div class=\"item cm-pdt-10\" *ngIf=\"typeReportSelected === targetReportType.statisticsByPersonal\">\r\n                <label class=\"col-sm-5 cm-pdt-5\"> Đơn vị</label>\r\n                <ghm-select [elementId]=\"'selectOfficeInList'\"\r\n                            [icon]=\"'fa fa-search'\"\r\n                            [liveSearch]=\"true\"\r\n                            [(ngModel)]=\"infoSearch.officeId\"\r\n                            [url]=\"urlOfficeSuggestion\"\r\n                            i18n-title=\"@@selectOffice\"\r\n                            [title]=\"'-- Chọn phòng ban --'\"\r\n                ></ghm-select>\r\n            </div>\r\n            <div class=\"item cm-pdt-10\">\r\n                <label class=\"col-sm-5 cm-pdt-5\">Bảng mục tiêu</label>\r\n                <ghm-select *ngIf=\"isShowTable\" [(ngModel)]=\"infoSearch.tableTargetId\"\r\n                            [data]=\"listTargetTable\"></ghm-select>\r\n            </div>\r\n\r\n            <div class=\"item cm-pdt-10\">\r\n                <label class=\"col-sm-5 cm-pdt-5\">Loại mục tiêu</label>\r\n                <ghm-select [(ngModel)]=\"infoSearch.targetType\" [data]=\"listTypeTarget\"></ghm-select>\r\n            </div>\r\n            <!--<div class=\"item cm-pdt-10\">-->\r\n            <!--<label class=\"col-sm-5 cm-pdt-5\"></label>-->\r\n            <!--<mat-checkbox [(ngModel)]=\"infoSearch.showOfficeHaveTarget\" [color]=\"'primary'\">-->\r\n            <!--Chỉ hiện thị các phòng ban có mục tiêu-->\r\n            <!--</mat-checkbox>-->\r\n            <!--</div>-->\r\n            <div class=\"center\">\r\n                <ghm-button classes=\"btn blue cm-mgt-10\" (click)=\"search()\">\r\n                    Tìm kiếm\r\n                </ghm-button>\r\n            </div>\r\n        </div>\r\n    </mat-expansion-panel>\r\n</div>\r\n<div class=\"cm-mgb-10\"\r\n     *ngIf=\"typeReportSelected === targetReportType.statisticsByUnits || typeReportSelected === targetReportType.statisticsByPersonal\">\r\n    <mat-expansion-panel [expanded]=\"true\">\r\n        <mat-expansion-panel-header class=\"panel-header\">\r\n            <mat-panel-title class=\"title\">\r\n                Biểu đồ báo cáo\r\n            </mat-panel-title>\r\n        </mat-expansion-panel-header>\r\n        <div class=\"chart-report col-sm-6\">\r\n            <div style=\"...\">\r\n                <ngx-charts-pie-chart *ngIf=\"isDataLoaded\"\r\n                                      [results]=\"progressData\"\r\n                                      [view]=\"view\"\r\n                                      [scheme]=\"colorScheme\"\r\n                                      [tooltipDisabled]=\"true\"\r\n                                      [tooltipText]=\"''\"\r\n                                      [animations]=\"true\"\r\n                >\r\n                </ngx-charts-pie-chart>\r\n            </div>\r\n        </div>\r\n        <div class=\"sub-chart-report col-sm-6 cm-mgt-10\">\r\n            <!--<ghm-select></ghm-select>-->\r\n            <h4><b>Mô tả màu sắc</b></h4>\r\n            <div class=\"item\">\r\n                <div class=\"notComplete cm-bg-green cm-pd-10 cm-mgr-10 cm-mgb-10\">\r\n                </div>\r\n                <span>\r\n                    Chưa hoàn thành\r\n                </span>\r\n            </div>\r\n            <div class=\"item\">\r\n                <div class=\"overDue cm-bg-red cm-pd-10 cm-mgr-10 cm-mgb-10\">\r\n\r\n                </div>\r\n                <span>\r\n                    Quá hạn\r\n                </span>\r\n            </div>\r\n            <div class=\"item\">\r\n                <div class=\"completed cm-bg-blue cm-pd-10 cm-mgr-10 cm-mgb-10\">\r\n\r\n                </div>\r\n                <span>\r\n                    Hoàn thành\r\n                </span>\r\n            </div>\r\n            <div class=\"item\">\r\n                <div class=\"completedOverDue cm-bg-orange cm-pd-10 cm-mgr-10 cm-mgb-10\">\r\n\r\n                </div>\r\n                <span>\r\n                    Hoàn thành chậm tiến độ\r\n                </span>\r\n            </div>\r\n            <div class=\"item\">\r\n                <div class=\"dontHaveData cm-bg-white cm-pd-10 cm-mgr-10 cm-mgb-10\">\r\n\r\n                </div>\r\n                <span>\r\n                    Không có dữ liệu\r\n                </span>\r\n            </div>\r\n        </div>\r\n    </mat-expansion-panel>\r\n</div>\r\n<div class=\"cm-mgb-10\"\r\n     *ngIf=\"typeReportSelected === targetReportType.statisticsByUnits || typeReportSelected === targetReportType.statisticsByPersonal\">\r\n    <mat-expansion-panel [expanded]=\"true\">\r\n        <mat-expansion-panel-header class=\"panel-header\">\r\n            <mat-panel-title class=\"title\">\r\n                Số liêu báo cáo\r\n            </mat-panel-title>\r\n        </mat-expansion-panel-header>\r\n        <div class=\"wrap-result-report cm-mgt-15\">\r\n            <table class=\"table table-bordered\">\r\n                <thead class=\"cm-bg-gray\">\r\n                <tr>\r\n                    <th class=\"index center middle\" rowspan=\"2\">STT</th>\r\n                    <th *ngIf=\"typeReportSelected === targetReportType.statisticsByUnits\" class=\"center middle\"\r\n                        rowspan=\"2\">Đơn vị\r\n                    </th>\r\n                    <th *ngIf=\"typeReportSelected === targetReportType.statisticsByPersonal\" class=\"center middle\"\r\n                        rowspan=\"2\">Nhân viên\r\n                    </th>\r\n                    <th class=\"center middle\" rowspan=\"2\">Tổng mục tiêu hoạt động</th>\r\n                    <th class=\"center\" colspan=\"2\">Chưa hoàn thành</th>\r\n                    <th class=\"center\" colspan=\"2\">Hoàn thành</th>\r\n                    <th class=\"center middle\" rowspan=\"2\">Hủy</th>\r\n                </tr>\r\n                <tr>\r\n                    <th class=\"center color-green\">Đúng tiến độ</th>\r\n                    <th class=\"center color-red\">Quá hạn</th>\r\n                    <th class=\"center text-color-blue\">Đúng tiến độ</th>\r\n                    <th class=\"center color-orange\">Chậm tiến độ</th>\r\n                </tr>\r\n                </thead>\r\n                <tbody>\r\n                <tr *ngFor=\"let item of listTargetReport; let i = index\">\r\n                    <td class=\"center\">{{i + 1}}</td>\r\n                    <td *ngIf=\"typeReportSelected === targetReportType.statisticsByUnits\">{{item.officeName}}</td>\r\n                    <td *ngIf=\"typeReportSelected === targetReportType.statisticsByPersonal\">\r\n                        <div>{{item.fullName}}</div>\r\n                        <div><i>{{item.officeName}}</i></div>\r\n                    </td>\r\n                    <td class=\"center\"><a\r\n                        (click)=\"showListTarget(item.officeId, typeStatusReport.all)\">{{item.totalTarget}}</a></td>\r\n                    <ng-container *ngIf=\"item.progressNotComplete > 0; else progressNotComplete\">\r\n                        <td class=\"center\"><a\r\n                            (click)=\"showListTarget(item.officeId, typeStatusReport.progressNotComplete)\"\r\n                            class=\"color-green\"\r\n                            [class.text-decoration-underline]=\"item.progressNotComplete > 0\">{{item.progressNotComplete}}</a>\r\n                        </td>\r\n                    </ng-container>\r\n                    <ng-template #progressNotComplete>\r\n                        <td class=\"center color-green\">0</td>\r\n                    </ng-template>\r\n\r\n                    <ng-container *ngIf=\"item.overDueNotComplete > 0; else overDueNotComplete\">\r\n                        <td class=\"center\"><a\r\n                            (click)=\"showListTarget(item.officeId, typeStatusReport.overDueNotComplete)\"\r\n                            class=\"color-red\"\r\n                            [class.text-decoration-underline]=\"item.overDueNotComplete > 0\">{{item.overDueNotComplete}}</a>\r\n                        </td>\r\n                    </ng-container>\r\n                    <ng-template #overDueNotComplete>\r\n                        <td class=\"center color-red\">0</td>\r\n                    </ng-template>\r\n\r\n                    <ng-container *ngIf=\"item.progressComplete > 0; else progressComplete\">\r\n                        <td class=\"center\"><a (click)=\"showListTarget(item.officeId, typeStatusReport.progressComplete)\"\r\n                                              class=\"text-color-blue\"\r\n                                              [class.text-decoration-underline]=\"item.progressComplete > 0\">{{item.progressComplete}}</a>\r\n                        </td>\r\n                    </ng-container>\r\n                    <ng-template #progressComplete>\r\n                        <td class=\"center text-color-blue\">0</td>\r\n                    </ng-template>\r\n\r\n                    <ng-container *ngIf=\"item.overDueComplete > 0; else overDueComplete\">\r\n                        <td class=\"center\"><a (click)=\"showListTarget(item.officeId, typeStatusReport.overDueComplete)\"\r\n                                              class=\"color-orange\"\r\n                                              [class.text-decoration-underline]=\"item.overDueComplete > 0\">{{item.overDueComplete}}</a>\r\n                        </td>\r\n                    </ng-container>\r\n                    <ng-template #overDueComplete>\r\n                        <td class=\"center color-orange\">0</td>\r\n                    </ng-template>\r\n\r\n                    <ng-container *ngIf=\"item.totalDestroy > 0; else totalDestroy\">\r\n                        <td class=\"center\" (click)=\"showListTarget(item.officeId, typeStatusReport.deleted)\">\r\n                            <a>{{item.totalDestroy}}</a></td>\r\n                    </ng-container>\r\n                    <ng-template #totalDestroy>\r\n                        <td class=\"center\">0</td>\r\n                    </ng-template>\r\n                </tr>\r\n                <tr class=\"cm-bg-gray\">\r\n                    <td rowspan=\"2\" colspan=\"2\" class=\"center middle\">Tổng</td>\r\n                    <td rowspan=\"2\" class=\"center middle\">{{ totalTargetActive }}</td>\r\n                    <td class=\"center\">{{ totalProgressNotComplete }}</td>\r\n                    <td class=\"center\">{{ totalOverDueNotComplete }}</td>\r\n                    <td class=\"center\">{{ totalProgressComplete }}</td>\r\n                    <td class=\"center\">{{ totalOverDueComplete }}</td>\r\n                    <td class=\"center middle\" rowspan=\"2\">{{ totalDelete }}</td>\r\n                </tr>\r\n                <tr class=\"cm-bg-gray\">\r\n                    <td class=\"center\" colspan=\"2\">{{totalNotComplete}}</td>\r\n                    <td class=\"center\" colspan=\"2\">{{totalComplete}}</td>\r\n                </tr>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n    </mat-expansion-panel>\r\n</div>\r\n<!-- for List -->\r\n<div class=\"cm-mgb-10\"\r\n     *ngIf=\"typeReportSelected === targetReportType.listTargetEmployee || typeReportSelected === targetReportType.generalTargetAndTask\">\r\n    <mat-expansion-panel [expanded]=\"true\">\r\n        <mat-expansion-panel-header class=\"panel-header\">\r\n            <mat-panel-title class=\"title\" i18n=\"infoSearch\">\r\n                Thông tin tìm kiếm\r\n            </mat-panel-title>\r\n        </mat-expansion-panel-header>\r\n        <div class=\"wrap-search cm-pdt-15 cm-pdb-15\">\r\n            <div class=\"item\">\r\n                <label class=\"col-sm-5 cm-pdt-5\" i18n=\"@@choseTime\">Chọn thời gian</label>\r\n                <ghm-select-datetime [setStartDate]=\"infoSearch.startDate\" [startDate]=\"infoSearch.startDate\"\r\n                                     [endDate]=\"infoSearch.endDate\"\r\n                                     (selectDate)=\"selectDate($event)\"></ghm-select-datetime>\r\n            </div>\r\n            <div class=\"item cm-pdt-10\">\r\n                <label class=\"col-sm-5 cm-pdt-5\" i18n=\"@@searchByTime\">Tìm kiếm thời gian theo</label>\r\n                <ghm-select [(ngModel)]=\"infoSearch.typeSearchTime\" [selectedItem]=\"\"\r\n                            [data]=\"listTypeSearchTime\"></ghm-select>\r\n            </div>\r\n            <div class=\"item cm-pdt-10\">\r\n                <label class=\"col-sm-5 cm-pdt-5\" i18n=\"@@office\"> Đơn vị</label>\r\n                <ghm-select [elementId]=\"'selectOfficeInList'\"\r\n                            [icon]=\"'fa fa-search'\"\r\n                            [liveSearch]=\"true\"\r\n                            [(ngModel)]=\"infoSearch.officeId\"\r\n                            [url]=\"urlOfficeSuggestion\"\r\n                            i18n-title=\"@@selectOffice\"\r\n                            (itemSelected)=\"getListUserByOffice($event)\"\r\n                            [title]=\"'-- Chọn phòng ban --'\"\r\n                ></ghm-select>\r\n            </div>\r\n            <div class=\"item cm-pdt-10\">\r\n                <label class=\"col-sm-5 cm-pdt-5\" i18n=\"@@employee\">Nhân viên</label>\r\n                <ghm-select [elementId]=\"'selectUser'\"\r\n                            [icon]=\"'fa fa-search'\"\r\n                            [liveSearch]=\"true\"\r\n                            [(ngModel)]=\"infoSearch.userId\"\r\n                            [data]=\"listUserPicker\"\r\n                            i18n-title=\"@@selectUser\"\r\n                            [title]=\"'-- Chọn nhân viên --'\"\r\n                ></ghm-select>\r\n            </div>\r\n            <div class=\"item cm-pdt-10\">\r\n                <label class=\"col-sm-5 cm-pdt-5\" i18n=\"tableTarget\">Bảng mục tiêu</label>\r\n                <ghm-select *ngIf=\"isShowTable\" [(ngModel)]=\"infoSearch.tableTargetId\"\r\n                            [data]=\"listTargetTable\"></ghm-select>\r\n            </div>\r\n            <div class=\"item cm-pdt-10\">\r\n                <label class=\"col-sm-5 cm-pdt-5\" i18n=\"@@typeTarget\">Loại mục tiêu</label>\r\n                <ghm-select i18n-title=\"@@selectTypeTarget\" [elementId]=\"'selectTypeTarget'\"\r\n                            [(ngModel)]=\"infoSearch.targetType\" [data]=\"listTypeTarget\"></ghm-select>\r\n            </div>\r\n            <!--<div class=\"item cm-pdt-10\">-->\r\n            <!--<label class=\"col-sm-5 cm-pdt-5\"></label>-->\r\n            <!--<mat-checkbox [(ngModel)]=\"infoSearch.showOfficeHaveTarget\" [color]=\"'primary'\">-->\r\n            <!--Chỉ hiện thị các phòng ban có mục tiêu-->\r\n            <!--</mat-checkbox>-->\r\n            <!--</div>-->\r\n            <div class=\"item cm-pdt-10\">\r\n                <label class=\"col-sm-5 cm-pdt-5\" i18n=\"@@status\">Trạng thái</label>\r\n                <ghm-select [(ngModel)]=\"infoSearch.status\" [data]=\"listTypeStatus\"></ghm-select>\r\n            </div>\r\n            <div class=\"center\">\r\n                <ghm-button classes=\"btn blue cm-mgt-10\" (click)=\"searchForList()\" i18n=\"@@search\">\r\n                    Tìm kiếm\r\n                </ghm-button>\r\n            </div>\r\n        </div>\r\n    </mat-expansion-panel>\r\n</div>\r\n<!-- for result search -->\r\n<div class=\"cm-mgb-10\"\r\n     *ngIf=\"(typeReportSelected === targetReportType.listTargetEmployee || typeReportSelected === targetReportType.generalTargetAndTask) && isShowResultSearch\">\r\n    <mat-expansion-panel [expanded]=\"true\">\r\n        <mat-expansion-panel-header class=\"panel-header\">\r\n            <mat-panel-title class=\"title\" i18n=\"infoSearch\">\r\n                Kết quả tìm kiếm\r\n            </mat-panel-title>\r\n        </mat-expansion-panel-header>\r\n        <div class=\"row wrap-search cm-pdt-15 cm-pdb-15\">\r\n            <pre>{{files | json}}</pre>\r\n            <table class=\"table table-bordered\">\r\n                <thead>\r\n                    <tr>\r\n                        <th>Tên</th>\r\n                        <th>Đối tượng</th>\r\n                        <th>Nhân viên/Đơn vị</th>\r\n                        <th>Trọng số</th>\r\n                        <th>Kết quả</th>\r\n                        <th>Tiến độ công việc</th>\r\n                        <th>Trạng thái</th>\r\n                        <th>Ngày bắt đầu</th>\r\n                        <th>Hạn hoàn thành</th>\r\n                        <th>Ngày hoàn thành</th>\r\n                    </tr>\r\n                </thead>\r\n                <tbody>\r\n                    <tr *ngFor=\"let item of listTreeSearchTarget; let i = index\">\r\n                        <td>{{item.data.name}}</td>\r\n                        <td *ngIf=\"item.data.childCount === 0\">Mục tiêu</td>\r\n                        <td *ngIf=\"item.data.childCount > 0\">Mục tiêu con</td>\r\n                        <td>\r\n                            <div class=\"fa fa-user-circle-o\">{{item.data.fullName}}</div>\r\n                            <div><span><i class=\"fa fa-sitemap\"></i></span> {{item.data.officeName}}</div>\r\n                        </td>\r\n                        <td class=\"center\">{{item.data.weight}}</td>\r\n                        <td class=\"center\">{{item.data.percentCompleted}} %</td>\r\n                        <td class=\"center\">{{item.data.totalTaskComplete}} / {{item.data.totalTask}}</td>\r\n                        <td class=\"center\">{{ item.data.status === 0 ? 'Chưa hoàn thành' : 'hoàn thành'}}</td>\r\n                        <td class=\"center\">{{item.data.startDate}}</td>\r\n                        <td class=\"center\">{{item.data.endDate}}</td>\r\n                        <td class=\"center\">{{item.data.completedDate}}</td>\r\n                    </tr>\r\n                </tbody>\r\n\r\n            </table>\r\n        </div>\r\n    </mat-expansion-panel>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/task/target/targets/target-report/target-report.component.scss":
/*!****************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-report/target-report.component.scss ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".item {\n  display: flex;\n  width: 40%;\n  margin-left: 30%; }\n  .item mat-checkbox {\n    width: 100%; }\n  .sub-chart-report .item {\n  margin-left: 0px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3RhcmdldC90YXJnZXRzL3RhcmdldC1yZXBvcnQvRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFwcFxcbW9kdWxlc1xcdGFza1xcdGFyZ2V0XFx0YXJnZXRzXFx0YXJnZXQtcmVwb3J0XFx0YXJnZXQtcmVwb3J0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksYUFBYTtFQUNiLFVBQVM7RUFDVCxnQkFBZSxFQUFBO0VBSG5CO0lBS1EsV0FBVyxFQUFBO0VBR25CO0VBRVEsMkJBQTJCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9tb2R1bGVzL3Rhc2svdGFyZ2V0L3RhcmdldHMvdGFyZ2V0LXJlcG9ydC90YXJnZXQtcmVwb3J0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3NyYy9hc3NldHMvc3R5bGVzL2NvbmZpZ1wiO1xyXG4uaXRlbSB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgd2lkdGg6NDAlO1xyXG4gICAgbWFyZ2luLWxlZnQ6MzAlO1xyXG4gICAgbWF0LWNoZWNrYm94IHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxufVxyXG4uc3ViLWNoYXJ0LXJlcG9ydCB7XHJcbiAgICAuaXRlbSB7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDBweCAhaW1wb3J0YW50OztcclxuICAgIH1cclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/modules/task/target/targets/target-report/target-report.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-report/target-report.component.ts ***!
  \**************************************************************************************/
/*! exports provided: TargetReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetReportComponent", function() { return TargetReportComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _const_target_report_type_const__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./const/target-report-type.const */ "./src/app/modules/task/target/targets/target-report/const/target-report-type.const.ts");
/* harmony import */ var _model_info_search_viewmodel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./model/info-search.viewmodel */ "./src/app/modules/task/target/targets/target-report/model/info-search.viewmodel.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _const_target_type_search_time_const__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./const/target-type-search-time.const */ "./src/app/modules/task/target/targets/target-report/const/target-type-search-time.const.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _target_report_list_target_report_list_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./target-report-list/target-report-list.component */ "./src/app/modules/task/target/targets/target-report/target-report-list/target-report-list.component.ts");
/* harmony import */ var _const_type_status_report_const__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./const/type-status-report.const */ "./src/app/modules/task/target/targets/target-report/const/type-status-report.const.ts");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _target_table_service_target_table_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../target-table/service/target-table.service */ "./src/app/modules/task/target/target-table/service/target-table.service.ts");
/* harmony import */ var _services_target_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../services/target.service */ "./src/app/modules/task/target/targets/services/target.service.ts");
/* harmony import */ var _constant_type_target_const__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../constant/type-target.const */ "./src/app/modules/task/target/targets/constant/type-target.const.ts");
/* harmony import */ var _hr_organization_office_services_office_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../../hr/organization/office/services/office.service */ "./src/app/modules/hr/organization/office/services/office.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _shareds_components_nh_user_picker_nh_user_picker_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../../../../shareds/components/nh-user-picker/nh-user-picker.service */ "./src/app/shareds/components/nh-user-picker/nh-user-picker.service.ts");
/* harmony import */ var _constant_target_status_const__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../constant/target-status.const */ "./src/app/modules/task/target/targets/constant/target-status.const.ts");


















var TargetReportComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TargetReportComponent, _super);
    function TargetReportComponent(targetTableService, diaLog, userPickerService, targetService, officeService) {
        var _this = _super.call(this) || this;
        _this.targetTableService = targetTableService;
        _this.diaLog = diaLog;
        _this.userPickerService = userPickerService;
        _this.targetService = targetService;
        _this.officeService = officeService;
        _this.targetReportType = _const_target_report_type_const__WEBPACK_IMPORTED_MODULE_2__["TargetReportTypeConst"];
        _this.isDataLoaded = false;
        _this.progressData = [];
        _this.view = [700, 400];
        _this.colorScheme = {
            domain: []
        };
        _this.listTreeSearchTarget = [];
        _this.listOffice = [];
        _this.typeReportSelected = _this.targetReportType.statisticsByUnits;
        _this.infoSearch = new _model_info_search_viewmodel__WEBPACK_IMPORTED_MODULE_3__["InfoSearchViewModel"]();
        _this.targetTypeSearchTimeConst = _const_target_type_search_time_const__WEBPACK_IMPORTED_MODULE_5__["TargetTypeSearchTimeConst"];
        _this.totalRows = 0;
        _this.isShowResultSearch = false;
        _this.isShowTable = false;
        _this.listTargetReport = [];
        _this.typeTargetConst = _constant_type_target_const__WEBPACK_IMPORTED_MODULE_13__["TypeTarget"];
        _this.listTargetTable = [];
        _this.listTypeSelect = [
            { id: _this.targetReportType.statisticsByUnits, name: 'Thống kê mục tiêu theo đơn vị' },
            { id: _this.targetReportType.statisticsByPersonal, name: 'Thống kê mục tiêu nhân viên' },
            { id: _this.targetReportType.listTargetEmployee, name: 'Danh sách mục tiêu nhân viên' },
            { id: _this.targetReportType.generalTargetAndTask, name: 'Danh sách tổng hợp mục tiêu công viêc' }
        ];
        _this.listTypeSearchTime = [
            { id: _this.targetTypeSearchTimeConst.starteDateToCompleteDate, name: 'Khoảng ngày: Ngày bắt đầu - Hạn hoàn thành' },
            { id: _this.targetTypeSearchTimeConst.startDate, name: 'Ngày bắt đầu' },
            { id: _this.targetTypeSearchTimeConst.createDate, name: 'Ngày tạo' },
            { id: _this.targetTypeSearchTimeConst.endDate, name: 'Hạn hoàn thành' },
            { id: _this.targetTypeSearchTimeConst.completeDate, name: 'Ngày hoàn thành' }
        ];
        _this.listTypeTarget = [
            { id: _this.typeTargetConst.all, name: 'Tất cả' },
            { id: _this.typeTargetConst.personal, name: 'Mục tiêu nhân viên' },
            { id: _this.typeTargetConst.office, name: 'Mục tiêu đơn vị' }
        ];
        _this.typeTargetStatus = _constant_target_status_const__WEBPACK_IMPORTED_MODULE_17__["TargetStatus"];
        _this.listTypeStatus = [
            { id: _this.typeTargetStatus.new, name: 'Chưa hoàn thành' },
            { id: _this.typeTargetStatus.finish, name: 'Hoàn thành' },
            { id: _this.typeTargetStatus.all, name: 'Tất cả' }
        ];
        _this.selectedItem = { id: _this.targetReportType.statisticsByUnits, name: 'Thống kê mục tiêu theo đơn vị' };
        _this.totalTargetActive = 0;
        _this.totalProgressNotComplete = 0;
        _this.totalOverDueNotComplete = 0;
        _this.totalProgressComplete = 0;
        _this.totalOverDueComplete = 0;
        _this.totalNotComplete = 0;
        _this.totalComplete = 0;
        _this.totalDelete = 0;
        _this.typeStatusReport = _const_type_status_report_const__WEBPACK_IMPORTED_MODULE_9__["TypeStatusReport"];
        return _this;
    }
    TargetReportComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.urlOfficeSuggestion = _environments_environment__WEBPACK_IMPORTED_MODULE_15__["environment"].apiGatewayUrl + "api/v1/hr/offices/suggestions";
        this.appService.setupPage(this.pageId.TARGET, this.pageId.TARGET_REPORT, 'Quản lý mục tiêu', 'Báo cáo mục tiêu');
        // this.infoSearch.startDate = moment().subtract(1, 'months').format('YYYY-MM-DD');
        // this.infoSearch.endDate = moment().format('YYYY-MM-DD');
        // this.infoSearch.typeSearchTime = 0;
        // this.infoSearch.tableTargetId = '0';
        // this.infoSearch.targetType = 3;
        // this.infoSearch.showOfficeHaveTarget = true;
        this.listTargetTable.push({ id: '0', name: 'Tất cả' });
        this.targetTableService.search().subscribe(function (result) {
            lodash__WEBPACK_IMPORTED_MODULE_6__["each"](result.items, function (item) {
                _this.listTargetTable.push({ id: item.id, name: item.name });
            });
            _this.isShowTable = true;
        });
        // this.officeService.getTree().subscribe((result: any) => {
        //     this.listOffice = result;
        // });
        this.search();
    };
    TargetReportComponent.prototype.search = function () {
        var _this = this;
        this.totalTargetActive = 0;
        this.totalProgressNotComplete = 0;
        this.totalOverDueNotComplete = 0;
        this.totalProgressComplete = 0;
        this.totalOverDueComplete = 0;
        this.totalNotComplete = 0;
        this.totalComplete = 0;
        this.totalDelete = 0;
        this.progressData = [];
        this.isDataLoaded = false;
        this.targetService.searchForReport(this.typeReportSelected, this.infoSearch).subscribe(function (result) {
            _this.listTargetReport = result.items;
            lodash__WEBPACK_IMPORTED_MODULE_6__["each"](_this.listTargetReport, function (item) {
                _this.totalTargetActive += item.totalTarget;
                _this.totalProgressNotComplete += item.progressNotComplete;
                _this.totalOverDueNotComplete += item.overDueNotComplete;
                _this.totalProgressComplete += item.progressComplete;
                _this.totalOverDueComplete += item.overDueComplete;
                _this.totalNotComplete += item.progressNotComplete;
                _this.totalNotComplete += item.overDueNotComplete;
                _this.totalComplete += item.overDueComplete;
                _this.totalComplete += item.progressComplete;
                _this.totalDelete += item.totalDestroy;
            });
            _this.handleValuePieChart();
        });
        this.infoSearch = new _model_info_search_viewmodel__WEBPACK_IMPORTED_MODULE_3__["InfoSearchViewModel"]();
    };
    TargetReportComponent.prototype.searchForList = function () {
        var _this = this;
        this.listTreeSearchTarget = null;
        if (this.infoSearch.targetType === this.typeTargetConst.office) {
            this.targetService.searchOfficeTarget(this.infoSearch.officeId, '', this.infoSearch.startDate, this.infoSearch.endDate, this.infoSearch.tableTargetId, this.infoSearch.status).subscribe(function (result) {
                _this.listTreeSearchTarget = result.items;
            });
        }
        else if (this.infoSearch.targetType === this.typeTargetConst.personal) {
            this.targetService.searchPersonalTarget(this.infoSearch.userId, '', this.infoSearch.startDate, this.infoSearch.endDate, this.infoSearch.tableTargetId, this.infoSearch.status).subscribe(function (result) {
                _this.listTreeSearchTarget = result.items;
            });
        }
        else {
            this.targetService.searchOfficeTarget(this.infoSearch.officeId, '', this.infoSearch.startDate, this.infoSearch.endDate, this.infoSearch.tableTargetId, this.infoSearch.status).subscribe(function (result) {
                _this.listTreeSearchTarget = result.items;
            });
            this.targetService.searchPersonalTarget(this.infoSearch.userId, '', this.infoSearch.startDate, this.infoSearch.endDate, this.infoSearch.tableTargetId, this.infoSearch.status).subscribe(function (result) {
                _this.listTreeSearchTarget.concat(result.items);
            });
        }
        this.isShowResultSearch = true;
    };
    TargetReportComponent.prototype.selectDate = function (value) {
        this.infoSearch.startDate = moment__WEBPACK_IMPORTED_MODULE_4__(value.startDate).format('YYYY-MM-DD').toString();
        this.infoSearch.endDate = moment__WEBPACK_IMPORTED_MODULE_4__(value.endDate).format('YYYY-MM-DD').toString();
    };
    TargetReportComponent.prototype.handleValuePieChart = function () {
        this.progressData.push({ 'name': 'progressNotComplete', 'value': (this.totalProgressNotComplete / this.totalTargetActive) * 100 });
        this.progressData.push({ 'name': 'overDueNotComplete', 'value': (this.totalOverDueNotComplete / this.totalTargetActive) * 100 });
        this.progressData.push({ 'name': 'progressComplete', 'value': (this.totalProgressComplete / this.totalTargetActive) * 100 });
        this.progressData.push({ 'name': 'overDueComplete', 'value': (this.totalOverDueComplete / this.totalTargetActive) * 100 });
        if (this.totalTargetActive === 0) {
            this.colorScheme.domain = ['white'];
            this.progressData.push({ 'name': 'dontHaveValue', 'value': 1 });
        }
        else {
            this.colorScheme.domain = ['forestgreen', 'red', 'dodgerblue', '#f39c12'];
        }
        this.isDataLoaded = true;
    };
    TargetReportComponent.prototype.showListTarget = function (officeId, typeStatus) {
        var data = lodash__WEBPACK_IMPORTED_MODULE_6__["find"](this.listTargetReport, function (item) {
            return item.officeId === officeId;
        });
        var listTargetDiaLog = this.diaLog.open(_target_report_list_target_report_list_component__WEBPACK_IMPORTED_MODULE_8__["TargetReportListComponent"], {
            id: "targetReportList-" + officeId + "-" + typeStatus,
            data: { listTarget: data.target, type: typeStatus },
            disableClose: true,
            width: '60%',
        });
    };
    TargetReportComponent.prototype.getListUserByOffice = function (value) {
        var _this = this;
        this.userPickerService.search('', value.id, 1, 100)
            .subscribe(function (result) {
            var list = [];
            lodash__WEBPACK_IMPORTED_MODULE_6__["each"](result.items, function (item) {
                var user = { id: item.id, name: item.fullName };
                list.push(user);
            });
            _this.listUserPicker = list;
        });
    };
    TargetReportComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-target-report',
            template: __webpack_require__(/*! ./target-report.component.html */ "./src/app/modules/task/target/targets/target-report/target-report.component.html"),
            styles: [__webpack_require__(/*! ./target-report.component.scss */ "./src/app/modules/task/target/targets/target-report/target-report.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_target_table_service_target_table_service__WEBPACK_IMPORTED_MODULE_11__["TargetTableService"], _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatDialog"], _shareds_components_nh_user_picker_nh_user_picker_service__WEBPACK_IMPORTED_MODULE_16__["NhUserPickerService"],
            _services_target_service__WEBPACK_IMPORTED_MODULE_12__["TargetService"], _hr_organization_office_services_office_service__WEBPACK_IMPORTED_MODULE_14__["OfficeService"]])
    ], TargetReportComponent);
    return TargetReportComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_10__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/task/target/targets/target-update-method-calculate-result/target-update-method-caculate-result.scss":
/*!*****************************************************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-update-method-calculate-result/target-update-method-caculate-result.scss ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "::ng-deep .mat-radio-button.mat-primary .mat-radio-ripple .mat-ripple-element {\n  background: none !important; }\n\n::ng-deep .mat-radio-button.mat-primary .mat-radio-inner-circle {\n  background-color: #3598dc !important;\n  /*inner circle color change*/ }\n\n::ng-deep.mat-radio-button.mat-primary.mat-radio-checked .mat-radio-outer-circle {\n  border-color: #3598dc !important;\n  /*outer ring color change*/ }\n\n::ng-deep .mat-expansion-indicator:after {\n  color: #fff !important; }\n\n.target-update-method-calculate-result {\n  width: 600px; }\n\n.target-update-method-calculate-result .portlet-title {\n    padding: 0 !important;\n    margin-bottom: 0px; }\n\n.target-update-method-calculate-result .portlet-title ul {\n      list-style: none;\n      margin-bottom: 0;\n      padding: 0;\n      display: block;\n      width: 100%; }\n\n.target-update-method-calculate-result .portlet-title ul li {\n        display: inline-block;\n        float: left; }\n\n.target-update-method-calculate-result .portlet-title ul li.icon a {\n          display: block;\n          width: 48px;\n          height: 48px; }\n\n.target-update-method-calculate-result .portlet-title ul li.icon a.btn-close {\n            border-left: 1px solid #ddd; }\n\n.target-update-method-calculate-result .portlet-title ul li h4 {\n          margin-top: 15px;\n          margin-left: 15px; }\n\n.target-update-method-calculate-result .portlet-body {\n    padding-top: 0px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3RhcmdldC90YXJnZXRzL3RhcmdldC11cGRhdGUtbWV0aG9kLWNhbGN1bGF0ZS1yZXN1bHQvRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFwcFxcbW9kdWxlc1xcdGFza1xcdGFyZ2V0XFx0YXJnZXRzXFx0YXJnZXQtdXBkYXRlLW1ldGhvZC1jYWxjdWxhdGUtcmVzdWx0XFx0YXJnZXQtdXBkYXRlLW1ldGhvZC1jYWN1bGF0ZS1yZXN1bHQuc2NzcyIsInNyYy9hcHAvbW9kdWxlcy90YXNrL3RhcmdldC90YXJnZXRzL3RhcmdldC11cGRhdGUtbWV0aG9kLWNhbGN1bGF0ZS1yZXN1bHQvRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFzc2V0c1xcc3R5bGVzXFxfY29uZmlnLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDSSwyQkFBMkIsRUFBQTs7QUFHL0I7RUFDSSxvQ0FBd0M7RUFBRSw0QkFBQSxFQUE2Qjs7QUFHM0U7RUFDSSxnQ0FBb0M7RUFBRSwwQkFBQSxFQUEyQjs7QUFHckU7RUFFUSxzQkFBd0IsRUFBQTs7QUFJaEM7RUFDSSxZQUFZLEVBQUE7O0FBRGhCO0lBR1EscUJBQXFCO0lBQ3JCLGtCQUFrQixFQUFBOztBQUoxQjtNQU1ZLGdCQUFnQjtNQUNoQixnQkFBZ0I7TUFDaEIsVUFBVTtNQUNWLGNBQWM7TUFDZCxXQUFXLEVBQUE7O0FBVnZCO1FBYWdCLHFCQUFxQjtRQUNyQixXQUFXLEVBQUE7O0FBZDNCO1VBa0J3QixjQUFjO1VBQ2QsV0FBVztVQUNYLFlBQVksRUFBQTs7QUFwQnBDO1lBc0I0QiwyQkN0Q1YsRUFBQTs7QURnQmxCO1VBNEJvQixnQkFBZ0I7VUFDaEIsaUJBQWlCLEVBQUE7O0FBN0JyQztJQW9DUSxnQkFBZ0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvdGFzay90YXJnZXQvdGFyZ2V0cy90YXJnZXQtdXBkYXRlLW1ldGhvZC1jYWxjdWxhdGUtcmVzdWx0L3RhcmdldC11cGRhdGUtbWV0aG9kLWNhY3VsYXRlLXJlc3VsdC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uLy4uLy4uLy4uLy4uLy4uL2Fzc2V0cy9zdHlsZXMvY29uZmlnXCI7XHJcblxyXG46Om5nLWRlZXAgLm1hdC1yYWRpby1idXR0b24ubWF0LXByaW1hcnkgLm1hdC1yYWRpby1yaXBwbGUgLm1hdC1yaXBwbGUtZWxlbWVudCB7XHJcbiAgICBiYWNrZ3JvdW5kOiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXJhZGlvLWJ1dHRvbi5tYXQtcHJpbWFyeSAubWF0LXJhZGlvLWlubmVyLWNpcmNsZSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItYmx1ZSAhaW1wb3J0YW50OyAvKmlubmVyIGNpcmNsZSBjb2xvciBjaGFuZ2UqL1xyXG59XHJcblxyXG46Om5nLWRlZXAubWF0LXJhZGlvLWJ1dHRvbi5tYXQtcHJpbWFyeS5tYXQtcmFkaW8tY2hlY2tlZCAubWF0LXJhZGlvLW91dGVyLWNpcmNsZSB7XHJcbiAgICBib3JkZXItY29sb3I6ICRjb2xvci1ibHVlICFpbXBvcnRhbnQ7IC8qb3V0ZXIgcmluZyBjb2xvciBjaGFuZ2UqL1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC1leHBhbnNpb24taW5kaWNhdG9yIHtcclxuICAgICY6YWZ0ZXIge1xyXG4gICAgICAgIGNvbG9yOiAkd2hpdGUgIWltcG9ydGFudDtcclxuICAgIH1cclxufVxyXG5cclxuLnRhcmdldC11cGRhdGUtbWV0aG9kLWNhbGN1bGF0ZS1yZXN1bHQge1xyXG4gICAgd2lkdGg6IDYwMHB4O1xyXG4gICAgLnBvcnRsZXQtdGl0bGUge1xyXG4gICAgICAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgdWwge1xyXG4gICAgICAgICAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgICAgICAgICBsaSB7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuXHJcbiAgICAgICAgICAgICAgICAmLmljb24ge1xyXG4gICAgICAgICAgICAgICAgICAgIGEge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDQ4cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogNDhweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJi5idG4tY2xvc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAkYm9yZGVyQ29sb3I7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaDQge1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLnBvcnRsZXQtYm9keSB7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDBweDtcclxuICAgIH1cclxufVxyXG4iLCIkZGVmYXVsdC1jb2xvcjogIzIyMjtcclxuJGZvbnQtZmFtaWx5OiBcIkFyaWFsXCIsIHRhaG9tYSwgSGVsdmV0aWNhIE5ldWU7XHJcbiRjb2xvci1ibHVlOiAjMzU5OGRjO1xyXG4kbWFpbi1jb2xvcjogIzAwNzQ1NTtcclxuJGJvcmRlckNvbG9yOiAjZGRkO1xyXG4kc2Vjb25kLWNvbG9yOiAjYjAxYTFmO1xyXG4kdGFibGUtYmFja2dyb3VuZC1jb2xvcjogIzAwOTY4ODtcclxuJGJsdWU6ICMwMDdiZmY7XHJcbiRkYXJrLWJsdWU6ICMwMDcyQkM7XHJcbiRicmlnaHQtYmx1ZTogI2RmZjBmZDtcclxuJGluZGlnbzogIzY2MTBmMjtcclxuJHB1cnBsZTogIzZmNDJjMTtcclxuJHBpbms6ICNlODNlOGM7XHJcbiRyZWQ6ICNkYzM1NDU7XHJcbiRvcmFuZ2U6ICNmZDdlMTQ7XHJcbiR5ZWxsb3c6ICNmZmMxMDc7XHJcbiRncmVlbjogIzI4YTc0NTtcclxuJHRlYWw6ICMyMGM5OTc7XHJcbiRjeWFuOiAjMTdhMmI4O1xyXG4kd2hpdGU6ICNmZmY7XHJcbiRncmF5OiAjODY4ZTk2O1xyXG4kZ3JheS1kYXJrOiAjMzQzYTQwO1xyXG4kcHJpbWFyeTogIzAwN2JmZjtcclxuJHNlY29uZGFyeTogIzZjNzU3ZDtcclxuJHN1Y2Nlc3M6ICMyOGE3NDU7XHJcbiRpbmZvOiAjMTdhMmI4O1xyXG4kd2FybmluZzogI2ZmYzEwNztcclxuJGRhbmdlcjogI2RjMzU0NTtcclxuJGxpZ2h0OiAjZjhmOWZhO1xyXG4kZGFyazogIzM0M2E0MDtcclxuJGxhYmVsLWNvbG9yOiAjNjY2O1xyXG4kYmFja2dyb3VuZC1jb2xvcjogI0VDRjBGMTtcclxuJGJvcmRlckFjdGl2ZUNvbG9yOiAjODBiZGZmO1xyXG4kYm9yZGVyUmFkaXVzOiAwO1xyXG4kZGFya0JsdWU6ICM0NUEyRDI7XHJcbiRsaWdodEdyZWVuOiAjMjdhZTYwO1xyXG4kbGlnaHQtYmx1ZTogI2Y1ZjdmNztcclxuJGJyaWdodEdyYXk6ICM3NTc1NzU7XHJcbiRtYXgtd2lkdGgtbW9iaWxlOiA3NjhweDtcclxuJG1heC13aWR0aC10YWJsZXQ6IDk5MnB4O1xyXG4kbWF4LXdpZHRoLWRlc2t0b3A6IDEyODBweDtcclxuXHJcbi8vIEJFR0lOOiBNYXJnaW5cclxuQG1peGluIG5oLW1nKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1ndCgkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi10b3A6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWdiKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ2woJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tbGVmdDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ3IoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vLyBFTkQ6IE1hcmdpblxyXG5cclxuLy8gQkVHSU46IFBhZGRpbmdcclxuQG1peGluIG5oLXBkKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZzogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZHQoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLXRvcDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZGIoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZGwoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGRyKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy1yaWdodDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogUGFkZGluZ1xyXG5cclxuLy8gQkVHSU46IFdpZHRoXHJcbkBtaXhpbiBuaC13aWR0aCgkd2lkdGgpIHtcclxuICAgIHdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxuICAgIG1pbi13aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbiAgICBtYXgtd2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vLyBFTkQ6IFdpZHRoXHJcblxyXG4vLyBCRUdJTjogSWNvbiBTaXplXHJcbkBtaXhpbiBuaC1zaXplLWljb24oJHNpemUpIHtcclxuICAgIHdpZHRoOiAkc2l6ZTtcclxuICAgIGhlaWdodDogJHNpemU7XHJcbiAgICBmb250LXNpemU6ICRzaXplO1xyXG59XHJcblxyXG4vLyBFTkQ6IEljb24gU2l6ZVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/modules/task/target/targets/target-update-method-calculate-result/target-update-method-calculate-result.component.html":
/*!****************************************************************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-update-method-calculate-result/target-update-method-calculate-result.component.html ***!
  \****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"target-update-method-calculate-result\">\r\n    <div class=\"portlet light bordered cm-mgb-0\">\r\n        <div class=\"portlet-title\">\r\n            <ul>\r\n                <li>\r\n                    <h4 class=\"bold\">\r\n                        Thiết lập cách tính kết quả\r\n                    </h4>\r\n                </li>\r\n                <li class=\"icon pull-right\">\r\n                    <a href=\"javascript://\" class=\"btn-close\" imgsrc=\"dlg-close\"\r\n                       (click)=\"dialogRef.close()\">\r\n                    </a>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n        <div class=\"portlet-body\">\r\n            <div class=\"w100pc cm-pd-15\">\r\n                <mat-radio-group color=\"primary\">\r\n                    <mat-radio-button color=\"primary\"\r\n                                      *ngFor=\"let item of listMethodCalculateResult\"\r\n                                      [checked]=\"item.id === methodCalculateResult\"\r\n                                      [value]=\"item.id\"\r\n                                      (change)=\"methodCalculateResultUpdate = item.id\">\r\n                        <span>{{item.name}}</span>\r\n                    </mat-radio-button>\r\n                </mat-radio-group>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"portlet-foot center\">\r\n        <ghm-button classes=\"btn blue cm-mgr-5\" i18n=\"@@update\"\r\n                    (click)=\"updateMethodCalculateResult()\">\r\n            Cập nhập\r\n        </ghm-button>\r\n        <button type=\"button\" class=\"btn btn-light\" (click)=\"dialogRef.close()\">\r\n            Đóng\r\n        </button>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/task/target/targets/target-update-method-calculate-result/target-update-method-calculate-result.component.ts":
/*!**************************************************************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-update-method-calculate-result/target-update-method-calculate-result.component.ts ***!
  \**************************************************************************************************************************************/
/*! exports provided: TargetUpdateMethodCalculateResultComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetUpdateMethodCalculateResultComponent", function() { return TargetUpdateMethodCalculateResultComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_target_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/target.service */ "./src/app/modules/task/target/targets/services/target.service.ts");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");






var TargetUpdateMethodCalculateResultComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TargetUpdateMethodCalculateResultComponent, _super);
    function TargetUpdateMethodCalculateResultComponent(data, targetService, toastr, dialogRef) {
        var _this = _super.call(this) || this;
        _this.data = data;
        _this.targetService = targetService;
        _this.toastr = toastr;
        _this.dialogRef = dialogRef;
        _this.listMethodCalculateResult = [
            { id: 0, name: 'Theo mục tiêu con/KQTC cấp 1 (theo công việc khi chưa có mục tiêu con/KQTC và tự nhập khi chưa có công việc)' },
            { id: 1, name: 'Tự nhập' }
        ];
        return _this;
    }
    TargetUpdateMethodCalculateResultComponent.prototype.ngOnInit = function () {
        if (this.data) {
            this.targetId = this.data.targetId;
            this.methodCalculateResult = this.data.methodCalculateResult;
        }
    };
    TargetUpdateMethodCalculateResultComponent.prototype.updateMethodCalculateResult = function () {
        var _this = this;
        if (this.methodCalculateResultUpdate === undefined || this.methodCalculateResultUpdate === this.methodCalculateResult) {
            this.toastr.error('Ban chua thay doi ket cach tinh');
            return;
        }
        this.targetService.updateMethodCalculateResult(this.targetId, this.methodCalculateResultUpdate).subscribe(function () {
            _this.dialogRef.close({ methodCalculateResult: _this.methodCalculateResultUpdate });
        });
    };
    TargetUpdateMethodCalculateResultComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-target-update-method-calculate-result',
            template: __webpack_require__(/*! ./target-update-method-calculate-result.component.html */ "./src/app/modules/task/target/targets/target-update-method-calculate-result/target-update-method-calculate-result.component.html"),
            styles: [__webpack_require__(/*! ./target-update-method-caculate-result.scss */ "./src/app/modules/task/target/targets/target-update-method-calculate-result/target-update-method-caculate-result.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_4__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _services_target_service__WEBPACK_IMPORTED_MODULE_2__["TargetService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogRef"]])
    ], TargetUpdateMethodCalculateResultComponent);
    return TargetUpdateMethodCalculateResultComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_3__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/task/target/targets/target-update-quick/target-update-quick.component.html":
/*!****************************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-update-quick/target-update-quick.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row target-update-quick\">\r\n    <div class=\"col-sm-8\">\r\n        <textarea #namInput class=\"form-control w100pc\" [value]=\"targetDetail?.name\" name=\"name\" rows=\"4\"></textarea>\r\n        <span *ngIf=\"errorName\" class=\"help-error\">{{errorName}}</span>\r\n        <div class=\"cm-mgt-5\">\r\n            <button (click)=\"updateName(namInput.value)\" class=\"btn blue cm-mgr-5\" i18n=\"@@update\">Cập nhập\r\n            </button>\r\n            <button class=\"btn btn-light\" i18n=\"@@close\" ghm-dismiss=\"true\" type=\"button\">\r\n                Đóng\r\n            </button>\r\n        </div>\r\n    </div>\r\n    <div class=\"col-sm-4 sidebar\">\r\n        <ul class=\"list-style-none cm-pdl-0\">\r\n            <li class=\"cursor-pointer\" >\r\n                <a ghmDialogTrigger=\"\"\r\n                   [ghmDialogTriggerFor]=\"diaLogUpdateStatus\">Cập nhập trạng thái</a>\r\n            </li>\r\n            <li class=\"cursor-pointer\" >\r\n                <a ghmDialogTrigger=\"\"\r\n                   [ghmDialogTriggerFor]=\"diaLogUpdateResult\">Cập nhập kết quả</a>\r\n            </li>\r\n            <li class=\"cursor-pointer\">\r\n                <a ghmDialogTrigger=\"\"\r\n                   [ghmDialogTriggerFor]=\"diaLogUpdateWeight\">Sửa trọng số</a>\r\n            </li>\r\n            <li class=\"cursor-pointer\" [swal]=\"confirmDeleteTarget\"\r\n                (confirm)=\"deleteTarget()\">\r\n                <a>Hủy mục tiêu</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</div>\r\n\r\n<ghm-dialog #diaLogUpdateStatus [backdropStatic]=\"true\" position=\"center\"\r\n     (show)=\"showDialogUpdateStatus()\">\r\n    <ghm-dialog-header [showCloseButton]=\"false\">\r\n        <span i18n=\"@@updateStatus\" class=\"cm-pdl-10\">Cập nhập trạng thái</span>\r\n    </ghm-dialog-header>\r\n    <ghm-dialog-content>\r\n        <div class=\"form-group\">\r\n            <ghm-select\r\n                [value]=\"targetDetail?.status\"\r\n                [icon]=\"''\"\r\n                [elementId]=\"'selectPercentComplete'\"\r\n                [data]=\"[{id: targetStatus.new, name: 'Chưa hoàn thành'}, {id: targetStatus.finish, name: 'Hoàn thành'}]\"\r\n                (itemSelected)=\"status = $event.id\">\r\n            </ghm-select>\r\n            <span *ngIf=\"errorStatus\" class=\"help-error\">{{errorStatus}}</span>\r\n        </div>\r\n    </ghm-dialog-content>\r\n    <ghm-dialog-footer>\r\n        <div class=\"center cm-mgl-20\">\r\n            <button (click)=\"updateStatus()\" class=\"btn blue cm-mgr-5\" i18n=\"@@update\">Cập nhập</button>\r\n            <button ghm-dismiss=\"\" class=\"btn btn-light\" i18n=\"@@close\" type=\"button\">\r\n                Đóng\r\n            </button>\r\n        </div>\r\n    </ghm-dialog-footer>\r\n</ghm-dialog>\r\n\r\n<ghm-dialog #diaLogUpdateResult [backdropStatic]=\"true\" position=\"center\"\r\n            (show)=\"showDialogUpdateResult()\">\r\n    <ghm-dialog-header [showCloseButton]=\"false\">\r\n        <span i18n=\"@@updateResult\" class=\"cm-pdl-10\">Cập nhập kết quả</span>\r\n    </ghm-dialog-header>\r\n    <ghm-dialog-content>\r\n        <div class=\"form-group\">\r\n            <label class=\"ghm-label\" ghmLabel=\"Phần trăm hoàn thành\" i18n=\"@@percentCompleted\"></label>\r\n            <ghm-input #percentCompleteInput\r\n                       [isDisabled]=\"!(targetDetail?.methodCalculateResult === 1 || (targetDetail?.totalTask === 0 && targetDetail?.childCount === 0))\"\r\n                       [elementId]=\"'percentCompleted'\"\r\n                       (keyUp.enter)=\"updateResult(percentCompleteInput?.value)\"></ghm-input>\r\n            <span *ngIf=\"errorPercentCompleted\" class=\"help-error\">\r\n                        {{errorPercentCompleted}}\r\n             </span>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <a class=\"color-bright-gray text-decoration-underline\" (click)=\"showMethodCalculateResultModal()\">Thiết lập\r\n                cách tính kết quả</a>\r\n        </div>\r\n    </ghm-dialog-content>\r\n    <ghm-dialog-footer>\r\n        <div class=\"center cm-mgl-20\">\r\n            <button (click)=\"updateResult(percentCompleteInput?.value)\" class=\"btn blue cm-mgr-5\"\r\n                    i18n=\"@@update\"\r\n                    [disabled]=\"!(targetDetail?.methodCalculateResult === 1 || (targetDetail?.totalTask === 0 && targetDetail?.childCount === 0))\">\r\n                Cập nhập\r\n            </button>\r\n            <button ghm-dismiss=\"\" class=\"btn btn-light\" i18n=\"@@close\"\r\n                    type=\"button\">\r\n                Đóng\r\n            </button>\r\n        </div>\r\n    </ghm-dialog-footer>\r\n</ghm-dialog>\r\n\r\n<ghm-dialog #diaLogUpdateWeight [backdropStatic]=\"true\" position=\"center\"\r\n            (show)=\"showDiaLogUpdateWeight()\">\r\n    <ghm-dialog-header [showCloseButton]=\"false\">\r\n        <span i18n=\"@@filterTarget\" class=\"cm-pdl-10\">Cập nhập trọng số</span>\r\n    </ghm-dialog-header>\r\n    <ghm-dialog-content>\r\n        <div class=\"form-group\">\r\n            <label i18n-ghmLabel=\"@@percentComplete\" ghmLabel=\"Trọng số\"></label>\r\n            <ghm-input #weightInput\r\n                       [elementId]=\"'weight'\"\r\n                       (keyUp.enter)=\"updateWeight(weightInput.value)\"></ghm-input>\r\n            <span *ngIf=\"errorWeight\" class=\"help-error\">\r\n                {{errorWeight}}\r\n            </span>\r\n        </div>\r\n    </ghm-dialog-content>\r\n    <ghm-dialog-footer>\r\n        <div class=\"center cm-mgl-20\">\r\n            <button (click)=\"updateWeight(weightInput.value)\" class=\"btn blue cm-mgr-5\" i18n=\"@@update\">Cập\r\n                nhập\r\n            </button>\r\n            <button ghm-dismiss=\"\" class=\"btn btn-light\" i18n=\"@@close\"\r\n                    type=\"button\">\r\n                Đóng\r\n            </button>\r\n        </div>\r\n    </ghm-dialog-footer>\r\n</ghm-dialog>\r\n\r\n<swal\r\n    #confirmDeleteTarget\r\n    i18n=\"@@confirmDeleteTarget\"\r\n    i18n-title\r\n    i18n-text\r\n    title=\"Bạn có muốn xóa muốn xóa mục tiêu này ?\"\r\n    text=\"Bạn không thể lấy lại mục tiêu này sau khi xóa.\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n"

/***/ }),

/***/ "./src/app/modules/task/target/targets/target-update-quick/target-update-quick.component.scss":
/*!****************************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-update-quick/target-update-quick.component.scss ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".target-update-quick {\n  margin-right: -10px; }\n  .target-update-quick .sidebar {\n    padding-left: 0px;\n    padding-right: 0px;\n    margin-top: -7px;\n    background-color: #f5f7f7;\n    margin-bottom: -7px; }\n  .target-update-quick .sidebar ul {\n      margin-bottom: 0px; }\n  .target-update-quick .sidebar ul li {\n        padding: 8px;\n        border-bottom: 1px solid #eaeaea; }\n  .target-update-quick .sidebar ul li a {\n          color: #0072BC;\n          text-decoration: none; }\n  .target-update-quick .sidebar ul li:hover {\n          background-color: #dff0fd; }\n  .target-update-quick .sidebar ul li:last-child {\n          border-bottom: none; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3RhcmdldC90YXJnZXRzL3RhcmdldC11cGRhdGUtcXVpY2svRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFwcFxcbW9kdWxlc1xcdGFza1xcdGFyZ2V0XFx0YXJnZXRzXFx0YXJnZXQtdXBkYXRlLXF1aWNrXFx0YXJnZXQtdXBkYXRlLXF1aWNrLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9tb2R1bGVzL3Rhc2svdGFyZ2V0L3RhcmdldHMvdGFyZ2V0LXVwZGF0ZS1xdWljay9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXNzZXRzXFxzdHlsZXNcXF9jb25maWcuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNJLG1CQUFtQixFQUFBO0VBRHZCO0lBR1EsaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIseUJDNEJZO0lEM0JaLG1CQUFtQixFQUFBO0VBUDNCO01BeUJZLGtCQUFrQixFQUFBO0VBekI5QjtRQVVnQixZQUFZO1FBVVosZ0NBQWdDLEVBQUE7RUFwQmhEO1VBWW9CLGNDTkQ7VURPQyxxQkFBcUIsRUFBQTtFQWJ6QztVQWlCb0IseUJBQXlCLEVBQUE7RUFqQjdDO1VBc0JvQixtQkFBbUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvdGFzay90YXJnZXQvdGFyZ2V0cy90YXJnZXQtdXBkYXRlLXF1aWNrL3RhcmdldC11cGRhdGUtcXVpY2suY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0ICcuLi8uLi8uLi8uLi8uLi8uLi9hc3NldHMvc3R5bGVzL2NvbmZpZyc7XHJcblxyXG4udGFyZ2V0LXVwZGF0ZS1xdWljayB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IC0xMHB4O1xyXG4gICAgLnNpZGViYXIge1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAtN3B4O1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRsaWdodC1ibHVlO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IC03cHg7XHJcbiAgICAgICAgdWwge1xyXG4gICAgICAgICAgICBsaSB7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiA4cHg7XHJcbiAgICAgICAgICAgICAgICBhIHtcclxuICAgICAgICAgICAgICAgICAgICBjb2xvcjogJGRhcmstYmx1ZTtcclxuICAgICAgICAgICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2RmZjBmZDtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2VhZWFlYTtcclxuICAgICAgICAgICAgICAgICY6bGFzdC1jaGlsZCB7XHJcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogbm9uZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiIsIiRkZWZhdWx0LWNvbG9yOiAjMjIyO1xyXG4kZm9udC1mYW1pbHk6IFwiQXJpYWxcIiwgdGFob21hLCBIZWx2ZXRpY2EgTmV1ZTtcclxuJGNvbG9yLWJsdWU6ICMzNTk4ZGM7XHJcbiRtYWluLWNvbG9yOiAjMDA3NDU1O1xyXG4kYm9yZGVyQ29sb3I6ICNkZGQ7XHJcbiRzZWNvbmQtY29sb3I6ICNiMDFhMWY7XHJcbiR0YWJsZS1iYWNrZ3JvdW5kLWNvbG9yOiAjMDA5Njg4O1xyXG4kYmx1ZTogIzAwN2JmZjtcclxuJGRhcmstYmx1ZTogIzAwNzJCQztcclxuJGJyaWdodC1ibHVlOiAjZGZmMGZkO1xyXG4kaW5kaWdvOiAjNjYxMGYyO1xyXG4kcHVycGxlOiAjNmY0MmMxO1xyXG4kcGluazogI2U4M2U4YztcclxuJHJlZDogI2RjMzU0NTtcclxuJG9yYW5nZTogI2ZkN2UxNDtcclxuJHllbGxvdzogI2ZmYzEwNztcclxuJGdyZWVuOiAjMjhhNzQ1O1xyXG4kdGVhbDogIzIwYzk5NztcclxuJGN5YW46ICMxN2EyYjg7XHJcbiR3aGl0ZTogI2ZmZjtcclxuJGdyYXk6ICM4NjhlOTY7XHJcbiRncmF5LWRhcms6ICMzNDNhNDA7XHJcbiRwcmltYXJ5OiAjMDA3YmZmO1xyXG4kc2Vjb25kYXJ5OiAjNmM3NTdkO1xyXG4kc3VjY2VzczogIzI4YTc0NTtcclxuJGluZm86ICMxN2EyYjg7XHJcbiR3YXJuaW5nOiAjZmZjMTA3O1xyXG4kZGFuZ2VyOiAjZGMzNTQ1O1xyXG4kbGlnaHQ6ICNmOGY5ZmE7XHJcbiRkYXJrOiAjMzQzYTQwO1xyXG4kbGFiZWwtY29sb3I6ICM2NjY7XHJcbiRiYWNrZ3JvdW5kLWNvbG9yOiAjRUNGMEYxO1xyXG4kYm9yZGVyQWN0aXZlQ29sb3I6ICM4MGJkZmY7XHJcbiRib3JkZXJSYWRpdXM6IDA7XHJcbiRkYXJrQmx1ZTogIzQ1QTJEMjtcclxuJGxpZ2h0R3JlZW46ICMyN2FlNjA7XHJcbiRsaWdodC1ibHVlOiAjZjVmN2Y3O1xyXG4kYnJpZ2h0R3JheTogIzc1NzU3NTtcclxuJG1heC13aWR0aC1tb2JpbGU6IDc2OHB4O1xyXG4kbWF4LXdpZHRoLXRhYmxldDogOTkycHg7XHJcbiRtYXgtd2lkdGgtZGVza3RvcDogMTI4MHB4O1xyXG5cclxuLy8gQkVHSU46IE1hcmdpblxyXG5AbWl4aW4gbmgtbWcoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW46ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWd0KCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLXRvcDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ2IoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1nbCgkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1ncigkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1yaWdodDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogTWFyZ2luXHJcblxyXG4vLyBCRUdJTjogUGFkZGluZ1xyXG5AbWl4aW4gbmgtcGQoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkdCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctdG9wOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkYigkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctYm90dG9tOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkbCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctbGVmdDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZHIoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBQYWRkaW5nXHJcblxyXG4vLyBCRUdJTjogV2lkdGhcclxuQG1peGluIG5oLXdpZHRoKCR3aWR0aCkge1xyXG4gICAgd2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG4gICAgbWluLXdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxuICAgIG1heC13aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogV2lkdGhcclxuXHJcbi8vIEJFR0lOOiBJY29uIFNpemVcclxuQG1peGluIG5oLXNpemUtaWNvbigkc2l6ZSkge1xyXG4gICAgd2lkdGg6ICRzaXplO1xyXG4gICAgaGVpZ2h0OiAkc2l6ZTtcclxuICAgIGZvbnQtc2l6ZTogJHNpemU7XHJcbn1cclxuXHJcbi8vIEVORDogSWNvbiBTaXplXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/modules/task/target/targets/target-update-quick/target-update-quick.component.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/target-update-quick/target-update-quick.component.ts ***!
  \**************************************************************************************************/
/*! exports provided: TargetUpdateQuickComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetUpdateQuickComponent", function() { return TargetUpdateQuickComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../shareds/components/ghm-dialog/ghm-dialog.component */ "./src/app/shareds/components/ghm-dialog/ghm-dialog.component.ts");
/* harmony import */ var _constant_target_status_const__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constant/target-status.const */ "./src/app/modules/task/target/targets/constant/target-status.const.ts");
/* harmony import */ var _services_target_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/target.service */ "./src/app/modules/task/target/targets/services/target.service.ts");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _viewmodel_target_viewmodel__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../viewmodel/target.viewmodel */ "./src/app/modules/task/target/targets/viewmodel/target.viewmodel.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _target_update_method_calculate_result_target_update_method_calculate_result_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../target-update-method-calculate-result/target-update-method-calculate-result.component */ "./src/app/modules/task/target/targets/target-update-method-calculate-result/target-update-method-calculate-result.component.ts");










var TargetUpdateQuickComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TargetUpdateQuickComponent, _super);
    function TargetUpdateQuickComponent(targetService, utilService, dialog) {
        var _this = _super.call(this) || this;
        _this.targetService = targetService;
        _this.utilService = utilService;
        _this.dialog = dialog;
        _this.targetDetail = new _viewmodel_target_viewmodel__WEBPACK_IMPORTED_MODULE_6__["TargetViewModel"]();
        _this.remove = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.targetStatus = _constant_target_status_const__WEBPACK_IMPORTED_MODULE_3__["TargetStatus"];
        return _this;
    }
    TargetUpdateQuickComponent.prototype.ngOnInit = function () {
    };
    TargetUpdateQuickComponent.prototype.updateName = function (value) {
        var _this = this;
        if (!value || !value.trim()) {
            this.errorName = 'Tên mục tiêu không được để trông';
            return;
        }
        if (value.length > 256) {
            this.errorName = 'Tên mục tiêu không được lớn hơn 256 ký tự';
            return;
        }
        if (this.targetDetail.name === value) {
            this.errorName = 'Tên mục tiêu chưa thay đổi';
            return;
        }
        this.targetService.updateNameTarget(this.targetDetail.id, value.trim()).subscribe(function () {
            _this.targetDetail.name = value;
            _this.saveSuccessful.emit(_this.targetDetail);
        });
    };
    TargetUpdateQuickComponent.prototype.updateStatus = function () {
        var _this = this;
        if (!this.status && this.status !== 0) {
            this.errorStatus = 'Trạng thái không được để trống';
            return;
        }
        this.errorStatus = null;
        this.targetService.updateStatusCompleted(this.targetDetail.id, this.status)
            .pipe().subscribe(function () {
            _this.targetDetail.status = _this.status;
            _this.saveSuccessful.emit(_this.targetDetail);
            _this.diaLogUpdateStatus.dismiss();
        });
    };
    TargetUpdateQuickComponent.prototype.updateResult = function (value) {
        var _this = this;
        value = !value ? 0 : value;
        if (!this.utilService.isNumber(value)) {
            this.errorPercentCompleted = 'Phần trăm hoàn thành phải là số';
            return;
        }
        if (value < 0 || value > 1000) {
            this.errorPercentCompleted = 'Phần trăm hoàn thành phải lớn hơn 0 và nhỏ hơn 1000';
            return;
        }
        if (this.targetDetail.percentCompleted === parseInt(value)) {
            this.errorWeight = 'Phần trăm hoàn thành chưa thay đổi';
            this.utilService.focusElement('percentCompleted');
            return;
        }
        this.errorPercentCompleted = null;
        this.targetService.updatePercentCompleted(this.targetDetail.id, value).subscribe(function () {
            _this.targetDetail.percentCompleted = value;
            _this.saveSuccessful.emit(_this.targetDetail);
            _this.diaLogUpdateResult.dismiss();
        });
    };
    TargetUpdateQuickComponent.prototype.updateWeight = function (value) {
        var _this = this;
        value = !value ? 0 : value;
        if (!this.utilService.isNumber(value)) {
            this.errorWeight = 'Trọng số phải là số';
            return;
        }
        if (value < 0 || value > 100) {
            this.errorWeight = 'Trọng số phải lớn hơn hoặc bằng 0 và nhở hơn hoặc bằng 100';
            return;
        }
        if (this.targetDetail.weight === parseInt(value)) {
            this.errorWeight = 'Trọng số chưa thay đổi';
            this.utilService.focusElement('weight');
            return;
        }
        this.errorWeight = null;
        this.targetService.updateWeight(this.targetDetail.id, value).subscribe(function () {
            _this.targetDetail.weight = value;
            _this.saveSuccessful.emit(_this.targetDetail);
            _this.diaLogUpdateWeight.dismiss();
        });
    };
    TargetUpdateQuickComponent.prototype.deleteTarget = function () {
        var _this = this;
        this.targetService.deleteTarget(this.targetDetail.id).subscribe(function () {
            _this.remove.emit(_this.targetDetail.id);
        });
    };
    TargetUpdateQuickComponent.prototype.showDiaLogUpdateWeight = function () {
        this.utilService.setValueElement('weight', true, this.targetDetail.weight);
        this.utilService.focusElement('weight');
    };
    TargetUpdateQuickComponent.prototype.showDialogUpdateResult = function () {
        this.utilService.setValueElement('percentCompleted', true, this.targetDetail.percentCompleted);
        if (this.targetDetail.methodCalculateResult === 1 || (this.targetDetail.totalTask === 0 && this.targetDetail.childCount === 0)) {
            this.utilService.focusElement('percentCompleted');
        }
    };
    TargetUpdateQuickComponent.prototype.showMethodCalculateResultModal = function () {
        var _this = this;
        var methodCalculatorResultDialog = this.dialog.open(_target_update_method_calculate_result_target_update_method_calculate_result_component__WEBPACK_IMPORTED_MODULE_9__["TargetUpdateMethodCalculateResultComponent"], {
            id: "targetUpdateMethodCalculateResultDialog-" + this.targetDetail.id,
            data: { targetId: this.targetDetail.id, methodCalculateResult: this.targetDetail.methodCalculateResult }
        });
        methodCalculatorResultDialog.afterClosed().subscribe(function (data) {
            if (data) {
                _this.saveSuccessMethodCalculateResult(data.methodCalculateResult);
            }
        });
    };
    TargetUpdateQuickComponent.prototype.showDialogUpdateStatus = function () {
        this.status = this.targetDetail.status;
    };
    TargetUpdateQuickComponent.prototype.saveSuccessMethodCalculateResult = function (value) {
        this.targetDetail.methodCalculateResult = value;
        if (value === 1) {
            this.utilService.setValueElement('percentCompleted', true, this.targetDetail.percentCompleted);
            this.utilService.focusElement('percentCompleted');
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('diaLogUpdateStatus'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_2__["GhmDialogComponent"])
    ], TargetUpdateQuickComponent.prototype, "diaLogUpdateStatus", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('diaLogUpdateResult'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_2__["GhmDialogComponent"])
    ], TargetUpdateQuickComponent.prototype, "diaLogUpdateResult", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('diaLogUpdateWeight'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_2__["GhmDialogComponent"])
    ], TargetUpdateQuickComponent.prototype, "diaLogUpdateWeight", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _viewmodel_target_viewmodel__WEBPACK_IMPORTED_MODULE_6__["TargetViewModel"])
    ], TargetUpdateQuickComponent.prototype, "targetDetail", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TargetUpdateQuickComponent.prototype, "remove", void 0);
    TargetUpdateQuickComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'target-update-quick',
            template: __webpack_require__(/*! ./target-update-quick.component.html */ "./src/app/modules/task/target/targets/target-update-quick/target-update-quick.component.html"),
            styles: [__webpack_require__(/*! ./target-update-quick.component.scss */ "./src/app/modules/task/target/targets/target-update-quick/target-update-quick.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_target_service__WEBPACK_IMPORTED_MODULE_4__["TargetService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_7__["UtilService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatDialog"]])
    ], TargetUpdateQuickComponent);
    return TargetUpdateQuickComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_5__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/task/target/targets/targets-form-detail/targets-form-detail.component.html":
/*!****************************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/targets-form-detail/targets-form-detail.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"target-modal-detail\">\r\n    <div class=\"portlet light bordered cm-mgb-0\">\r\n        <div class=\"portlet-title\">\r\n            <ul>\r\n                <li class=\"icon\">\r\n                    <a imgsrc=\"dlg-goal\"></a>\r\n                </li>\r\n                <li>\r\n                    <h4 class=\"bold\">\r\n                        Thông tin mục tiêu\r\n                    </h4>\r\n                </li>\r\n                <li class=\"icon pull-right\">\r\n                    <a href=\"javascript://\" class=\"btn-close\" imgsrc=\"dlg-close\"\r\n                       (click)=\"dialogRef.close({isModified: this.isModified})\">\r\n                    </a>\r\n                </li>\r\n                <li class=\"icon pull-right\" *ngIf=\"permissionEdit\">\r\n                    <a href=\"javascript://\" imgsrc=\"dlg-action\" ghmDialogTrigger=\"\"\r\n                       [ghmDialogTriggerFor]=\"targetDetailAction\"></a>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n        <form action=\"\" class=\"form-horizontal\" [formGroup]=\"model\">\r\n            <div class=\"portlet-body\">\r\n                <div class=\"col-sm-12 cm-pdr-0\">\r\n                    <div class=\"col-sm-8\">\r\n                        <div class=\"row cm-mgb-5\">\r\n                            <div *ngIf=\"!isShowNameEdit\" (click)=\"showEditName()\" class=\"text-editor\">\r\n                                {{targetDetail?.name}}\r\n                            </div>\r\n                            <div class=\"title-edit\" *ngIf=\"isShowNameEdit && permissionEdit\">\r\n                                <textarea class=\"form-control cm-mgb-5\" formControlName=\"name\"\r\n                                          id=\"textAreaName\"></textarea>\r\n                                <span class=\"help-block\">\r\n                                {formErrors?.name, select ,\r\n                                required {Tên mục tiêu không được bỏ trống}\r\n                                maxlength {Tên mục tiêu không được quá 256 ký tự}}\r\n                                </span>\r\n                                <div class=\"pull-left\">\r\n                                    <button class=\"btn btn-sm blue cm-mgr-5\" (click)=\"updateName()\">Cập nhập\r\n                                    </button>\r\n                                    <button class=\"btn btn-sm btn-light\" (click)=\"isShowNameEdit = !isShowNameEdit\">\r\n                                        Đóng\r\n                                    </button>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"row cm-mgb-10\">\r\n                            <div class=\"tick-task\">\r\n                                <span>Công việc: {{targetDetail?.totalTaskComplete}}/{{targetDetail?.totalTask}}</span>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"date-time pull-left\" [class.cursor-pointer]=\"permissionEdit\"\r\n                                 [class.btn-primary]=\"targetDetail.status === targetStatus.finish && targetDetail?.overdueDate <= 0\"\r\n                                 [class.btn-warning]=\"targetDetail.status === targetStatus.finish && targetDetail?.overdueDate > 0\"\r\n                                 [class.btn-danger]=\"targetDetail.status === targetStatus.new && targetDetail?.overdueDate > 0\"\r\n                                 [class.btn-success]=\"targetDetail.status === targetStatus.new && targetDetail?.overdueDate <= 0\">\r\n                                <a ghmDialogTrigger=\"\" [ghmDialogTriggerFor]=\"dialogUpdateTime\"\r\n                                   *ngIf=\"permissionEdit; else spanDateTime\">\r\n                                    <i class=\"fa fa-calendar cm-mgr-5\"></i>\r\n                                    <span>Thời hạn {{targetDetail.startDate | date:'dd-MM-y'}} - {{targetDetail.endDate | date:'dd-MM-y'}}</span>\r\n                                    <i class=\"fa fa-caret-down cm-pdl-5\"></i>\r\n                                </a>\r\n                                <ng-template #spanDateTime>\r\n                                    <div>\r\n                                        <i class=\"fa fa-calendar cm-mgr-5\"></i>\r\n                                        <span>Thời hạn {{targetDetail.startDate | date:'dd-MM-y'}} - {{targetDetail.endDate | date:'dd-MM-y'}}</span>\r\n                                        <i class=\"fa fa-caret-down cm-pdl-5\"></i>\r\n                                    </div>\r\n                                </ng-template>\r\n                                <ghm-dialog #dialogUpdateTime [backdropStatic]=\"true\"\r\n                                            [position]=\"'center'\" (show)=\"openDialogDate()\"\r\n                                            title=\"\">\r\n                                    <ghm-dialog-header>\r\n                                        <span i18n=\"@@updateTime\">Cập nhập thời gian</span>\r\n                                    </ghm-dialog-header>\r\n                                    <ghm-dialog-content>\r\n                                        <div class=\"form-group\"\r\n                                             [class.has-error]=\"formErrors.startDate || formErrors.endDate\">\r\n                                            <label class=\"ghm-lable\">Ngày bắt đầu </label>\r\n                                            <nh-date [format]=\"'DD/MM/YYYY'\" formControlName=\"startDate\"\r\n                                                     name=\"startDate\"></nh-date>\r\n                                            <span class=\"help-block\">\r\n                                            { formErrors?.startDate, select,\r\n                                                required {Ngày bắt đầu không được để trống}\r\n                                                isValid {Ngày bắt đầu không đúng định dạng}\r\n                                                notAfter {Ngày bắt đầu không được sau ngày kết thúc}}\r\n                                        </span>\r\n                                        </div>\r\n                                        <div class=\"form-group\">\r\n                                            <label class=\"ghm-lable\">Ngày kết thúc </label>\r\n                                            <nh-date [format]=\"'DD/MM/YYYY'\" formControlName=\"endDate\"\r\n                                                     name=\"endDate\"></nh-date>\r\n                                            <span class=\"help-block\">\r\n                                            { formErrors?.endDate, select,\r\n                                                required {Ngày kết thúc không được để trống}\r\n                                                isValid {Ngày kết thúc không đúng định dạng}\r\n                                                notBefore {Ngày kết không được trước trước kết thúc}}\r\n                                        </span>\r\n                                        </div>\r\n                                    </ghm-dialog-content>\r\n                                    <ghm-dialog-footer>\r\n                                        <div class=\"center\">\r\n                                            <button class=\"btn btn-sm blue cm-mgr-5\" (click)=\"updateDateTime()\"\r\n                                                    i18n=\"@@update\">\r\n                                                Cập nhập\r\n                                            </button>\r\n                                            <button class=\"btn btn-sm btn-light\" i18n=\"@@close\" ghm-dismiss=\"true\"\r\n                                                    type=\"button\">\r\n                                                Đóng\r\n                                            </button>\r\n                                        </div>\r\n                                    </ghm-dialog-footer>\r\n                                </ghm-dialog>\r\n                            </div>\r\n                            <div class=\"pull-right\">\r\n                                <ghm-select formControlName=\"status\"\r\n                                            [icon]=\"''\"\r\n                                            [readonly]=\"!permissionEdit\"\r\n                                            [elementId]=\"'selectPercentComplete'\"\r\n                                            [value]=\"targetDetail?.status\"\r\n                                            [data]=\"[{id: targetStatus.new, name: 'Chưa hoàn thành'},\r\n                                             {id: targetStatus.finish, name: 'Hoàn thành'}]\"\r\n                                            [classColor]=\"targetDetail.status === targetStatus.finish && targetDetail?.overdueDate <= 0 ? 'btn-primary'\r\n                                                            : targetDetail.status === targetStatus.finish && targetDetail?.overdueDate > 0? 'btn-warning'\r\n                                                             : targetDetail.status === targetStatus.new && targetDetail?.overdueDate > 0 ? 'btn-danger'\r\n                                                              : targetDetail.status === targetStatus.new && targetDetail?.overdueDate <= 0 ? 'btn-success' : ''\"\r\n                                            (itemSelected)=\"updateStatusComplete($event)\">\r\n                                </ghm-select>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"row cm-mgt-10\" *ngIf=\"!isShowDescription\">\r\n                            <div\r\n                                *ngIf=\"(!targetDetail?.description || !targetDetail?.description.trim()) && permissionEdit; else descriptionDetail \"\r\n                                class=\"drag-drop-box center\"\r\n                                (click)=\"showEditDescription()\">\r\n                                <span>Click để nhập mô tả</span>\r\n                            </div>\r\n                            <ng-template #descriptionDetail>\r\n                                <div class=\"text-editor font-size-15 font-weight-normal\"\r\n                                     (click)=\"showEditDescription()\">\r\n                                    {{targetDetail?.description}}\r\n                                </div>\r\n                            </ng-template>\r\n                        </div>\r\n                        <div class=\"row cm-mgt-10\" *ngIf=\"isShowDescription && permissionEdit\">\r\n                     <textarea class=\"form-control cm-mgb-10\" formControlName=\"description\"\r\n                               id=\"textAreaDescription\"></textarea>\r\n                            <button class=\"btn btn-sm blue\" (click)=\"updateDescription()\">Cập nhập</button>\r\n                            <button class=\"btn btn-sm btn-light cm-mgl-5\"\r\n                                    (click)=\"isShowDescription = !isShowDescription\">\r\n                                Đóng\r\n                            </button>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"target-actual cm-mgt-10\">\r\n                                <span class=\"font-size-18 cm-pdr-5 pull-left\">Tiến độ thực hiện</span>\r\n                                <a class=\"process-method-result pull-left\" *ngIf=\"permissionEdit\"\r\n                                   (click)=\"showMethodCalculateResultModal()\">Thiết lập\r\n                                    cách tính kết quả</a>\r\n                                <a class=\"pull-right\" ghmDialogTrigger=\"\"\r\n                                   [ghmDialogTriggerFor]=\"dialogUpdatePercentCompleted\"\r\n                                   *ngIf=\"permissionEdit && (targetDetail?.methodCalculateResult === 1 || (targetDetail.totalTask == 0 && targetDetail.childCount === 0)); else spanPercentCompleted\">\r\n                                    Cập nhập | {{targetDetail?.percentCompleted}}%\r\n                                </a>\r\n                                <ng-template #spanPercentCompleted>\r\n                                    <span class=\"pull-right\">{{targetDetail?.percentCompleted ? targetDetail?.percentCompleted : 0}}%</span>\r\n                                </ng-template>\r\n                                <ghm-dialog #dialogUpdatePercentCompleted [backdropStatic]=\"false\"\r\n                                            [position]=\"'center'\"\r\n                                            (show)=\"showUpdatePercentCompleted()\">\r\n                                    <ghm-dialog-header>\r\n                                        <span i18n=\"@@updateTime\" class=\"cm-pdl-10\">Cập nhập % hoàn thành</span>\r\n                                    </ghm-dialog-header>\r\n                                    <ghm-dialog-content>\r\n                                        <div class=\"form-group\" [class.has-error]=\"formErrors.percentCompleted\">\r\n                                            <ghm-input formControlName=\"percentCompleted\"\r\n                                                       [elementId]=\"'percentCompletedInput'\"\r\n                                                       (keyUp.enter)=\"updatePercentCompleted()\"></ghm-input>\r\n                                            <span class=\"help-block\" i18n=\"@@errorPercentCompleted\">\r\n                                            {formErrors?.percentCompleted, select ,\r\n                                                isValid {Phần trăm hoàn thành từ 1 > 1000}\r\n                                                greaterThan {Phần trăm hoàn thành phải lớn hơn 0}\r\n                                                lessThan {Phần trăm hoàn thành phải nhỏ hơn 1000}}\r\n                                            </span>\r\n                                        </div>\r\n                                    </ghm-dialog-content>\r\n                                    <ghm-dialog-footer>\r\n                                        <div class=\"center\">\r\n                                            <button class=\"btn btn-sm blue cm-mgr-5\"\r\n                                                    (click)=\"updatePercentCompleted()\"\r\n                                                    i18n=\"@@update\">Cập nhập\r\n                                            </button>\r\n                                            <button class=\"btn btn-sm btn-light\" i18n=\"@@close\" ghm-dismiss=\"true\"\r\n                                                    type=\"button\">\r\n                                                Đóng\r\n                                            </button>\r\n                                        </div>\r\n                                    </ghm-dialog-footer>\r\n                                </ghm-dialog>\r\n                                <div class=\"progress w100pc cm-mgb-5\">\r\n                                    <div class=\"progress-bar\" role=\"progressbar\"\r\n                                         [class.active]=\"targetDetail.status === targetStatus.finish && targetDetail?.overdueDate <= 0\"\r\n                                         [class.progress-bar-warning]=\"targetDetail.status === targetStatus.finish && targetDetail?.overdueDate > 0\"\r\n                                         [class.progress-bar-danger]=\"targetDetail.status === targetStatus.new && targetDetail?.overdueDate > 0\"\r\n                                         [class.progress-bar-success]=\"targetDetail.status === targetStatus.new && targetDetail?.overdueDate <= 0\"\r\n                                         [attr.aria-valuenow]=\"targetDetail?.percentCompleted\"\r\n                                         aria-valuemin=\"0\"\r\n                                         aria-valuemax=\"100\"\r\n                                         [ngStyle]=\"{'width': targetDetail?.percentCompleted + '%'}\">\r\n                                        <span class=\"sr-only\">{{ targetDetail?.percentCompleted }}%</span>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"row cm-mgt-10\">\r\n                            <target-group-tree\r\n                                *ngIf=\"targetDetail?.targetChildren && targetDetail?.targetChildren.length > 0\"\r\n                                [targetId]=\"targetDetail?.id\"\r\n                                [data]=\"targetGroupTreeChild\"\r\n                                [isShortInfo]=\"true\"\r\n                                [isShowDescription]=\"true\"\r\n                                [type]=\"targetDetail?.type\"\r\n                                (editData)=\"showTarget($event.id)\"\r\n                                (remove)=\"searchTargetChild(targetDetail?.id)\"\r\n                                (onSaveSuccess)=\"searchTargetChild(targetDetail?.id)\"></target-group-tree>\r\n                            <div class=\"drag-drop-box center\" *ngIf=\"permissionTargetChild\">\r\n                                <a class=\"color-gray\" (click)=\"addTargetChild()\"><i class=\"fa fa-plus\"> Thêm mục\r\n                                    tiêu con</i>\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"row cm-mgt-10\">\r\n                            <ghm-attachments [listFileAttachment]=\"targetDetail?.attachments\"\r\n                                             [objectId]=\"targetDetail?.id\"\r\n                                             [isEnable]=\"permissionEdit\"\r\n                                             [objectType]=\"objectType\"\r\n                                             (removeFile)=\"removeAttachment($event)\"\r\n                                             (selectFile)=\"selectFileAttachment($event)\"></ghm-attachments>\r\n                        </div>\r\n                        <div class=\"row cm-mgt-10\" *ngIf=\"permissionComment\">\r\n                            <ghm-comment #comment\r\n                                         [objectType]=\"objectType\"\r\n                                         [objectId]=\"targetDetail?.id\"\r\n                                         (onSendCommentComplete)=\"isModified = true\"\r\n                                         (onRemoveComment)=\"isModified= true\"></ghm-comment>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-sm-4 cm-pdr-0 target-information\">\r\n                        <ghm-app-suggestion\r\n                            [header]=\"targetDetail?.type === typeTarget.personal ? 'Bạn đang xem mục tiêu của' : 'Bạn đang xem mục tiêu của đơn vị'\"\r\n                            [title]=\"targetDetail?.type === typeTarget.personal ? targetDetail?.fullName : targetDetail.officeName\"\r\n                            [avatar]=\"targetDetail?.type === typeTarget.personal ? targetDetail?.avatar : ''\"\r\n                            [description]=\"targetDetail?.type === typeTarget.personal ? targetDetail?.titleName : targetDetail.fullName\"\r\n                        ></ghm-app-suggestion>\r\n                        <hr class=\"cm-mg-0\">\r\n                        <div class=\"app-suggest\">\r\n                            <div class=\"header\">\r\n                                Danh sách người thực hiện\r\n                            </div>\r\n                            <ul class=\"list-style-none cm-pdl-0\">\r\n                                <li *ngFor=\"let item of targetDetail?.participants; let i = index\">\r\n                                    <div class=\"media\" ghmDialogTrigger=\"\"\r\n                                         *ngIf=\"permissionEdit; else spanTargetParticipants\"\r\n                                         [ghmDialogTriggerFor]=\"dialogRoleParticipants\"\r\n                                         [ghmDialogData]=\"item\">\r\n                                        <div class=\"media-left\">\r\n                                            <img ghmImage\r\n                                                 [src]=\"item?.image\"\r\n                                                 [errorImageUrl]=\"'/assets/images/noavatar.png'\"\r\n                                                 class=\"avatar-sm\"/>\r\n                                        </div>\r\n                                        <div class=\"media-body\">\r\n                                            <span>{{item.fullName}}</span>\r\n                                            <a class=\"setting pull-right\">\r\n                                                <i class=\"fa fa-cog\" aria-hidden=\"true\"></i>\r\n                                            </a>\r\n                                        </div>\r\n                                    </div>\r\n                                    <ng-template #spanTargetParticipants>\r\n                                        <div class=\"media\">\r\n                                            <div class=\"media-left\">\r\n                                                <img ghmImage\r\n                                                     [src]=\"item?.image\"\r\n                                                     [errorImageUrl]=\"'/assets/images/noavatar.png'\"\r\n                                                     class=\"avatar-sm\"/>\r\n                                            </div>\r\n                                            <div class=\"media-body\">\r\n                                                <span>{{item.fullName}}</span>\r\n                                                <a class=\"setting pull-right\">\r\n                                                    <i class=\"fa fa-cog\" aria-hidden=\"true\"></i>\r\n                                                </a>\r\n                                            </div>\r\n                                        </div>\r\n                                    </ng-template>\r\n                                </li>\r\n                            </ul>\r\n                            <a class=\"help-text cm-pdb-5\" ghmDialogTrigger=\"\" *ngIf=\"permissionEdit\"\r\n                               [ghmDialogTriggerFor]=\"dialogParticipants\">\r\n                                Bạn muốn thêm người thực hiên ?\r\n                            </a>\r\n                        </div>\r\n                        <div class=\"app-suggest\">\r\n                            <div class=\"info-target\">\r\n                                <div class=\"header\">\r\n                                    <span i18n=\"@@infoTarget\">Thông tin mục tiêu</span>\r\n                                </div>\r\n                                <div class=\"row cm-pdb-10\" *ngIf=\"targetDetail?.parentId\">\r\n                                    <div class=\"col-sm-6\">\r\n                                        <span i18n=\"@@dateComplete\">Thông tin mục tiêu cha</span>\r\n                                    </div>\r\n                                    <div class=\"col-sm-6\">\r\n                                        <a class=\"cm-pdb-5\" (click)=\"showTarget(targetDetail?.parentId)\">\r\n                                            {{targetDetail.parentName}}\r\n                                        </a>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"row cm-pdb-10\" *ngIf=\"targetDetail?.status !== targetStatus.new\">\r\n                                    <div class=\"col-sm-6\">\r\n                                        <span i18n=\"@@dateComplete\">Ngày hoàn thành</span>\r\n                                    </div>\r\n                                    <div class=\"col-sm-6\">\r\n                                        <a ghmDialogTrigger=\"\" [ghmDialogTriggerFor]=\"dialogUpdateCompleteDate\"\r\n                                           class=\"cm-pdb-5\"\r\n                                           *ngIf=\"permissionEdit; else spanCompleteDate\">\r\n                                            {{targetDetail.completedDate | date: 'dd-MM-y'}}\r\n                                        </a>\r\n                                        <ng-template #spanCompleteDate>\r\n                                            <span>Thời hạn {{targetDetail.completedDate | date:'dd-MM-y'}}</span>\r\n                                        </ng-template>\r\n                                        <ghm-dialog #dialogUpdateCompleteDate (show)=\"openDialogCompleteDate()\"\r\n                                                    [backdropStatic]=\"true\"\r\n                                                    [position]=\"'center'\">\r\n                                            <ghm-dialog-header><span i18n=\"@@updateTime\" class=\"cm-pdl-10\">Cập nhập thời gian hoàn thành</span>\r\n                                            </ghm-dialog-header>\r\n                                            <ghm-dialog-content>\r\n                                                <div class=\"form-group\">\r\n                                                    <nh-date [format]=\"'DD/MM/YYYY'\"\r\n                                                             formControlName=\"completeDate\"></nh-date>\r\n                                                </div>\r\n                                            </ghm-dialog-content>\r\n                                            <ghm-dialog-footer>\r\n                                                <div class=\"center\">\r\n                                                    <button class=\"btn btn-sm blue cm-mgr-5\"\r\n                                                            (click)=\"updateCompleteDate()\"\r\n                                                            i18n=\"@@update\">\r\n                                                        Cập nhập\r\n                                                    </button>\r\n                                                    <button class=\"btn btn-sm btn-light\" i18n=\"@@close\"\r\n                                                            ghm-dismiss=\"true\"\r\n                                                            type=\"button\">\r\n                                                        Đóng\r\n                                                    </button>\r\n                                                </div>\r\n                                            </ghm-dialog-footer>\r\n                                        </ghm-dialog>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"row cm-pdb-10\" *ngIf=\"!targetDetail?.parentId\">\r\n                                    <div class=\"col-sm-6 target-title\">\r\n                                        <span i18n=\"tableTarget\">Bảng mục tiêu </span>\r\n                                    </div>\r\n                                    <div class=\"col-sm-6 value\">\r\n                                        <a class=\"help-text\" ghmDialogTrigger=\"\"\r\n                                           [ghmDialogTriggerFor]=\"diaLogTargetTable\"\r\n                                           *ngIf=\"permissionEdit; else spanTargetTable\">\r\n                                            {{targetDetail?.targetTableName}}\r\n                                        </a>\r\n                                        <ng-template #spanTargetTable>\r\n                                            <span>{{targetDetail?.targetTableName}}</span>\r\n                                        </ng-template>\r\n                                        <ghm-dialog #diaLogTargetTable [backdropStatic]=\"true\" [size]=\"'sm'\"\r\n                                                    [position]=\"'center'\" (show)=\"showTargetTable()\">\r\n                                            <ghm-dialog-header>\r\n                                            <span i18n=\"@@updateTargetTable\"\r\n                                                  class=\"cm-pdl-10\">Cập nhập bảng mục tiêu</span>\r\n                                            </ghm-dialog-header>\r\n                                            <ghm-dialog-content>\r\n                                                <div class=\"form-group cm-mgb-10\"\r\n                                                     [class.has-error]=\"formErrors?.targetTableId\">\r\n                                                    <label class=\"control-label\" i18n-ghmLabel=\"@@targetTable\"\r\n                                                           ghmLabel=\"Bảng Mục tiêu\"\r\n                                                           required=\"true\"></label>\r\n                                                    <ghm-select\r\n                                                        formControlName=\"targetTableId\"\r\n                                                        [elementId]=\"'selectTargetTable'\"\r\n                                                        [data]=\"listTargetTable\"\r\n                                                        i18n-title=\"@@listTableTarget\"\r\n                                                        [title]=\"'Chọn bảng mục tiêu'\"\r\n                                                        (itemSelected)=\"selectTargetTableId($event)\"\r\n                                                    ></ghm-select>\r\n                                                    <span class=\"help-block\">{formErrors?.targetTableId, select, required {Vui lòng chọn bảng mục tiêu}}</span>\r\n                                                </div>\r\n                                                <div class=\"form-group cm-mgb-10\"\r\n                                                     [class.has-error]=\"formErrors?.targetGroupId\">\r\n                                                    <label class=\"control-label\" i18n-ghmLabel=\"@@targetGroup\"\r\n                                                           ghmLabel=\"Nhóm Mục tiêu\"\r\n                                                           required=\"true\"></label>\r\n                                                    <ghm-select\r\n                                                        formControlName=\"targetGroupId\"\r\n                                                        [elementId]=\"'selectTargetGroup'\"\r\n                                                        [data]=\"listTargetGroup\"\r\n                                                        i18n=\"@@listTargetGroup\"\r\n                                                        i18n-title=\"@@selectTargetGroup\"\r\n                                                        [title]=\"'-- Chọn nhóm mục tiêu --'\"></ghm-select>\r\n                                                    <span class=\"help-block\">{formErrors?.targetGroupId, select, required {Vui lòng chọn nhóm mục tiêu}}</span>\r\n                                                </div>\r\n                                            </ghm-dialog-content>\r\n                                            <ghm-dialog-footer>\r\n                                                <div class=\"center\">\r\n                                                    <button class=\"btn btn-sm blue cm-mgr-5\"\r\n                                                            (click)=\"updateTableTarget()\" i18n=\"@@update\">\r\n                                                        Cập nhập\r\n                                                    </button>\r\n                                                    <button class=\"btn btn-sm btn-light\" i18n=\"@@close\"\r\n                                                            ghm-dismiss=\"true\"\r\n                                                            type=\"button\">\r\n                                                        Đóng\r\n                                                    </button>\r\n                                                </div>\r\n                                            </ghm-dialog-footer>\r\n                                        </ghm-dialog>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"row cm-pdb-10\">\r\n                                    <div class=\"col-sm-6 target-title\">\r\n                                        <span i18n=\"@@weight\">Trọng số</span>\r\n                                    </div>\r\n                                    <div class=\"col-sm-6 value\">\r\n                                        <a class=\"help-text\" ghmDialogTrigger=\"\"\r\n                                           [ghmDialogTriggerFor]=\"diaLogWeight\"\r\n                                           *ngIf=\"permissionEdit; else spanWeight\">\r\n                                            {{targetDetail?.weight}}%\r\n                                        </a>\r\n                                        <ng-template #spanWeight>\r\n                                            <span>{{targetDetail?.weight}}%</span>\r\n                                        </ng-template>\r\n                                        <ghm-dialog #diaLogWeight [backdropStatic]=\"true\"\r\n                                                    [position]=\"'center'\"\r\n                                                    (show)=\"showDialogWeight()\">\r\n                                            <ghm-dialog-header>\r\n                                                <span i18n=\"@@updateTime\" class=\"cm-pdl-10\">Cập nhập trọng số</span>\r\n                                            </ghm-dialog-header>\r\n                                            <ghm-dialog-content>\r\n                                                <div class=\"form-group\" [class.has-error]=\"formErrors?.weight\">\r\n                                                    <ghm-input formControlName=\"weight\"\r\n                                                               [elementId]=\"'weightUpdate'\"\r\n                                                               (keyUp.enter)=\"updateWeight()\"></ghm-input>\r\n                                                    <span class=\"help-block\">\r\n                                                    { formErrors?.weight, select,\r\n                                                        isValid {weight not valid}\r\n                                                        greaterThan {Trọng số phải lớn hơn hoặc bằng 0}\r\n                                                        lessThan {Tọng số phải nhỏ hơn hoặc bằng 100}\r\n                                                        }\r\n                                                </span>\r\n                                                </div>\r\n                                            </ghm-dialog-content>\r\n                                            <ghm-dialog-footer>\r\n                                                <div class=\"center\">\r\n                                                    <button class=\"btn btn-sm blue cm-mgr-5\"\r\n                                                            (click)=\"updateWeight()\"\r\n                                                            i18n=\"@@update\">\r\n                                                        Cập nhập\r\n                                                    </button>\r\n                                                    <button class=\"btn btn-sm btn-light\" i18n=\"@@close\"\r\n                                                            ghm-dismiss=\"true\"\r\n                                                            type=\"button\">\r\n                                                        Đóng\r\n                                                    </button>\r\n                                                </div>\r\n                                            </ghm-dialog-footer>\r\n                                        </ghm-dialog>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"row cm-pdb-10\" *ngIf=\"!targetDetail?.parentId\">\r\n                                    <div class=\"col-sm-6 target-title\">\r\n                                        <span i18n=\"@@group\">Nhóm</span>\r\n                                    </div>\r\n                                    <div class=\"col-sm-6 value\">\r\n                                        <a class=\"help-text\" ghmDialogTrigger=\"\"\r\n                                           [ghmDialogTriggerFor]=\"dialogTargetGroup\"\r\n                                           *ngIf=\"permissionEdit; else spanTargetGroup\">\r\n                                            {{ targetDetail?.targetGroupName}}\r\n                                        </a>\r\n                                        <ng-template #spanTargetGroup>\r\n                                            <span>{{targetDetail?.targetGroupName}}</span>\r\n                                        </ng-template>\r\n                                        <ghm-dialog #dialogTargetGroup [size]=\"'sm'\" [backdropStatic]=\"true\"\r\n                                                    (show)=\"showDialogTargetGroup()\"\r\n                                                    [position]=\"'center'\">\r\n                                            <ghm-dialog-header>\r\n                                                <span i18n=\"@@updateGroup\" class=\"cm-pdl-10\">Cập nhập nhóm</span>\r\n                                            </ghm-dialog-header>\r\n                                            <ghm-dialog-content>\r\n                                                <div class=\"form-group cm-mgb-10\"\r\n                                                     [class.has-error]=\"formErrors?.targetGroupId\">\r\n                                                    <ghm-select\r\n                                                        formControlName=\"targetGroupId\"\r\n                                                        [elementId]=\"'selectTargetGroup'\"\r\n                                                        [data]=\"listTargetGroup\"\r\n                                                        i18n=\"@@listTargetGroup\"\r\n                                                        [selectedItem]=\"targetDetail.targetGroupId\"\r\n                                                        i18n-title=\"@@selectTargetTable\"\r\n                                                        [title]=\"'-- Chọn nhóm mục tiêu --'\"></ghm-select>\r\n                                                    <span class=\"help-block\">{formErrors?.targetGroupId, select, required {Vui lòng chọn nhóm mục tiêu}}</span>\r\n                                                </div>\r\n                                            </ghm-dialog-content>\r\n                                            <ghm-dialog-footer>\r\n                                                <div class=\"center\">\r\n                                                    <button class=\"btn btn-sm blue cm-mgr-5\"\r\n                                                            (click)=\"updateTargetGroup()\" i18n=\"@@update\">\r\n                                                        Cập nhập\r\n                                                    </button>\r\n                                                    <button class=\"btn btn-sm btn-light\" i18n=\"@@close\"\r\n                                                            ghm-dismiss=\"true\"\r\n                                                            type=\"button\">\r\n                                                        Đóng\r\n                                                    </button>\r\n                                                </div>\r\n                                            </ghm-dialog-footer>\r\n                                        </ghm-dialog>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"row cm-pdb-10\">\r\n                                    <div class=\"col-sm-6 target-title\">\r\n                                        <span i18n=\"@@measurementMethod\">Phương pháp đo</span>\r\n                                    </div>\r\n                                    <div class=\"col-sm-6 value\">\r\n                                        <a class=\"help-text\" ghmDialogTrigger=\"\"\r\n                                           [ghmDialogTriggerFor]=\"dialogMeasurementMethod\"\r\n                                           *ngIf=\"permissionEdit; else spanMeasurementMethod\">\r\n                                            {{ targetDetail?.measurementMethod ? targetDetail?.measurementMethod :\r\n                                            'Click để nhập'}}\r\n                                        </a>\r\n                                        <ng-template #spanMeasurementMethod>\r\n                                            {{targetDetail?.measurementMethod}}\r\n                                        </ng-template>\r\n                                        <ghm-dialog #dialogMeasurementMethod [backdropStatic]=\"true\"\r\n                                                    [position]=\"'center'\" (show)=\"showDialogMeasurementMethod()\">\r\n                                            <ghm-dialog-header>\r\n                                                <span i18n=\"@@updatemeasurementMethod\"\r\n                                                      class=\"cm-pdl-10\">Cập nhập phương pháp đo</span>\r\n                                            </ghm-dialog-header>\r\n                                            <ghm-dialog-content>\r\n                                                <div class=\"form-group\"\r\n                                                     [class.has-error]=\"formErrors?.measurementMethod\">\r\n                                                    <ghm-input formControlName=\"measurementMethod\"\r\n                                                               [elementId]=\"'measurementMethodUpdate'\"\r\n                                                               (keyUp.enter)=\"updateMeasurementMethod()\">\r\n                                                    </ghm-input>\r\n                                                    <span class=\"help-block\">\r\n                                                    { formErrors?.measurementMethod, select,\r\n                                                        maxlength {phương pháp đo không được quá 256 kí tự}\r\n                                                        }\r\n                                                </span>\r\n                                                </div>\r\n                                            </ghm-dialog-content>\r\n                                            <ghm-dialog-footer>\r\n                                                <div class=\"center\">\r\n                                                    <button class=\"btn btn-sm blue cm-mgr-5\"\r\n                                                            (click)=\"updateMeasurementMethod()\" i18n=\"@@update\">\r\n                                                        Cập nhập\r\n                                                    </button>\r\n                                                    <button class=\"btn btn-sm btn-light\" i18n=\"@@close\"\r\n                                                            ghm-dismiss=\"true\"\r\n                                                            type=\"button\">\r\n                                                        Đóng\r\n                                                    </button>\r\n                                                </div>\r\n                                            </ghm-dialog-footer>\r\n                                        </ghm-dialog>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"row cm-pdb-10\">\r\n                                    <div class=\"col-sm-6 target-title\">\r\n                                        <span i18n=\"@@levelOfSharing\">Chia sẻ</span>\r\n                                    </div>\r\n                                    <div class=\"col-sm-6 value\">\r\n                                        <a class=\"help-text\" ghmDialogTrigger=\"\"\r\n                                           [ghmDialogTriggerFor]=\"dialogLevelOfSharing\"\r\n                                           *ngIf=\"permissionEdit; spanLevelOfSharing\">\r\n                                            <span> {targetDetail?.levelOfSharing, select, 0 {Riêng trong từng phòng ban/bộ phận} 1 {Chung trong toàn doanh nghiệp}}</span>\r\n                                        </a>\r\n                                        <ng-template #spanLevelOfSharing>\r\n                                            <span> {targetDetail?.levelOfSharing, select, 0 {Riêng trong từng phòng ban/bộ phận} 1 {Chung trong toàn doanh nghiệp}}</span>\r\n                                        </ng-template>\r\n                                        <ghm-dialog #dialogLevelOfSharing [backdropStatic]=\"true\"\r\n                                                    [position]=\"'center'\"\r\n                                                    (show)=\"showDialogLevelOfSharing()\">\r\n                                            <ghm-dialog-header>\r\n                                                <span i18n=\"@@updateTime\"\r\n                                                      class=\"cm-pdl-10\">Cập nhập mức độ chia sẻ</span>\r\n                                            </ghm-dialog-header>\r\n                                            <ghm-dialog-content>\r\n                                                <div class=\"form-group cm-mgb-10\"\r\n                                                     [class.has-error]=\"formErrors?.levelOfSharing\">\r\n                                                    <ghm-select\r\n                                                        formControlName=\"levelOfSharing\"\r\n                                                        [elementId]=\"'levelOfSharing'\"\r\n                                                        [data]=\"listLevelOfSharing\"\r\n                                                        i18n=\"@@listLevelOfSharing\"\r\n                                                        [selectedItem]=\"targetDetail?.levelOfSharing\"\r\n                                                        i18n-title\r\n                                                        [title]=\"'-- Chọn mức độ chia sẻ --'\"></ghm-select>\r\n                                                    <span class=\"help-block\">{formErrors?.levelOfSharing, select, required {Vui lòng chọn mức độ chia sẻ}}</span>\r\n                                                </div>\r\n                                            </ghm-dialog-content>\r\n                                            <ghm-dialog-footer>\r\n                                                <div class=\"center\">\r\n                                                    <button class=\"btn btn-sm blue cm-mgr-5\"\r\n                                                            (click)=\"updateLevelOfSharing()\" i18n=\"@@update\">\r\n                                                        Cập nhập\r\n                                                    </button>\r\n                                                    <button class=\"btn btn-sm btn-light\" i18n=\"@@close\"\r\n                                                            ghm-dismiss=\"true\"\r\n                                                            type=\"button\">\r\n                                                        Đóng\r\n                                                    </button>\r\n                                                </div>\r\n                                            </ghm-dialog-footer>\r\n                                        </ghm-dialog>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"row cm-pdb-10\">\r\n                                    <div class=\"col-sm-6 target-title\">\r\n                                        <span i18n=\"@@creator\">Người tạo</span>\r\n                                    </div>\r\n                                    <div class=\"col-sm-6 value\">\r\n                                        <span>{{targetDetail?.creatorFullName}}</span>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"row cm-pdb-10\">\r\n                                    <div class=\"col-sm-6 target-title\">\r\n                                        <span i18n=\"@@createTime\">Ngày tạo</span>\r\n                                    </div>\r\n                                    <div class=\"col-sm-6 value\">\r\n                                        <span>{{targetDetail?.createTime | dateTimeFormat: 'DD/MM/YYYY'}}</span>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"portlet-foot center\">\r\n                <button type=\"button\" class=\"btn btn-light\" (click)=\"dialogRef.close({isModified: this.isModified})\">\r\n                    Đóng\r\n                </button>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</div>\r\n\r\n<swal\r\n    #confirmDeleteTargetParticipants\r\n    i18n-title=\"@@confirmDeleteTargetParticipantsTitle\"\r\n    i18n-text=\"@@confirmDeleteTargetParticipantsText\"\r\n    title=\"Bạn có chắc chắn muốn xóa người thực hiện này?\"\r\n    text=\"Bạn không thể khôi phục lại chức vụ này sau khi xóa.\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Đồng Ý\"\r\n    cancelButtonText=\"Hủy\">\r\n</swal>\r\n\r\n<app-log></app-log>\r\n\r\n<ghm-dialog #dialogParticipants [backdropStatic]=\"true\">\r\n    <ghm-dialog-header>\r\n        <span i18n=\"@@updateTime\" class=\"cm-pdl-10\">Thêm người thực hiện</span>\r\n    </ghm-dialog-header>\r\n    <ghm-dialog-content>\r\n        <ghm-select-user-picker\r\n            [selectUsers]=\"listSelectedUser\"\r\n            (itemSelectUser)=\"selectUserParticipants($event)\"\r\n            (acceptSelectUsers)=\"onAcceptSelectUsers($event)\">\r\n        </ghm-select-user-picker>\r\n    </ghm-dialog-content>\r\n</ghm-dialog>\r\n\r\n<ghm-dialog #dialogRoleParticipants [backdropStatic]=\"true\"\r\n            (show)=\"showDialogRolParticipant()\"\r\n            [position]=\"'center'\">\r\n    <ghm-dialog-header>\r\n        <span i18n=\"@@roleParticipants\" class=\"cm-pdl-10\">Quyền người tham gia</span>\r\n    </ghm-dialog-header>\r\n    <ghm-dialog-content>\r\n        <ul class=\"list-style-none cm-pdl-5\">\r\n            <li>\r\n                <mat-checkbox\r\n                    [checked]=\"targetParticipantInfo?.isRead\"\r\n                    [disabled]=\"true\"\r\n                    color=\"primary\">\r\n                    <span> Đọc</span>\r\n                </mat-checkbox>\r\n            </li>\r\n            <li>\r\n                <mat-checkbox\r\n                    [checked]=\"targetParticipantInfo?.isNotification\"\r\n                    [disabled]=\"targetParticipantInfo?.userId === targetDetail?.userId\"\r\n                    (change)=\"onChangePermission(targetParticipantInfo?.userId, permissionTargetConst.isNotification)\"\r\n                    color=\"primary\">\r\n                    <span> Thông báo</span>\r\n                </mat-checkbox>\r\n            </li>\r\n            <li>\r\n                <mat-checkbox\r\n                    [checked]=\"targetParticipantInfo?.isDisputation\"\r\n                    [disabled]=\"targetParticipantInfo?.userId === targetDetail?.userId\"\r\n                    (change)=\"onChangePermission(targetParticipantInfo?.userId, permissionTargetConst.isDisputation)\"\r\n                    color=\"primary\">\r\n                    <span> Thảo luận</span>\r\n                </mat-checkbox>\r\n            </li>\r\n            <li>\r\n                <mat-checkbox\r\n                    [checked]=\"targetParticipantInfo?.isReport\"\r\n                    [disabled]=\"targetParticipantInfo?.userId === targetDetail?.userId\"\r\n                    (change)=\"onChangePermission(targetParticipantInfo?.userId, permissionTargetConst.isReport)\"\r\n                    color=\"primary\">\r\n                    <span> Báo cáo</span>\r\n                </mat-checkbox>\r\n            </li>\r\n            <li>\r\n                <mat-checkbox\r\n                    [checked]=\"targetParticipantInfo?.isWriteTask\"\r\n                    [disabled]=\"targetParticipantInfo?.userId === targetDetail?.userId\"\r\n                    (change)=\"onChangePermission(targetParticipantInfo?.userId, permissionTargetConst.isWriteTask)\"\r\n                    color=\"primary\">\r\n                    <span> Ghi công việc</span>\r\n                </mat-checkbox>\r\n            </li>\r\n            <li>\r\n                <mat-checkbox\r\n                    [checked]=\"targetParticipantInfo?.isWriteTarget\"\r\n                    [disabled]=\"targetParticipantInfo?.userId === targetDetail?.userId\"\r\n                    (change)=\"onChangePermission(targetParticipantInfo?.userId, permissionTargetConst.isWriteTarget)\"\r\n                    color=\"primary\">\r\n                    <span> Ghi mục tiêu</span>\r\n                </mat-checkbox>\r\n            </li>\r\n            <li>\r\n                <mat-checkbox\r\n                    [checked]=\"targetParticipantInfo?.isWriteTargetChild\"\r\n                    [disabled]=\"targetParticipantInfo?.userId === targetDetail?.userId\"\r\n                    (change)=\"onChangePermission(targetParticipantInfo?.userId, permissionTargetConst.isWriteTargetChild)\"\r\n                    color=\"primary\">\r\n                    <span> Quyền trên mục tiêu con hạn chế</span>\r\n                </mat-checkbox>\r\n            </li>\r\n        </ul>\r\n        <div class=\"center\" *ngIf=\"targetParticipantInfo?.userId !== targetDetail?.userId\">\r\n            <button class=\"btn blue cm-mgr-5\"\r\n                    (click)=\"updateTargetParticipantsRole(targetParticipantInfo)\">Cập nhập\r\n            </button>\r\n            <button class=\"btn btn-light cm-pdl-10\" ghm-dismiss=\"true\">Đóng</button>\r\n        </div>\r\n    </ghm-dialog-content>\r\n    <ghm-dialog-footer *ngIf=\"targetParticipantInfo?.userId != targetDetail?.userId\">\r\n        <div class=\"center\">\r\n            <span>Loại bỏ nhân viên khỏi danh sách?</span>\r\n            <button class=\"btn btn-danger\" [swal]=\"confirmDeleteTargetParticipants\"\r\n                    (confirm)=\"deleteTargetParticipants(targetParticipantInfo?.id)\">Loại bỏ\r\n            </button>\r\n        </div>\r\n    </ghm-dialog-footer>\r\n</ghm-dialog>\r\n\r\n<ghm-dialog #targetDetailAction [size]=\"'sm'\"\r\n            position=\"center\"\r\n            class=\"act-item-text\">\r\n    <ghm-dialog-content class=\"target-detail-action-container\">\r\n        <ul class=\"target-detail-action\">\r\n            <li (click)=\"forwardTargetChild()\">\r\n                <div class=\"wrap-img\">\r\n                    <i class=\"fa fa-share-square-o\"></i>\r\n                </div>\r\n                Chuyển thành mục tiêu con\r\n            </li>\r\n            <li (click)=\"showLog()\">\r\n                <div class=\"wrap-img\"><i class=\"fa fa-history\"></i></div>\r\n                Lịch sử mục tiêu\r\n            </li>\r\n            <li [swal]=\"confirmDeleteTarget\" (confirm)=\"deleteTarget()\">\r\n                <div class=\"wrap-img\"><i class=\"fa fa-trash\"></i></div>\r\n                Hủy mục tiêu\r\n            </li>\r\n        </ul>\r\n    </ghm-dialog-content>\r\n</ghm-dialog>\r\n\r\n<swal\r\n    #confirmDeleteTarget\r\n    i18n=\"@@confirmDeleteTarget\"\r\n    i18n-title\r\n    i18n-text\r\n    title=\"Bạn có muốn xóa muốn xóa mục tiêu này ?\"\r\n    text=\"Bạn không thể lấy lại mục tiêu này sau khi xóa.\"\r\n    type=\"question\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n"

/***/ }),

/***/ "./src/app/modules/task/target/targets/targets-form-detail/targets-form-detail.component.scss":
/*!****************************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/targets-form-detail/targets-form-detail.component.scss ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".target-modal-detail .portlet-title {\n  padding: 0 !important;\n  margin-bottom: 0px; }\n  .target-modal-detail .portlet-title ul {\n    list-style: none;\n    margin-bottom: 0;\n    padding: 0;\n    display: block;\n    width: 100%; }\n  .target-modal-detail .portlet-title ul li {\n      display: inline-block;\n      float: left; }\n  .target-modal-detail .portlet-title ul li.icon a {\n        display: block;\n        width: 48px;\n        height: 48px; }\n  .target-modal-detail .portlet-title ul li.icon a.btn-close {\n          border-left: 1px solid #ddd; }\n  .target-modal-detail .portlet-title ul li h4 {\n        margin-top: 15px; }\n  .target-modal-detail .portlet-body {\n  padding-top: 0px; }\n  .target-modal-detail .date-time {\n  color: white;\n  display: flex;\n  -webkit-border-radius: 4px !important; }\n  .target-modal-detail .date-time i {\n    padding-top: 3px !important; }\n  .target-modal-detail .date-time .ghm-dialog-label {\n    display: inline !important;\n    padding-left: 5px !important;\n    padding-right: 5px !important; }\n  .target-modal-detail .date-time a {\n    color: white;\n    text-decoration: none;\n    padding: 6px 12px; }\n  .target-modal-detail .date-time div {\n    padding: 6px 12px; }\n  .target-modal-detail .text-editor {\n  font-size: 18px;\n  cursor: default;\n  font-weight: 600;\n  color: #343a40; }\n  .target-modal-detail .text-editor:hover {\n    color: #0072BC; }\n  .target-modal-detail .target-actual {\n  line-height: 40px; }\n  .target-modal-detail .target-actual a {\n    text-decoration: underline;\n    color: #757575;\n    cursor: pointer; }\n  .target-modal-detail .target-information .app-suggest {\n  border-radius: 0px !important;\n  border-bottom: 1px solid #eaeaea; }\n  .target-modal-detail .target-information .app-suggest:last-child {\n    border-bottom: none; }\n  .target-modal-detail .target-information .app-suggest .help-text {\n    text-decoration: none; }\n  .target-modal-detail .target-information .app-suggest ul li {\n    padding: 5px 0px;\n    border-bottom: 1px dashed #eaeaea;\n    cursor: pointer; }\n  .target-modal-detail .target-information .app-suggest ul li .media-body {\n      position: relative; }\n  .target-modal-detail .target-information .app-suggest ul li .media-body .setting {\n        position: absolute;\n        top: 8px;\n        right: 0px; }\n  .target-modal-detail .target-information .app-suggest ul li:hover {\n      background-color: #dff0fd; }\n  .target-modal-detail .progress {\n  height: 10px; }\n  ghm-dialog-content.target-detail-action-container {\n  padding: 0; }\n  ghm-dialog-content.target-detail-action-container ul.target-detail-action {\n    list-style: none;\n    padding-left: 0;\n    margin-bottom: 0; }\n  ghm-dialog-content.target-detail-action-container ul.target-detail-action li {\n      padding: 12px 12px 12px 48px;\n      position: relative;\n      float: left;\n      width: 100%;\n      min-height: 48px;\n      cursor: pointer;\n      color: #0072bc;\n      border-bottom: 1px solid #ddd; }\n  ghm-dialog-content.target-detail-action-container ul.target-detail-action li:hover {\n        background-color: #dff0fd; }\n  ghm-dialog-content.target-detail-action-container ul.target-detail-action li div.wrap-img {\n        position: absolute;\n        top: 0;\n        left: 0;\n        width: 48px;\n        height: 48px; }\n  ghm-dialog-content.target-detail-action-container ul.target-detail-action li div.wrap-img i {\n          font-size: 24px;\n          padding: 15px;\n          display: block; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3RhcmdldC90YXJnZXRzL3RhcmdldHMtZm9ybS1kZXRhaWwvRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFwcFxcbW9kdWxlc1xcdGFza1xcdGFyZ2V0XFx0YXJnZXRzXFx0YXJnZXRzLWZvcm0tZGV0YWlsXFx0YXJnZXRzLWZvcm0tZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9tb2R1bGVzL3Rhc2svdGFyZ2V0L3RhcmdldHMvdGFyZ2V0cy1mb3JtLWRldGFpbC9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXNzZXRzXFxzdHlsZXNcXF9jb25maWcuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUVRLHFCQUFxQjtFQUNyQixrQkFBa0IsRUFBQTtFQUgxQjtJQUtZLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsVUFBVTtJQUNWLGNBQWM7SUFDZCxXQUFXLEVBQUE7RUFUdkI7TUFZZ0IscUJBQXFCO01BQ3JCLFdBQVcsRUFBQTtFQWIzQjtRQWlCd0IsY0FBYztRQUNkLFdBQVc7UUFDWCxZQUFZLEVBQUE7RUFuQnBDO1VBcUI0QiwyQkNuQlYsRUFBQTtFREZsQjtRQTJCb0IsZ0JBQWdCLEVBQUE7RUEzQnBDO0VBa0NRLGdCQUFnQixFQUFBO0VBbEN4QjtFQXVDUSxZQUFZO0VBQ1osYUFBYTtFQUNiLHFDQUFxQyxFQUFBO0VBekM3QztJQTJDWSwyQkFBMkIsRUFBQTtFQTNDdkM7SUE4Q1ksMEJBQTBCO0lBQzFCLDRCQUE0QjtJQUM1Qiw2QkFBNkIsRUFBQTtFQWhEekM7SUFvRFksWUFBWTtJQUNaLHFCQUFxQjtJQUNyQixpQkFBaUIsRUFBQTtFQXREN0I7SUF5RFksaUJBQWlCLEVBQUE7RUF6RDdCO0VBOERRLGVBQWU7RUFDZixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNDOUNXLEVBQUE7RURuQm5CO0lBbUVZLGNDN0RPLEVBQUE7RURObkI7RUF3RVEsaUJBQWlCLEVBQUE7RUF4RXpCO0lBMEVZLDBCQUEwQjtJQUMxQixjQ3hDUTtJRHlDUixlQUFlLEVBQUE7RUE1RTNCO0VBa0ZZLDZCQUE2QjtFQUM3QixnQ0FBZ0MsRUFBQTtFQW5GNUM7SUFzRmdCLG1CQUFtQixFQUFBO0VBdEZuQztJQXlGZ0IscUJBQXFCLEVBQUE7RUF6RnJDO0lBcUdvQixnQkFBZ0I7SUFDaEIsaUNBQWlDO0lBSWpDLGVBQWUsRUFBQTtFQTFHbkM7TUE4RndCLGtCQUFrQixFQUFBO0VBOUYxQztRQWdHNEIsa0JBQWtCO1FBQ2xCLFFBQVE7UUFDUixVQUFVLEVBQUE7RUFsR3RDO01Bd0d3Qix5QkNqR0gsRUFBQTtFRFByQjtFQWlIUSxZQUFZLEVBQUE7RUFJcEI7RUFDSSxVQUFVLEVBQUE7RUFEZDtJQUlRLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsZ0JBQWdCLEVBQUE7RUFOeEI7TUFTWSw0QkFBNEI7TUFDNUIsa0JBQWtCO01BQ2xCLFdBQVc7TUFDWCxXQUFXO01BQ1gsZ0JBQWdCO01BQ2hCLGVBQWU7TUFDZixjQUFjO01BQ2QsNkJDbklNLEVBQUE7RURtSGxCO1FBbUJnQix5QkFDSixFQUFBO0VBcEJaO1FBdUJnQixrQkFBa0I7UUFDbEIsTUFBTTtRQUNOLE9BQU87UUFDUCxXQUFXO1FBQ1gsWUFBWSxFQUFBO0VBM0I1QjtVQThCb0IsZUFBZTtVQUNmLGFBQWE7VUFDYixjQUFjLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9tb2R1bGVzL3Rhc2svdGFyZ2V0L3RhcmdldHMvdGFyZ2V0cy1mb3JtLWRldGFpbC90YXJnZXRzLWZvcm0tZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCAnLi4vLi4vLi4vLi4vLi4vLi4vYXNzZXRzL3N0eWxlcy9jb25maWcnO1xyXG5cclxuLnRhcmdldC1tb2RhbC1kZXRhaWwge1xyXG4gICAgLnBvcnRsZXQtdGl0bGUge1xyXG4gICAgICAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgdWwge1xyXG4gICAgICAgICAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgICAgICAgICBsaSB7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuXHJcbiAgICAgICAgICAgICAgICAmLmljb24ge1xyXG4gICAgICAgICAgICAgICAgICAgIGEge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDQ4cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogNDhweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJi5idG4tY2xvc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAkYm9yZGVyQ29sb3I7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaDQge1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLnBvcnRsZXQtYm9keSB7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDBweDtcclxuICAgIH1cclxuXHJcbiAgICAuZGF0ZS10aW1lIHtcclxuICAgICAgICAvL3BhZGRpbmc6IDZweCAxMnB4O1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgaSB7XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAzcHggIWltcG9ydGFudDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmdobS1kaWFsb2ctbGFiZWwge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiA1cHggIWltcG9ydGFudDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogNXB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBhIHtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDZweCAxMnB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBkaXYge1xyXG4gICAgICAgICAgICBwYWRkaW5nOiA2cHggMTJweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLnRleHQtZWRpdG9yIHtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgY3Vyc29yOiBkZWZhdWx0O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICAgICAgY29sb3I6ICRncmF5LWRhcms7XHJcbiAgICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgICAgIGNvbG9yOiAkZGFyay1ibHVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAudGFyZ2V0LWFjdHVhbCB7XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgYSB7XHJcbiAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gICAgICAgICAgICBjb2xvcjogJGJyaWdodEdyYXk7XHJcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLnRhcmdldC1pbmZvcm1hdGlvbiB7XHJcbiAgICAgICAgLmFwcC1zdWdnZXN0IHtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWFlYWVhO1xyXG5cclxuICAgICAgICAgICAgJjpsYXN0LWNoaWxkIHtcclxuICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b206IG5vbmU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmhlbHAtdGV4dCB7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdWwge1xyXG4gICAgICAgICAgICAgICAgbGkge1xyXG4gICAgICAgICAgICAgICAgICAgIC5tZWRpYS1ib2R5IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc2V0dGluZyB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b3A6IDhweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJpZ2h0OiAwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogNXB4IDBweDtcclxuICAgICAgICAgICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggZGFzaGVkICNlYWVhZWE7XHJcbiAgICAgICAgICAgICAgICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRicmlnaHQtYmx1ZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5wcm9ncmVzcyB7XHJcbiAgICAgICAgaGVpZ2h0OiAxMHB4O1xyXG4gICAgfVxyXG59XHJcblxyXG5naG0tZGlhbG9nLWNvbnRlbnQudGFyZ2V0LWRldGFpbC1hY3Rpb24tY29udGFpbmVyIHtcclxuICAgIHBhZGRpbmc6IDA7XHJcblxyXG4gICAgdWwudGFyZ2V0LWRldGFpbC1hY3Rpb24ge1xyXG4gICAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAwO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcblxyXG4gICAgICAgIGxpIHtcclxuICAgICAgICAgICAgcGFkZGluZzogMTJweCAxMnB4IDEycHggNDhweDtcclxuICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDQ4cHg7XHJcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgICAgY29sb3I6ICMwMDcyYmM7XHJcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAkYm9yZGVyQ29sb3I7XHJcblxyXG4gICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNkZmYwZmRcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZGl2LndyYXAtaW1nIHtcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgICAgIHRvcDogMDtcclxuICAgICAgICAgICAgICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogNDhweDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogNDhweDtcclxuXHJcbiAgICAgICAgICAgICAgICBpIHtcclxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDI0cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iLCIkZGVmYXVsdC1jb2xvcjogIzIyMjtcclxuJGZvbnQtZmFtaWx5OiBcIkFyaWFsXCIsIHRhaG9tYSwgSGVsdmV0aWNhIE5ldWU7XHJcbiRjb2xvci1ibHVlOiAjMzU5OGRjO1xyXG4kbWFpbi1jb2xvcjogIzAwNzQ1NTtcclxuJGJvcmRlckNvbG9yOiAjZGRkO1xyXG4kc2Vjb25kLWNvbG9yOiAjYjAxYTFmO1xyXG4kdGFibGUtYmFja2dyb3VuZC1jb2xvcjogIzAwOTY4ODtcclxuJGJsdWU6ICMwMDdiZmY7XHJcbiRkYXJrLWJsdWU6ICMwMDcyQkM7XHJcbiRicmlnaHQtYmx1ZTogI2RmZjBmZDtcclxuJGluZGlnbzogIzY2MTBmMjtcclxuJHB1cnBsZTogIzZmNDJjMTtcclxuJHBpbms6ICNlODNlOGM7XHJcbiRyZWQ6ICNkYzM1NDU7XHJcbiRvcmFuZ2U6ICNmZDdlMTQ7XHJcbiR5ZWxsb3c6ICNmZmMxMDc7XHJcbiRncmVlbjogIzI4YTc0NTtcclxuJHRlYWw6ICMyMGM5OTc7XHJcbiRjeWFuOiAjMTdhMmI4O1xyXG4kd2hpdGU6ICNmZmY7XHJcbiRncmF5OiAjODY4ZTk2O1xyXG4kZ3JheS1kYXJrOiAjMzQzYTQwO1xyXG4kcHJpbWFyeTogIzAwN2JmZjtcclxuJHNlY29uZGFyeTogIzZjNzU3ZDtcclxuJHN1Y2Nlc3M6ICMyOGE3NDU7XHJcbiRpbmZvOiAjMTdhMmI4O1xyXG4kd2FybmluZzogI2ZmYzEwNztcclxuJGRhbmdlcjogI2RjMzU0NTtcclxuJGxpZ2h0OiAjZjhmOWZhO1xyXG4kZGFyazogIzM0M2E0MDtcclxuJGxhYmVsLWNvbG9yOiAjNjY2O1xyXG4kYmFja2dyb3VuZC1jb2xvcjogI0VDRjBGMTtcclxuJGJvcmRlckFjdGl2ZUNvbG9yOiAjODBiZGZmO1xyXG4kYm9yZGVyUmFkaXVzOiAwO1xyXG4kZGFya0JsdWU6ICM0NUEyRDI7XHJcbiRsaWdodEdyZWVuOiAjMjdhZTYwO1xyXG4kbGlnaHQtYmx1ZTogI2Y1ZjdmNztcclxuJGJyaWdodEdyYXk6ICM3NTc1NzU7XHJcbiRtYXgtd2lkdGgtbW9iaWxlOiA3NjhweDtcclxuJG1heC13aWR0aC10YWJsZXQ6IDk5MnB4O1xyXG4kbWF4LXdpZHRoLWRlc2t0b3A6IDEyODBweDtcclxuXHJcbi8vIEJFR0lOOiBNYXJnaW5cclxuQG1peGluIG5oLW1nKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1ndCgkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi10b3A6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWdiKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ2woJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tbGVmdDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ3IoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vLyBFTkQ6IE1hcmdpblxyXG5cclxuLy8gQkVHSU46IFBhZGRpbmdcclxuQG1peGluIG5oLXBkKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZzogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZHQoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLXRvcDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZGIoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1wZGwoJHBpeGVsKSB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGRyKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy1yaWdodDogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIEVORDogUGFkZGluZ1xyXG5cclxuLy8gQkVHSU46IFdpZHRoXHJcbkBtaXhpbiBuaC13aWR0aCgkd2lkdGgpIHtcclxuICAgIHdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxuICAgIG1pbi13aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbiAgICBtYXgtd2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vLyBFTkQ6IFdpZHRoXHJcblxyXG4vLyBCRUdJTjogSWNvbiBTaXplXHJcbkBtaXhpbiBuaC1zaXplLWljb24oJHNpemUpIHtcclxuICAgIHdpZHRoOiAkc2l6ZTtcclxuICAgIGhlaWdodDogJHNpemU7XHJcbiAgICBmb250LXNpemU6ICRzaXplO1xyXG59XHJcblxyXG4vLyBFTkQ6IEljb24gU2l6ZVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/modules/task/target/targets/targets-form-detail/targets-form-detail.component.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/targets-form-detail/targets-form-detail.component.ts ***!
  \**************************************************************************************************/
/*! exports provided: TargetsFormDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetsFormDetailComponent", function() { return TargetsFormDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _shareds_constants_object_type_const__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../shareds/constants/object-type.const */ "./src/app/shareds/constants/object-type.const.ts");
/* harmony import */ var _shareds_components_ghm_comment_ghm_comment_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../shareds/components/ghm-comment/ghm-comment.component */ "./src/app/shareds/components/ghm-comment/ghm-comment.component.ts");
/* harmony import */ var _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../shareds/components/ghm-dialog/ghm-dialog.component */ "./src/app/shareds/components/ghm-dialog/ghm-dialog.component.ts");
/* harmony import */ var _services_target_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/target.service */ "./src/app/modules/task/target/targets/services/target.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _validators_datetime_validator__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../validators/datetime.validator */ "./src/app/validators/datetime.validator.ts");
/* harmony import */ var _validators_number_validator__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../validators/number.validator */ "./src/app/validators/number.validator.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _target_table_service_target_table_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../target-table/service/target-table.service */ "./src/app/modules/task/target/target-table/service/target-table.service.ts");
/* harmony import */ var _viewmodel_target_detail_viewmodel__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../viewmodel/target-detail.viewmodel */ "./src/app/modules/task/target/targets/viewmodel/target-detail.viewmodel.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _shareds_components_nh_user_picker_nh_user_picker_model__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../../../shareds/components/nh-user-picker/nh-user-picker.model */ "./src/app/shareds/components/nh-user-picker/nh-user-picker.model.ts");
/* harmony import */ var _models_target_participants_model__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../models/target-participants.model */ "./src/app/modules/task/target/targets/models/target-participants.model.ts");
/* harmony import */ var _models_target_participants_permission_const__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../models/target-participants-permission.const */ "./src/app/modules/task/target/targets/models/target-participants-permission.const.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _constant_target_status_const__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../constant/target-status.const */ "./src/app/modules/task/target/targets/constant/target-status.const.ts");
/* harmony import */ var _constant_type_target_const__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../constant/type-target.const */ "./src/app/modules/task/target/targets/constant/type-target.const.ts");
/* harmony import */ var _viewmodel_target_render_result_viewmodel__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../viewmodel/target-render-result.viewmodel */ "./src/app/modules/task/target/targets/viewmodel/target-render-result.viewmodel.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_22___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_22__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_23___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_23__);
/* harmony import */ var _targets_form_targets_form_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../targets-form/targets-form.component */ "./src/app/modules/task/target/targets/targets-form/targets-form.component.ts");
/* harmony import */ var _log_log_log_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ../../../log/log/log.component */ "./src/app/modules/task/log/log/log.component.ts");
/* harmony import */ var _target_update_method_calculate_result_target_update_method_calculate_result_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ../target-update-method-calculate-result/target-update-method-calculate-result.component */ "./src/app/modules/task/target/targets/target-update-method-calculate-result/target-update-method-calculate-result.component.ts");
/* harmony import */ var _target_share_component_target_select_tree_target_select_tree_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ../../target-share-component/target-select-tree/target-select-tree.component */ "./src/app/modules/task/target/target-share-component/target-select-tree/target-select-tree.component.ts");




























var TargetsFormDetailComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TargetsFormDetailComponent, _super);
    function TargetsFormDetailComponent(data, dialog, targetService, fb, dateTimeValidator, numberValidator, toastrService, utilService, targetTableService, dialogRef) {
        var _this = _super.call(this) || this;
        _this.data = data;
        _this.dialog = dialog;
        _this.targetService = targetService;
        _this.fb = fb;
        _this.dateTimeValidator = dateTimeValidator;
        _this.numberValidator = numberValidator;
        _this.toastrService = toastrService;
        _this.utilService = utilService;
        _this.targetTableService = targetTableService;
        _this.dialogRef = dialogRef;
        _this.isShowNameEdit = false;
        _this.targetDetail = new _viewmodel_target_detail_viewmodel__WEBPACK_IMPORTED_MODULE_12__["TargetDetailViewModel"]();
        _this.objectType = _shareds_constants_object_type_const__WEBPACK_IMPORTED_MODULE_3__["ObjectType"].target;
        _this.permissionTargetConst = _models_target_participants_permission_const__WEBPACK_IMPORTED_MODULE_16__["TargetParticipantsPermission"];
        _this.listSelectedUser = [];
        _this.isShowDescription = false;
        _this.listLevelOfSharing = [
            { id: 0, name: 'Riêng trong phòng ban/bộ phận' },
            { id: 1, name: 'Chung trong toàn doanh nghiệp' }
        ];
        _this.targetStatus = _constant_target_status_const__WEBPACK_IMPORTED_MODULE_18__["TargetStatus"];
        _this.typeTarget = _constant_type_target_const__WEBPACK_IMPORTED_MODULE_19__["TypeTarget"];
        _this.targetGroupTreeChild = new _viewmodel_target_render_result_viewmodel__WEBPACK_IMPORTED_MODULE_20__["TargetGroupResultViewModel"]();
        _this.urlUser = _environments_environment__WEBPACK_IMPORTED_MODULE_13__["environment"].apiGatewayUrl + "api/v1/hr/users/suggestions";
        return _this;
    }
    TargetsFormDetailComponent_1 = TargetsFormDetailComponent;
    TargetsFormDetailComponent.prototype.ngOnInit = function () {
        // this.appService.setupPage(this.pageId.TARGET, this.pageId.TARGET_EMPLOYEE, 'Quản lý mục tiêu', 'Chi tiết mục tiêu');
        this.buildForm();
        if (this.data) {
            this.listTargetTable = this.data.targetTable;
            if (this.data.id) {
                this.getDetail(this.data.id);
            }
        }
    };
    TargetsFormDetailComponent.prototype.getDetail = function (targetId) {
        var _this = this;
        this.targetService.getDetail(targetId).subscribe(function (result) {
            _this.targetDetail = result.data;
            _this.isUpdate = true;
            _this.id = targetId;
            lodash__WEBPACK_IMPORTED_MODULE_23__["each"](_this.targetDetail.participants, function (item) {
                _this.changePermissionRoleParticipants(item);
            });
            _this.permissionEdit = _this.checkPermissionRoleParticipants(_models_target_participants_permission_const__WEBPACK_IMPORTED_MODULE_16__["TargetParticipantsPermission"].isWriteTarget, _this.targetDetail.role);
            _this.permissionComment = _this.checkPermissionRoleParticipants(_models_target_participants_permission_const__WEBPACK_IMPORTED_MODULE_16__["TargetParticipantsPermission"].isDisputation, _this.targetDetail.role);
            _this.permissionTargetChild = _this.checkPermissionRoleParticipants(_models_target_participants_permission_const__WEBPACK_IMPORTED_MODULE_16__["TargetParticipantsPermission"].isWriteTargetChild, _this.targetDetail.role);
            _this.targetGroupTreeChild.targets = _this.targetDetail.targetChildren;
            _this.targetGroupTreeChild.targetGroupName = 'Mục tiêu con';
            _this.targetGroupTreeChild.targetGroupId = _this.targetDetail.targetGroupId;
            _this.model.patchValue(_this.targetDetail);
            _this.convertUserPicker();
            _this.resetForm();
            if (_this.targetDetail) {
                setTimeout(function () {
                    _this.commentForm.searchByObjectId(_this.objectType, _this.targetDetail.id);
                }, 200);
            }
        });
    };
    TargetsFormDetailComponent.prototype.showTargetTable = function () {
        var _this = this;
        this.model.patchValue({ targetTableId: this.targetDetail.targetTableId, targetGroupId: this.targetDetail.targetGroupId });
        this.targetTableService.searchTargetGroupByTargetTableId(this.targetDetail.targetTableId)
            .subscribe(function (result) {
            _this.listTargetGroup = result;
        });
        setTimeout(function () {
            _this.model.patchValue({ targetGroupId: _this.targetDetail.targetGroupId });
        }, 200);
    };
    TargetsFormDetailComponent.prototype.showDialogTargetGroup = function () {
        var _this = this;
        this.targetTableService.searchTargetGroupByTargetTableId(this.targetDetail.targetTableId)
            .subscribe(function (result) {
            _this.listTargetGroup = result;
            setTimeout(function () {
                _this.model.patchValue({ targetGroupId: _this.targetDetail.targetGroupId });
            });
        });
    };
    TargetsFormDetailComponent.prototype.openDialogDate = function () {
        this.model.patchValue({ startDate: this.targetDetail.startDate, endDate: this.targetDetail.endDate });
    };
    TargetsFormDetailComponent.prototype.openDialogCompleteDate = function () {
        this.model.patchValue({ completeDate: this.targetDetail.completedDate });
    };
    TargetsFormDetailComponent.prototype.selectTargetTableId = function (item) {
        var _this = this;
        this.targetDetail.targetTableId = item.id;
        this.targetTableService.searchTargetGroupByTargetTableId(this.targetDetail.targetTableId)
            .subscribe(function (result) {
            _this.listTargetGroup = result;
            _this.model.patchValue({
                targetGroupId: _this.listTargetGroup && _this.listTargetGroup.length > 0
                    ? _this.listTargetGroup[0].id : null
            });
        });
    };
    TargetsFormDetailComponent.prototype.showUpdatePercentCompleted = function () {
        this.utilService.focusElement('percentCompletedInput');
        this.model.patchValue({ percentCompleted: this.targetDetail.percentCompleted });
    };
    TargetsFormDetailComponent.prototype.showMethodCalculateResultModal = function () {
        var _this = this;
        var methodCalculatorResultDialog = this.dialog.open(_target_update_method_calculate_result_target_update_method_calculate_result_component__WEBPACK_IMPORTED_MODULE_26__["TargetUpdateMethodCalculateResultComponent"], {
            id: "targetUpdateMethodCalculateResultDialog-" + this.targetDetail.id,
            data: { targetId: this.targetDetail.id, methodCalculateResult: this.targetDetail.methodCalculateResult }
        });
        methodCalculatorResultDialog.afterClosed().subscribe(function (data) {
            if (data) {
                _this.targetDetail.methodCalculateResult = data.methodCalculateResult;
            }
        });
    };
    TargetsFormDetailComponent.prototype.showDialogMeasurementMethod = function () {
        this.model.patchValue({ measurementMethod: this.targetDetail.measurementMethod });
        this.utilService.focusElement('measurementMethodUpdate');
    };
    TargetsFormDetailComponent.prototype.showDialogLevelOfSharing = function () {
        this.model.patchValue({ levelOfSharing: this.targetDetail.levelOfSharing });
    };
    TargetsFormDetailComponent.prototype.selectUserParticipants = function (item) {
        var userInfo = lodash__WEBPACK_IMPORTED_MODULE_23__["find"](this.targetDetail.participants, function (items) {
            return items.userId === item.id;
        });
        if (!userInfo) {
            this.insertTargetParticipants(item);
            this.dialogParticipants.dismiss();
        }
        else {
            this.toastrService.error('participants does exist');
        }
    };
    TargetsFormDetailComponent.prototype.convertUserPicker = function () {
        var _this = this;
        this.listSelectedUser = [];
        lodash__WEBPACK_IMPORTED_MODULE_23__["each"](this.targetDetail.participants, function (item) {
            var userPicker = new _shareds_components_nh_user_picker_nh_user_picker_model__WEBPACK_IMPORTED_MODULE_14__["NhUserPicker"](item.userId, item.fullName, item.image, item.officeName + " - " + item.titleName);
            userPicker.isSelected = true;
            _this.listSelectedUser.push(userPicker);
        });
    };
    TargetsFormDetailComponent.prototype.deleteTargetParticipants = function (id) {
        var _this = this;
        this.targetService.deleteTargetParticipants(id).subscribe(function (result) {
            lodash__WEBPACK_IMPORTED_MODULE_23__["remove"](_this.targetDetail.participants, function (item) {
                return id === item.id;
            });
            _this.dialogRoleParticipants.dismiss();
        });
    };
    TargetsFormDetailComponent.prototype.insertTargetParticipants = function (user) {
        var _this = this;
        var targetParticipants = new _models_target_participants_model__WEBPACK_IMPORTED_MODULE_15__["TargetParticipants"]();
        targetParticipants.userId = user.id ? user.id.toString() : '';
        targetParticipants.fullName = user.fullName;
        targetParticipants.officeName = user.officeName;
        targetParticipants.titleName = user.positionName;
        targetParticipants.image = user.avartar;
        targetParticipants.isRead = true;
        targetParticipants.isNotification = true;
        targetParticipants.isReport = true;
        targetParticipants.isWriteTarget = false;
        targetParticipants.targetId = this.id;
        targetParticipants.isDisputation = true;
        targetParticipants.role = this.calculatorPermission(targetParticipants);
        this.targetService.insertTargetParticipants(targetParticipants).subscribe(function (result) {
            targetParticipants.id = result.data;
            _this.targetDetail.participants.push(targetParticipants);
            _this.convertUserPicker();
            _this.isModified = true;
        });
    };
    TargetsFormDetailComponent.prototype.changePermissionRoleParticipants = function (item) {
        item.isRead = this.checkPermissionRoleParticipants(this.permissionTargetConst.isRead, item.role);
        item.isNotification = this.checkPermissionRoleParticipants(this.permissionTargetConst.isNotification, item.role);
        item.isWriteTarget = this.checkPermissionRoleParticipants(this.permissionTargetConst.isWriteTarget, item.role);
        item.isWriteTargetChild = this.checkPermissionRoleParticipants(this.permissionTargetConst.isWriteTargetChild, item.role);
        item.isWriteTask = this.checkPermissionRoleParticipants(this.permissionTargetConst.isWriteTask, item.role);
        item.isReport = this.checkPermissionRoleParticipants(this.permissionTargetConst.isReport, item.role);
        item.isDisputation = this.checkPermissionRoleParticipants(this.permissionTargetConst.isDisputation, item.role);
    };
    TargetsFormDetailComponent.prototype.checkPermissionRoleParticipants = function (permission, permissions) {
        if (permission & permissions) {
            return true;
        }
        else {
            return false;
        }
    };
    TargetsFormDetailComponent.prototype.onChangePermission = function (id, permission) {
        var targetParticipants = lodash__WEBPACK_IMPORTED_MODULE_23__["find"](this.targetDetail.participants, function (result) {
            return result.userId === id;
        });
        switch (permission) {
            case this.permissionTargetConst.isNotification:
                targetParticipants.isNotification = !targetParticipants.isNotification;
                break;
            case this.permissionTargetConst.isDisputation:
                targetParticipants.isDisputation = !targetParticipants.isDisputation;
                break;
            case this.permissionTargetConst.isReport:
                targetParticipants.isReport = !targetParticipants.isReport;
                break;
            case this.permissionTargetConst.isWriteTask:
                targetParticipants.isWriteTask = !targetParticipants.isWriteTask;
                break;
            case this.permissionTargetConst.isWriteTarget:
                targetParticipants.isWriteTarget = !targetParticipants.isWriteTarget;
                break;
            case this.permissionTargetConst.isWriteTargetChild:
                targetParticipants.isWriteTargetChild = !targetParticipants.isWriteTargetChild;
                break;
        }
        targetParticipants.role = this.calculatorPermission(targetParticipants);
    };
    TargetsFormDetailComponent.prototype.updateTargetParticipantsRole = function (item) {
        var _this = this;
        item.role = this.calculatorPermission(item);
        this.targetService.updateTargetParticipantsRole(item.id, item.role).subscribe(function (result) {
            _this.dialogRoleParticipants.dismiss();
        });
    };
    // Code lại
    TargetsFormDetailComponent.prototype.onAcceptSelectUsers = function (value) {
        var _this = this;
        if (!this.listSelectedUser || this.listSelectedUser.length === 0) {
            lodash__WEBPACK_IMPORTED_MODULE_23__["each"](value.listUserInsert, function (item) {
                _this.insertTargetParticipants(item);
            });
            this.dialogParticipants.dismiss();
        }
        else {
            value.listUserInsert = lodash__WEBPACK_IMPORTED_MODULE_23__["differenceBy"](value.listUserInsert, this.listSelectedUser, 'id');
            if (value.listUserInsert && value.listUserInsert.length > 0) {
                lodash__WEBPACK_IMPORTED_MODULE_23__["each"](value.listUserInsert, function (item) {
                    _this.insertTargetParticipants(item);
                });
            }
            this.dialogParticipants.dismiss();
        }
    };
    TargetsFormDetailComponent.prototype.updateLevelOfSharing = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            if (this.model.value.levelOfSharing === this.targetDetail.levelOfSharing) {
                this.dialogLevelOfSharing.dismiss();
                return;
            }
            this.targetService.updateLevelOfSharing(this.id, this.model.value.levelOfSharing).subscribe(function () {
                _this.targetDetail.levelOfSharing = _this.model.value.levelOfSharing;
                _this.dialogLevelOfSharing.dismiss();
            });
        }
    };
    TargetsFormDetailComponent.prototype.showDialogWeight = function () {
        this.model.patchValue({ weight: this.targetDetail.weight });
        this.utilService.focusElement('weightUpdate');
    };
    TargetsFormDetailComponent.prototype.updateWeight = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            if (this.model.value.weight === this.targetDetail.weight) {
                this.dialogWeight.dismiss();
                return;
            }
            this.targetService.updateWeight(this.id, this.model.value.weight).subscribe(function () {
                _this.targetDetail.weight = _this.model.value.weight;
                _this.dialogWeight.dismiss();
                _this.isModified = true;
            });
        }
    };
    TargetsFormDetailComponent.prototype.updateMeasurementMethod = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            if (this.model.value.measurementMethod.trim() === this.targetDetail.measurementMethod) {
                this.dialogMeasurementMethod.dismiss();
                return;
            }
            this.targetService.updateMeasurementMethod(this.id, this.model.value.measurementMethod.trim()).subscribe(function () {
                _this.targetDetail.measurementMethod = _this.model.value.measurementMethod.trim();
                _this.isModified = true;
            });
            this.dialogMeasurementMethod.dismiss();
        }
    };
    TargetsFormDetailComponent.prototype.showEditName = function () {
        if (this.permissionEdit) {
            this.isShowNameEdit = true;
            this.model.patchValue({ name: this.targetDetail.name });
            this.utilService.focusElement('textAreaName');
        }
    };
    TargetsFormDetailComponent.prototype.showEditDescription = function () {
        if (this.permissionEdit) {
            this.isShowDescription = true;
            this.utilService.focusElement('textAreaDescription');
        }
    };
    TargetsFormDetailComponent.prototype.updateStatusComplete = function (item) {
        var _this = this;
        if (this.model.value.status === this.targetDetail.status) {
            return;
        }
        this.targetService.updateStatusCompleted(this.id, this.model.value.status).subscribe(function () {
            _this.targetDetail.status = item.id;
            _this.isModified = true;
            if (_this.targetDetail.status === _constant_target_status_const__WEBPACK_IMPORTED_MODULE_18__["TargetStatus"].finish) {
                _this.targetDetail.completedDate = new Date();
            }
            _this.targetDetail.overdueDate = _this.targetDetail.status === _this.targetStatus.new ?
                moment__WEBPACK_IMPORTED_MODULE_22__(new Date()).diff(moment__WEBPACK_IMPORTED_MODULE_22__(_this.targetDetail.endDate), 'days')
                : moment__WEBPACK_IMPORTED_MODULE_22__(_this.targetDetail.completedDate).diff(moment__WEBPACK_IMPORTED_MODULE_22__(_this.targetDetail.endDate), 'days');
        });
    };
    TargetsFormDetailComponent.prototype.updatePercentCompleted = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            if (this.targetDetail.percentCompleted === this.model.value.percentCompleted) {
                this.dialogUpdatePercentCompleted.dismiss();
                return;
            }
            this.targetService.updatePercentCompleted(this.id, this.model.value.percentCompleted).subscribe(function () {
                _this.targetDetail.percentCompleted = _this.model.value.percentCompleted;
                _this.dialogUpdatePercentCompleted.dismiss();
                _this.isModified = true;
            });
        }
    };
    TargetsFormDetailComponent.prototype.updateTableTarget = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            if (this.model.value.targetTableId === this.targetDetail.targetTableId &&
                this.model.value.targetGroupId === this.targetDetail.targetGroupId) {
                this.dialogTargetTable.dismiss();
                return;
            }
            this.targetService.updateTableGroup(this.id, this.model.value.targetTableId, this.model.value.targetGroupId).subscribe(function () {
                var targetTableInfo = lodash__WEBPACK_IMPORTED_MODULE_23__["find"](_this.listTargetTable, function (item) {
                    return item.id === _this.model.value.targetTableId;
                });
                var targetGroupInfo = lodash__WEBPACK_IMPORTED_MODULE_23__["find"](_this.listTargetGroup, function (targetGroup) {
                    return targetGroup.id === _this.model.value.targetGroupId;
                });
                _this.targetDetail.targetTableId = _this.model.value.targetTableId;
                _this.targetDetail.targetTableName = targetTableInfo ? targetTableInfo.name : '';
                _this.targetDetail.targetGroupName = targetGroupInfo ? targetGroupInfo.name : '';
                _this.targetDetail.targetGroupId = _this.model.value.targetGroupId;
                _this.dialogTargetTable.dismiss();
                _this.isModified = true;
            });
        }
    };
    TargetsFormDetailComponent.prototype.updateTargetGroup = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            if (this.model.value.targetGroupId === this.targetDetail.targetGroupId) {
                this.dialogTargetGroup.dismiss();
                return;
            }
            this.targetService.updateTargetGroupId(this.id, this.model.value.targetGroupId).subscribe(function () {
                _this.targetDetail.targetGroupId = _this.model.value.targetGroupId;
                _this.dialogTargetGroup.dismiss();
                _this.isModified = true;
            });
        }
    };
    TargetsFormDetailComponent.prototype.updateDateTime = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            if (this.model.value.startDate === this.targetDetail.startDate
                && this.model.value.endDate === this.targetDetail.endDate) {
                this.dialogUpdateTime.dismiss();
                return;
            }
            this.targetService.updateDateTime(this.id, this.model.value.startDate, this.model.value.endDate).subscribe(function () {
                _this.targetDetail.startDate = _this.model.value.startDate;
                _this.targetDetail.endDate = _this.model.value.endDate;
                _this.targetDetail.overdueDate = _this.targetDetail.status === _this.targetStatus.new ?
                    moment__WEBPACK_IMPORTED_MODULE_22__(new Date()).diff(moment__WEBPACK_IMPORTED_MODULE_22__(_this.targetDetail.endDate), 'days')
                    : moment__WEBPACK_IMPORTED_MODULE_22__(_this.targetDetail.completedDate).diff(moment__WEBPACK_IMPORTED_MODULE_22__(_this.targetDetail.endDate), 'days');
                _this.dialogUpdateTime.dismiss();
                _this.isModified = true;
            });
        }
    };
    TargetsFormDetailComponent.prototype.updateCompleteDate = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            if (this.model.value.completeDate === this.targetDetail.completedDate) {
                this.dialogCompleteDate.dismiss();
                return;
            }
            this.targetService.updateCompleteDate(this.id, this.model.value.completeDate).subscribe(function () {
                _this.targetDetail.completedDate = _this.model.value.completeDate;
                _this.dialogCompleteDate.dismiss();
            });
        }
    };
    TargetsFormDetailComponent.prototype.updateName = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            if (this.targetDetail.name === this.model.value.name.trim()) {
                this.isShowNameEdit = false;
                return;
            }
            this.targetService.updateNameTarget(this.id, this.model.value.name.trim()).subscribe(function () {
                _this.isShowNameEdit = false;
                _this.targetDetail.name = _this.model.value.name.trim();
                _this.isModified = true;
            });
        }
    };
    TargetsFormDetailComponent.prototype.updateDescription = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            if (this.targetDetail.description === this.model.value.description.trim()) {
                this.isShowDescription = false;
                return;
            }
            this.targetService.updateDescriptionTarget(this.id, this.model.value.description.trim()).subscribe(function () {
                _this.isShowDescription = false;
                _this.targetDetail.description = _this.model.value.description.trim();
                _this.isModified = true;
            });
        }
    };
    TargetsFormDetailComponent.prototype.showDialogRolParticipant = function () {
        this.targetParticipantInfo = this.dialogRoleParticipants.ghmDialogData;
    };
    TargetsFormDetailComponent.prototype.showTarget = function (id) {
        var _this = this;
        var targetDetailDialog = this.dialog.open(TargetsFormDetailComponent_1, {
            id: "targetDetailDialogLevel-" + id,
            data: { id: id, targetTable: this.listTargetTable },
            disableClose: true
        });
        targetDetailDialog.afterClosed()
            .subscribe(function (data) {
            if (data.isModified) {
                _this.searchTargetChild();
            }
        });
    };
    TargetsFormDetailComponent.prototype.addTargetChild = function () {
        var _this = this;
        var formAddTargetChildADialog = this.dialog.open(_targets_form_targets_form_component__WEBPACK_IMPORTED_MODULE_24__["TargetsFormComponent"], {
            id: "formAddTargetChild",
            data: { type: this.targetDetail.type, listTargetTable: this.listTargetTable, target: this.targetDetail },
            disableClose: true
        });
        formAddTargetChildADialog.afterClosed().subscribe(function (data) {
            if (data.isModified) {
                _this.searchTargetChild();
            }
        });
    };
    TargetsFormDetailComponent.prototype.calculatorPermission = function (item) {
        var result = 0;
        if (item.isRead) {
            result += this.permissionTargetConst.isRead;
        }
        if (item.isNotification) {
            result += this.permissionTargetConst.isNotification;
        }
        if (item.isDisputation) {
            result += this.permissionTargetConst.isDisputation;
        }
        if (item.isReport) {
            result += this.permissionTargetConst.isReport;
        }
        if (item.isWriteTask) {
            result += this.permissionTargetConst.isWriteTask;
        }
        if (item.isWriteTarget) {
            result += this.permissionTargetConst.isWriteTarget;
        }
        if (item.isWriteTargetChild) {
            result += this.permissionTargetConst.isWriteTargetChild;
        }
        return result;
    };
    TargetsFormDetailComponent.prototype.searchTargetChild = function () {
        var _this = this;
        this.targetService.getTargetChildTree(this.targetDetail.id)
            .subscribe(function (result) {
            _this.targetDetail.targetChildren = result.items;
            _this.targetGroupTreeChild.targets = _this.targetDetail.targetChildren;
            _this.generalPercentCompleteTargetDetail();
            _this.isModified = true;
        });
    };
    TargetsFormDetailComponent.prototype.deleteTarget = function () {
        var _this = this;
        this.targetService.deleteTarget(this.targetDetail.id).subscribe(function () {
            _this.isModified = true;
            _this.dialogRef.close({ isModified: _this.isModified });
        });
    };
    TargetsFormDetailComponent.prototype.forwardTargetChild = function () {
        var dialogForwardChild = this.dialog.open(_target_share_component_target_select_tree_target_select_tree_component__WEBPACK_IMPORTED_MODULE_27__["TargetSelectTreeComponent"], {
            id: "dialogForwardChild",
            data: {
                type: this.targetDetail.type,
                userId: this.targetDetail.userId,
                officeId: this.targetDetail.officeId,
                targetTableId: this.targetDetail.targetTableId
            },
            disableClose: true
        });
        dialogForwardChild.afterClosed().subscribe(function (data) {
            if (data && data.targetParent) {
            }
        });
    };
    TargetsFormDetailComponent.prototype.selectFileAttachment = function (value) {
        var _this = this;
        if (value) {
            if (value.fileInsert) {
                this.targetService.insertAttachment(this.targetDetail.id, value.fileInsert).subscribe(function (result) {
                    value.fileInsert.id = result.data;
                    _this.targetDetail.attachments.push(value.fileInsert);
                });
            }
            if (value.listFileInsert) {
                this.targetService.insertListAttachment(this.targetDetail.id, value.listFileInsert)
                    .subscribe(function (result) {
                    if (result.data) {
                        lodash__WEBPACK_IMPORTED_MODULE_23__["each"](result.data, function (item) {
                            item.isImage = _this.targetService.checkIsImage(item.extension);
                            _this.targetDetail.attachments.push(item);
                        });
                    }
                });
            }
        }
    };
    TargetsFormDetailComponent.prototype.removeAttachment = function (value) {
        var _this = this;
        if (value) {
            this.targetService.deleteAttachment(this.targetDetail.id, value.id).subscribe(function () {
                lodash__WEBPACK_IMPORTED_MODULE_23__["remove"](_this.targetDetail.attachments, function (attachment) {
                    return attachment.id === value.id;
                });
            });
        }
    };
    TargetsFormDetailComponent.prototype.showLog = function () {
        this.logComponent.open(this.targetDetail.id, 0);
    };
    TargetsFormDetailComponent.prototype.generalPercentCompleteTargetDetail = function () {
        var _this = this;
        if (this.targetDetail.targetChildren) {
            var listTargetParent = lodash__WEBPACK_IMPORTED_MODULE_23__["filter"](this.targetDetail.targetChildren, function (targetTree) {
                return targetTree.parentId === _this.targetDetail.id;
            });
            var totalWeight = lodash__WEBPACK_IMPORTED_MODULE_23__["sumBy"](listTargetParent, function (targetTree) {
                return targetTree.data.weight;
            });
            var totalPercentComplete = lodash__WEBPACK_IMPORTED_MODULE_23__["sumBy"](listTargetParent, function (targetTree) {
                return targetTree.data.weight * targetTree.data.percentCompleted;
            });
            this.targetDetail.percentCompleted = totalWeight > 0 ? Math.round(totalPercentComplete / totalWeight) : 0;
        }
    };
    TargetsFormDetailComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.renderFormError(['name', 'description',
            'targetGroupId', 'targetTableId', 'levelOfSharing', 'startDate', 'endDate', 'weight',
            'percentCompleted', 'measurementMethod']);
        this.validationMessages = this.renderFormErrorMessage([
            { 'name': ['required', 'maxlength'] },
            { 'description': ['maxlength'] },
            { 'targetGroupId': ['required'] },
            { 'targetTableId': ['required'] },
            { 'description': ['maxlength'] },
            { 'levelOfSharing': ['required'] },
            { 'startDate': ['required', 'isValid', 'notAfter'] },
            { 'endDate': ['required', 'isValid', 'notBefore'] },
            { 'weight': ['isValid', 'greaterThan', 'lessThan'] },
            { 'percentCompleted': ['isValid', 'greaterThan', 'lessThan'] },
            { 'measurementMethod': ['maxlength'] }
        ]);
        this.model = this.fb.group({
            status: [this.targetDetail.status],
            officeId: [this.targetDetail.officeId],
            name: [this.targetDetail.name, [_angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(256)]],
            targetGroupId: [this.targetDetail.targetGroupId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required
                ]],
            targetTableId: [this.targetDetail.targetTableId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required
                ]],
            description: [this.targetDetail.description, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(4000)
                ]],
            levelOfSharing: [this.targetDetail.levelOfSharing, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required
                ]],
            startDate: [this.targetDetail.startDate, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required, this.dateTimeValidator.isValid, this.dateTimeValidator.notAfter('endDate')
                ]],
            endDate: [this.targetDetail.endDate, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required, this.dateTimeValidator.isValid, this.dateTimeValidator.notBefore('startDate')
                ]],
            type: [this.targetDetail.type],
            userId: [this.targetDetail.userId],
            fullName: [this.targetDetail.fullName],
            weight: [this.targetDetail.weight, [this.numberValidator.isValid,
                    this.numberValidator.greaterThan(-1), this.numberValidator.lessThan(101)]],
            percentCompleted: [this.targetDetail.percentCompleted, [this.numberValidator.isValid, this.numberValidator.greaterThan(-1),
                    this.numberValidator.lessThan(1001)]],
            measurementMethod: [this.targetDetail.measurementMethod, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(256)],
            methodCalculateResult: [this.targetDetail.methodCalculateResult],
            completeDate: [this.targetDetail.completedDate]
        });
        this.subscribers.modelValueChanges = this.model.valueChanges.subscribe(function () { return _this.validateModel(false); });
    };
    TargetsFormDetailComponent.prototype.resetForm = function () {
        this.isShowDescription = false;
        this.isShowNameEdit = false;
    };
    var TargetsFormDetailComponent_1;
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_log_log_log_component__WEBPACK_IMPORTED_MODULE_25__["LogComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _log_log_log_component__WEBPACK_IMPORTED_MODULE_25__["LogComponent"])
    ], TargetsFormDetailComponent.prototype, "logComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_shareds_components_ghm_comment_ghm_comment_component__WEBPACK_IMPORTED_MODULE_4__["GhmCommentComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_comment_ghm_comment_component__WEBPACK_IMPORTED_MODULE_4__["GhmCommentComponent"])
    ], TargetsFormDetailComponent.prototype, "commentForm", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('dialogUpdateTime'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_5__["GhmDialogComponent"])
    ], TargetsFormDetailComponent.prototype, "dialogUpdateTime", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('dialogUpdatePercentCompleted'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_5__["GhmDialogComponent"])
    ], TargetsFormDetailComponent.prototype, "dialogUpdatePercentCompleted", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('diaLogTargetTable'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_5__["GhmDialogComponent"])
    ], TargetsFormDetailComponent.prototype, "dialogTargetTable", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('diaLogWeight'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_5__["GhmDialogComponent"])
    ], TargetsFormDetailComponent.prototype, "dialogWeight", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('dialogTargetGroup'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_5__["GhmDialogComponent"])
    ], TargetsFormDetailComponent.prototype, "dialogTargetGroup", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('dialogMeasurementMethod'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_5__["GhmDialogComponent"])
    ], TargetsFormDetailComponent.prototype, "dialogMeasurementMethod", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('dialogLevelOfSharing'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_5__["GhmDialogComponent"])
    ], TargetsFormDetailComponent.prototype, "dialogLevelOfSharing", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('dialogParticipants'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_5__["GhmDialogComponent"])
    ], TargetsFormDetailComponent.prototype, "dialogParticipants", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('dialogRoleParticipants'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_5__["GhmDialogComponent"])
    ], TargetsFormDetailComponent.prototype, "dialogRoleParticipants", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('dialogUpdateCompleteDate'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_5__["GhmDialogComponent"])
    ], TargetsFormDetailComponent.prototype, "dialogCompleteDate", void 0);
    TargetsFormDetailComponent = TargetsFormDetailComponent_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'targets-form-detail',
            template: __webpack_require__(/*! ./targets-form-detail.component.html */ "./src/app/modules/task/target/targets/targets-form-detail/targets-form-detail.component.html"),
            styles: [__webpack_require__(/*! ./targets-form-detail.component.scss */ "./src/app/modules/task/target/targets/targets-form-detail/targets-form-detail.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_21__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_21__["MatDialog"],
            _services_target_service__WEBPACK_IMPORTED_MODULE_6__["TargetService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"],
            _validators_datetime_validator__WEBPACK_IMPORTED_MODULE_8__["DateTimeValidator"],
            _validators_number_validator__WEBPACK_IMPORTED_MODULE_9__["NumberValidator"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_17__["ToastrService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_10__["UtilService"], _target_table_service_target_table_service__WEBPACK_IMPORTED_MODULE_11__["TargetTableService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_21__["MatDialogRef"]])
    ], TargetsFormDetailComponent);
    return TargetsFormDetailComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/task/target/targets/targets-form/targets-form.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/targets-form/targets-form.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"target-modal-add\">\r\n    <div class=\"portlet light bordered cm-mgb-0\">\r\n        <div class=\"portlet-title\">\r\n            <ul>\r\n                <li class=\"icon\">\r\n                    <a imgsrc=\"dlg-goal\"></a>\r\n                </li>\r\n                <li>\r\n                    <h4 class=\"bold\">\r\n                        Tạo mục tiêu\r\n                    </h4>\r\n                </li>\r\n                <li class=\"icon pull-right\">\r\n                    <a href=\"javascript://\" class=\"btn-close\" imgsrc=\"dlg-close\"\r\n                       (click)=\"dialogRef.close({isModified: isModified})\">\r\n                    </a>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n        <form class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n            <div class=\"portlet-body\">\r\n                <div class=\"form-body\">\r\n                    <div class=\"form-group cm-mgb-10\" [class.has-error]=\"formErrors?.type\" *ngIf=\"!targetParentInfo\">\r\n                        <label class=\"col-sm-4\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <mat-radio-group color=\"primary\" formControlName=\"type\" (change)=\"changeTargetType($event)\">\r\n                                <mat-radio-button color=\"primary\" [checked]=\"true\"\r\n                                                  [value]=\"1\">\r\n                                    <span class=\"cm-mgt-15 cm-mgr-20 cm-note-radio\">Mục tiêu nhân viên</span>\r\n                                </mat-radio-button>\r\n                                <mat-radio-button color=\"primary\"\r\n                                                  [value]=\"0\">\r\n                                    <span class=\"cm-mgt-15 cm-note-radio\">Mục tiêu đơn vị</span>\r\n                                </mat-radio-button>\r\n                            </mat-radio-group>\r\n                            <span class=\"help-block\">\r\n                            {formErrors?.type , select, required {Vui lòng chọn loại mục tiêu}}\r\n                        </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group cm-mgb-10\">\r\n                        <label class=\"col-sm-4 ghm-label\" *ngIf=\"model?.value?.type === 1\" ghmLabel=\"Nhân viên\"\r\n                               i18n=\"@@targetParticipant\"\r\n                               [required]=\"true\"></label>\r\n                        <label class=\"col-sm-4 ghm-label\" *ngIf=\"model?.value?.type === 0\" ghmLabel=\"Đơn vị\"\r\n                               i18n=\"@@targetParticipant\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <ghm-suggestion-user *ngIf=\"model.value.type === 1\"\r\n                                                 #ghmSelectUser\r\n                                                 (itemSelected)=\"selectUser($event)\">\r\n                            </ghm-suggestion-user>\r\n                            <nh-dropdown-tree [data]=\"officeTree\" *ngIf=\"model.value.type === 0\"\r\n                                              i18n-title=\"@@selectOffice\"\r\n                                              [title]=\"'-- Chọn Phòng Ban --'\"\r\n                                              [width]=\"350\"\r\n                                              (nodeSelected)=\"onSelectOffice($event)\"></nh-dropdown-tree>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group cm-mgb-10\">\r\n                        <div class=\"col-sm-12\">\r\n                            <hr>\r\n                        </div>\r\n                        <div class=\"col-sm-12\">\r\n                            <a class=\"pull-right text-decoration-none\" (click)=\"showTargetLibrary()\">\r\n                                <i class=\"fa fa-bullseye\" aria-hidden=\"true\"></i>\r\n                                Tạo từ thư viện mục tiêu\r\n                            </a>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group cm-mgb-10\" [class.has-error]=\"formErrors?.name\">\r\n                        <label class=\"col-sm-4 ghm-label\" ghmLabel=\"Mục tiêu\" i18n-ghmLabel=\"@@targetName\"\r\n                               required=\"true\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <ghm-input [elementId]=\"'name'\"\r\n                                       formControlName=\"name\">\r\n                            </ghm-input>\r\n                            <span class=\"help-block\">\r\n                            {formErrors?.name, select,\r\n                            required {Vui lòng nhập mục tiêu}\r\n                            maxlength{ Mục tiêu không được lớn hơn 256 ký tự}}\r\n                        </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group cm-mgb-10\" [class.has-error]=\"formErrors?.targetTableId\">\r\n                        <label class=\"col-sm-4 ghm-label\" i18n-ghmLabel=\"@@targetTable\" ghmLabel=\"Bảng Mục tiêu\"\r\n                               required=\"true\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <ghm-select\r\n                                formControlName=\"targetTableId\"\r\n                                [elementId]=\"'selectTargetTable'\"\r\n                                [data]=\"listTargetTable\"\r\n                                [readonly]=\"targetParentInfo\"\r\n                                i18n-title=\"@@listTableTarget\"\r\n                                [title]=\"'Chọn bảng mục tiêu'\"\r\n                                (itemSelected)=\"selectTargetTableId($event)\"></ghm-select>\r\n                            <span\r\n                                class=\"help-block\">{formErrors?.targetTableId, select, required {Vui lòng chọn bảng mục tiêu}}</span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group cm-mgb-10\" [class.has-error]=\"formErrors?.levelOfSharing\">\r\n                        <label class=\"col-sm-4 ghm-label\" required=\"true\" i18n-ghmLabel=\"@@targetTable\"\r\n                               ghmLabel=\"Mức độ chia sẻ\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <ghm-select\r\n                                [elementId]=\"'selectLevelOfSharing'\"\r\n                                formControlName=\"levelOfSharing\"\r\n                                [data]=\"[\r\n                                    {id: 0, name: model.value.type === targetType.office ? 'Riêng trong từng phòng ban bộ phạn' : 'Riêng'},\r\n                                    {id: 1, name: model.value.type === targetType.office ? 'Chung trong toàn doanh nghiệp' : 'Chung'}\r\n                                    ]\"\r\n                                title=\"Chọn mức độ chia sẻ\"\r\n                                i18n-title=@@levelOfSharing></ghm-select>\r\n                            <span class=\"help-block\">{formErrors?.levelOfSharing, select, required {Vui lòng chọn mức độ chia sẻ}}</span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group cm-mgb-10\" [class.has-error]=\"formErrors?.targetGroupId\">\r\n                        <label class=\"col-sm-4 ghm-label\" i18n-ghmLabel=\"@@targetGroup\" ghmLabel=\"Nhóm mục tiêu\"\r\n                               required=\"true\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <ghm-select\r\n                                formControlName=\"targetGroupId\"\r\n                                [elementId]=\"'selectTargetGroup'\"\r\n                                [readonly]=\"targetParentInfo\"\r\n                                [data]=\"listTargetGroup\"\r\n                                i18n=\"@@listTargetGroup\"\r\n                                i18n-title\r\n                                [title]=\"'-- Chọn nhóm mục tiêu --'\"></ghm-select>\r\n                            <span\r\n                                class=\"help-block\">{formErrors?.targetGroupId, select, required {Vui lòng chọn nhóm mục tiêu}}</span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group cm-mgb-10\" [class.has-error]=\"formErrors?.weight\">\r\n                        <label class=\"col-sm-4 ghm-label\" ghmLabel=\"Trọng số\" i18n-ghmLabel=\"@@weight\"></label>\r\n                        <div class=\"col-sm-5\">\r\n                            <ghm-input [elementId]=\"'weight'\"\r\n                                       formControlName=\"weight\">\r\n                            </ghm-input>\r\n                            <span class=\"help-block\">\r\n                            {formErrors?.weight, select,\r\n                            isValid{ Trọng số phải là số}\r\n                            greaterThan {Trọng số phải lớn hơn hoặc bằng 0}\r\n                            lessThan {Trọng số phải nhỏ hơn hoặc bằng 100}}\r\n                        </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group cm-mgb-10\" [class.has-error]=\"formErrors?.startDate\">\r\n                        <label i18n-ghmLabel=\"@@startDate\" ghmLabel=\"Ngày bắt đầu\"\r\n                               class=\"col-sm-4 ghm-label\" [required]=\"true\"></label>\r\n                        <div class=\"col-sm-5\">\r\n                            <nh-date [format]=\"'DD/MM/YYYY'\"\r\n                                     formControlName=\"startDate\"></nh-date>\r\n                            <span class=\"help-block\">{ formErrors?.startDate, select,\r\n                                                           required {Ngày bắt đầu không được để trống}\r\n                                                           isValid {Ngày bắt đầu không đúng định dạng}\r\n                                                            notAfter {Ngày bắt đầu không được sau ngày kết thúc}}\r\n                        </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group cm-mgb-10\" [class.has-error]=\"formErrors?.endDate\">\r\n                        <label i18n-ghmLabel=\"@@endDate\" [required]=\"true\" ghmLabel=\"Hạn hoàn thành\"\r\n                               class=\"col-sm-4 ghm-label\"></label>\r\n                        <div class=\"col-sm-5\">\r\n                            <nh-date [format]=\"'DD/MM/YYYY'\"\r\n                                     formControlName=\"endDate\"></nh-date>\r\n                            <span class=\"help-block\">{ formErrors?.endDate, select,\r\n                                                           required {Hạn hoàn thành không được để trống}\r\n                                                           isValid {Hạn hoàn thành không đúng định dạng}\r\n                                                           notBefore {Hạn hoàn thành không được trước ngày bắt đầu}}\r\n                       </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group cm-mgb-10\" [class.has-error]=\"formErrors?.measurementMethod\">\r\n                        <label class=\"col-sm-4 ghm-label\" ghmLabel=\"Phương pháp đo\"\r\n                               i18n-ghmLabel=\"@@measurementMethod\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <ghm-input [elementId]=\"'measurementMethod'\"\r\n                                       formControlName=\"measurementMethod\">\r\n                            </ghm-input>\r\n                            <span class=\"help-block\">\r\n                            {formErrors?.measurementMethod, select,\r\n                            maxlength{ Phương pháp đo không được lớn hơn 256 ký tự}}\r\n                        </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group cm-mgb-10\" *ngIf=\"!isShowMore\">\r\n                        <label class=\"col-sm-4 ghm-label\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <a href=\"javascript://\" class=\"text-decoration-none\" i18n=\"@@informationMore\"\r\n                               (click)=\"isShowMore = !isShowMore\">Thông tin\r\n                                mở rộng\r\n                                <i class=\"fa fa-angle-double-down\" aria-hidden=\"true\"></i>\r\n                            </a>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group cm-mgb-10\" [class.has-error]=\"formErrors?.description\" *ngIf=\"isShowMore\">\r\n                        <label class=\"col-sm-4 ghm-label\" ghmLabel=\"Mô tả\" i18n-ghmLabel=\"@@description\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                        <textarea class=\"form-control\" formControlName=\"description\"\r\n                                  rows=\"3\">\r\n                        </textarea>\r\n                            <span class=\"help-block\">\r\n                            {formErrors?.description, select,\r\n                            maxLength{ Mô tả không được lớn hơn 4000 ký tự}}\r\n                        </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group cm-mgb-10\" *ngIf=\"isShowMore && !targetParentInfo\">\r\n                        <div class=\"col-sm-12 cm-mgb-10\">\r\n                            <mat-expansion-panel [expanded]=\"isShowMore\" class=\"panel-user-participants\">\r\n                                <mat-expansion-panel-header class=\"panel-header\">\r\n                                    <mat-panel-title class=\"title\">\r\n                                        <i class=\"fa fa-list\" aria-hidden=\"true\"> Thêm mục tiêu liên kết</i>\r\n                                    </mat-panel-title>\r\n                                </mat-expansion-panel-header>\r\n                                <div class=\"cm-pd-10\">\r\n                                    <div class=\"drag-drop-box\" *ngIf=\"!targetParent; else targetParentDetail\">\r\n                                        <a class=\"text-decoration-none color-dark-blue\" (click)=\"addTargetChild()\">Thêm\r\n                                            mục tiêu liên kết</a>\r\n                                    </div>\r\n                                    <ng-template #targetParentDetail>\r\n                                        <div class=\"box-info-link\">\r\n                                            <div class=\"media\">\r\n                                                <div class=\"media-left\">\r\n                                                    <i class=\"fa fa-bullseye color-dark-blue\"></i>\r\n                                                </div>\r\n                                                <div class=\"media-body\">\r\n                                                    <span class=\"color-dark-blue bold\">{{targetParent.name}}</span>\r\n                                                    <a class=\"pull-right\" (click)=\"targetParent = null\"\r\n                                                       data-icon=\"delete-16\"></a>\r\n                                                </div>\r\n                                            </div>\r\n                                            <div class=\"cm-mgt-5\">\r\n                                                <i *ngIf=\"targetParent.type === 1\" class=\"fa fa-user color-bright-gray\">\r\n                                                    {{targetParent.fullName}}</i>\r\n                                                <i *ngIf=\"targetParent.type === 0\"\r\n                                                   class=\"fa fa-sitemap color-bright-gray\">\r\n                                                    {{targetParent.officeName}}</i>\r\n                                            </div>\r\n                                            <div class=\"pull-right\">\r\n                                                <a href=\"javascript://\" (click)=\"addTargetChild()\"\r\n                                                   class=\"text-decoration-none color-dark-blue\">\r\n                                                    Chọn mục tiêu khác</a>\r\n                                            </div>\r\n                                        </div>\r\n                                    </ng-template>\r\n                                </div>\r\n                            </mat-expansion-panel>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group cm-mgb-10\" *ngIf=\"isShowMore\">\r\n                        <div class=\"col-sm-12 cm-mgb-10\">\r\n                            <mat-expansion-panel [expanded]=\"isShowMore\" class=\"panel-user-participants\">\r\n                                <mat-expansion-panel-header class=\"panel-header\">\r\n                                    <mat-panel-title class=\"title\">\r\n                                        <i class=\"fa fa-users\" aria-hidden=\"true\"></i> Thêm người thực hiện\r\n                                    </mat-panel-title>\r\n                                </mat-expansion-panel-header>\r\n                                <div class=\"\">\r\n                                    <ghm-select-user-picker\r\n                                        [selectUsers]=\"listSelectedUsers\"\r\n                                        (itemSelectUser)=\"selectUserParticipants($event)\"\r\n                                        (acceptSelectUsers)=\"onAcceptSelectUsers($event)\"\r\n                                    ></ghm-select-user-picker>\r\n                                    <div class=\"list-user\" *ngIf=\"listSelectedUsers.length > 0\">\r\n                                        <div class=\"w100pc\" *ngFor=\"let item of listTargetParticipants; let i of index\">\r\n                                            <div class=\"box-user-info\">\r\n                                                <div class=\"media\">\r\n                                                    <div class=\"media-left\">\r\n                                                        <img ghmImage src=\"{{item.image}}\" class=\"avatar-sm\"\r\n                                                             [errorImageUrl]=\"'/assets/images/noavatar.png'\">\r\n                                                    </div>\r\n                                                    <div class=\"media-body\">\r\n                                                        <span>{{item.fullName}}</span>\r\n                                                        <a class=\"pull-right\" (click)=\"removeParticipants(item.userId)\"\r\n                                                           data-icon=\"delete-16\"></a>\r\n                                                    </div>\r\n                                                </div>\r\n                                                <div class=\"list-permission\">\r\n                                                    <mat-checkbox class=\"permission-item\" [(checked)]=\"item.isRead\"\r\n                                                                  [disabled]=\"true\"\r\n                                                                  color=\"primary\">\r\n                                                        <span i18n=\"roleRead\">Đọc</span>\r\n                                                    </mat-checkbox>\r\n                                                    <mat-checkbox class=\"permission-item\"\r\n                                                                  color=\"primary\"\r\n                                                                  [(checked)]=\"item.isNotification\"\r\n                                                                  (change)=\"onChangePermission(item.userId, permissionTargetConst.isNotification)\">\r\n                                                        <span i18n=\"@@roleNotification\">Thông báo</span>\r\n                                                    </mat-checkbox>\r\n                                                    <mat-checkbox class=\"permission-item\" color=\"primary\"\r\n                                                                  [(checked)]=\"item.isDisputation\"\r\n                                                                  (change)=\"onChangePermission(item.userId, permissionTargetConst.isDisputation)\">\r\n                                                        <span i18n=\"@@roleDisputation\">Thảo Luận</span>\r\n                                                    </mat-checkbox>\r\n                                                    <mat-checkbox class=\"permission-item\" color=\"primary\"\r\n                                                                  [(checked)]=\"item.isReport\"\r\n                                                                  (change)=\"onChangePermission(item.userId, permissionTargetConst.isReport)\">\r\n                                                        <span i18n=\"@@roleReport\">Báo cáo</span>\r\n                                                    </mat-checkbox>\r\n                                                    <mat-checkbox class=\"permission-item\" color=\"primary\"\r\n                                                                  [(checked)]=\"item.isWriteTask\"\r\n                                                                  (change)=\"onChangePermission(item.userId, permissionTargetConst.isWriteTask)\">\r\n                                                        <span i18n=\"@@roleWriteTask\">Ghi công việc</span>\r\n                                                    </mat-checkbox>\r\n                                                    <mat-checkbox class=\"permission-item\" color=\"primary\"\r\n                                                                  [(checked)]=\"item.isWriteTarget\"\r\n                                                                  (change)=\"onChangePermission(item.userId, permissionTargetConst.isWriteTarget)\">\r\n                                                        <span i18n=\"@@roleWriteTarget\">Ghi mục tiêu</span>\r\n                                                    </mat-checkbox>\r\n                                                    <mat-checkbox class=\"permission-item\" color=\"primary\"\r\n                                                                  [(checked)]=\"item.isWriteTargetChild\"\r\n                                                                  (change)=\"onChangePermission(item.userId, permissionTargetConst.isWriteTargetChild)\">\r\n                                                        <span i18n=\"@@roleWriteTarget\">Ghi trên mục tiêu con</span>\r\n                                                    </mat-checkbox>\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </mat-expansion-panel>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group cm-mgb-10\" *ngIf=\"isShowMore\">\r\n                        <div class=\"col-sm-12 cm-mgb-10\">\r\n                            <ghm-attachments\r\n                                [listFileAttachment]=\"listAttachment\"\r\n                                [objectType]=\"0\"\r\n                                (selectFile)=\"selectFileAttachment($event)\"></ghm-attachments>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group cm-mgb-10\" *ngIf=\"isShowMore\">\r\n                        <label class=\"col-sm-4\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <a href=\"javascript://\" class=\"text-decoration-none\" i18n=\"@@informationCollapse\"\r\n                               (click)=\"isShowMore = !isShowMore\">Thu gọn\r\n                                <i class=\"fa fa-angle-double-up\" aria-hidden=\"true\"></i>\r\n                            </a>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"portlet-foot center\">\r\n                <ghm-button [classes]=\"'btn blue cm-mgr-5'\" [loading]=\"isSaving\" i18n=\"@@update\">\r\n                    Thực hiện\r\n                </ghm-button>\r\n                <button type=\"button\" class=\"btn btn-light\" (click)=\"dialogRef.close({isModified: this.isModified})\">\r\n                    Đóng\r\n                </button>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/task/target/targets/targets-form/targets-form.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/targets-form/targets-form.component.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "::ng-deep .mat-radio-button.mat-primary .mat-radio-ripple .mat-ripple-element {\n  background: none !important; }\n\n::ng-deep .mat-radio-button.mat-primary .mat-radio-inner-circle {\n  background-color: #3598dc !important;\n  /*inner circle color change*/ }\n\n::ng-deep.mat-radio-button.mat-primary.mat-radio-checked .mat-radio-outer-circle {\n  border-color: #3598dc !important;\n  /*outer ring color change*/ }\n\n::ng-deep .mat-expansion-indicator:after {\n  color: #fff !important; }\n\n.target-modal-add {\n  width: 600px; }\n\n.target-modal-add .portlet-title {\n    padding: 0 !important; }\n\n.target-modal-add .portlet-title ul {\n      list-style: none;\n      margin-bottom: 0;\n      padding: 0;\n      display: block;\n      width: 100%; }\n\n.target-modal-add .portlet-title ul li {\n        display: inline-block;\n        float: left; }\n\n.target-modal-add .portlet-title ul li.icon a {\n          display: block;\n          width: 48px;\n          height: 48px; }\n\n.target-modal-add .portlet-title ul li h4 {\n          margin-top: 15px; }\n\n.target-modal-add .portlet-body {\n    padding-top: 0px; }\n\n.target-modal-add .panel-user-participants {\n    background-color: #f5f7f7; }\n\n.target-modal-add .panel-user-participants .user-select {\n      display: flex;\n      margin: 10px 0px; }\n\n.target-modal-add .panel-user-participants .user-select .btn-select-user-picker {\n        margin-left: 5px;\n        margin-top: 10px;\n        vertical-align: middle; }\n\n.target-modal-add .panel-user-participants .user-select .btn-select-user-picker i {\n          color: #0072BC;\n          font-size: 25px;\n          font-weight: normal !important; }\n\n.target-modal-add .panel-user-participants .list-user {\n      margin: 10px 0px; }\n\n.target-modal-add .panel-user-participants .list-user .box-user-info {\n        padding: 10px;\n        background-color: #dff0fd;\n        margin-bottom: 10px; }\n\n.target-modal-add .panel-user-participants .list-user .box-user-info .media {\n          margin-bottom: 5px; }\n\n.target-modal-add .panel-user-participants .list-user .box-user-info .media .media-body {\n            vertical-align: middle; }\n\n.target-modal-add .panel-user-participants .list-user .list-permission {\n        padding-left: 5px; }\n\n.target-modal-add .panel-user-participants .list-user .list-permission .permission-item {\n          padding-right: 5px; }\n\n.target-modal-add .panel-user-participants .list-user .list-permission .permission-item .mat-checkbox-inner-container {\n            margin-right: 5px !important; }\n\n.mat-expansion-panel .mat-expansion-panel-header {\n  border-radius: 0px !important;\n  background-color: #45A2D2 !important;\n  padding: 0px 15px 0px 7px !important; }\n\n.mat-expansion-panel .mat-expansion-panel-header .title {\n    font-size: 14px;\n    color: white; }\n\n.mat-expansion-panel .mat-expansion-panel-header i {\n    font-size: 16px;\n    font-weight: normal !important;\n    margin-right: 5px; }\n\n.mat-expansion-panel .mat-expansion-panel-body {\n  padding: 0 10px;\n  background-color: #f5f7f7 !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3RhcmdldC90YXJnZXRzL3RhcmdldHMtZm9ybS9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXBwXFxtb2R1bGVzXFx0YXNrXFx0YXJnZXRcXHRhcmdldHNcXHRhcmdldHMtZm9ybVxcdGFyZ2V0cy1mb3JtLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9tb2R1bGVzL3Rhc2svdGFyZ2V0L3RhcmdldHMvdGFyZ2V0cy1mb3JtL0Q6XFxQcm9qZWN0XFxHaG1BcHBsaWNhdGlvblxcY2xpZW50c1xcZ2htYXBwbGljYXRpb25jbGllbnQvc3JjXFxhc3NldHNcXHN0eWxlc1xcX2NvbmZpZy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0ksMkJBQTJCLEVBQUE7O0FBRy9CO0VBQ0ksb0NBQXdDO0VBQUUsNEJBQUEsRUFBNkI7O0FBRzNFO0VBQ0ksZ0NBQW9DO0VBQUUsMEJBQUEsRUFBMkI7O0FBR3JFO0VBRVEsc0JBQXdCLEVBQUE7O0FBSWhDO0VBQ0ksWUFBWSxFQUFBOztBQURoQjtJQUdRLHFCQUFxQixFQUFBOztBQUg3QjtNQUtZLGdCQUFnQjtNQUNoQixnQkFBZ0I7TUFDaEIsVUFBVTtNQUNWLGNBQWM7TUFDZCxXQUFXLEVBQUE7O0FBVHZCO1FBWWdCLHFCQUFxQjtRQUNyQixXQUFXLEVBQUE7O0FBYjNCO1VBaUJ3QixjQUFjO1VBQ2QsV0FBVztVQUNYLFlBQVksRUFBQTs7QUFuQnBDO1VBd0JvQixnQkFBZ0IsRUFBQTs7QUF4QnBDO0lBK0JRLGdCQUFnQixFQUFBOztBQS9CeEI7SUFtQ1EseUJDbkJZLEVBQUE7O0FEaEJwQjtNQXFDWSxhQUFhO01BQ2IsZ0JBQWdCLEVBQUE7O0FBdEM1QjtRQXlDZ0IsZ0JBQWdCO1FBQ2hCLGdCQUFnQjtRQUNoQixzQkFBc0IsRUFBQTs7QUEzQ3RDO1VBNkNvQixjQ3pERDtVRDBEQyxlQUFlO1VBQ2YsOEJBQThCLEVBQUE7O0FBL0NsRDtNQW9EWSxnQkFBZ0IsRUFBQTs7QUFwRDVCO1FBdURnQixhQUFhO1FBQ2IseUJDbkVLO1FEb0VMLG1CQUFtQixFQUFBOztBQXpEbkM7VUEyRG9CLGtCQUFrQixFQUFBOztBQTNEdEM7WUE2RHdCLHNCQUFzQixFQUFBOztBQTdEOUM7UUFtRWdCLGlCQUFpQixFQUFBOztBQW5FakM7VUFxRW9CLGtCQUFrQixFQUFBOztBQXJFdEM7WUF3RXdCLDRCQUE0QixFQUFBOztBQVlwRDtFQUVRLDZCQUE2QjtFQUM3QixvQ0FBc0M7RUFDdEMsb0NBQW9DLEVBQUE7O0FBSjVDO0lBT1ksZUFBZTtJQUNmLFlBQVksRUFBQTs7QUFSeEI7SUFZWSxlQUFlO0lBQ2YsOEJBQThCO0lBQzlCLGlCQUFpQixFQUFBOztBQWQ3QjtFQW1CUSxlQUFlO0VBQ2Ysb0NBQW9DLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9tb2R1bGVzL3Rhc2svdGFyZ2V0L3RhcmdldHMvdGFyZ2V0cy1mb3JtL3RhcmdldHMtZm9ybS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi8uLi8uLi8uLi8uLi9hc3NldHMvc3R5bGVzL2NvbmZpZ1wiO1xyXG5cclxuOjpuZy1kZWVwIC5tYXQtcmFkaW8tYnV0dG9uLm1hdC1wcmltYXJ5IC5tYXQtcmFkaW8tcmlwcGxlIC5tYXQtcmlwcGxlLWVsZW1lbnQge1xyXG4gICAgYmFja2dyb3VuZDogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC1yYWRpby1idXR0b24ubWF0LXByaW1hcnkgLm1hdC1yYWRpby1pbm5lci1jaXJjbGUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWJsdWUgIWltcG9ydGFudDsgLyppbm5lciBjaXJjbGUgY29sb3IgY2hhbmdlKi9cclxufVxyXG5cclxuOjpuZy1kZWVwLm1hdC1yYWRpby1idXR0b24ubWF0LXByaW1hcnkubWF0LXJhZGlvLWNoZWNrZWQgLm1hdC1yYWRpby1vdXRlci1jaXJjbGUge1xyXG4gICAgYm9yZGVyLWNvbG9yOiAkY29sb3ItYmx1ZSAhaW1wb3J0YW50OyAvKm91dGVyIHJpbmcgY29sb3IgY2hhbmdlKi9cclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtZXhwYW5zaW9uLWluZGljYXRvciB7XHJcbiAgICAmOmFmdGVyIHtcclxuICAgICAgICBjb2xvcjogJHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbn1cclxuXHJcbi50YXJnZXQtbW9kYWwtYWRkIHtcclxuICAgIHdpZHRoOiA2MDBweDtcclxuICAgIC5wb3J0bGV0LXRpdGxlIHtcclxuICAgICAgICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgdWwge1xyXG4gICAgICAgICAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgICAgICAgICBsaSB7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuXHJcbiAgICAgICAgICAgICAgICAmLmljb24ge1xyXG4gICAgICAgICAgICAgICAgICAgIGEge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDQ4cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogNDhweDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaDQge1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLnBvcnRsZXQtYm9keSB7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDBweDtcclxuICAgIH1cclxuXHJcbiAgICAucGFuZWwtdXNlci1wYXJ0aWNpcGFudHMge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRsaWdodC1ibHVlO1xyXG4gICAgICAgIC51c2VyLXNlbGVjdCB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIG1hcmdpbjogMTBweCAwcHg7XHJcblxyXG4gICAgICAgICAgICAuYnRuLXNlbGVjdC11c2VyLXBpY2tlciB7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICAgICAgICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgICAgICAgICAgICAgICBpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb2xvcjogJGRhcmstYmx1ZTtcclxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDI1cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbCAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5saXN0LXVzZXIge1xyXG4gICAgICAgICAgICBtYXJnaW46IDEwcHggMHB4O1xyXG5cclxuICAgICAgICAgICAgLmJveC11c2VyLWluZm8ge1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRicmlnaHQtYmx1ZTtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICAgICAgICAgICAgICAubWVkaWEge1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgICAgICAgICAgICAgICAgICAubWVkaWEtYm9keSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAubGlzdC1wZXJtaXNzaW9uIHtcclxuICAgICAgICAgICAgICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG4gICAgICAgICAgICAgICAgLnBlcm1pc3Npb24taXRlbSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZy1yaWdodDogNXB4O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAubWF0LWNoZWNrYm94LWlubmVyLWNvbnRhaW5lciB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogNXB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC50YXJnZXQtcGFyZW50LWRldGFpbCB7XHJcblxyXG4gICAgfVxyXG59XHJcblxyXG4ubWF0LWV4cGFuc2lvbi1wYW5lbCB7XHJcbiAgICAubWF0LWV4cGFuc2lvbi1wYW5lbC1oZWFkZXIge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDBweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRkYXJrQmx1ZSAhaW1wb3J0YW50O1xyXG4gICAgICAgIHBhZGRpbmc6IDBweCAxNXB4IDBweCA3cHggIWltcG9ydGFudDtcclxuXHJcbiAgICAgICAgLnRpdGxlIHtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpIHtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogbm9ybWFsICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogNXB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAubWF0LWV4cGFuc2lvbi1wYW5lbC1ib2R5IHtcclxuICAgICAgICBwYWRkaW5nOiAwIDEwcHg7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2Y1ZjdmNyAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuIiwiJGRlZmF1bHQtY29sb3I6ICMyMjI7XHJcbiRmb250LWZhbWlseTogXCJBcmlhbFwiLCB0YWhvbWEsIEhlbHZldGljYSBOZXVlO1xyXG4kY29sb3ItYmx1ZTogIzM1OThkYztcclxuJG1haW4tY29sb3I6ICMwMDc0NTU7XHJcbiRib3JkZXJDb2xvcjogI2RkZDtcclxuJHNlY29uZC1jb2xvcjogI2IwMWExZjtcclxuJHRhYmxlLWJhY2tncm91bmQtY29sb3I6ICMwMDk2ODg7XHJcbiRibHVlOiAjMDA3YmZmO1xyXG4kZGFyay1ibHVlOiAjMDA3MkJDO1xyXG4kYnJpZ2h0LWJsdWU6ICNkZmYwZmQ7XHJcbiRpbmRpZ286ICM2NjEwZjI7XHJcbiRwdXJwbGU6ICM2ZjQyYzE7XHJcbiRwaW5rOiAjZTgzZThjO1xyXG4kcmVkOiAjZGMzNTQ1O1xyXG4kb3JhbmdlOiAjZmQ3ZTE0O1xyXG4keWVsbG93OiAjZmZjMTA3O1xyXG4kZ3JlZW46ICMyOGE3NDU7XHJcbiR0ZWFsOiAjMjBjOTk3O1xyXG4kY3lhbjogIzE3YTJiODtcclxuJHdoaXRlOiAjZmZmO1xyXG4kZ3JheTogIzg2OGU5NjtcclxuJGdyYXktZGFyazogIzM0M2E0MDtcclxuJHByaW1hcnk6ICMwMDdiZmY7XHJcbiRzZWNvbmRhcnk6ICM2Yzc1N2Q7XHJcbiRzdWNjZXNzOiAjMjhhNzQ1O1xyXG4kaW5mbzogIzE3YTJiODtcclxuJHdhcm5pbmc6ICNmZmMxMDc7XHJcbiRkYW5nZXI6ICNkYzM1NDU7XHJcbiRsaWdodDogI2Y4ZjlmYTtcclxuJGRhcms6ICMzNDNhNDA7XHJcbiRsYWJlbC1jb2xvcjogIzY2NjtcclxuJGJhY2tncm91bmQtY29sb3I6ICNFQ0YwRjE7XHJcbiRib3JkZXJBY3RpdmVDb2xvcjogIzgwYmRmZjtcclxuJGJvcmRlclJhZGl1czogMDtcclxuJGRhcmtCbHVlOiAjNDVBMkQyO1xyXG4kbGlnaHRHcmVlbjogIzI3YWU2MDtcclxuJGxpZ2h0LWJsdWU6ICNmNWY3Zjc7XHJcbiRicmlnaHRHcmF5OiAjNzU3NTc1O1xyXG4kbWF4LXdpZHRoLW1vYmlsZTogNzY4cHg7XHJcbiRtYXgtd2lkdGgtdGFibGV0OiA5OTJweDtcclxuJG1heC13aWR0aC1kZXNrdG9wOiAxMjgwcHg7XHJcblxyXG4vLyBCRUdJTjogTWFyZ2luXHJcbkBtaXhpbiBuaC1tZygkcGl4ZWwpIHtcclxuICAgIG1hcmdpbjogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ3QoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tdG9wOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1nYigkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1ib3R0b206ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWdsKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLWxlZnQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWdyKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBNYXJnaW5cclxuXHJcbi8vIEJFR0lOOiBQYWRkaW5nXHJcbkBtaXhpbiBuaC1wZCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmc6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGR0KCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy10b3A6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGRiKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy1ib3R0b206ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGRsKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy1sZWZ0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkcigkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctcmlnaHQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vLyBFTkQ6IFBhZGRpbmdcclxuXHJcbi8vIEJFR0lOOiBXaWR0aFxyXG5AbWl4aW4gbmgtd2lkdGgoJHdpZHRoKSB7XHJcbiAgICB3aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbiAgICBtaW4td2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG4gICAgbWF4LXdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBXaWR0aFxyXG5cclxuLy8gQkVHSU46IEljb24gU2l6ZVxyXG5AbWl4aW4gbmgtc2l6ZS1pY29uKCRzaXplKSB7XHJcbiAgICB3aWR0aDogJHNpemU7XHJcbiAgICBoZWlnaHQ6ICRzaXplO1xyXG4gICAgZm9udC1zaXplOiAkc2l6ZTtcclxufVxyXG5cclxuLy8gRU5EOiBJY29uIFNpemVcclxuIl19 */"

/***/ }),

/***/ "./src/app/modules/task/target/targets/targets-form/targets-form.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/targets-form/targets-form.component.ts ***!
  \************************************************************************************/
/*! exports provided: TargetsFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetsFormComponent", function() { return TargetsFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _models_target_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../models/target.model */ "./src/app/modules/task/target/targets/models/target.model.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _services_target_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/target.service */ "./src/app/modules/task/target/targets/services/target.service.ts");
/* harmony import */ var _hr_organization_office_services_office_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../hr/organization/office/services/office.service */ "./src/app/modules/hr/organization/office/services/office.service.ts");
/* harmony import */ var _constant_type_target_const__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../constant/type-target.const */ "./src/app/modules/task/target/targets/constant/type-target.const.ts");
/* harmony import */ var _target_table_service_target_table_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../target-table/service/target-table.service */ "./src/app/modules/task/target/target-table/service/target-table.service.ts");
/* harmony import */ var _validators_number_validator__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../../../validators/number.validator */ "./src/app/validators/number.validator.ts");
/* harmony import */ var _validators_datetime_validator__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../../../validators/datetime.validator */ "./src/app/validators/datetime.validator.ts");
/* harmony import */ var _shareds_components_nh_user_picker_nh_user_picker_model__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../../../shareds/components/nh-user-picker/nh-user-picker.model */ "./src/app/shareds/components/nh-user-picker/nh-user-picker.model.ts");
/* harmony import */ var _shareds_components_nh_user_picker_nh_user_picker_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../../../../shareds/components/nh-user-picker/nh-user-picker.component */ "./src/app/shareds/components/nh-user-picker/nh-user-picker.component.ts");
/* harmony import */ var _models_target_participants_model__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../models/target-participants.model */ "./src/app/modules/task/target/targets/models/target-participants.model.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _models_target_participants_permission_const__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../models/target-participants-permission.const */ "./src/app/modules/task/target/targets/models/target-participants-permission.const.ts");
/* harmony import */ var _target_libraries_services_target_group_library_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../target-libraries/services/target-group-library.service */ "./src/app/modules/task/target/target-libraries/services/target-group-library.service.ts");
/* harmony import */ var _shareds_components_ghm_select_ghm_select_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../../../../../shareds/components/ghm-select/ghm-select.component */ "./src/app/shareds/components/ghm-select/ghm-select.component.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../../../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _shareds_components_ghm_suggestion_user_ghm_suggestion_user_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../../../../../shareds/components/ghm-suggestion-user/ghm-suggestion-user.component */ "./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.component.ts");
/* harmony import */ var _target_libraries_target_library_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../../target-libraries/target-library.component */ "./src/app/modules/task/target/target-libraries/target-library.component.ts");
/* harmony import */ var _target_share_component_target_select_tree_target_select_tree_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ../../target-share-component/target-select-tree/target-select-tree.component */ "./src/app/modules/task/target/target-share-component/target-select-tree/target-select-tree.component.ts");


























var TargetsFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TargetsFormComponent, _super);
    function TargetsFormComponent(data, dialog, utilService, fb, officeService, targetTableService, numberValidator, dateTimeValidator, toastrService, targetService, targetGroupLibraryService, dialogRef) {
        var _this = _super.call(this) || this;
        _this.data = data;
        _this.dialog = dialog;
        _this.utilService = utilService;
        _this.fb = fb;
        _this.officeService = officeService;
        _this.targetTableService = targetTableService;
        _this.numberValidator = numberValidator;
        _this.dateTimeValidator = dateTimeValidator;
        _this.toastrService = toastrService;
        _this.targetService = targetService;
        _this.targetGroupLibraryService = targetGroupLibraryService;
        _this.dialogRef = dialogRef;
        _this.target = new _models_target_model__WEBPACK_IMPORTED_MODULE_6__["Target"]();
        _this.officeTree = [];
        _this.targetType = _constant_type_target_const__WEBPACK_IMPORTED_MODULE_10__["TypeTarget"];
        _this.isShowMore = false;
        _this.listSelectedUsers = [];
        _this.listTargetParticipants = [];
        _this.permissionTargetConst = _models_target_participants_permission_const__WEBPACK_IMPORTED_MODULE_18__["TargetParticipantsPermission"];
        _this.listLevelOfSharing = [];
        _this.listAttachment = [];
        _this.urlUser = _environments_environment__WEBPACK_IMPORTED_MODULE_21__["environment"].apiGatewayUrl + "api/v1/hr/users/suggestions";
        return _this;
    }
    TargetsFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.buildForm();
        if (this.data) {
            this.listTargetTable = this.data.listTargetTable;
            this.type = this.data.type;
            if (this.data.target) {
                this.targetParentInfo = this.data.target;
            }
            setTimeout(function () {
                _this.add(_this.data.targetTable);
            });
        }
        this.listLevelOfSharing = [
            { id: 0, name: this.type === _constant_type_target_const__WEBPACK_IMPORTED_MODULE_10__["TypeTarget"].office ? 'Riêng trong từng phòng ban bộ phạn' : 'Riêng' },
            { id: 1, name: this.type === _constant_type_target_const__WEBPACK_IMPORTED_MODULE_10__["TypeTarget"].office ? 'Chung trong toàn doanh nghiệp' : 'Chung' }
        ];
    };
    // Hàm thêm
    TargetsFormComponent.prototype.add = function (targetTable) {
        var _this = this;
        this.isUpdate = false;
        this.isShowMore = false;
        this.resetForm();
        if (this.type === this.targetType.personal) {
            setTimeout(function () {
                _this.ghmSelectUser.selectedItem = new _shareds_components_ghm_select_ghm_select_component__WEBPACK_IMPORTED_MODULE_20__["GhmSelect"](_this.currentUser.id, _this.currentUser.fullName, '', _this.currentUser.avatar, _this.currentUser, 1, true, true);
            });
            this.model.patchValue({ userId: this.currentUser.id, fullName: this.currentUser.fullName });
        }
        else {
            this.officeService.getOfficeUserTree().subscribe(function (result) {
                _this.officeTree = result;
            });
        }
        if (targetTable && !this.targetParentInfo) {
            this.searchTargetGroupByTargetId(targetTable.id);
        }
        else {
            this.searchTargetGroupByTargetId(this.targetParentInfo.targetTableId);
        }
        if (this.targetParentInfo) {
            this.model.patchValue({
                type: this.targetParentInfo.type,
                startDate: this.targetParentInfo.startDate,
                endDate: this.targetParentInfo.endDate,
                targetTableId: this.targetParentInfo.targetTableId,
                levelOfSharing: this.targetParentInfo.levelOfSharing,
                measurementMethod: this.targetParentInfo.measurementMethod,
                methodCalculateResult: this.targetParentInfo.methodCalculateResult,
                parentId: this.targetParentInfo.id
            });
        }
        else {
            this.model.patchValue({ type: this.type, startDate: '', endDate: '', targetTableId: targetTable ? targetTable.id : '' });
        }
        this.utilService.focusElement('name');
    };
    TargetsFormComponent.prototype.selectUser = function (value) {
        if (value) {
            this.model.patchValue({ userId: value.id, fullName: value.name });
        }
    };
    // thay đổi loại mục tiêu
    TargetsFormComponent.prototype.changeTargetType = function (value) {
        var _this = this;
        if (value) {
            if (value.value === this.targetType.personal) {
                setTimeout(function () {
                    _this.ghmSelectUser.selectedItem = new _shareds_components_ghm_select_ghm_select_component__WEBPACK_IMPORTED_MODULE_20__["GhmSelect"](_this.currentUser.id, _this.currentUser.fullName, '', _this.currentUser.avatar, _this.currentUser, 1, true, true);
                    _this.model.patchValue({ userId: _this.currentUser.id, fullName: _this.currentUser.fullName });
                });
            }
            else {
                this.officeService.getOfficeUserTree().subscribe(function (result) {
                    _this.officeTree = result;
                });
            }
        }
    };
    // Chọn phòng ban
    TargetsFormComponent.prototype.onSelectOffice = function (item) {
        if (item) {
            this.model.patchValue({ officeId: item.id });
        }
    };
    // Thay đổi quyền người tham gia
    TargetsFormComponent.prototype.onChangePermission = function (id, permission) {
        var targetParticipants = lodash__WEBPACK_IMPORTED_MODULE_17__["find"](this.listTargetParticipants, function (result) {
            return result.userId === id;
        });
        switch (permission) {
            case this.permissionTargetConst.isNotification:
                targetParticipants.isNotification = !targetParticipants.isNotification;
                break;
            case this.permissionTargetConst.isDisputation:
                targetParticipants.isDisputation = !targetParticipants.isDisputation;
                break;
            case this.permissionTargetConst.isReport:
                targetParticipants.isReport = !targetParticipants.isReport;
                break;
            case this.permissionTargetConst.isWriteTask:
                targetParticipants.isWriteTask = !targetParticipants.isWriteTask;
                break;
            case this.permissionTargetConst.isWriteTarget:
                targetParticipants.isWriteTarget = !targetParticipants.isWriteTarget;
                break;
            case this.permissionTargetConst.isWriteTargetChild:
                targetParticipants.isWriteTargetChild = !targetParticipants.isWriteTargetChild;
                break;
        }
    };
    // Đồng ý chọn danh sách người tham gia
    TargetsFormComponent.prototype.onAcceptSelectUsers = function (value) {
        var _this = this;
        if (!this.listSelectedUsers || this.listSelectedUsers.length === 0) {
            this.listSelectedUsers = value.listUser;
            this.convertListUserPickerToUserParticipants();
        }
        else {
            if (value.listUserInsert && value.listUserInsert.length > 0) {
                lodash__WEBPACK_IMPORTED_MODULE_17__["each"](value.listUserInsert, function (userInsert) {
                    var userOwnerId = _this.model.value.type === _this.targetType.personal ? _this.model.value.userId : '';
                    if (userInsert.id !== userOwnerId) {
                        _this.insertTargetParticipants(userInsert);
                        _this.insertUserSelect(userInsert);
                    }
                });
            }
            if (value.listUserRemove && value.listUserRemove.length > 0) {
                lodash__WEBPACK_IMPORTED_MODULE_17__["each"](value.listUserRemove, function (userRemove) {
                    _this.removeParticipants(userRemove.id.toString());
                });
            }
        }
    };
    // Chọn user from ghm-select
    TargetsFormComponent.prototype.selectUserParticipants = function (value) {
        var userOwnerId = this.model.value.type === this.targetType.personal ? this.model.value.userId : '';
        if (value && userOwnerId !== value.id) {
            var userPicker = new _shareds_components_nh_user_picker_nh_user_picker_model__WEBPACK_IMPORTED_MODULE_14__["NhUserPicker"](value.id, value.fullName, value.avatar, value.officeName + " - " + value.positionName);
            this.insertTargetParticipants(userPicker);
            this.insertUserSelect(userPicker);
        }
    };
    // Xóa người tham gia
    TargetsFormComponent.prototype.removeParticipants = function (userId) {
        lodash__WEBPACK_IMPORTED_MODULE_17__["remove"](this.listTargetParticipants, function (item) {
            return item.userId === userId;
        });
        lodash__WEBPACK_IMPORTED_MODULE_17__["remove"](this.listSelectedUsers, function (userSelect) {
            return userSelect.id === userId;
        });
    };
    // Chọn bảng mục tiêu
    TargetsFormComponent.prototype.selectTargetTableId = function (item) {
        this.model.patchValue({ targetTableId: item ? item.id : '' });
        if (item.id) {
            this.model.patchValue({ targetTableId: item.id });
            this.searchTargetGroupByTargetId(item.id);
        }
        else {
            this.listTargetGroup = [];
        }
    };
    // Hiển thị danh sách thư viện mục tiêu
    TargetsFormComponent.prototype.showTargetLibrary = function () {
        var _this = this;
        var targetLibraryDialog = this.dialog.open(_target_libraries_target_library_component__WEBPACK_IMPORTED_MODULE_24__["TargetLibraryComponent"], {
            id: "targetLibrary",
            data: { isSelect: true }
        });
        targetLibraryDialog.afterClosed()
            .subscribe(function (data) {
            if (data && data.targetLibrary) {
                var targetLibrary = data.targetLibrary;
                _this.model.patchValue({
                    name: targetLibrary.name,
                    description: targetLibrary.description,
                    measurementMethod: targetLibrary.measurementMethod
                });
            }
        });
    };
    // Chọn file
    TargetsFormComponent.prototype.selectFileAttachment = function (value) {
        var _this = this;
        if (value) {
            if (value.listFileInsert) {
                lodash__WEBPACK_IMPORTED_MODULE_17__["each"](value.listFileInsert, function (file) {
                    _this.listAttachment.push(file);
                });
            }
            if (value.fileInsert) {
                this.listAttachment.push(value.fileInsert);
            }
        }
    };
    TargetsFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.utilService.onValueChanged(this.model, this.formErrors, this.validationMessages, true);
        if (isValid) {
            this.target = this.model.value;
            lodash__WEBPACK_IMPORTED_MODULE_17__["each"](this.listTargetParticipants, function (item) {
                item.role = _this.calculatorPermission(item);
            });
            this.target.targetAttachments = this.listAttachment;
            this.target.targetParticipants = this.listTargetParticipants;
            if (this.isUpdate) {
            }
            else {
                this.isSaving = true;
                this.targetService.insert(this.target)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function (result) {
                    _this.isModified = true;
                    _this.target.id = result.data;
                    _this.dialogRef.close({ isModified: _this.isModified, target: _this.target });
                    // this.saveSuccessful.emit(this.target);
                });
            }
        }
    };
    TargetsFormComponent.prototype.addTargetChild = function () {
        var _this = this;
        var dialogTargetAddChild = this.dialog.open(_target_share_component_target_select_tree_target_select_tree_component__WEBPACK_IMPORTED_MODULE_25__["TargetSelectTreeComponent"], {
            id: "formAddTargetChild",
            data: {
                type: this.model.value.type,
                userId: this.model.value.userId,
                officeId: this.model.value.officeId,
                targetTableId: this.model.value.targetTableId
            },
            disableClose: true
        });
        dialogTargetAddChild.afterClosed().subscribe(function (data) {
            if (data && data.targetParent) {
                _this.targetParent = data.targetParent;
                _this.model.patchValue({ parentId: _this.targetParent.id });
            }
        });
    };
    TargetsFormComponent.prototype.searchTargetGroupByTargetId = function (targetTableId) {
        var _this = this;
        this.targetTableService.searchTargetGroupByTargetTableId(targetTableId)
            .subscribe(function (result) {
            _this.listTargetGroup = result;
            if (_this.targetParentInfo && _this.listTargetGroup) {
                _this.model.patchValue({ targetGroupId: _this.targetParentInfo.targetGroupId });
            }
        });
    };
    TargetsFormComponent.prototype.calculatorPermission = function (item) {
        var result = 0;
        if (item.isRead) {
            result += this.permissionTargetConst.isRead;
        }
        if (item.isNotification) {
            result += this.permissionTargetConst.isNotification;
        }
        if (item.isDisputation) {
            result += this.permissionTargetConst.isDisputation;
        }
        if (item.isReport) {
            result += this.permissionTargetConst.isReport;
        }
        if (item.isWriteTask) {
            result += this.permissionTargetConst.isWriteTask;
        }
        if (item.isWriteTarget) {
            result += this.permissionTargetConst.isWriteTarget;
        }
        if (item.isWriteTargetChild) {
            result += this.permissionTargetConst.isWriteTargetChild;
        }
        return result;
    };
    TargetsFormComponent.prototype.convertListUserPickerToUserParticipants = function () {
        var _this = this;
        this.listTargetParticipants = [];
        lodash__WEBPACK_IMPORTED_MODULE_17__["each"](this.listSelectedUsers, function (userPicker) {
            _this.insertTargetParticipants(userPicker);
        });
    };
    TargetsFormComponent.prototype.insertTargetParticipants = function (user) {
        var userParticipantInfo = lodash__WEBPACK_IMPORTED_MODULE_17__["find"](this.listTargetParticipants, function (targetParticipant) {
            return targetParticipant.userId === user.id;
        });
        if (!userParticipantInfo) {
            var targetParticipants = new _models_target_participants_model__WEBPACK_IMPORTED_MODULE_16__["TargetParticipants"]();
            targetParticipants.userId = user.id ? user.id.toString() : '';
            targetParticipants.fullName = user.fullName;
            targetParticipants.image = user.avatar;
            targetParticipants.role = this.calculatorPermission(targetParticipants);
            this.listTargetParticipants.push(targetParticipants);
        }
    };
    TargetsFormComponent.prototype.insertUserSelect = function (userPicker) {
        var userInfo = lodash__WEBPACK_IMPORTED_MODULE_17__["find"](this.listSelectedUsers, function (item) {
            return item.id === userPicker.id;
        });
        if (!userInfo) {
            this.listSelectedUsers.push(userPicker);
        }
    };
    TargetsFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.renderFormError(['name', 'description',
            'targetGroupId', 'targetTableId', 'levelOfSharing', 'startDate', 'endDate', 'weight', 'measurementMethod']);
        this.validationMessages = this.renderFormErrorMessage([
            { 'name': ['required', 'maxlength'] },
            { 'description': ['maxlength'] },
            { 'targetGroupId': ['required'] },
            { 'targetTableId': ['required'] },
            { 'description': ['maxLength'] },
            { 'levelOfSharing': ['required'] },
            { 'startDate': ['required', 'isValid', 'notAfter'] },
            { 'endDate': ['required', 'isValid', 'notBefore'] },
            { 'weight': ['isValid', 'greaterThan', 'lessThan'] },
            { 'measurementMethod': ['maxlength'] }
        ]);
        this.model = this.fb.group({
            officeId: [this.target.officeId],
            parentId: [this.target.parentId],
            name: [this.target.name, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(256)]],
            targetGroupId: [this.target.targetGroupId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required
                ]],
            targetTableId: [this.target.targetTableId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required
                ]],
            description: [this.target.description, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(4000)
                ]],
            levelOfSharing: [this.target.levelOfSharing, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required
                ]],
            startDate: [this.target.startDate, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, this.dateTimeValidator.isValid, this.dateTimeValidator.notAfter('endDate')
                ]],
            endDate: [this.target.endDate, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, this.dateTimeValidator.isValid, this.dateTimeValidator.notBefore('startDate')
                ]],
            type: [this.target.type],
            userId: [this.target.userId],
            fullName: [this.target.fullName],
            weight: [this.target.weight, [this.numberValidator.isValid,
                    this.numberValidator.greaterThan(-1), this.numberValidator.lessThan(101)]],
            measurementMethod: [this.target.measurementMethod, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(256)]
        });
        this.subscribers.modelValueChanges = this.model.valueChanges.subscribe(function () { return _this.validateModel(false); });
    };
    TargetsFormComponent.prototype.resetForm = function () {
        this.listAttachment = [];
        this.listTargetGroup = [];
        this.listSelectedUsers = [];
        this.model.patchValue(new _models_target_model__WEBPACK_IMPORTED_MODULE_6__["Target"]());
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_shareds_components_nh_user_picker_nh_user_picker_component__WEBPACK_IMPORTED_MODULE_15__["NhUserPickerComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_user_picker_nh_user_picker_component__WEBPACK_IMPORTED_MODULE_15__["NhUserPickerComponent"])
    ], TargetsFormComponent.prototype, "userPickerComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('ghmSelectUser'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_suggestion_user_ghm_suggestion_user_component__WEBPACK_IMPORTED_MODULE_23__["GhmSuggestionUserComponent"])
    ], TargetsFormComponent.prototype, "ghmSelectUser", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], TargetsFormComponent.prototype, "listTargetTable", void 0);
    TargetsFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-targets-form',
            template: __webpack_require__(/*! ./targets-form.component.html */ "./src/app/modules/task/target/targets/targets-form/targets-form.component.html"),
            styles: [__webpack_require__(/*! ./targets-form.component.scss */ "./src/app/modules/task/target/targets/targets-form/targets-form.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_22__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_22__["MatDialog"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_5__["UtilService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _hr_organization_office_services_office_service__WEBPACK_IMPORTED_MODULE_9__["OfficeService"],
            _target_table_service_target_table_service__WEBPACK_IMPORTED_MODULE_11__["TargetTableService"],
            _validators_number_validator__WEBPACK_IMPORTED_MODULE_12__["NumberValidator"],
            _validators_datetime_validator__WEBPACK_IMPORTED_MODULE_13__["DateTimeValidator"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"],
            _services_target_service__WEBPACK_IMPORTED_MODULE_8__["TargetService"],
            _target_libraries_services_target_group_library_service__WEBPACK_IMPORTED_MODULE_19__["TargetGroupLibraryService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_22__["MatDialogRef"]])
    ], TargetsFormComponent);
    return TargetsFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/task/target/targets/targets.component.html":
/*!********************************************************************!*\
  !*** ./src/app/modules/task/target/targets/targets.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span i18n=\"@@listTargetTable\">\r\n        Mục tiêu {type, select, 0 {đơn vị} 1 {nhân viên}}\r\n    </span>\r\n</h1>\r\n<div class=\"row tool-bar\">\r\n    <div class=\"bar-act\">\r\n        <div class=\"bar-act-left\">\r\n            <a class=\"act-item\" (click)=\"addTarget()\" *ngIf=\"permission.add\">\r\n                <i class=\"fa fa-plus  cm-pdr-5\"></i>\r\n                <span class=\"act-item-text\" i18n=\"@@createTarget\"> Tạo mục tiêu</span>\r\n            </a>\r\n            <a class=\"act-item\" *ngIf=\"permission.edit\">\r\n                <i class=\"fa fa-file-excel-o color-green cm-pdr-5\" aria-hidden=\"true\"></i>\r\n                <span class=\"act-item-text\" i18n=\"@@importTarget\"> Import mục tiêu</span>\r\n            </a>\r\n        </div>\r\n        <div class=\"bar-act-right\">\r\n            <a class=\"act-item\" ghmDialogTrigger=\"\" [ghmDialogTriggerFor]=\"ghmDialog\">\r\n                <i class=\"fa fa-filter cm-pdr-5\" aria-hidden=\"true\"></i>\r\n                <span class=\"act-item-text\">Lọc mục tiêu</span>\r\n            </a>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"col-md-9\">\r\n        <ng-container *ngIf=\"listData && listData.length > 0; else notData\">\r\n            <div class=\"target-list\" *ngFor=\"let data of listData; let i = index\">\r\n                <div class=\"para-title\">\r\n                    <b class=\"pull-left\">{{data.targetTableName}}</b>\r\n                    <a class=\"pull-right\" *ngIf=\"i === 0\" (click)=\"showTargetChild()\">{isShowTargetChild, select, 0\r\n                        {Hiển thị mục tiêu con} 1 {Ẩn mục tiêu con}} </a>\r\n                </div>\r\n                <ng-container *ngIf=\"data.targetGroups && data.targetGroups.length > 0\">\r\n                    <div *ngFor=\"let targetGroup of data.targetGroups\">\r\n                        <target-group-tree [data]=\"targetGroup\"\r\n                                           [type]=\"type\"\r\n                                           (editData)=\"update($event)\"\r\n                                           (remove)=\"search()\"\r\n                                           (onSaveSuccess)=\"quickUpdate($event)\"></target-group-tree>\r\n                    </div>\r\n                </ng-container>\r\n            </div>\r\n        </ng-container>\r\n        <ng-template #notData>\r\n            <div class=\"box-confirm bg-info-blue\">\r\n                <i class=\"fa fa-info-circle color-dark-blue font-size-18\"\r\n                   aria-hidden=\"true\"></i><span> Không có dữ liệu</span>\r\n            </div>\r\n        </ng-template>\r\n    </div>\r\n    <div class=\"col-md-3 cm-pdl-0\">\r\n        <div class=\"gw-suggest\">\r\n            <div class=\"gr-box\">\r\n                <div class=\"gr-header center\">\r\n                    Tiến độ hiện tại\r\n                </div>\r\n                <div class=\"gr-content center\">\r\n                    {{totalPercentComplete | formatNumber}}%\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <ghm-app-suggestion\r\n            [header]=\"type === typeTarget.personal ? 'Bạn đang xem mục tiêu của' : 'Bạn đang xem mục tiêu của đơn vị'\"\r\n            [title]=\"type === typeTarget.personal ? user?.fullName : office?.name\"\r\n            [avatar]=\"type === typeTarget.personal ? user?.avatar : ''\"\r\n            [description]=\"type === typeTarget.personal ? user?.positionName : ''\"></ghm-app-suggestion>\r\n        <div class=\"app-suggest cm-mgt-15\" *ngIf=\"type == typeTarget.personal\">\r\n            <div class=\"header\">\r\n                <span> Bạn muốn xem mục tiêu của người khác</span>\r\n            </div>\r\n            <ghm-suggestion-user (itemSelected)=\"selectUser($event)\">\r\n            </ghm-suggestion-user>\r\n        </div>\r\n        <div class=\"app-suggest cm-mgt-15\" *ngIf=\"type === typeTarget.office\">\r\n            <div class=\"header\">\r\n                <span> Bạn muốn xem mục tiêu của đơn vị khác</span>\r\n            </div>\r\n            <ghm-select [elementId]=\"'selectOfficeInList'\"\r\n                        [icon]=\"'fa fa-search'\"\r\n                        [liveSearch]=\"true\"\r\n                        [url]=\"urlOfficeSuggestion\"\r\n                        i18n-title=\"@@selectOffice\"\r\n                        [title]=\"'-- Chọn phòng ban --'\"\r\n                        (itemSelected)=\"selectOffice($event)\"></ghm-select>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<ghm-dialog #ghmDialog [size]=\"'sm'\"\r\n            [backdropStatic]=\"true\" class=\"cm-mgt-0\">\r\n    <ghm-dialog-header>\r\n        <i class=\"fa fa-filter color-dark-blue\"> </i>\r\n        <span i18n=\"@@filterTarget\" class=\"cm-pdl-10\">Lọc mục tiêu</span>\r\n    </ghm-dialog-header>\r\n    <form action=\"\" (ngSubmit)=\"search()\">\r\n        <ghm-dialog-content>\r\n            <div class=\"row\">\r\n                <div class=\"form-group\">\r\n                    <div class=\"col-sm-12 cm-mgt-10\">\r\n                        <label class=\"label-control bold\" i18n=\"@@targetTable\">Bảng mục tiêu</label>\r\n                        <ghm-select\r\n                            [elementId]=\"'selectTargetTableSearch'\"\r\n                            [data]=\"listTargetTable\"\r\n                            title=\"Tất cả\"\r\n                            i18n-title=\"@@all\"\r\n                            (itemSelected)=\"selectTargetTable($event)\"></ghm-select>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <div class=\"col-sm-12 cm-mgt-10\">\r\n                        <label class=\"label-control bold\" i18n=\"@@status\">Trạng thái</label>\r\n                        <ghm-select\r\n                            [data]=\"[{id: targetStatus.new, name: 'Chưa hoàn thành'}, {id: targetStatus.finish, name: 'Hoàn thành'}]\"\r\n                            [value]=\"status\"\r\n                            [title]=\"'Tất cả'\"\r\n                            i18n-title=\"@@all\"\r\n                            (itemSelected)=\"status = $event.id\"\r\n                        ></ghm-select>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\" *ngIf=\"type === typeTarget.personal\">\r\n                    <div class=\"col-sm-12 cm-mgt-10\">\r\n                        <label class=\"label-control bold\" i18n=\"@@targetType\">Loại mục tiêu</label>\r\n                        <ul class=\"list-inline cm-mgb-0\">\r\n                            <li class=\"list-inline-item\">\r\n                                <mat-checkbox color=\"primary\"\r\n                                              (change)=\"changeTargetType(typeTarget.personal)\"\r\n                                              [checked]=\"listType.indexOf(typeTarget.personal) !== -1\">\r\n                                    Cá nhân\r\n                                </mat-checkbox>\r\n                            </li>\r\n                            <li class=\"list-inline-item\">\r\n                                <mat-checkbox color=\"primary\"\r\n                                              (change)=\"changeTargetType(typeTarget.office)\"\r\n                                              [checked]=\"listType.indexOf(typeTarget.office) !== -1\">\r\n                                    Đơn vị\r\n                                </mat-checkbox>\r\n                            </li>\r\n                            <li class=\"list-inline-item\">\r\n                                <mat-checkbox color=\"primary\"\r\n                                              (change)=\"changeTargetType(typeTarget.participants)\"\r\n                                              [checked]=\"listType.indexOf(typeTarget.participants) !== -1\">\r\n                                    Tham gia\r\n                                </mat-checkbox>\r\n                            </li>\r\n                        </ul>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <div class=\"col-sm-12 cm-mgt-10\">\r\n                        <label class=\"label-control bold\" i18n=\"@@time\">Thời gian</label>\r\n                        <div class=\"inline-block w100pc\">\r\n                            <mat-checkbox color=\"primary\" [checked]=\"isShowCurrentTime\"\r\n                                          (change)=\"clickShowCurrentTime()\">Chỉ hiện thị thời gian hiện tại\r\n                            </mat-checkbox>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-sm-12\">\r\n                        <ghm-select-datetime\r\n                            [dateRange]=\"dateRange\"\r\n                            [readonly]=\"isShowCurrentTime\"\r\n                            (selectDate)=\"selectDate($event)\"\r\n                        ></ghm-select-datetime>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </ghm-dialog-content>\r\n        <ghm-dialog-footer>\r\n            <div class=\"center\">\r\n                <button class=\"btn blue cm-mgr-5\" i18n=\"@@search\">Tìm kiếm</button>\r\n                <button class=\"btn btn-light\" i18n=\"@@close\" ghm-dismiss=\"true\" type=\"button\">\r\n                    Đóng\r\n                </button>\r\n            </div>\r\n        </ghm-dialog-footer>\r\n    </form>\r\n</ghm-dialog>\r\n"

/***/ }),

/***/ "./src/app/modules/task/target/targets/targets.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/modules/task/target/targets/targets.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".target-list {\n  position: relative;\n  float: left;\n  border-radius: 4px !important;\n  -moz-border-radius: 4px;\n  -webkit-border-radius: 4px;\n  -ms-border-radius: 4px;\n  -o-border-radius: 4px;\n  min-height: 32px;\n  background-color: #f5f7f7;\n  padding: 12px;\n  margin-bottom: 12px; }\n  .target-list .para-title {\n    display: inline-block;\n    margin-bottom: 10px;\n    width: 100%; }\n  .target-list .para-title a {\n      color: #757575;\n      text-decoration: underline; }\n  i {\n  font-weight: normal !important; }\n  .gw-suggest .gr-box {\n  padding: 10px 0px;\n  width: 100%;\n  border-radius: 4px !important;\n  border-radius: 4px;\n  color: #fff;\n  background: #27ae60;\n  font-size: 16px; }\n  .gw-suggest .gr-box .gr-content {\n    font-size: 24px;\n    font-weight: bold; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy90YXNrL3RhcmdldC90YXJnZXRzL0Q6XFxQcm9qZWN0XFxHaG1BcHBsaWNhdGlvblxcY2xpZW50c1xcZ2htYXBwbGljYXRpb25jbGllbnQvc3JjXFxhcHBcXG1vZHVsZXNcXHRhc2tcXHRhcmdldFxcdGFyZ2V0c1xcdGFyZ2V0cy5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbW9kdWxlcy90YXNrL3RhcmdldC90YXJnZXRzL0Q6XFxQcm9qZWN0XFxHaG1BcHBsaWNhdGlvblxcY2xpZW50c1xcZ2htYXBwbGljYXRpb25jbGllbnQvc3JjXFxhc3NldHNcXHN0eWxlc1xcX2NvbmZpZy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCw2QkFBNkI7RUFDN0IsdUJBQXVCO0VBQ3ZCLDBCQUEwQjtFQUMxQixzQkFBc0I7RUFDdEIscUJBQXFCO0VBQ3JCLGdCQUFnQjtFQUNoQix5QkN5QmdCO0VEeEJoQixhQUFhO0VBQ2IsbUJBQW1CLEVBQUE7RUFYdkI7SUFjUSxxQkFBcUI7SUFDckIsbUJBQW1CO0lBQ25CLFdBQVcsRUFBQTtFQWhCbkI7TUFrQlksY0NpQlE7TURoQlIsMEJBQTBCLEVBQUE7RUFLdEM7RUFDSSw4QkFBOEIsRUFBQTtFQUdsQztFQUVRLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsNkJBQTZCO0VBSzdCLGtCQUFrQjtFQUNsQixXQ3JCSTtFRHNCSixtQkNOWTtFRE9aLGVBQWUsRUFBQTtFQVp2QjtJQWNZLGVBQWU7SUFDZixpQkFBaUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvdGFzay90YXJnZXQvdGFyZ2V0cy90YXJnZXRzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCAnLi4vLi4vLi4vLi4vLi4vYXNzZXRzL3N0eWxlcy9jb25maWcnO1xyXG5cclxuLnRhcmdldC1saXN0IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAtbW96LWJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gICAgLW1zLWJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgIC1vLWJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgIG1pbi1oZWlnaHQ6IDMycHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkbGlnaHQtYmx1ZTtcclxuICAgIHBhZGRpbmc6IDEycHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMnB4O1xyXG5cclxuICAgIC5wYXJhLXRpdGxlIHtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBhIHtcclxuICAgICAgICAgICAgY29sb3I6ICRicmlnaHRHcmF5O1xyXG4gICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbmkge1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uZ3ctc3VnZ2VzdCB7XHJcbiAgICAuZ3ItYm94IHtcclxuICAgICAgICBwYWRkaW5nOiAxMHB4IDBweDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA0cHggIWltcG9ydGFudDtcclxuICAgICAgICAtbW96LWJvcmRlci1yYWRpdXM6IDRweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgLW1zLWJvcmRlci1yYWRpdXM6IDRweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIC1vLWJvcmRlci1yYWRpdXM6IDRweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgICAgICBjb2xvcjogJHdoaXRlO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICRsaWdodEdyZWVuO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgICAgICAuZ3ItY29udGVudCB7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjRweDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5cclxuIiwiJGRlZmF1bHQtY29sb3I6ICMyMjI7XHJcbiRmb250LWZhbWlseTogXCJBcmlhbFwiLCB0YWhvbWEsIEhlbHZldGljYSBOZXVlO1xyXG4kY29sb3ItYmx1ZTogIzM1OThkYztcclxuJG1haW4tY29sb3I6ICMwMDc0NTU7XHJcbiRib3JkZXJDb2xvcjogI2RkZDtcclxuJHNlY29uZC1jb2xvcjogI2IwMWExZjtcclxuJHRhYmxlLWJhY2tncm91bmQtY29sb3I6ICMwMDk2ODg7XHJcbiRibHVlOiAjMDA3YmZmO1xyXG4kZGFyay1ibHVlOiAjMDA3MkJDO1xyXG4kYnJpZ2h0LWJsdWU6ICNkZmYwZmQ7XHJcbiRpbmRpZ286ICM2NjEwZjI7XHJcbiRwdXJwbGU6ICM2ZjQyYzE7XHJcbiRwaW5rOiAjZTgzZThjO1xyXG4kcmVkOiAjZGMzNTQ1O1xyXG4kb3JhbmdlOiAjZmQ3ZTE0O1xyXG4keWVsbG93OiAjZmZjMTA3O1xyXG4kZ3JlZW46ICMyOGE3NDU7XHJcbiR0ZWFsOiAjMjBjOTk3O1xyXG4kY3lhbjogIzE3YTJiODtcclxuJHdoaXRlOiAjZmZmO1xyXG4kZ3JheTogIzg2OGU5NjtcclxuJGdyYXktZGFyazogIzM0M2E0MDtcclxuJHByaW1hcnk6ICMwMDdiZmY7XHJcbiRzZWNvbmRhcnk6ICM2Yzc1N2Q7XHJcbiRzdWNjZXNzOiAjMjhhNzQ1O1xyXG4kaW5mbzogIzE3YTJiODtcclxuJHdhcm5pbmc6ICNmZmMxMDc7XHJcbiRkYW5nZXI6ICNkYzM1NDU7XHJcbiRsaWdodDogI2Y4ZjlmYTtcclxuJGRhcms6ICMzNDNhNDA7XHJcbiRsYWJlbC1jb2xvcjogIzY2NjtcclxuJGJhY2tncm91bmQtY29sb3I6ICNFQ0YwRjE7XHJcbiRib3JkZXJBY3RpdmVDb2xvcjogIzgwYmRmZjtcclxuJGJvcmRlclJhZGl1czogMDtcclxuJGRhcmtCbHVlOiAjNDVBMkQyO1xyXG4kbGlnaHRHcmVlbjogIzI3YWU2MDtcclxuJGxpZ2h0LWJsdWU6ICNmNWY3Zjc7XHJcbiRicmlnaHRHcmF5OiAjNzU3NTc1O1xyXG4kbWF4LXdpZHRoLW1vYmlsZTogNzY4cHg7XHJcbiRtYXgtd2lkdGgtdGFibGV0OiA5OTJweDtcclxuJG1heC13aWR0aC1kZXNrdG9wOiAxMjgwcHg7XHJcblxyXG4vLyBCRUdJTjogTWFyZ2luXHJcbkBtaXhpbiBuaC1tZygkcGl4ZWwpIHtcclxuICAgIG1hcmdpbjogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ3QoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tdG9wOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1nYigkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1ib3R0b206ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWdsKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLWxlZnQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWdyKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBNYXJnaW5cclxuXHJcbi8vIEJFR0lOOiBQYWRkaW5nXHJcbkBtaXhpbiBuaC1wZCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmc6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGR0KCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy10b3A6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGRiKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy1ib3R0b206ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGRsKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy1sZWZ0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkcigkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctcmlnaHQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vLyBFTkQ6IFBhZGRpbmdcclxuXHJcbi8vIEJFR0lOOiBXaWR0aFxyXG5AbWl4aW4gbmgtd2lkdGgoJHdpZHRoKSB7XHJcbiAgICB3aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbiAgICBtaW4td2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG4gICAgbWF4LXdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBXaWR0aFxyXG5cclxuLy8gQkVHSU46IEljb24gU2l6ZVxyXG5AbWl4aW4gbmgtc2l6ZS1pY29uKCRzaXplKSB7XHJcbiAgICB3aWR0aDogJHNpemU7XHJcbiAgICBoZWlnaHQ6ICRzaXplO1xyXG4gICAgZm9udC1zaXplOiAkc2l6ZTtcclxufVxyXG5cclxuLy8gRU5EOiBJY29uIFNpemVcclxuIl19 */"

/***/ }),

/***/ "./src/app/modules/task/target/targets/targets.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/modules/task/target/targets/targets.component.ts ***!
  \******************************************************************/
/*! exports provided: TargetsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetsComponent", function() { return TargetsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _targets_form_targets_form_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./targets-form/targets-form.component */ "./src/app/modules/task/target/targets/targets-form/targets-form.component.ts");
/* harmony import */ var _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../shareds/components/ghm-dialog/ghm-dialog.component */ "./src/app/shareds/components/ghm-dialog/ghm-dialog.component.ts");
/* harmony import */ var _services_target_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./services/target.service */ "./src/app/modules/task/target/targets/services/target.service.ts");
/* harmony import */ var _target_table_service_target_table_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../target-table/service/target-table.service */ "./src/app/modules/task/target/target-table/service/target-table.service.ts");
/* harmony import */ var _targets_form_detail_targets_form_detail_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./targets-form-detail/targets-form-detail.component */ "./src/app/modules/task/target/targets/targets-form-detail/targets-form-detail.component.ts");
/* harmony import */ var _constant_type_target_const__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./constant/type-target.const */ "./src/app/modules/task/target/targets/constant/type-target.const.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _viewmodel_target_render_result_viewmodel__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./viewmodel/target-render-result.viewmodel */ "./src/app/modules/task/target/targets/viewmodel/target-render-result.viewmodel.ts");
/* harmony import */ var _constant_target_status_const__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./constant/target-status.const */ "./src/app/modules/task/target/targets/constant/target-status.const.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _shareds_constants_date_range_const__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../../../shareds/constants/date-range.const */ "./src/app/shareds/constants/date-range.const.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shareds_components_ghm_suggestion_user_ghm_suggestion_user_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../../../shareds/components/ghm-suggestion-user/ghm-suggestion-user.component */ "./src/app/shareds/components/ghm-suggestion-user/ghm-suggestion-user.component.ts");


















var TargetsComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TargetsComponent, _super);
    function TargetsComponent(targetService, targetTableService, route, router, dialog) {
        var _this = _super.call(this) || this;
        _this.targetService = targetService;
        _this.targetTableService = targetTableService;
        _this.route = route;
        _this.router = router;
        _this.dialog = dialog;
        _this.typeTarget = _constant_type_target_const__WEBPACK_IMPORTED_MODULE_8__["TypeTarget"];
        _this.targetStatus = _constant_target_status_const__WEBPACK_IMPORTED_MODULE_12__["TargetStatus"];
        _this.listType = [_constant_type_target_const__WEBPACK_IMPORTED_MODULE_8__["TypeTarget"].office, _constant_type_target_const__WEBPACK_IMPORTED_MODULE_8__["TypeTarget"].personal, _constant_type_target_const__WEBPACK_IMPORTED_MODULE_8__["TypeTarget"].participants];
        _this.isShowTargetChild = false;
        _this.currentUser = _this.appService.currentUser;
        _this.urlUserSuggestion = _environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].apiGatewayUrl + "api/v1/hr/users/suggestions";
        _this.urlOfficeSuggestion = _environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].apiGatewayUrl + "api/v1/hr/offices/suggestions";
        _this.targetTableService.searchForSuggestion()
            .subscribe(function (result) {
            _this.listTargetTable = result;
            if (_this.listTargetTable && _this.listTargetTable.length > 0) {
                _this.targetTableSearch = lodash__WEBPACK_IMPORTED_MODULE_10__["first"](_this.listTargetTable);
            }
        });
        _this.user = new _shareds_components_ghm_suggestion_user_ghm_suggestion_user_component__WEBPACK_IMPORTED_MODULE_17__["UserSuggestion"](_this.currentUser.id, _this.currentUser.fullName, '', '', _this.currentUser.avatar, true);
        return _this;
    }
    TargetsComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.route.routeConfig.path === 'personals') {
            this.type = _constant_type_target_const__WEBPACK_IMPORTED_MODULE_8__["TypeTarget"].personal;
            this.appService.setupPage(this.pageId.TARGET, this.pageId.TARGET_LIST, 'Quản lý mục tiêu', 'Mục tiêu cá nhân');
        }
        else {
            this.type = _constant_type_target_const__WEBPACK_IMPORTED_MODULE_8__["TypeTarget"].office;
            this.appService.setupPage(this.pageId.TARGET, this.pageId.TARGET_LIST, 'Quản lý mục tiêu', 'Mục tiêu đơn vị');
        }
        this.subscribers.data = this.route.data
            .subscribe(function (result) {
            if (result && result.data) {
                _this.listResult = result.data.items;
                _this.renderResult(result.data.items);
            }
        });
    };
    TargetsComponent.prototype.search = function () {
        var _this = this;
        if (this.type === this.typeTarget.office) {
            this.targetService.searchOfficeTarget(this.office ? this.office.id : null, this.keyword, this.startDate, this.endDate, this.targetTableId, this.status).subscribe(function (result) {
                _this.listResult = result.items;
                _this.renderResult(result.items);
                _this.ghmDialog.dismiss();
            });
        }
        else {
            this.targetService.searchPersonalTarget(this.user.id, this.keyword, this.startDate, this.endDate, this.targetTableId, this.status, this.listType).subscribe(function (result) {
                _this.listResult = result.items;
                _this.renderResult(result.items);
                _this.ghmDialog.dismiss();
            });
        }
    };
    TargetsComponent.prototype.addTarget = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_targets_form_targets_form_component__WEBPACK_IMPORTED_MODULE_3__["TargetsFormComponent"], {
            id: 'targetAddDialog',
            data: { type: this.type, listTargetTable: this.listTargetTable, targetTable: this.targetTableSearch },
            disableClose: true
        });
        dialogRef.afterClosed()
            .subscribe(function (data) {
            if (data.isModified) {
                _this.search();
            }
        });
    };
    TargetsComponent.prototype.update = function (target) {
        var _this = this;
        var dialogRef = this.dialog.open(_targets_form_detail_targets_form_detail_component__WEBPACK_IMPORTED_MODULE_7__["TargetsFormDetailComponent"], {
            id: 'targetDetailDialog',
            data: { id: target.id, targetTable: this.listTargetTable },
            disableClose: true
        });
        dialogRef.afterClosed()
            .subscribe(function (data) {
            if (data.isModified) {
                _this.search();
            }
        });
    };
    TargetsComponent.prototype.selectUser = function (value) {
        if (value) {
            this.user = value.data;
        }
        else {
            this.user = new _shareds_components_ghm_suggestion_user_ghm_suggestion_user_component__WEBPACK_IMPORTED_MODULE_17__["UserSuggestion"](this.currentUser.id, this.currentUser.fullName, '', '', this.currentUser.avatar, true);
        }
        this.search();
    };
    TargetsComponent.prototype.selectTargetTable = function (value) {
        if (value) {
            this.targetTableSearch = value;
            this.targetTableId = value.id;
        }
        else {
            this.targetTableSearch = null;
            this.targetTableId = null;
        }
    };
    TargetsComponent.prototype.selectOffice = function (value) {
        this.office = value ? value : null;
        this.search();
    };
    TargetsComponent.prototype.changeTargetType = function (type) {
        var existsType = lodash__WEBPACK_IMPORTED_MODULE_10__["find"](this.listType, function (item) {
            return item === type;
        });
        if (existsType) {
            lodash__WEBPACK_IMPORTED_MODULE_10__["remove"](this.listType, function (typeRemove) {
                return typeRemove === type;
            });
        }
        else {
            this.listType.push(type);
        }
    };
    TargetsComponent.prototype.clickShowCurrentTime = function () {
        this.isShowCurrentTime = !this.isShowCurrentTime;
        if (this.isShowCurrentTime) {
            this.dateRange = _shareds_constants_date_range_const__WEBPACK_IMPORTED_MODULE_15__["DateRange"].thisYear;
            this.startDate = moment__WEBPACK_IMPORTED_MODULE_14__().startOf('year');
            this.endDate = moment__WEBPACK_IMPORTED_MODULE_14__().endOf('year');
        }
        else {
            this.dateRange = null;
        }
    };
    TargetsComponent.prototype.showTargetChild = function () {
        var _this = this;
        this.isShowTargetChild = !this.isShowTargetChild;
        if (this.listResult && this.listResult.length > 0) {
            lodash__WEBPACK_IMPORTED_MODULE_10__["each"](this.listResult, function (result) {
                result.state.opened = _this.isShowTargetChild;
            });
        }
        this.renderResult(this.listResult);
    };
    TargetsComponent.prototype.selectDate = function (value) {
        if (value) {
            this.startDate = value.startDate;
            this.endDate = value.endDate;
        }
    };
    TargetsComponent.prototype.quickUpdate = function (value) {
        this.search();
    };
    TargetsComponent.prototype.renderResult = function (listTarget) {
        var _this = this;
        if (listTarget && listTarget.length > 0) {
            this.listData = [];
            var groupByTargetTableIds = lodash__WEBPACK_IMPORTED_MODULE_10__["groupBy"](listTarget, function (targetTable) {
                return targetTable.targetTableId;
            });
            lodash__WEBPACK_IMPORTED_MODULE_10__["each"](groupByTargetTableIds, function (groupByTargetTables) {
                var keyTargetTable = groupByTargetTables[0];
                var listTargetGroupResult = [];
                if (groupByTargetTables && groupByTargetTables.length > 0) {
                    var groupByTargetGroupIds = lodash__WEBPACK_IMPORTED_MODULE_10__["groupBy"](groupByTargetTables, function (targetGroup) {
                        return targetGroup.targetGroupId;
                    });
                    if (groupByTargetGroupIds) {
                        lodash__WEBPACK_IMPORTED_MODULE_10__["each"](groupByTargetGroupIds, function (groupByTargetGroups) {
                            var keyTargetGroup = groupByTargetGroups[0];
                            listTargetGroupResult.push(new _viewmodel_target_render_result_viewmodel__WEBPACK_IMPORTED_MODULE_11__["TargetGroupResultViewModel"](keyTargetGroup.targetGroupId, keyTargetGroup.targetGroupName, keyTargetGroup.targetGroupColor, keyTargetGroup.targetGroupOrder, groupByTargetGroups));
                        });
                    }
                }
                _this.listData.push(new _viewmodel_target_render_result_viewmodel__WEBPACK_IMPORTED_MODULE_11__["TargetTableResultViewModel"](keyTargetTable.targetTableId, keyTargetTable.targetTableName, keyTargetTable.targetTableOrder, listTargetGroupResult));
            });
            this.generalTotalComplete();
        }
        else {
            this.listData = [];
        }
    };
    TargetsComponent.prototype.generalTotalComplete = function () {
        var listTargetParent = lodash__WEBPACK_IMPORTED_MODULE_10__["filter"](this.listResult, function (targetTree) {
            return !targetTree.parentId;
        });
        var totalWeight = lodash__WEBPACK_IMPORTED_MODULE_10__["sumBy"](listTargetParent, function (targetTree) {
            return targetTree.data.weight;
        });
        var totalPercentComplete = lodash__WEBPACK_IMPORTED_MODULE_10__["sumBy"](listTargetParent, function (targetTree) {
            return targetTree.data.weight * targetTree.data.percentCompleted;
        });
        this.totalPercentComplete = totalWeight > 0 ? Math.round(totalPercentComplete / totalWeight) : 0;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_targets_form_targets_form_component__WEBPACK_IMPORTED_MODULE_3__["TargetsFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _targets_form_targets_form_component__WEBPACK_IMPORTED_MODULE_3__["TargetsFormComponent"])
    ], TargetsComponent.prototype, "targetsFormComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_targets_form_detail_targets_form_detail_component__WEBPACK_IMPORTED_MODULE_7__["TargetsFormDetailComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _targets_form_detail_targets_form_detail_component__WEBPACK_IMPORTED_MODULE_7__["TargetsFormDetailComponent"])
    ], TargetsComponent.prototype, "targetFormDetailComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('ghmDialog'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_ghm_dialog_ghm_dialog_component__WEBPACK_IMPORTED_MODULE_4__["GhmDialogComponent"])
    ], TargetsComponent.prototype, "ghmDialog", void 0);
    TargetsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-target',
            template: __webpack_require__(/*! ./targets.component.html */ "./src/app/modules/task/target/targets/targets.component.html"),
            styles: [__webpack_require__(/*! ./targets.component.scss */ "./src/app/modules/task/target/targets/targets.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_target_service__WEBPACK_IMPORTED_MODULE_5__["TargetService"],
            _target_table_service_target_table_service__WEBPACK_IMPORTED_MODULE_6__["TargetTableService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_16__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_16__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_13__["MatDialog"]])
    ], TargetsComponent);
    return TargetsComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/task/target/targets/viewmodel/target-detail.viewmodel.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/viewmodel/target-detail.viewmodel.ts ***!
  \**********************************************************************************/
/*! exports provided: TargetDetailViewModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetDetailViewModel", function() { return TargetDetailViewModel; });
var TargetDetailViewModel = /** @class */ (function () {
    function TargetDetailViewModel() {
    }
    return TargetDetailViewModel;
}());



/***/ }),

/***/ "./src/app/modules/task/target/targets/viewmodel/target-render-result.viewmodel.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/viewmodel/target-render-result.viewmodel.ts ***!
  \*****************************************************************************************/
/*! exports provided: TargetTableResultViewModel, TargetGroupResultViewModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetTableResultViewModel", function() { return TargetTableResultViewModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetGroupResultViewModel", function() { return TargetGroupResultViewModel; });
var TargetTableResultViewModel = /** @class */ (function () {
    function TargetTableResultViewModel(targetTableId, targetTableName, targetTableOrder, targetGroups) {
        this.targetTableId = targetTableId;
        this.targetTableName = targetTableName;
        this.targetTableOrder = targetTableOrder;
        this.targetGroups = targetGroups;
    }
    return TargetTableResultViewModel;
}());

var TargetGroupResultViewModel = /** @class */ (function () {
    function TargetGroupResultViewModel(targetGroupId, targetGroupName, targetGroupColor, targetGroupOrder, targets) {
        this.targetGroupId = targetGroupId;
        this.targetGroupColor = targetGroupColor;
        this.targetGroupName = targetGroupName;
        this.targetGroupOrder = targetGroupOrder;
        this.targets = targets ? targets : [];
        this.isShowTarget = true;
    }
    return TargetGroupResultViewModel;
}());



/***/ }),

/***/ "./src/app/modules/task/target/targets/viewmodel/target.viewmodel.ts":
/*!***************************************************************************!*\
  !*** ./src/app/modules/task/target/targets/viewmodel/target.viewmodel.ts ***!
  \***************************************************************************/
/*! exports provided: TargetViewModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetViewModel", function() { return TargetViewModel; });
var TargetViewModel = /** @class */ (function () {
    function TargetViewModel(id, targetTableId, targetTableName, targetTableOrder, targetGroupId, targetGroupName, targetGroupColor, targetGroupOrder, parentId, childCount, idPath, name, type, userId, fullName, avatar, officeId, officeName, weight, startDate, endDate, completedDate, percentCompleted, status, totalTask, totalTaskComplete, totalComment, methodCalculateResult) {
        this.id = id;
        this.name = name;
        this.weight = weight;
        this.percentCompleted = percentCompleted;
        this.status = status;
    }
    return TargetViewModel;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-app-suggestion/ghm-app-suggestion.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-app-suggestion/ghm-app-suggestion.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"app-suggest\">\r\n    <div class=\"header\">\r\n        <span>{{header}}</span>\r\n    </div>\r\n    <div class=\"media\">\r\n        <div class=\"media-left\">\r\n            <img ghmImage\r\n                 [src]=\"avatar\"\r\n                 [errorImageUrl]=\"'/assets/images/noavatar.png'\"\r\n                 class=\"avatar-sm\"/>\r\n        </div>\r\n        <div class=\"media-body\">\r\n            <h4 class=\"media-heading\">{{title}}</h4>\r\n            <i>{{description}}</i>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/shareds/components/ghm-app-suggestion/ghm-app-suggestion.component.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-app-suggestion/ghm-app-suggestion.component.scss ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".app-suggest {\n  background: #f5f7f7;\n  border-radius: 0px !important;\n  -moz-border-radius: 0px !important;\n  -webkit-border-radius: 0px !important;\n  -ms-border-radius: 0px !important;\n  -o-border-radius: 0px !important;\n  padding: 15px; }\n  .app-suggest .header {\n    color: #27ae60;\n    font-size: 15px;\n    margin-bottom: 10px; }\n  .app-suggest .media {\n    margin-top: 0px; }\n  .app-suggest .media .media-left {\n      padding-right: 5px; }\n  .app-suggest .media h4 {\n      font-size: 15px;\n      font-weight: 500; }\n  .app-suggest .media .media-heading {\n      margin-top: 0px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL2dobS1hcHAtc3VnZ2VzdGlvbi9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXBwXFxzaGFyZWRzXFxjb21wb25lbnRzXFxnaG0tYXBwLXN1Z2dlc3Rpb25cXGdobS1hcHAtc3VnZ2VzdGlvbi5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL2dobS1hcHAtc3VnZ2VzdGlvbi9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXNzZXRzXFxzdHlsZXNcXF9jb25maWcuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNJLG1CQ2lDZ0I7RURoQ2hCLDZCQUE2QjtFQUM3QixrQ0FBa0M7RUFDbEMscUNBQXFDO0VBQ3JDLGlDQUFpQztFQUNqQyxnQ0FBZ0M7RUFDaEMsYUFBYSxFQUFBO0VBUGpCO0lBVVEsY0N1Qlk7SUR0QlosZUFBZTtJQUNmLG1CQUFtQixFQUFBO0VBWjNCO0lBZ0JRLGVBQWUsRUFBQTtFQWhCdkI7TUFrQlksa0JBQWtCLEVBQUE7RUFsQjlCO01BcUJZLGVBQWU7TUFDZixnQkFBZ0IsRUFBQTtFQXRCNUI7TUEwQlksMEJBQTBCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWRzL2NvbXBvbmVudHMvZ2htLWFwcC1zdWdnZXN0aW9uL2dobS1hcHAtc3VnZ2VzdGlvbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi8uLi8uLi9hc3NldHMvc3R5bGVzL2NvbmZpZ1wiO1xyXG5cclxuLmFwcC1zdWdnZXN0IHtcclxuICAgIGJhY2tncm91bmQ6ICRsaWdodC1ibHVlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAtbW96LWJvcmRlci1yYWRpdXM6IDBweCAhaW1wb3J0YW50O1xyXG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAwcHggIWltcG9ydGFudDtcclxuICAgIC1tcy1ib3JkZXItcmFkaXVzOiAwcHggIWltcG9ydGFudDtcclxuICAgIC1vLWJvcmRlci1yYWRpdXM6IDBweCAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZzogMTVweDtcclxuXHJcbiAgICAuaGVhZGVyIHtcclxuICAgICAgICBjb2xvcjogJGxpZ2h0R3JlZW47XHJcbiAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLm1lZGlhIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgICAgICAgLm1lZGlhLWxlZnQge1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGg0IHtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLm1lZGlhLWhlYWRpbmcge1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAwcHggIWltcG9ydGFudDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIiwiJGRlZmF1bHQtY29sb3I6ICMyMjI7XHJcbiRmb250LWZhbWlseTogXCJBcmlhbFwiLCB0YWhvbWEsIEhlbHZldGljYSBOZXVlO1xyXG4kY29sb3ItYmx1ZTogIzM1OThkYztcclxuJG1haW4tY29sb3I6ICMwMDc0NTU7XHJcbiRib3JkZXJDb2xvcjogI2RkZDtcclxuJHNlY29uZC1jb2xvcjogI2IwMWExZjtcclxuJHRhYmxlLWJhY2tncm91bmQtY29sb3I6ICMwMDk2ODg7XHJcbiRibHVlOiAjMDA3YmZmO1xyXG4kZGFyay1ibHVlOiAjMDA3MkJDO1xyXG4kYnJpZ2h0LWJsdWU6ICNkZmYwZmQ7XHJcbiRpbmRpZ286ICM2NjEwZjI7XHJcbiRwdXJwbGU6ICM2ZjQyYzE7XHJcbiRwaW5rOiAjZTgzZThjO1xyXG4kcmVkOiAjZGMzNTQ1O1xyXG4kb3JhbmdlOiAjZmQ3ZTE0O1xyXG4keWVsbG93OiAjZmZjMTA3O1xyXG4kZ3JlZW46ICMyOGE3NDU7XHJcbiR0ZWFsOiAjMjBjOTk3O1xyXG4kY3lhbjogIzE3YTJiODtcclxuJHdoaXRlOiAjZmZmO1xyXG4kZ3JheTogIzg2OGU5NjtcclxuJGdyYXktZGFyazogIzM0M2E0MDtcclxuJHByaW1hcnk6ICMwMDdiZmY7XHJcbiRzZWNvbmRhcnk6ICM2Yzc1N2Q7XHJcbiRzdWNjZXNzOiAjMjhhNzQ1O1xyXG4kaW5mbzogIzE3YTJiODtcclxuJHdhcm5pbmc6ICNmZmMxMDc7XHJcbiRkYW5nZXI6ICNkYzM1NDU7XHJcbiRsaWdodDogI2Y4ZjlmYTtcclxuJGRhcms6ICMzNDNhNDA7XHJcbiRsYWJlbC1jb2xvcjogIzY2NjtcclxuJGJhY2tncm91bmQtY29sb3I6ICNFQ0YwRjE7XHJcbiRib3JkZXJBY3RpdmVDb2xvcjogIzgwYmRmZjtcclxuJGJvcmRlclJhZGl1czogMDtcclxuJGRhcmtCbHVlOiAjNDVBMkQyO1xyXG4kbGlnaHRHcmVlbjogIzI3YWU2MDtcclxuJGxpZ2h0LWJsdWU6ICNmNWY3Zjc7XHJcbiRicmlnaHRHcmF5OiAjNzU3NTc1O1xyXG4kbWF4LXdpZHRoLW1vYmlsZTogNzY4cHg7XHJcbiRtYXgtd2lkdGgtdGFibGV0OiA5OTJweDtcclxuJG1heC13aWR0aC1kZXNrdG9wOiAxMjgwcHg7XHJcblxyXG4vLyBCRUdJTjogTWFyZ2luXHJcbkBtaXhpbiBuaC1tZygkcGl4ZWwpIHtcclxuICAgIG1hcmdpbjogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ3QoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tdG9wOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1nYigkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1ib3R0b206ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWdsKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLWxlZnQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWdyKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBNYXJnaW5cclxuXHJcbi8vIEJFR0lOOiBQYWRkaW5nXHJcbkBtaXhpbiBuaC1wZCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmc6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGR0KCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy10b3A6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGRiKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy1ib3R0b206ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGRsKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy1sZWZ0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkcigkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctcmlnaHQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vLyBFTkQ6IFBhZGRpbmdcclxuXHJcbi8vIEJFR0lOOiBXaWR0aFxyXG5AbWl4aW4gbmgtd2lkdGgoJHdpZHRoKSB7XHJcbiAgICB3aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbiAgICBtaW4td2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG4gICAgbWF4LXdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBXaWR0aFxyXG5cclxuLy8gQkVHSU46IEljb24gU2l6ZVxyXG5AbWl4aW4gbmgtc2l6ZS1pY29uKCRzaXplKSB7XHJcbiAgICB3aWR0aDogJHNpemU7XHJcbiAgICBoZWlnaHQ6ICRzaXplO1xyXG4gICAgZm9udC1zaXplOiAkc2l6ZTtcclxufVxyXG5cclxuLy8gRU5EOiBJY29uIFNpemVcclxuIl19 */"

/***/ }),

/***/ "./src/app/shareds/components/ghm-app-suggestion/ghm-app-suggestion.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-app-suggestion/ghm-app-suggestion.component.ts ***!
  \***************************************************************************************/
/*! exports provided: GhmAppSuggestionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmAppSuggestionComponent", function() { return GhmAppSuggestionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var GhmAppSuggestionComponent = /** @class */ (function () {
    function GhmAppSuggestionComponent() {
        this.header = 'Bạn đang xem mục tiêu của';
        this.avatar = '';
        this.title = '';
        this.description = '';
    }
    GhmAppSuggestionComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmAppSuggestionComponent.prototype, "header", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmAppSuggestionComponent.prototype, "avatar", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmAppSuggestionComponent.prototype, "title", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmAppSuggestionComponent.prototype, "description", void 0);
    GhmAppSuggestionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ghm-app-suggestion',
            template: __webpack_require__(/*! ./ghm-app-suggestion.component.html */ "./src/app/shareds/components/ghm-app-suggestion/ghm-app-suggestion.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./ghm-app-suggestion.component.scss */ "./src/app/shareds/components/ghm-app-suggestion/ghm-app-suggestion.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], GhmAppSuggestionComponent);
    return GhmAppSuggestionComponent;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-app-suggestion/ghm-app-suggestion.module.ts":
/*!************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-app-suggestion/ghm-app-suggestion.module.ts ***!
  \************************************************************************************/
/*! exports provided: GhmAppSuggestionModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmAppSuggestionModule", function() { return GhmAppSuggestionModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ghm_app_suggestion_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ghm-app-suggestion.component */ "./src/app/shareds/components/ghm-app-suggestion/ghm-app-suggestion.component.ts");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../core/core.module */ "./src/app/core/core.module.ts");





var GhmAppSuggestionModule = /** @class */ (function () {
    function GhmAppSuggestionModule() {
    }
    GhmAppSuggestionModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_ghm_app_suggestion_component__WEBPACK_IMPORTED_MODULE_3__["GhmAppSuggestionComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _core_core_module__WEBPACK_IMPORTED_MODULE_4__["CoreModule"]
            ],
            exports: [_ghm_app_suggestion_component__WEBPACK_IMPORTED_MODULE_3__["GhmAppSuggestionComponent"]]
        })
    ], GhmAppSuggestionModule);
    return GhmAppSuggestionModule;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-color-picker/ghm-color-picker.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-color-picker/ghm-color-picker.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"girl-container\">\r\n    <div class=\"grid pull-left cm-mgt-5 cm-mgl-5 cursor-pointer\" *ngFor=\"let color of listColorLabel\"\r\n         (click)=\"selectedItem(color)\" [class.selected]=\"color.id === colorId\"\r\n         [ngStyle]=\"{'background-color': color.id}\">\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/shareds/components/ghm-color-picker/ghm-color-picker.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-color-picker/ghm-color-picker.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".girl-container {\n  display: flex;\n  flex-wrap: wrap;\n  justify-content: center; }\n  .girl-container .grid {\n    height: 60px;\n    border-radius: 5px !important;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    flex-basis: 23%; }\n  .girl-container .grid.selected {\n      border: 4px solid #0072bc; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL2dobS1jb2xvci1waWNrZXIvRDpcXFByb2plY3RcXEdobUFwcGxpY2F0aW9uXFxjbGllbnRzXFxnaG1hcHBsaWNhdGlvbmNsaWVudC9zcmNcXGFwcFxcc2hhcmVkc1xcY29tcG9uZW50c1xcZ2htLWNvbG9yLXBpY2tlclxcZ2htLWNvbG9yLXBpY2tlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQWE7RUFDYixlQUFlO0VBQ2YsdUJBQXVCLEVBQUE7RUFIM0I7SUFLUSxZQUFZO0lBQ1osNkJBQTZCO0lBQzdCLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsdUJBQXVCO0lBQ3ZCLGVBQWUsRUFBQTtFQVZ2QjtNQVlZLHlCQUF5QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL2dobS1jb2xvci1waWNrZXIvZ2htLWNvbG9yLXBpY2tlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5naXJsLWNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC13cmFwOiB3cmFwO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAuZ3JpZCB7XHJcbiAgICAgICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBmbGV4LWJhc2lzOiAyMyU7XHJcbiAgICAgICAgJi5zZWxlY3RlZCB7XHJcbiAgICAgICAgICAgIGJvcmRlcjogNHB4IHNvbGlkICMwMDcyYmM7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/shareds/components/ghm-color-picker/ghm-color-picker.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-color-picker/ghm-color-picker.component.ts ***!
  \***********************************************************************************/
/*! exports provided: GhmColorPickerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmColorPickerComponent", function() { return GhmColorPickerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);



var GhmColorPickerComponent = /** @class */ (function () {
    function GhmColorPickerComponent() {
        this.selectColor = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.listColorLabel = [
            { id: '#fad390' },
            { id: '#f8c291' },
            { id: '#6a89cc' },
            { id: '#82ccdd' },
            { id: '#b8e994' },
            { id: '#f6b93b' },
            { id: '#e55039' },
            { id: '#4a69bd' },
            { id: '#60a3bc' },
            { id: '#78e08f' },
            { id: '#fa983a' },
            { id: '#eb2f06' },
            { id: '#1e3799' },
            { id: '#3c6382' },
            { id: '#3498db' },
            { id: '#e58e26' },
            { id: '#b71540' },
            { id: '#0c2461' },
            { id: '#0a3d62' },
            { id: '#079992' }
        ];
    }
    GhmColorPickerComponent.prototype.ngOnInit = function () {
    };
    GhmColorPickerComponent.prototype.selectedItem = function (item) {
        var _this = this;
        lodash__WEBPACK_IMPORTED_MODULE_2__["each"](this.listColorLabel, function (color) {
            color.selected = false;
            if (color.id === item.id) {
                _this.colorId = color.id;
                _this.selectColor.emit(_this.colorId);
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmColorPickerComponent.prototype, "colorId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GhmColorPickerComponent.prototype, "selectColor", void 0);
    GhmColorPickerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ghm-color-picker',
            template: __webpack_require__(/*! ./ghm-color-picker.component.html */ "./src/app/shareds/components/ghm-color-picker/ghm-color-picker.component.html"),
            styles: [__webpack_require__(/*! ./ghm-color-picker.component.scss */ "./src/app/shareds/components/ghm-color-picker/ghm-color-picker.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], GhmColorPickerComponent);
    return GhmColorPickerComponent;
}());



/***/ }),

/***/ "./src/app/shareds/components/ghm-color-picker/ghm-color-picker.module.ts":
/*!********************************************************************************!*\
  !*** ./src/app/shareds/components/ghm-color-picker/ghm-color-picker.module.ts ***!
  \********************************************************************************/
/*! exports provided: GhmColorPickerModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GhmColorPickerModule", function() { return GhmColorPickerModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ghm_color_picker_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ghm-color-picker.component */ "./src/app/shareds/components/ghm-color-picker/ghm-color-picker.component.ts");




var GhmColorPickerModule = /** @class */ (function () {
    function GhmColorPickerModule() {
    }
    GhmColorPickerModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
            ],
            declarations: [_ghm_color_picker_component__WEBPACK_IMPORTED_MODULE_3__["GhmColorPickerComponent"]],
            exports: [_ghm_color_picker_component__WEBPACK_IMPORTED_MODULE_3__["GhmColorPickerComponent"]]
        })
    ], GhmColorPickerModule);
    return GhmColorPickerModule;
}());



/***/ }),

/***/ "./src/app/shareds/constants/object-type.const.ts":
/*!********************************************************!*\
  !*** ./src/app/shareds/constants/object-type.const.ts ***!
  \********************************************************/
/*! exports provided: ObjectType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ObjectType", function() { return ObjectType; });
var ObjectType = {
    target: 0,
    task: 1,
};


/***/ }),

/***/ "./src/app/shareds/constants/pattern.const.ts":
/*!****************************************************!*\
  !*** ./src/app/shareds/constants/pattern.const.ts ***!
  \****************************************************/
/*! exports provided: Pattern */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Pattern", function() { return Pattern; });
var Pattern = {
    phoneNumber: '^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$',
    email: '^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$',
    whiteSpace: '.*\\S.*'
};


/***/ }),

/***/ "./src/app/shareds/pipe/format-number/format-number.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/shareds/pipe/format-number/format-number.module.ts ***!
  \********************************************************************/
/*! exports provided: FormatNumberModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormatNumberModule", function() { return FormatNumberModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _format_number_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./format-number.pipe */ "./src/app/shareds/pipe/format-number/format-number.pipe.ts");



var FormatNumberModule = /** @class */ (function () {
    function FormatNumberModule() {
    }
    FormatNumberModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [],
            exports: [_format_number_pipe__WEBPACK_IMPORTED_MODULE_2__["FormatNumberPipe"]],
            declarations: [_format_number_pipe__WEBPACK_IMPORTED_MODULE_2__["FormatNumberPipe"]],
            providers: [],
        })
    ], FormatNumberModule);
    return FormatNumberModule;
}());



/***/ }),

/***/ "./src/app/shareds/pipe/format-number/format-number.pipe.ts":
/*!******************************************************************!*\
  !*** ./src/app/shareds/pipe/format-number/format-number.pipe.ts ***!
  \******************************************************************/
/*! exports provided: FormatNumberPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormatNumberPipe", function() { return FormatNumberPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FormatNumberPipe = /** @class */ (function () {
    function FormatNumberPipe() {
    }
    FormatNumberPipe.prototype.transform = function (value, exponent) {
        return this.formatMoney(value, exponent, ',', '.');
    };
    FormatNumberPipe.prototype.formatMoney = function (value, c, d, t) {
        var n = value;
        c = isNaN(c = Math.abs(c)) ? 0 : c;
        d = d === undefined ? '.' : d;
        t = t === undefined ? ',' : t;
        var s = n < 0 ? '-' : '';
        var i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c)));
        this.j = (this.j = i.length) > 3 ? this.j % 3 : 0;
        return s + (this.j ? i.substr(0, this.j) + t : '') + i.substr(this.j).replace(/(\d{3})(?=\d)/g, '$1' + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
    };
    FormatNumberPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({ name: 'formatNumber' })
    ], FormatNumberPipe);
    return FormatNumberPipe;
}());



/***/ }),

/***/ "./src/app/validators/number.validator.ts":
/*!************************************************!*\
  !*** ./src/app/validators/number.validator.ts ***!
  \************************************************/
/*! exports provided: NumberValidator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NumberValidator", function() { return NumberValidator; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NumberValidator = /** @class */ (function () {
    function NumberValidator() {
    }
    NumberValidator.prototype.isValid = function (c) {
        if (c && c.value && c.value != null) {
            if (isNaN(parseFloat(c.value)) || !isFinite(c.value)) {
                return { isValid: false };
            }
        }
        return null;
    };
    NumberValidator.prototype.greaterThan = function (value) {
        return function (c) {
            if (value !== undefined && c.value) {
                if (c.value <= value) {
                    return { greaterThan: false };
                }
            }
            return null;
        };
    };
    NumberValidator.prototype.lessThan = function (value) {
        return function (c) {
            if (value && c.value) {
                if (c.value >= value) {
                    return { lessThan: false };
                }
            }
            return null;
        };
    };
    NumberValidator.prototype.range = function (value) {
        return function (c) {
            if (value && c.value) {
                if (c.value < value.fromValue || c.value > value.toValue) {
                    return { invalidRange: false };
                }
            }
            return null;
        };
    };
    NumberValidator = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NumberValidator);
    return NumberValidator;
}());



/***/ })

}]);
//# sourceMappingURL=modules-task-target-target-module.js.map