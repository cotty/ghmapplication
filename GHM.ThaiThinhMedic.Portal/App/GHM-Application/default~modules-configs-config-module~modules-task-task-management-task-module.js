(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~modules-configs-config-module~modules-task-task-management-task-module"],{

/***/ "./src/app/modules/configs/role/role.service.ts":
/*!******************************************************!*\
  !*** ./src/app/modules/configs/role/role.service.ts ***!
  \******************************************************/
/*! exports provided: RoleService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleService", function() { return RoleService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../shareds/constants/permission.const */ "./src/app/shareds/constants/permission.const.ts");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");









var RoleService = /** @class */ (function () {
    function RoleService(appConfig, spinnerService, toastr, http) {
        this.appConfig = appConfig;
        this.spinnerService = spinnerService;
        this.toastr = toastr;
        this.http = http;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].apiGatewayUrl + "api/v1/core/roles";
    }
    RoleService.prototype.resolve = function (route, state) {
        var queryParams = route.params;
        return this.search(queryParams.keyword, queryParams.page, queryParams.pageSize);
    };
    RoleService.prototype.search = function (keyword, page, pageSize) {
        return this.http.get(this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString())
        });
    };
    RoleService.prototype.insert = function (role) {
        var _this = this;
        return this.http.post(this.url, {
            name: role.name,
            description: role.description,
            type: role.type,
            concurrencyStamp: role.concurrencyStamp,
            userIds: role.users.map(function (user) {
                return user.id;
            }),
            rolePagePermissions: role.rolesPagesViewModels
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    RoleService.prototype.update = function (id, role) {
        var _this = this;
        return this.http.post(this.url + "/" + id, {
            name: role.name,
            description: role.description,
            type: role.type,
            concurrencyStamp: role.concurrencyStamp,
            userIds: role.users.map(function (user) {
                return user.id;
            }),
            rolePagePermissions: role.rolesPagesViewModels
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    RoleService.prototype.updatePermissions = function (roleId, pageId, permissions) {
        var _this = this;
        this.spinnerService.show();
        return this.http.post(this.url + "/pages/" + roleId + "/" + pageId + "/" + permissions, {})
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    RoleService.prototype.deletePermission = function (roleId, pageId) {
        var _this = this;
        return this.http.delete(this.url + "/pages/" + roleId + "/" + pageId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    RoleService.prototype.delete = function (id) {
        var _this = this;
        return this.http.delete(this.url + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    RoleService.prototype.addNewRolePage = function (roleId, rolePagePermission) {
        var _this = this;
        return this.http.post(this.url + "/" + roleId + "/pages", rolePagePermission)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    RoleService.prototype.updateUsers = function (id, users) {
        var _this = this;
        this.spinnerService.show();
        return this.http
            .post(this.url + "/" + id + "/users", users.map(function (user) {
            return user.id;
        }))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    RoleService.prototype.getRolesPages = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/" + id + "/pages")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            var data = result.items;
            if (data) {
                var rolePages_1 = [];
                data.forEach(function (rolePageSearch) {
                    var rolePage = {
                        pageId: rolePageSearch.pageId,
                        pageName: rolePageSearch.pageName,
                        full: _this.checkHasFullPermission(rolePageSearch.permissions),
                        view: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].view, rolePageSearch.permissions),
                        add: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].add, rolePageSearch.permissions),
                        edit: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].edit, rolePageSearch.permissions),
                        delete: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].delete, rolePageSearch.permissions),
                        export: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].export, rolePageSearch.permissions),
                        print: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].print, rolePageSearch.permissions),
                        approve: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].approve, rolePageSearch.permissions),
                        report: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].report, rolePageSearch.permissions),
                    };
                    rolePages_1.push(rolePage);
                });
                return rolePages_1;
            }
            return [];
        }));
    };
    RoleService.prototype.getAllPages = function () {
        return this.http.get(this.url + "/pages")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            var rolesPages = [];
            if (result && result.items) {
                result.items.forEach(function (item) {
                    rolesPages.push({
                        pageId: item.pageId,
                        pageName: item.pageName,
                        view: false,
                        add: false,
                        edit: false,
                        delete: false,
                        export: false,
                        print: false,
                        approve: false,
                        report: false
                    });
                });
            }
            return rolesPages;
        }));
    };
    RoleService.prototype.getRoleDetail = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            var data = result.data;
            var roleDetail = {
                id: data.id,
                name: data.name,
                concurrencyStamp: data.concurrencyStamp,
                description: data.description,
                rolePages: [],
                users: data.users.map(function (user) {
                    return {
                        id: user.userId,
                        fullName: user.fullName,
                        avatar: user.avatar,
                        description: user.userName
                    };
                })
            };
            if (data.rolesPagesViewModels && data.rolesPagesViewModels.length > 0) {
                data.rolesPagesViewModels.forEach(function (rolePage) {
                    var idPathArray = rolePage.idPath.split('.');
                    if (idPathArray.length > 1) {
                        for (var i = 1; i < idPathArray.length; i++) {
                            rolePage.pageName = '&nbsp;&nbsp;&nbsp;&nbsp;' + rolePage.pageName;
                        }
                    }
                    roleDetail.rolePages.push({
                        pageId: rolePage.pageId,
                        pageName: rolePage.pageName,
                        full: _this.checkHasFullPermission(rolePage.permissions),
                        view: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].view, rolePage.permissions),
                        add: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].add, rolePage.permissions),
                        edit: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].edit, rolePage.permissions),
                        delete: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].delete, rolePage.permissions),
                        export: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].export, rolePage.permissions),
                        print: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].print, rolePage.permissions),
                        approve: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].approve, rolePage.permissions),
                        report: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].report, rolePage.permissions),
                    });
                });
            }
            return roleDetail;
        }));
    };
    RoleService.prototype.getPages = function (id) {
        var _this = this;
        return this.http.get(this.url + "/" + id + "/pages")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            var rolesPages = [];
            if (result && result.items) {
                result.items.forEach(function (item) {
                    console.log(item);
                    rolesPages.push({
                        pageId: item.pageId,
                        pageName: item.pageName,
                        full: _this.checkHasFullPermission(item.permissions),
                        view: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].view, item.permissions),
                        add: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].add, item.permissions),
                        edit: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].edit, item.permissions),
                        delete: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].delete, item.permissions),
                        export: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].export, item.permissions),
                        print: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].print, item.permissions),
                        approve: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].approve, item.permissions),
                        report: _this.checkPermission(_shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].report, item.permissions),
                    });
                });
            }
            return rolesPages;
        }));
    };
    RoleService.prototype.removeUser = function (roleId, userId) {
        var _this = this;
        this.spinnerService.show();
        return this.http.delete(this.url + "/" + roleId + "/users/" + userId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    RoleService.prototype.checkHasFullPermission = function (permissions) {
        return _shareds_constants_permission_const__WEBPACK_IMPORTED_MODULE_6__["Permission"].full === permissions;
    };
    RoleService.prototype.checkPermission = function (permission, permissions) {
        return (permissions & permission) === permission;
    };
    RoleService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_3__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_7__["SpinnerService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], RoleService);
    return RoleService;
}());



/***/ })

}]);
//# sourceMappingURL=default~modules-configs-config-module~modules-task-task-management-task-module.js.map