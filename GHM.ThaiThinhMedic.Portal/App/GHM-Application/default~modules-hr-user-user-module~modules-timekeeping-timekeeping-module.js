(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~modules-hr-user-user-module~modules-timekeeping-timekeeping-module"],{

/***/ "./src/app/modules/hr/user/services/user.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/modules/hr/user/services/user.service.ts ***!
  \**********************************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../environments/environment */ "./src/environments/environment.ts");









var UserService = /** @class */ (function () {
    function UserService(appConfig, http, spinnerService, toastr) {
        this.appConfig = appConfig;
        this.http = http;
        this.spinnerService = spinnerService;
        this.toastr = toastr;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].apiGatewayUrl + "api/v1/hr/users";
    }
    UserService.prototype.resolve = function (route, state) {
        var queryParams = route.queryParams;
        var keyword = queryParams.keyword;
        var status = queryParams.status;
        var officeIdPath = queryParams.officeIdPath;
        var positionId = queryParams.position;
        var gender = queryParams.gender;
        var monthBirthDay = queryParams.month;
        var yearBirthday = queryParams.year;
        var academicRank = queryParams.academicRank;
        var page = queryParams.page;
        var pageSize = queryParams.pageSize;
        return this.search(keyword, positionId, status, officeIdPath, gender, monthBirthDay, yearBirthday, academicRank, page, pageSize);
    };
    UserService.prototype.search = function (keyword, positionId, status, officeIdPath, gender, monthBirthDay, yearBirthday, academicRank, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = this.appConfig.PAGE_SIZE; }
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('keyword', keyword ? keyword : '')
            .set('status', status ? status : '')
            .set('officeIdPath', officeIdPath ? officeIdPath : '')
            .set('positionId', positionId ? positionId : '')
            .set('gender', gender != null && gender !== undefined ? gender.toString() : '')
            .set('monthBirthDay', monthBirthDay ? monthBirthDay.toString() : '')
            .set('yearBirthday', yearBirthday ? yearBirthday.toString() : '')
            .set('academicRank', academicRank ? academicRank.toString() : '')
            .set('page', page ? page.toString() : '1')
            .set('pageSize', pageSize ? pageSize.toString() : this.appConfig.PAGE_SIZE.toString());
        return this.http.get("" + this.url, {
            params: params
        });
    };
    UserService.prototype.searchForSelect = function (keyword, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 20; }
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('keyword', keyword)
            .set('page', page.toString())
            .set('pageSize', pageSize.toString());
        return this.http.get(this.url + "/suggestions", {
            params: params
        });
    };
    UserService.prototype.getDetail = function (id) {
        var _this = this;
        this.spinnerService.show();
        return this.http.get(this.url + "/" + id, {})
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(function () { return _this.spinnerService.hide(); }));
    };
    UserService.prototype.insert = function (user) {
        var _this = this;
        return this.http.post("" + this.url, {
            fullName: user.fullName,
            userName: user.userName,
            avatar: user.avatar,
            birthday: user.birthday ? moment__WEBPACK_IMPORTED_MODULE_6__(user.birthday, 'DD/MM/YYYY').format('MM/DD/YYYY') : '',
            idCardNumber: user.idCardNumber,
            idCardDateOfIssue: user.idCardDateOfIssue ? moment__WEBPACK_IMPORTED_MODULE_6__(user.idCardDateOfIssue, 'DD/MM/YYYY').format('MM/DD/YYYY') : '',
            gender: user.gender,
            ethnic: user.ethnic,
            denomination: user.denomination,
            tin: user.tin,
            joinedDate: user.joinedDate ? moment__WEBPACK_IMPORTED_MODULE_6__(user.joinedDate, 'DD/MM/YYYY').format('MM/DD/YYYY') : '',
            bankingNumber: user.bankingNumber,
            nationalId: user.nationalId,
            provinceId: user.provinceId,
            districtId: user.districtId,
            marriedStatus: user.marriedStatus,
            officeId: user.officeId,
            titleId: user.titleId,
            positionId: user.positionId,
            userType: user.userType,
            passportId: user.passportId,
            passportDateOfIssue: user.passportDateOfIssue ? moment__WEBPACK_IMPORTED_MODULE_6__(user.passportDateOfIssue, 'DD/MM/YYYY').format('MM/DD/YYYY') : '',
            enrollNumber: user.enrollNumber,
            cardNumber: user.cardNumber,
            ext: user.ext,
            academicRank: user.academicRank,
            userTranslations: user.modelTranslations,
            userContacts: user.userContacts,
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    UserService.prototype.update = function (id, user) {
        var _this = this;
        return this.http.post(this.url + "/" + id, {
            fullName: user.fullName,
            userName: user.userName,
            avatar: user.avatar,
            birthday: user.birthday ? moment__WEBPACK_IMPORTED_MODULE_6__(user.birthday, 'DD/MM/YYYY').format('MM/DD/YYYY') : '',
            idCardNumber: user.idCardNumber,
            idCardDateOfIssue: user.idCardDateOfIssue ? moment__WEBPACK_IMPORTED_MODULE_6__(user.idCardDateOfIssue, 'DD/MM/YYYY').format('MM/DD/YYYY') : '',
            gender: user.gender,
            ethnic: user.ethnic,
            denomination: user.denomination,
            tin: user.tin,
            joinedDate: user.joinedDate ? moment__WEBPACK_IMPORTED_MODULE_6__(user.joinedDate, 'DD/MM/YYYY').format('MM/DD/YYYY') : '',
            bankingNumber: user.bankingNumber,
            nationalId: user.nationalId,
            provinceId: user.provinceId,
            districtId: user.districtId,
            marriedStatus: user.marriedStatus,
            officeId: user.officeId,
            titleId: user.titleId,
            positionId: user.positionId,
            userType: user.userType,
            passportId: user.passportId,
            passportDateOfIssue: user.passportDateOfIssue ? moment__WEBPACK_IMPORTED_MODULE_6__(user.passportDateOfIssue, 'DD/MM/YYYY').format('MM/DD/YYYY') : '',
            enrollNumber: user.enrollNumber,
            cardNumber: user.cardNumber,
            ext: user.ext,
            academicRank: user.academicRank,
            concurrencyStamp: user.concurrencyStamp,
            userTranslations: user.modelTranslations,
            userContacts: user.userContacts,
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    UserService.prototype.updateAvatar = function (userId, avatar) {
        var _this = this;
        return this.http.post(this.url + "/update-avatar", '', {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('userId', userId)
                .set('avatar', avatar)
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    UserService.prototype.delete = function (id) {
        var _this = this;
        return this.http.delete(this.url + "/" + id, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('id', id)
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    UserService.prototype.export = function (officeIdPath) {
        var url = this.url + "/export-user?officeIdPath=" + (officeIdPath == null ? '' : officeIdPath);
        return this.http.get(url, { responseType: 'blob' })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) {
            return new Blob([response]);
        }));
    };
    UserService.prototype.exportLabor = function (officeIdPath, fileName) {
        var url = this.url + "/export-labor-contract?officeIdPath=" + (officeIdPath == null ? '' : officeIdPath);
        return this.http.get(url, { responseType: 'blob' })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) {
            return new Blob([response]);
        }));
    };
    UserService.prototype.exportRecord = function (officeIdPath, fileName) {
        var url = this.url + "/export-user-record?officeIdPath=" + (officeIdPath == null ? '' : officeIdPath);
        return this.http.get(url, { responseType: 'blob' })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) {
            return new Blob([response]);
        }));
    };
    UserService.prototype.changePassword = function (oldPassword, newPassword) {
        return this.http.post(this.url + "/change-password", '', {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('oldPassword', oldPassword)
                .set('newPassword', newPassword)
        });
    };
    UserService.prototype.getUserId = function () {
        return this.http.get(this.url + "/generate-user-id");
    };
    UserService.prototype.getUserByOfficeIdAndChild = function (officeId, isChild) {
        return this.http.get(this.url + "/search-user-by-officeId-and-isChild", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('officeId', officeId.toString())
                .set('isChild', isChild.toString())
        });
    };
    UserService.prototype.getUserByUserId = function (userId) {
        return this.http.get(this.url + "/search-user-by-userId", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]().set('userId', userId)
        });
    };
    UserService.prototype.searchForSuggestion = function (keyword, officeIdPath, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 20; }
        return this.http.get(this.url + "/search-for-suggestion", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : '20')
        });
    };
    UserService.prototype.insertUserContact = function (userContact) {
        var _this = this;
        return this.http.post(this.url + "/insert-user-contact", userContact).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    UserService.prototype.updateUserContact = function (id, userContact) {
        var _this = this;
        return this.http.post(this.url + "/update-user-contact", userContact, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('id', id ? id : '')
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    UserService.prototype.deleteUserContact = function (id) {
        var _this = this;
        return this.http.post(this.url + "/delete-user-contact", '', {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('id', id)
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) {
            _this.toastr.success(result.message);
            return result;
        }));
    };
    UserService.prototype.getUserInfo = function (id) {
        return this.http.get(this.url + "/short-user-info/" + id, {});
    };
    UserService.prototype.suggestions = function (keyword, page, pageSize) {
        var url = this.url + "/suggestions";
        return this.http
            .get(url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
                .set('keyword', keyword ? keyword : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : '10')
        });
    };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_4__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_7__["SpinnerService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/shareds/pipe/datetime-format/datetime-format.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/shareds/pipe/datetime-format/datetime-format.module.ts ***!
  \************************************************************************/
/*! exports provided: DatetimeFormatModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatetimeFormatModule", function() { return DatetimeFormatModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _datetime_format_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./datetime-format.pipe */ "./src/app/shareds/pipe/datetime-format/datetime-format.pipe.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");




var DatetimeFormatModule = /** @class */ (function () {
    function DatetimeFormatModule() {
    }
    DatetimeFormatModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"]],
            exports: [_datetime_format_pipe__WEBPACK_IMPORTED_MODULE_2__["DateTimeFormatPipe"]],
            declarations: [_datetime_format_pipe__WEBPACK_IMPORTED_MODULE_2__["DateTimeFormatPipe"]],
            providers: [],
        })
    ], DatetimeFormatModule);
    return DatetimeFormatModule;
}());



/***/ }),

/***/ "./src/app/shareds/pipe/datetime-format/datetime-format.pipe.ts":
/*!**********************************************************************!*\
  !*** ./src/app/shareds/pipe/datetime-format/datetime-format.pipe.ts ***!
  \**********************************************************************/
/*! exports provided: DateTimeFormatPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DateTimeFormatPipe", function() { return DateTimeFormatPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);



var DateTimeFormatPipe = /** @class */ (function () {
    function DateTimeFormatPipe() {
        this._inputDateTimeAllowedFormat = [
            'DD/MM/YYYY',
            'DD/MM/YYYY HH:mm',
            'DD/MM/YYYY HH:mm:ss',
            'DD/MM/YYYY HH:mm Z',
            'DD-MM-YYYY',
            'DD-MM-YYYY HH:mm',
            'DD-MM-YYYY HH:mm:ss',
            'DD-MM-YYYY HH:mm Z',
            // --------------------
            'MM/DD/YYYY',
            'MM/DD/YYYY HH:mm',
            'MM/DD/YYYY HH:mm:ss',
            'MM/DD/YYYY HH:mm Z',
            'MM-DD-YYYY',
            'MM-DD-YYYY HH:mm',
            'MM-DD-YYYY HH:mm:ss',
            'MM-DD-YYYY HH:mm Z',
            // --------------------
            'YYYY/MM/DD',
            'YYYY/MM/DD HH:mm',
            'YYYY/MM/DD HH:mm:ss',
            'YYYY/MM/DD HH:mm Z',
            'YYYY-MM-DD',
            'YYYY-MM-DD HH:mm',
            'YYYY-MM-DD HH:mm:ss',
            'YYYY-MM-DD HH:mm Z',
            // --------------------
            'YYYY/DD/MM',
            'YYYY/DD/MM HH:mm',
            'YYYY/DD/MM HH:mm:ss',
            'YYYY/DD/MM HH:mm Z',
            'YYYY-DD-MM',
            'YYYY-DD-MM HH:mm',
            'YYYY-DD-MM HH:mm:ss',
            'YYYY-DD-MM HH:mm Z',
        ];
    }
    DateTimeFormatPipe.prototype.transform = function (value, exponent, isUtc) {
        if (isUtc === void 0) { isUtc = false; }
        return this.formatDate(value, exponent, isUtc);
    };
    DateTimeFormatPipe.prototype.formatDate = function (value, format, isUtc) {
        if (isUtc === void 0) { isUtc = false; }
        if (!moment__WEBPACK_IMPORTED_MODULE_2__(value, this._inputDateTimeAllowedFormat).isValid()) {
            return '';
        }
        return isUtc ? moment__WEBPACK_IMPORTED_MODULE_2__["utc"](value, this._inputDateTimeAllowedFormat).format(format)
            : moment__WEBPACK_IMPORTED_MODULE_2__(value, this._inputDateTimeAllowedFormat).format(format);
        // return value;
    };
    DateTimeFormatPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({ name: 'dateTimeFormat' })
    ], DateTimeFormatPipe);
    return DateTimeFormatPipe;
}());



/***/ }),

/***/ "./src/app/validators/number.validator.ts":
/*!************************************************!*\
  !*** ./src/app/validators/number.validator.ts ***!
  \************************************************/
/*! exports provided: NumberValidator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NumberValidator", function() { return NumberValidator; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NumberValidator = /** @class */ (function () {
    function NumberValidator() {
    }
    NumberValidator.prototype.isValid = function (c) {
        if (c && c.value && c.value != null) {
            if (isNaN(parseFloat(c.value)) || !isFinite(c.value)) {
                return { isValid: false };
            }
        }
        return null;
    };
    NumberValidator.prototype.greaterThan = function (value) {
        return function (c) {
            if (value !== undefined && c.value) {
                if (c.value <= value) {
                    return { greaterThan: false };
                }
            }
            return null;
        };
    };
    NumberValidator.prototype.lessThan = function (value) {
        return function (c) {
            if (value && c.value) {
                if (c.value >= value) {
                    return { lessThan: false };
                }
            }
            return null;
        };
    };
    NumberValidator.prototype.range = function (value) {
        return function (c) {
            if (value && c.value) {
                if (c.value < value.fromValue || c.value > value.toValue) {
                    return { invalidRange: false };
                }
            }
            return null;
        };
    };
    NumberValidator = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NumberValidator);
    return NumberValidator;
}());



/***/ })

}]);
//# sourceMappingURL=default~modules-hr-user-user-module~modules-timekeeping-timekeeping-module.js.map