(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-notifications-notification-module"],{

/***/ "./src/app/modules/notifications/notification-routing.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/modules/notifications/notification-routing.module.ts ***!
  \**********************************************************************/
/*! exports provided: routes, NotificationRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationRoutingModule", function() { return NotificationRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _notification_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./notification.component */ "./src/app/modules/notifications/notification.component.ts");
/* harmony import */ var _notification_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./notification.service */ "./src/app/modules/notifications/notification.service.ts");
/* harmony import */ var _shareds_layouts_layout_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shareds/layouts/layout.component */ "./src/app/shareds/layouts/layout.component.ts");
/* harmony import */ var _shareds_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shareds/services/auth-guard.service */ "./src/app/shareds/services/auth-guard.service.ts");







var routes = [
    {
        path: '',
        component: _shareds_layouts_layout_component__WEBPACK_IMPORTED_MODULE_5__["LayoutComponent"],
        canActivate: [_shareds_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]],
        children: [
            {
                path: '',
                component: _notification_component__WEBPACK_IMPORTED_MODULE_3__["NotificationComponent"],
                resolve: {
                    data: _notification_service__WEBPACK_IMPORTED_MODULE_4__["NotificationService"]
                }
            },
        ]
    }
];
var NotificationRoutingModule = /** @class */ (function () {
    function NotificationRoutingModule() {
    }
    NotificationRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
            providers: [_notification_service__WEBPACK_IMPORTED_MODULE_4__["NotificationService"]]
        })
    ], NotificationRoutingModule);
    return NotificationRoutingModule;
}());



/***/ }),

/***/ "./src/app/modules/notifications/notification.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/modules/notifications/notification.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"notification-container\">\r\n    <h1 class=\"title\">Thông báo của bạn</h1>\r\n    <hr>\r\n    <ul class=\"dropdown-menu-list list-notify-container\" #notifyContentContainer>\r\n        <li class=\"notify-item\" *ngFor=\"let item of listItems$ | async\" [class.info]=\"item.type === 0\"\r\n            [class.warning]=\"item.type === 1\"\r\n            [class.danger]=\"item.type === 2\" [class.success]=\"item.type === 3\" [class.readed]=\"item.isRead\"\r\n            (click)=\"updateRead(item)\">\r\n            <button class=\"notification-close\" (click)=\"closeNotify(item)\">\r\n                <i class=\"fa fa-times\"></i>\r\n            </button>\r\n            <div class=\"notify-left\">\r\n                <a href=\"javascript://\">\r\n                    <img ghmImage=\"\" [src]=\"item.fromUserImage\" [alt]=\"item.title\">\r\n                    <!--<nh-image [baseUrl]=\"baseUrl\" [value]=\"item.fromUserImage\" [alt]=\"item.title\" [width]=\"40\"-->\r\n                              <!--[height]=\"40\"></nh-image>-->\r\n                </a>\r\n            </div>\r\n            <div class=\"notify-body\">\r\n                <p class=\"content\" *ngIf=\"!item.image\" [innerHTML]=\"item.content\"></p>\r\n                <div class=\"notify-item\" *ngIf=\"item.image\">\r\n                    <div class=\"notify-body\">\r\n                        <p class=\"content\" [innerHTML]=\"item.content\"></p>\r\n                    </div>\r\n                    <div class=\"notify-right\">\r\n                        <a href=\"javascript://\">\r\n                            <!--<nh-image [baseUrl]=\"baseUrl\" [value]=\"item.image\" [width]=\"40\" [height]=\"40\"></nh-image>-->\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n                <div class=\"times\">\r\n                    <i class=\"fa fa-clock-o\"></i>{{item.time}}\r\n                </div>\r\n            </div>\r\n        </li>\r\n    </ul>\r\n\r\n    <ghm-paging [totalRows]=\"totalRows\" [currentPage]=\"currentPage\" [pageShow]=\"6\" (pageClick)=\"search($event)\"\r\n                [isDisabled]=\"isSearching\" [pageName]=\"'thông báo'\"></ghm-paging>\r\n</div><!-- END: .notification-container -->\r\n"

/***/ }),

/***/ "./src/app/modules/notifications/notification.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/modules/notifications/notification.component.ts ***!
  \*****************************************************************/
/*! exports provided: NotificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationComponent", function() { return NotificationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _notification_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./notification.service */ "./src/app/modules/notifications/notification.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _configs_app_config__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../configs/app.config */ "./src/app/configs/app.config.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");







var NotificationComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](NotificationComponent, _super);
    function NotificationComponent(appConfig, route, notificationService) {
        var _this = _super.call(this) || this;
        _this.route = route;
        _this.notificationService = notificationService;
        return _this;
        // this.baseUrl = appConfig.baseUrl;
    }
    NotificationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.listItems$ = this.route.data.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            var data = result.data;
            if (data) {
                _this.totalRows = data.totalRows;
                return data.items;
            }
        }));
    };
    NotificationComponent.prototype.updateRead = function (notification) {
        this.notificationService.updateReadStatus(notification.id, true);
    };
    NotificationComponent.prototype.search = function (currentPage) {
        var _this = this;
        this.listItems$ = this.notificationService.search(this.isRead, currentPage)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) {
            _this.totalRows = result.totalRows;
            return result.items;
        }));
    };
    NotificationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-notification',
            template: __webpack_require__(/*! ./notification.component.html */ "./src/app/modules/notifications/notification.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_app_config__WEBPACK_IMPORTED_MODULE_5__["APP_CONFIG"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _notification_service__WEBPACK_IMPORTED_MODULE_3__["NotificationService"]])
    ], NotificationComponent);
    return NotificationComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/notifications/notification.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/modules/notifications/notification.module.ts ***!
  \**************************************************************/
/*! exports provided: NotificationModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationModule", function() { return NotificationModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _notification_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./notification.component */ "./src/app/modules/notifications/notification.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _notification_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./notification-routing.module */ "./src/app/modules/notifications/notification-routing.module.ts");
/* harmony import */ var _shareds_components_ghm_paging_ghm_paging_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shareds/components/ghm-paging/ghm-paging.module */ "./src/app/shareds/components/ghm-paging/ghm-paging.module.ts");






var NotificationModule = /** @class */ (function () {
    function NotificationModule() {
    }
    NotificationModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _notification_routing_module__WEBPACK_IMPORTED_MODULE_4__["NotificationRoutingModule"], _shareds_components_ghm_paging_ghm_paging_module__WEBPACK_IMPORTED_MODULE_5__["GhmPagingModule"]],
            exports: [],
            declarations: [_notification_component__WEBPACK_IMPORTED_MODULE_2__["NotificationComponent"]],
            providers: [],
        })
    ], NotificationModule);
    return NotificationModule;
}());



/***/ }),

/***/ "./src/app/modules/notifications/notification.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/modules/notifications/notification.service.ts ***!
  \***************************************************************/
/*! exports provided: NotificationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationService", function() { return NotificationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);







var NotificationService = /** @class */ (function () {
    function NotificationService(http, toastr) {
        this.http = http;
        this.toastr = toastr;
        this.url = 'notification/';
    }
    NotificationService.prototype.resolve = function (route, state) {
        var queryParams = route.queryParams;
        var isRead = queryParams.isRead;
        var page = queryParams.page;
        var pageSize = queryParams.pageSize;
        return this.search(isRead, page, pageSize);
        // return null;
    };
    NotificationService.prototype.updateReadStatus = function (id, isRead) {
        var _this = this;
        this.http.get(this.url + "update-read", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
                .set('id', id.toString())
                .set('isRead', isRead.toString())
        }).subscribe(function (result) {
            _this.toastr.success(result.message);
        });
    };
    NotificationService.prototype.search = function (isRead, page, pageSize) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 20; }
        return this.http.get(this.url + "search", {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
                .set('isRead', isRead !== undefined && isRead != null ? isRead.toString() : '')
                .set('page', page ? page.toString() : '1')
                .set('pageSize', pageSize ? pageSize.toString() : '')
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            lodash__WEBPACK_IMPORTED_MODULE_5__["each"](result.items, function (item) {
                item.time = moment__WEBPACK_IMPORTED_MODULE_6__(item.time).fromNow();
            });
            return result;
        }));
    };
    NotificationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"]])
    ], NotificationService);
    return NotificationService;
}());



/***/ })

}]);
//# sourceMappingURL=modules-notifications-notification-module.js.map