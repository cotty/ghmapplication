(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~modules-configs-config-module~modules-customer-config-config-module~modules-customer-custome~606d9c5e"],{

/***/ "./src/app/shareds/components/nh-select/nh-select.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/shareds/components/nh-select/nh-select.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<button [ngClass]=\"'btn ' + btnClasses \" [class.disabled]=\"!isEnable\" type=\"button\" (click)=\"buttonClick()\" [disabled]=\"readonly\">\r\n    {{ !label ? title : label }}\r\n    <span class=\"caret\"></span>\r\n</button>\r\n\r\n<ng-template #dropdownTemplate>\r\n    <div class=\"nh-select-menu\">\r\n        <div class=\"search-box\" *ngIf=\"liveSearch\">\r\n            <input [id]=\"inputId\" type=\"text\" class=\"form-control w100pc\"\r\n                   placeholder=\"Enter keyword\"\r\n                   i18n-placeholder=\"@@enterKeyword\"\r\n                   (keydown.enter)=\"$event.preventDefault()\"\r\n                   (keyup)=\"searchKeyUp($event, searchBox.value)\"\r\n                   #searchBox/>\r\n        </div>\r\n        <hr *ngIf=\"liveSearch\"/>\r\n        <ul class=\"wrapper-list-menu\" *ngIf=\"!readonly\">\r\n            <li *ngIf=\"data?.length > 0\" (click)=\"selectItem({id: null, name: title})\">\r\n                {{title}}\r\n            </li>\r\n            <li *ngIf=\"isSearching\" class=\"center\">\r\n                <i class=\"fa fa-spinner fa-pulse\"></i>\r\n            </li>\r\n            <li class=\"nh-select-item\" *ngFor=\"let item of source\"\r\n                [class.selected]=\"item.selected\"\r\n                [class.active]=\"item.active\"\r\n                (click)=\"selectItem(item)\">\r\n                <img src=\"{{item.image}}\" *ngIf=\"item.image\">\r\n                <i *ngIf=\"item.icon\" [ngClass]=\"item.icon\"></i>\r\n                {{item.name }}\r\n                <i class=\"fa fa-check nh-selected-icon color-green\"\r\n                   *ngIf=\"item.selected && multiple\"></i>\r\n            </li>\r\n            <li *ngIf=\"source?.length === 0 && isInsertValue\" class=\"background-none\">\r\n                <button class=\"btn btn-primary btn-block\" type=\"button\" (click)=\"insertValue()\"><i\r\n                    class=\"fa fa-plus\"></i>Thêm mới\r\n                </button>\r\n            </li>\r\n            <li *ngIf=\"source?.length === 0 && !isInsertValue\" class=\"no-data\">Không có dữ liệu</li>\r\n        </ul>\r\n    </div>\r\n</ng-template>\r\n"

/***/ }),

/***/ "./src/app/shareds/components/nh-select/nh-select.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/shareds/components/nh-select/nh-select.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nh-select {\n  text-align: left;\n  display: inline-block; }\n  nh-select.nh-multiple li.nh-select-item {\n    position: relative; }\n  nh-select.nh-multiple li.nh-select-item.selected {\n      background: none; }\n  nh-select.nh-multiple li.nh-select-item .nh-selected-icon {\n      position: absolute;\n      top: 5px;\n      right: 5px;\n      color: white; }\n  nh-select.no-border > button {\n    background: white !important;\n    border: none !important; }\n  nh-select.has-error > button.nh-select-button {\n    border: 1px solid #dc3545 !important; }\n  nh-select button.nh-select-button {\n    max-width: 250px;\n    border-radius: 0 !important;\n    text-overflow: ellipsis;\n    white-space: nowrap;\n    overflow: hidden;\n    margin: 0 !important;\n    min-height: 33px;\n    border: 1px solid #ddd;\n    background: white;\n    padding: 7px 14px; }\n  nh-select button.nh-select-button:focus {\n      border-radius: 0 !important;\n      border: 1px solid #80bdff;\n      box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25) !important; }\n  .nh-select-menu {\n  display: block;\n  max-width: 250px;\n  min-width: 250px;\n  background: white;\n  position: relative;\n  border: 1px solid #ddd;\n  border-radius: 3px !important; }\n  .nh-select-menu:before, .nh-select-menu:after {\n    content: '';\n    width: 0;\n    height: 0;\n    position: absolute; }\n  .nh-select-menu.nh-menu-top {\n    margin-bottom: 10px; }\n  .nh-select-menu.nh-menu-top:before {\n      width: 0;\n      height: 0;\n      bottom: -8px;\n      border-left: 8px solid transparent;\n      border-right: 8px solid transparent;\n      border-top: 8px solid #ddd; }\n  .nh-select-menu.nh-menu-top:after {\n      bottom: -7px;\n      border-left: 7px solid transparent;\n      border-right: 7px solid transparent;\n      border-top: 7px solid white; }\n  .nh-select-menu.nh-menu-bottom {\n    margin-top: 10px; }\n  .nh-select-menu.nh-menu-bottom:before {\n      top: -8px;\n      border-left: 8px solid transparent;\n      border-right: 8px solid transparent;\n      border-bottom: 8px solid #ddd; }\n  .nh-select-menu.nh-menu-bottom:after {\n      top: -7px;\n      border-left: 7px solid transparent;\n      border-right: 7px solid transparent;\n      border-bottom: 7px solid white; }\n  .nh-select-menu.nh-menu-left:before {\n    left: 10px; }\n  .nh-select-menu.nh-menu-left:after {\n    left: 11px; }\n  .nh-select-menu.nh-menu-right:before {\n    right: 10px; }\n  .nh-select-menu.nh-menu-right:after {\n    right: 11px; }\n  .nh-select-menu .search-box {\n    padding: 5px; }\n  .nh-select-menu hr {\n    margin: 0 !important; }\n  .nh-select-menu ul {\n    list-style: none;\n    padding-left: 0;\n    margin-bottom: 0;\n    max-height: 300px;\n    overflow-y: auto;\n    border-top: 1px solid #ddd; }\n  .nh-select-menu ul li {\n      padding: 8px 8px 8px 12px;\n      display: block;\n      width: 100%;\n      border-bottom: none !important;\n      white-space: nowrap;\n      text-overflow: ellipsis;\n      overflow: hidden;\n      -webkit-user-select: none;\n         -moz-user-select: none;\n          -ms-user-select: none;\n              user-select: none;\n      border-bottom: 1px solid #eaeaea !important; }\n  .nh-select-menu ul li:last-child {\n        border-bottom: none !important; }\n  .nh-select-menu ul li:hover, .nh-select-menu ul li.active, .nh-select-menu ul li.selected {\n        cursor: pointer;\n        color: white;\n        background: #007bff; }\n  .nh-select-menu ul li a {\n        -webkit-user-select: none;\n           -moz-user-select: none;\n            -ms-user-select: none;\n                user-select: none;\n        text-decoration: none; }\n  .nh-select-menu ul li a:hover, .nh-select-menu ul li a:active, .nh-select-menu ul li a:visited, .nh-select-menu ul li a:focus {\n          text-decoration: none; }\n  .nh-select-menu ul .background-none {\n      background: none !important; }\n  .nh-select-menu ul li.nh-select-item a:visited, .nh-select-menu ul li.nh-select-item a:active, .nh-select-menu ul li.nh-select-item a:focus {\n      outline: none;\n      text-decoration: none; }\n  .nh-select-menu ul li.nh-select-item.selected {\n      color: white;\n      background: #007bff; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL25oLXNlbGVjdC9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXBwXFxzaGFyZWRzXFxjb21wb25lbnRzXFxuaC1zZWxlY3RcXG5oLXNlbGVjdC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvc2hhcmVkcy9jb21wb25lbnRzL25oLXNlbGVjdC9EOlxcUHJvamVjdFxcR2htQXBwbGljYXRpb25cXGNsaWVudHNcXGdobWFwcGxpY2F0aW9uY2xpZW50L3NyY1xcYXNzZXRzXFxzdHlsZXNcXF9jb25maWcuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFPQTtFQUNJLGdCQUFnQjtFQUNoQixxQkFBcUIsRUFBQTtFQUZ6QjtJQU1ZLGtCQUFrQixFQUFBO0VBTjlCO01BU2dCLGdCQUFnQixFQUFBO0VBVGhDO01BYWdCLGtCQUFrQjtNQUNsQixRQUFRO01BQ1IsVUFBVTtNQUNWLFlBQVksRUFBQTtFQWhCNUI7SUFzQlEsNEJBQTRCO0lBQzVCLHVCQUF1QixFQUFBO0VBdkIvQjtJQTJCUSxvQ0FBb0MsRUFBQTtFQTNCNUM7SUErQlEsZ0JBQWdCO0lBQ2hCLDJCQUF1QztJQUN2Qyx1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixvQkFBb0I7SUFDcEIsZ0JBQWdCO0lBQ2hCLHNCQ3pDVTtJRDBDVixpQkFBaUI7SUFDakIsaUJBQWlCLEVBQUE7RUF4Q3pCO01BMkNZLDJCQUF1QztNQUN2Qyx5QkNuQmU7TURvQmYsMkRBQTJELEVBQUE7RUFLdkU7RUFDSSxjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUVoQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLHNCQzVEYztFRDZEZCw2QkFBNkIsRUFBQTtFQVJqQztJQVdRLFdBQVc7SUFDWCxRQUFRO0lBQ1IsU0FBUztJQUNULGtCQUFrQixFQUFBO0VBZDFCO0lBaUJRLG1CQUFtQixFQUFBO0VBakIzQjtNQW9CWSxRQUFRO01BQ1IsU0FBUztNQUNULFlBQVk7TUFDWixrQ0FBa0M7TUFDbEMsbUNBQW1DO01BQ25DLDBCQzlFTSxFQUFBO0VEcURsQjtNQTRCWSxZQUFZO01BQ1osa0NBQWtDO01BQ2xDLG1DQUFtQztNQUNuQywyQkFBMkIsRUFBQTtFQS9CdkM7SUFtQ1EsZ0JBQWdCLEVBQUE7RUFuQ3hCO01Bc0NZLFNBQVM7TUFDVCxrQ0FBa0M7TUFDbEMsbUNBQW1DO01BQ25DLDZCQzlGTSxFQUFBO0VEcURsQjtNQTZDWSxTQUFTO01BQ1Qsa0NBQWtDO01BQ2xDLG1DQUFtQztNQUNuQyw4QkFBOEIsRUFBQTtFQWhEMUM7SUFxRFksVUFBVSxFQUFBO0VBckR0QjtJQXdEWSxVQUFVLEVBQUE7RUF4RHRCO0lBNkRZLFdBQVcsRUFBQTtFQTdEdkI7SUFnRVksV0FBVyxFQUFBO0VBaEV2QjtJQXFFUSxZQUFZLEVBQUE7RUFyRXBCO0lBeUVRLG9CQUFvQixFQUFBO0VBekU1QjtJQTZFUSxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsZ0JBQWdCO0lBQ2hCLDBCQ3ZJVSxFQUFBO0VEcURsQjtNQXFGWSx5QkFBeUI7TUFDekIsY0FBYztNQUNkLFdBQVc7TUFDWCw4QkFBOEI7TUFDOUIsbUJBQW1CO01BQ25CLHVCQUF1QjtNQUN2QixnQkFBZ0I7TUFDaEIseUJBQWlCO1NBQWpCLHNCQUFpQjtVQUFqQixxQkFBaUI7Y0FBakIsaUJBQWlCO01BQ2pCLDJDQUEyQyxFQUFBO0VBN0Z2RDtRQStGZ0IsOEJBQThCLEVBQUE7RUEvRjlDO1FBa0dnQixlQUFlO1FBQ2YsWUFBWTtRQUNaLG1CQ3ZJQyxFQUFBO0VEbUNqQjtRQXdHZ0IseUJBQWlCO1dBQWpCLHNCQUFpQjtZQUFqQixxQkFBaUI7Z0JBQWpCLGlCQUFpQjtRQUNqQixxQkFBcUIsRUFBQTtFQXpHckM7VUE0R29CLHFCQUFxQixFQUFBO0VBNUd6QztNQWtIWSwyQkFBMkIsRUFBQTtFQWxIdkM7TUFzSFksYUFBYTtNQUNiLHFCQUFxQixFQUFBO0VBdkhqQztNQTJIWSxZQUFZO01BQ1osbUJDL0pLLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWRzL2NvbXBvbmVudHMvbmgtc2VsZWN0L25oLXNlbGVjdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi8uLi8uLi9hc3NldHMvc3R5bGVzL2NvbmZpZ1wiO1xyXG5cclxuLy8kbWFpbi1jb2xvcjogIzlBMTJCMztcclxuLy8kbWFpbi1iZy1jb2xvcjogIzAwNzQ1NTtcclxuLy8kYm9yZGVyQ29sb3I6ICNkZGQ7XHJcbi8vJGRhbmdlcjogI2RjMzU0NTtcclxuXHJcbm5oLXNlbGVjdCB7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cclxuICAgICYubmgtbXVsdGlwbGUge1xyXG4gICAgICAgIGxpLm5oLXNlbGVjdC1pdGVtIHtcclxuICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG5cclxuICAgICAgICAgICAgJi5zZWxlY3RlZCB7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiBub25lO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAubmgtc2VsZWN0ZWQtaWNvbiB7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgICAgICB0b3A6IDVweDtcclxuICAgICAgICAgICAgICAgIHJpZ2h0OiA1cHg7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgJi5uby1ib3JkZXIgPiBidXR0b24ge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgJi5oYXMtZXJyb3IgPiBidXR0b24ubmgtc2VsZWN0LWJ1dHRvbiB7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgJGRhbmdlciAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG5cclxuICAgIGJ1dHRvbi5uaC1zZWxlY3QtYnV0dG9uIHtcclxuICAgICAgICBtYXgtd2lkdGg6IDI1MHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6ICRib3JkZXJSYWRpdXMgIWltcG9ydGFudDtcclxuICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgbWluLWhlaWdodDogMzNweDtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAkYm9yZGVyQ29sb3I7XHJcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgcGFkZGluZzogN3B4IDE0cHg7XHJcblxyXG4gICAgICAgICY6Zm9jdXMge1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAkYm9yZGVyUmFkaXVzICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRib3JkZXJBY3RpdmVDb2xvcjtcclxuICAgICAgICAgICAgYm94LXNoYWRvdzogMCAwIDAgMC4ycmVtIHJnYmEoMCwgMTIzLCAyNTUsIDAuMjUpICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4ubmgtc2VsZWN0LW1lbnUge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBtYXgtd2lkdGg6IDI1MHB4O1xyXG4gICAgbWluLXdpZHRoOiAyNTBweDtcclxuICAgIC8vYm9yZGVyOiAxcHggc29saWQgJGJvcmRlckNvbG9yO1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAkYm9yZGVyQ29sb3I7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzcHggIWltcG9ydGFudDtcclxuXHJcbiAgICAmOmJlZm9yZSwgJjphZnRlciB7XHJcbiAgICAgICAgY29udGVudDogJyc7XHJcbiAgICAgICAgd2lkdGg6IDA7XHJcbiAgICAgICAgaGVpZ2h0OiAwO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIH1cclxuICAgICYubmgtbWVudS10b3Age1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcblxyXG4gICAgICAgICY6YmVmb3JlIHtcclxuICAgICAgICAgICAgd2lkdGg6IDA7XHJcbiAgICAgICAgICAgIGhlaWdodDogMDtcclxuICAgICAgICAgICAgYm90dG9tOiAtOHB4O1xyXG4gICAgICAgICAgICBib3JkZXItbGVmdDogOHB4IHNvbGlkIHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICBib3JkZXItcmlnaHQ6IDhweCBzb2xpZCB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgYm9yZGVyLXRvcDogOHB4IHNvbGlkICRib3JkZXJDb2xvcjtcclxuICAgICAgICB9XHJcbiAgICAgICAgJjphZnRlciB7XHJcbiAgICAgICAgICAgIGJvdHRvbTogLTdweDtcclxuICAgICAgICAgICAgYm9yZGVyLWxlZnQ6IDdweCBzb2xpZCB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgYm9yZGVyLXJpZ2h0OiA3cHggc29saWQgdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGJvcmRlci10b3A6IDdweCBzb2xpZCB3aGl0ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAmLm5oLW1lbnUtYm90dG9tIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG5cclxuICAgICAgICAmOmJlZm9yZSB7XHJcbiAgICAgICAgICAgIHRvcDogLThweDtcclxuICAgICAgICAgICAgYm9yZGVyLWxlZnQ6IDhweCBzb2xpZCB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgYm9yZGVyLXJpZ2h0OiA4cHggc29saWQgdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDhweCBzb2xpZCAkYm9yZGVyQ29sb3I7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAmOmFmdGVyIHtcclxuICAgICAgICAgICAgdG9wOiAtN3B4O1xyXG4gICAgICAgICAgICBib3JkZXItbGVmdDogN3B4IHNvbGlkIHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICBib3JkZXItcmlnaHQ6IDdweCBzb2xpZCB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogN3B4IHNvbGlkIHdoaXRlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgICYubmgtbWVudS1sZWZ0IHtcclxuICAgICAgICAmOmJlZm9yZSB7XHJcbiAgICAgICAgICAgIGxlZnQ6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgICY6YWZ0ZXIge1xyXG4gICAgICAgICAgICBsZWZ0OiAxMXB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgICYubmgtbWVudS1yaWdodCB7XHJcbiAgICAgICAgJjpiZWZvcmUge1xyXG4gICAgICAgICAgICByaWdodDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgJjphZnRlciB7XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxMXB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuc2VhcmNoLWJveCB7XHJcbiAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgfVxyXG5cclxuICAgIGhyIHtcclxuICAgICAgICBtYXJnaW46IDAgIWltcG9ydGFudDtcclxuICAgIH1cclxuXHJcbiAgICB1bCB7XHJcbiAgICAgICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgICAgICBtYXgtaGVpZ2h0OiAzMDBweDtcclxuICAgICAgICBvdmVyZmxvdy15OiBhdXRvO1xyXG4gICAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAkYm9yZGVyQ29sb3I7XHJcbiAgICAgICAgLy9ib3gtc2hhZG93OiAwIDJweCA2cHggcmdiYSgwLCAwLCAwLCAuMyksIDAgMCAxcHggMCByZ2JhKDAsIDAsIDAsIC4zKTtcclxuICAgICAgICBsaSB7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDhweCA4cHggOHB4IDEycHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWFlYWVhICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgICY6bGFzdC1jaGlsZCB7XHJcbiAgICAgICAgICAgICAgICBib3JkZXItYm90dG9tOiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgJjpob3ZlciwgJi5hY3RpdmUsICYuc2VsZWN0ZWQge1xyXG4gICAgICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogJHByaW1hcnk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGEge1xyXG4gICAgICAgICAgICAgICAgdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcblxyXG4gICAgICAgICAgICAgICAgJjpob3ZlciwgJjphY3RpdmUsICY6dmlzaXRlZCwgJjpmb2N1cyB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuYmFja2dyb3VuZC1ub25lIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGkubmgtc2VsZWN0LWl0ZW0gYTp2aXNpdGVkLCBsaS5uaC1zZWxlY3QtaXRlbSBhOmFjdGl2ZSwgbGkubmgtc2VsZWN0LWl0ZW0gYTpmb2N1cyB7XHJcbiAgICAgICAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxpLm5oLXNlbGVjdC1pdGVtLnNlbGVjdGVkIHtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAkcHJpbWFyeTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIiwiJGRlZmF1bHQtY29sb3I6ICMyMjI7XHJcbiRmb250LWZhbWlseTogXCJBcmlhbFwiLCB0YWhvbWEsIEhlbHZldGljYSBOZXVlO1xyXG4kY29sb3ItYmx1ZTogIzM1OThkYztcclxuJG1haW4tY29sb3I6ICMwMDc0NTU7XHJcbiRib3JkZXJDb2xvcjogI2RkZDtcclxuJHNlY29uZC1jb2xvcjogI2IwMWExZjtcclxuJHRhYmxlLWJhY2tncm91bmQtY29sb3I6ICMwMDk2ODg7XHJcbiRibHVlOiAjMDA3YmZmO1xyXG4kZGFyay1ibHVlOiAjMDA3MkJDO1xyXG4kYnJpZ2h0LWJsdWU6ICNkZmYwZmQ7XHJcbiRpbmRpZ286ICM2NjEwZjI7XHJcbiRwdXJwbGU6ICM2ZjQyYzE7XHJcbiRwaW5rOiAjZTgzZThjO1xyXG4kcmVkOiAjZGMzNTQ1O1xyXG4kb3JhbmdlOiAjZmQ3ZTE0O1xyXG4keWVsbG93OiAjZmZjMTA3O1xyXG4kZ3JlZW46ICMyOGE3NDU7XHJcbiR0ZWFsOiAjMjBjOTk3O1xyXG4kY3lhbjogIzE3YTJiODtcclxuJHdoaXRlOiAjZmZmO1xyXG4kZ3JheTogIzg2OGU5NjtcclxuJGdyYXktZGFyazogIzM0M2E0MDtcclxuJHByaW1hcnk6ICMwMDdiZmY7XHJcbiRzZWNvbmRhcnk6ICM2Yzc1N2Q7XHJcbiRzdWNjZXNzOiAjMjhhNzQ1O1xyXG4kaW5mbzogIzE3YTJiODtcclxuJHdhcm5pbmc6ICNmZmMxMDc7XHJcbiRkYW5nZXI6ICNkYzM1NDU7XHJcbiRsaWdodDogI2Y4ZjlmYTtcclxuJGRhcms6ICMzNDNhNDA7XHJcbiRsYWJlbC1jb2xvcjogIzY2NjtcclxuJGJhY2tncm91bmQtY29sb3I6ICNFQ0YwRjE7XHJcbiRib3JkZXJBY3RpdmVDb2xvcjogIzgwYmRmZjtcclxuJGJvcmRlclJhZGl1czogMDtcclxuJGRhcmtCbHVlOiAjNDVBMkQyO1xyXG4kbGlnaHRHcmVlbjogIzI3YWU2MDtcclxuJGxpZ2h0LWJsdWU6ICNmNWY3Zjc7XHJcbiRicmlnaHRHcmF5OiAjNzU3NTc1O1xyXG4kbWF4LXdpZHRoLW1vYmlsZTogNzY4cHg7XHJcbiRtYXgtd2lkdGgtdGFibGV0OiA5OTJweDtcclxuJG1heC13aWR0aC1kZXNrdG9wOiAxMjgwcHg7XHJcblxyXG4vLyBCRUdJTjogTWFyZ2luXHJcbkBtaXhpbiBuaC1tZygkcGl4ZWwpIHtcclxuICAgIG1hcmdpbjogJHBpeGVsICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBtaXhpbiBuaC1tZ3QoJHBpeGVsKSB7XHJcbiAgICBtYXJnaW4tdG9wOiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLW1nYigkcGl4ZWwpIHtcclxuICAgIG1hcmdpbi1ib3R0b206ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWdsKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLWxlZnQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtbWdyKCRwaXhlbCkge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBNYXJnaW5cclxuXHJcbi8vIEJFR0lOOiBQYWRkaW5nXHJcbkBtaXhpbiBuaC1wZCgkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmc6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGR0KCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy10b3A6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGRiKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy1ib3R0b206ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWl4aW4gbmgtcGRsKCRwaXhlbCkge1xyXG4gICAgcGFkZGluZy1sZWZ0OiAkcGl4ZWwgIWltcG9ydGFudDtcclxufVxyXG5cclxuQG1peGluIG5oLXBkcigkcGl4ZWwpIHtcclxuICAgIHBhZGRpbmctcmlnaHQ6ICRwaXhlbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vLyBFTkQ6IFBhZGRpbmdcclxuXHJcbi8vIEJFR0lOOiBXaWR0aFxyXG5AbWl4aW4gbmgtd2lkdGgoJHdpZHRoKSB7XHJcbiAgICB3aWR0aDogJHdpZHRoICFpbXBvcnRhbnQ7XHJcbiAgICBtaW4td2lkdGg6ICR3aWR0aCAhaW1wb3J0YW50O1xyXG4gICAgbWF4LXdpZHRoOiAkd2lkdGggIWltcG9ydGFudDtcclxufVxyXG5cclxuLy8gRU5EOiBXaWR0aFxyXG5cclxuLy8gQkVHSU46IEljb24gU2l6ZVxyXG5AbWl4aW4gbmgtc2l6ZS1pY29uKCRzaXplKSB7XHJcbiAgICB3aWR0aDogJHNpemU7XHJcbiAgICBoZWlnaHQ6ICRzaXplO1xyXG4gICAgZm9udC1zaXplOiAkc2l6ZTtcclxufVxyXG5cclxuLy8gRU5EOiBJY29uIFNpemVcclxuIl19 */"

/***/ }),

/***/ "./src/app/shareds/components/nh-select/nh-select.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/shareds/components/nh-select/nh-select.component.ts ***!
  \*********************************************************************/
/*! exports provided: NhSelect, NhSelectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NhSelect", function() { return NhSelect; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NhSelectComponent", function() { return NhSelectComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/cdk/overlay */ "./node_modules/@angular/cdk/esm5/overlay.es5.js");
/* harmony import */ var _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/cdk/portal */ "./node_modules/@angular/cdk/esm5/portal.es5.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");









var NhSelect = /** @class */ (function () {
    function NhSelect(id, name, icon, data, index, active, selected) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.data = data;
        this.index = index;
        this.active = active;
        this.selected = selected;
    }
    return NhSelect;
}());

var NhSelectComponent = /** @class */ (function () {
    function NhSelectComponent(overlay, viewContainerRef, http, el, renderer) {
        var _this = this;
        this.overlay = overlay;
        this.viewContainerRef = viewContainerRef;
        this.http = http;
        this.el = el;
        this.renderer = renderer;
        this._data = [];
        this._selectedItem = null;
        this.multiple = false;
        this.liveSearch = false;
        this.isEnable = true;
        this.width = 250;
        this.isInsertValue = false;
        this.pageSize = 10;
        this.readonly = false;
        this.selectedItems = [];
        this.btnClasses = 'btn-light';
        /**
         * @deprecated use itemSelected instead
         */
        this.onSelectItem = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.itemSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.valueChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.shown = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.hidden = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @deprecated use valueInserted instead.
         */
        this.onInsertValue = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.valueInserted = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.keywordPressed = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.isSearching = false;
        this.source = [];
        this.currentPage = 1;
        this.totalRows = 0;
        this.totalPages = 0;
        this.positionStrategy = new _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_6__["GlobalPositionStrategy"]();
        this.searchTerm$ = new rxjs__WEBPACK_IMPORTED_MODULE_8__["BehaviorSubject"]('');
        this.propagateChange = function () {
        };
        this.inputId = "nh-select-" + (new Date().getTime() + Math.floor((Math.random() * 10) + 1));
        this.searchTerm$
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["debounceTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["distinctUntilChanged"])())
            .subscribe(function (term) {
            if (_this.liveSearch && _this.url) {
                _this.search(term);
            }
        });
    }
    NhSelectComponent_1 = NhSelectComponent;
    Object.defineProperty(NhSelectComponent.prototype, "value", {
        get: function () {
            return this._value;
        },
        set: function (val) {
            if (val != null) {
                if (val instanceof Array) {
                    this._value = val;
                    var selectedItem = lodash__WEBPACK_IMPORTED_MODULE_3__["filter"](this.source, function (item) {
                        return val.indexOf(item.id) > -1;
                    });
                    if (selectedItem && selectedItem.length > 0) {
                        lodash__WEBPACK_IMPORTED_MODULE_3__["each"](selectedItem, function (item) {
                            item.selected = true;
                        });
                        this.label = this.getSelectedName(selectedItem);
                    }
                    else {
                        this.label = this.title;
                    }
                }
                else {
                    this.getSelectedItem(val);
                }
            }
            else {
                if (this.multiple) {
                    this.getSelectedItem(val);
                }
                else {
                    this.label = this.title;
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NhSelectComponent.prototype, "data", {
        get: function () {
            return this._data;
        },
        set: function (values) {
            var _this = this;
            setTimeout(function () {
                if (values) {
                    _this._data = values;
                    _this.source = values.map(function (item, index) {
                        var obj = item;
                        obj.index = index;
                        obj.active = false;
                        if (_this.value && _this.value instanceof Array) {
                            item.selected = _this.value.indexOf(item.id) > -1;
                        }
                        else {
                            item.selected = item.id === _this.value;
                        }
                        return obj;
                    });
                    var labelName = _this.source.filter(function (item) {
                        return item.selected;
                    }).map(function (item) { return item.name; }).join(',');
                    if (labelName) {
                        _this.label = labelName;
                    }
                }
            });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NhSelectComponent.prototype, "selectedItem", {
        get: function () {
            return this._selectedItem;
        },
        set: function (value) {
            this._selectedItem = value;
            if (value) {
                this.label = value.name ? value.name : this.title;
            }
        },
        enumerable: true,
        configurable: true
    });
    NhSelectComponent.prototype.ngOnInit = function () {
        if (this.url) {
            this.search();
        }
    };
    NhSelectComponent.prototype.ngAfterViewInit = function () {
        this.overlayRef = this.overlay.create({
            positionStrategy: this.positionStrategy
        });
    };
    NhSelectComponent.prototype.ngOnDestroy = function () {
        this.dismissMenu();
    };
    NhSelectComponent.prototype.ngOnChanges = function (changes) {
    };
    NhSelectComponent.prototype.searchKeyUp = function (e, term) {
        var _this = this;
        var keyCode = e.keyCode;
        // Navigate
        if (keyCode === 27) {
            // Check
        }
        if (keyCode === 27 || keyCode === 17 || e.ctrlKey) {
            return;
        }
        if (keyCode === 37 || keyCode === 38 || keyCode === 39 || keyCode === 40 || keyCode === 13) {
            this.navigate(keyCode);
            e.preventDefault();
        }
        else {
            if (!term) {
                this.source = this.data.map(function (item, index) {
                    var obj = item;
                    obj.index = index;
                    obj.active = false;
                    obj.selected = false;
                    return obj;
                });
                return;
            }
            if (this.url) {
                // this.search(term);
                this.searchTerm$.next(term);
            }
            else {
                var searchResult = lodash__WEBPACK_IMPORTED_MODULE_3__["filter"](this.data, function (item) {
                    return _this.stripToVietnameChar(item.name).indexOf(_this.stripToVietnameChar(term)) > -1;
                });
                this.source = searchResult.map(function (item, index) {
                    var obj = item;
                    obj.index = index;
                    obj.active = false;
                    obj.selected = false;
                    return obj;
                });
            }
        }
    };
    NhSelectComponent.prototype.buttonClick = function () {
        this.initDropdownMenu();
    };
    NhSelectComponent.prototype.selectItem = function (item) {
        if (!this.multiple) {
            this.label = item.name;
            lodash__WEBPACK_IMPORTED_MODULE_3__["each"](this.source, function (data) {
                data.selected = false;
            });
            item.selected = true;
            this.value = item.id;
            this.propagateChange(item.id);
            this.onSelectItem.emit(item);
            this.itemSelected.emit(item);
            this.dismissMenu();
        }
        else {
            item.selected = !item.selected;
            var selectedItem = lodash__WEBPACK_IMPORTED_MODULE_3__["filter"](this.source, function (source) {
                return source.selected;
            });
            this.label = selectedItem && selectedItem.length > 0 ? this.getSelectedName(selectedItem) : this.title;
            if (this.value instanceof Array) {
                var selectedIds = selectedItem.map(function (selected) {
                    return selected.id;
                });
                this.onSelectItem.emit(selectedItem);
                this.itemSelected.emit(selectedItem);
                this.propagateChange(selectedIds);
            }
            else {
                this.onSelectItem.emit(selectedItem);
                this.itemSelected.emit(selectedItem);
                this.propagateChange(item.id);
            }
        }
    };
    NhSelectComponent.prototype.onClick = function (event) {
        var menuElement = this.overlayRef.overlayElement.getElementsByClassName('nh-select-menu')[0];
        if (menuElement && !menuElement.contains(event.target)
            && !this.el.nativeElement.contains(event.target)) {
            this.dismissMenu();
        }
    };
    // @HostListener('scroll', ['$event'])
    // onWindowScroll() {
    //     console.log('window scroll');
    // }
    NhSelectComponent.prototype.stripToVietnameChar = function (str) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
        str = str.replace(/đ/g, 'd');
        return str;
    };
    NhSelectComponent.prototype.registerOnChange = function (fn) {
        this.propagateChange = fn;
    };
    NhSelectComponent.prototype.writeValue = function (value) {
        if (value != null && value !== undefined && value !== '') {
            this.value = value;
        }
        else {
            this.label = this.title;
        }
    };
    NhSelectComponent.prototype.registerOnTouched = function () {
    };
    NhSelectComponent.prototype.validate = function (c) {
        this.value = c.value;
    };
    NhSelectComponent.prototype.resetSelectedList = function () {
        lodash__WEBPACK_IMPORTED_MODULE_3__["each"](this.source, function (item) {
            item.selected = false;
        });
        this.label = this.title;
    };
    NhSelectComponent.prototype.insertValue = function () {
        this.label = this.searchTerm$.value;
        this.onInsertValue.emit(this.searchTerm$.value);
        this.valueInserted.emit(this.searchTerm$.value);
    };
    NhSelectComponent.prototype.clear = function () {
        this.selectedItems = [];
        this.label = this.title;
    };
    NhSelectComponent.prototype.getSelectedItem = function (val) {
        var _this = this;
        lodash__WEBPACK_IMPORTED_MODULE_3__["each"](this.source, function (item) {
            if (item.id === val) {
                _this.label = item.name;
                item.selected = true;
            }
            else {
                item.selected = false;
            }
        });
        this._value = val;
        this.valueChange.emit(this._value);
    };
    NhSelectComponent.prototype.getSelectedName = function (listItem) {
        return listItem.map(function (item) {
            return item.name;
        }).join(', ');
    };
    NhSelectComponent.prototype.navigate = function (key) {
        var selectedItem = this.source.find(function (item) {
            return item.active;
        });
        switch (key) {
            case 37:
                this.back(selectedItem);
                break;
            case 38:
                this.back(selectedItem);
                break;
            case 39:
                this.next(selectedItem);
                break;
            case 40:
                this.next(selectedItem);
                break;
            case 13:
                if (selectedItem) {
                    this.selectItem(selectedItem);
                }
                break;
        }
    };
    NhSelectComponent.prototype.next = function (selectedItem) {
        if (!selectedItem) {
            var firstItem = this.source[0];
            if (firstItem) {
                firstItem.active = true;
            }
        }
        else {
            var index = selectedItem.index + 1;
            if (index > this.source.length - 1) {
                index = 0;
            }
            var currentItem = this.source[index];
            this.resetActiveStatus();
            currentItem.active = true;
        }
    };
    NhSelectComponent.prototype.back = function (selectedItem) {
        if (!selectedItem) {
            var lastItem = this.source[this.source.length - 1];
            if (lastItem) {
                lastItem.active = true;
            }
        }
        else {
            var index = selectedItem.index - 1;
            if (index < 0) {
                index = this.source.length - 1;
            }
            var currentItem = this.source[index];
            this.resetActiveStatus();
            currentItem.active = true;
        }
    };
    NhSelectComponent.prototype.resetActiveStatus = function () {
        this.source.forEach(function (item) { return item.active = false; });
    };
    NhSelectComponent.prototype.initDropdownMenu = function () {
        if (this.overlayRef) {
            if (!this.overlayRef.hasAttached()) {
                this.overlayRef.attach(new _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_7__["TemplatePortal"](this.dropdownTemplate, this.viewContainerRef));
                var clientRect = this.el.nativeElement.getBoundingClientRect();
                var menuElement = this.overlayRef.overlayElement.getElementsByClassName('nh-select-menu')[0];
                var menuHeight = this.overlayRef.overlayElement.getElementsByClassName('nh-select-menu')[0].clientHeight;
                var windowWidth = window.innerWidth;
                var windowHeight = window.innerHeight;
                var isLeft = windowWidth - (clientRect.left + 350) > 0;
                var isTop = windowHeight - (clientRect.top + clientRect.height + menuHeight + 10) < 0;
                var left = isLeft ? clientRect.left : clientRect.left - (250 - clientRect.width);
                var top_1 = isTop ? clientRect.top - menuHeight - 10 : clientRect.top + clientRect.height;
                this.positionStrategy.left(left + "px");
                this.positionStrategy.top(top_1 + "px");
                this.renderer.addClass(menuElement, isTop ? 'nh-menu-top' : 'nh-menu-bottom');
                this.renderer.addClass(menuElement, isLeft ? 'nh-menu-left' : 'nh-menu-right');
                this.positionStrategy.apply();
                if (this.liveSearch && document.getElementById(this.inputId)) {
                    document.getElementById(this.inputId).focus();
                }
                this.shown.emit();
            }
            else {
                this.overlayRef.detach();
            }
        }
    };
    NhSelectComponent.prototype.dismissMenu = function () {
        if (this.overlayRef && this.overlayRef.hasAttached()) {
            this.overlayRef.detach();
            this.hidden.emit();
        }
    };
    NhSelectComponent.prototype.search = function (searchTerm) {
        var _this = this;
        this.source = [];
        this.isSearching = true;
        if (!this.url) {
            this.isSearching = false;
            return;
        }
        this.http
            .get(this.url, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpParams"]()
                .set('keyword', searchTerm ? searchTerm : '')
                .set('pageSize', this.pageSize ? this.pageSize.toString() : '10')
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.isSearching = false; }))
            .subscribe(function (result) {
            var items = result.items;
            _this.totalRows = result.totalRows;
            _this.paging();
            _this.source = items.map(function (item, index) {
                var obj = item;
                obj.index = index;
                obj.active = false;
                obj.selected = false;
                return obj;
            });
        });
    };
    NhSelectComponent.prototype.paging = function () {
        var pageSize = this.pageSize ? this.pageSize : 10;
        this.totalPages = Math.ceil(this.totalRows / pageSize);
    };
    var NhSelectComponent_1;
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('dropdownTemplate'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"])
    ], NhSelectComponent.prototype, "dropdownTemplate", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSelectComponent.prototype, "multiple", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSelectComponent.prototype, "liveSearch", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], NhSelectComponent.prototype, "title", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSelectComponent.prototype, "isEnable", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSelectComponent.prototype, "width", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSelectComponent.prototype, "isInsertValue", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], NhSelectComponent.prototype, "url", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], NhSelectComponent.prototype, "loading", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSelectComponent.prototype, "pageSize", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSelectComponent.prototype, "readonly", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSelectComponent.prototype, "selectedItems", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSelectComponent.prototype, "btnClasses", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSelectComponent.prototype, "onSelectItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSelectComponent.prototype, "itemSelected", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSelectComponent.prototype, "valueChange", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSelectComponent.prototype, "shown", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSelectComponent.prototype, "hidden", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSelectComponent.prototype, "onInsertValue", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSelectComponent.prototype, "valueInserted", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NhSelectComponent.prototype, "keywordPressed", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Array])
    ], NhSelectComponent.prototype, "data", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
    ], NhSelectComponent.prototype, "value", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
    ], NhSelectComponent.prototype, "selectedItem", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('document:click', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], NhSelectComponent.prototype, "onClick", null);
    NhSelectComponent = NhSelectComponent_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'nh-select',
            template: __webpack_require__(/*! ./nh-select.component.html */ "./src/app/shareds/components/nh-select/nh-select.component.html"),
            providers: [
                { provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALUE_ACCESSOR"], useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () { return NhSelectComponent_1; }), multi: true },
                { provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALIDATORS"], useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () { return NhSelectComponent_1; }), multi: true }
            ],
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./nh-select.component.scss */ "./src/app/shareds/components/nh-select/nh-select.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_6__["Overlay"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]])
    ], NhSelectComponent);
    return NhSelectComponent;
}());



/***/ }),

/***/ "./src/app/shareds/components/nh-select/nh-select.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/shareds/components/nh-select/nh-select.module.ts ***!
  \******************************************************************/
/*! exports provided: NhSelectModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NhSelectModule", function() { return NhSelectModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _nh_select_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./nh-select.component */ "./src/app/shareds/components/nh-select/nh-select.component.ts");
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/cdk/overlay */ "./node_modules/@angular/cdk/esm5/overlay.es5.js");





var NhSelectModule = /** @class */ (function () {
    function NhSelectModule() {
    }
    NhSelectModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_4__["OverlayModule"]],
            declarations: [_nh_select_component__WEBPACK_IMPORTED_MODULE_3__["NhSelectComponent"]],
            exports: [_nh_select_component__WEBPACK_IMPORTED_MODULE_3__["NhSelectComponent"]]
        })
    ], NhSelectModule);
    return NhSelectModule;
}());



/***/ })

}]);
//# sourceMappingURL=default~modules-configs-config-module~modules-customer-config-config-module~modules-customer-custome~606d9c5e.js.map