(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~modules-hr-organization-organization-module~modules-warehouse-goods-goods-module"],{

/***/ "./src/app/modules/hr/organization/office/models/office-contact.model.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/models/office-contact.model.ts ***!
  \*******************************************************************************/
/*! exports provided: OfficeContact */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfficeContact", function() { return OfficeContact; });
var OfficeContact = /** @class */ (function () {
    function OfficeContact() {
    }
    return OfficeContact;
}());



/***/ }),

/***/ "./src/app/modules/hr/organization/office/models/office-translation.model.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/models/office-translation.model.ts ***!
  \***********************************************************************************/
/*! exports provided: OfficeTranslation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfficeTranslation", function() { return OfficeTranslation; });
var OfficeTranslation = /** @class */ (function () {
    function OfficeTranslation() {
    }
    return OfficeTranslation;
}());



/***/ }),

/***/ "./src/app/modules/hr/organization/office/models/office.model.ts":
/*!***********************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/models/office.model.ts ***!
  \***********************************************************************/
/*! exports provided: Office */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Office", function() { return Office; });
var Office = /** @class */ (function () {
    function Office(officeType, order, status, parentId, isActive) {
        this.officeType = officeType ? officeType : 0;
        this.order = order ? order : 0;
        this.status = status;
        this.parentId = parentId;
        this.isActive = isActive ? isActive : true;
    }
    return Office;
}());



/***/ }),

/***/ "./src/app/modules/hr/organization/office/office-contact/office-contact-form.component.html":
/*!**************************************************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/office-contact/office-contact-form.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #officeContactFormModal\r\n          size=\"sm\"\r\n          (onHidden)=\"onModalHidden()\">\r\n    <form action=\"\" class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content>\r\n            <div class=\"form-group\"\r\n                 [class.has-error]=\"formErrors.userId\">\r\n                <label i18n=\"@@fullName\" i18n-ghmLabel ghmLabel=\"Họ và tên nhân viên\"\r\n                       class=\"col-sm-12\"\r\n                       [required]=\"true\"></label>\r\n                <div class=\"col-sm-12\">\r\n                    <ghm-user-suggestion\r\n                        [selectedUser]=\"selectedUser\"\r\n                        (userSelected)=\"onUserSelected($event)\"\r\n                    ></ghm-user-suggestion>\r\n                    <span class=\"help-block\">\r\n                        {\r\n                        formErrors.userId,\r\n                        select, required {Vui lòng chọn nhân viên}\r\n                        }\r\n                    </span>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\"\r\n                 [class.has-error]=\"formErrors.phoneNumber\">\r\n                <label i18n=\"@@phoneNumber\" i18n-ghmLabel ghmLabel=\"Số điện thoại\"\r\n                       class=\"col-sm-12\"\r\n                       [required]=\"true\"></label>\r\n                <div class=\"col-sm-12\">\r\n                    {{formErrors.phoneNumber}}\r\n                    <input type=\"text\" class=\"form-control\" formControlName=\"phoneNumber\"\r\n                           i18n-placeholder=\"@@enterPhoneNumber\"\r\n                           placeholder=\"Vui lòng nhập số điện thoại.\">\r\n                    <span class=\"help-block\">\r\n                        {\r\n                        formErrors.phoneNumber,\r\n                        select,\r\n                        required {Phone number is required}\r\n                        maxlength {Phone number not allowed over 50 characters}\r\n                        pattern {Phone number invalid}\r\n                        }\r\n                    </span>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\"\r\n                 [class.has-error]=\"formErrors.email\">\r\n                <label i18n=\"@@email\" i18n-ghmLabel ghmLabel=\"Email\"\r\n                       class=\"col-sm-12\"></label>\r\n                <div class=\"col-sm-12\">\r\n                    <input type=\"text\" class=\"form-control\" formControlName=\"email\"\r\n                           i18n-placeholder=\"@@enterEmail\"\r\n                           placeholder=\"Vui lòng nhập email.\">\r\n                    <span class=\"help-block\">\r\n                        {\r\n                        formErrors.email,\r\n                        select,\r\n                        maxlength {Email not allowed over 500 characters}\r\n                        pattern {Email invalid}\r\n                        }\r\n                    </span>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\"\r\n                 [class.has-error]=\"formErrors.fax\">\r\n                <label i18n=\"@@fax\" i18n-ghmLabel ghmLabel=\"Fax\"\r\n                       class=\"col-sm-12\"\r\n                ></label>\r\n                <div class=\"col-sm-12\">\r\n                    {{formErrors.fax}}\r\n                    <input type=\"text\" class=\"form-control\" formControlName=\"fax\"\r\n                           i18n-placeholder=\"@@enterFax\"\r\n                           placeholder=\"Vui lòng nhập fax.\">\r\n                    <span class=\"help-block\">\r\n                        {\r\n                        formErrors.fax,\r\n                        select,\r\n                        maxlength {Fax not allowed over 50 characters}\r\n                        pattern {Fax invalid}\r\n                        }\r\n                    </span>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <mat-checkbox [checked]=\"isCreateAnother\" (change)=\"isCreateAnother = !isCreateAnother\"\r\n                          *ngIf=\"!isUpdate\"\r\n                          i18n=\"@@isCreateAnother\"\r\n                          class=\"cm-mgr-5\"\r\n                          color=\"primary\">\r\n                Tiếp tục thêm\r\n            </mat-checkbox>\r\n            <ghm-button classes=\"btn btn-primary cm-mgr-5\" [loading]=\"isSaving\" i18n=\"@@save\">\r\n                Lưu\r\n            </ghm-button>\r\n            <ghm-button type=\"button\" classes=\"btn btn-default\"\r\n                        nh-dismiss=\"true\"\r\n                        [loading]=\"isSaving\"\r\n                        i18n=\"@@cancel\">\r\n                Hủy bỏ\r\n            </ghm-button>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/organization/office/office-contact/office-contact-form.component.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/office-contact/office-contact-form.component.ts ***!
  \************************************************************************************************/
/*! exports provided: OfficeContactFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfficeContactFormComponent", function() { return OfficeContactFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _services_office_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/office.service */ "./src/app/modules/hr/organization/office/services/office.service.ts");
/* harmony import */ var _models_office_contact_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../models/office-contact.model */ "./src/app/modules/hr/organization/office/models/office-contact.model.ts");
/* harmony import */ var _shareds_components_ghm_user_suggestion_ghm_user_suggestion_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../shareds/components/ghm-user-suggestion/ghm-user-suggestion.component */ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");










var OfficeContactFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](OfficeContactFormComponent, _super);
    function OfficeContactFormComponent(fb, utilService, officeService) {
        var _this = _super.call(this) || this;
        _this.fb = fb;
        _this.utilService = utilService;
        _this.officeService = officeService;
        _this.contact = new _models_office_contact_model__WEBPACK_IMPORTED_MODULE_5__["OfficeContact"]();
        return _this;
    }
    OfficeContactFormComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    OfficeContactFormComponent.prototype.onModalHidden = function () {
        this.selectedUser = null;
        this.model.reset();
    };
    OfficeContactFormComponent.prototype.onUserSelected = function (user) {
        this.selectedUser = user;
        if (user) {
            this.model.patchValue({
                userId: user.id,
                fullName: user.fullName,
                avatar: user.avatar,
                officeName: user.officeName,
                positionName: user.positionName
            });
        }
        else {
            this.model.patchValue({
                userId: null,
                fullName: null,
                avatar: null,
                officeName: null,
                positionName: null
            });
        }
    };
    OfficeContactFormComponent.prototype.add = function () {
        this.isUpdate = false;
        this.officeContactFormModal.open();
    };
    OfficeContactFormComponent.prototype.edit = function (officeContact) {
        this.isUpdate = true;
        this.selectedUser = new _shareds_components_ghm_user_suggestion_ghm_user_suggestion_component__WEBPACK_IMPORTED_MODULE_6__["UserSuggestion"](officeContact.userId, officeContact.fullName, officeContact.officeName, officeContact.positionName, officeContact.avatar);
        this.model.patchValue(officeContact);
        this.officeContactFormModal.open();
    };
    OfficeContactFormComponent.prototype.save = function () {
        var isValid = this.validateModel(true);
        if (isValid) {
            if (this.isUpdate) {
                this.updateContact();
            }
            else {
                this.addContact();
            }
        }
    };
    OfficeContactFormComponent.prototype.updateContact = function () {
        var _this = this;
        this.contact = this.model.value;
        if (this.officeId) {
            this.isSaving = true;
            this.officeService
                .updateContact(this.officeId, this.contact.id, this.contact)
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["finalize"])(function () { return _this.isSaving = false; }))
                .subscribe(function () {
                _this.saveSuccessful.emit(_this.contact);
                _this.officeContactFormModal.dismiss();
            });
        }
        else {
            this.saveSuccessful.emit(this.contact);
            this.officeContactFormModal.dismiss();
        }
    };
    OfficeContactFormComponent.prototype.addContact = function () {
        var _this = this;
        this.contact = this.model.value;
        if (this.officeId) {
            this.isSaving = true;
            this.officeService
                .addContact(this.officeId, this.contact)
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["finalize"])(function () { return _this.isSaving = false; }))
                .subscribe(function (result) {
                _this.contact.id = result.data;
                _this.saveSuccessful.emit(_this.contact);
                _this.afterSave();
            });
        }
        else {
            this.contact.id = this.utilService
                .generateRandomNumber()
                .toString();
            this.saveSuccessful.emit(this.contact);
            this.afterSave();
        }
    };
    OfficeContactFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.renderFormError(['userId', 'phoneNumber', 'email', 'fax']);
        this.validationMessages = this.renderFormErrorMessage([
            { 'userId': ['required', 'maxlength'] },
            { 'phoneNumber': ['required', 'maxlength', 'pattern'] },
            { 'email': ['maxlength', 'pattern'] },
            { 'fax': ['maxlength', 'pattern'] },
        ]);
        this.model = this.fb.group({
            id: [this.contact.id],
            userId: [this.contact.userId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(50)
                ]],
            fullName: [this.contact.fullName],
            avatar: [this.contact.avatar],
            officeName: [this.contact.officeName],
            positionName: [this.contact.positionName],
            phoneNumber: [this.contact.phoneNumber, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(50),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].pattern('^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$')
                ]],
            email: [this.contact.email, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(500),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].pattern('^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$')
                ]],
            fax: [this.contact.fax, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(50),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].pattern('^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$')
                ]]
        });
        this.model.valueChanges.subscribe(function () { return _this.validateModel(false); });
    };
    OfficeContactFormComponent.prototype.afterSave = function () {
        if (this.isCreateAnother) {
            this.selectedUser = null;
            this.model.reset();
        }
        else {
            this.officeContactFormModal.dismiss();
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('officeContactFormModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__["NhModalComponent"])
    ], OfficeContactFormComponent.prototype, "officeContactFormModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], OfficeContactFormComponent.prototype, "officeId", void 0);
    OfficeContactFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-office-contact-form',
            template: __webpack_require__(/*! ./office-contact-form.component.html */ "./src/app/modules/hr/organization/office/office-contact/office-contact-form.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_8__["UtilService"],
            _services_office_service__WEBPACK_IMPORTED_MODULE_4__["OfficeService"]])
    ], OfficeContactFormComponent);
    return OfficeContactFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_2__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/organization/office/office-contact/office-contact.component.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/office-contact/office-contact.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h4 class=\"title caption-subject font-blue-madison bold uppercase\" i18n=\"@@contactInfo\"> Thông tin liên hệ </h4>\r\n<hr>\r\n<div class=\"row\">\r\n    <div class=\"col-sm-12 text-right\">\r\n        <button type=\"button\" class=\"btn blue\" i18n=\"@@add\" (click)=\"add()\">\r\n            Thêm\r\n        </button>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <div class=\"table-responsive\">\r\n            <table class=\"table table-stripped table-hover\">\r\n                <thead>\r\n                <tr>\r\n                    <th class=\"center w50\" i18n=\"@@no\">STT</th>\r\n                    <th class=\"w250\" i18n=\"@@Contact\">Họ tên người liên hệ</th>\r\n                    <th i18n=\"@@phoneNumber\">Số điện thoại</th>\r\n                    <th i18n=\"@@email\">Email</th>\r\n                    <th i18n=\"@@fax\">Fax</th>\r\n                    <th class=\"w150\" i18n=\"@@actions\">Actions</th>\r\n                </tr>\r\n                </thead>\r\n                <tbody>\r\n                <tr *ngFor=\"let contact of officeContacts; index as i\">\r\n                    <td class=\"center middle\">{{ i + 1 }}</td>\r\n                    <td class=\"middle\">\r\n                        <div class=\"media\">\r\n                            <div class=\"media-left\">\r\n                                <a href=\"javascript://\">\r\n                                    <img ghm-image class=\"media-object avatar-md\"\r\n                                         src=\"{{ contact.avatar }}\"\r\n                                         alt=\"{{ contact.fullName }}\">\r\n                                </a>\r\n                            </div>\r\n                            <div class=\"media-body\">\r\n                                <h4 class=\"media-heading\">{{ contact.fullName }}</h4>\r\n                                <div class=\"description\">{{ contact.officeName }} - {{ contact.positionName }}</div>\r\n                            </div>\r\n                        </div>\r\n                    </td>\r\n                    <td class=\"middle\">{{ contact.phoneNumber }}</td>\r\n                    <td class=\"middle\">{{ contact.email }}</td>\r\n                    <td class=\"middle\">{{ contact.fax }}</td>\r\n                    <td class=\"text-right middle w100\">\r\n                        <button type=\"button\" class=\"btn blue btn-sm\" (click)=\"edit(contact)\">\r\n                            <i class=\"fa fa-edit\"></i>\r\n                        </button>\r\n                        <button type=\"button\" class=\"btn red btn-sm\" (click)=\"delete(contact.id)\">\r\n                            <i class=\"fa fa-trash-o\"></i>\r\n                        </button>\r\n                    </td>\r\n                </tr>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<app-office-contact-form\r\n    [officeId]=\"officeId\"\r\n    (saveSuccessful)=\"onSaveSuccess($event)\"\r\n></app-office-contact-form>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/organization/office/office-contact/office-contact.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/office-contact/office-contact.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: OfficeContactComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfficeContactComponent", function() { return OfficeContactComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _office_contact_form_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./office-contact-form.component */ "./src/app/modules/hr/organization/office/office-contact/office-contact-form.component.ts");
/* harmony import */ var _services_office_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/office.service */ "./src/app/modules/hr/organization/office/services/office.service.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);






var OfficeContactComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](OfficeContactComponent, _super);
    function OfficeContactComponent(officeService) {
        var _this = _super.call(this) || this;
        _this.officeService = officeService;
        _this.officeContacts = [];
        return _this;
    }
    OfficeContactComponent.prototype.ngOnInit = function () {
    };
    OfficeContactComponent.prototype.onSaveSuccess = function (officeContact) {
        var officeContactInfo = lodash__WEBPACK_IMPORTED_MODULE_5__["find"](this.officeContacts, function (contact) {
            return contact.id === officeContact.id;
        });
        if (officeContactInfo) {
            officeContactInfo.userId = officeContact.userId;
            officeContactInfo.fullName = officeContact.fullName;
            officeContactInfo.avatar = officeContact.avatar;
            officeContactInfo.phoneNumber = officeContact.phoneNumber;
            officeContactInfo.email = officeContact.email;
            officeContactInfo.fax = officeContact.fax;
        }
        else {
            this.officeContacts.push(officeContact);
        }
    };
    OfficeContactComponent.prototype.add = function () {
        this.officeContactFormComponent.add();
    };
    OfficeContactComponent.prototype.edit = function (officeContact) {
        this.officeContactFormComponent.edit(officeContact);
    };
    OfficeContactComponent.prototype.delete = function (contactId) {
        var _this = this;
        if (this.officeId) {
            this.subscribers.deleteContact = this.officeService.deleteContact(this.officeId, contactId)
                .subscribe(function () {
                _this.removeContact(contactId);
            });
        }
        else {
            this.removeContact(contactId);
        }
    };
    OfficeContactComponent.prototype.removeContact = function (contactId) {
        lodash__WEBPACK_IMPORTED_MODULE_5__["remove"](this.officeContacts, function (contact) {
            return contact.id === contactId;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_office_contact_form_component__WEBPACK_IMPORTED_MODULE_3__["OfficeContactFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _office_contact_form_component__WEBPACK_IMPORTED_MODULE_3__["OfficeContactFormComponent"])
    ], OfficeContactComponent.prototype, "officeContactFormComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], OfficeContactComponent.prototype, "officeId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], OfficeContactComponent.prototype, "officeContacts", void 0);
    OfficeContactComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-office-contact',
            template: __webpack_require__(/*! ./office-contact.component.html */ "./src/app/modules/hr/organization/office/office-contact/office-contact.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_office_service__WEBPACK_IMPORTED_MODULE_4__["OfficeService"]])
    ], OfficeContactComponent);
    return OfficeContactComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_2__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/organization/office/office-detail/office-detail.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/office-detail/office-detail.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #officeDetailModal size=\"lg\"\r\n          [backdropStatic]=\"false\"\r\n          (onShown)=\"onModalShown()\"\r\n          (onHidden)=\"onModalHidden()\">\r\n    <nh-modal-content>\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-4\">\r\n                <div class=\"portlet light bordered\">\r\n                    <div class=\"portlet-title\">\r\n                        <div class=\"caption font-green-sharp\">\r\n                            <i class=\"icon-speech font-green-sharp\"></i>\r\n                            <span class=\"caption-subject bold uppercase\" i18n=\"@@officeOrganizationTitle\">Office organization</span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"portlet-body\">\r\n                        <nh-tree\r\n                            [data]=\"officeTree\"\r\n                            (nodeSelected)=\"onNodeSelected($event)\">\r\n                        </nh-tree>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <!-- END: .col-sm-4 -->\r\n            <div class=\"col-sm-8\">\r\n                <div class=\"portlet light bordered\">\r\n                    <div class=\"portlet-title\">\r\n                        <div class=\"caption font-green-sharp\">\r\n                            <i class=\"icon-speech font-green-sharp\"></i>\r\n                            <span class=\"caption-subject bold uppercase\" i18n=\"@@officeDetailTitle\">\r\n                                Office detail\r\n                            </span>\r\n                        </div>\r\n                        <div class=\"actions\" *ngIf=\"permission.edit || permission.delete\">\r\n                            <a href=\"javascript://\"\r\n                               class=\"btn btn-circle blue cm-mgr-5\"\r\n                               *ngIf=\"permission.edit\"\r\n                               (click)=\"edit()\"\r\n                            >\r\n                                <i class=\"fa fa-edit\"></i>\r\n                            </a>\r\n                            <a href=\"javascript:;\" class=\"btn btn-circle red-sunglo\"\r\n                               *ngIf=\"permission.delete\"\r\n                               [swal]=\"confirmDeleteOffice\" (confirm)=\"deleteOffice()\">\r\n                                <i class=\"fa fa-trash-o\"></i>\r\n                            </a>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"portlet-body\">\r\n                        <div class=\"form-horizontal\">\r\n                            <div class=\"tabbable-custom\">\r\n                                <ul class=\"nav nav-tabs \">\r\n                                    <li [class.active]=\"viewType === 0\">\r\n                                        <a href=\"javascript://\" (click)=\"changeViewType(0)\" i18n=\"@@officeInfo\">\r\n                                            Office info\r\n                                        </a>\r\n                                    </li>\r\n                                    <li [class.active]=\"viewType === 1\">\r\n                                        <a href=\"javascript://\" (click)=\"changeViewType(1)\" i18n=\"@@listPosition\">\r\n                                            List positions\r\n                                        </a>\r\n                                    </li>\r\n                                </ul>\r\n\r\n                                <div class=\"tab-content\">\r\n                                    <div class=\"tab-pane active\">\r\n                                        <ng-container *ngIf=\"viewType ===  0; else listPositionTemplate\">\r\n                                            <div class=\"form-group\">\r\n                                                <label i18n-ghmLabel=\"@@parentOffice\" ghmLabel=\"Parent office\"\r\n                                                       class=\"col-sm-3 control-label\"></label>\r\n                                                <div class=\"col-sm-3\">\r\n                                                    <div class=\"form-control\">{{ officeDetail?.parentName }}</div>\r\n                                                </div>\r\n                                                <label i18n-ghmLabel=\"@@officeCode\" ghmLabel=\"Office code\"\r\n                                                       class=\"col-sm-3 control-label\"></label>\r\n                                                <div class=\"col-sm-3\">\r\n                                                    <div class=\"form-control\">{{ officeDetail?.code }}</div>\r\n                                                </div>\r\n                                            </div>\r\n                                            <div class=\"form-group\">\r\n                                                <label i18n-ghmLabel=\"@@officeName\" ghmLabel=\"Office name\"\r\n                                                       class=\"col-sm-3 control-label\"></label>\r\n                                                <div class=\"col-sm-3\">\r\n                                                    <div class=\"form-control\">{{ officeDetail?.name }}</div>\r\n                                                </div>\r\n                                                <label i18n-ghmLabel=\"@@shortName\" ghmLabel=\"Short name\"\r\n                                                       class=\"col-sm-3 control-label\"></label>\r\n                                                <div class=\"col-sm-3\">\r\n                                                    <div class=\"form-control\">{{ officeDetail?.shortName }}</div>\r\n                                                </div>\r\n                                            </div>\r\n                                            <div class=\"form-group\">\r\n                                                <label i18n-ghmLabel=\"@@officeType\" ghmLabel=\"Office type\"\r\n                                                       class=\"col-sm-3 control-label\"></label>\r\n                                                <div class=\"col-sm-3\">\r\n                                                    <div class=\"form-control\" i18n=\"@@officeType\">\r\n\r\n                                                    </div>\r\n                                                </div>\r\n                                                <label i18n-ghmLabel=\"@@status\" ghmLabel=\"Status\"\r\n                                                       class=\"col-sm-3 control-label\"></label>\r\n                                                <div class=\"col-sm-3\">\r\n                                                <span class=\"badge cm-mgt-10\"\r\n                                                      [class.badge-success]=\"officeDetail?.isActive\"\r\n                                                      [class.badge-danger]=\"!officeDetail?.isActive\"\r\n                                                >\r\n                                                    {officeDetail?.isActive, select, 0 {InActivated} 1 {Activated} other {}}\r\n                                                </span>\r\n                                                </div>\r\n                                            </div>\r\n                                            <div class=\"form-group\">\r\n                                                <label i18n=\"@@description\" i18n-ghmLabel ghmLabel=\"Description\"\r\n                                                       class=\"col-sm-3 control-label\"></label>\r\n                                                <div class=\"col-sm-9\">\r\n                                                    <div class=\"form-control height-auto\">{{ officeDetail?.description\r\n                                                        }}\r\n                                                    </div>\r\n                                                </div>\r\n                                            </div>\r\n                                            <div class=\"form-group\">\r\n                                                <label i18n=\"@@address\" i18n-ghmLabel ghmLabel=\"Address\"\r\n                                                       class=\"col-sm-3 control-label\"></label>\r\n                                                <div class=\"col-sm-9\">\r\n                                                    <div class=\"form-control height-auto\">{{ officeDetail?.address }}\r\n                                                    </div>\r\n                                                </div>\r\n                                            </div>\r\n                                            <div class=\"form-group\">\r\n                                                <div class=\"col-sm-12\">\r\n                                                    <app-office-contact\r\n                                                        #detailOfficeContact\r\n                                                        [officeId]=\"officeDetail?.id\"\r\n                                                        [officeContacts]=\"officeDetail?.officeContacts\">\r\n                                                    </app-office-contact>\r\n                                                </div>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                    </div>\r\n                                    <!-- END: .tab-pane -->\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <!-- END: .col-sm-8 -->\r\n        </div>\r\n    </nh-modal-content>\r\n    <nh-modal-footer>\r\n        <ghm-button classes=\"btn btn-default\"\r\n                    nh-dismiss=\"true\"\r\n                    type=\"button\">\r\n            <span i18n=\"@@close\">Close</span>\r\n        </ghm-button>\r\n    </nh-modal-footer>\r\n</nh-modal>\r\n\r\n<nh-modal #addPositionModal size=\"sm\"\r\n          [backdropStatic]=\"true\"\r\n          (onShown)=\"onAddPositionModalShown()\">\r\n    <nh-modal-header>\r\n        <span i18n=\"@@addPosition\">Add position</span>\r\n    </nh-modal-header>\r\n    <nh-modal-content style=\"min-height: 250px;\">\r\n        <nh-suggestion\r\n            i18n-placeholder=\"@@typePositionNameForSearch\"\r\n            placeholder=\"Type position name for search\"\r\n            [multiple]=\"true\"\r\n            [sources]=\"positions\"\r\n            [selectedItems]=\"selectedPositions\"\r\n            [loading]=\"isSearchingPositions\"\r\n            (searched)=\"searchPositionForSuggestion($event)\"\r\n        ></nh-suggestion>\r\n    </nh-modal-content>\r\n    <nh-modal-footer>\r\n        <button type=\"button\" class=\"btn blue\" i18n=\"@@accept\" (click)=\"acceptAddPosition()\">\r\n            Accept\r\n        </button>\r\n    </nh-modal-footer>\r\n</nh-modal>\r\n\r\n<ng-template #listPositionTemplate>\r\n    <form class=\"form-inline\" (ngSubmit)=\"searchPosition(1)\">\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"form-group cm-mgr-5\">\r\n                <div class=\"input-group\">\r\n                    <input type=\"text\" class=\"form-control\"\r\n                           i18n-placeholder=\"@@enterKeyword\"\r\n                           placeholder=\"Enter keyword\"\r\n                           name=\"searchPositionKeyword\"\r\n                           [(ngModel)]=\"keyword\">\r\n                    <span class=\"input-group-btn\">\r\n                        <button class=\"btn blue\" type=\"submit\">\r\n                            <i class=\"fa fa-spinner fa-pulse\" *ngIf=\"isSearching\"></i>\r\n                            <i class=\"fa fa-search\" *ngIf=\"!isSearching\"></i>\r\n                        </button>\r\n                    </span>\r\n                </div>\r\n            </div>\r\n            <!--<div class=\"form-group\">-->\r\n                <!--<ghm-logic-->\r\n                    <!--*ngIf=\"permission.delete\"-->\r\n                    <!--icon=\"fa fa-search\"-->\r\n                    <!--classes=\"btn blue\"-->\r\n                    <!--[loading]=\"isSearching\"-->\r\n                <!--&gt;-->\r\n                <!--</ghm-logic>-->\r\n            <!--</div>-->\r\n            <div class=\"form-group pull-right\" *ngIf=\"officePermission.add\">\r\n                <button type=\"button\" class=\"btn blue\" i18n=\"@@add\" (click)=\"showAddPositionModal()\">\r\n                    Add\r\n                </button>\r\n            </div>\r\n        </div>\r\n    </form>\r\n    <table class=\"table table-stripped table-hover\">\r\n        <thead>\r\n        <tr>\r\n            <th class=\"center w50\" i18n=\"@@no\">No</th>\r\n            <th i18n=\"@@positionName\">Position Name</th>\r\n            <th class=\"center w100\" i18n=\"@@isManager\">Is Manager</th>\r\n            <th class=\"center w100\" i18n=\"@@isMultiple\">Is Multiple</th>\r\n            <th class=\"w70 center\" i18n=\"@@actions\">Actions</th>\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n        <tr *ngFor=\"let item of listItems; let i = index\">\r\n            <td class=\"center\">{{ (currentPage - 1) * pageSize + i + 1 }}</td>\r\n            <td>{{ item.positionName }}</td>\r\n            <td class=\"center\">\r\n                <i class=\"fa fa-check color-green\" *ngIf=\"item.isManager\"></i>\r\n            </td>\r\n            <td class=\"center\">\r\n                <i class=\"fa fa-check color-green\" *ngIf=\"item.isMultiple\"></i>\r\n            </td>\r\n            <td class=\"center\">\r\n                <ghm-button\r\n                    *ngIf=\"permission.delete\"\r\n                    icon=\"fa fa-trash-o\" classes=\"btn red btn-sm\"\r\n                    [swal]=\"confirmDeleteOfficePosition\"\r\n                    (confirm)=\"deletePosition(item.positionId)\"></ghm-button>\r\n            </td>\r\n        </tr>\r\n        </tbody>\r\n    </table>\r\n    <ghm-paging\r\n        i18n-pageName=\"@@position\"\r\n        pageName=\"Position\"\r\n        [totalRows]=\"totalRows\"\r\n        [currentPage]=\"currentPage\"\r\n        [pageShow]=\"6\"\r\n        [isDisabled]=\"isSearching\"\r\n        (pageClick)=\"searchPosition($event)\"></ghm-paging>\r\n</ng-template>\r\n\r\n<swal\r\n    #confirmDeleteOfficePosition\r\n    i18n-title=\"@@confirmDeleteOfficePositionTitle\"\r\n    i18n-text=\"@@confirmDeleteOfficePositionText\"\r\n    title=\"Are you sure want to remove this position out of office.\"\r\n    text=\"You can't recover after delete.\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Accept\"\r\n    cancelButtonText=\"Cancel\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n\r\n<swal\r\n    #confirmDeleteOffice\r\n    i18n-title=\"@@confirmDeleteOfficeTitle\"\r\n    i18n-text=\"@@confirmDeleteOfficeText\"\r\n    title=\"Are you sure want to delete this office.\"\r\n    text=\"You can't recover this office after delete.\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Accept\"\r\n    cancelButtonText=\"Cancel\"\r\n    [showCancelButton]=\"true\"\r\n    [focusCancel]=\"true\">\r\n</swal>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/organization/office/office-detail/office-detail.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/office-detail/office-detail.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: OfficeDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfficeDetailComponent", function() { return OfficeDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_office_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/office.service */ "./src/app/modules/hr/organization/office/services/office.service.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _services_office_position_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/office-position.service */ "./src/app/modules/hr/organization/office/services/office-position.service.ts");
/* harmony import */ var _position_position_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../position/position.service */ "./src/app/modules/hr/organization/position/position.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");










var OfficeDetailComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](OfficeDetailComponent, _super);
    function OfficeDetailComponent(location, router, positionService, officePositionService, officeService) {
        var _this = _super.call(this) || this;
        _this.location = location;
        _this.router = router;
        _this.positionService = positionService;
        _this.officePositionService = officePositionService;
        _this.officeService = officeService;
        _this.officeDeleted = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.edited = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.officeTree = [];
        _this.subscribers = {};
        _this.viewType = 0;
        _this.positions = [];
        _this.selectedPositions = [];
        _this.isSearchingPositions = false;
        return _this;
    }
    OfficeDetailComponent.prototype.ngOnInit = function () {
        this.officePermission = this.appService.getPermissionByPageId(this.pageId.OFFICE);
    };
    OfficeDetailComponent.prototype.onModalShown = function () {
        var _this = this;
        this.subscribers.getTree = this.officeService.getTree()
            .subscribe(function (result) { return _this.officeTree = result; });
        this.viewType = 0;
    };
    OfficeDetailComponent.prototype.onModalHidden = function () {
        this.location.go('/organization/offices');
    };
    OfficeDetailComponent.prototype.onNodeSelected = function (node) {
        this.selectedOfficeId = node.id;
        this.getDataByViewType();
    };
    OfficeDetailComponent.prototype.edit = function () {
        this.officeDetailModal.dismiss();
        this.edited.emit(this.officeDetail.id);
    };
    OfficeDetailComponent.prototype.deleteOffice = function () {
        var _this = this;
        this.officeService.delete(this.officeDetail.id)
            .subscribe(function () {
            _this.officeDeleted.emit();
        });
    };
    OfficeDetailComponent.prototype.changeViewType = function (viewType) {
        if (this.viewType === viewType) {
            return;
        }
        if (viewType === 1) {
            this.keyword = '';
        }
        this.viewType = viewType;
        this.getDataByViewType();
    };
    OfficeDetailComponent.prototype.showDetail = function (officeId) {
        this.selectedOfficeId = officeId;
        this.getDetail();
        this.officeDetailModal.open();
    };
    OfficeDetailComponent.prototype.closeModal = function () {
        this.officeDetailModal.dismiss();
    };
    OfficeDetailComponent.prototype.getDetail = function () {
        var _this = this;
        this.subscribers.getDetail = this.officeService.getDetail(this.selectedOfficeId)
            .subscribe(function (result) {
            _this.officeDetail = result.data;
        });
    };
    OfficeDetailComponent.prototype.searchPosition = function (currentPage) {
        var _this = this;
        this.currentPage = currentPage;
        this.subscribers.searchPositions = this.officePositionService
            .search(this.keyword, this.selectedOfficeId, this.currentPage, this.pageSize)
            .subscribe(function (result) {
            _this.totalRows = result.totalRows;
            _this.listItems = result.items;
        });
    };
    OfficeDetailComponent.prototype.deletePosition = function (positionId) {
        var _this = this;
        this.subscribers.deletePosition = this.officePositionService.delete(positionId, this.officeDetail.id)
            .subscribe(function () { return _this.searchPosition(_this.currentPage); });
    };
    OfficeDetailComponent.prototype.searchPositionForSuggestion = function (keyword) {
        var _this = this;
        this.isSearchingPositions = true;
        this.subscribers.searchForSuggestion =
            this.positionService.searchForSuggestion(keyword)
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return _this.isSearchingPositions = false; }))
                .subscribe(function (result) { return _this.positions = result; });
    };
    OfficeDetailComponent.prototype.showAddPositionModal = function () {
        this.addPositionModal.open();
    };
    OfficeDetailComponent.prototype.acceptAddPosition = function () {
        var _this = this;
        this.officePositionService
            .insert(this.selectedOfficeId, this.selectedPositions.map(function (item) {
            return item.id;
        }))
            .subscribe(function () {
            _this.addPositionModal.dismiss();
            _this.searchPosition(1);
        });
    };
    OfficeDetailComponent.prototype.onAddPositionModalShown = function () {
        this.selectedPositions = [];
    };
    OfficeDetailComponent.prototype.getDataByViewType = function () {
        switch (this.viewType) {
            case 0:
                this.getDetail();
                break;
            case 1:
                this.searchPosition(1);
                break;
            default:
                this.getDetail();
                break;
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('officeDetailModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__["NhModalComponent"])
    ], OfficeDetailComponent.prototype, "officeDetailModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('addPositionModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_3__["NhModalComponent"])
    ], OfficeDetailComponent.prototype, "addPositionModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], OfficeDetailComponent.prototype, "officeDeleted", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], OfficeDetailComponent.prototype, "edited", void 0);
    OfficeDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-office-detail',
            template: __webpack_require__(/*! ./office-detail.component.html */ "./src/app/modules/hr/organization/office/office-detail/office-detail.component.html"),
            providers: [
                _services_office_position_service__WEBPACK_IMPORTED_MODULE_5__["OfficePositionService"],
                _angular_common__WEBPACK_IMPORTED_MODULE_8__["Location"], { provide: _angular_common__WEBPACK_IMPORTED_MODULE_8__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_8__["PathLocationStrategy"] }
            ]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_8__["Location"],
            _angular_router__WEBPACK_IMPORTED_MODULE_9__["Router"],
            _position_position_service__WEBPACK_IMPORTED_MODULE_6__["PositionService"],
            _services_office_position_service__WEBPACK_IMPORTED_MODULE_5__["OfficePositionService"],
            _services_office_service__WEBPACK_IMPORTED_MODULE_2__["OfficeService"]])
    ], OfficeDetailComponent);
    return OfficeDetailComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_4__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/organization/office/office-form/office-form.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/office-form/office-form.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #officeFormModal size=\"full\"\r\n          (onShown)=\"onModalShow()\"\r\n          (onHidden)=\"onModalHidden()\">\r\n    <nh-modal-header>\r\n        {isUpdate, select, 0 {Thêm mới phòng ban} 1 {Cập nhật phòng ban} other {}}\r\n    </nh-modal-header>\r\n    <form class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-4\">\r\n                    <div class=\"portlet light bordered\">\r\n                        <div class=\"portlet-title\">\r\n                            <div class=\"caption font-green-sharp\">\r\n                                <i class=\"icon-speech font-green-sharp\"></i>\r\n                                <span class=\"caption-subject font-blue-madison bold uppercase\"\r\n                                      i18n=\"@@officeOrganizationTitle\">Sơ đồ tổ chức</span>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"portlet-body\">\r\n                            <nh-tree [data]=\"officeTree\">\r\n                            </nh-tree>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <!-- END: .col-sm-4 -->\r\n                <div class=\"col-sm-8\">\r\n                    <div class=\"portlet light bordered\">\r\n                        <div class=\"portlet-title\">\r\n                            <div class=\"caption font-green-sharp\">\r\n                                <i class=\"icon-speech font-green-sharp\"></i>\r\n                                <span class=\"caption-subject font-blue-madison bold uppercase\"\r\n                                      i18n=\"@@officeInfo\">\r\n                                    Thông tin phòng ban\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"portlet-body form\">\r\n                            <div class=\"form-body\">\r\n                                <div formArrayName=\"translations\">\r\n                                    <div class=\"form-group\" *ngIf=\"languages && languages.length > 1\">\r\n                                        <label i18n-ghmLabel=\"@@language\" ghmLabel=\"Language\"\r\n                                               class=\"col-sm-4 control-label\"></label>\r\n                                        <div class=\"col-sm-8\">\r\n                                            <nh-select [data]=\"languages\"\r\n                                                       i18n-title=\"@@pleaseSelectLanguage\"\r\n                                                       title=\"-- Chọn ngôn ngữ --\"\r\n                                                       name=\"language\"\r\n                                                       [(value)]=\"currentLanguage\"\r\n                                                       (onSelectItem)=\"currentLanguage = $event.id\"></nh-select>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"form-group\">\r\n                                        <label i18n-ghmLabel=\"@@parentOffice\" ghmLabel=\"Phòng ban cấp trên\"\r\n                                               class=\"col-sm-3 control-label\"></label>\r\n                                        <div class=\"col-sm-3\" [formGroup]=\"model\">\r\n                                            <nh-dropdown-tree\r\n                                                [data]=\"officeTree\" i18n-title=\"@@selectParentOffice\"\r\n                                                title=\"-- Chọn phòng ban cấp trên --\"\r\n                                                formControlName=\"parentId\">\r\n                                            </nh-dropdown-tree>\r\n                                        </div>\r\n                                        <span\r\n                                            [class.has-error]=\"formErrors?.code\">\r\n                                                <label i18n-ghmLabel=\"@@officeCode\" ghmLabel=\"Mã phòng ban\"\r\n                                                       class=\"col-sm-3 control-label\" [required]=\"true\"></label>\r\n                                                <div class=\"col-sm-3\" [formGroup]=\"model\">\r\n                                                    <input type=\"text\" class=\"form-control\"\r\n                                                           i18n-placeholder=\"@@enterOfficeCodePlaceHolder\"\r\n                                                           placeholder=\"Nhập mã phòng ban.\"\r\n                                                           formControlName=\"code\">\r\n                                                    <span class=\"help-block\">\r\n                                                        {\r\n                                                            formErrors?.code, select,\r\n                                                            required {Mã phòng ban không được để trống}\r\n                                                            maxLength {Mã phòng ban không được vượt quá 50 ký tự}\r\n                                                            pattern {Mã phòng ban chỉ chứa các ký tự a-z, A-Z, 0-9}\r\n                                                        }\r\n                                                    </span>\r\n                                                </div>\r\n                                            </span>\r\n                                    </div>\r\n                                    <div class=\"form-group\"\r\n                                         *ngFor=\"let modelTranslation of translations.controls; index as i\"\r\n                                         [hidden]=\"modelTranslation.value.languageId !== currentLanguage\"\r\n                                         [formGroupName]=\"i\">\r\n                                            <span\r\n                                                [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.name\">\r\n                                                <label i18n-ghmLabel=\"@@officeName\" ghmLabel=\"Tên phòng ban\"\r\n                                                       class=\"col-sm-3 control-label\" [required]=\"true\"></label>\r\n                                                <div class=\"col-sm-3\">\r\n                                                    <input type=\"text\" class=\"form-control\"\r\n                                                           i18n-placeholder=\"@@enterOfficeNamePlaceHolder\"\r\n                                                           placeholder=\"Enter office name.\"\r\n                                                           formControlName=\"name\">\r\n                                                    <span class=\"help-block\">\r\n                                                        { translationFormErrors[modelTranslation.value.languageId]?.name,\r\n                                                        select, required {Tên phòng ban không được để trống}\r\n                                                        maxLength {Tên phòng ban không được vượt quá 256 ký tự} }\r\n                                                    </span>\r\n                                                </div>\r\n                                            </span>\r\n                                        <span\r\n                                            [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.shortName\">\r\n                                                <label i18n-ghmLabel=\"@@shortName\" ghmLabel=\"Tên phòng ban viết tắt\"\r\n                                                       class=\"col-sm-3 control-label\" [required]=\"true\"></label>\r\n                                                <div class=\"col-sm-3\">\r\n                                                    <input type=\"text\" class=\"form-control\"\r\n                                                           i18n-placeholder=\"@@enterShortNamePlaceHolder\"\r\n                                                           placeholder=\"Tên phòng ban viết tắt.\"\r\n                                                           formControlName=\"shortName\">\r\n                                                    <span class=\"help-block\">\r\n                                                        {translationFormErrors[modelTranslation.value.languageId]?.shortName,\r\n                                                         select, required {Tên phòng ban viết tắt không được để trống} maxLength\r\n                                                        {Tên phòng ban viết tắt không được vượt quá 256 ký tự} }\r\n                                                    </span>\r\n                                                </div>\r\n                                            </span>\r\n                                    </div>\r\n                                    <div class=\"form-group\" [formGroup]=\"model\">\r\n                                        <label i18n-ghmLabel=\"@@officeType\" ghmLabel=\"Loại phòng ban\"\r\n                                               class=\"col-sm-3 control-label\" [required]=\"true\"></label>\r\n                                        <div class=\"col-sm-3\">\r\n                                            <nh-select [data]=\"officeTypes\" i18n-title=\"@@selectOfficeType\"\r\n                                                       title=\"-- Chọn loại phòng ban --\"\r\n                                                       formControlName=\"officeType\"></nh-select>\r\n                                            <span class=\"help-block\">\r\n                                                    { formErrors?.officeType, select, required {Vui lòng chọn loại phòng ban}}\r\n                                                </span>\r\n                                        </div>\r\n                                        <label i18n-ghmLabel=\"@@status\" ghmLabel=\"Trạng thái\"\r\n                                               class=\"col-sm-3 control-label\" [required]=\"true\"></label>\r\n                                        <div class=\"col-sm-3\">\r\n                                            <mat-slide-toggle color=\"primary\" formControlName=\"isActive\"\r\n                                                              i18n=\"@@isActive\"\r\n                                                              class=\"cm-mgt-5\">\r\n                                                {model.value.isActive, select, 0 {Chưa kích hoạt} 1 {Kích hoạt}}\r\n                                            </mat-slide-toggle>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"form-group\"\r\n                                         [hidden]=\"modelTranslation.value.languageId !== currentLanguage\"\r\n                                         *ngFor=\"let modelTranslation of translations.controls; index as i\"\r\n                                         [formGroupName]=\"i\"\r\n                                         [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.description\">\r\n                                        <label i18n=\"@@description\" i18n-ghmLabel ghmLabel=\"Mô tả\"\r\n                                               class=\"col-sm-3 control-label\"></label>\r\n                                        <div class=\"col-sm-9\">\r\n                                                <textarea class=\"form-control\" rows=\"3\" formControlName=\"description\"\r\n                                                          i18n=\"@@enterDescriptionPlaceholder\" i18n-placeholder\r\n                                                          placeholder=\"Enter description.\"></textarea>\r\n                                            <span class=\"help-block\">\r\n                                                    { translationFormErrors[modelTranslation.value.languageId]?.description,\r\n                                                    select, maxLength {Mô tả phòng ban không được vượt quá 256 ký tự} }\r\n                                                </span>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"form-group\"\r\n                                         [hidden]=\"modelTranslation.value.languageId !== currentLanguage\"\r\n                                         *ngFor=\"let modelTranslation of translations.controls; index as i\"\r\n                                         [formGroupName]=\"i\"\r\n                                         [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.address\">\r\n                                        <label i18n=\"@@address\" i18n-ghmLabel ghmLabel=\"Địa chỉ\"\r\n                                               class=\"col-sm-3 control-label\"></label>\r\n                                        <div class=\"col-sm-9\">\r\n                                                <textarea class=\"form-control\" rows=\"3\" formControlName=\"address\"\r\n                                                          i18n=\"@@enterAddressPlaceholder\" i18n-placeholder\r\n                                                          placeholder=\"Nhập địa chỉ.\"></textarea>\r\n                                            <span class=\"help-block\">\r\n                                                    {translationFormErrors[modelTranslation.value.languageId]?.description,\r\n                                                    select, maxLength {Địa chỉ phòng ban không được vượt quá 256 ký tự}}\r\n                                                </span>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"form-group\">\r\n                                        <div class=\"col-sm-12\">\r\n                                            <app-office-contact\r\n                                                #officeFormContact\r\n                                                [officeId]=\"id\"\r\n                                                [officeContacts]=\"contacts\">\r\n                                            </app-office-contact>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <!-- END: .col-sm-8 -->\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <mat-checkbox [checked]=\"isCreateAnother\" (change)=\"isCreateAnother = !isCreateAnother\"\r\n                          *ngIf=\"!isUpdate\"\r\n                          i18n=\"@@isCreateAnother\"\r\n                          class=\"cm-mgr-5\"\r\n                          color=\"primary\">\r\n                Tiếp tục thêm\r\n            </mat-checkbox>\r\n            <ghm-button classes=\"btn blue cm-mgr-5\"\r\n                        [loading]=\"isSaving\">\r\n                <span i18n=\"@@Save\">Lưu</span>\r\n            </ghm-button>\r\n            <ghm-button classes=\"btn btn-default\"\r\n                        nh-dismiss=\"true\"\r\n                        [type]=\"'button'\"\r\n                        [loading]=\"isSaving\">\r\n                <span i18n=\"@@close\">Đóng</span>\r\n            </ghm-button>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/organization/office/office-form/office-form.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/office-form/office-form.component.ts ***!
  \*************************************************************************************/
/*! exports provided: OfficeFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfficeFormComponent", function() { return OfficeFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _office_title_office_title_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../office-title/office-title.component */ "./src/app/modules/hr/organization/office/office-title/office-title.component.ts");
/* harmony import */ var _services_office_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/office.service */ "./src/app/modules/hr/organization/office/services/office.service.ts");
/* harmony import */ var _models_office_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../models/office.model */ "./src/app/modules/hr/organization/office/models/office.model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _configs_page_id_config__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../configs/page-id.config */ "./src/app/configs/page-id.config.ts");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _models_office_translation_model__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../models/office-translation.model */ "./src/app/modules/hr/organization/office/models/office-translation.model.ts");
/* harmony import */ var _models_office_contact_model__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../models/office-contact.model */ "./src/app/modules/hr/organization/office/models/office-contact.model.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");

















var OfficeFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](OfficeFormComponent, _super);
    function OfficeFormComponent(pageId, location, fb, officeService, toastr, spinnerService, utilService) {
        var _this = _super.call(this) || this;
        _this.location = location;
        _this.fb = fb;
        _this.officeService = officeService;
        _this.toastr = toastr;
        _this.spinnerService = spinnerService;
        _this.utilService = utilService;
        _this.onEditorKeyup = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.onCloseForm = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.office = new _models_office_model__WEBPACK_IMPORTED_MODULE_6__["Office"]();
        _this.contact = new _models_office_contact_model__WEBPACK_IMPORTED_MODULE_14__["OfficeContact"]();
        _this.officeTree = [];
        _this.modelTranslation = new _models_office_translation_model__WEBPACK_IMPORTED_MODULE_13__["OfficeTranslation"]();
        _this.contactFormErrors = {};
        _this.contactValidationMessages = {};
        _this.isGettingTree = false;
        _this.officeTypes = [];
        _this.contacts = [];
        _this.buildFormLanguage = function (language) {
            _this.translationFormErrors[language] = _this.utilService.renderFormError(['name', 'shortName', 'address', 'description']);
            _this.translationValidationMessage[language] = _this.utilService.renderFormErrorMessage([
                { name: ['required', 'maxlength'] },
                { description: ['maxlength'] },
                { address: ['maxlength'] },
                { shortName: ['required', 'maxlength'] }
            ]);
            var translationModel = _this.fb.group({
                languageId: [language],
                name: [
                    _this.modelTranslation.name,
                    [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(256)]
                ],
                shortName: [
                    _this.modelTranslation.shortName,
                    [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)]
                ],
                address: [
                    _this.modelTranslation.address,
                    [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(500)]
                ],
                description: [
                    _this.modelTranslation.description,
                    [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(500)]
                ]
            });
            translationModel.valueChanges.subscribe(function (data) {
                return _this.validateTranslation(false);
            });
            return translationModel;
        };
        _this.renderOfficeType();
        return _this;
    }
    OfficeFormComponent.prototype.ngOnInit = function () {
        this.office = new _models_office_model__WEBPACK_IMPORTED_MODULE_6__["Office"]();
        this.renderForm();
        this.getOfficeTree();
    };
    OfficeFormComponent.prototype.onModalShow = function () {
        this.isModified = false;
    };
    OfficeFormComponent.prototype.onModalHidden = function () {
        this.isUpdate = false;
        this.resetForm();
        this.location.go('/organization/offices');
        if (this.isModified) {
            this.saveSuccessful.emit();
        }
    };
    OfficeFormComponent.prototype.add = function () {
        this.getOfficeTree();
        this.validateModel(false);
        this.officeFormModal.open();
    };
    OfficeFormComponent.prototype.edit = function (id) {
        this.isUpdate = true;
        this.id = id;
        this.getDetail(id);
        this.officeFormModal.open();
    };
    OfficeFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.validateModel();
        var isLanguageValid = this.validateLanguage();
        if (isValid && isLanguageValid) {
            this.office = this.model.value;
            this.office.officeContacts = this.contacts;
            this.isSaving = true;
            if (this.isUpdate) {
                this.officeService
                    .update(this.id, this.office)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return (_this.isSaving = false); }))
                    .subscribe(function () {
                    _this.isModified = true;
                    _this.officeFormModal.dismiss();
                });
            }
            else {
                this.officeService
                    .insert(this.office)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () { return (_this.isSaving = false); }))
                    .subscribe(function () {
                    _this.isModified = true;
                    if (_this.isCreateAnother) {
                        _this.getOfficeTree();
                        _this.resetForm();
                    }
                    else {
                        _this.officeFormModal.dismiss();
                    }
                });
            }
        }
    };
    // saveContact() {
    //     const isValid = this.validateFormGroup(
    //         this.contactModel,
    //         this.contactFormErrors,
    //         this.contactValidationMessages,
    //         true
    //     );
    //     if (isValid) {
    //         this.contact = this.contactModel.value;
    //         const existsByUserId = _.countBy(
    //             this.contacts,
    //             (contact: OfficeContact) => {
    //                 return contact.userId === this.contact.userId;
    //             }
    //         ).true;
    //         if (existsByUserId && existsByUserId > 0) {
    //             return;
    //         }
    //         if (this.isUpdateContact) {
    //             this.updateContact();
    //         } else {
    //             this.addContact();
    //         }
    //     }
    // }
    OfficeFormComponent.prototype.closeForm = function () {
        this.onCloseForm.emit();
    };
    OfficeFormComponent.prototype.reloadTree = function () {
        var _this = this;
        this.isGettingTree = true;
        this.officeService.getTree().subscribe(function (result) {
            _this.isGettingTree = false;
            _this.officeTree = result;
        });
    };
    OfficeFormComponent.prototype.onParentSelect = function (office) {
        this.model.patchValue({ parentId: office ? office.id : null });
    };
    OfficeFormComponent.prototype.showListTitleTab = function () {
        this.officeTitleComponent.search(1);
    };
    OfficeFormComponent.prototype.getDetail = function (id) {
        var _this = this;
        this.subscribers.officeDetail = this.officeService
            .getEditDetail(id)
            .subscribe(function (result) {
            var officeDetail = result.data;
            if (officeDetail) {
                _this.model.patchValue({
                    code: officeDetail.code,
                    isActive: officeDetail.isActive,
                    order: officeDetail.order,
                    officeType: officeDetail.officeType,
                    parentId: officeDetail.parentId
                });
                if (officeDetail.officeTranslations && officeDetail.officeTranslations.length > 0) {
                    _this.translations.controls.forEach(function (model) {
                        var detail = lodash__WEBPACK_IMPORTED_MODULE_15__["find"](officeDetail.officeTranslations, function (officeTranslation) {
                            return (officeTranslation.languageId ===
                                model.value.languageId);
                        });
                        if (detail) {
                            model.patchValue(detail);
                        }
                    });
                }
                if (officeDetail.officeContacts &&
                    officeDetail.officeContacts.length > 0) {
                    _this.contacts = officeDetail.officeContacts;
                }
            }
        });
    };
    OfficeFormComponent.prototype.getOfficeTree = function () {
        var _this = this;
        this.subscribers.getTree = this.officeService
            .getTree()
            .subscribe(function (result) {
            _this.officeTree = result;
        });
    };
    OfficeFormComponent.prototype.renderForm = function () {
        this.buildForm();
        this.buildContactForm();
        this.renderTranslationArray(this.buildFormLanguage);
    };
    OfficeFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.utilService.renderFormError([
            'name',
            'description',
            'code'
        ]);
        this.validationMessages = this.utilService.renderFormErrorMessage([
            { officeType: ['required'] },
            { code: ['required', 'maxlength', 'pattern'] }
        ]);
        this.model = this.fb.group({
            officeType: [this.office.officeType],
            // 'email': [this.office.email, [
            //     Validators.maxLength(150),
            //     Validators.pattern(
            //         '(([^<>()\[\]\.,;:\s@"]+(\.[^<>()\[\]\.,;:\s@"]+)*)|(".+"))@(([^<>()[\]\.,;:\s@"]+\.)+[^<>()[\]\.,;:\s@"]{2,})'
            //     )
            // ]],
            code: [this.office.code, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[a-zA-Z0-9]+$')
                ]],
            order: [this.office.order],
            parentId: [this.office.parentId],
            isActive: [this.office.isActive],
            translations: this.fb.array([])
        });
        this.model.valueChanges.subscribe(function (data) { return _this.validateModel(false); });
    };
    OfficeFormComponent.prototype.resetForm = function () {
        this.id = null;
        this.model.patchValue({
            officeType: 0,
            order: 0,
            code: 0,
            parentId: null,
            isActive: true
        });
        this.translations.controls.forEach(function (model) {
            model.patchValue({
                name: '',
                shortName: '',
                description: '',
                address: ''
            });
        });
        this.contacts = [];
        this.clearFormError(this.formErrors);
        this.clearFormError(this.translationFormErrors);
    };
    OfficeFormComponent.prototype.renderOfficeType = function () {
        this.officeTypes = [
            { id: 0, name: 'Thường' },
            { id: 1, name: 'Nhân sự' },
            { id: 2, name: 'Ban giám đốc' },
            { id: 3, name: 'Công ty độc lập' }
        ];
    };
    OfficeFormComponent.prototype.buildContactForm = function () {
        var _this = this;
        this.contactFormErrors = this.utilService.renderFormError(['userId']);
        this.contactValidationMessages = this.utilService.renderFormErrorMessage([
            { userId: ['required'] },
            { phoneNumber: ['maxlength'] },
            { email: ['maxlength'] },
            { fax: ['maxlength'] }
        ]);
        this.contactModel = this.fb.group({
            userId: [this.contact.userId],
            fullName: [this.contact.fullName],
            avatar: [this.contact.avatar],
            phoneNumber: [this.contact.phoneNumber, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)]],
            email: [this.contact.email, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)]],
            fax: [this.contact.fax, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)]]
        });
        this.contactModel.valueChanges.subscribe(function () {
            return _this.validateFormGroup(_this.contactModel, _this.contactFormErrors, _this.contactValidationMessages, false);
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('officeFormModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_12__["NhModalComponent"])
    ], OfficeFormComponent.prototype, "officeFormModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_office_title_office_title_component__WEBPACK_IMPORTED_MODULE_4__["OfficeTitleComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _office_title_office_title_component__WEBPACK_IMPORTED_MODULE_4__["OfficeTitleComponent"])
    ], OfficeFormComponent.prototype, "officeTitleComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], OfficeFormComponent.prototype, "elementId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], OfficeFormComponent.prototype, "onEditorKeyup", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], OfficeFormComponent.prototype, "onCloseForm", void 0);
    OfficeFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-office-form',
            template: __webpack_require__(/*! ./office-form.component.html */ "./src/app/modules/hr/organization/office/office-form/office-form.component.html"),
            providers: [
                _services_office_service__WEBPACK_IMPORTED_MODULE_5__["OfficeService"],
                _angular_common__WEBPACK_IMPORTED_MODULE_16__["Location"], { provide: _angular_common__WEBPACK_IMPORTED_MODULE_16__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_16__["PathLocationStrategy"] }
            ]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_page_id_config__WEBPACK_IMPORTED_MODULE_9__["PAGE_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_common__WEBPACK_IMPORTED_MODULE_16__["Location"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _services_office_service__WEBPACK_IMPORTED_MODULE_5__["OfficeService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_10__["SpinnerService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_11__["UtilService"]])
    ], OfficeFormComponent);
    return OfficeFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_8__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/organization/office/office-title/office-title.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/office-title/office-title.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-sm-12 cm-mgb-10\">\r\n        <form class=\"form-inline\">\r\n            <div class=\"form-group\">\r\n                <!-- TODO: Check this -->\r\n                <!--<nh-suggestion-->\r\n                    <!--[url]=\"'office-title/search'\"-->\r\n                    <!--[clearAfterSelect]=\"true\"-->\r\n                    <!--[material]=\"false\"-->\r\n                    <!--[placeholder]=\"'Nhập tên chức danh cần tìm'\"-->\r\n                    <!--(onSelectItem)=\"onSelectTitle($event)\"-->\r\n                <!--&gt;</nh-suggestion>-->\r\n            </div>\r\n        </form>\r\n    </div>\r\n    <div class=\"col-sm-12\">\r\n        <table class=\"table table-bordered table-hover\">\r\n            <thead>\r\n            <tr>\r\n                <th class=\"middle center w50\">STT</th>\r\n                <th class=\"middle center\">Tên chức danh</th>\r\n                <th class=\"middle center w100\">Là trưởng đơn vị</th>\r\n                <th class=\"middle center w100\">Cho phép chọn nhiều</th>\r\n                <th class=\"middle center w50\">Xóa</th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            <tr *ngFor=\"let item of listTitle; let i = index\">\r\n                <td class=\"center\">{{i + 1}}</td>\r\n                <td>{{item.titleName}}</td>\r\n                <td class=\"center\">\r\n                    <i class=\"fa fa-check color-green size-18\" *ngIf=\"item.isLeader\"></i>\r\n                </td>\r\n                <td class=\"center\">\r\n                    <i class=\"fa fa-check color-green size-18\" *ngIf=\"item.isMultiple\"></i>\r\n                </td>\r\n                <td class=\"center\">\r\n                    <button mat-mini-fab color=\"warn\" type=\"button\" (click)=\"delete(item)\">\r\n                        <i class=\"fa fa-trash-o\"></i>\r\n                    </button>\r\n                </td>\r\n            </tr>\r\n            </tbody>\r\n        </table>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <ghm-paging [totalRows]=\"totalRows\" [currentPage]=\"currentPage\" [pageShow]=\"6\" (pageClick)=\"search($event)\"\r\n                [isDisabled]=\"isSearching\" [pageName]=\"'chức danh'\"></ghm-paging>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/organization/office/office-title/office-title.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/office-title/office-title.component.ts ***!
  \***************************************************************************************/
/*! exports provided: OfficeTitleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfficeTitleComponent", function() { return OfficeTitleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _services_office_position_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/office-position.service */ "./src/app/modules/hr/organization/office/services/office-position.service.ts");
/* harmony import */ var _base_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../base.component */ "./src/app/base.component.ts");






var OfficeTitleComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](OfficeTitleComponent, _super);
    function OfficeTitleComponent(toastr, officeTitleService) {
        var _this = _super.call(this) || this;
        _this.toastr = toastr;
        _this.officeTitleService = officeTitleService;
        _this.listTitle = [];
        _this.searchTerm = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        return _this;
    }
    OfficeTitleComponent.prototype.ngOnInit = function () {
        // this.searchTerm.subscribe(term => {
        //     this.isSearching = true;
        //     this.keyword = term;
        //     this.officeTitleService.searchTitleByOfficeId(this.keyword, this.officeId, this.currentPage, this.pageSize)
        //         .subscribe((result: any) => {
        //             if (result) {
        //                 this.isSearching = false;
        //                 this.listTitle = result.items;
        //                 this.totalRows = result.totalRows;
        //             }
        //         });
        // });
    };
    OfficeTitleComponent.prototype.onSelectTitle = function (title) {
        // this.isSaving = true;
        // this.officeTitleService.insert(title.id, this.officeId).subscribe(result => {
        //     this.isSaving = false;
        //
        //     if (result === -1) {
        //         this.toastr.error(`Chức danh ${title.name} đã tồn tại trong phòng ban này. Vui lòng kiểm tra lại`);
        //         return;
        //     }
        //
        //     if (result === -2) {
        //         this.toastr.error('Chức danh không tồn tại hoặc đã bị xóa. Vui lòng kiểm tra lại hoặc liên hệ với quản trị viên.');
        //         return;
        //     }
        //
        //     if (result === -3) {
        //         this.toastr.error('Thông tin phòng ban không tồn tại hoặc đã bị xóa. Vui lòng kiểm tra lại hoặc liên hệ với quản trị viên');
        //         return;
        //     }
        //
        //     if (result > 0) {
        //         this.search(1);
        //         this.toastr.success('Thêm chức danh vào phòng ban thành công.');
        //         return;
        //     }
        // });
    };
    OfficeTitleComponent.prototype.search = function (currentPage) {
        this.currentPage = currentPage;
        this.searchTerm.next(this.keyword);
    };
    OfficeTitleComponent.prototype.delete = function (officeTitle) {
        // swal({
        //     title: `Bạn có chắc chắn muốn xóa chức danh: "${officeTitle.titleName}" ra khỏi phòng ban này không?`,
        //     text: this.message.deleteWarning,
        //     type: 'warning',
        //     showCancelButton: true,
        //     confirmButtonColor: '#DD6B55',
        //     confirmButtonText: 'Đồng ý',
        //     cancelButtonText: 'Hủy bỏ'
        // }).then((isConfirm: boolean) => {
        //     if (isConfirm) {
        //         this.officeTitleService.delete(officeTitle.titleId, officeTitle.officeId).subscribe(result => {
        //             if (result === -1) {
        //                 this.toastr.error(`Chức danh: "${officeTitle.titleName}" không tồn tại trong phòng ban này.`);
        //                 return;
        //             }
        //
        //             if (result === -2) {
        //                 this.toastr.error('Chức danh của phòng ban này đang được người dùng sử dụng. Vui lòng xóa chức danh của người dùng trước khi xóa chức danh trong phòng ban.');
        //                 return;
        //             }
        //
        //             if (result > 0) {
        //                 // this.toastr.success("Xóa chức danh khỏi phòng ban thành công.");
        //                 setTimeout(() => {
        //                     swal({
        //                         title: 'Đã xóa',
        //                         text: 'Xóa chức vụ thành công.',
        //                         type: 'success',
        //                         timer: 1500,
        //                         showConfirmButton: false
        //                     });
        //                 }, 200);
        //                 this.search(this.currentPage);
        //                 return;
        //             }
        //         });
        //     }
        // }, () => {
        // });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], OfficeTitleComponent.prototype, "officeId", void 0);
    OfficeTitleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-office-title-list',
            template: __webpack_require__(/*! ./office-title.component.html */ "./src/app/modules/hr/organization/office/office-title/office-title.component.html"),
            providers: [_services_office_position_service__WEBPACK_IMPORTED_MODULE_4__["OfficePositionService"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            _services_office_position_service__WEBPACK_IMPORTED_MODULE_4__["OfficePositionService"]])
    ], OfficeTitleComponent);
    return OfficeTitleComponent;
}(_base_component__WEBPACK_IMPORTED_MODULE_5__["BaseComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/organization/office/office-tree/office-tree.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/office-tree/office-tree.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-dropdown-tree\r\n    title=\"-- Chọn phòng ban --\"\r\n    i18n=\"@@selectOffice\"\r\n    [data]=\"officeTree\"\r\n    [selectedText]=\"selectedText\"\r\n    (nodeSelected)=\"onOfficeSelected($event)\"></nh-dropdown-tree>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/organization/office/office-tree/office-tree.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/office-tree/office-tree.component.ts ***!
  \*************************************************************************************/
/*! exports provided: OfficeTreeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfficeTreeComponent", function() { return OfficeTreeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_office_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/office.service */ "./src/app/modules/hr/organization/office/services/office.service.ts");



var OfficeTreeComponent = /** @class */ (function () {
    function OfficeTreeComponent(officeService) {
        this.officeService = officeService;
        this.officeSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.officeTree = [];
    }
    OfficeTreeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.officeService.getTree()
            .subscribe(function (result) {
            _this.officeTree = result;
        });
    };
    OfficeTreeComponent.prototype.onOfficeSelected = function (office) {
        this.officeSelected.emit(office);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], OfficeTreeComponent.prototype, "officeSelected", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], OfficeTreeComponent.prototype, "selectedText", void 0);
    OfficeTreeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-office-tree',
            template: __webpack_require__(/*! ./office-tree.component.html */ "./src/app/modules/hr/organization/office/office-tree/office-tree.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_office_service__WEBPACK_IMPORTED_MODULE_2__["OfficeService"]])
    ], OfficeTreeComponent);
    return OfficeTreeComponent;
}());



/***/ }),

/***/ "./src/app/modules/hr/organization/office/office.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/office.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@listOfficePageTitle\">Danh sách phòng ban</span>\r\n    <small i18n=\"@@officeModuleTitle\">Quản lý phòng ban</small>\r\n</h1>\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <form class=\"form-inline cm-mgb-10\" (ngSubmit)=\"search(1)\">\r\n            <div class=\"form-group cm-mgr-5\">\r\n                <input type=\"text\" class=\"form-control\" i18n=\"@@enterKeyword\" i18n-placeholder\r\n                       placeholder=\"Nhập từ khóa tìm kiêm\" name=\"keyword\" [(ngModel)]=\"keyword\">\r\n            </div>\r\n            <div class=\"form-group cm-mgr-5\">\r\n                <nh-select\r\n                    i18n-title=\"@@filterByStatusTitle\"\r\n                    title=\"-- Tìm kiếm bởi trạng thái --\"\r\n                    [data]=\"listActiveSearch\"\r\n                    [(value)]=\"isActive\"\r\n                    (onSelectItem)=\"search(1)\"></nh-select>\r\n            </div>\r\n            <div class=\"form-group cm-mgr-5\">\r\n                <ghm-button icon=\"fa search\" classes=\"btn blue\" [loading]=\"isSearching\">\r\n                    <i class=\"fa fa-search\"></i>\r\n                </ghm-button>\r\n            </div>\r\n            <div class=\"form-group cm-mgr-5\">\r\n                <button class=\"btn btn-default\" (click)=\"refresh()\">\r\n                    <i class=\"fa fa-refresh\"></i>\r\n                </button>\r\n            </div>\r\n            <div class=\"form-group pull-right\">\r\n                <button type=\"button\" class=\"btn blue\" (click)=\"add()\"\r\n                        i18n=\"@@add\" *ngIf=\"permission.add\">\r\n                    Thêm\r\n                </button>\r\n            </div>\r\n        </form>\r\n    </div>\r\n    <div class=\"col-sm-12\">\r\n        <table class=\"table table-hover table-stripped\">\r\n            <thead>\r\n            <tr>\r\n                <th class=\"center middle w50\" i18n=\"@@no\">STT</th>\r\n                <th class=\"middle\" i18n=\"@@officeName\">Tên phòng ban</th>\r\n                <th class=\"middle\" i18n=\"@@parentOffice\">Parent office</th>\r\n                <th class=\"middle\" i18n=\"@@officeCode\">Mã phòng ban</th>\r\n                <th class=\"middle\" i18n=\"@@officeCode\">Loại phòng ban</th>\r\n                <th class=\"middle w50\" i18n=\"@@activeStatus\">Trạng thái</th>\r\n                <th class=\"text-right middle w150\" i18n=\"@@actions\" *ngIf=\"permission.edit || permission.delete\">\r\n                    Thao tác\r\n                </th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            <tr *ngFor=\"let office of listItems$ | async; let i = index\">\r\n                <td class=\"center middle\">{{ (currentPage - 1) * pageSize + i + 1 }}</td>\r\n                <td class=\"middle\">\r\n                    <!--<i *ngFor=\"let item of createRange(office.level)\"-->\r\n                    <!--class=\"fa fa-long-arrow-right cm-mgr-5 color-blue\"></i>-->\r\n                    <a href=\"javasript://\" (click)=\"edit(office.id)\"\r\n                       *ngIf=\"permission.edit; else officeNameWithoutEdit\">\r\n                        <span [innerHTML]=\"office.nameLevel\"></span>\r\n                        {{office.name}}\r\n                    </a>\r\n                    <ng-template #officeNameWithoutEdit>\r\n                        {{ office.name }}\r\n                    </ng-template>\r\n                </td>\r\n                <td class=\"middle\">\r\n                    {{ office.parentName }}\r\n                </td>\r\n                <td class=\"middle\">{{ office.code }}</td>\r\n                <td class=\"middle\">\r\n                        <span i18n=\"@@officeType\" class=\"badge\"\r\n                              [class.badge-info]=\"office.officeType == 0\"\r\n                              [class.badge-success]=\"office.officeType == 1\"\r\n                              [class.badge-danger]=\"office.officeType == 2\"\r\n                              [class.badge-warning]=\"office.officeType == 3\"\r\n                        >\r\n                            {office.officeType, plural, =0 {Normal} =1 {Hr} =2 {Director} =3 {Stand alone company} other {N/A}}\r\n                        </span>\r\n                </td>\r\n                <td class=\"middle\">\r\n                        <span class=\"badge\" [class.badge-danger]=\"!office.isActive\"\r\n                              [class.badge-success]=\"office.isActive\">{office.activeStatus, select, active {Activated} inActive {In active}}</span>\r\n                </td>\r\n                <td class=\"text-right middle\" *ngIf=\"permission.edit || permission.delete\">\r\n                    <!--<ghm-button *ngIf=\"permission.edit\" icon=\"fa fa-eye\" classes=\"btn btn-default btn-sm\"-->\r\n                                <!--(clicked)=\"\"></ghm-button>-->\r\n                    <!--<ghm-button *ngIf=\"permission.edit\" icon=\"fa fa-edit\" classes=\"btn blue btn-sm\"-->\r\n                                <!--(clicked)=\"edit(office.id)\"></ghm-button>-->\r\n                    <!--<ghm-button *ngIf=\"permission.delete\" icon=\"fa fa-trash-o\" classes=\"btn red btn-sm\"-->\r\n                                <!--[swal]=\"confirmDeleteOffice\" (confirm)=\"delete(office.id)\"></ghm-button>-->\r\n                    <nh-dropdown>\r\n                        <button type=\"button\" class=\"btn btn-sm btn-light btn-no-background no-border\" matTooltip=\"Menu\">\r\n                            <mat-icon>more_horiz</mat-icon>\r\n                        </button>\r\n                        <ul class=\"nh-dropdown-menu right\" role=\"menu\">\r\n                            <li>\r\n                                <a *ngIf=\"permission.view\"\r\n                                   i18n=\"@@view\"\r\n                                (click)=\"detail(office.id)\">\r\n                                    <mat-icon class=\"menu-icon\">info</mat-icon>\r\n                                    Chi tiết\r\n                                </a>\r\n                            </li>\r\n                            <li>\r\n                                <a *ngIf=\"permission.edit\"\r\n                                   i18n=\"@@edit\"\r\n                                (click)=\"edit(office.id)\">\r\n                                    <mat-icon class=\"menu-icon\">edit</mat-icon>\r\n                                    Sửa\r\n                                </a>\r\n                            </li>\r\n                            <li>\r\n                                <a (click)=\"confirm(office.id)\" i18n=\"@@delete\">\r\n                                    <mat-icon class=\"menu-icon\">delete</mat-icon>\r\n                                    Xóa\r\n                                </a>\r\n                            </li>\r\n                        </ul>\r\n                    </nh-dropdown>\r\n                </td>\r\n            </tr>\r\n            </tbody>\r\n        </table>\r\n        <ghm-paging\r\n            [totalRows]=\"totalRows\"\r\n            [currentPage]=\"currentPage\"\r\n            [pageSize]=\"pageSize\"\r\n            [pageShow]=\"6\"\r\n            [isDisabled]=\"isSearching\"\r\n            (pageClick)=\"search($event)\"></ghm-paging>\r\n    </div>\r\n</div>\r\n\r\n<app-office-form (saveSuccessful)=\"search(1)\"></app-office-form>\r\n<app-office-detail (edited)=\"edit($event)\"></app-office-detail>\r\n<swal #confirmDeleteOffice\r\n      i18n-title=\"@@confirmDeleteOfficeTitle\"\r\n      i18n-text=\"@@confirmDeleteOfficeText\"\r\n      title=\"Bạn có muốn xóa phòng ban này?\"\r\n      text=\"Bạn không thể khôi phục lại phòng ban sau khi xóa.\"\r\n      type=\"question\"\r\n      i18n-confirmButtonText=\"@@accept\"\r\n      i18n-cancelButtonText=\"@@cancel\"\r\n      confirmButtonText=\"Đồng ý\"\r\n      cancelButtonText=\"Hủy\"\r\n      [showCancelButton]=\"true\">\r\n</swal>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/organization/office/office.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/modules/hr/organization/office/office.component.ts ***!
  \********************************************************************/
/*! exports provided: OfficeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfficeComponent", function() { return OfficeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_office_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./services/office.service */ "./src/app/modules/hr/organization/office/services/office.service.ts");
/* harmony import */ var _shareds_components_nh_tab_nh_tab_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../shareds/components/nh-tab/nh-tab.component */ "./src/app/shareds/components/nh-tab/nh-tab.component.ts");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _configs_page_id_config__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../configs/page-id.config */ "./src/app/configs/page-id.config.ts");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _office_form_office_form_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./office-form/office-form.component */ "./src/app/modules/hr/organization/office/office-form/office-form.component.ts");
/* harmony import */ var _office_detail_office_detail_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./office-detail/office-detail.component */ "./src/app/modules/hr/organization/office/office-detail/office-detail.component.ts");
/* harmony import */ var _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../../shareds/models/filter-link.model */ "./src/app/shareds/models/filter-link.model.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");















var OfficeComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](OfficeComponent, _super);
    function OfficeComponent(pageId, location, route, router, title, spinnerService, utilService, officeService) {
        var _this = _super.call(this) || this;
        _this.pageId = pageId;
        _this.location = location;
        _this.route = route;
        _this.router = router;
        _this.title = title;
        _this.spinnerService = spinnerService;
        _this.utilService = utilService;
        _this.officeService = officeService;
        _this.status = null;
        _this.listActiveSearch = [];
        // Test.
        _this.data = [];
        _this.subscribers.getListActiveSearch = _this.appService.getListActiveSearch()
            .subscribe(function (result) { return _this.listActiveSearch = result; });
        return _this;
    }
    OfficeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.appService.setupPage(this.pageId.HR, this.pageId.OFFICE);
        this.listItems$ = this.route.data.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            var data = result.data;
            _this.totalRows = data.totalRows;
            return data.items;
        }));
        this.officeService.searchForSuggestion('')
            .subscribe(function (result) { return _this.data = result; });
    };
    OfficeComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.subscribers.queryParams = this.route.queryParams.subscribe(function (params) {
            var url = _this.router.url;
            var id = params.id;
            if (url.indexOf('detail') > -1 && id) {
                setTimeout(function () { return _this.detail(id); });
            }
            if (url.indexOf('edit') && id) {
                setTimeout(function () { return _this.edit(id); });
            }
        });
    };
    OfficeComponent.prototype.onSuggestionSearched = function (keyword) {
        var _this = this;
        this.officeService.searchForSuggestion(keyword)
            .subscribe(function (result) { return _this.data = result; });
    };
    OfficeComponent.prototype.search = function (currentPage) {
        var _this = this;
        this.currentPage = currentPage;
        this.isSearching = true;
        this.renderFilterLink();
        this.listItems$ = this.officeService
            .search(this.keyword, this.isActive, this.currentPage, this.pageSize)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () { return (_this.isSearching = false); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            _this.totalRows = result.totalRows;
            return result.items;
        }));
    };
    OfficeComponent.prototype.refresh = function () {
        this.keyword = '';
        this.isActive = null;
    };
    OfficeComponent.prototype.add = function () {
        this.officeFormComponent.add();
    };
    OfficeComponent.prototype.edit = function (officeId) {
        this.location.go("/organization/offices/edit?id=" + officeId);
        this.officeFormComponent.edit(officeId);
    };
    OfficeComponent.prototype.detail = function (officeId) {
        this.location.go("/organization/offices/detail?id=" + officeId);
        this.officeDetailComponent.showDetail(officeId);
    };
    OfficeComponent.prototype.delete = function (officeId) {
        var _this = this;
        this.subscribers.deleteOffice = this.officeService.delete(officeId)
            .subscribe(function () { return _this.search(1); });
    };
    OfficeComponent.prototype.onChangeActiveStatus = function (office) {
        office.isActive = !office.isActive;
        // this.officeService.updateIsActive(office).subscribe((result: IActionResultResponse) => {
        //     // if (result === -1) {
        //     //     this.toastr.error(this.formatString(this.message.notExists, 'Phòng ban'));
        //     //     return;
        //     // }
        //     //
        //     // if (result > 0) {
        //     //     this.toastr.success(`${office.isActive ? 'Kích hoạt' : 'Bỏ kích hoạt'} phòng ban "${office.name}" thành công.`);
        //     //     return;
        //     // }
        //     //
        //     // if (result === 0) {
        //     //     this.toastr.warning('Vui lòng thay đổi trạng thái của phòng ban');
        //     //     return;
        //     // }
        //
        //     this.toastr.warning(result.message, result.title);
        //     return;
        // });
    };
    OfficeComponent.prototype.onTabClosed = function (data) {
        if (data.active) {
            this.search(this.currentPage);
            this.tabComponent.setTabActiveById('tabListOffice');
        }
    };
    OfficeComponent.prototype.createRange = function (number) {
        var items = [];
        for (var i = 1; i <= number; i++) {
            items.push(i);
        }
        return items;
    };
    OfficeComponent.prototype.renderFilterLink = function () {
        var path = '/organization/offices';
        var query = this.utilService.renderLocationFilter([
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_13__["FilterLink"]('keyword', this.keyword),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_13__["FilterLink"]('page', this.currentPage),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_13__["FilterLink"]('pageSize', this.pageSize)
        ]);
        this.location.go(path, query);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_shareds_components_nh_tab_nh_tab_component__WEBPACK_IMPORTED_MODULE_6__["NhTabComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_tab_nh_tab_component__WEBPACK_IMPORTED_MODULE_6__["NhTabComponent"])
    ], OfficeComponent.prototype, "tabComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_office_form_office_form_component__WEBPACK_IMPORTED_MODULE_11__["OfficeFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _office_form_office_form_component__WEBPACK_IMPORTED_MODULE_11__["OfficeFormComponent"])
    ], OfficeComponent.prototype, "officeFormComponent", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_office_detail_office_detail_component__WEBPACK_IMPORTED_MODULE_12__["OfficeDetailComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _office_detail_office_detail_component__WEBPACK_IMPORTED_MODULE_12__["OfficeDetailComponent"])
    ], OfficeComponent.prototype, "officeDetailComponent", void 0);
    OfficeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-office',
            template: __webpack_require__(/*! ./office.component.html */ "./src/app/modules/hr/organization/office/office.component.html"),
            providers: [
                _services_office_service__WEBPACK_IMPORTED_MODULE_5__["OfficeService"],
                _angular_common__WEBPACK_IMPORTED_MODULE_14__["Location"], { provide: _angular_common__WEBPACK_IMPORTED_MODULE_14__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_14__["PathLocationStrategy"] }
            ]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_page_id_config__WEBPACK_IMPORTED_MODULE_8__["PAGE_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_common__WEBPACK_IMPORTED_MODULE_14__["Location"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_9__["SpinnerService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_10__["UtilService"],
            _services_office_service__WEBPACK_IMPORTED_MODULE_5__["OfficeService"]])
    ], OfficeComponent);
    return OfficeComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_7__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/organization/organization-routing.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/modules/hr/organization/organization-routing.module.ts ***!
  \************************************************************************/
/*! exports provided: routes, OrganizationRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrganizationRoutingModule", function() { return OrganizationRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _office_office_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./office/office.component */ "./src/app/modules/hr/organization/office/office.component.ts");
/* harmony import */ var _office_services_office_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./office/services/office.service */ "./src/app/modules/hr/organization/office/services/office.service.ts");
/* harmony import */ var _title_title_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./title/title.component */ "./src/app/modules/hr/organization/title/title.component.ts");
/* harmony import */ var _title_title_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./title/title.service */ "./src/app/modules/hr/organization/title/title.service.ts");
/* harmony import */ var _position_position_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./position/position.component */ "./src/app/modules/hr/organization/position/position.component.ts");
/* harmony import */ var _position_position_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./position/position.service */ "./src/app/modules/hr/organization/position/position.service.ts");









var routes = [
    {
        path: '', component: _office_office_component__WEBPACK_IMPORTED_MODULE_3__["OfficeComponent"],
        resolve: {
            data: _office_services_office_service__WEBPACK_IMPORTED_MODULE_4__["OfficeService"]
        }
    },
    {
        path: 'offices', component: _office_office_component__WEBPACK_IMPORTED_MODULE_3__["OfficeComponent"],
        resolve: {
            data: _office_services_office_service__WEBPACK_IMPORTED_MODULE_4__["OfficeService"]
        },
        children: [
            { path: 'detail', component: _office_office_component__WEBPACK_IMPORTED_MODULE_3__["OfficeComponent"] },
            { path: 'edit', component: _office_office_component__WEBPACK_IMPORTED_MODULE_3__["OfficeComponent"] },
        ]
    },
    {
        path: 'titles', component: _title_title_component__WEBPACK_IMPORTED_MODULE_5__["TitleComponent"],
        resolve: {
            data: _title_title_service__WEBPACK_IMPORTED_MODULE_6__["TitleService"]
        }
    },
    {
        path: 'positions', component: _position_position_component__WEBPACK_IMPORTED_MODULE_7__["PositionComponent"],
        resolve: {
            data: _position_position_service__WEBPACK_IMPORTED_MODULE_8__["PositionService"]
        }
    },
];
var OrganizationRoutingModule = /** @class */ (function () {
    function OrganizationRoutingModule() {
    }
    OrganizationRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
            providers: [_title_title_service__WEBPACK_IMPORTED_MODULE_6__["TitleService"], _position_position_service__WEBPACK_IMPORTED_MODULE_8__["PositionService"], _office_services_office_service__WEBPACK_IMPORTED_MODULE_4__["OfficeService"]]
        })
    ], OrganizationRoutingModule);
    return OrganizationRoutingModule;
}());



/***/ }),

/***/ "./src/app/modules/hr/organization/organization.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/modules/hr/organization/organization.module.ts ***!
  \****************************************************************/
/*! exports provided: OrganizationModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrganizationModule", function() { return OrganizationModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _organization_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./organization-routing.module */ "./src/app/modules/hr/organization/organization-routing.module.ts");
/* harmony import */ var _title_title_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./title/title.component */ "./src/app/modules/hr/organization/title/title.component.ts");
/* harmony import */ var _title_title_form_title_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./title/title-form/title-form.component */ "./src/app/modules/hr/organization/title/title-form/title-form.component.ts");
/* harmony import */ var _position_position_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./position/position.component */ "./src/app/modules/hr/organization/position/position.component.ts");
/* harmony import */ var _position_position_form_position_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./position/position-form/position-form.component */ "./src/app/modules/hr/organization/position/position-form/position-form.component.ts");
/* harmony import */ var _office_office_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./office/office.component */ "./src/app/modules/hr/organization/office/office.component.ts");
/* harmony import */ var _office_office_form_office_form_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./office/office-form/office-form.component */ "./src/app/modules/hr/organization/office/office-form/office-form.component.ts");
/* harmony import */ var _office_office_detail_office_detail_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./office/office-detail/office-detail.component */ "./src/app/modules/hr/organization/office/office-detail/office-detail.component.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../shareds/components/nh-modal/nh-modal.module */ "./src/app/shareds/components/nh-modal/nh-modal.module.ts");
/* harmony import */ var _shareds_components_ghm_paging_ghm_paging_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../shareds/components/ghm-paging/ghm-paging.module */ "./src/app/shareds/components/ghm-paging/ghm-paging.module.ts");
/* harmony import */ var _shareds_components_nh_select_nh_select_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../shareds/components/nh-select/nh-select.module */ "./src/app/shareds/components/nh-select/nh-select.module.ts");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _office_office_title_office_title_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./office/office-title/office-title.component */ "./src/app/modules/hr/organization/office/office-title/office-title.component.ts");
/* harmony import */ var _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @toverux/ngx-sweetalert2 */ "./node_modules/@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _shareds_components_tinymce_tinymce_module__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../../shareds/components/tinymce/tinymce.module */ "./src/app/shareds/components/tinymce/tinymce.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shareds_components_nh_tree_nh_tree_module__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../../../shareds/components/nh-tree/nh-tree.module */ "./src/app/shareds/components/nh-tree/nh-tree.module.ts");
/* harmony import */ var _shareds_components_nh_suggestion_nh_suggestion_module__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../../../shareds/components/nh-suggestion/nh-suggestion.module */ "./src/app/shareds/components/nh-suggestion/nh-suggestion.module.ts");
/* harmony import */ var _shareds_components_ghm_user_suggestion_ghm_user_suggestion_module__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../../../shareds/components/ghm-user-suggestion/ghm-user-suggestion.module */ "./src/app/shareds/components/ghm-user-suggestion/ghm-user-suggestion.module.ts");
/* harmony import */ var _office_office_contact_office_contact_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./office/office-contact/office-contact.component */ "./src/app/modules/hr/organization/office/office-contact/office-contact.component.ts");
/* harmony import */ var _office_office_contact_office_contact_form_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./office/office-contact/office-contact-form.component */ "./src/app/modules/hr/organization/office/office-contact/office-contact-form.component.ts");
/* harmony import */ var _office_office_tree_office_tree_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./office/office-tree/office-tree.component */ "./src/app/modules/hr/organization/office/office-tree/office-tree.component.ts");
/* harmony import */ var _shareds_components_nh_dropdown_nh_dropdown_module__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ../../../shareds/components/nh-dropdown/nh-dropdown.module */ "./src/app/shareds/components/nh-dropdown/nh-dropdown.module.ts");
/* harmony import */ var _shareds_components_ghm_input_ghm_input_module__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ../../../shareds/components/ghm-input/ghm-input.module */ "./src/app/shareds/components/ghm-input/ghm-input.module.ts");
/* harmony import */ var _shareds_components_ghm_select_ghm_select_module__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ../../../shareds/components/ghm-select/ghm-select.module */ "./src/app/shareds/components/ghm-select/ghm-select.module.ts");





























var OrganizationModule = /** @class */ (function () {
    function OrganizationModule() {
    }
    OrganizationModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_organization_routing_module__WEBPACK_IMPORTED_MODULE_3__["OrganizationRoutingModule"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _shareds_components_nh_modal_nh_modal_module__WEBPACK_IMPORTED_MODULE_11__["NhModalModule"], _shareds_components_ghm_paging_ghm_paging_module__WEBPACK_IMPORTED_MODULE_12__["GhmPagingModule"], _shareds_components_nh_select_nh_select_module__WEBPACK_IMPORTED_MODULE_13__["NhSelectModule"], _core_core_module__WEBPACK_IMPORTED_MODULE_14__["CoreModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_17__["MatCheckboxModule"], _shareds_components_tinymce_tinymce_module__WEBPACK_IMPORTED_MODULE_18__["TinymceModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_19__["ReactiveFormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_19__["FormsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_17__["MatTooltipModule"], _shareds_components_nh_tree_nh_tree_module__WEBPACK_IMPORTED_MODULE_20__["NHTreeModule"], _angular_material__WEBPACK_IMPORTED_MODULE_17__["MatSlideToggleModule"],
                _shareds_components_nh_suggestion_nh_suggestion_module__WEBPACK_IMPORTED_MODULE_21__["NhSuggestionModule"], _shareds_components_ghm_user_suggestion_ghm_user_suggestion_module__WEBPACK_IMPORTED_MODULE_22__["GhmUserSuggestionModule"], _angular_material__WEBPACK_IMPORTED_MODULE_17__["MatIconModule"], _shareds_components_nh_dropdown_nh_dropdown_module__WEBPACK_IMPORTED_MODULE_26__["NhDropdownModule"], _shareds_components_ghm_input_ghm_input_module__WEBPACK_IMPORTED_MODULE_27__["GhmInputModule"], _shareds_components_ghm_select_ghm_select_module__WEBPACK_IMPORTED_MODULE_28__["GhmSelectModule"],
                _toverux_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_16__["SweetAlert2Module"].forRoot({
                    buttonsStyling: false,
                    customClass: 'modal-content',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn',
                    confirmButtonText: 'Đồng ý',
                    showCancelButton: true,
                    cancelButtonText: 'Hủy bỏ'
                })],
            exports: [_office_office_tree_office_tree_component__WEBPACK_IMPORTED_MODULE_25__["OfficeTreeComponent"]],
            declarations: [
                // Titles.
                _title_title_component__WEBPACK_IMPORTED_MODULE_4__["TitleComponent"], _title_title_form_title_form_component__WEBPACK_IMPORTED_MODULE_5__["TitleFormComponent"],
                // Positions.
                _position_position_component__WEBPACK_IMPORTED_MODULE_6__["PositionComponent"], _position_position_form_position_form_component__WEBPACK_IMPORTED_MODULE_7__["PositionFormComponent"],
                // Offices.
                _office_office_component__WEBPACK_IMPORTED_MODULE_8__["OfficeComponent"], _office_office_form_office_form_component__WEBPACK_IMPORTED_MODULE_9__["OfficeFormComponent"], _office_office_detail_office_detail_component__WEBPACK_IMPORTED_MODULE_10__["OfficeDetailComponent"], _office_office_title_office_title_component__WEBPACK_IMPORTED_MODULE_15__["OfficeTitleComponent"], _office_office_contact_office_contact_component__WEBPACK_IMPORTED_MODULE_23__["OfficeContactComponent"],
                _office_office_contact_office_contact_form_component__WEBPACK_IMPORTED_MODULE_24__["OfficeContactFormComponent"], _office_office_tree_office_tree_component__WEBPACK_IMPORTED_MODULE_25__["OfficeTreeComponent"]
            ],
            providers: [],
        })
    ], OrganizationModule);
    return OrganizationModule;
}());



/***/ }),

/***/ "./src/app/modules/hr/organization/position/models/position-translation.model.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/modules/hr/organization/position/models/position-translation.model.ts ***!
  \***************************************************************************************/
/*! exports provided: PositionTranslation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PositionTranslation", function() { return PositionTranslation; });
var PositionTranslation = /** @class */ (function () {
    function PositionTranslation() {
    }
    return PositionTranslation;
}());



/***/ }),

/***/ "./src/app/modules/hr/organization/position/position-form/position-form.component.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/modules/hr/organization/position/position-form/position-form.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #positionFormModal\r\n          size=\"md\"\r\n          (shown)=\"onModalShown()\"\r\n          (onHidden)=\"onModalHidden()\">\r\n    <nh-modal-header>\r\n        <i class=\"fa fa-graduation-cap\"></i>\r\n        <span i18n=\"@@positionFormTitle\">{isUpdate, select, 1 {Cập nhật chức vụ} 0 {Thêm mới chức vụ}}</span>\r\n    </nh-modal-header>\r\n    <form class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content>\r\n            <div class=\"form-group\" *ngIf=\"languages && languages.length > 1\">\r\n                <label i18n-ghmLabel=\"@@language\" ghmLabel=\"Ngôn ngữ\"\r\n                       class=\"col-sm-4 control-label\"></label>\r\n                <div class=\"col-sm-8\">\r\n                    <ghm-select [data]=\"languages\"\r\n                                i18n-title=\"@@pleaseSelectLanguage\"\r\n                                title=\"-- Chọn ngôn ngữ --\"\r\n                                name=\"language\"\r\n                                [elementId]=\"'selectlanguage'\"\r\n                                [(value)]=\"currentLanguage\"\r\n                                (itemSelected)=\"currentLanguage = $event.id\"></ghm-select>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\"\r\n                 [class.has-error]=\"formErrors.titleId\">\r\n                <label i18n=\"@@selectTitle\" i18n-ghmLabel ghmLabel=\"Chọn chức danh\"\r\n                       class=\"col-sm-4 control-label\"\r\n                       [required]=\"true\"\r\n                ></label>\r\n                <div class=\"col-sm-8\">\r\n                    <ghm-select\r\n                        i18n-title=\"@@selectTitlePlaceholder\"\r\n                        [data]=\"titles\"\r\n                        [elementId]=\"'selectTitle'\"\r\n                        title=\"-- Chọn chức danh --\"\r\n                        formControlName=\"titleId\"></ghm-select>\r\n                    <span class=\"help-block\">\r\n                        {\r\n                        formErrors.titleId,\r\n                        select, required {Chọn chức danh} other {}\r\n                        }\r\n                    </span>\r\n                </div>\r\n            </div>\r\n            <span formArrayName=\"translations\">\r\n                <div class=\"form-group\"\r\n                     *ngFor=\"let modelTranslation of translations.controls; index as i\"\r\n                     [hidden]=\"modelTranslation.value.languageId !== currentLanguage\"\r\n                     [formGroupName]=\"i\"\r\n                     [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.name\">\r\n                            <label i18n=\"@@shortName\" i18n-ghmLabel ghmLabel=\"Tên viết tắt\"\r\n                                   class=\"col-sm-4 control-label\"\r\n                                   [required]=\"true\"\r\n                            ></label>\r\n                            <div class=\"col-sm-8\">\r\n                                <ghm-input i18n-placeholder=\"@@enterTitleShortNamePlaceHolder\"\r\n                                           placeholder=\"Nhập tên viết tắt.\"\r\n                                           formControlName=\"shortName\"></ghm-input>\r\n                                <span class=\"help-block\">\r\n                                    {\r\n                                    translationFormErrors[modelTranslation.value.languageId]?.shortName,\r\n                                    select, required {Chức vụ viết tăt không được để trống} maxlength {Chức vụ viết tắt không được vượt quá 20 ký tự}\r\n                                    }\r\n                                </span>\r\n                            </div>\r\n                </div>\r\n            </span>\r\n            <div formArrayName=\"translations\">\r\n                <div class=\"form-group\"\r\n                     *ngFor=\"let modelTranslation of translations.controls; index as i\"\r\n                     [hidden]=\"modelTranslation.value.languageId !== currentLanguage\"\r\n                     [formGroupName]=\"i\"\r\n                     [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.name\">\r\n                    <label i18n=\"@@positionName\" i18n-ghmLabel\r\n                           ghmLabel=\"Tên chức vụ\" for=\"\" class=\"col-sm-4 control-label\"\r\n                           [required]=\"true\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <ghm-input i18n-placeholder=\"@@enterPositionName\"\r\n                                   placeholder=\"Nhập tên chức vụ\" elementId=\"name\"\r\n                                   formControlName=\"name\"></ghm-input>\r\n                        <span class=\"help-block\">\r\n                                        {\r\n                                        translationFormErrors[modelTranslation.value.languageId]?.name,\r\n                                        select, required {Tên chức vụ không được để trống} maxlength {Tên chức vụ không được vượt quá 256 ký tự}\r\n                                        }\r\n                                    </span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <div class=\"col-sm-8 col-sm-offset-4\">\r\n                    <mat-checkbox color=\"primary\" formControlName=\"isActive\" i18n=\"@@isActive\">\r\n                        {model.value.isActive, select, 0 {Chưa kích hoạt} 1 {Kích hoạt}}\r\n                    </mat-checkbox>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <div class=\"col-sm-8 col-sm-offset-4\">\r\n                    <mat-checkbox color=\"primary\" formControlName=\"isManager\" i18n=\"@@isManager\">\r\n                        Là quản lý\r\n                    </mat-checkbox>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <div class=\"col-sm-8 col-sm-offset-4\">\r\n                    <mat-checkbox color=\"primary\" formControlName=\"isMultiple\" i18n=\"@@isMultiple\">\r\n                        Cho phép chọn nhiều\r\n                    </mat-checkbox>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label i18n-ghmLabel=\"@@description\" ghmLabel=\"Sử dụng cho phòng ban\"\r\n                       class=\"col-sm-4 control-label\"\r\n                ></label>\r\n                <div class=\"col-sm-8\">\r\n                    <nh-suggestion\r\n                        i18n-placeholder=\"@@officeSuggestionPlaceholder\"\r\n                        placeholder=\"Nhập tên phòng ban\"\r\n                        [multiple]=\"true\"\r\n                        [sources]=\"offices\"\r\n                        [loading]=\"isSearchingOffice\"\r\n                        [selectedItems]=\"selectedOffices\"\r\n                        (searched)=\"onSearched($event)\"\r\n                        (itemSelected)=\"onSelectedOffice($event)\"\r\n                    ></nh-suggestion>\r\n                </div>\r\n            </div>\r\n            <div formArrayName=\"translations\">\r\n                <div class=\"form-group\"\r\n                     *ngFor=\"let modelTranslation of translations.controls; index as i\"\r\n                     [hidden]=\"modelTranslation.value.languageId !== currentLanguage\"\r\n                     [formGroupName]=\"i\"\r\n                     [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.description\">\r\n                    <label i18n=\"@@description\" i18n-ghmLabel ghmLabel=\"Mô tả\"\r\n                           class=\"col-sm-4 control-label\"\r\n                    ></label>\r\n                    <div class=\"col-sm-8\">\r\n                                    <textarea class=\"form-control\" rows=\"2\" formControlName=\"description\"\r\n                                              i18n=\"@@enterDescriptionPlaceholder\" i18n-placeholder\r\n                                              placeholder=\"Enter description.\"></textarea>\r\n                        <span class=\"help-block\">\r\n                                        {\r\n                                        translationFormErrors[modelTranslation.value.languageId]?.description,\r\n                                        select, maxMength {Môt tả chức vụ không được vượt quá 500 ký tự}\r\n                                        }\r\n                                    </span>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\"\r\n                     *ngFor=\"let modelTranslation of translations.controls; index as i\"\r\n                     [hidden]=\"modelTranslation.value.languageId !== currentLanguage\"\r\n                     [formGroupName]=\"i\"\r\n                     [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.purpose\">\r\n                    <label i18n=\"@@purposse\" i18n-ghmLabel ghmLabel=\"Mục đích\"\r\n                           class=\"col-sm-4 control-label\"\r\n                    ></label>\r\n                    <div class=\"col-sm-8\">\r\n                                    <textarea class=\"form-control\" rows=\"2\" formControlName=\"purpose\"\r\n                                              i18n=\"@@enterPurposePlaceholder\" i18n-placeholder\r\n                                              placeholder=\"Nhập mục đích\"></textarea>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\"\r\n                     *ngFor=\"let modelTranslation of translations.controls; index as i\"\r\n                     [hidden]=\"modelTranslation.value.languageId !== currentLanguage\"\r\n                     [formGroupName]=\"i\"\r\n                     [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.otherRequire\">\r\n                    <label i18n=\"@@otherRequire\" i18n-ghmLabel ghmLabel=\"Yêu cầu khác\"\r\n                           class=\"col-sm-4 control-label\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                                    <textarea class=\"form-control\" rows=\"2\" formControlName=\"otherRequire\"\r\n                                              i18n=\"@@enterOtherRequirePlaceholder\" i18n-placeholder\r\n                                              placeholder=\"Nhập yêu câu khác.\"></textarea>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\"\r\n                     *ngFor=\"let modelTranslation of translations.controls; index as i\"\r\n                     [hidden]=\"modelTranslation.value.languageId !== currentLanguage\"\r\n                     [formGroupName]=\"i\"\r\n                     [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.responsibility\">\r\n                    <label i18n=\"@@responsibility\" i18n-ghmLabel ghmLabel=\"Trách nhiệm\"\r\n                           class=\"col-sm-4 control-label\"\r\n                    ></label>\r\n                    <div class=\"col-sm-8\">\r\n                                    <textarea class=\"form-control\" rows=\"2\" formControlName=\"responsibility\"\r\n                                              i18n=\"@@enterResponsibilityPlaceholder\" i18n-placeholder\r\n                                              placeholder=\"Nhập trách nhiệm.\"></textarea>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <!--<logic type=\"logic\" (click)=\"\"></logic>-->\r\n            <mat-checkbox [checked]=\"isCreateAnother\" (change)=\"isCreateAnother = !isCreateAnother\"\r\n                          *ngIf=\"!isUpdate\"\r\n                          i18n=\"@@isCreateAnother\"\r\n                          class=\"cm-mgr-5\"\r\n                          color=\"primary\">\r\n                Tiếp tục thêm\r\n            </mat-checkbox>\r\n            <ghm-button classes=\"btn blue cm-mgr-5\" [loading]=\"isSaving\" i18n=\"@@save\">\r\n                Lưu\r\n            </ghm-button>\r\n            <ghm-button classes=\"btn btn-default\" i18n=\"@@cancel\"\r\n                        type=\"button\" nh-dismiss=\"true\">\r\n                Đóng\r\n            </ghm-button>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/organization/position/position-form/position-form.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/modules/hr/organization/position/position-form/position-form.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: PositionFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PositionFormComponent", function() { return PositionFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _position_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../position.service */ "./src/app/modules/hr/organization/position/position.service.ts");
/* harmony import */ var _position_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../position.model */ "./src/app/modules/hr/organization/position/position.model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _configs_page_id_config__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../configs/page-id.config */ "./src/app/configs/page-id.config.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _models_position_translation_model__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../models/position-translation.model */ "./src/app/modules/hr/organization/position/models/position-translation.model.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _title_title_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../title/title.service */ "./src/app/modules/hr/organization/title/title.service.ts");
/* harmony import */ var _office_services_office_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../office/services/office.service */ "./src/app/modules/hr/organization/office/services/office.service.ts");















var PositionFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](PositionFormComponent, _super);
    function PositionFormComponent(pageId, fb, renderer, positionService, toastr, utilService, officeService, titleService) {
        var _this = _super.call(this) || this;
        _this.pageId = pageId;
        _this.fb = fb;
        _this.renderer = renderer;
        _this.positionService = positionService;
        _this.toastr = toastr;
        _this.utilService = utilService;
        _this.officeService = officeService;
        _this.titleService = titleService;
        _this.onSaveSuccess = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.position = new _position_model__WEBPACK_IMPORTED_MODULE_5__["Position"]();
        _this.titleTranslation = new _models_position_translation_model__WEBPACK_IMPORTED_MODULE_11__["PositionTranslation"]();
        _this.titles = [];
        _this.selectedOffices = [];
        _this.offices = [];
        _this.isSearchingOffice = false;
        _this.buildFormLanguage = function (language) {
            _this.translationFormErrors[language] = _this.renderFormError(['name', 'shortName', 'description']);
            _this.translationValidationMessage[language] = _this.renderFormErrorMessage([
                { 'name': ['required', 'maxlength'] },
                { 'description': ['maxlength'] },
                { 'shortName': ['required', 'maxlength'] }
            ]);
            var translationModel = _this.fb.group({
                languageId: [language],
                name: [_this.titleTranslation.name, [
                        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(256)
                    ]],
                shortName: [_this.titleTranslation.shortName, [
                        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)
                    ]],
                description: [_this.titleTranslation.description, [
                        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(500)
                    ]],
                otherRequire: [_this.titleTranslation.otherRequire],
                responsibility: [_this.titleTranslation.responsibility],
                purpose: [_this.titleTranslation.purpose],
            });
            translationModel.valueChanges.subscribe(function (data) { return _this.validateTranslation(false); });
            return translationModel;
        };
        return _this;
    }
    PositionFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.titles.length === 0) {
            this.subscribers.getTitles = this.titleService.getAllActivated()
                .subscribe(function (result) {
                _this.titles = result;
            });
        }
        this.renderForm();
    };
    PositionFormComponent.prototype.onModalShown = function () {
        this.isModified = false;
        this.utilService.focusElement('name');
    };
    PositionFormComponent.prototype.onModalHidden = function () {
        this.validateModel(false);
        this.resetModel();
        this.isUpdate = false;
        this.selectedOffices = [];
        if (this.isModified) {
            this.saveSuccessful.emit();
        }
    };
    PositionFormComponent.prototype.onSelectedOffice = function (items) {
        this.selectedOffices = items;
    };
    PositionFormComponent.prototype.onSearched = function (keyword) {
        var _this = this;
        this.isSearchingOffice = true;
        this.subscribers.searchSuggestionOffice = this.officeService
            .searchForSuggestion(keyword)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.isSearchingOffice = false; }))
            .subscribe(function (result) { return _this.offices = result; });
    };
    PositionFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.validateModel();
        var isLanguageValid = this.validateLanguage();
        if (isValid && isLanguageValid) {
            this.isSaving = true;
            this.position = this.model.value;
            this.position.officeIds = this.selectedOffices.map(function (item) {
                return item.id;
            });
            if (this.isUpdate) {
                this.positionService
                    .update(this.position)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function () {
                    _this.isModified = true;
                    _this.positionFormModal.dismiss();
                });
            }
            else {
                this.positionService
                    .insert(this.position)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function () {
                    _this.isModified = true;
                    if (!_this.isCreateAnother) {
                        _this.positionFormModal.dismiss();
                    }
                    else {
                        _this.validateModel(false);
                        _this.resetModel();
                    }
                });
            }
        }
    };
    PositionFormComponent.prototype.add = function () {
        this.isUpdate = false;
        this.validateModel(false);
        this.positionFormModal.open();
    };
    PositionFormComponent.prototype.edit = function (position) {
        this.isUpdate = true;
        this.position = position;
        this.getDetail(position.id);
        this.positionFormModal.open();
    };
    PositionFormComponent.prototype.resetModel = function () {
        this.isUpdate = false;
        this.model.patchValue({
            id: '',
            titleId: '',
            isActive: true,
            isManager: false,
            isMultiple: false,
        });
        this.translations.controls.forEach(function (model) {
            model.patchValue({
                name: '',
                shortName: '',
                description: '',
                purpose: '',
                otherRequire: '',
                responsibility: ''
            });
        });
        this.clearFormError(this.formErrors);
        this.clearFormError(this.translationFormErrors);
    };
    PositionFormComponent.prototype.getDetail = function (id) {
        var _this = this;
        this.subscribers.getPositionDetail = this.positionService.getDetail(id)
            .subscribe(function (result) {
            var positionDetail = result.data;
            if (positionDetail) {
                _this.model.patchValue({
                    id: positionDetail.id,
                    isActive: positionDetail.isActive,
                    isMultiple: positionDetail.isMultiple,
                    isManager: positionDetail.isManager,
                    order: positionDetail.order,
                    titleId: positionDetail.titleId,
                    concurrencyStamp: positionDetail.concurrencyStamp,
                });
                if (positionDetail.positionTranslations && positionDetail.positionTranslations.length > 0) {
                    _this.translations.controls.forEach(function (model) {
                        var detail = lodash__WEBPACK_IMPORTED_MODULE_12__["find"](positionDetail.positionTranslations, function (positionTranslation) {
                            return positionTranslation.languageId === model.value.languageId;
                        });
                        if (detail) {
                            model.patchValue(detail);
                        }
                    });
                }
                if (positionDetail.officesPositions && positionDetail.officesPositions.length > 0) {
                    _this.selectedOffices = positionDetail.officesPositions;
                }
            }
        });
    };
    PositionFormComponent.prototype.renderForm = function () {
        this.buildForm();
        this.renderTranslationArray(this.buildFormLanguage);
    };
    PositionFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.utilService.renderFormError(['name', 'shortName', 'description', 'titleId']);
        this.validationMessages = this.utilService.renderFormErrorMessage([
            { 'name': ['required', 'maxlength'] },
            { 'shortName': ['required', 'maxlength'] },
            { 'description': ['maxlength'] },
            { 'titleId': ['required'] },
        ]);
        this.model = this.fb.group({
            id: [this.position.id],
            titleId: [this.position.titleId, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
                ]],
            isManager: [this.position.isManager],
            isMultiple: [this.position.isMultiple],
            isActive: [this.position.isActive],
            concurrencyStamp: [this.position.concurrencyStamp],
            translations: this.fb.array([])
        });
        this.model.valueChanges.subscribe(function (data) {
            return _this.validateFormGroup(_this.model, _this.formErrors, _this.validationMessages);
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('positionFormModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_8__["NhModalComponent"])
    ], PositionFormComponent.prototype, "positionFormModal", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], PositionFormComponent.prototype, "onSaveSuccess", void 0);
    PositionFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-position-form',
            template: __webpack_require__(/*! ./position-form.component.html */ "./src/app/modules/hr/organization/position/position-form/position-form.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_page_id_config__WEBPACK_IMPORTED_MODULE_9__["PAGE_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"],
            _position_service__WEBPACK_IMPORTED_MODULE_4__["PositionService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_10__["UtilService"],
            _office_services_office_service__WEBPACK_IMPORTED_MODULE_14__["OfficeService"],
            _title_title_service__WEBPACK_IMPORTED_MODULE_13__["TitleService"]])
    ], PositionFormComponent);
    return PositionFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_7__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/organization/position/position.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/modules/hr/organization/position/position.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@listPositionPageTitle\">Danh sách chức vụ</span>\r\n    <small i18n=\"@@positionModuleTitle\">Quản lý chức vụ</small>\r\n</h1>\r\n\r\n<form class=\"form-inline cm-mgb-10\" (ngSubmit)=\"search(1)\" #positionSearchForm=\"ngForm\">\r\n    <div class=\"form-group\">\r\n        <ghm-input icon=\"fa fa-search\"\r\n                   i18n-placeholder=\"@@enterSearchKeyWord\"\r\n                   placeholder=\"Nhập từ khóa tìm kiếm\"\r\n                   (remove)=\"search(1)\"\r\n                   [(ngModel)]=\"keyword\" name=\"keyword\"></ghm-input>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <button type=\"submit\" class=\"btn blue\">\r\n            <i class=\"fa fa-search\" *ngIf=\"!isSearching\"></i>\r\n            <i class=\"fa fa-pulse fa-spinner\" *ngIf=\"isSearching\"></i>\r\n        </button>\r\n    </div>\r\n    <!--<div class=\"form-group cm-mgr-5\">-->\r\n        <!--<button type=\"button\" class=\"btn btn-default\" (click)=\"refresh()\">-->\r\n            <!--<i class=\"fa fa-refresh\"></i>-->\r\n        <!--</button>-->\r\n    <!--</div>-->\r\n    <div class=\"form-group pull-right\" *ngIf=\"permission.add\">\r\n        <button type=\"button\" class=\"btn blue\" (click)=\"add()\" i18n=\"@@add\">\r\n            Thêm\r\n        </button>\r\n    </div>\r\n</form><!-- END: search-form -->\r\n\r\n<div class=\"\">\r\n    <table class=\"table table-hover table-stripped\">\r\n        <thead>\r\n        <tr>\r\n            <th class=\"middle center w50\" i18n=\"@@no\">STT</th>\r\n            <th class=\"middle w250\" i18n=\"@@positionName\">Tên chức vụ</th>\r\n            <th class=\"middle w100\" i18n=\"@@shortName\">Tên viết tắt</th>\r\n            <th class=\"middle w100\" i18n=\"@@isManager\">Is manager</th>\r\n            <th class=\"middle w100\" i18n=\"@@isMultiple\">Is multiple</th>\r\n            <th class=\"middle\" i18n=\"@@description\">Mô tả</th>\r\n            <th class=\"middle text-right w100\" i18n=\"@@action\">Thao tác</th>\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n        <tr *ngFor=\"let position of listItems$ | async; let i = index\">\r\n            <td class=\"center middle\">{{ (currentPage - 1) * pageSize + i + 1 }}</td>\r\n            <td class=\"middle\">\r\n                <a href=\"javascript://\" (click)=\"edit(position)\"\r\n                   *ngIf=\"permission.edit; else positionNameWithoutEdit\">{{position.name}}</a>\r\n                <ng-template #positionNameWithoutEdit>\r\n                    {{ position.name }}\r\n                </ng-template>\r\n            </td>\r\n            <td class=\"middle\">{{ position.shortName }}</td>\r\n            <td class=\"middle center\">\r\n                <i class=\"fa fa-check color-green\" *ngIf=\"position.isManager\"></i>\r\n            </td>\r\n            <td class=\"middle center\">\r\n                <i class=\"fa fa-check color-green\" *ngIf=\"position.isMultiple\"></i>\r\n            </td>\r\n            <td class=\"middle\">{{ position.description }}</td>\r\n            <td class=\"text-right middle\">\r\n                <button\r\n                    class=\"btn blue btn-sm\"\r\n                    i18n=\"@@edit\"\r\n                    i18n-matTooltip\r\n                    matTooltip=\"Sửa\"\r\n                    [matTooltipPosition]=\"'above'\"\r\n                    (click)=\"edit(position)\">\r\n                    <i class=\"fa fa-pencil\"></i>\r\n                </button>\r\n                <ghm-button\r\n                    *ngIf=\"permission.delete\"\r\n                    icon=\"fa fa-trash-o\" classes=\"btn btn-danger btn-sm\"\r\n                    [swal]=\"confirmDeletePosition\"\r\n                    (confirm)=\"delete(position.id)\"></ghm-button>\r\n            </td>\r\n        </tr>\r\n        </tbody>\r\n    </table>\r\n</div><!-- END: table-responsive -->\r\n\r\n<ghm-paging [totalRows]=\"totalRows\"\r\n            [currentPage]=\"currentPage\"\r\n            [pageShow]=\"6\"\r\n            [isDisabled]=\"isSearching\"\r\n            [pageSize]=\"pageSize\"\r\n            (pageClick)=\"search($event)\"\r\n></ghm-paging>\r\n\r\n<app-position-form (saveSuccessful)=\"search(currentPage)\"></app-position-form>\r\n\r\n<swal\r\n    #confirmDeletePosition\r\n    i18n-title=\"@@confirmDeletePositionTitle\"\r\n    i18n-text=\"@@confirmDeletePositionText\"\r\n    title=\"Bạn có chắc chắn muốn xóa chức vụ này?\"\r\n    text=\"Bạn không thể khôi phục lại chức vụ này sau khi xóa.\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Đồng Ý\"\r\n    cancelButtonText=\"Hủy\">\r\n</swal>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/organization/position/position.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/modules/hr/organization/position/position.component.ts ***!
  \************************************************************************/
/*! exports provided: PositionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PositionComponent", function() { return PositionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _position_form_position_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./position-form/position-form.component */ "./src/app/modules/hr/organization/position/position-form/position-form.component.ts");
/* harmony import */ var _position_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./position.service */ "./src/app/modules/hr/organization/position/position.service.ts");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _configs_page_id_config__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../configs/page-id.config */ "./src/app/configs/page-id.config.ts");
/* harmony import */ var _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../core/spinner/spinner.service */ "./src/app/core/spinner/spinner.service.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../../shareds/models/filter-link.model */ "./src/app/shareds/models/filter-link.model.ts");















var PositionComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](PositionComponent, _super);
    function PositionComponent(pageId, location, positionService, spinnerService, toastr, router, route, utilService) {
        var _this = _super.call(this) || this;
        _this.pageId = pageId;
        _this.location = location;
        _this.positionService = positionService;
        _this.spinnerService = spinnerService;
        _this.toastr = toastr;
        _this.router = router;
        _this.route = route;
        _this.utilService = utilService;
        _this.searchTerm = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
        return _this;
    }
    PositionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.appService.setupPage(this.pageId.HR, this.pageId.POSITION, 'Quản lý chức vụ', 'Danh sách chức vụ');
        this.searchTerm
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["debounceTime"])(500))
            .subscribe(function (term) {
        });
        this.listItems$ = this.route.data.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            var data = result.data;
            _this.totalRows = data.totalRows;
            return data.items;
        }));
    };
    PositionComponent.prototype.ngAfterContentInit = function () {
    };
    PositionComponent.prototype.search = function (currentPage) {
        var _this = this;
        this.currentPage = currentPage;
        this.renderFilterLink();
        this.isSearching = true;
        this.listItems$ = this.positionService.search(this.keyword, this.isManager, this.isMultiple, this.isActive, this.currentPage, this.pageSize)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.isSearching = false; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            _this.totalRows = result.totalRows;
            return result.items;
        }));
    };
    PositionComponent.prototype.refresh = function () {
        this.isActive = null;
        this.isManager = null;
        this.isMultiple = null;
        this.keyword = '';
        this.search(1);
    };
    PositionComponent.prototype.add = function () {
        this.positionFormComponent.add();
    };
    PositionComponent.prototype.edit = function (position) {
        this.positionFormComponent.edit(position);
    };
    PositionComponent.prototype.delete = function (id) {
        var _this = this;
        this.positionService.delete(id)
            .subscribe(function () { return _this.search(_this.currentPage); });
    };
    PositionComponent.prototype.renderFilterLink = function () {
        var path = '/organization/positions';
        var query = this.utilService.renderLocationFilter([
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_13__["FilterLink"]('pageId', this.currentPage),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_13__["FilterLink"]('pageSize', this.pageSize)
        ]);
        this.location.go(path, query);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_position_form_position_form_component__WEBPACK_IMPORTED_MODULE_7__["PositionFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _position_form_position_form_component__WEBPACK_IMPORTED_MODULE_7__["PositionFormComponent"])
    ], PositionComponent.prototype, "positionFormComponent", void 0);
    PositionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-position-component',
            template: __webpack_require__(/*! ./position.component.html */ "./src/app/modules/hr/organization/position/position.component.html"),
            providers: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"], { provide: _angular_common__WEBPACK_IMPORTED_MODULE_2__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_2__["PathLocationStrategy"] }]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_page_id_config__WEBPACK_IMPORTED_MODULE_10__["PAGE_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"],
            _position_service__WEBPACK_IMPORTED_MODULE_8__["PositionService"],
            _core_spinner_spinner_service__WEBPACK_IMPORTED_MODULE_11__["SpinnerService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_12__["UtilService"]])
    ], PositionComponent);
    return PositionComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_9__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/organization/position/position.model.ts":
/*!********************************************************************!*\
  !*** ./src/app/modules/hr/organization/position/position.model.ts ***!
  \********************************************************************/
/*! exports provided: Position */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Position", function() { return Position; });
var Position = /** @class */ (function () {
    function Position(id, order) {
        this.id = id;
        this.order = order;
        this.isManager = false;
        this.isMultiple = false;
        this.isActive = true;
    }
    return Position;
}());



/***/ }),

/***/ "./src/app/modules/hr/organization/title/models/title-translation.model.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/modules/hr/organization/title/models/title-translation.model.ts ***!
  \*********************************************************************************/
/*! exports provided: TitleTranslation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TitleTranslation", function() { return TitleTranslation; });
var TitleTranslation = /** @class */ (function () {
    function TitleTranslation() {
    }
    return TitleTranslation;
}());



/***/ }),

/***/ "./src/app/modules/hr/organization/title/title-form/title-form.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/modules/hr/organization/title/title-form/title-form.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nh-modal #titleFormModal\r\n          size=\"md\"\r\n          (shown)=\"onTitleFormModalShown()\"\r\n          (onHidden)=\"onTitleFormModalHidden()\">\r\n    <nh-modal-header>\r\n        <i class=\"fa fa-user-secret\" aria-hidden=\"true\"></i>\r\n        <!--<span *ngIf=\"isUpdate; else addNewTitleTemplate\" i18n=\"@@updateTitle\">Update title</span>-->\r\n        <!--<ng-template #addNewTitleTemplate i18N=\"@@addNewTitle\">-->\r\n        <!--Add new title-->\r\n        <!--</ng-template>-->\r\n        <span i18n=\"@@titleFormTitle\">{isUpdate, select, 1 {Cập nhật chức danh} 0 {Thêm mới chức danh}}</span>\r\n    </nh-modal-header>\r\n    <form class=\"form-horizontal\" (ngSubmit)=\"save()\" [formGroup]=\"model\">\r\n        <nh-modal-content>\r\n            <div formArrayName=\"translations\">\r\n                <div class=\"form-group\" *ngIf=\"languages && languages.length > 1\">\r\n                    <label i18n-ghmLabel=\"@@language\" ghmLabel=\"Language\"\r\n                           class=\"col-sm-4 control-label\"></label>\r\n                    <div class=\"col-sm-8\">\r\n                        <ghm-select [data]=\"languages\"\r\n                                    i18n-title=\"@@pleaseSelectLanguage\"\r\n                                    title=\"-- Chọn ngôn ngữ --\"\r\n                                    name=\"language\"\r\n                                    [(value)]=\"currentLanguage\"\r\n                                    (itemSelected)=\"currentLanguage = $event.id\"></ghm-select>\r\n                    </div>\r\n                </div>\r\n                <div *ngFor=\"let modelTranslation of translations.controls; index as i\"\r\n                     [hidden]=\"modelTranslation.value.languageId !== currentLanguage\"\r\n                     [formGroupName]=\"i\">\r\n                    <div class=\"form-group\"\r\n                         [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.name\">\r\n                        <label i18n=\"@@titleName\" i18n-ghmLabel ghmLabel=\"Tên chức danh\"\r\n                               class=\"col-sm-4 control-label\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <ghm-input i18n-placeholder=\"@@enterTitleNamePlaceHolder\"\r\n                                       [elementId]=\"'name'\"\r\n                                       placeholder=\"Nhập tên chức danh.\"\r\n                                       formControlName=\"name\"></ghm-input>\r\n                            <span class=\"help-block\">\r\n                                        {\r\n                                        translationFormErrors[modelTranslation.value.languageId]?.name,\r\n                                        select, required {Tên chức danh không được để trống} maxLength {Tên chức danh không được vượt quá 256 ký tự}\r\n                                        }\r\n                                    </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group\"\r\n                         [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.shortName\">\r\n                        <label i18n=\"@@shortName\" i18n-ghmLabel ghmLabel=\"Tên viết tắt\"\r\n                               class=\"col-sm-4 control-label\"\r\n                               [required]=\"true\"></label>\r\n                        <div class=\"col-sm-8\">\r\n                            <ghm-input i18n-placeholder=\"@@enterTitleShortNamePlaceHolder\"\r\n                                       [elementId]=\"'shortName'\"\r\n                                       placeholder=\"Nhập chức danh viết tắt.\"\r\n                                       formControlName=\"shortName\"></ghm-input>\r\n                            <span class=\"help-block\">\r\n                                        {\r\n                                        translationFormErrors[modelTranslation.value.languageId]?.shortName,\r\n                                        select, required {Chức danh viết tắt không được để trống} maxLength {Chức danh viết tắt không được vượt quá 50 ký tự}\r\n                                        }\r\n                                    </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group\"\r\n                         [class.has-error]=\"translationFormErrors[modelTranslation.value.languageId]?.description\">\r\n                        <label i18n=\"@@description\" i18n-ghmLabel ghmLabel=\"Mô tả\"\r\n                               class=\"col-sm-4 control-label\"\r\n                        ></label>\r\n                        <div class=\"col-sm-8\">\r\n                                    <textarea class=\"form-control\" rows=\"3\" formControlName=\"description\"\r\n                                              i18n=\"@@enterDescriptionPlaceholder\" i18n-placeholder\r\n                                              placeholder=\"Nhập mô tả.\"></textarea>\r\n                            <span class=\"help-block\">\r\n                                        {\r\n                                        translationFormErrors[modelTranslation.value.languageId]?.description,\r\n                                        select, maxLength {Mô tả chức danh không được vượt quá 500 ký tự}\r\n                                        }\r\n                                    </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\" [formGroup]=\"model\">\r\n                    <div class=\"col-sm-8 col-sm-offset-4\">\r\n                        <mat-checkbox color=\"primary\" formControlName=\"isActive\" i18n=\"@@isActive\">\r\n                            {model.value.isActive, select, 0 {Chứa kích hoạt} 1 {Kích hoạt}}\r\n                        </mat-checkbox>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </nh-modal-content>\r\n        <nh-modal-footer>\r\n            <mat-checkbox [checked]=\"isCreateAnother\" (change)=\"isCreateAnother = !isCreateAnother\"\r\n                          *ngIf=\"!isUpdate\"\r\n                          i18n=\"@@isCreateAnother\"\r\n                          class=\"cm-mgr-5\"\r\n                          color=\"primary\">\r\n                Tiếp tục thêm\r\n            </mat-checkbox>\r\n            <ghm-button classes=\"btn blue cm-mgr-5\" [loading]=\"isSaving\" i18n=\"@@save\">\r\n                Lưu\r\n            </ghm-button>\r\n            <ghm-button type=\"button\" classes=\"btn btn-default\"\r\n                        nh-dismiss=\"true\"\r\n                        [loading]=\"isSaving\"\r\n                        i18n=\"@@cancel\">\r\n                Hủy\r\n            </ghm-button>\r\n        </nh-modal-footer>\r\n    </form>\r\n</nh-modal>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/organization/title/title-form/title-form.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/modules/hr/organization/title/title-form/title-form.component.ts ***!
  \**********************************************************************************/
/*! exports provided: TitleFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TitleFormComponent", function() { return TitleFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _title_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../title.model */ "./src/app/modules/hr/organization/title/title.model.ts");
/* harmony import */ var _title_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../title.service */ "./src/app/modules/hr/organization/title/title.service.ts");
/* harmony import */ var _configs_page_id_config__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../configs/page-id.config */ "./src/app/configs/page-id.config.ts");
/* harmony import */ var _base_form_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../base-form.component */ "./src/app/base-form.component.ts");
/* harmony import */ var _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../shareds/components/nh-modal/nh-modal.component */ "./src/app/shareds/components/nh-modal/nh-modal.component.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _models_title_translation_model__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../models/title-translation.model */ "./src/app/modules/hr/organization/title/models/title-translation.model.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_12__);













var TitleFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TitleFormComponent, _super);
    function TitleFormComponent(pageId, fb, route, router, titleService, utilService) {
        var _this = _super.call(this) || this;
        _this.pageId = pageId;
        _this.fb = fb;
        _this.route = route;
        _this.router = router;
        _this.titleService = titleService;
        _this.utilService = utilService;
        _this.title = new _title_model__WEBPACK_IMPORTED_MODULE_5__["Title"]();
        _this.modelTranslation = new _models_title_translation_model__WEBPACK_IMPORTED_MODULE_11__["TitleTranslation"]();
        _this.buildFormLanguage = function (language) {
            _this.translationFormErrors[language] = _this.renderFormError(['name', 'shortName', 'description']);
            _this.translationValidationMessage[language] = _this.renderFormErrorMessage([
                { 'name': ['required', 'maxlength'] },
                { 'description': ['maxlength'] },
                { 'shortName': ['required', 'maxlength'] }
            ]);
            var pageTranslationModel = _this.fb.group({
                languageId: [language],
                name: [_this.modelTranslation.name, [
                        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(256)
                    ]],
                shortName: [_this.modelTranslation.shortName, [
                        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)
                    ]],
                description: [_this.modelTranslation.description, [
                        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(500)
                    ]],
            });
            pageTranslationModel.valueChanges.subscribe(function (data) { return _this.validateTranslation(false); });
            return pageTranslationModel;
        };
        return _this;
    }
    TitleFormComponent.prototype.ngOnInit = function () {
        this.renderForm();
    };
    TitleFormComponent.prototype.onTitleFormModalShown = function () {
        this.utilService.focusElement('name');
    };
    TitleFormComponent.prototype.onTitleFormModalHidden = function () {
        this.resetModels();
        if (this.isModified) {
            this.saveSuccessful.emit();
        }
    };
    TitleFormComponent.prototype.add = function () {
        this.titleFormModal.open();
        this.validateModel(false);
    };
    TitleFormComponent.prototype.edit = function (title) {
        this.isUpdate = true;
        this.title = title;
        this.getDetail(title.id);
        this.titleFormModal.open();
    };
    TitleFormComponent.prototype.save = function () {
        var _this = this;
        var isValid = this.validateModel();
        var isLanguageValid = this.validateTranslation();
        if (isValid && isLanguageValid) {
            this.isSaving = true;
            this.title = this.model.value;
            if (this.isUpdate) {
                this.titleService.update(this.title.id, this.title)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function () {
                    _this.isModified = true;
                    _this.titleFormModal.dismiss();
                });
            }
            else {
                this.titleService.insert(this.title)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () { return _this.isSaving = false; }))
                    .subscribe(function () {
                    _this.utilService.focusElement('name');
                    if (_this.isCreateAnother) {
                        _this.resetModels();
                    }
                    else {
                        _this.isModified = true;
                        _this.titleFormModal.dismiss();
                    }
                });
            }
        }
    };
    TitleFormComponent.prototype.getDetail = function (id) {
        var _this = this;
        this.subscribers.getTitleDetail = this.titleService.getDetail(id)
            .subscribe(function (result) {
            var titleDetail = result.data;
            if (titleDetail) {
                _this.model.patchValue({
                    id: titleDetail.id,
                    isActive: titleDetail.isActive,
                    order: titleDetail.order,
                    concurrencyStamp: titleDetail.concurrencyStamp,
                });
                if (titleDetail.translations && titleDetail.translations.length > 0) {
                    _this.translations.controls.forEach(function (model) {
                        var detail = lodash__WEBPACK_IMPORTED_MODULE_12__["find"](titleDetail.translations, function (titleTranslation) {
                            return titleTranslation.languageId === model.value.languageId;
                        });
                        if (detail) {
                            model.patchValue(detail);
                        }
                    });
                }
            }
        });
    };
    TitleFormComponent.prototype.renderForm = function () {
        this.buildForm();
        this.renderTranslationArray(this.buildFormLanguage);
    };
    TitleFormComponent.prototype.buildForm = function () {
        var _this = this;
        this.formErrors = this.utilService.renderFormError(['name', 'shortName', 'description']);
        this.validationMessages = this.utilService.renderFormErrorMessage([
            { 'name': ['required', 'maxlength'] },
            { 'shortName': ['required', 'maxlength'] },
        ]);
        this.model = this.fb.group({
            'id': [this.title.id],
            'concurrencyStamp': [this.title.concurrencyStamp],
            'isActive': [this.title.isActive],
            'translations': this.fb.array([])
        });
        this.model.valueChanges.subscribe(function (data) { return _this.validateModel(false); });
    };
    TitleFormComponent.prototype.resetModels = function () {
        this.isUpdate = false;
        this.model.patchValue({
            isActive: true,
        });
        this.translations.controls.forEach(function (model) {
            model.patchValue({
                name: '',
                shortName: '',
                description: ''
            });
        });
        this.clearFormError(this.formErrors);
        this.clearFormError(this.translationFormErrors);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('titleFormModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shareds_components_nh_modal_nh_modal_component__WEBPACK_IMPORTED_MODULE_9__["NhModalComponent"])
    ], TitleFormComponent.prototype, "titleFormModal", void 0);
    TitleFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-title-form',
            template: __webpack_require__(/*! ./title-form.component.html */ "./src/app/modules/hr/organization/title/title-form/title-form.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_page_id_config__WEBPACK_IMPORTED_MODULE_7__["PAGE_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _title_service__WEBPACK_IMPORTED_MODULE_6__["TitleService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_10__["UtilService"]])
    ], TitleFormComponent);
    return TitleFormComponent;
}(_base_form_component__WEBPACK_IMPORTED_MODULE_8__["BaseFormComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/organization/title/title.component.html":
/*!********************************************************************!*\
  !*** ./src/app/modules/hr/organization/title/title.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"page-title\">\r\n    <span class=\"cm-mgr-5\" i18n=\"@@listTitlePageTitle\">Danh sách chức danh</span>\r\n    <small i18n=\"@@titleModuleTitle\">Quản lý chức danh</small>\r\n</h1>\r\n\r\n<form class=\"form-inline cm-mgb-10\" (ngSubmit)=\"search(1)\">\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <ghm-input icon=\"fa fa-search\" i18n-placeholder=\"@@enterKeyword\"\r\n                   (remove)=\"search(1)\"\r\n                   placeholder=\"Nhập từ khóa tìm kiếm\"\r\n                   name=\"keyword\" [(ngModel)]=\"keyword\"></ghm-input>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <ghm-select\r\n            i18n-title=\"@@filterByStatusTitle\"\r\n            title=\"-- Tìm kiếm theo trạng thái --\"\r\n            [data]=\"listActiveSearch\"\r\n            [(value)]=\"isActive\"\r\n            (itemSelected)=\"search(1)\"></ghm-select>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <button class=\"btn blue\" [disabled]=\"isSearching\">\r\n            <i class=\"fa fa-search\" *ngIf=\"!isSearching\"></i>\r\n            <i class=\"fa fa-pulse fa-spinner\" *ngIf=\"isSearching\"></i>\r\n        </button>\r\n    </div>\r\n    <div class=\"form-group cm-mgr-5\">\r\n        <button type=\"button\" class=\"btn btn-default\" (click)=\"refresh()\">\r\n            <i class=\"fa fa-refresh\"></i>\r\n        </button>\r\n    </div>\r\n    <div class=\"form-group pull-right\">\r\n        <button class=\"btn blue\" (click)=\"add()\" i18n=\"@@add\"\r\n                *ngIf=\"permission.add\">\r\n            Thêm mới\r\n        </button>\r\n    </div>\r\n</form>\r\n<div class=\"\">\r\n    <table class=\"table table-hover table-stripped\">\r\n        <thead>\r\n        <tr>\r\n            <th class=\"middle center w50\" i18n=\"@@no\">STT</th>\r\n            <th class=\"middle\" i18n=\"@@titleName\">Tên chức danh</th>\r\n            <th class=\"middle w100\" i18n=\"@@shortName\">Tên viết tắt</th>\r\n            <th class=\"middle w70 middle\" i18n=\"@@status\">Trạng thái</th>\r\n            <th class=\"middle\" i18n=\"@@description\">Môt tả</th>\r\n            <th class=\"middle text-right w150\" i18n=\"@@action\" *ngIf=\"permission.edit || permission.delete\">Thao tác\r\n            </th>\r\n        </tr>\r\n        </thead>\r\n        <tbody>\r\n        <tr *ngFor=\"let title of listItems$ | async; let i = index\">\r\n            <td class=\"center middle\">{{ (currentPage - 1) * pageSize + i + 1 }}</td>\r\n            <td class=\"middle\">\r\n                <a href=\"javascript://\"\r\n                   (click)=\"edit(title)\"\r\n                   *ngIf=\"permission.edit; else noEditTemplate\">{{ title.name }}</a>\r\n                <ng-template #noEditTemplate>\r\n                    {{ title.name }}\r\n                </ng-template>\r\n            </td>\r\n            <td class=\"middle\">{{ title.shortName }}</td>\r\n            <td class=\"middle\">\r\n                <span class=\"badge\"\r\n                      [class.badge-success]=\"title.isActive\"\r\n                      [class.badge-danger]=\"!title.isActive\">{title.activeStatus, select, active {Kích hoạt} inActive {Chưa kích hoạt}}</span>\r\n            </td>\r\n            <td class=\"middle\">{{ title.description }}</td>\r\n            <td class=\"middle text-right w150\" *ngIf=\"permission.edit || permission.delete\">\r\n                <ghm-button\r\n                    *ngIf=\"permission.edit\"\r\n                    icon=\"fa fa-edit\" classes=\"btn blue btn-sm\"\r\n                    (clicked)=\"edit(title)\"></ghm-button>\r\n                <ghm-button\r\n                    *ngIf=\"permission.delete\"\r\n                    icon=\"fa fa-trash-o\" classes=\"btn red btn-sm\"\r\n                    [swal]=\"confirmDeleteTitle\"\r\n                    (confirm)=\"delete(title.id)\"></ghm-button>\r\n            </td>\r\n        </tr>\r\n        </tbody>\r\n    </table>\r\n</div>\r\n<ghm-paging [totalRows]=\"totalRows\"\r\n            [currentPage]=\"currentPage\"\r\n            [pageShow]=\"6\"\r\n            [isDisabled]=\"isSearching\"\r\n            [pageSize]=\"pageSize\"\r\n            (pageClick)=\"search($event)\"\r\n></ghm-paging>\r\n\r\n<app-title-form (saveSuccessful)=\"search(currentPage)\"></app-title-form>\r\n\r\n<swal\r\n    #confirmDeleteTitle\r\n    i18n-title=\"@@confirmDeleteTitle\"\r\n    i18n-text=\"@@confirmDeleteText\"\r\n    title=\"Bạn có muốn xóa chức danh này?\"\r\n    text=\"Bạn không thể khôi phục chức danh này sau khi xóa.\"\r\n    type=\"question\"\r\n    i18n-confirmButtonText=\"@@accept\"\r\n    i18n-cancelButtonText=\"@@cancel\"\r\n    confirmButtonText=\"Đồng ý\"\r\n    cancelButtonText=\"Hủy\">\r\n</swal>\r\n"

/***/ }),

/***/ "./src/app/modules/hr/organization/title/title.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/modules/hr/organization/title/title.component.ts ***!
  \******************************************************************/
/*! exports provided: TitleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TitleComponent", function() { return TitleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _title_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./title.model */ "./src/app/modules/hr/organization/title/title.model.ts");
/* harmony import */ var _title_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./title.service */ "./src/app/modules/hr/organization/title/title.service.ts");
/* harmony import */ var _title_form_title_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./title-form/title-form.component */ "./src/app/modules/hr/organization/title/title-form/title-form.component.ts");
/* harmony import */ var _base_list_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../base-list.component */ "./src/app/base-list.component.ts");
/* harmony import */ var _configs_page_id_config__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../configs/page-id.config */ "./src/app/configs/page-id.config.ts");
/* harmony import */ var _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../shareds/services/util.service */ "./src/app/shareds/services/util.service.ts");
/* harmony import */ var _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../shareds/models/filter-link.model */ "./src/app/shareds/models/filter-link.model.ts");












var TitleComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TitleComponent, _super);
    function TitleComponent(pageId, location, titleService, utilService, router, route) {
        var _this = _super.call(this) || this;
        _this.pageId = pageId;
        _this.location = location;
        _this.titleService = titleService;
        _this.utilService = utilService;
        _this.router = router;
        _this.route = route;
        _this.title = new _title_model__WEBPACK_IMPORTED_MODULE_5__["Title"]();
        _this.listActiveSearch = [];
        _this.subscribers.getListActiveSearch = _this.appService.getListActiveSearch()
            .subscribe(function (result) { return _this.listActiveSearch = result; });
        return _this;
    }
    TitleComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.appService.setupPage(this.pageId.HR, this.pageId.TITLE, 'Quản lý chức danh', 'Danh sách chức danh');
        this.listItems$ = this.route.data.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) {
            var data = result.data;
            _this.totalRows = data.totalRows;
            return data.items;
        }));
        this.subscribers.queryParams = this.route.queryParams.subscribe(function (queryParam) {
            if (queryParam.isActive) {
                _this.isActive = !!queryParam.isActive;
            }
            if (queryParam.page) {
                _this.currentPage = parseInt(queryParam.page);
            }
            if (queryParam.pageSize) {
                _this.pageSize = parseInt(queryParam.pageSize);
            }
        });
    };
    TitleComponent.prototype.search = function (currentPage) {
        var _this = this;
        this.currentPage = currentPage;
        this.renderFilterLink();
        this.isSearching = true;
        this.listItems$ = this.titleService.search(this.keyword, this.isActive, this.currentPage, this.pageSize)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(function () { return _this.isSearching = false; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (data) {
            _this.totalRows = data.totalRows;
            return data.items;
        }));
    };
    TitleComponent.prototype.refresh = function () {
        this.keyword = '';
        this.isActive = null;
        this.search(1);
    };
    TitleComponent.prototype.add = function () {
        this.titleFormComponent.add();
    };
    TitleComponent.prototype.edit = function (title) {
        this.titleFormComponent.edit(title);
    };
    TitleComponent.prototype.delete = function (id) {
        var _this = this;
        this.titleService.delete(id)
            .subscribe(function () {
            _this.search(_this.currentPage);
            return;
        });
    };
    TitleComponent.prototype.renderFilterLink = function () {
        var path = '/organization/titles';
        var query = this.utilService.renderLocationFilter([
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_11__["FilterLink"]('keyword', this.keyword),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_11__["FilterLink"]('isActive', this.isActive),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_11__["FilterLink"]('pageId', this.currentPage),
            new _shareds_models_filter_link_model__WEBPACK_IMPORTED_MODULE_11__["FilterLink"]('pageSize', this.pageSize)
        ]);
        this.location.go(path, query);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_title_form_title_form_component__WEBPACK_IMPORTED_MODULE_7__["TitleFormComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _title_form_title_form_component__WEBPACK_IMPORTED_MODULE_7__["TitleFormComponent"])
    ], TitleComponent.prototype, "titleFormComponent", void 0);
    TitleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-title-component',
            template: __webpack_require__(/*! ./title.component.html */ "./src/app/modules/hr/organization/title/title.component.html"),
            providers: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"], { provide: _angular_common__WEBPACK_IMPORTED_MODULE_4__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_4__["PathLocationStrategy"] }]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_configs_page_id_config__WEBPACK_IMPORTED_MODULE_9__["PAGE_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"],
            _title_service__WEBPACK_IMPORTED_MODULE_6__["TitleService"],
            _shareds_services_util_service__WEBPACK_IMPORTED_MODULE_10__["UtilService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], TitleComponent);
    return TitleComponent;
}(_base_list_component__WEBPACK_IMPORTED_MODULE_8__["BaseListComponent"]));



/***/ }),

/***/ "./src/app/modules/hr/organization/title/title.model.ts":
/*!**************************************************************!*\
  !*** ./src/app/modules/hr/organization/title/title.model.ts ***!
  \**************************************************************/
/*! exports provided: Title */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Title", function() { return Title; });
var Title = /** @class */ (function () {
    function Title(name, shortName, description, isActive) {
        this.name = name;
        this.shortName = shortName;
        this.description = description;
        this.isActive = isActive ? isActive : true;
    }
    return Title;
}());



/***/ })

}]);
//# sourceMappingURL=default~modules-hr-organization-organization-module~modules-warehouse-goods-goods-module.js.map