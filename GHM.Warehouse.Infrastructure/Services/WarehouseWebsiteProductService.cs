﻿using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.Services;
using GHM.Infrastructure.ViewModels;
using GHM.Warehouse.Domain.Constants;
using GHM.Warehouse.Domain.IRepository;
using GHM.Warehouse.Domain.IServices;
using GHM.Warehouse.Domain.ModelMetas;
using GHM.Warehouse.Domain.Models;
using GHM.Warehouse.Domain.Resources;
using GHM.Warehouse.Domain.ViewModels;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Warehouse.Infrastructure.Services
{
    public class WarehouseWebsiteProductService : IWarehouseWebsiteProductService
    {
        private readonly IWarehouseWebsiteConfigRepository _warehouseWebsiteConfigRepository;
        private readonly IWarehouseWebsiteProductRepository _warehouseWebsiteProductRepository;
        private readonly IWebsiteProductTranslationRepository _websiteProductTranslationRepository;
        private readonly IWebsiteProductCategoriesRepository _websiteProductCategoriesRepository;
        private readonly IProductRepository _productRepository;
        private readonly IConfiguration _configuration;
        private readonly IProductTranslationRepository _productTranslationRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmWarehouseResource> _warehouseResourceService;

        public WarehouseWebsiteProductService(IResourceService<SharedResource> sharedResourceService,
            IResourceService<GhmWarehouseResource> warehouseResourceService,
            IWarehouseWebsiteProductRepository warehouseWebsiteProductRepository,
            IWarehouseWebsiteConfigRepository warehouseWebsiteConfigRepository,
            IWebsiteProductCategoriesRepository websiteProductCategoriesRepository,
            IWebsiteProductTranslationRepository websiteProductTranslationRepository,
            IProductRepository productRepository,
            IConfiguration configuration,
            IProductTranslationRepository productTranslationRepository)
        {
            _websiteProductCategoriesRepository = websiteProductCategoriesRepository;
            _productTranslationRepository = productTranslationRepository;
            _productRepository = productRepository;
            _websiteProductTranslationRepository = websiteProductTranslationRepository;
            _warehouseWebsiteConfigRepository = warehouseWebsiteConfigRepository;
            _warehouseWebsiteProductRepository = warehouseWebsiteProductRepository;
            _sharedResourceService = sharedResourceService;
            _warehouseResourceService = warehouseResourceService;
            _configuration = configuration;
        }

        public async Task<ActionResultResponse> DeleteAsync(string tenantId, string websiteId, string productId)
        {
            var warehouseInfo = await _warehouseWebsiteConfigRepository.GetWarehouseByWebsite(tenantId, websiteId);

            if (warehouseInfo == null)
                return new ActionResultResponse(-2, _warehouseResourceService.GetString("Warehouse does not exist"));

            var websiteExist = await _warehouseWebsiteConfigRepository.CheckExist(tenantId, warehouseInfo.WarehouseId, websiteId);

            if (!websiteExist)
                return new ActionResultResponse(-1, _warehouseResourceService.GetString("Website does not exist"));

            var info = await _warehouseWebsiteProductRepository.GetDetail(tenantId, warehouseInfo.WarehouseId, websiteId, productId, false);

            if (info == null)
                return new ActionResultResponse(-2, _warehouseResourceService.GetString("Product website does not exist"));

            info.IsDelete = true;

            return await _warehouseWebsiteProductRepository.Delete(info) < 0 ? new ActionResultResponse(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong))
                : new ActionResultResponse(1, _warehouseResourceService.GetString("Delete successfully!"));
        }

        public async Task<ActionResultResponse<WebsiteProductDetailViewModel>> GetDetailAsync(string tenantId, string websiteId, string productId, string languageId)
        {
            var warehouseInfo = await _warehouseWebsiteConfigRepository.GetWarehouseByWebsite(tenantId, websiteId);

            if (warehouseInfo == null)
                return new ActionResultResponse<WebsiteProductDetailViewModel>(-2, _warehouseResourceService.GetString("Warehouse does not exist"));

            var websiteProduct = await _warehouseWebsiteProductRepository.GetDetails(tenantId, warehouseInfo.WarehouseId, websiteId, productId, languageId);

            return new ActionResultResponse<WebsiteProductDetailViewModel>
            {
                Code = 1,
                Data = websiteProduct
            };
        }

        public async Task<SearchResult<WebsiteProductViewModel>> SearchAsync(string tenantId, string websiteId, int? categoryId, string productName, string languageId, int page, int pageSize)
        {
            var warehouseInfo = await _warehouseWebsiteConfigRepository.GetWarehouseByWebsite(tenantId, websiteId);

            if (warehouseInfo == null)
                return new SearchResult<WebsiteProductViewModel>(-2, _warehouseResourceService.GetString("Warehouse does not exist"));

            var items = await _warehouseWebsiteProductRepository.Search(tenantId, warehouseInfo.WarehouseId, websiteId, categoryId, productName, languageId, page, pageSize, out int totalRows);

            return new SearchResult<WebsiteProductViewModel>
            {
                Code = 1,
                Items = items,
                TotalRows = totalRows
            };
        }

        public async Task<SearchResult<ProductSearchForSelectViewModel>> SearchForSelect(string tenantId, string languageId, string websiteId, string keyword, int? categoryId, int page, int pageSize)
        {
            var warehouseInfo = await _warehouseWebsiteConfigRepository.GetWarehouseByWebsite(tenantId, websiteId);

            if (warehouseInfo == null)
                return new SearchResult<ProductSearchForSelectViewModel>(-2, _warehouseResourceService.GetString("Warehouse does not exist"));

            var items = await _warehouseWebsiteProductRepository.SearchForSelectAsync(tenantId, warehouseInfo.WarehouseId, websiteId, keyword, languageId, page, pageSize, out int totalRows);

            return new SearchResult<ProductSearchForSelectViewModel>
            {
                Code = 1,
                Items = items,
                TotalRows = totalRows
            };

        }

        public async Task<SearchResult<WebsiteProductSuggestViewModel>> SearchForSuggestion(string tenantId, string warehouseId, string websiteId, string productId)
        {
            var items = await _warehouseWebsiteProductRepository.SearchForSuggestion(tenantId, warehouseId, websiteId, productId);

            return new SearchResult<WebsiteProductSuggestViewModel>
            {
                Items = items
            };
        }

        public async Task<ActionResultResponse> UpdateAsync(string tenantId, string websiteId, string productId, string languageId, WebsiteProductMeta websiteProductMeta)
        {
            var warehouseInfo = await _warehouseWebsiteConfigRepository.GetWarehouseByWebsite(tenantId, websiteId);

            if (warehouseInfo == null)
                return new ActionResultResponse(-2, _warehouseResourceService.GetString("Warehouse does not exist"));

            var productExist = await _productRepository.CheckExists(productId, tenantId);

            if (!productExist)
                return new ActionResultResponse(-1, _warehouseResourceService.GetString("Product does not exist"));

            var info = await _warehouseWebsiteProductRepository.GetDetail(tenantId, warehouseInfo.WarehouseId, websiteId, productId, false);

            if (info == null)
            {
                var websiteExist = await _warehouseWebsiteConfigRepository.CheckExist(tenantId, warehouseInfo.WarehouseId, websiteId);

                if (!websiteExist)
                    return new ActionResultResponse(-3, _warehouseResourceService.GetString("Website does not exist"));

                var websiteProduct = new WebsiteProduct
                {
                    CreatorId = websiteProductMeta.CreatorId,
                    CreatorFullName = websiteProductMeta.CreatorFullName,
                    WarehouseId = warehouseInfo.WarehouseId,
                    WebsiteId = websiteId,
                    ProductId = productId,
                    TenantId = tenantId,
                    IsHot = websiteProductMeta.IsHot,
                    IsHomePage = websiteProductMeta.IsHomePage
                };
                var resultInsert = await _warehouseWebsiteProductRepository.Insert(websiteProduct);

                if (resultInsert < 0)
                    return new ActionResultResponse(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
            }
            else
            {
                info.IsHot = websiteProductMeta.IsHot;
                info.IsHomePage = websiteProductMeta.IsHomePage;

                var resultUpdate = await _warehouseWebsiteProductRepository.Update(info);

                if (resultUpdate < 0)
                    return new ActionResultResponse(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
            }


            var apiUrls = _configuration.GetApiUrl();
            var httpClient = new HttpClientService();

            if (apiUrls == null)
                return new ActionResultResponse<string>(-5,
                    _sharedResourceService.GetString(
                        "Missing some configuration. Please contact with administrator."));

            var newsTags = new List<TagSubjectViewModel>();

            foreach (var productTranslation in websiteProductMeta.ModelTranslations)
            {
                var infoTranslation = await _websiteProductTranslationRepository.GetInfo(tenantId, warehouseInfo.WarehouseId,
                    websiteId, productId, productTranslation.LanguageId, false);

                if (infoTranslation == null)
                {
                    var checkProductExist = await _warehouseWebsiteProductRepository.CheckProductExistAsync(tenantId, warehouseInfo.WarehouseId, websiteId, productId);

                    if (!checkProductExist)
                        return new ActionResultResponse(-3, _warehouseResourceService.GetString("Product does not exist"));

                    var checkSeoLinkExist = await _websiteProductTranslationRepository.CheckSeoLinkExistAsync(tenantId, websiteId, productTranslation.SeoLink);

                    if (checkSeoLinkExist)
                        return new ActionResultResponse(-3, _warehouseResourceService.GetString("Seo link does exist ! please update try again"));

                    var websiteProductTranslation = new WebsiteProductTranslation
                    {
                        ProductId = productId,
                        TenantId = tenantId,
                        WebsiteId = websiteId,
                        WarehouseId = warehouseInfo.WarehouseId,
                        SeoLink = productTranslation.SeoLink,
                        Alt = productTranslation.Alt,
                        Content = productTranslation.Content,
                        MetaDescription = productTranslation.MetaDescription,
                        MetaKeyword = productTranslation.MetaKeyword,
                        LanguageId = productTranslation.LanguageId
                    };

                    await _websiteProductTranslationRepository.Insert(websiteProductTranslation);
                }
                else
                {
                    if (infoTranslation.SeoLink != productTranslation.SeoLink)
                    {
                        var checkSeoLinkExist = await _websiteProductTranslationRepository.CheckSeoLinkExistAsync(tenantId, websiteId, productTranslation.SeoLink);

                        if (checkSeoLinkExist)
                            return new ActionResultResponse(-3, _warehouseResourceService.GetString("Seo link does exist ! please update try again"));
                    }

                    infoTranslation.SeoLink = productTranslation.SeoLink;
                    infoTranslation.MetaDescription = productTranslation.MetaDescription;
                    infoTranslation.MetaKeyword = productTranslation.MetaKeyword;
                    infoTranslation.Content = productTranslation.Content;
                    infoTranslation.Alt = productTranslation.Alt;

                    await _websiteProductTranslationRepository.Update(infoTranslation);
                }
                if(productTranslation.Tags != null) {
                    var videoTagInsert = new TagSubjectViewModel
                    {
                        TenantId = tenantId,
                        CreatorId = websiteProductMeta.CreatorId,
                        CreatorFullName = websiteProductMeta.CreatorFullName,
                        CreatorAvatar = websiteProductMeta.CreatorAvatar,
                        Type = TagType.Product,
                        SubjectId = info.ProductId,
                        LanguageId = productTranslation.LanguageId.Trim(),
                        Tags = productTranslation.Tags
                    };
                    newsTags.Add(videoTagInsert);
                }
            }

            if(newsTags.Count() > 0) { 
                var resultTag = await httpClient.PostAsync<ActionResultResponse>($"{apiUrls.CoreApiUrl}tags", newsTags);

                if (resultTag.Code < 0)
                    return new ActionResultResponse(-5, _warehouseResourceService.GetString("Something went wrogn when add tag"));
            }
            // Insert Category
            var resultDelete = await _websiteProductCategoriesRepository.DeleteByProductsById(info.ProductId, warehouseInfo.WarehouseId, tenantId, websiteId);

            if (resultDelete < 0)
                return new ActionResultResponse(-5, _warehouseResourceService.GetString("Something went wrong when add categories"));

            foreach (var categoriesId in websiteProductMeta.Categories)
            {
                var categoryproduct = new WebsiteProductCategories
                {
                    ProductId = info.ProductId,
                    ProductWebsiteCategoryId = categoriesId,
                    TenantId = tenantId,
                    WarehouseId = warehouseInfo.WarehouseId,
                    WebsiteId = websiteId
                };

                await _websiteProductCategoriesRepository.Insert(categoryproduct);
            }


            return new ActionResultResponse(1, _warehouseResourceService.GetString("Update product successfully!"));
        }

        public async Task<ActionResultResponse> UpdateHomePage(string tenantId, string websiteId, string productid, bool isHomePage)
        {
            var warehouseInfo = await _warehouseWebsiteConfigRepository.GetWarehouseByWebsite(tenantId, websiteId);

            if (warehouseInfo == null)
                return new ActionResultResponse(-2, _warehouseResourceService.GetString("Warehouse does not exist"));

            var info = await _warehouseWebsiteProductRepository.GetDetail(tenantId, warehouseInfo.WarehouseId, websiteId, productid, false);

            info.IsHomePage = isHomePage;

            return await _warehouseWebsiteProductRepository.Update(info) < 0 ? new ActionResultResponse(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong))
                : new ActionResultResponse(1, _warehouseResourceService.GetString("Update is homepage successfully"));
        }

        public async Task<ActionResultResponse> UpdateHot(string tenantId, string websiteId, string productid, bool isHot)
        {
            var warehouseInfo = await _warehouseWebsiteConfigRepository.GetWarehouseByWebsite(tenantId, websiteId);

            if (warehouseInfo == null)
                return new ActionResultResponse(-2, _warehouseResourceService.GetString("Warehouse does not exist"));

            var info = await _warehouseWebsiteProductRepository.GetDetail(tenantId, warehouseInfo.WarehouseId, websiteId, productid, false);

            info.IsHot = isHot;

            return await _warehouseWebsiteProductRepository.Update(info) < 0 ? new ActionResultResponse(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong))
                : new ActionResultResponse(1, _warehouseResourceService.GetString("Update is Hot successfully"));
        }
    }
}