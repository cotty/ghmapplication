﻿using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.Services;
using GHM.Infrastructure.ViewModels;
using GHM.Warehouse.Domain.IRepository;
using GHM.Warehouse.Domain.IServices;
using GHM.Warehouse.Domain.ModelMetas;
using GHM.Warehouse.Domain.Models;
using GHM.Warehouse.Domain.Resources;
using GHM.Warehouse.Domain.ViewModels;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Warehouse.Infrastructure.Services
{
    public class WebsiteCategoryService : IWebsiteCategoryService
    {
        private readonly IWebsiteProductCategoriesRepository _websiteProductCategoriesRepository;
        private readonly IWebsiteCategoryRepository _websiteCategoryRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmWarehouseResource> _resourceService;
        private readonly IWarehouseWebsiteConfigRepository _warehouseWebsiteConfigRepository;
        private readonly IWebsiteCategoryTranslationRepository _websiteCategoryTranslationRepository;
        private IConfiguration _configuration;
        public WebsiteCategoryService(
                IWebsiteCategoryTranslationRepository websiteCategoryTranslationRepository,
                IWarehouseWebsiteConfigRepository warehouseWebsiteConfigRepository,
                IWebsiteCategoryRepository websiteCategoryRepository,
                IResourceService<SharedResource> sharedResourceService,
                IResourceService<GhmWarehouseResource> resourceService,
                IWebsiteProductCategoriesRepository websiteProductCategoriesRepository,
                IConfiguration configuration
            )
        {
            _websiteCategoryTranslationRepository = websiteCategoryTranslationRepository;
            _warehouseWebsiteConfigRepository = warehouseWebsiteConfigRepository;
            _websiteProductCategoriesRepository = websiteProductCategoriesRepository;
            _configuration = configuration;
            _websiteCategoryRepository = websiteCategoryRepository;
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
        }
        public async Task<ActionResultResponse> Delete(string tenantId, string websiteId, int id)
        {
            var info = await _websiteCategoryRepository.GetInfo(tenantId, websiteId, id, false);

            if (info == null)
                return new ActionResultResponse(-1, _resourceService.GetString("Category does not exist"));
            //check menu exist
            var request = _configuration.GetApiThrowServiceInfo();
            var httpClient = new HttpThrowClientService();
            var isExistsInMenu = await httpClient.GetAsync<ActionResultResponse<bool>>($"{request.ApiGatewayUrl}api/v1/website/menus/check-subject/{websiteId}/{id}/{SubjectType.ProductCategory}");

            if (isExistsInMenu.Data)
                return new ActionResultResponse(-2, _resourceService.GetString("Category are being used by menu. You can not delete this category"));
            //check product exist
            var checkExist = await _websiteProductCategoriesRepository.CheckCategoryExist(tenantId, websiteId, id);

            if (checkExist)
                return new ActionResultResponse(-3, _resourceService.GetString("Category are being used by product. You cannot delete"));

            info.IsDelete = true;
            return await _websiteCategoryRepository.Delete(info) < 0 ? new ActionResultResponse(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong))
                : new ActionResultResponse(1, _resourceService.GetString("Delete category successfully!"));
        }

        public async Task<ActionResultResponse<WebsiteProductCategoryViewModel>> GetDetailAsync(string tenantId, string websiteId, int id)
        {
            var categoryInfo = await _websiteCategoryRepository.GetInfo(tenantId, websiteId, id, true);

            if (categoryInfo == null)
                return new ActionResultResponse<WebsiteProductCategoryViewModel>(-1, _resourceService.GetString("category does not exist"));

            var categoryDetail = new WebsiteProductCategoryViewModel
            {
                Id = categoryInfo.Id,
                ParentId = categoryInfo.ParentId,
                ConcurrencyStamp = categoryInfo.ConcurrencyStamp,
                IsActive = categoryInfo.IsActive,
                Order = categoryInfo.Order,
                BannerImage = categoryInfo.BannerImage,
                CategoryTranslations = await _websiteCategoryTranslationRepository.GetByCategoryId(websiteId, id)
            };

            return new ActionResultResponse<WebsiteProductCategoryViewModel>
            {
                Code = 1,
                Data = categoryDetail
    };
        }

        public async Task<SearchResult<TreeData>> GetTreeAsync(string tenantId, string websiteId, string keyword, string languageId, bool isActive)
        {
            var categories = await _websiteCategoryRepository.SearchAll(tenantId, websiteId, keyword, languageId, isActive);

            return new SearchResult<TreeData>
            {
                Items = await RenderCategoryTree(null)
            };

            #region Local functions
            async Task<List<TreeData>> RenderCategoryTree(int? parentId)
            {
                var tree = new List<TreeData>();
                var childrenCategories = categories.Where(x => x.ParentId == parentId).ToList();

                if(childrenCategories.Any())
                {
                    foreach(var category in childrenCategories.OrderBy(x => x.Order))
                    {
                        var childrens = await RenderCategoryTree(category.Id);
                        tree.Add(new TreeData
                        {
                             Id =category.Id,
                             ChildCount = childrens.Count,
                             Children = childrens,
                             Data = category,
                             ParentId = category.ParentId,
                             IdPath = category.IdPath,
                             Text = category.Name
                        });

                    }
                }
                return tree;
            }
            #endregion
        }

        public async Task<ActionResultResponse<int>> InsertAsync(string tenantId, string websiteId, WebsiteCategoryMeta websiteCategoryMeta)
        {
            var checkWebsiteExist = await _warehouseWebsiteConfigRepository.CheckExist(tenantId, websiteId);

            if (!checkWebsiteExist)
                return new ActionResultResponse<int>(-1, "Website does not config");

            var category = new WebsiteProductCategory
            {
                TenantId = tenantId,
                WebsiteId = websiteId,
                ConcurrencyStamp = Guid.NewGuid().ToString(),
                CreatorId = websiteCategoryMeta.CreatorId,
                CreatorFullName = websiteCategoryMeta.CreatorFullName,
                IsActive = websiteCategoryMeta.IsActive,
                BannerImage = websiteCategoryMeta.BannerImage,
                IdPath = "-1"
            };

            if(websiteCategoryMeta.ParentId.HasValue)
            {
                var parentInfo = await _websiteCategoryRepository.GetInfo(tenantId, websiteId, websiteCategoryMeta.ParentId, true);

                if (parentInfo == null)
                    return new ActionResultResponse<int>(-1, _resourceService.GetString("Parent category does not exist"));

                category.ParentId = parentInfo.Id;
                category.IdPath = $"{parentInfo.IdPath}.-1";
                var order = await _websiteCategoryRepository.CountByParentId(websiteId, websiteCategoryMeta.ParentId);
                category.Order = order;
                category.OrderPath = $"{parentInfo.OrderPath}.{order}";
            }
            else
            {
                var order = await _websiteCategoryRepository.CountByParentId(websiteId, null);
                category.Order = order;
                category.OrderPath = order.ToString();
            }

            var result = await _websiteCategoryRepository.Insert(category);
            if (result <= 0)
                return new ActionResultResponse<int>(-1, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await UpdateIdPath();
            return await AddTranslation();

            async Task RollbackInsert()
            {
                await _websiteCategoryRepository.ForceDelete(tenantId, websiteId ,category.Id);
            }

            async Task UpdateIdPath()
            {
                category.IdPath = category.IdPath.Replace("-1", category.Id.ToString());
                await _websiteCategoryRepository.UpdateCategoryIdPathAsync(tenantId, websiteCategoryMeta, category.Id, category.IdPath);
            }

            async Task<ActionResultResponse<int>> AddTranslation()
            {
                var websiteCategoryTranslation = new List<WebsiteProductCategoryTranslation>();

                foreach(var categoryTranslation in websiteCategoryMeta.CategoryTranslations)
                {
                    var resultAddTranslation = await AddCategoryTranslation(tenantId, websiteId, category.Id, categoryTranslation);
                    if(resultAddTranslation.Code < 0)
                    {
                        await RollbackInsert();
                        return new ActionResultResponse<int>(-3, _resourceService.GetString("insert category translation have something went wrong!")); ;
                    }

                    websiteCategoryTranslation.Add(resultAddTranslation.Data);
                }
                var resultInsertCategoryTranslation = await _websiteCategoryTranslationRepository.Inserts(websiteCategoryTranslation);
                if (resultInsertCategoryTranslation > 0)
                    return new ActionResultResponse<int>(1, _resourceService.GetString("Add new category successful."));

                await RollbackInsert();
                return new ActionResultResponse<int>(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
            }
        }

        public async Task<SearchResult<WebsiteCategorySearchForSelectViewModel>> SearchForSelect(string tenantId, string websiteId, string keyword, string languageId, int page, int pageSize)
        {
            return new SearchResult<WebsiteCategorySearchForSelectViewModel>
            {
                Items = await _websiteCategoryRepository.SearchForSelect(tenantId, websiteId, languageId, keyword, page, pageSize, out var totalRows),
                TotalRows = totalRows
            };
        }

        public async Task<SearchResult<Suggestion<int>>> Suggestions(string tenantId, string websiteId, string keyword, string languageId, int page, int pageSize)
        {
            return new SearchResult<Suggestion<int>>
            {
                Items = await _websiteCategoryRepository.Suggestions(tenantId, websiteId, languageId, keyword, page, pageSize, out var totalRows),
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse<string>> Update(string tenantId, string websiteId, int id, WebsiteCategoryMeta categoryMeta)
        {
            var info = await _websiteCategoryRepository.GetInfo(tenantId, websiteId ,id, false);
            if (info == null)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Category does not exists."));

            if (categoryMeta.ConcurrencyStamp != info.ConcurrencyStamp)
                return new ActionResultResponse<string>(-2,
                    _resourceService.GetString("This category already updated by another. You can not update this category."));

            var oldIdPath = info.IdPath;
            info.IsActive = categoryMeta.IsActive;
            info.Order = categoryMeta.Order;
            info.OrderPath = categoryMeta.Order.ToString();
            info.ConcurrencyStamp = Guid.NewGuid().ToString();
            info.LastUpdate = DateTime.Now;
            info.BannerImage = categoryMeta.BannerImage;
            if (info.ParentId != categoryMeta.ParentId)
            {
                if (!categoryMeta.ParentId.HasValue)
                {
                    info.ParentId = null;
                    info.IdPath = info.Id.ToString();

                    var order = await _websiteCategoryRepository.CountByParentId(websiteId, null);
                    info.Order = order;
                    info.OrderPath = order.ToString();
                }
                else
                {
                    if (categoryMeta.ParentId == info.Id)
                        return new ActionResultResponse<string>(-3, _resourceService.GetString("Parent category can not be it selft."));


                    var parentInfo = await _websiteCategoryRepository.GetInfo(tenantId, websiteId, categoryMeta.ParentId.Value, false);
                    if (parentInfo == null)
                        return new ActionResultResponse<string>(-3, _resourceService.GetString("Parent category does not exists."));

                    info.ParentId = parentInfo.Id;
                    info.IdPath = $"{parentInfo.IdPath}.{info.Id}";

                    var order = await _websiteCategoryRepository.CountByParentId(websiteId, parentInfo.Id);
                    info.Order = order;
                    info.OrderPath = $"{parentInfo.OrderPath}.{order}";
                }
            }

            await _websiteCategoryRepository.Update(info);
            await UpdateChildrenIdPath();
            var resultUpdateTranslation = await SaveCategoryTranslation();
            if (resultUpdateTranslation.Code <= 0)
                return new ActionResultResponse<string>(-5, _resourceService.GetString("Update category translation have something wrong !"));

            return new ActionResultResponse<string>(1, _resourceService.GetString("Update categogry successful."), "", info.ConcurrencyStamp);

            #region Local functions
            async Task<ActionResultResponse> SaveCategoryTranslation()
            {
                var listNewCategoryTranslations = new List<WebsiteProductCategoryTranslation>();
                foreach (var categoryTranslation in categoryMeta.CategoryTranslations)
                {
                    // Check name exists.
                    var isNameExists = await _websiteCategoryTranslationRepository.CheckExists(tenantId, websiteId,
                        categoryTranslation.LanguageId, info.Id, categoryTranslation.Name);
                    if (isNameExists)
                        return new ActionResultResponse<WebsiteProductCategoryTranslation>(-4,
                            _resourceService.GetString("Category name: \"{0}\" already exsits.", categoryTranslation.SeoLink));

                    categoryTranslation.SeoLink = !string.IsNullOrEmpty(categoryTranslation.SeoLink)
                        ? categoryTranslation.SeoLink.Trim()
                        : categoryTranslation.Name.Trim().ToUrlString().ToLower();

                    // Check seoLink exists.
                    var isSeoLinkExists = await _websiteCategoryTranslationRepository.CheckSeoLinkExists(tenantId, websiteId,
                        categoryTranslation.LanguageId, info.Id, categoryTranslation.SeoLink);
                    if (isSeoLinkExists)
                        return new ActionResultResponse<WebsiteProductCategoryTranslation>(-5,
                            _resourceService.GetString("Seolink: \"{0}\" already exsits.", categoryTranslation.SeoLink));

                    var categoryTranslationInfo =
                        await _websiteCategoryTranslationRepository.GetInfo(websiteId, info.Id, categoryTranslation.LanguageId);

                    // Add new category translation.
                    if (categoryTranslationInfo == null)
                    {
                        listNewCategoryTranslations.Add(new WebsiteProductCategoryTranslation
                        {
                            WebsiteProductCategoryId = info.Id,
                            LanguageId = categoryTranslation.LanguageId,
                            Name = categoryTranslation.Name.Trim(),
                            MetaTitle = categoryTranslation.MetaTitle?.Trim(),
                            Description = categoryTranslation.Description?.Trim(),
                            MetaDescription = categoryTranslation.MetaDescription?.Trim(),
                            UnsignName = categoryTranslation.Name.Trim().StripVietnameseChars().ToUpper(),
                            SeoLink = categoryTranslation.SeoLink
                        });
                    }
                    else
                    {
                        categoryTranslationInfo.Name = categoryTranslation.Name.Trim();
                        categoryTranslationInfo.MetaTitle = categoryTranslation.MetaTitle?.Trim();
                        categoryTranslationInfo.Description = categoryTranslation.Description?.Trim();
                        categoryTranslationInfo.MetaDescription = categoryTranslation.MetaDescription?.Trim();
                        categoryTranslationInfo.UnsignName = categoryTranslation.Name.Trim().StripVietnameseChars().ToUpper();
                        categoryTranslationInfo.SeoLink = categoryTranslation.SeoLink;
                        await _websiteCategoryTranslationRepository.Update(categoryTranslationInfo);
                    }
                }

                return new ActionResultResponse(1);
            }

            async Task UpdateChildrenIdPath()
            {
                if (info.IdPath != oldIdPath)
                {
                    await _websiteCategoryRepository.UpdateChildrenIdPath(websiteId, oldIdPath, info.IdPath);
                }
            }
            #endregion
        }

        #region Private
        private async Task<ActionResultResponse<WebsiteProductCategoryTranslation>> AddCategoryTranslation(string tenantId, string websiteId, int categoryId, WebsiteCategoryTranslationMeta categoryTranslationMeta)
        {
            var isNameExists = await _websiteCategoryTranslationRepository.CheckExists(tenantId, websiteId,
               categoryTranslationMeta.LanguageId, categoryId, categoryTranslationMeta.Name);

            if (isNameExists)
                return new ActionResultResponse<WebsiteProductCategoryTranslation>(-1, _resourceService.GetString("Category name: \"{0}\" already exists.", categoryTranslationMeta.Name));

            categoryTranslationMeta.SeoLink = !string.IsNullOrEmpty(categoryTranslationMeta.SeoLink)
               ? categoryTranslationMeta.SeoLink.Trim()
               : categoryTranslationMeta.Name.Trim().ToUrlString().ToLower();

            var isSeoLinkExists = await _websiteCategoryTranslationRepository.CheckSeoLinkExists(tenantId, websiteId,
              categoryTranslationMeta.LanguageId, categoryId, categoryTranslationMeta.SeoLink);
            if (isSeoLinkExists)
                return new ActionResultResponse<WebsiteProductCategoryTranslation>(-2,
                    _resourceService.GetString("Seolink: \"{0}\" already exsits.", categoryTranslationMeta.SeoLink));

            return new ActionResultResponse<WebsiteProductCategoryTranslation>
            {
                Data = new WebsiteProductCategoryTranslation
                {
                    TenantId = tenantId,
                    WebsiteId =websiteId,
                    WebsiteProductCategoryId = categoryId,
                    LanguageId = categoryTranslationMeta.LanguageId,
                    Name = categoryTranslationMeta.Name.Trim(),
                    MetaTitle = categoryTranslationMeta.MetaTitle?.Trim(),
                    Description = categoryTranslationMeta.Description?.Trim(),
                    MetaDescription = categoryTranslationMeta.MetaDescription?.Trim(),
                    UnsignName = categoryTranslationMeta.Name.Trim().StripVietnameseChars().ToUpper(),
                    SeoLink = categoryTranslationMeta.SeoLink
                }
            };
        }
        #endregion
    }
}
