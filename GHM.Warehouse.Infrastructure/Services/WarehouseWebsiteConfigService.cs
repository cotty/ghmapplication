﻿using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using GHM.Warehouse.Domain.IRepository;
using GHM.Warehouse.Domain.IServices;
using GHM.Warehouse.Domain.Models;
using GHM.Warehouse.Domain.Resources;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Warehouse.Infrastructure.Services
{
    public class WarehouseWebsiteConfigService : IWarehouseWebsiteConfigService
    {
        private readonly IWarehouseWebsiteConfigRepository _warehouseWebsiteConfigRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmWarehouseResource> _warehouseResourceService;

        public WarehouseWebsiteConfigService(IWarehouseWebsiteConfigRepository warehouseWebsiteConfigRepository)
        {
            _warehouseWebsiteConfigRepository = warehouseWebsiteConfigRepository;
        }
        public async Task<SearchResult<WarehouseWebsiteConfig>> SearchAsync(string tenantId, string warehouseId, string websiteId, int page, int pageSize)
        {
            var items = await _warehouseWebsiteConfigRepository.Search(tenantId, warehouseId, websiteId,out int totalRows);

            return new SearchResult<WarehouseWebsiteConfig>
            {
                Items = items,
                TotalRows = totalRows
            };
        }
    }
}
