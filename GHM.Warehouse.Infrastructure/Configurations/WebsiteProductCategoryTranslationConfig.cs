﻿using GHM.Warehouse.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Infrastructure.Configurations
{
    public class WebsiteProductCategoryTranslationConfiguration : IEntityTypeConfiguration<WebsiteProductCategoryTranslation>
    {
        public void Configure(EntityTypeBuilder<WebsiteProductCategoryTranslation> builder)
        {
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.WebsiteProductCategoryId).HasColumnName("WebsiteProductCategoryId").IsRequired().HasColumnType("int");
            builder.Property(x => x.WebsiteId).HasColumnName("WebsiteId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.LanguageId).HasColumnName("LanguageId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(10);
            builder.Property(x => x.Name).HasColumnName("Name").IsRequired().HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.Description).HasColumnName("Description").HasColumnType("nvarchar").HasMaxLength(500);
            builder.Property(x => x.UnsignName).HasColumnName("UnsignName").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(256);
            builder.Property(x => x.ParentName).HasColumnName("ParentName").HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.IsDelete).HasColumnName("IsDelete").IsRequired().HasColumnType("bit");
            builder.Property(x => x.MetaTitle).HasColumnName("MetaTitle").HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.MetaDescription).HasColumnName("MetaDescription").HasColumnType("nvarchar").HasMaxLength(500);
            builder.Property(x => x.SeoLink).HasColumnName("SeoLink").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(500);
            builder.Property(x => x.Alt).HasColumnName("Alt").HasColumnType("nvarchar").HasMaxLength(500);
            builder.Property(x => x.BannerImage).HasColumnName("BannerImage").HasColumnType("nvarchar").HasMaxLength(500);
            builder.ToTable("WebsiteProductCategoryTranslations");
        }
    }
}
