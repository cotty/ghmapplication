﻿using GHM.Warehouse.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Infrastructure.Configurations
{
    public class GoodsReceiptNoteConfig : IEntityTypeConfiguration<GoodsReceiptNote>
    {
        public void Configure(EntityTypeBuilder<GoodsReceiptNote> builder)
        {
            builder.Property(x => x.Id).IsRequired().HasMaxLength(50).IsUnicode(false);

            builder.HasMany(x => x.GoodsReceiptNoteDetails)
                .WithOne(x => x.GoodsReceiptNote);
            builder.ToTable("GoodsReceiptNotes").HasKey(x => new { x.TenantId, x.Id });
        }
    }
}
