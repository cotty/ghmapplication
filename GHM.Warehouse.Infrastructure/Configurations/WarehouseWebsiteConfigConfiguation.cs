﻿using GHM.Warehouse.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Infrastructure.Configurations
{
    public class WarehouseWebsiteConfigConfiguation : IEntityTypeConfiguration<WarehouseWebsiteConfig>
    {
        public void Configure(EntityTypeBuilder<WarehouseWebsiteConfig> builder)
        {
            builder.Property(x => x.WarehouseId).HasColumnName("WareHouseId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.WebsiteId).HasColumnName("WebsiteId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.Name).HasColumnName("Name").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.ToTable("WarehouseWebsiteConfig").HasKey(x => new { x.WarehouseId, x.WebsiteId, x.TenantId });
        }
    }
}
