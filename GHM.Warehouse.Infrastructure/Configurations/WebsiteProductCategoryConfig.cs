﻿using GHM.Warehouse.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Infrastructure.Configurations
{
    public class WebsiteProductCategoryConfig : IEntityTypeConfiguration<WebsiteProductCategories>
    {
        public void Configure(EntityTypeBuilder<WebsiteProductCategories> builder)
        {

            builder.Property(x => x.ProductId).HasColumnName("ProductId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.ProductWebsiteCategoryId).HasColumnName("ProductWebsiteCategoryId").IsRequired().HasColumnType("int");
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.WebsiteId).HasColumnName("WebsiteId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.WarehouseId).HasColumnName("WarehouseId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.ToTable("WebsiteProductCategories").HasKey(x => new { x.ProductWebsiteCategoryId, x.TenantId, x.WebsiteId, x.WarehouseId });
        }
    }
}
