﻿using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.SqlServer;
using GHM.Warehouse.Domain.IRepository;
using GHM.Warehouse.Domain.ModelMetas;
using GHM.Warehouse.Domain.Models;
using GHM.Warehouse.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Warehouse.Infrastructure.Repository
{
    public class WebsiteCategoryRepository : RepositoryBase, IWebsiteCategoryRepository
    {
        private readonly IRepository<WebsiteProductCategory> _websiteProductCategory;
        public WebsiteCategoryRepository(IDbContext context): base(context)
        {
            _websiteProductCategory = Context.GetRepository<WebsiteProductCategory>();
        }
        public async Task<int> CountByParentId(string websiteId, int? parentId)
        {
            return await _websiteProductCategory.CountAsync(x => x.WebsiteId == websiteId && x.ParentId == parentId);
        }

        public async Task<int> Delete(WebsiteProductCategory info)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(string tenantId, string websiteId, int id)
        {
            var info = await GetInfo(tenantId,websiteId, id, false);
            if (info == null)
                return -1;

            info.IsDelete = true;

            return await Context.SaveChangesAsync();
        }

        public Task<WebsiteProductCategoryViewModel> GetDetail(string tenantId, string websiteId, int id)
        {
            throw new NotImplementedException();
        }

        public async Task<WebsiteProductCategory> GetInfo(string tenantId, string websiteId, int id, bool isReadOnly = false)
        {
            return await _websiteProductCategory.GetAsync(isReadOnly, x => x.TenantId == tenantId && x.WebsiteId == websiteId && x.Id == id);
        }

        public async Task<WebsiteProductCategory> GetInfo(string tenantId, string websiteId, int? parentId, bool isReadOnly = false)
        {
            return await _websiteProductCategory.GetAsync(isReadOnly, x => x.TenantId == tenantId && x.WebsiteId == websiteId && x.Id == parentId);
        }

        public async Task<int> Insert(WebsiteProductCategory category)
        {
            _websiteProductCategory.Create(category);
            return await Context.SaveChangesAsync();
        }

        public async Task<List<CategoryViewModel>> SearchAll(string tenantId, string websiteId, string keyword, string languageId, bool? isActive)
        {
            Expression<Func<WebsiteProductCategory, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId && x.WebsiteId == websiteId;
            Expression<Func<WebsiteProductCategoryTranslation, bool>> specTranslation = x => x.LanguageId == languageId && x.WebsiteId == websiteId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            if (isActive.HasValue)
                spec = spec.And(x => x.IsActive == isActive);

            var query = Context.Set<WebsiteProductCategory>().Where(spec)
                .Join(Context.Set<WebsiteProductCategoryTranslation>().Where(specTranslation), c => c.Id, ct => ct.WebsiteProductCategoryId,
                    (c, ct) => new CategoryViewModel
                    {
                        Id = c.Id,
                        Name = ct.Name,
                        IsActive = c.IsActive,
                        ParentId = c.ParentId,
                        IdPath = c.IdPath,
                        Order = c.Order
                    }).AsNoTracking();

            return await query.ToListAsync();
        }

        public Task<List<WebsiteCategorySearchForSelectViewModel>> SearchForSelect(string tenantId, string websiteId, string languageId, string keyword, int page, int pageSize, out int totalRows)
        {
            Expression<Func<WebsiteProductCategory, bool>> spec = x => x.IsActive && !x.IsDelete && x.TenantId == tenantId && x.WebsiteId == websiteId;
            Expression<Func<WebsiteProductCategoryTranslation, bool>> specTranslation = x => x.LanguageId == languageId && x.WebsiteId == websiteId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            var query = Context.Set<WebsiteProductCategory>().Where(spec)
                .Join(Context.Set<WebsiteProductCategoryTranslation>().Where(specTranslation), c => c.Id, ct => ct.WebsiteProductCategoryId,
                    (c, ct) => new WebsiteCategorySearchForSelectViewModel()
                    {
                        Id = c.Id,
                        Name = ct.Name,
                        SeoLink = ct.SeoLink,
                    }).AsNoTracking();

            totalRows = query.Count();
            return query
                .OrderByDescending(x => x.Name)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        public Task<List<Suggestion<int>>> Suggestions(string tenantId, string websiteId, string languageId, string keyword, int page, int pageSize, out int totalRows)
        {
            Expression<Func<WebsiteProductCategory, bool>> spec = x => x.IsActive && !x.IsDelete && x.TenantId == tenantId && x.WebsiteId == websiteId;
            Expression<Func<WebsiteProductCategoryTranslation, bool>> specTranslation = x => x.LanguageId == languageId && x.WebsiteId == websiteId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                specTranslation = specTranslation.And(x => x.UnsignName.Contains(keyword));
            }

            var query = Context.Set<WebsiteProductCategory>().Where(spec)
                .Join(Context.Set<WebsiteProductCategoryTranslation>().Where(specTranslation), c => c.Id, ct => ct.WebsiteProductCategoryId,
                    (c, ct) => new Suggestion<int>()
                    {
                        Id = c.Id,
                        Name = ct.Name
                    }).AsNoTracking();

            totalRows = query.Count();
            return query
                .OrderByDescending(x => x.Name)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        public async Task<int> Update(WebsiteProductCategory info)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateCategoryIdPathAsync(string tenantId, WebsiteCategoryMeta websiteCategoryMeta, int id, string idPath)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateChildrenIdPath(string websiteId, string oldIdPath, string idPath)
        {
            var childrens = await _websiteProductCategory.GetsAsync(false, x => x.WebsiteId == websiteId && x.IdPath.StartsWith($"{oldIdPath}."));
            if (!childrens.Any())
                return -1;

            foreach (var children in childrens)
            {
                var oldIdPathSave = children.IdPath;
                children.IdPath = $"{idPath}.{children.Id}";
                await UpdateChildrenIdPath(websiteId, oldIdPathSave, $"{children.IdPath}");
            }
            return await Context.SaveChangesAsync();
        }
    }
}
