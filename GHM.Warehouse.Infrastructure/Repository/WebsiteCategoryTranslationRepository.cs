﻿using GHM.Infrastructure.SqlServer;
using GHM.Warehouse.Domain.IRepository;
using GHM.Warehouse.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Warehouse.Infrastructure.Repository
{
    public class WebsiteCategoryTranslationRepository : RepositoryBase, IWebsiteCategoryTranslationRepository
    {
        private readonly IRepository<WebsiteProductCategoryTranslation> _websiteCategoryTranslation;
        public WebsiteCategoryTranslationRepository(IDbContext context): base(context)
        {
            _websiteCategoryTranslation = Context.GetRepository<WebsiteProductCategoryTranslation>();
        }

        public async Task<bool> CheckExists(string tenantId, string websiteId, string languageId, int categoryId, string name)
        {
            return await _websiteCategoryTranslation.ExistAsync(x => x.TenantId == tenantId && x.WebsiteId == websiteId && 
            x.WebsiteProductCategoryId != categoryId && x.Name == name && x.LanguageId == languageId && !x.IsDelete);
        }

        public async Task<bool> CheckSeoLinkExists(string tenantId, string websiteId, string languageId, int categoryId, string seoLink)
        {
            return await _websiteCategoryTranslation.ExistAsync(x => x.TenantId == tenantId && x.LanguageId == languageId &&
            x.WebsiteId == websiteId && x.SeoLink == seoLink && x.WebsiteProductCategoryId != categoryId && !x.IsDelete);
        }

        public async Task<List<WebsiteProductCategoryTranslation>> GetByCategoryId(string websiteId, int id)
        {
            return await _websiteCategoryTranslation.GetsAsync(true, x => x.WebsiteProductCategoryId == id && x.WebsiteId == websiteId && !x.IsDelete);
        }

        public async Task<WebsiteProductCategoryTranslation> GetInfo(string websiteId, int id, string languageId)
        {
            return await _websiteCategoryTranslation.GetAsync(false, x => x.WebsiteId == websiteId && x.LanguageId == languageId
            && x.WebsiteProductCategoryId == id && !x.IsDelete);
        }

        public async Task<int> Inserts(List<WebsiteProductCategoryTranslation> websiteCategoryTranslation)
        {
            _websiteCategoryTranslation.Creates(websiteCategoryTranslation);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(WebsiteProductCategoryTranslation categoryTranslationInfo)
        {
            return await Context.SaveChangesAsync();
        }
    }
}
