﻿using GHM.Infrastructure.SqlServer;
using GHM.Warehouse.Domain.IRepository;
using GHM.Warehouse.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Warehouse.Infrastructure.Repository
{
    public class WebsiteProductCategoriesRepository : RepositoryBase, IWebsiteProductCategoriesRepository
    {
        private readonly IRepository<WebsiteProductCategories> _websiteProductCategoriesRepository;
        public WebsiteProductCategoriesRepository(IDbContext context): base(context)
        {
            _websiteProductCategoriesRepository = Context.GetRepository<WebsiteProductCategories>();
        }
        public async Task<bool> CheckCategoryExist(string tenantId, string websiteId, int id)
        {
            return await _websiteProductCategoriesRepository.ExistAsync(x => x.TenantId == tenantId && x.WebsiteId == websiteId && x.ProductWebsiteCategoryId == id);
        }

        public async Task<int> DeleteByProductsById(string productId, string warehouseId, string tenantId, string websiteId)
        {
            var list = await _websiteProductCategoriesRepository.GetsAsync(false, x => x.ProductId == productId && x.WarehouseId == warehouseId &&
            x.TenantId == tenantId && x.WebsiteId == websiteId);

            if(list == null || !list.Any())
            {
                return 1;
            }

            _websiteProductCategoriesRepository.Deletes(list);

            return await Context.SaveChangesAsync(); 

        }

        public async Task<int> Insert(WebsiteProductCategories websiteProductCategories)
        {
            _websiteProductCategoriesRepository.Create(websiteProductCategories);
            return await Context.SaveChangesAsync();
        }
    }
}
