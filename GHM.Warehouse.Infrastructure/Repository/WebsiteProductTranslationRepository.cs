﻿using GHM.Infrastructure.SqlServer;
using GHM.Warehouse.Domain.IRepository;
using GHM.Warehouse.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Warehouse.Infrastructure.Repository
{
    public class WebsiteProductTranslationRepository : RepositoryBase, IWebsiteProductTranslationRepository
    {
        private readonly IRepository<WebsiteProductTranslation> _websiteProductTranslation;

        public WebsiteProductTranslationRepository(IDbContext context): base(context)
        {
            _websiteProductTranslation = Context.GetRepository<WebsiteProductTranslation>();
        }
        public async Task<bool> CheckSeoLinkExistAsync(string tenantId, string websiteId, string seoLink)
        {
            return await _websiteProductTranslation.ExistAsync(x => x.TenantId == tenantId && x.WebsiteId == websiteId && x.SeoLink == seoLink);
        }

        public async Task<WebsiteProductTranslation> GetInfo(string tenantId, string warehouseId, string websiteId, string productId, string languageId,bool isReadOnly = false)
        {
            return await _websiteProductTranslation.GetAsync(isReadOnly, x => x.TenantId == tenantId && x.WarehouseId == warehouseId && x.WebsiteId == websiteId && x.ProductId == productId);
        }

        public async Task<int> Insert(WebsiteProductTranslation websiteProductTranslation)
        {
            _websiteProductTranslation.Create(websiteProductTranslation);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(WebsiteProductTranslation infoTranslation)
        {
            return await Context.SaveChangesAsync();
        }
    }
}
