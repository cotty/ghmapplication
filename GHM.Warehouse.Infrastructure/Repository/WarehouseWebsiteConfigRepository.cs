﻿using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using GHM.Warehouse.Domain.IRepository;
using GHM.Warehouse.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Warehouse.Infrastructure.Repository
{
    public class WarehouseWebsiteConfigRepository : RepositoryBase, IWarehouseWebsiteConfigRepository
    {
        private readonly IRepository<WarehouseWebsiteConfig> _warhouseWebsiteConfigRepository;
        public WarehouseWebsiteConfigRepository(IDbContext context): base(context)
        {
            _warhouseWebsiteConfigRepository = Context.GetRepository<WarehouseWebsiteConfig>();
        }

        public async Task<bool> CheckExist(string tenantId, string warehouseId, string websiteId)
        {
            return await _warhouseWebsiteConfigRepository.ExistAsync(x => x.TenantId == tenantId && x.WarehouseId == warehouseId && x.WebsiteId == websiteId);
        }

        public async Task<bool> CheckExist(string tenantId, string websiteId)
        {
            return await _warhouseWebsiteConfigRepository.ExistAsync(x => x.TenantId == tenantId && x.WebsiteId == websiteId);
        }

        public async Task<WarehouseWebsiteConfig> GetWarehouseByWebsite(string tenant, string websiteId)
        {
            return await _warhouseWebsiteConfigRepository.GetAsync(true, x => x.TenantId == tenant && x.WebsiteId == websiteId);
        }

        public Task<List<WarehouseWebsiteConfig>> Search(string tenantId, string warehouseId, string websiteId, out int totalRows)
        {
            Expression<Func<WarehouseWebsiteConfig, bool>> spec = x => x.TenantId == tenantId && x.WarehouseId == warehouseId;

            if (!string.IsNullOrEmpty(websiteId))
                spec = spec.And( x => x.WebsiteId == websiteId);

            totalRows = _warhouseWebsiteConfigRepository.Count(spec);
            return _warhouseWebsiteConfigRepository.GetsAsync(true, spec);
        }
    }
}
