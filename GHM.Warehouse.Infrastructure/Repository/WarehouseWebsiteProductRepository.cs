﻿using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.Services;
using GHM.Infrastructure.SqlServer;
using GHM.Warehouse.Domain.IRepository;
using GHM.Warehouse.Domain.Models;
using GHM.Warehouse.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Warehouse.Infrastructure.Repository
{
    public class WarehouseWebsiteProductRepository : RepositoryBase, IWarehouseWebsiteProductRepository
    {
        private readonly IRepository<WebsiteProduct> _websiteProductRepository;
        private readonly IConfiguration _configuration;

        public WarehouseWebsiteProductRepository(IDbContext context, IConfiguration configuration) : base(context)
        {
            _configuration = configuration;
            _websiteProductRepository = Context.GetRepository<WebsiteProduct>();
        }
        public async Task<bool> CheckProductExistAsync(string tenantId, string warehouseId, string websiteId, string productId)
        {
            return await _websiteProductRepository.ExistAsync(x => x.TenantId == tenantId && x.WarehouseId == warehouseId && x.ProductId == productId && !x.IsDelete);

        }

        public async Task<int> Delete(WebsiteProduct info)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<WebsiteProduct> GetDetail(string tenantId, string warehouseId, string websiteId, string productId, bool isReadOnly = false)
        {
            return await _websiteProductRepository.GetAsync(isReadOnly, x => x.TenantId == tenantId && x.WarehouseId == warehouseId && x.WebsiteId == websiteId && x.ProductId == productId);
        }

        public async Task<WebsiteProductDetailViewModel> GetDetails(string tenantId, string warehouseId, string websiteId, string productId, string languageId)
        {
            Expression<Func<WebsiteProduct, bool>> specWebP = x => x.TenantId == tenantId && x.WarehouseId == warehouseId && x.WebsiteId == websiteId && !x.IsDelete && x.ProductId == productId;

            Expression<Func<Product, bool>> specP = x => x.TenantId == tenantId && x.Id == productId;

            Expression<Func<ProductUnit, bool>> specPU = x => x.TenantId == tenantId && x.ProductId == productId && x.IsDefault && !x.IsDelete;

            Expression<Func<ProductTranslation, bool>> specPT = x => x.TenantId == tenantId && x.ProductId == productId;

            Expression<Func<WebsiteProductTranslation, bool>> specWebPT = x => x.TenantId == tenantId && x.WarehouseId == warehouseId && x.WebsiteId == websiteId && x.ProductId == productId;
 
            Expression<Func<WebsiteProductCategories, bool>> specWPC = x => x.TenantId == tenantId && x.ProductId == productId && x.WebsiteId == websiteId;

            Expression<Func<WebsiteProductCategoryTranslation, bool>> specWPCT = x => x.TenantId == tenantId  && x.WebsiteId == websiteId;


            var apiUrls = _configuration.GetApiUrl();

            var resultTag = await new HttpClientService()
                .GetAsync<List<SubjectTagViewModel>>($"{apiUrls.CoreApiUrl}/tags/{tenantId}/{productId}");

            var query1 = from p in Context.Set<Product>().Where(specP)
                         join pu in Context.Set<ProductUnit>().Where(specPU) on p.Id equals pu.ProductId
                         join pt in Context.Set<ProductTranslation>().Where(specPT) on p.Id equals pt.ProductId
                         group new {pt} by new {p.Id, p.Thumbnail, pu.SalePrice} into g
                         select new
                         {
                             productId = g.Key.Id,
                             featureImage = g.Key.Thumbnail,
                             salePrice = g.Key.SalePrice,
                             translation = g.Select( x => new WebsiteProductTranslationViewModel
                             {
                                 LanguageId = x.pt.LanguageId,
                                 Name = x.pt.Name,
                                 Description = x.pt.Description,
                                 UnsignName = x.pt.UnsignName,
                             })
                         };
            var resultTest = query1.ToList();
            var query2 = from wp in Context.Set<WebsiteProduct>().Where(specWebP)
                         join wpt in Context.Set<WebsiteProductTranslation>().Where(specWebPT) on wp.ProductId equals wpt.ProductId
                         join wpc in Context.Set<WebsiteProductCategories>().Where(specWPC) on wp.ProductId equals wpc.ProductId
                         join wpct in Context.Set<WebsiteProductCategoryTranslation>().Where(specWPCT) on wpc.ProductWebsiteCategoryId equals wpct.WebsiteProductCategoryId
                         group new { wpt, wpc, wpct } by new { wp.LikeCount, wp.CommentCount, wp.ViewCount, wp.ProductId, wp.IsHot, wp.IsHomePage } into g
                         select new
                         {
                             isHot = g.Key.IsHot,
                             isHomePage = g.Key.IsHomePage,
                             productId = g.Key.ProductId,
                             likeCount = g.Key.LikeCount,
                             commentCount = g.Key.CommentCount,
                             viewCount = g.Key.ViewCount,
                             translation = g != null ? g.Select(x => new WebsiteProductTranslationViewModel
                             {
                                 LanguageId = x.wpt.LanguageId,
                                 Content = x.wpt.Content,
                                 Alt = x.wpt.Alt,
                                 MetaDescription = x.wpt.MetaDescription,
                                 MetaKeyword = x.wpt.MetaKeyword,
                                 SeoLink = x.wpt.SeoLink
                             }) : null,
                             categories = g != null ? g.Select(x => new WebsiteProductCategoriesViewModel
                             {
                                 CategoryId = x.wpc.ProductWebsiteCategoryId,
                                 ProductId = x.wpc.ProductId,
                                 CategoryName = x.wpct.Name
                             }).ToList() : null
                         };
            if(query2.Count() > 0) {
            var result = from q1 in query1
                         join q2 in query2 on q1.productId equals q2.productId into ps
                         from q2 in ps.DefaultIfEmpty()
                         select new WebsiteProductDetailViewModel
                         {
                             FeatureImage = q1.featureImage,
                             LikeCount = q2.likeCount,
                             SalePrice = q1.salePrice,
                             CommentCount = q2.commentCount,
                             ViewCount = q2.viewCount,
                             IsHomePage = q2.isHomePage,
                             IsHot = q2.isHot,
                             Translations = q1.translation.Count() > 0 ?
                             (from q1t in q1.translation
                              join qlt in q2.translation on q1t.LanguageId equals qlt.LanguageId into ptg
                              from q2t in ptg.DefaultIfEmpty()
                              select new WebsiteProductTranslationViewModel
                              {
                                  LanguageId = q1t.LanguageId,
                                  Name = q1t.Name,
                                  UnsignName = q1t.UnsignName,
                                  Content = q2t.Content,
                                  Alt = q2t.Alt,
                                  Description = q1t.Description,
                                  MetaDescription = q2t.MetaDescription,
                                  MetaKeyword = q2t.MetaKeyword,
                                  SeoLink = q2t.SeoLink,
                                  Tags = resultTag != null ? resultTag.Where(t => t.LanguageId == q1t.LanguageId).ToList() : null
                              }).ToList() : null,
                             Categories = q2.categories.Count() > 0 ? q2.categories : null
                         };
                return await result.FirstOrDefaultAsync();
            } else
            {
                var result = from q1 in query1
                             select new WebsiteProductDetailViewModel
                             {
                                 FeatureImage = q1.featureImage,
                                 ProductId = q1.productId,
                                 Translations = q1.translation.ToList()
                             };
                return await result.FirstOrDefaultAsync();
            }
           
        }

        public async Task<int> Insert(WebsiteProduct websiteProduct)
        {
            _websiteProductRepository.Create(websiteProduct);
            return await Context.SaveChangesAsync();
        }

        public Task<List<WebsiteProductViewModel>> Search(string tenantId, string warehouseId, string websiteId, int? categoryId,
            string productName, string languageId, int page, int pageSize, out int totalRows)
        {
            Expression<Func<Product, bool>> specP = x => x.TenantId == tenantId;

            Expression<Func<ProductUnit, bool>> specPU = x => x.TenantId == tenantId && x.IsDefault && !x.IsDelete;

            Expression<Func<ProductTranslation, bool>> specPT = x => x.TenantId == tenantId;

   

            Expression<Func<GoodsReceiptNoteDetail, bool>> specGoods = x => x.TenantId == tenantId && x.WarehouseId == warehouseId;
            if (!string.IsNullOrEmpty(productName))
                specPT.And(x => x.Name.Equals(productName));

           

            var query1 = from gr in Context.Set<GoodsReceiptNoteDetail>().Where(specGoods)
                         join pd in Context.Set<Product>().Where(specP) on gr.ProductId equals pd.Id
                         join pu in Context.Set<ProductUnit>().Where(specPU) on pd.Id equals pu.ProductId
                         join pt in Context.Set<ProductTranslation>().Where(specPT) on pd.Id equals pt.ProductId
                         select new
                         {
                             productId = pd.Id,
                             thumbnail = pd.Thumbnail,
                             name = pt.Name,
                             salePrice = pu.SalePrice,
                             createTime = pd.CreateTime
                         };
            var total = query1.Count();

            Expression<Func<WebsiteProduct, bool>> specWebP = x => x.TenantId == tenantId && x.WarehouseId == warehouseId && x.WebsiteId == websiteId && !x.IsDelete;

            Expression<Func<WebsiteProductCategories, bool>> specWPC = x => x.TenantId == tenantId && x.WarehouseId == warehouseId && x.WebsiteId == websiteId;

            Expression<Func<WebsiteProductCategoryTranslation, bool>> specWPCT = x => x.TenantId == tenantId && x.WebsiteId == websiteId;

            if (categoryId.HasValue)
                specWPC.And(x => x.ProductWebsiteCategoryId == categoryId);

            var query2 = from wp in Context.Set<WebsiteProduct>().Where(specWebP)
                         join wpc in Context.Set<WebsiteProductCategories>().Where(specWPC) on wp.ProductId equals wpc.ProductId
                         join wpct in Context.Set<WebsiteProductCategoryTranslation>().Where(specWPCT) on wpc.ProductWebsiteCategoryId equals wpct.WebsiteProductCategoryId
                         group new { wpct } by new { wp.ProductId, wp.IsHomePage, wp.WebsiteId, wp.IsHot, wp.WarehouseId, wp.CommentCount, wp.LikeCount, wp.ViewCount } into g
                         select new
                         {
                             websiteId = g.Key.WebsiteId,
                             productId = g.Key.ProductId,
                             isHot = g.Key.IsHot,
                             isHomePage = g.Key.IsHomePage,
                             wareHouseId = g.Key.WarehouseId,
                             commentCount = g.Key.CommentCount,
                             likeCount = g.Key.LikeCount,
                             viewCount = g.Key.ViewCount,
                             categoriesName = g.Select(x => x.wpct.Name).ToList()
                         };
            var total2 = query2.Count();
            //var query3 = from q1 in query1
            //             from q2 in query2
            //             where q1.productId == q2.productId
            //             select new WebsiteProductViewModel
            //             {
            //                 ProductId = q1.productId,
            //                 WarehouseId = q2.wareHouseId,
            //                 WebsiteId = q2.websiteId,
            //                 IsHot = q2.isHot,
            //                 IsHomePage = q2.isHomePage,
            //                 CommentCount = q2.commentCount,
            //                 LikeCount = q2.likeCount,
            //                 ViewCount = q2.viewCount,
            //                 Name = q1.name,
            //                 SalePrice = q1.salePrice,
            //                 FeatureImage = q1.thumbnail,
            //                 CreateTime = q1.createTime,
            //                 CategoriesName = q2.categoriesName
            //             };
            var result = from q1 in query1
                         join g in query2 on q1.productId equals g.productId into ps
                         from q2 in ps.DefaultIfEmpty()
                          select new WebsiteProductViewModel
                         {
                             ProductId = q1.productId,
                             WarehouseId = q2.wareHouseId,
                             WebsiteId = q2.websiteId,
                             IsHot = q2.isHot,
                             IsHomePage = q2.isHomePage,
                             CommentCount = q2.commentCount,
                             LikeCount = q2.likeCount,
                             ViewCount = q2.viewCount,
                             Name = q1.name,
                             SalePrice = q1.salePrice,
                             FeatureImage = q1.thumbnail,
                             CreateTime = q1.createTime,
                             CategoriesName = q2.categoriesName
                         };

            totalRows = result.Count();

            return result.OrderByDescending(x => x.CreateTime).Skip((page - 1) * pageSize).Take(pageSize).AsNoTracking().ToListAsync();
        }

        public Task<List<ProductSearchForSelectViewModel>> SearchForSelectAsync(string tenantId, string warehouseId, string websiteId, string keyword, string languageId, int page, int pageSize, out int totalRows)
        {
            Expression<Func<WebsiteProduct, bool>> specWP = x => x.TenantId == tenantId && x.WarehouseId == warehouseId && x.WebsiteId == websiteId;
            Expression<Func<WebsiteProductTranslation, bool>> specWPT = x => x.TenantId == tenantId && x.WarehouseId == warehouseId && x.WebsiteId == websiteId && x.LanguageId == languageId;
            Expression<Func<ProductTranslation, bool>> specPT = x => x.TenantId == tenantId && !x.IsDelete;
            Expression<Func<Product, bool>> specP = x => x.TenantId == tenantId && !x.IsDelete;

            if (!string.IsNullOrEmpty(keyword))
                specPT.And(x => x.Name.Contains(keyword));

            var query = from wp in Context.Set<WebsiteProduct>().Where(specWP)
                        join wpt in Context.Set<WebsiteProductTranslation>().Where(specWPT) on wp.ProductId equals wpt.ProductId
                        join p in Context.Set<Product>().Where(specP) on wp.ProductId equals p.Id
                        join pt in Context.Set<ProductTranslation>().Where(specPT) on p.Id equals pt.ProductId
                        select new ProductSearchForSelectViewModel
                        {
                            Id = wp.ProductId,
                            FeatureImage = p.Thumbnail,
                            SeoLink = wpt.SeoLink,
                            LanguageId = wpt.LanguageId,
                            Title = pt.Name,
                            LastUpdate = p.LastUpdateTime
                        };
            totalRows = query.Count();
            return query.OrderByDescending(x => x.LastUpdate).Skip((page - 1) * pageSize).Take(pageSize).AsNoTracking().ToListAsync();
        }

        public async Task<List<WebsiteProductSuggestViewModel>> SearchForSuggestion(string tenantId, string warehouseId, string websiteId, string productId)
        {
            Expression<Func<WebsiteProduct, bool>> spec = x => x.TenantId == tenantId && x.ProductId == productId && !x.IsDelete;

            if (!string.IsNullOrEmpty(websiteId))
                spec = spec.And(x => x.WebsiteId == websiteId);

            if (!string.IsNullOrEmpty(warehouseId))
                spec = spec.And(x => x.WebsiteId == warehouseId);

            var query = Context.Set<WebsiteProduct>().Where(spec)
                        .Join(Context.Set<WarehouseWebsiteConfig>().Where(x => x.TenantId == tenantId),
                        wp => wp.WebsiteId, wwc => wwc.WebsiteId, (wp, wwc) =>
                          new WebsiteProductSuggestViewModel
                          {
                              WebsiteId = wp.WebsiteId,
                              Name = wwc.Name,
                              ProductId = wp.ProductId
                          }
                      );
            return await query.OrderByDescending(x => x.ProductId).AsNoTracking().ToListAsync();
        }

        public async Task<int> Update(WebsiteProduct info)
        {
            return await Context.SaveChangesAsync();
        }
    }
}
