﻿using GHM.Tasks.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Tasks.Infrastructure.Configurations
{
    public class AttachmentsConfig : IEntityTypeConfiguration<Attachment>
    {
        public void Configure(EntityTypeBuilder<Attachment> builder)
        {
            builder.Property(x => x.Id).HasColumnName("Id").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.ObjectId).HasColumnName("ObjectId").IsRequired().HasColumnType("bigint");
            builder.Property(x => x.ObjectType).HasColumnName("ObjectType").IsRequired(false).HasColumnType("int");
            builder.Property(x => x.FileId).HasColumnName("FileId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.FileName).HasColumnName("FileName").IsRequired().HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.DonwloadCount).HasColumnName("DonwloadCount").IsRequired().HasColumnType("int");
            builder.Property(x => x.IsDelete).HasColumnName("IsDelete").IsRequired().HasColumnType("bit");
            builder.Property(x => x.Type).HasColumnName("Type").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.Url).HasColumnName("Url").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(500);
            builder.Property(x => x.ViewCount).HasColumnName("ViewCount").IsRequired(false).HasColumnType("nvarchar");
            builder.Property(x => x.CreateTime).HasColumnName("CreateTime").IsRequired().HasColumnType("datetime2");
            builder.Property(x => x.CreatorFullName).HasColumnName("CreatorFullName").IsRequired().HasColumnType("nvarchar").HasMaxLength(200);
            builder.ToTable("Attachments").HasKey(x => x.Id);
        }
    }
}
