﻿using GHM.Tasks.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Tasks.Infrastructure.Configurations
{
    public class CheckListConfig : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.Property(x => x.Id).HasColumnName("Id").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.UserId).HasColumnName("UserId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.FullName).HasColumnName("FullName").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
            builder.Property(x => x.Content).HasColumnName("Content").IsRequired().HasColumnType("nvarchar").HasMaxLength(4000);
            builder.Property(x => x.ObjectId).HasColumnName("ObjectId").IsRequired().HasColumnType("bigint");
            builder.Property(x => x.ObjectType).HasColumnName("Type").IsRequired().HasColumnType("tinyint");
            builder.Property(x => x.ParentId).HasColumnName("ParentId").IsRequired(false).HasColumnType("bigint");
            builder.Property(x => x.CreateTime).HasColumnName("CreateTime").IsRequired().HasColumnType("datetime");
            builder.Property(x => x.ChildCount).HasColumnName("ChildCount").IsRequired().HasColumnType("int");
            builder.Property(x => x.LikeCount).HasColumnName("LikeCount").IsRequired().HasColumnType("int");
            builder.Property(x => x.IsDelete).HasColumnName("IsDelete").IsRequired().HasColumnType("bit");
            builder.Property(x => x.Image).HasColumnName("Image").IsRequired(false).IsUnicode(false).HasColumnType("varchar").HasMaxLength(500);
            builder.Property(x => x.UrlAttachment).HasColumnName("UrlAttachment").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(500);

            builder.ToTable("Comments").HasKey(x => x.Id);
        }
    }
}
