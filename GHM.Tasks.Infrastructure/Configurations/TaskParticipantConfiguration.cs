﻿using GHM.Tasks.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Tasks.Infrastructure.Configurations
{
    public class TaskParticipantConfiguration : IEntityTypeConfiguration<TaskParticipant>
    {
        public void Configure(EntityTypeBuilder<TaskParticipant> builder)
        {
            builder.Property(x => x.Id).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.TenantId).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.TaskId).IsRequired();
            builder.Property(x => x.UserId).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.FullName).IsRequired().IsUnicode().HasMaxLength(50);
            builder.Property(x => x.Avatar).IsRequired(false).IsUnicode(false).HasMaxLength(500);
            builder.Property(x => x.PositionId).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.PositionName).IsRequired().IsUnicode().HasMaxLength(256);
            builder.Property(x => x.OfficeId).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.OfficeName).IsRequired().IsUnicode().HasMaxLength(256);
            builder.Property(x => x.OfficeIdPath).IsRequired().IsUnicode().HasMaxLength(4000);
            builder.HasMany(x => x.TaskRoleGroup)
                .WithOne(x => x.TaskParticipant)
                .HasForeignKey(x => x.TaskParticipantId);
            builder.ToTable("TaskParticipants")
                .HasKey(x => x.Id);
        }
    }
}
