﻿using GHM.Tasks.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace GHM.Tasks.Infrastructure.Configurations
{
    public class LogConfig : IEntityTypeConfiguration<Log>
    {
        public void Configure(EntityTypeBuilder<Log> builder)
        {
            builder.Property(x => x.Id).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.TenantId).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.Disposition).IsRequired();
            builder.Property(x => x.ObjectId).IsRequired();
            builder.Property(x => x.ObjectType).IsRequired();
            builder.Property(x => x.UserId).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.FullName).IsRequired(false).HasMaxLength(50);
            builder.Property(x => x.Avatar).IsRequired(false).HasMaxLength(500).IsUnicode(false);
            builder.Property(x => x.FromValue).IsRequired(false).HasMaxLength(4000);
            builder.Property(x => x.ToValue).IsRequired(false).HasMaxLength(4000);
            builder.Property(x => x.CreateTime).IsRequired();
            builder.Property(x => x.Type).IsRequired();

            builder.ToTable("Logs").HasKey(x => x.Id);
        }
    }
}
