using GHM.Tasks.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clinic.DataAccess
{
    public class CheckListConfig : IEntityTypeConfiguration<CheckList>
    {
        public void Configure(EntityTypeBuilder<CheckList> builder)
        {
            builder.Property(x => x.Id).HasColumnName("Id").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.TenantId).HasColumnName("TenantId").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.TaskId).HasColumnName("TaskId").IsRequired().HasColumnType("bigint");
            builder.Property(x => x.Name).HasColumnName("Name").IsRequired().HasColumnType("nvarchar").HasMaxLength(256);
            builder.Property(x => x.IsDelete).HasColumnName("IsDelete").IsRequired().HasColumnType("bit");
            builder.Property(x => x.CreatorId).HasColumnName("CreatorId").IsRequired(false).HasColumnType("nvarchar").HasMaxLength(50);
            builder.Property(x => x.CreatorFullName).HasColumnName("CreatorFullName").IsRequired().HasColumnType("nvarchar").HasMaxLength(200);
            builder.Property(x => x.CreateTime).HasColumnName("CreateTime").IsRequired().HasColumnType("datetime");
            builder.Property(x => x.LastUpdate).HasColumnName("LastUpdateTime").IsRequired().HasColumnType("datetime");
            builder.Property(x => x.LastUpdateUserId).HasColumnName("LastUpdateUserId").IsRequired(false).IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.ConcurrencyStamp).HasColumnName("ConcurrencyStamp").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            builder.Property(x => x.LastUpdateFullName).HasColumnName("LastUpdateFullName").IsRequired().HasColumnType("nvarchar").HasMaxLength(200);
            builder.Property(x => x.Type).HasColumnName("Type").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);

            builder.ToTable("CheckLists").HasKey(x => x.Id);
        }
    }

}
