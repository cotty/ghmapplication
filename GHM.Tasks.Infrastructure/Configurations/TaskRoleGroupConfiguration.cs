﻿using GHM.Tasks.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GHM.Tasks.Infrastructure.Configurations
{
    public class TaskRoleGroupConfiguration : IEntityTypeConfiguration<TaskRoleGroup>
    {
        public void Configure(EntityTypeBuilder<TaskRoleGroup> builder)
        {
            builder.Property(x => x.TenantId).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.TaskParticipantId).IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(x => x.RoleGroupId).IsRequired().IsUnicode(false).HasMaxLength(50);

            builder.HasOne(x => x.TaskParticipant)
                .WithMany(x => x.TaskRoleGroup)
                .HasForeignKey(x => x.TaskParticipantId);

            builder.ToTable("TaskRoleGroups").HasKey(x => new { x.TenantId, x.RoleGroupId, x.TaskParticipantId });
        }
    }
}
