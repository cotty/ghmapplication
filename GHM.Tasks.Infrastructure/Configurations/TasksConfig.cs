﻿using Microsoft.EntityFrameworkCore;

namespace GHM.Tasks.Infrastructure.Configurations
{
    public class TasksConfig : IEntityTypeConfiguration<Domain.Models.Tasks>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Domain.Models.Tasks> builder)
        {
            builder.Property(x => x.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Property(x => x.TenantId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.Name).IsRequired().HasMaxLength(256);
            builder.Property(x => x.UnsignName).IsRequired().HasMaxLength(256).IsUnicode(false);
            builder.Property(x => x.CreatorId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.CreatorFullName).IsRequired().HasMaxLength(50);
            builder.Property(x => x.CreatorAvatar).IsRequired(false).HasMaxLength(500).IsUnicode(false);
            builder.Property(x => x.CreatorPositionId).IsRequired();
            builder.Property(x => x.CreatorPositionName).IsRequired().HasMaxLength(256);
            builder.Property(x => x.CreatorOfficeId).IsRequired();
            builder.Property(x => x.CreatorOfficeName).IsRequired().HasMaxLength(256);
            builder.Property(x => x.CreatorOfficeIdPath).IsRequired().HasMaxLength(500).IsUnicode(false);
            builder.Property(x => x.ResponsibleId).IsRequired().HasMaxLength(50).IsUnicode(false);
            builder.Property(x => x.ResponsibleFullName).IsRequired().HasMaxLength(50);
            builder.Property(x => x.ResponsibleAvatar).IsRequired(false).HasMaxLength(500).IsUnicode(false);
            builder.Property(x => x.ResponsiblePositionId).IsRequired().HasMaxLength(50);
            builder.Property(x => x.ResponsiblePositionName).IsRequired().HasMaxLength(256);
            builder.Property(x => x.ResponsibleOfficeId).IsRequired();
            builder.Property(x => x.ResponsibleOfficeName).IsRequired();
            builder.Property(x => x.ResponsibleOfficeIdPath).IsRequired().HasMaxLength(500).IsUnicode(false);
            builder.Property(x => x.Description).IsRequired(false).HasMaxLength(500);
            builder.Property(x => x.EstimateStartDate).IsRequired();
            builder.Property(x => x.EstimateEndDate).IsRequired();
            builder.Property(x => x.EstimateCount).IsRequired().HasDefaultValue(1);
            builder.Property(x => x.StartDate).IsRequired(false);
            builder.Property(x => x.EndDate).IsRequired(false);
            builder.Property(x => x.TotalDays).IsRequired(false);
            builder.Property(x => x.Status).IsRequired();
            builder.Property(x => x.Explanation).IsRequired(false).HasMaxLength(500);
            builder.Property(x => x.Day).IsRequired();
            builder.Property(x => x.Month).IsRequired();
            builder.Property(x => x.Quarter).IsRequired();
            builder.Property(x => x.Year).IsRequired();
            builder.Property(x => x.IsDelete).IsRequired().HasDefaultValue(false);
            builder.Property(x => x.IsIdea).IsRequired().HasDefaultValue(false);
            builder.Property(x => x.IsIdea).IsRequired().HasDefaultValue(false);
            builder.Property(x => x.IsTemplate).IsRequired().HasDefaultValue(false);
            builder.Property(x => x.ChildCount).IsRequired().HasDefaultValue(0);
            builder.Property(x => x.ParentId).IsRequired(false);
            builder.Property(x => x.ParentName).IsRequired(false).HasMaxLength(256);
            builder.Property(x => x.IdPath).IsRequired().HasMaxLength(500).IsUnicode(false);
            builder.Property(x => x.ApproveTime).IsRequired(false);
            builder.Property(x => x.Deadline).IsRequired(false);
            builder.Property(x => x.FromTemplateId).IsRequired(false);
            builder.Property(x => x.IsAllowResponsibleUpdate).IsRequired().HasDefaultValue(false);
            builder.ToTable("Tasks").HasKey(x => x.Id);
        }
    }
}
