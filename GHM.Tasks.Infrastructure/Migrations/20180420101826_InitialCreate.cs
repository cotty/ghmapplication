﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GHM.Tasks.Infrastructure.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApproveTime = table.Column<DateTime>(nullable: true),
                    ChildCount = table.Column<int>(nullable: false, defaultValue: 0),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    CreatorAvatar = table.Column<string>(unicode: false, maxLength: 500, nullable: false),
                    CreatorFullName = table.Column<string>(maxLength: 50, nullable: false),
                    CreatorId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    CreatorOfficeId = table.Column<int>(nullable: false),
                    CreatorOfficeIdPath = table.Column<string>(unicode: false, maxLength: 500, nullable: false),
                    CreatorOfficeName = table.Column<string>(maxLength: 256, nullable: false),
                    CreatorTitleId = table.Column<int>(nullable: false),
                    CreatorTitleName = table.Column<string>(maxLength: 256, nullable: false),
                    Day = table.Column<int>(nullable: false),
                    Deadline = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    EndDate = table.Column<DateTime>(nullable: true),
                    EstimateCount = table.Column<double>(nullable: false, defaultValue: 1.0),
                    EstimateEndDate = table.Column<DateTime>(nullable: false),
                    EstimateStartDate = table.Column<DateTime>(nullable: false),
                    Explanation = table.Column<string>(maxLength: 500, nullable: true),
                    FromTemplateId = table.Column<long>(nullable: true),
                    IdPath = table.Column<string>(unicode: false, maxLength: 500, nullable: false),
                    IsAllowResponsibleUpdate = table.Column<bool>(nullable: false, defaultValue: false),
                    IsDelete = table.Column<bool>(nullable: false, defaultValue: false),
                    IsIdea = table.Column<bool>(nullable: false, defaultValue: false),
                    IsTemplate = table.Column<bool>(nullable: false, defaultValue: false),
                    ManagerUserId = table.Column<string>(maxLength: 50, nullable: false),
                    Month = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    ParentId = table.Column<long>(nullable: true),
                    ParentName = table.Column<string>(maxLength: 256, nullable: true),
                    Quarter = table.Column<byte>(nullable: false),
                    ResponsibleAvatar = table.Column<string>(unicode: false, maxLength: 500, nullable: true),
                    ResponsibleFullName = table.Column<string>(maxLength: 50, nullable: false),
                    ResponsibleId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ResponsibleOfficeId = table.Column<int>(nullable: false),
                    ResponsibleOfficeIdPath = table.Column<string>(unicode: false, maxLength: 500, nullable: false),
                    ResponsibleOfficeName = table.Column<string>(nullable: false),
                    ResponsibleTitleId = table.Column<int>(nullable: false),
                    ResponsibleTitleName = table.Column<string>(maxLength: 256, nullable: false),
                    StartDate = table.Column<DateTime>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    TenantId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    TotalDays = table.Column<int>(nullable: true),
                    UnsignName = table.Column<string>(unicode: false, maxLength: 256, nullable: false),
                    Year = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TasksAttachments",
                columns: table => new
                {
                    TaskId = table.Column<long>(nullable: false),
                    AttachmentId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    AttachmentName = table.Column<string>(maxLength: 256, nullable: false),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    DonwloadCount = table.Column<int>(nullable: false),
                    FullName = table.Column<string>(maxLength: 50, nullable: false),
                    IsDelete = table.Column<bool>(nullable: false, defaultValue: false),
                    OwnerId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Type = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Url = table.Column<string>(unicode: false, maxLength: 500, nullable: false),
                    ViewCount = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TasksAttachments", x => new { x.TaskId, x.AttachmentId });
                });

            migrationBuilder.CreateTable(
                name: "TasksLogs",
                columns: table => new
                {
                    Id = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    FromValue = table.Column<string>(maxLength: 4000, nullable: false),
                    FullName = table.Column<string>(maxLength: 50, nullable: false),
                    TaskId = table.Column<long>(nullable: false),
                    ToValue = table.Column<string>(maxLength: 4000, nullable: true),
                    Type = table.Column<int>(nullable: false),
                    UpdateDisposition = table.Column<int>(nullable: true),
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TasksLogs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TasksParticipants",
                columns: table => new
                {
                    TaskId = table.Column<long>(nullable: false),
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Avatar = table.Column<string>(unicode: false, maxLength: 500, nullable: true),
                    FullName = table.Column<string>(maxLength: 50, nullable: false),
                    OfficeId = table.Column<int>(nullable: false),
                    OfficeIdPath = table.Column<string>(unicode: false, maxLength: 500, nullable: false),
                    OfficeName = table.Column<string>(maxLength: 256, nullable: false),
                    Roles = table.Column<int>(nullable: false),
                    TitleId = table.Column<int>(nullable: false),
                    TitleName = table.Column<string>(maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TasksParticipants", x => new { x.TaskId, x.UserId });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.DropTable(
                name: "TasksAttachments");

            migrationBuilder.DropTable(
                name: "TasksLogs");

            migrationBuilder.DropTable(
                name: "TasksParticipants");
        }
    }
}
