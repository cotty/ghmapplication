﻿using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Infrastructure.Configurations;
using Microsoft.EntityFrameworkCore;

namespace GHM.Tasks.Infrastructure
{
    public class TasksDbContext : DbContextBase
    {
        public TasksDbContext(DbContextOptions<TasksDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Tập tin đính kèm
            builder.Entity<Attachment>().ToTable("Attachments").HasKey(t => t.Id);

            // Check list công việc
            builder.Entity<CheckList>().ToTable("CheckLists").HasKey(t => t.Id);

            // Bình luận
            builder.Entity<Comment>().ToTable("Comments").HasKey(t => t.Id);

            // Nhật ký thay đổi
            builder.Entity<Log>().ToTable("Logs").HasKey(t => t.Id);

            // Nhóm quyền trong task
            builder.Entity<RoleGroup>().ToTable("RoleGroups").HasKey(t => t.Id);

            // Nhóm thư viện tiêu chí
            builder.Entity<TargetGroupLibrary>().ToTable("TargetGroupLibraries").HasKey(t => t.Id);

            // Nhóm tiêu chí
            builder.Entity<TargetGroup>().ToTable("TargetGroups").HasKey(t => t.Id);

            //Nhoms Thư viện tiêu chí
            builder.Entity<TargetGroupLibrary>().ToTable("TargetGroupLibraries").HasKey(t => t.Id);
            // Thu vien tieu chi
            builder.Entity<TargetLibrary>().ToTable("TargetLibraries").HasKey(t => t.Id);
            // Người tham gia mục tiêu
            builder.Entity<TargetParticipant>().ToTable("TargetParticipants").HasKey(t => t.Id);

            // Mục tiêu
            builder.Entity<Target>().ToTable("Targets").HasKey(t => t.Id);

            //Attachment
            builder.Entity<Attachment>().ToTable("Attachments").HasKey(x => x.Id);
            // Bảng mục tiêu
            builder.Entity<TargetTable>().ToTable("TargetTables").HasKey(t => t.Id);

            // Nhóm công việc trong thư viện
            builder.Entity<TaskGroupLibrary>().ToTable("TaskGroupLibraries").HasKey(t => t.Id);

            // Thư viện công việc
            builder.Entity<TaskLibrary>().ToTable("TaskLibraries").HasKey(t => t.Id);

            // Người tham gia công việc
            //builder.Entity<TaskParticipant>().ToTable("TaskParticipants").HasKey(t => t.Id);

            // Nhóm quyền người tham gia công việc
            //builder.Entity<TaskRoleGroup>().ToTable("TaskRoleGroups").HasKey(t => new { t.RoleGroupId, t.TaskParticipantId, t.TenantId });
            builder.ApplyConfiguration(new TaskParticipantConfiguration());
            builder.ApplyConfiguration(new TaskRoleGroupConfiguration());
            builder.ApplyConfiguration(new LogConfig());

            // Công việc
            builder.Entity<Domain.Models.Tasks>().ToTable("Tasks").HasKey(t => t.Id);
            // Role Group
            builder.Entity<RoleGroup>().ToTable("RoleGroups").HasKey(x => new { x.Id, x.TenantId });
            // Cong VIec Mau
            builder.Entity<TaskLibrary>().ToTable("TaskLibraries").HasKey(t => t.Id);
            // Nhom Cong viec mau
            builder.Entity<TaskGroupLibrary>().ToTable("TaskGroupLibraries").HasKey(t => t.Id);
            //Task Notification
            builder.Entity<TaskNotification>().ToTable("TaskNotification").HasKey(t => t.Id);
        }
    }
}
