﻿using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GHM.Tasks.Infrastructure.Repository
{
    public class TasksLibraryRepository : RepositoryBase, ITasksLibraryRepository
    {
        public readonly IRepository<TaskLibrary> _taskLibraryRepository;
        public TasksLibraryRepository(IDbContext context) : base(context)
        {
            _taskLibraryRepository = Context.GetRepository<TaskLibrary>();
        }

        public async Task<bool> CheckExistsByName(string tenantId, string taskGroupLibraryId, string name)
        {
            Expression<Func<TaskLibrary, bool>> spec = x => !x.IsDelete && x.TaskGroupLibraryId == taskGroupLibraryId
            && x.TenantId == tenantId && x.Name == name;

            return await _taskLibraryRepository.ExistAsync(spec);
        }

        public async Task<int> ForceDelete(string tenantId, string userId, string userFullName, long id)
        {
            var info = await GetInfo(tenantId, id);
            info.IsDelete = true;
            info.LastUpdateFullName = userFullName;
            info.LastUpdateUserId = userId;

            return await Context.SaveChangesAsync();
        }

        public async Task<TaskLibrary> GetInfo(string tenantId, long id, bool isReadOnly = false)
        {
            return await _taskLibraryRepository.GetAsync(isReadOnly, x => !x.IsDelete && x.TenantId == tenantId && x.Id == id);
        }

        public async Task<int> Insert(TaskLibrary taskLibrary)
        {
            _taskLibraryRepository.Create(taskLibrary);
            return await Context.SaveChangesAsync();
        }

        public Task<List<TaskGroupLibrary>> Search(string tenantId, long keyword, int page, int pageSize)
        {
            throw new NotImplementedException();
        }

        public async Task<int> Update(string tenantId, long id, TaskLibrary targetLibrary)
        {
            return await Context.SaveChangesAsync();
        }
    }
}
