﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace GHM.Tasks.Infrastructure.Repository
{
    public class TasksParticipantRepository : RepositoryBase, ITasksParticipantRepository
    {
        private readonly IRepository<TaskParticipant> _tasksParticipantsRepository;
        public TasksParticipantRepository(IDbContext context) : base(context)
        {
            _tasksParticipantsRepository = Context.GetRepository<TaskParticipant>();
        }

        public async Task<int> Save(TaskParticipant participant)
        {
            var info = await GetInfo(participant.TenantId, participant.TaskId, participant.UserId);
            if (info == null)
            {
                // Create new.
                _tasksParticipantsRepository.Create(participant);
                return await Context.SaveChangesAsync();
            }

            // Update existsing participant.
            //info.Roles = participant.Roles;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<TaskParticipant> listParticipant)
        {
            _tasksParticipantsRepository.Creates(listParticipant);
            return await Context.SaveChangesAsync();
        }

        public async Task<List<TaskParticipant>> GetsByTaskId(string tenantId, long taskId, bool isReadOnly)
        {
            return await _tasksParticipantsRepository.GetsAsync(isReadOnly, x => x.TaskId == taskId && x.TenantId == tenantId);
        }

        public async Task<bool> CheckExists(long taskId, string userId)
        {
            return await _tasksParticipantsRepository.ExistAsync(x => x.TaskId == taskId && x.UserId == userId);
        }

        public async Task<int> Delete(string tenantId, long taskId, string userId)
        {
            var taskParticipantInfo = await GetInfo(tenantId, taskId, userId);
            if (taskParticipantInfo == null)
                return -1;

            _tasksParticipantsRepository.Delete(taskParticipantInfo);
            return await Context.SaveChangesAsync();
        }

        public async Task<List<ParticipantViewModel>> GetsByTaskId(string tenantId, long taskId)
        {
            var query = from tp in Context.Set<TaskParticipant>()
                        join trg in Context.Set<TaskRoleGroup>() on tp.Id equals trg.TaskParticipantId
                        join rg in Context.Set<RoleGroup>() on trg.RoleGroupId equals rg.Id
                        where tp.TenantId == tenantId && trg.TenantId == tenantId && rg.TenantId == tenantId
                              && tp.TaskId == taskId
                        group new { trg, rg } by new { tp.Id, tp.FullName, tp.UserId } into g
                        select new ParticipantViewModel
                        {
                            Id = g.Key.Id,
                            UserId = g.Key.UserId,
                            FullName = g.Key.FullName,
                            TaskRoleGroupParticipants = g.Select(x => new RoleParticipantTaskViewModel
                            {
                                TaskParticipantsId = x.trg.TaskParticipantId,
                                RoleGroupId = x.trg.RoleGroupId,
                                Role = x.rg.Role
                            }).ToList()
                        };
            return await query.ToListAsync();
        }

        public async Task<List<TaskRoleParticipantsViewModel>> GetsAllByTaskId(string tenantId, long taskId)
        {
            var query = from tp in Context.Set<TaskParticipant>()
                        join trg in Context.Set<TaskRoleGroup>().Where(x=> x.TenantId == tenantId) on tp.Id equals trg.TaskParticipantId into g1
                        from trg in g1.DefaultIfEmpty()
                        join rg in Context.Set<RoleGroup>().Where(x => x.TenantId == tenantId) on trg.RoleGroupId equals rg.Id into g2
                        from rg in g2.DefaultIfEmpty()
                        where tp.TaskId == taskId && tp.TenantId == tenantId
                        group new { trg, rg } by new { tp.Id, tp.UserId, tp.FullName, tp.Avatar, tp.OfficeId, tp.PositionName, tp.OfficeName }
                        into g
                        select new TaskRoleParticipantsViewModel
                        {
                            Id = g.Key.Id,
                            UserId = g.Key.UserId,
                            FullName = g.Key.FullName,
                            Avatar = g.Key.Avatar,
                            OfficeId = g.Key.OfficeId,
                            OfficeName = g.Key.OfficeName,
                            PositionName = g.Key.PositionName,
                            RoleGroupIds = g.Select(x => new RoleGroupViewModel()
                            {
                                Id = x.trg != null ? x.trg.RoleGroupId : "",
                                Role = x.rg != null ? x.rg.Role : TaskRole.Read,
                                Name = x.rg != null ? x.rg.Name : ""
                            }).ToList()
                        };
            return await query.ToListAsync();
        }

        public async Task<int> Update()
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<TaskParticipant> GetInfo(string tenantId, long taskId, string userId)
        {
            return await _tasksParticipantsRepository.GetAsync(false, x => x.TaskId == taskId && x.UserId == userId && x.TenantId == tenantId);
        }

        public async Task<bool> CheckExist(string tenantId, long taskId, string userId)
        {
            return await _tasksParticipantsRepository.ExistAsync(x => x.TenantId == tenantId && x.TaskId == taskId && x.UserId == userId);
        }

        public async Task<int> FoceDeleteByTaskId(string tenantId, long taskId)
        {
            var listTaskPaticipants = await GetsByTaskId(tenantId, taskId, false);
            if (listTaskPaticipants == null || !listTaskPaticipants.Any())
                return -1;

            _tasksParticipantsRepository.Deletes(listTaskPaticipants);
            return await Context.SaveChangesAsync();
        }
    }
}
