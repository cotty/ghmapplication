﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;

namespace GHM.Tasks.Infrastructure.Repository
{
    public class LogRepository : RepositoryBase, ILogRepository
    {
        private readonly IRepository<Log> _logRepository;
        public LogRepository(IDbContext context) : base(context)
        {
            _logRepository = Context.GetRepository<Log>();
        }

        public async Task<int> Insert(Log log)
        {
            _logRepository.Create(log);
            return await Context.SaveChangesAsync();
        }

        public Task<List<Log>> Search(string tenantId, ObjectType logType, long id, int page, int pageSize, out int totalRows)
        {
            Expression<Func<Log, bool>> spec = x =>
                x.TenantId == tenantId && x.ObjectType == logType && x.ObjectId == id;

            var sort = Context.Filters.Sort<Log, DateTime>(x => x.CreateTime, true);
            var paging = Context.Filters.Page<Log>(page, pageSize);

            totalRows = _logRepository.Count(spec);
            return _logRepository.GetsAsync(true, spec, sort, paging);
        }
    }
}
