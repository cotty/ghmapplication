﻿using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GHM.Tasks.Infrastructure.Repository
{
    public class TargetLibraryRepository : RepositoryBase,ITargetLibraryRepository
    {
        public readonly IRepository<TargetLibrary> _targetLibrary;
        public TargetLibraryRepository(IDbContext context) : base(context)
        {
            _targetLibrary = Context.GetRepository<TargetLibrary>();
        }

        public async Task<int> ForceDelete(string tenantId, string id)
        {
            var info = await GetInfo(tenantId, id);
            if(info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<bool> CheckInfoByName(string tenantId, string targetGroupLibraryId, string name)
        {
            return await _targetLibrary.ExistAsync(x => !x.IsDelete && x.TenantId == tenantId && x.TargetGroupLibraryId == targetGroupLibraryId
            && x.Name == name);
        }

        public async Task<TargetLibrary> GetInfo(string tenantId, string id, bool isReadOnly = false)
        {
            return await _targetLibrary.GetAsync(isReadOnly, x => !x.IsDelete && x.TenantId == tenantId && x.Id == id);
        }

        public async Task<int> Insert(TargetLibrary targetLibrary)
        {
            _targetLibrary.Create(targetLibrary);
            return await Context.SaveChangesAsync();
        }

        public Task<List<TargetLibrary>> Search(string tenantId, string keyword, int page, int pageSize)
        {
            throw new NotImplementedException();
        }

        public Task<int> Update(TargetLibrary targetLibrary)
        {
            return Context.SaveChangesAsync();
        }
    }
}
