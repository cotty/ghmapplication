﻿using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Tasks.Infrastructure.Repository
{
    public class TargetParticipantRepository : RepositoryBase, ITargetParticipantRepository
    {
        private readonly IRepository<TargetParticipant> _targetParticipantRepository;

        public TargetParticipantRepository(IDbContext context) : base(context)
        {
            _targetParticipantRepository = Context.GetRepository<TargetParticipant>();
        }

        public async Task<bool> CheckExistsByTargetIdUserId(string tenantId, long targetId, string userId)
        {
            return await _targetParticipantRepository.ExistAsync(x => !x.IsDelete && x.TargetId == targetId && x.UserId == userId);
        }

        public async Task<int> FoceDeleteByTargetId(string tenantId, long targetId)
        {
            var listTargetParicipantByTargetId = await _targetParticipantRepository.GetsAsync(false, x => !x.IsDelete && x.TenantId == tenantId
            && x.TargetId == targetId);

            if (listTargetParicipantByTargetId == null || !listTargetParicipantByTargetId.Any())
                return -1;

            foreach(var item in listTargetParicipantByTargetId)
            {
                item.IsDelete = true;
            }
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(string tenantId, string id)
        {
            var participants = await _targetParticipantRepository.GetAsync(false, x => !x.IsDelete && x.TenantId == tenantId && x.Id == id);

            if (participants == null)
                return -1;

            participants.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<TargetParticipant> GetInfo(string tenantId, string id, bool isReadOnly = false)
        {
            return await _targetParticipantRepository.GetAsync(isReadOnly, x => !x.IsDelete && x.TenantId == tenantId && x.Id == id);
        }

        public async Task<TargetParticipant> GetInfo(string tenantId, long targetId, string userId, bool isReadOnly = false)
        {
            return await _targetParticipantRepository.GetAsync(isReadOnly, x => !x.IsDelete
            && x.TenantId == tenantId && x.UserId == userId && x.TargetId == targetId);
        }

        public Task<List<TargetParticipant>> GetListByTargetId(string tenantId, long id, out int totalRows)
        {
            Expression<Func<TargetParticipant, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId && x.TargetId == id;

            var listParticipant = Context.Set<TargetParticipant>().Where(spec);

            totalRows = _targetParticipantRepository.Count();

            return listParticipant.ToListAsync();
        }

        public async Task<int> InsertListParticipant(string tenantId, string userFullName, string userId,
            List<TargetParticipant> targetParticipantsMeta)
        {
            _targetParticipantRepository.Creates(targetParticipantsMeta);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> InsertParticipants(TargetParticipant targetParticipant)
        {
            _targetParticipantRepository.Create(targetParticipant);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(TargetParticipant info)
        {
            return await Context.SaveChangesAsync();
        }
    }
}
