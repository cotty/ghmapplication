﻿using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Tasks.Infrastructure.Repository
{
    public class CommentRepository : RepositoryBase, ICommentRepository
    {
        private IRepository<Comment> _commentRepository;
        public CommentRepository(IDbContext context) : base(context)
        {
            _commentRepository = Context.GetRepository<Comment>();
        }
        public async Task<bool> CheckExist(string tenantId, long? id)
        {
            return await _commentRepository.ExistAsync(x => !x.IsDelete && x.TenantId == tenantId && x.Id == id);
        }

        public async Task<int> ForceDelete(string tenantId, long id)
        {
            var info = await _commentRepository.GetAsync(false, x => x.TenantId == tenantId && x.Id == id);
            if (info == null)
                return -1;

            info.IsDelete = true;

            return await Context.SaveChangesAsync();
        }

        public async Task<Comment> GetAsync(string tenantId, long? id)
        {
            return await _commentRepository.GetAsync(false, x => x.TenantId == tenantId && x.Id == id);
        }

        public Task<List<Comment>> GetsAllByObjectId(string tenantId, ObjectType objectType, long objectId, int page, int pageSize, out int totalRows)
        {
            Expression<Func<Comment, bool>> spec = x => x.TenantId == tenantId && x.ObjectType == objectType && x.ObjectId == objectId && !x.IsDelete;
            var listComment = Context.Set<Comment>().Where(spec);

            totalRows = listComment.Count();

            return listComment.OrderByDescending(x => x.CreateTime)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        public async Task<int> Insert(Comment comment)
        {
            _commentRepository.Create(comment);

            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateChildCount(string tenantId, long? parentId)
        {
            var info = await GetAsync(tenantId, parentId);
            info.ChildCount++;

            return await Context.SaveChangesAsync();
        }

        public async Task<int> GetTotalCommentObjectId(string tenantId, long objectId, ObjectType objectType)
        {
            return await _commentRepository.CountAsync(x => !x.IsDelete && x.TenantId == tenantId && x.ObjectId == objectId && x.ObjectType == objectType);
        }
    }
}
