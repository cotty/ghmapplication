﻿using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GHM.Tasks.Infrastructure.Repository
{
    public class TargetRepository : RepositoryBase, ITargetRepository
    {
        private readonly IRepository<Target> _targetRepository;
        private readonly IRepository<TargetParticipant> _targetParticipantRepository;
        public TargetRepository(IDbContext context) : base(context)
        {
            _targetRepository = Context.GetRepository<Target>();
            _targetParticipantRepository = Context.GetRepository<TargetParticipant>();
        }

        public Task<bool> CheckExist(string tenantId, long id)
        {
            return _targetRepository.ExistAsync(x => !x.IsDelete && x.Id == id && x.TenantId == tenantId);
        }

        public async Task<bool> CheckNameExist(string tenantId, string targetGroupId, string targetTableId, string name)
        {
            return await _targetRepository.ExistAsync(x => !x.IsDelete && x.TenantId == tenantId
            && x.TargetGroupId == targetGroupId && x.TargetTableId == targetTableId && x.Name == name);
        }

        public async Task<int> Delete(string tenantId, long id)
        {
            var info = await GetInfo(tenantId, id, false);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> ForceDelete(string tenantId, long id)
        {
            var info = await GetInfo(tenantId, id, false);
            if (info == null)
                return -1;

            _targetRepository.Delete(info);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> GetChildCount(string tenantId, long id)
        {
            return await _targetRepository.CountAsync(x => x.TenantId == tenantId && x.ParentId == id && !x.IsDelete);
        }

        public async Task<int> GetChildCountComplete(string tenantId, long id)
        {
            return await _targetRepository.CountAsync(x => x.TenantId == tenantId && x.ParentId == id
            && !x.IsDelete && x.Status == TargetStatus.Finish);
        }

        public async Task<int> GetCountByGroup(string tenantId, string targetGroupId)
        {
            return await _targetRepository.CountAsync(x => x.TenantId == tenantId && x.TargetGroupId == targetGroupId && !x.IsDelete);
        }

        public async Task<int> GetCountByTargetTable(string tenantId, string targetTableId)
        {
            return await _targetRepository.CountAsync(x => x.TenantId == tenantId
            && x.TargetTableId == targetTableId && !x.IsDelete);
        }

        public async Task<bool> CheckExists(string tenantId, long targetId, TargetType targetType)
        {
            return await _targetRepository.ExistAsync(x =>
                x.TenantId == tenantId && x.Id == targetId && x.Type == targetType && !x.IsDelete);
        }

        public async Task<Target> GetInfo(string tenantId, long id, bool isReadOnly = false)
        {
            return await _targetRepository.GetAsync(isReadOnly, x => x.TenantId == tenantId && x.Id == id && !x.IsDelete);
        }

        public async Task<int> Insert(Target target)
        {
            _targetRepository.Create(target);
            return await Context.SaveChangesAsync();
        }

        public Task<List<TargetViewModel>> SearchTargetPersonal(string tenantId, string currentUserId, string currentOfficeIdPath,
            string userId, string keyword, DateTime? startDate, DateTime? endDate, string targetTableId, TargetStatus? status, List<int> listType)
        {
            Expression<Func<Target, bool>> spec = x => x.TenantId == tenantId && !x.IsDelete;
            Expression<Func<TargetGroup, bool>> specGroup = x => !x.IsDelete && x.TenantId == tenantId;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (!string.IsNullOrEmpty(targetTableId))
            {
                spec = spec.And(x => x.TargetTableId == targetTableId);
                specGroup = specGroup.And(x => x.TargetTableId == targetTableId);
            }

            if (startDate.HasValue)
            {
                startDate = startDate.Value.Date.AddMilliseconds(-1);
                spec = spec.And(x => x.StartDate >= startDate);
            }

            if (endDate.HasValue)
            {
                endDate = endDate.Value.Date.AddDays(1).AddMilliseconds(-1);
                spec = spec.And(x => x.StartDate <= endDate);
            }

            if (status.HasValue)
            {
                spec = spec.And(x => x.Status == status);
            }

            spec = spec.And(x => (x.UserId == userId && (x.CreatorId == currentUserId || userId == currentUserId || x.OfficeIdPath.Contains(currentOfficeIdPath + '.') || x.LevelOfSharing == LevelOfSharing.publics)
              && ((x.Type == TargetType.Personal && listType.Contains((int)TargetType.Personal)) || (x.Type == TargetType.Office && listType.Contains((int)TargetType.Office))))
              || (x.UserId != userId && listType.Contains((int)TargetType.Participants)));

            var result = from target in Context.Set<Target>().Where(spec)
                         join targetGroup in Context.Set<TargetGroup>().Where(specGroup)
                         on target.TargetGroupId equals targetGroup.Id
                         join targetTable in Context.Set<TargetTable>().Where(x => x.TenantId == tenantId && !x.IsDelete
                         && x.Status == TargetTableStatus.Using && (string.IsNullOrEmpty(targetTableId) || x.Id == targetTableId))
                         on target.TargetTableId equals targetTable.Id
                         join targetParicipant in Context.Set<TargetParticipant>().Where(x => !x.IsDelete && x.TenantId == tenantId
                         && x.UserId == userId && ((x.Role & (int)TargetRole.Read) == (int)TargetRole.Read))
                         on target.Id equals targetParicipant.TargetId
                         select new TargetViewModel()
                         {
                             Id = target.Id,
                             TargetTableId = target.TargetTableId,
                             TargetTableName = targetTable.Name,
                             TargetTableOrder = targetTable.Order,
                             TargetGroupId = target.TargetGroupId,
                             TargetGroupName = targetGroup.Name,
                             TargetGroupColor = targetGroup.Color,
                             TargetGroupOrder = targetGroup.Order,
                             ParentId = target.ParentId,
                             ChildCount = target.ChildCount,
                             ChildCountComplete = target.ChildCountComplete ?? 0,
                             IdPath = target.IdPath,
                             Name = target.Name,
                             Type = target.Type,
                             UserId = target.UserId,
                             FullName = target.FullName,
                             OfficeId = target.OfficeId,
                             OfficeName = target.OfficeName,
                             Weight = target.Weight,
                             StartDate = target.StartDate,
                             EndDate = target.EndDate,
                             PercentCompleted = target.PercentCompleted,
                             CompletedDate = target.CompletedDate,
                             Status = target.Status,
                             TotalTask = target.TotalTask,
                             TotalTaskComplete = target.TotalTaskComplete,
                             TotalComment = target.TotalComment,
                             MethodCalculateResult = target.MethodCalculateResult,
                             IsWireTarget = target.CreatorId == currentUserId || ((targetParicipant.Role & (int)TargetRole.WriteTarget) == (int)TargetRole.WriteTarget && targetParicipant.UserId == currentUserId),
                             OverdueDate = target.Status == TargetStatus.New ? (int)DateTime.Now.Date.Subtract(target.EndDate.Date).TotalDays
                             : target.CompletedDate.HasValue ? (int)target.CompletedDate.Value.Date.Subtract(target.EndDate.Date).TotalDays : 0
                         };

            return result.AsNoTracking().OrderBy(x => x.TargetTableOrder).ThenBy(x => x.TargetGroupOrder).ToListAsync();
        }

        public Task<List<TargetViewModel>> SearchTargetOffice(string tenantId, int officeId, string currentOfficeIdPath,
            string currentUserId, string keyword,
            DateTime? startDate, DateTime? endDate, string targetTableId, TargetStatus? status)
        {          
            Expression<Func<Target, bool>> spec = x => x.TenantId == tenantId && !x.IsDelete
            && x.Type == TargetType.Office && x.OfficeId == officeId && (x.OfficeIdPath.Contains(currentOfficeIdPath + '.')
            || x.OfficeIdPath == currentOfficeIdPath
            || x.UserId == currentUserId || x.LevelOfSharing == LevelOfSharing.publics || x.CreatorId == currentUserId);

            Expression<Func<TargetGroup, bool>> specGroup = x => !x.IsDelete && x.TenantId == tenantId;
            if (!string.IsNullOrEmpty(targetTableId))
            {
                spec = spec.And(x => x.TargetTableId == targetTableId);
                specGroup = specGroup.And(x => x.TargetTableId == targetTableId);
            }

            if (startDate.HasValue)
            {
                startDate = startDate.Value.Date.AddMilliseconds(-1);
                spec = spec.And(x => x.StartDate >= startDate);
            }

            if (endDate.HasValue)
            {
                endDate = endDate.Value.Date.AddDays(1).AddMilliseconds(-1);
                spec = spec.And(x => x.StartDate <= endDate);
            }

            if (status.HasValue)
            {
                spec = spec.And(x => x.Status == status);
            }

            var result = from target in Context.Set<Target>().Where(spec)
                         join targetGroup in Context.Set<TargetGroup>().Where(specGroup)
                         on target.TargetGroupId equals targetGroup.Id
                         join targetTable in Context.Set<TargetTable>().Where(x => x.TenantId == tenantId && !x.IsDelete
                         && x.Status == TargetTableStatus.Using && (string.IsNullOrEmpty(targetTableId) || x.Id == targetTableId))
                         on target.TargetTableId equals targetTable.Id
                         join targetParicipant in Context.Set<TargetParticipant>().Where(x => !x.IsDelete && x.TenantId == tenantId
                         && x.UserId == currentUserId && ((x.Role & (int)TargetRole.Read) == (int)TargetRole.Read))
                          on target.Id equals targetParicipant.TargetId into g
                         from targetParicipant in g.DefaultIfEmpty()
                         select new TargetViewModel()
                         {
                             Id = target.Id,
                             TargetTableId = target.TargetTableId,
                             TargetTableName = targetTable.Name,
                             TargetTableOrder = targetTable.Order,
                             TargetGroupId = target.TargetGroupId,
                             TargetGroupName = targetGroup.Name,
                             TargetGroupColor = targetGroup.Color,
                             TargetGroupOrder = targetGroup.Order,
                             ParentId = target.ParentId,
                             ChildCount = target.ChildCount,
                             ChildCountComplete = target.ChildCountComplete ?? 0,
                             IdPath = target.IdPath,
                             Name = target.Name,
                             Type = target.Type,
                             UserId = target.UserId,
                             FullName = target.FullName,
                             OfficeId = target.OfficeId,
                             OfficeName = target.OfficeName,
                             Weight = target.Weight,
                             StartDate = target.StartDate,
                             EndDate = target.EndDate,
                             CompletedDate = target.CompletedDate,
                             PercentCompleted = target.PercentCompleted,
                             Status = target.Status,
                             TotalTask = target.TotalTask,
                             TotalTaskComplete = target.TotalTaskComplete,
                             TotalComment = target.TotalComment,
                             MethodCalculateResult = target.MethodCalculateResult,
                             IsWireTarget = target.CreatorId == currentUserId
                             || ((targetParicipant.Role & (int)TargetRole.WriteTarget) == (int)TargetRole.WriteTarget && targetParicipant.UserId == currentUserId),
                             OverdueDate = target.Status == TargetStatus.New ? (int)DateTime.Now.Date.Subtract(target.EndDate.Date).TotalDays
                             :target.CompletedDate.HasValue ? (int)target.CompletedDate.Value.Date.Subtract(target.EndDate.Date).TotalDays: 0
                         };

            return result.AsNoTracking().OrderBy(x => x.TargetTableOrder).ThenBy(x => x.TargetGroupOrder).ToListAsync();
        }

        public async Task<int> Update(Target target)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateChildCount(string tenantId, long id)
        {
            var info = await GetInfo(tenantId, id);
            if (info == null)
                return -1;

            var childCount = await GetChildCount(tenantId, id);
            var childCountComplete = await GetChildCountComplete(tenantId, id);
            info.ChildCount = childCount;
            info.ChildCountComplete = childCountComplete;

            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateIdPath(long id, string idPath)
        {
            var info = await _targetRepository.GetAsync(false, x => x.Id == id);
            info.IdPath = idPath;

            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateTotalComment(string tenantId, long id, int totalComment)
        {
            var info = await GetInfo(tenantId, id);
            if (info == null)
                return -1;

            info.TotalComment = totalComment;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateTotalTask(string tenantId, long id, int totalTask, int toalTaskComplete)
        {
            var info = await GetInfo(tenantId, id);
            if (info == null)
                return -1;

            info.TotalTask = totalTask;
            info.TotalTaskComplete = toalTaskComplete;

            if (info.MethodCalculateResult == TargetMethodCalculateResultType.TargetChild && info.ChildCount == 0)
            {
                var percentComplete = totalTask > 0 ? Math.Round((decimal)toalTaskComplete * 100 / totalTask) : 0;
                info.PercentCompleted = (double)percentComplete;
                info.Status = (double)percentComplete == 100 ? TargetStatus.Finish : TargetStatus.New;
                await GeneralPercentCompletedOfTarget(tenantId, info.ParentId, info.IdPath);
            }
            return await Context.SaveChangesAsync();
        }

        public Task<List<TargetViewModel>> SearchSuggestion(string tenantId, string userId, string keyword, string targetTableId,
            TargetType? targetType, bool? isGetFinish = true)
        {
            Expression<Func<Target, bool>> spec = x => x.TenantId == tenantId && !x.IsDelete;
            if (targetType.HasValue)
            {
                spec = spec.And(x => x.Type == targetType);
            }

            if (!string.IsNullOrEmpty(targetTableId))
            {
                spec = spec.And(x => x.TargetTableId.Contains(targetTableId));
            }

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.Trim().StripVietnameseChars().ToUpper();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (isGetFinish.HasValue && !isGetFinish.Value)
            {
                spec = spec.And(x => x.Status != TargetStatus.Finish);
            }

            var result = from target in Context.Set<Target>().Where(spec)
                         join targetTable in Context.Set<TargetTable>().Where(x => x.TenantId == tenantId && !x.IsDelete && x.Status == TargetTableStatus.Using)
                         on target.TargetTableId equals targetTable.Id
                         join targetParicipant in Context.Set<TargetParticipant>().Where(x => !x.IsDelete && x.TenantId == tenantId
                         && x.UserId == userId)
                         on target.Id equals targetParicipant.TargetId into g
                         from targetParicipant in g.DefaultIfEmpty()
                         where target.UserId == userId || ((targetParicipant.Role & (int)TargetRole.WriteTask) == (int)TargetRole.WriteTask && targetParicipant.UserId == userId)
                         select new TargetViewModel()
                         {
                             Id = target.Id,
                             ParentId = target.ParentId,
                             IdPath = target.IdPath,
                             Name = target.Name,
                             ChildCount = target.ChildCount,
                             FullName = target.FullName,
                             Type = target.Type,
                             OfficeName = target.OfficeName
                         };

            return result.AsNoTracking().OrderBy(x => x.Name).ToListAsync();
        }

        public async Task<double?> SumWeightByParentId(string tenantId, string targetGroupId, long? parentId)
        {
            var listTarget = await _targetRepository.GetsAsync(true, x => !x.IsDelete && x.TenantId == tenantId
            && x.ParentId == parentId && x.TargetGroupId == targetGroupId);
            if (listTarget == null || !listTarget.Any())
                return 0;

            return listTarget.Sum(x => x.Weight);
        }

        public Task<List<TargetViewModel>> GetTargetChildById(string tenantId, string userId, string idPath)
        {
            Expression<Func<Target, bool>> spec = x => x.TenantId == tenantId && !x.IsDelete && x.IdPath.StartsWith(idPath + '.');
            Expression<Func<TargetGroup, bool>> specGroup = x => !x.IsDelete && x.TenantId == tenantId;

            var result = from target in Context.Set<Target>().Where(spec)
                         join targetGroup in Context.Set<TargetGroup>().Where(specGroup)
                         on target.TargetGroupId equals targetGroup.Id
                         join targetTable in Context.Set<TargetTable>().Where(x => x.TenantId == tenantId && !x.IsDelete
                         && x.Status == TargetTableStatus.Using)
                         on target.TargetTableId equals targetTable.Id
                         join targetParicipant in Context.Set<TargetParticipant>().Where(x => !x.IsDelete && x.TenantId == tenantId
                        && x.UserId == userId)
                        on target.Id equals targetParicipant.TargetId into g
                         from targetParicipant in g.DefaultIfEmpty()
                         select new TargetViewModel()
                         {
                             Id = target.Id,
                             TargetTableId = target.TargetTableId,
                             TargetTableName = targetTable.Name,
                             TargetTableOrder = targetTable.Order,
                             TargetGroupId = target.TargetGroupId,
                             TargetGroupName = targetGroup.Name,
                             TargetGroupColor = targetGroup.Color,
                             TargetGroupOrder = targetGroup.Order,
                             ParentId = target.ParentId,
                             ChildCount = target.ChildCount,
                             ChildCountComplete = target.ChildCountComplete ?? 0,
                             IdPath = target.IdPath,
                             Name = target.Name,
                             Description = target.Description,
                             Type = target.Type,
                             UserId = target.UserId,
                             FullName = target.FullName,
                             OfficeId = target.OfficeId,
                             OfficeName = target.OfficeName,
                             Weight = target.Weight,
                             StartDate = target.StartDate,
                             EndDate = target.EndDate,
                             CompletedDate = target.CompletedDate,
                             PercentCompleted = target.PercentCompleted,
                             Status = target.Status,
                             TotalTask = target.TotalTask,
                             TotalTaskComplete = target.TotalTaskComplete,
                             TotalComment = target.TotalComment,
                             MethodCalculateResult = target.MethodCalculateResult,
                             IsWireTarget = target.CreatorId == userId || ((targetParicipant.Role & (int)TargetRole.WriteTarget) == (int)TargetRole.WriteTarget),
                             OverdueDate = target.Status == TargetStatus.New ? (int)DateTime.Now.Date.Subtract(target.EndDate.Date).TotalDays
                             : (int)target.CompletedDate.Value.Date.Subtract(target.EndDate.Date).TotalDays
                         };

            return result.AsNoTracking().OrderBy(x => x.TargetTableOrder).ThenBy(x => x.TargetGroupOrder).ToListAsync();
        }

        public async Task<int> EquallyWeight(string tenantId, TargetType type, string targetGroupId, long? parentId)
        {
            var listTarget = await _targetRepository.GetsAsync(false, x => !x.IsDelete && x.TenantId == tenantId
            && x.ParentId == parentId && x.TargetGroupId == targetGroupId && x.Type == type);
            if (listTarget == null || !listTarget.Any())
                return 0;

            var weightBalance = Math.Round((double)(100 / listTarget.Count), 2);
            foreach (var target in listTarget)
            {
                target.Weight = weightBalance;
            }

            if (parentId.HasValue)
            {
                var targetParentInfo = await GetInfo(tenantId, parentId.Value);
                targetParentInfo.PercentCompleted = listTarget.Sum(x => x.PercentCompleted * weightBalance) / 100;
            }

            return await Context.SaveChangesAsync();
        }

        public async Task<List<Target>> GetAllTargetParent(string tenantId, long? parentId, string idPath, bool isReadOnly = false)
        {
            return await _targetRepository.GetsAsync(isReadOnly, x => x.TenantId == tenantId
            && (idPath.StartsWith(x.IdPath + '.') || x.ParentId == parentId) && !x.IsDelete);
        }

        public async Task<List<Target>> GetAllTargetChildren(string tenantId, string idPath, bool isReadOnly = false)
        {
            return await _targetRepository.GetsAsync(isReadOnly, x => x.TenantId == tenantId
           && (x.IdPath.StartsWith(idPath + '.')) && !x.IsDelete);
        }

        public async Task<List<Target>> GetTargetChildren(string tenantId, long? parentId, bool isReadOnly = false)
        {
            return await _targetRepository.GetsAsync(isReadOnly, x => x.TenantId == tenantId
           && x.ParentId == parentId && !x.IsDelete);
        }

        public Task<List<ReportTargetViewModel>> SearchForReport(string tenantId, DateTime? startDate, DateTime? endDate,
            TypeSearchTime typeSearchTime, string tableTargetId, TargetType typeTarget, TargetReportType targetReportType,
            int? officeId, string userId, out int totalRows)
        {
            Expression<Func<Target, bool>> spec = x => x.TenantId == tenantId;

            if (typeSearchTime == TypeSearchTime.StarteDateToEndDate)
            {
                if (startDate.HasValue)
                {
                    startDate = startDate.Value.Date;
                    spec = spec.And(x => x.StartDate >= startDate);
                }

                if (endDate.HasValue)
                {
                    endDate = endDate.Value.Date.AddDays(1).AddMilliseconds(-1);
                    spec = spec.And(x => x.EndDate <= endDate);
                }
            }

            if (typeSearchTime == TypeSearchTime.StartDate)
            {
                if (startDate.HasValue)
                {
                    startDate = startDate.Value.Date;
                    spec = spec.And(x => x.StartDate >= startDate);
                }

                if (endDate.HasValue)
                {
                    endDate = endDate.Value.Date.AddDays(1).AddMilliseconds(-1);
                    spec = spec.And(x => x.StartDate <= endDate);
                }
            }

            if (typeSearchTime == TypeSearchTime.CreateDate)
            {
                if (startDate.HasValue)
                {
                    startDate = startDate.Value.Date;
                    spec = spec.And(x => x.CreateTime >= startDate);
                }

                if (endDate.HasValue)
                {
                    endDate = endDate.Value.Date.AddDays(1).AddMilliseconds(-1);
                    spec = spec.And(x => x.CreateTime <= endDate);
                }
            }

            if (typeSearchTime == TypeSearchTime.EndDate)
            {
                if (startDate.HasValue)
                {
                    startDate = startDate.Value.Date;
                    spec = spec.And(x => x.EndDate >= startDate);
                }

                if (endDate.HasValue)
                {
                    endDate = endDate.Value.Date.AddDays(1).AddMilliseconds(-1);
                    spec = spec.And(x => x.EndDate <= endDate);
                }
            }

            if (typeSearchTime == TypeSearchTime.CompleteDate)
            {
                if (startDate.HasValue)
                {
                    startDate = startDate.Value.Date;
                    spec = spec.And(x => x.CompletedDate >= startDate);
                }

                if (endDate.HasValue)
                {
                    endDate = endDate.Value.Date.AddDays(1).AddMilliseconds(-1);
                    spec = spec.And(x => x.CompletedDate <= endDate);
                }
            }

            if (!string.IsNullOrEmpty(tableTargetId))
                spec = spec.And(x => x.TargetTableId == tableTargetId);

            if (typeTarget != TargetType.All)
                spec = spec.And(x => x.Type == typeTarget);

            if (officeId.HasValue)
                spec = spec.And(x => x.OfficeId == officeId.Value);

            if (targetReportType == TargetReportType.statisticsByPersonal)
            {
                if (!string.IsNullOrEmpty(userId))
                    spec = spec.And(x => x.UserId == userId);

                totalRows = _targetRepository.Count(spec);
                var query = from tg in Context.Set<Target>().Where(spec)
                            group new { tg } by new { tg.UserId, tg.FullName, tg.OfficeId, tg.OfficeName } into g
                            select new ReportTargetViewModel
                            {
                                UserId = g.Key.UserId,
                                FullName = g.Key.FullName,
                                OfficeId = g.Key.OfficeId,
                                OfficeName = g.Key.OfficeName,
                                ProgressNotComplete = g.Count(x => !x.tg.IsDelete && x.tg.Status == TargetStatus.New && x.tg.EndDate >= DateTime.Now),
                                OverDueNotComplete = g.Count(x => !x.tg.IsDelete && x.tg.Status == TargetStatus.New && x.tg.EndDate < DateTime.Now),
                                ProgressComplete = g.Count(x => !x.tg.IsDelete && x.tg.Status == TargetStatus.Finish && x.tg.EndDate >= x.tg.CompletedDate),
                                OverDueComplete = g.Count(x => !x.tg.IsDelete && x.tg.Status == TargetStatus.Finish && x.tg.EndDate < x.tg.CompletedDate),
                                TotalDestroy = g.Count(x => x.tg.IsDelete),
                                TotalTarget = g.Count(x => !x.tg.IsDelete),
                            };
                return query.ToListAsync();
            }
            else
            {
                totalRows = _targetRepository.Count(spec);
                var query = from tg in Context.Set<Target>().Where(spec)
                            group new { tg } by new { tg.OfficeId, tg.OfficeName } into g
                            select new ReportTargetViewModel
                            {
                                OfficeId = g.Key.OfficeId,
                                OfficeName = g.Key.OfficeName,
                                ProgressNotComplete = g.Count(x => !x.tg.IsDelete && x.tg.Status == TargetStatus.New && x.tg.EndDate >= DateTime.Now),
                                OverDueNotComplete = g.Count(x => !x.tg.IsDelete && x.tg.Status == TargetStatus.New && x.tg.EndDate < DateTime.Now),
                                ProgressComplete = g.Count(x => !x.tg.IsDelete && x.tg.Status == TargetStatus.Finish && x.tg.EndDate >= x.tg.CompletedDate),
                                OverDueComplete = g.Count(x => !x.tg.IsDelete && x.tg.Status == TargetStatus.Finish && x.tg.EndDate < x.tg.CompletedDate),
                                TotalDestroy = g.Count(x => x.tg.IsDelete),
                                TotalTarget = g.Count(x => !x.tg.IsDelete),
                            };
                return query.ToListAsync();
            }
        }

        // Tổng hợp dữ liêu lên mục tiêu cha
        public async Task<int> GeneralPercentCompletedOfTarget(string tenantId, long? parentId, string idPath)
        {
            var listAllTargetParent = await GetAllTargetParent(tenantId, parentId, idPath);
            if (listAllTargetParent == null || !listAllTargetParent.Any())
                return -1;

            return await UpdatePercentCompleteRecursive(tenantId, listAllTargetParent, parentId);
        }

        public async Task<int> UpdatePercentCompleteRecursive(string tenantId, List<Target> listTargets, long? parentId)
        {
            var parentTargetInfo = listTargets.FirstOrDefault(x => x.Id == parentId);
            if (parentTargetInfo == null || parentTargetInfo.MethodCalculateResult == TargetMethodCalculateResultType.Manually)
                return -2;

            var listChild = await GetTargetChildren(tenantId, parentId, true);
            if (listChild != null && listChild.Any())
            {
                var totalWeight = listChild.Sum(x => x.Weight);
                var totalPercentComplete = listChild.Sum(x => x.Weight * x.PercentCompleted);

                parentTargetInfo.PercentCompleted = totalWeight > 0 ? Math.Round((float)(totalPercentComplete / totalWeight), 2) : 0;
                await Context.SaveChangesAsync();
                await UpdatePercentCompleteRecursive(tenantId, listTargets, parentTargetInfo.ParentId);
            }

            return 1;
        }

        public async Task<bool> CheckPermission(string tenantId, Target targetInfo, string userId, TargetRole role)
        {
            if (targetInfo != null)
            {
                var targetParticipantInfo = await _targetParticipantRepository.GetAsync(true, x => x.TenantId == tenantId && x.TargetId == targetInfo.Id
                 && x.UserId == userId && !x.IsDelete);

                if ((targetParticipantInfo != null && (targetParticipantInfo.Role & (int)role) != (int)role)
                    && targetInfo.CreatorId != userId)
                    return false;

                return true;
            }
            return false;
        }

        public Task<List<TargetViewModel>> SearchReportDetail(string tenantId, TargetFilterReport targetFilterReport, int page, int pageSize, out int totalRows)
        {
            Expression<Func<Target, bool>> spec = x => x.TenantId == tenantId && !x.IsDelete;

            if (targetFilterReport.TypeSearchTime == TypeSearchTime.StarteDateToEndDate)
            {
                if (targetFilterReport.StartDate.HasValue)
                {
                    targetFilterReport.StartDate = targetFilterReport.StartDate.Value.Date;
                    spec = spec.And(x => x.StartDate >= targetFilterReport.StartDate);
                }

                if (targetFilterReport.EndDate.HasValue)
                {
                    targetFilterReport.EndDate = targetFilterReport.EndDate.Value.Date.AddDays(1).AddMilliseconds(-1);
                    spec = spec.And(x => x.EndDate <= targetFilterReport.EndDate);
                }
            }

            if (targetFilterReport.TypeSearchTime == TypeSearchTime.StartDate)
            {
                if (targetFilterReport.StartDate.HasValue)
                {
                    targetFilterReport.StartDate = targetFilterReport.StartDate.Value.Date;
                    spec = spec.And(x => x.StartDate >= targetFilterReport.StartDate);
                }

                if (targetFilterReport.EndDate.HasValue)
                {
                    targetFilterReport.EndDate = targetFilterReport.EndDate.Value.Date.AddDays(1).AddMilliseconds(-1);
                    spec = spec.And(x => x.StartDate <= targetFilterReport.EndDate);
                }
            }

            if (targetFilterReport.TypeSearchTime == TypeSearchTime.CreateDate)
            {
                if (targetFilterReport.StartDate.HasValue)
                {
                    targetFilterReport.StartDate = targetFilterReport.StartDate.Value.Date;
                    spec = spec.And(x => x.CreateTime >= targetFilterReport.StartDate);
                }

                if (targetFilterReport.EndDate.HasValue)
                {
                    targetFilterReport.EndDate = targetFilterReport.EndDate.Value.Date.AddDays(1).AddMilliseconds(-1);
                    spec = spec.And(x => x.CreateTime <= targetFilterReport.EndDate);
                }
            }

            if (targetFilterReport.TypeSearchTime == TypeSearchTime.EndDate)
            {
                if (targetFilterReport.StartDate.HasValue)
                {
                    targetFilterReport.StartDate = targetFilterReport.StartDate.Value.Date;
                    spec = spec.And(x => x.EndDate >= targetFilterReport.StartDate);
                }

                if (targetFilterReport.EndDate.HasValue)
                {
                    targetFilterReport.EndDate = targetFilterReport.EndDate.Value.Date.AddDays(1).AddMilliseconds(-1);
                    spec = spec.And(x => x.EndDate <= targetFilterReport.EndDate);
                }
            }

            if (targetFilterReport.TypeSearchTime == TypeSearchTime.CompleteDate)
            {
                if (targetFilterReport.StartDate.HasValue)
                {
                    targetFilterReport.StartDate = targetFilterReport.StartDate.Value.Date;
                    spec = spec.And(x => x.CompletedDate >= targetFilterReport.StartDate);
                }

                if (targetFilterReport.EndDate.HasValue)
                {
                    targetFilterReport.EndDate = targetFilterReport.EndDate.Value.Date.AddDays(1).AddMilliseconds(-1);
                    spec = spec.And(x => x.CompletedDate <= targetFilterReport.EndDate);
                }
            }

            if (!string.IsNullOrEmpty(targetFilterReport.TableTargetId))
                spec = spec.And(x => x.TargetTableId == targetFilterReport.TableTargetId);

            if (targetFilterReport.TargetType != TargetType.All)
                spec = spec.And(x => x.Type == targetFilterReport.TargetType);

            if (targetFilterReport.OfficeId.HasValue)
                spec = spec.And(x => x.OfficeId == targetFilterReport.OfficeId.Value);

            if (!string.IsNullOrEmpty(targetFilterReport.UserId))
                spec = spec.And(x => x.UserId == targetFilterReport.UserId);

            if (targetFilterReport.Status.HasValue && targetFilterReport.Status.Value != TargetStatus.All)
                spec = spec.And(x => x.Status == targetFilterReport.Status.Value);

            var result = from target in Context.Set<Target>().Where(spec)
                         join targetGroup in Context.Set<TargetGroup>().Where(x => !x.IsDelete && x.TenantId == tenantId)
                         on target.TargetGroupId equals targetGroup.Id
                         join targetTable in Context.Set<TargetTable>().Where(x => x.TenantId == tenantId && !x.IsDelete
                         && x.Status == TargetTableStatus.Using && (string.IsNullOrEmpty(targetFilterReport.TableTargetId) || x.Id == targetFilterReport.TableTargetId))
                         on target.TargetTableId equals targetTable.Id
                         select new TargetViewModel()
                         {
                             Id = target.Id,
                             TargetTableId = target.TargetTableId,
                             TargetTableName = targetTable.Name,
                             TargetTableOrder = targetTable.Order,
                             TargetGroupId = target.TargetGroupId,
                             TargetGroupName = targetGroup.Name,
                             TargetGroupColor = targetGroup.Color,
                             TargetGroupOrder = targetGroup.Order,
                             ParentId = target.ParentId,
                             ChildCount = target.ChildCount,
                             ChildCountComplete = target.ChildCountComplete ?? 0,
                             IdPath = target.IdPath,
                             Name = target.Name,
                             Type = target.Type,
                             UserId = target.UserId,
                             FullName = target.FullName,
                             OfficeId = target.OfficeId,
                             OfficeName = target.OfficeName,
                             Weight = target.Weight,
                             StartDate = target.StartDate,
                             EndDate = target.EndDate,
                             CompletedDate = target.CompletedDate,
                             PercentCompleted = target.PercentCompleted,
                             Status = target.Status,
                             TotalTask = target.TotalTask,
                             TotalTaskComplete = target.TotalTaskComplete,
                             TotalComment = target.TotalComment,
                             MethodCalculateResult = target.MethodCalculateResult,
                             CreateTime = target.CreateTime,
                             Description = target.Description
                         };

            totalRows = result.Count();
            return result.AsNoTracking().OrderBy(x => x.TargetTableOrder).ThenBy(x => x.TargetGroupOrder).ThenBy(x=> x.IdPath)
                .Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
        }
    }
}
