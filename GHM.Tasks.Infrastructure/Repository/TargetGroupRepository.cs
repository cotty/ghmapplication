﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace GHM.Tasks.Infrastructure.Repository
{
    public class TargetGroupRepository : RepositoryBase, ITargetGroupRepository
    {
        public readonly IRepository<TargetGroup> _targetGroupRepository;
        public TargetGroupRepository(IDbContext context) : base(context)
        {
            _targetGroupRepository = context.GetRepository<TargetGroup>();
        }

        public async Task<bool> CheckExistsName(string tenantId, string targetTableId, string name)
        {
            return await _targetGroupRepository.ExistAsync(x => x.TenantId == tenantId && x.TargetTableId == targetTableId
                         && !x.IsDelete && x.Name == name);
        }

        public async Task<int> Count(string tenantId)
        {
            return await _targetGroupRepository.CountAsync(x => x.TenantId == tenantId && !x.IsDelete);
        }

        public async Task<int> Delete(string tenantId, string id)
        {
            var targetGroupInfo = await GetInfo(tenantId, id);
            if (targetGroupInfo == null)
                return -1;

            targetGroupInfo.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<TargetGroup> GetInfo(string tenantId, string id, bool isReadOnly = false)
        {
            return await _targetGroupRepository.GetAsync(isReadOnly, x => x.TenantId == tenantId && x.Id == id && !x.IsDelete);
        }

        public async Task<int> Insert(TargetGroup targetGroup)
        {
            _targetGroupRepository.Create(targetGroup);

            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(List<TargetGroup> targetGroups)
        {
            _targetGroupRepository.Creates(targetGroups);

            return await Context.SaveChangesAsync();
        }

        public Task<List<TargetGroupSearchViewModel>> Search(string tenantId, string targetTableId)
        {
            Expression<Func<TargetGroup, bool>> spec = x => x.TenantId == tenantId && x.TargetTableId == targetTableId && !x.IsDelete;
            var result = Context.Set<TargetGroup>().Where(spec)
                         .Select(x => new TargetGroupSearchViewModel()
                         {
                             Id = x.Id,
                             TargetTableId = x.TargetTableId,
                             Name = x.Name,
                             Description = x.Description,
                             Order = x.Order,
                             Color = x.Color,
                             IsActive = x.IsActive,
                             ConcurrencyStamp = x.ConcurrencyStamp
                         }).AsNoTracking();

            return result.OrderBy(x => x.Order).AsNoTracking().ToListAsync();
        }

        public async Task<int> Update(TargetGroup targetGroup)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> CountByTargetTableId(string tenantId, string targetTableId)
        {
            return await _targetGroupRepository.CountAsync(x => x.TargetTableId == targetTableId && x.TenantId == tenantId && !x.IsDelete);
        }

        public async Task<int> SyncOrder(string tenantId, string targetTableId)
        {
            var listTargetGroupActive = await _targetGroupRepository.GetsAsync(false, x => !x.IsDelete && x.TenantId == tenantId
            && x.IsActive && x.TargetTableId == targetTableId);

            if (listTargetGroupActive != null && listTargetGroupActive.Count > 0)
            {
                var i = 1;
                foreach (var targetGroup in listTargetGroupActive.OrderBy(x => x.Order))
                {
                    targetGroup.Order = i;
                    i++;
                }

                return Context.SaveChanges();
            }
            return 1;
        }      

        public async Task<List<TargetGroupSuggestionViewModel>> GetTargetGroupByTargetTable(string tenantId, string targetTableId)
        {
            var query = Context.Set<TargetGroup>().Where(x => !x.IsDelete && x.TenantId == tenantId 
            && x.TargetTableId == targetTableId && x.IsActive)
                .Select(x => new TargetGroupSuggestionViewModel
                {
                    Id = x.Id,
                    Name = x.Name
                }).OrderByDescending(x => x.Name);

            return await query.AsNoTracking().ToListAsync();
        }

        public Task<bool> CheckExist(string tenantId, string id)
        {
            return _targetGroupRepository.ExistAsync(x => !x.IsDelete && x.TenantId == tenantId && x.Id == id);
        }
    }
}
