﻿using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Tasks.Infrastructure.Repository
{
    public class TargetGroupLibraryRepository : RepositoryBase, ITargetGroupLibraryRepository
    {
        private readonly IRepository<TargetGroupLibrary> _targetGroupLibrary;

        public TargetGroupLibraryRepository(IDbContext context) : base(context)
        {
            _targetGroupLibrary = Context.GetRepository<TargetGroupLibrary>();
        }

        public async Task<int> ForceDelete(string tenantId, string userId, string userFullName, string id)
        {
            var targetGroupLibrary = await GetInfo(tenantId, id);
            if (targetGroupLibrary == null)
                return -1;

            targetGroupLibrary.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<TargetGroupLibrary> GetInfo(string tenantId, string id, bool isReadOnly = false)
        {
            return await _targetGroupLibrary.GetAsync(isReadOnly, x=> !x.IsDelete && x.TenantId == tenantId && x.Id == id);
        }

        public async Task<bool> CheckInfoByName(string tenantId, string name)
        {
            return await _targetGroupLibrary.ExistAsync(x => x.TenantId == tenantId && x.Name == name && !x.IsDelete);
        }

        public async Task<int> Insert(TargetGroupLibrary targetLibrary)
        {
            _targetGroupLibrary.Create(targetLibrary);
            return await Context.SaveChangesAsync();
        }

        public Task<List<TargetGroupLibrary>> Search(string tenantId, string keyword, int page, int pageSize)
        {
            throw new NotImplementedException();
        }

        public async Task<int> Update(TargetGroupLibrary targetGroupLibrary)
        {
            return await Context.SaveChangesAsync();
        }

        public Task<List<TargetGroupLibraryViewModel>> GetAllAndTargetLibrary(string tenantId)
        {
            Expression<Func<TargetGroupLibrary, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId;
            var query = from tgl in Context.Set<TargetGroupLibrary>().Where(spec)
                        join tl in Context.Set<TargetLibrary>().Where(x => !x.IsDelete) on tgl.Id equals tl.TargetGroupLibraryId into tls
                        from tl in tls.DefaultIfEmpty()
                        group new { tgl, tl}
                        by new { tgl.Id, tgl.Name, tgl.ConcurrencyStamp } into g
                        select new TargetGroupLibraryViewModel
                        {
                            Id = g.Key.Id,
                            Name = g.Key.Name,
                            ConcurrencyStamp = g.Key.ConcurrencyStamp,
                            TargetLibrary = g.Count(x=> x.tl != null) > 0 ?
                            g.Select(x => new TargetLibraryViewModel
                            {
                                Id = x.tl.Id ,
                                Description = x.tl.Description,
                                MeasurementMethod =  x.tl.MeasurementMethod,
                                Name =  x.tl.Name,
                                TargetGroupLibraryId =  x.tl.TargetGroupLibraryId,
                                ConcurrencyStamp = x.tl.ConcurrencyStamp
                            }).OrderBy(x => x.Id).ToList() : null
                        };

            return query.AsNoTracking().OrderBy(x => x.Name).ToListAsync();
        }
    }
}
