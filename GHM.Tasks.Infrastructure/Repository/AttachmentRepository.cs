﻿using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GHM.Tasks.Infrastructure.Repository
{
    public class AttachmentRepository : RepositoryBase, IAttachmentRepository
    {
        private readonly IRepository<Attachment> _attachmentRepository;
        public AttachmentRepository(IDbContext context) : base(context)
        {
            _attachmentRepository = Context.GetRepository<Attachment>();
        }

        public async Task<bool> CheckExistsByFileIdObjectId(string tenantId, long objectId, ObjectType objectType, string fileId)
        {
            return await _attachmentRepository.ExistAsync(x => !x.IsDelete && x.ObjectId == objectId && x.ObjectType == objectType
                                                        && x.FileId == fileId);
        }

        public async Task<int> Delete(string tenantId, string id)
        {
            var info = await _attachmentRepository.GetAsync(false, x => x.TenantId == tenantId && x.Id == id);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public Task<List<Attachment>> GetsAllByObjectId(string tenantId, ObjectType objectType, long objectId, int page, int pageSize, out int totalAttachment)
        {
            Expression<Func<Attachment, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId && x.ObjectType == objectType && x.ObjectId == objectId;
            var listComment = Context.Set<Attachment>().Where(spec);

            totalAttachment = _attachmentRepository.Count();

            return listComment.OrderByDescending(x => x.CreateTime)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        public async Task<int> InsertAttachment(Attachment attachment)
        {
            _attachmentRepository.Create(attachment);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> InsertAttachments(List<Attachment> attachments)
        {
            _attachmentRepository.Creates(attachments);
            return await Context.SaveChangesAsync();
        }

        public async Task<Attachment> GetInfo(string tenantId, string id, bool isReadOnly = false)
        {
            return await _attachmentRepository.GetAsync(isReadOnly, x => x.TenantId == tenantId && !x.IsDelete && x.Id == id);
        }
    }
}
