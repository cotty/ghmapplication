﻿using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;
using System;
using System.Threading.Tasks;

namespace GHM.Tasks.Infrastructure.Repository
{
    public class TaskGroupRepository: RepositoryBase, ITaskGroupRepository
    {
        private readonly IRepository<TaskGroup> _taskGroupRepository;
        public TaskGroupRepository(IDbContext context) : base(context)
        {
            _taskGroupRepository = Context.GetRepository<TaskGroup>();
        }

        public async Task<bool> CheckExistsName(string tenantId, string name)
        {
            return await _taskGroupRepository.ExistAsync(x => x.TenantId == tenantId && !x.IsDelete && x.Name == name);
        }

        public Task<int> CountByActive(string tenantId)
        {
            throw new NotImplementedException();
        }

        public Task<int> Delete(string tenantId, string taskGroupId)
        {
            throw new NotImplementedException();
        }

        public async Task<TaskGroup> GetInfo(string tenantId, string taskGroupId, bool isReadOnly = false)
        {
            return await _taskGroupRepository.GetAsync(isReadOnly, x => x.TenantId == tenantId
            && x.Id == taskGroupId && !x.IsDelete);
        }

        public async Task<int> Insert(TaskGroup taskGroup)
        {
            _taskGroupRepository.Create(taskGroup);

            return await Context.SaveChangesAsync();
        }

        public Task<int> SyncOrder(string tenantId)
        {
            throw new NotImplementedException();
        }

        public async Task<int> Update(TaskGroup taskGroup)
        {
            return await Context.SaveChangesAsync();
        }
    }
}
