﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace GHM.Tasks.Infrastructure.Repository
{
    public class TargetTableRepository : RepositoryBase, ITargetTableRepository
    {
        public readonly IRepository<TargetTable> _targetTableRepository;

        public TargetTableRepository(IDbContext context) : base(context)
        {
            _targetTableRepository = Context.GetRepository<TargetTable>();
        }

        public async Task<bool> CheckExistsName(string tenantId, string name)
        {
            return await _targetTableRepository.ExistAsync(x => x.TenantId == tenantId && !x.IsDelete && x.Name == name);
        }

        public async Task<int> CountByActive(string tenantId)
        {
            return await _targetTableRepository.CountAsync(x => x.TenantId == tenantId && !x.IsDelete && x.Status.HasValue
                   && x.Status.Value == TargetTableStatus.Using);
        }

        public async Task<int> Delete(string tenantId, string targetTableId)
        {
            var targetTableInfo = await GetInfo(tenantId, targetTableId);
            if (targetTableInfo == null)
                return -1;

            targetTableInfo.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<TargetTable> GetInfo(string tenantId, string targetTableId, bool isReadOnly = false)
        {
            return await _targetTableRepository.GetAsync(isReadOnly, x => !x.IsDelete && x.TenantId == tenantId && x.Id == targetTableId);
        }

        public async Task<int> Insert(TargetTable targetTable)
        {
            _targetTableRepository.Create(targetTable);
            return await Context.SaveChangesAsync();
        }

        public Task<List<TargetTableSearchViewModel>> Search(string tenantId)
        {
            Expression<Func<TargetTable, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId;

            var query = Context.Set<TargetTable>().Where(spec)
                .AsNoTracking()
                 .OrderBy(x => x.Order == null)
                .ThenBy(x => x.Order)
                .Select(x => new TargetTableSearchViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate,
                    Status = x.Status,
                    Order = x.Order
                });

            return query.AsNoTracking().ToListAsync();
        }

        public async Task<int> Update(TargetTable targetTable)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> SyncOrder(string tenantId)
        {
            var listTargetTableActive = await _targetTableRepository.GetsAsync(false, x => !x.IsDelete && x.TenantId == tenantId
            && x.Status.HasValue && x.Status.Value == TargetTableStatus.Using);

            if (listTargetTableActive != null && listTargetTableActive.Count > 0)
            {
                var i = 1;
                foreach (var targetTable in listTargetTableActive.OrderBy(x => x.Order))
                {
                    targetTable.Order = i;
                    i++;
                }

                return Context.SaveChanges();
            }

            return 1;
        }

        public async Task<List<TargetTableForSuggestionViewModel>> GetForSuggestion(string tenantId)
        {
            var query = Context.Set<TargetTable>().Where(x => !x.IsDelete && x.TenantId == tenantId && x.Status == TargetTableStatus.Using)
                .Select(x => new TargetTableForSuggestionViewModel
                {
                    Id = x.Id,
                    Name = x.Name
                }).OrderByDescending(x => x.Name);

            return await query.AsNoTracking().ToListAsync();
        }
    }
}
