﻿using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Tasks.Domain.ViewModels;

namespace GHM.Tasks.Infrastructure.Repository
{
    public class RoleGroupRepository : RepositoryBase, IRoleGroupRepository
    {
        private readonly IRepository<RoleGroup> _roleGroupRepository;
        public RoleGroupRepository(IDbContext context) : base(context)
        {
            _roleGroupRepository = Context.GetRepository<RoleGroup>();
        }

        public async Task<bool> CheckExistsByName(string tenantId, string name)
        {
            return await _roleGroupRepository.ExistAsync(x => x.TenantId == tenantId && x.Name == name);
        }

        public async Task<List<RoleGroupViewModel>> GetAll(string tenantId)
        {
            return await _roleGroupRepository.GetsAsAsync(x => new RoleGroupViewModel
            {
                Id = x.Id,
                Name = x.Name,
                Role = x.Role,
            }, x => x.TenantId == tenantId && x.Id != "NNV" && x.Id != "NGV" && x.Id != "CVN");// && x.Id != "NNV" && x.Id != "NGV"
        }

        public async Task<int> Delete(string tenantId, string keyword)
        {
            var roleGroup = await GetInfoById(tenantId, keyword);
            if (roleGroup == null)
                return -1;

            _roleGroupRepository.Delete(roleGroup);
            return await Context.SaveChangesAsync();
        }

        public Task<RoleGroup> GetInfo(string tenantId, string keyword)
        {
            Expression<Func<RoleGroup, bool>> spec = x => x.TenantId == tenantId;

            spec = spec.And(x => x.TenantId == tenantId && x.Name.Contains(keyword));
            return _roleGroupRepository.GetAsync(false, spec);
        }

        public async Task<RoleGroup> GetInfoById(string tenantId, string id)
        {
            return await _roleGroupRepository.GetAsync(false, x => x.TenantId == tenantId && x.Id == id);
        }

        public async Task<int> Insert(string tenantId, RoleGroup roleGroup)
        {
            _roleGroupRepository.Create(roleGroup);
            return await Context.SaveChangesAsync();
        }

        public Task<List<RoleGroup>> Search(string tenantId, string keyword, int page, int pageSize, out int totalRows)
        {
            Expression<Func<RoleGroup, bool>> spec = x => x.TenantId == tenantId;

            if (!string.IsNullOrEmpty(keyword))
            {
                spec = spec.And(x => x.Name.Contains(keyword));
            }

            var sort = Context.Filters.Sort<RoleGroup, string>(x => x.Id, true);
            var paging = Context.Filters.Page<RoleGroup>(page, pageSize);
            totalRows = _roleGroupRepository.Count(spec);
            return _roleGroupRepository.GetsAsync(true, spec, sort, paging);
        }

        public async Task<int> Update(RoleGroup roleGroup)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Inserts(string tenantId, List<RoleGroup> roleGroups)
        {
            _roleGroupRepository.Creates(roleGroups);

            return await Context.SaveChangesAsync();
        }
    }
}
