﻿using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Tasks.Infrastructure.Repository
{
    public class TaskNotificationRepository : RepositoryBase,ITaskNotificationRepository
    {
        private readonly IRepository<TaskNotification> _taskNotificationRepository;
        public TaskNotificationRepository(IDbContext context) : base(context) {
            _taskNotificationRepository = Context.GetRepository<TaskNotification>();
        }
        public async Task<TaskNotification> GetInfo(string id, string tenantId)
        {
            return await _taskNotificationRepository.GetAsync(false, x => x.Id == id && x.TenantId == tenantId);
        }

        public async Task<TaskNotification> GetInfoByTaskId(long taskId, string tenantId, bool readOnly = false)
        {
            return await _taskNotificationRepository.GetAsync(readOnly, x => x.TaskId == taskId && x.TenantId == tenantId);
        }

        public async Task<int> Insert(TaskNotification taskNotification)
        {
            _taskNotificationRepository.Create(taskNotification);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(TaskNotification info)
        {
            return await Context.SaveChangesAsync();
        }
    }
}
