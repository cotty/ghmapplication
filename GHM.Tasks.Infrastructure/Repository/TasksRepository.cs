﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.SqlServer;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.Resources;
using GHM.Tasks.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace GHM.Tasks.Infrastructure.Repository
{
    using GHM.Tasks.Domain.ModelMetas;
    using Tasks = Domain.Models.Tasks;
    public class TasksRepository : RepositoryBase, ITasksRepository
    {
        private readonly IRepository<Tasks> _taskRepository;
        private readonly IResourceService<GhmTaskResource> _taskResourceService;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        public TasksRepository(IDbContext context, IResourceService<GhmTaskResource> taskResourceService,
            IResourceService<SharedResource> sharedResourceService) : base(context)
        {
            _taskResourceService = taskResourceService;
            _sharedResourceService = sharedResourceService;
            _taskRepository = Context.GetRepository<Tasks>();
        }

        public Task<List<T>> Search<T>(Expression<Func<Tasks, T>> projector, string tenantId, string keyword, int? officeId, string userId, byte? status,
            DateTime? estmateStateDate, DateTime? estimateEndDate, int page, int pageSize, out int totalRows)
        {
            throw new NotImplementedException();
        }

        public Task<List<T>> SearchForManager<T>(Expression<Func<Tasks, T>> projector, string keyword, string userId, byte? status, DateTime? fromDateStart,
            DateTime? toDateStart, DateTime? fromDateEnd, DateTime? toDateEnd, bool? isOverdue, int page, int pageSize,
            out int totalRows)
        {
            throw new NotImplementedException();
        }

        public Task<List<T>> SearchForManager<T>(Expression<Func<Tasks, T>> projector, string keyword, List<string> listUser, byte? status, DateTime? fromDateStart,
            DateTime? toDateStart, DateTime? fromDateEnd, DateTime? toDateEnd, bool? isOverdue, int page, int pageSize,
            out int totalRows)
        {
            throw new NotImplementedException();
        }

        public async Task<long> Insert(Tasks task)
        {
            _taskRepository.Create(task);
            var result = await Context.SaveChangesAsync();
            return result > 0 ? task.Id : result;
        }

        public async Task<List<Tasks>> GetsSubTasks(string tenantId, long parentId)
        {
            return await _taskRepository.GetsAsync(false, x => x.ParentId == parentId && !x.IsDelete);
        }

        public async Task<int> Update(Tasks task)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<ActionResultResponse> Update(Tasks task, string userId, string fullName)
        {
            var result = await Context.SaveChangesAsync();
            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString("Something went wrong please contact with administrator.")
                : _taskResourceService.GetString("Update task successful."));
        }

        public async Task<int> UpdateChildCount(string tenantId, long taskId)
        {
            var taskInfo = await GetInfo(tenantId, taskId, false);
            if (taskInfo == null)
                return -1;

            var childCount = await GetChildCountByTaskId(tenantId, taskId);
            taskInfo.ChildCount = childCount;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> GetChildCountByTaskId(string tenantId, long taskId)
        {
            return await _taskRepository.CountAsync(x => !x.IsDelete && x.TenantId == tenantId && x.ParentId == taskId);
        }

        public async Task<ActionResultResponse> UpdateDeadline(long id, DateTime deadline, string userId, string fullName)
        {
            throw new NotImplementedException();
        }

        public async Task<List<ActionResultResponse>> UpdatesApproveFinish(List<long> taskIds, bool isApprove, string userId, string fullName)
        {
            throw new NotImplementedException();
        }

        public async Task<Tasks> GetInfo(string tenantId, long id, bool isReadOnly = false)
        {
            return await _taskRepository.GetAsync(isReadOnly, x => x.TenantId == tenantId && x.Id == id && !x.IsDelete);
        }

        public async Task<ActionResultResponse> Delete(string tenantId, long taskId)
        {
            var taskInfo = await GetInfo(tenantId, taskId);
            if (taskInfo == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("Task does not exists."));

            taskInfo.IsDelete = true;
            var result = await Context.SaveChangesAsync();
            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString("Something went wrong please contact with administrator.")
                : _taskResourceService.GetString("Delete task successful."));
        }

        public async Task<SearchResult<TasksSearchViewModel>> SearchProgressTask(string keyword, string currentUserId, string userId,
            int? officeId, DateTime? estimateStartDate, DateTime? estimateEndDate, int page, int pageSize)
        {
            Expression<Func<Tasks, bool>> spec = x => !x.IsDelete && !x.ParentId.HasValue && x.Status == TasksStatus.InProgress;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripChars().Trim();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (!string.IsNullOrEmpty(userId))
                spec = spec.And(x => x.ResponsibleId == userId);

            if (officeId.HasValue)
                spec = spec.And(x => x.ResponsibleOfficeId == officeId.Value);

            if (string.IsNullOrEmpty(userId) && !officeId.HasValue)
                spec = spec.And(x => x.ResponsibleId == currentUserId);

            if (estimateStartDate.HasValue)
                spec = spec.And(x => x.EstimateStartDate.Date == estimateStartDate.Value.Date);

            if (estimateEndDate.HasValue)
                spec = spec.And(x => x.EstimateEndDate.Date == estimateEndDate.Value.Date);

            var sort = Context.Filters.Sort<Tasks, long>(x => x.Id, true);
            var paging = Context.Filters.Page<Tasks>(page, pageSize);

            var totalRows = _taskRepository.Count(spec);
            var items = await _taskRepository.GetsAsAsync(x => new TasksSearchViewModel
            {
                Id = x.Id,
                Name = x.Name,
                CreateTime = x.CreateTime,
                CreatorId = x.CreatorId,
                CreatorFullName = x.CreatorFullName,
                CreatorAvatar = x.CreatorAvatar,
                CreatorPositionId = x.CreatorPositionId,
                CreatorPositionName = x.CreatorPositionName,
                CreatorOfficeId = x.CreatorOfficeId,
                CreatorOfficeName = x.CreatorOfficeName,
                CreatorOfficeIdPath = x.CreatorOfficeIdPath,
                ResponsibleId = x.ResponsibleId,
                ResponsibleFullName = x.ResponsibleFullName,
                ResponsibleAvatar = x.ResponsibleAvatar,
                ResponsiblePositionId = x.ResponsiblePositionId,
                ResponsiblePositionName = x.ResponsiblePositionName,
                ResponsibleOfficeId = x.ResponsibleOfficeId,
                ResponsibleOfficeName = x.ResponsibleOfficeName,
                ResponsibleOfficeIdPath = x.ResponsibleOfficeIdPath,
                Status = x.Status,
                ChildCount = x.ChildCount,
                EstimateStartDate = x.EstimateStartDate,
                EstimateEndDate = x.EstimateEndDate,
                EstimateCount = x.EstimateCount,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                IsIdea = x.IsIdea,
                IsAllowResponsibleUpdate = x.IsAllowResponsibleUpdate
            }, spec, sort, paging);
            return new SearchResult<TasksSearchViewModel>
            {
                Items = items,
                TotalRows = totalRows
            };
        }

        public async Task<SearchResult<TasksSearchViewModel>> SearchMyTaskOwned(string keyword, TasksStatus? status, string currentUserId, string userId, int? officeId,
            DateTime? estimateStartDate, DateTime? estimateEndDate, int page, int pageSize)
        {
            Expression<Func<Tasks, bool>> spec = x => !x.IsDelete && !x.ParentId.HasValue && x.CreatorId == currentUserId;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripChars().Trim();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (status.HasValue)
                spec = spec.And(x => x.Status == status.Value);

            if (!string.IsNullOrEmpty(userId))
                spec = spec.And(x => x.ResponsibleId == userId);

            if (officeId.HasValue)
                spec = spec.And(x => x.ResponsibleOfficeId == officeId.Value);

            if (estimateStartDate.HasValue)
                spec = spec.And(x => x.EstimateStartDate.Date == estimateStartDate.Value.Date);

            if (estimateEndDate.HasValue)
                spec = spec.And(x => x.EstimateEndDate.Date == estimateEndDate.Value.Date);

            var sort = Context.Filters.Sort<Tasks, long>(x => x.Id, true);
            var paging = Context.Filters.Page<Tasks>(page, pageSize);

            var totalRows = _taskRepository.Count(spec);
            var items = await _taskRepository.GetsAsAsync(x => new TasksSearchViewModel
            {
                Id = x.Id,
                Name = x.Name,
                CreateTime = x.CreateTime,
                CreatorId = x.CreatorId,
                CreatorFullName = x.CreatorFullName,
                CreatorAvatar = x.CreatorAvatar,
                CreatorPositionId = x.CreatorPositionId,
                CreatorPositionName = x.CreatorPositionName,
                CreatorOfficeId = x.CreatorOfficeId,
                CreatorOfficeName = x.CreatorOfficeName,
                CreatorOfficeIdPath = x.CreatorOfficeIdPath,
                ResponsibleId = x.ResponsibleId,
                ResponsibleFullName = x.ResponsibleFullName,
                ResponsibleAvatar = x.ResponsibleAvatar,
                ResponsiblePositionId = x.ResponsiblePositionId,
                ResponsiblePositionName = x.ResponsiblePositionName,
                ResponsibleOfficeId = x.ResponsibleOfficeId,
                ResponsibleOfficeName = x.ResponsibleOfficeName,
                ResponsibleOfficeIdPath = x.ResponsibleOfficeIdPath,
                Status = x.Status,
                ChildCount = x.ChildCount,
                EstimateStartDate = x.EstimateStartDate,
                EstimateEndDate = x.EstimateEndDate,
                EstimateCount = x.EstimateCount,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                IsIdea = x.IsIdea,
                IsAllowResponsibleUpdate = x.IsAllowResponsibleUpdate
            }, spec, sort, paging);
            return new SearchResult<TasksSearchViewModel>
            {
                Items = items,
                TotalRows = totalRows
            };
        }

        public async Task<SearchResult<TasksSearchViewModel>> SearchParticipantOrFollowed(string keyword, TasksStatus? status, string currentUserId, string userId, int? officeId, bool isParticipant,
            DateTime? estimateStartDate, DateTime? estimateEndDate, int page, int pageSize)
        {
            Expression<Func<Tasks, bool>> spec = x => !x.IsDelete && !x.ParentId.HasValue;
            Expression<Func<TaskParticipant, bool>> specParticipant = x => true;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripChars().Trim();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (status.HasValue)
                spec = spec.And(x => x.Status == status);

            if (!string.IsNullOrEmpty(userId))
                specParticipant = specParticipant.And(x => x.UserId == userId);

            if (officeId.HasValue)
                specParticipant = specParticipant.And(x => x.OfficeId == officeId.Value);

            if (string.IsNullOrEmpty(userId) && !officeId.HasValue)
                specParticipant = specParticipant.And(x => x.UserId == currentUserId);

            if (estimateStartDate.HasValue)
                spec = spec.And(x => x.EstimateStartDate.Date == estimateStartDate.Value.Date);

            if (estimateEndDate.HasValue)
                spec = spec.And(x => x.EstimateEndDate.Date == estimateEndDate.Value.Date);

            //if (isParticipant)
            //{
            //    specParticipant = specParticipant.And(x =>
            //        (x.Role & (int)ParticipantRole.Participant) == (int)ParticipantRole.Participant);
            //}
            //else
            //{
            //    specParticipant = specParticipant.And(x =>
            //        (x.Roles & (int)ParticipantRole.Observer) == (int)ParticipantRole.Observer);
            //}

            var query = Context.Set<Tasks>().Where(spec).Join(Context.Set<TaskParticipant>().Where(specParticipant),
                t => t.Id, tp => tp.TaskId, (t, tp) => new { t, tp }).AsNoTracking();

            var totalRows = query.Count();
            var items = await query
                .Select(x => new TasksSearchViewModel()
                {
                    Id = x.t.Id,
                    Name = x.t.Name,
                    CreateTime = x.t.CreateTime,
                    CreatorId = x.t.CreatorId,
                    CreatorFullName = x.t.CreatorFullName,
                    CreatorAvatar = x.t.CreatorAvatar,
                    CreatorPositionId = x.t.CreatorPositionId,
                    CreatorPositionName = x.t.CreatorPositionName,
                    CreatorOfficeId = x.t.CreatorOfficeId,
                    CreatorOfficeName = x.t.CreatorOfficeName,
                    CreatorOfficeIdPath = x.t.CreatorOfficeIdPath,
                    ResponsibleId = x.t.ResponsibleId,
                    ResponsibleFullName = x.t.ResponsibleFullName,
                    ResponsibleAvatar = x.t.ResponsibleAvatar,
                    ResponsiblePositionId = x.t.ResponsiblePositionId,
                    ResponsiblePositionName = x.t.ResponsiblePositionName,
                    ResponsibleOfficeId = x.t.ResponsibleOfficeId,
                    ResponsibleOfficeName = x.t.ResponsibleOfficeName,
                    ResponsibleOfficeIdPath = x.t.ResponsibleOfficeIdPath,
                    Status = x.t.Status,
                    ChildCount = x.t.ChildCount,
                    EstimateStartDate = x.t.EstimateStartDate,
                    EstimateEndDate = x.t.EstimateEndDate,
                    EstimateCount = x.t.EstimateCount,
                    StartDate = x.t.StartDate,
                    EndDate = x.t.EndDate,
                    IsIdea = x.t.IsIdea,
                    IsAllowResponsibleUpdate = x.t.IsAllowResponsibleUpdate
                })
                .OrderBy(x => x.Id)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            return new SearchResult<TasksSearchViewModel>
            {
                Items = items,
                TotalRows = totalRows
            };
        }

        public async Task<SearchResult<TasksSearchViewModel>> SearchAllTask(string keyword, TasksStatus? status, string currentUserId, string userId, int? officeId,
            DateTime? estimateStartDate, DateTime? estimateEndDate, int page, int pageSize)
        {
            Expression<Func<Tasks, bool>> spec = x => !x.IsDelete && !x.ParentId.HasValue;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripChars().Trim();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (status.HasValue)
                spec = spec.And(x => x.Status == status);

            if (estimateStartDate.HasValue)
                spec = spec.And(x => x.EstimateStartDate.Date == estimateStartDate.Value.Date);

            if (estimateEndDate.HasValue)
                spec = spec.And(x => x.EstimateEndDate.Date == estimateEndDate.Value.Date);

            var query = Context.Set<Tasks>().Where(spec).GroupJoin(Context.Set<TaskParticipant>(), t => t.Id,
                    tp => tp.TaskId, (t, tp) => new { t, tp })
                .SelectMany(x => x.tp.DefaultIfEmpty(), (x, tp) => new { x.t, tp });

            if (!string.IsNullOrEmpty(userId))
                query = query.Where(x => x.t.ResponsibleId == userId || x.t.CreatorId == userId || x.tp.UserId == userId);

            if (officeId.HasValue)
                query = query.Where(x =>
                    x.t.ResponsibleOfficeId == officeId || x.t.CreatorOfficeId == officeId ||
                    x.tp.OfficeId == officeId);

            if (string.IsNullOrEmpty(userId) && !officeId.HasValue)
                query = query.Where(x =>
                    x.t.ResponsibleId == currentUserId || x.t.CreatorId == currentUserId ||
                    x.tp.UserId == currentUserId);

            var totalRows = query.Select(x => x.t.Id).Distinct().Count();
            var items = await query.Select(x => new TasksSearchViewModel
            {
                Id = x.t.Id,
                Name = x.t.Name,
                CreateTime = x.t.CreateTime,
                CreatorId = x.t.CreatorId,
                CreatorFullName = x.t.CreatorFullName,
                CreatorAvatar = x.t.CreatorAvatar,
                CreatorPositionId = x.t.CreatorPositionId,
                CreatorPositionName = x.t.CreatorPositionName,
                CreatorOfficeId = x.t.CreatorOfficeId,
                CreatorOfficeName = x.t.CreatorOfficeName,
                CreatorOfficeIdPath = x.t.CreatorOfficeIdPath,
                ResponsibleId = x.t.ResponsibleId,
                ResponsibleFullName = x.t.ResponsibleFullName,
                ResponsibleAvatar = x.t.ResponsibleAvatar,
                ResponsiblePositionId = x.t.ResponsiblePositionId,
                ResponsiblePositionName = x.t.ResponsiblePositionName,
                ResponsibleOfficeId = x.t.ResponsibleOfficeId,
                ResponsibleOfficeName = x.t.ResponsibleOfficeName,
                ResponsibleOfficeIdPath = x.t.ResponsibleOfficeIdPath,
                Status = x.t.Status,
                ChildCount = x.t.ChildCount,
                EstimateStartDate = x.t.EstimateStartDate,
                EstimateEndDate = x.t.EstimateEndDate,
                EstimateCount = x.t.EstimateCount,
                StartDate = x.t.StartDate,
                EndDate = x.t.EndDate,
                IsIdea = x.t.IsIdea,
                IsAllowResponsibleUpdate = x.t.IsAllowResponsibleUpdate
            }).Distinct().OrderByDescending(x => x.Id).Skip((page - 1) * pageSize).Take(pageSize).AsNoTracking().ToListAsync();
            return new SearchResult<TasksSearchViewModel>
            {
                Items = items,
                TotalRows = totalRows
            };
        }

        public async Task<SearchResult<TasksSearchViewModel>> SearchTaskOverdue(string keyword, TasksStatus? status, string currentUserId, string userId, int? officeId,
            DateTime? estimateStartDate, DateTime? estimateEndDate, int page, int pageSize)
        {
            Expression<Func<Tasks, bool>> spec = x => !x.IsDelete && !x.ParentId.HasValue
                && ((!x.EndDate.HasValue && x.EstimateEndDate < DateTime.Now) || (x.EndDate.HasValue && x.EstimateEndDate < x.EndDate));
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripChars().Trim();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (status.HasValue)
                spec = spec.And(x => x.Status == status);

            if (!string.IsNullOrEmpty(userId))
                spec = spec.And(x => x.ResponsibleId == userId);

            if (officeId.HasValue)
                spec = spec.And(x => x.ResponsibleOfficeId == officeId.Value);

            if (string.IsNullOrEmpty(userId) && !officeId.HasValue)
                spec = spec.And(x => x.ResponsibleId == currentUserId);

            if (estimateStartDate.HasValue)
                spec = spec.And(x => x.EstimateStartDate.Date == estimateStartDate.Value.Date);

            if (estimateEndDate.HasValue)
                spec = spec.And(x => x.EstimateEndDate.Date == estimateEndDate.Value.Date);

            var sort = Context.Filters.Sort<Tasks, long>(x => x.Id, true);
            var paging = Context.Filters.Page<Tasks>(page, pageSize);

            var totalRows = _taskRepository.Count(spec);
            var items = await _taskRepository.GetsAsAsync(x => new TasksSearchViewModel
            {
                Id = x.Id,
                Name = x.Name,
                CreateTime = x.CreateTime,
                CreatorId = x.CreatorId,
                CreatorFullName = x.CreatorFullName,
                CreatorAvatar = x.CreatorAvatar,
                CreatorPositionId = x.CreatorPositionId,
                CreatorPositionName = x.CreatorPositionName,
                CreatorOfficeId = x.CreatorOfficeId,
                CreatorOfficeName = x.CreatorOfficeName,
                CreatorOfficeIdPath = x.CreatorOfficeIdPath,
                ResponsibleId = x.ResponsibleId,
                ResponsibleFullName = x.ResponsibleFullName,
                ResponsibleAvatar = x.ResponsibleAvatar,
                ResponsiblePositionId = x.ResponsiblePositionId,
                ResponsiblePositionName = x.ResponsiblePositionName,
                ResponsibleOfficeId = x.ResponsibleOfficeId,
                ResponsibleOfficeName = x.ResponsibleOfficeName,
                ResponsibleOfficeIdPath = x.ResponsibleOfficeIdPath,
                Status = x.Status,
                ChildCount = x.ChildCount,
                EstimateStartDate = x.EstimateStartDate,
                EstimateEndDate = x.EstimateEndDate,
                EstimateCount = x.EstimateCount,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                IsIdea = x.IsIdea,
                IsAllowResponsibleUpdate = x.IsAllowResponsibleUpdate
            }, spec, sort, paging);
            return new SearchResult<TasksSearchViewModel>
            {
                Items = items,
                TotalRows = totalRows
            };
        }

        public async Task<int> UpdateIdPath(string tenantId, long taskId, string taskIdPath)
        {
            var taskInfo = await GetInfo(tenantId, taskId);
            if (taskInfo == null)
                return -1;

            taskInfo.IdPath = taskIdPath;
            return await Context.SaveChangesAsync();
        }

        public Task<List<TasksSearchViewModel>> Search(TaskFilter taskFilter, out int totalRows)
        {
            var query = from t in Context.Set<Tasks>()
                        join tg in Context.Set<Target>() on t.TargetId equals tg.Id
                        join tp in Context.Set<TaskParticipant>() on t.Id equals tp.TaskId
                        where
                            ((t.ResponsibleId == taskFilter.CurrentUserId && taskFilter.WorkingType == WorkingType.Personal)
                            || (t.CreatorId != taskFilter.CurrentUserId && t.ResponsibleId != taskFilter.CurrentUserId && taskFilter.WorkingType == WorkingType.Combination)
                            || (taskFilter.WorkingType == WorkingType.Both && (t.CreatorId != taskFilter.CurrentUserId
                            || (t.CreatorId == t.ResponsibleId && t.CreatorId == taskFilter.CurrentUserId)))
                            || (taskFilter.WorkingType == WorkingType.Create && t.CreatorId == taskFilter.CurrentUserId))
                            && tp.UserId == taskFilter.CurrentUserId
                            && !t.IsDelete && !tg.IsDelete
                              && t.TenantId == taskFilter.TenantId && tg.TenantId == taskFilter.TenantId && tp.TenantId == taskFilter.TenantId
                              && (!taskFilter.StartDate.HasValue || t.EstimateStartDate >= taskFilter.StartDate.Value.Date)
                              && (!taskFilter.EndDate.HasValue || t.EstimateStartDate <= taskFilter.EndDate.Value.Date.AddDays(1).AddMilliseconds(-1))
                              && t.Type == taskFilter.Type
                              && (!taskFilter.Status.HasValue || t.Status == taskFilter.Status)
                              && (!taskFilter.TargetId.HasValue || t.TargetId == taskFilter.TargetId)
                        group t by new
                        {
                            t.Id,
                            TaskName = t.Name,
                            t.ParentId,
                            t.Status,
                            t.StartDate,
                            t.EndDate,
                            t.EstimateStartDate,
                            t.EstimateEndDate,
                            t.PercentCompleted,
                            t.ChecklistCount,
                            t.CompletedChecklistCount,
                            t.CommentCount,
                            t.ResponsibleId,
                            t.ResponsibleFullName,
                            t.TargetId,
                            t.ConcurrencyStamp,
                            t.Type,
                            TargetName = tg.Name
                        } into g
                        select new TasksSearchViewModel
                        {
                            Id = g.Key.Id,
                            Name = g.Key.TaskName,
                            ParentId = g.Key.ParentId,
                            Status = g.Key.Status,
                            StartDate = g.Key.StartDate,
                            EndDate = g.Key.EndDate,
                            EstimateEndDate = g.Key.EstimateEndDate,
                            EstimateStartDate = g.Key.EstimateStartDate,
                            PercentCompleted = g.Key.PercentCompleted,
                            ChecklistCount = g.Key.ChecklistCount,
                            CompletedChecklistCount = g.Key.CompletedChecklistCount,
                            CommentCount = g.Key.CommentCount,
                            ResponsibleId = g.Key.ResponsibleId,
                            ResponsibleFullName = g.Key.ResponsibleFullName,
                            TargetId = g.Key.TargetId,
                            TargetName = g.Key.TargetName,
                            ConcurrencyStamp = g.Key.ConcurrencyStamp,
                            OverdueDate = g.Key.Status == TasksStatus.NotStartYet || g.Key.Status == TasksStatus.InProgress || g.Key.Status == TasksStatus.DeclineFinish ?
                            DateTime.Now.Subtract(g.Key.EstimateEndDate).TotalMinutes
                            : (g.Key.Status == TasksStatus.PendingToFinish || g.Key.Status == TasksStatus.Completed) && g.Key.EndDate.HasValue ? g.Key.EndDate.Value.Subtract(g.Key.EstimateEndDate).TotalMinutes : 0,
                            Type = g.Key.Type
                        };

            totalRows = query.Count();
            return query.AsNoTracking()
                .Skip((taskFilter.Page - 1) * taskFilter.PageSize)
                .Take(taskFilter.PageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public Task<List<TasksSearchViewModel>> GetTaskChilByTaskId(string tenantId, string idPath)
        {
            var query = from t in Context.Set<Tasks>()
                        join tg in Context.Set<Target>() on t.TargetId equals tg.Id
                        where t.IdPath.StartsWith(idPath + '.')
                              && !t.IsDelete && !tg.IsDelete
                              && t.TenantId == tenantId && tg.TenantId == tenantId
                        select new TasksSearchViewModel
                        {
                            Id = t.Id,
                            ParentId = t.ParentId,
                            Name = t.Name,
                            Status = t.Status,
                            StartDate = t.StartDate,
                            EndDate = t.EndDate,
                            EstimateEndDate = t.EstimateEndDate,
                            EstimateStartDate = t.EstimateStartDate,
                            PercentCompleted = t.PercentCompleted,
                            ChecklistCount = t.ChecklistCount,
                            CompletedChecklistCount = t.CompletedChecklistCount,
                            CommentCount = t.CommentCount,
                            ResponsibleId = t.ResponsibleId,
                            ResponsibleFullName = t.ResponsibleFullName,
                            TargetId = t.TargetId,
                            TargetName = tg.Name,
                            ChildCount = t.ChildCount,
                            ConcurrencyStamp = t.ConcurrencyStamp,
                            OverdueDate = t.Status == TasksStatus.NotStartYet || t.Status == TasksStatus.InProgress || t.Status == TasksStatus.DeclineFinish ?
                            (int)DateTime.Now.Subtract(t.EstimateEndDate).TotalMinutes
                             : (t.Status == TasksStatus.PendingToFinish || t.Status == TasksStatus.Completed) && t.EndDate.HasValue ? (int)t.EndDate.Value.Subtract(t.EstimateEndDate).TotalMinutes : 0,
                            Type = t.Type
                        };

            return query.AsNoTracking().ToListAsync();
        }

        public async Task<TaskDetailViewModel> GetDetail(string tenantId, long taskId)
        {
            var query = from t in Context.Set<Tasks>()
                        where t.TenantId == tenantId && t.Id == taskId
                        select new TaskDetailViewModel
                        {
                            Id = t.Id,
                            Name = t.Name,
                            IdPath = t.IdPath,
                            ParentId = t.ParentId,
                            Status = t.Status,
                            Type = t.Type,
                            CreatorFullName = t.CreatorFullName,
                            CreatorPositionName = t.CreatorPositionName,
                            CreatorId = t.CreatorId,
                            CreatorOfficeName = t.CreatorOfficeName,
                            CreatorAvatar = t.CreatorAvatar,
                            CreateTime = t.CreateTime,
                            Description = t.Description,
                            ResponsibleId = t.ResponsibleId,
                            ReponsibleOfficeIdPath = t.ResponsibleOfficeIdPath,
                            ResponsiblePositionName = t.ResponsiblePositionName,
                            EstimateEndDate = t.EstimateEndDate,
                            EstimateStartDate = t.EstimateStartDate,
                            StartDate = t.StartDate,
                            EndDate = t.EndDate,
                            ResponsibleAvatar = t.ResponsibleAvatar,
                            TargetId = t.TargetId,
                            ResponsibleFullName = t.ResponsibleFullName,
                            MethodCalculatorResult = t.MethodCalculateResult,
                            ConcurrencyStamp = t.ConcurrencyStamp,
                            PercentCompleted = t.PercentCompleted,
                            OverdueDate = t.Status == TasksStatus.NotStartYet || t.Status == TasksStatus.InProgress || t.Status == TasksStatus.DeclineFinish ?
                            (int)DateTime.Now.Subtract(t.EstimateEndDate).TotalMinutes
                             : (t.Status == TasksStatus.PendingToFinish || t.Status == TasksStatus.Completed) && t.EndDate.HasValue
                             ? (int)t.EndDate.Value.Subtract(t.EstimateEndDate).TotalMinutes : 0,
                            ChildCount = t.ChildCount
                        };

            return await query.FirstOrDefaultAsync();
        }

        public Task<bool> CheckInForByName(string tenantId, string name)
        {
            return _taskRepository.ExistAsync(x => x.TenantId == tenantId && x.Name == name);
        }

        public async Task<bool> CheckExist(string tenantId, long id)
        {
            return await _taskRepository.ExistAsync(x => x.TenantId == tenantId && x.Id == id);
        }

        public async Task<int> GetTotalTaskByTargetId(string tenantId, long targetId)
        {
            return await _taskRepository.CountAsync(x => x.TenantId == tenantId && !x.IsDelete && x.TargetId == targetId
            && !x.ParentId.HasValue);
        }

        public async Task<int> GetTotalTaskCompleteByTargetId(string tenantId, long targetId)
        {
            return await _taskRepository.CountAsync(x => x.TenantId == tenantId && !x.IsDelete && x.TargetId == targetId
            && x.Status == TasksStatus.Completed && !x.ParentId.HasValue);
        }

        public async Task<int> UpdateTotalComment(string tenantId, long id, int totalComment)
        {
            var info = await GetInfo(tenantId, id);
            if (info == null)
                return -1;

            info.CommentCount = totalComment;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeleteTaskChild(string tenantId, long id)
        {
            var listTaskChild = await _taskRepository.GetsAsync(false, x => x.TenantId == tenantId && x.ParentId == id && !x.IsDelete);
            if (listTaskChild == null || listTaskChild.Any())
                return -1;

            foreach (var task in listTaskChild)
            {
                task.IsDelete = true;
            }

            return await Context.SaveChangesAsync();
        }

        public async Task<List<Tasks>> GetAllTaskParent(string tenantId, long? parentId, string idPath, bool isReadOnly = false)
        {
            return await _taskRepository.GetsAsync(isReadOnly, x => x.TenantId == tenantId
            && (idPath.StartsWith(x.IdPath + '.') || x.ParentId == parentId) && !x.IsDelete);
        }

        public async Task<List<Tasks>> GetTaskChildren(string tenantId, long? parentId, bool isReadOnly = false)
        {
            return await _taskRepository.GetsAsync(isReadOnly, x => x.TenantId == tenantId && x.ParentId == parentId && !x.IsDelete);
        }

        // Tổng hợp dữ liêu lên mục tiêu cha
        public async Task<int> GeneralPercentCompletedOfTask(string tenantId, long? parentId, string idPath)
        {
            var listAllTaskParent = await GetAllTaskParent(tenantId, parentId, idPath);
            if (listAllTaskParent == null || !listAllTaskParent.Any())
                return -1;

            return await UpdatePercentCompleteRecursive(tenantId, listAllTaskParent, parentId);
        }

        public async Task<int> UpdatePercentCompleteRecursive(string tenantId, List<Tasks> listTask, long? parentId)
        {
            var parentTaskInfo = listTask.FirstOrDefault(x => x.Id == parentId);
            if (parentTaskInfo == null || parentTaskInfo.MethodCalculateResult == TaskMethodCalculateResult.Manually
                || parentTaskInfo.MethodCalculateResult == TaskMethodCalculateResult.CheckList)
                return -2;

            var listChild = await GetTaskChildren(tenantId, parentId, true);
            if (listChild != null && listChild.Any())
            {
                var totalWeight = listChild.Count;
                var totalPercentComplete = listChild.Count(x => !x.IsDelete && x.Status == TasksStatus.Completed);

                parentTaskInfo.PercentCompleted = totalWeight > 0 ? Math.Round((decimal)(totalPercentComplete / totalWeight), 2) : 0;
                await Context.SaveChangesAsync();
                await UpdatePercentCompleteRecursive(tenantId, listTask, parentTaskInfo.ParentId);
            }

            return 1;
        }

        public async Task<List<ReportTaskViewModel>> SearchForReport(string tenantId, TaskReportFilter taskFilterReport)
        {
            Expression<Func<Tasks, bool>> spec = x => x.TenantId == tenantId;
            if (taskFilterReport.TypeSearchTime == TypeSearchTime.StarteDateToEndDate)
            {
                if (taskFilterReport.StartDate.HasValue)
                {
                    spec = spec.And(x => x.EstimateStartDate >= taskFilterReport.StartDate.Value.Date);
                }

                if (taskFilterReport.EndDate.HasValue)
                {
                    spec = spec.And(x => x.EstimateEndDate <= taskFilterReport.EndDate.Value.Date.AddDays(1).AddMilliseconds(-1));
                }
            }

            if (taskFilterReport.TypeSearchTime == TypeSearchTime.StartDate)
            {
                if (taskFilterReport.StartDate.HasValue)
                {
                    spec = spec.And(x => x.EstimateStartDate >= taskFilterReport.StartDate.Value.Date);
                }

                if (taskFilterReport.EndDate.HasValue)
                {
                    spec = spec.And(x => x.EstimateStartDate <= taskFilterReport.EndDate.Value.Date.AddDays(1).AddMilliseconds(-1));
                }
            }

            if (taskFilterReport.TypeSearchTime == TypeSearchTime.EndDate)
            {
                if (taskFilterReport.StartDate.HasValue)
                {
                    spec = spec.And(x => x.EstimateEndDate >= taskFilterReport.StartDate.Value.Date);
                }

                if (taskFilterReport.EndDate.HasValue)
                {
                    spec = spec.And(x => x.EstimateEndDate <= taskFilterReport.EndDate.Value.Date.AddDays(1).AddMilliseconds(-1));
                }
            }

            if (taskFilterReport.TypeSearchTime == TypeSearchTime.CreateDate)
            {
                if (taskFilterReport.EndDate.HasValue)
                {
                    spec = spec.And(x => x.CreateTime >= taskFilterReport.StartDate.Value.Date);
                }

                if (taskFilterReport.EndDate.HasValue)
                {
                    spec = spec.And(x => x.CreateTime <= taskFilterReport.EndDate.Value.Date.AddDays(1).AddMilliseconds(-1));
                }
            }

            else if (taskFilterReport.TypeSearchTime == TypeSearchTime.CompleteDate)
            {
                if (taskFilterReport.EndDate.HasValue)
                {
                    spec = spec.And(x => x.EndDate >= taskFilterReport.StartDate.Value.Date);
                }

                if (taskFilterReport.EndDate.HasValue)
                {
                    spec = spec.And(x => x.EndDate <= taskFilterReport.EndDate.Value.Date.AddDays(1).AddMilliseconds(-1));
                }
            }

            if (taskFilterReport.OfficeId.HasValue)
                spec = spec.And(x => x.ResponsibleOfficeId == taskFilterReport.OfficeId);

            if (taskFilterReport.TaskType.HasValue)
                spec = spec.And(x => x.Type == taskFilterReport.TaskType);

            if (taskFilterReport.TaskReportType == TaskReportType.statisticsByUnits)
            {
                var query = from t in Context.Set<Tasks>().Where(spec)
                            join tg in Context.Set<Target>().Where(x => !x.IsDelete && x.TenantId == tenantId) on t.TargetId equals tg.Id
                            join targetTable in Context.Set<TargetTable>().Where(x => !x.IsDelete && x.TenantId == tenantId
                           && x.Status == TargetTableStatus.Using && (string.IsNullOrEmpty(taskFilterReport.TableTargetId) || x.Id == taskFilterReport.TableTargetId))
                            on tg.TargetTableId equals targetTable.Id
                            group new { t } by new { t.ResponsibleOfficeId, t.ResponsibleOfficeName } into g
                            select new ReportTaskViewModel
                            {
                                OfficeId = g.Key.ResponsibleOfficeId,
                                OfficeName = g.Key.ResponsibleOfficeName,
                                TotalTask = g.Count(x => !x.t.IsDelete),
                                TotalDestroy = g.Count(x => x.t.IsDelete),
                                ProgressNotComplete = g.Count(x => !x.t.IsDelete && (x.t.Status == TasksStatus.InProgress || x.t.Status == TasksStatus.NotStartYet
                                 || x.t.Status == TasksStatus.DeclineFinish) && x.t.EstimateEndDate >= DateTime.Now),
                                OverDueNotComplete = g.Count(x => !x.t.IsDelete && (x.t.Status == TasksStatus.InProgress || x.t.Status == TasksStatus.NotStartYet
                                 || x.t.Status == TasksStatus.DeclineFinish) && x.t.EstimateEndDate < DateTime.Now),
                                ProgressComplete = g.Count(x => !x.t.IsDelete && (x.t.Status == TasksStatus.Completed ||
                                x.t.Status == TasksStatus.PendingToFinish) && x.t.EstimateEndDate >= x.t.EndDate),
                                OverDueComplete = g.Count(x => !x.t.IsDelete && (x.t.Status == TasksStatus.Completed
                                || x.t.Status == TasksStatus.PendingToFinish) && x.t.EstimateEndDate < x.t.EndDate),
                            };
                return await query.ToListAsync();
            }

            if (taskFilterReport.TaskReportType == TaskReportType.statisticsByPersonal)
            {
                if (!string.IsNullOrEmpty(taskFilterReport.UserId))
                    spec = spec.And(x => x.ResponsibleId == taskFilterReport.UserId);

                var query = from t in Context.Set<Tasks>().Where(spec)
                            join tg in Context.Set<Target>().Where(x => !x.IsDelete && x.TenantId == tenantId) on t.TargetId equals tg.Id
                            join targetTable in Context.Set<TargetTable>().Where(x => !x.IsDelete && x.TenantId == tenantId
                            && x.Status == TargetTableStatus.Using && (string.IsNullOrEmpty(taskFilterReport.TableTargetId) || x.Id == taskFilterReport.TableTargetId))
                            on tg.TargetTableId equals targetTable.Id
                            group new { t } by new { t.ResponsibleId, t.ResponsibleFullName, t.ResponsibleOfficeId, t.ResponsibleOfficeName } into g
                            select new ReportTaskViewModel
                            {
                                UserId = g.Key.ResponsibleId,
                                FullName = g.Key.ResponsibleFullName,
                                OfficeId = g.Key.ResponsibleOfficeId,
                                OfficeName = g.Key.ResponsibleOfficeName,
                                TotalTask = g.Count(x => !x.t.IsDelete),
                                TotalDestroy = g.Count(x => x.t.IsDelete),
                                ProgressNotComplete = g.Count(x => !x.t.IsDelete && (x.t.Status == TasksStatus.InProgress || x.t.Status == TasksStatus.NotStartYet || x.t.Status == TasksStatus.DeclineFinish) && x.t.EstimateEndDate >= DateTime.Now),
                                OverDueNotComplete = g.Count(x => !x.t.IsDelete && (x.t.Status == TasksStatus.InProgress || x.t.Status == TasksStatus.NotStartYet || x.t.Status == TasksStatus.DeclineFinish) && x.t.EstimateEndDate < DateTime.Now),
                                ProgressComplete = g.Count(x => !x.t.IsDelete && (x.t.Status == TasksStatus.Completed || x.t.Status == TasksStatus.PendingToFinish) && x.t.EstimateEndDate >= x.t.EndDate),
                                OverDueComplete = g.Count(x => !x.t.IsDelete && (x.t.Status == TasksStatus.Completed || x.t.Status == TasksStatus.PendingToFinish) && x.t.EstimateEndDate < x.t.EndDate),
                            };
                return await query.AsNoTracking().ToListAsync();
            }

            return null;
        }

        public Task<List<TaskViewModel>> SearchReportDetail(string tenantId, TaskReportFilter taskFilterReport, int page, int pageSize, out int totalRows)
        {
            Expression<Func<Tasks, bool>> spec = x => x.TenantId == tenantId;
            if (taskFilterReport.TypeSearchTime == TypeSearchTime.StarteDateToEndDate)
            {
                if (taskFilterReport.StartDate.HasValue)
                {
                    spec = spec.And(x => x.EstimateStartDate >= taskFilterReport.StartDate.Value.Date);
                }

                if (taskFilterReport.EndDate.HasValue)
                {
                    spec = spec.And(x => x.EstimateEndDate <= taskFilterReport.EndDate.Value.Date.AddDays(1).AddMilliseconds(-1));
                }
            }

            if (taskFilterReport.TypeSearchTime == TypeSearchTime.StartDate)
            {
                if (taskFilterReport.StartDate.HasValue)
                {
                    spec = spec.And(x => x.EstimateStartDate >= taskFilterReport.StartDate.Value.Date);
                }

                if (taskFilterReport.EndDate.HasValue)
                {
                    spec = spec.And(x => x.EstimateStartDate <= taskFilterReport.EndDate.Value.Date.AddDays(1).AddMilliseconds(-1));
                }
            }

            if (taskFilterReport.TypeSearchTime == TypeSearchTime.EndDate)
            {
                if (taskFilterReport.StartDate.HasValue)
                {
                    spec = spec.And(x => x.EstimateEndDate >= taskFilterReport.StartDate.Value.Date);
                }

                if (taskFilterReport.EndDate.HasValue)
                {
                    spec = spec.And(x => x.EstimateEndDate <= taskFilterReport.EndDate.Value.Date.AddDays(1).AddMilliseconds(-1));
                }
            }

            if (taskFilterReport.TypeSearchTime == TypeSearchTime.CreateDate)
            {
                if (taskFilterReport.EndDate.HasValue)
                {
                    spec = spec.And(x => x.CreateTime >= taskFilterReport.StartDate.Value.Date);
                }

                if (taskFilterReport.EndDate.HasValue)
                {
                    spec = spec.And(x => x.CreateTime <= taskFilterReport.EndDate.Value.Date.AddDays(1).AddMilliseconds(-1));
                }
            }

            else if (taskFilterReport.TypeSearchTime == TypeSearchTime.CompleteDate)
            {
                if (taskFilterReport.EndDate.HasValue)
                {
                    spec = spec.And(x => x.EndDate >= taskFilterReport.StartDate.Value.Date);
                }

                if (taskFilterReport.EndDate.HasValue)
                {
                    spec = spec.And(x => x.EndDate <= taskFilterReport.EndDate.Value.Date.AddDays(1).AddMilliseconds(-1));
                }
            }

            if (taskFilterReport.OfficeId.HasValue)
                spec = spec.And(x => x.ResponsibleOfficeId == taskFilterReport.OfficeId);

            if (taskFilterReport.TaskType.HasValue)
                spec = spec.And(x => x.Type == taskFilterReport.TaskType);

            if (!string.IsNullOrEmpty(taskFilterReport.UserId))
                spec = spec.And(x => x.ResponsibleId == taskFilterReport.UserId);

            var query = from t in Context.Set<Tasks>().Where(spec)
                        join tg in Context.Set<Target>().Where(x => !x.IsDelete && x.TenantId == tenantId) on t.TargetId equals tg.Id
                        join targetTable in Context.Set<TargetTable>().Where(x => !x.IsDelete && x.TenantId == tenantId
                       && x.Status == TargetTableStatus.Using && (string.IsNullOrEmpty(taskFilterReport.TableTargetId) || x.Id == taskFilterReport.TableTargetId))
                        on tg.TargetTableId equals targetTable.Id
                        select new TaskViewModel
                        {
                            Id = t.Id,
                            ParentId = t.ParentId,
                            Name = t.Name,
                            Status = t.Status,
                            StartDate = t.StartDate,
                            IdPath = t.IdPath,
                            EndDate = t.EndDate,
                            CreateTime = t.CreateTime,
                            EstimateEndDate = t.EstimateEndDate,
                            EstimateStartDate = t.EstimateStartDate,
                            PercentCompleted = t.PercentCompleted,
                            ChecklistCount = t.ChecklistCount,
                            CompletedChecklistCount = t.CompletedChecklistCount,
                            CommentCount = t.CommentCount,
                            ResponsibleId = t.ResponsibleId,
                            ResponsibleFullName = t.ResponsibleFullName,
                            ResponsibleOfficeName = t.ResponsibleOfficeName,
                            TargetName = tg.Name,
                            ChildCount = t.ChildCount,
                            CreatorFullName = t.CreatorFullName,
                            Type = t.Type,
                            Description = t.Description,
                        };

            totalRows = query.Count();
            return query.OrderBy(x => x.IdPath).ThenBy(x => x.CreateTime).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
        }
    }
}

