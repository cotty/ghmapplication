﻿using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GHM.Tasks.Infrastructure.Repository
{
    using Tasks = Domain.Models.Tasks;
    public class TasksCheckListsRepository : RepositoryBase, ITasksCheckListsRepository
    {
        private readonly IRepository<CheckList> _checkListRepository;
        public TasksCheckListsRepository(IDbContext context) : base(context)
        {
            _checkListRepository = Context.GetRepository<CheckList>();
        }

        public async Task<CheckList> GetInfo(string tenantId, long taskId, string id, bool isReadOnly = false)
        {
            return await _checkListRepository.GetAsync(isReadOnly,
                x => x.TenantId == tenantId && x.TaskId == taskId && x.Id == id);
        }

        public Task<bool> CheckExistsByName(string tenantId, long taskId, TaskType type, string name)
        {
            return _checkListRepository.ExistAsync(x => x.TenantId == tenantId && x.Name == name
            && !x.IsDelete && x.TaskId == taskId && x.Type == type);
        }

        public async Task<int> Delete(string tenantId, long taskId, string checklistId)
        {
            var info = await GetInfo(tenantId, taskId, checklistId);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateCompleteStatus(string tenantId, long taskId, string checklistId, bool isComplete)
        {
            var info = await GetInfo(tenantId, taskId, checklistId);
            if (info == null)
                return -1;

            info.Status = isComplete;
            return await Context.SaveChangesAsync();
        }

        public async Task<decimal> GetCompletedPercent(string tenantId, long taskId, TaskType type)
        {
            var totalChecklist =
                await _checkListRepository.CountAsync(x => x.TenantId == tenantId && x.TaskId == taskId && !x.IsDelete && x.Type == type);
            var totalCompleted = await _checkListRepository.CountAsync(x => x.TenantId == tenantId && x.Type == type
                                                                            && x.TaskId == taskId && x.Status &&
                                                                            !x.IsDelete);
            var percent = (decimal)totalCompleted / totalChecklist * 100;
            return percent;
        }

        public async Task<int> ForceDelete(string tenantId, string id)
        {
            var info = await GetInfo(tenantId, id);
            if (info == null)
                return -1;

            info.IsDelete = true;
            return await Context.SaveChangesAsync();
        }

        public async Task<CheckList> GetInfo(string tenantId, string id, bool isReadOnly = false)
        {
            return await _checkListRepository.GetAsync(isReadOnly, x => x.TenantId == tenantId && x.Id == id && !x.IsDelete);
        }

        public async Task<int> Update(CheckList checkList)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> InsertAsync(CheckList checkList)
        {
            _checkListRepository.Create(checkList);
            return await Context.SaveChangesAsync();
        }

        public Task<List<CheckListsViewModel>> SearchByTasksTypeId(string tenantId, long taskId, TaskType type)
        {
            Expression<Func<CheckList, bool>> spec = x => x.TenantId == tenantId && x.Type == type && x.TaskId == taskId && !x.IsDelete;

            if (type == TaskType.TaskLibrary)
            {
                var result = from checkList in Context.Set<CheckList>().Where(spec).AsNoTracking()
                             join taskLibrary in Context.Set<TaskLibrary>().Where(x => !x.IsDelete && x.TenantId == tenantId).AsNoTracking()
                                  on checkList.TaskId equals taskLibrary.Id
                             select new CheckListsViewModel
                             {
                                 Id = checkList.Id,
                                 Name = checkList.Name,
                                 ConcurrencyStamp = checkList.ConcurrencyStamp,
                                 IsComplete = checkList.Status,
                                 TaskId = taskLibrary.Id,
                                 TaskName = taskLibrary.Name,
                                 Type = checkList.Type
                             };

                return result.OrderBy(x => x.Name).ToListAsync();
            }
            else
            {
                var result = from checkList in Context.Set<CheckList>().Where(spec).AsNoTracking()
                             join task in Context.Set<Tasks>().Where(x => !x.IsDelete && x.TenantId == tenantId).AsNoTracking()
                                  on checkList.TaskId equals task.Id
                             select new CheckListsViewModel
                             {
                                 Id = checkList.Id,
                                 Name = checkList.Name,
                                 ConcurrencyStamp = checkList.ConcurrencyStamp,
                                 IsComplete = checkList.Status,
                                 TaskId = task.Id,
                                 TaskName = task.Name,
                                 Type = checkList.Type
                             };

                return result.OrderBy(x => x.Name).ToListAsync();
            }
        }

        public async Task<int> UpdateAsync(string tenantId, string id, CheckList checkList)
        {
            return await Context.SaveChangesAsync();
        }

        public async Task<int> GetTotalCheckListByTask(string tenantId, long taskId, TaskType type, bool isCountComplete)
        {
            Expression<Func<CheckList, bool>> spec = x => x.TenantId == tenantId && !x.IsDelete && x.TaskId == taskId && x.Type == type;
            if (isCountComplete)
                spec = spec.And(x => x.Status == true);

            return await _checkListRepository.CountAsync(spec);
        }
    }
}
