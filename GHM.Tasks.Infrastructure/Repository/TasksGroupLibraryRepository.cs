﻿using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GHM.Tasks.Infrastructure.Repository
{
    public class TasksGroupLibraryRepository : RepositoryBase, ITasksGroupLibraryRepository
    {
        private readonly IRepository<TaskGroupLibrary> _taskGroupLibrary;
        public TasksGroupLibraryRepository(IDbContext context) : base(context)
        {
            _taskGroupLibrary = Context.GetRepository<TaskGroupLibrary>();
        }

        public async Task<bool> CheckExistsByName(string tenantId, string name)
        {
            Expression<Func<TaskGroupLibrary, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId && x.Name == name;
            return await _taskGroupLibrary.ExistAsync(spec);
        }

        public async Task<int> ForceDelete(string tenantId, string userId, string userFullName, string id)
        {
            var info = await GetInfo(tenantId, id);
            info.IsDelete = true;
            info.LastUpdateFullName = userFullName;
            info.LastUpdateUserId = userId;

            return await Context.SaveChangesAsync();
        }

        public Task<List<TaskGroupLibraryViewModel>> GetAllTasksLibrary(string tenantId)
        {
            Expression<Func<TaskGroupLibrary, bool>> spec = x => !x.IsDelete && x.TenantId == tenantId;

            var query = from tgl in Context.Set<TaskGroupLibrary>().Where(spec)
                        join tl in Context.Set<TaskLibrary>().Where(x => !x.IsDelete && x.TenantId == tenantId)
                        on tgl.Id equals tl.TaskGroupLibraryId into rl1
                        from b in rl1.DefaultIfEmpty()
                        group new { tgl, b}
                        by new { tgl.Id, tgl.Name, tgl.Order } into g
                        select new TaskGroupLibraryViewModel
                        {
                            Id = g.Key.Id,
                            Name = g.Key.Name,
                            Order = g.Key.Order,
                            TasksLibraries = g.Count(x => x.b != null) > 0 ? g.Select(x => new TaskLibraryViewModel
                            {
                                Id = x.b.Id,
                                Name = x.b.Name,
                                Description = x.b.Description,
                                MethodCalculateResult = x.b.MethodCalculateResult,
                                TaskGroupLibraryId = x.b.TaskGroupLibraryId,
                                TaskGroupLibraryName = g.Key.Name,
                                ConcurrencyStamp = x.b.ConcurrencyStamp
                            }).OrderBy(x => x.Id).ToList() : null
                        };

            return query.AsNoTracking().OrderByDescending(x => x.Order).ToListAsync();
        }

        public async Task<TaskGroupLibrary> GetInfo(string tenantId, string id, bool isReadOnly = false)
        {
            return await _taskGroupLibrary.GetAsync(isReadOnly, x => !x.IsDelete && x.TenantId == tenantId && x.Id == id);
        }

        public async Task<int> Insert(TaskGroupLibrary taskGroupLibrary)
        {
            _taskGroupLibrary.Create(taskGroupLibrary);
            return await Context.SaveChangesAsync();
        }

        public Task<List<TaskGroupLibrary>> Search(string tenantId, string keyword, int page, int pageSize)
        {
            throw new NotImplementedException();
        }

        public async Task<int> Update(TaskGroupLibrary taskGroupLibrary)
        {
            return await Context.SaveChangesAsync();
        }       
    }
}
