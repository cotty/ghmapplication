﻿using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Tasks.Infrastructure.Repository
{
    public class TaskGroupShareRepository : RepositoryBase, ITaskGroupShareRepository
    {
        private readonly IRepository<TaskGroupShare> _taskGroupShareRepository;
        public TaskGroupShareRepository(IDbContext context) : base(context)
        {
            _taskGroupShareRepository = Context.GetRepository<TaskGroupShare>();
        }

        public async Task<bool> CheckExistsUserId(string tenantId, string groupId, string userId)
        {
            return await _taskGroupShareRepository.ExistAsync(x => x.TenantId == tenantId && x.GroupId == groupId && x.UserId == userId);
        }

        public async Task<int> Delete(string tenantId, string taskGroupShareId)
        {
            var taskGroupShareInfo = await GetInfo(tenantId, taskGroupShareId);
            if (taskGroupShareInfo == null)
                return -1;

            _taskGroupShareRepository.Delete(taskGroupShareInfo);
            return await Context.SaveChangesAsync();
        }

        public async Task<TaskGroupShare> GetInfo(string tenantId, string taskGroupShareId, bool isReadOnly = false)
        {
            return await _taskGroupShareRepository.GetAsync(isReadOnly, x => x.TenantId == tenantId && x.Id == taskGroupShareId);
        }

        public async Task<int> Insert(TaskGroupShare taskGroupShare)
        {
            _taskGroupShareRepository.Create(taskGroupShare);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Update(TaskGroupShare taskGroupShare)
        {
            return await Context.SaveChangesAsync();
        }
    }
}
