﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GHM.Infrastructure.SqlServer;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace GHM.Tasks.Infrastructure.Repository
{
    public class TaskRoleGroupRepository : RepositoryBase, ITaskRoleGroupRepository
    {
        private readonly IRepository<TaskRoleGroup> _taskRoleGroupRepository;
        public TaskRoleGroupRepository(IDbContext context) : base(context)
        {
            _taskRoleGroupRepository = Context.GetRepository<TaskRoleGroup>();
        }

        public async Task<bool> CheckExist(string tenantId, string taskParticipantsId, string roleGroupId)
        {
            return await _taskRoleGroupRepository.ExistAsync(x => x.TenantId == tenantId && x.TaskParticipantId == taskParticipantsId && x.RoleGroupId == roleGroupId);
        }

        public async Task<int> DeleteAllById(string participantId, string tenantId)
        {
            var listUser = await _taskRoleGroupRepository.GetsAsync(false, x => x.TenantId == tenantId && x.TaskParticipantId == participantId);
            if (listUser == null)
                return 1;

            _taskRoleGroupRepository.Deletes(listUser);
            return await Context.SaveChangesAsync();

        }

        public async Task<int> Deletes(List<TaskRoleGroup> listRoleGroup)
        {
            _taskRoleGroupRepository.Deletes(listRoleGroup);
            return await Context.SaveChangesAsync();
        }

        public async Task<List<TaskRoleGroup>> GetAllByParticipantId(string tenantId, string id)
        {
            return await _taskRoleGroupRepository.GetsAsync(false, x => x.TenantId == tenantId && x.TaskParticipantId == id);
        }

        public async Task<List<TaskRoleGroupViewModel>> GetDetail(string tenantId, long taskId, string userId)
        {
            var query = from tp in Context.Set<TaskParticipant>()
                        join trg in Context.Set<TaskRoleGroup>() on tp.Id equals trg.TaskParticipantId
                        join rg in Context.Set<RoleGroup>() on new { trg.RoleGroupId, trg.TenantId } equals new { RoleGroupId = rg.Id, rg.TenantId }
                        where tp.TaskId == taskId && tp.TenantId == tenantId
                                                  && trg.TenantId == tenantId && rg.TenantId == tenantId && tp.UserId == userId
                        select new TaskRoleGroupViewModel
                        {
                            UserId = tp.UserId,
                            Role = rg.Role,
                            ParticipantId = tp.Id
                        };

            return await query.ToListAsync();
        }

        public async Task<List<TaskRoleParticipantsViewModel>> GetDetail(string tenantId, long taskId)
        {
            var query = from tp in Context.Set<TaskParticipant>()
                        join trg in Context.Set<TaskRoleGroup>() on tp.Id equals trg.TaskParticipantId
                        join rg in Context.Set<RoleGroup>() on new { trg.RoleGroupId, trg.TenantId } equals new { RoleGroupId = rg.Id, rg.TenantId }
                        where tp.TaskId == taskId && tp.TenantId == tenantId && trg.TenantId == tenantId && rg.TenantId == tenantId
                        group new { trg, rg } by new { tp.Id, tp.UserId } into g
                        select new TaskRoleParticipantsViewModel
                        {
                            Id = g.Key.Id,
                            UserId = g.Key.UserId,
                            RoleGroupIds = g.Select(x => new RoleGroupViewModel()
                            {
                                Id = x.trg.RoleGroupId,
                                Role = x.rg.Role,
                                Name = x.rg.Name
                            }).ToList()
                        };
            return await query.ToListAsync();
        }

        public async Task<int> Insert(List<TaskRoleGroup> taskRoleGroups)
        {
            _taskRoleGroupRepository.Creates(taskRoleGroups);
            return await Context.SaveChangesAsync();
        }

        public async Task<int> Insert(string id, string roleGroupId, string tenantId)
        {
            var taskRoleGroup = new TaskRoleGroup
            {
                TaskParticipantId = id,
                RoleGroupId = roleGroupId,
                TenantId = tenantId
            };
            _taskRoleGroupRepository.Create(taskRoleGroup);
            return await Context.SaveChangesAsync();
        }
    }
}
