﻿using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.Services;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.Resources;
using GHM.Tasks.Domain.ViewModels;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using State = GHM.Tasks.Domain.ViewModels.State;

namespace GHM.Tasks.Infrastructure.Services
{
    public class TargetService : ITargetService
    {
        private readonly ITargetRepository _targetRepository;
        private readonly ITargetGroupRepository _targetGroupRepository;
        private readonly ITargetParticipantRepository _targetParticipantRepository;
        private readonly ITargetTableRepository _targetTableRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmTaskResource> _taskResourceService;
        private readonly IAttachmentRepository _attachmentRepository;
        private readonly IConfiguration _configuration;
        private readonly ICommentRepository _commentRepository;
        private readonly ILogRepository _logRepository;
        private readonly ITasksRepository _tasksRepository;
        private readonly IHttpClientService _clientService;

        public TargetService(ITargetRepository targetRepository, ITargetGroupRepository targetGroupRepository, ITargetParticipantRepository targetParticipantRepository,
            ITargetTableRepository targetTableRepository, IAttachmentRepository attachmentRepository,
            IResourceService<SharedResource> sharedResourceService, IResourceService<GhmTaskResource> taskResourceService, IHttpClientService httpClientService,
            IConfiguration configuration, ICommentRepository commentRepository, ILogRepository logRepository, ITasksRepository tasksRepository)
        {
            _targetTableRepository = targetTableRepository;
            _targetRepository = targetRepository;
            _attachmentRepository = attachmentRepository;
            _targetParticipantRepository = targetParticipantRepository;
            _targetGroupRepository = targetGroupRepository;
            _sharedResourceService = sharedResourceService;
            _taskResourceService = taskResourceService;
            _configuration = configuration;
            _clientService = httpClientService;
            _commentRepository = commentRepository;
            _logRepository = logRepository;
            _tasksRepository = tasksRepository;
        }

        public async Task<ActionResultResponse> ForceDelete(string tenantId, string userId, long id)
        {
            var info = await _targetRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("Target does not exist"));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (info.ChildCount > 0)
                return new ActionResultResponse(-3, _taskResourceService.GetString("This target have child target. So you can't remove this target"));

            if (info.Status == TargetStatus.Finish)
                return new ActionResultResponse(-4, _taskResourceService.GetString("This target have complete"));

            var coutTaskByTargetId = await _tasksRepository.GetTotalTaskByTargetId(tenantId, id);
            if (coutTaskByTargetId > 0)
                return new ActionResultResponse(-4, _taskResourceService.GetString("This target have task using"));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, info, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var result = await _targetRepository.Delete(tenantId, id);
            if (result > 0 && info.ParentId.HasValue)
                await _targetRepository.UpdateChildCount(tenantId, info.ParentId.Value);

            if (info.ParentId.HasValue)
            {
                await _targetRepository.UpdateChildCount(tenantId, info.ParentId.Value);
            }
            await _targetRepository.GeneralPercentCompletedOfTarget(tenantId, info.ParentId, info.IdPath);

            return new ActionResultResponse(result, result < 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong) : _taskResourceService.GetString("Delete Successfully"));
        }

        public async Task<ActionResultResponse<long>> Insert(string tenantId, string userId, string userFullName, string avatar, TargetMeta targetMeta)
        {
            var targetTableInfo = await _targetTableRepository.GetInfo(tenantId, targetMeta.TargetTableId, true);
            if (targetTableInfo == null)
                return new ActionResultResponse<long>(-1, _taskResourceService.GetString("Target table does not exist"));

            var targetGroupInfo = await _targetGroupRepository.GetInfo(tenantId, targetMeta.TargetGroupId, true);
            if (targetGroupInfo == null)
                return new ActionResultResponse<long>(-2, _taskResourceService.GetString("Target group does not exist"));

            targetMeta.Name = targetMeta.Name?.Trim();
            var isExistsByName = await _targetRepository.CheckNameExist(tenantId, targetMeta.TargetGroupId,
                targetMeta.TargetTableId, targetMeta.Name);
            if (isExistsByName)
                return new ActionResultResponse<long>(-3, _taskResourceService.GetString("Target name does exist"));

            if (targetMeta.StartDate > targetMeta.EndDate)
                return new ActionResultResponse<long>(-4, _taskResourceService.GetString("Start date can't bigger end date"));

            if (targetMeta.EndDate < DateTime.Now)
                return new ActionResultResponse<long>(-19, _taskResourceService.GetString("End date can't less than date now"));

            if ((targetMeta.StartDate < targetTableInfo.StartDate && targetTableInfo.StartDate.HasValue)
                || (targetMeta.EndDate > targetTableInfo.EndDate && targetTableInfo.EndDate.HasValue))
                return new ActionResultResponse<long>(-18, _taskResourceService.GetString("Target isn't in range targertable"));
            // Kiem tra tong trong so khong duoc lon hon 100
            //if (targetMeta.Weight.HasValue)
            //{
            //    var totalWeigthPrevious = await _targetRepository.SumWeightByParentId(tenantId, targetMeta.TargetGroupId, targetMeta.ParentId);
            //    if (totalWeigthPrevious + targetMeta.Weight.Value > 100)
            //        return new ActionResultResponse<long>(-19, _taskResourceService.GetString("Total weight of target parent or target group great than 100"));
            //}

            var apiUrls = _configuration.GetApiUrl();
            if (apiUrls == null)
                return new ActionResultResponse<long>(-6,
                    _sharedResourceService.GetString(
                        "Missing some configuration. Please contact with administrator."));

            var currentUserInfo = await _clientService.GetUserInfo(tenantId, userId);
            if (currentUserInfo == null)
                return new ActionResultResponse<long>(-14, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            var shortUserInfoOfLeaderOffice = new ShortUserInfoViewModel();
            if (targetMeta.Type == TargetType.Personal)
            {
                if (string.IsNullOrEmpty(targetMeta.UserId))
                    return new ActionResultResponse<long>(-13, _taskResourceService.GetString("User is required"));

                var userInfo = await _clientService.GetUserInfo(tenantId, targetMeta.UserId);
                if (userInfo == null)
                    return new ActionResultResponse<long>(-6, _taskResourceService.GetString("User does not exists"));

                if (userId != targetMeta.UserId)
                {
                    if (currentUserInfo.UserType == (int)UserType.Staff ||
                        (userInfo.OfficeIdPath != currentUserInfo.OfficeIdPath && userInfo.OfficeIdPath.IndexOf(currentUserInfo.OfficeIdPath + '.') == -1))
                        return new ActionResultResponse<long>(-12, _taskResourceService.GetString("User not permission create target for user"));
                }

                targetMeta.FullName = userInfo.FullName;
                targetMeta.Avatar = userInfo.Avatar;
                targetMeta.TitleId = userInfo.TitleId;
                targetMeta.TitleName = userInfo.TitleName;
                targetMeta.OfficeIdPath = userInfo.OfficeIdPath;
                targetMeta.OfficeId = userInfo.OfficeId;
                targetMeta.OfficeName = userInfo.OfficeName;
            }

            // Check tao cho phong ban
            if (targetMeta.Type == TargetType.Office)
            {
                if (!targetMeta.OfficeId.HasValue)
                    return new ActionResultResponse<long>(-14, _taskResourceService.GetString("Office is required"));

                var officeInfo = await _clientService
                    .GetAsync<OfficeInfoViewModel>($"{apiUrls.HrApiUrl}/offices/{tenantId}/office-short-info/{targetMeta.OfficeId.Value}");

                if (officeInfo == null)
                    return new ActionResultResponse<long>(-15, _taskResourceService.GetString("Office does not exists"));

                shortUserInfoOfLeaderOffice = await _clientService.GetAsync<ShortUserInfoViewModel>($"{apiUrls.HrApiUrl}/users/{tenantId}/short-user-info-leader-office/{targetMeta.OfficeId.Value}");
                if (shortUserInfoOfLeaderOffice == null)
                    return new ActionResultResponse<long>(-17, _taskResourceService.GetString("Office does not exists leader"));

                if (currentUserInfo.UserType == (int)UserType.Staff ||
                    (officeInfo.OfficeIdPath != currentUserInfo.OfficeIdPath && officeInfo.OfficeIdPath.IndexOf(currentUserInfo.OfficeIdPath + '.') == -1))
                    return new ActionResultResponse<long>(-16, _taskResourceService.GetString("User not permission create target for this office"));

                targetMeta.OfficeName = officeInfo.Name;
                targetMeta.UserId = shortUserInfoOfLeaderOffice.UserId;
                targetMeta.FullName = shortUserInfoOfLeaderOffice.FullName;
                targetMeta.TitleId = shortUserInfoOfLeaderOffice.TitleId;
                targetMeta.TitleName = shortUserInfoOfLeaderOffice.TitleName;
                targetMeta.Avatar = shortUserInfoOfLeaderOffice.Avatar;
                targetMeta.OfficeIdPath = shortUserInfoOfLeaderOffice.OfficeIdPath;
            }

            var userOwnerId = targetMeta.Type == TargetType.Personal ? targetMeta.UserId : shortUserInfoOfLeaderOffice.UserId;
            if (targetMeta.TargetParticipants != null && targetMeta.TargetParticipants.Any())
            {
                var targetParticipantsInfo = targetMeta.TargetParticipants.FirstOrDefault(x => x.UserId == userOwnerId);
                if (targetParticipantsInfo != null)
                {
                    targetParticipantsInfo.Role = 127;
                }
                else
                {
                    targetMeta.TargetParticipants.Add(new TargetParticipantsMeta()
                    {
                        UserId = userOwnerId,
                        Role = 127
                    });
                }
            }
            else
            {
                targetMeta.TargetParticipants.Add(new TargetParticipantsMeta()
                {
                    UserId = userOwnerId,
                    Role = 127
                });
            }

            var target = new Target
            {
                ChildCount = 0,
                Description = targetMeta.Description?.Trim(),
                LevelOfSharing = targetMeta.LevelOfSharing,
                MeasurementMethod = targetMeta.MeasurementMethod,
                MethodCalculateResult = targetMeta.MethodCalculateResult,
                Name = targetMeta.Name,
                UnsignName = targetMeta.Name.StripVietnameseChars().ToUpper(),
                OfficeId = targetMeta.OfficeId,
                OfficeName = targetMeta.OfficeName,
                OfficeIdPath = targetMeta.OfficeIdPath,
                StartDate = targetMeta.StartDate,
                EndDate = targetMeta.EndDate,
                ParentId = targetMeta.ParentId,
                CreatorId = userId,
                CreatorFullName = userFullName, //check
                UserId = targetMeta.UserId,
                FullName = targetMeta.FullName,
                TitleId = targetMeta.TitleId,
                TitleName = targetMeta.TitleName,
                Avatar = targetMeta.Avatar,
                TargetTableId = targetMeta.TargetTableId,
                TargetGroupId = targetMeta.TargetGroupId,
                Type = targetMeta.Type,
                TenantId = tenantId,
                Weight = targetMeta.Weight
            };

            if (targetMeta.ParentId.HasValue)
            {
                var parentInfo = await _targetRepository.GetInfo(tenantId, targetMeta.ParentId.Value);
                if (parentInfo == null)
                    return new ActionResultResponse<long>(-5, _taskResourceService.GetString("Task parent does not exist"));

                var isHasPermission = await _targetRepository.CheckPermission(tenantId, parentInfo, userId, TargetRole.WriteTargetChild);
                if (!isHasPermission)
                    return new ActionResultResponse<long>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

                target.ParentId = parentInfo.Id;
                target.TargetGroupId = parentInfo.TargetGroupId;
                target.TargetTableId = parentInfo.TargetTableId;
                target.Type = parentInfo.Type;
                target.IdPath = $"{parentInfo.IdPath}.-1";
            }

            var result = await _targetRepository.Insert(target);
            if (result <= 0)
                return new ActionResultResponse<long>(result, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            #region 
            // Update IdPath
            if (targetMeta.ParentId.HasValue)
            {
                target.IdPath = target.IdPath.Replace("-1", target.Id.ToString());
            }
            else
            {
                target.IdPath = $"{target.Id.ToString()}";
            }
            await _targetRepository.UpdateIdPath(target.Id, target.IdPath);

            //Update parent ChildCount va tin
            if (target.ParentId.HasValue)
            {
                await _targetRepository.UpdateChildCount(tenantId, target.ParentId.Value);
                await _targetRepository.GeneralPercentCompletedOfTarget(tenantId, target.ParentId, target.IdPath);
            }

            #endregion
            // Insert participant target
            if (targetMeta.TargetParticipants != null && targetMeta.TargetParticipants.Any() &&
                targetMeta.TargetParticipants.Count > 0)
            {
                var resultInsertTargetParicipant = await InsertParticipant();
                if (resultInsertTargetParicipant.Code <= 0)
                {
                    await RollbackInsertTarget();
                    return resultInsertTargetParicipant;
                }
            }

            #region
            // Insert target attachment
            if (targetMeta.TargetAttachments.Count > 0)
            {
                var resultInsertAttachment = await InsertAttachments();
                if (resultInsertAttachment.Code <= 0)
                {
                    await RollbackInsertTargetParicipants();
                    await RollbackInsertTarget();
                    return resultInsertAttachment;
                }
            }

            #endregion
            return new ActionResultResponse<long>(result, _taskResourceService.GetString("Insert target successfully"), "", target.Id);

            #region Local functions
            async Task<ActionResultResponse<long>> InsertParticipant()
            {
                List<TargetParticipant> targetParticipants = new List<TargetParticipant>();
                var listStringUserId = new List<string>();

                foreach (var user in targetMeta.TargetParticipants)
                {
                    var isUserExists = await _targetParticipantRepository.CheckExistsByTargetIdUserId(tenantId, target.Id, user.UserId);
                    if (isUserExists) continue;
                    listStringUserId.Add(user.UserId);
                }

                var resultTargetParicipants = await _clientService
                    .PostAsync<List<ShortUserInfoViewModel>>($"{apiUrls.HrApiUrl}/users/{tenantId}/short-user-info",
                        listStringUserId);

                if (resultTargetParicipants == null || !resultTargetParicipants.Any())
                {
                    await RollbackInsertTarget();
                    return new ActionResultResponse<long>(-7,
                        _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
                }

                foreach (var userParicipant in resultTargetParicipants)
                {
                    var targetParicipantMeta = targetMeta.TargetParticipants.FirstOrDefault(x => x.UserId == userParicipant.UserId);
                    if (targetParicipantMeta != null)
                    {
                        var targetParicipantInsert = new TargetParticipant
                        {
                            TargetId = target.Id,
                            UserId = userParicipant.UserId,
                            Image = userParicipant.Avatar,
                            FullName = userParicipant.FullName,
                            OfficeId = userParicipant.OfficeId,
                            OfficeName = userParicipant.OfficeName,
                            TitleId = userParicipant.TitleId,
                            TitleName = userParicipant.TitleName,
                            Role = targetParicipantMeta.Role,
                            TenantId = tenantId,
                            OfficeIdPath = userParicipant.OfficeIdPath,
                            ConcurrencyStamp = Guid.NewGuid().ToString()
                        };

                        targetParticipants.Add(targetParicipantInsert);
                    }
                }

                var resultTargetParticipant = await _targetParticipantRepository.InsertListParticipant(tenantId,
                    userFullName, userId, targetParticipants);
                if (resultTargetParticipant > 0)
                {
                    // Notification
                    return new ActionResultResponse<long>(result,
                        _taskResourceService.GetString("Add participant successful."));
                }

                return new ActionResultResponse<long>(-8,
                    _taskResourceService.GetString("Can not insert participant. Please contact with administrator."));
            }

            async Task<ActionResultResponse<long>> InsertAttachments()
            {
                List<Attachment> targetAttachment = new List<Attachment>();

                foreach (var attachment in targetMeta.TargetAttachments)
                {
                    var isExistsAttachment = await _attachmentRepository.CheckExistsByFileIdObjectId(tenantId, target.Id,
                        ObjectType.Target, attachment.FileId);
                    if (!isExistsAttachment)
                    {
                        var attachmentId = Guid.NewGuid().ToString();
                        var item = new Attachment
                        {
                            Id = attachmentId,
                            FileId = attachment.FileId,
                            FileName = attachment.FileName,
                            Url = attachment.Url,
                            ObjectType = ObjectType.Target,
                            ObjectId = target.Id,
                            TenantId = tenantId,
                            CreateTime = attachment.CreateTime,
                            CreatorId = attachment.CreatorId,
                            Type = attachment.Type,
                            Extension = attachment.Extension,
                            CreatorFullName = attachment.CreatorFullName,
                            ConcurrencyStamp = attachmentId,
                        };
                        targetAttachment.Add(item);
                    }
                }

                var resultAttachment = await _attachmentRepository.InsertAttachments(targetAttachment);

                if (resultAttachment <= 0)
                    return new ActionResultResponse<long>(-10, _taskResourceService.GetString("Insert attachment does have something wrong."));

                return new ActionResultResponse<long>(resultAttachment,
                    _taskResourceService.GetString("Add attachment successful."));
            }

            async Task RollbackInsertTarget()
            {
                await _targetRepository.ForceDelete(tenantId, target.Id);
            }

            async Task RollbackInsertTargetParicipants()
            {
                await _targetParticipantRepository.FoceDeleteByTargetId(tenantId, target.Id);
            }
            #endregion
        }

        public async Task<ActionResultResponse<string>> InsertParticipants(string tenantId, string userId, string fullName, string avatar,
            TargetParticipantsMeta targetParticipantsMeta)
        {
            var targetInfo = await _targetRepository.GetInfo(tenantId, targetParticipantsMeta.TargetId, true);
            if (targetInfo == null)
                return new ActionResultResponse<string>(-1, _taskResourceService.GetString("Target does not exist"));

            var checkExist = await _targetParticipantRepository.CheckExistsByTargetIdUserId(tenantId, targetParticipantsMeta.TargetId, targetParticipantsMeta.UserId);
            if (checkExist)
                return new ActionResultResponse<string>(-2, _taskResourceService.GetString("Participants does exist"));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, targetInfo, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (targetParticipantsMeta.UserId == targetInfo.UserId)
                return new ActionResultResponse<string>(-2, _taskResourceService.GetString("User is create this target"));

            var userInfo = await _clientService.GetUserInfo(tenantId, targetParticipantsMeta.UserId);
            if (userInfo == null)
                return new ActionResultResponse<string>(-3, _taskResourceService.GetString("User does exist"));

            TargetParticipant targetParticipant = new TargetParticipant
            {
                Role = targetParticipantsMeta.Role,
                TargetId = targetParticipantsMeta.TargetId,
                UserId = targetParticipantsMeta.UserId,
                FullName = targetParticipantsMeta.FullName,
                TenantId = tenantId,
                Image = targetParticipantsMeta.Image,
                TitleId = userInfo.TitleId,
                TitleName = userInfo.TitleName,
                OfficeId = userInfo.OfficeId,
                OfficeName = userInfo.OfficeName,
                Id = Guid.NewGuid().ToString()
            };

            var result = await _targetParticipantRepository.InsertParticipants(targetParticipant);
            if (result < 0)
                return new ActionResultResponse<string>(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TargetInsertParticipant,
                FromValue = JsonConvert.SerializeObject(targetParticipant),
                ToValue = null,
                ObjectId = targetInfo.Id,
                ObjectType = ObjectType.Target,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Json
            });

            return new ActionResultResponse<string>(1, _taskResourceService.GetString("Insert successfully!"), "", targetParticipant.Id);
        }

        public async Task<ActionResultResponse> UpdateMeasurementMethod(string tenantId, string userId, string fullName, string avatar, long id,
            string measurementMethod)
        {
            var info = await _targetRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("Target does not exist"));

            if (tenantId != info.TenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (info.MeasurementMethod == measurementMethod?.Trim())
                return new ActionResultResponse(-2, _taskResourceService.GetString("You does not measurement method"));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, info, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var oldMeasurementMethod = info.MeasurementMethod;

            info.ConcurrencyStamp = Guid.NewGuid().ToString();
            info.LastUpdateUserId = userId;
            info.LastUpdateFullName = fullName;
            info.MeasurementMethod = measurementMethod?.Trim();

            var result = await _targetRepository.Update(info);
            if (result < 0)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TargetUpdateMeasurementMethod,
                FromValue = oldMeasurementMethod,
                ToValue = measurementMethod,
                ObjectId = info.Id,
                ObjectType = ObjectType.Target,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Value
            });

            return new ActionResultResponse(1, _taskResourceService.GetString("Update measurementMethod Successfully"));
        }

        public async Task<ActionResultResponse> UpdateMethodCalculateResult(string tenantId, string userId, string fullName, string avatar, long id,
            TargetMethodCalculateResultType methodCalculateResult)
        {
            var info = await _targetRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("Target does not exist"));

            if (tenantId != info.TenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (info.MethodCalculateResult == methodCalculateResult)
                return new ActionResultResponse(-2, _taskResourceService.GetString("You does not change method calculate result"));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, info, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var oldMethodCaculateResult = info.MethodCalculateResult;

            info.ConcurrencyStamp = Guid.NewGuid().ToString();
            info.LastUpdateUserId = userId;
            info.LastUpdateFullName = fullName;
            info.MethodCalculateResult = methodCalculateResult;

            if (methodCalculateResult == TargetMethodCalculateResultType.TargetChild)
            {
                var listTargetChild = await _targetRepository.GetTargetChildren(tenantId, info.Id, true);
                if (listTargetChild != null && listTargetChild.Any())
                {
                    var totalWeight = listTargetChild.Sum(x => x.Weight);
                    var totalPercentComplete = listTargetChild.Sum(x => x.Weight * x.PercentCompleted);

                    info.PercentCompleted = totalWeight > 0 ? Math.Round((float)(totalPercentComplete / totalWeight), 2) : 0;
                }
            }

            var result = await _targetRepository.Update(info);
            if (result < 0)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TargetUpdateMethodCaculateResult,
                FromValue = oldMethodCaculateResult.ToString(),
                ToValue = methodCalculateResult.ToString(),
                ObjectId = info.Id,
                ObjectType = ObjectType.Target,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Value
            });

            return new ActionResultResponse(1, _taskResourceService.GetString("Update measurementMethod Successfully"));
        }

        public async Task<ActionResultResponse> UpdateRoleParticipant(string tenantId, string userId, string fullName, string avatar,
            string id, int roleParticipants)
        {
            var info = await _targetParticipantRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("Target participants does not exist"));

            if (tenantId != info.TenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var targetInfo = await _targetRepository.GetInfo(tenantId, info.TargetId, true);
            if (targetInfo == null)
                return new ActionResultResponse(-3, _taskResourceService.GetString("Target does not exist"));

            if (info.UserId == targetInfo.UserId)
                return new ActionResultResponse(-4, _taskResourceService.GetString("User does not permission update role for this user"));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, targetInfo, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var oldRole = info.Role;

            info.ConcurrencyStamp = Guid.NewGuid().ToString();
            info.Role = roleParticipants;

            var result = await _targetParticipantRepository.Update(info);
            if (result < 0)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TargetUpdateRoleParticipant,
                FromValue = JsonConvert.SerializeObject(new
                {
                    info.FullName,
                    role = oldRole
                }),
                ToValue = JsonConvert.SerializeObject(new
                {
                    info.FullName,
                    role = roleParticipants
                }),
                ObjectId = targetInfo.Id,
                ObjectType = ObjectType.Target,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Json
            });

            return new ActionResultResponse(1, _taskResourceService.GetString("Update Target participants Successfully"));
        }

        public async Task<ActionResultResponse> UpdateTargetStatus(string tenantId, string userId, string fullName, string avatar, long id, TargetStatus status)
        {
            var info = await _targetRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("Target does not exist"));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (info.Status == status)
                return new ActionResultResponse(-2, _taskResourceService.GetString("You does not change status"));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, info, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var oldStatus = info.Status;

            info.Status = status;
            info.CompletedDate = status == TargetStatus.Finish ? DateTime.Now : info.CompletedDate;
            info.LastUpdate = DateTime.Now;
            info.LastUpdateUserId = userId;
            info.LastUpdateFullName = fullName;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();

            var result = await _targetRepository.Update(info);
            if (result < 0)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TargetUpdateStatus,
                FromValue = oldStatus.ToString(),
                ToValue = status.ToString(),
                ObjectId = info.Id,
                ObjectType = ObjectType.Target,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Value
            });

            if (info.ParentId.HasValue)
                await _targetRepository.UpdateChildCount(tenantId, info.ParentId.Value);

            return new ActionResultResponse(1, _taskResourceService.GetString("update target status successfully"));
        }

        public async Task<ActionResultResponse> UpdateTargetDescription(string tenantId, string userId, string fullName, string avatar,
            long id, string description)
        {
            var info = await _targetRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("target does not exist"));

            if (info.Description == description?.Trim())
                return new ActionResultResponse(-2, _taskResourceService.GetString("You does not change description"));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, info, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var oldDescription = info.Description;

            info.Description = description?.Trim();
            info.LastUpdate = DateTime.Now;
            info.LastUpdateUserId = userId;
            info.LastUpdateFullName = fullName;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();

            var result = await _targetRepository.Update(info);
            if (result < 0)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TargetUpdateDescription,
                FromValue = oldDescription,
                ToValue = description,
                ObjectId = info.Id,
                ObjectType = ObjectType.Target,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Value
            });

            return new ActionResultResponse(1, _taskResourceService.GetString("update target description successfully"));
        }

        public async Task<ActionResultResponse> UpdateTargetName(string tenantId, string userId, string fullName, string avatar, long id, string name)
        {
            var info = await _targetRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("Target does not exist"));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (info.Name == name?.Trim())
                return new ActionResultResponse(-3, _taskResourceService.GetString("You does not change name"));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, info, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var oldTargetName = info.Name;

            info.Name = name?.Trim();
            info.UnsignName = name.StripVietnameseChars().ToUpper();
            info.LastUpdateUserId = userId;
            info.LastUpdateFullName = fullName;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();

            var result = await _targetRepository.Update(info);
            if (result < 0)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TargetUpdateName,
                FromValue = oldTargetName,
                ToValue = name,
                ObjectId = info.Id,
                ObjectType = ObjectType.Target,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Value
            });

            return new ActionResultResponse(1, _taskResourceService.GetString("update target Name successfully"));
        }

        public async Task<ActionResultResponse> UpdateTargetPercentCompleted(string tenantId, string userId, string fullName, string avatar,
            long id, double percentCompleted)
        {
            var info = await _targetRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("target does not exist"));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (info.PercentCompleted == percentCompleted)
                return new ActionResultResponse(-3, _taskResourceService.GetString("You does not change percent completed"));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, info, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (percentCompleted <= 0 || percentCompleted >= 1000)
                return new ActionResultResponse(-1, _taskResourceService.GetString("Percent Completed does have error. Percent must > 0 And < 100"));

            var oldPercentCompleted = info.PercentCompleted;

            info.PercentCompleted = percentCompleted;
            info.LastUpdateUserId = userId;
            info.LastUpdateFullName = fullName;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();

            var result = await _targetRepository.Update(info);
            if (result < 0)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await _targetRepository.GeneralPercentCompletedOfTarget(tenantId, info.ParentId, info.IdPath);
            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TargetUpdatePercentCompleted,
                FromValue = oldPercentCompleted.ToString(),
                ToValue = percentCompleted.ToString(),
                ObjectId = info.Id,
                ObjectType = ObjectType.Target,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Value
            });

            return new ActionResultResponse(1, _taskResourceService.GetString("update target percent completed successfully"));
        }

        public async Task<ActionResultResponse> UpdateTargetTableGroup(string tenantId, string userId, string fullName, string avatar, long id,
            string targetTableId, string targetGroupId)
        {
            var info = await _targetRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("target does not exist"));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (info.ParentId.HasValue)
                return new ActionResultResponse(-9, _taskResourceService.GetString("This target is target child"));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, info, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (info.TargetTableId == targetTableId && info.TargetGroupId == targetGroupId)
                return new ActionResultResponse(-3, _taskResourceService.GetString("You does not change target table"));

            var oldTargetGroupInfo = await _targetGroupRepository.GetInfo(tenantId, info.TargetGroupId, true);
            if (oldTargetGroupInfo == null)
                return new ActionResultResponse(-5, _taskResourceService.GetString("Target group does not exist"));

            var targetGroupInfo = await _targetGroupRepository.GetInfo(tenantId, targetGroupId, true);
            if (targetGroupInfo == null)
                return new ActionResultResponse(-2, _taskResourceService.GetString("Target group does not exist"));

            var oldTargetTableInfo = await _targetTableRepository.GetInfo(tenantId, targetTableId, true);
            if (oldTargetTableInfo == null)
                return new ActionResultResponse(-3, _taskResourceService.GetString("Target table does not exist"));

            var targetTableInfo = await _targetTableRepository.GetInfo(tenantId, targetTableId, true);
            if (targetTableInfo == null)
                return new ActionResultResponse(-3, _taskResourceService.GetString("Target table does not exist"));

            if (targetGroupInfo.TargetTableId != targetTableInfo.Id)
                return new ActionResultResponse(-4, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            info.TargetGroupId = targetGroupId;
            info.TargetTableId = targetTableId;
            info.LastUpdateUserId = userId;
            info.LastUpdateFullName = fullName;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();

            var result = await _targetRepository.Update(info);
            if (result <= 0)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            var listTargetChild = await _targetRepository.GetAllTargetChildren(tenantId, info.IdPath);
            if (listTargetChild != null && listTargetChild.Any())
            {
                foreach (var targetChild in listTargetChild)
                {
                    targetChild.TargetGroupId = targetGroupId;
                    targetChild.TargetTableId = targetTableId;

                    await _targetRepository.Update(targetChild);
                };
            }

            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TargetUpdateTargetTable,
                FromValue = JsonConvert.SerializeObject(new
                {
                    TargetGroupName = oldTargetGroupInfo.Name,
                    TargetTableName = oldTargetTableInfo.Name,
                }),
                ToValue = JsonConvert.SerializeObject(new
                {
                    TargetGroupName = targetGroupInfo.Name,
                    TargetTableName = targetTableInfo.Name,
                }),
                ObjectId = info.Id,
                ObjectType = ObjectType.Target,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Json
            });

            return new ActionResultResponse(1, _taskResourceService.GetString("update table group successfully"));
        }

        public async Task<ActionResultResponse> UpdateTargetDate(string tenantId, string userId, string fullName, string avatar, long id,
            DateTime starDate, DateTime endDate)
        {
            if (starDate > endDate)
                return new ActionResultResponse(-1, _taskResourceService.GetString("Please check start date and end date again"));

            if (endDate < DateTime.Now)
                return new ActionResultResponse<long>(-19, _taskResourceService.GetString("End date can't less than date now"));

            var info = await _targetRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("target does not exist"));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, info, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var oldStartDate = info.StartDate;
            var oldEndDate = info.EndDate;

            info.StartDate = starDate;
            info.EndDate = endDate;
            info.LastUpdateUserId = userId;
            info.LastUpdateFullName = fullName;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();
            var result = await _targetRepository.Update(info);

            if (result < 0)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TargetUpdateTargetDate,
                FromValue = JsonConvert.SerializeObject(new
                {
                    StartDate = oldStartDate,
                    EndDate = oldEndDate,
                }),
                ToValue = JsonConvert.SerializeObject(new
                {
                    StartDate = starDate,
                    EndDate = endDate,
                }),
                ObjectId = info.Id,
                ObjectType = ObjectType.Target,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Json
            });

            return new ActionResultResponse(1, _taskResourceService.GetString("update target datetime successfully"));
        }

        public async Task<ActionResultResponse> UpdateTargetCompleteDate(string tenantId, string userId, string fullName, string avatar, long id,
            DateTime completeDate)
        {
            var info = await _targetRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("target does not exist"));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (info.CompletedDate == completeDate)
                return new ActionResultResponse(-3, _taskResourceService.GetString("You does not change complete date"));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, info, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (info.Status == TargetStatus.New)
                return new ActionResultResponse(-3, _taskResourceService.GetString("Target have yet complete"));

            if (completeDate > DateTime.Now)
                return new ActionResultResponse(-4, _taskResourceService.GetString("Complete date must lessthan or equal this time"));

            var oldCompleteDate = info.CompletedDate;

            info.CompletedDate = completeDate;
            info.LastUpdateUserId = userId;
            info.LastUpdateFullName = fullName;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();

            var result = await _targetRepository.Update(info);
            if (result < 0)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TargetUpdateTargetCompleteDate,
                FromValue = oldCompleteDate.ToString(),
                ToValue = completeDate.ToString(),
                ObjectId = info.Id,
                ObjectType = ObjectType.Target,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.DateTime
            });

            return new ActionResultResponse(1, _taskResourceService.GetString("update target datetime successfully"));
        }

        public async Task<ActionResultResponse> UpdateWeight(string tenantId, string userId, string fullName, string avatar, long id, double weight)
        {
            var info = await _targetRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("target does not exist"));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (info.Weight == weight)
                return new ActionResultResponse(-3, _taskResourceService.GetString("You does not change weight"));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, info, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            //var totalWeigthPrevious = await _targetRepository.SumWeightByParentId(tenantId, info.TargetGroupId, info.ParentId);
            //if (totalWeigthPrevious + weight - info.Weight > 100)
            //    return new ActionResultResponse<long>(-3, _taskResourceService.GetString("Total weight of target parent or target group great than 100"));

            var oldWeight = info.Weight;

            info.Weight = weight;
            info.LastUpdateUserId = userId;
            info.LastUpdateFullName = fullName;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();

            var result = await _targetRepository.Update(info);
            if (result < 0)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await _targetRepository.GeneralPercentCompletedOfTarget(tenantId, info.ParentId, info.IdPath);
            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TargetUpdateWeight,
                FromValue = oldWeight.ToString(),
                ToValue = weight.ToString(),
                ObjectId = info.Id,
                ObjectType = ObjectType.Target,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Value
            });

            return new ActionResultResponse(1, _taskResourceService.GetString("update target weight successfully"));
        }

        public async Task<ActionResultResponse<TargetDetailViewModel>> GetDetail(string tenantId, long id, string userId, string fullName, string avatar)
        {
            var targetInfo = await _targetRepository.GetInfo(tenantId, id, true);
            if (targetInfo == null)
                return new ActionResultResponse<TargetDetailViewModel>
                {
                    Code = -1,
                    Data = null,
                    Message = "Target does not exist"
                };

            var targetTableInfo = await _targetTableRepository.GetInfo(tenantId, targetInfo.TargetTableId, true);
            if (targetTableInfo == null)
                return new ActionResultResponse<TargetDetailViewModel>
                {
                    Code = -2,
                    Data = null,
                    Message = "TargetTable does not exist"
                };

            var targetGroupInfo = await _targetGroupRepository.GetInfo(tenantId, targetInfo.TargetGroupId);
            if (targetGroupInfo == null)
                return new ActionResultResponse<TargetDetailViewModel>
                {
                    Code = -3,
                    Data = null,
                    Message = "TargetGroup does not exist"
                };

            var currentInfo = await _clientService.GetUserInfo(tenantId, userId);

            var listParticipant = await _targetParticipantRepository.GetListByTargetId(tenantId, targetInfo.Id, out int totalParticipants);
            var listAttachment = await _attachmentRepository.GetsAllByObjectId(tenantId, ObjectType.Target, targetInfo.Id, 1, 10, out int totalAttachment);
            var listTargetChildren = await _targetRepository.GetTargetChildById(tenantId, userId, targetInfo.IdPath);

            var targetDetail = new TargetDetailViewModel
            {
                Id = targetInfo.Id,
                TargetTableId = targetInfo.TargetTableId,
                TargetGroupId = targetInfo.TargetGroupId,
                TargetGroupName = targetGroupInfo.Name,
                TargetGroupColor = targetGroupInfo.Color,
                ParentId = targetInfo.ParentId,
                ChildCount = targetInfo.ChildCount,
                IdPath = targetInfo.IdPath,
                Name = targetInfo.Name,
                Type = targetInfo.Type,
                UserId = targetInfo.UserId,
                FullName = targetInfo.FullName,
                Avatar = targetInfo.Avatar,
                OfficeId = targetInfo.OfficeId,
                OfficeName = targetInfo.OfficeName,
                Weight = targetInfo.Weight,
                StartDate = targetInfo.StartDate,
                EndDate = targetInfo.EndDate,
                CompletedDate = targetInfo.CompletedDate,
                LevelOfSharing = targetInfo.LevelOfSharing,
                PercentCompleted = targetInfo.PercentCompleted,
                Status = targetInfo.Status,
                Description = targetInfo.Description,
                TotalTask = targetInfo.TotalTask,
                TotalTaskComplete = targetInfo.TotalTaskComplete,
                TotalAttachment = totalAttachment,
                Participants = listParticipant,
                Attachments = listAttachment,
                TargetTableName = targetTableInfo.Name,
                MeasurementMethod = targetInfo.MeasurementMethod,
                MethodCalculateResult = targetInfo.MethodCalculateResult,
                CreateTime = targetInfo.CreateTime,
                CreatorId = targetInfo.CreatorId,
                CreatorFullName = targetInfo.CreatorFullName,
                TitleName = targetInfo.TitleName,
                TitleId = targetInfo?.TitleId,
                Role = 127,
                TargetChildren = listTargetChildren != null && listTargetChildren.Any() ? RenderTree(listTargetChildren, targetInfo.Id) : null,
                OverdueDate = targetInfo.Status == TargetStatus.New ? (int)DateTime.Now.Date.Subtract(targetInfo.EndDate.Date).TotalDays
                             : targetInfo.CompletedDate.HasValue ?(int)targetInfo.CompletedDate?.Date.Subtract(targetInfo.EndDate.Date).TotalDays : 0
            };

            if (userId != targetInfo.CreatorId)
            {
                var targetPaticipantInfo = await _targetParticipantRepository.GetInfo(tenantId, targetInfo.Id, userId, true);
                if (targetPaticipantInfo == null && targetInfo.LevelOfSharing == LevelOfSharing.privates
                    && targetInfo.OfficeId != currentInfo.OfficeId)
                    return new ActionResultResponse<TargetDetailViewModel>
                    {
                        Code = -4,
                        Data = null,
                        Message = "User not in this target"
                    };

                targetDetail.Role = targetPaticipantInfo != null ? targetPaticipantInfo.Role : (int)TargetRole.Read;
            }

            if (targetInfo.ParentId.HasValue)
            {
                var targetParentInfo = await _targetRepository.GetInfo(tenantId, targetInfo.ParentId.Value, true);
                if (targetParentInfo == null)
                    return new ActionResultResponse<TargetDetailViewModel>
                    {
                        Code = -5,
                        Data = null,
                        Message = "Target parent does not exists"
                    };

                targetDetail.ParentName = targetParentInfo.Name;
            }

            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TargetGetDetail,
                FromValue = string.Empty,
                ToValue = string.Empty,
                ObjectId = targetDetail.Id,
                ObjectType = ObjectType.Target,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Value
            });

            return new ActionResultResponse<TargetDetailViewModel>
            {
                Code = 1,
                Data = targetDetail,
                Message = "Get Successfully!"
            };
        }

        public async Task<ActionResultResponse> UpdateLevelOfSharing(string tenantId, string userId, string fullName, string avatar,
            long id, LevelOfSharing levelOfSharing)
        {
            var info = await _targetRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("target does not exist"));

            if (info.LevelOfSharing == levelOfSharing)
                return new ActionResultResponse(-3, _taskResourceService.GetString("You does not change levelOfSharing"));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, info, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var oldlevelOfSharing = info.LevelOfSharing;

            info.LevelOfSharing = levelOfSharing;
            info.LastUpdateUserId = userId;
            info.LastUpdateFullName = fullName;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();

            var result = await _targetRepository.Update(info);
            if (result < 0)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TargetUpdateLevelOfSharing,
                FromValue = oldlevelOfSharing.ToString(),
                ToValue = levelOfSharing.ToString(),
                ObjectId = info.Id,
                ObjectType = ObjectType.Target,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Value
            });

            return new ActionResultResponse(1, _taskResourceService.GetString("update target level of Sharing completed successfully"));
        }

        public async Task<ActionResultResponse> UpdateTargetGroupId(string tenantId, string userId, string fullName, string avatar, long id, string groupId)
        {
            var targetInfo = await _targetRepository.GetInfo(tenantId, id, false);
            if (targetInfo == null)
                return new ActionResultResponse(-2, _taskResourceService.GetString("target does not exist"));

            if (targetInfo.TargetGroupId == groupId)
                return new ActionResultResponse(-3, _taskResourceService.GetString("You does not change target group"));

            var targetGroupInfo = await _targetGroupRepository.GetInfo(tenantId, groupId, true);
            if (targetGroupInfo == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("Target group does not exist"));

            var oldTargetGroupInfo = await _targetGroupRepository.GetInfo(tenantId, targetInfo.TargetGroupId, true);
            if (oldTargetGroupInfo == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("Target group does not exist"));

            if (targetInfo.ParentId.HasValue)
                return new ActionResultResponse(-3, _taskResourceService.GetString("This target is target child."));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, targetInfo, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            targetInfo.TargetGroupId = groupId;
            targetInfo.LastUpdateUserId = userId;
            targetInfo.LastUpdateFullName = fullName;
            targetInfo.ConcurrencyStamp = Guid.NewGuid().ToString();

            var result = await _targetRepository.Update(targetInfo);
            if (result < 0)
                return new ActionResultResponse(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            var listTargetChild = await _targetRepository.GetAllTargetChildren(tenantId, targetInfo.IdPath);
            if (listTargetChild != null && listTargetChild.Any())
            {
                foreach (var targetChild in listTargetChild)
                {
                    targetChild.TargetGroupId = groupId;
                    await _targetRepository.Update(targetChild);
                };
            }

            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TargetUpdateTargetGroupId,
                FromValue = JsonConvert.SerializeObject(new
                {
                    TargetGroupId = oldTargetGroupInfo.Id,
                    TargetGroupName = oldTargetGroupInfo.Name,
                }),
                ToValue = JsonConvert.SerializeObject(new
                {
                    TargetGroupId = targetGroupInfo.Id,
                    TargetGroupName = targetGroupInfo.Name,
                }),
                ObjectId = targetInfo.Id,
                ObjectType = ObjectType.Target,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Json
            });

            return new ActionResultResponse(1, _taskResourceService.GetString("Update Target Group Successfully"));
        }

        public async Task<SearchResult<TargetTreeViewModel>> SearchTargetOffice(string tenantId, int? officeId, string currentUserId, string keyword,
            DateTime? startDate, DateTime? endDate, string targetTableId, TargetStatus? status)
        {
            var apiUrls = _configuration.GetApiUrl();
            if (apiUrls == null)
                return new SearchResult<TargetTreeViewModel>(-2, _sharedResourceService.GetString(
                        "Missing some configuration. Please contact with administrator."));

            var currentUserInfo = await _clientService.GetUserInfo(tenantId, currentUserId);
            if (currentUserInfo == null)
                return new SearchResult<TargetTreeViewModel>
                {
                    Code = -1,
                    Message = ErrorMessage.SomethingWentWrong
                };

            if (!officeId.HasValue)
                officeId = currentUserInfo.OfficeId;

            var officeInfo = await _clientService
                   .GetAsync<OfficeInfoViewModel>($"{apiUrls.HrApiUrl}/offices/{tenantId}/office-short-info/{officeId}");
            if (officeInfo == null)
                return new SearchResult<TargetTreeViewModel>(-3, _taskResourceService.GetString("Office does not exists"));

            //if(officeId != currentUserInfo.OfficeId && officeInfo.OfficeIdPath.IndexOf(currentUserInfo.OfficeIdPath + '.') == -1)
            //    return new SearchResult<TargetTreeViewModel>(-4, _taskResourceService.GetString("User not permession view target of this office"));

            var targets = await _targetRepository.SearchTargetOffice(tenantId, officeId.Value, currentUserInfo.OfficeIdPath, currentUserId, keyword,
                startDate, endDate, targetTableId, status);
            if (targets == null || !targets.Any())
                return new SearchResult<TargetTreeViewModel>
                {
                    Code = 1,
                    Items = null
                };

            var tree = RenderTree(targets, null);
            return new SearchResult<TargetTreeViewModel>
            {
                Code = 1,
                Items = tree
            };
        }

        public async Task<SearchResult<TargetTreeViewModel>> SearchTargetPersonal(string tenantId, string currentUserId, string userId, string keyword,
            DateTime? startDate, DateTime? endDate, string targetTableId,
            TargetStatus? status, List<int> listType)
        {
            if (listType == null || !listType.Any() || listType.Count == 0)
                return new SearchResult<TargetTreeViewModel>
                {
                    Code = 1,
                    Items = null,
                };
  
            var currentUserInfo = await _clientService.GetUserInfo(tenantId, currentUserId);
            if (currentUserInfo == null)
                return new SearchResult<TargetTreeViewModel>
                {
                    Code = -1,
                    Message = ErrorMessage.SomethingWentWrong
                };

            if (string.IsNullOrEmpty(userId))
                userId = currentUserInfo.UserId;

            if (userId != currentUserId)
            {
                var userInfo = await _clientService.GetUserInfo(tenantId, userId);
                if (userInfo == null)
                    return new SearchResult<TargetTreeViewModel>
                    {
                        Code = -1,
                        Message = _taskResourceService.GetString("User does not exists")
                    };
            }

            var targets = await _targetRepository.SearchTargetPersonal(tenantId, currentUserId, currentUserInfo.OfficeIdPath, userId, keyword,
                startDate, endDate, targetTableId, status, listType);
            if (targets == null || !targets.Any() || targets.Count == 0)
                return new SearchResult<TargetTreeViewModel>
                {
                    Code = 1,
                    Items = null
                };

            var tree = RenderTree(targets, null);
            return new SearchResult<TargetTreeViewModel>
            {
                Code = 1,
                Items = tree
            };
        }

        public async Task<ActionResultResponse> DeleteParticipants(string tenantId, string id, string userId, string fullName, string avatar)
        {
            var info = await _targetParticipantRepository.GetInfo(tenantId, id, true);
            if (info == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("Target participants does not exist"));

            if (tenantId != info.TenantId)
                return new ActionResultResponse(-2, _taskResourceService.GetString(ErrorMessage.NotHavePermission));

            var targetInfo = await _targetRepository.GetInfo(tenantId, info.TargetId, true);
            if (targetInfo == null)
                return new ActionResultResponse(-3, _taskResourceService.GetString("Target does not exist"));

            if (info.UserId == targetInfo.UserId)
                return new ActionResultResponse(-4, _taskResourceService.GetString("User does not permission delete this user"));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, targetInfo, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var result = await _targetParticipantRepository.ForceDelete(tenantId, id);
            if (result < 0)
                return new ActionResultResponse(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TargetDeleteParticipants,
                FromValue = info.FullName,
                ObjectId = targetInfo.Id,
                ObjectType = ObjectType.Target,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.User
            });

            return new ActionResultResponse(1, _taskResourceService.GetString("Delete participants taget succesfully"));
        }

        public async Task<SearchResult<TargetTreeViewModel>> SearchSuggestion(string tenantId, string userId, string keyword, string targetTableId, TargetType? targetType, bool? isGetFinish)
        {
            var listData = await _targetRepository.SearchSuggestion(tenantId, userId, keyword, targetTableId, targetType, isGetFinish);
            return new SearchResult<TargetTreeViewModel>
            {
                Items = RenderTree(listData, null),
                Code = 1
            };
        }

        public async Task<ActionResultResponse> EquallyWeight(string tenantId, TargetType type, string targetGroupId, long? parentId)
        {
            var result = await _targetRepository.EquallyWeight(tenantId, type, targetGroupId, parentId);
            if (result >= 0)
            {
                if (parentId.HasValue)
                {
                    var targetParentInfo = await _targetRepository.GetInfo(tenantId, parentId.Value);
                    if (targetParentInfo != null)
                    {
                        await _targetRepository.GeneralPercentCompletedOfTarget(tenantId, targetParentInfo.ParentId, targetParentInfo.IdPath);
                    }
                }
            }

            return new ActionResultResponse(result, result < 0 ?
                _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong) : "Equally weight success");
        }

        public async Task<SearchResult<TargetTreeViewModel>> GetTargetChildTree(string tenantId, string userId, long id)
        {
            var targetInfo = await _targetRepository.GetInfo(tenantId, id);
            if (targetInfo == null)
                return new SearchResult<TargetTreeViewModel>
                {
                    Code = 1,
                    Items = null,
                };

            var listTargetChildTree = await _targetRepository.GetTargetChildById(tenantId, userId, targetInfo.IdPath);
            if (listTargetChildTree == null || !listTargetChildTree.Any())
            {
                return new SearchResult<TargetTreeViewModel>
                {
                    Code = 2,
                    Items = null,
                };
            }

            return new SearchResult<TargetTreeViewModel>
            {
                Code = 3,
                Items = RenderTree(listTargetChildTree, id)
            };
        }

        #region TargetAttachment
        public async Task<ActionResultResponse> DeleteAttachment(string tenantId, string userId, string fullName, string avatar,
            long targetId, string attachmentId)
        {
            var attachmentInfo = await _attachmentRepository.GetInfo(tenantId, attachmentId, true);
            if (attachmentInfo == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("Attachment doest not exists"));

            var targetInfo = await _targetRepository.GetInfo(tenantId, targetId, true);
            if (targetInfo == null)
                return new ActionResultResponse(-2, _taskResourceService.GetString("Target doest not exists"));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, targetInfo, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var resultDeleteAttachment = await _attachmentRepository.Delete(tenantId, attachmentId);
            if (resultDeleteAttachment < 0)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            return new ActionResultResponse(resultDeleteAttachment, _taskResourceService.GetString("delete attachment successfully"));
        }

        public async Task<ActionResultResponse> InsertAttachment(string tenantId, string userId, string fullName, string avatar, long targetId,
            AttachmentMeta attachmentMeta)
        {
            var targetInfo = await _targetRepository.GetInfo(tenantId, targetId, true);
            if (targetInfo == null)
                return new ActionResultResponse<string>(-1, _taskResourceService.GetString("Target does not exists"));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, targetInfo, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var isCheckExists = await _attachmentRepository.CheckExistsByFileIdObjectId(tenantId, targetId, ObjectType.Target, attachmentMeta.FileId);
            if (isCheckExists)
                return new ActionResultResponse<string>(-3, _taskResourceService.GetString("Attachment does exists in this target"));

            var attachmentId = Guid.NewGuid().ToString();
            var attachment = new Attachment
            {
                Id = attachmentId,
                ConcurrencyStamp = attachmentId,
                FileId = attachmentMeta.FileId,
                CreatorId = attachmentMeta.CreatorId,
                CreatorFullName = attachmentMeta.CreatorFullName,
                CreateTime = attachmentMeta.CreateTime,
                FileName = attachmentMeta.FileName,
                Type = attachmentMeta.Type,
                Url = attachmentMeta.Url,
                TenantId = tenantId,
                ObjectId = targetId,
                ObjectType = ObjectType.Target,
                Extension = attachmentMeta.Extension
            };

            var result = await _attachmentRepository.InsertAttachment(attachment);
            if (result <= 0)
                return new ActionResultResponse<string>(-1, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            return new ActionResultResponse<string>(result, _taskResourceService.GetString("Insert attachment successfully"), "", attachmentId);
        }

        public async Task<ActionResultResponse<List<Attachment>>> InsertAttachments(string tenantId, string userId, string fullName, string avatar,
            long targetId, List<AttachmentMeta> attachmentMeta)
        {
            var targetInfo = await _targetRepository.GetInfo(tenantId, targetId, true);
            if (targetInfo == null)
                return new ActionResultResponse<List<Attachment>>(-1, _taskResourceService.GetString("Target does not exists"));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, targetInfo, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new ActionResultResponse<List<Attachment>>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            List<Attachment> attachments = new List<Attachment>();
            foreach (var attachment in attachmentMeta)
            {
                var isCheckExists = await _attachmentRepository.CheckExistsByFileIdObjectId(tenantId, targetId, ObjectType.Target, attachment.FileId);
                if (!isCheckExists)
                {
                    var attachmentId = Guid.NewGuid().ToString();
                    var item = new Attachment
                    {
                        Id = attachmentId,
                        ConcurrencyStamp = attachmentId,
                        FileId = attachment.FileId,
                        CreatorId = attachment.CreatorId,
                        CreatorFullName = attachment.CreatorFullName,
                        CreateTime = attachment.CreateTime,
                        FileName = attachment.FileName,
                        Type = attachment.Type,
                        Extension = attachment.Extension,
                        Url = attachment.Url,
                        TenantId = tenantId,
                        ObjectId = targetId,
                        ObjectType = ObjectType.Target,
                    };
                    attachments.Add(item);
                }
            }

            var resultInsert = await _attachmentRepository.InsertAttachments(attachments);
            if (resultInsert <= 0)
                return new ActionResultResponse<List<Attachment>>(resultInsert, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            return new ActionResultResponse<List<Attachment>>(resultInsert, _taskResourceService.GetString("Insert attachment successfully"), "", attachments);
        }
        #endregion

        #region Log
        public async Task<SearchResult<Log>> SearchHistory(string tenantId, long targetId, string userId, int page, int pageSize)
        {
            var targetInfo = await _targetRepository.GetInfo(tenantId, targetId, true);
            if (targetInfo == null)
                return new SearchResult<Log>(-403, _taskResourceService.GetString("Target does not exists"));

            var isHasPermission = await _targetRepository.CheckPermission(tenantId, targetInfo, userId, TargetRole.WriteTarget);
            if (!isHasPermission)
                return new SearchResult<Log>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var logs = await _logRepository.Search(tenantId, ObjectType.Target, targetId, page, pageSize,
                out int totalRows);

            return new SearchResult<Log>(logs, totalRows);
        }
        #endregion

        private List<TargetTreeViewModel> RenderTree(List<TargetViewModel> targets, long? parentId)
        {
            var tree = new List<TargetTreeViewModel>();
            var parentTargets = new List<TargetViewModel>();

            if (!parentId.HasValue)
            {
                var listTargetId = targets.Select(x => x.Id);
                parentTargets = targets.Where(x => x.ParentId == parentId
                || (!parentId.HasValue && x.ParentId.HasValue && !listTargetId.Contains(x.ParentId.Value))).ToList();
            }
            else
            {
                parentTargets = targets.Where(x => x.ParentId == parentId).ToList();
            }
            if (parentTargets.Any())
            {
                parentTargets.ForEach(target =>
                {
                    var childCount = targets.Where(x => x.ParentId == target.Id).Count();
                    var treeData = new TargetTreeViewModel
                    {
                        Id = target.Id,
                        TargetTableId = target.TargetTableId,
                        TargetTableName = target.TargetTableName,
                        TargetTableOrder = target.TargetTableOrder,
                        TargetGroupId = target.TargetGroupId,
                        TargetGroupName = target.TargetGroupName,
                        TargetGroupColor = target.TargetGroupColor,
                        TargetGroupOrder = target.TargetGroupOrder,
                        Name = target.Name,
                        ParentId = target.ParentId,
                        IdPath = target.IdPath,
                        Data = target,
                        ChildCount = childCount,
                        State = new State(),
                        Children = RenderTree(targets, target.Id),
                    };
                    tree.Add(treeData);
                });
            }
            return tree;
        }

        #region
        public async Task<SearchResult<ReportTargetViewModel>> SearchForReport(string tenantId, DateTime? startDate, DateTime? endDate,
            TypeSearchTime typeSearchTime, string tableTargetId, TargetType typeTarget, TargetReportType targetReportType,
            int? officeId, string userId)
        {
            var result = await _targetRepository.SearchForReport(tenantId, startDate, endDate, typeSearchTime, tableTargetId, typeTarget,
                targetReportType, officeId, userId, out int totalRows);

            return new SearchResult<ReportTargetViewModel>
            {
                Code = 1,
                Items = result,
                TotalRows = totalRows
            };
        }

        public async Task<SearchResult<TargetViewModel>> SearchReportDetail(string tenantId, TargetFilterReport targetFilterReport, int page, int pageSize)
        {
            var result = await _targetRepository.SearchReportDetail(tenantId, targetFilterReport, page, pageSize, out int totaRows);

            return new SearchResult<TargetViewModel>
            {
                Code = 1,
                Items = result,
                TotalRows = totaRows
            };
        }
        #endregion
    }
}