﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.Services;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.Resources;
using GHM.Tasks.Domain.ViewModels;
using Microsoft.Extensions.Configuration;

namespace GHM.Tasks.Infrastructure.Services
{
    public class TasksParticipantsService : ITasksParticipantsService
    {
        private readonly ITasksParticipantRepository _tasksParticipantRepository;
        private readonly ITasksRepository _tasksRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmTaskResource> _taskResourceService;
        private readonly IConfiguration _configuration;
        private readonly IHttpClientService _httpClient;
        private readonly ITaskRoleGroupRepository _taskRoleGroupRepository;

        public TasksParticipantsService(ITasksParticipantRepository tasksParticipantRepository, IHttpClientService httpClientService,
            IResourceService<GhmTaskResource> taskResourceService, IResourceService<SharedResource> sharedResourceService,
            IConfiguration configuration, ITasksRepository tasksRepository, ITaskRoleGroupRepository taskRoleGroupRepository)
        {
            _tasksParticipantRepository = tasksParticipantRepository;
            _taskResourceService = taskResourceService;
            _sharedResourceService = sharedResourceService;
            _configuration = configuration;
            _tasksRepository = tasksRepository;
            _httpClient = httpClientService;
            _taskRoleGroupRepository = taskRoleGroupRepository;
        }

        public async Task<ActionResultResponse> Save(string tenantId, long taskId, List<TaskParticipantMeta> taskParticipantsMeta)
        {
            var taskInfo = await _tasksRepository.GetInfo(tenantId, taskId, true);
            if (taskInfo == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("Task does not exists."));

            var apiUrls = _configuration.GetApiUrl();
            var apiServiceInfo = _configuration.GetApiServiceInfo();
            if (apiUrls == null || apiServiceInfo == null)
                return new ActionResultResponse(-1, _sharedResourceService.GetString("Some configuration error. Please contact with administrator."));

            foreach (var item in taskParticipantsMeta)
            {
                var userInfo = await _httpClient.GetUserInfo(tenantId, item.UserId);
                if (userInfo == null)
                    return new ActionResultResponse(-2, _taskResourceService.GetString("Participant info does not exists."));

                var taskExist = await _tasksRepository.CheckExist(tenantId, taskId);
                if (!taskExist)
                    return new ActionResultResponse(-2, _taskResourceService.GetString("Task does not exist"));

                var taskParticipantsExist = await _tasksParticipantRepository.CheckExist(tenantId, taskId, userInfo.UserId);
                if (taskParticipantsExist)
                    return new ActionResultResponse(-2, _taskResourceService.GetString("task participants does exist"));

                var taskParticipants = new TaskParticipant
                {
                    Id = Guid.NewGuid().ToString(),
                    TenantId = tenantId,
                    Avatar = userInfo.Avatar,
                    UserId = userInfo.UserId,
                    FullName = userInfo.FullName,
                    OfficeId = userInfo.OfficeId,
                    OfficeIdPath = userInfo.OfficeIdPath,
                    OfficeName = userInfo.OfficeName,
                    PositionId = userInfo.PositionId,
                    PositionName = userInfo.PositionName,
                    TaskId = taskId
                };
                var resultSaveTaskParticipants = await _tasksParticipantRepository.Save(taskParticipants);
                if (resultSaveTaskParticipants < 0)
                    return new ActionResultResponse(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

                var taskRoleGroups = new List<TaskRoleGroup>();
                foreach (var roleGroup in item.RoleGroupIds)
                {
                    if (roleGroup.Id != "NGV" && roleGroup.Id != "NNV" && roleGroup.Id != "CVN")
                    {
                        var taskRoleGroup = new TaskRoleGroup
                        {
                            TenantId = tenantId,
                            RoleGroupId = roleGroup.Id,
                            TaskParticipantId = taskParticipants.Id
                        };
                        taskRoleGroups.Add(taskRoleGroup);
                    }
                }

                var resultSaveTaskRoleGroups = await _taskRoleGroupRepository.Insert(taskRoleGroups);
                if (resultSaveTaskRoleGroups < 0)
                {
                    await RollbackInsertTaskParticipants(item.UserId);
                    return new ActionResultResponse(-6, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
                }
            }

            return new ActionResultResponse(1, _taskResourceService.GetString("Insert task participants Successfully!"));

            async Task RollbackInsertTaskParticipants(string userId)
            {
                await _tasksParticipantRepository.Delete(tenantId, taskId, userId);
            }
        }

        public async Task<List<ParticipantViewModel>> GetsByTaskId(string tenantId, long taskId, string currentUserId)
        {
            var taskInfo = await _tasksRepository.GetInfo(tenantId, taskId);
            if (taskInfo == null)
                return null;

            var isParticipant = await _tasksParticipantRepository.CheckExists(taskId, currentUserId);
            if (taskInfo.CreatorId != currentUserId && taskInfo.ResponsibleId == currentUserId && !isParticipant)
                return null;

            return await _tasksParticipantRepository.GetsByTaskId(tenantId, taskId);
        }

        public async Task<ActionResultResponse> Delete(string tenantId, long taskId, string userId, string currentUserId)
        {
            var taskInfo = await _tasksParticipantRepository.GetInfo(tenantId, taskId, userId);
            if (taskInfo == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("Task participants not found."));

            var listRoleGroup = await _taskRoleGroupRepository.GetAllByParticipantId(tenantId, taskInfo.Id);
            if (listRoleGroup != null)
            {
                var deleteRoleGroup = await _taskRoleGroupRepository.Deletes(listRoleGroup);
                if (deleteRoleGroup < 0)
                    return new ActionResultResponse(-1, _taskResourceService.GetString("Can't Delete role group by participants"));
            }

            var result = await _tasksParticipantRepository.Delete(tenantId, taskId, userId);
            if (result <= 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator"));


            return new ActionResultResponse(result, _taskResourceService.GetString("Delete participant successful."));
        }

        public async Task<ActionResultResponse> Update(string tenantId, long taskId, string userId,
            TaskParticipantMeta taskParticipantMeta)
        {
            var taskParticipants = await _tasksParticipantRepository.GetInfo(tenantId, taskId, userId);
            if (taskParticipants == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("task participants does not exist"));

            var removeAllTaskRoleGroup = await _taskRoleGroupRepository.DeleteAllById(taskParticipants.Id, tenantId);
            if (removeAllTaskRoleGroup < 0)
                return new ActionResultResponse(-2, _taskResourceService.GetString("Dont remove role group"));

            foreach (var roleGroup in taskParticipantMeta.RoleGroupIds)
            {
                var taskRoleParticipantsExist = await _taskRoleGroupRepository.CheckExist(tenantId, taskParticipants.Id, roleGroup.Id);

                if (!taskRoleParticipantsExist)
                    await _taskRoleGroupRepository.Insert(taskParticipants.Id, roleGroup.Id, tenantId);
            }
            return new ActionResultResponse(1, _taskResourceService.GetString("Update task role group scueessfully"));
        }

        public async Task<List<TaskRoleParticipantsViewModel>> GetsAllByTaskId(string tenantId, long taskId)
        {
            return await _tasksParticipantRepository.GetsAllByTaskId(tenantId, taskId);
        }
    }
}
