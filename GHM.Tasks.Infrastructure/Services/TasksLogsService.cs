﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.Resources;

namespace GHM.Tasks.Infrastructure.Services
{
    public class TasksLogsService : ITasksLogsService
    {
        private readonly ITasksRepository _tasksRepository;
        private readonly ITasksParticipantRepository _tasksParticipantsRepository;
        private readonly ITasksLogRepository _tasksLogsRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmTaskResource> _taskResourceService;

        public TasksLogsService(ITasksRepository tasksRepository, ITasksLogRepository tasksLogsRepository, ITasksParticipantRepository tasksParticipantsRepository, IResourceService<SharedResource> sharedResourceService, IResourceService<GhmTaskResource> taskResourceService)
        {
            _tasksRepository = tasksRepository;
            _tasksLogsRepository = tasksLogsRepository;
            _tasksParticipantsRepository = tasksParticipantsRepository;
            _sharedResourceService = sharedResourceService;
            _taskResourceService = taskResourceService;
        }

        public async Task<SearchResult<TasksLogs>> GetsByTaskId(long taskId, string currentUserId, int page, int pageSize)
        {
            var taskInfo = await _tasksRepository.GetInfo(taskId);
            if (taskInfo == null)
                return new SearchResult<TasksLogs>
                {
                    Code = -1,
                    Message = _taskResourceService.GetString("Task not found.")
                };

            var isParticipant = await _tasksParticipantsRepository.CheckExists(taskId, currentUserId);
            if (taskInfo.CreatorId != currentUserId && taskInfo.ResponsibleId != currentUserId && !isParticipant)
                return new SearchResult<TasksLogs>
                {
                    Code = -2,
                    Message = _sharedResourceService.GetString("You do not have permission to do this action.")
                };

            var items = await _tasksLogsRepository.GetsByTaskId(taskId, page, pageSize, out var totalRows);
            return new SearchResult<TasksLogs>
            {
                Items = items,
                TotalRows = totalRows
            };
        }
    }
}
