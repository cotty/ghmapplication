﻿using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.Resources;
using System;
using System.Threading.Tasks;

namespace GHM.Tasks.Infrastructure.Services
{
    public class TasksLibraryService : ITasksLibraryService
    {
        private readonly ITasksLibraryRepository _tasksLibraryRepository;
        private readonly ITasksGroupLibraryRepository _tasksGroupLibraryRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmTaskResource> _resourceService;

        public TasksLibraryService(ITasksLibraryRepository tasksLibraryRepository, ITasksGroupLibraryRepository tasksGroupLibraryRepository,
            IResourceService<SharedResource> sharedResourceService, IResourceService<GhmTaskResource> resourceService)
        {
            _tasksGroupLibraryRepository = tasksGroupLibraryRepository;
            _tasksLibraryRepository = tasksLibraryRepository;
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
        }

        public async Task<ActionResultResponse> ForceDelete(string tenantId, string userId, string userFullName, long id)
        {
            var result = await _tasksLibraryRepository.ForceDelete(tenantId, userId, userFullName, id);

            if (result < 0)
                return new ActionResultResponse(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            return new ActionResultResponse(1, _resourceService.GetString("Delete tasks library successfully"));
        }

        public async Task<ActionResultResponse<TaskLibrary>> Insert(string tenantId, string userId, string userFullName,
            TasksLibraryMeta tasksLibraryMeta)
        {
            tasksLibraryMeta.Name = tasksLibraryMeta.Name?.Trim();
            var taskGroupLibraryInfo = await _tasksGroupLibraryRepository.GetInfo(tenantId, tasksLibraryMeta.TaskGroupLibraryId, true);
            if (taskGroupLibraryInfo == null)
                return new ActionResultResponse<TaskLibrary>(-1, _resourceService.GetString("Tasks group library does not exist"));

            var isExistsByName = await _tasksLibraryRepository.CheckExistsByName(tenantId, taskGroupLibraryInfo.Id, tasksLibraryMeta.Name);

            if (isExistsByName)
                return new ActionResultResponse<TaskLibrary>(-2, _resourceService.GetString("Tasks library does exist"));

            var concurrencyStamp = Guid.NewGuid().ToString();
            var taskLibrary = new TaskLibrary
            {
                TenantId = tenantId,
                Name = tasksLibraryMeta.Name,
                Description = tasksLibraryMeta.Description?.Trim(),
                MethodCalculateResult = tasksLibraryMeta.MethodCalculateResult,
                TaskGroupLibraryId = tasksLibraryMeta.TaskGroupLibraryId,
                CreatorId = userId,
                CreatorFullName = userFullName,
                ConcurrencyStamp = concurrencyStamp
            };

            var result = await _tasksLibraryRepository.Insert(taskLibrary);
            return new ActionResultResponse<TaskLibrary>(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _resourceService.GetString("Insert task library successfully"), "", taskLibrary);
        }

        public Task<SearchResult<TaskLibrary>> Search(string tenantId, string keyword, int page, int pageSize)
        {
            throw new NotImplementedException();
        }

        public async Task<ActionResultResponse<string>> Update(string tenantId, long id, string userId, string userFullName, TasksLibraryMeta tasksLibraryMeta)
        {
            var info = await _tasksLibraryRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Task library does not exist"));

            var taskGroupLibraryInfo = await _tasksGroupLibraryRepository.GetInfo(tenantId, tasksLibraryMeta.TaskGroupLibraryId, true);
            if (taskGroupLibraryInfo == null)
                return new ActionResultResponse<string>(-2, _resourceService.GetString("Tasks group library does not exist"));

            if (info.Name != tasksLibraryMeta.Name)
            {
                tasksLibraryMeta.Name = tasksLibraryMeta.Name?.Trim();
                var isExistsByName = await _tasksLibraryRepository.CheckExistsByName(tenantId, tasksLibraryMeta.TaskGroupLibraryId, tasksLibraryMeta.Name);
                if (isExistsByName)
                    return new ActionResultResponse<string>(-3, _resourceService.GetString("Name tasks group library does exist"));
            }

            if (info.TenantId != tenantId)
                return new ActionResultResponse<string>(-4, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (info.ConcurrencyStamp != tasksLibraryMeta.ConcurrencyStamp)
                return new ActionResultResponse<string>(-5, _resourceService.GetString(ErrorMessage.AlreadyUpdatedByAnother));

            info.Name = tasksLibraryMeta.Name;
            info.Description = tasksLibraryMeta.Description?.Trim();
            info.MethodCalculateResult = tasksLibraryMeta.MethodCalculateResult;
            info.LastUpdate = DateTime.Now;
            info.LastUpdateFullName = userFullName;
            info.LastUpdateUserId = userId;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();

            var result = await _tasksLibraryRepository.Update(tenantId, id, info);
            return new ActionResultResponse<string>(result, result < 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _resourceService.GetString("Update tasks group library successfully"), "", info.ConcurrencyStamp);
        }
    }
}
