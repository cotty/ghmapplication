﻿using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.Resources;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Tasks.Domain.ViewModels;
using GHM.Tasks.Domain.Constants;

namespace GHM.Tasks.Infrastructure.Services
{
    public class TaskRoleGroupService : IRoleGroupService
    {
        public IRoleGroupRepository _roleGroupRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmTaskResource> _resourceService;

        public TaskRoleGroupService(IRoleGroupRepository taskRoleGroupRepository, IResourceService<GhmTaskResource> resourceService,
            IResourceService<SharedResource> sharedResourceService)
        {
            _roleGroupRepository = taskRoleGroupRepository;
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
        }

        public async Task<ActionResultResponse> Delete(string tenantId, string roleGroupId)
        {
            var info = await _roleGroupRepository.GetInfoById(tenantId, roleGroupId);
            if (info == null)
                return new ActionResultResponse(-1, _resourceService.GetString("Role group does not exist"));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (roleGroupId.Trim().ToLower() == "ngv" || roleGroupId.Trim().ToLower() == "nnv" || roleGroupId.Trim().ToLower() == "cvn")
                return new ActionResultResponse(-3, _resourceService.GetString("You can not delete default role."));

            var result = await _roleGroupRepository.Delete(tenantId, roleGroupId);
            return new ActionResultResponse(result, result < 0 ? _resourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _resourceService.GetString("Delete Successfully"));
        }

        public async Task<List<RoleGroupViewModel>> SearchAll(string tenantId)
        {
            return await _roleGroupRepository.GetAll(tenantId);
        }

        public async Task<ActionResultResponse> Insert(string tenantId, string creatorId, string creatorFullName, RoleGroupMeta roleGroupMeta)
        {
            roleGroupMeta.Name = roleGroupMeta.Name?.Trim();
            var isRoleExist = await _roleGroupRepository.CheckExistsByName(tenantId, roleGroupMeta.Name);
            if (isRoleExist)
                return new ActionResultResponse(-1, _resourceService.GetString("Name role group already exist"));

            var roleGroup = new RoleGroup
            {
                TenantId = tenantId,
                Description = roleGroupMeta.Description?.Trim(),
                Role = roleGroupMeta.Role,
                Name = roleGroupMeta.Name,
                CreatorId = roleGroupMeta.CreatorId,
                CreatorFullName = roleGroupMeta.CreatorFullName
            };
            var result = await _roleGroupRepository.Insert(tenantId, roleGroup);
            return new ActionResultResponse(result, result < 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _resourceService.GetString("Insert Role Group Successfully"));
        }

        public async Task<SearchResult<RoleGroup>> Search(string tenantId, string creatorId, string fullName, string keyword, int page, int pageSize)
        {
            var result = await _roleGroupRepository.Search(tenantId, keyword, page, pageSize, out var totalRows);
            var isExistsRoleDefault = result.Exists(x => x.Id == "NNV");
            if (!isExistsRoleDefault)
            {
                var listRoleGroup = new List<RoleGroup>
                {
                        new RoleGroup
                        {
                            Id = "NNV",
                            TenantId = tenantId,
                            Name = "Người nhận việc",
                            Role = TaskRole.Read,
                            CreatorId = creatorId,
                            CreatorFullName = fullName,
                        },
                        new RoleGroup
                        {
                            Id = "NGV",
                            TenantId = tenantId,
                            Name = "Người giao việc",
                            Role = TaskRole.Read,
                             CreatorId = creatorId,
                            CreatorFullName = fullName,
                        },
                        new RoleGroup
                        {
                            Id = "CVN",
                            TenantId = tenantId,
                            Name = "Công việc nhóm",
                             CreatorId = creatorId,
                            CreatorFullName = fullName,
                            Role = TaskRole.Read
                        }
                };

                await _roleGroupRepository.Inserts(tenantId, listRoleGroup);
                result = await _roleGroupRepository.Search(tenantId, keyword, page, pageSize, out var totalRow1s);
                totalRows = totalRow1s;
            }

            return new SearchResult<RoleGroup>
            {
                TotalRows = totalRows,
                Items = result
            };
        }

        public async Task<ActionResultResponse> Update(string tenantId, string id, string lastUpdateUserId, string lastUpdateFullName,
            RoleGroupMeta roleGroupMeta)
        {
            var roleGroup = await _roleGroupRepository.GetInfoById(tenantId, id);
            if (roleGroup == null)
                return new ActionResultResponse(-1, _resourceService.GetString("Task role group does not exists"));

            if (roleGroup.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (roleGroup.ConcurrencyStamp != roleGroupMeta.ConcurrencyStamp)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.AlreadyUpdatedByAnother));

            if (roleGroup.Name != roleGroupMeta.Name)
            {
                roleGroupMeta.Name = roleGroupMeta.Name?.Trim();
                var isExistsByName = await _roleGroupRepository.CheckExistsByName(tenantId, roleGroupMeta.Name);
                if (isExistsByName)
                    return new ActionResultResponse(-1, _resourceService.GetString("Name role group already exist"));

                if (roleGroup.Id.Trim().ToLower() == "ngv" || roleGroup.Id.Trim().ToLower() == "nnv"
                    || roleGroup.Id.Trim().ToLower() == "cvn")
                    return new ActionResultResponse(-3, _resourceService.GetString("You can not update name default role."));
            }

            roleGroup.Description = roleGroupMeta.Description?.Trim();
            roleGroup.Name = roleGroupMeta.Name;
            roleGroup.Role = roleGroupMeta.Role;
            roleGroup.ConcurrencyStamp = Guid.NewGuid().ToString();

            var result = await _roleGroupRepository.Update(roleGroup);

            return new ActionResultResponse(result < 0 ? result : 1, result < 0 ?
                _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _resourceService.GetString("Update task role group successfully"));
        }
    }
}
