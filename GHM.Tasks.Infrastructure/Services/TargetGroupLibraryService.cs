﻿using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.Resources;
using GHM.Tasks.Domain.ViewModels;
using System;
using System.Threading.Tasks;

namespace GHM.Tasks.Infrastructure.Services
{
    public class TargetGroupLibraryService : ITargetGroupLibraryService
    {
        private readonly ITargetGroupLibraryRepository _targetGroupLibraryRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmTaskResource> _taskResourceService;

        public TargetGroupLibraryService(ITargetGroupLibraryRepository targetGroupLibraryRepository,
            IResourceService<GhmTaskResource> taskResorceService
            , IResourceService<SharedResource> sharedResourceService)
        {
            _targetGroupLibraryRepository = targetGroupLibraryRepository;
            _sharedResourceService = sharedResourceService;
            _taskResourceService = taskResorceService;
        }

        public async Task<ActionResultResponse> ForceDelete(string tenantId, string userId, string userFullName, string id)
        {
            var info = await _targetGroupLibraryRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("Target group library does not exists"));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var result = await _targetGroupLibraryRepository.ForceDelete(tenantId, userId, userFullName, id);
            if (result > 0)
                return new ActionResultResponse(1, _taskResourceService.GetString("Delete target group library successfully"));

            return new ActionResultResponse(-5, _sharedResourceService.GetString("Something went wrong. Please contact Adminstrator"));
        }

        public async Task<ActionResultResponse> Insert(string tenantId, string userId, string userFullName,
            TargetGroupLibraryMeta targetGroupLibraryMeta)
        {
            targetGroupLibraryMeta.Name = targetGroupLibraryMeta.Name?.Trim();
            var isExistByName = await _targetGroupLibraryRepository.CheckInfoByName(tenantId, targetGroupLibraryMeta.Name);
            if (isExistByName)
                return new ActionResultResponse(-1, _taskResourceService.GetString("Target group library does exist"));

            var info = new TargetGroupLibrary
            {
                Id = Guid.NewGuid().ToString(),
                TenantId = tenantId,
                CreatorFullName = userFullName,
                CreatorId = userId,
                LastUpdateFullName = userFullName,
                LastUpdateUserId = userId,
                Name = targetGroupLibraryMeta.Name
            };

            var result = await _targetGroupLibraryRepository.Insert(info);
            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                                           : _taskResourceService.GetString("Insert target group library successfully!"));
        }

        public Task<SearchResult<TargetGroupLibrary>> Search(string tenantId, string keyword, int page, int pageSize)
        {
            throw new NotImplementedException();
        }

        public async Task<ActionResultResponse> Update(string tenantId, string id, string userId, string userFullName,
            TargetGroupLibraryMeta targetGroupLibraryMeta)
        {
            var info = await _targetGroupLibraryRepository.GetInfo(tenantId, id);

            if (info.TenantId != tenantId)
                return new ActionResultResponse<string>(-1, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (info == null)
                return new ActionResultResponse(-2, _taskResourceService.GetString("target group library does not exist"));

            if (info.ConcurrencyStamp != targetGroupLibraryMeta.ConcurrencyStamp)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.AlreadyUpdatedByAnother));

            if (targetGroupLibraryMeta.Name != info.Name)
            {
                targetGroupLibraryMeta.Name = targetGroupLibraryMeta.Name?.Trim();
                var isExistByName = await _targetGroupLibraryRepository.CheckInfoByName(tenantId, targetGroupLibraryMeta.Name);
                if (isExistByName)
                    return new ActionResultResponse(-1, _taskResourceService.GetString("target group library does exist"));
            }

            info.LastUpdate = DateTime.Now;
            info.LastUpdateFullName = userFullName;
            info.LastUpdateUserId = userId;
            info.Name = targetGroupLibraryMeta.Name;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();

            var result = await _targetGroupLibraryRepository.Update(info);

            if (result < 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            return new ActionResultResponse(1, _taskResourceService.GetString("target group library update successfully"), info.Id);
        }

        public async Task<SearchResult<TargetGroupLibraryViewModel>> GetAllAndTargetLibrary(string tenantId)
        {
            var result = await _targetGroupLibraryRepository.GetAllAndTargetLibrary(tenantId);
            return new SearchResult<TargetGroupLibraryViewModel>
            {
                Items = result
            };
        }
    }
}
