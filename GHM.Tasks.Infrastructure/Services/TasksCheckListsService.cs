﻿using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.Resources;
using GHM.Tasks.Domain.ViewModels;
using System;
using System.Threading.Tasks;

namespace GHM.Tasks.Infrastructure.Services
{
    public class TasksCheckListsService : ITasksCheckListsService
    {
        public readonly ITasksCheckListsRepository _tasksCheckListsRepository;
        private readonly ITasksRepository _tasksRepository;
        private readonly ITasksLibraryRepository _tasksLibraryRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmTaskResource> _resourceService;
        public TasksCheckListsService(ITasksCheckListsRepository tasksCheckListsRepository,
            IResourceService<SharedResource> sharedResourceService, IResourceService<GhmTaskResource> resourceService,
            ITasksRepository tasksRepository, ITasksLibraryRepository tasksLibraryRepository)
        {
            _tasksCheckListsRepository = tasksCheckListsRepository;
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
            _tasksRepository = tasksRepository;
            _tasksLibraryRepository = tasksLibraryRepository;
        }

        public async Task<ActionResultResponse> ForceDelete(string tenantId, string userId, string userFullName, string id)
        {
            var info = await _tasksCheckListsRepository.GetInfo(tenantId, id);

            if (info == null)
                return new ActionResultResponse(-1, _resourceService.GetString("Check lists does not exist"));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-1, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var result = await _tasksCheckListsRepository.ForceDelete(tenantId, id);
            return new ActionResultResponse(result,
                result < 0
                    ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                    : _resourceService.GetString("Delete check list successfully "));
        }

        public async Task<ActionResultResponse<string>> Insert(string tenantId, string userId, string userFullName, CheckListsMeta checkList)
        {
            checkList.Name = checkList.Name?.Trim();
            if (checkList.Type == TaskType.TaskLibrary)
            {
                var taskLibraryInfo = await _tasksLibraryRepository.GetInfo(tenantId, checkList.TaskId, true);
                if (taskLibraryInfo == null)
                    return new ActionResultResponse<string>(-1, _resourceService.GetString("Task library does not exist"));
            }
            else
            {
                var taskInfo = await _tasksRepository.GetInfo(tenantId, checkList.TaskId, true);
                if (taskInfo == null)
                    return new ActionResultResponse<string>(-1, _resourceService.GetString("Task does not exist"));
            }

            var isExistsByName = await _tasksCheckListsRepository.CheckExistsByName(tenantId, checkList.TaskId, checkList.Type, checkList.Name);
            if (isExistsByName)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Checklist does exist"));

            var checkListId = Guid.NewGuid().ToString();
            var info = new CheckList
            {
                Id = checkListId,
                TenantId = tenantId,
                CreatorFullName = userFullName,
                CreatorId = userId,
                Name = checkList.Name,
                Type = checkList.Type,
                TaskId = checkList.TaskId,
                Status = checkList.Status,
                ConcurrencyStamp = checkListId,
            };

            var result = await _tasksCheckListsRepository.InsertAsync(info);
            return new ActionResultResponse<string>(result,
                result < 0
                    ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                    : _resourceService.GetString("Insert Check list successfully!"), "", checkListId);
        }

        public async Task<SearchResult<CheckListsViewModel>> SearchByTasksTypeId(string tenantId, long taskId, TaskType type)
        {
            var items = await _tasksCheckListsRepository.SearchByTasksTypeId(tenantId, taskId, type);
            return new SearchResult<CheckListsViewModel>
            {
                Items = items
            };
        }

        public async Task<ActionResultResponse<string>> Update(string tenantId, string id, string userId, string userFullName, CheckListsMeta checkList)
        {
            var info = await _tasksCheckListsRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("check list does not exist"));

            if (info.TenantId != tenantId)
                return new ActionResultResponse<string>(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (checkList.Type == TaskType.TaskLibrary)
            {
                var taskLibraryInfo = await _tasksLibraryRepository.GetInfo(tenantId, checkList.TaskId, true);
                if (taskLibraryInfo == null)
                    return new ActionResultResponse<string>(-1, _resourceService.GetString("Task library does not exist"));
            }
            else
            {
                var taskInfo = await _tasksRepository.GetInfo(tenantId, checkList.TaskId, true);
                if (taskInfo == null)
                    return new ActionResultResponse<string>(-1, _resourceService.GetString("Task does not exist"));
            }

            if (checkList.Name != info.Name)
            {
                checkList.Name = checkList.Name?.Trim();
                var isExistsByName = await _tasksCheckListsRepository.CheckExistsByName(tenantId, checkList.TaskId, checkList.Type, checkList.Name);
                if (isExistsByName)
                    return new ActionResultResponse<string>(-1, _resourceService.GetString("Checklist already exist"));
            }

            info.LastUpdateUserId = userId;
            info.LastUpdateFullName = userFullName;
            info.LastUpdate = DateTime.Now;
            info.Status = checkList.Status;
            info.Type = checkList.Type;
            info.Name = checkList.Name;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();

            var result = await _tasksCheckListsRepository.UpdateAsync(tenantId, id, info);
            return new ActionResultResponse<string>(result <= 0 ? result : 1, result < 0
                ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _resourceService.GetString("Update success"), "", info.ConcurrencyStamp);

        }
    }
}
