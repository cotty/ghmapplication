﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.Resources;
using GHM.Tasks.Domain.ViewModels;

namespace GHM.Tasks.Infrastructure.Services
{
    public class TargetTableService : ITargetTableService
    {
        public readonly ITargetTableRepository _targetTableRepository;
        public readonly ITargetGroupRepository _targetGroupRepository;
        public readonly ITargetRepository _targetRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmTaskResource> _resourceService;

        public TargetTableService(ITargetTableRepository targetTableRepository, ITargetGroupRepository targetGroupRepository,
            ITargetRepository targetRepository,
        IResourceService<SharedResource> sharedResourceService, IResourceService<GhmTaskResource> resourceService)
        {
            _targetTableRepository = targetTableRepository;
            _targetGroupRepository = targetGroupRepository;
            _targetRepository = targetRepository;
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
        }

        public async Task<ActionResultResponse> Delete(string id, string tenantId)
        {
            var targetTableInfo = await _targetTableRepository.GetInfo(tenantId, id);
            if (targetTableInfo == null)
                return new ActionResultResponse(-1, _resourceService.GetString("Target table not exists"));

            if (targetTableInfo.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var countByTargetTable = await _targetRepository.GetCountByTargetTable(tenantId, id);
            if (countByTargetTable > 0)
                return new ActionResultResponse(-3, _resourceService.GetString("Target table is using"));

            targetTableInfo.IsDelete = true;
            var result = await _targetTableRepository.Update(targetTableInfo);

            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                                           : _resourceService.GetString("Delete success"));
        }

        public async Task<ActionResultResponse<TargetTableDetailViewModel>> GetDetail(string tenantId, string id, string creatorId, 
            string cretorFullName)
        {
            var targetTableInfo = await _targetTableRepository.GetInfo(tenantId, id);
            if (targetTableInfo == null)
                return new ActionResultResponse<TargetTableDetailViewModel>(-1, _resourceService.GetString("Target table not exists"));

            if (targetTableInfo.TenantId != tenantId)
                return new ActionResultResponse<TargetTableDetailViewModel>(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var countTargetGroup = await _targetGroupRepository.CountByTargetTableId(tenantId, targetTableInfo.Id);
            if (countTargetGroup == 0)
            {
                if (targetTableInfo.Type == TargetTableType.Basic)
                {
                    var targetGroupId = Guid.NewGuid().ToString();
                    var targetGroup = new TargetGroup()
                    {
                        TenantId = tenantId,
                        TargetTableId = targetTableInfo.Id,
                        Name = "Mục tiêu",
                        Color = "#9BBB59",
                        IsActive = true,
                        Order = 1,
                        CreatorFullName = cretorFullName,
                        CreatorId = creatorId,
                        Id = targetGroupId,
                        ConcurrencyStamp = targetGroupId
                    };

                    await _targetGroupRepository.Insert(targetGroup);
                }
                else
                {
                    var listTargetGroup = new List<TargetGroup>();
                    for (int i = 1; i <= 4; i++)
                    {
                        var targetGroupId = Guid.NewGuid().ToString();
                        var targetGroup = new TargetGroup()
                        {
                            TenantId = tenantId,
                            TargetTableId = targetTableInfo.Id,
                            Name = i == 1 ? "Tài chính" : i == 2 ? "Khách hàng" : i == 3 ? "Quy trình nội bộ" : "Đào tạo và phát triển",
                            Color = i == 1 ? "#9BBB59" : i == 2 ? "#5CB37C" : i == 3 ? "#29AAE3" : "#8064A2",
                            IsActive = true,
                            Order = i,
                            CreatorFullName = cretorFullName,
                            CreatorId = creatorId,
                            Id = targetGroupId,
                            ConcurrencyStamp = targetGroupId
                        };

                        listTargetGroup.Add(targetGroup);
                    }

                    await _targetGroupRepository.Inserts(listTargetGroup);
                }
            }

            var listTableGroup = await _targetGroupRepository.Search(tenantId, targetTableInfo.Id);

            var targetTableDetail = new TargetTableDetailViewModel()
            {
                Id = targetTableInfo.Id,
                Name = targetTableInfo.Name,
                Description = targetTableInfo.Description,
                StartDate = targetTableInfo.StartDate,
                EndDate = targetTableInfo.EndDate,
                Status = targetTableInfo.Status,
                Order = targetTableInfo.Order,
                ConcurrencyStamp = targetTableInfo.ConcurrencyStamp,
                TargetGroups = listTableGroup
            };

            return new ActionResultResponse<TargetTableDetailViewModel>
            {
                Data = targetTableDetail,
            };
        }

        public async Task<ActionResultResponse<string>> Insert(string tenantId, string creatorId, string creatorFullName,
            TargetTableMeta targetTableMeta)
        {
            targetTableMeta.Name = targetTableMeta.Name?.Trim();
            var isExistsByName = await _targetTableRepository.CheckExistsName(tenantId, targetTableMeta.Name);
            if (isExistsByName)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Name already exist"));

            var targetTableId = Guid.NewGuid().ToString();
            var targetTable = new TargetTable()
            {
                Id = targetTableId,
                TenantId = tenantId,
                Name = targetTableMeta.Name,
                Description = targetTableMeta.Description?.Trim(),
                StartDate = targetTableMeta.StartDate,
                EndDate = targetTableMeta.EndDate,
                Type = targetTableMeta.Type,
                ConcurrencyStamp = targetTableId,
                CreatorId = creatorId,
                CreatorFullName = creatorFullName
            };

            var result = await _targetTableRepository.Insert(targetTable);
            return new ActionResultResponse<string>(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                                         : _resourceService.GetString("Insert success"), "", targetTableId);
        }

        public async Task<SearchResult<TargetTableSearchViewModel>> Search(string tenantId)
        {
            var items = await _targetTableRepository.Search(tenantId);
            return new SearchResult<TargetTableSearchViewModel>
            {
                Items = items,
            };
        }

        public async Task<ActionResultResponse<string>> Update(string id, string tenantId, string lastUpdateUserId, string lastUpdateFullName,
            TargetTableMeta targetTableMeta)
        {
            var targetTableInfo = await _targetTableRepository.GetInfo(tenantId, id);
            if (targetTableInfo == null)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Target table not exists"));

            if (targetTableInfo.TenantId != tenantId)
                return new ActionResultResponse<string>(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (targetTableInfo.ConcurrencyStamp != targetTableMeta.ConcurrencyStamp)
                return new ActionResultResponse<string>(-3, _sharedResourceService.GetString(ErrorMessage.AlreadyUpdatedByAnother));

            if (targetTableInfo.Name != targetTableMeta.Name)
            {
                targetTableMeta.Name = targetTableMeta.Name?.Trim();
                var isExistsName = await _targetTableRepository.CheckExistsName(tenantId, targetTableMeta.Name);
                if (isExistsName)
                    return new ActionResultResponse<string>(-4, _resourceService.GetString("Target table already exists"));
            }

            targetTableInfo.LastUpdate = DateTime.Now;
            targetTableInfo.LastUpdateUserId = lastUpdateUserId;
            targetTableInfo.LastUpdateFullName = lastUpdateFullName;
            targetTableInfo.ConcurrencyStamp = Guid.NewGuid().ToString();
            targetTableInfo.Name = targetTableMeta.Name;
            targetTableInfo.Description = targetTableMeta.Description?.Trim();
            targetTableInfo.StartDate = targetTableMeta.StartDate;
            targetTableInfo.EndDate = targetTableMeta.EndDate;

            var result = await _targetTableRepository.Update(targetTableInfo);
            return new ActionResultResponse<string>(result, result < 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                                        : _resourceService.GetString("Update success"), "", targetTableInfo.ConcurrencyStamp);
        }

        public async Task<ActionResultResponse> UpdateOrder(string tenantId, string id, int order)
        {
            var targetTableInfo = await _targetTableRepository.GetInfo(tenantId, id);
            if (targetTableInfo == null)
                return new ActionResultResponse(-1, _resourceService.GetString("Target table not exists"));

            if (targetTableInfo.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            targetTableInfo.Order = order;
            var result = await _targetTableRepository.Update(targetTableInfo);

            if (result > 0)
                await _targetTableRepository.SyncOrder(tenantId);

            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                                           : _resourceService.GetString("Update order success"));
        }

        public async Task<ActionResultResponse> UpdateStatus(string tenantId, string id, TargetTableStatus status)
        {
            var targetTableInfo = await _targetTableRepository.GetInfo(tenantId, id);
            if (targetTableInfo == null)
                return new ActionResultResponse(-1, _resourceService.GetString("Target table not exists"));

            if (targetTableInfo.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var countByActive = await _targetTableRepository.CountByActive(tenantId);
            if(countByActive == 1 && status != TargetTableStatus.Using)
                return new ActionResultResponse(-3, _resourceService.GetString("You have least one target table active."));

            targetTableInfo.Status = status;
            if (status != TargetTableStatus.Using)
                targetTableInfo.Order = null;
            else
            {               
                targetTableInfo.Order = countByActive + 1;
            }

            var result = await _targetTableRepository.Update(targetTableInfo);
            if (result > 0)
                await _targetTableRepository.SyncOrder(tenantId);

            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                                           : _resourceService.GetString("Update status success"));
        }

        public async Task<SearchResult<TargetTableForSuggestionViewModel>> GetForSuggestion(string tenantId)
        {
            var result = await _targetTableRepository.GetForSuggestion(tenantId);

            return new SearchResult<TargetTableForSuggestionViewModel>
            {
                Items = result
            };
        }
    }
}
