﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using GHM.Events;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.Services;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.Resources;
using GHM.Tasks.Domain.ViewModels;
using Microsoft.Extensions.Configuration;

namespace GHM.Tasks.Infrastructure.Services
{
    using State = Domain.ViewModels.State;
    using Tasks = Domain.Models.Tasks;
    public class TasksService : ITasksService
    {
        private readonly IConfiguration _configuration;
        private readonly ApiUrlSettings _apiUrls;
        private readonly ApiServiceInfo _apiServiceInfo;
        private readonly ITargetRepository _targetRepository;
        private readonly ITaskRoleGroupRepository _taskRoleGroupRepository;
        private readonly IRoleGroupRepository _roleGroupRepository;
        private readonly ILogRepository _logRepository;
        private readonly ITasksCheckListsRepository _taskCheckListsRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmTaskResource> _resourceService;
        private readonly ITasksRepository _tasksRepository;
        private readonly ITasksParticipantRepository _tasksParticipantsRepository;
        private readonly ITaskNotificationRepository _taskNotificationRepository;
        private readonly IAttachmentRepository _attachmentRepository;
        private readonly IHttpClientService _httpClient;

        public TasksService(ITasksRepository tasksRepository, IResourceService<GhmTaskResource> taskResourceService,
            IResourceService<SharedResource> sharedResourceService, IConfiguration configuration,
                        ITasksParticipantRepository tasksParticipantsRepository, ITargetRepository targetRepository,
            ITaskRoleGroupRepository taskRoleGroupRepository, IRoleGroupRepository roleGroupRepository, IHttpClientService httpClientService,
            ITaskNotificationRepository taskNotificationRepository, ILogRepository logRepository, ITasksCheckListsRepository taskCheckListsRepository, IAttachmentRepository attachmentRepository)
        {
            _tasksRepository = tasksRepository;
            _resourceService = taskResourceService;
            _sharedResourceService = sharedResourceService;
            _tasksParticipantsRepository = tasksParticipantsRepository;
            _targetRepository = targetRepository;
            _taskRoleGroupRepository = taskRoleGroupRepository;
            _roleGroupRepository = roleGroupRepository;
            _configuration = configuration;
            _taskNotificationRepository = taskNotificationRepository;
            _logRepository = logRepository;
            _taskCheckListsRepository = taskCheckListsRepository;

            _httpClient = httpClientService;
            _apiUrls = _configuration.GetApiUrl();
            _apiServiceInfo = _configuration.GetApiServiceInfo();
            _attachmentRepository = attachmentRepository;
        }

        public async Task<SearchResult<TasksSearchViewModel>> Search(TaskFilter taskFilter)
        {
            var items = await _tasksRepository.Search(taskFilter, out int totalRows);
            if (taskFilter.Type == TasksKind.Group)
            {
                var listTaskId = items.Select(x => x.Id);

                items = items.Where(x => x.ParentId == null || (x.ParentId.HasValue && !listTaskId.Contains(x.ParentId.Value))).ToList();
            }

            return new SearchResult<TasksSearchViewModel>(items, totalRows);
        }

        public async Task<ActionResultResponse> UpdateResponsible(string tenantId, long id, string responsibleId, string currentUserId, string fullName)
        {
            if (_apiUrls == null || _apiServiceInfo == null)
                return new ActionResultResponse(-1, _sharedResourceService.GetString("Some configuration error. Please contact with administrator."));

            var taskInfo = await _tasksRepository.GetInfo(tenantId, id);
            if (taskInfo == null)
                return new ActionResultResponse(-2, _resourceService.GetString("Task not found."));

            if (taskInfo.CreatorId != currentUserId && taskInfo.ResponsibleId == responsibleId && !taskInfo.IsAllowResponsibleUpdate)
                return new ActionResultResponse(-3, _resourceService.GetString("You do not have permission to update this task."));

            var userInfo = await _httpClient.GetUserInfo(tenantId, responsibleId);
            if (userInfo == null)
                return new ActionResultResponse(-4, _resourceService.GetString("Responsible info does not exists."));

            taskInfo.ResponsibleId = userInfo.UserId;
            taskInfo.ResponsibleFullName = userInfo.FullName;
            taskInfo.ResponsibleAvatar = userInfo.Avatar;
            taskInfo.ResponsiblePositionName = userInfo.PositionName;
            taskInfo.ResponsiblePositionId = userInfo.PositionId;
            taskInfo.ResponsibleOfficeId = userInfo.OfficeId;
            taskInfo.ResponsibleOfficeName = userInfo.OfficeName;
            taskInfo.ResponsibleOfficeIdPath = userInfo.OfficeIdPath;
            var result = await _tasksRepository.Update(taskInfo);
            return new ActionResultResponse(result, _resourceService.GetString("Change responsible successful."));
        }

        public async Task<ActionResultResponse> Delete(string tenantId, long id, string currentUserId, string fullName)
        {
            var taskInfo = await _tasksRepository.GetInfo(tenantId, id);
            if (taskInfo == null)
                return new ActionResultResponse(-1, _resourceService.GetString("Tasks not found."));

            if (taskInfo.Status == TasksStatus.Completed)
                return new ActionResultResponse(-2, _resourceService.GetString("Tasks is complete."));

            if (taskInfo.ChildCount > 0)
                return new ActionResultResponse(-3, _resourceService.GetString("Tasks have taskchild"));

            taskInfo.IsDelete = true;
            var result = await _tasksRepository.Update(taskInfo);
            if (result <= 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString("Something went wrong. Please contact with administrator."));

            // Delete subtask.
            var subTasks = await _tasksRepository.DeleteTaskChild(tenantId, taskInfo.Id);
            if (taskInfo.ParentId.HasValue)
            {
                await _tasksRepository.UpdateChildCount(tenantId, taskInfo.ParentId.Value);
                await _tasksRepository.GeneralPercentCompletedOfTask(tenantId, taskInfo.ParentId, taskInfo.IdPath);
            }

            if (!taskInfo.ParentId.HasValue)
                await UpdateTotalTaskInTarget(tenantId, taskInfo.TargetId);

            return new ActionResultResponse(result, result <= 0
                ? _sharedResourceService.GetString("Something went wrong. Please contact with administrator.")
                : _resourceService.GetString("Delete task sucecssful."));
            //            return new ActionResultResponse();
        }

        public async Task<ActionResultResponse<long>> Insert(TaskMeta taskMeta)
        {
            // Kiêm tra thời hạn.
            if (DateTime.Compare(taskMeta.EstimateStartDate, taskMeta.EstimateEndDate) > 0)
                return new ActionResultResponse<long>(-1, _resourceService.GetString("Start date must before end date"));

            if (taskMeta.Type == TasksKind.Personal && taskMeta.ParentId.HasValue)
                return new ActionResultResponse<long>(-1, _resourceService.GetString("Target personal does exits task parent."));

            if(taskMeta.Type == TasksKind.Group && taskMeta.Status == TasksStatus.PendingToFinish)
                return new ActionResultResponse<long>(-1, _resourceService.GetString("Bạn không thể chọn trạng thái hoàn thành cho công việc nhóm"));

            // Kiểm tra thông tin nhân viên.
            var responsibleInfo = await _httpClient.GetUserInfo(taskMeta.TenantId, taskMeta.ResponsibleId);
            if (responsibleInfo == null)
                return new ActionResultResponse<long>(-2,
                    _sharedResourceService.GetString(ErrorMessage.NotFound, _resourceService.GetString("Responsible info")));

            // Lấy về thông tin người tạo.
            var creatorInfo = await _httpClient.GetUserInfo(taskMeta.TenantId, taskMeta.UserId);
            if (creatorInfo == null)
                return new ActionResultResponse<long>(-3,
                    _sharedResourceService.GetString(ErrorMessage.NotFound, _resourceService.GetString("Creator info")));

            // Kiểm tra thông tin mục tiêu.
            var targetInfo =
                await _targetRepository.GetInfo(taskMeta.TenantId, taskMeta.TargetId, true);
            if (targetInfo == null)
                return new ActionResultResponse<long>(-4,
                    _sharedResourceService.GetString(ErrorMessage.NotFound, _resourceService.GetString("Target")));

            var isHasPermissionInTarget = await _targetRepository.CheckPermission(taskMeta.TenantId, targetInfo, taskMeta.UserId,
                TargetRole.WriteTask);
            if (!isHasPermissionInTarget)
                return new ActionResultResponse<long>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var task = new Domain.Models.Tasks
            {
                TenantId = taskMeta.TenantId,
                Name = taskMeta.Name?.Trim(),
                UnsignName = taskMeta.Name?.Trim().StripVietnameseChars(),
                CreatorId = taskMeta.UserId,
                CreatorFullName = taskMeta.FullName,
                CreatorOfficeId = creatorInfo.OfficeId,
                CreatorOfficeName = creatorInfo.OfficeName,
                CreatorOfficeIdPath = creatorInfo.OfficeIdPath,
                CreatorAvatar = creatorInfo.Avatar,
                CreatorPositionId = creatorInfo.PositionId,
                CreatorPositionName = creatorInfo.PositionName,
                ResponsibleId = taskMeta.ResponsibleId,
                ResponsibleFullName = responsibleInfo.FullName,
                ResponsiblePositionId = responsibleInfo.PositionId,
                ResponsiblePositionName = responsibleInfo.PositionName,
                ResponsibleOfficeId = responsibleInfo.OfficeId,
                ResponsibleOfficeName = responsibleInfo.OfficeName,
                ResponsibleOfficeIdPath = responsibleInfo.OfficeIdPath,
                ResponsibleAvatar = responsibleInfo.Avatar,
                EstimateStartDate = taskMeta.EstimateStartDate,
                EstimateEndDate = taskMeta.EstimateEndDate,
                Status = taskMeta.Status,
                EndDate = taskMeta.Status == TasksStatus.PendingToFinish ? taskMeta.EstimateEndDate : taskMeta.EndDate,
                TargetId = taskMeta.TargetId,
                Description = taskMeta.Description?.Trim(),
                ParentId = taskMeta.ParentId,
                Type = taskMeta.Type,
                MethodCalculateResult = taskMeta.Type == TasksKind.Group ? TaskMethodCalculateResult.TaskChild : TaskMethodCalculateResult.CheckList
            };
            if (taskMeta.ParentId.HasValue)
            {
                // Kiểm tra task có tồn tại không.
                var parentTaskInfo = await _tasksRepository.GetInfo(taskMeta.TenantId, taskMeta.ParentId.Value, true);
                if (parentTaskInfo == null)
                    return new ActionResultResponse<long>(-5,
                    _sharedResourceService.GetString(ErrorMessage.NotFound, _resourceService.GetString("Parent task")));

                // Kiểm tra quyền.
                if (parentTaskInfo.ResponsibleId != taskMeta.UserId &&
                    parentTaskInfo.CreatorId != taskMeta.UserId)
                {
                    // Lấy về vai trò của người dùng trong công việc.
                    var listTaskRoleGroup = await _taskRoleGroupRepository.GetDetail(taskMeta.TenantId,
                        taskMeta.ParentId.Value, taskMeta.UserId);
                    if (listTaskRoleGroup == null || !listTaskRoleGroup.Any())
                        return new ActionResultResponse<long>(-6, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

                    var isHasPermission = listTaskRoleGroup.Any(x =>
                        (x.Role & TaskRole.Full) == TaskRole.Full || (x.Role & TaskRole.Write) == TaskRole.Write);
                    if (!isHasPermission)
                        return new ActionResultResponse<long>(-7, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));
                }

                task.ParentId = parentTaskInfo.Id;
                task.IdPath = $"{parentTaskInfo.IdPath}.-1";
                task.TargetId = parentTaskInfo.TargetId;
            }

            var result = await _tasksRepository.Insert(task);
            if (result <= 0)
                return new ActionResultResponse<long>(-8, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            // Cập nhật lại đường dẫn IdPath.   
            if (taskMeta.ParentId.HasValue)
            {
                task.IdPath = task.IdPath.Replace("-1", task.Id.ToString());
            }
            else
            {
                task.IdPath = $"{task.Id.ToString()}";
            }
            await _tasksRepository.UpdateIdPath(task.TenantId, task.Id, task.IdPath.Replace("-1", task.Id.ToString()));

            if (taskMeta.ParentId.HasValue)
            {
                await _tasksRepository.UpdateChildCount(taskMeta.TenantId, taskMeta.ParentId.Value);
                await _tasksRepository.GeneralPercentCompletedOfTask(taskMeta.TenantId, taskMeta.ParentId, task.IdPath);
            }

            await AddTaskParticipants();

            if (taskMeta.Type == TasksKind.Group)
            {
                if (taskMeta.Participants != null && taskMeta.Participants.Any())
                {
                    foreach (var taskParticipantMeta in taskMeta.Participants)
                    {
                        var userInfo =
                            await _httpClient.GetUserInfo(taskMeta.TenantId, taskParticipantMeta.UserId);
                        if (userInfo == null)
                            continue;

                        await AddGroupTask(taskParticipantMeta, userInfo);
                    }
                }
            }

            if (taskMeta.Attachments != null && taskMeta.Attachments.Any())
            {
                await AddTaskAttachments();
            }

            if (!taskMeta.ParentId.HasValue)
                await UpdateTotalTaskInTarget(taskMeta.TenantId, taskMeta.TargetId);

            return new ActionResultResponse<long>
            {
                Code = result,
                Message = _sharedResourceService.GetString(SuccessMessage.AddSuccessful,
                    _resourceService.GetString("Task")),
                Data = task.Id
            };

            async Task AddTaskParticipants()
            {
                var listParticipant = new List<TaskParticipant>();
                // Thêm quyền người tạo.
                var creatorParticipantRole = await _roleGroupRepository.GetInfoById(task.TenantId, "NGV");
                if (creatorParticipantRole != null)
                {
                    listParticipant.Add(new TaskParticipant
                    {
                        TenantId = taskMeta.TenantId,
                        FullName = task.CreatorFullName,
                        Avatar = task.CreatorAvatar,
                        OfficeId = task.CreatorOfficeId,
                        OfficeIdPath = task.CreatorOfficeIdPath,
                        OfficeName = task.CreatorOfficeName,
                        TaskId = task.Id,
                        PositionId = task.CreatorPositionId,
                        PositionName = task.CreatorPositionName,
                        UserId = task.CreatorId,
                        TaskRoleGroup = new List<TaskRoleGroup>
                        {
                            new TaskRoleGroup
                            {
                                TenantId = task.TenantId,
                                RoleGroupId = creatorParticipantRole.Id
                            }
                        }
                    });
                }

                // Thêm quyền người chịu trách nhiệm.
                var responsibleParticipantRole = await _roleGroupRepository.GetInfoById(task.TenantId, "NNV");
                if (responsibleParticipantRole != null)
                {
                    listParticipant.Add(new TaskParticipant
                    {
                        TenantId = taskMeta.TenantId,
                        FullName = task.ResponsibleFullName,
                        Avatar = task.ResponsibleAvatar,
                        OfficeId = task.ResponsibleOfficeId,
                        OfficeIdPath = task.ResponsibleOfficeIdPath,
                        OfficeName = task.ResponsibleOfficeName,
                        TaskId = task.Id,
                        PositionId = task.ResponsiblePositionId,
                        PositionName = task.ResponsiblePositionName,
                        UserId = task.ResponsibleId,
                        TaskRoleGroup = new List<TaskRoleGroup>
                        {
                            new TaskRoleGroup
                            {
                                TenantId = task.TenantId,
                                RoleGroupId = responsibleParticipantRole.Id
                            }
                        }
                    });
                }

                if (taskMeta.Participants != null && taskMeta.Participants.Any())
                {
                    // Check nếu là công việc nhóm
                    var targetGroupParticipantRole = await _roleGroupRepository.GetInfoById(task.TenantId, "CVN");
                    foreach (var taskParticipantMeta in taskMeta.Participants)
                    {
                        var userInfo = await _httpClient.GetUserInfo(taskMeta.TenantId, taskParticipantMeta.UserId);
                        if (userInfo == null)
                            continue;

                        var taskParticipant = new TaskParticipant
                        {
                            TenantId = taskMeta.TenantId,
                            FullName = userInfo.FullName,
                            Avatar = userInfo.Avatar,
                            OfficeId = userInfo.OfficeId,
                            OfficeIdPath = userInfo.OfficeIdPath,
                            OfficeName = userInfo.OfficeName,
                            TaskId = task.Id,
                            PositionId = userInfo.PositionId,
                            PositionName = userInfo.PositionName,
                            UserId = userInfo.UserId
                        };
                        taskParticipant.TaskRoleGroup = taskMeta.Type == TasksKind.Personal ?
                            taskParticipantMeta.RoleGroupIds.Where(x => x != null)
                            .Select(x => new TaskRoleGroup
                            {
                                TenantId = task.TenantId,
                                TaskParticipantId = taskParticipant.Id,
                                RoleGroupId = x.Id
                            }).ToList() : new List<TaskRoleGroup>
                            {
                                new TaskRoleGroup
                                {
                                    TenantId = task.TenantId,
                                    RoleGroupId = targetGroupParticipantRole.Id
                                }
                            };
                        listParticipant.Add(taskParticipant);
                    }
                }

                var resultInsertPaticipant = await _tasksParticipantsRepository.Inserts(listParticipant);
                if (resultInsertPaticipant <= 0)
                {
                    await RollbackInsertTaskChild(task.Id);
                    await RollbackInsertTask(task.Id);
                }
            }

            async Task AddTaskAttachments()
            {
                List<Attachment> taskAttachment = new List<Attachment>();

                foreach (var attachment in taskMeta.Attachments)
                {
                    var isExistsAttachment = await _attachmentRepository.CheckExistsByFileIdObjectId(taskMeta.TenantId, task.Id,
                        ObjectType.Task, attachment.FileId);
                    if (!isExistsAttachment)
                    {
                        var attachmentId = Guid.NewGuid().ToString();
                        var item = new Attachment
                        {
                            Id = attachmentId,
                            FileId = attachment.FileId,
                            FileName = attachment.FileName,
                            Url = attachment.Url,
                            ObjectType = ObjectType.Task,
                            ObjectId = task.Id,
                            TenantId = taskMeta.TenantId,
                            CreateTime = attachment.CreateTime,
                            CreatorId = attachment.CreatorId,
                            Type = attachment.Type,
                            Extension = attachment.Extension,
                            CreatorFullName = attachment.CreatorFullName,
                            ConcurrencyStamp = attachmentId,
                        };
                        taskAttachment.Add(item);
                    }
                }

                var resultAttachment = await _attachmentRepository.InsertAttachments(taskAttachment);
                if (resultAttachment <= 0)
                {
                    await RollbackInsertTaskParicipants(task.Id);
                    await RollbackInsertTaskChild(task.Id);
                    await RollbackInsertTask(task.Id);
                }
            }

            // Thêm tự động công việc con cho các thành viên trong nhóm
            async Task AddGroupTask(TaskParticipantMeta taskParticipantMeta, ShortUserInfoViewModel userInfo)
            {
                var taskGroup = new Domain.Models.Tasks
                {
                    TenantId = taskMeta.TenantId,
                    Name = taskMeta.Name?.Trim() + "_" + userInfo.FullName,
                    UnsignName = (taskMeta.Name?.Trim() + "_" + userInfo.FullName).StripVietnameseChars(),
                    CreatorId = taskMeta.UserId,
                    CreatorFullName = taskMeta.FullName,
                    CreatorOfficeId = creatorInfo.OfficeId,
                    CreatorOfficeName = creatorInfo.OfficeName,
                    CreatorOfficeIdPath = creatorInfo.OfficeIdPath,
                    CreatorAvatar = creatorInfo.Avatar,
                    CreatorPositionId = creatorInfo.TitleId,
                    CreatorPositionName = creatorInfo.TitleName,
                    ResponsibleId = userInfo.UserId,
                    ResponsibleFullName = userInfo.FullName,
                    ResponsiblePositionId = userInfo.PositionId,
                    ResponsiblePositionName = userInfo.PositionName,
                    ResponsibleOfficeId = userInfo.OfficeId,
                    ResponsibleOfficeName = userInfo.OfficeName,
                    ResponsibleOfficeIdPath = userInfo.OfficeIdPath,
                    ResponsibleAvatar = userInfo.Avatar,
                    EstimateStartDate = taskMeta.EstimateStartDate,
                    EstimateEndDate = taskMeta.EstimateEndDate,
                    Status = taskMeta.Status,
                    TargetId = taskMeta.TargetId,
                    Description = taskMeta.Description?.Trim(),
                    ParentId = task.Id,
                    Type = taskMeta.Type,
                    IdPath = $"{task.IdPath}.-1"
                };

                var resultInsertTaskGroup = await _tasksRepository.Insert(taskGroup);
                if (resultInsertTaskGroup <= 0)
                {
                    await RollbackInsertTaskParicipants(task.Id);
                    await RollbackInsertTaskChild(task.Id);
                    await RollbackInsertTask(task.Id);
                }

                await _tasksRepository.UpdateIdPath(taskGroup.TenantId, taskGroup.Id, taskGroup.IdPath.Replace("-1", taskGroup.Id.ToString()));
                await _tasksRepository.UpdateChildCount(taskMeta.TenantId, task.Id);

                var listParticipant = new List<TaskParticipant>();
                // Thêm quyền người tạo.
                var creatorParticipantRole = await _roleGroupRepository.GetInfoById(task.TenantId, "NGV");
                if (creatorParticipantRole != null)
                {
                    listParticipant.Add(new TaskParticipant
                    {
                        TenantId = taskMeta.TenantId,
                        FullName = taskGroup.CreatorFullName,
                        Avatar = taskGroup.CreatorAvatar,
                        OfficeId = taskGroup.CreatorOfficeId,
                        OfficeIdPath = taskGroup.CreatorOfficeIdPath,
                        OfficeName = taskGroup.CreatorOfficeName,
                        TaskId = taskGroup.Id,
                        PositionId = taskGroup.CreatorPositionId,
                        PositionName = taskGroup.CreatorPositionName,
                        UserId = taskGroup.CreatorId,
                        TaskRoleGroup = new List<TaskRoleGroup>
                        {
                            new TaskRoleGroup
                            {
                                TenantId = task.TenantId,
                                RoleGroupId = creatorParticipantRole.Id
                            }
                        }
                    });
                }

                // Thêm quyền người chịu trách nhiệm.
                var responsibleParticipantRole = await _roleGroupRepository.GetInfoById(task.TenantId, "NNV");
                if (responsibleParticipantRole != null)
                {
                    listParticipant.Add(new TaskParticipant
                    {
                        TenantId = taskMeta.TenantId,
                        FullName = taskGroup.ResponsibleFullName,
                        Avatar = taskGroup.ResponsibleAvatar,
                        OfficeId = taskGroup.ResponsibleOfficeId,
                        OfficeIdPath = taskGroup.ResponsibleOfficeIdPath,
                        OfficeName = taskGroup.ResponsibleOfficeName,
                        TaskId = taskGroup.Id,
                        PositionId = taskGroup.ResponsiblePositionId,
                        PositionName = taskGroup.ResponsiblePositionName,
                        UserId = taskGroup.ResponsibleId,
                        TaskRoleGroup = new List<TaskRoleGroup>
                        {
                            new TaskRoleGroup
                            {
                                TenantId = taskGroup.TenantId,
                                RoleGroupId = responsibleParticipantRole.Id
                            }
                        }
                    });
                }

                var resultInsertPaticipantGroup = await _tasksParticipantsRepository.Inserts(listParticipant);
                if (resultInsertPaticipantGroup <= 0)
                {
                    await RollbackInsertTask(taskGroup.Id);
                    await RollbackInsertTaskChild(task.Id);
                    await RollbackInsertTask(task.Id);
                }
            }

            async Task RollbackInsertTask(long taskId)
            {
                await _tasksRepository.Delete(taskMeta.TenantId, taskId);
            }

            async Task RollbackInsertTaskParicipants(long taskId)
            {
                await _tasksParticipantsRepository.FoceDeleteByTaskId(taskMeta.TenantId, taskId);
            }

            async Task RollbackInsertTaskChild(long taskId)
            {
                await _tasksRepository.DeleteTaskChild(taskMeta.TenantId, taskId);
            }
        }

        // Duyet cong viec
        public async Task<ActionResultResponse<string>> UpdateStatus(string tenantId, string userId, string fullName, string avatar,
            TaskStatusMeta task)
        {
            var taskInfo = await _tasksRepository.GetInfo(tenantId, task.TaskId);
            if (taskInfo == null)
                return new ActionResultResponse<string>(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound, _resourceService.GetString("Task info")));

            var directManagerInfo = await _httpClient.GetAsync<ShortUserInfoViewModel>($"{_apiUrls.HrApiUrl}/users/{tenantId}/direct-manager/{taskInfo.ResponsibleId}");
            if (directManagerInfo == null || userId != directManagerInfo.UserId)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (taskInfo.ConcurrencyStamp != task.ConcurrencyStamp)
                return new ActionResultResponse<string>(-2,
                    _sharedResourceService.GetString(ErrorMessage.AlreadyUpdatedByAnother,
                        _resourceService.GetString("Task")));

            var oldStatus = taskInfo.Status;

            taskInfo.Status = task.Status;
            taskInfo.LastUpdate = DateTime.Now;
            taskInfo.LastUpdateUserId = userId;
            taskInfo.LastUpdateFullName = fullName;
            taskInfo.ConcurrencyStamp = Guid.NewGuid().ToString();
            if (taskInfo.Status == TasksStatus.DeclineFinish)
                taskInfo.EndDate = null;

            if (taskInfo.Status == TasksStatus.Completed)
                taskInfo.ApproveTime = DateTime.Now;

            var result = await _tasksRepository.Update(taskInfo);
            if (result <= 0)
                return new ActionResultResponse<string>(result, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            await _tasksRepository.GeneralPercentCompletedOfTask(tenantId, taskInfo.ParentId, taskInfo.IdPath);

            if (!taskInfo.ParentId.HasValue)
                await UpdateTotalTaskInTarget(tenantId, taskInfo.TargetId);

            await InsertTaskStatusLog(tenantId, taskInfo.Id, userId, fullName, avatar, oldStatus, task.Status);
            return new ActionResultResponse<string>(result, _sharedResourceService.GetString(SuccessMessage.UpdateSuccessful,
                        _resourceService.GetString("Task info")), "", taskInfo.ConcurrencyStamp);
        }

        public async Task<ActionResultResponse<string>> UpdateDeadline(string tenantId, string userId, string fullName, long taskId,
            string concurrencyStamp, DateTime startDate, DateTime endDate)
        {
            var taskInfo = await _tasksRepository.GetInfo(tenantId, taskId);
            if (taskInfo == null)
                return new ActionResultResponse<string>(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound, _resourceService.GetString("Task info")));

            var isHasPermission = await CheckPermission(tenantId, taskId, userId);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (taskInfo.ConcurrencyStamp != concurrencyStamp)
                return new ActionResultResponse<string>(-2,
                    _sharedResourceService.GetString(ErrorMessage.AlreadyUpdatedByAnother,
                        _resourceService.GetString("Task")));

            taskInfo.EstimateStartDate = startDate;
            taskInfo.EstimateEndDate = endDate;
            taskInfo.LastUpdateUserId = userId;
            taskInfo.LastUpdateFullName = fullName;
            var result = await _tasksRepository.Update(taskInfo);
            return new ActionResultResponse<string>(result, result < 0
                ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : result == 0
                    ? _sharedResourceService.GetString(ErrorMessage.NothingChanges)
                    : _sharedResourceService.GetString(SuccessMessage.UpdateSuccessful,
                        _resourceService.GetString("Task info")));
        }

        public async Task<ActionResultResponse<TaskDetailViewModel>> GetDetail(string tenantId, long taskId, string userId,
            string fullName, string avatar)
        {
            var currentUserInfo = await _httpClient.GetUserInfo(tenantId, userId);
            if (currentUserInfo == null)
                return new ActionResultResponse<TaskDetailViewModel>(-403,
                   _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var taskInfo = await _tasksRepository.GetDetail(tenantId, taskId);
            if (taskInfo == null)
                return new ActionResultResponse<TaskDetailViewModel>(403,
                    _sharedResourceService.GetString(ErrorMessage.NotFound, _resourceService.GetString("Task info")));

            var targetInfo = await _targetRepository.GetInfo(tenantId, taskInfo.TargetId, true);
            if (targetInfo == null)
                return new ActionResultResponse<TaskDetailViewModel>(403,
                    _sharedResourceService.GetString(ErrorMessage.NotFound, _resourceService.GetString("Target info")));

            var isHasPermission = await CheckPermission(tenantId, taskId, userId, TaskRole.Read);
            var directManagerInfo = await _httpClient.GetAsync<ShortUserInfoViewModel>($"{_apiUrls.HrApiUrl}/users/{tenantId}/direct-manager/{taskInfo.ResponsibleId}");

            if (!isHasPermission && !(currentUserInfo.UserType == (int)UserType.Leader && (taskInfo.ReponsibleOfficeIdPath.StartsWith(currentUserInfo.OfficeIdPath + '.')
                || taskInfo.ReponsibleOfficeIdPath == currentUserInfo.OfficeIdPath) || (directManagerInfo != null && directManagerInfo.UserId == userId)))
                return new ActionResultResponse<TaskDetailViewModel>(-20,
                    _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            // await InsertTaskViewDetailLog(tenantId, taskId, userId, fullName, avatar);
            taskInfo.Attachments = await _attachmentRepository.GetsAllByObjectId(tenantId, ObjectType.Task, taskId, 1, 20, out int totalAttachment);
            taskInfo.Checklists = await _taskCheckListsRepository.SearchByTasksTypeId(tenantId, taskId, TaskType.Task);
            taskInfo.Participants = await _tasksParticipantsRepository.GetsAllByTaskId(tenantId, taskId);
            taskInfo.RoleGroups = await _roleGroupRepository.Search(tenantId, string.Empty, 1, int.MaxValue, out var totalRoleGroup);
            taskInfo.IsAllowApprove = false;
            taskInfo.TargetName = targetInfo.Name;

            if (taskInfo.Type == TasksKind.Group && taskInfo.ChildCount > 0)
            {
                var listTaskChild = await _tasksRepository.GetTaskChilByTaskId(tenantId, taskInfo.IdPath);
                taskInfo.TaskChild = listTaskChild != null && listTaskChild.Any() ? RenderTree(listTaskChild, taskId) : null;
            }

            if (userId != taskInfo.ResponsibleId)
            {
                taskInfo.IsAllowApprove = (directManagerInfo != null && directManagerInfo.UserId == userId) || (directManagerInfo == null && userId == targetInfo.CreatorId);
            }

            return new ActionResultResponse<TaskDetailViewModel>(taskInfo);
        }

        private async Task InsertTaskViewDetailLog(string tenantId, long taskId, string userId, string fullName, string avatar)
        {
            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TaskViewDetail,
                ObjectId = taskId,
                ObjectType = ObjectType.Task,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Value
            });
        }

        private async Task<bool> CheckPermission(string tenantId, long taskId, string userId, TaskRole role = TaskRole.Write)
        {
            var taskInfo = await _tasksRepository.GetDetail(tenantId, taskId);
            if (taskInfo.CreatorId == userId)
                return true;

            var taskRoleGroup = await _taskRoleGroupRepository.GetDetail(tenantId, taskId, userId);
            return taskRoleGroup != null && taskRoleGroup.Any(x => x.Role == TaskRole.Full || ((int)x.Role & (int)role) == (int)role);
        }

        public async Task<ActionResultResponse> UpdateNotification(long id, string tenantId,
            string userId, string userName, string avatar, TaskNotificationMeta taskNotificationMeta)
        {
            var apiUrls = _configuration.GetApiUrl();
            if(apiUrls == null)
                return new ActionResultResponse<string>(-6, _sharedResourceService.GetString("Missing some configuaration. Please contact with adminstrator"));

            var listUsers = await _tasksParticipantsRepository.GetsAllByTaskId(tenantId, id);

            if (listUsers == null)
                return new ActionResultResponse<string>(-2, _resourceService.GetString("Task does not participants"));

            var checkTask = await _tasksRepository.CheckExist(tenantId, id);
            if (!checkTask)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Task does not exist"));

            var scheduleJobMeta = new ScheduleJobMeta
            {
                Content = $"{taskNotificationMeta.TasksName}",
                JobType = JobType.Task,
                Date = taskNotificationMeta.Dates,
                Hour = taskNotificationMeta.Hours,
                Minutes = taskNotificationMeta.Minutes,
                JobTypeId = id.ToString(),
                ScheduleType = taskNotificationMeta.ScheduleType,
                Notifications = listUsers.Select(x => new NotificationModel
                {
                    Id = Guid.NewGuid().ToString(),
                    TenantId = tenantId,
                    Title = $"<b>{userName}</b> send you a notification about task.",
                    Content = $"{taskNotificationMeta.TasksName}",
                    SenderId = userId,
                    SenderFullName = userName,
                    SenderAvatar = avatar,
                    Url = $"tasks/{id}",
                    ReceiverId = x.UserId,
                    IsSend = false
                }).ToList()
        };

            var scheduleJob = await _httpClient.GetAsync<ActionResultResponse<ScheduleJob>>($"{apiUrls.NotificationApiUrl}schedule-job/detail/{JobType.Task}/{tenantId}/{id}");
            var result = new ActionResultResponse();
            if (scheduleJob.Data == null)
            {
                result =  await _httpClient.PostAsync<ActionResultResponse>($"{apiUrls.NotificationApiUrl}schedule-job/insert", scheduleJobMeta);
            } else
            {
                result = await _httpClient.PostAsync<ActionResultResponse>($"{apiUrls.NotificationApiUrl}schedule-job/update", scheduleJobMeta);
            }

            return result.Code < 0 ? new ActionResultResponse(-5, _resourceService.GetString("insert schedule job have some thing went wrong"))
                : new ActionResultResponse(1, _resourceService.GetString("Insert schedule job sucessfully"));
        
            async Task<bool> ValidateTime(string dateInString)
            {
                if (DateTime.TryParse(dateInString, out DateTime temp) && temp > DateTime.MinValue)
                {
                    return true;
                }
                return false;
            }
        }

        public async Task<ActionResultResponse<string>> Update(string tenantId, long taskId, TaskMeta taskMeta)
        {
            var taskInfo = await _tasksRepository.GetInfo(tenantId, taskId);
            if (taskInfo == null)
                return new ActionResultResponse<string>(-1, _sharedResourceService.GetString(ErrorMessage.NotFound, _resourceService.GetString("Task info")));

            var isHasPermission = await CheckPermission(tenantId, taskId, taskMeta.UserId);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (taskInfo.ConcurrencyStamp != taskMeta.ConcurrencyStamp)
                return new ActionResultResponse<string>(-2,
                    _sharedResourceService.GetString(ErrorMessage.AlreadyUpdatedByAnother));

            var oldTaskName = taskInfo.Name;
            var oldTaskStatus = taskInfo.Status;
            var oldStartDate = taskInfo.EstimateStartDate;
            var oldEndDate = taskInfo.EstimateEndDate;
            var oldDescription = taskInfo.Description;
            var oldPercentCompleted = taskInfo.PercentCompleted;
            var oldMethodCaculateResult = taskInfo.MethodCalculateResult;

            taskInfo.Name = taskMeta.Name?.Trim();
            taskInfo.UnsignName = taskInfo.Name.StripVietnameseChars().ToUpper();
            taskInfo.LastUpdate = DateTime.Now;
            taskInfo.LastUpdateFullName = taskMeta.FullName;
            taskInfo.LastUpdateUserId = taskMeta.UserId;
            taskInfo.Status = taskMeta.Status;

            if (taskInfo.Status == TasksStatus.PendingToFinish)
            {
                if (taskMeta.EndDate.HasValue)
                {
                    if (DateTime.Compare(taskMeta.EndDate.Value, DateTime.Now) > 0)
                        return new ActionResultResponse<string>(-10, _resourceService.GetString("End date must before this time"));
                }

                taskInfo.EndDate = taskMeta.EndDate ?? DateTime.Now;
            }

            if (taskInfo.Status == TasksStatus.InProgress)
            {
                taskInfo.StartDate = DateTime.Now;
                taskInfo.EndDate = null;
            }

            if (taskInfo.Status == TasksStatus.NotStartYet)
            {
                taskInfo.StartDate = null;
                taskInfo.EndDate = null;
            }

            taskInfo.Description = taskMeta.Description?.Trim();
            taskInfo.EstimateStartDate = taskMeta.EstimateStartDate;
            taskInfo.EstimateEndDate = taskMeta.EstimateEndDate;
            taskInfo.MethodCalculateResult = taskMeta.MethodCalculatorResult;
            taskInfo.PercentCompleted = taskMeta.PercentCompleted;
            taskInfo.ConcurrencyStamp = Guid.NewGuid().ToString();

            var result = await _tasksRepository.Update(taskInfo);
            if (result > 0)
            {
                // Cập nhật lại logs.
                await UpdateTaskLog();
                if (oldTaskStatus != taskInfo.Status && taskInfo.ParentId.HasValue)
                    await _tasksRepository.GeneralPercentCompletedOfTask(tenantId, taskInfo.ParentId, taskInfo.IdPath);

                if (taskInfo.Type == TasksKind.Group && taskInfo.ParentId.HasValue && taskInfo.Status == TasksStatus.InProgress)
                {
                    var taskParentInfo = await _tasksRepository.GetInfo(tenantId, taskInfo.ParentId.Value);
                    if (taskParentInfo != null && taskParentInfo.Status == TasksStatus.NotStartYet)
                    {
                        taskParentInfo.Status = TasksStatus.InProgress;
                        await _tasksRepository.Update(taskParentInfo);
                    }
                }
                //await UpdateTotalTaskInTarget(taskInfo.TenantId, taskInfo.TargetId);
            }

            return new ActionResultResponse<string>(result, result < 0
                ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : result == 0
                    ? _sharedResourceService.GetString(ErrorMessage.NothingChanges)
                    : _sharedResourceService.GetString(SuccessMessage.UpdateSuccessful,
                        _resourceService.GetString("Task info")), string.Empty, taskInfo.ConcurrencyStamp);

            async Task UpdateTaskLog()
            {
                if (oldTaskName != taskInfo.Name)
                    await InsertTaskNameLog(tenantId, taskId, taskMeta.UserId, taskMeta.FullName, taskMeta.Avatar, oldTaskName, taskInfo.Name);
                if (oldTaskStatus != taskInfo.Status)
                    await InsertTaskStatusLog(tenantId, taskId, taskMeta.UserId, taskMeta.FullName, taskMeta.Avatar, oldTaskStatus, taskInfo.Status);
                if (oldDescription != taskInfo.Description)
                    await InsertTaskDescriptionLog(tenantId, taskId, taskMeta.UserId, taskMeta.FullName, taskMeta.Avatar, oldDescription, taskInfo.Description);
                if (DateTime.Compare(oldStartDate, taskInfo.EstimateStartDate) != 0)
                    await InsertTaskEstimateStartDateLog(tenantId, taskId, taskMeta.UserId, taskMeta.FullName, taskMeta.Avatar,
                        oldStartDate, taskInfo.EstimateStartDate);
                if (DateTime.Compare(oldEndDate, taskInfo.EstimateEndDate) != 0)
                    await InsertTaskEstimateEndDateLog(tenantId, taskId, taskMeta.UserId, taskMeta.FullName, taskMeta.Avatar,
                        oldEndDate, taskInfo.EstimateEndDate);
                if (oldPercentCompleted != taskInfo.PercentCompleted)
                    await InsertTaskProgressLog(tenantId, taskId, taskMeta.UserId, taskMeta.FullName, taskMeta.Avatar, oldPercentCompleted, taskInfo.PercentCompleted);
                if (oldMethodCaculateResult != taskInfo.MethodCalculateResult)
                    await InsertTaskMethodCaculateResultLog(tenantId, taskId, taskMeta.UserId, taskMeta.FullName, taskMeta.Avatar,
                        oldMethodCaculateResult, taskInfo.MethodCalculateResult);
            }
        }

        public async Task<SearchResult<Log>> SearchHistory(string tenantId, long taskId, string userId, int page, int pageSize)
        {
            var isHasPermission = await CheckPermission(tenantId, taskId, userId);
            if (!isHasPermission)
                return new SearchResult<Log>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var logs = await _logRepository.Search(tenantId, ObjectType.Task, taskId, page, pageSize,
                out int totalRows);
            return new SearchResult<Log>(logs, totalRows);
        }

        public async Task<ActionResultResponse<dynamic>> InsertChecklist(CheckListsMeta checklistMeta)
        {
            var taskInfo = await _tasksRepository.GetInfo(checklistMeta.TenantId, checklistMeta.TaskId);
            if (taskInfo == null)
                return new ActionResultResponse<dynamic>(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound, _resourceService.GetString("Task")));

            var isHasPermission = await CheckPermission(checklistMeta.TenantId, checklistMeta.TaskId, checklistMeta.UserId);
            if (!isHasPermission)
                return new ActionResultResponse<dynamic>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var checklist = new CheckList
            {
                TenantId = checklistMeta.TenantId,
                CreatorId = checklistMeta.UserId,
                CreatorFullName = checklistMeta.FullName,
                TaskId = checklistMeta.TaskId,
                Name = checklistMeta.Name,
                Type = TaskType.Task,
            };
            var result = await _taskCheckListsRepository.InsertAsync(checklist);
            if (result <= 0)
                return new ActionResultResponse<dynamic>(result,
                    _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            // Cập nhật lại tiến độ của công việc.
            await UpdateTotalCheckList(checklistMeta.TenantId, taskInfo);

            return new ActionResultResponse<dynamic>(result, _sharedResourceService.GetString(SuccessMessage.AddSuccessful,
                    _resourceService.GetString("checklist")), string.Empty, new
                    {
                        checklist.Id,
                        checklist.ConcurrencyStamp
                    });
        }

        public async Task<ActionResultResponse<string>> UpdateChecklist(string id, CheckListsMeta checklistMeta)
        {
            var checklistInfo = await _taskCheckListsRepository.GetInfo(checklistMeta.TenantId, checklistMeta.TaskId, id);
            if (checklistInfo == null)
                return new ActionResultResponse<string>(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound, _resourceService.GetString("checklist")));

            var isHasPermission = await CheckPermission(checklistMeta.TenantId, checklistMeta.TaskId, checklistMeta.UserId);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (checklistMeta.ConcurrencyStamp != checklistInfo.ConcurrencyStamp)
                return new ActionResultResponse<string>(-2, _sharedResourceService.GetString(ErrorMessage.AlreadyUpdatedByAnother));

            checklistInfo.ConcurrencyStamp = Guid.NewGuid().ToString();
            checklistInfo.Name = checklistMeta.Name?.Trim();
            var result = await _taskCheckListsRepository.Update(checklistInfo);
            return new ActionResultResponse<string>(result, result < 0
                ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : result == 0
                    ? _sharedResourceService.GetString(ErrorMessage.NothingChanges)
                    : _sharedResourceService.GetString(SuccessMessage.UpdateSuccessful,
                        _resourceService.GetString("checklist")), string.Empty, checklistInfo.ConcurrencyStamp);
        }

        public async Task<ActionResultResponse> DeleteChecklist(string tenantId, string userId, long taskId, string checklistId)
        {
            var taskInfo = await _tasksRepository.GetInfo(tenantId, taskId);
            if (taskInfo == null)
                return new ActionResultResponse<dynamic>(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound, _resourceService.GetString("Task")));

            var isHasPermission = await CheckPermission(tenantId, taskId, userId);
            if (!isHasPermission)
                return new ActionResultResponse(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var result = await _taskCheckListsRepository.Delete(tenantId, taskId, checklistId);
            if (result <= 0)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            // Cập nhật lại tiến độ của công việc.
            await UpdateTotalCheckList(tenantId, taskInfo);

            return new ActionResultResponse(result, _sharedResourceService.GetString(SuccessMessage.DeleteSuccessful,
                _resourceService.GetString("checklist")));
        }

        public async Task<ActionResultResponse<string>> UpdateCompleteStatus(string tenantId, string userId, long taskId, string checklistId,
            bool isComplete)
        {
            var taskInfo = await _tasksRepository.GetInfo(tenantId, taskId);
            if (taskInfo == null)
                return new ActionResultResponse<string>(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound, _resourceService.GetString("Task")));

            var isHasPermission = await CheckPermission(tenantId, taskId, userId);
            if (!isHasPermission)
                return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var result = await _taskCheckListsRepository.UpdateCompleteStatus(tenantId, taskId, checklistId, isComplete);
            if (result <= 0)
                return new ActionResultResponse<string>(-2, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            // Cập nhật lại tiến độ của công việc.            
            await UpdateTotalCheckList(tenantId, taskInfo);

            return new ActionResultResponse<string>(result,
                _sharedResourceService.GetString(SuccessMessage.UpdateSuccessful,
                    _resourceService.GetString("checklist status")));
        }

        #region TargetAttachment
        public async Task<ActionResultResponse> DeleteAttachment(string tenantId, string userId, string fullName, string avatar,
            long taskId, string attachmentId)
        {
            var attachmentInfo = await _attachmentRepository.GetInfo(tenantId, attachmentId, true);
            if (attachmentInfo == null)
                return new ActionResultResponse(-1, _resourceService.GetString("Attachment doest not exists"));

            var taskInfo = await _tasksRepository.GetInfo(tenantId, taskId, true);
            if (taskInfo == null)
                return new ActionResultResponse(-2, _resourceService.GetString("Task doest not exists"));

            var isHasPermission = await CheckPermission(tenantId, taskId, userId);
            if (!isHasPermission)
                return new ActionResultResponse(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var resultDeleteAttachment = await _attachmentRepository.Delete(tenantId, attachmentId);
            if (resultDeleteAttachment < 0)
                return new ActionResultResponse(-3, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            return new ActionResultResponse(resultDeleteAttachment, _resourceService.GetString("Delete attachment successfully"));
        }

        public async Task<ActionResultResponse> InsertAttachment(string tenantId, string userId, string fullName, string avatar, long taskId,
            AttachmentMeta attachmentMeta)
        {
            var taskInfo = await _tasksRepository.GetInfo(tenantId, taskId, true);
            if (taskInfo == null)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Task does not exists"));

            var isHasPermission = await CheckPermission(tenantId, taskId, userId);
            if (!isHasPermission)
                return new ActionResultResponse(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var isCheckExists = await _attachmentRepository.CheckExistsByFileIdObjectId(tenantId, taskId, ObjectType.Task, attachmentMeta.FileId);
            if (isCheckExists)
                return new ActionResultResponse<string>(-3, _resourceService.GetString("Attachment does exists in this target"));

            var attachmentId = Guid.NewGuid().ToString();
            var attachment = new Attachment
            {
                Id = attachmentId,
                ConcurrencyStamp = attachmentId,
                FileId = attachmentMeta.FileId,
                CreatorId = attachmentMeta.CreatorId,
                CreatorFullName = attachmentMeta.CreatorFullName,
                CreateTime = attachmentMeta.CreateTime,
                FileName = attachmentMeta.FileName,
                Type = attachmentMeta.Type,
                Url = attachmentMeta.Url,
                TenantId = tenantId,
                ObjectId = taskId,
                ObjectType = ObjectType.Task,
                Extension = attachmentMeta.Extension
            };

            var result = await _attachmentRepository.InsertAttachment(attachment);
            if (result <= 0)
                return new ActionResultResponse<string>(-1, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            return new ActionResultResponse<string>(result, _resourceService.GetString("Insert attachment successfully"), "", attachmentId);
        }

        public async Task<ActionResultResponse<List<Attachment>>> InsertAttachments(string tenantId, string userId, string fullName, string avatar,
            long taskId, List<AttachmentMeta> attachmentMeta)
        {
            var taskInfo = await _tasksRepository.GetInfo(tenantId, taskId, true);
            if (taskInfo == null)
                return new ActionResultResponse<List<Attachment>>(-1, _resourceService.GetString("Target does not exists"));

            var isHasPermission = await CheckPermission(tenantId, taskId, userId);
            if (!isHasPermission)
                return new ActionResultResponse<List<Attachment>>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            List<Attachment> attachments = new List<Attachment>();
            foreach (var attachment in attachmentMeta)
            {
                var isCheckExists = await _attachmentRepository.CheckExistsByFileIdObjectId(tenantId, taskId, ObjectType.Task, attachment.FileId);
                if (!isCheckExists)
                {
                    var attachmentId = Guid.NewGuid().ToString();
                    var item = new Attachment
                    {
                        Id = attachmentId,
                        ConcurrencyStamp = attachmentId,
                        FileId = attachment.FileId,
                        CreatorId = attachment.CreatorId,
                        CreatorFullName = attachment.CreatorFullName,
                        CreateTime = attachment.CreateTime,
                        FileName = attachment.FileName,
                        Type = attachment.Type,
                        Extension = attachment.Extension,
                        Url = attachment.Url,
                        TenantId = tenantId,
                        ObjectId = taskId,
                        ObjectType = ObjectType.Task,
                    };
                    attachments.Add(item);
                }
            }

            var resultInsert = await _attachmentRepository.InsertAttachments(attachments);
            if (resultInsert <= 0)
                return new ActionResultResponse<List<Attachment>>(resultInsert, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            return new ActionResultResponse<List<Attachment>>(resultInsert, _resourceService.GetString("Insert attachment successfully"), "", attachments);
        }
        #endregion

        #region UpdateCheckList
        private async Task<int> UpdateTotalCheckList(string tenantId, Tasks taskInfo)
        {
            var totalCheckList = await _taskCheckListsRepository.GetTotalCheckListByTask(tenantId, taskInfo.Id, TaskType.Task);
            var totalCompleted = await _taskCheckListsRepository.GetTotalCheckListByTask(tenantId, taskInfo.Id, TaskType.Task, true);
            if (taskInfo.MethodCalculateResult == TaskMethodCalculateResult.CheckList)
            {
                var percent = (decimal)totalCompleted / totalCheckList * 100;
                taskInfo.PercentCompleted = percent;
            }
            taskInfo.ChecklistCount = totalCheckList;
            taskInfo.CompletedChecklistCount = totalCompleted;
            return await _tasksRepository.Update(taskInfo);
        }
        #endregion

        #region Notification
        public async Task SendNotificationToParticipants(string tenantId, string creatorId, string creatorFullName, string creatorAvatar,
            long taskId, string taskName)
        {
            var listUsers = await _tasksParticipantsRepository.GetsByTaskId(tenantId, taskId, false);
            var notificationHelper = new NotificationHelper();
            if (listUsers != null && listUsers.Any())
            {
                var notificationEvents = listUsers.Select(x => new Notifications
                {
                    TenantId = tenantId,
                    Title = $"<b>{creatorFullName}</b> send you a notification about task.",
                    Content = $"{taskName}",
                    SenderId = creatorId,
                    SenderFullName = creatorFullName,
                    SenderAvatar = creatorAvatar,
                    Url = $"tasks/{taskId}",
                    ReceiverId = x.UserId,
                    Type = (int)NotificationType.Info,
                    IsSystem = false
                }).ToList();
                foreach (var notificationEvent in notificationEvents)
                {
                    notificationHelper.Send(notificationEvent);
                }
            }
        }
        #endregion       

        public Task<ActionResultResponse> UpdatePercentCompleted(string tenantId, long id, int percentCompleted)
        {
            throw new NotImplementedException();
        }

        public async Task<SearchResult<TaskTreeViewModel>> GetTaskChildTree(string tenantId, string userId, long taskId)
        {
            var isHasPermission = await CheckPermission(tenantId, taskId, userId, TaskRole.Read);
            if (!isHasPermission)
                return new SearchResult<TaskTreeViewModel>(-403,
                    _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            var taskInfo = await _tasksRepository.GetInfo(tenantId, taskId, true);
            if (taskInfo == null)
                return new SearchResult<TaskTreeViewModel>(-1,
                    _sharedResourceService.GetString(ErrorMessage.NotFound, _resourceService.GetString("Task info")));

            var listTaskChild = await _tasksRepository.GetTaskChilByTaskId(tenantId, taskInfo.IdPath);
            if (listTaskChild == null || !listTaskChild.Any())
                return new SearchResult<TaskTreeViewModel>
                {
                    Items = null,
                    Code = 1
                };

            var taskTrees = RenderTree(listTaskChild, taskId);
            return new SearchResult<TaskTreeViewModel>
            {
                Items = taskTrees,
                Code = 1
            };
        }

        #region Log
        private async Task InsertTaskMethodCaculateResultLog(string tenantId, long taskId, string userId, string fullName, string avatar,
            TaskMethodCalculateResult oldMethodCaculateResult, TaskMethodCalculateResult newMethodCaculateResult)
        {
            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TaskUpdateCalculatorResultMethod,
                FromValue = oldMethodCaculateResult.ToString(),
                ToValue = newMethodCaculateResult.ToString(),
                ObjectId = taskId,
                ObjectType = ObjectType.Task,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Value
            });
        }

        private async Task InsertTaskProgressLog(string tenantId, long taskId, string userId, string fullName, string avatar, decimal oldPercentCompleted,
            decimal newPercentCompleted)
        {
            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TaskUpdateProgress,
                FromValue = oldPercentCompleted.ToString(CultureInfo.InvariantCulture),
                ToValue = newPercentCompleted.ToString(CultureInfo.InvariantCulture),
                ObjectId = taskId,
                ObjectType = ObjectType.Task,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Value
            });
        }

        private async Task InsertTaskEstimateStartDateLog(string tenantId, long taskId, string userId, string fullName, string avatar, DateTime oldStartDate,
            DateTime newEstimateStartDate)
        {
            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TaskUpdateEstimateStartDate,
                FromValue = oldStartDate.ToString(CultureInfo.InvariantCulture),
                ToValue = newEstimateStartDate.ToString(CultureInfo.InvariantCulture),
                ObjectId = taskId,
                ObjectType = ObjectType.Task,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.DateTime
            });
        }

        private async Task InsertTaskEstimateEndDateLog(string tenantId, long taskId, string userId, string fullName, string avatar, DateTime oldEndDate,
            DateTime newEndDate)
        {
            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TaskUpdateEstimateEndDate,
                FromValue = oldEndDate.ToString(CultureInfo.InvariantCulture),
                ToValue = newEndDate.ToString(CultureInfo.InvariantCulture),
                ObjectId = taskId,
                ObjectType = ObjectType.Task,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.DateTime
            });
        }

        private async Task InsertTaskDescriptionLog(string tenantId, long taskId, string userId, string fullName, string avatar,
            string oldDescription, string newDescription)
        {
            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TaskUpdateDescription,
                FromValue = oldDescription,
                ToValue = newDescription,
                ObjectId = taskId,
                ObjectType = ObjectType.Task,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Value
            });
        }

        private async Task InsertTaskStatusLog(string tenantId, long taskId, string userId, string fullName, string avatar,
            TasksStatus oldTaskStatus, TasksStatus newTaskStatus)
        {
            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TaskUpdateStatus,
                FromValue = ((int)oldTaskStatus).ToString(),
                ToValue = ((int)newTaskStatus).ToString(),
                ObjectId = taskId,
                ObjectType = ObjectType.Task,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Value
            });
        }

        private async Task InsertTaskNameLog(string tenantId, long taskId, string userId, string fullName, string avatar, string oldTaskName,
            string newTaskName)
        {
            await _logRepository.Insert(new Log
            {
                TenantId = tenantId,
                Disposition = LogUpdateDisposition.TaskUpdateName,
                FromValue = oldTaskName,
                ToValue = newTaskName,
                ObjectId = taskId,
                ObjectType = ObjectType.Task,
                UserId = userId,
                FullName = fullName,
                Avatar = avatar,
                Type = LogType.Value
            });
        }

        private async Task<int> UpdateTotalTaskInTarget(string tenantId, long targetId)
        {
            var totalTask = await _tasksRepository.GetTotalTaskByTargetId(tenantId, targetId);
            var totalTaskComplete = await _tasksRepository.GetTotalTaskCompleteByTargetId(tenantId, targetId);

            return await _targetRepository.UpdateTotalTask(tenantId, targetId, totalTask, totalTaskComplete);
        }
        #endregion

        #region RenderTree
        private List<TaskTreeViewModel> RenderTree(List<TasksSearchViewModel> tasks, long? parentId)
        {
            var tree = new List<TaskTreeViewModel>();
            var parentTasks = new List<TasksSearchViewModel>();

            if (!parentId.HasValue)
            {
                var listTaskId = tasks.Select(x => x.Id);
                parentTasks = tasks.Where(x => x.ParentId == parentId
                || (!parentId.HasValue && x.ParentId.HasValue && !listTaskId.Contains(x.ParentId.Value))).ToList();
            }
            else
            {
                parentTasks = tasks.Where(x => x.ParentId == parentId).ToList();
            }

            if (parentTasks.Any())
            {
                parentTasks.ForEach(task =>
                {
                    var treeData = new TaskTreeViewModel
                    {
                        Id = task.Id,
                        Name = task.Name,
                        ParentId = task.ParentId,
                        IdPath = task.IdPath,
                        Data = task,
                        ChildCount = task.ChildCount,
                        State = new State(),
                        Children = RenderTree(tasks, task.Id)
                    };
                    tree.Add(treeData);
                });
            }
            return tree;
        }

        public async Task<SearchResult<ReportTaskViewModel>> SearchForReport(string tenantId, TaskReportFilter taskReportFilter)
        {
            if (taskReportFilter.TaskReportType == TaskReportType.statisticsByPersonal
                || taskReportFilter.TaskReportType == TaskReportType.listByPersonal)
            {
                var apiUrls = _configuration.GetApiUrl();

                if (apiUrls == null)
                    return new SearchResult<ReportTaskViewModel>
                    {
                        Code = -1,
                        Message = "Office does not exist"
                    };

                if (taskReportFilter.OfficeId.HasValue)
                {
                    var officeInfo = await _httpClient
                    .GetAsync<OfficeInfoViewModel>($"{apiUrls.HrApiUrl}/offices/{tenantId}/office-short-info/{taskReportFilter.OfficeId.Value}");

                    if (officeInfo == null)
                        return new SearchResult<ReportTaskViewModel>
                        {
                            Code = -1,
                            Message = "Đơn vị không tồn tại"
                        };
                }

                if (!string.IsNullOrEmpty(taskReportFilter.UserId))
                {
                    var userInfo = _httpClient.GetUserInfo(tenantId, taskReportFilter.UserId);
                    if (userInfo == null)
                        return new SearchResult<ReportTaskViewModel>
                        {
                            Code = -1,
                            Message = "Nhân viên không tồn tại"
                        };
                }
            }

            var items = await _tasksRepository.SearchForReport(tenantId, taskReportFilter);
            return new SearchResult<ReportTaskViewModel>
            {
                Code = 1,
                Items = items,
            };
        }

        public async Task<SearchResult<TaskViewModel>> SearchReportDetail(string tenantId, TaskReportFilter taskReportFilter, int page, int pageSize)
        {
            if (taskReportFilter.TaskReportType == TaskReportType.statisticsByPersonal
                || taskReportFilter.TaskReportType == TaskReportType.listByPersonal)
            {
                var apiUrls = _configuration.GetApiUrl();

                if (apiUrls == null)
                    return new SearchResult<TaskViewModel>
                    {
                        Code = -1,
                        Message = "Office does not exist"
                    };

                if (taskReportFilter.OfficeId.HasValue)
                {
                    var officeInfo = await _httpClient
                    .GetAsync<OfficeInfoViewModel>($"{apiUrls.HrApiUrl}/offices/{tenantId}/office-short-info/{taskReportFilter.OfficeId.Value}");

                    if (officeInfo == null)
                        return new SearchResult<TaskViewModel>
                        {
                            Code = -1,
                            Message = "Đơn vị không tồn tại"
                        };
                }

                if (!string.IsNullOrEmpty(taskReportFilter.UserId))
                {
                    var userInfo = _httpClient.GetUserInfo(tenantId, taskReportFilter.UserId);
                    if (userInfo == null)
                        return new SearchResult<TaskViewModel>
                        {
                            Code = -1,
                            Message = "Nhân viên không tồn tại"
                        };
                }
            }

            var items = await _tasksRepository.SearchReportDetail(tenantId, taskReportFilter, page, pageSize, out int totalRows);
            return new SearchResult<TaskViewModel>
            {
                Code = 1,
                Items = items,
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse<TaskNotification>> GetNotificationDetail(long id, string tenantId)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
