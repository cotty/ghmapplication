﻿using System;
using System.Threading.Tasks;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.Resources;
using GHM.Tasks.Domain.ViewModels;

namespace GHM.Tasks.Infrastructure.Services
{
    public class TargetGroupService : ITargetGroupService
    {
        private readonly ITargetGroupRepository _targetGroupRepository;
        private readonly ITargetTableRepository _targetTableRepository;
        private readonly ITargetRepository _targetRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmTaskResource> _resourceService;

        public TargetGroupService(ITargetGroupRepository targetGroupRepository, ITargetTableRepository targetTableRepository,
            ITargetRepository targetRepository,
            IResourceService<SharedResource> sharedResourceService, IResourceService<GhmTaskResource> resourceService)
        {
            _targetGroupRepository = targetGroupRepository;
            _targetTableRepository = targetTableRepository;
            _targetRepository = targetRepository;
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
        }

        public async Task<ActionResultResponse> Delete(string id, string tenantId)
        {
            var targetGroupInfo = await _targetGroupRepository.GetInfo(tenantId, id);
            if (targetGroupInfo == null)
                return new ActionResultResponse(-1, _resourceService.GetString("Target group not exists"));

            if (targetGroupInfo.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));           

            var countTarget = await _targetRepository.GetCountByGroup(tenantId, id);
            if(countTarget > 0)
                return new ActionResultResponse(-3, _resourceService.GetString("Target group have target using"));

            targetGroupInfo.IsDelete = true;
            var result = await _targetGroupRepository.Update(targetGroupInfo);

            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                                           : _resourceService.GetString("Delete success"));
        }        

        public async Task<SearchResult<TargetGroupSuggestionViewModel>> GetTargetGroupByTargetTableId(string tenantId, string targetTableId)
        {
            var result = await _targetGroupRepository.GetTargetGroupByTargetTable(tenantId, targetTableId);

            return new SearchResult<TargetGroupSuggestionViewModel>
            {
                Items = result
            };
        }

        public async Task<ActionResultResponse<string>> Insert(string tenantId, string creatorId,
            string creatorFullName, string targetTableId, TargetGroupMeta targetGroupMeta)
        {
            targetGroupMeta.Name = targetGroupMeta.Name?.Trim();
            var targetTableInfo = await _targetTableRepository.GetInfo(tenantId, targetTableId, true);
            if(targetTableInfo == null)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("TargetGroup does not exist"));

            var isExistByName = await _targetGroupRepository.CheckExistsName(tenantId, targetTableId, targetGroupMeta.Name);
            if (isExistByName)
                return new ActionResultResponse<string>(-2, _resourceService.GetString("Name already exist"));            

            var targetGroupId = Guid.NewGuid().ToString();
            var countRows = await _targetGroupRepository.CountByTargetTableId(tenantId, targetTableId);
            var targetGroup = new TargetGroup()
            {
                Id = targetGroupId,
                TenantId = tenantId,
                TargetTableId = targetTableId,
                Name = targetGroupMeta.Name,
                Description = targetGroupMeta.Description?.Trim(),
                IsActive = true,
                Color = targetGroupMeta.Color,
                Order = countRows + 1,
                ConcurrencyStamp = targetGroupId,
                CreatorId = creatorId,
                CreatorFullName = creatorFullName
            };

            var result = await _targetGroupRepository.Insert(targetGroup);
            return new ActionResultResponse<string>(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                                         : _resourceService.GetString("Insert success"), "", targetGroupId);
        }

        public async Task<ActionResultResponse<string>> Update(string id, string tenantId, string lastUpdateUserId, string lastUpdateFullName,
            TargetGroupMeta targetGroupMeta)
        {          
            var targetGroupInfo = await _targetGroupRepository.GetInfo(tenantId, id);
            if (targetGroupInfo == null)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Target group not exists"));

            if (targetGroupInfo.TenantId != tenantId)
                return new ActionResultResponse<string>(-4, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (targetGroupInfo.ConcurrencyStamp != targetGroupMeta.ConcurrencyStamp)
                return new ActionResultResponse<string>(-5, _sharedResourceService.GetString(ErrorMessage.AlreadyUpdatedByAnother));

            var targetTableInfo = await _targetTableRepository.GetInfo(tenantId, targetGroupInfo.TargetTableId, true);
            if (targetTableInfo == null)
                return new ActionResultResponse<string>(-2, _resourceService.GetString("TargetGroup does not exist"));

            if(targetTableInfo.Status == TargetTableStatus.StopUsing)
                return new ActionResultResponse<string>(-3, _resourceService.GetString("TargetGroup is stop using"));         

            var countTarget = await _targetRepository.GetCountByGroup(tenantId, id);
            if (countTarget > 0)
                return new ActionResultResponse<string>(-6, _resourceService.GetString("Target group have target using"));

            if (targetGroupMeta.Name != targetGroupInfo.Name)
            {
                targetGroupMeta.Name = targetGroupMeta.Name?.Trim();
                var isExistByName = await _targetGroupRepository.CheckExistsName(tenantId, targetGroupInfo.TargetTableId, targetGroupMeta.Name);
                if (isExistByName)
                    return new ActionResultResponse<string>(-4, _resourceService.GetString("Target table already exists"));
            }

            targetGroupInfo.LastUpdate = DateTime.Now;
            targetGroupInfo.LastUpdateUserId = lastUpdateUserId;
            targetGroupInfo.LastUpdateFullName = lastUpdateFullName;
            targetGroupInfo.ConcurrencyStamp = Guid.NewGuid().ToString();
            targetGroupInfo.Name = targetGroupMeta.Name;
            targetGroupInfo.Color = targetGroupMeta.Color;
            targetGroupInfo.Description = targetGroupMeta.Description?.Trim();

            var result = await _targetGroupRepository.Update(targetGroupInfo);
            return new ActionResultResponse<string>(result, result < 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                                        : _resourceService.GetString("Update success"), "", targetGroupInfo.ConcurrencyStamp);
        }

        public async Task<ActionResultResponse> UpdateColor(string tenantId, string id, string color)
        {
            var targetGroupInfo = await _targetGroupRepository.GetInfo(tenantId, id);
            if (targetGroupInfo == null)
                return new ActionResultResponse(-1, _resourceService.GetString("Target table not exists"));

            if (targetGroupInfo.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            targetGroupInfo.Color = color;
            var result = await _targetGroupRepository.Update(targetGroupInfo);

            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                                           : _resourceService.GetString("Update color success"));
        }

        public async Task<ActionResultResponse> UpdateOrder(string tenantId, string id, int order)
        {
            var targetGroupInfo = await _targetGroupRepository.GetInfo(tenantId, id);
            if (targetGroupInfo == null)
                return new ActionResultResponse(-1, _resourceService.GetString("Target table not exists"));

            if (targetGroupInfo.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            targetGroupInfo.Order = order;
            var result = await _targetGroupRepository.Update(targetGroupInfo);

            if (result > 0)
                await _targetGroupRepository.SyncOrder(tenantId, targetGroupInfo.TargetTableId);

            return new ActionResultResponse(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                                           : _resourceService.GetString("Update order success"));
        }
    }
}
