﻿using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.Constants;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.Resources;
using System.Linq;
using System.Threading.Tasks;

namespace GHM.Tasks.Infrastructure.Services
{
    public class CommentService : ICommentService
    {
        private ICommentRepository _commentRepository;
        private ITasksRepository _taskRepository;
        private ITargetRepository _targetRepository;
        private readonly ITaskRoleGroupRepository _taskRoleGroupRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmTaskResource> _taskResourceService;
        public CommentService(ICommentRepository commentRepository, IResourceService<SharedResource> sharedResourceService,
            ITaskRoleGroupRepository taskRoleGroupRepository,
            IResourceService<GhmTaskResource> taskResourceService, ITasksRepository tasksRepository, ITargetRepository targetRepository)
        {
            _commentRepository = commentRepository;
            _taskRepository = tasksRepository;
            _targetRepository = targetRepository;
            _taskRoleGroupRepository = taskRoleGroupRepository;
            _sharedResourceService = sharedResourceService;
            _taskResourceService = taskResourceService;
        }

        public async Task<ActionResultResponse> ForceDelete(string tenantId, string userId, long id)
        {
            var info = await _commentRepository.GetAsync(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, _taskResourceService.GetString("Comment does not exist"));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (info.UserId != userId)
                return new ActionResultResponse(-3, _taskResourceService.GetString("You not permession delete comment"));

            if (info.ObjectType == ObjectType.Target)
            {
                var targetInfo = await _targetRepository.GetInfo(tenantId, info.ObjectId, true);
                if (targetInfo == null)
                    return new ActionResultResponse
                    {
                        Code = -4,
                        Message = _taskResourceService.GetString("Target does not exist")
                    };

                var isHasPermission = await _targetRepository.CheckPermission(tenantId, targetInfo, userId, TargetRole.Disputation);
                if (!isHasPermission)
                    return new ActionResultResponse(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));
            }

            if (info.ObjectType == ObjectType.Task)
            {
                var taskInfo = await _taskRepository.GetInfo(tenantId, info.ObjectId, true);
                if (taskInfo == null)
                    return new ActionResultResponse
                    {
                        Code = -5,
                        Message = _taskResourceService.GetString("Task does not exist")
                    };

                var isHasPermission = await CheckPermission(tenantId, info.ObjectId, userId);
                if (!isHasPermission)
                    return new ActionResultResponse<string>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));
            }

            var result = await _commentRepository.ForceDelete(tenantId, id);
            if (result > 0)
            {
                var totalCommnetByObjectId = await _commentRepository.GetTotalCommentObjectId(tenantId, info.ObjectId, info.ObjectType);
                if (info.ObjectType == ObjectType.Target)
                {
                    await _targetRepository.UpdateTotalComment(tenantId, info.ObjectId, totalCommnetByObjectId);
                    return new ActionResultResponse(1, _taskResourceService.GetString("Delete Successfully!"));
                }

                if (info.ObjectType == ObjectType.Task)
                {
                    await _taskRepository.UpdateTotalComment(tenantId, info.ObjectId, totalCommnetByObjectId);
                    return new ActionResultResponse(1, _taskResourceService.GetString("Delete Successfully!"));
                }
            }

            return new ActionResultResponse(-7, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
        }

        public async Task<SearchResult<Comment>> GetsAllByObject(string tenantId, string userId, ObjectType objectType, long objectId,
            int page, int pageSize)
        {
            if (objectType == ObjectType.Target)
            {
                var targetInfo = await _targetRepository.GetInfo(tenantId, objectId, true);
                if (targetInfo == null)
                    return new SearchResult<Comment>
                    {
                        Items = null,
                        Message = _taskResourceService.GetString("Target does not exist")
                    };

                var isHasPermission = await _targetRepository.CheckPermission(tenantId, targetInfo, userId, TargetRole.Disputation);
                if (!isHasPermission)
                    return new SearchResult<Comment>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));
            }
            else
            {
                var taskInfo = await _taskRepository.GetInfo(tenantId, objectId, true);
                if (taskInfo == null)
                    return new SearchResult<Comment>
                    {
                        Items = null,
                        Message = _taskResourceService.GetString("Task does not exist")
                    };

                //var isHasPermission = await CheckPermission(tenantId, objectId, userId);
                //if (!isHasPermission)
                //    return new SearchResult<Comment>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));
            }

            var items = await _commentRepository.GetsAllByObjectId(tenantId, objectType, objectId, page, pageSize, out int totalRows);
            return new SearchResult<Comment>
            {
                Items = items,
                TotalRows = totalRows
            };
        }

        public async Task<ActionResultResponse<Comment>> Post(string tenantId, string userId, string fullName, string avatar,
            CommentMeta commentMeta)
        {
            if (commentMeta.ObjectType == ObjectType.Target)
            {
                var targetInfo = await _targetRepository.GetInfo(tenantId, commentMeta.ObjectId, true);
                if (targetInfo == null)
                    return new ActionResultResponse<Comment>
                    {
                        Code = -1,
                        Message = _taskResourceService.GetString("target does not exist")
                    };

                var isHasPermission = await _targetRepository.CheckPermission(tenantId, targetInfo, userId, TargetRole.Disputation);
                if (!isHasPermission)
                    return new ActionResultResponse<Comment>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));
            }
            else
            {
                var taskInfo = await _taskRepository.GetInfo(tenantId, commentMeta.ObjectId, true);
                if (taskInfo == null)
                    return new ActionResultResponse<Comment>
                    {
                        Code = -2,
                        Message = _taskResourceService.GetString("Task does not exist")
                    };

                var isHasPermission = await CheckPermission(tenantId, taskInfo.Id, userId);
                if (!isHasPermission)
                    return new ActionResultResponse<Comment>(-403, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));
            }

            if (commentMeta.ParentId.HasValue)
            {
                var commentParentInfo = await _commentRepository.GetAsync(tenantId, commentMeta.ParentId);
                if (commentParentInfo == null)
                    return new ActionResultResponse<Comment>(-2, _taskResourceService.GetString("Parent does not exist"));
            }

            var comment = new Comment
            {
                TenantId = tenantId,
                UserId = userId,
                FullName = fullName,
                Image = avatar,
                Content = commentMeta.Content,
                ObjectId = commentMeta.ObjectId,
                ObjectType = commentMeta.ObjectType,
                UrlAttachment = commentMeta.UrlAttachment,
                LikeCount = 0,
                ChildCount = 0
            };

            var result = await _commentRepository.Insert(comment);
            if (result > 0)
            {
                // update childCount
                if (commentMeta.ParentId.HasValue)
                    await _commentRepository.UpdateChildCount(tenantId, commentMeta.ParentId);

                var totalCommnetByObjectId = await _commentRepository.GetTotalCommentObjectId(tenantId, commentMeta.ObjectId, commentMeta.ObjectType);
                if (commentMeta.ObjectType == ObjectType.Target)
                    await _targetRepository.UpdateTotalComment(tenantId, commentMeta.ObjectId, totalCommnetByObjectId);

                if (commentMeta.ObjectType == ObjectType.Task)
                    await _taskRepository.UpdateTotalComment(tenantId, commentMeta.ObjectId, totalCommnetByObjectId);

                return new ActionResultResponse<Comment>(1, _taskResourceService.GetString("Insert comment succesfully"), "", comment);
            }

            return new ActionResultResponse<Comment>(-5, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));
        }

        private async Task<bool> CheckPermission(string tenantId, long taskId, string userId)
        {
            var taskInfo = await _taskRepository.GetDetail(tenantId, taskId);
            if (taskInfo.CreatorId == userId)
                return true;

            var taskRoleGroup = await _taskRoleGroupRepository.GetDetail(tenantId, taskId, userId);
            return taskRoleGroup != null && taskRoleGroup.Any(x => x.Role == TaskRole.Full || x.Role == TaskRole.Comment);
        }
    }
}
