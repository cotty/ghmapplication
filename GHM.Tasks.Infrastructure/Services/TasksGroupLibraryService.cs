﻿using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.Resources;
using GHM.Tasks.Domain.ViewModels;
using System;
using System.Threading.Tasks;

namespace GHM.Tasks.Infrastructure.Services
{
    public class TasksGroupLibraryService : ITasksGroupLibraryService
    {
        private readonly ITasksGroupLibraryRepository _tasksGroupLibraryRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmTaskResource> _resourceService;
        public TasksGroupLibraryService(ITasksGroupLibraryRepository tasksGroupLibraryRepository,
            IResourceService<SharedResource> sharedResourceService, IResourceService<GhmTaskResource> resourceService)
        {
            _tasksGroupLibraryRepository = tasksGroupLibraryRepository;
            _sharedResourceService = sharedResourceService;
            _resourceService = resourceService;
        }

        public async Task<ActionResultResponse> ForceDelete(string tenantId, string userId, string userFullName, string id)
        {
            var result = await _tasksGroupLibraryRepository.ForceDelete(tenantId, userId, userFullName, id);

            return new ActionResultResponse(result, result < 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong) : _resourceService.GetString("Delete tasks group library successfully"));
        }

        public async Task<SearchResult<TaskGroupLibraryViewModel>> GetAllTasksLibrary(string tenantId)
        {
            var items = await _tasksGroupLibraryRepository.GetAllTasksLibrary(tenantId);

            return new SearchResult<TaskGroupLibraryViewModel>
            {
                Items = items
            };
        }

        public async Task<ActionResultResponse<string>> Insert(string tenantId, string userId, string userFullName,
            TasksGroupLibraryMeta tasksGroupLibraryMeta)
        {
            tasksGroupLibraryMeta.Name = tasksGroupLibraryMeta.Name?.Trim();
            var info = await _tasksGroupLibraryRepository.CheckExistsByName(tenantId, tasksGroupLibraryMeta.Name);
            if (info)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Tasks group library does exist"));

            var taskGroupLibraryId = Guid.NewGuid().ToString();
            var taskGroupLibrary = new TaskGroupLibrary
            {
                Id = taskGroupLibraryId,
                TenantId = tenantId,
                Name = tasksGroupLibraryMeta.Name,
                Description = tasksGroupLibraryMeta.Description?.Trim(),
                CreatorId = userId,
                CreatorFullName = userFullName,
                ConcurrencyStamp = taskGroupLibraryId
            };

            var result = await _tasksGroupLibraryRepository.Insert(taskGroupLibrary);
            return new ActionResultResponse<string>(result, result <= 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                 : _resourceService.GetString("Insert task group library successfully"), "", taskGroupLibraryId);
        }

        public Task<SearchResult<TaskGroupLibrary>> Search(string tenantId, string keyword, int page, int pageSize)
        {
            throw new NotImplementedException();
        }

        public async Task<ActionResultResponse<string>> Update(string tenantId, string id, string userId, string userFullName,
            TasksGroupLibraryMeta tasksGroupLibraryMeta)
        {
            var info = await _tasksGroupLibraryRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse<string>(-1, _resourceService.GetString("Task group library does not exist"));

            if (info.Name != tasksGroupLibraryMeta.Name)
            {
                tasksGroupLibraryMeta.Name = tasksGroupLibraryMeta.Name?.Trim();
                var isExistsByName = await _tasksGroupLibraryRepository.CheckExistsByName(tenantId, tasksGroupLibraryMeta.Name);
                if (isExistsByName)
                    return new ActionResultResponse<string>(-1, _resourceService.GetString("Name tasks group library does exist"));
            }

            if (info.TenantId != tenantId)
                return new ActionResultResponse<string>(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (info.ConcurrencyStamp != tasksGroupLibraryMeta.ConcurrencyStamp)
                return new ActionResultResponse<string>(-1, _sharedResourceService.GetString(ErrorMessage.AlreadyUpdatedByAnother));

            info.Name = tasksGroupLibraryMeta.Name;
            info.Description = tasksGroupLibraryMeta.Description?.Trim();
            info.LastUpdateFullName = userFullName;
            info.LastUpdateUserId = userId;
            info.LastUpdate = DateTime.Now;
            info.ConcurrencyStamp = Guid.NewGuid().ToString();

            var result = await _tasksGroupLibraryRepository.Update(info);

            return new ActionResultResponse<string>(result, result < 0 ? _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong)
                : _resourceService.GetString("Update tasks group library successfully"), "", info.ConcurrencyStamp);
        }

        public async Task<ActionResultResponse<TargetGroupLibraryDetailViewModel>> GetDetail(string tenantId, string id)
        {
            var info = await _tasksGroupLibraryRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse<TargetGroupLibraryDetailViewModel>(-1, _resourceService.GetString("Task group library does not exist"));

            var taskGroupLibraryDetail = new TargetGroupLibraryDetailViewModel()
            {
                Id = info.Id,
                Name = info.Name,
                Description = info.Description,
                ConcurrencyStamp = info.ConcurrencyStamp,
                Order = info.Order
            };

            return new ActionResultResponse<TargetGroupLibraryDetailViewModel>
            {
                Data = taskGroupLibraryDetail,
                Code = 1
            };
        }
    }
}
