﻿using GHM.Infrastructure.Constants;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using GHM.Infrastructure.ViewModels;
using GHM.Tasks.Domain.IRepository;
using GHM.Tasks.Domain.IServices;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Models;
using GHM.Tasks.Domain.Resources;
using System;
using System.Threading.Tasks;

namespace GHM.Tasks.Infrastructure.Services
{
    public class TargetLibraryService : ITargetLibraryService
    {
        private readonly ITargetGroupLibraryRepository _targetGroupLibraryRepository;
        private readonly ITargetLibraryRepository _targetLibraryRepository;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IResourceService<GhmTaskResource> _taskResourceService;
        public TargetLibraryService(ITargetLibraryRepository targetLibraryRepository, ITargetGroupLibraryRepository targetGroupLibraryRepository,
            IResourceService<GhmTaskResource> taskResorceService
            , IResourceService<SharedResource> sharedResourceService)
        {
            _targetGroupLibraryRepository = targetGroupLibraryRepository;
            _targetLibraryRepository = targetLibraryRepository;
            _sharedResourceService = sharedResourceService;
            _taskResourceService = taskResorceService;
        }
        public async Task<ActionResultResponse> ForceDelete(string tenantId, string userId, string userFullName, string id)
        {
            var info = await _targetLibraryRepository.GetInfo(tenantId, id);
            if (info == null)
                return new ActionResultResponse(-1, _sharedResourceService.GetString("Target library does not exist"));

            if (info.TenantId != tenantId)
                return new ActionResultResponse(-2, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            info.IsDelete = true;
            var result = await _targetLibraryRepository.Update(info);
            if (result < 0)
                return new ActionResultResponse(result, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            return new ActionResultResponse(1, _sharedResourceService.GetString("Insert taget library successfully"));
        }

        public async Task<ActionResultResponse<string>> Insert(string tenantId, string userId, string userFullName,
            TargetLibraryMeta targetLibraryMeta)
        {
            targetLibraryMeta.Name = targetLibraryMeta.Name?.Trim();
            var targetGroupLibraryInfo = await _targetGroupLibraryRepository.GetInfo(tenantId, targetLibraryMeta.TargetGroupLibraryId);
            if (targetGroupLibraryInfo == null)
                return new ActionResultResponse<string>(-1, _taskResourceService.GetString("target group library does not exist"));

            var isExistsByName = await _targetLibraryRepository.CheckInfoByName(tenantId, targetLibraryMeta.TargetGroupLibraryId, targetLibraryMeta.Name);
            if (isExistsByName)
                return new ActionResultResponse<string>(-1, _taskResourceService.GetString("target library does exist"));

            var targetLibraryId = Guid.NewGuid().ToString();
            var info = new TargetLibrary
            {
                Id = targetLibraryId,
                ConcurrencyStamp = targetLibraryId,
                CreatorId = userId,
                Description = targetLibraryMeta.Description?.Trim(),
                CreatorFullName = userFullName,
                TenantId = tenantId,
                MeasurementMethod = targetLibraryMeta.MeasurementMethod?.Trim(),
                Name = targetLibraryMeta.Name,
                TargetGroupLibraryId = targetGroupLibraryInfo.Id
            };

            var result = await _targetLibraryRepository.Insert(info);
            if (result <= 0)
                return new ActionResultResponse<string>(result, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            return new ActionResultResponse<string>(result, _taskResourceService.GetString("Insert target library successfully"), "", info.Id);
        }

        public Task<SearchResult<TargetGroupLibrary>> Search(string tenantId, string keyword, int page, int pageSize)
        {
            throw new NotImplementedException();
        }

        public async Task<ActionResultResponse<string>> Update(string tenantId, string id, string userId, string userFullName,
            TargetLibraryMeta targetLibraryMeta)
        {
            var info = await _targetLibraryRepository.GetInfo(tenantId, id);
            if (info.TenantId != tenantId)
                return new ActionResultResponse<string>(-1, _sharedResourceService.GetString(ErrorMessage.NotHavePermission));

            if (info.ConcurrencyStamp != targetLibraryMeta.ConcurrencyStamp)
                return new ActionResultResponse<string>(-2, _sharedResourceService.GetString(ErrorMessage.AlreadyUpdatedByAnother));

            var targetGroupLibraryInfo = await _targetGroupLibraryRepository.GetInfo(tenantId, targetLibraryMeta.TargetGroupLibraryId);
            if (targetGroupLibraryInfo == null)
                return new ActionResultResponse<string>(-3, _taskResourceService.GetString("Target group library does not exist"));

            if (info.Name != targetLibraryMeta.Name)
            {
                targetLibraryMeta.Name = targetLibraryMeta.Name?.Trim();
                var isExistsByName = await _targetLibraryRepository.CheckInfoByName(tenantId, info.TargetGroupLibraryId, targetLibraryMeta.Name);
                if (isExistsByName)
                    return new ActionResultResponse<string>(-4, _taskResourceService.GetString("Target library does not exist"));
            }

            info.TargetGroupLibraryId = targetLibraryMeta.TargetGroupLibraryId;
            info.LastUpdate = DateTime.Now;
            info.LastUpdateFullName = userFullName;
            info.LastUpdateUserId = userId;
            info.Name = targetLibraryMeta.Name;
            info.Description = targetLibraryMeta.Description?.Trim();
            info.MeasurementMethod = targetLibraryMeta.MeasurementMethod?.Trim();
            info.ConcurrencyStamp = Guid.NewGuid().ToString();

            var result = await _targetLibraryRepository.Update(info);

            if (result < 0)
                return new ActionResultResponse<string>(result, _sharedResourceService.GetString(ErrorMessage.SomethingWentWrong));

            return new ActionResultResponse<string>(1, _sharedResourceService.GetString("Update target library successfully"), "", info.ConcurrencyStamp);
        }
    }
}
