﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Helpers.Validations;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Resources;

namespace GHM.Tasks.Infrastructure.Validations
{
    class ChecklistMetaValidator : AbstractValidator<CheckListsMeta>
    {
        public ChecklistMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmTaskResource> taskResourceService)
        {
            RuleFor(x => x.Name)
                .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.", taskResourceService.GetString("Name")))
                .MaximumLength(256).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    taskResourceService.GetString("Name"), 256));

            RuleFor(x => x.TaskId)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseSelect,
                    taskResourceService.GetString("Task")));
        }
    }
}
