﻿using FluentValidation;
using GHM.Infrastructure.Helpers.Validations;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Resources;

namespace GHM.Tasks.Infrastructure.Validations
{
    public class TargetLibraryValidator : AbstractValidator<TargetLibraryMeta>
    {
        public TargetLibraryValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmTaskResource> taskResourceService)
        {
            RuleFor(x => x.Name)
               .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.", taskResourceService.GetString("Name")))
               .MaximumLength(256).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    taskResourceService.GetString("Target table"), 256));

            RuleFor(x => x.Description)
                .MaximumLength(4000)
                .WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    taskResourceService.GetString("Description"), 4000));

            RuleFor(x => x.MeasurementMethod)
                .MaximumLength(256)
                .WithMessage(sharedResourceService.GetString("{0} is not allowed over  {1} characters", taskResourceService.GetString("Measurement Method"), 256));
        }
    }
}
