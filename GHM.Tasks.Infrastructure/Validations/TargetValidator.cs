﻿using FluentValidation;
using GHM.Infrastructure.Helpers.Validations;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Resources;

namespace GHM.Tasks.Infrastructure.Validations
{
    public class TargetValidator : AbstractValidator<TargetMeta>
    {
        public TargetValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmTaskResource> taskResourceService)
        {
            RuleFor(x => x.Name)
               .NotNullAndEmpty(sharedResourceService.GetString("{0} can not be null.", taskResourceService.GetString("Name")))
               .MaximumLength(256).WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    taskResourceService.GetString("Target table"), 256));

            RuleFor(x => x.Type)
                .NotNull()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.", taskResourceService.GetString("Target Type")))
                .IsInEnum()
                .WithMessage(sharedResourceService.GetString("{0} is not valid.", taskResourceService.GetString("Target Type")));

            RuleFor(x => x.TargetTableId)
              .NotNull()
              .WithMessage(sharedResourceService.GetString("{0} can not be null.", taskResourceService.GetString("Target Table")));

            RuleFor(x => x.TargetGroupId)
                .NotNull()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.", taskResourceService.GetString("Target Group")));

            RuleFor(x=> x.StartDate)
                .NotNull()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.", taskResourceService.GetString("Start Date")));

            RuleFor(x => x.EndDate)
               .NotNull()
               .WithMessage(sharedResourceService.GetString("{0} can not be null.", taskResourceService.GetString("End Date")));

            RuleFor(x => x.Description)
                .MaximumLength(4000)
                .WithMessage(sharedResourceService.GetString("{0} is not allowed over {1} characters.",
                    taskResourceService.GetString("Description"), 4000));

            RuleFor(x => x.MeasurementMethod)
                .MaximumLength(256)
                .WithMessage(sharedResourceService.GetString("{0} is not allowed over  {1} characters", taskResourceService.GetString("Measurement Method"), 256));

            RuleFor(x => x.PercentCompleted)
               .GreaterThan(0)
               .WithMessage(sharedResourceService.GetString("{0} is must greater than 0", taskResourceService.GetString("Percent Completed")))
               .LessThan(1000)
               .WithMessage(sharedResourceService.GetString("{0} is must less than 1000", taskResourceService.GetString("Percent Completed")));

            RuleFor(x => x.Weight)
              .GreaterThan(-1)
              .WithMessage(sharedResourceService.GetString("{0} is must greater than or equal 0", taskResourceService.GetString("Weight")))
              .LessThan(101)
              .WithMessage(sharedResourceService.GetString("{0} is must less than or equal 100", taskResourceService.GetString("Weight")));
        }
    }
}
