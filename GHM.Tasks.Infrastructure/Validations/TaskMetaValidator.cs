﻿using FluentValidation;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.Helpers.Validations;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Resources;
using GHM.Tasks.Domain.ModelMetas;
using GHM.Tasks.Domain.Resources;

namespace GHM.Tasks.Infrastructure.Validations
{
    class TaskMetaValidator : AbstractValidator<TaskMeta>
    {
        public TaskMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmTaskResource> resourceService)
        {
            RuleFor(x => x.Name)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseEnter, resourceService.GetString("Task name")))
                .MaximumLength(256).WithMessage(sharedResourceService.GetString(ValidatorMessage.MustNotExceed,
                    resourceService.GetString("Task name"), 256));

            RuleFor(x => x.EstimateStartDate)
                .NotNull()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                    resourceService.GetString("Start date")))
                .MustBeValidDate(sharedResourceService.GetString(ValidatorMessage.InValid, resourceService.GetString("startdate")));

            RuleFor(x => x.EstimateEndDate)
                .NotNull()
                .WithMessage(sharedResourceService.GetString("{0} can not be null.",
                    resourceService.GetString("End date")))
                .MustBeValidDate(sharedResourceService.GetString(ValidatorMessage.InValid, resourceService.GetString("startdate")));

            RuleFor(x => x.TargetId)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseSelect,
                    resourceService.GetString("Target")));

            RuleFor(x => x.Type)
              .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseSelect,
                  resourceService.GetString("TaskType")));

            RuleFor(x => x.Description)
                .MaximumLength(500)
                .WithMessage(sharedResourceService.GetString(ValidatorMessage.MustNotExceed,
                    resourceService.GetString("Task description"), 500));

            RuleFor(x => x.ResponsibleId)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseSelect, resourceService.GetString("Staff")))
                .MaximumLength(256).WithMessage(sharedResourceService.GetString(ValidatorMessage.MustNotExceed,
                    resourceService.GetString("Staff id"), 50));

            RuleForEach(x => x.Participants)
                .SetValidator(new TaskParticipantMetaValidator(sharedResourceService, resourceService));
        }
    }

    public class TaskParticipantMetaValidator : AbstractValidator<TaskParticipantMeta>
    {
        public TaskParticipantMetaValidator(IResourceService<SharedResource> sharedResourceService, IResourceService<GhmTaskResource> resourceService)
        {
            RuleFor(x => x.UserId)
                .NotNullAndEmpty(sharedResourceService.GetString(ValidatorMessage.PleaseSelect, resourceService.GetString("Please select staff")))
                .MaximumLength(50).WithMessage(sharedResourceService.GetString(ValidatorMessage.MustNotExceed,
                    resourceService.GetString("User Id"), 50));
        }
    }
}
