﻿using System.Threading.Tasks;
using GHM.EventBus.Abstractions;
using GHM.Events;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Services;
using GHM.Infrastructure.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Core.Api.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        //private readonly IEventBus _eventBus;

        public ValuesController()
        {
            //_eventBus = eventBus;
        }

        // GET api/values/5
        [AcceptVerbs("GET")]
        public async Task<IActionResult> Get()
        {
        //    var httpClientService = new HttpClientService();
        //    var officeInfo = await httpClientService.GetAsync<OfficeInfoViewModel>($"http://localhost:5002/api/v1/offices/info/12");
            //if (officeInfo == null)
            //    BadRequest("");
            return Ok("DqwdqwD");
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
