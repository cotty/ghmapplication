﻿using System;
using System.Threading.Tasks;
using GHM.Core.Domain.IServices;
using GHM.Core.Domain.ModelMetas;
using GHM.Infrastructure.CustomAttributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Core.Api.Controllers
{
    [Authorize]
    [Route("api/v{version:apiVersion}/[controller]")]
    [Produces("application/json")]
    public class ClientsController : ControllerBase
    {
        private readonly IClientService _clientService;
        public ClientsController(IClientService clientService)
        {
            _clientService = clientService;
        }

        [AcceptVerbs("GET")]
        public async Task<IActionResult> Search(string keyword, bool? enabled, int page = 1, int pageSize = 20)
        {
            var result = await _clientService.Search(keyword, enabled, page, pageSize);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("get-client-id"), AcceptVerbs("GET")]
        public IActionResult GetClientId()
        {
            return Ok(Guid.NewGuid().ToString());
        }

        [AcceptVerbs("POST"), ValidateModel]
        public async Task<IActionResult> Insert([FromBody]ClientMeta client)
        {
            var result = await _clientService.Insert(client);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("POST")]
        public async Task<IActionResult> Update(string id, [FromBody]ClientMeta clientMeta)
        {
            var result = await _clientService.Update(clientMeta);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }

        [Route("delete"), AcceptVerbs("POST")]
        public async Task<IActionResult> Delete(string clientId)
        {
            var result = await _clientService.Delete(clientId);
            if (result.Code <= 0)
                return BadRequest(result);
            return Ok(result);
        }
    }
}
