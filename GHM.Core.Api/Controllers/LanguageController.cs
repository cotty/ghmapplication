﻿using System.Threading.Tasks;
using GHM.Core.Domain.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GHM.Core.Api.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/[controller]s")]
    public class LanguageController : GhmControllerBase
    {
        private readonly ILanguageRepository _languageRepository;
        private readonly ITenantLanguageRepository _tenantLanguageRepository;

        public LanguageController(ITenantLanguageRepository tenantLanguageRepository, ILanguageRepository languageRepository)
        {
            _tenantLanguageRepository = tenantLanguageRepository;
            _languageRepository = languageRepository;
        }

        [AcceptVerbs("GET")]
        public async Task<IActionResult> GetLanguage()
        {
            return Ok(await _tenantLanguageRepository.GetAllLanguage(CurrentUser.TenantId));
        }

        [Route("all"), AcceptVerbs("GET")]
        public async Task<IActionResult> GetAllLanguage()
        {
            return Ok(await _languageRepository.GetAllLanguage(x => new
            {
                LanguageId = x.Id,
                x.Name,
                x.Description
            }));
        }

        [Route("suggestions"), AcceptVerbs("GET")]
        public async Task<IActionResult> Suggestions()
        {
            return Ok(await _languageRepository.GetAllLanguage(x => new
            {
                LanguageId = x.Id,
                x.Id,
                x.Name,
                x.Description
            }));
        }
    }
}