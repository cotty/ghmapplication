﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GHM.Core.Domain.IRepository;
using GHM.Core.Domain.IServices;
using GHM.Core.Domain.ModelMetas;
using GHM.Core.Domain.Models.AccountViewModels;
using GHM.Infrastructure.Constants;
using GHM.Infrastructure.CustomAttributes;
using GHM.Infrastructure.Filters;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.IServices;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace GHM.Core.Api.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    [Produces("application/json")]
    public class AccountsController : GhmControllerBase
    {
        private readonly IUserAccountRepository _userAccountRepository;
        private readonly IMemoryCache _cache;
        private readonly IResourceService<SharedResource> _sharedResourceService;
        private readonly IUserAccountService _userAccountService;

        public AccountsController(IUserAccountRepository userAccountRepository, IMemoryCache cache,
            IResourceService<SharedResource> sharedResourceService, IUserAccountService userAccountService)
        {
            _userAccountRepository = userAccountRepository;
            _cache = cache;
            _sharedResourceService = sharedResourceService;
            _userAccountService = userAccountService;
        }

        [AcceptVerbs("GET")]
        [AllowPermission(PageId.ConfigAccount, Permission.View)]
        [CheckPermission]
        public async Task<IActionResult> Search(string keyword, bool? isActive, int page = 1, int pageSize = 20)
        {
            return Ok(await _userAccountService.Search(CurrentUser.TenantId, keyword, isActive, page, pageSize));
        }

        [Route("register"), AcceptVerbs("POST"), ValidateModel]
        public async Task<IActionResult> Register([FromBody]RegisterViewModel model)
        {
            var passwordSalt = Generate.GenerateRandomBytes(Generate.PasswordSaltLength);
            var passwordHash = Generate.GetInputPasswordHash(model.Password, passwordSalt);
            var userAccount = new UserAccount
            {
                UserName = model.UserName,
                Email = model.Email,
                PasswordHash = Convert.ToBase64String(passwordHash),
                PasswordSalt = passwordSalt,
            };
            var result = await _userAccountRepository.CreateAsync(userAccount, new CancellationToken());
            if (result.Succeeded)
            {
                var accountInfo = await _userAccountRepository.GetInfoByUserName(CurrentUser.TenantId, model.UserName);
                return Ok(new ActionResultResponse<string>
                {
                    Code = 1,
                    Message = "Thêm mới người dùng thành công.",
                    Data = accountInfo.Id
                });
            }

            return BadRequest(new ActionResultResponse(int.Parse(result.Errors.FirstOrDefault()?.Code), result.Errors.FirstOrDefault()?.Description));
        }

        [AllowAnonymous]
        [Route("confirm-email"), AcceptVerbs("GET")]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                //return RedirectToAction(nameof(HomeController.Index), "Home");
                return BadRequest(new ActionResultResponse(-1, "Email không được để trống."));
            }
            var user = await _userAccountRepository.FindByIdAsync(userId, new CancellationToken());
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }

            return Ok();
            //var result = await _userAccountRepository.ConfirmEmailAsync(user, code);
            //return View(result.Succeeded ? "ConfirmEmail" : "Error");
            //if (result.Succeeded)
            //{
            //    return Ok(new ActionResultResponse(1, "Đã xác nhận Email"));
            //}
            //else
            //{
            //    return BadRequest(new ActionResultResponse(-2, "Xác nhận Email thất bại."));
            //}
        }

        [AcceptVerbs("POST")]
        [CheckPermission]
        public async Task<IActionResult> InsertUserAccount(AccountUserMeta userAccount)
        {
            var result = await _userAccountService.InsertUserAccount(userAccount);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("POST"), ValidateModel]
        [CheckPermission]
        public async Task<IActionResult> UpdateUserAccount(string id, AccountUserMeta userAccount)
        {
            var result = await _userAccountService.UpdateUserAccount(id, userAccount);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("change-password"), AcceptVerbs("POST"), ValidateModel]      
        [CheckPermission]
        public async Task<IActionResult> UpdatePassword([FromBody]UpdatePasswordMeta updatePasswordMeta)
        {
            var result = await _userAccountService.UpdatePassword(CurrentUser.Id, updatePasswordMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("reset-password/{id}"), AcceptVerbs("POST"), ValidateModel]
        [AllowPermission(PageId.User, Permission.Update)]
        [AllowPermission(PageId.ConfigAccount, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> ResetPassword(string id)
        {
            var updatePasswordMeta = new UpdatePasswordMeta
            {
                NewPassword = "123456",
                ConfirmNewPassword = "123456",
                OldPassword = "12345",
                IsResetPassword = true
            };

            var result = await _userAccountService.UpdatePassword(id, updatePasswordMeta);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("{id}"), AcceptVerbs("DELETE")]
        public async Task<IActionResult> DeleteAccount(string id)
        {
            var result = await _userAccountService.DeleteUserAccount(id);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Lấy về sử dụng trong service nội bộ.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("info/{id}"), AcceptVerbs("GET")]
        public async Task<IActionResult> GetUserInfo(string id)
        {
            var result = await _userAccountService.GetUserInfo(id);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Khóa tài khoản
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("lock-account/{userName}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.ConfigAccount, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> LockAccount(string userName)
        {
            var result = await _userAccountService.LockAccount(userName);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Mở khóa tài khoản
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [Route("reset-lock/{userName}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.ConfigAccount, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> ResetLockout(string userName)
        {
            var result = await _userAccountService.ResetLockout(userName);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        /// <summary>
        /// Cập nhật trạng thái
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [Route("update-IsActive/{userName}/{isActive}"), AcceptVerbs("POST")]
        [AllowPermission(PageId.ConfigAccount, Permission.Update)]
        [CheckPermission]
        public async Task<IActionResult> UpdateIsActive(string userName, bool isActive)
        {
            var result = await _userAccountService.UpdateIsActive(userName, isActive);
            if (result.Code < 0)
                return BadRequest(result);

            return Ok(result);
        }

        #region Private
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
        #endregion
    }
}