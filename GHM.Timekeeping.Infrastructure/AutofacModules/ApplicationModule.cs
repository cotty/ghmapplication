﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.MongoDb;
using GHM.Infrastructure.Services;
using GHM.Timekeeping.Domain.IRepository;
using GHM.Timekeeping.Infrastructure.Repository;
using Microsoft.Extensions.Options;

namespace GHM.Timekeeping.Infrastructure.AutofacModules
{
    public class ApplicationModule : Autofac.Module
    {
        public string ConnectionString { get; }
        //public IOptions<ApiUrlSettings> ApiUrlSettings { get; set; }
        //public IOptions<ApiServiceInfo> ApiServiceInfo { get; set; }

        public ApplicationModule(string connectionString
            //, IOptions<ApiUrlSettings> apiUrlSettings, IOptions<ApiServiceInfo> apiServiceInfo
            )
        {
            ConnectionString = connectionString;
            //ApiUrlSettings = apiUrlSettings;
            //ApiServiceInfo = apiServiceInfo;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MongoDbContext>()
                .As<IMongoDbContext>()
                .WithParameter("connectionString", ConnectionString)
                .InstancePerLifetimeScope();

            builder.RegisterType<WorkScheduleRepository>()
                .As<IWorkScheduleRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<MachineRepository>()
                .As<IMachineRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ShiftRepository>()
                .As<IShiftRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ShiftGroupRepository>()
                .As<IShiftGroupRepository>()
                .InstancePerLifetimeScope();
        }
    }
}
