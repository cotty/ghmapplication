﻿namespace GHM.Timekeeping.Infrastructure.Helpers
{
    public class TimeMachineHelper
    {
        //public zkemkeeper.CZKEMClass axCZKEM = new zkemkeeper.CZKEMClass();
        public bool IsConnected { get; set; }
        private readonly int _machineNo;
        private readonly int _enrollNumber;

        public TimeMachineHelper()
        {

        }

        public TimeMachineHelper(int machineNo, int enrollNumber)
        {
            _machineNo = machineNo;
            _enrollNumber = enrollNumber;
        }

        public bool Connect(string ipAddress, int port)
        {
            //IsConnected = axCZKEM.Connect_Net(ipAddress, port);
            return IsConnected;
        }

        /// <summary>
        /// Lấy về số serial của máy chấm công
        /// </summary>
        /// <param name="ipAddress">Địa chỉ IP đến máy chấm công VD: 172.17.108.253</param>
        /// <param name="port">Cổng kết nối đến máy chấm công. Mặc định 4370</param>
        /// <returns></returns>
        public string GetSerialNumber(string ipAddress, int port)
        {
            var machineSerial = "";
            //if (IsConnected)
            //    axCZKEM.GetSerialNumber(1, out machineSerial);
            //else
            //{
            //    IsConnected = Connect(ipAddress, port);
            //    axCZKEM.GetSerialNumber(1, out machineSerial);
            //}
            return machineSerial;
        }

        /// <summary>
        /// Thêm mới người dùng vào máy chấm công.
        /// </summary>        
        /// <param name="ip">Địa chỉ IP của máy chấm công cần thêm</param>
        /// <param name="port">Công kết nối đến máy chấm công</param>
        /// <param name="fullName">Tên người dùng</param>
        /// <param name="cardNumber"></param>
        /// <param name="password">Mật khẩu nếu có</param>
        /// <returns></returns>
        public int SetUserInfo(string ip, int port, string fullName, string cardNumber, string password = "")
        {
            var isConnected = Connect(ip, port);
            if (!isConnected) return -2;

            //axCZKEM.EnableDevice(_machineNo, false);//disable the device

            //if (!string.IsNullOrEmpty(cardNumber))
            //    axCZKEM.SetStrCardNumber(cardNumber); // Before you using function SetUserInfo,set the card number to make sure you can upload it to the device

            //axCZKEM.ReadAllUserID(_machineNo);//read all the user information to the memory                
            //if (axCZKEM.SSR_SetUserInfo(_machineNo, _enrollNumber.ToString(), fullName, password, 0, true))
            //    return _enrollNumber;

            return -1;
        }
    }
}
