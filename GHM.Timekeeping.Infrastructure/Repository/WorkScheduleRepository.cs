﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Domain.Constant;
using GHM.Timekeeping.Domain.IRepository;
using GHM.Timekeeping.Domain.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace GHM.Timekeeping.Infrastructure.Repository
{
    public class WorkScheduleRepository : MongoDbRepositoryBase, IWorkScheduleRepository
    {
        private readonly IMongoDbRepository<WorkSchedule, string> _workScheduleRepository;
        public WorkScheduleRepository(IMongoDbContext context) : base(context)
        {
            _workScheduleRepository = Context.GetRepository<WorkSchedule, string>();
        }

        public IShiftRepository ShiftRepository { get; set; }
        public IShiftGroupRepository ShiftGroupRepository { get; set; }

        public Task<List<WorkSchedule>> Search(string keyword, int officeId, int page, int pageSize, out long totalRows)
        {
            Expression<Func<WorkSchedule, bool>> spec = x => x.OfficeId == officeId && !x.ToDate.HasValue;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.ToLower().StripVietnameseChars();
                spec = spec.And(x => x.UnsignName.ToLower().Contains(keyword));
            }

            var sort = Context.Filters.Sort<WorkSchedule, string>(x => x.Id);
            var paging = Context.Filters.Page<WorkSchedule>(page, pageSize);

            totalRows = _workScheduleRepository.Count(spec);
            return _workScheduleRepository.GetsAsync(spec, sort, paging);
        }

        public async Task<long> Insert(WorkSchedule workSchedule)
        {
            var isExists = await _workScheduleRepository.ExistsAsync(x => x.EnrollNumber == workSchedule.EnrollNumber);
            if (isExists)
                return -1;

            await _workScheduleRepository.AddAsync(workSchedule);
            return 1;
        }

        public async Task<long> Save(WorkSchedule workSchedule)
        {
            var isWorkscheduleExists = await _workScheduleRepository.ExistsAsync(x => x.UserId == workSchedule.UserId);
            if (!isWorkscheduleExists)
                return -1;

            var shiftGroupInfo = await ShiftGroupRepository.GetInfo(workSchedule.ShiftGroupId);
            if (shiftGroupInfo == null)
                return -2;

            workSchedule.ShiftGroupId = shiftGroupInfo.Id;
            workSchedule.ShiftGroupName = shiftGroupInfo.Name;

            foreach (var workScheduleShift in workSchedule.Shifts)
            {
                var shiftInfo = await ShiftRepository.GetInfo(workScheduleShift.Id);
                if (shiftInfo == null)
                    return -3;

                workScheduleShift.Code = shiftInfo.Code;
                workScheduleShift.ReportName = shiftInfo.ReportName;
                workScheduleShift.StartTime = shiftInfo.StartTime;
                workScheduleShift.EndTime = shiftInfo.EndTime;
                workScheduleShift.InLatency = shiftInfo.InLatency;
                workScheduleShift.OutLatency = shiftInfo.OutLatency;
                workScheduleShift.MeaningTime = shiftInfo.MeaningTime;
                workScheduleShift.IsOvertime = shiftInfo.IsOvertime;
                workScheduleShift.ReferenceId = shiftInfo.ReferenceId;
                workScheduleShift.WorkUnit = shiftInfo.WorkUnit;
            }

            // Check if first time update. Just update existing record.
            var isExists = await _workScheduleRepository.ExistsAsync(x => x.UserId == workSchedule.UserId && !x.Shifts.Any() && !x.ToDate.HasValue);
            if (isExists)
            {
                return await _workScheduleRepository.UpdateAsync(x => x.UserId == workSchedule.UserId && !x.Shifts.Any() && !x.ToDate.HasValue,
                    Builders<WorkSchedule>.Update
                        .Set(x => x.ShiftGroupId, workSchedule.ShiftGroupId)
                        .Set(x => x.ShiftGroupName, workSchedule.ShiftGroupName)
                        .Set(x => x.Shifts, workSchedule.Shifts)
                        .Set(x => x.FromDate, DateTime.Now));
            }

            // Update expire date for existing workschedule.
            await _workScheduleRepository.UpdateAsync(x => x.UserId == workSchedule.UserId && !x.ToDate.HasValue,
                Builders<WorkSchedule>.Update.Set(x => x.ToDate, DateTime.Now));

            // Add new workschedule.
            workSchedule.Id = string.Empty;
            await _workScheduleRepository.AddAsync(workSchedule);
            return 1;
        }

        public async Task<long> SaveMany(List<WorkSchedule> workSchedules)
        {
            long result = 0;
            foreach (var workSchedule in workSchedules)
            {
                result += await Save(workSchedule);
            }
            return result;
        }

        /// <summary>
        /// Lấy lịch làm việc hiện tại của nhân viên
        /// </summary>
        /// <param name="enrollNumber">Mã chấm công nhân viên</param>
        /// <returns></returns>
        public async Task<WorkSchedule> GetByEnrollNumber(int enrollNumber)
        {
            return await _workScheduleRepository.GetAsync(x => x.EnrollNumber == enrollNumber && !x.ToDate.HasValue);
        }

        public async Task<List<WorkSchedule>> GetAllByEnrollNumber(int enrollNumber)
        {
            return await _workScheduleRepository.GetsAsync(x => x.EnrollNumber == enrollNumber);
        }

        public async Task<WorkSchedule> GetByEnrollNumberAndCheckInTime(int enrollNumber, DateTime checkInTime)
        {
            //var workSchedule = await _workScheduleRepository.GetAsync(x => x.EnrollNumber == enrollNumber && x.ToDate.HasValue
            //                         && checkInTime >= x.FromDate && checkInTime <= x.ToDate.Value);

            //if (workSchedule != null)
            //    return workSchedule;
            return await _workScheduleRepository.GetAsync(
                x => x.EnrollNumber == enrollNumber && !x.ToDate.HasValue);
        }

        public async Task<WorkSchedule> GetCurrentWorkScheduleByUserId(string userId)
        {
            return await _workScheduleRepository.GetAsync(x => x.UserId.Equals(userId) && !x.ToDate.HasValue);
        }

        public async Task<long> UpdateShift(Shift shift)
        {
            var builder = Builders<WorkSchedule>.Filter;
            var filter = builder.ElemMatch(x => x.Shifts, s => s.Id.Equals(shift.Id)) & builder.Where(x => !x.ToDate.HasValue);
            var update = Builders<WorkSchedule>.Update
                .Set("Shifts.$.ReportName", shift.ReportName)
                .Set("Shifts.$.StartTime", shift.StartTime)
                .Set("Shifts.$.EndTime", shift.EndTime)
                .Set("Shifts.$.MeaningTime", shift.MeaningTime)
                .Set("Shifts.$.Code", shift.Code)
                .Set("Shifts.$.InLatency", shift.InLatency)
                .Set("Shifts.$.OutLatency", shift.OutLatency)
                .Set("Shifts.$.IsOvertime", shift.IsOvertime)
                .Set("Shifts.$.ReferenceId", shift.ReferenceId)
                .Set("Shifts.$.WorkUnit", shift.WorkUnit);
            return await _workScheduleRepository.UpdateAsync(filter, update);
        }

        public async Task<WorkSchedule> GetInfo(string id)
        {
            return await _workScheduleRepository.GetAsync(x => x.Id.Equals(id) && !x.ToDate.HasValue);
        }

        public async Task<bool> CheckIsHasWorkSchedule(int enrollNumber, string shiftId, DateTime checkInTime)
        {
            var workSchedules = await _workScheduleRepository.GetsAsync(x => x.EnrollNumber == enrollNumber);
            var workSchedule = workSchedules.FirstOrDefault(x => x.FromDate <= checkInTime && x.ToDate >= checkInTime);
            if (workSchedule != null)
            {
                return CheckWorkingDay(checkInTime, workSchedule.Shifts.FirstOrDefault(s => s.Id.Equals(shiftId)));
            }

            workSchedule = workSchedules.FirstOrDefault(x => !x.ToDate.HasValue);
            return workSchedule != null && CheckWorkingDay(checkInTime, workSchedule.Shifts.FirstOrDefault(s => s.Id.Equals(shiftId)));
        }

        public async Task<bool> CheckIsWeekLeave(int enrollNumber, string shiftId, DateTime dayCheck)
        {
            var utcDayCheck = DateTime.SpecifyKind(dayCheck, DateTimeKind.Utc);
            var workSchedules = await _workScheduleRepository.GetsAsync(x => x.EnrollNumber == enrollNumber);
            var workSchedule = workSchedules.FirstOrDefault(x => x.FromDate <= utcDayCheck && x.ToDate >= utcDayCheck);
            if (workSchedule != null)
            {
                return !CheckWorkingDay(utcDayCheck, workSchedule.Shifts.FirstOrDefault(s => s.Id.Equals(shiftId)));
            }
            workSchedule = workSchedules.FirstOrDefault(x => !x.ToDate.HasValue);
            return workSchedule != null && !CheckWorkingDay(utcDayCheck, workSchedule.Shifts.FirstOrDefault(s => s.Id.Equals(shiftId)));
        }

        public async Task<bool> CheckShiftExists(string shiftId)
        {
            return await _workScheduleRepository.ExistsAsync(x => x.Shifts.Any(s => s.Id.Equals(shiftId)));
        }

        public async Task<bool> CheckShiftGroupExists(string shiftGroupId)
        {
            return await _workScheduleRepository.ExistsAsync(x => x.Id.Equals(shiftGroupId));
        }

        public async Task<long> CountShiftsByEnrollNumber(int enrollNumber)
        {
            return await _workScheduleRepository.CountAysnc(x => x.EnrollNumber == enrollNumber);
        }

        public async Task<WorkSchedule> GetMyWorkSchedule(string userId)
        {
            return await _workScheduleRepository.GetAsync(x => x.UserId.Equals(userId) && !x.ToDate.HasValue);
        }

        public List<ShiftReference> GetMyWorkScheduleShift(string userId)
        {
            var shiftGroupReference = _workScheduleRepository.Raw.FirstOrDefault(x => x.UserId.Equals(userId) && !x.ToDate.HasValue);
            return shiftGroupReference?.Shifts;
        }

        public async Task<long> UpdateOfficeInfo(int officeId, string officeName)
        {
            var filter = Builders<WorkSchedule>.Filter.Where(x => x.OfficeId == officeId);
            var update = Builders<WorkSchedule>.Update
                .Set(x => x.OfficeName, officeName);
            return await _workScheduleRepository.UpdateAsync(filter, update);
        }

        public async Task<WorkSchedule> GetByUserId(string userId)
        {
            return await _workScheduleRepository.GetAsync(x => x.UserId.Equals(userId) && !x.ToDate.HasValue);
        }

        private static bool CheckWorkingDay(DateTime checkInTime, ShiftReference shift)
        {
            if (shift == null) return false;
            switch (checkInTime.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    return (shift.WorkingDaysValue & WorkingDayValue.Monday) == WorkingDayValue.Monday;
                case DayOfWeek.Tuesday:
                    return (shift.WorkingDaysValue & WorkingDayValue.Tuesday) == WorkingDayValue.Tuesday;
                case DayOfWeek.Wednesday:
                    return (shift.WorkingDaysValue & WorkingDayValue.Wednesday) == WorkingDayValue.Wednesday;
                case DayOfWeek.Thursday:
                    return (shift.WorkingDaysValue & WorkingDayValue.Thursday) == WorkingDayValue.Thursday;
                case DayOfWeek.Friday:
                    return (shift.WorkingDaysValue & WorkingDayValue.Friday) == WorkingDayValue.Friday;
                case DayOfWeek.Saturday:
                    return (shift.WorkingDaysValue & WorkingDayValue.Saturday) == WorkingDayValue.Saturday;
                case DayOfWeek.Sunday:
                    return (shift.WorkingDaysValue & WorkingDayValue.Sunday) == WorkingDayValue.Sunday;
                default:
                    return false;
            }
        }
    }
}
