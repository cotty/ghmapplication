﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Domain.IRepository;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Infrastructure.Repository
{
    public class CheckInCheckOutAggregateRepository : MongoDbRepositoryBase, ICheckInCheckOutAggregateRepository
    {
        private readonly IMongoDbRepository<ReportByShift, string> _checkInCheckOutAggregateRepository;
        public CheckInCheckOutAggregateRepository(IMongoDbContext context) : base(context)
        {
            _checkInCheckOutAggregateRepository = Context.GetRepository<ReportByShift, string>();
        }

        public async Task<string> Insert(ReportByShift aggregate)
        {
            var isExists = await CheckExists(aggregate.Day, aggregate.Month, aggregate.Year);
            if (isExists)
            {
                // 
            }
            await _checkInCheckOutAggregateRepository.AddAsync(aggregate);
            return aggregate.Id;
        }

        public async Task<bool> CheckExists(int day, int month, int year)
        {
            return await _checkInCheckOutAggregateRepository.ExistsAsync(
                x => x.Day == day && x.Month == month && x.Year == year);
        }
    }
}
