﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Domain.IRepository;
using GHM.Timekeeping.Domain.ModelMetas;
using GHM.Timekeeping.Domain.Models;
using MongoDB.Driver;

namespace GHM.Timekeeping.Infrastructure.Repository
{
    public class ConfigHolidayRepository : MongoDbRepositoryBase, IConfigHolidayRepository
    {
        private readonly IMongoDbRepository<Holidays, string> _timekeepingConfigHolidayRepository;
        public ConfigHolidayRepository(IMongoDbContext context) : base(context)
        {
            _timekeepingConfigHolidayRepository = context.GetRepository<Holidays, string>();
        }

        public async Task<Holidays> Insert(TimekeepingHolidaysMeta timekeepingHoliday)
        {
            var isExists = await CheckExistsByDateRange(timekeepingHoliday.Id, timekeepingHoliday.FromDay,
                timekeepingHoliday.ToDay, timekeepingHoliday.Year);
            if (isExists)
                return null;

            return await _timekeepingConfigHolidayRepository.AddAsync(new Holidays
            {
                Name = timekeepingHoliday.Name,
                FromDay = timekeepingHoliday.FromDay,
                ToDay = timekeepingHoliday.ToDay,
                IsActive = timekeepingHoliday.IsActive,
                IsRangerDate = timekeepingHoliday.ToDay != null,
                Year = timekeepingHoliday.Year
            });
        }

        public async Task<long> Update(TimekeepingHolidaysMeta timekeepingHoliday)
        {
            var isExists = await CheckExistsByDateRange(timekeepingHoliday.Id, timekeepingHoliday.FromDay,
                timekeepingHoliday.ToDay, timekeepingHoliday.Year);
            if (isExists)
                return -1;

            var info = await _timekeepingConfigHolidayRepository.GetAsync(x => x.Id.Equals(timekeepingHoliday.Id));
            if (info == null)
                return -2;

            if (info.Year < DateTime.Today.Year)
                return -3;

            info.Name = timekeepingHoliday.Name;
            info.FromDay = timekeepingHoliday.FromDay;
            info.ToDay = timekeepingHoliday.ToDay;
            info.IsRangerDate = timekeepingHoliday.ToDay?.Day != null && timekeepingHoliday.ToDay.Month.HasValue;
            info.IsActive = timekeepingHoliday.IsActive;
            return await _timekeepingConfigHolidayRepository.UpdateAsync(info);
        }

        public async Task<long> Delete(string id)
        {
            var info = await _timekeepingConfigHolidayRepository.GetAsync(x => x.Id.Equals(id) && !x.IsDelete);
            if (info == null)
                return -1;

            if (info.Year < DateTime.Today.Year)
            {
                return -2;
            }

            info.IsDelete = true;
            return await _timekeepingConfigHolidayRepository.UpdateAsync(x => x.Id.Equals(id), Builders<Holidays>.Update.Set(x => x.IsDelete, true));
        }

        public async Task<List<T>> SearchAll<T>(Expression<Func<Holidays, T>> projector)
        {
            return await _timekeepingConfigHolidayRepository.GetsAsAsync(projector, x => !x.IsDelete);
        }

        public async Task<List<Holidays>> SearchAll(int year, bool? isActive = null)
        {
            Expression<Func<Holidays, bool>> spec = x => !x.IsDelete && x.Year == year;
            if (isActive.HasValue)
            {
                spec = spec.And(x => x.IsActive == isActive.Value);
            }
            return await _timekeepingConfigHolidayRepository.GetsAsync(spec);
        }

        public async Task<long> UpdateActiveStatus(string id, bool isActive)
        {
            return await _timekeepingConfigHolidayRepository.UpdateAsync(x => x.Id.Equals(id),
                Builders<Holidays>.Update.Set("isActive", isActive));
        }

        public async Task<bool> CheckExists(int day, int month, int year)
        {
            return await _timekeepingConfigHolidayRepository.ExistsAsync(
                x => x.Year == year && x.IsActive && !x.IsDelete && ((x.FromDay.Day.HasValue && x.FromDay.Day.Value == day && x.FromDay.Month.HasValue &&
                       x.FromDay.Month.Value == month)
                      || (x.FromDay.Day.HasValue && x.FromDay.Day.Value <= day && x.FromDay.Month.HasValue &&
                          x.FromDay.Month.Value <= month
                          && x.ToDay.Day.HasValue && x.ToDay.Day.Value >= day && x.ToDay.Month.HasValue &&
                          x.ToDay.Month.Value >= month))
            );

        }

        public async Task<List<Holidays>> SearchByMonthAndYear(int month, int year)
        {
            return await _timekeepingConfigHolidayRepository.GetsAsync(
                x => !x.IsDelete && x.IsActive && (x.FromDay.Month == month || x.ToDay.Month == month) &&
                     x.Year == year);
        }

        private async Task<bool> CheckExistsByDateRange(string id, DayObject fromDay, DayObject toDay, int year)
        {
            if (fromDay == null && toDay == null)
                return false;

            if (toDay == null)
                return await _timekeepingConfigHolidayRepository.ExistsAsync(x => !x.Id.Equals(id) && x.FromDay.Day == fromDay.Day && x.ToDay.Month == fromDay.Month
                                                                                  && x.Year == year);

            return await _timekeepingConfigHolidayRepository.ExistsAsync(x => !x.Id.Equals(id) && fromDay.Day >= x.FromDay.Day && x.FromDay.Month == fromDay.Month &&
                                                                              toDay.Day <= x.ToDay.Month && toDay.Month == x.ToDay.Month && x.Year == year);
        }
    }
}
