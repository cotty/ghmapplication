﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Domain.Constant;
using GHM.Timekeeping.Domain.IRepository;
using GHM.Timekeeping.Domain.Models;
using MongoDB.Driver;

namespace GHM.Timekeeping.Infrastructure.Repository
{
    public class CheckInCheckOutHistoryRepository : MongoDbRepositoryBase, ICheckInCheckOutHistoryRepository
    {
        private readonly IMongoDbRepository<CheckInCheckOutHistory, string> _checkInCheckOutHistoryRepository;
        private readonly IConfigHolidayRepository _holidayRepository;
        private readonly IShiftRepository _shiftRepository;
        private readonly IWorkScheduleRepository _workScheduleRepository;

        public CheckInCheckOutHistoryRepository(IMongoDbContext context, IConfigHolidayRepository holidayRepository, IShiftRepository shiftRepository, IWorkScheduleRepository workScheduleRepository) : base(context)
        {
            _holidayRepository = holidayRepository;
            _shiftRepository = shiftRepository;
            _workScheduleRepository = workScheduleRepository;
            _checkInCheckOutHistoryRepository = Context.GetRepository<CheckInCheckOutHistory, string>();
        }

        public async Task<long> Save(CheckInCheckOutHistory history)
        {
            var isExists = await CheckExists(history.EnrollNumber, history.Year, history.Month, history.Day,
                history.Hour, history.Minute, history.Second, history.ShiftId);

            // Nếu tồn tại rồi cập nhật lại, chưa có thêm mới.
            if (isExists)
                return await Update(history);

            return await Insert(history);
        }

        public async Task<long> Insert(CheckInCheckOutHistory history)
        {
            await _checkInCheckOutHistoryRepository.AddAsync(history);
            return 1;
        }

        public async Task<long> Update(CheckInCheckOutHistory history)
        {
            return await _checkInCheckOutHistoryRepository.UpdateAsync(
                x => x.EnrollNumber == history.EnrollNumber && x.Year == history.Year
                     && x.Month == history.Month && x.Day == history.Day && x.Hour == history.Hour &&
                     x.Minute == history.Minute && x.Second == history.Second && x.ShiftId.Equals(history.ShiftId), history);
        }

        public async Task<bool> CheckExists(int enrollNumber, int year, int month, int day, int hour, int minute, int second, string shiftId)
        {
            return await _checkInCheckOutHistoryRepository.ExistsAsync(
                x => x.EnrollNumber == enrollNumber && x.Year == year && x.Month == month && x.Day == day && x.Hour == hour && x.Minute == minute &&
                     x.Second == second && x.ShiftId.Equals(shiftId));
        }

        public async Task<bool> CheckIsHasCheckIn(int enrollNumber, string shiftId, int day, int month, int year)
        {
            return await _checkInCheckOutHistoryRepository.ExistsAsync(
                x => x.EnrollNumber == enrollNumber && x.ShiftId.Equals(shiftId) && x.Day == day
                     && x.Month == month && x.Year == year && x.IsCheckIn);
        }

        public async Task<bool> CheckExistsByDayAndShift(DateTime day, string shiftId)
        {
            return await _checkInCheckOutHistoryRepository.ExistsAsync(
                x => x.Day == day.Day && x.Month == day.Month && x.Year == day.Year && x.ShiftId == x.Id);
        }

        public async Task<CheckInCheckOutHistory> GetInfo(string id)
        {
            return await _checkInCheckOutHistoryRepository.GetAsync(x => x.Id.Equals(id));
        }

        public async Task<long> AddNewEndShiftReference(string referenceId, int enrollNumber, string machineId, int day, int month, int year)
        {
            var referenceInfo = await _shiftRepository.GetInfo(referenceId);
            if (referenceInfo == null)
                return -1;

            var checkInTime = new DateTime(year, month, day, referenceInfo.EndTime.Hour,
                referenceInfo.EndTime.Minute, referenceInfo.EndTime.Second);

            // Kiểm tra ngày chấm công có thuộc ca làm việc hay không.
            var isExistsInWorkingSchedule = await _workScheduleRepository.CheckIsHasWorkSchedule(enrollNumber, referenceId, checkInTime);
            if (!isExistsInWorkingSchedule)
                return -2;

            var isHoliday = await _holidayRepository.CheckExists(checkInTime.Day, checkInTime.Month, checkInTime.Year);

            var isEndShiftExists = await _checkInCheckOutHistoryRepository.ExistsAsync(
                x => x.ShiftId.Equals(referenceId) && x.Day == checkInTime.Day
                     && x.Month == checkInTime.Month && x.Year == checkInTime.Year);

            var checkInCheckOut = new CheckInCheckOutHistory
            {
                CheckInTime = DateTime.Now,
                Day = day,
                Month = month,
                Year = year,
                EnrollNumber = enrollNumber,
                Hour = checkInTime.Hour,
                Minute = checkInTime.Minute,
                Second = checkInTime.Second,
                IsSunday = checkInTime.DayOfWeek == DayOfWeek.Sunday,
                IsHoliday = isHoliday,
                IsCheckIn = false,
                MachineId = machineId,
                Method = CheckInCheckOutMethod.Auto,
                ShiftId = referenceInfo.Id,
                WorkUnit = referenceInfo.WorkUnit
            };

            // Nếu đã tồn tại cập nhật lại thông tin đã có thành giờ kết thúc ca ra.
            if (isEndShiftExists)
            {
                return await _checkInCheckOutHistoryRepository.UpdateAsync(x => x.ShiftId.Equals(referenceId) && x.Day == checkInTime.Day
                                                                                && x.Month == checkInTime.Month && x.Year == checkInTime.Year && !x.IsCheckIn
                                                                                , checkInCheckOut);
            }

            // Nếu tồn tại thời gian chấm công vào thì mới tự động thêm chấm công ra.
            return await Insert(checkInCheckOut);
        }

        public async Task<List<CheckInCheckOutHistory>> GetsForAggregate(int enrollNumber, string shiftId, int day, int month, int year)
        {
            return await _checkInCheckOutHistoryRepository.GetsAsync(
                x => x.EnrollNumber == enrollNumber && x.Day == day && x.Month == month && x.Year == year && x.ShiftId.Equals(shiftId));
        }

        public async Task<List<CheckInCheckOutHistory>> GetCheckInCheckOutHistory(int enrollNumber, int day, int month, int year)
        {
            var sort = Context.Filters.Sort<CheckInCheckOutHistory, DateTime>(x => x.CheckInTime);
            return await _checkInCheckOutHistoryRepository.GetsAsync(
                x => x.EnrollNumber == enrollNumber && x.Day == day && x.Month == month && x.Year == year, sort);
        }

        public async Task<decimal> CountHolidays(int day, int month, int year, int enrollNumber)
        {
            var listCheckInCheckOut = await _checkInCheckOutHistoryRepository.GetsAsync(
                x => x.EnrollNumber == enrollNumber && x.Day == day && x.Month == month && x.Year == year && x.IsHoliday);
            return listCheckInCheckOut.GroupBy(x => new { x.ShiftId, x.WorkUnit })
                .Sum(x => x.Key.WorkUnit);
        }
    }
}
