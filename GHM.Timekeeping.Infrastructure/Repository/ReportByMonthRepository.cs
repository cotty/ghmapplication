﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Domain.Constant;
using GHM.Timekeeping.Domain.IRepository;
using GHM.Timekeeping.Domain.Models;
using MongoDB.Driver;

namespace GHM.Timekeeping.Infrastructure.Repository
{
    public class ReportByMonthRepository : MongoDbRepositoryBase, IReportByMonthRepository
    {
        private readonly IMongoDbRepository<ReportByMonth, string> _reportByMonthRepository;
        private readonly IConfigHolidayRepository _holidayRepository;
        private readonly ICheckInCheckOutHistoryRepository _checkInCheckOutHistoryRepository;
        private readonly IWorkScheduleRepository _workScheduleRepository;
        public IReportByShiftRepository ReportByShiftRepository { get; set; }

        public ReportByMonthRepository(IMongoDbContext context, IConfigHolidayRepository holidayRepository, IWorkScheduleRepository workScheduleRepository, ICheckInCheckOutHistoryRepository checkInCheckOutHistoryRepository) : base(context)
        {
            _holidayRepository = holidayRepository;
            _workScheduleRepository = workScheduleRepository;
            _checkInCheckOutHistoryRepository = checkInCheckOutHistoryRepository;
            _reportByMonthRepository = Context.GetRepository<ReportByMonth, string>();
        }

        public async Task<long> Save(ReportByMonth reportByMonth)
        {
            // Check Exists
            var isExists = await CheckExists(reportByMonth.EnrollNumber, reportByMonth.Month, reportByMonth.Year);
            if (isExists)
            {
                var filter = Builders<ReportByMonth>.Filter.Where(x => x.Month == reportByMonth.Month && x.Year == reportByMonth.Year && x.EnrollNumber == reportByMonth.EnrollNumber);
                var update = Builders<ReportByMonth>.Update
                    .Set("ReportShiftAggregates", reportByMonth.ReportShiftAggregates)
                    .Set("TotalSundays", reportByMonth.TotalSundays)
                    .Set("TotalNormalDays", reportByMonth.TotalNormalDays)
                    .Set("TotalHolidays", reportByMonth.TotalHolidays)
                    .Set("TotalOvertime", reportByMonth.TotalOvertime)
                    .Set("TotalAnnualLeave", reportByMonth.TotalAnnualLeave)
                    .Set("TotalUnpaidLeave", reportByMonth.TotalUnpaidLeave)
                    .Set("TotalCompensatory", reportByMonth.TotalCompensatory)
                    .Set("TotalInsuranceLeave", reportByMonth.TotalInsuranceLeave)
                    .Set("TotalEntitlement", reportByMonth.TotalEntitlement)
                    .Set("TotalHolidaysLeave", reportByMonth.TotalHolidaysLeave)
                    .Set("TotalInvalidWorkingDays", reportByMonth.TotalInvalidWorkingDays)
                    .Set("TotalDaysValidMeal", reportByMonth.TotalDaysValidMeal)
                    .Set("TotalInLateMin", reportByMonth.TotalInLateMin)
                    .Set("TotalOutLateMin", reportByMonth.TotalOutLateMin)
                    .Set("TotalInSoonMin", reportByMonth.TotalInSoonMin)
                    .Set("TotalOutSoonMin", reportByMonth.TotalOutSoonMin)
                    .Set("TotalDayOff", reportByMonth.TotalDayOff)
                    .Set("TotalInvalidWorkSchedule", reportByMonth.TotalInvalidWorkSchedule)
                    .Set("TotalInLatencyMin", reportByMonth.TotalInLatencyMin)
                    .Set("TotalOutLatencyMin", reportByMonth.TotalOutLatencyMin);

                return await _reportByMonthRepository.UpdateAsync(filter, update);
            }

            await _reportByMonthRepository.AddAsync(reportByMonth);
            return 1;
        }

        public async Task Aggregate(int enrollNumber, int month, int year)
        {
            var listReportByShifts = await ReportByShiftRepository.GetListReportByShiftForAggregateByUser(enrollNumber, month, year);
            var groupShiftReports = listReportByShifts.GroupBy(x => new { x.EnrollNumber, x.UserId, x.FullName, x.UnsignName, x.OfficeId, x.OfficeIdPath, x.Month, x.Year });
            foreach (var groupShiftReport in groupShiftReports)
            {
                // Tổng số ngày nghỉ tuần
                decimal totalDayOff = await CalculateTotalDayOff(groupShiftReport.Key.EnrollNumber, groupShiftReport.Key.Year, groupShiftReport.Key.Month);

                var listShift = groupShiftReport.GroupBy(x => new { x.ShiftId, x.ShiftCode });
                var listShiftAggregate = listShift.Select(x => new ReportShiftAggregate
                {
                    ShiftId = x.Key.ShiftId,
                    ShiftCode = x.Key.ShiftCode,
                    TotalDays = x.Where(t => !t.IsSunday && !t.IsHoliday && !t.Method.HasValue).Sum(t => t.WorkUnit)
                });

                var totalAnnualLeave = groupShiftReport.Where(x => x.Method.HasValue && x.Method.Value == DayOffMethod.AnnualLeave).Sum(x => x.WorkUnit);
                var totalUnpaidLeave = groupShiftReport.Where(x => x.Method.HasValue && x.Method.Value == DayOffMethod.UnpaidLeave).Sum(x => x.WorkUnit);
                var totalInsuranceLeave = groupShiftReport.Where(x => x.Method.HasValue && x.Method.Value == DayOffMethod.InsuranceLeave).Sum(x => x.WorkUnit);
                var totalCompensatory = groupShiftReport.Where(x => x.Method.HasValue && x.Method.Value == DayOffMethod.Compensatory).Sum(x => x.WorkUnit);
                var totalEntitlement = groupShiftReport.Where(x => x.Method.HasValue && x.Method.Value == DayOffMethod.Entitlement).Sum(x => x.WorkUnit);
                var totalInvalidWorkingDays = groupShiftReport.Where(x => !x.IsValid).Sum(x => x.WorkUnit);
                var totalValidMeal = groupShiftReport.Where(x => !x.Method.HasValue).ToList().GroupBy(x => x.Day).Select(x => new
                {
                    Day = x.Key,
                    TotalWorkingMin = x.Sum(t => t.TotalWorkingMin + t.TotalOvertimeMin)
                }).Count(x => x.TotalWorkingMin > 270);
                var totalInValidWorkSchedule = groupShiftReport.Where(x => !x.IsValidWorkSchedule).Sum(x => x.WorkUnit);
                var totalInLateMin = groupShiftReport.Sum(x => x.InLateMin);
                var totalInSoonMin = groupShiftReport.Sum(x => x.InSoonMin);
                var totalOutLateMin = groupShiftReport.Sum(x => x.OutLateMin);
                var totalOutSoonMin = groupShiftReport.Sum(x => x.OutSoonMin);

                var monthReport = new ReportByMonth
                {
                    EnrollNumber = groupShiftReport.Key.EnrollNumber,
                    UserId = groupShiftReport.Key.UserId,
                    FullName = groupShiftReport.Key.FullName,
                    UnsignName = groupShiftReport.Key.UnsignName,
                    OfficeId = groupShiftReport.Key.OfficeId,
                    OfficeIdPath = groupShiftReport.Key.OfficeIdPath,
                    Month = groupShiftReport.Key.Month,
                    Year = groupShiftReport.Key.Year,
                    TotalNormalDays = groupShiftReport.Where(x => !x.IsSunday && !x.IsHoliday && !x.IsOvertime && !x.Method.HasValue).Sum(x => x.WorkUnit),
                    TotalSundays = groupShiftReport.Where(x => x.IsSunday && !x.IsOvertime && !x.Method.HasValue).Sum(x => x.WorkUnit),
                    TotalHolidays = groupShiftReport.Where(x => x.IsHoliday && !x.IsOvertime && !x.Method.HasValue).Sum(x => x.WorkUnit),
                    TotalOvertime = groupShiftReport.Where(x => x.IsOvertime && !x.Method.HasValue).Sum(x => x.TotalWorkingMin) + groupShiftReport.Where(x => !x.Method.HasValue).Sum(x => x.TotalOvertimeMin),
                    ReportShiftAggregates = listShiftAggregate.ToList(),
                    TotalAnnualLeave = totalAnnualLeave,
                    TotalUnpaidLeave = totalUnpaidLeave,
                    TotalInsuranceLeave = totalInsuranceLeave,
                    TotalCompensatory = totalCompensatory,
                    TotalEntitlement = totalEntitlement,
                    TotalInvalidWorkingDays = totalInvalidWorkingDays,
                    TotalDaysValidMeal = totalValidMeal,
                    TotalInLateMin = totalInLateMin,
                    TotalInSoonMin = totalInSoonMin,
                    TotalOutLateMin = totalOutLateMin,
                    TotalOutSoonMin = totalOutSoonMin,
                    DaysInMonth = DateTime.DaysInMonth(year, month),
                    TotalInvalidWorkSchedule = totalInValidWorkSchedule
                };

                // Lấy về tổng số ngày nghỉ lễ trong tháng
                decimal totalHolidaysLeave;
                decimal totalAdditionDayOff;

                CalculateTotalHolidayInMonth(groupShiftReport.Key.EnrollNumber, groupShiftReport.Key.Year, groupShiftReport.Key.Month,
                    out totalHolidaysLeave, out totalAdditionDayOff);

                monthReport.TotalDayOff = totalDayOff + totalAdditionDayOff;
                monthReport.TotalHolidaysLeave = totalHolidaysLeave;

                // Save report by month into database
                await Save(monthReport);
            }
        }

        public async Task<ReportByMonth> GetInfo(int enrollNumber, int month, int year)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> CheckExists(int enrollNumber, int month, int year)
        {
            return await _reportByMonthRepository.ExistsAsync(
                x => x.EnrollNumber == enrollNumber && x.Month == month && x.Year == year);
        }

        public async Task<List<ReportByMonth>> Search(string keyword, int officeId, int month, int year)
        {
            Expression<Func<ReportByMonth, bool>> spec =
                x => x.OfficeId == officeId && x.Month == month && x.Year == year;

            var sort = Context.Filters.Sort<ReportByMonth, string>(a => a.UserId);

            if (string.IsNullOrEmpty(keyword)) return await _reportByMonthRepository.GetsAsync(spec, sort);

            keyword = keyword.ToLower().StripVietnameseChars();
            spec = spec.And(x => x.UnsignName.ToLower().Contains(keyword));
            return await _reportByMonthRepository.GetsAsync(spec, sort);
        }

        public async Task<List<ReportByMonth>> SearchMyResult(string userId, byte? month, int year)
        {
            Expression<Func<ReportByMonth, bool>> spec =
                x => x.Year == year && x.UserId.Equals(userId);

            if (month.HasValue)
            {
                spec = spec.And(x => x.Month == month.Value);
            }
            return await _reportByMonthRepository.GetsAsync(spec);
        }

        private void CalculateTotalHolidayInMonth(int enrollNumber, int year, int month
            , out decimal totalHolidayLeave, out decimal additionDayOff)
        {
            var listHolidayInMonth = Task.Run(() => _holidayRepository.SearchByMonthAndYear(month, year)).Result;
            var daysInMonth = DateTime.DaysInMonth(year, month);
            List<DateTime> listHolidays = new List<DateTime>();

            listHolidayInMonth.ForEach(holiday =>
            {
                if (holiday.ToDay.Day.HasValue && holiday.ToDay.Month.HasValue)
                {
                    var fromDate = new DateTime(year, holiday.FromDay.Month.Value, holiday.FromDay.Day.Value);
                    var toDate = new DateTime(year, holiday.ToDay.Month.Value, holiday.ToDay.Day.Value);

                    // Nếu ngày nghỉ sang tháng sau. Đến ngày sẽ bằng ngày cuối cùng của tháng này.				
                    if (holiday.ToDay.Month > month)
                    {
                        toDate = new DateTime(year, month, daysInMonth);
                    }

                    // Nếu ngày bắt đầu nghỉ là tháng trước sẽ tính ngày bắt đầu là mùng 1 của tháng này.
                    if (holiday.FromDay.Month < month)
                    {
                        fromDate = new DateTime(year, month, 1);
                    }

                    var dayCount = (decimal)Math.Ceiling((toDate.AddDays(1) - fromDate).TotalDays);

                    // Kiểm tra ngày nghỉ lễ có trùng vào ngày nghỉ tuần không. Nếu có công thêm 1 ngày nghỉ vào tổng số ngày nghỉ.									
                    if (dayCount < 1) return;
                    for (var i = 0; i < dayCount; i++)
                    {
                        var nextDate = fromDate.AddDays(i);

                        listHolidays.Add(nextDate);
                    }
                }
                else
                {
                    if (!holiday.FromDay.Day.HasValue || !holiday.FromDay.Month.HasValue)
                        return;

                    var aggregateDay = new DateTime(year, holiday.FromDay.Month.Value, holiday.FromDay.Day.Value);
                    listHolidays.Add(aggregateDay);
                }
            });

            var totalHoliday = listHolidays.Count;
            decimal totalDaysWorkingOnHolidays = 0;
            decimal dayOff = 0;
            foreach (var holiday in listHolidays)
            {
                dayOff += Task.Run(() => CalculateAdditionDayOff(holiday, enrollNumber)).Result;
                totalDaysWorkingOnHolidays += Task.Run(() => _checkInCheckOutHistoryRepository.CountHolidays(holiday.Day, holiday.Month, holiday.Year, enrollNumber)).Result;
            }

            additionDayOff = dayOff;
            totalHolidayLeave = totalHoliday - totalDaysWorkingOnHolidays;
        }

        private async Task<decimal> CalculateAdditionDayOff(DateTime date, int enrollNumber)
        {
            decimal totalAdditionDayOff = 0;
            var workScheduleInfo = await _workScheduleRepository.GetByEnrollNumber(enrollNumber);
            if (workScheduleInfo.Shifts == null)
                return 0;

            workScheduleInfo.Shifts.ForEach(shift =>
            {
                var workingDayValues = shift.WorkingDaysValue;
                if (date.DayOfWeek == DayOfWeek.Monday && (workingDayValues & WorkingDayValue.Monday) != WorkingDayValue.Monday)
                {
                    totalAdditionDayOff += shift.WorkUnit;
                }

                if (date.DayOfWeek == DayOfWeek.Tuesday && (workingDayValues & WorkingDayValue.Tuesday) != WorkingDayValue.Tuesday)
                {
                    totalAdditionDayOff += shift.WorkUnit;
                }

                if (date.DayOfWeek == DayOfWeek.Wednesday && (workingDayValues & WorkingDayValue.Wednesday) != WorkingDayValue.Wednesday)
                {
                    totalAdditionDayOff += shift.WorkUnit;
                }

                if (date.DayOfWeek == DayOfWeek.Thursday && (workingDayValues & WorkingDayValue.Thursday) != WorkingDayValue.Thursday)
                {
                    totalAdditionDayOff += shift.WorkUnit;
                }

                if (date.DayOfWeek == DayOfWeek.Friday && (workingDayValues & WorkingDayValue.Friday) != WorkingDayValue.Friday)
                {
                    totalAdditionDayOff += shift.WorkUnit;
                }

                if (date.DayOfWeek == DayOfWeek.Saturday && (workingDayValues & WorkingDayValue.Saturday) != WorkingDayValue.Saturday)
                {
                    totalAdditionDayOff += shift.WorkUnit;
                }

                if (date.DayOfWeek == DayOfWeek.Sunday && (workingDayValues & WorkingDayValue.Sunday) != WorkingDayValue.Sunday)
                {
                    totalAdditionDayOff += shift.WorkUnit;
                }
            });

            return totalAdditionDayOff;
        }

        private async Task<decimal> CalculateTotalDayOff(int enrollNumber, int year, int month)
        {
            var workSchedule = await _workScheduleRepository.GetByEnrollNumber(enrollNumber);
            if (workSchedule.Shifts == null)
                return 0;

            decimal totalDayOff = 0;
            workSchedule.Shifts.ForEach(shift =>
            {
                var workingDayValues = shift.WorkingDaysValue;
                if ((workingDayValues & WorkingDayValue.Monday) != WorkingDayValue.Monday)
                {
                    var totalMondayInMonth = DateTimeHelper.NumberOfParticularDaysInMonth(year, month, DayOfWeek.Monday);
                    totalDayOff += totalMondayInMonth * shift.WorkUnit;
                }

                if ((workingDayValues & WorkingDayValue.Tuesday) != WorkingDayValue.Tuesday)
                {
                    var totalMondayInMonth = DateTimeHelper.NumberOfParticularDaysInMonth(year, month, DayOfWeek.Tuesday);
                    totalDayOff += totalMondayInMonth * shift.WorkUnit;
                }

                if ((workingDayValues & WorkingDayValue.Wednesday) != WorkingDayValue.Wednesday)
                {
                    var totalMondayInMonth = DateTimeHelper.NumberOfParticularDaysInMonth(year, month, DayOfWeek.Wednesday);
                    totalDayOff += totalMondayInMonth * shift.WorkUnit;
                }

                if ((workingDayValues & WorkingDayValue.Thursday) != WorkingDayValue.Thursday)
                {
                    var totalMondayInMonth = DateTimeHelper.NumberOfParticularDaysInMonth(year, month, DayOfWeek.Thursday);
                    totalDayOff += totalMondayInMonth * shift.WorkUnit;
                }

                if ((workingDayValues & WorkingDayValue.Friday) != WorkingDayValue.Friday)
                {
                    var totalMondayInMonth = DateTimeHelper.NumberOfParticularDaysInMonth(year, month, DayOfWeek.Friday);
                    totalDayOff += totalMondayInMonth * shift.WorkUnit;
                }

                if ((workingDayValues & WorkingDayValue.Saturday) != WorkingDayValue.Saturday)
                {
                    var totalMondayInMonth = DateTimeHelper.NumberOfParticularDaysInMonth(year, month, DayOfWeek.Saturday);
                    totalDayOff += totalMondayInMonth * shift.WorkUnit;
                }

                if ((workingDayValues & WorkingDayValue.Sunday) != WorkingDayValue.Sunday)
                {
                    var totalMondayInMonth = DateTimeHelper.NumberOfParticularDaysInMonth(year, month, DayOfWeek.Sunday);
                    totalDayOff += totalMondayInMonth * shift.WorkUnit;
                }
            });

            return totalDayOff;
        }
    }
}
