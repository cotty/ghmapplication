﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Domain.IRepository;
using GHM.Timekeeping.Domain.Models;
using MongoDB.Driver;

namespace GHM.Timekeeping.Infrastructure.Repository
{
    public class ShiftGroupRepository : MongoDbRepositoryBase, IShiftGroupRepository
    {
        private readonly IMongoDbRepository<ShiftGroup, string> _shiftGroupRepository;
        private readonly IWorkScheduleRepository _workScheduleRepository;
        public ShiftGroupRepository(IMongoDbContext context, IWorkScheduleRepository workScheduleRepository) : base(context)
        {
            workScheduleRepository.ShiftGroupRepository = this;
            _workScheduleRepository = workScheduleRepository;
            _shiftGroupRepository = Context.GetRepository<ShiftGroup, string>();
        }

        public async Task<ShiftGroup> Insert(ShiftGroup shiftGroup)
        {
            var nameExists = await CheckNameExists(shiftGroup.Id, shiftGroup.Name);
            if (nameExists)
                return null;

            return await _shiftGroupRepository.AddAsync(shiftGroup);
        }

        public async Task<long> Update(ShiftGroup shiftGroup)
        {
            var nameExists = await CheckNameExists(shiftGroup.Id, shiftGroup.Name);
            if (nameExists)
                return -1;

            return await _shiftGroupRepository.UpdateAsync(x => x.Id.Equals(shiftGroup.Id),
                Builders<ShiftGroup>.Update
                    .Set(x => x.Description, shiftGroup.Description)
                    .Set(x => x.Name, shiftGroup.Name)
                    .Set(x => x.IsActive, shiftGroup.IsActive)
                    .Set(x => x.Shifts, shiftGroup.Shifts));
        }

        public async Task<long> UpdateActive(string id, bool isActive)
        {
            return await _shiftGroupRepository.UpdateAsync(x => x.Id.Equals(id),
                Builders<ShiftGroup>.Update.Set("isActive", isActive));
        }

        public async Task<long> Delete(string id)
        {
            var isInUse = await _workScheduleRepository.CheckShiftGroupExists(id);
            if (isInUse)
                return -1;

            return await _shiftGroupRepository.UpdateAsync(x => x.Id.Equals(id), Builders<ShiftGroup>.Update.Set("isDelete", true));
        }

        public async Task<ShiftGroup> GetInfo(string id)
        {
            return await _shiftGroupRepository.GetAsync(id);
        }

        public async Task<long> UpdateShift(Shift shift)
        {
            var filter = Builders<ShiftGroup>.Filter.ElemMatch(x => x.Shifts, s => s.Id.Equals(shift.Id));
            var update = Builders<ShiftGroup>.Update
                .Set("Shifts.$.Name", shift.Name)
                .Set("Shifts.$.StartTime", shift.StartTime)
                .Set("Shifts.$.EndTime", shift.EndTime)
                .Set("Shifts.$.MeaningTime", shift.MeaningTime)
                .Set("Shifts.$.Code", shift.Code)
                .Set("Shifts.$.InLatency", shift.InLatency)
                .Set("Shifts.$.OutLatency", shift.OutLatency)
                .Set("Shifts.$.IsOvertime", shift.IsOvertime)
                .Set("Shifts.$.ReferenceId", shift.ReferenceId)
                .Set("Shifts.$.WorkUnit", shift.WorkUnit);
            return await _shiftGroupRepository.UpdateAsync(filter, update);
        }

        public async Task<List<ShiftGroup>> SearchAll(bool? isActive = null)
        {
            Expression<Func<ShiftGroup, bool>> spec = x => !x.IsDelete;
            if (isActive.HasValue)
            {
                spec = spec.And(x => x.IsActive == isActive.Value);
            }
            return await _shiftGroupRepository.GetsAsync(spec);
        }

        private async Task<bool> CheckNameExists(string id, string name)
        {
            return await _shiftGroupRepository.ExistsAsync(x => !x.Id.Equals(id) && x.Name.Equals(name));
        }
    }
}
