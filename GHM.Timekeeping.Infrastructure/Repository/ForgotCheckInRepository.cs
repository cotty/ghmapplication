﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Domain.Constant;
using GHM.Timekeeping.Domain.IRepository;
using GHM.Timekeeping.Domain.Models;
using MongoDB.Driver;

namespace GHM.Timekeeping.Infrastructure.Repository
{
    public class ForgotCheckInRepository : MongoDbRepositoryBase, IForgotCheckInRepository
    {
        private readonly IMongoDbRepository<ForgotCheckIn, string> _forgotCheckinRepository;
        private readonly IShiftRepository _shiftRepository;        

        public ForgotCheckInRepository(IMongoDbContext context, IShiftRepository shiftRepository) : base(context)
        {
            _shiftRepository = shiftRepository;            
            _forgotCheckinRepository = Context.GetRepository<ForgotCheckIn, string>();
        }

        public async Task<long> Insert(ForgotCheckIn forgotCheckin)
        {
            var isExist = await CheckExists(forgotCheckin.ShiftId,
                forgotCheckin.UserId, forgotCheckin.Day, forgotCheckin.Month, forgotCheckin.Year);
            if (isExist)
                return -1;

            var shiftInfo = await _shiftRepository.GetInfo(forgotCheckin.ShiftId);
            if (shiftInfo == null)
                return -2;

            forgotCheckin.ShiftId = shiftInfo.Id;
            forgotCheckin.ShiftReportName = shiftInfo.ReportName;            
            forgotCheckin.CreateTime = DateTime.Now;
            await _forgotCheckinRepository.AddAsync(forgotCheckin);
            return 1;
        }

        public async Task<long> Update(ForgotCheckIn forgotCheckin)
        {
            var info = await GetInfo(forgotCheckin.Id);
            if (info == null)
                return -1;

            if (info.Status != ApproveStatus.WaitingManagerApprove)
                return -2;

            var shiftInfo = await _shiftRepository.GetInfo(forgotCheckin.ShiftId);
            if (shiftInfo == null)
                return -3;

            info.ShiftId = shiftInfo.Id;
            info.ShiftReportName = shiftInfo.ReportName;
            info.RegisterDate = forgotCheckin.RegisterDate;
            info.Note = forgotCheckin.Note;
            info.Day = forgotCheckin.RegisterDate.Day;
            info.Month = forgotCheckin.RegisterDate.Month;
            info.Year = forgotCheckin.RegisterDate.Year;
            info.Quarter = forgotCheckin.RegisterDate.GetQuarter();
            return await _forgotCheckinRepository.UpdateAsync(info);
        }

        public async Task<long> Delete(string id)
        {
            return await _forgotCheckinRepository.DeleteAsync(x => x.Id.Equals(id));
        }

        public async Task<long> Approve(string id, bool isApprove, string note)
        {
            var status = isApprove
                ? ApproveStatus.ManagerApprove
                : ApproveStatus.ManagerDecline;
            return await _forgotCheckinRepository.UpdateAsync(x => x.Id == id, Builders<ForgotCheckIn>.Update
                .Set(x => x.Status, status)
                .Set(x => x.DeclineReason, note));
        }

        public async Task<ForgotCheckIn> GetInfo(string id)
        {
            return await _forgotCheckinRepository.GetAsync(x => x.Id.Equals(id));
        }

        public async Task<ForgotCheckIn> GetInfo(string userId, string shiftId, int day, int month, int year)
        {
            return await _forgotCheckinRepository.GetAsync(x => x.UserId.Equals(userId) &&
                                                                   x.ShiftId.Equals(shiftId) && x.Day == day &&
                                                                   x.Month == month && x.Year == year && x.Status == ApproveStatus.ManagerApprove);
        }

        public async Task<bool> CheckExists(string shiftId, string userId, int day, int month, int year)
        {
            return await _forgotCheckinRepository.ExistsAsync(x => x.UserId.Equals(userId) && x.ShiftId.Equals(shiftId)
                && x.Day == day && x.Month == month && x.Year == year);
        }

        public Task<List<ForgotCheckIn>> Search(string currentUserId, string userId, int month, int year, byte type, ApproveStatus? status, int page,
            int pageSize, out long totalRows)
        {
            Expression<Func<ForgotCheckIn, bool>> spec = x => x.Month == month && x.Year == year;

            switch (type)
            {
                case 0:
                    spec = spec.And(x => x.UserId.Equals(currentUserId));
                    break;
                case 1:
                    spec = spec.And(x => x.ManagerUserId.Equals(currentUserId));

                    if (!string.IsNullOrEmpty(userId))
                    {
                        spec = spec.And(x => x.UserId.Equals(userId));
                    }
                    break;
            }

            if (status.HasValue)
            {
                spec = spec.And(x => x.Status == status.Value);
            }

            var sort = Context.Filters.Sort<ForgotCheckIn, DateTime>(x => x.RegisterDate, true);
            var paging = Context.Filters.Page<ForgotCheckIn>(page, pageSize);

            totalRows = _forgotCheckinRepository.Count(spec);
            return _forgotCheckinRepository.GetsAsync(spec, sort, paging);
        }
    }
}
