﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Domain.IRepository;
using GHM.Timekeeping.Domain.ModelMetas;
using GHM.Timekeeping.Domain.Models;
using MongoDB.Driver;

namespace GHM.Timekeeping.Infrastructure.Repository
{
    // TODO: Check
    public class InLateOutEarlyFrequentlyRepository : MongoDbRepositoryBase, IInLateOutEarlyFrequentlyRepository
    {
        private readonly IMongoDbRepository<InLateOutEarlyFrequently, string> _inLateOutEarlyFrequentlyRepository;
        private readonly IShiftRepository _shiftRepository;
        private readonly IWorkScheduleRepository _workScheduleRepository;
        public InLateOutEarlyFrequentlyRepository(IMongoDbContext context, IShiftRepository shiftRepository, IWorkScheduleRepository workScheduleRepository) : base(context)
        {
            _shiftRepository = shiftRepository;
            _workScheduleRepository = workScheduleRepository;
            _inLateOutEarlyFrequentlyRepository = Context.GetRepository<InLateOutEarlyFrequently, string>();
        }

        public async Task<bool> CheckExists(string id, string userId, DateTime? fromDate, DateTime? toDate)
        {
            Expression<Func<InLateOutEarlyFrequently, bool>> spec = x => x.Id != id && x.UserId.Equals(userId) && !x.IsDelete;
            if (fromDate.HasValue)
            {
                spec = spec.And(x => x.FromDate <= fromDate);
            }

            if (toDate.HasValue)
            {
                spec = spec.And(x => x.ToDate >= toDate);
            }

            return await _inLateOutEarlyFrequentlyRepository.ExistsAsync(spec);
        }

        public async Task<long> Insert(TimekeepingInLateOutEarlyFrequentlyMeta inLateOutEarlyFrequentlyMeta)
        {
            var isExists = await CheckExists(inLateOutEarlyFrequentlyMeta.Id, inLateOutEarlyFrequentlyMeta.UserId, inLateOutEarlyFrequentlyMeta.FromDate,
                inLateOutEarlyFrequentlyMeta.ToDate);
            if (isExists)
                return -1;

            // Check User exists
            //var userInfo = await _userRepository.GetInfo(inLateOutEarlyFrequentlyMeta.UserId, true);
            //if (userInfo == null)
            //    return -2;

            var workScheduleShift = _workScheduleRepository.GetMyWorkScheduleShift(inLateOutEarlyFrequentlyMeta.UserId);
            if (workScheduleShift == null || !workScheduleShift.Any())
                return -3;

            //var today = DateTime.Today;
            //if (inLateOutEarlyFrequentlyMeta.IsActive)
            //{
            //    // Update all other is active by userId to false.                
            //    await _inLateOutEarlyFrequentlyRepository.UpdateAsync(
            //        x => x.UserId.Equals(inLateOutEarlyFrequentlyMeta.UserId) && x.IsActive && !x.IsDelete && (!x.ToDate.HasValue || (x.ToDate.HasValue && x.ToDate.Value > today)),
            //        Builders<InLateOutEarlyFrequently>.Update
            //            .Set(x => x.IsActive, false)
            //            .Set(x => x.ToDate, today)
            //    );
            //}

            //var insertObject = new InLateOutEarlyFrequently
            //{
            //    UserId = userInfo.Id,
            //    FullName = userInfo.FullName,
            //    TitleId = userInfo.TitleId,
            //    TitleName = userInfo.TitleName,
            //    OfficeId = userInfo.OfficeId,
            //    OfficeName = userInfo.OfficeName,
            //    OfficeIdPath = userInfo.OfficeIdPath,
            //    Avatar = userInfo.Avatar,
            //    FromDate = inLateOutEarlyFrequentlyMeta.FromDate,
            //    ToDate = inLateOutEarlyFrequentlyMeta.ToDate,
            //    InOutFrequentlyDetails = inLateOutEarlyFrequentlyMeta.InOutFrequentlyDetails,
            //    UnsignName = userInfo.UnsignName,
            //    Reason = inLateOutEarlyFrequentlyMeta.Reason,
            //    Note = inLateOutEarlyFrequentlyMeta.Note,
            //    IsActive = inLateOutEarlyFrequentlyMeta.IsActive
            //};

            // Check if active set fromDate is today.
            //if (inLateOutEarlyFrequentlyMeta.IsActive && !inLateOutEarlyFrequentlyMeta.FromDate.HasValue)
            //{
            //    insertObject.FromDate = DateTime.Now;
            //}

            //await _inLateOutEarlyFrequentlyRepository.AddAsync(insertObject);
            return 1;
        }

        public async Task<long> Update(TimekeepingInLateOutEarlyFrequentlyMeta inLateOutEarlyFrequentlyMeta)
        {
            var isExists = await CheckExists(inLateOutEarlyFrequentlyMeta.Id, inLateOutEarlyFrequentlyMeta.UserId, inLateOutEarlyFrequentlyMeta.FromDate,
                inLateOutEarlyFrequentlyMeta.ToDate);
            if (isExists)
                return -1;

            var info = await GetInfo(inLateOutEarlyFrequentlyMeta.Id);
            if (info == null)
                return -2;

            var updateBuilder = Builders<InLateOutEarlyFrequently>.Update;
            var queryUpdate = updateBuilder.
                Set(x => x.FromDate, inLateOutEarlyFrequentlyMeta.FromDate)
                .Set(x => x.ToDate, inLateOutEarlyFrequentlyMeta.ToDate)
                .Set(x => x.IsActive, inLateOutEarlyFrequentlyMeta.IsActive)
                .Set(x => x.Reason, inLateOutEarlyFrequentlyMeta.Reason)
                .Set(x => x.Note, inLateOutEarlyFrequentlyMeta.Note)
                .Set(x => x.IsActive, inLateOutEarlyFrequentlyMeta.IsActive)
                .Set(x => x.InOutFrequentlyDetails, inLateOutEarlyFrequentlyMeta.InOutFrequentlyDetails);

            if (!info.IsActive && inLateOutEarlyFrequentlyMeta.IsActive && !info.FromDate.HasValue)
            {
                queryUpdate = queryUpdate.Set(x => x.FromDate, DateTime.Now);
            }

            //if (!info.IsActive && inLateOutEarlyFrequentlyMeta.IsActive)
            //{
            //    var today = DateTime.Today;
            //    // Update all other is active by userId to false.                
            //    await _inLateOutEarlyFrequentlyRepository.UpdateAsync(
            //        x => x.UserId.Equals(inLateOutEarlyFrequentlyMeta.UserId) && x.IsActive && !x.IsDelete && (!x.ToDate.HasValue || (x.ToDate.HasValue && x.ToDate.Value > today)),
            //        Builders<InLateOutEarlyFrequently>.Update
            //            .Set(x => x.IsActive, false)
            //            .Set(x => x.ToDate, today)
            //    );

            //    if (!info.FromDate.HasValue)
            //        queryUpdate = queryUpdate.Set(x => x.FromDate, today);
            //}

            return await _inLateOutEarlyFrequentlyRepository.UpdateAsync(x => x.Id.Equals(inLateOutEarlyFrequentlyMeta.Id),
                queryUpdate);
        }

        public async Task<long> InsertDetail(string id, InOutFrequentlyDetail inOutFrequentlyDetail)
        {
            var info = await GetInfo(id);
            if (info == null)
                return -1;

            //var detailInfo = info.InOutFrequentlyDetails.FirstOrDefault(x =>
            //    x.IsDelete && x.IsInLate == inOutFrequentlyDetail.IsInLate &&
            //    x.ShiftId == inOutFrequentlyDetail.ShiftId && x.DayOfWeek == inOutFrequentlyDetail.DayOfWeek);

            //var filter = Builders<InLateOutEarlyFrequently>.Filter;
            //if (detailInfo != null)
            //{
            //    // Update deleted detail to false.                
            //    var queryFilter = filter.And(
            //        filter.Eq(x => x.Id, id),
            //        filter.ElemMatch(x => x.InOutFrequentlyDetails, d => d.ShiftId == inOutFrequentlyDetail.ShiftId
            //                                                             && d.IsInLate == inOutFrequentlyDetail.IsInLate &&
            //                                                             d.DayOfWeek == inOutFrequentlyDetail.DayOfWeek
            //                                                             && d.IsDelete)
            //        );
            //    return await _inLateOutEarlyFrequentlyRepository.UpdateAsync(queryFilter,
            //        Builders<InLateOutEarlyFrequently>.Update
            //            .Set("InOutFrequentlyDetails.$.Id", newDetailId)
            //            .Set("InOutFrequentlyDetails.$.IsDelete", false)
            //            .Set("InOutFrequentlyDetails.$.TotalMinutes", inOutFrequentlyDetail.TotalMinutes)
            //    );
            //}

            var isDetailExists = info.InOutFrequentlyDetails.Any(x =>
                !x.IsDelete && x.IsInLate == inOutFrequentlyDetail.IsInLate &&
                x.ShiftId == inOutFrequentlyDetail.ShiftId && x.DayOfWeek == inOutFrequentlyDetail.DayOfWeek);
            if (isDetailExists)
                return -2;

            // Insert new detail.
            return await _inLateOutEarlyFrequentlyRepository.UpdateAsync(x => x.Id == id,
                Builders<InLateOutEarlyFrequently>.Update
                    .AddToSet(x => x.InOutFrequentlyDetails, inOutFrequentlyDetail));
        }

        public async Task<long> UpdateDetail(string id, InOutFrequentlyDetail inOutFrequentlyDetail)
        {
            var info = await GetInfo(id);
            if (info == null || !info.InOutFrequentlyDetails.Any())
                return -1;

            var shiftInfo = await _shiftRepository.GetInfo(inOutFrequentlyDetail.ShiftId);
            if (shiftInfo == null)
                return -2;

            if (info.InOutFrequentlyDetails.Any(x =>
                x.Id != inOutFrequentlyDetail.Id && x.ShiftId == inOutFrequentlyDetail.ShiftId &&
                x.IsInLate == inOutFrequentlyDetail.IsInLate && !x.IsDelete
                && x.DayOfWeek == inOutFrequentlyDetail.DayOfWeek))
                return -3;

            var filter = Builders<InLateOutEarlyFrequently>.Filter;
            var inOutFrequentlyDetailFilter = filter.And(
                filter.Eq(x => x.Id, id),
                filter.ElemMatch(x => x.InOutFrequentlyDetails, d => d.Id == inOutFrequentlyDetail.Id));
            var update = Builders<InLateOutEarlyFrequently>.Update
                .Set("InOutFrequentlyDetails.$.DayOfWeek", inOutFrequentlyDetail.DayOfWeek)
                .Set("InOutFrequentlyDetails.$.ShiftId", shiftInfo.Id)
                .Set("InOutFrequentlyDetails.$.ShiftReportName", shiftInfo.ReportName)
                .Set("InOutFrequentlyDetails.$.IsInLate", inOutFrequentlyDetail.IsInLate)
                .Set("InOutFrequentlyDetails.$.TotalMinutes", inOutFrequentlyDetail.TotalMinutes);

            return await _inLateOutEarlyFrequentlyRepository.UpdateAsync(inOutFrequentlyDetailFilter, update);
        }

        public async Task<long> Delete(string id)
        {
            return await _inLateOutEarlyFrequentlyRepository.UpdateAsync(x => x.Id == id,
                Builders<InLateOutEarlyFrequently>.Update.Set(x => x.IsDelete, true));
        }

        public async Task<long> DeleteDetail(string id, string detailId)
        {
            var filter = Builders<InLateOutEarlyFrequently>.Filter;
            var inOutFrequentlyDetailFilter = filter.And(
                filter.Eq(x => x.Id, id),
                filter.ElemMatch(x => x.InOutFrequentlyDetails, d => d.Id == detailId));
            var updateBuilder = Builders<InLateOutEarlyFrequently>.Update.Set("InOutFrequentlyDetails.$.IsDelete", true);
            return await _inLateOutEarlyFrequentlyRepository.UpdateAsync(inOutFrequentlyDetailFilter, updateBuilder);
        }

        public async Task<long> UpdateActive(string id, bool isActive)
        {
            return await _inLateOutEarlyFrequentlyRepository.UpdateAsync(x => x.Id.Equals(id),
                Builders<InLateOutEarlyFrequently>.Update.Set(x => x.IsActive, isActive));
        }

        public async Task<InLateOutEarlyFrequently> GetInfo(string id)
        {
            return await _inLateOutEarlyFrequentlyRepository.GetAsync(x => x.Id.Equals(id) && !x.IsDelete);
        }

        public async Task<InLateOutEarlyFrequently> GetInfoByUserId(string userId, DateTime dayCheck)
        {
            Expression<Func<InLateOutEarlyFrequently, bool>> spec = x => x.UserId == userId && !x.IsDelete && x.IsActive
            && (!x.FromDate.HasValue || (x.FromDate.HasValue && x.FromDate.Value <= dayCheck)) &&
            (!x.ToDate.HasValue || (x.ToDate.HasValue && x.ToDate >= dayCheck));
            return await _inLateOutEarlyFrequentlyRepository.GetAsync(spec);
        }

        public Task<List<InLateOutEarlyFrequently>> Search(string keyword, bool? isActive, DateTime? fromDate, DateTime? toDate, int page,
            int pageSize, out long totalRows)
        {
            Expression<Func<InLateOutEarlyFrequently, bool>> spec = x => !x.IsDelete;

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripVietnameseChars().ToLower();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (isActive.HasValue)
            {
                spec = spec.And(x => x.IsActive == isActive.Value);
            }

            if (fromDate.HasValue)
            {
                spec = spec.And(x => x.FromDate <= fromDate.Value);
            }

            if (toDate.HasValue)
            {
                spec = spec.And(x => x.ToDate >= toDate.Value);
            }

            var query = _inLateOutEarlyFrequentlyRepository.Collection.Find(spec)
                .SortByDescending(x => x.Id)
                .Skip((page - 1) * pageSize)
                .Limit(pageSize)
                .Project(x => new InLateOutEarlyFrequently
                {
                    Id = x.Id,
                    UserId = x.UserId,
                    FullName = x.FullName,
                    Avatar = x.Avatar,
                    TitleId = x.TitleId,
                    OfficeId = x.OfficeId,
                    TitleName = x.TitleName,
                    OfficeName = x.OfficeName,
                    OfficeIdPath = x.OfficeIdPath,
                    IsActive = x.IsActive,
                    Reason = x.Reason,
                    Note = x.Note,
                    CreateTime = x.CreateTime,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    InOutFrequentlyDetails = x.InOutFrequentlyDetails.Where(d => !d.IsDelete).ToArray()
                });

            totalRows = query.Count();
            return query.ToListAsync();
        }
    }
}
