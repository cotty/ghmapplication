﻿
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Domain.IRepository;

namespace GHM.Timekeeping.Infrastructure.Repository
{
    public class ConfigRepository : MongoDbRepositoryBase, ITimekeepingRepository
    {
        public ConfigRepository(IMongoDbContext context) : base(context)
        {
        }
    }
}
