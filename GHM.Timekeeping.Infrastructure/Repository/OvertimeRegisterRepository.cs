﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Domain.Constant;
using GHM.Timekeeping.Domain.IRepository;
using GHM.Timekeeping.Domain.Models;
using MongoDB.Driver;

namespace GHM.Timekeeping.Infrastructure.Repository
{
    // TODO: Check
    public class OvertimeRegisterRepository : MongoDbRepositoryBase, IOvertimeRegisterRepository
    {
        private readonly IMongoDbRepository<OvertimeRegister, string> _overtimeRegisterRepository;
        private readonly IShiftRepository _shiftRepository;

        public OvertimeRegisterRepository(IMongoDbContext context, IShiftRepository shiftRepository) : base(context)
        {
            _shiftRepository = shiftRepository;
            _overtimeRegisterRepository = Context.GetRepository<OvertimeRegister, string>();
        }

        public async Task<long> Insert(OvertimeRegister overtimeRegister)
        {
            var isExist = await CheckExists(overtimeRegister.ShiftId,
                overtimeRegister.UserId, overtimeRegister.Day, overtimeRegister.Month, overtimeRegister.Year);
            if (isExist)
                return -1;

            var shiftInfo = await _shiftRepository.GetInfo(overtimeRegister.ShiftId);
            if (shiftInfo == null)
                return -2;

            overtimeRegister.ShiftId = shiftInfo.Id;
            overtimeRegister.ShiftReportName = shiftInfo.ReportName;
            overtimeRegister.Day = overtimeRegister.RegisterDate.Day;
            overtimeRegister.Month = overtimeRegister.RegisterDate.Month;
            overtimeRegister.Year = overtimeRegister.RegisterDate.Year;
            overtimeRegister.Quarter = overtimeRegister.RegisterDate.GetQuarter();
            overtimeRegister.CreateTime = DateTime.Now;
            await _overtimeRegisterRepository.AddAsync(overtimeRegister);
            return 1;
        }

        public async Task<long> Update(OvertimeRegister overtimeRegister)
        {
            var info = await GetInfo(overtimeRegister.Id);
            if (info == null)
                return -1;

            if (info.Status != ApproveStatus.WaitingManagerApprove)
                return -2;

            var shiftInfo = await _shiftRepository.GetInfo(overtimeRegister.ShiftId);
            if (shiftInfo == null)
                return -3;

            info.ShiftId = shiftInfo.Id;
            info.ShiftReportName = shiftInfo.ReportName;
            info.RegisterDate = overtimeRegister.RegisterDate;
            info.Note = overtimeRegister.Note;
            info.Day = overtimeRegister.RegisterDate.Day;
            info.Month = overtimeRegister.RegisterDate.Month;
            info.Year = overtimeRegister.RegisterDate.Year;
            info.Quarter = overtimeRegister.RegisterDate.GetQuarter();
            info.Type = overtimeRegister.Type;
            info.From = overtimeRegister.From;
            info.To = overtimeRegister.To;
            info.TotalMinutes = overtimeRegister.TotalMinutes;
            return await _overtimeRegisterRepository.UpdateAsync(info);
        }

        public async Task<long> Delete(string id)
        {
            return await _overtimeRegisterRepository.UpdateAsync(x => x.Id.Equals(id), Builders<OvertimeRegister>.Update.Set(x => x.IsDelete, true));
        }

        public async Task<long> Approve(string id, string userId, bool isApprove, string note)
        {
            var info = await GetInfo(id);
            if (info == null)
                return -1;

            //var userInfo = await _userRepository.GetInfo(userId, true);
            //if (userInfo == null)
            //    return -2;

            //if (info.ManagerUserId != userId)
            //    return -3;

            //info.Status = isApprove
            //    ? ApproveStatus.ManagerApprove
            //    : ApproveStatus.ManagerDecline;

            //info.ApproverId = userId;
            //info.ApproverFullName = userInfo.ApproverFullName;
            //info.ApproveTime = DateTime.Now;
            //info.DeclineReason = note;
            return await _overtimeRegisterRepository.UpdateAsync(x => x.Id == info.Id, info);
        }

        public async Task<OvertimeRegister> GetInfo(string id)
        {
            return await _overtimeRegisterRepository.GetAsync(x => x.Id.Equals(id) && !x.IsDelete);
        }

        public async Task<OvertimeRegister> GetInfo(string userId, string shiftId, int day, int month, int year)
        {
            return await _overtimeRegisterRepository.GetAsync(x => x.UserId.Equals(userId) &&
                                                                   x.ShiftId.Equals(shiftId) && x.Day == day &&
                                                                   x.Month == month && x.Year == year && x.Status == ApproveStatus.ManagerApprove
                                                                   && !x.IsDelete);
        }

        public async Task<bool> CheckExists(string shiftId, string userId, int day, int month, int year)
        {
            return await _overtimeRegisterRepository.ExistsAsync(x => x.UserId.Equals(userId) && x.ShiftId.Equals(shiftId)
                && x.Day == day && x.Month == month && x.Year == year && !x.IsDelete);
        }

        public Task<List<OvertimeRegister>> Search(string currentUserId, string userId, int month, int year, byte type, ApproveStatus? status, int page,
            int pageSize, out long totalRows)
        {
            Expression<Func<OvertimeRegister, bool>> spec = x => x.Month == month && x.Year == year && !x.IsDelete;

            switch (type)
            {
                case 0:
                    spec = spec.And(x => x.UserId.Equals(currentUserId));
                    break;
                case 1:
                    spec = spec.And(x => x.ManagerUserId.Equals(currentUserId));

                    if (!string.IsNullOrEmpty(userId))
                    {
                        spec = spec.And(x => x.UserId.Equals(userId));
                    }
                    break;
            }

            if (status.HasValue)
            {
                spec = spec.And(x => x.Status == status.Value);
            }

            var sort = Context.Filters.Sort<OvertimeRegister, DateTime>(x => x.RegisterDate, true);
            var paging = Context.Filters.Page<OvertimeRegister>(page, pageSize);

            totalRows = _overtimeRegisterRepository.Count(spec);
            return _overtimeRegisterRepository.GetsAsync(spec, sort, paging);
        }
    }
}
