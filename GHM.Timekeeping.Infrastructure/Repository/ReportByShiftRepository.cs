﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.MongoDb;
using GHM.Infrastructure.MongoDb.Models;
using GHM.Timekeeping.Domain.Constant;
using GHM.Timekeeping.Domain.IRepository;
using GHM.Timekeeping.Domain.Models;
using MongoDB.Driver;

namespace GHM.Timekeeping.Infrastructure.Repository
{
    public class ReportByShiftRepository : MongoDbRepositoryBase, IReportByShiftRepository
    {
        private readonly IMongoDbRepository<ReportByShift, string> _reportByShiftRepository;
        private readonly IReportByMonthRepository _reportByMonthRepository;
        private readonly IShiftRepository _shiftRepository;
        private readonly IConfigHolidayRepository _holidayRepository;
        private readonly IWorkScheduleRepository _workScheduleRepository;

        public ReportByShiftRepository(IMongoDbContext context, IReportByMonthRepository reportByMonthRepository, IShiftRepository shiftRepository, IConfigHolidayRepository holidayRepository, IWorkScheduleRepository workScheduleRepository) : base(context)
        {
            _reportByMonthRepository = reportByMonthRepository;
            _shiftRepository = shiftRepository;
            _holidayRepository = holidayRepository;
            _workScheduleRepository = workScheduleRepository;
            _reportByMonthRepository.ReportByShiftRepository = this;
            _reportByShiftRepository = Context.GetRepository<ReportByShift, string>();
        }

        public IDayOffRepository DayOffRepository { get; set; }

        public async Task<long> Save(ReportByShift reportByShift)
        {
            // Check exists
            var isExists = await CheckExists(reportByShift.Day, reportByShift.Month, reportByShift.Year,
                reportByShift.EnrollNumber, reportByShift.ShiftId);

            if (isExists)
            {
                var filter = Builders<ReportByShift>.Filter.Where(x => x.Day == reportByShift.Day && x.Month == reportByShift.Month
                && x.Year == reportByShift.Year
                && x.EnrollNumber == reportByShift.EnrollNumber
                && x.ShiftId.Equals(reportByShift.ShiftId));

                var updateBuilder = Builders<ReportByShift>.Update.Set(x => x.EnrollNumber, reportByShift.EnrollNumber)
                    .Set(x => x.Month, reportByShift.Month)
                    .Set(x => x.Year, reportByShift.Year)
                    .Set(x => x.Quarter, reportByShift.Quarter)
                    .Set(x => x.IsSunday, reportByShift.IsSunday)
                    .Set(x => x.IsHoliday, reportByShift.IsHoliday)
                    .Set(x => x.In, reportByShift.In)
                    .Set(x => x.Out, reportByShift.Out)
                    .Set(x => x.InTime, reportByShift.InTime)
                    .Set(x => x.OutTime, reportByShift.OutTime)
                    .Set(x => x.InLateMin, reportByShift.InLateMin)
                    .Set(x => x.InSoonMin, reportByShift.InSoonMin)
                    .Set(x => x.OutLateMin, reportByShift.OutLateMin)
                    .Set(x => x.OutSoonMin, reportByShift.OutSoonMin)
                    .Set(x => x.Day, reportByShift.Day)
                    .Set(x => x.Month, reportByShift.Month)
                    .Set(x => x.Quarter, reportByShift.Quarter)
                    .Set(x => x.Year, reportByShift.Year)
                    .Set(x => x.IsSunday, reportByShift.IsSunday)
                    .Set(x => x.IsHoliday, reportByShift.IsHoliday)
                    .Set(x => x.IsOvertime, reportByShift.IsOvertime)
                    .Set(x => x.Method, reportByShift.Method)
                    .Set(x => x.IsValid, reportByShift.IsValid)
                    .Set(x => x.InLatencyMin, reportByShift.InLatencyMin)
                    .Set(x => x.OutLatencyMin, reportByShift.OutLatencyMin)
                    .Set(x => x.TotalOvertimeMin, reportByShift.TotalOvertimeMin);

                return await _reportByShiftRepository.UpdateAsync(filter, updateBuilder);
            }

            await _reportByShiftRepository.AddAsync(reportByShift);
            return 1;
        }

        public async Task<long> Insert(ReportByShift reportByShift)
        {
            var isExists = await CheckExists(reportByShift.Day, reportByShift.Month, reportByShift.Year,
                reportByShift.EnrollNumber, reportByShift.ShiftId);
            if (isExists)
                return -1;

            await _reportByShiftRepository.AddAsync(reportByShift);
            return 1;
        }

        public async Task<long> InsertFromDayOffRegister(List<ReportByShift> reportByShifts)
        {
            if (!reportByShifts.Any())
                return -1;

            // Thêm vào bảng báo cáo theo ca
            await _reportByShiftRepository.AddManyAsync(reportByShifts);

            // Tổng hợp lên tháng
            var groupMonth = reportByShifts.GroupBy(x => new { x.EnrollNumber, x.Month, x.Year });
            foreach (var group in groupMonth)
            {
                await _reportByMonthRepository.Aggregate(group.Key.EnrollNumber, group.Key.Month, group.Key.Year);
            }
            return 1;
        }

        public async Task<long> DeleteShift(int day, int month, int year, int enrollNumber, string shiftId)
        {
            var shiftInfo = await _reportByShiftRepository.GetAsync(x =>
                x.Day == day && x.Month == month && x.Year == year && x.EnrollNumber == enrollNumber
                && x.ShiftId.Equals(shiftId));
            if (shiftInfo == null)
                return -1;

            var result = await _reportByShiftRepository.DeleteAsync(x =>
                x.Day == day && x.Month == month && x.Year == year && x.EnrollNumber == enrollNumber
                && x.ShiftId.Equals(shiftId));
            if (result <= 0)
                return result;

            // Kiểm tra nếu là ngày lễ thêm mới nghỉ lễ.
            var isHoliday = await _holidayRepository.CheckExists(day, month, year);
            var dayCheck = new DateTime(year, month, day);
            var isWeekLeave = await _workScheduleRepository.CheckIsWeekLeave(enrollNumber, shiftId, dayCheck);
            var dayOffs = await DayOffRepository.GetListDayOffRegister(enrollNumber, dayCheck);
            if (isHoliday || isWeekLeave || dayOffs.Any())
            {
                var shiftReport = new ReportByShift
                {
                    CreateTime = DateTime.Now,
                    Day = day,
                    Month = month,
                    Year = year,
                    EnrollNumber = enrollNumber,
                    UserId = shiftInfo.UserId,
                    FullName = shiftInfo.FullName,
                    IsHoliday = isHoliday,
                    Method = isWeekLeave ? DayOffMethod.WeekLeave : (DayOffMethod?)null,
                    IsSunday = dayCheck.DayOfWeek == DayOfWeek.Sunday,
                    IsValid = false,
                    ShiftId = shiftInfo.ShiftId,
                    ShiftCode = shiftInfo.ShiftCode,
                    ShiftReportName = shiftInfo.ShiftReportName,
                    OfficeId = shiftInfo.OfficeId,
                    OfficeIdPath = shiftInfo.OfficeIdPath,
                    Quarter = shiftInfo.Quarter,
                    Type = ReportByShiftType.Auto,
                    IsValidWorkSchedule = true,
                };
                if (dayOffs.Any())
                {
                    foreach (var dayOff in dayOffs)
                    {
                        shiftReport.Method = (DayOffMethod)dayOff.Status;
                        await Insert(shiftReport);
                    }
                }
                else
                {
                    await Insert(shiftReport);
                }
            }
            return result;
        }

        public async Task<List<ReportByShift>> GetListTimesheet(string keyword, int officeId, int month, int year)
        {
            Expression<Func<ReportByShift, bool>> spec = x => x.OfficeId == officeId && x.Month == month && x.Year == year;
            if (string.IsNullOrEmpty(keyword)) return await _reportByShiftRepository.GetsAsync(spec);

            keyword = keyword.ToLower().StripVietnameseChars();
            spec = spec.And(x => x.UnsignName.ToLower().Contains(keyword));
            return await _reportByShiftRepository.GetsAsync(spec);
        }

        public async Task<List<ReportByShift>> GetListMyTimeSheet(string userId, int? month, int year)
        {
            Expression<Func<ReportByShift, bool>> spec = x => x.UserId.Equals(userId) && x.Year == year;
            if (month.HasValue)
            {
                spec = spec.And(x => x.Month == month.Value);
            }
            return await _reportByShiftRepository.GetsAsync(spec);
        }

        public async Task<List<ReportByShift>> GetListReportByShiftForAggregate(int month, int year)
        {
            return await _reportByShiftRepository.GetsAsync(x => x.Month == month && x.Year == year);
        }

        public async Task<List<ReportByShift>> GetListReportByShiftForAggregateByUser(int enrollNumber, int month, int year)
        {
            return await _reportByShiftRepository.GetsAsync(
                x => x.EnrollNumber == enrollNumber && x.Month == month && x.Year == year);
        }

        public async Task<List<ReportByShift>> GetListTimesheetResult(string keyword, int officeId, byte month, int year)
        {
            throw new NotImplementedException();
        }

        public async Task<List<ReportByShift>> GetListReportByShiftByUser(int enrollNumber, byte month, int year)
        {
            return await _reportByShiftRepository.GetsAsync(
                x => x.EnrollNumber.Equals(enrollNumber) && x.Month == month && x.Year == year);
        }

        public async Task<bool> CheckExists(int day, int month, int year, int enrollNumber, string shiftId)
        {
            return await _reportByShiftRepository.ExistsAsync(x => x.Day == day
                                                            && x.Month == month
                                                            && x.Year == year
                                                            && x.EnrollNumber == enrollNumber
                                                            && x.ShiftId.Equals(shiftId));
        }

        public async Task<bool> CheckIsHasCheckInOrCheckOut(int day, int month, int year, int enrollNumber, string shiftId, bool isCheckIn)
        {
            Expression<Func<ReportByShift, bool>> spec =
                x => x.Day == day && x.Month == month && x.Year == year && x.EnrollNumber == enrollNumber &&
                     x.ShiftId.Equals(shiftId);

            spec = isCheckIn ? spec.And(x => x.In.HasValue) : spec.And(x => x.Out.HasValue);
            return await _reportByShiftRepository.ExistsAsync(spec);
        }

        public async Task<long> UpdateLatency(int enrollNumber, string shiftId, int day, int month, int year, int inLatencyMin, int outLatencyMin
            , string inLatencyReason, string outLatencyReason)
        {
            var result = await _reportByShiftRepository.UpdateAsync(
                x => x.EnrollNumber == enrollNumber && x.ShiftId.Equals(shiftId) && x.Day == day && x.Month == month &&
                     x.Year == year,
                Builders<ReportByShift>.Update.Set(x => x.InLatencyMin, inLatencyMin)
                    .Set(x => x.OutLatencyMin, outLatencyMin)
                    .Set(x => x.InLatencyReason, inLatencyReason)
                    .Set(x => x.OutLatencyReason, outLatencyReason));

            if (result > 0)
            {
                // cập nhật lên tháng.
                await _reportByMonthRepository.Aggregate(enrollNumber, month, year);
            }
            return result;
        }

        public async Task<long> UpdateOvertimeRegister(int enrollNumber, string shiftId, int day, int month, int year, int totalMinutes)
        {
            var result = await _reportByShiftRepository.UpdateAsync(
                x => x.EnrollNumber == enrollNumber && x.ShiftId.Equals(shiftId) && x.Day == day && x.Month == month &&
                     x.Year == year,
                Builders<ReportByShift>.Update.Set(x => x.TotalOvertimeMin, totalMinutes));

            if (result > 0)
            {
                // Cập nhật lên tháng.
                await _reportByMonthRepository.Aggregate(enrollNumber, month, year);
            }
            return result;
        }

        public async Task<long> UpdateCheckInCheckOutTime(ForgotCheckIn forgotCheckIn)
        {
            Expression<Func<ReportByShift, bool>> spec = x => x.ShiftId.Equals(forgotCheckIn.ShiftId)
                                                              && x.UserId.Equals(forgotCheckIn.UserId) && x.Day == forgotCheckIn.Day && x.Month == forgotCheckIn.Month
                                                              && x.Year == forgotCheckIn.Year;

            var shiftInfo = await _shiftRepository.GetInfo(forgotCheckIn.ShiftId);
            if (shiftInfo == null)
                return -1;

            var isHoliday =
                await _holidayRepository.CheckExists(forgotCheckIn.Day, forgotCheckIn.Month, forgotCheckIn.Year);

            var time = forgotCheckIn.IsCheckIn
                ? new DateTime(forgotCheckIn.Year, forgotCheckIn.Month, forgotCheckIn.Day, shiftInfo.StartTime.Hour,
                    shiftInfo.StartTime.Minute, 0)
                : new DateTime(forgotCheckIn.Year, forgotCheckIn.Month, forgotCheckIn.Day, shiftInfo.EndTime.Hour,
                    shiftInfo.EndTime.Minute, 0);

            var checkInTime = forgotCheckIn.IsCheckIn
                ? new DateTime(forgotCheckIn.Year, forgotCheckIn.Month, forgotCheckIn.Day, shiftInfo.StartTime.Hour,
                    shiftInfo.StartTime.Minute, 0)
                : (DateTime?)null;
            var checkOutTime = !forgotCheckIn.IsCheckIn
                ? new DateTime(forgotCheckIn.Year, forgotCheckIn.Month, forgotCheckIn.Day, shiftInfo.EndTime.Hour,
                    shiftInfo.EndTime.Minute, 0)
                : (DateTime?)null;

            var isValidWorkSchedule = await _workScheduleRepository.CheckIsHasWorkSchedule(forgotCheckIn.EnrollNumber,
                forgotCheckIn.ShiftId, time);

            var info = await _reportByShiftRepository.GetAsync(spec);
            // Trường hợp chưa có bản ghi ReportByShift sẽ thêm mới bản ghi
            if (info == null)
            {
                var reportByShift = new ReportByShift
                {
                    Day = forgotCheckIn.Day,
                    Month = forgotCheckIn.Month,
                    Year = forgotCheckIn.Month,
                    UserId = forgotCheckIn.UserId,
                    EnrollNumber = forgotCheckIn.EnrollNumber,
                    FullName = forgotCheckIn.FullName,
                    In = checkInTime,
                    Out = checkOutTime,
                    InTime = forgotCheckIn.IsCheckIn ? shiftInfo.StartTime : null,
                    OutTime = !forgotCheckIn.IsCheckIn ? shiftInfo.EndTime : null,
                    Type = ReportByShiftType.Manual,
                    IsSunday = DateTime.Today.DayOfWeek == DayOfWeek.Sunday,
                    IsHoliday = isHoliday,
                    OfficeId = forgotCheckIn.OfficeId,
                    OfficeIdPath = forgotCheckIn.OfficeIdPath,
                    Quarter = (forgotCheckIn.Month + 2) / 3,
                    ShiftId = shiftInfo.Id,
                    ShiftCode = shiftInfo.Code,
                    ShiftReportName = shiftInfo.ReportName,
                    TotalOvertimeMin = 0,
                    IsOvertime = shiftInfo.IsOvertime,
                    IsValid = checkInTime != null && checkOutTime != null,
                    IsValidWorkSchedule = isValidWorkSchedule,
                    WorkUnit = shiftInfo.WorkUnit,
                    UnsignName = forgotCheckIn.UnsignName
                };

                // Thêm mới vào báo cáo theo ca. Vì là trường hợp chưa tồn tại bản ghi nào của ca này nên sẽ k tổng hợp lên tháng.
                await Insert(reportByShift);
                return 1;
            }

            info.In = forgotCheckIn.IsCheckIn
                ? new DateTime(forgotCheckIn.Year, forgotCheckIn.Month, forgotCheckIn.Day, shiftInfo.StartTime.Hour,
                    shiftInfo.StartTime.Minute, 0)
                : info.In;
            info.Out = !forgotCheckIn.IsCheckIn
                ? new DateTime(forgotCheckIn.Year, forgotCheckIn.Month, forgotCheckIn.Day, shiftInfo.EndTime.Hour,
                    shiftInfo.EndTime.Minute, 0)
                : info.Out;

            info.InTime = forgotCheckIn.IsCheckIn ? new TimeObject { Hour = shiftInfo.StartTime.Hour, Minute = shiftInfo.StartTime.Minute } : info.InTime;
            info.OutTime = !forgotCheckIn.IsCheckIn ? new TimeObject { Hour = shiftInfo.EndTime.Hour, Minute = shiftInfo.EndTime.Minute } : info.OutTime;
            info.TotalWorkingMin = info.In != null && info.Out != null ? (int)(info.Out.Value - info.In.Value).TotalMinutes : 0;

            // Trường hợp có rồi thì sẽ update lại và tính ra thời gian làm việc của ca đó.
            var result = await _reportByShiftRepository.UpdateAsync(spec, Builders<ReportByShift>.Update
                .Set(x => x.In, info.In)
                .Set(x => x.Out, info.Out)
                .Set(x => x.InTime, info.InTime)
                .Set(x => x.InTime, info.OutTime)
                .Set(x => x.InTime, info.InTime)
                .Set(x => x.TotalWorkingMin, info.TotalWorkingMin)
                .Set(x => x.IsValid, info.In.HasValue && info.Out.HasValue));

            if (result > 0)
            {
                // Tổng hợp kết quả dữ liệu lên tháng.
                await _reportByMonthRepository.Aggregate(forgotCheckIn.EnrollNumber, forgotCheckIn.Month,
                    forgotCheckIn.Year);
            }

            return result;
        }

        public async Task<long> MarkAsValid(int day, int month, int year, int enrollNumber, string shiftId, bool isCheckIn)
        {
            Expression<Func<ReportByShift, bool>> spec = x => x.Day == day && x.Month == month && x.Year == year && x.EnrollNumber == enrollNumber
                && x.ShiftId.Equals(shiftId);
            var info = await _reportByShiftRepository.GetAsync(spec);
            if (info == null)
                return -1;

            var shiftInfo = await _shiftRepository.GetInfo(info.ShiftId);
            if (shiftInfo == null)
                return -2;

            var fixedTime = isCheckIn
                ? new DateTime(info.Year, info.Month, info.Day, shiftInfo.StartTime.Hour, shiftInfo.StartTime.Minute,
                    shiftInfo.StartTime.Second)
                : new DateTime(info.Year, info.Month, info.Day, shiftInfo.EndTime.Hour, shiftInfo.EndTime.Minute,
                    shiftInfo.EndTime.Second);

            info.In = isCheckIn ? fixedTime : info.In;
            info.Out = !isCheckIn ? fixedTime : info.Out;

            if (info.In.HasValue && info.Out.HasValue)
            {
                info.TotalWorkingMin = (int)(info.Out.Value - info.In.Value).TotalMinutes;
            }

            var result = await _reportByShiftRepository.UpdateAsync(spec, Builders<ReportByShift>
                .Update
                .Set(x => x.In, info.In)
                .Set(x => x.Out, info.Out)
                .Set(x => x.TotalWorkingMin, info.TotalWorkingMin)
                .Set(x => x.InTime, new TimeObject
                {
                    Hour = info.In?.Hour ?? 0,
                    Minute = info.In?.Minute ?? 0,
                    Second = info.In?.Second ?? 0
                })
                .Set(x => x.OutTime, new TimeObject
                {
                    Hour = info.Out?.Hour ?? 0,
                    Minute = info.Out?.Minute ?? 0,
                    Second = info.Out?.Second ?? 0
                })
                .Set(x => x.IsValid, info.In.HasValue && info.Out.HasValue));

            if (result > 0)
            {
                await _reportByMonthRepository.Aggregate(info.EnrollNumber, info.Month, info.Year);
            }

            return result;
        }

        public async Task<long> ChangeMethod(int day, int month, int year, int enrollNumber, string shiftId, DayOffMethod method)
        {
            Expression<Func<ReportByShift, bool>> spec = x => x.Day == day && x.Month == month && x.Year == year && x.EnrollNumber == enrollNumber
                                                              && x.ShiftId.Equals(shiftId);
            return await _reportByShiftRepository.UpdateAsync(spec, Builders<ReportByShift>.Update
                .Set(x => x.IsValid, true)
                .Set(x => x.Method, method));
        }
    }
}
