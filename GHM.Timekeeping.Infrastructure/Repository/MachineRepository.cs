﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Domain.IRepository;
using GHM.Timekeeping.Domain.Models;

namespace GHM.Timekeeping.Infrastructure.Repository
{
    // TODO: check
    public class MachineRepository : MongoDbRepositoryBase, IMachineRepository
    {
        private readonly IMongoDbRepository<Machine, string> _machineRepository;
        public MachineRepository(IMongoDbContext context) : base(context)
        {
            _machineRepository = Context.GetRepository<Machine, string>();
        }

        public async Task<long> Insert(Machine machine)
        {
            var ipExists = await CheckExistsByIp(machine.Id, machine.Ip);
            if (ipExists)
                return -1;

            var machineNoExists = await CheckMachineNoExists(machine.Id, machine.No);
            if (machineNoExists)
                return -2;

            var serialNumberExsits = await CheckSerialExists(machine.Id, machine.SerialNumber);
            if (serialNumberExsits)
                return -3;

            await _machineRepository.AddAsync(machine);
            return 1;
        }

        public async Task<long> Update(Machine machine)
        {
            var ipExists = await CheckExistsByIp(machine.Id, machine.Ip);
            if (ipExists)
                return -1;

            var machineNoExists = await CheckMachineNoExists(machine.Id, machine.No);
            if (machineNoExists)
                return -2;

            var serialNumberExsits = await CheckSerialExists(machine.Id, machine.SerialNumber);
            if (serialNumberExsits)
                return -3;

            return await _machineRepository.UpdateAsync(machine);
        }

        public async Task<int> InsertUserToMachine(int enrollNumber, string fullName, string cardNumber = "", string password = "")
        {
            var listMachines = await GetAllMachine();
            if (listMachines == null || !listMachines.Any())
                return -1;

            foreach (var machine in listMachines)
            {
                //var result = new TimeMachineHelper(machine.No, enrollNumber).SetUserInfo(machine.Ip, machine.Port, fullName, cardNumber);
                //if (result < 0)
                //{
                //    // TODO: Insert log
                //}
            }

            return 1;
        }

        public async Task<long> Delete(string id)
        {
            return await _machineRepository.DeleteAsync(id);
        }

        public async Task<Machine> GetInfo(string id)
        {
            return await _machineRepository.GetAsync(x => x.Id.Equals(id));
        }

        public async Task<List<Machine>> GetAllMachine(bool? isActive = null)
        {
            Expression<Func<Machine, bool>> spec = x => true;
            if (isActive.HasValue)
            {
                spec = spec.And(x => x.IsActive == isActive);
            }
            return await _machineRepository.GetsAsync(spec);
        }


        #region Private region

        private async Task<bool> CheckExistsByIp(string id, string ip)
        {
            return await _machineRepository.ExistsAsync(x => !x.Id.Equals(id) && x.Ip.Equals(ip));
        }

        private async Task<bool> CheckMachineNoExists(string id, int machineNo)
        {
            return await _machineRepository.ExistsAsync(x => !x.Id.Equals(id) && x.No == machineNo);
        }

        private async Task<bool> CheckSerialExists(string id, string serial)
        {
            return await _machineRepository.ExistsAsync(x => !x.Id.Equals(id) && x.SerialNumber.Equals(serial));
        }
        #endregion
    }
}
