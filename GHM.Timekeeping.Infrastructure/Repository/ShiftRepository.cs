﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Domain.IRepository;
using GHM.Timekeeping.Domain.Models;
using MongoDB.Driver;

namespace GHM.Timekeeping.Infrastructure.Repository
{
    public class ShiftRepository : MongoDbRepositoryBase, IShiftRepository
    {
        private readonly IMongoDbRepository<Shift, string> _shiftRepository;
        private readonly IShiftGroupRepository _shiftGroupRepository;
        private readonly IWorkScheduleRepository _workScheduleRepository;
        public ShiftRepository(IMongoDbContext context, IShiftGroupRepository shiftGroupRepository, IWorkScheduleRepository workScheduleRepository) : base(context)
        {
            _shiftGroupRepository = shiftGroupRepository;
            workScheduleRepository.ShiftRepository = this;
            _workScheduleRepository = workScheduleRepository;
            _shiftRepository = Context.GetRepository<Shift, string>();
        }

        #region Shift
        public async Task<long> Insert(Shift shift)
        {
            var nameExists = await CheckShiftNameExists(shift.Id, shift.Name);
            if (nameExists)
                return -1;

            var codeExists = await CheckShiftCodeExists(shift.Id, shift.Code, shift.Name);
            if (codeExists)
                return -2;

            await _shiftRepository.AddAsync(shift);
            return 1;
        }

        public async Task<long> Update(Shift shift)
        {
            var nameExists = await CheckShiftNameExists(shift.Id, shift.Name);
            if (nameExists)
                return -1;

            var codeExists = await CheckShiftCodeExists(shift.Id, shift.Code, shift.Name);
            if (codeExists)
                return -2;

            var result = await _shiftRepository.UpdateAsync(shift);
            if (result < 0)
                return result;

            await _shiftGroupRepository.UpdateShift(shift);
            await _workScheduleRepository.UpdateShift(shift);
            return result;
        }


        // TODO: Kiểm tra các điều kiện để xóa 1 ca làm việc
        public async Task<long> Delete(string id)
        {
            // Kiểm tra ca làm việc có tồn tại trong danh sách lịch làm việc nhân viên không.
            var isExistsInWorkSchedule = await _workScheduleRepository.CheckShiftExists(id);
            if (isExistsInWorkSchedule)
                return -1;

            return await _shiftRepository.UpdateAsync(x => x.Id.Equals(id),
                Builders<Shift>.Update.Set(x => x.IsDelete, true));
        }

        public async Task<List<Shift>> SearchAll()
        {
            return await _shiftRepository.GetsAsync(x => !x.IsDelete);
        }

        public async Task<Shift> GetInfo(string id)
        {
            return await _shiftRepository.GetAsync(x => x.Id.Equals(id) && !x.IsDelete);
        }

        private async Task<bool> CheckShiftCodeExists(string id, string code, string name)
        {
            return await _shiftRepository.ExistsAsync(x => !x.Id.Equals(id) && x.Code.Equals(code) && x.Name.Equals(name) && !x.IsDelete);
        }

        private async Task<bool> CheckShiftNameExists(string id, string name)
        {
            return await _shiftRepository.ExistsAsync(x => !x.Id.Equals(id) && x.Name.Equals(name) && !x.IsDelete);
        }
        #endregion
    }
}
