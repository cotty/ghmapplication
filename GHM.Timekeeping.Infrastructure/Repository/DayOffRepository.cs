﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.Extensions;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Domain.Constant;
using GHM.Timekeeping.Domain.IRepository;
using GHM.Timekeeping.Domain.Models;
using MongoDB.Driver;

namespace GHM.Timekeeping.Infrastructure.Repository
{
    // TODO: Check
    public class DayOffRepository : MongoDbRepositoryBase, IDayOffRepository
    {
        private readonly IMongoDbRepository<DayOff, string> _dayOffRepository;
        //private readonly IReportByShiftRepository _reportByShiftRepository;
        private readonly IReportByMonthRepository _reportByMonthRepository;
        //private readonly IUserRepository _userRepository;

        public DayOffRepository(IMongoDbContext context,
            //IReportByShiftRepository reportByShiftRepository, 
            IReportByMonthRepository reportByMonthRepository) : base(context)
        {
            //reportByShiftRepository.DayOffRepository = this;
            //_reportByShiftRepository = reportByShiftRepository;
            _reportByMonthRepository = reportByMonthRepository;
            //_userRepository = userRepository;
            _dayOffRepository = Context.GetRepository<DayOff, string>();
        }

        public Task<List<DayOff>> Search(string currentUserId, string keyword, DateTime? fromDate, DateTime? toDate, DayOffStatus? status, byte type, int page, int pageSize,
            out long totalRows)
        {
            Expression<Func<DayOff, bool>> spec = x => true;
            spec = type == 0
                ? spec.And(x => x.UserId.Equals(currentUserId))
                : type == 1 ? spec.And(x => x.ManagerUserId.Equals(currentUserId))
                : spec.And(x => x.ApproverUserId.Equals(currentUserId)
                        && x.Status != DayOffStatus.WaitingManagerApprove
                        && x.Status != DayOffStatus.ManagerDecline);

            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.StripVietnameseChars();
                spec = spec.And(x => x.UnsignName.Contains(keyword));
            }

            if (fromDate.HasValue)
                spec = spec.And(x => x.CreateTime >= fromDate);

            if (toDate.HasValue)
                spec = spec.And(x => x.CreateTime <= toDate);

            if (status.HasValue)
                spec = spec.And(x => x.Status == status);

            var sort = Context.Filters.Sort<DayOff, DateTime>(x => x.CreateTime, true);
            var paging = Context.Filters.Page<DayOff>(page, pageSize);

            totalRows = _dayOffRepository.Count(spec);
            return _dayOffRepository.GetsAsync(spec, sort, paging);
        }

        public async Task<int> Insert(DayOff register)
        {
            var dates = register.Dates.Where(x => x.Method.HasValue).ToList();
            foreach (var registerDate in dates)
            {
                var isExists = await CheckExists(register.Id, register.EnrollNumber, registerDate.ShiftId, registerDate.Date);
                if (isExists)
                    return -1;

                registerDate.Day = registerDate.Date.Day;
                registerDate.Month = registerDate.Date.Month;
                registerDate.Year = registerDate.Date.Year;
            }

            await _dayOffRepository.AddAsync(register);
            return 1;
        }

        public async Task<long> Update(DayOff register)
        {
            var registerInfo = await GetInfo(register.Id);
            if (registerInfo == null)
            {
                return -1;
            }
            if (register.Status != (byte)DayOffStatus.WaitingManagerApprove && register.Status != DayOffStatus.ManagerDecline)
            {
                return -4;
            }
            if (registerInfo.Status == DayOffStatus.ManagerApprove ||
                registerInfo.Status == DayOffStatus.ApproverApprove)
            {
                return -2;
            }
            if (register.ToDate.HasValue)
            {
                register.ToDate = register.ToDate.Value.AddDays(1).AddMinutes(-1);
            }
            foreach (var registerDate in register.Dates)
            {
                var isExists = await CheckExists(register.Id, register.EnrollNumber, registerDate.ShiftId, registerDate.Date);
                if (isExists)
                    return -3;

                registerDate.Day = registerDate.Date.Day;
                registerDate.Month = registerDate.Date.Month;
                registerDate.Year = registerDate.Date.Year;
            }

            var updateBuilder = Builders<DayOff>.Update;
            var update = updateBuilder.Set(x => x.FromDate, register.FromDate)
                .Set(x => x.ToDate, register.ToDate)
                .Set(x => x.Dates, register.Dates)
                .Set(x => x.Reason, register.Reason)
                .Set(x => x.TotalDays, register.TotalDays);

            if (registerInfo.Status == DayOffStatus.Cancel)
            {
                update = updateBuilder.Combine(updateBuilder.Set(x => x.Status, DayOffStatus.WaitingManagerApprove));
            }

            return await _dayOffRepository.UpdateAsync(x => x.Id == register.Id, update);
        }

        public async Task<long> Delete(string id)
        {
            var info = await GetInfo(id);
            if (info == null)
                return -1;

            if (info.Status > DayOffStatus.WaitingManagerApprove)
                return -2;

            return await _dayOffRepository.DeleteAsync(id);
        }

        public async Task<long> Approve(DayOffApproveStatusModel dayOffApprove)
        {
            var info = await GetInfo(dayOffApprove.Id);
            if (info == null)
                return -1;

            if (info.ManagerUserId != dayOffApprove.CurrentUserId || info.ApproverUserId != dayOffApprove.CurrentUserId)
                return -2;

            foreach (var dayOffDate in dayOffApprove.Dates)
            {
                if (dayOffApprove.Status == DayOffStatus.ManagerApprove && dayOffDate.Method.HasValue && dayOffDate.Method != DayOffMethod.HolidayLeave && dayOffDate.Method != DayOffMethod.WeekLeave)
                {
                    dayOffDate.IsManagerApprove = dayOffDate.Method != DayOffMethod.HolidayLeave
                                                  && dayOffDate.Method != DayOffMethod.UnAuthorizedLeave && dayOffDate.Method != DayOffMethod.WeekLeave
                                                  && dayOffDate.IsManagerApprove.HasValue && dayOffDate.IsManagerApprove.Value;
                }
                if (dayOffApprove.Status == DayOffStatus.ApproverApprove && dayOffDate.Method.HasValue && dayOffDate.Method != DayOffMethod.HolidayLeave && dayOffDate.Method != DayOffMethod.WeekLeave)
                {
                    dayOffDate.IsApproverApprove = dayOffDate.Method != DayOffMethod.HolidayLeave
                                                   && dayOffDate.Method != DayOffMethod.UnAuthorizedLeave && dayOffDate.Method != DayOffMethod.WeekLeave
                                                   && dayOffDate.IsApproverApprove.HasValue && dayOffDate.IsApproverApprove.Value;
                }

                dayOffDate.Day = dayOffDate.Date.Day;
                dayOffDate.Month = dayOffDate.Date.Month;
                dayOffDate.Year = dayOffDate.Date.Year;
            }
            return await UpdateApproveStatus(dayOffApprove);
        }

        public async Task<long> ApproveAll(DayOffApproveAllModel dayOffApprove, string currentUserId)
        {
            var dayOffInfo = await GetInfo(dayOffApprove.Id);
            if (dayOffInfo == null)
                return -1;

            if (dayOffInfo.ManagerUserId != currentUserId && dayOffInfo.ApproverUserId != currentUserId)
            {
                return -403;
            }

            if (dayOffInfo.Status == DayOffStatus.ManagerApprove ||
                dayOffInfo.Status == DayOffStatus.ApproverApprove)
            {
                return -3;
            }

            // Check total annual remain.
            var totalAnnual = dayOffInfo.Dates.Where(x => x.Method == (byte)DayOffMethod.AnnualLeave)
                .Sum(x => x.ShiftWorkUnit);
            if (dayOffApprove.IsApprove)
            {
                //var userInfo = await _userRepository.GetInfo(dayOffInfo.UserId);
                if (totalAnnual > dayOffApprove.UserHolidayRemaining)
                {
                    return -4;
                }
            }

            var updateBuilder = Builders<DayOff>.Update;
            var updateFilter = Builders<DayOff>.Filter.Where(x => x.Id == dayOffApprove.Id
                                                                  && x.Dates.Any(d =>
                                                                      d.Method.HasValue &&
                                                                      d.Method.Value != DayOffMethod.WeekLeave &&
                                                                      d.Method.Value !=
                                                                      DayOffMethod.UnAuthorizedLeave));
            // Is Manager
            if (dayOffInfo.ManagerUserId == currentUserId)
            {
                var status = DayOffStatus.ManagerApprove;
                if (dayOffApprove.IsApprove && dayOffInfo.TotalDays >= 5)
                {
                    // If have no approver config status will be ManagerApprove.
                    status = !string.IsNullOrEmpty(dayOffInfo.ApproverUserId) ? DayOffStatus.ManagerApproveWaitingApproverApprove : DayOffStatus.ManagerApprove;
                }
                if (!dayOffApprove.IsApprove)
                {
                    status = DayOffStatus.ManagerDecline;
                }

                var updateQuery = updateBuilder.Set(x => x.Status, status)
                    .Set(x => x.ManagerNote, dayOffApprove.Note)
                    .Set(x => x.ManagerDeclineReason, dayOffApprove.Reason);

                if (status == DayOffStatus.ManagerApprove)
                {

                    foreach (var dayOffDate in dayOffInfo.Dates)
                    {
                        dayOffDate.IsManagerApprove = true;
                    }
                    updateQuery = updateQuery.Set(x => x.Dates, dayOffInfo.Dates)
                        .Set(x => x.TotalApprovedDays, dayOffInfo.TotalDays);
                }

                var result = await _dayOffRepository.UpdateManyAsync(updateFilter, updateQuery);
                if (result <= 0)
                    return result;

                // TODO: Check.
                // Update holidays remain.                                
                //if (dayOffApprove.IsApprove && totalAnnual > 0)
                //    await _userRepository.UpdateUserHoliday(dayOffInfo.UserId, totalAnnual);

                if (status == DayOffStatus.ManagerApprove)
                {
                    // TODO: Check
                    //var userInfo = await _userRepository.GetInfo(dayOffInfo.UserId, true);
                    //if (userInfo == null)
                    //    return result;

                    //var dayOffInfoAfterUpdate = await GetInfo(dayOffInfo.Id);
                    //var listReportByShift = (from day in dayOffInfoAfterUpdate.Dates
                    //                         where day?.Method != null && ((day.IsManagerApprove.HasValue && day.IsManagerApprove.Value)
                    //                                                       || (day.IsApproverApprove.HasValue && day.IsApproverApprove.Value))
                    //                         select new ReportByShift
                    //                         {
                    //                             Day = day.Day,
                    //                             Month = day.Month,
                    //                             Year = day.Year,
                    //                             EnrollNumber = dayOffInfo.EnrollNumber,
                    //                             FullName = dayOffInfo.FullName,
                    //                             UserId = dayOffInfo.UserId,
                    //                             Type = ReportByShiftType.Manual,
                    //                             OfficeId = dayOffInfo.OfficeId,
                    //                             OfficeIdPath = dayOffInfo.OfficeIdPath,
                    //                             Method = day.Method,
                    //                             UnsignName = dayOffInfo.UnsignName,
                    //                             WorkUnit = day.ShiftWorkUnit,
                    //                             ShiftId = day.ShiftId,
                    //                             ShiftCode = day.ShiftCode,
                    //                             ShiftReportName = day.ShiftReportName,
                    //                             IsValid = true
                    //                         }).ToList();

                    //await _reportByShiftRepository.InsertFromDayOffRegister(listReportByShift);
                }
                return result;
            }

            // Is Approver
            if (dayOffInfo.ApproverUserId == currentUserId)
            {
                var updateQuery = updateBuilder.Set(x => x.Status,
                        dayOffApprove.IsApprove
                            ? DayOffStatus.ApproverApprove
                            : DayOffStatus.ApproverDecline)
                    .Set(x => x.ApproverNote, dayOffApprove.Note)
                    .Set(x => x.ApproverDeclineReason, dayOffApprove.Reason);

                if (dayOffApprove.IsApprove)
                {
                    //updateQuery = updateQuery.Set("Dates.$.IsApproverApprove", true).Set(x => x.TotalApprovedDays, dayOffInfo.TotalDays);
                    foreach (var dayOffDate in dayOffInfo.Dates)
                    {
                        dayOffDate.IsManagerApprove = true;
                    }
                    updateQuery = updateQuery.Set(x => x.Dates, dayOffInfo.Dates)
                        .Set(x => x.TotalApprovedDays, dayOffInfo.TotalDays);
                }
                var result = await _dayOffRepository.UpdateManyAsync(updateFilter, updateQuery);
                if (result <= 0)
                    return result;

                // TODO: Check
                // Update holidays remain.
                //if (totalAnnual > 0 && dayOffApprove.IsApprove)
                //{
                //    await _userRepository.UpdateUserHoliday(dayOffInfo.UserId, totalAnnual);
                //}

                if (!dayOffApprove.IsApprove) return result;

                // TODO: Check
                //var userInfo = await _userRepository.GetInfo(dayOffInfo.UserId, true);
                //if (userInfo == null)
                //    return result;

                //var dayOffAfterUpdateInfo = await GetInfo(dayOffInfo.Id);
                //var listReportByShift = (from day in dayOffAfterUpdateInfo.Dates
                //                         where day?.Method != null && ((day.IsManagerApprove.HasValue && day.IsManagerApprove.Value)
                //                                                       || (day.IsApproverApprove.HasValue && day.IsApproverApprove.Value))
                //                         select new ReportByShift
                //                         {
                //                             Day = day.Day,
                //                             Month = day.Month,
                //                             Year = day.Year,
                //                             EnrollNumber = dayOffInfo.EnrollNumber,
                //                             FullName = dayOffInfo.FullName,
                //                             UserId = dayOffInfo.UserId,
                //                             Type = ReportByShiftType.Manual,
                //                             OfficeId = dayOffInfo.OfficeId,
                //                             OfficeIdPath = dayOffInfo.OfficeIdPath,
                //                             Method = day.Method,
                //                             UnsignName = dayOffInfo.UnsignName,
                //                             WorkUnit = day.ShiftWorkUnit,
                //                             ShiftId = day.ShiftId,
                //                             ShiftCode = day.ShiftCode,
                //                             ShiftReportName = day.ShiftReportName,
                //                             IsValid = true
                //                         }).ToList();

                //await _reportByShiftRepository.InsertFromDayOffRegister(listReportByShift);
                return result;
            }
            return 0;
        }

        public async Task<long> Cancel(string id)
        {
            return await _dayOffRepository.UpdateAsync(x => x.Id.Equals(id),
                Builders<DayOff>.Update.Set(x => x.Status, DayOffStatus.Cancel));
        }

        public async Task<DayOff> GetInfo(string id)
        {
            return await _dayOffRepository.GetAsync(x => x.Id.Equals(id));
        }

        public async Task<List<DayOff>> GetListDayOffRegister(int enrollNumber, DateTime day)
        {
            //var utlDateTime = DateTime.SpecifyKind(day, DateTimeKind.Utc);
            return await _dayOffRepository
                .GetsAsync(x => (x.FromDate == day || x.FromDate <= day && day <= x.ToDate)
                                && x.EnrollNumber == enrollNumber);
        }

        public async Task<long> InsertApprovedDayOffIntoReportByShift(int enrollNumber, DateTime day)
        {
            //var userInfo = await _userRepository.GetInfo(enrollNumber, true);
            //if (userInfo == null)
            //    return -1;

            var utlDateTime = DateTime.SpecifyKind(day, DateTimeKind.Utc);
            var listDayOffApproved = await _dayOffRepository
                .GetsAsync(x => (x.Status == DayOffStatus.ManagerApprove || x.Status == DayOffStatus.ApproverApprove)
                                      && (x.FromDate == utlDateTime || x.FromDate <= utlDateTime && utlDateTime <= x.ToDate)
                                      && x.EnrollNumber == enrollNumber);

            if (!listDayOffApproved.Any())
                return -2;

            var listReportByShifts = new List<ReportByShift>();
            listDayOffApproved.ForEach(dayOffApproved =>
            {
                var daysForInsert = dayOffApproved?.Dates.Where(x => x.Day == day.Day && x.Month == day.Month && x.Year == day.Year).ToList();
                if (daysForInsert != null && daysForInsert.Any())
                {
                    listReportByShifts.AddRange(from dayOffDate in daysForInsert
                                                where dayOffDate != null && dayOffDate.Method == DayOffMethod.WeekLeave
                                                select new ReportByShift
                                                {
                                                    Day = dayOffDate.Day,
                                                    Month = dayOffDate.Month,
                                                    Year = dayOffDate.Year,
                                                    EnrollNumber = dayOffApproved.EnrollNumber,
                                                    FullName = dayOffApproved.FullName,
                                                    UserId = dayOffApproved.Id,
                                                    Type = ReportByShiftType.Manual,
                                                    OfficeId = dayOffApproved.OfficeId,
                                                    OfficeIdPath = dayOffApproved.OfficeIdPath,
                                                    Method = dayOffDate.Method,
                                                    UnsignName = dayOffApproved.UnsignName,
                                                    WorkUnit = dayOffDate.ShiftWorkUnit,
                                                    ShiftId = dayOffDate.ShiftId,
                                                    ShiftCode = dayOffDate.ShiftCode,
                                                    ShiftReportName = dayOffDate.ShiftReportName,
                                                });
                }
            });

            if (!listReportByShifts.Any()) return -3;

            //await _reportByShiftRepository.InsertFromDayOffRegister(listReportByShifts);
            return 1;
        }

        public async Task<bool> CheckExists(string id, int enrollNumber, string shiftId, DateTime day)
        {
            return await _dayOffRepository.ExistsAsync(x => x.Id != id && x.EnrollNumber == enrollNumber &&
                x.Dates.Any(t => t.ShiftId.Equals(shiftId)) && x.Status != DayOffStatus.ApproverApprove
                && x.Status != DayOffStatus.ManagerApprove && x.FromDate <= day && x.ToDate >= day);
        }

        #region Private
        private async Task<long> UpdateApproveStatus(DayOffApproveStatusModel dayOffApprove)
        {
            switch (dayOffApprove.Type)
            {
                case DayOffApproveType.Manager:
                    return await _dayOffRepository.UpdateAsync(x => x.Id == dayOffApprove.Id, Builders<DayOff>.Update
                        .Set(x => x.Status, dayOffApprove.Status)
                        .Set(x => x.ManagerNote, dayOffApprove.ManagerNote)
                        .Set(x => x.TotalApprovedDays, dayOffApprove.Dates.Where(d => d.Method.HasValue && d.Method.Value != DayOffMethod.WeekLeave && d.Method.Value != DayOffMethod.UnAuthorizedLeave
                                                                                      && d.IsManagerApprove.HasValue && d.IsManagerApprove.Value).Sum(d => d.ShiftWorkUnit))
                        .Set(x => x.ManagerDeclineReason, dayOffApprove.Status == DayOffStatus.ManagerDecline ? dayOffApprove.ManagerDeclineReason : "")
                        .Set(x => x.ManagerApproveTime, DateTime.Now)
                        .Set(x => x.Dates, dayOffApprove.Dates));
                case DayOffApproveType.Approver:
                    return await _dayOffRepository.UpdateAsync(x => x.Id == dayOffApprove.Id, Builders<DayOff>.Update
                        .Set(x => x.Status, dayOffApprove.Status)
                        .Set(x => x.ApproverNote, dayOffApprove.ApproverNote)
                        .Set(x => x.TotalApprovedDays, dayOffApprove.Dates.Where(d => d.Method.HasValue && d.Method.Value != DayOffMethod.WeekLeave && d.Method.Value != DayOffMethod.UnAuthorizedLeave
                                                                                      && d.IsApproverApprove.HasValue && d.IsApproverApprove.Value).Sum(d => d.ShiftWorkUnit))
                        .Set(x => x.ApproverDeclineReason, dayOffApprove.Status == DayOffStatus.ApproverDecline ? dayOffApprove.ApproverDeclineReason : "")
                        .Set(x => x.ApproverApproveTime, DateTime.Now)
                        .Set(x => x.Dates, dayOffApprove.Dates));
                default:
                    return -1;
            }
        }
        #endregion
    }
}
