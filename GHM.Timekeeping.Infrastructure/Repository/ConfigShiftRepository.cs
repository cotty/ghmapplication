﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Domain.IRepository;
using GHM.Timekeeping.Domain.Models;
using MongoDB.Driver;

namespace GHM.Timekeeping.Infrastructure.Repository
{
    public class ConfigShiftRepository : MongoDbRepositoryBase, IConfigShiftRepository
    {
        private readonly IMongoDbRepository<Shift, string> _timekeeingShiftRepository;
        private readonly IMongoDbRepository<ShiftGroup, string> _timekeeingShiftGroupRepository;
        public ConfigShiftRepository(IMongoDbContext context) : base(context)
        {
            _timekeeingShiftRepository = Context.GetRepository<Shift, string>();
            _timekeeingShiftGroupRepository = Context.GetRepository<ShiftGroup, string>();
        }

        #region Shift
        public async Task<long> Insert(Shift shift)
        {
            var nameExists = await CheckShiftNameExists(shift.Id, shift.Name);
            if (nameExists)
                return -1;

            var codeExists = await CheckShiftCodeExists(shift.Id, shift.Code, shift.Name);
            if (codeExists)
                return -2;

            await _timekeeingShiftRepository.AddAsync(shift);
            return 1;
        }

        public async Task<long> Update(Shift shift)
        {
            var nameExists = await CheckShiftNameExists(shift.Id, shift.Name);
            if (nameExists)
                return -1;

            var codeExists = await CheckShiftCodeExists(shift.Id, shift.Code, shift.Name);
            if (codeExists)
                return -2;

            return await _timekeeingShiftRepository.UpdateAsync(shift);
        }


        // TODO: Kiểm tra các điều kiện để xóa 1 ca làm việc
        public Task<long> Delete(string id)
        {
            throw new System.NotImplementedException();
        }

        public async Task<List<Shift>> SearchAll()
        {
            return await _timekeeingShiftRepository.GetsAsync(x => !x.IsDelete);
        }

        #endregion

        #region Shift group
        public async Task<ShiftGroup> InsertGroup(ShiftGroup shiftGroup)
        {
            var nameExists = await CheckGroupNameExists(shiftGroup.Id, shiftGroup.Name);
            if (nameExists)
                return null;

            return await _timekeeingShiftGroupRepository.AddAsync(shiftGroup);
        }

        public async Task<long> UpdateGroup(ShiftGroup shiftGroup)
        {
            var nameExists = await CheckGroupNameExists(shiftGroup.Id, shiftGroup.Name);
            if (nameExists)
                return -1;

            return await _timekeeingShiftGroupRepository.UpdateAsync(shiftGroup);
        }

        public async Task<long> UpdateGroupActive(string id, bool isActive)
        {
            return await _timekeeingShiftGroupRepository.UpdateAsync(x => x.Id.Equals(id),
                Builders<ShiftGroup>.Update.Set("isActive", isActive));
        }

        public async Task<long> DeleteGroup(string id)
        {
            return await _timekeeingShiftGroupRepository.UpdateAsync(x => x.Id.Equals(id), Builders<ShiftGroup>.Update.Set("isDelete", true));
        }

        public async Task<ShiftGroup> GetGroupInfo(string id)
        {
            return await _timekeeingShiftGroupRepository.GetAsync(id);
        }

        public async Task<List<ShiftGroup>> SearchAllGroup(bool? isActive)
        {
            Expression<Func<ShiftGroup, bool>> spec = x => !x.IsDelete;
            if (isActive.HasValue)
            {
                spec = spec.And(x => x.IsActive == isActive.Value);
            }
            return await _timekeeingShiftGroupRepository.GetsAsync(spec);
        }

        private async Task<bool> CheckGroupNameExists(string id, string name)
        {
            return await _timekeeingShiftGroupRepository.ExistsAsync(x => !x.Id.Equals(id) && x.Name.Equals(name));
        }

        private async Task<bool> CheckShiftCodeExists(string id, string code, string name)
        {
            return await _timekeeingShiftRepository.ExistsAsync(x => !x.Id.Equals(id) && x.Code.Equals(code) && x.Name.Equals(name));
        }

        private async Task<bool> CheckShiftNameExists(string id, string name)
        {
            return await _timekeeingShiftRepository.ExistsAsync(x => !x.Id.Equals(id) && x.Name.Equals(name));
        }
        #endregion
    }
}
