﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GHM.Infrastructure.Helpers;
using GHM.Infrastructure.MongoDb;
using GHM.Timekeeping.Domain.IRepository;
using GHM.Timekeeping.Domain.Models;
using MongoDB.Driver;

namespace GHM.Timekeeping.Infrastructure.Repository
{
    // TODO: Check app config.
    public class InLateOutEarlyRepository : MongoDbRepositoryBase, IInLateOutEarlyRepository
    {
        private readonly IMongoDbRepository<InLateOutEarly, string> _inLateOutEarlyRepository;        
        private readonly IShiftRepository _shiftRepository;
        //private readonly IAppConfigRepository _appConfigRepository;

        public InLateOutEarlyRepository(IMongoDbContext context, IShiftRepository shiftRepository
            //IAppConfigRepository appConfigRepository
            ) : base(context)
        {
            _shiftRepository = shiftRepository;
            //_appConfigRepository = appConfigRepository;
            _inLateOutEarlyRepository = Context.GetRepository<InLateOutEarly, string>();
        }

        public Task<List<InLateOutEarly>> Search(string userId, string currentUserId, byte type, int month, int year, bool? isConfirm, int page, int pageSize, out long totalRows)
        {
            Expression<Func<InLateOutEarly, bool>> spec = x => x.Month == month && x.Year == year && !x.IsDelete;
            if (!string.IsNullOrEmpty(userId))
            {
                spec = spec.And(x => x.UserId.Equals(userId));
            }

            if (type == 0)
            {
                spec = spec.And(x => x.UserId.Equals(currentUserId));
            }

            if (type == 1)
            {
                spec = spec.And(x => x.ManagerUserId == currentUserId);
            }

            if (type == 2)
            {
                spec = spec.And(x => x.CreatorId == currentUserId && !x.UserId.Equals(currentUserId));
            }

            if (isConfirm.HasValue)
            {
                spec = spec.And(x => x.IsConfirmed == isConfirm.Value);
            }

            var sort = Context.Filters.Sort<InLateOutEarly, string>(x => x.Id, true);
            var paging = Context.Filters.Page<InLateOutEarly>(page, pageSize);

            totalRows = _inLateOutEarlyRepository.Count(spec);
            return _inLateOutEarlyRepository.GetsAsync(spec, sort, paging);
        }

        public async Task<long> Insert(InLateOutEarly inLateOutEarly, string currentUserId)
        {
            foreach (var shift in inLateOutEarly.Shifts)
            {
                var isExists = await CheckExists(inLateOutEarly.Id, inLateOutEarly.UserId, shift.IsInLate, inLateOutEarly.Day, inLateOutEarly.Month, inLateOutEarly.Year,
                    shift.ShiftId);
                if (isExists)
                    return -1;

                var shiftInfo = await _shiftRepository.GetInfo(shift.ShiftId);
                if (shiftInfo == null)
                    return -3;

                shift.ShiftReportName = shiftInfo.ReportName;
                shift.ShiftCode = shiftInfo.Code;
                shift.StartTime = shiftInfo.StartTime;
                shift.EndTime = shiftInfo.EndTime;
            }
            await _inLateOutEarlyRepository.AddAsync(inLateOutEarly);
            return 1;
        }

        public async Task<long> Update(InLateOutEarly inLateOutEarly, string currentUserId)
        {
            var info = await GetInfo(inLateOutEarly.Id);
            if (info == null)
                return -1;

            if (info.CreatorId != currentUserId)
                return -5;

            if (info.IsConfirmed)
                return -4;

            foreach (var shift in inLateOutEarly.Shifts)
            {
                var isExists = await CheckExists(inLateOutEarly.Id, info.UserId, shift.IsInLate, inLateOutEarly.Day, inLateOutEarly.Month, inLateOutEarly.Year,
                    shift.ShiftId);
                if (isExists)
                    return -2;

                var shiftInfo = await _shiftRepository.GetInfo(shift.ShiftId);
                if (shiftInfo == null)
                    return -3;

                shift.ShiftReportName = shiftInfo.ReportName;
                shift.ShiftCode = shiftInfo.Code;
                shift.StartTime = shiftInfo.StartTime;
                shift.EndTime = shiftInfo.EndTime;
            }

            return await _inLateOutEarlyRepository.UpdateAsync(x => x.Id == inLateOutEarly.Id, Builders<InLateOutEarly>
                .Update
                .Set(x => x.RegisterDate, inLateOutEarly.RegisterDate)
                .Set(x => x.Day, inLateOutEarly.RegisterDate.Day)
                .Set(x => x.Month, inLateOutEarly.RegisterDate.Month)
                .Set(x => x.Year, inLateOutEarly.RegisterDate.Year)
                .Set(x => x.Shifts, inLateOutEarly.Shifts)
            );
        }

        public async Task<long> UpdateConfirmStatus(string id)
        {
            return await _inLateOutEarlyRepository.UpdateAsync(x => x.Id.Equals(id), Builders<InLateOutEarly>.Update
                .Set(x => x.IsConfirmed, true)
                .Set(x => x.ConfirmDateTime, DateTime.Now));
        }

        public async Task<long> Approve(InLateOutEarlyUpdateApproveStatusModel inLateOutEarly)
        {
            foreach (var shift in inLateOutEarly.Shifts)
            {
                var updateBuilder = Builders<InLateOutEarly>.Filter;
                var updateFilter = updateBuilder.Where(x => x.Id == inLateOutEarly.Id) & updateBuilder.ElemMatch(x => x.Shifts, s => s.IsInLate == shift.IsInLate && s.ShiftId == shift.ShiftId);
                var updateFields = Builders<InLateOutEarly>.Update.Set("shifts.$.isApprove", shift.IsApprove)
                    .Set("shifts.$.declineReason", !shift.IsApprove ? shift.DeclineReason : string.Empty);
                await _inLateOutEarlyRepository.UpdateAsync(updateFilter, updateFields);
            }

            return await UpdateConfirmStatus(inLateOutEarly.Id);
        }

        public async Task<long> Delete(string id)
        {
            // Check isConfirmed
            var isConfirmed = await _inLateOutEarlyRepository.ExistsAsync(x => x.Id.Equals(id) && x.IsConfirmed);
            if (isConfirmed)
            {
                return -1;
            }
            return await _inLateOutEarlyRepository.UpdateAsync(x => x.Id.Equals(id), Builders<InLateOutEarly>.Update.Set(x => x.IsDelete, true));
        }

        public async Task<InLateOutEarly> GetInfo(string id)
        {
            return await _inLateOutEarlyRepository.GetAsync(x => x.Id == id && !x.IsDelete);
        }

        public async Task<InLateOutEarly> GetInfo(int enrollNumber, int day, int month, int year, string shiftId)
        {
            return await _inLateOutEarlyRepository.GetAsync(
                x => x.EnrollNumber == enrollNumber && x.Day == day && x.Month == month && x.Year == year && !x.IsDelete);
        }

        public async Task<bool> CheckIsAllowAddMore(string id, string userId, int month, int year)
        {
            var totalRegistered = await GetTotalApprovedInLateOutEarlyTimes(userId, month, year);
            //var config = await _appConfigRepository.GetByGroup("TimekeepingGeneralConfig");
            var maxInOutTimes = 3;
            //var maxInOut = config?.FirstOrDefault(x => x.Key.Equals($"{typeof(TimekeepingGeneralConfig).Namespace}.{Nameof<TimekeepingGeneralConfig>.Property(t => t.MaxInOutTimes)}"));
            //if (maxInOut != null)
            //{
            //    maxInOutTimes = int.Parse(maxInOut.Value);
            //}
            return totalRegistered < maxInOutTimes;
        }

        public async Task<bool> CheckIsValidMin(int min)
        {
            //var config = await _appConfigRepository.GetByGroup("TimekeepingGeneralConfig");
            var maxMin = 60;
            //var maxMinConfig =
            //    config?.FirstOrDefault(x => x.Key.Equals(
            //        $"{typeof(TimekeepingGeneralConfig).Namespace}.{Nameof<TimekeepingGeneralConfig>.Property(t => t.MaxInOutMin)}"));

            //if (maxMinConfig != null)
            //    maxMin = int.Parse(maxMinConfig.Value);

            return min <= maxMin;
        }

        public async Task<bool> CheckExists(string id, string userId, bool isInLate, int day, int month, int year, string shiftId)
        {
            return await _inLateOutEarlyRepository.ExistsAsync(x =>
                !x.Id.Equals(id) && x.UserId.Equals(userId) && x.Day == day && x.Month == month &&
                x.Year == year && !x.IsDelete && x.Shifts.Any(s => s.IsInLate == isInLate && s.ShiftId == shiftId));
        }

        public int GetTotalInLatencyMin(int enrollNumber, int month, int year)
        {
            //return _inLateOutEarlyRepository.Raw
            //    .Where(x => x.EnrollNumber == enrollNumber && x.Month == month && x.Year == year && x.IsInLate && !x.IsDelete)
            //    .Sum(x => x.InOutMin);
            return 1;
        }

        public int GetTotalOutLatencyMin(int enrollNumber, int month, int year)
        {
            //return _inLateOutEarlyRepository.Raw
            //    .Where(x => x.EnrollNumber == enrollNumber && x.Month == month && x.Year == year && !x.IsInLate && !x.IsDelete)
            //    .Sum(x => x.InOutMin);
            return 1;
        }

        public async Task<int> GetTotalApprovedInLateOutEarlyTimes(string userId, int month, int year)
        {
            var approvedShifts = await _inLateOutEarlyRepository.GetsAsync(x =>
                x.UserId == userId && x.Month == month && x.Year == year
                && x.Shifts.Any(s => s.IsApprove.HasValue && s.IsApprove.Value));
            return approvedShifts.Any()
                ? approvedShifts.Sum(x => x.Shifts.Count(s => s.IsApprove.HasValue && s.IsApprove.Value)) : 0;
        }
    }
}
