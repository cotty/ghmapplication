﻿using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Warehouse.Domain.ModelMetas;
using GHM.Warehouse.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Warehouse.Domain.IServices
{
    public interface IWebsiteCategoryService
    {
        Task<SearchResult<TreeData>> GetTreeAsync(string tenantId, string websiteId, string keyword, string languageId, bool isActive);

        Task<ActionResultResponse<WebsiteProductCategoryViewModel>> GetDetailAsync(string tenantId, string websiteId, int id);

        Task<ActionResultResponse<int>> InsertAsync(string tenantId, string websiteId, WebsiteCategoryMeta websiteCategoryMeta);

        Task<ActionResultResponse<string>> Update(string tenantId, string websiteId, int id, WebsiteCategoryMeta websiteCategoryMeta);

        Task<ActionResultResponse> Delete(string tenantId, string websiteId, int id);

        Task<SearchResult<Suggestion<int>>> Suggestions(string tenantId, string websiteId, string keyword, string languageId, int page, int pageSize);

        Task<SearchResult<WebsiteCategorySearchForSelectViewModel>> SearchForSelect(string tenantId, string websiteId, string keyword, string languageId, int page, int pageSize);
    }
}
