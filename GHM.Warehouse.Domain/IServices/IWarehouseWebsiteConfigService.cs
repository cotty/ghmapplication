﻿using GHM.Infrastructure.ViewModels;
using GHM.Warehouse.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Warehouse.Domain.IServices
{
    public interface IWarehouseWebsiteConfigService
    {
        Task<SearchResult<WarehouseWebsiteConfig>> SearchAsync(string tenantId, string warehouseId, string websiteId, int page, int pageSize);
    }
}
