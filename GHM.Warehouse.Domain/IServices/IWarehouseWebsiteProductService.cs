﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GHM.Infrastructure.Models;
using GHM.Infrastructure.ViewModels;
using GHM.Warehouse.Domain.ModelMetas;
using GHM.Warehouse.Domain.ViewModels;

namespace GHM.Warehouse.Domain.IServices
{
    public interface IWarehouseWebsiteProductService
    {
        Task<ActionResultResponse> UpdateAsync(string tenantId, string websiteId, string productId, string languageId, WebsiteProductMeta websiteProductMeta);

        Task<ActionResultResponse> DeleteAsync(string tenantId, string websiteId, string productId);

        Task<SearchResult<WebsiteProductViewModel>> SearchAsync(string tenantId, string websiteId, int? categoryId, string productName, string languageId, int page, int pageSize);

        Task<ActionResultResponse<WebsiteProductDetailViewModel>> GetDetailAsync(string tenantId, string websiteId, string productId, string languageId);

        Task<SearchResult<WebsiteProductSuggestViewModel>> SearchForSuggestion(string tenantId, string warehouseId, string websiteId, string productId);

        Task<ActionResultResponse> UpdateHot(string tenantId, string websiteId, string productid, bool isHot);

        Task<ActionResultResponse> UpdateHomePage(string tenantId, string websiteId, string productid, bool isHomePage);

        Task<SearchResult<ProductSearchForSelectViewModel>> SearchForSelect(string tenantId, string name, string websiteId, string keyword, int? categoryId, int page, int pageSize);
    }
}
