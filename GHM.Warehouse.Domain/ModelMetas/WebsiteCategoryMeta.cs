﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.ModelMetas
{
    public class WebsiteCategoryMeta
    {
        public bool IsActive { get; set; }
        public int? ParentId { get; set; }
        public string BannerImage { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
        public string CreatorAvatar { get; set; }
        public string ConcurrencyStamp { get; set; }
        public int Order { get; set; }
        public bool? IsHomePage { get; set; }
        public List<WebsiteCategoryTranslationMeta> CategoryTranslations { get; set; }
    }
}
