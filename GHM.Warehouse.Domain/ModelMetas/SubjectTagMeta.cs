﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.ModelMetas
{
    public class SubjectTagMeta
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
