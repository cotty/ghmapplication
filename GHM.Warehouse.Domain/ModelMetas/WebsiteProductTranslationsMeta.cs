﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.ModelMetas
{
    public class WebsiteProductTranslationsMeta
    {
        public string LanguageId { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }
        public string SeoLink { get; set; }
        public string Content { get; set; }
        public string Alt { get; set; }
        public List<SubjectTagMeta> Tags { get; set; }
    }
}
