﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.ModelMetas
{
    public class WebsiteProductMeta
    {
        public bool IsHot { get; set; }
        public bool IsHomePage { get; set; }
        public string CreatorId { get; set; }
        public List<int> Categories { get; set; }
        public string CreatorFullName { get; set; }
        public string CreatorAvatar { get; set; }
        public List<WebsiteProductTranslationsMeta> ModelTranslations { get; set; }
    }
}
