﻿using GHM.Warehouse.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Warehouse.Domain.IRepository
{
    public interface IWebsiteProductTranslationRepository
    {
        Task<WebsiteProductTranslation> GetInfo(string tenantId, string warehouseId, string websiteId, string productId, string languageId, bool isReadOnly);

        Task<bool> CheckSeoLinkExistAsync(string tenantId, string websiteId, string seoLink);

        Task<int> Update(WebsiteProductTranslation infoTranslation);

        Task<int> Insert(WebsiteProductTranslation websiteProductTranslation);
    }
}
