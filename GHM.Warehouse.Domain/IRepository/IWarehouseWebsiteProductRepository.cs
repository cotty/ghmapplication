﻿using GHM.Infrastructure.Models;
using GHM.Warehouse.Domain.Models;
using GHM.Warehouse.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Warehouse.Domain.IRepository
{
    public interface IWarehouseWebsiteProductRepository
    {
        Task<WebsiteProduct> GetDetail(string tenantId, string warehouseId, string websiteId, string productId, bool isReadOnly);

        Task<int> Delete(WebsiteProduct info);

        Task<WebsiteProductDetailViewModel> GetDetails(string tenantId, string warehouseId, string websiteId, string productId, string languageId);

        Task<List<WebsiteProductViewModel>> Search(string tenantId, string warehouseId, string websiteId, int? categoryId, string productName, string languageId, int page, int pageSize, out int totalRows);

        Task<int> Insert(WebsiteProduct websiteProduct);

        Task<int> Update(WebsiteProduct info);

        Task<bool> CheckProductExistAsync(string tenantId, string warehouseId, string websiteId, string productId);

        Task<List<WebsiteProductSuggestViewModel>> SearchForSuggestion(string tenantId, string warehouseId, string websiteId, string productId);

        Task<List<ProductSearchForSelectViewModel>> SearchForSelectAsync(string tenantId, string warehouseId, string websiteId, string keyword, string languageId, int page, int pageSize, out int totalRows);
    }
}
