﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GHM.Warehouse.Domain.Models;

namespace GHM.Warehouse.Domain.IRepository
{
    public interface IWebsiteCategoryTranslationRepository
    {
        Task<bool> CheckExists(string tenantId, string websiteId, string languageId, int categoryId, string name);

        Task<bool> CheckSeoLinkExists(string tenantId, string websiteId, string languageId, int categoryId, string seoLink);

        Task<int> Inserts(List<WebsiteProductCategoryTranslation> websiteCategoryTranslation);

        Task<WebsiteProductCategoryTranslation> GetInfo(string websiteId, int id, string languageId);

        Task<int> Update(WebsiteProductCategoryTranslation categoryTranslationInfo);

        Task<List<WebsiteProductCategoryTranslation>> GetByCategoryId(string websiteId, int id);
    }
}
