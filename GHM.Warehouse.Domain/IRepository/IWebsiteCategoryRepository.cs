﻿using GHM.Infrastructure.Models;
using GHM.Warehouse.Domain.ModelMetas;
using GHM.Warehouse.Domain.Models;
using GHM.Warehouse.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Warehouse.Domain.IRepository
{
    public interface IWebsiteCategoryRepository
    {
        Task<WebsiteProductCategory> GetInfo(string tenantId, string websiteId, int id, bool isReadOnly);

        Task<int> Delete(WebsiteProductCategory info);

        Task<WebsiteProductCategoryViewModel> GetDetail(string tenantId, string websiteId, int id);

        Task<List<CategoryViewModel>> SearchAll(string tenantId, string websiteId, string keyword, string languageId, bool? isActive);

        Task<WebsiteProductCategory> GetInfo(string tenantId, string websiteId, int? parentId, bool v);

        Task<int> CountByParentId(string websiteId, int? parentId);

        Task<int> Insert(WebsiteProductCategory category);

        Task<int> UpdateCategoryIdPathAsync(string tenantId, WebsiteCategoryMeta websiteCategoryMeta, int id, string idPath);

        Task<int> ForceDelete(string tenantId,string websiteId ,int id);

        Task<int> Update(WebsiteProductCategory info);

        Task<int> UpdateChildrenIdPath(string websiteId, string oldIdPath, string idPath);
        Task<List<WebsiteCategorySearchForSelectViewModel>> SearchForSelect(string tenantId, string websiteId, string languageId, string keyword, int page, int pageSize, out int totalRows);

        Task<List<Suggestion<int>>> Suggestions(string tenantId, string websiteId, string languageId, string keyword, int page, int pageSize, out int totalRows);
    }
}
