﻿using GHM.Warehouse.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Warehouse.Domain.IRepository
{
    public interface IWebsiteProductCategoriesRepository
    {
        Task<bool> CheckCategoryExist(string tenantId, string websiteId, int id);

        Task<int> Insert(WebsiteProductCategories websiteProductCategories);
        
        Task<int> DeleteByProductsById(string productId, string warehouseId, string tenantId, string websiteId);
    }
}
