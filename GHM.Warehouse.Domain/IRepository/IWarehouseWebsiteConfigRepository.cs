﻿using GHM.Warehouse.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GHM.Warehouse.Domain.IRepository
{
    public interface IWarehouseWebsiteConfigRepository
    {
        Task<List<WarehouseWebsiteConfig>> Search(string tenantId, string warehouseId, string websiteId, out int totalRows);

        Task<bool> CheckExist(string tenantId, string warehouseId, string websiteId);

        Task<WarehouseWebsiteConfig> GetWarehouseByWebsite(string tenant, string websiteId);

        Task<bool> CheckExist(string tenantId, string websiteId);
    }
}
