﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.Models
{
    public class WebsiteProduct
    {
        public string ProductId { get; set; }
        public string TenantId { get; set; }
        public string WarehouseId { get; set; }
        public DateTime CreateTime { get; set; }
        public int LikeCount { get; set; }
        public int CommentCount { get; set; }
        public int ViewCount { get; set; }
        public bool IsHot { get; set; }
        public DateTime? LastUpdateHot { get; set; }
        public bool IsHomePage { get; set; }
        public DateTime? LastUpdateHomePage { get; set; }
        public string WebsiteId { get; set; }
        public bool IsDelete { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
        public string LastUpdateUserId { get; set; }
        public string LastUpdateFullName { get; set; }
        public WebsiteProduct()
        {
            ConcurrencyStamp = Guid.NewGuid().ToString();
            CreateTime = DateTime.Now;
            LikeCount = 0;
            CommentCount = 0;
            IsDelete = false;
            ViewCount = 0;
        }
    }
}
