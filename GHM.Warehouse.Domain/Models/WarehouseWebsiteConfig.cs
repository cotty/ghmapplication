﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.Models
{
    public class WarehouseWebsiteConfig
    {
        public string WarehouseId { get; set; }
        public string WebsiteId { get; set; }
        public string TenantId { get; set; }
        public string Name { get; set; }
    }
}
