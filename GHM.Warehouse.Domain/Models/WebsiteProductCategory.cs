﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.Models
{
    public class WebsiteProductCategory
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string TenantId { get; set; }
        public string IdPath { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public int Order { get; set; }
        public string OrderPath { get; set; }
        public int ChildCount { get; set; }
        public string BannerImage { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string LastUpdateUserId { get; set; }
        public DateTime? LastUpdate { get; set; }
        public string LastUpdateFullName { get; set; }
        public string WebsiteId { get; set; }

        public WebsiteProductCategory()
        {
            CreateTime = DateTime.Now;
            IsDelete = false;
            ChildCount = 0;
            ConcurrencyStamp = Guid.NewGuid().ToString();
        }
    }
}
