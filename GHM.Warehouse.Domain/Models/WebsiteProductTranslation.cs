﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.Models
{
    public class WebsiteProductTranslation
    {
        public string ProductId { get; set; }
        public string TenantId { get; set; }
        public string WarehouseId { get; set; }
        public string LanguageId { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }
        public string SeoLink { get; set; }
        public string Content { get; set; }
        public string Alt { get; set; }
        public string WebsiteId { get; set; }

        public virtual WebsiteProduct WebsiteProduct { get; set; }
    }
}
