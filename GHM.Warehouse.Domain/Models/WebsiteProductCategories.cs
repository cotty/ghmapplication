﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.Models
{
    public class WebsiteProductCategories
    {
        public string ProductId { get; set; }
        public int ProductWebsiteCategoryId { get; set; }
        public string TenantId { get; set; }
        public string WebsiteId { get; set; }
        public string WarehouseId { get; set; }
    }
}
