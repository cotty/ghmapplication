﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.Models
{
    public class WebsiteProductCategoryTranslation
    {
        public string TenantId { get; set; }
        public int WebsiteProductCategoryId { get; set; }
        public string WebsiteId { get; set; }
        public string LanguageId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string UnsignName { get; set; }
        public string ParentName { get; set; }
        public bool IsDelete { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string SeoLink { get; set; }
        public string Alt { get; set; }
        public string BannerImage { get; set; }

        public WebsiteProductCategoryTranslation()
        {
            IsDelete = false;
        }
    }

}
