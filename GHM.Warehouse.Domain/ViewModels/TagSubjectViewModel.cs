﻿using GHM.Warehouse.Domain.Constants;
using GHM.Warehouse.Domain.ModelMetas;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.ViewModels
{
    public class TagSubjectViewModel
    {
        public string TenantId { get; set; }
        public string CreatorId { get; set; }
        public string CreatorFullName { get; set; }
        public string CreatorAvatar { get; set; }
        public TagType Type { get; set; }
        public string SubjectId { get; set; }
        public string LanguageId { get; set; }
        public List<SubjectTagMeta> Tags { get; set; }
    }
}
