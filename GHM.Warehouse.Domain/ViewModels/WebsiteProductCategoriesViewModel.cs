﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.ViewModels
{
    public class WebsiteProductCategoriesViewModel
    {
        public string ProductId { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
