﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.ViewModels
{
    public class ProductSearchForSelectViewModel
    {
        public string Id { get; set; }
        public string FeatureImage { get; set; }
        public string LanguageId { get; set; }
        public string Title { get; set; }
        public string SeoLink { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
