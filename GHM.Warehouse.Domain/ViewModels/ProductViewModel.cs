﻿namespace GHM.Warehouse.Domain.IRepository
{
    public class ProductViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string UnitId { get; set; }
        public string UnitName { get; set; }
    }
}
