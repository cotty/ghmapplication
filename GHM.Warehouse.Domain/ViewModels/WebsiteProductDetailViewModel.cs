﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.ViewModels
{
    public class WebsiteProductDetailViewModel
    {
        public string ProductId { get; set; }
        public bool? IsHot { get; set; }
        public bool? IsHomePage { get; set; }
        public string FeatureImage { get; set; }
        public int? LikeCount { get; set; }
        public decimal? SalePrice { get; set; }
        public int? CommentCount { get; set; }
        public int? ViewCount { get; set; }

        public List<WebsiteProductTranslationViewModel> Translations { get; set; }
        //public List<ProductImageViewModel> Images { get; set; }
        public List<WebsiteProductCategoriesViewModel> Categories { get; set; }

    }
}
