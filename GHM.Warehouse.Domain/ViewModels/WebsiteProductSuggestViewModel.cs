﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.ViewModels
{
    public class WebsiteProductSuggestViewModel
    {
        public string WebsiteId { get; set; }
        public string ProductId { get; set; }
        public string Name { get; set; }
    }
}
