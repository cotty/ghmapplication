﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.ViewModels
{
    public class WebsiteProductViewModel
    {
        public string ProductId { get; set; }
        public string WarehouseId { get; set; }
        public string WebsiteId { get; set; }
        public bool IsHot { get; set; }
        public bool IsHomePage { get; set; }
        public string Name { get; set; }
        public List<string> CategoriesName { get; set; }
        public string FeatureImage { get; set; }
        public int? LikeCount { get; set; }
        public int? CommentCount { get; set; }
        public int? ViewCount { get; set; }
        public decimal? SalePrice { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
