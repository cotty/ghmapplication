﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.ViewModels
{
    public class WebsiteCategorySearchForSelectViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string BannerImage { get; set; }
        public string SeoLink { get; set; }
    }
}
