﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.ViewModels
{
    public class ProductCategoryForSelectViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
