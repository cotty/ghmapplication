﻿using GHM.Warehouse.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GHM.Warehouse.Domain.ViewModels
{
    public class WebsiteProductCategoryViewModel
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public string ConcurrencyStamp { get; set; }
        public int? ParentId { get; set; }
        public int Order { get; set; }
        public bool? IsHomePage { get; set; }
        public string BannerImage { get; set; }
        public List<WebsiteProductCategoryTranslation> CategoryTranslations { get; set; }
    }
}
